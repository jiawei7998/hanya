package com.singlee.capital.invest.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.invest.model.TtTrdInvestFollowup;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TtTrdInvestFollowupMapper extends Mapper<TtTrdInvestFollowup>{
	
	/**
	 * 查询投后报告
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowup(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 获取投后报告序列
	 * @return
	 */
	public int getInvestFollowupSeq();
	
	/**
	 * 查询投后报告,待审批
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowupApprove(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查询投后报告,已审批
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowupFinished(Map<String, Object> params, RowBounds rb);

}
