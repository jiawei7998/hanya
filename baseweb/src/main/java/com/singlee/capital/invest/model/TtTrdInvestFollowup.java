package com.singlee.capital.invest.model;

import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 投后报告
 * @author dingzy
 *
 */
@Entity
@Table(name="TT_TRD_INVEST_FOLLOWUP")
public class TtTrdInvestFollowup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String dealNo;		//流水号
	private String refNo;		//原交易流水号
	private String dealType;	//审批类型
	private String sponsor;		//审批发起人
	private String sponInst;	//审批发起机构
	private String remark;		//备注
	private String lastUpdate;	//最后更新时间
	private String investStatus;	//审批状态
	private String explain;		//报告期数
	@Transient
	private String prdName;		//产品名称
	
	@Transient
	private String task_id;		//流程任务ID
	
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getInvestStatus() {
		return investStatus;
	}
	public void setInvestStatus(String investStatus) {
		this.investStatus = investStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	@Override
	public String toString() {
		return "TtTrdInvestFollowup [dealNo=" + dealNo + ", refNo=" + refNo + ", dealType=" + dealType + ", sponsor="
				+ sponsor + ", sponInst=" + sponInst + ", remark=" + remark + ", lastUpdate=" + lastUpdate
				+ ", investStatus=" + investStatus + ", explain=" + explain + ", prdName=" + prdName + ", task_id="
				+ task_id + ", user=" + user + ", institution=" + institution + "]";
	}
	
}
