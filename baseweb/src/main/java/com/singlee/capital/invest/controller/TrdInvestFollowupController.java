package com.singlee.capital.invest.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.invest.model.TtTrdInvestFollowup;
import com.singlee.capital.invest.service.TrdInvestFollowupService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 投后报告 Controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/investFollowupController")
public class TrdInvestFollowupController extends CommonController {
	
	@Autowired
	private TrdInvestFollowupService trdInvestFollowupService;
	
	/**
	 * 查询投后报告,我发起的
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/selectInvestFollowup")
	@ResponseBody
	public RetMsg<PageInfo<TtTrdInvestFollowup>> selectInvestFollowup(@RequestBody Map<String,Object> map){
		String sponsor = SlSessionHelper.getUserId();
		map.put("sponsor", sponsor);
		return RetMsgHelper.ok(trdInvestFollowupService.selectTtTrdInvestFollowup(map));
	}
	
	/**
	 * 查询投后报告,待审批
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/selectTtTrdInvestFollowupApprove")
	@ResponseBody
	public RetMsg<PageInfo<TtTrdInvestFollowup>> selectTtTrdInvestFollowupApprove(@RequestBody Map<String,Object> map){
		String sponsor = SlSessionHelper.getUserId();
		map.put("sponsor", sponsor);
		return RetMsgHelper.ok(trdInvestFollowupService.selectTtTrdInvestFollowupApprove(map));
	}
	
	/**
	 * 查询投后报告,已审批
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/selectTtTrdInvestFollowupFinished")
	@ResponseBody
	public RetMsg<PageInfo<TtTrdInvestFollowup>> selectTtTrdInvestFollowupFinished(@RequestBody Map<String,Object> map){
		String sponsor = SlSessionHelper.getUserId();
		map.put("sponsor", sponsor);
		return RetMsgHelper.ok(trdInvestFollowupService.selectTtTrdInvestFollowupFinished(map));
	}
	
	/**
	 * 获取投后报告序列
	 * @return
	 */
	@RequestMapping(value="/getInvestFollowupSeq")
	@ResponseBody
	public RetMsg<Serializable> getInvestFollowupSeq(){
		String seq = trdInvestFollowupService.getInvestFollowupSeq();
		return RetMsgHelper.ok(seq);
	}
	
	/**
	 * 新增投后报告
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/insertInvestFollowup")
	@ResponseBody
	public RetMsg<Serializable> insertInvestFollowup(@RequestBody Map<String,Object> map){
		trdInvestFollowupService.insertInvestFollowup(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改投后报告
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/modifyInvestFollowup")
	@ResponseBody
	public RetMsg<Serializable> modifyInvestFollowup(@RequestBody Map<String,Object> map){
		trdInvestFollowupService.modifyInvestFollowup(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除投后报告
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/deleteInvestFollowup")
	@ResponseBody
	public RetMsg<Serializable> deleteInvestFollowup(@RequestBody String[] dealNos){
		trdInvestFollowupService.deleteInvestFollowup(dealNos);
		return RetMsgHelper.ok();
	}
	

}
