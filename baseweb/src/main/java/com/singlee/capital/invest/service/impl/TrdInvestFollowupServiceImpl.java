package com.singlee.capital.invest.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.invest.mapper.TtTrdInvestFollowupMapper;
import com.singlee.capital.invest.model.TtTrdInvestFollowup;
import com.singlee.capital.invest.service.TrdInvestFollowupService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service("trdInvestFollowupService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrdInvestFollowupServiceImpl implements TrdInvestFollowupService,SlbpmCallBackInteface {
	
	@Autowired
	private TtTrdInvestFollowupMapper ttTrdInvestFollowupMapper;

	/**
	 * 查询投后报告
	 */
	@Override
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowup(Map<String, Object> params) {
		return ttTrdInvestFollowupMapper.selectTtTrdInvestFollowup(params, ParameterUtil.getRowBounds(params));
	}

	/**
	 * 查询投后报告序列
	 */
	@Override
	public String getInvestFollowupSeq() {
		int seq = ttTrdInvestFollowupMapper.getInvestFollowupSeq();
		return "IF"+DateUtil.getCurrentCompactDateTimeAsString()+seq;
	}
	
	/**
	 * 新增投后报告
	 */
	@Override
	public void insertInvestFollowup(Map<String,Object> map){
		TtTrdInvestFollowup followup = MaptoObj(map);
		ttTrdInvestFollowupMapper.insert(followup);
	}
	
	/**
	 * 修改投后报告
	 */
	@Override
	public void modifyInvestFollowup(Map<String,Object> map){
		TtTrdInvestFollowup followup = MaptoObj(map);
		ttTrdInvestFollowupMapper.updateByPrimaryKey(followup);
	}
	
	/**
	 * Map转换成对象
	 * @return
	 */
	public TtTrdInvestFollowup MaptoObj(Map<String,Object> map){
		TtTrdInvestFollowup followup = new TtTrdInvestFollowup();
		followup.setDealNo(ParameterUtil.getString(map, "iFDealNo", ""));
		JY.require(!"".equals(ParameterUtil.getString(map, "iFDealNo", "")), "流水号为空");
		followup.setRefNo(ParameterUtil.getString(map, "refNo", ""));
		followup.setDealType(DictConstants.DealType.Approve);
		followup.setSponsor(SlSessionHelper.getUserId());
		followup.setSponInst(SlSessionHelper.getInstitutionId());
		followup.setRemark(ParameterUtil.getString(map, "remark", ""));
		followup.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		followup.setInvestStatus(DictConstants.ApproveStatus.New);
		followup.setExplain(ParameterUtil.getString(map, "explain", ""));
		return followup;
	}

	/**
	 * 删除投后报告
	 * @param dealNo
	 */
	@Override
	public void deleteInvestFollowup(String[] dealNos) {
		for(int i =0;i<dealNos.length;i++){
			ttTrdInvestFollowupMapper.deleteByPrimaryKey(dealNos[i]);
		}
	}

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		TtTrdInvestFollowup followup = new TtTrdInvestFollowup();
		followup.setDealNo(serial_no);
		followup = ttTrdInvestFollowupMapper.selectByPrimaryKey(followup);
		if(DictConstants.FlowType.InvestFollowupFlow.equals(flow_type)){
			followup.setInvestStatus(status);
		}
		ttTrdInvestFollowupMapper.updateByPrimaryKey(followup);
		if(DictConstants.ApproveStatus.New.equals(status)){
			
		}else if(DictConstants.ApproveStatus.WaitApprove.equals(status)){
			
		}else if(DictConstants.ApproveStatus.ApprovedNoPass.equals(status)){
			
		}else if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
			
		}else if(DictConstants.ApproveStatus.Cancle.equals(status)){
			
		}
	}

	/**
	 * 待审批列表
	 */
	@Override
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowupApprove(Map<String, Object> params) {
		return ttTrdInvestFollowupMapper.selectTtTrdInvestFollowupApprove(params, ParameterUtil.getRowBounds(params));
	}

	/**
	 * 已审批列表
	 */
	@Override
	public Page<TtTrdInvestFollowup> selectTtTrdInvestFollowupFinished(Map<String, Object> params) {
		return ttTrdInvestFollowupMapper.selectTtTrdInvestFollowupFinished(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		TtTrdInvestFollowup followup = new TtTrdInvestFollowup();
		followup.setDealNo(serial_no);
		followup = ttTrdInvestFollowupMapper.selectByPrimaryKey(followup);
		return followup;
	}

}
