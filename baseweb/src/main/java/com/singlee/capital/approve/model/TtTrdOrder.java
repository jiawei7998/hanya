package com.singlee.capital.approve.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 审批单信息
 * @author gm
 *
 */
@Entity
@Table(name = "TT_TRD_ORDER")
public class TtTrdOrder implements Serializable,Cloneable {

	private static final long serialVersionUID = -3055698775463523593L;

    /**  审批单号  */
	@Id
    private String order_id;

    /**  委托日期  */
    private String order_date;

    /**  委托时间  */
    private String order_time;

    /**  内部交易号  */
    private String in_orderid;

    /**  操作人  */
    private String user_id;

    /**  0101:现券买入
0102:现券卖出
0201:质押式正回购
0202:质押式逆回购
0301:分销买入
0302:分销卖出  */
    private String trdtype;

    /**  交易对手  */
    private String party_id;

    /**  金融工具代码   */
    private String i_code;

    /**  资产类型   */
    private String a_type;

    /**  市场类型 BANK-银行间   */
    private String m_type;

    /**  金融工具名称   */
    private String i_name;

    /**  错误代码  */
    private String err_code;

    /**  错误信息  */
    private String err_info;

    /**  组合号   */
    private String group_id;

    /**  执行市场  */
    private String exe_market;

    /**  交易来源 1=内部交易 2=COMPLEX   */
    private String order_source;

    /**  对手方中债托管帐号   */
    private String party_zzdaccid;

    /**  本方中债托管帐号   */
    private String self_zzdaccid;

    /**  导入时间  */
    private String imp_time;

    /**  首期结算方式  */
    private String fst_settle_type;

    /**  到期结算方式   */
    private String end_settle_type;

    /**  首期结算日期 YYYY-MM-DD   */
    private String fst_settle_date;

    /**  到期结算日期 YYYY-MM-DD   */
    private String end_settle_date;

    /**  期限（天）  */
    private double term_days;

    /**  清算速度 0:T+0
1:T+1  */
    private String settle_days;

    /**  交易对手类型：1外部交易对手
2内部交易对手  */
    private String party_type;

    /**  券面总额   */
    private double totalamount;

    /**  券面数量   */
    private double totalcount;

    /**  到期收益率 质押式回购填回购利率   */
    private double ytm;

    /**  净价(元)   */
    private double netprice;

    /**  全价(元)   */
    private double fullprice;

    /**  应计利息(元)   */
    private double aiprice;

    /**  净价金额(元)   */
    private double netamount;

    /**  全价金额(元)   */
    private double fullamount;

    /**  利息金额(元)   */
    private double aiamount;

    /**  本方交易员编号   */
    private String self_traderid;

    /**  本方交易员名称   */
    private String self_tradername;

    /**  本方银行帐号   */
    private String self_bankacccode;

    /**  本方账户名称   */
    private String self_bankaccname;

    /**  本方开户行行号   */
    private String self_bankcode;

    /**  本方开户行名称   */
    private String self_bankname;

    /**  本方大额支付号   */
    private String self_largepaymentcode;

    /**  本方内部证券账户编码   */
    private String self_insecuaccid;

    /**  本方外部证券账户编码   */
    private String self_outsecuaccid;

    /**  本方内部资金账户编码   */
    private String self_incashaccid;

    /**  本方外部资金账户编码   */
    private String self_outcashaccid;

    /**  对手方交易员编号   */
    private String party_traderid;

    /**  对手方交易员名称   */
    private String party_tradername;

    /**  对手方银行帐号   */
    private String party_bankacccode;

    /**  对手方账户名称   */
    private String party_bankaccname;

    /**  对手方开户行行号   */
    private String party_bankcode;

    /**  对手方开户行名称   */
    private String party_bankname;

    /**  对手方大额支付号   */
    private String party_largepaymentcode;

    /**  对手方内部证券账户编码   */
    private String party_insecuaccid;

    /**  对手方外部证券账户编码   */
    private String party_outsecuaccid;

    /**  对手方内部资金账户编码   */
    private String party_incashaccid;

    /**  对手方外部资金账户编码   */
    private String party_outcashaccid;

    /**  0:待确认   */
    private String order_status;

    /**  操作员编号   */
    private String operator_id;

    /**  操作时间  */
    private String operate_time;

    /**  备注信息   */
    private String memo;

    /**  中债估值净价  */
    private double zzd_netprice;

    /**  中债收益率  */
    private double zzd_ytm;

    /**  预成交额度  */
    private double preorder_amount;

    /**  实际占用额度  */
    private double used_amount;

    /**  剩余可用额度  */
    private double remain_amount;

    /**  启用标示  */
    private String is_active;

    /**  结算费用  */
    private double settle_fee;

    /**  交易费用  */
    private double trade_fee;

    /** 首期结算金额 **/
    private double fst_settle_amount;

    /** 到期结算金额 **/
    private double end_settle_amount;
    
    /** 行权收益率 **/
    private double strike_yield ;
    
    /** 计息天数 **/
    private double cal_days;
    
    /**
     * 产品名称
     */
    @Transient
    private String prd_name;
    /**
     * 产品类型
     */
    @Transient
    private String prd_type;

    public double getCal_days() {
		return cal_days;
	}

	public void setCal_days(double cal_days) {
		this.cal_days = cal_days;
	}

	public double getStrike_yield() {
		return strike_yield;
	}

	public void setStrike_yield(double strike_yield) {
		this.strike_yield = strike_yield;
	}

	public double getFst_settle_amount() {
		return fst_settle_amount;
	}

	public void setFst_settle_amount(double fst_settle_amount) {
		this.fst_settle_amount = fst_settle_amount;
	}

	public double getEnd_settle_amount() {
		return end_settle_amount;
	}

	public void setEnd_settle_amount(double end_settle_amount) {
		this.end_settle_amount = end_settle_amount;
	}

		/**-------  Generate Getter And Setter   --------**/
    public String getOrder_id(){
          return this.order_id;
    }

	public void setOrder_id(String order_id){
          this.order_id=order_id;
    }

    public String getOrder_date(){
          return this.order_date;
    }

    public void setOrder_date(String order_date){
          this.order_date=order_date;
    }

    public String getOrder_time(){
          return this.order_time;
    }

    public void setOrder_time(String order_time){
          this.order_time=order_time;
    }

    public String getIn_orderid(){
          return this.in_orderid;
    }

    public void setIn_orderid(String in_orderid){
          this.in_orderid=in_orderid;
    }

    public String getUser_id(){
          return this.user_id;
    }

    public void setUser_id(String user_id){
          this.user_id=user_id;
    }

    public String getTrdtype(){
          return this.trdtype;
    }

    public void setTrdtype(String trdtype){
          this.trdtype=trdtype;
    }

    public String getParty_id(){
          return this.party_id;
    }

    public void setParty_id(String party_id){
          this.party_id=party_id;
    }

    public String getI_code(){
          return this.i_code;
    }

    public void setI_code(String i_code){
          this.i_code=i_code;
    }

    public String getA_type(){
          return this.a_type;
    }

    public void setA_type(String a_type){
          this.a_type=a_type;
    }

    public String getM_type(){
          return this.m_type;
    }

    public void setM_type(String m_type){
          this.m_type=m_type;
    }

    public String getI_name(){
          return this.i_name;
    }

    public void setI_name(String i_name){
          this.i_name=i_name;
    }

    public String getErr_code(){
          return this.err_code;
    }

    public void setErr_code(String err_code){
          this.err_code=err_code;
    }

    public String getErr_info(){
          return this.err_info;
    }

    public void setErr_info(String err_info){
          this.err_info=err_info;
    }

    public String getGroup_id(){
          return this.group_id;
    }

    public void setGroup_id(String group_id){
          this.group_id=group_id;
    }

    public String getExe_market(){
          return this.exe_market;
    }

    public void setExe_market(String exe_market){
          this.exe_market=exe_market;
    }

    public String getOrder_source() {
		return order_source;
	}

	public void setOrder_source(String order_source) {
		this.order_source = order_source;
	}

	public String getParty_zzdaccid(){
          return this.party_zzdaccid;
    }

    public void setParty_zzdaccid(String party_zzdaccid){
          this.party_zzdaccid=party_zzdaccid;
    }

    public String getSelf_zzdaccid() {
		return self_zzdaccid;
	}

	public void setSelf_zzdaccid(String self_zzdaccid) {
		this.self_zzdaccid = self_zzdaccid;
	}

	public String getImp_time(){
          return this.imp_time;
    }

    public void setImp_time(String imp_time){
          this.imp_time=imp_time;
    }

    public String getFst_settle_type(){
          return this.fst_settle_type;
    }

    public void setFst_settle_type(String fst_settle_type){
          this.fst_settle_type=fst_settle_type;
    }

    public String getEnd_settle_type(){
          return this.end_settle_type;
    }

    public void setEnd_settle_type(String end_settle_type){
          this.end_settle_type=end_settle_type;
    }

    public String getFst_settle_date(){
          return this.fst_settle_date;
    }

    public void setFst_settle_date(String fst_settle_date){
          this.fst_settle_date=fst_settle_date;
    }

    public String getEnd_settle_date(){
          return this.end_settle_date;
    }

    public void setEnd_settle_date(String end_settle_date){
          this.end_settle_date=end_settle_date;
    }

    public double getTerm_days(){
          return this.term_days;
    }

    public void setTerm_days(double term_days){
          this.term_days=term_days;
    }

    public String getSettle_days(){
          return this.settle_days;
    }

    public void setSettle_days(String settle_days){
          this.settle_days=settle_days;
    }

    public String getParty_type(){
          return this.party_type;
    }

    public void setParty_type(String party_type){
          this.party_type=party_type;
    }

    public double getTotalamount(){
          return this.totalamount;
    }

    public void setTotalamount(double totalamount){
          this.totalamount=totalamount;
    }

    public double getTotalcount(){
          return this.totalcount;
    }

    public void setTotalcount(double totalcount){
          this.totalcount=totalcount;
    }

    public double getYtm(){
          return this.ytm;
    }

    public void setYtm(double ytm){
          this.ytm=ytm;
    }

    public double getNetprice(){
          return this.netprice;
    }

    public void setNetprice(double netprice){
          this.netprice=netprice;
    }

    public double getFullprice(){
          return this.fullprice;
    }

    public void setFullprice(double fullprice){
          this.fullprice=fullprice;
    }

    public double getAiprice(){
          return this.aiprice;
    }

    public void setAiprice(double aiprice){
          this.aiprice=aiprice;
    }

    public double getNetamount(){
          return this.netamount;
    }

    public void setNetamount(double netamount){
          this.netamount=netamount;
    }

    public double getFullamount(){
          return this.fullamount;
    }

    public void setFullamount(double fullamount){
          this.fullamount=fullamount;
    }

    public double getAiamount(){
          return this.aiamount;
    }

    public void setAiamount(double aiamount){
          this.aiamount=aiamount;
    }

    public String getSelf_traderid(){
          return this.self_traderid;
    }

    public void setSelf_traderid(String self_traderid){
          this.self_traderid=self_traderid;
    }

    public String getSelf_tradername(){
          return this.self_tradername;
    }

    public void setSelf_tradername(String self_tradername){
          this.self_tradername=self_tradername;
    }

    public String getSelf_bankacccode(){
          return this.self_bankacccode;
    }

    public void setSelf_bankacccode(String self_bankacccode){
          this.self_bankacccode=self_bankacccode;
    }

    public String getSelf_bankaccname(){
          return this.self_bankaccname;
    }

    public void setSelf_bankaccname(String self_bankaccname){
          this.self_bankaccname=self_bankaccname;
    }

    public String getSelf_bankcode(){
          return this.self_bankcode;
    }

    public void setSelf_bankcode(String self_bankcode){
          this.self_bankcode=self_bankcode;
    }

    public String getSelf_bankname(){
          return this.self_bankname;
    }

    public void setSelf_bankname(String self_bankname){
          this.self_bankname=self_bankname;
    }

    public String getSelf_largepaymentcode(){
          return this.self_largepaymentcode;
    }

    public void setSelf_largepaymentcode(String self_largepaymentcode){
          this.self_largepaymentcode=self_largepaymentcode;
    }

    public String getSelf_insecuaccid(){
          return this.self_insecuaccid;
    }

    public void setSelf_insecuaccid(String self_insecuaccid){
          this.self_insecuaccid=self_insecuaccid;
    }

    public String getSelf_outsecuaccid(){
          return this.self_outsecuaccid;
    }

    public void setSelf_outsecuaccid(String self_outsecuaccid){
          this.self_outsecuaccid=self_outsecuaccid;
    }

    public String getSelf_incashaccid(){
          return this.self_incashaccid;
    }

    public void setSelf_incashaccid(String self_incashaccid){
          this.self_incashaccid=self_incashaccid;
    }

    public String getSelf_outcashaccid(){
          return this.self_outcashaccid;
    }

    public void setSelf_outcashaccid(String self_outcashaccid){
          this.self_outcashaccid=self_outcashaccid;
    }

    public String getParty_traderid(){
          return this.party_traderid;
    }

    public void setParty_traderid(String party_traderid){
          this.party_traderid=party_traderid;
    }

    public String getParty_tradername(){
          return this.party_tradername;
    }

    public void setParty_tradername(String party_tradername){
          this.party_tradername=party_tradername;
    }

    public String getParty_bankacccode(){
          return this.party_bankacccode;
    }

    public void setParty_bankacccode(String party_bankacccode){
          this.party_bankacccode=party_bankacccode;
    }

    public String getParty_bankaccname(){
          return this.party_bankaccname;
    }

    public void setParty_bankaccname(String party_bankaccname){
          this.party_bankaccname=party_bankaccname;
    }

    public String getParty_bankcode(){
          return this.party_bankcode;
    }

    public void setParty_bankcode(String party_bankcode){
          this.party_bankcode=party_bankcode;
    }

    public String getParty_bankname(){
          return this.party_bankname;
    }

    public void setParty_bankname(String party_bankname){
          this.party_bankname=party_bankname;
    }

    public String getParty_largepaymentcode(){
          return this.party_largepaymentcode;
    }

    public void setParty_largepaymentcode(String party_largepaymentcode){
          this.party_largepaymentcode=party_largepaymentcode;
    }

    public String getParty_insecuaccid(){
          return this.party_insecuaccid;
    }

    public void setParty_insecuaccid(String party_insecuaccid){
          this.party_insecuaccid=party_insecuaccid;
    }

    public String getParty_outsecuaccid(){
          return this.party_outsecuaccid;
    }

    public void setParty_outsecuaccid(String party_outsecuaccid){
          this.party_outsecuaccid=party_outsecuaccid;
    }

    public String getParty_incashaccid(){
          return this.party_incashaccid;
    }

    public void setParty_incashaccid(String party_incashaccid){
          this.party_incashaccid=party_incashaccid;
    }

    public String getParty_outcashaccid(){
          return this.party_outcashaccid;
    }

    public void setParty_outcashaccid(String party_outcashaccid){
          this.party_outcashaccid=party_outcashaccid;
    }

    public String getOrder_status(){
          return this.order_status;
    }

    public void setOrder_status(String order_status){
          this.order_status=order_status;
    }

    public String getOperator_id(){
          return this.operator_id;
    }

    public void setOperator_id(String operator_id){
          this.operator_id=operator_id;
    }

    public String getOperate_time(){
          return this.operate_time;
    }

    public void setOperate_time(String operate_time){
          this.operate_time=operate_time;
    }

    public String getMemo(){
          return this.memo;
    }

    public void setMemo(String memo){
          this.memo=memo;
    }

    public double getZzd_netprice(){
          return this.zzd_netprice;
    }

    public void setZzd_netprice(double zzd_netprice){
          this.zzd_netprice=zzd_netprice;
    }

    public double getZzd_ytm(){
          return this.zzd_ytm;
    }

    public void setZzd_ytm(double zzd_ytm){
          this.zzd_ytm=zzd_ytm;
    }

    public double getPreorder_amount() {
		return preorder_amount;
	}

	public void setPreorder_amount(double preorder_amount) {
		this.preorder_amount = preorder_amount;
	}

	public double getUsed_amount(){
          return this.used_amount;
    }

    public void setUsed_amount(double used_amount){
          this.used_amount=used_amount;
    }

    public double getRemain_amount(){
          return this.remain_amount;
    }

    public void setRemain_amount(double remain_amount){
          this.remain_amount=remain_amount;
    }

    public String getIs_active(){
          return this.is_active;
    }

    public void setIs_active(String is_active){
          this.is_active=is_active;
    }

    public double getSettle_fee(){
          return this.settle_fee;
    }

    public void setSettle_fee(double settle_fee){
          this.settle_fee=settle_fee;
    }

    public double getTrade_fee(){
          return this.trade_fee;
    }

    public void setTrade_fee(double trade_fee){
          this.trade_fee=trade_fee;
    }
    
    public String getPrd_name() {
		return prd_name;
	}

	public void setPrd_name(String prd_name) {
		this.prd_name = prd_name;
	}

	public String getPrd_type() {
		return prd_type;
	}

	public void setPrd_type(String prd_type) {
		this.prd_type = prd_type;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{
    	return super.clone();
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtTrdOrder [order_id=");
		builder.append(order_id);
		builder.append(", order_date=");
		builder.append(order_date);
		builder.append(", order_time=");
		builder.append(order_time);
		builder.append(", in_orderid=");
		builder.append(in_orderid);
		builder.append(", user_id=");
		builder.append(user_id);
		builder.append(", trdtype=");
		builder.append(trdtype);
		builder.append(", party_id=");
		builder.append(party_id);
		builder.append(", i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", i_name=");
		builder.append(i_name);
		builder.append(", err_code=");
		builder.append(err_code);
		builder.append(", err_info=");
		builder.append(err_info);
		builder.append(", group_id=");
		builder.append(group_id);
		builder.append(", exe_market=");
		builder.append(exe_market);
		builder.append(", order_source=");
		builder.append(order_source);
		builder.append(", party_zzdaccid=");
		builder.append(party_zzdaccid);
		builder.append(", self_zzdaccid=");
		builder.append(self_zzdaccid);
		builder.append(", imp_time=");
		builder.append(imp_time);
		builder.append(", fst_settle_type=");
		builder.append(fst_settle_type);
		builder.append(", end_settle_type=");
		builder.append(end_settle_type);
		builder.append(", fst_settle_date=");
		builder.append(fst_settle_date);
		builder.append(", end_settle_date=");
		builder.append(end_settle_date);
		builder.append(", term_days=");
		builder.append(term_days);
		builder.append(", settle_days=");
		builder.append(settle_days);
		builder.append(", party_type=");
		builder.append(party_type);
		builder.append(", totalamount=");
		builder.append(totalamount);
		builder.append(", totalcount=");
		builder.append(totalcount);
		builder.append(", ytm=");
		builder.append(ytm);
		builder.append(", netprice=");
		builder.append(netprice);
		builder.append(", fullprice=");
		builder.append(fullprice);
		builder.append(", aiprice=");
		builder.append(aiprice);
		builder.append(", netamount=");
		builder.append(netamount);
		builder.append(", fullamount=");
		builder.append(fullamount);
		builder.append(", aiamount=");
		builder.append(aiamount);
		builder.append(", self_traderid=");
		builder.append(self_traderid);
		builder.append(", self_tradername=");
		builder.append(self_tradername);
		builder.append(", self_bankacccode=");
		builder.append(self_bankacccode);
		builder.append(", self_bankaccname=");
		builder.append(self_bankaccname);
		builder.append(", self_bankcode=");
		builder.append(self_bankcode);
		builder.append(", self_bankname=");
		builder.append(self_bankname);
		builder.append(", self_largepaymentcode=");
		builder.append(self_largepaymentcode);
		builder.append(", self_insecuaccid=");
		builder.append(self_insecuaccid);
		builder.append(", self_outsecuaccid=");
		builder.append(self_outsecuaccid);
		builder.append(", self_incashaccid=");
		builder.append(self_incashaccid);
		builder.append(", self_outcashaccid=");
		builder.append(self_outcashaccid);
		builder.append(", party_traderid=");
		builder.append(party_traderid);
		builder.append(", party_tradername=");
		builder.append(party_tradername);
		builder.append(", party_bankacccode=");
		builder.append(party_bankacccode);
		builder.append(", party_bankaccname=");
		builder.append(party_bankaccname);
		builder.append(", party_bankcode=");
		builder.append(party_bankcode);
		builder.append(", party_bankname=");
		builder.append(party_bankname);
		builder.append(", party_largepaymentcode=");
		builder.append(party_largepaymentcode);
		builder.append(", party_insecuaccid=");
		builder.append(party_insecuaccid);
		builder.append(", party_outsecuaccid=");
		builder.append(party_outsecuaccid);
		builder.append(", party_incashaccid=");
		builder.append(party_incashaccid);
		builder.append(", party_outcashaccid=");
		builder.append(party_outcashaccid);
		builder.append(", order_status=");
		builder.append(order_status);
		builder.append(", operator_id=");
		builder.append(operator_id);
		builder.append(", operate_time=");
		builder.append(operate_time);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", prd_name=");
		builder.append(prd_name);
		builder.append(", prd_type=");
		builder.append(prd_type);
		builder.append(", zzd_netprice=");
		builder.append(zzd_netprice);
		builder.append(", zzd_ytm=");
		builder.append(zzd_ytm);
		builder.append(", preorder_amount=");
		builder.append(preorder_amount);
		builder.append(", used_amount=");
		builder.append(used_amount);
		builder.append(", remain_amount=");
		builder.append(remain_amount);
		builder.append(", is_active=");
		builder.append(is_active);
		builder.append(", settle_fee=");
		builder.append(settle_fee);
		builder.append(", trade_fee=");
		builder.append(trade_fee);
		builder.append(", fst_settle_amount=");
		builder.append(fst_settle_amount);
		builder.append(", end_settle_amount=");
		builder.append(end_settle_amount);
		builder.append(", strike_yield=");
		builder.append(strike_yield);
		builder.append("]");
		return builder.toString();
	}
    
}
