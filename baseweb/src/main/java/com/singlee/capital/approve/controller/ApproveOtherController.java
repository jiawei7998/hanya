package com.singlee.capital.approve.controller;

import com.singlee.capital.approve.service.ApproveOtherService;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * 审批单非标准方法controller
 * @author kevin_gm
 *
 */
@Controller
@RequestMapping(value = "/ApproveOtherController")
public class ApproveOtherController extends CommonController {

	@Autowired
	private ApproveOtherService approveOtherService;

	
//	/**
//	 * 存放同业定期自动转存。根据到期指令的交易单转换出一张新的审批单数据
//	 * @param request
//	 * @return
//	 * @throws IOException
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/loadApproveAutomaticRedeposit")
//	public HashMap<String,Object> loadApproveAutomaticRedeposit (@RequestBody HashMap<String, Object> map) throws IOException{
//		String trade_id = ParameterUtil.getString(map, "trade_id", "");
//		String version_number = ParameterUtil.getString(map, "version_number", "");
//		return approveOtherService.loadApproveAutomaticRedeposit(trade_id, version_number);
//	}
	/**
	 * 质押债券信息查询
	 * @param request
	 * 参数 ：
	 * 		inSecuAccid 内部证券账户
	 *      hostMarket 托管市场
	 * 		iCode 金融工具ID
	 * 		aType 资产类型
	 * 		mType 市场类型
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/selectBondInfo")
	public Map<String,Object> selectBondInfo(@RequestBody HashMap<String, Object> map) throws IOException{
		String inSecuAccId = ParameterUtil.getString(map, "inSecuAccId", "");
		String hostMarket = ParameterUtil.getString(map, "hostMarket", "");
		String iCode = ParameterUtil.getString(map, "iCode", "");
		String aType = ParameterUtil.getString(map, "aType", "");
		String mType = ParameterUtil.getString(map, "mType", "");
		return approveOtherService.getBondInfo(inSecuAccId, hostMarket, new InstrumentId(iCode,aType,mType));
	}
}
