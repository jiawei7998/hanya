package com.singlee.capital.approve.pojo;

import com.singlee.capital.approve.model.TtTrdOrder;

/**
 * 审批单编辑时查询对象
 * @author kevin_gm
 *
 */
public class OrderEditVo extends TtTrdOrder {

	private static final long serialVersionUID = 2980990468591514784L;
	private String party_code;//交易对手代码
	private String Party_Shortname;//交易对手名称
	private String user_name;//用户名
	private String folder;//业务类型字典项的folder，用于aprrove页面链接
	private String party_bigkind;//交易对手大类
	private String party_smallkind;//交易对手小类
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getParty_code() {
		return party_code;
	}
	public void setParty_code(String party_code) {
		this.party_code = party_code;
	}
	public String getParty_Shortname() {
		return Party_Shortname;
	}
	public void setParty_Shortname(String party_Shortname) {
		Party_Shortname = party_Shortname;
	}
	public String getParty_bigkind() {
		return party_bigkind;
	}
	public void setParty_bigkind(String party_bigkind) {
		this.party_bigkind = party_bigkind;
	}
	public String getParty_smallkind() {
		return party_smallkind;
	}
	public void setParty_smallkind(String party_smallkind) {
		this.party_smallkind = party_smallkind;
	}
}
