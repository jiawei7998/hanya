package com.singlee.capital.approve.controller;

import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.model.TtAccOutCashFixed;
import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.approve.pojo.AccOutCashListByCustomerNoVo;
import com.singlee.capital.approve.service.ApproveInfoService;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.market.model.TtMktBondValuing;
import com.singlee.capital.market.pojo.IBond;
import com.singlee.capital.market.service.MktBondService;
import com.singlee.capital.market.service.MktBondValuingService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 审批单公共信息调用conroller
 * @author kevin_gm
 * 仅用于审批单公共部分逻辑调用,不包含审批单业务逻辑.
 * 1.根据内部证券账户编号查询关联的外部证券信息
 * 2.选中内部证券账户后显示关联的交易员列表
 * 
 */
@Controller
@RequestMapping(value = "/ApproveInfoController")
public class ApproveInfoController extends CommonController {
//
//	/** 审批单公共信息service ***/
	@Autowired
	private ApproveInfoService approveInfoService;
//	
	@Autowired
	private MktBondService mktBondService;
//	
	@Autowired
	private AccOutCashService accOutCashService;
	
	@Autowired
	private AccInCashService accInCashService;
//	
	@Autowired
	private MktBondValuingService mktBondValuingService;
	
	/**
	 * 根据交易对手code读取定期账户信息
	 * @param map
	 * accid：内部证券账户编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAccOutCashFixedByPartyCode")
	public List<TtAccOutCashFixed> searchAccOutCashFixedByPartyCode(@RequestBody Map<String,Object> map) throws IOException{
		String inst_id = SlSessionHelper.getInstitutionId();
		String partyCode = ParameterUtil.getString(map, "party_code", "");
		JY.require(!StringUtil.checkEmptyNull(partyCode), "交易对手code不允许传入空值.");
		String bookInstId = accInCashService.getBookInstIdByInCashAccId(SlSessionHelperExt.getInCashAccId());
		return accOutCashService.getAccOutCashFixedByPartyCode(inst_id, partyCode,bookInstId);
	}
	
	/**
	 * 根据交易对手code读取活期账户信息
	 * @param map
	 * accid：内部证券账户编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAccOutCashCurrentByPartyCode")
	public List<TtAccOutCash> searchAccOutCashCurrentByPartyCode(@RequestBody Map<String,Object> map) throws IOException{
		String inst_id = SlSessionHelper.getInstitutionId();
		String partyCode = ParameterUtil.getString(map, "party_code", "");
		JY.require(!StringUtil.checkEmptyNull(partyCode), "交易对手code不允许传入空值.");
		return accOutCashService.getAccOutCashCurrentByPartyCode(inst_id, partyCode);
	}
	/**
	 * 根据内部证券账户编号查询关联的外部证券信息
	 * @param map
	 * accid：内部证券账户编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAccOutCashList")
	public List<TtAccOutCash> searchAccOutCashList(@RequestBody Map<String,Object> map) throws IOException{
		String acc_in_secu_id = ParameterUtil.getString(map,"accid","");
		@SuppressWarnings("unchecked")
		List<String> acctype_list = (List<String>) (map.get("acctype_list") == null?null:map.get("acctype_list"));
		List<TtAccOutCash> list = approveInfoService.searchAccOutCashList(acc_in_secu_id,acctype_list);
		return list;
	}
//
//	/**
//	 * 选中内部证券账户后显示关联的交易员列表
//	 * is_public = 1 查询所有有效的用户信息
//	 * is_public = 0 查询owner用户
//	 * @param is_public
//	 * @param owner
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchOwnerListByAccId")
//	public List<UserVo> searchOwnerListByAccId(HttpEntity<String> request) throws IOException{
//		String json = request.getBody();
//		Map<String,String> map = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
//		String is_public = ParameterUtil.getString(map,"is_public","");
//		String owner = ParameterUtil.getString(map,"owner","");
//		return approveInfoService.searchOwnerListByAccId(is_public, owner);
//	}
	/**
	 * 从金融工具中读取债券信息
	 * @param request
	 * 	i_code
	 * 	a_type
	 *  m_type
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIBondInfo")
	public IBond searchIBondInfo(@RequestBody Map<String,String> map) throws IOException{
		String i_code = ParameterUtil.getString(map,"i_code","");
		String a_type = ParameterUtil.getString(map,"a_type","");
		String m_type = ParameterUtil.getString(map,"m_type","");
		String setdate = ParameterUtil.getString(map,"setdate","");
		IBond bond = mktBondService.getMktIBond(i_code, a_type, m_type);
		if(StringUtil.notNullOrEmpty(setdate) && !StringUtil.IsEqual(setdate, bond.getValuing_date())){
			TtMktBondValuing mktBondValuing = mktBondValuingService.getMktBondValuingOnSomeDate(i_code, a_type, m_type, setdate);
			if(mktBondValuing != null){
				bond.setNet_price(mktBondValuing.getNet_price());
				bond.setYield(MathUtil.round(mktBondValuing.getYield()/100.0, 6));
			}else{
				bond.setHave_valuing(false);
			}
		}
		return bond;
	}
	/**
	 * 根据交易对手号，查询该交易对手关联的外部自己账户信息
	 * @param map
	 * customer_no:客户编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAccOutCashListByPartyId")
	public List<AccOutCashListByCustomerNoVo> searchAccOutCashListByPartyId(@RequestBody Map<String,String> map) throws IOException{
		String party_id = ParameterUtil.getString(map,"party_id","");
		String settle_date = ParameterUtil.getString(map,"settle_date","");
		String accid = "";
		String inCashAccId = ParameterUtil.getString(map,"inCashAccId","");
		String inst_id = ""	;
		if(StringUtil.checkEmptyNull(inCashAccId)){
			inst_id = SlSessionHelper.getInstitutionId();
		}
		else{
			TtAccInCash inAccCash = accInCashService.getAcc(inCashAccId);
			inst_id = inAccCash.getInstId();
		}
		String trdType = ParameterUtil.getString(map,"trdType","");
		String status = ParameterUtil.getString(map,"status","");
		if(DictConstants.TrdType.CurrentToBankAccOff.equals(trdType)){
			status = "";
		}else{
			status = DictConstants.AccStatus.Enabled;
		}
		return approveInfoService.searchAccOutCashListByPartyId(party_id,accid,inst_id,settle_date,status);
	}
//	/**
//	 * 根据交易对手号，查询该交易对手关联的外部账户信息（机构隔离）
//	 * @param map
//	 * customer_no:客户编号
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchAccCustCashListByPartyId")
//	public List<AccCustCashListByCustomerNoVo> searchAccCustCashListByPartyId(HttpEntity<String> request) throws IOException{
//		String json = request.getBody();
//		Map<String,String> map = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
//		String party_id = ParameterUtil.getString(map,"party_id","");
//		String accid = "";
//		return approveInfoService.searchAccCustCashListByPartyId(party_id,accid);
//	}
}
