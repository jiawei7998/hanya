package com.singlee.capital.approve.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutSecu;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.BookQueryVo;
import com.singlee.capital.approve.pojo.OrderEditVo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 审批单信息保存接口
 * @author kevin_gm
 *
 */
public interface TrdOrderMapper extends Mapper<TtTrdOrder> {
	
	/**
	 * 根据审批单编号查询审批记录对象，此方法带行级锁
	 * @param map
	 * @return
	 */
	TtTrdOrder selectOrderForOrderId(HashMap<String,Object> map);
	
	/**
	 * 查询
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<TtTrdOrder> getOrderVoPage(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据金融工具IAM查询相应审批记录对象
	 * @param map
	 * @return
	 */
	List<TtTrdOrder> selectOrderForIAM(HashMap<String,Object> map);

	/**
	 * 查询时显示审批单明细信息（支持批量查询）
	 * @param map
	 * @return
	 */
	OrderEditVo selectOrderEditForOrderId(HashMap<String,Object> map);
	/**
	 * 查询时显示审批单明细信息（支持批量查询）
	 * @param map
	 * @return
	 */
	List<OrderEditVo> selectOrderEditListForOrderId(HashMap<String,Object> map);
	
	/**
	 * 获取审批单编号
	 * @return
	 */
	String getOrderId(HashMap<String,Object> map);

	/**
	 * 审批单插入
	 * @param ttTrdOrder
	 */
	void insertApprove(TtTrdOrder ttTrdOrder);
	
	/**
	 * 审批单更新
	 * @param ttTrdOrder
	 */
	void updateApprove(TtTrdOrder ttTrdOrder);
	
	/**
	 * 审批单Bo对象查询
	 * @param map
	 */
	List<TtTrdOrder> selectTtTrdOrder(HashMap<String,Object> map);
	/**
	 * 限额计算审批单Bo对象查询
	 * @param map
	 */
	List<TtTrdOrder> selectTtTrdOrder4Limit(HashMap<String,Object> map);
	
	/**
	 * 根据债券托管市场查询外部证券账户信息
	 * @param map
	 * @return
	 */
	TtAccOutSecu selectAccOutSectForBond(HashMap<String,Object> map);
	
	/**
	 * 根据参数查询台账信息
	 */
	List<BookQueryVo> selectBookList(Map<String,Object> map);
	
	/**
	 * 根据参数查询同业投资信息
	 */
	List<BookQueryVo> selectIbInvest(Map<String,Object> map);
	
	/**
	 * 根据参数统计同业投资信息的数量
	 */
	int countIbInvest(Map<String, Object> param);
	
	/**
	 * 根据参数统计相应台账的数量
	 */
	int countBookList(Map<String, Object> param);
	
	/**
	 * 根据参数统计同业拆借信息的数量
	 */
	int countIBLend(Map<String, Object> param);
	
	/**
	 * 根据参数统计买断式回购信息的数量
	 */
	int countOutrightRepo(Map<String, Object> param);
	
	/**
	 * 根据参数统计分销买入信息的数量
	 */
	int countDistribution(Map<String, Object> param);
	
	/**
	 * 根据参数统计同业存单发行信息的数量
	 */
	int countIBCDIssue(Map<String, Object> param);
	
	/**
	 * 根据参数查询同业拆借信息
	 */
	List<BookQueryVo> selectIBLend(Map<String,Object> map);
	
	/**
	 * 根据参数查询买断式回购信息
	 */
	List<BookQueryVo> selectOutrightRepo(Map<String,Object> map);
	
	/**
	 * 根据参数查询分销买入信息
	 */
	List<BookQueryVo> selectDistribution(Map<String,Object> map);
	
	/**
	 * 根据参数查询同业存单发行信息
	 */
	List<BookQueryVo> selectIBCDIssue(Map<String,Object> map);
	
	/**
	 * 现券买卖导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expBondBuySellPrint(String order_id);
	
	/**
	 * 买断式回购导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expOutrightRepoPrint(String order_id);
	
	/**
	 * 分销买入导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expDistributionPrint(String order_id);
	
	/**
	 * 同业存单发行导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expIBCDIssuePrint(String order_id);

	/**
	 * 现券买卖导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expPledgedReport(String order_id);
	
	/**
	 * 现券买卖导出打印模板
	 * @param order_id
	 * @return
	 */
	List<Map<String, Object>> expPledgedBondReport(String order_id);

	/**
	 * 同业存放导出打印模板 
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expBankReport(String order_id);
	/**
	 * 票据导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expBillReport(String order_id);
	/**
	 * 非标导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expUnAssetReport(String order_id);
	/**
	 * 同业存放导出打印模板 
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expFromBankReport(String order_id);
/**
	 * 理财投资导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expMoneyInvestReport(String order_id);

	/**
	 * 导出理财投资赎回交易的交易列表
	 * add by kairui @ 20150530
	 * @param order_id
	 * @return
	 */
	List<Map<String,Object>> expMoneyInvestRedemptionReport(String order_id);

	/**
	 * 资管计划打印模板
	 * @param order_id
	 * @return
	 */
	Map<String,Object> expAssetPlanInvestReport(String order_id);

	/**
	 * 导出资管计划卖出交易的标的列表
	 * @param order_id
	 * @return
	 */
	List<Map<String,Object>> expAssetPlanSellTargetReport(String order_id);
	/**
	 * 信托导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expTrustInvestReport(String order_id);
	/**
	 * 定向资产管理计划导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expFixedAssetplanReport(String order_id);
	/**
	 * 同业拆借导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expBankLoanReport(String order_id);
	
	/**
	 * 债券发行导出打印模板
	 * @param order_id
	 * @return
	 */
	Map<String, Object> expBondIssuePrint(String orderId);
	
	/**
	 * 取当日总规模
	 * @return
	 */
	double getTotalAmount();
}
