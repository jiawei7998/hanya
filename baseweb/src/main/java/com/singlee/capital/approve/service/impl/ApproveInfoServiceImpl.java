package com.singlee.capital.approve.service.impl;

import com.joyard.jc.service.CalendarService;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.approve.mapper.ApproveInfoMapper;
import com.singlee.capital.approve.pojo.AccCustCashListByCustomerNoVo;
import com.singlee.capital.approve.pojo.AccOutCashListByCustomerNoVo;
import com.singlee.capital.approve.service.ApproveInfoService;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *  审批单公共信息调用业务层
 * @author gm
 *
 */
@Service("approveInfoService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveInfoServiceImpl implements ApproveInfoService {

	/**用户数据管理*/
	@Autowired
	public TaUserMapper userDao;
	
	/** 审批单公共信息调用dao**/
	@Autowired
	public ApproveInfoMapper approveInfoDao;
	
	/**
	 * 选中内部证券账户后显示关联的交易员列表
	 * is_public = 1 查询所有有效的用户信息
	 * is_public = 0 查询owner用户
	 * @param is_public
	 * @param owner
	 * @return
	 */
	@Override
	public List<TaUser> searchOwnerListByAccId(String is_public,String owner){
		List<TaUser> userList = new ArrayList<TaUser>();
		if(DictConstants.YesNo.YES.equals(is_public)){
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("is_active", DictConstants.AccStatus.Enabled);
			userList = userDao.searchUser(map);
		}else{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("user_id", owner);
			map.put("is_active", DictConstants.AccStatus.Enabled);
			userList = userDao.searchUser(map);
		}
		return userList;
	}
	


	/**
	 * 根据内部证券账户编号查询关联的外部证券信息
	 * @param map
	 * accid：内部证券账户编号
	 * @return
	 */
	@Override
	public List<TtAccOutCash> searchAccOutCashList(String acc_in_secu_id,List<String> acctype_list){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("accid", acc_in_secu_id);
		map.put("acctype_list", acctype_list);
		return approveInfoDao.searchAccOutCashList(map);
	}
	/**
	 * 根据交易对手号，查询该交易对手关联的外部自己账户信息
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	@Override
	public List<AccOutCashListByCustomerNoVo> searchAccOutCashListByPartyId(String party_id,String accid, String inst_id, String settle_date,String status){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("party_id", party_id);
		map.put("accid", accid);
		map.put("inst_id", inst_id);
		map.put("settle_date", settle_date);
		map.put("status", status);
		return approveInfoDao.searchAccOutCashListByPartyId(map);
	}

	/**
	 * 根据交易对手号，查询该交易对手关联的外部账户信息
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	@Override
	public List<AccCustCashListByCustomerNoVo> searchAccCustCashListByPartyId(String party_id,String accid,String opInstId){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("party_id", party_id);
		map.put("accid", accid);
		map.put("inst_id", opInstId);
		return approveInfoDao.searchAccCustCashListByPartyId(map);
	}
	
	/**
	 * 业务流程判断，非交易日不允许进行审批单、委托单、交易单的新增、修改、删除等业务操作
	 */
	@Override
	public void processToJudgeTradeDate(String m_type,String date){
		boolean isTradeDate= CalendarService.getInstance().isIBTradeDate(date);
		JY.require(isTradeDate, "在非交易日不可进行单据操作.");
	}
}
