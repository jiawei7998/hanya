package com.singlee.capital.approve.pojo;
/**
 * 根据交易对手号，查询该交易对手关联的外部自己账户信息
 * @author thinkpad
 *
 */
public class AccOutCashListByCustomerNoVo {

	private String accid;
	private String accname;
	private String bankacc;
	private double curr_rate;
	private String approve_rate;
	private String coupon_type;
	private String open_date;
	private String bank_name;
	private String bank_large_code;
	
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getBank_large_code() {
		return bank_large_code;
	}
	public void setBank_large_code(String bank_large_code) {
		this.bank_large_code = bank_large_code;
	}
	public String getOpen_date() {
		return open_date;
	}
	public void setOpen_date(String open_date) {
		this.open_date = open_date;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public double getCurr_rate() {
		return curr_rate;
	}
	public void setCurr_rate(double curr_rate) {
		this.curr_rate = curr_rate;
	}
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getBankacc() {
		return bankacc;
	}
	public void setBankacc(String bankacc) {
		this.bankacc = bankacc;
	}
	public String getApprove_rate() {
		return approve_rate;
	}
	public void setApprove_rate(String approve_rate) {
		this.approve_rate = approve_rate;
	}
	
}
