package com.singlee.capital.approve.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.BookQueryVo;
import com.singlee.capital.approve.pojo.OrderEditVo;
import com.singlee.capital.approve.service.ApproveInfoService;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.approve.service.ApproveServiceListener;
import com.singlee.capital.base.util.SettleConstants;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SysParamService;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.DurationService;
import com.singlee.capital.trade.service.TradeManageService;
import com.singlee.capital.trade.service.impl.TradeManager;
import com.singlee.capital.trust.mapper.TrdTrustMapper;
import com.singlee.capital.trust.model.TtTrdTrust;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 审批单管理业务层
 * 
 * @author thinkpad
 * 
 */
//@Service("approveManageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveManageServiceImpl implements ApproveManageService, SlbpmCallBackInteface, TaskEventInterface {

	/** 交易单管理dao **/
	@Autowired
	private TrdTradeMapper tradeManageDao;

	/** 委托单管理dao **/
	@Autowired
	private TrdTrustMapper trustManageDao;

	/** 审批单管理dao **/
	@Autowired
	private TrdOrderMapper approveManageDao;

	/** 系统参数表 **/
	@Autowired
	private SysParamService sysParamService;
	@Autowired
	private DurationService durationService;

	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;

	// /** 系统参数表 **/
	// @Autowired
	// private LimitEngine limitEngine;
	//
	// /** 消息发送 **/
	// @Autowired
	// private MsgService msgService;
	//
	/** 审批流程 **/
	// @Autowired
	// private FlowService flowService;
	//
	// @Autowired
	// private TrustMatchingService trustMatchingService;

	@Autowired
	private ApproveInfoService approveInfoService;
	// @Autowired
	// private CreditService creditService;

	@Autowired
	private TradeManageService tradeManageService;
	@Autowired
	private UserParamService userParamService;

	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * 
	 * @param map
	 * @return
	 */
	@Override
	public Object searchApproveList(HashMap<String, Object> map) {
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),
				DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		int pageNumber = ParameterUtil.getInt(map, "pageNumber", 1);
		int pageSize = ParameterUtil.getInt(map, "pageSize", 12);
		String trdtype_search = ParameterUtil.getString(map, "trdtype_search", "");
		String order_status_search = ParameterUtil.getString(map, "order_status_search", "");
		String opUserId = ParameterUtil.getString(map, "opUserId", "");
		map.put("trdtype_search", StringUtil.checkEmptyNull(trdtype_search) ? null : trdtype_search.split(","));
		map.put("order_status_search",
				StringUtil.checkEmptyNull(order_status_search) ? null : order_status_search.split(","));
		map.put("user_id", opUserId);
		map.put("userId", opUserId);
		return approveServiceInterface.searchApproveList(map, pageNumber, pageSize);
	}

	/**
	 * 查询审批单明细信息
	 * 
	 * @param map
	 * @return
	 */
	@Override
	public HashMap<String, Object> selectApproveInfo(HashMap<String, Object> map) {
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("order_id"))), "缺少必须的参数order_id.");

		// 1.查询审批单明细信息
		OrderEditVo orderEditVo = approveManageDao.selectOrderEditForOrderId(map);
		HashMap<String, Object> objMap = ParentChildUtil.ClassToHashMap(orderEditVo);

		// 2.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),
				DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.selectApproveInfo(objMap);

		return objMap;
	}

	/**
	 * 保存审批单信息
	 * 
	 * @param map
	 */
	@Override
	@AutoLogMethod(value = "保存审批单")
	public TtTrdOrder saveApproveInfo(HashMap<String, Object> map) {
		String trdType = ParameterUtil.getString(map, "trdtype", "");
		String type = ParameterUtil.getString(map, "type", "");
		System.out.println(type);
		// 获取具体业务接口
		String folder = ParameterUtil.getString(map, "folder", "");
		if (StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))) {
			try {
				folder = sysParamService.selectSysParam("000002", trdType).getP_prop1();
			} catch (Exception e) {
			}
		}
		if (StringUtil.checkEmptyNull(folder)) {
			TaSysParam sysParam = new TaSysParam();
			sysParam.setP_code("000002");
			sysParam.setP_editabled(DictConstants.YesNo.NO);
			sysParam.setP_name("前台页面业务类型对应folder(系统生成)");
			sysParam.setP_prop1("30");
			sysParam.setP_value(trdType);
			sysParam.setP_type("010002");
			sysParamService.add(sysParam);
			folder = sysParam.getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		// 1.在保存之前处理
		approveServiceInterface.beforeSave(map);
		// 2.增加逻辑，当为非测试环境下，当业务日期 不等于 自然日期时不允许保存审批单
		if (userParamService.getBooleanSysParamByName("system.isJudgeSysDateAndBusDate", false)) {
			String settlementDate = dayendDateService.getSettlementDate();
			JY.require(DateUtil.getCurrentDateAsString().equals(settlementDate), "业务日历不等于当前日期,无法执行保存操作.");
		}
//		// 3.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		TtTrdOrder ttTrdOrder = new TtTrdOrder();
		// 4.将hashmap转换为TtTrdOrder对象
		ParentChildUtil.HashMapToClass(map, ttTrdOrder);
		// 增加内部账户判断
		// TtAccInSecu ttAccInSecu =
		// accInSecuService.getAcc(ttTrdOrder.getSelf_insecuaccid());
		// JY.require(ttAccInSecu != null &&
		// DictConstants.AccStatus.Enabled.equals(ttAccInSecu.getStatus()),
		// "保存审批单时,请确认本方内部证券账户为可用状态.");
		if (StringUtil.checkEmptyNull(ttTrdOrder.getSelf_incashaccid())) {
			ttTrdOrder.setSelf_incashaccid(ParameterUtil.getString(map, "accInCash", ""));
		}
		JY.require(!StringUtil.checkEmptyNull(ttTrdOrder.getSelf_incashaccid()), "保存审批单时,内部资金账号不能为空.");
		// 增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdOrder.getM_type(), ttTrdOrder.getOrder_date());
		boolean isAdd = true;
		// 5.当order_id存在时，锁定order记录,当order_id不存在时，则预先取得orader_id的值
		String opUserId = ParameterUtil.getString(map, "opUserId", "");
		if (!StringUtil.checkEmptyNull(ttTrdOrder.getOrder_id())) {
			isAdd = false;
			HashMap<String, Object> mapRow = new HashMap<String, Object>();
			mapRow.put("order_id", map.get("order_id"));
			// mapRow.put("is_locked", true);
			TtTrdOrder ttTrdOrderOld = approveManageDao.selectOrderForOrderId(mapRow);
			if (DictConstants.TrdType.EntrustAssetPlan.equals(ttTrdOrderOld.getTrdtype())) {
				if (DictConstants.ApproveStatus.Verified.equals(ttTrdOrderOld.getOrder_status())
						|| DictConstants.ApproveStatus.ApprovedPass.equals(ttTrdOrderOld.getOrder_status())) {
					ttTrdOrderOld.setOrder_status(DictConstants.ApproveStatus.New);
				}
			}
			// 修改时校验规则
			JY.require(DictConstants.ApproveStatus.New.equals(ttTrdOrderOld.getOrder_status()), "仅允许修改状态为[新增]的审批单据.");
			JY.require(
					StringUtil.isNullOrEmpty(ttTrdOrderOld.getUser_id()) || opUserId.equals(ttTrdOrderOld.getUser_id()),
					"仅允许修改本人所属单据.");

		} else {
			isAdd = true;
			String order_id = "";
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", map.get("trdtype"));
			order_id = approveManageDao.getOrderId(mapSel);

			ttTrdOrder.setOrder_id(order_id);
			ttTrdOrder.setOrder_time(DateUtil.getCurrentDateTimeAsString());
			ttTrdOrder.setUser_id(opUserId);
		}

		// 增加交易对手客户大类与小类所属判断
		/*
		 * String partyId = ParameterUtil.getString(map, "party_id",
		 * ttTrdOrder.getParty_id());
		 * 
		 * CounterPartyVo counterPartyVo = counterPartyService.selectCP(partyId, null);
		 * JY.require(counterPartyVo != null, "客户%s数据有问题，请检查", partyId);
		 * JY.require(!StringUtil.checkEmptyNull(counterPartyVo.getParty_bigkind()),
		 * "未设置客户所属客户大类.");
		 * JY.require(!StringUtil.checkEmptyNull(counterPartyVo.getParty_smallkind()),
		 * "未设置客户所属客户小类.");
		 */
		// 6.默认属性赋值
		ttTrdOrder.setIs_active(DictConstants.YesNo.YES);
		ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.New);
		ttTrdOrder.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdOrder.setOperator_id(opUserId);

		// 7.调用其他业务接口
		approveServiceInterface.saveApprove(map, ttTrdOrder, isAdd);
		// 5.根据order_id判断执行新增或修改操作
		if (isAdd) {
			approveManageDao.insert(ttTrdOrder);
		} else {
			approveManageDao.updateApprove(ttTrdOrder);
		}
		return ttTrdOrder;
	}

	/**
	 * 注销审批单信息
	 * 
	 * @param order_id
	 */
	@Override
	@AutoLogMethod(value = "取消审批单")
	public void cancelApprove(String order_id, String folder, String opUserId) {
//		// 2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId()==null?"999999" : SlSessionHelper.getInstitutionId());
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", order_id);
		mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(mapRow);
		String order_status = ttTrdOrder.getOrder_status();
		/*
		 * if(!folder.equals("53")){ if
		 * (DictConstants.ApproveStatus.WaitApprove.equals(order_status) ||
		 * DictConstants.ApproveStatus.Executed.equals(order_status) ||
		 * DictConstants.ApproveStatus.Executing.equals(order_status) ||
		 * DictConstants.ApproveStatus.Cancle.equals(order_status)) {
		 * JY.raise("不能注销[待审批]、[执行中]、[执行完成]与[已注销]的审批单据."); }
		 * 
		 * }
		 */

		// 6.消息发送
		// List<ApproveLogVo> list = flowService.searchFlowLog("", "", order_id,
		// "", "", "",false);
		// for (ApproveLogVo approveLogVo : list) {
		// //最后审批通过时user_id的编号为空格，需过滤该数据
		// if(StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim())){
		// continue;
		// }
		// // msgService.puslishApprovedCancel(order_id,
		// approveLogVo.getUser_id().trim());
		// }

		// 待审批与审批中的单据在撤销审批时需取消审批流程
		if (DictConstants.ApproveStatus.Approving.equals(order_status)
				|| DictConstants.ApproveStatus.WaitApprove.equals(order_status)) {
			// flowService.endFlowInstance(opUserId, ttTrdOrder.getOrder_id(),
			// "撤销审批", DictConstants.ApproveStatus.Cancle);
		}
		opUserId = StringUtil.isNullOrEmpty(opUserId) ? "admin" : opUserId;
		if (DictConstants.ApproveStatus.New.equals(ttTrdOrder.getOrder_status())) {
			// 1.审批单状态为新增时,禁用该审批单
			ttTrdOrder.setIs_active(DictConstants.YesNo.NO);
			ttTrdOrder.setOperate_time(DateUtil.getCurrentDateTimeAsString());
			ttTrdOrder.setOperator_id(opUserId);
			approveManageDao.updateApprove(ttTrdOrder);
		}
		/*
		 * if(folder.equals("53")){
		 * if(DictConstants.ApproveStatus.WaitApprove.equals(order_status)||
		 * DictConstants.ApproveStatus.Executed.equals(order_status) ||
		 * DictConstants.ApproveStatus.Executing.equals(order_status)){
		 * statusChange(DictConstants.FlowType.BusinessApproveFlow, "",
		 * ttTrdOrder.getOrder_id(), DictConstants.ApproveStatus.Cancle, ""); } }
		 */
		else if (DictConstants.ApproveStatus.WaitApprove.equals(order_status)) {

			// 审批单修改时，该审批单状态变更已通过"flowService.endFlowInstance"调用statusChange此处则无需再调用
		} else {
			// 2.审批单状态不等于新增时，更改审批单状态(调用statusChange方法，此逻辑需要调用限额接口)
			statusChange(DictConstants.FlowType.BusinessApproveFlow, "", ttTrdOrder.getOrder_id(),
					DictConstants.ApproveStatus.Cancle, "");
		}

		// 4.调用其他业务接口
		if (!"admin".equals(opUserId)) {
			JY.require(StringUtils.isEmpty(ttTrdOrder.getUser_id()) || opUserId.equals(ttTrdOrder.getUser_id()),
					"仅允许修改本人所属单据.");
		}
		if (StringUtil.isNullOrEmpty(folder)) {
			folder = sysParamService.selectSysParam("000002", ttTrdOrder.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.cancelApprove(ttTrdOrder);
	}

	/**
	 * 审批拒绝
	 * 
	 * @param order_id
	 */
	@Override
	public void noPassApprove(TtTrdOrder ttTrdOrder, String folder) {
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.noPassApprove(ttTrdOrder);

	}

	/**
	 * 流程状态变更(用于审批流程函数回调)
	 * 
	 * @param flow_id   流程id
	 * @param serial_no 序号
	 * @param status    最新审批状态
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		// Map<String, Object> prdPropMap = null;
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", serial_no);
		mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(mapRow);
		// 保存原来的审批状态
		// String oldOrderStatus = ttTrdOrder.getOrder_status();
		// 更新审批状态
		if (DictConstants.FlowType.BusinessApproveFlow.equals(flow_type)) {
			ttTrdOrder.setOrder_status(status);
		}
		ttTrdOrder.setStrike_yield(0);
		approveManageDao.updateApprove(ttTrdOrder);
		// if(DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype()))
		// prdPropMap = productAndApproveMainService.getProductMain(serial_no); //
		// 获取交易单产品性质(1.资产，2负债，3投资额度)

		// 限额接口调用：更改后审批单状态为【待审批】则需要增加限额，审批单状态为【审批拒绝】、【新建】或【注销】时限额为减
		// boolean isCancel = false;
		if (DictConstants.ApproveStatus.New.equals(status)) {
			// isCancel = true;
			// 判断释放额度(退回发起或者驳回到新建)
			/*
			 * if (DictConstants.prdProp.assets.equals(prdPropMap.get("prdProp"))) { //
			 * 非投资额度流程才会存在释放 if
			 * (DictConstants.FlowCompleteType.Redo.equals(flowCompleteType) ||
			 * DictConstants.FlowCompleteType.Back.equals(flowCompleteType)) {
			 * trdQuotaService.orderReleaseQuota(serial_no); } }
			 */

			// 6.消息发送
			// List<ApproveLogVo> list = flowService.searchFlowLog("", "",
			// serial_no, "", "", "",false);
			// for (ApproveLogVo approveLogVo : list) {
			// //最后审批通过时user_id的编号为空格，需过滤该数据
			// if(StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim()) ||
			// ttTrdOrder.getUser_id().equals(approveLogVo.getUser_id())){
			// continue;
			// }
			// // msgService.puslishApprovingCancel(serial_no,
			// approveLogVo.getUser_id().trim());
			// }
		} else if (DictConstants.ApproveStatus.Cancle.equals(status)) {
			// isCancel = true;
			// 注销要释放额度

			// 6.消息发送
			// List<ApproveLogVo> list = flowService.searchFlowLog("", "",
			// serial_no, "", "", "",false);
			// for (ApproveLogVo approveLogVo : list) {
			// //最后审批通过时user_id的编号为空格，需过滤该数据
			// if(StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim()) ||
			// ttTrdOrder.getUser_id().equals(approveLogVo.getUser_id())){
			// continue;
			// }
			// // msgService.puslishApproveCancel(serial_no,
			// approveLogVo.getUser_id().trim());
			// }
		} else if (DictConstants.ApproveStatus.WaitApprove.equals(status)) {
			// isCancel = false;
			// 提交审批到待审批，审批单开始占用额度
			if (DictConstants.TrdType.Custom.equals(ttTrdOrder.getTrdtype())) {

			}

			// //是否占用同业授信额度(信贷系统处理)
			// if(userParamService.getBooleanSysParamByName("system.isSendCreditSys",
			// true)){
			//
			// }

		} else if (DictConstants.ApproveStatus.ApprovedNoPass.equals(status)) {
			// 消息发送功能
			// msgService.puslishApprovedNoPass(ttTrdOrder.getOrder_id(),
			// ttTrdOrder.getSelf_traderid());
			// isCancel = true;

			// 释放额度 （拒绝动作到拒绝）

		} else if (DictConstants.ApproveStatus.ApprovedPass.equals(status)) {
			// msgService.puslishApprovedPassed(ttTrdOrder.getOrder_id(),
			// ttTrdOrder.getSelf_traderid());
			String folder = sysParamService.selectSysParam("000002", ttTrdOrder.getTrdtype()).getP_prop1();
			// 审批完成
			if (tradeManageService.needTrade(ttTrdOrder, folder)) {
				// 3.向trade表插入数据
				TradeManager r = new TradeManager();
				TtTrdTrade trdTrade = r.getTradeByOrder(ttTrdOrder);
				trdTrade.setTrade_id("");
				HashMap<String, Object> map = ParentChildUtil.ClassToHashMap(trdTrade);
				map.put("opUserId", ttTrdOrder.getOperator_id());
				// map.put("prdProp", prdPropMap.get("prdProp"));
				tradeManageService.saveTradeInfo(map);
				updateApproveRemainAmount(ttTrdOrder, 0, ttTrdOrder.getTotalamount(), true,
						ttTrdOrder.getOperator_id());
			} else {
				tradeManageService.notNeedTradeProcess(ttTrdOrder, folder);
			}
		} else if (DictConstants.ApproveStatus.ApprovedNoPass.equals(status)) {
			this.noPassApprove(ttTrdOrder,
					sysParamService.selectSysParam("000002", ttTrdOrder.getTrdtype()).getP_prop1());
		} else {
		}

		/*
		 * List<TrdTradeVo> trades = new ArrayList<TrdTradeVo>(); TradeManager mgr = new
		 * TradeManager(); TrdTradeVo trade = null; //
		 * if(DictConstants.ApproveStatus.Cancle.equals(status) &&
		 * ttTrdOrder.getPreorder_amount() > ttTrdOrder.getRemain_amount()){ // trade =
		 * mgr.getTradeByOrder(ttTrdOrder,true); // } // else{ trade =
		 * mgr.getTradeByOrder(ttTrdOrder); // } trades.add(trade); List<String>
		 * limitKindList = new ArrayList<String>();
		 * limitKindList.add(DictConstants.LimitKind.NormalLimit);
		 * limitKindList.add(DictConstants.LimitKind.ApproveScene);
		 * 
		 * limitEngine.limitRefresh(trades,limitKindList ,flow_id,
		 * Constants.LimitCallType.APPFLOW.toString(),isCancel); //取消信贷授信额度
		 * if(isCancel){ //是否部分注销 //
		 * if(DictConstants.ApproveStatus.Cancle.equals(status) &&
		 * ttTrdOrder.getPreorder_amount() > ttTrdOrder.getRemain_amount()){ //
		 * creditService.creditRelease4partCancel(trade, isCancel,
		 * DictConstants.CreditSourceType.Approve); // } // else{
		 * //如果老状态为审批中，新状态为新建，则不释放授信
		 * if(DictConstants.ApproveStatus.Approving.equals(oldOrderStatus) &&
		 * DictConstants.ApproveStatus.New.equals(status)){
		 * 
		 * } else{ String errorInfo = creditService.creditHandle(trade,
		 * true,DictConstants.CreditSourceType.Approve,false);
		 * if(!StringUtil.checkEmptyNull(errorInfo)){ throw new RException("信贷额度处理出错！" +
		 * errorInfo); } } // } }
		 */
	}

	/**
	 * 审批单生成交易单
	 * 
	 * @param map
	 */
	@Override
	@AutoLogMethod(value = "审批单生成交易单")
	public TtTrdTrade createTrade(HashMap<String, Object> map) {
		String opUserId = ParameterUtil.getString(map, "operator_id", "");
//		// 0.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(opInstId);
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("order_id"))), "缺少必须的参数order_id.");
		// 1.锁定原纪录
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", map.get("order_id"));
		mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrderOld = approveManageDao.selectOrderForOrderId(mapRow);
		if (!(ttTrdOrderOld.getTrdtype().equals(DictConstants.TrdType.EntrustAssetPlan)
				|| ttTrdOrderOld.getTrdtype().equals(DictConstants.TrdType.EntrustAssetPlanConfig))) {
			// 2.规则验证
			JY.require(
					DictConstants.ApproveStatus.Executing.equals(ttTrdOrderOld.getOrder_status())
							|| DictConstants.ApproveStatus.ApprovedPass.equals(ttTrdOrderOld.getOrder_status()),
					"仅允许状态为[审批通过]或[执行中]的审批单据执行生成交易单操作.");
			JY.require(opUserId.equals(ttTrdOrderOld.getUser_id()), "仅允许修改本人所属单据.");
		}
		// 4.交易单对象转换
		TtTrdTrade ttTrdTrade = OrderTransTrade(ttTrdOrderOld);
		ttTrdTrade.setOperator_id(opUserId);
		// 增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdTrade.getM_type(), ttTrdTrade.getTrade_date());
		// 增加交易日与当前业务日不符，不允许生成交易单。
		if (!(DictConstants.TrdType.IBCDIssue.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.AssetPlanBuy.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.AssetPlanSell.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.EntrustAssetPlan.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.MoneyProdIssue.equals(ttTrdTrade.getTrdtype()))) {
			JY.require(dayendDateService.isSettlementDate(ttTrdTrade.getTrade_date()), "交易日期%s与当前业务日不符，不允许生成交易单。",
					ttTrdTrade.getTrade_date());
		}
		// 5.调用其他业务接口
		String folder = ParameterUtil.getString(map, "folder", "");
		if (StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))) {
			folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.createTrade(map, ttTrdTrade);
		// 6.交易单写入
		tradeManageDao.insertTrade(ttTrdTrade);

		// 7.更新审批单额度
		updateApproveRemainAmount(ttTrdOrderOld, 0, ttTrdTrade.getTotalamount(), true, opUserId);

		// 8 交易单后续处理流程
		approveServiceInterface.createdTradeHandle(ttTrdTrade, folder);

		return ttTrdTrade;
	}

	/**
	 * 将审批单对象转换为交易单对象
	 * 
	 * @return
	 */
	private TtTrdTrade OrderTransTrade(TtTrdOrder ttTrdOrder) {
		TtTrdTrade ttTrdTrade = new TtTrdTrade();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trdtype", ttTrdOrder.getTrdtype());
		ttTrdTrade.setTrade_id(tradeManageDao.getTradeId(map));

		ttTrdTrade.setVersion_number(0);
		ttTrdTrade.setTrade_date(ttTrdOrder.getOrder_date());
		ttTrdTrade.setTrade_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setTrade_user(ttTrdOrder.getUser_id());
		// ttTrdTrade.setConfirm_date(ttTrdOrder.getConfirm_date());
		// ttTrdTrade.setConfirm_time(ttTrdOrder.getConfirm_time());
		// ttTrdTrade.setConfirm_user(ttTrdOrder.getConfirm_user());
		ttTrdTrade.setOrder_id(ttTrdOrder.getOrder_id());
		// ttTrdTrade.setTrust_id(ttTrdOrder.getTrust_id());
		// ttTrdTrade.setTrust_version(ttTrdOrder.getTrust_version());
		ttTrdTrade.setIn_tradeid(ttTrdOrder.getIn_orderid());
		// ttTrdTrade.setOut_tradeid(ttTrdOrder.getOut_tradeid());
		// ttTrdTrade.setCust_tradeid(ttTrdOrder.getCust_tradeid());
		ttTrdTrade.setSelf_zzdaccid(ttTrdOrder.getSelf_zzdaccid());
		ttTrdTrade.setParty_zzdaccid(ttTrdOrder.getParty_zzdaccid());
		ttTrdTrade.setTrdtype(ttTrdOrder.getTrdtype());
		ttTrdTrade.setParty_id(ttTrdOrder.getParty_id());
		ttTrdTrade.setI_code(ttTrdOrder.getI_code());
		ttTrdTrade.setA_type(ttTrdOrder.getA_type());
		ttTrdTrade.setM_type(ttTrdOrder.getM_type());
		ttTrdTrade.setI_name(ttTrdOrder.getI_name());
		ttTrdTrade.setErr_code(ttTrdOrder.getErr_code());
		ttTrdTrade.setErr_info(ttTrdOrder.getErr_info());
		ttTrdTrade.setGroup_id(ttTrdOrder.getGroup_id());
		// ttTrdTrade.setRef_tradedate(ttTrdOrder.getRef_tradedate());
		// ttTrdTrade.setRef_tradeid(ttTrdOrder.getRef_tradeid());
		ttTrdTrade.setExe_market(ttTrdOrder.getExe_market());
		ttTrdTrade.setTrade_source(DictConstants.TrustSource.SysTrust);
		ttTrdTrade.setImp_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setFst_settle_type(ttTrdOrder.getFst_settle_type());
		ttTrdTrade.setEnd_settle_type(ttTrdOrder.getEnd_settle_type());
		ttTrdTrade.setFst_settle_date(ttTrdOrder.getFst_settle_date());
		ttTrdTrade.setEnd_settle_date(ttTrdOrder.getEnd_settle_date());
		ttTrdTrade.setTerm_days(ttTrdOrder.getTerm_days());
		ttTrdTrade.setSettle_days(ttTrdOrder.getSettle_days());
		ttTrdTrade.setParty_type(ttTrdOrder.getParty_type());
		ttTrdTrade.setTotalamount(ttTrdOrder.getTotalamount());
		ttTrdTrade.setTotalcount(ttTrdOrder.getTotalcount());
		ttTrdTrade.setYtm(ttTrdOrder.getYtm());
		ttTrdTrade.setNetprice(ttTrdOrder.getNetprice());
		ttTrdTrade.setFullprice(ttTrdOrder.getFullprice());
		ttTrdTrade.setAiprice(ttTrdOrder.getAiprice());
		ttTrdTrade.setNetamount(ttTrdOrder.getNetamount());
		ttTrdTrade.setFullamount(ttTrdOrder.getFullamount());
		ttTrdTrade.setAiamount(ttTrdOrder.getAiamount());
		ttTrdTrade.setSelf_traderid(ttTrdOrder.getSelf_traderid());
		ttTrdTrade.setSelf_tradername(ttTrdOrder.getSelf_tradername());
		ttTrdTrade.setSelf_bankacccode(ttTrdOrder.getSelf_bankacccode());
		ttTrdTrade.setSelf_bankaccname(ttTrdOrder.getSelf_bankaccname());
		ttTrdTrade.setSelf_bankcode(ttTrdOrder.getSelf_bankcode());
		ttTrdTrade.setSelf_bankname(ttTrdOrder.getSelf_bankname());
		ttTrdTrade.setSelf_largepaymentcode(ttTrdOrder.getSelf_largepaymentcode());
		ttTrdTrade.setSelf_insecuaccid(ttTrdOrder.getSelf_insecuaccid());
		ttTrdTrade.setSelf_outsecuaccid(ttTrdOrder.getSelf_outsecuaccid());
		ttTrdTrade.setSelf_incashaccid(ttTrdOrder.getSelf_incashaccid());
		ttTrdTrade.setSelf_outcashaccid(ttTrdOrder.getSelf_outcashaccid());
		ttTrdTrade.setParty_traderid(ttTrdOrder.getParty_traderid());
		ttTrdTrade.setParty_tradername(ttTrdOrder.getParty_tradername());
		ttTrdTrade.setParty_bankacccode(ttTrdOrder.getParty_bankacccode());
		ttTrdTrade.setParty_bankaccname(ttTrdOrder.getParty_bankaccname());
		ttTrdTrade.setParty_bankcode(ttTrdOrder.getParty_bankcode());
		ttTrdTrade.setParty_bankname(ttTrdOrder.getParty_bankname());
		ttTrdTrade.setParty_largepaymentcode(ttTrdOrder.getParty_largepaymentcode());
		ttTrdTrade.setParty_insecuaccid(ttTrdOrder.getParty_insecuaccid());
		ttTrdTrade.setParty_outsecuaccid(ttTrdOrder.getParty_outsecuaccid());
		ttTrdTrade.setParty_incashaccid(ttTrdOrder.getParty_incashaccid());
		ttTrdTrade.setParty_outcashaccid(ttTrdOrder.getParty_outcashaccid());
		ttTrdTrade.setTrade_status(DictConstants.TradeStatus.NEW);
		ttTrdTrade.setOperator_id(ttTrdOrder.getOperator_id());
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setMemo(ttTrdOrder.getMemo());
		ttTrdTrade.setZzd_netprice(ttTrdOrder.getZzd_netprice());
		ttTrdTrade.setZzd_ytm(ttTrdOrder.getZzd_ytm());
		ttTrdTrade.setTradeamount(ttTrdOrder.getFst_settle_amount());
		ttTrdTrade.setIs_active(ttTrdOrder.getIs_active());
		ttTrdTrade.setTrade_fee(ttTrdOrder.getTrade_fee());
		ttTrdTrade.setSettle_fee(ttTrdOrder.getSettle_fee());
		ttTrdTrade.setSettleamount(
				ttTrdOrder.getFst_settle_amount() + ttTrdOrder.getTrade_fee() + ttTrdOrder.getSettle_fee());
		ttTrdTrade.setFst_settle_amount(ttTrdOrder.getFst_settle_amount());
		ttTrdTrade.setEnd_settle_amount(ttTrdOrder.getEnd_settle_amount());
		ttTrdTrade.setStrike_yield(ttTrdOrder.getStrike_yield());
		return ttTrdTrade;
	}

	/**
	 * 审批单生成委托单
	 * 
	 * @param map
	 */
	@Override
	@AutoLogMethod(value = "审批单生成委托单")
	public TtTrdTrust createTrust(HashMap<String, Object> map) {
		String opUserId = ParameterUtil.getString(map, "operator_id", "");
//		// 2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(opInstId);

		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("order_id"))), "缺少必须的参数order_id.");
		// 1.锁定原纪录
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", map.get("order_id"));
		mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrderOld = approveManageDao.selectOrderForOrderId(mapRow);

		// 2.规则验证
		JY.require(
				DictConstants.ApproveStatus.Executing.equals(ttTrdOrderOld.getOrder_status())
						|| DictConstants.ApproveStatus.ApprovedPass.equals(ttTrdOrderOld.getOrder_status()),
				"仅允许状态为[审批通过]或[执行中]的审批单据执行生成委托单操作.");
		JY.require(opUserId.equals(ttTrdOrderOld.getUser_id()), "仅允许修改本人所属单据.");
		JY.require(SettleConstants.allowCreateTrust(ttTrdOrderOld.getTrdtype()), "当前业务不允许直接生成委托单.");

		// 4.交易单对象转换
		TtTrdTrust ttTrdTrust = OrderTransTrust(ttTrdOrderOld);
		ttTrdTrust.setOperator_id(opUserId);
		// 增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdTrust.getM_type(), ttTrdTrust.getTrust_date());
		// 增加交易日与当前业务日不符，不允许生成委托单。
		JY.require(dayendDateService.isSettlementDate(ttTrdTrust.getTrust_date()), "交易日期%s与当前业务日不符，不允许生成委托单。",
				ttTrdTrust.getTrust_date());
		// 5.调用其他业务接口
		String folder = ParameterUtil.getString(map, "folder", "");
		if (StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))) {
			folder = sysParamService.selectSysParam("000002", ttTrdTrust.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.createTrust(map, ttTrdTrust);

		// 6.交易单写入
		trustManageDao.insertTrust(ttTrdTrust);
		// 7.自动匹配成交单
		// trustMatchingService.autoMatchingCFETS(ttTrdTrust);
		// 8.更新审批单额度
		updateApproveRemainAmount(ttTrdOrderOld, 0, ttTrdTrust.getTotalamount(), true, opUserId);
		return ttTrdTrust;
	}

	/**
	 * 将审批单对象转换为委托单对象
	 * 
	 * @return
	 */
	private TtTrdTrust OrderTransTrust(TtTrdOrder ttTrdOrder) {
		TtTrdTrust ttTrdTrust = new TtTrdTrust();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trdtype", ttTrdOrder.getTrdtype());
		ttTrdTrust.setTrust_id(trustManageDao.getTrustId(map));

		// String settle_date = settlementDate;
		String now_time = DateUtil.getCurrentDateTimeAsString();

		ttTrdTrust.setVersion_number(0);
		ttTrdTrust.setTrust_date(ttTrdOrder.getOrder_date());
		ttTrdTrust.setTrust_time(now_time);
		ttTrdTrust.setConfirm_date("");
		ttTrdTrust.setConfirm_time("");
		ttTrdTrust.setOrder_id(ttTrdOrder.getOrder_id());
		ttTrdTrust.setSelf_zzdaccid(ttTrdOrder.getSelf_zzdaccid());
		ttTrdTrust.setParty_zzdaccid(ttTrdOrder.getParty_zzdaccid());
		ttTrdTrust.setTrdtype(ttTrdOrder.getTrdtype());
		ttTrdTrust.setParty_id(ttTrdOrder.getParty_id());
		ttTrdTrust.setI_code(ttTrdOrder.getI_code());
		ttTrdTrust.setA_type(ttTrdOrder.getA_type());
		ttTrdTrust.setM_type(ttTrdOrder.getM_type());
		ttTrdTrust.setI_name(ttTrdOrder.getI_name());
		ttTrdTrust.setErr_code(ttTrdOrder.getErr_code());
		ttTrdTrust.setErr_info(ttTrdOrder.getErr_info());
		ttTrdTrust.setGroup_id(ttTrdOrder.getGroup_id());
		ttTrdTrust.setRef_tradedate("");
		ttTrdTrust.setRef_tradeid("");
		ttTrdTrust.setExe_market(ttTrdOrder.getExe_market());
		ttTrdTrust.setTrust_source(DictConstants.TrustSource.SysTrust);
		ttTrdTrust.setImp_time(ttTrdOrder.getImp_time());
		ttTrdTrust.setFst_settle_type(ttTrdOrder.getFst_settle_type());
		ttTrdTrust.setEnd_settle_type(ttTrdOrder.getEnd_settle_type());
		ttTrdTrust.setFst_settle_date(ttTrdOrder.getFst_settle_date());
		ttTrdTrust.setEnd_settle_date(ttTrdOrder.getEnd_settle_date());
		ttTrdTrust.setTerm_days(ttTrdOrder.getTerm_days());
		ttTrdTrust.setSettle_days(ttTrdOrder.getSettle_days());
		ttTrdTrust.setParty_type(ttTrdOrder.getParty_type());
		ttTrdTrust.setTotalamount(ttTrdOrder.getTotalamount());
		ttTrdTrust.setTotalcount(ttTrdOrder.getTotalcount());
		ttTrdTrust.setYtm(ttTrdOrder.getYtm());
		ttTrdTrust.setNetprice(ttTrdOrder.getNetprice());
		ttTrdTrust.setFullprice(ttTrdOrder.getFullprice());
		ttTrdTrust.setAiprice(ttTrdOrder.getAiprice());
		ttTrdTrust.setNetamount(ttTrdOrder.getNetamount());
		ttTrdTrust.setFullamount(ttTrdOrder.getFullamount());
		ttTrdTrust.setAiamount(ttTrdOrder.getAiamount());
		ttTrdTrust.setSelf_traderid(ttTrdOrder.getSelf_traderid());
		ttTrdTrust.setSelf_tradername(ttTrdOrder.getSelf_tradername());
		ttTrdTrust.setSelf_bankacccode(ttTrdOrder.getSelf_bankacccode());
		ttTrdTrust.setSelf_bankaccname(ttTrdOrder.getSelf_bankaccname());
		ttTrdTrust.setSelf_bankcode(ttTrdOrder.getSelf_bankcode());
		ttTrdTrust.setSelf_bankname(ttTrdOrder.getSelf_bankname());
		ttTrdTrust.setSelf_largepaymentcode(ttTrdOrder.getSelf_largepaymentcode());
		ttTrdTrust.setSelf_insecuaccid(ttTrdOrder.getSelf_insecuaccid());
		ttTrdTrust.setSelf_outsecuaccid(ttTrdOrder.getSelf_outsecuaccid());
		ttTrdTrust.setSelf_incashaccid(ttTrdOrder.getSelf_incashaccid());
		ttTrdTrust.setSelf_outcashaccid(ttTrdOrder.getSelf_outcashaccid());
		ttTrdTrust.setParty_traderid(ttTrdOrder.getParty_traderid());
		ttTrdTrust.setParty_tradername(ttTrdOrder.getParty_tradername());
		ttTrdTrust.setParty_bankacccode(ttTrdOrder.getParty_bankacccode());
		ttTrdTrust.setParty_bankaccname(ttTrdOrder.getParty_bankaccname());
		ttTrdTrust.setParty_bankcode(ttTrdOrder.getParty_bankcode());
		ttTrdTrust.setParty_bankname(ttTrdOrder.getParty_bankname());
		ttTrdTrust.setParty_largepaymentcode(ttTrdOrder.getParty_largepaymentcode());
		ttTrdTrust.setParty_insecuaccid(ttTrdOrder.getParty_insecuaccid());
		ttTrdTrust.setParty_outsecuaccid(ttTrdOrder.getParty_outsecuaccid());
		ttTrdTrust.setParty_incashaccid(ttTrdOrder.getParty_incashaccid());
		ttTrdTrust.setParty_outcashaccid(ttTrdOrder.getParty_outcashaccid());
		ttTrdTrust.setTrust_status(DictConstants.TrustStatus.NEW);
		ttTrdTrust.setOperator_id(ttTrdOrder.getOperator_id());
		ttTrdTrust.setOperate_time(now_time);
		ttTrdTrust.setMemo(ttTrdOrder.getMemo());
		ttTrdTrust.setZzd_netprice(ttTrdOrder.getZzd_netprice());
		ttTrdTrust.setZzd_ytm(ttTrdOrder.getZzd_ytm());
		ttTrdTrust.setPretrust_amount(ttTrdOrder.getTotalamount());
		ttTrdTrust.setUsed_amount(0);
		ttTrdTrust.setRemain_amount(ttTrdOrder.getTotalamount());
		ttTrdTrust.setIs_active(ttTrdOrder.getIs_active());
		ttTrdTrust.setTrade_fee(ttTrdOrder.getTrade_fee());
		ttTrdTrust.setSettle_fee(ttTrdOrder.getSettle_fee());
		ttTrdTrust.setConfirm_user("");
		ttTrdTrust.setTrust_user(ttTrdOrder.getUser_id());
		ttTrdTrust.setFst_settle_amount(ttTrdOrder.getFst_settle_amount());
		ttTrdTrust.setEnd_settle_amount(ttTrdOrder.getEnd_settle_amount());
		ttTrdTrust.setStrike_yield(ttTrdOrder.getStrike_yield());
		return ttTrdTrust;
	}

	/**
	 * 更新审批单余额信息
	 * 
	 * @param order_id
	 * @param addAmount
	 * @param subAmount
	 * @param isUsed    (主要解决 used_amount remain_amount preorder_amount
	 *                  均为0的情况，如活期销户)
	 *                  true表示,增加Used_amount减少Remain_amount,表示生成委托单、交易单
	 *                  false表示,减少Used_amount增加Remain_amount,表示注销委托单、交易单
	 */
	@Override
	public void updateApproveRemainAmount(TtTrdOrder ttTrdOrder, double oldAmount, double newAmount, boolean isUsed,
			String opUserId) {
		// 1.计算审批单额度
		double used_amount = ttTrdOrder.getUsed_amount();
		double remain_amount = ttTrdOrder.getRemain_amount();
		double preorder_amount = ttTrdOrder.getPreorder_amount();
		used_amount = used_amount - oldAmount + newAmount;
		remain_amount = remain_amount + oldAmount - newAmount;
		JY.require(remain_amount >= 0, "审批单[" + ttTrdOrder.getOrder_id() + "]剩余额度不足.");

		ttTrdOrder.setRemain_amount(remain_amount);
		ttTrdOrder.setUsed_amount(used_amount);

		// 2.审批单状态判断
		String order_status = "";
		if (isUsed) {
			if (used_amount == preorder_amount) {
				order_status = DictConstants.ApproveStatus.ApprovedPass;// mod
																		// by
																		// lihuabing
																		// 解决审批单生成核实单状态问题
			} else {
				order_status = DictConstants.ApproveStatus.Executing;
			}
		} else {
			if (remain_amount == preorder_amount) {
				order_status = DictConstants.ApproveStatus.ApprovedPass;
			} else {
				order_status = DictConstants.ApproveStatus.Executing;
			}
		}
		ttTrdOrder.setOrder_status(order_status);

		ttTrdOrder.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdOrder.setOperator_id(opUserId);

		// 3.更新纪录
		approveManageDao.updateApprove(ttTrdOrder);
	}

	@Override
	public Page<BookQueryVo> getBookList(Map<String, Object> map, int pageNum, int pageSize) {
		Page<BookQueryVo> ph = new Page<BookQueryVo>();
		return ph;
	}

	@Override
	public void updateApproveCreditAmount(String orderId, double amount) {
		if (StringUtil.isNullOrEmpty(orderId)) {
			throw new RException("更新信贷额度出错，审批单号不能为空");
		}
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", orderId);
		mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(mapRow);
		if (ttTrdOrder == null) {
			throw new RException("更新信贷额度出错，找不到对应的审批单信息,审批单号:" + orderId);
		}
		approveManageDao.updateApprove(ttTrdOrder);
	}

	/**
	 * 删除order数据
	 */
	@Override
	public void delTrdOrder(TtTrdOrder order) {
		approveManageDao.deleteByPrimaryKey(order);

	}

	/**
	 * 获取order详细对象
	 */
	@Override
	public TtTrdOrder getTrdorder(TtTrdOrder order) {
		return approveManageDao.selectByPrimaryKey(order);
	}

	@Override
	public double getTotamAmount() {
		return approveManageDao.getTotalAmount();
	}

	/**
	 * 修改审批单
	 * 
	 * @param order
	 */
	@Override
	public void updateOrder(TtTrdOrder order) {
		approveManageDao.updateApprove(order);
	}

	/**
	 * 存续期结算退回前台
	 */
	@Override
	public void withdrawOrder(String trade_id, String bustype) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("order_id", null);
		TtTrdTrade trade = tradeManageService.selectOneTradeIdorOrderId(map);

		// 1.指令处理
		TrdTradeVo trdTradeVo = new TrdTradeVo();
		ParentChildUtil.fatherToChildWithoutException(trade, trdTradeVo);
//		settleInstService.withdrawInstruction(trdTradeVo);
		// 2.删除trade + AdvanceMaturity / MultiResale
		int a = tradeManageService.delTrade(map);
		JY.require(a == 1, "清除放款核实单有误！");
		if (DictConstants.TrdType.CustomAdvanceMaturity.equals(bustype)) { // 自定义提前终止
			int b = durationService.delAdvanceMaturity(trade_id);
			JY.require(b == 1, "处理提前到期业务预审单状态有误！");
		} else if (DictConstants.TrdType.CustomMultiResale.equals(bustype)) { // 自定义资产卖断
			int c = durationService.delMultiResale(trade_id);
			JY.require(c == 1, "处理资产卖断业务预审单状态有误！");
		}

		// 3.修改审批单状态新建
		TtTrdOrder order = new TtTrdOrder();
		order.setOrder_id(trade.getOrder_id());
		order = approveManageDao.selectByPrimaryKey(order);
		order.setOrder_status(DictConstants.ApproveStatus.New);
		order.setUsed_amount(0.0);
		order.setRemain_amount(order.getTotalamount());
		approveManageDao.updateApprove(order);

	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", serial_no);
		// mapRow.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(mapRow);
		return ttTrdOrder;
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		// TODO Auto-generated method stub

	}

}
