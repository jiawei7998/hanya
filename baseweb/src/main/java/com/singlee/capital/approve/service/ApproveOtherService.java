package com.singlee.capital.approve.service;

import com.singlee.capital.common.pojo.InstrumentId;

import java.util.Map;



/**
 * 现券买卖业务层
 * @author kevin_gm
 *
 */
public interface ApproveOtherService {
//	/**
//	 * 存放同业定期自动转存。根据到期指令的交易单转换出一张新的审批单数据
//	 * @param request
//	 * @return
//	 * @throws IOException
//	 */
//	public HashMap<String,Object> loadApproveAutomaticRedeposit (String trade_id,String version_number);
//	
	/**
	 * 查询债券余额、估值信息
	 * @param inSecuAccId
	 * @param hostMarket
	 * @param instrumentId
	 * @return
	 */
	public Map<String,Object> getBondInfo(String inSecuAccId,String hostMarket,InstrumentId instrumentId);
	
}
