package com.singlee.capital.approve.mapper;

import com.singlee.capital.approve.pojo.OrderEditVo;

import java.io.IOException;
import java.util.HashMap;

/**
 * 审批单非标准方法Dao
 * @author kevin_gm
 *
 */
public interface ApproveOtherMapper {
	/**
	 * 存放同业定期自动转存。根据到期指令的交易单转换出一张新的审批单数据
	 * @param request
	 * @return
	 * @throws IOException
	 */
	OrderEditVo selectTradeTransOrderEditVo (HashMap<String, Object> map);
}
