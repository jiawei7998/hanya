package com.singlee.capital.approve.service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.BookQueryVo;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.model.TtTrdTrust;

import java.util.HashMap;
import java.util.Map;

/**
 * 审批单管理业务层
 * @author thinkpad
 *
 */
public interface ApproveManageService {
	
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	Object searchApproveList(HashMap<String,Object> map);
	/**
	 * 查询审批单明细信息
	 * @param map
	 * @return
	 */
	HashMap<String,Object> selectApproveInfo(HashMap<String,Object> map);
	/**
	 * 保存审批单信息
	 * @param map
	 */
	TtTrdOrder saveApproveInfo(HashMap<String,Object> map);
	
	/**
	 * 审批单生成交易单
	 * @param map
	 */
	TtTrdTrade createTrade(HashMap<String,Object> map);

	/**
	 * 审批单生成委托单
	 * @param map
	 */
	TtTrdTrust createTrust(HashMap<String,Object> map);
	
	/**
	 * 注销审批单信息
	 * @param order_id
	 */
	void cancelApprove(String order_id,String folder,String opUserId);
	
	/**
	 * 审批单审批拒绝
	 * @param order_id
	 */
	void noPassApprove(TtTrdOrder ttTrdOrder,String folder);
	/**
	 * 更新审批单余额信息
	 * @param order_id
	 * @param addAmount
	 * @param subAmount
	 * @param isUsed 
	 * 		true表示,增加Used_amount减少Remain_amount,表示生成委托单、交易单
	 * 		false表示,减少Used_amount增加Remain_amount,表示注销委托单、交易单
	 */
	void updateApproveRemainAmount(TtTrdOrder ttTrdOrder,double oldAmount,double newAmount,boolean isUsed,String opUserId);
	
	/**
	 * 根据条件查询台账
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * 
	 * map内参数：【都可为空】
	 * 
	 * @return
	 */
	Page<BookQueryVo> getBookList(Map<String, Object> map, int pageNum, int pageSize);
	/**
	 * 更新审批单信贷额度
	 * @param orderId
	 * @param amount
	 */
	void updateApproveCreditAmount(String orderId, double amount);
	
	/**
	 * 删除order数据
	 * @param order
	 */
	void delTrdOrder(TtTrdOrder order);
	
	/**
	 * 获取order详细对象
	 * @param order
	 * @return
	 */
	TtTrdOrder getTrdorder(TtTrdOrder order);
	
	/**
	 * 取当日总额度
	 * @return
	 */
	public double getTotamAmount();
	
	/**
	 * 修改审批单
	 * @param order
	 */
	public void updateOrder(TtTrdOrder order);
	
	/**
	 * 存续期退回前台
	 * @param trade_id
	 */
	public void withdrawOrder(String trade_id,String bus_type);
}
