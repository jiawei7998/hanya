package com.singlee.capital.approve.service.impl;

import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.OrderEditVo;
import com.singlee.capital.approve.service.ApproveInterfaceService;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 审批单内部使用接口
 * 
 * @author kevin_gm
 *
 */
@Service("approveInterfaceService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveInterfaceServiceImpl implements ApproveInterfaceService {

	@Autowired
	private TrdOrderMapper approveManageDao;

	/**
	 * 查询交易单对象
	 * 
	 * @param order_id
	 * @param isLocked
	 * @return
	 */
	@Override
	public TtTrdOrder selectTtTrdOrder(String order_id) {
		return selectTtTrdOrder(order_id, false);
	}

	/**
	 * 查询交易单对象
	 * 
	 * @param order_id
	 * @param isLocked
	 * @return
	 */
	@Override
	public TtTrdOrder selectTtTrdOrder(String order_id, boolean isLocked) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("order_id", order_id);
		if (isLocked == true) {
			map.put("is_locked", true);
		}
		return approveManageDao.selectOrderForOrderId(map);
	}

	/**
	 * 查询审批单对象
	 * 
	 * @param 到期指令
	 * @param isLocked
	 * @return
	 */
	@Override
	public List<TtTrdOrder> selectTtTrdOrder4fixedToBankTransfer(String endInstrId) {
		if (StringUtil.checkEmptyNull(endInstrId)) {
			return null;
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("in_orderid", endInstrId);
		map.put("is_active", "1");
		List<TtTrdOrder> orderList = approveManageDao.selectTtTrdOrder(map);
		List<TtTrdOrder> retList = new ArrayList<TtTrdOrder>();
		if (orderList != null) {
			for (TtTrdOrder item : orderList) {
				if (!(item.getOrder_status().equals(DictConstants.ApproveStatus.Cancle)
						|| item.getOrder_status().equals(DictConstants.ApproveStatus.ApprovedNoPass))) {
					retList.add(item);
				}
			}
		}
		return retList;
	}

	/**
	 * 查询审批单明细信息
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public OrderEditVo selectOrderEditVo(String order_id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("order_id", order_id);
		OrderEditVo orderEditVo = approveManageDao.selectOrderEditForOrderId(map);

		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("i_code", orderEditVo.getI_code());
		paramMap.put("a_type", orderEditVo.getA_type());
		paramMap.put("m_type", orderEditVo.getM_type());
		paramMap.put("version_number", 0);
//		if(  DictConstants.TrdType.PGNFinancial.equals(orderEditVo.getTrdtype())   
//			|| DictConstants.TrdType.NOPGNFinancial.equals(orderEditVo.getTrdtype())
//			|| DictConstants.TrdType.Trust.equals(orderEditVo.getTrdtype())
//			|| DictConstants.TrdType.DirectionalAsset.equals(orderEditVo.getTrdtype())
//			|| DictConstants.TrdType.Other.equals(orderEditVo.getTrdtype())
//				){
//			TtTrdUnassetInstrument ttTrdUnassetInstrument = approveUnAssetDao.select(paramMap);
//			orderEditVo.setParty_Shortname(ttTrdUnassetInstrument.getPartyname());
//			orderEditVo.setFst_settle_date(ttTrdUnassetInstrument.getStart_date());
//			orderEditVo.setEnd_settle_date(ttTrdUnassetInstrument.getMater_date());
//		}else if(DictConstants.TrdType.BuyingAndReturnSale.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.SaleAndRepo.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.BuyRediscounts.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.SaleRediscounts.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.Rediscount.equals(orderEditVo.getTrdtype())){
//
//			TtTrdBillInstrument ttTrdBillInstrument = billInstrumentDao.select(paramMap);
//			orderEditVo.setParty_Shortname(ttTrdBillInstrument.getPartyname());
//			orderEditVo.setFst_settle_date(ttTrdBillInstrument.getStart_date());
//			orderEditVo.setEnd_settle_date(ttTrdBillInstrument.getMater_date());
//		}

		return orderEditVo;
	}

	/**
	 * 查询审批单明细信息
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public HashMap<String, OrderEditVo> selectOrderEditVoHashMap(List<String> order_id_list) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("order_id_list", order_id_list);
		List<OrderEditVo> list = approveManageDao.selectOrderEditListForOrderId(map);
		HashMap<String, OrderEditVo> hashmap = new HashMap<String, OrderEditVo>();
		for (OrderEditVo orderEditVo : list) {
			// 非标资产查询时，由于在审批单中不存在party_code 则party_name从金融工具中查询
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("i_code", orderEditVo.getI_code());
			paramMap.put("a_type", orderEditVo.getA_type());
			paramMap.put("m_type", orderEditVo.getM_type());
			paramMap.put("version_number", 0);
//			if(  DictConstants.TrdType.PGNFinancial.equals(orderEditVo.getTrdtype())   
//				|| DictConstants.TrdType.NOPGNFinancial.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.Trust.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.DirectionalAsset.equals(orderEditVo.getTrdtype())
//				|| DictConstants.TrdType.Other.equals(orderEditVo.getTrdtype())
//					){
//				TtTrdUnassetInstrument ttTrdUnassetInstrument = approveUnAssetDao.select(paramMap);
//				orderEditVo.setParty_Shortname(ttTrdUnassetInstrument.getPartyname());
//				orderEditVo.setFst_settle_date(ttTrdUnassetInstrument.getStart_date());
//				orderEditVo.setEnd_settle_date(ttTrdUnassetInstrument.getMater_date());
//			}else if(DictConstants.TrdType.BuyingAndReturnSale.equals(orderEditVo.getTrdtype())
//					|| DictConstants.TrdType.SaleAndRepo.equals(orderEditVo.getTrdtype())
//					|| DictConstants.TrdType.BuyRediscounts.equals(orderEditVo.getTrdtype())
//					|| DictConstants.TrdType.SaleRediscounts.equals(orderEditVo.getTrdtype())
//					|| DictConstants.TrdType.Rediscount.equals(orderEditVo.getTrdtype())){
//
//				TtTrdBillInstrument ttTrdBillInstrument = billInstrumentDao.select(paramMap);
//				orderEditVo.setParty_Shortname(ttTrdBillInstrument.getPartyname());
//				orderEditVo.setFst_settle_date(ttTrdBillInstrument.getStart_date());
//				orderEditVo.setEnd_settle_date(ttTrdBillInstrument.getMater_date());
//			}else if (DictConstants.TrdType.SellRepoOutright.equals(orderEditVo.getTrdtype())
//					|| DictConstants.TrdType.ReverseRepoOutright.equals(orderEditVo.getTrdtype())){
//				TrdOutRightInstrumentVo trdOutRightInstrumentVo = tradeOutRightInstrumentDao.selectInstrumentVo(paramMap).get(0);
//				orderEditVo.setFst_settle_date(trdOutRightInstrumentVo.getFstsetdate());
//				orderEditVo.setEnd_settle_date(trdOutRightInstrumentVo.getEndsetdate());
////				orderEditVo.setTotalamount(trdOutRightInstrumentVo.getFstamount());
//			}

			// edit by gm 查询结果不包含shortname的显示
			hashmap.put(orderEditVo.getOrder_id(), orderEditVo);
		}
		return hashmap;
	}

	@Override
	public List<TtTrdOrder> selectTtTrdOrder4Limit(HashMap<String, Object> map) {
		return approveManageDao.selectTtTrdOrder4Limit(map);
	}

	/**
	 * 现券买卖打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expBondBuySellReport(String order_id) {
		return approveManageDao.expBondBuySellPrint(order_id);
	}

	/**
	 * 买断式回购打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expOutrightRepo(String order_id) {
		return approveManageDao.expOutrightRepoPrint(order_id);
	}

	/**
	 * 分销买入打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expDistribution(String order_id) {
		return approveManageDao.expDistributionPrint(order_id);
	}

	/**
	 * 质押式回购打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expPledgedReport(String order_id) {
		Map<String, Object> ret = approveManageDao.expPledgedReport(order_id);
		List<Map<String, Object>> list = approveManageDao.expPledgedBondReport(order_id);
		ret.put("list", list);
		return ret;
	}

	/**
	 * 同业存放导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expBankReport(String order_id) {
		return approveManageDao.expBankReport(order_id);
	}

	/**
	 * 票据导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expBillReport(String order_id) {
		return approveManageDao.expBillReport(order_id);
	}

	/**
	 * 非标导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expUnAssetReport(String order_id) {
		return approveManageDao.expUnAssetReport(order_id);
	}

	/**
	 * 同业存放导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expFromBankReport(String order_id) {
		return approveManageDao.expFromBankReport(order_id);
	}

	/**
	 * 理财投资导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expMoneyInvestReport(String order_id) {
		return approveManageDao.expMoneyInvestReport(order_id);
	}

	@Override
	public Map<String, Object> expAssetPlanInvestReport(String order_id) {
		return approveManageDao.expAssetPlanInvestReport(order_id);
	}

	/**
	 * 导出理财投资赎回交易的交易列表 add by kairui @ 20150530
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public List<Map<String, Object>> expMoneyInvestRedemptionReport(String order_id) {
		return approveManageDao.expMoneyInvestRedemptionReport(order_id);
	}

	@Override
	public List<Map<String, Object>> expAssetPlanInvestSellTargetReport(String order_id) {
		return approveManageDao.expAssetPlanSellTargetReport(order_id);
	}

	/**
	 * 信托投资导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expTrustInvestReport(String order_id) {
		return approveManageDao.expTrustInvestReport(order_id);
	}

	/**
	 * 定向资产管理计划导出打印模板
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public Map<String, Object> expFixedAssetplanReport(String order_id) {
		return approveManageDao.expFixedAssetplanReport(order_id);
	}

	/**
	 * 同业存单发行打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expIBCDIssueReport(String order_id) {
		return approveManageDao.expIBCDIssuePrint(order_id);
	}

	/**
	 * 债券发行打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expBondIssueReport(String order_id) {
		return approveManageDao.expBondIssuePrint(order_id);
	}

	/**
	 * 同业拆借打印导出
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<String, Object> expBankLoanReport(String order_id) {
		return approveManageDao.expBankLoanReport(order_id);
	}

}
