package com.singlee.capital.approve.service;

import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.model.TtTrdTrust;

import java.util.HashMap;

/**
 * 审批单业务层接口，所有业务必须实现该接口
 * @author kevin_gm
 *
 */
public interface ApproveServiceListener {

	/**
	 * 审批单查询
	 * @param map
	 * @return
	 */
	public Object searchApproveList(HashMap<String,Object> map,int pageNum,int pageSize);
	
	/**
	 * 查询审批单明细信息
	 * @param map
	 * @return
	 */
	public void selectApproveInfo(HashMap<String,Object> map);
	
	/**
	 * 审批单保存方法
	 * @param map：前台传入参数
	 * @param ttTrdOrder：默认赋值后的TtTrdOrder对象
	 * @param isAdd：true 表示当前执行新增操作
	 */
	public void saveApprove(HashMap<String, Object> map,TtTrdOrder ttTrdOrder, boolean isAdd);
	
	/**
	 * 审批单生成交易单
	 * @param map：前台返回的hashmap对象
	 * @param ttTrdTrade：原审批单生成的交易单对象，可根据实际需求修改该对象
	 * @return
	 */
	public void createTrade(HashMap<String,Object> map,TtTrdTrade ttTrdTrade);

	/**
	 * 审批单生成交易单 后续处理 
	 */
	public void createdTradeHandle(TtTrdTrade trd, String folder);
	
	/**
	 * 审批单生成委托单
	 * @param map：前台返回的hashmap对象
	 * @param ttTrdTrust：原审批单生成的交易单对象，可根据实际需求修改该对象
	 * @return
	 */
	public void createTrust(HashMap<String,Object> map,TtTrdTrust ttTrdTrust);
	
	/**
	 * 注销审批单
	 * @param ttTrdOrder
	 */
	public void cancelApprove(TtTrdOrder ttTrdOrder);
	
	/**
	 * 审批单审批拒绝
	 * @param ttTrdOrder
	 */
	public void noPassApprove(TtTrdOrder ttTrdOrder);
	
	
	void beforeSave(HashMap<String,Object> map);
	

	public boolean need2Trade(TtTrdOrder ttTrdOrder);
	/**
	 * 不生成核实单的处理逻辑
	 * @param ttTrdOrder
	 */
	public void notNeedTradeProcess(TtTrdOrder ttTrdOrder);
}
