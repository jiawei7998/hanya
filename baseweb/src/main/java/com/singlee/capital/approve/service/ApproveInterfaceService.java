package com.singlee.capital.approve.service;

import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.OrderEditVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 审批单内部使用接口
 * @author kevin_gm
 *
 */
public interface ApproveInterfaceService {
	/**
	 * 查询交易单对象
	 * @param order_id
	 * @param isLocked
	 * @return
	 */
	public TtTrdOrder selectTtTrdOrder(String order_id);
	/**
	 * 查询交易单对象
	 * @param order_id
	 * @param isLocked
	 * @return
	 */
	public TtTrdOrder selectTtTrdOrder(String order_id,boolean isLocked);
	/**
	 * 查询审批单明细信息
	 * @param order_id
	 * @return
	 */
	public OrderEditVo selectOrderEditVo(String order_id);


	/**
	 * 查询审批单明细信息
	 * @param order_id
	 * @return
	 */
	public HashMap<String, OrderEditVo> selectOrderEditVoHashMap(List<String> order_id_list);
	
	/**
	 * 限额查询审批单
	 * @return
	 */
	public List<TtTrdOrder> selectTtTrdOrder4Limit(HashMap<String,Object> map);

	/**
	 * 查询审批单对象
	 * @param 到期指令
	 * @param isLocked
	 * @return
	 */
	public List<TtTrdOrder> selectTtTrdOrder4fixedToBankTransfer(String endInstrId);
	/**
	 * 现券买卖打印导出
	 * @param id
	 * @return
	 */
	public Map<String,Object> expBondBuySellReport(String order_id);
	/**
	 * 质押式回购打印导出
	 * @param id
	 * @return
	 */
	public Map<String,Object> expPledgedReport(String order_id);
	/**
	 * 买断式回购打印导出
	 * @param id
	 * @return
	 */
	public Map<String,Object> expOutrightRepo(String order_id);
	/**
	 *分销买入打印导出
	 * @param id
	 * @return
	 */
	public Map<String,Object> expDistribution(String order_id);
	/**
	 *同业存单发行打印导出
	 * @param id
	 * @return
	 */
	public Map<String,Object> expIBCDIssueReport(String order_id);
	/**
	 * 同业存放导出打印模板 
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expBankReport(String order_id);

	/**
	 * 票据导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expBillReport(String order_id);
	/**
	 * 非标导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expUnAssetReport(String order_id);
	
	/**
	 * 同业存放导出打印模板 
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expFromBankReport(String order_id);
	
	/**
	 * 理财投资导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expMoneyInvestReport(String order_id);

	/**
	 *  资管计划打出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expAssetPlanInvestReport(String order_id);

	/**
	 * 导出理财投资赎回交易的交易列表
	 * add by kairui @ 20150530
	 * @param order_id
	 * @return
	 */
	public List<Map<String,Object>> expMoneyInvestRedemptionReport(String order_id);

	/**
	 * 导出理财投资赎回交易的交易列表
	 * add by kairui @ 20150530
	 * @param order_id
	 * @return
	 */
	public List<Map<String,Object>> expAssetPlanInvestSellTargetReport(String order_id);
	/**
	 * 信托导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expTrustInvestReport(String order_id);
	
	/**
	 * 定向资产管理计划导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expFixedAssetplanReport(String order_id);
	
	/**
	 * 同业拆借导出打印模板
	 * @param order_id
	 * @return
	 */
	public Map<String,Object> expBankLoanReport(String order_id);
	
	/**
	 *债券发行打印导出
	 * @param id
	 * @return
	 */
	public Map<String, Object> expBondIssueReport(String orderId);
}

