package com.singlee.capital.approve.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.pojo.BookQueryVo;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.model.TtTrdTrust;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.*;
/**
 * 审批单管理控制层
 * @author kevin_gm
 *
 */
@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/ApproveManageController")
public class ApproveManageController extends CommonController {
	
	/** 审批单管理 **/
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private UserParamService userParamService;
	/**
	 * 审批单列表查询
	 * @param request
	 * 参数 ：
	 * 		pageNumber 页码
	 * 		pageSize 记录
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchApproveList")
	public Object searchApproveList(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("opUserId", SlSessionHelper.getUserId());
		return approveManageService.searchApproveList(map);
	}

	/**
	 * 审批单明细查询
	 * @param request
	 * 参数 ：
	 * 		pageNumber 页码
	 * 		pageSize 记录
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/selectApprove")
	public HashMap<String,Object> selectApprove(@RequestBody HashMap<String, Object> map) throws IOException{
		return approveManageService.selectApproveInfo(map);
	}

	/**
	 * 审批单保存
	 * @param request
	 * 参数 ：
	 * 		pageNumber 页码
	 * 		pageSize 记录
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/saveApprove")
	public TtTrdOrder saveApprove(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("opUserId", SlSessionHelper.getUserId());
		map.put("accInCash", SlSessionHelperExt.getInCashAccId());
		return approveManageService.saveApproveInfo(map);
	}

	/**
	 * 根据传入的order_id查询审批单公共部分信息
	 * @param order_id  审批单编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/cancelApprove")
	public List<HashMap<String,Object>> cancelApprove(@RequestBody HashMap<String, Object> map) throws IOException{
		List<String> order_ids = (List<String>)map.get("order_ids");
		String folder = ParameterUtil.getString(map,"folder","");
		
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		List<HashMap<String,Object>> failOrderList = new ArrayList<HashMap<String,Object>>();
		for(String orderId : order_ids){
			try{
				//注销审批单
				approveManageService.cancelApprove(orderId,folder,SlSessionHelper.getUserId());
			}catch(Exception e){
				//记录异常审批单号及异常信息
				HashMap<String,Object> err = new HashMap<String,Object>();
				err.put("order_id", orderId);
				err.put("err_msg", e.getMessage());
				failOrderList.add(err);
			}
		}
		//返回异常审批单号及异常信息
		return failOrderList;
	}
	
	/**
	 * 审批单批量生成交易单
	 * @param request
	 * 参数 ：
	 * 		order_ids 审批单[]
	 *      folder 业务参数
	 * @return
	 * @throws IOException 
	 */
	
	@ResponseBody
	@RequestMapping(value = "/createTradeBatch")
	public List<HashMap<String,Object>> createTradeBatch(@RequestBody HashMap<String, Object> map) throws IOException{
		List<String> orderIdList = (List<String>)map.get("order_ids");
		String folder = ParameterUtil.getString(map, "folder", null);
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		List<HashMap<String,Object>> failOrderList = new ArrayList<HashMap<String,Object>>();
		for(String orderId : orderIdList){
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("order_id", orderId);
			param.put("folder", folder);
			param = approveManageService.selectApproveInfo(param);
			try{
				//校验执行市场
				JY.require(DictConstants.BankExeMarket.BankOut.equals(ParameterUtil.getString(param, "exe_market", null)),"执行市场有误,无法生成交易单.");
				param.put("operator_id", SlSessionHelper.getUserId());
				param.put("opInstId", SlSessionHelper.getInstitutionId());
				//生成交易单
				approveManageService.createTrade(param);
			}catch(Exception e){
				JY.error(e.getMessage(),e);
				//记录异常审批单号及异常信息
				HashMap<String,Object> err = new HashMap<String,Object>();
				err.put("order_id", orderId);
				err.put("err_msg", e.getMessage());
				failOrderList.add(err);
			}
		}
		//返回异常审批单号及异常信息
		return failOrderList;
	}
	
	/**
	 * 审批单生成交易单
	 * @param request
	 * 参数 ：
	 * 		pageNumber 页码
	 * 		pageSize 记录
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/createTrade")
	public TtTrdTrade createTrade(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("operator_id", SlSessionHelper.getUserId());
		map.put("opInstId", SlSessionHelper.getInstitutionId());
		return approveManageService.createTrade(map);
	}
	
	/**
	 * 审批单批量生成委托单
	 * @param request
	 * 参数 ：
	 * 		order_ids 审批单[]
	 *      folder 业务参数
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/createTrustBatch")
	public List<HashMap<String,Object>> createTrustBatch(@RequestBody HashMap<String, Object> map) throws IOException{
		List<String> orderIdList = (List<String>)map.get("order_ids");
		String folder = ParameterUtil.getString(map, "folder", null);
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		List<HashMap<String,Object>> failOrderList = new ArrayList<HashMap<String,Object>>();
		for(String orderId : orderIdList){
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("order_id", orderId);
			param.put("folder", folder);
			param = approveManageService.selectApproveInfo(param);
			try{
				//校验执行市场
				JY.require(DictConstants.BankExeMarket.BankIn.equals(ParameterUtil.getString(param, "exe_market", null)),"执行市场有误,无法生成委托单.");
				param.put("operator_id", SlSessionHelper.getUserId());
				param.put("opInstId", SlSessionHelper.getInstitutionId());
				//生成委托单
				approveManageService.createTrust(param);
			}catch(Exception e){
				//记录异常审批单号及异常信息
				HashMap<String,Object> err = new HashMap<String,Object>();
				err.put("order_id", orderId);
				err.put("err_msg", e.getMessage());
				failOrderList.add(err);
			}
		}
		//返回异常审批单号及异常信息
		return failOrderList;
	}

	/**
	 * 审批单生成委托单
	 * @param request
	 * 参数 ：
	 * 		pageNumber 页码
	 * 		pageSize 记录
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/createTrust")
	public TtTrdTrust createTrust(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("operator_id", SlSessionHelper.getUserId());
		map.put("opInstId", SlSessionHelper.getInstitutionId());
		return approveManageService.createTrust(map);
	}
	/**
	 * 查询台账信息
	 * 
	 * map内参数：
	 * 		  pageNumber	 当前页码【不为空】
	 *		  pageSize		 每页条数【不为空】
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchBookList")
	@ResponseBody
	public Page<BookQueryVo> searchBookList(@RequestBody HashMap<String, Object> paramMap) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String partyShortName = ParameterUtil.getString(paramMap, "part_name_search", "");
		String inputPartyName = ParameterUtil.getString(paramMap, "input_party_name_search", "");
		String settleStatus = ParameterUtil.getString(paramMap, "settle_status_search", "");
		String approveStatus = ParameterUtil.getString(paramMap, "order_status_search", "");
		String beginDate = ParameterUtil.getString(paramMap, "begin_search", "");
		String endDate = ParameterUtil.getString(paramMap, "end_search", "");
		String traderName = ParameterUtil.getString(paramMap, "trader_name_search", "");
		String manageUserName = ParameterUtil.getString(paramMap, "manage_name_search", "");
		String verfyUserName = ParameterUtil.getString(paramMap, "verfy_name_search", "");
		String trade_id = ParameterUtil.getString(paramMap, "trade_id_search", "");
		String order_id = ParameterUtil.getString(paramMap, "order_id_search", "");
		String folder = ParameterUtil.getString(paramMap, "folder", "");
		String trdTypeStr = ParameterUtil.getString(paramMap, "trdtype_search", "");
		String instId = ParameterUtil.getString(paramMap, "inst_id", "");
		String i_code = ParameterUtil.getString(paramMap, "i_code_search", "");
		String i_name = ParameterUtil.getString(paramMap, "i_name_search", "");
		List<String> trdTypeList = new ArrayList<String>();
		List<String> settleStatusList = new ArrayList<String>();
		List<String> approveStatusList = new ArrayList<String>();
		List<String> instList = new ArrayList<String>();
		if(!StringUtil.checkEmptyNull(approveStatus)){
			approveStatusList.addAll(Arrays.asList(approveStatus.split(",")));
		}
		if(!StringUtil.checkEmptyNull(settleStatus)){
			settleStatusList.addAll(Arrays.asList(settleStatus.split(",")));
		}
		if(!StringUtil.checkEmptyNull(instId)){
			instList.addAll(Arrays.asList(instId.split(",")));
		}
		if(!StringUtil.checkEmptyNull(trdTypeStr)){
			trdTypeList.addAll(Arrays.asList(trdTypeStr.split(",")));
		}
		else{
			if("1".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.BondBuy);
				trdTypeList.add(DictConstants.TrdType.BondSell);
			}
			else if("2".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.SellRepoPledged);
				trdTypeList.add(DictConstants.TrdType.ReverseRepoPledged);
			}
			else if("6".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.CurrentToBankTransfer);
				trdTypeList.add(DictConstants.TrdType.CurrentToBankBack);
				trdTypeList.add(DictConstants.TrdType.CurrentToBankAccOff);
			}
			else if("4".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.DepositToBankFixed);
				trdTypeList.add(DictConstants.TrdType.DepositToBankFixedTransfer);
			}
			else if("7".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.CurrentFromBankIn);
			}
			else if("5".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.DepositFromBankFixed);
			}
			else if("9".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.BuyingAndReturnSale);
				trdTypeList.add(DictConstants.TrdType.BuyRediscounts);
				trdTypeList.add(DictConstants.TrdType.SaleAndRepo);
				trdTypeList.add(DictConstants.TrdType.SaleRediscounts);
				trdTypeList.add(DictConstants.TrdType.Rediscount);
			}
			else if("11".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.MoneyBuy);
				trdTypeList.add(DictConstants.TrdType.MoneySell);
				map.put("folder", folder);
			}
			else if("12".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.AssetPlanBuy);
				trdTypeList.add(DictConstants.TrdType.AssetPlanSell);
				map.put("folder", folder);
			}
			else if("13".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.AssetPlanBuy);
			}
			else if("14".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.SellRepoOutright);
				trdTypeList.add(DictConstants.TrdType.ReverseRepoOutright);
			}
			else if("16".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.DistributeBuy);
			}
			else if("17".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.IbBorrowing);
				trdTypeList.add(DictConstants.TrdType.IbLending);
				map.put("folder", folder);
			}
			else if("18".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.IBCDIssue);
				map.put("folder", folder);
			}
			else if("99".equals(folder)){
				trdTypeList.add(DictConstants.TrdType.DirectionalAsset);
				trdTypeList.add(DictConstants.TrdType.PGNFinancial);
				trdTypeList.add(DictConstants.TrdType.Other);
				trdTypeList.add(DictConstants.TrdType.NOPGNFinancial);
				trdTypeList.add(DictConstants.TrdType.Trust);
			}
		}
		int page_num = ParameterUtil.getInt(paramMap, "pageNumber", 1);
		int page_size = ParameterUtil.getInt(paramMap, "pageSize", 12);
		//map.put("status", settleStatus);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("i_code", i_code);
		map.put("i_name", i_name);
		if(!StringUtil.checkEmptyNull(partyShortName)) {
			map.put("party_shortname", partyShortName);
		}
		else if(!StringUtil.checkEmptyNull(inputPartyName)){
			if("9".equals(folder)){
				map.put("bill_party_shortname", inputPartyName);
			}
			else if("99".equals(folder)){
				map.put("unasset_party_shortname", inputPartyName);
			}
		}
		map.put("manage_user_name", manageUserName);
		map.put("verfy_user_name", verfyUserName);
		map.put("self_tradername", traderName);
		map.put("trade_id", trade_id);
		map.put("order_id", order_id);
		map.put("folder", folder);
		
		if(trdTypeList != null && trdTypeList.size() > 0){
			map.put("trdTypeList", trdTypeList);
		}
		if(settleStatusList != null && settleStatusList.size() > 0){
			map.put("settleStatusList", settleStatusList);
		}
		if(approveStatusList != null && approveStatusList.size() > 0){
			map.put("approveStatusList", approveStatusList);
		}
		if(instList != null && instList.size() > 0){
			map.put("instList", instList);
		}else{
			//机构特殊处理，总行能看到分行数据2015/09/17
			instId = SlSessionHelper.getInstitutionId();
			if(!userParamService.getSysParamByName("system.headBankInstId","000000").equals(instId)){
				instList.add(instId);
				map.put("instList", instList);
			}
		}
		//机构特殊处理，总行能看到分行数据
		//if(StringUtil.checkEmptyNull(instId)){
		//	instId = SessionManager.getCurrentInstId();
		//	if(!SystemProperties.headBankInstId.equals(instId))
		//		map.put("inst_id", instId);
		//}
		//else{
		//	map.put("inst_id", instId);
		//}
		return (Page<BookQueryVo>) approveManageService.getBookList(map, page_num, page_size);
	}
	
}
