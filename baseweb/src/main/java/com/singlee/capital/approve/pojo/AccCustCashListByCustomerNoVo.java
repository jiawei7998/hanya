package com.singlee.capital.approve.pojo;
/**
 * 根据交易对手号，查询该交易对手关联的外部账户信息
 * @author thinkpad
 *
 */
public class AccCustCashListByCustomerNoVo {
	private String accid;
	private String accname;
	private String bankacccode;
	private String coupon_type;
	private String open_date;
	private String rate;
	private String cash_serial_no;
	
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getBankacccode() {
		return bankacccode;
	}
	public void setBankacccode(String bankacccode) {
		this.bankacccode = bankacccode;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public String getOpen_date() {
		return open_date;
	}
	public void setOpen_date(String open_date) {
		this.open_date = open_date;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getCash_serial_no() {
		return cash_serial_no;
	}
	public void setCash_serial_no(String cash_serial_no) {
		this.cash_serial_no = cash_serial_no;
	}
	
}
