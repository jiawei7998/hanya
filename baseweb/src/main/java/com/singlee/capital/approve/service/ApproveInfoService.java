package com.singlee.capital.approve.service;

import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.approve.pojo.AccCustCashListByCustomerNoVo;
import com.singlee.capital.approve.pojo.AccOutCashListByCustomerNoVo;
import com.singlee.capital.system.model.TaUser;

import java.util.List;

/**
 * 审批单公共信息调用conroller
 * @author kevin_gm
 * 仅用于审批单公共部分逻辑调用,不包含审批单业务逻辑.
 * 1.根据内部证券账户编号查询关联的外部证券信息
 * 2.选中内部证券账户后显示关联的交易员列表
 * 
 */
public interface ApproveInfoService {
	/**
	 * 选中内部证券账户后显示关联的交易员列表
	 * is_public = 1 查询所有有效的用户信息
	 * is_public = 0 查询owner用户
	 * @param is_public
	 * @param owner
	 * @return
	 */
	public List<TaUser> searchOwnerListByAccId(String is_public,String owner);
	/**
	 * 根据内部证券账户编号查询关联的外部证券信息
	 * @param map
	 * accid：内部证券账户编号
	 * @return
	 */
	public List<TtAccOutCash> searchAccOutCashList(String acc_in_secu_id,List<String> acctype_list);
	/**
	 * 根据交易对手号，查询该交易对手关联的外部自己账户信息
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	public List<AccOutCashListByCustomerNoVo> searchAccOutCashListByPartyId(String party_id,String accid,String inst_id, String settle_date,String status);
	/**
	 * 根据交易对手号，查询该交易对手关联的外部账户信息
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	public List<AccCustCashListByCustomerNoVo> searchAccCustCashListByPartyId(String party_id,String accid,String opInstId);

	/**
	 * 业务流程判断，非交易日不允许进行审批单、委托单、交易单的新增、修改、删除等业务操作
	 */
	public void processToJudgeTradeDate(String m_type,String date);
}
