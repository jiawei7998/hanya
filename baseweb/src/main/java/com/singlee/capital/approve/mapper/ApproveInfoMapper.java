package com.singlee.capital.approve.mapper;

import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.approve.pojo.AccCustCashListByCustomerNoVo;
import com.singlee.capital.approve.pojo.AccOutCashListByCustomerNoVo;

import java.util.HashMap;
import java.util.List;


/**
 * 
 * @author kevin_gm
 *
 */
public interface ApproveInfoMapper {

	/**
	 * 根据内部证券账户id 查询外部资金账户信息
	 * @param map
	 * @return
	 */
	List<TtAccOutCash> searchAccOutCashList(HashMap<String, Object> map);
	
	/**
	 * 根据交易对手号，查询该交易对手关联的外部自己账户信息
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	List<AccOutCashListByCustomerNoVo> searchAccOutCashListByPartyId(HashMap<String, Object> map);
	
	/**
	 * 根据交易对手id，外部资金账户id查询交易对手账户信息。（编辑时使用）
	 * @param map
	 * party_id:交易对手id
	 * accid:外部资金账户id（允许为空）
	 * @return
	 */
	List<AccCustCashListByCustomerNoVo> searchAccCustCashListByPartyId(HashMap<String, Object> map);
}
