package com.singlee.capital.base.pojo;

import com.singlee.capital.acc.model.TtAccInSecu;

/**
 * 用户对应内部证券账户权限列表对象
 * @author gm
 *
 */
public class UserAccInSecuVisitVo extends TtAccInSecu {

	private static final long serialVersionUID = -1992491334101925725L;
	/******************同TtAccInSecuVisit属性***********************/
	private String user_id;
	private String visit_id;
	private String visit_level;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getVisit_id() {
		return visit_id;
	}
	public void setVisit_id(String visit_id) {
		this.visit_id = visit_id;
	}
	public String getVisit_level() {
		return visit_level;
	}
	public void setVisit_level(String visit_level) {
		this.visit_level = visit_level;
	}
	
}
