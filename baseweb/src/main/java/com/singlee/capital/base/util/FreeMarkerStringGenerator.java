package com.singlee.capital.base.util;

import com.singlee.capital.common.util.JY;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;


/**
 * 
 * @author x230i
 *
 */
public class FreeMarkerStringGenerator {
	
	private static class SingletonHolder {
		private static final FreeMarkerStringGenerator INSTANCE = new FreeMarkerStringGenerator();
	}

	public static final FreeMarkerStringGenerator getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	private StringTemplateLoader STL;
	private Configuration CONFIG;
	
	private FreeMarkerStringGenerator(){
		STL = new StringTemplateLoader();
		CONFIG = new Configuration();
		CONFIG.setTemplateLoader(STL);
	}
	
	public void addTemplate(String name, String temp){
		STL.putTemplate(name, temp);
	}
	
	public void checkTemplate(String name, Map<String,Object> map){
		stringGenerate(name,map);
	}
	
	
	public String stringGenerate(String name, String template, Map<String, Object> map){
		addTemplate(name, template);
		return stringGenerate(name, map);
	}
	
	public String stringGenerate(String name, Map<String, Object> map){
		Template tmp = null;
		try {
			tmp = CONFIG.getTemplate(name);
		} catch (IOException e) {
			JY.raise(String.format("获取模板[%s]异常：%s。",name, e.getMessage()));
		}
		JY.require(tmp!=null, "没有找到[%s]模板！",name);
		StringWriter sw = new StringWriter();
		//生成sql
		try {
			tmp.process(map, sw);
		} catch (TemplateException e) {
			JY.raise(String.format("解析失败，原因:%s\r\n%s",e.getMessage(), e.getFTLInstructionStack()));
		} catch (IOException e) {
			JY.raise(String.format("解析失败，原因:%s",e.getMessage()));
		}
		return sw.toString();
	}
}

