package com.singlee.capital.base.controller;

import com.singlee.capital.acc.pojo.GenericTreeModule;
import com.singlee.capital.base.model.TtAccInSecuVisitListVo;
import com.singlee.capital.base.pojo.AccInSecuTreeAttributesVo;
import com.singlee.capital.base.pojo.UserAccInSecuVisitVo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserAccInSecuService;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 用户账户权限树
 * @author gm
 */
@Controller
@RequestMapping(value = "/UserAccInSecuController")
public class UserAccInSecuController extends CommonController {

	/**
	 * 柜员内部证券账户树权限管理 业务层
	 */
	@Autowired
	private UserAccInSecuService userAccInSecuService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserParamService userParamService;
	/**
	 * 查询柜员对应所有账户的权限信息
	 * @param request
	 * 参数 ：
	 * 		user_id 柜员id
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchUserAccInSecuForVo")
	public List<UserAccInSecuVisitVo> searchUserAccInSecuForVo(@RequestBody Map<String,String> map) throws IOException{
		String user_id = ParameterUtil.getString(map,"user_id","");
		String inst_id = ParameterUtil.getString(map,"inst_id","");
		String accName_search = ParameterUtil.getString(map,"accName_search","");
		String visit_level_search = ParameterUtil.getString(map,"visit_level_search","");
		if(userParamService.getSysParamByName("system.headBankInstId", "000000").equals(inst_id)){
			inst_id = "";
		}
		//根据user_id查询机构
		TaUser user = userService.getUserById(user_id);
		inst_id = user.getInstId();
		List<UserAccInSecuVisitVo> retList = userAccInSecuService.searchUserAccInSecuForVo(user_id, inst_id, accName_search, visit_level_search);
		return retList;
	}

	/**
	 * 保存柜员对应账户权限信息
	 * @param request
	 * 参数 ：
	 * 		visit_id 访问序列号
	 * 		user_id 柜员ID
	 * 		visit_level 访问权限级别 0-无权限 1-可浏览 2-可交易
	 * 		accid 内部证券账户
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTtAccInSecuVisit")
	public void saveTtAccInSecuVisit(@RequestBody TtAccInSecuVisitListVo ttAccInSecuVisitListVo) throws IOException{
		userAccInSecuService.saveTtAccInSecuVisit(ttAccInSecuVisitListVo.getList());
	}

	/**
	 * 根据用户id及权限级别读取到有权限的内部证券账户树
	 * @param request
	 * user_id:柜员id
	 * visit_level：访问级别
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAccInSecuTreeModuleBuyUserIdAndLevel")
	public List<GenericTreeModule<AccInSecuTreeAttributesVo>> searchAccInSecuTreeModuleBuyUserIdAndLevel(@RequestBody Map<String,Object> map) throws IOException{
		String user_id = SlSessionHelper.getUserId();//读取当前登录用户的user_id
//		String visit_level = ParameterUtil.getString(map,"visit_level","");
		if(map != null){
			map.put("userId", user_id);
			map.put("instId", SlSessionHelper.getInstitutionId());//去除账户树显示时机构隔离限制
		}
		List<GenericTreeModule<AccInSecuTreeAttributesVo>> retList = userAccInSecuService.searchAccInSecuTreeModuleBuyUserIdAndLevel(map);
		return retList;
	}
	
}
