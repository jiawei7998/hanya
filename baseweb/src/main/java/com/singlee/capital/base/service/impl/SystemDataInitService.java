package com.singlee.capital.base.service.impl;

import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.BaseExcelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 系统数据初始化
 * @author Libo
 *
 */
@Service("SystemDataInitService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SystemDataInitService {
	private static Logger log = LoggerFactory.getLogger(SystemDataInitService.class);
	
	@Autowired
	private UserService userService;
	
	
	@Autowired
	private AccInCashService accInCashService;
	
	@Autowired
	private AccOutCashService accoutcashservice;
	
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	
	@Autowired
	private BaseExcelService baseExcleService;
	
	@Autowired
	private TrdOrderMapper trdOrderMapper;
	
	
	/**
	 * 初始化所有数据
	 */
	public void initAll(Map<String,List<Map<String,Object>>> excelData){
		//initDate = dayendDateService.getSettlementDate();
		log.debug("系统初始化数据开始...");
		preDeal(excelData);
		//initCounterparty(excelData);	//客户信息（因在系统中已录入，所以取消）
		initCounterpartyAcc(excelData);		//对手清算路径
		initProductApprove(excelData);	//main表
		initBaseAsset(excelData);	//基础资产
		initTrade(excelData);	//放款（核实、交易）表
//		initBalance(excelData);   //业务余额
//		initAccountingBalance(excelData);	//核算余额
		initIncreaseCredit(excelData);	//增信措施
		initRiskAsset(excelData);	//风险资产
		initRiskSlowRelease(excelData);	//保证金
		initBaseAssetBill(excelData);	//票据基础资产
		initStockInfo(excelData);	//股票信息
		initCrossMarket(excelData);	//交叉营销
		initOrder(excelData);	//预审（审批）表
		log.debug("系统初始化数据完成...");
	}
	/**
	 * 预处理
	 * @param excelData
	 */
	private void preDeal(Map<String,List<Map<String,Object>>> excelData){
		log.info("系统初始化数据预处理开始...");
	}
	
	/**
	 * 初始化交易信息
	 * @param excelData
	 */
	private void initProductApprove(Map<String,List<Map<String,Object>>> excelData){
		List<Map<String,Object>> tradeList = excelData.get("资产余额及业务数据");
		if(tradeList != null && tradeList.size() > 0){
		}
	}
	/**
	 * 初始化交易信息
	 * @param excelData
	 */
	private void initTrade(Map<String,List<Map<String,Object>>> excelData){
		List<Map<String,Object>> tradeList = excelData.get("资产余额及业务数据");
		if(tradeList != null && tradeList.size() > 0){
			for(int i=0;i<tradeList.size();i++){
				//1 tt_trd_trade table
				Map<String,Object> tradeInfoMap = tradeList.get(i);
				if("1".equals(ParameterUtil.getString(tradeInfoMap, "PRD_PROP", ""))){
					TtTrdTrade ttTrdTrade = new TtTrdTrade();
					ttTrdTrade.setTrade_id(ParameterUtil.getString(tradeInfoMap, "ID", ""));
					ttTrdTrade.setVersion_number(0);
					ttTrdTrade.setTrade_date(ParameterUtil.getString(tradeInfoMap, "TRD_DATE", ""));
					ttTrdTrade.setTrade_time(ParameterUtil.getString(tradeInfoMap, "TRD_DATE"+" 00:00:00", ""));
					ttTrdTrade.setTrade_user(ParameterUtil.getString(tradeInfoMap, "USER", ""));
					ttTrdTrade.setConfirm_date(ParameterUtil.getString(tradeInfoMap, "TRD_DATE", ""));
					ttTrdTrade.setConfirm_user(ParameterUtil.getString(tradeInfoMap, "USER", ""));
					ttTrdTrade.setOrder_id(ParameterUtil.getString(tradeInfoMap, "ORDER_ID", ""));
					ttTrdTrade.setTrust_version(0);
					ttTrdTrade.setTrdtype(DictConstants.TrdType.Custom);
					ttTrdTrade.setParty_id(ParameterUtil.getString(tradeInfoMap, "PARTY_ID", ""));
					ttTrdTrade.setI_code(ParameterUtil.getString(tradeInfoMap, "ID", ""));
					ttTrdTrade.setA_type(ParameterUtil.getString(tradeInfoMap, "A_TYPE", ""));
					ttTrdTrade.setM_type(ParameterUtil.getString(tradeInfoMap, "M_TYPE", ""));
					ttTrdTrade.setI_name(ParameterUtil.getString(tradeInfoMap, "I_NAME", ""));
					ttTrdTrade.setImp_time(DateUtil.getCurrentDateTimeAsString());
					ttTrdTrade.setFst_settle_date(ParameterUtil.getString(tradeInfoMap, "T_DATE", ""));
					ttTrdTrade.setEnd_settle_date(ParameterUtil.getString(tradeInfoMap, "ME_DATE", ""));
					ttTrdTrade.setTerm_days(ParameterUtil.getDouble(tradeInfoMap, "TERM", 0));
					ttTrdTrade.setTotalamount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0));
					ttTrdTrade.setTotalcount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0));
					ttTrdTrade.setYtm(0);
					ttTrdTrade.setNetprice(0);
					ttTrdTrade.setFullprice(0);
					ttTrdTrade.setAiprice(0);
					ttTrdTrade.setNetamount(0);
					ttTrdTrade.setFullamount(0);
					ttTrdTrade.setAiamount(ParameterUtil.getDouble(tradeInfoMap, "INT_AMT", 0));
					ttTrdTrade.setSelf_traderid("admin");
					ttTrdTrade.setSelf_bankacccode(ParameterUtil.getString(tradeInfoMap, "SELF_ACC_CODE", ""));
					ttTrdTrade.setSelf_bankaccname(ParameterUtil.getString(tradeInfoMap, "SELF_ACC_NAME", ""));
					ttTrdTrade.setSelf_bankcode(ParameterUtil.getString(tradeInfoMap, "SELF_BANK_CODE", ""));
					ttTrdTrade.setSelf_bankname(ParameterUtil.getString(tradeInfoMap, "SELF_BANK_NAME", ""));
					ttTrdTrade.setSelf_insecuaccid("401");
					ttTrdTrade.setSelf_outsecuaccid("-1");
					ttTrdTrade.setSelf_incashaccid(ParameterUtil.getString(tradeInfoMap, "SELF_INCASHACCID", ""));
					ttTrdTrade.setSelf_outcashaccid(ParameterUtil.getString(tradeInfoMap, "SELF_OUTCASHACCID", ""));
					ttTrdTrade.setParty_bankacccode(ParameterUtil.getString(tradeInfoMap, "PARTY_ACC_CODE", ""));
					ttTrdTrade.setParty_bankaccname(ParameterUtil.getString(tradeInfoMap, "PARTY_ACC_NAME", ""));
					ttTrdTrade.setParty_bankcode(ParameterUtil.getString(tradeInfoMap, "PARTY_BANK_CODE", ""));
					ttTrdTrade.setParty_bankname(ParameterUtil.getString(tradeInfoMap, "PARTY_BANK_NAME", ""));
					ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Accounted);
					ttTrdTrade.setOperator_id("admin");
					ttTrdTrade.setOperate_time(DateUtil.getCurrentCompactDateTimeAsString());
					ttTrdTrade.setZzd_netprice(0);
					ttTrdTrade.setZzd_ytm(0);
					ttTrdTrade.setTradeamount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0)-ParameterUtil.getDouble(tradeInfoMap, "INT_AMT", 0));
					ttTrdTrade.setIs_active(ParameterUtil.getString(tradeInfoMap, "IS_ACTIVE", ""));
					ttTrdTrade.setTrade_fee(0);
					ttTrdTrade.setSettle_fee(0);
					ttTrdTrade.setFst_settle_amount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0)-ParameterUtil.getDouble(tradeInfoMap, "INT_AMT", 0));
					ttTrdTrade.setEnd_settle_amount(ParameterUtil.getDouble(tradeInfoMap, "M_AMT", 0));
					ttTrdTrade.setStrike_yield(0);
					ttTrdTrade.setCal_days(0);
					ttTrdTrade.setTrade_source(DictConstants.TrustSource.InitImport);
//					ttTrdTrade.setTotalamount(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0) * 10000);
//					double amortIr = ParameterUtil.getDouble(tradeInfoMap, "AMORT_IR", 0);
//					ttTrdTrade.setFullprice(MathUtil.round(ttTrdTrade.getNetprice() + buyAi,4));
//					ttTrdTrade.setNetamount(MathUtil.round(netAmount,2));
					tradeManageMapper.insertTrade(ttTrdTrade);
				}
				
			}
		}
	}
//	/**
//	 * 初始化业务余额
//	 */
//	private void initBalance(Map<String,List<Map<String,Object>>> excelData){
//		log.info("初始化业务余额开始...");
//		List<Map<String,Object>> tradeList = excelData.get("资产余额及业务数据");
//		if(tradeList != null && tradeList.size() > 0){
//			for(Map<String,Object> tradeInfoMap : tradeList){
//				if("1".equals(ParameterUtil.getString(tradeInfoMap, "PRD_PROP", ""))){
//					TtTrdBalanceSecu balanceSecu = new TtTrdBalanceSecu();
//					balanceSecu.setBegin_date(dayendDateService.getSettlementDate());   //当前业务日期
//					balanceSecu.setInsecuaccid(ParameterUtil.getString(tradeInfoMap, "ACC_SECU_IN", "")); //内部证券账户
//					balanceSecu.setOutsecuaccid(ParameterUtil.getString(tradeInfoMap, "ACC_SECU_OUT", ""));		//外部证券账户
//					balanceSecu.setI_code(ParameterUtil.getString(tradeInfoMap, "ID", ""));
//					balanceSecu.setA_type(ParameterUtil.getString(tradeInfoMap, "A_TYPE", ""));
//					balanceSecu.setM_type(ParameterUtil.getString(tradeInfoMap, "M_TYPE", ""));
//					balanceSecu.setB_l_amount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0.0));
//					balanceSecu.setB_l_avaamount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0.0));
//					balanceSecu.setB_l_cost(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0.0));
//					balanceSecu.setB_l_cost_nofee(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0.0));
//					balanceSecu.setB_l_cost_netprice(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0.0));
//					balanceSecu.setB_l_actual_pl(0.0);
//					balanceSecu.setR_l_amount(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0.0));
//					balanceSecu.setR_l_avaamount(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0.0));
//					balanceSecu.setR_l_cost(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0.0));
//					balanceSecu.setR_l_cost_nofee(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0.0));
//					balanceSecu.setR_l_cost_netprice(ParameterUtil.getDouble(tradeInfoMap, "AMOUNT", 0.0));
//					balanceSecu.setR_l_actual_pl(0.0);
//					balanceSecu.setB_updatetime(DateUtil.getCurrentDateTimeAsString());
//					balanceSecu.setR_updatetime(DateUtil.getCurrentDateTimeAsString());
//					
//					trdBalanceSecuService.addSecuBalance(balanceSecu);
//				}
//				
//			}
//		}
//		log.info("初始化业务余额完成...");
//	}
//	/**
//	 * 初始化核算余额
//	 */
//	private void initAccountingBalance(Map<String,List<Map<String,Object>>> excelData){
//		log.info("初始化核算余额开始...");
//		List<Map<String,Object>> balanceList = excelData.get("资产余额及业务数据");
//		if(balanceList != null && balanceList.size() > 0){
//			for(Map<String,Object> balanceMap : balanceList){
//				if("1".equals(ParameterUtil.getString(balanceMap, "PRD_PROP", ""))){
//					//计算当周期的利息和本月的利息
//					String i_code = ParameterUtil.getString(balanceMap, "I_CODE", "");
//					String a_type = ParameterUtil.getString(balanceMap, "A_TYPE", "");
//					String m_type = ParameterUtil.getString(balanceMap, "M_TYPE", "");
//					String acc_id_in = ParameterUtil.getString(balanceMap, "ACC_SECU_IN", "");
//					String acc_id_out = ParameterUtil.getString(balanceMap, "ACC_SECU_OUT", "");
//					double amount = 10000 * ParameterUtil.getDouble(balanceMap, "AMOUNT", 0);
//					
//					//创建余额
//					TAccountingBalance balance = new TAccountingBalance();
//					balance.setBegin_date(initDate);
//					balance.setEnd_date(initDate);
//					balance.setTsk_id(Constants.AccountTask.KJHS);
//					balance.setSecu_or_cash("S");
//					balance.setAcct_id_in(acc_id_in);
//					balance.setAcct_id_out(acc_id_out);
//					balance.setCcy1(DictConstants.Currency.CNY);
//					balance.setI_code(i_code);
//					balance.setA_type(a_type);
//					balance.setM_type(m_type);
//					balance.setTrade_id(i_code);
//					balance.setAmount(amount);
//					balance.setReal_cp(balance.getAmount());
//					balance.setAi(ParameterUtil.getDouble(balanceMap, "AI", 0));
//					balance.setDue_ai(0);
//					balance.setAi_cost(0);
//					balance.setAmort_date(ParameterUtil.getString(balanceMap, "T_DATE", ""));//划款日
//					balance.setAmort_date_end(ParameterUtil.getString(balanceMap, "INT_M_DATE", ""));//首次付息截止日期
//					balance.setAmort_ir_begin(ParameterUtil.getDouble(balanceMap, "INT_AMT", 0));//首次付息金额
//					balance.setAmort_ir(balance.getAmort_ir_begin() - ParameterUtil.getDouble(balanceMap, "AMORT_IR", 0));//首次付息金额 - 已摊销金额
//					balance.setPrft_ir(ParameterUtil.getDouble(balanceMap, "AI", 0) + ParameterUtil.getDouble(balanceMap, "AMORT_IR", 0));
//					balance.setAmort_cost(0);
//					balance.setAmort_ir_hp(balance.getAmort_ir());
//					balance.setAmort_ytm(ParameterUtil.getDouble(balanceMap, "RATE", 0));
//					balance.setChg_fv(0);
//					balance.setPrft_fv(0);
//					balance.setDraw_ai(balance.getAi());
//					balance.setDraw_amort(balance.getAmort_ir_begin() - balance.getAmort_ir());
//					balance.setUpdate_time("2000-01-01 00:00:00");
//					
//					accountingBalanceService.save(AccountingConstants.HistoryOrCurrent.Current, balance);
//				}
//			}
//		}
//		log.info("初始化核算余额完成...");
//	}
	
	/**
	 * 初始化客户信息
	 * @param excelData
	 */
	@SuppressWarnings("unused")
	private void initCounterparty(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化客户信息开始...");
		log.info("初始化客户信息结束...");
	}
	
	/**
	 * 初始化清算信息
	 * @param excelData
	 */
	private void initCounterpartyAcc(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化清算信息开始...");
		log.info("初始化清算信息结束...");
	}
	
	/**
	 *初始化基础资产信息
	 *@param excelData
	 *@author wuyc
	 */
	private void initBaseAsset(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化基础资产信息开始...");
		List<Map<String,Object>> baseAssetList = excelData.get("基础资产");
		if(baseAssetList != null && baseAssetList.size() > 0){
			baseExcleService.initBaseAsset(baseAssetList);
		}
		log.info("初始化基础资产信息结束...");
	}
	
	/**
	 * 增信措施
	 * @param excelData
	 */
	private void initIncreaseCredit(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化增信对手信息开始...");
		List<Map<String,Object>> ICList = excelData.get("增信措施");
		if(ICList != null && ICList.size()>0){
			baseExcleService.initIncreaseCredit(ICList);
		}
		log.info("初始化增信对手信息结束...");
	}
	
	/**
	 * 风险资产
	 * @param excelData
	 */
	private void initRiskAsset(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化风险资产信息开始...");
		List<Map<String,Object>> raList = excelData.get("风险资产");
		if(raList != null && raList.size()>0){
			baseExcleService.initRiskAasset(raList);
		}
		log.info("初始化风险资产信息结束...");
	}
	
	/**
	 * 保证金
	 * @param excelData
	 */
	private void initRiskSlowRelease(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化保证金信息开始...");
		List<Map<String,Object>> rsrList = excelData.get("保证金");
		if(rsrList != null && rsrList.size()>0){
			baseExcleService.initRiskSlowRelease(rsrList);
		}
		log.info("初始化保证金信息结束...");
	}
	
	/**
	 * 票据基础资产
	 * @param excelData
	 */
	private void initBaseAssetBill(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化票据基础资产信息开始...");
		List<Map<String,Object>> babList = excelData.get("票据基础资产");
		if(babList != null && babList.size()>0){
			baseExcleService.initBaseAssetBill(babList);
		}
		log.info("初始化票据基础资产信息结束...");
	}
	
	/**
	 * 股票信息
	 * @param excelData
	 */
	private void initStockInfo(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化股票信息开始...");
		List<Map<String,Object>> siList = excelData.get("股票信息");
		if(siList != null && siList.size()>0){
			baseExcleService.initStockInfo(siList);
		}
		log.info("初始化股票信息结束...");
	}
	
	/**
	 * 交叉营销
	 * @param excelData
	 */
	private void initCrossMarket(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化交叉营销信息开始...");
		List<Map<String,Object>> cmList = excelData.get("交叉营销");
		if(cmList != null && cmList.size()>0){
			baseExcleService.initCrossMarket(cmList);
		}
		log.info("初始化交叉营销信息结束...");
	}
	
	/**
	 * 初始化tt_trd_order
	 * @param excelData
	 */
	private void initOrder(Map<String,List<Map<String,Object>>> excelData){
		log.info("初始化order信息开始...");
		List<Map<String,Object>> tradeList = excelData.get("资产余额及业务数据");
		if(tradeList != null && tradeList.size() > 0){
			for(int i=0;i<tradeList.size();i++){
				//1 tt_trd_order table
				Map<String,Object> tradeInfoMap = tradeList.get(i);
				TtTrdOrder ttTrdOrder = new TtTrdOrder();
				String ORDER_ID = ParameterUtil.getString(tradeInfoMap, "ORDER_ID", "");
				JY.require(StringUtils.isNotEmpty(ORDER_ID), "审批单号不能为空请核实！");
				ttTrdOrder.setOrder_id(ORDER_ID);
				ttTrdOrder.setOrder_time(DateUtil.getCurrentDateAsString()+" 00:00:00");
				ttTrdOrder.setUser_id(ParameterUtil.getString(tradeInfoMap, "USER", ""));
				ttTrdOrder.setParty_id(ParameterUtil.getString(tradeInfoMap, "PARTY_ID", ""));
				ttTrdOrder.setI_code(ParameterUtil.getString(tradeInfoMap, "ORDER_ID", ""));
				ttTrdOrder.setA_type(ParameterUtil.getString(tradeInfoMap, "A_TYPE", ""));
				ttTrdOrder.setM_type(ParameterUtil.getString(tradeInfoMap, "M_TYPE", ""));
				ttTrdOrder.setI_name(ParameterUtil.getString(tradeInfoMap, "I_NAME", ""));
				ttTrdOrder.setTerm_days(ParameterUtil.getDouble(tradeInfoMap, "TERM", 0));
				ttTrdOrder.setParty_type("1");
				ttTrdOrder.setTotalamount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0));
				ttTrdOrder.setTotalcount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0));
				ttTrdOrder.setYtm(0);
				ttTrdOrder.setNetprice(0);
				ttTrdOrder.setFullprice(0);
				ttTrdOrder.setAiprice(0);
				ttTrdOrder.setNetamount(0);
				ttTrdOrder.setFullamount(0);
				ttTrdOrder.setAiamount(0);
				ttTrdOrder.setSelf_traderid(ParameterUtil.getString(tradeInfoMap, "USER", ""));
				ttTrdOrder.setSelf_bankacccode(ParameterUtil.getString(tradeInfoMap, "SELF_ACC_CODE", ""));
				ttTrdOrder.setSelf_bankaccname(ParameterUtil.getString(tradeInfoMap, "SELF_ACC_NAME", ""));
				ttTrdOrder.setSelf_bankcode(ParameterUtil.getString(tradeInfoMap, "SELF_BANK_CODE", ""));
				ttTrdOrder.setSelf_bankname(ParameterUtil.getString(tradeInfoMap, "SELF_BANK_NAME", ""));
				ttTrdOrder.setSelf_insecuaccid("401");
				ttTrdOrder.setSelf_outsecuaccid("-1");
				ttTrdOrder.setSelf_incashaccid(ParameterUtil.getString(tradeInfoMap, "SELF_INCASHACCID", ""));
				ttTrdOrder.setSelf_outcashaccid(ParameterUtil.getString(tradeInfoMap, "SELF_OUTCASHACCID", ""));
				ttTrdOrder.setParty_bankacccode(ParameterUtil.getString(tradeInfoMap, "PARTY_ACC_CODE", ""));
				ttTrdOrder.setParty_bankaccname(ParameterUtil.getString(tradeInfoMap, "PARTY_ACC_NAME", ""));
				ttTrdOrder.setParty_bankcode(ParameterUtil.getString(tradeInfoMap, "PARTY_BANK_CODE", ""));
				ttTrdOrder.setParty_bankname(ParameterUtil.getString(tradeInfoMap, "PARTY_BANK_NAME", ""));
				if("2".equals(ParameterUtil.getString(tradeInfoMap, "PRD_PROP", ""))){
					ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.ApprovedPass);
				}else{
					ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.Accounted);
				}			
				ttTrdOrder.setOperator_id("admin");
				ttTrdOrder.setOperate_time(DateUtil.getCurrentCompactDateTimeAsString());
				ttTrdOrder.setZzd_netprice(0);
				ttTrdOrder.setZzd_ytm(0);
				ttTrdOrder.setIs_active("1");
				ttTrdOrder.setSettle_fee(0);
				ttTrdOrder.setTrade_fee(0);
				ttTrdOrder.setFst_settle_amount(ParameterUtil.getDouble(tradeInfoMap, "AMT", 0)-ParameterUtil.getDouble(tradeInfoMap, "INT_AMT", 0));
				ttTrdOrder.setEnd_settle_amount(ParameterUtil.getDouble(tradeInfoMap, "M_AMT", 0));
				ttTrdOrder.setStrike_yield(0);
				ttTrdOrder.setCal_days(0);
				ttTrdOrder.setTrdtype("3001");
				ttTrdOrder.setOrder_source(DictConstants.TrustSource.InitImport);
				trdOrderMapper.insert(ttTrdOrder);
				//初始化导入数据，只需要占交易对手额度
				if("1".equals(ParameterUtil.getString(tradeInfoMap, "PRD_PROP", ""))){
				}
			}
		}
		log.info("初始化order信息信息结束...");
	}
	
}
