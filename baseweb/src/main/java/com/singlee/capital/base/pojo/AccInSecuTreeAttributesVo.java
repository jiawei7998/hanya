package com.singlee.capital.base.pojo;

import com.singlee.capital.acc.model.TtAccInSecu;

/**
 * 构建内部证券账户树组件时使用对象
 * @author gm
 *
 */
public class AccInSecuTreeAttributesVo extends TtAccInSecu {

	private static final long serialVersionUID = -5950817242921565901L;
	private String package_id;
	private String visit_level;
	private String package_type;
	private String package_name;
	private String package_pid;
	private String acc_out_secu_id;//外部证券账户
	private String host_accid;//中债登托管账户
	private String child_in_secu_ids;//该节点下所有内部证券账户子节点集合
	private String acc_status;//账户节点的状态
	
	public String getAcc_status() {
		return acc_status;
	}
	public void setAcc_status(String acc_status) {
		this.acc_status = acc_status;
	}
	public String getChild_in_secu_ids() {
		return child_in_secu_ids;
	}
	public void setChild_in_secu_ids(String child_in_secu_ids) {
		this.child_in_secu_ids = child_in_secu_ids;
	}
	public String getHost_accid() {
		return host_accid;
	}
	public void setHost_accid(String host_accid) {
		this.host_accid = host_accid;
	}
	public String getAcc_out_secu_id() {
		return acc_out_secu_id;
	}
	public void setAcc_out_secu_id(String acc_out_secu_id) {
		this.acc_out_secu_id = acc_out_secu_id;
	}
	public String getPackage_id() {
		return package_id;
	}
	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}
	public String getVisit_level() {
		return visit_level;
	}
	public void setVisit_level(String visit_level) {
		this.visit_level = visit_level;
	}
	public String getPackage_type() {
		return package_type;
	}
	public void setPackage_type(String package_type) {
		this.package_type = package_type;
	}
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public String getPackage_pid() {
		return package_pid;
	}
	public void setPackage_pid(String package_pid) {
		this.package_pid = package_pid;
	}
	
}
