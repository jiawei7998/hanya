package com.singlee.capital.base.util;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务流程配置文件
 * @author kevin_gm
 *
 */
public class SettleConstants {
	public static Map<String,String> beanNameMap=new HashMap<String,String>();
	
	//允许直接从审批单生成交易单的业务类型
	public static String[] allowCreateTrustList = {
		DictConstants.TrdType.BondBuy,
		DictConstants.TrdType.BondSell,
		DictConstants.TrdType.ReverseRepoPledged,
		DictConstants.TrdType.SellRepoPledged,
		DictConstants.TrdType.IbBorrowing,
		DictConstants.TrdType.IbLending,
		DictConstants.TrdType.SellRepoOutright,
		DictConstants.TrdType.ReverseRepoOutright,
		DictConstants.TrdType.IBCDIssue,
		DictConstants.TrdType.BondLendIn,
		DictConstants.TrdType.BondLendOut,
		DictConstants.TrdType.BondIssue,
		DictConstants.TrdType.BondFwdBuy,
		DictConstants.TrdType.BondFwdSell,
		DictConstants.TrdType.Custom
	};
	//初始化
	static{
		beanNameMap.put("1", "BondBuySellServiceImpl");
		beanNameMap.put("2", "PledgedServiceImpl");
		beanNameMap.put("4", "DepositToBankFixedServiceImpl");
		beanNameMap.put("5", "DepositFromBankFixedServiceImpl");
		beanNameMap.put("6", "CurrentToBankServiceImpl");
		beanNameMap.put("7", "CurrentFromBankInServiceImpl");
		beanNameMap.put("8", "BondExerciseServiceImpl");
		beanNameMap.put("9", "BillServiceImpl");
		beanNameMap.put("11", "MoneyServiceImpl");
		beanNameMap.put("12", "AssetPlanServiceImpl");
		beanNameMap.put("13", "FixedAssetplanServiceImpl");
		beanNameMap.put("99", "UnAssetServiceImpl");
		beanNameMap.put("17", "BankLoanServiceImpl");
		beanNameMap.put("14", "OutrightRepoServiceImpl");
		beanNameMap.put("16", "DistributeServiceImpl");
		beanNameMap.put("18", "IbcdServiceImpl");
		beanNameMap.put("20", "BondIssueServiceImpl");
		beanNameMap.put("21", "BondLendServiceImpl");
		beanNameMap.put("22", "BondfwdServiceImpl");
		beanNameMap.put("23", "TreasuryServiceImpl");
		beanNameMap.put("30", "CustomServiceImpl");
		beanNameMap.put("50", "MoneyIssueServiceImpl");
		beanNameMap.put("51", "ProdFeeServiceImpl");
		beanNameMap.put("52", "BenefitServiceImpl");
		beanNameMap.put("53", "EnTrustAssetServiceImpl");
		beanNameMap.put("60", "FundCustomServiceImpl");
		
	}
	/**
	 * 判断业务类型是否允许直接生成交易单
	 * @param trdtype
	 * @return
	 */
	public static boolean allowCreateTrust(String trdtype){
		List<String> trdList = Arrays.asList(allowCreateTrustList);
		return trdList.contains(trdtype);
	}

	/**
	 * 根据不同业务返回bean名称
	 * @param folder
	 * @param trdType
	 * @return
	 */
	public static String getBeanFromTrdType(String folder,String trdType){
		String typesName = "";
		
		if(DictConstants.BillsType.Approve.equals(trdType)){
			typesName = "approve";
		}else if(DictConstants.BillsType.Trust.equals(trdType)){
			typesName = "trust";
		}else if(DictConstants.BillsType.Trade.equals(trdType)){
			typesName = "trade";
		}else{
			JY.raise("传入的业务类型["+trdType+"]不存在.");
		}
		//modify shm 2015-06-25
		String beanName =beanNameMap.get(folder);
		if (StringUtil.isNullOrEmpty(beanName)){
			throw new RException("业务类型[" +folder+"]找不到指定的bean文件.");
		}
		return typesName + beanName;
	}
}
