package com.singlee.capital.base.model;

import com.singlee.capital.system.model.TtAccInSecuVisit;

import java.util.List;

/**
 * 前台批量保存柜员对应内部证券账户权限关系时使用
 * @author gm
 *
 */
public class TtAccInSecuVisitListVo {

	private List<TtAccInSecuVisit> list;

	public List<TtAccInSecuVisit> getList() {
		return list;
	}

	public void setList(List<TtAccInSecuVisit> list) {
		this.list = list;
	}
}
