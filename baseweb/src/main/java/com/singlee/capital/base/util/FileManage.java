package com.singlee.capital.base.util;

import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * 文件管理
 * 1.临时文件上传
 * 2.临时文件移动至正式文件上传目录
 * @author thinkpad
 *
 */
public class FileManage {
	
	/**
	 * 从提交的请求中读取文件对象
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	public static List<UploadedFile> requestExtractor(MultipartHttpServletRequest request) throws IOException{
		List<UploadedFile> uploadedFileList = new ArrayList<UploadedFile>();
		Iterator<String> itr = request.getFileNames();
		while (itr != null && itr.hasNext()) {
			List<MultipartFile> mpfList = request.getFiles(itr.next());
			for(MultipartFile mpf : mpfList){
				UploadedFile uploadedFile = new UploadedFile();
				String file_full_name = mpf.getOriginalFilename();
				uploadedFile.setFullname(file_full_name);
				uploadedFile.setBytes(mpf.getBytes());
				uploadedFileList.add(uploadedFile);
			}
		}
		return uploadedFileList;
	}

	/**
	 * 文件写入目录(覆盖已存在的目录)
	 * @throws IOException 
	 */
	public static void fileToCatalog(List<UploadedFile> uploadedFileList,String pathName) throws IOException{
		for (UploadedFile uploadedFile : uploadedFileList) {
			File file = new File(pathName, uploadedFile.getFullname());
			if (!file.exists()) {
				file.createNewFile();
			}
			FileUtils.writeByteArrayToFile(file, uploadedFile.getBytes());
		}
	}
	
	/**
	 * 文件移动 (该方法存在问题，移动后文件不可用)
	 * @param sourcePath
	 * @param targetPath
	 * @throws IOException 
	 */
	public static void fileMove(String sourcePath,String targetPath,String file_src) throws IOException{
		//源文件
		File sourceFile = new File(sourcePath, file_src);
		if (!sourceFile.exists()) {
			JY.raiseRException("文件移动异常:源文件夹不存在文件["+file_src+"].");
		}
        InputStream fis = new BufferedInputStream(new FileInputStream(sourceFile));
        byte[] buffer = new byte[fis.available()];
        //目标文件
		File targetFile = new File(targetPath, file_src);
		if (targetFile.exists()) {
			JY.raiseRException("文件移动异常:目标文件夹已存在文件["+file_src+"].");
		}
		targetFile.createNewFile();
		FileUtils.writeByteArrayToFile(targetFile, buffer);
		//删除源文件
		fis.close();
		sourceFile.delete();
	}
	
	/**
	 * 读取文件
	 * @param pathName
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static UploadedFile getUploadedFile(String pathName,String fileName) throws IOException{
		UploadedFile uploadedFile = new UploadedFile();
		File file = new File(pathName, fileName);
		if (!file.exists()) {
			return null;
		}
        InputStream fis = new BufferedInputStream(new FileInputStream(file));
        byte[] buffer = new byte[fis.available()];
		uploadedFile.setBytes(buffer);
		uploadedFile.setFullname(fileName);
		return uploadedFile;
	}
	
}
