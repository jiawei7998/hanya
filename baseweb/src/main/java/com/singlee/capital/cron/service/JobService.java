package com.singlee.capital.cron.service;

import com.github.pagehelper.Page;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.cron.model.TaJobConfig;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 任务服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface JobService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param jobId - 任务主键
	 * @return 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public TaJob getJobById(String jobId);

	public List<TaJob> getTaJobLst();
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public Page<TaJob> getJobPage (Map<String, String> params);
	
	/**
	 * 新增
	 * 
	 * @param job - 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public TaJob createJob(TaJob job);
	
	/**
	 * 修改
	 * 
	 * @param job - 任务对象
	 * @author Hunter
	 * @return 
	 * @date 2016-11-30
	 */
	public TaJob updateJob(TaJob job);
	
	/**
	 * 删除
	 * 
	 * @param jobIds - 任务主键列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public void deleteJob(String[] jobIds);
	
	/**
	 * 启动任务
	 * 
	 * @param jobId - 任务id
	 * @author Hunter
	 * @date 2016-12-12
	 */
	public TaJob startJob(String jobId);
	
	/**
	 * 停止任务
	 * 
	 * @param jobId - 任务id
	 * @author Hunter
	 * @date 2016-12-12
	 */
	public TaJob stopJob(String jobId);
	
	/**
	 * 执行任务
	 * 
	 * @param jobId - 任务id
	 * @author Hunter
	 * @date 2016-12-12
	 */
	public void execJob(String jobId);

	/**
	 * 定时任务初始化
	 * 
	 * @param jobId - 任务id
	 * @author Hunter
	 * @date 2016-12-12
	 */
	public void init();
	
	/**
	 * 按版本号竞争任务
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @return 
	 * @date 2016-11-30
	 */
	public int updateJobVersion(Map<String, Object> params);

	/**
	 * 按jobId更新任务
	 * 
	 * @param job - 任务对象
	 * @author Hunter
	 * @return 
	 * @date 2016-12-19
	 */
	public void updateJobById(TaJob job);
	/**
	 * 根据JOB的ID号获取JOB配置
	 * @param jobId
	 * @return
	 */
	public TaJobConfig getTaJobConfigByJobId(int jobId);
	
	public List<TaJobConfig> getJobConfigPage (Map<String, String> params);
	
	public TaJobConfig createJobConfig(TaJobConfig job);
	
	public TaJobConfig updateJobConfig(TaJobConfig job);
	
	public void deleteJobConfig(String[] jobIds);

	
}
