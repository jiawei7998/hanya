package com.singlee.capital.cron.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.capital.cron.model.TaJobLog;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 定时任务日志持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TaJobLogMapper extends MyMapper<TaJobLog> {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public Page<TaJobLog> getJobLogList(Map<String, String> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询列表
	 * 
	 * @param params - 请求参数
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public List<TaJobLog> getJobLogList(Map<String, String> params);
}