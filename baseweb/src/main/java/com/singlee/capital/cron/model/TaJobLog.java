package com.singlee.capital.cron.model;

import com.singlee.capital.common.cron.CronJobLog;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @projectName 同业业务管理系统
 * @className 定时任务日志
 * @description TODO
 * @author 倪航
 * @createDate 2016-11-30 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */

@Entity
@Table(name = "TA_JOB_LOG")
public class TaJobLog implements Serializable , CronJobLog{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3873636587213809991L;

	/**
	 * 任务编号 序列自动生成
	 */
	@Id
	private String jobId;
	
	/**
	 * 开始时间
	 */
	private String startTime;
	
	/**
	 * 结束时间
	 */
	private String endTime;
	
	/**
	 * 执行时间
	 */
	private Integer execTime;
	
	/**
	 * 执行人
	 */
	private String execUser;
	
	/**
	 * 执行IP
	 */
	private String execIp;
	
	/**
	 * 执行结果
	 */
	private String result;

	/**
	 * 数据库时间
	 */

	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT TO_CHAR(SYSTIMESTAMP,'YYYY-MM-DD HH24:MI:SSXFF3') FROM DUAL")
	private String dbTime;
	
	@Transient
	private boolean ok;
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getExecTime() {
		return execTime;
	}

	public void setExecTime(Integer execTime) {
		this.execTime = execTime;
	}

	public String getExecUser() {
		return execUser;
	}

	public void setExecUser(String execUser) {
		this.execUser = execUser;
	}

	public String getExecIp() {
		return execIp;
	}

	public void setExecIp(String execIp) {
		this.execIp = execIp;
	}

	public String getResult() {
		return result;
	}

	@Override
	public void setResult(String result) {
		this.result = result;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	@Override
	public boolean ok() {
		// TODO Auto-generated method stub
		return this.ok;
	}

	public String getDbTime() {
		return dbTime;
	}

	public void setDbTime(String dbTime) {
		this.dbTime = dbTime;
	}
	
}
