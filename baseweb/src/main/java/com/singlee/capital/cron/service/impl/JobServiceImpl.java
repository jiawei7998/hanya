package com.singlee.capital.cron.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.cron.Cron;
import com.singlee.capital.common.cron.CronClass;
import com.singlee.capital.common.cron.CronClassImpl;
import com.singlee.capital.common.cron.CronJob;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.cron.mapper.TaJobConfigMapper;
import com.singlee.capital.cron.mapper.TaJobMapper;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.cron.model.TaJobConfig;
import com.singlee.capital.cron.pojo.CronJobImpl;
import com.singlee.capital.cron.service.JobService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @projectName 同业业务管理系统
 * @className 任务服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service("jobService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class JobServiceImpl implements JobService {
	private final static String cacheName ="com.singlee.capital.system.service.job";
	private final static String cacheNameConfig ="com.singlee.capital.system.service.jobconfig";
	@Autowired
	private TaJobMapper jobMapper;
	@Autowired
	private TaJobConfigMapper jobConfigMapper;

	@Override
	@Cacheable(value = cacheName ,key = "'JobId:'+#p0")
	public TaJob getJobById(String JobId) {
		return jobMapper.getJobById(JobId);
	}

	@Override
    public List<TaJob> getTaJobLst(){
		Map<String, String> params = new HashMap<String, String>();
		params.put("isDisabled", DictConstants.Status.Enabled);
		List<TaJob> jobs = jobMapper.getJobList(params);
		return jobs;
	}

	@Override
	public Page<TaJob> getJobPage(Map<String, String> params) {
		return jobMapper.getJobList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	@AutoLogMethod(value = "创建任务")
	@CachePut(value = cacheName ,key = "'JobId:'+#job.jobId")
	public synchronized TaJob createJob(TaJob job) {
		Map<String, String> params = new HashMap<String, String>();
		Integer i = 0;
		// 判断表单名称是否存
		if (StringUtil.isNotEmpty(job.getJobName())) {
			params.put("jobName", job.getJobName());
			i = jobMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("任务名称已存在！");
			}
		}
		job.setVersion(new Integer(0));
		// 保存任务
		jobMapper.insert(job);
		return getJobById(job.getJobId());
	}

	@Override
	@AutoLogMethod(value = "修改任务")
	@CachePut(value = cacheName ,key = "'JobId:'+#job.jobId")
	public synchronized TaJob updateJob(TaJob job) {
		Map<String, String> params = new HashMap<String, String>();
		Integer i = 0;
		// 判断表单名称是否存在
		if (StringUtil.isNotEmpty(job.getJobName())) {
			params.put("jobId", job.getJobId());
			params.put("jobName", job.getJobName());
			i = jobMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("任务名称已存在！");
			}
		}
		// 更新任务
		jobMapper.updateByPrimaryKeySelective(job);
		return getJobById(job.getJobId());
	}

	@Override
	@AutoLogMethod(value = "删除任务")
	@CacheEvict(value = cacheName ,key = "'JobId:'+#p0")
	public void deleteJob(String[] jobIds) {
		for (int i = 0; i < jobIds.length; i++) {
			jobMapper.deleteByPrimaryKey(jobIds[i]);
		}
	}

	@Override
	@AutoLogMethod(value = "启动任务")
	@CachePut(value = cacheName ,key = "'JobId:'+#p0")
	public synchronized TaJob startJob(String jobId) {
		Cron.getInstance().enableCronJob(jobId);
		// 立马执行
//		Cron.getInstance().forceRunCronJob(jobId);
		TaJob job = jobMapper.getJobById(jobId);
		List<TaJob> jobs = new ArrayList<TaJob>();
		jobs.add(job);
		runJobs(jobs);
		job.setIsDisabled(DictConstants.Status.Enabled);
		jobMapper.updateByPrimaryKeySelective(job);
		return getJobById(jobId);
	}

	@Override
	@AutoLogMethod(value = "停止任务")
	@CachePut(value = cacheName ,key = "'JobId:'+#p0")
	public synchronized TaJob stopJob(String jobId) {
		Cron.getInstance().disableCronJob(jobId);
//		Cron.getInstance().terminateCronJob(jobId);
		TaJob job = jobMapper.getJobById(jobId);
		job.setIsDisabled(DictConstants.Status.Disabled);
		jobMapper.updateByPrimaryKeySelective(job);
		return getJobById(jobId);
	}

	@Override
	public synchronized void execJob(String jobId) {
		TaJob job = jobMapper.getJobById(jobId);
		if (DictConstants.jobStatus.Executeing.equals(job.getJobStatus())) {
			JY.raise("任务正在执行，请勿重复操作！");
		}
		Cron.getInstance().forceRunCronJob(jobId);
		job.setManRunDate(DateUtil.getCurrentDateTimeAsString());
		jobMapper.updateByPrimaryKeySelective(job);
	}

	@Override
	public void init() {
		Map<String, String> params = new HashMap<String, String>();
		JY.info("[INFO]==> JobServiceImpl初始化库中所有可用的Jobs......");
		params.put("isDisabled", DictConstants.Status.Enabled);
		List<TaJob> jobs = jobMapper.getJobList(params);
		JY.info("[INFO]==> JobServiceImpl查找所有Jobs共[" + jobs.size() + "]个，并准备启动...");
		runJobs(jobs);
	}

	@Override
	public int updateJobVersion(Map<String, Object> params) {
		return jobMapper.updateJobVersion(params);
	}

	@Override
	public void updateJobById(TaJob job) {
		jobMapper.updateByPrimaryKeySelective(job);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	private void runJobs(List<TaJob> jobs) {
		Set<CronJob> inCronJobs = new HashSet<CronJob>();
		List<String> runableJobIds = new ArrayList<String>();
		JY.info("==> 开始循环处理job...");
		for (TaJob job : jobs) {
			List<CronClass> clazz = new LinkedList<CronClass>();
			Map<String, Object> parameter = new HashMap<String, Object>();
			String classList = job.getClassList();
			String[] classListArray = null;
			if (classList != null) {
				classListArray = classList.split("\\|");
			}
			String parameters = job.getParameters();
			String[] parametersArray = null;
			if (parameters != null) {
				parametersArray = parameters.split("\\|");
			}

			if (classListArray != null) {
				for (int i = 0; i < classListArray.length; i++) {
					if (parametersArray != null) {
						String[] pars = parametersArray[i].split(",");
						parameter.clear();
						for (int j = 0; j < pars.length; j++) {
							String[] par = pars[j].split(":");
							parameter.put(par[0], par[1]);
						}
					}
					clazz.add(new CronClassImpl(classListArray[i], true, parameter));
				}
			}
			// 判断job是否重复添加
			Set<CronJob> cronJobs = Cron.getInstance().getCronJobs();
			CronJob cronJob;
			Iterator<CronJob> iter = cronJobs.iterator();
			while (iter.hasNext()) {
				cronJob = (CronJob) iter.next();
				if (cronJob.getJobId().equals(job.getJobId())) {
					return;}
			} // for
			inCronJobs.addAll(cronJobs);
			if (DictConstants.Status.Enabled.equals(job.getIsDisabled())) {
				JY.info(String.format("==> 开始准备执行【编号：[%s] , 名称：[%s] , 调度频率：[%s]】的任务", job.getJobId(), job.getJobName(),job.getSchedule()));
				CronJob cj = new CronJobImpl(job.getJobId(), job.getJobName(), job.getSchedule(),
						DictConstants.YesNo.YES.equals(job.getIsStartup()) ? true : false, false, clazz);
				SlSession session = SlSessionHelper.getSlSession();
				cj.setSession(session);
				inCronJobs.add(cj);
				runableJobIds.add(job.getJobId());
			}
		}
		Cron.getInstance().reload(inCronJobs, 0, false);
		for (String jobId : runableJobIds) {
			Cron.getInstance().enableCronJob(jobId);
		}
	}

	@Override
	@Cacheable(value = cacheNameConfig ,key = "'JobId:'+T(String).valueOf(#p0)")
	public TaJobConfig getTaJobConfigByJobId(int jobId) {
		// TODO Auto-generated method stub
		return this.jobConfigMapper.selectByPrimaryKey(jobId);
	}

	@Override
	public List<TaJobConfig> getJobConfigPage(Map<String, String> params) {
		// TODO Auto-generated method stub
		return jobConfigMapper.selectAll();
	}

	@Override
	public TaJobConfig createJobConfig(TaJobConfig job) {
		// TODO Auto-generated method stub
		jobConfigMapper.insert(job);
		return getTaJobConfigByJobId(job.getJobId());
	}

	@Override
	@CachePut(value = cacheNameConfig ,key = "'JobId:'+#job.jobId")
	public TaJobConfig updateJobConfig(TaJobConfig job) {
		// TODO Auto-generated method stub
		jobConfigMapper.updateByPrimaryKeySelective(job);
		return getTaJobConfigByJobId(job.getJobId());
	}

	@Override
	@CacheEvict(value = cacheNameConfig ,key = "'JobId:'+#p0")
	public void deleteJobConfig(String[] jobIds) {
		// TODO Auto-generated method stub
		for (String string : jobIds) {
			jobConfigMapper.deleteByPrimaryKey(Integer.valueOf(string.trim()));
		}
	}

}
