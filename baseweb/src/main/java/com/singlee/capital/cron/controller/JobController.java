package com.singlee.capital.cron.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.cron.model.TaJobConfig;
import com.singlee.capital.cron.model.TaJobLog;
import com.singlee.capital.cron.service.JobLogService;
import com.singlee.capital.cron.service.JobService;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
/**
 * @projectName 同业业务管理系统
 * @className 任务控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午5:08:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/JobController")
public class JobController extends CommonController {
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private JobLogService jobLogService;
	
	/**
	 * 根据ID查询单个明细
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/searchJobById")
	public RetMsg<TaJob> searchJobById(@RequestBody Map<String,Object> params){
		String jobId = ParameterUtil.getString(params, "jobId", "");
		TaJob job = jobService.getJobById(jobId);
		return RetMsgHelper.ok(job);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageJob")
	public RetMsg<PageInfo<TaJob>> searchPageJob(@RequestBody Map<String, String> params){
		Page<TaJob> page = jobService.getJobPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增
	 * 
	 * @param job - 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/addJob")
	public RetMsg<Serializable> addJob(@RequestBody TaJob job) {
		jobService.createJob(job);
		//操作job配置表
		TaJobConfig jobConfig = new TaJobConfig();
		jobConfig.setJobId(Integer.parseInt(job.getJobId()));
		jobConfig.setExecIp(job.getSingletonFlag());
		jobService.createJobConfig(jobConfig);
		
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * 
	 * @param form - 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/editJob")
	public RetMsg<Serializable> editJob(@RequestBody TaJob job) {
		jobService.updateJob(job);
		//操作job配置表
		TaJobConfig jobConfig = new TaJobConfig();
		jobConfig.setJobId(Integer.parseInt(job.getJobId()));
		jobConfig.setExecIp(job.getSingletonFlag());
		jobService.updateJobConfig(jobConfig);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 * 
	 * @param form - 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/removeJob")
	public RetMsg<Serializable> removeJob(@RequestBody String[] jobIds) {
		jobService.deleteJob(jobIds);
		//操作job配置表
		jobService.deleteJob(jobIds);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 启动/停止任务
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/switchJob")
	public RetMsg<Serializable> switchJob(@RequestBody Map<String, String> params) {
		boolean checked = ParameterUtil.getBoolean(params, "checked", false);
		String jobId = ParameterUtil.getString(params, "jobId", "");
		if (checked == true) {
			jobService.startJob(jobId);
			return RetMsgHelper.ok("启动成功");
		} else {
			jobService.stopJob(jobId);
			return RetMsgHelper.ok("停止成功");
		}
	}
	
	/**
	 * 执行任务
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/execJob")
	public RetMsg<Serializable> execJob(@RequestBody Map<String, String> params) {
		String jobId = ParameterUtil.getString(params, "jobId", "");
		jobService.execJob(jobId);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageJobConfig")
	public RetMsg<List<TaJobConfig>> searchPageJobConfig(@RequestBody Map<String, String> params){
		List<TaJobConfig> list = jobService.getJobConfigPage(params);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/addJobConfig")
	public RetMsg<Serializable> addJob(@RequestBody TaJobConfig job) {
		jobService.createJobConfig(job);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/editJobConfig")
	public RetMsg<Serializable> editJobConfig(@RequestBody TaJobConfig job) {
		jobService.updateJobConfig(job);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/removeJobConfig")
	public RetMsg<Serializable> removeJobConfig(@RequestBody String[] jobIds) {
		jobService.deleteJobConfig(jobIds);
		return RetMsgHelper.ok();
	}

	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-11-30
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageJobLogs")
	public RetMsg<PageInfo<TaJobLog>> searchJobLogs(@RequestBody Map<String, String> params){
		Page<TaJobLog> page = jobLogService.getJobLogPage(params);
		return RetMsgHelper.ok(page);
	}
}
