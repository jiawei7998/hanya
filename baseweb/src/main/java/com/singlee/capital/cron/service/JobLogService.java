package com.singlee.capital.cron.service;

import com.github.pagehelper.Page;
import com.singlee.capital.cron.model.TaJobLog;

import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 任务日志服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface JobLogService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public Page<TaJobLog> getJobLogPage (Map<String, String> params);
	
	/**
	 * 新增
	 * 
	 * @param job - 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public void createJobLog(TaJobLog jobLog);
}
