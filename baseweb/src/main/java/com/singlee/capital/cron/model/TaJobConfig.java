package com.singlee.capital.cron.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="TA_JOB_CONFIG")
public class TaJobConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int jobId;
	
	private String execIp;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getExecIp() {
		return execIp;
	}

	public void setExecIp(String execIp) {
		this.execIp = execIp;
	}
	

}
