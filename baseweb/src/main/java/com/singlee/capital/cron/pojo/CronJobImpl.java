/*
 * Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.singlee.capital.cron.pojo;

import com.singlee.capital.common.cron.AbstractCronJobImpl;
import com.singlee.capital.common.cron.CronClass;
import com.singlee.capital.common.cron.CronJobLog;
import com.singlee.capital.common.cron.Utils;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.cron.model.TaJobConfig;
import com.singlee.capital.cron.model.TaJobLog;
import com.singlee.capital.cron.service.JobLogService;
import com.singlee.capital.cron.service.JobService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;

import java.net.*;
import java.util.*;

/**
 * represents a single cron job.
 * 
 * @author Jarrett Taylor
 */
public class CronJobImpl extends AbstractCronJobImpl {

    private JobLogService jobLogService;
    private JobService jobService;

    public CronJobImpl(String inJobId, String inJobName, String inSchedule, boolean inStartup, boolean inDisabled,
        List<? extends CronClass> inCronClasses) {
        super(inJobId, inJobName, inSchedule, inStartup, inDisabled, inCronClasses);
        jobLogService = SpringContextHolder.getBean("jobLogService");
        jobService = SpringContextHolder.getBean("jobService");
    }

    @Override
    protected CronJobLog beforeRun() {
        TaJob job = jobService.getJobById(this.getJobId());
        String localIp = null;
        TaJobLog jobLog = new TaJobLog();
        jobLog.setResult("执行成功");
        jobLog.setStartTime(DateUtil.getCurrentDateAsString("yyyy-MM-dd HH:mm:ss.SSS"));
        JY.info(String.format("==> 任务执行前处理，编号:%s,优先级%s,任务名称:%s",job.getJobId(),Thread.currentThread().getPriority(), job.getJobName()));
        if (!"1".equals(job.getIsSingleton())) {// 非单例模式需要判断IP
	        try {
	            localIp = getCurrentIp().getHostAddress();
	            // 获取数据配置modify by shenzl 2018/11/13 修改linux下获取ip
	            TaJobConfig taJobConfig = jobService.getTaJobConfigByJobId(Integer.parseInt(job.getJobId()));
	            JY.info(String.format("==> 任务执行前处理，判断任务是否可以执行【编号:%s，执行机器的IP:%s,数据库配置的IP:%s】",job.getJobId(), localIp,taJobConfig.getExecIp()));
	            if (null == taJobConfig || !StringUtils.trimToEmpty(taJobConfig.getExecIp()).equals(localIp)) {
	            	JY.info(String.format("==> 任务执行前处理，编号:%s,任务名称:%s,不允许执行!",job.getJobId(), job.getJobName()));
	            	jobLog.setOk(false);
	                jobLog.setResult(localIp + "不允许执行!");
	                return jobLog;
	            }
	        } catch (Exception e) {
	            log.error("获取IP异常");
	            jobLog.setResult("获取IP异常");
	        }
        }
        //添加joblog
        JY.info(String.format("==> 任务执行前处理，添加编号:%s,任务名称:%s日志",job.getJobId(), job.getJobName()));
        jobLog.setJobId(this.jobId);
        if (session != null) {
            jobLog.setExecUser(SlSessionHelper.getUserId(session));
            jobLog.setExecIp(SlSessionHelper.getIp(session));
        } else {
            jobLog.setExecUser("自动执行");
            jobLog.setExecIp(localIp);
        }
        jobLog.setStartTime(DateUtil.getCurrentDateAsString("yyyy-MM-dd HH:mm:ss.SSS"));

        Integer oldVersion = job.getVersion();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", job.getJobId());
        params.put("jobStatus", DictConstants.jobStatus.Executeing);
        params.put("version", oldVersion + 1);
        params.put("oldVersion", oldVersion);
        int count = jobService.updateJobVersion(params);

        if (DictConstants.YesNo.YES.equals(job.getIsSingleton())) {
            if (DictConstants.jobStatus.Executeing.equals(job.getJobStatus())) {
                jobLog.setOk(false);
                jobLog.setResult("其他服务器正在执行中...");
                return jobLog;
            }

            if (count == 1) {
                jobLog.setOk(true);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                String nowRunDate = DateUtil.format(Utils.getNextRun(Utils.convertSchedule(schedule), cal.getTime()), "yyyy-MM-dd HH:mm:ss");
                log.debug(job.getNextRunDate() + "==" + nowRunDate);
                // modify by shenzl 2018-05-27 取消时间判断
                // if (!nowRunDate.equals(job.getNextRunDate())) {
                // jobLog.setOk(false);
                // jobLog.setResult("任务被" + job.getSingletonFlag() + "抢占，由该服务器单例执行。");
                // return jobLog;
                // }
            } else {
                jobLog.setOk(false);
                jobLog.setResult("竞争失败，由其他服务器执行...");
            }
            return jobLog;
        }
        jobLog.setOk(true);
        return jobLog;
    }

    @Override
    protected void afterRun(CronJobLog l) {
        TaJob job = jobService.getJobById(this.getJobId());
        JY.info(String.format("==> 任务执行后处理，编号:%s,优先级%s,任务名称:%s开始",job.getJobId(), Thread.currentThread().getPriority(),job.getJobName()));
        TaJobLog jobLog = (TaJobLog)l;
        if (l.ok()) {
            job.setSingletonFlag(jobLog.getExecIp());
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        job.setLastRunDate(DateUtil.format(cal.getTime(), "yyyy-MM-dd HH:mm:ss"));
        cal.add(Calendar.MINUTE, 1);
        cal.set(Calendar.SECOND, 0);
        job.setNextRunDate( DateUtil.format(Utils.getNextRun(Utils.convertSchedule(schedule), cal.getTime()), "yyyy-MM-dd HH:mm:ss"));
        job.setJobStatus(DictConstants.jobStatus.Pending);
        jobService.updateJobById(job);
        jobLog.setEndTime(DateUtil.getCurrentDateAsString("yyyy-MM-dd HH:mm:ss.SSS"));
        Integer execTime = DateUtil.timesBetween(jobLog.getStartTime(), jobLog.getEndTime());
        jobLog.setExecTime(execTime);
        if (jobLog.isOk()) {
            jobLogService.createJobLog(jobLog);}
        JY.info(String.format("==> 任务执行后处理，编号:%s,任务名称:%s完成",job.getJobId(), job.getJobName()));
    }

    @Override
    protected void errHandle(CronJobLog l, String err) {
        l.setResult(err);
    }

    public static InetAddress getCurrentIp() {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            if (inetAddress.isSiteLocalAddress()) {
                return inetAddress;
            }
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            networkInterfaces.nextElement();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface)networkInterfaces.nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while (nias.hasMoreElements()) {
                    InetAddress ia = (InetAddress)nias.nextElement();
                    if (!ia.isLinkLocalAddress() && !ia.isLoopbackAddress() && ia instanceof Inet4Address) {
                        return ia;
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

} // class
