package com.singlee.capital.cron.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.capital.cron.model.TaJob;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 定时任务持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TaJobMapper extends MyMapper<TaJob> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param jobId - 任务主键
	 * @return 任务对象
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public TaJob getJobById(String JobId);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public Page<TaJob> getJobList(Map<String, String> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询列表
	 * 
	 * @param params - 请求参数
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-11-30
	 */
	public List<TaJob> getJobList(Map<String, String> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 记录数
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public int judgeDuplicate(Map<String, String> params);
	
	/**
	 * 按版本号竞争任务
	 * 
	 * @param params - 请求参数
	 * @return 记录数
	 * @author Hunter
	 * @date 2016-12-16
	 */
	public int updateJobVersion(Map<String, Object> params);
	
	public List<TaJob> getJobByclassList(Map<String, String> params);
	
	public List<TaJob> getEnableJobList(Map<String, String> params);
	
	
}