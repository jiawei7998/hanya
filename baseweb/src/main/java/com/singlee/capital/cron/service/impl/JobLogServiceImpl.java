package com.singlee.capital.cron.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.cron.mapper.TaJobLogMapper;
import com.singlee.capital.cron.model.TaJobLog;
import com.singlee.capital.cron.service.JobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 任务日志服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-11-30 下午4:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service("jobLogService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class JobLogServiceImpl implements JobLogService {
	@Autowired
	private TaJobLogMapper jobLogMapper;
	
	@Override
	public Page<TaJobLog> getJobLogPage(Map<String, String> params) {
		return jobLogMapper.getJobLogList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	@AutoLogMethod(value="创建任务日志")
	public void createJobLog(TaJobLog jobLog) {
		//保存任务
		jobLogMapper.insert(jobLog);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
