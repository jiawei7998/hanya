package com.singlee.capital.cron.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @projectName 同业业务管理系统
 * @className 定时任务
 * @description TODO
 * @author 倪航
 * @createDate 2016-11-30 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */

@Entity
@Table(name = "TA_JOB")
public class TaJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2294739040239155972L;
	
	/**
	 * 任务编号 序列自动生成
	 */
	@Id
	private String jobId;
	
	/**
	 * 任务名称
	 */
	private String jobName;
	
	/**
	 * 时间表
	 */
	private String schedule;
	
	/**
	 * 执行类列表
	 */
	private String classList;
	
	/**
	 * 执行类参数
	 */
	private String parameters;
	
	/**
	 * 上次手动执行时间
	 */
	private String manRunDate;
	
	/**
	 * 上次执行时间
	 */
	private String lastRunDate;
	
	/**
	 * 下次执行时间
	 */
	private String nextRunDate;
	
	/**
	 * 任务状态
	 */
	private String jobStatus;
	
	/**
	 * 启用/停用
	 */
	private String isDisabled;
	
	/**
	 * 是否销毁
	 */
	private String isKilled;
	
	/**
	 * 是否立即启动
	 */
	private String isStartup;
	
	/**
	 * 是否单例执行
	 */
	private String isSingleton;
	
	/**
	 * 单例执行标志
	 */
	private String singletonFlag;
	
	/**
	 * 是否允许手动执行
	 */
	private String isManual;
	
	/**
	 * 是否允许关闭
	 */
	private String isDisable;
	
	/**
	 * 版本号
	 */
	private Integer version;
	
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getClassList() {
		return classList;
	}

	public void setClassList(String classList) {
		this.classList = classList;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getManRunDate() {
		return manRunDate;
	}

	public void setManRunDate(String manRunDate) {
		this.manRunDate = manRunDate;
	}

	public String getLastRunDate() {
		return lastRunDate;
	}

	public void setLastRunDate(String lastRunDate) {
		this.lastRunDate = lastRunDate;
	}

	public String getNextRunDate() {
		return nextRunDate;
	}

	public void setNextRunDate(String nextRunDate) {
		this.nextRunDate = nextRunDate;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(String isDisabled) {
		this.isDisabled = isDisabled;
	}

	public String getIsKilled() {
		return isKilled;
	}

	public void setIsKilled(String isKilled) {
		this.isKilled = isKilled;
	}

	public String getIsStartup() {
		return isStartup;
	}

	public void setIsStartup(String isStartup) {
		this.isStartup = isStartup;
	}

	public String getIsSingleton() {
		return isSingleton;
	}

	public void setIsSingleton(String isSingleton) {
		this.isSingleton = isSingleton;
	}

	public String getSingletonFlag() {
		return singletonFlag;
	}

	public void setSingletonFlag(String singletonFlag) {
		this.singletonFlag = singletonFlag;
	}

	public String getIsManual() {
		return isManual;
	}

	public void setIsManual(String isManual) {
		this.isManual = isManual;
	}

	public String getIsDisable() {
		return isDisable;
	}

	public void setIsDisable(String isDisable) {
		this.isDisable = isDisable;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
