package com.singlee.capital.settle.service.impl.dayend;

import com.singlee.capital.base.util.KeyValuePair;
import com.singlee.capital.common.util.JY;

import java.util.ArrayList;
import java.util.List;


/**
 * 持仓结算工厂 负责确定具体的持仓结算类实现
 * 
 * @author lihb
 * 
 */
public class PositionSettleFactory {
	public static final List<KeyValuePair<List<String>, Class<? extends AbstractPositionSettle>>> POSITIONSETTLE_CONFIG = new ArrayList<KeyValuePair<List<String>, Class<? extends AbstractPositionSettle>>>();
	private static class SingletonHolder {
		private static final PositionSettleFactory INSTANCE = new PositionSettleFactory();
	}

	private PositionSettleFactory() {
	}

	public static final PositionSettleFactory getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public AbstractPositionSettle getPositionSettleObj(String atype) {
		AbstractPositionSettle ret = null;
		for (KeyValuePair<List<String>, Class<? extends AbstractPositionSettle>> pair : POSITIONSETTLE_CONFIG) {
			if (pair.key().contains(atype)) {
				Class<? extends AbstractPositionSettle> clazz = pair.value();
				try {
					ret = clazz.newInstance();
				} catch (InstantiationException e) {
					JY.raise("无法实例化" + clazz.getName() + ": " + e.getMessage());
				} catch (IllegalAccessException e) {
					JY.raise("无法实例化" + clazz.getName() + ": " + e.getMessage());
				}
				break;
			}
		}
		if (ret == null) {
			JY.raise("无法实例化持仓结算对象，业务类型" + atype + "没有暂时不支持！");
		}
		// 日志记录
		return ret;
	}
}
