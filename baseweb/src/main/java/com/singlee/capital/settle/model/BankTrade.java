package com.singlee.capital.settle.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TT_TRD_BANKLIST")
public class BankTrade implements Serializable{
	
	private static final long serialVersionUID = 1L;
	/**
	 * 行号
	 */
	private String branch;
	/**
	 * 全称
	 */
	private String branchname;
	/**
	 * 简称
	 */
	private String shortname;
	/**
	 * 导入日期
	 */
	private String impltime;
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	public String getImpltime() {
		return impltime;
	}
	public void setImpltime(String impltime) {
		this.impltime = impltime;
	}
}
