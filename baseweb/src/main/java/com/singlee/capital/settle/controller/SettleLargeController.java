package com.singlee.capital.settle.controller;

import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.settle.large.model.TtSettleLargeInfo;
import com.singlee.capital.settle.large.model.TtSettleLargeInfoMap;
import com.singlee.capital.settle.large.service.SettleLargeInfoMapService;
import com.singlee.capital.settle.large.service.SettleLargeInfoService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 来账管理 /SettleLargeController/
 * @author Lihb
 * 
 */
@Controller
@RequestMapping(value = "/SettleLargeController", method = RequestMethod.POST)
public class SettleLargeController extends CommonController {

	@Autowired
	public SettleLargeInfoService settleLargeInfoService;
	@Autowired
	public SettleLargeInfoMapService settleLargeInfoMapService;
	
	/**
	 * 查询大额支付来账信息
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchSettleLargeInfo")
	@ResponseBody
	public RetMsg<PageInfo<TtSettleLargeInfo>> searchSettleLargeInfo(@RequestBody HashMap<String, Object> paramMap) throws IOException {
		//状态
		String largeStatusStr = ParameterUtil.getString(paramMap, "large_status_search", "");
		if(!StringUtil.isNullOrEmpty(largeStatusStr)){
			paramMap.put("largeStatusList", Arrays.asList(largeStatusStr.split(",")));
		}
		//类型
		String largeTypeStr = ParameterUtil.getString(paramMap, "large_type_search", "");
		if(!StringUtil.isNullOrEmpty(largeTypeStr)){
			paramMap.put("largeTypeList", Arrays.asList(largeTypeStr.split(",")));
		}
		Page<TtSettleLargeInfo> retList = settleLargeInfoService.pageSettleLargeInfos(paramMap);
	    return RetMsgHelper.ok(retList);	
	}
	/**
	 * 新增回款信息
	 * 
	 * @param user - 回款对象
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/addLargeInfo")
	public RetMsg<TtSettleLargeInfo> addLargeInfo(@RequestBody TtSettleLargeInfo largeInfo) throws RException {
		largeInfo.setCreate_user(SlSessionHelper.getUserId());
		settleLargeInfoService.createLargeInfo(largeInfo);
		return RetMsgHelper.ok(largeInfo);
	}
	
	/**
	 * 修改回款
	 * 
	 * @param user - 回款对象
	 * @author lihuabing
	 * @date 2016-7-28
	 */
	@ResponseBody
	@RequestMapping(value = "/updateLargeInfo")
	public RetMsg<Serializable> updateLargeInfo(@RequestBody TtSettleLargeInfo largeInfo) throws RException {
		largeInfo.setUpdate_user(SlSessionHelper.getUserId());
		settleLargeInfoService.updateLargeInfo(largeInfo);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除回款
	 * 
	 * @param userIds[] - 用户ID[]
	 * @author lihuabing
	 * @date 2016-7-28
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLargeInfo")
	public RetMsg<Serializable> deleteLargeInfo(@RequestBody String[] largeIds) throws RException {
		settleLargeInfoService.deleteLargeInfo(largeIds);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除回款
	 * 
	 * @param userIds[] - 用户ID[]
	 * @author lihuabing
	 * @date 2016-7-28
	 */
	@ResponseBody
	@RequestMapping(value = "/sendLargeInfo")
	public RetMsg<Serializable> sendLargeInfo(@RequestBody String[] largeIds) throws RException {
		settleLargeInfoService.sendLargeInfo(largeIds,SlSessionHelper.getUserId());
		return RetMsgHelper.ok();
	}
	/**
	 * 查询大额支付来账映射信息
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchSettleLargeInfoMaps")
	@ResponseBody
	public RetMsg<PageInfo<TtSettleLargeInfoMap>> searchSettleLargeInfoMaps(@RequestBody HashMap<String, Object> paramMap) throws IOException {
		Page<TtSettleLargeInfoMap> retList = settleLargeInfoMapService.pageSettleLargeInfoMaps(paramMap);
	    return RetMsgHelper.ok(retList);	
	}
	/**
	 * 新增回款映射信息
	 * 
	 * @param largeinfoMap - 回款映射对象
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/addLargeInfoMaps")
	public RetMsg<Serializable> addLargeInfoMaps(@RequestBody TtSettleLargeInfoMap largeInfoMap) throws RException {
		settleLargeInfoMapService.createLargeInfoMap(largeInfoMap);
		return RetMsgHelper.ok();
	}
	/**
	 * 新增回款映射信息
	 * 
	 * @param largeinfoMap - 回款映射对象
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/confirmLargeInfo")
	public RetMsg<Serializable> confirmLargeInfo(@RequestBody Map<String, Object> map) throws RException {
		String largeId = ParameterUtil.getString(map, "largeId", "");
		List<TtSettleLargeInfoMap> largeInfoMaps = FastJsonUtil.parseObject(ParameterUtil.getString(map, "largeInfoMaps", ""), new TypeReference<List<TtSettleLargeInfoMap>>() {});
		settleLargeInfoService.confirmLargeInfo(largeId, largeInfoMaps, SlSessionHelper.getUserId());
		return RetMsgHelper.ok();
	}
	/**
	 * 新增回款映射信息
	 * 
	 * @param largeinfoMap - 回款映射对象
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/returnLargeInfos")
	public RetMsg<Serializable> returnLargeInfos(@RequestBody Map<String, Object> map) throws RException {
		String remark = ParameterUtil.getString(map, "remark", "");
		List<String> largeIds = FastJsonUtil.parseObject(ParameterUtil.getString(map, "largeIds", ""), new TypeReference<List<String>>() {});
		settleLargeInfoService.returnLargeInfos(remark, largeIds, SlSessionHelper.getUserId());
		return RetMsgHelper.ok();
	}
	/**
	 * 作废回款信息
	 * 
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/cancelLargeInfos")
	public RetMsg<Serializable> cancelLargeInfos(@RequestBody Map<String, Object> map) throws RException {
		String remark = ParameterUtil.getString(map, "remark", "");
		List<String> largeIds = FastJsonUtil.parseObject(ParameterUtil.getString(map, "largeIds", ""), new TypeReference<List<String>>() {});
		settleLargeInfoService.cancelLargeInfos(remark, largeIds, SlSessionHelper.getUserId());
		return RetMsgHelper.ok();
	}
	
	/**
	 * 同步回款信息
	 * 
	 * @author lihuabing
	 * @date 2016-11-15
	 */
	@ResponseBody
	@RequestMapping(value = "/synLargeInfos")
	public RetMsg<Serializable> synLargeInfos(@RequestBody Map<String, Object> map) throws RException {
		settleLargeInfoService.synLargeInfos(SlSessionHelper.getUserId(),SlSessionHelper.getInstitutionId());
		return RetMsgHelper.ok();
	}
}