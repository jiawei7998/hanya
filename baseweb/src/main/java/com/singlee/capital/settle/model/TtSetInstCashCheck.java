package com.singlee.capital.settle.model;

/**
 * TT_SETTLE_INST_CASH_CHECK      
 * @author   Libo
 * Create Time  2013-01-24 15:20:03
 */
public class TtSetInstCashCheck implements Cloneable{


    /**    */
    private String flow_serialno;

    /**    */
    private String paytrdcode;

    /**    */
    private String bankcode;

    /**    */
    private String delegatedate;

    /**    */
    private String pending_acc_code;

    /**    */
    private String pending_sn;

    /**  0：不需要
1：需要  */
    private String need_sendpay;

    /**    */
    private String hostflowno;

    /**    */
    private String core_pending_sn;

    /**  资金指令编号  */
    private String cash_instr_id;
    
    /** 来账金额 **/
    private double lrcv_amount;
    
    /** 来账手续费 **/
    private double lrcv_fee;
    
    /** 来账匹配方式 **/
    private String lrcv_match_type;
    
    private double lrcv_total_amount; 
    
    /** 付款账号 **/
    private String payAccountNo;
    
    /** 付款用户名 **/
    private String payAccountUserName;
    
    /** 付款行号 **/
    private String payBankNo;
    
    /** 收款账号 **/
    private String receiveAccountNo;
    
    /** 收款用户名 **/
    private String receiveAccountUserName;
    
    /** 收款行号**/
    private String receiveBankNo;
    
    /** 备注**/
    private String remark;


         /**-------  Generate Getter And Setter   --------**/


    public String getFlow_serialno(){
          return this.flow_serialno;
    }

    public void setFlow_serialno(String flow_serialno){
          this.flow_serialno=flow_serialno;
    }

    public String getPaytrdcode(){
          return this.paytrdcode;
    }

    public void setPaytrdcode(String paytrdcode){
          this.paytrdcode=paytrdcode;
    }

    public String getBankcode(){
          return this.bankcode;
    }

    public void setBankcode(String bankcode){
          this.bankcode=bankcode;
    }

    public String getDelegatedate(){
          return this.delegatedate;
    }

    public void setDelegatedate(String delegatedate){
          this.delegatedate=delegatedate;
    }

    public String getPending_acc_code(){
          return this.pending_acc_code;
    }

    public void setPending_acc_code(String pending_acc_code){
          this.pending_acc_code=pending_acc_code;
    }

    public String getPending_sn(){
          return this.pending_sn;
    }

    public void setPending_sn(String pending_sn){
          this.pending_sn=pending_sn;
    }

    public String getNeed_sendpay(){
          return this.need_sendpay;
    }

    public void setNeed_sendpay(String need_sendpay){
          this.need_sendpay=need_sendpay;
    }

    public String getHostflowno(){
          return this.hostflowno;
    }

    public void setHostflowno(String hostflowno){
          this.hostflowno=hostflowno;
    }

    public String getCore_pending_sn(){
          return this.core_pending_sn;
    }

    public void setCore_pending_sn(String core_pending_sn){
          this.core_pending_sn=core_pending_sn;
    }

    public String getCash_instr_id(){
          return this.cash_instr_id;
    }

    public void setCash_instr_id(String cash_instr_id){
          this.cash_instr_id=cash_instr_id;
    }
    
    public double getLrcv_amount() {
		return lrcv_amount;
	}

	public void setLrcv_amount(double lrcv_amount) {
		this.lrcv_amount = lrcv_amount;
	}

	public double getLrcv_fee() {
		return lrcv_fee;
	}

	public void setLrcv_fee(double lrcv_fee) {
		this.lrcv_fee = lrcv_fee;
	}

	public String getLrcv_match_type() {
		return lrcv_match_type;
	}

	public void setLrcv_match_type(String lrcv_match_type) {
		this.lrcv_match_type = lrcv_match_type;
	}

	public double getLrcv_total_amount() {
		return lrcv_total_amount;
	}

	public void setLrcv_total_amount(double lrcv_total_amount) {
		this.lrcv_total_amount = lrcv_total_amount;
	}
	

	public String getPayAccountNo() {
		return payAccountNo;
	}

	public void setPayAccountNo(String payAccountNo) {
		this.payAccountNo = payAccountNo;
	}

	public String getPayAccountUserName() {
		return payAccountUserName;
	}

	public void setPayAccountUserName(String payAccountUserName) {
		this.payAccountUserName = payAccountUserName;
	}

	public String getPayBankNo() {
		return payBankNo;
	}

	public void setPayBankNo(String payBankNo) {
		this.payBankNo = payBankNo;
	}

	public String getReceiveAccountNo() {
		return receiveAccountNo;
	}

	public void setReceiveAccountNo(String receiveAccountNo) {
		this.receiveAccountNo = receiveAccountNo;
	}

	public String getReceiveAccountUserName() {
		return receiveAccountUserName;
	}

	public void setReceiveAccountUserName(String receiveAccountUserName) {
		this.receiveAccountUserName = receiveAccountUserName;
	}

	public String getReceiveBankNo() {
		return receiveBankNo;
	}

	public void setReceiveBankNo(String receiveBankNo) {
		this.receiveBankNo = receiveBankNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSetInstCashCheck [flow_serialno=");
		builder.append(flow_serialno);
		builder.append(", paytrdcode=");
		builder.append(paytrdcode);
		builder.append(", bankcode=");
		builder.append(bankcode);
		builder.append(", delegatedate=");
		builder.append(delegatedate);
		builder.append(", pending_acc_code=");
		builder.append(pending_acc_code);
		builder.append(", pending_sn=");
		builder.append(pending_sn);
		builder.append(", need_sendpay=");
		builder.append(need_sendpay);
		builder.append(", hostflowno=");
		builder.append(hostflowno);
		builder.append(", core_pending_sn=");
		builder.append(core_pending_sn);
		builder.append(", cash_instr_id=");
		builder.append(cash_instr_id);
		builder.append(", lrcv_amount=");
		builder.append(lrcv_amount);
		builder.append(", lrcv_fee=");
		builder.append(lrcv_fee);
		builder.append(", lrcv_match_type=");
		builder.append(lrcv_match_type);
		builder.append(", lrcv_total_amount=");
		builder.append(lrcv_total_amount);
		builder.append(", payAccountNo=");
		builder.append(payAccountNo);
		builder.append(", payAccountUserName=");
		builder.append(payAccountUserName);
		builder.append(", payBankNo=");
		builder.append(payBankNo);
		builder.append(", receiveAccountNo=");
		builder.append(receiveAccountNo);
		builder.append(", receiveAccountUserName=");
		builder.append(receiveAccountUserName);
		builder.append(", receiveBankNo=");
		builder.append(receiveBankNo);
		builder.append(", remark=");
		builder.append(remark);
		builder.append("]");
		return builder.toString();
	}

}