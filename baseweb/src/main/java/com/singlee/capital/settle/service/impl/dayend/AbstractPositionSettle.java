package com.singlee.capital.settle.service.impl.dayend;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.AccInSecuService;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.settle.mapper.SettleInstCashCheckMapper;
import com.singlee.capital.settle.mapper.SettleInstCashMapper;
import com.singlee.capital.settle.mapper.SettleInstSecuMapper;
import com.singlee.capital.settle.mapper.SettleInstructionMapper;
import com.singlee.capital.settle.model.TtSetInstCashCheck;
import com.singlee.capital.settle.pojo.*;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.TradeInterfaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractPositionSettle {
	protected static Logger log = LoggerFactory.getLogger(AbstractPositionSettle.class);
	
	//当前日期
	protected String curDate;
	//持仓头寸
	protected TrdBalanceSecuVo secuBalance;
	//资金头寸
	protected TrdBalanceCashVo cashBalance;
	//金融工具代码
	protected InstrumentId instrumentId;
	//交易
	protected TrdTradeVo trade;
	//本金现金流
	protected TtCashflowCapital principalCashFlow;
	//利息现金流
	protected TtCashflowInterest interestCashFlow;
	//理论日期
	protected String calDate;
	//实际日期
	protected String actDate;
	//指令提示信息
	protected String hint;
	//指令业务类型
	protected String bussType;
	//操作员
	protected String opUserId;
	//操作机构
	protected String opInstId;
	//主指令
	protected SettleInstVo settleInstVo;
	//原始交易指令
	protected SettleInstVo originalSettleInstVo;
	//原始交易资金指令 
	protected List<SettleInstCashVo> originalCashVos;
	//资管计划编号
	protected String assetNo;
	
	
	protected boolean instrCanRefresh = true;
	public boolean isCanSettle = false;
	
	/**
	 * 持仓结算预处理
	 */
	public abstract void preHandler();
	
	public abstract void positionSettle();
	
	public void saveMainInstructionBase(){
		settleInstVo.setInstr_id(getSettleInstDao().getSetInstSequenceId());
		settleInstVo.setCreate_user(opUserId);
		settleInstVo.setCreate_time(DateUtil.getCurrentDateTimeAsString());
		settleInstVo.setSettle_date(actDate);
		settleInstVo.setBus_type(bussType);
		settleInstVo.setStatus(DictConstants.SettleInstState.NEW);
		settleInstVo.setCal_date(calDate);
		settleInstVo.setReserve1(assetNo);
		if(trade != null){
			settleInstVo.setSettle_type(trade.getEnd_settle_type());
			settleInstVo.setSecu_in_accid(trade.getSelf_insecuaccid());
			settleInstVo.setCash_in_accid(trade.getSelf_incashaccid());
			settleInstVo.setSecu_out_accid(trade.getSelf_outsecuaccid());
			settleInstVo.setCash_out_accid(trade.getSelf_outcashaccid());
			settleInstVo.setTradeid(trade.getTrade_id());
			settleInstVo.setTrade_version(trade.getVersion_number());
			settleInstVo.setI_code(trade.getI_code());
			settleInstVo.setA_type(trade.getA_type());
			settleInstVo.setM_type(trade.getM_type());
		}
		settleInstVo.setP_i_code(this.instrumentId.getI_code());
		settleInstVo.setP_a_type(this.instrumentId.getA_type());
		settleInstVo.setP_m_type(this.instrumentId.getM_type());
		settleInstVo.setIs_theory_payment("1");
	}
	
	public void saveSecuInstructionBase(SettleInstSecuVo secuInst){
		//指令公共部分信息
		secuInst.setSecu_instr_id(getSettleInstDao().getSetInstSequenceId());
		secuInst.setCreate_user(opUserId);
		secuInst.setCreate_time(DateUtil.getCurrentDateTimeAsString());
		secuInst.setI_code(this.instrumentId.getI_code());
		secuInst.setA_type(this.instrumentId.getA_type());
		secuInst.setM_type(this.instrumentId.getM_type());
		secuInst.setStatus(DictConstants.SettleSecuInstState.New);
		secuInst.setBeg_date(actDate);
		secuInst.setInstr_id(this.settleInstVo.getInstr_id());
		settleInstVo.setSecu_in_accid(trade == null?"":trade.getSelf_insecuaccid());
		settleInstVo.setSecu_out_accid(trade == null?"":trade.getSelf_outsecuaccid());
		secuInst.setEnd_date("2099-12-31");
		secuInst.setMemo(this.hint);
	}
	
	public void saveCashInstructionBase(SettleInstCashVo cashInst){
		cashInst.setCreate_user(opUserId);
		cashInst.setCreate_time(DateUtil.getCurrentDateTimeAsString());
		cashInst.setCash_instr_id(getSettleInstDao().getSetInstSequenceId());
		cashInst.setParty_chgamount(0);
		cashInst.setStatus(DictConstants.SettleCashInstState.New);
		cashInst.setInstr_id(this.settleInstVo.getInstr_id());
		cashInst.setChg_date(this.settleInstVo.getSettle_date());
		if(trade == null){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("inst_id", settleInstVo.getInst_id());
			map.put("cashacctype", DictConstants.cashAccType.current);
			map.put("acctype", DictConstants.AccOutCashType.Settle);
			map.put("status", DictConstants.AccStatus.Enabled);
			Page<TtAccOutCash> ph = getAccOutCashService().getAccList(map);
			if(ph != null && ph.getTotal() > 0){
				TtAccOutCash accOutCash = ph.get(0);
				cashInst.setSelf_cash_in_accid(settleInstVo.getCash_in_accid());
				cashInst.setSelf_cash_out_accid(accOutCash.getAccid());
				cashInst.setSelf_bankacccode(accOutCash.getBankacc());
				cashInst.setSelf_bankaccname(accOutCash.getAccname());
				cashInst.setSelf_bankcode(accOutCash.getBankLargeCode());
				cashInst.setSelf_bankname(accOutCash.getBankName());
			}
		}
		else if(originalCashVos != null && originalCashVos.size() > 0){//通过原始资金指令获取清算路径
			SettleInstCashVo originalCashVo = originalCashVos.get(0);
			cashInst.setSelf_cash_in_accid(originalCashVo.getSelf_cash_in_accid());
			cashInst.setSelf_cash_out_accid(originalCashVo.getSelf_cash_out_accid());
			cashInst.setSelf_bankacccode(originalCashVo.getSelf_bankacccode());
			cashInst.setSelf_bankaccname(originalCashVo.getSelf_bankaccname());
			cashInst.setSelf_bankcode(originalCashVo.getSelf_bankcode());
			cashInst.setSelf_bankname(originalCashVo.getSelf_bankname());
			cashInst.setParty_bankacccode(originalCashVo.getParty_bankacccode());
			cashInst.setParty_bankaccname(originalCashVo.getParty_bankaccname());
			cashInst.setParty_bankcode(originalCashVo.getParty_bankcode());
			cashInst.setParty_bankname(originalCashVo.getParty_bankname());
		}
		else{
			cashInst.setSelf_cash_in_accid(trade.getSelf_incashaccid());
			cashInst.setSelf_cash_out_accid(trade.getSelf_outcashaccid());
			cashInst.setSelf_bankacccode(trade.getSelf_bankacccode());
			cashInst.setSelf_bankaccname(trade.getSelf_bankaccname());
			cashInst.setSelf_bankcode(trade.getSelf_bankcode());
			cashInst.setSelf_bankname(trade.getSelf_bankname());
			cashInst.setParty_bankacccode(trade.getParty_bankacccode());
			cashInst.setParty_bankaccname(trade.getParty_bankaccname());
			cashInst.setParty_bankcode(trade.getParty_bankcode());
			cashInst.setParty_bankname(trade.getParty_bankname());
		}
		String cashAcctype = getAccOutCashService().getAcc(cashInst.getSelf_cash_out_accid()).getAcctype();
		cashInst.setTransfer_type(DictConstants.AccOutCashType.Settle.equals(cashAcctype)?DictConstants.TransferType.HVPS:DictConstants.TransferType.BillPayment);
	}
		
	public void seveCashInstCheckBase(TtSetInstCashCheck cashCheck){
		cashCheck.setPaytrdcode("");
		cashCheck.setBankcode("");
		cashCheck.setDelegatedate(getDayendDateService().getSettlementDate());
		cashCheck.setPending_acc_code("");
		cashCheck.setPending_sn("");
	}
	protected SettleInstructionMapper getSettleInstDao() {
		return SpringContextHolder.getBean("settleInstructionMapper");
	}
	
	
	protected SettleInstSecuMapper getSettleInstSecuDao() {
		return SpringContextHolder.getBean("settleInstSecuMapper");
	}
	
	protected SettleInstCashMapper getSettleInstCashDao() {
		return SpringContextHolder.getBean("settleInstCashMapper");
	}
	
	protected AccInSecuService getAccInSecuService() {
		return SpringContextHolder.getBean("accInSecuService");
	}
	protected SettleInstCashCheckMapper getSettleInstCashCheckDao(){
		return SpringContextHolder.getBean("settleInstCashCheckMapper");
	}
	protected AccOutCashService getAccOutCashService() {
		return SpringContextHolder.getBean("accOutCashService");
	}
	protected TradeInterfaceService getTradeInterfaceService() {
		return SpringContextHolder.getBean("tradeInterfaceService");
	}
	protected AccInCashService getAccInCashService() {
		return SpringContextHolder.getBean("accInCashService");
	}
	
	public String getCurDate() {
		return curDate;
	}

	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}

	public TrdBalanceSecuVo getSecuBalance() {
		return secuBalance;
	}

	public void setSecuBalance(TrdBalanceSecuVo secuBalance) {
		this.secuBalance = secuBalance;
	}

	public TrdBalanceCashVo getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(TrdBalanceCashVo cashBalance) {
		this.cashBalance = cashBalance;
	}

	public InstrumentId getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(InstrumentId instrumentId) {
		this.instrumentId = instrumentId;
	}

	public TrdTradeVo getTrade() {
		return trade;
	}

	public void setTrade(TrdTradeVo trade) {
		this.trade = trade;
	}

	public TtCashflowCapital getPrincipalCashFlow() {
		return principalCashFlow;
	}

	public void setPrincipalCashFlow(TtCashflowCapital principalCashFlow) {
		this.principalCashFlow = principalCashFlow;
	}

	public TtCashflowInterest getInterestCashFlow() {
		return interestCashFlow;
	}

	public void setInterestCashFlow(TtCashflowInterest interestCashFlow) {
		this.interestCashFlow = interestCashFlow;
	}

	public String getOpUserId() {
		return opUserId;
	}

	public void setOpUserId(String opUserId) {
		this.opUserId = opUserId;
	}

	public String getOpInstId() {
		return opInstId;
	}

	public void setOpInstId(String opInstId) {
		this.opInstId = opInstId;
	}

	public String getCalDate() {
		return calDate;
	}

	public void setCalDate(String calDate) {
		this.calDate = calDate;
	}

	public String getActDate() {
		return actDate;
	}

	public void setActDate(String actDate) {
		this.actDate = actDate;
	}

	protected DayendDateService getDayendDateService(){

		return SpringContextHolder.getBean("dayendDateService");
	}

	public String getAssetNo() {
		return assetNo;
	}

	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
}
