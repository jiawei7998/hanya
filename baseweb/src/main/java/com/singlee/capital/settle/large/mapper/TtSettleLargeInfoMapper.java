package com.singlee.capital.settle.large.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.large.model.TtSettleLargeInfo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 大额来往帐信息对象
 * @author lihuabing
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TtSettleLargeInfoMapper extends Mapper<TtSettleLargeInfo> {
	
	/**
	 * 根据对象查询列表
	 * 
	 * @param user - 用户对象
	 * @param rb - 分页对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TtSettleLargeInfo> pageSettleLargeInfos(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据map参数查询用户列表
	 * @param map
	 * @return
	 */
	List<TtSettleLargeInfo> searchSettleLargeInfo(Map<String, Object> map);
	
}