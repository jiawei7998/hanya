package com.singlee.capital.settle.large.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TT_SETTLE_LARGEINFO_MAP")
public class TtSettleLargeInfoMap implements Serializable {
	private static final long serialVersionUID = 1L;
	private String large_id;
	/** 确认日期 **/
	private String match_date;
	/** 指令编号**/
	private String instr_id;
	/** 确认金额 **/
	private double match_amount;
	/** 结算金额 **/
	private double settle_amount;
	/** 核实单号 **/
	private String trade_id;
	/** 备注 **/
	private String memo;
	public String getLarge_id() {
		return large_id;
	}
	public void setLarge_id(String large_id) {
		this.large_id = large_id;
	}
	public String getMatch_date() {
		return match_date;
	}
	public void setMatch_date(String match_date) {
		this.match_date = match_date;
	}
	public String getInstr_id() {
		return instr_id;
	}
	public void setInstr_id(String instr_id) {
		this.instr_id = instr_id;
	}
	public double getMatch_amount() {
		return match_amount;
	}
	public void setMatch_amount(double match_amount) {
		this.match_amount = match_amount;
	}
	public double getSettle_amount() {
		return settle_amount;
	}
	public void setSettle_amount(double settle_amount) {
		this.settle_amount = settle_amount;
	}
	public String getTrade_id() {
		return trade_id;
	}
	public void setTrade_id(String trade_id) {
		this.trade_id = trade_id;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSettleLargeInfoMap [large_id=");
		builder.append(large_id);
		builder.append(", match_date=");
		builder.append(match_date);
		builder.append(", instr_id=");
		builder.append(instr_id);
		builder.append(", match_amount=");
		builder.append(match_amount);
		builder.append(", settle_amount=");
		builder.append(settle_amount);
		builder.append(", trade_id=");
		builder.append(trade_id);
		builder.append(", memo=");
		builder.append(memo);
		builder.append("]");
		return builder.toString();
	}
}
