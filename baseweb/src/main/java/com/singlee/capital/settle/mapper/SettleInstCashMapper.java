package com.singlee.capital.settle.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.model.TtSetInstCash;
import com.singlee.capital.settle.pojo.SettleInstCashVo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;



/**
 * 结算 资金指令 dao
 * @author LyonChen
 *
 */
public interface SettleInstCashMapper {


	/**
	 * map 参数内部
	 * inst_id 主指令号 允许为空
	 * begin_num 开始位置
	 * end_num 结束位置
	 * @return
	 */
	List<SettleInstCashVo> searchSetInstCash(Map<String, Object> param);
	int countSetInstCash(Map<String, Object> param);

	/**
	 * 获得资金指令序列号
	 * @return
	 */
	String getSetInstCashSequenceId();

	/**
	 * 增加
	 * @param mainSetInst
	 */
	void insertSetInstCash(TtSetInstCash secuSetInsts);

	/**
	 * 修改
	 * @param mainSetInst
	 */
	void updateSetInstCash(TtSetInstCash secuSetInsts);
	/**
	 * 删除
	 * @param mainSetInst
	 */
	void deleteSetInstCash(String inst_cash_id);
	/**
	 * 删除
	 * @param instrId
	 */
	void delCashInstByInstrId(String instrId);
	
	/**
	 * 查询最大的资金划拨流水号
	 * @param settleInstrId  主指令号
	 */
	String selectMaxCashSerialNoByCashInstId(String inst_cash_id);
	/**
	 * 资金指令加锁
	 * @param cashInstrIdList 资金指令ID列表
	 */
	List<SettleInstCashVo> selectForLock(Map<String, Object> map);
	Map<String, Object> selectPaymentCertificateByCashInstrId(String cashInstrId);
	/**
	 * 分页查询结算指令
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SettleInstCashVo> pageSettleInstCashs(Map<String, Object> map, RowBounds rb);
}
