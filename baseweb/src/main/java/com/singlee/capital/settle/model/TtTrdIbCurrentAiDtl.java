package com.singlee.capital.settle.model;

/**
 * TT_TRD_IBCURRENT_AIDTL      活期计息明细表
 * @author   Libo
 * Create Time  2013-05-28 16:09:03
 */
public class TtTrdIbCurrentAiDtl {

	/**  现金流ID  */
	private String cashflowid;

	/**  开始日期  */
	private String begin_date;

	/**  结束日期  */
	private String end_date;

	/**  总额（日积数累计）  */
	private double totalamount;

	/**  利率  */
	private double rate;

	/**  利息金额  */
	private double aiamount;

	/**  清算差别利率档位，其他默认0  */
	private double rate_level;

	/**-------  Generate Getter And Setter   --------**/

	public String getCashflowid() {
		return this.cashflowid;
	}

	public void setCashflowid(String cashflowid) {
		this.cashflowid = cashflowid;
	}

	public String getBegin_date() {
		return this.begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getEnd_date() {
		return this.end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public double getTotalamount() {
		return this.totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public double getRate() {
		return this.rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getAiamount() {
		return this.aiamount;
	}

	public void setAiamount(double aiamount) {
		this.aiamount = aiamount;
	}

	public double getRate_level() {
		return this.rate_level;
	}

	public void setRate_level(double rate_level) {
		this.rate_level = rate_level;
	}

}