package com.singlee.capital.settle.service.impl.flow;

import com.singlee.capital.settle.pojo.SettleData;

public interface ISettleFlow {
	/**
	 * 判断是否允许执行当前流程
	 * @param data 结算数据
	 * @return
	 */
	public boolean isCanExecuteSettleFlow(SettleData data);
	
	/**
	 * 执行当前流程
	 * @param data 结算数据
	 * @return
	 */
	public void executeSettleFlow(SettleData data);
	/**
	 * 判断是否允许执行当前抹账流程
	 * @param data 结算数据
	 * @return
	 */
	public boolean isCanExecuteEarseFlow(SettleData data);
	
	/**
	 * 执行当前抹账流程
	 * @param data 结算数据
	 * @return
	 */
	public void executeEarseFlow(SettleData data);
}
