package com.singlee.capital.settle.pojo;

public class BalanceCheckVo {
	/**外部账户ID**/
	private String outAccId;
	/**内部账户ID**/
	private String inAccId;
	/**业务余额**/
	private double busAmount;
	/**核算余额**/
	private double accountingAmount;
	/**差额**/
	private double diffAmount;
	/**余额类型**/
	private String balanceType;
	
	public String getOutAccId() {
		return outAccId;
	}
	public void setOutAccId(String outAccId) {
		this.outAccId = outAccId;
	}
	public String getInAccId() {
		return inAccId;
	}
	public void setInAccId(String inAccId) {
		this.inAccId = inAccId;
	}
	public double getBusAmount() {
		return busAmount;
	}
	public void setBusAmount(double busAmount) {
		this.busAmount = busAmount;
	}
	public double getAccountingAmount() {
		return accountingAmount;
	}
	public void setAccountingAmount(double accountingAmount) {
		this.accountingAmount = accountingAmount;
	}
	public double getDiffAmount() {
		return diffAmount;
	}
	public void setDiffAmount(double diffAmount) {
		this.diffAmount = diffAmount;
	}
	public String getBalanceType() {
		return balanceType;
	}
	public void setBalanceType(String balanceType) {
		this.balanceType = balanceType;
	}
	
}
