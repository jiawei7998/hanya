package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtTrdBalanceSecu;


public class TrdBalanceSecuVo extends TtTrdBalanceSecu implements Cloneable {
//	/** 账户信息 **/
//	private LimitAccountInfo limitAccountInfo;
//	/** 限额账户信息初始化标识 **/
//	private boolean limitAccountInfoInited;
//	/** 交易对手信息 **/
//	private LimitPartyInfo limitPartyInfo;
//	/** 限额交易对手信息初始化标识 **/
//	private boolean limitPartyInfoInited;
//	/** 金融工具信息 **/
//	private LimitInstrumentInfo limitInstrumentInfo;
//	/** 限额金融工具信息初始化标识 **/
//	private boolean limitInstrumentInfoInited;
//	/** ---------------持仓属性-------------------------**/
//	/**  持仓面额  */
//    @LimitField(fieldName = "持仓面额", limitKind = "1", unit = 1,unitText = "元")
//    private double posAmount;
//	/** 久期 **/
//	@LimitField(fieldName = "久期", limitKind = "1", unit = 1,unitText = "年",weightField = "curMarketValue")
//	private double duration;
//	/** 凸性 **/
//	@LimitField(fieldName = "凸性", limitKind = "1", unit = 1,unitText = "",weightField = "curMarketValue")
//	private double convexity;
//	/** 持仓基点价值 **/
//	@LimitField(fieldName = "持仓基点价值", limitKind = "1", unit = 1,unitText = "万元")
//	private double dvbp;
//	/** 当前市值 **/
//	private double curMarketValue;
	/** 持仓PTYPE **/
	private String pType;
	
	/**关联金融工具信息，请勿复用此字段 **/
	/** 关联金融工具代码 **/
	private String h_i_code;
	/** 关联金融工具资产类型 **/
	private String h_a_type;
	/** 关联金融工具市场类型 **/
	private String h_m_type;
	/** 关联金融工具产品类型**/
	private String h_p_type;
	
	public String getH_i_code() {
		return h_i_code;
	}
	public void setH_i_code(String h_i_code) {
		this.h_i_code = h_i_code;
	}
	public String getH_a_type() {
		return h_a_type;
	}
	public void setH_a_type(String h_a_type) {
		this.h_a_type = h_a_type;
	}
	public String getH_m_type() {
		return h_m_type;
	}
	public void setH_m_type(String h_m_type) {
		this.h_m_type = h_m_type;
	}
	public String getH_p_type() {
		return h_p_type;
	}
	public void setH_p_type(String h_p_type) {
		this.h_p_type = h_p_type;
	}
//	public LimitAccountInfo getLimitAccountInfo() {
//		return limitAccountInfo;
//	}
//	public void setLimitAccountInfo(LimitAccountInfo limitAccountInfo) {
//		this.limitAccountInfo = limitAccountInfo;
//	}
//	public LimitPartyInfo getLimitPartyInfo() {
//		return limitPartyInfo;
//	}
//	public void setLimitPartyInfo(LimitPartyInfo limitPartyInfo) {
//		this.limitPartyInfo = limitPartyInfo;
//	}
//	public LimitInstrumentInfo getLimitInstrumentInfo() {
//		return limitInstrumentInfo;
//	}
//	public void setLimitInstrumentInfo(LimitInstrumentInfo limitInstrumentInfo) {
//		this.limitInstrumentInfo = limitInstrumentInfo;
//	}
//	public double getDuration() {
//		TtMktBondValuing bondValuing = getBondValuing();
//		if(bondValuing != null)
//			this.duration = bondValuing.getMod_duration();
//		if(this.duration == 0){
//			this.duration = getRiskComputeBean().getDuration();
//		}
//		return duration;
//	}
//	public void setDuration(double duration) {
//		this.duration = duration;
//	}
//	public double getConvexity() {
//		TtMktBondValuing bondValuing = getBondValuing();
//		if(bondValuing != null)
//			this.convexity = getBondValuing().getConvexity();
//		if(this.convexity == 0){
//			this.convexity = getRiskComputeBean().getConvexity();
//		}
//		return convexity;
//	}
//	public void setConvexity(double convexity) {
//		this.convexity = convexity;
//	}
//	public double getDvbp() {
//		TtMktBondValuing bondValuing = getBondValuing();
//		if(bondValuing != null)
//			this.dvbp = getBondValuing().getDvbp();
//		if(this.dvbp == 0){
//			this.dvbp = getRiskComputeBean().getDvbp();
//		}
//		return this.dvbp * getR_l_amount();
//	}
//	public void setDvbp(double dvbp) {
//		this.dvbp = dvbp;
//	}
//	
//	public double getPosAmount() {
//		this.posAmount = getR_l_amount() * SettleCommonInfo.getFaceValueByInstrumentId(new InstrumentId(this.getI_code(), getA_type(), getM_type()));
//    	return posAmount;
//	}
//	public void setPosAmount(double posAmount) {
//		this.posAmount = posAmount;
//	}
//	
//	public boolean isLimitAccountInfoInited() {
//		return limitAccountInfoInited;
//	}
//	public void setLimitAccountInfoInited(boolean limitAccountInfoInited) {
//		this.limitAccountInfoInited = limitAccountInfoInited;
//	}
//	public boolean isLimitPartyInfoInited() {
//		return limitPartyInfoInited;
//	}
//	public void setLimitPartyInfoInited(boolean limitPartyInfoInited) {
//		this.limitPartyInfoInited = limitPartyInfoInited;
//	}
//	public boolean isLimitInstrumentInfoInited() {
//		return limitInstrumentInfoInited;
//	}
//	public void setLimitInstrumentInfoInited(boolean limitInstrumentInfoInited) {
//		this.limitInstrumentInfoInited = limitInstrumentInfoInited;
//	}
//	public TtMktBondValuing getBondValuing(){
//		MktBondValuingService mktBondValuingService = SpringContextHolder.getBean("mktBondValuingService");
//		return mktBondValuingService.getMktBondValuing(this.getI_code(), this.getA_type(), this.getM_type(), settlementDate);
//	}
//	public TtMktBond getMktBond(){
//		MktBondService mktBondService = SpringContextHolder.getBean("mktBondService");
//		return mktBondService.getMktBond(this.getI_code(), this.getA_type(), this.getM_type());
//	}
//	public RiskComputeBean getRiskComputeBean(){
//		RiskComputeBean riskComputeBean = new RiskComputeBean();
//		try{
//			TtMktBond bond = getMktBond();
//			if(bond != null){
//				riskComputeBean.setFpml(bond.getFpml());
//				riskComputeBean.setI_code(getI_code());
//				riskComputeBean.setA_type(getA_type());
//				riskComputeBean.setM_type(getM_type());
//				riskComputeBean.setBase_date(settlementDate);
//				ComputeService computeService = SpringContextHolder.getBean("computeService");
//				riskComputeBean = computeService.computeRisk(riskComputeBean);
//			}
//			else{
//				riskComputeBean.setThreoy_price(0);
//				riskComputeBean.setYield(0);
//			}
//		}catch(Exception e){
//			riskComputeBean.setThreoy_price(0);
//			riskComputeBean.setYield(0);
//		}
//		return riskComputeBean;
//	}
//	
//	public double getCurMarketValue() {
//		//获取估值信息
//		TtMktBondValuing bondValuing = getBondValuing();
//		if(bondValuing != null && bondValuing.getFull_price() > 0.0000001){
//			this.curMarketValue = this.getR_l_amount() * bondValuing.getFull_price();
//		}
//		else{
//		//取内部定价
//			this.curMarketValue = this.getR_l_amount() * getRiskComputeBean().getThreoy_price();
//		}
//		return curMarketValue;
//	}
//	public void setCurMarketValue(double curMarketValue) {
//		this.curMarketValue = curMarketValue;
//	}
	
	public String getpType() {
		return pType;
	}
	public void setpType(String pType) {
		this.pType = pType;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrdBalanceSecuVo [pType=");
		builder.append(pType);
		builder.append(", h_i_code=");
		builder.append(h_i_code);
		builder.append(", h_a_type=");
		builder.append(h_a_type);
		builder.append(", h_m_type=");
		builder.append(h_m_type);
		builder.append(", h_p_type=");
		builder.append(h_p_type);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
