package com.singlee.capital.settle.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.settle.mapper.BankMapper;
import com.singlee.capital.settle.model.BankTrade;
import com.singlee.capital.settle.service.BankService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Service
public class BankServiceImpl implements BankService{
	@Autowired
	private BankMapper bankMapper;
	
	@Override
	public Page<BankTrade> queryBankList(@RequestBody Map<String,Object> params){
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<BankTrade> result = bankMapper.queryBankList(params, rowBounds);
		return result;
	}
	
}
