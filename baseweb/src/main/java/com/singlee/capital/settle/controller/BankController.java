package com.singlee.capital.settle.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.settle.model.BankTrade;
import com.singlee.capital.settle.service.BankService;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 银行信息管理
 * @author wzm
 *
 */
@Controller
@RequestMapping(value = "/BankController")
public class BankController extends CommonController{
	/**
	 * 银行查询服务
	 */
	@Autowired
	private BankService bankService;
	
	/**
	 * 查询托管行列表
	 * 
	 * @return 
	 * @author wzm
	 * @date 2016-11-14
	 */
	@ResponseBody
	@RequestMapping(value = "/queryBankList")
	public RetMsg<PageInfo<BankTrade>> queryBankList(@RequestBody Map<String,Object> params){
		String regex = "\\s+";
		String branchname = ParameterUtil.getString(params,"branchname","");
		String trimBname = branchname.trim();
		String RBname = trimBname.replaceAll(regex,"%");
		params.put("branchname", RBname);
		Page<BankTrade> page = bankService.queryBankList(params);
		return RetMsgHelper.ok(page);
	}	
}
