package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtSetInst;


public class SettleInstVo extends TtSetInst {
	/** 内部证券账户名称 **/
	private String accName;
	/** 执行市场 **/
	private String exeMarket;
	/** 中债指令信息 **/
	private SettleZZDInstrInfoVo zzdInstrInfo;
	/** 交易员名称 **/
	private String self_tradername;
	/** 交易日期 **/
	private String trade_date;
	/** 经办员名称 **/
	private String manage_user_name;
	/** 复合员名称 **/
	private String verfy_user_name;
	/** 机构名称 **/
	private String inst_name;
	/** 授权柜员名称 **/
	private String auth_user_name;
	/** 流程任务ID **/
	private String task_id;
	/** 自定义产品类型名称 **/
	private String prd_name;
	/** 产品名称 **/
	private String i_name;
	
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public String getExeMarket() {
		return exeMarket;
	}
	public void setExeMarket(String exeMarket) {
		this.exeMarket = exeMarket;
	}
	public SettleZZDInstrInfoVo getZzdInstrInfo() {
		return zzdInstrInfo;
	}
	public void setZzdInstrInfo(SettleZZDInstrInfoVo zzdInstrInfo) {
		this.zzdInstrInfo = zzdInstrInfo;
	}
	public String getSelf_tradername() {
		return self_tradername;
	}
	public void setSelf_tradername(String self_tradername) {
		this.self_tradername = self_tradername;
	}
	public String getManage_user_name() {
		return manage_user_name;
	}
	public void setManage_user_name(String manage_user_name) {
		this.manage_user_name = manage_user_name;
	}
	public String getVerfy_user_name() {
		return verfy_user_name;
	}
	public void setVerfy_user_name(String verfy_user_name) {
		this.verfy_user_name = verfy_user_name;
	}
	public String getInst_name() {
		return inst_name;
	}
	public void setInst_name(String inst_name) {
		this.inst_name = inst_name;
	}
	public String getTrade_date() {
		return trade_date;
	}
	public void setTrade_date(String trade_date) {
		this.trade_date = trade_date;
	}
	public String getAuth_user_name() {
		return auth_user_name;
	}
	public void setAuth_user_name(String auth_user_name) {
		this.auth_user_name = auth_user_name;
	}
	
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	
	public String getPrd_name() {
		return prd_name;
	}
	public void setPrd_name(String prd_name) {
		this.prd_name = prd_name;
	}
	public String getI_name() {
		return i_name;
	}
	public void setI_name(String i_name) {
		this.i_name = i_name;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SettleInstVo [accName=");
		builder.append(accName);
		builder.append(", exeMarket=");
		builder.append(exeMarket);
		builder.append(", zzdInstrInfo=");
		builder.append(zzdInstrInfo);
		builder.append(", self_tradername=");
		builder.append(self_tradername);
		builder.append(", trade_date=");
		builder.append(trade_date);
		builder.append(", manage_user_name=");
		builder.append(manage_user_name);
		builder.append(", verfy_user_name=");
		builder.append(verfy_user_name);
		builder.append(", inst_name=");
		builder.append(inst_name);
		builder.append(", auth_user_name=");
		builder.append(auth_user_name);
		builder.append(", task_id=");
		builder.append(task_id);
		builder.append(", prd_name=");
		builder.append(prd_name);
		builder.append(", i_name=");
		builder.append(i_name);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
}
