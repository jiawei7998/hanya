package com.singlee.capital.settle.service;

import com.singlee.capital.settle.model.TtTrdAuthLog;

import java.util.List;
import java.util.Map;

public interface TrdAuthLogService {
	/**
	 * 根据条件查询授权信息
	 * @return
	 */
	public List<TtTrdAuthLog> getTrdAuthLogList(Map<String,Object> map);
	
	public TtTrdAuthLog getTrdAuthLogById(String authLogId);
	
	/**
	 * 保存授权信息
	 * @param trdAuthLog 
	 * @return
	 */
	public TtTrdAuthLog addTrdAuthLog(TtTrdAuthLog trdAuthLog);
	
	public void modifyTrdAuthLog(TtTrdAuthLog trdAuthLog);
	
}
