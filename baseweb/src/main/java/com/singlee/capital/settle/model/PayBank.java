package com.singlee.capital.settle.model;

/**
 * TT_TRD_BANK       支付系统行名行号 
 * @author   Libo
 * Create Time  2013-11-06 17:58:44
 */
public class PayBank {

    /**  行号  */
    private String branch;

    /**  清算行号  */
    private String cleanbranch;

    /**  行名  */
    private String branchname;

    /**  简称  */
    private String shortname;

    /**  状态  */
    private String status;

    /**  导入时间  */
    private String impltime;


         /**-------  Generate Getter And Setter   --------**/


    public String getBranch(){
          return this.branch;
    }

    public void setBranch(String branch){
          this.branch=branch;
    }

    public String getCleanbranch(){
          return this.cleanbranch;
    }

    public void setCleanbranch(String cleanbranch){
          this.cleanbranch=cleanbranch;
    }

    public String getBranchname(){
          return this.branchname;
    }

    public void setBranchname(String branchname){
          this.branchname=branchname;
    }

    public String getShortname(){
          return this.shortname;
    }

    public void setShortname(String shortname){
          this.shortname=shortname;
    }

    public String getStatus(){
          return this.status;
    }

    public void setStatus(String status){
          this.status=status;
    }

    public String getImpltime(){
          return this.impltime;
    }

    public void setImpltime(String impltime){
          this.impltime=impltime;
    }


}