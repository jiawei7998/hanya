package com.singlee.capital.settle.service.impl;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.settle.mapper.SettleInstCashCheckMapper;
import com.singlee.capital.settle.model.TtSetInstCashCheck;
import com.singlee.capital.settle.service.SettleInstCashCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("settleInstCashCheckService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SettleInstCashCheckServiceImpl implements
		SettleInstCashCheckService {

	@Autowired
	private SettleInstCashCheckMapper settleInstCashCheckDao;
	@Autowired
	private DayendDateService dayendDateService;
	@Override
	public List<TtSetInstCashCheck> getCashInstCheckByCashInstrId(
			String cashInstrId) {
		if(StringUtil.checkEmptyNull(cashInstrId)) {
			throw new RException("资金指令号不能为空！");}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("cash_instr_id", cashInstrId);
		return settleInstCashCheckDao.searchSetInstCashCheck(map);
	}

	@Override
	public List<TtSetInstCashCheck> getCashInstCheckList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return settleInstCashCheckDao.searchSetInstCashCheck(map);
	}

	@Override
	public TtSetInstCashCheck getCashInstCheckByCheckId(
			String cashInstCheckId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TtSetInstCashCheck addCashInstCheckInfo(
			TtSetInstCashCheck cashInstCheck) {
		// TODO Auto-generated method stub
		settleInstCashCheckDao.insertSetInstCashCheck(cashInstCheck);
		return null;
	}

	@Override
	public void modifyCashInstCheck(TtSetInstCashCheck cashInstCheck) {
		// TODO Auto-generated method stub
		settleInstCashCheckDao.updateSetInstCashCheck(cashInstCheck);
	}

	@Override
	public void delCashInstCheckByCashInstrId(String cashInstId) {
		JY.require(!StringUtil.isNullOrEmpty(cashInstId), "资金指令号不能为空!");
		settleInstCashCheckDao.deleteSetInstCashCheck(cashInstId);
	}

	/**
	 * 根据主指令号删除资金流水单
	 * @param instrId   主指令ID
	 */
	@Override
	public void delInstCashCheckByInstrId(String instrId){
		if(StringUtil.checkEmptyNull(instrId)) {
			return;}
		settleInstCashCheckDao.delInstCashCheckByInstrId(instrId);
	}

	@Override
	public String getLarteRcvStatusByHostFlowNo(String hostFlowNo,double largeRcvAmount) {
		
		return "";
	}
	@Override
	public double getLarteRcvMatchedAmountByHostFlowNo(String hostFlowNo) {
		// TODO Auto-generated method stub
		double matchedAmount = 0;
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("hostflowno", hostFlowNo);
		map.put("delegatedate", dayendDateService.getSettlementDate());
		List<TtSetInstCashCheck> checkList = getCashInstCheckList(map);
		if(checkList != null && checkList.size() > 0){
			for(TtSetInstCashCheck item : checkList){
				//SettleInstCashVo cashInst = settleInstCashService.getCashInstByCashInstId(item.getCash_instr_id());
				matchedAmount += item.getLrcv_amount();
			}
		}
		return matchedAmount;
	}
}
