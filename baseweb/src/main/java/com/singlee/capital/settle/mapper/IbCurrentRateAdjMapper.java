package com.singlee.capital.settle.mapper;

import com.singlee.capital.settle.model.TtTrdIbCurrentRateAdj;
import com.singlee.capital.settle.pojo.DepositCurrentRateAdjVo;

import java.util.List;
import java.util.Map;


/**
 * TtTrdIbCurrentRateAdj      同业活期利率调整表
 * @author   Libo
 * Create Time  2013-08-30 11:44:25
 */

public interface IbCurrentRateAdjMapper {

  /**
   * 查询  
   * @param map
   * map参数：
   *      p_type_search 产品分类--存放同业，同业存放  【必输】
   *      approve_status 状态  【必输】
   *      createuser 创建者  【必输】
   */
  public List<DepositCurrentRateAdjVo>  select(Map<String,Object> map);

  /** 记录数，参数同select */
  public int count(Map<String,Object> map);

//  /** 最大日期的调整记录  */
//  public TtTrdIbCurrentRateAdj max(TtTrdIbCurrentRateAdj obj);
//  
  /** 最大日期 审批通过的 记录 */
  public TtTrdIbCurrentRateAdj maxApprovePass (TtTrdIbCurrentRateAdj obj);

  /** 当前日期 审批通过的 记录 */
  public TtTrdIbCurrentRateAdj currentApprovePass (Map<String,String> obj);
   
  
  /**   */
  public DepositCurrentRateAdjVo selectOne(String id);
  
  /** 新增 */
  public int insert(TtTrdIbCurrentRateAdj obj);

  /** 修改 */
  public int update(TtTrdIbCurrentRateAdj obj);

  /** 删除 */
  public int deleteFromDate(TtTrdIbCurrentRateAdj obj);
  
  /** 删除 */
  public void delete(String id);

}