package com.singlee.capital.settle.large.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.settle.large.mapper.TtSettleLargeInfoMapMapper;
import com.singlee.capital.settle.large.model.TtSettleLargeInfoMap;
import com.singlee.capital.settle.large.service.SettleLargeInfoMapService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 大额来往账映射service实现
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
@Service("settleLargeInfoMapService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SettleLargeInfoMapServiceImpl implements SettleLargeInfoMapService {
	
	/* 用户mapper层  */
	@Autowired
	private TtSettleLargeInfoMapMapper ttSettleLargeInfoMapMapper;
	
	@Override
	public List<TtSettleLargeInfoMap> searchSettleLargeInfoMaps(Map<String, Object> params) {
		List<TtSettleLargeInfoMap> result = ttSettleLargeInfoMapMapper.searchSettleLargeInfoMap(params);
		return result;
	}
	
	@Override
	public Page<TtSettleLargeInfoMap> pageSettleLargeInfoMaps(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TtSettleLargeInfoMap> result = ttSettleLargeInfoMapMapper.pageSettleLargeInfoMaps(params, rowBounds);
		return result;
	}
	
	@Override
	@AutoLogMethod(value="创建大额来往帐映射")
	public void createLargeInfoMap(TtSettleLargeInfoMap largeInfoMap) throws RException {
		ttSettleLargeInfoMapMapper.insert(largeInfoMap);
	}
	
	@Override
	public void createLargeInfoMaps(String largeId,List<TtSettleLargeInfoMap> largeInfoMaps) {
		//先删除所有的map
		ttSettleLargeInfoMapMapper.deleteSettleLargeInfoMapById(largeId);
		//再新增map
		if(largeInfoMaps != null && largeInfoMaps.size() > 0){
			for(TtSettleLargeInfoMap largeInfoMap : largeInfoMaps){
				ttSettleLargeInfoMapMapper.insert(largeInfoMap);
			}
		}
	}

	@Override
	public void updateLargeInfoMap(TtSettleLargeInfoMap largeInfo) {
		TtSettleLargeInfoMap u = ttSettleLargeInfoMapMapper.selectByPrimaryKey(largeInfo);
		if (u == null) {
			JY.raiseRException("查不到对应的大额来往账映射信息");
		}
		ttSettleLargeInfoMapMapper.updateSettleLargeInfoMapById(largeInfo);
	}

	@Override
	public void deleteLargeInfoMap(String[] largeIds) {
		Map<String, Object> map = new HashMap<String, Object>();
		for(String largeId : largeIds){
			map.put("large_id", largeId);
			List<TtSettleLargeInfoMap> u = ttSettleLargeInfoMapMapper.searchSettleLargeInfoMap(map);
			if (u == null || u.size() == 0) {
				JY.raiseRException("查不到对应的大额来往账映射信息");
			}
			ttSettleLargeInfoMapMapper.deleteSettleLargeInfoMapById(largeId);
		}
	}

}
