package com.singlee.capital.settle.service.impl;

import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.service.AccInSecuService;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.market.pojo.IBond;
import com.singlee.capital.market.service.MktBondService;
import com.singlee.capital.system.dict.DictConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * 结算公共配置类
 * 
 * @author lihb
 */
public class SettleCommonInfo {
	// 不需要资金结算的业务
	public static final List<String> NoCashSettleBusType = new ArrayList<String>();
	// 中债业务
	public static final List<String> ZZBusType = new ArrayList<String>();
	// 同业活期类业务
	public static final List<String> IBCurrentBusType = new ArrayList<String>();
	// 同业定期业务
	public static final List<String> IBFixedBusType = new ArrayList<String>();
	// 核算支持券指令类型
	public static final List<String> AccountingSupportSecuInstrType = new ArrayList<String>();
	// 核算可允许不处理的券指令类型
	public static final List<String> AccountingCanSkipSecuInstrType = new ArrayList<String>();
	// 核算支持资金指令类型
	public static final List<String> AccountingSupportCashInstrType = new ArrayList<String>();
	// 核算可允许不处理的资金指令类型
	public static final List<String> AccountingCanSkipCashInstrType = new ArrayList<String>();
	// 非前台发起交易业务类型
	public static final List<String> NoTradeSettleBusType = new ArrayList<String>();
	// 付款业务类型
	public static final List<String> PaymentSettleBusType = new ArrayList<String>();
	// 自动转存业务类型
	public static final List<String> AutoReDepositBusType = new ArrayList<String>();
	// 非合约类资产
	public static final List<String> NoContractPosition = new ArrayList<String>();
	// 需要前置抹账检查的业务
	public static final List<String> NeedEarsePreCheckBusType = new ArrayList<String>();
	// 非合约、交易类业务类型
	public static final List<String> NoTradePosBusType = new ArrayList<String>();
	//可进行指令延期的业务类型
	public static final List<String> SettleDelayBusType = new ArrayList<String>();
	// 同业定期手工业务类型
	public static final List<String> DepositToBankFixedManualBusType = new ArrayList<String>();
	// 同业活期手工业务类型
	public static final List<String> CurrentToBankManualBusType = new ArrayList<String>();
    //投资理财
	public static final List<String> AssetPlanMoneyManualBusType = new ArrayList<String>();
	//场内业务
	public static final List<String> BankInBusType = new ArrayList<String>();
	//大额支付业务
	public static final List<String> HvpsBusType = new ArrayList<String>();
	//需要后台清算业务
	public static final List<String> SettleBusType = new ArrayList<String>();
	//理财资管产品类业务
	public static final List<String> AmMoneyProdBusType = new ArrayList<String>();
	//理财资管产品前台付款类业务--主动管理
	public static final List<String> AmFrontPaymentSettleBusType = new ArrayList<String>();
	//理财资管产品前台收款类业务--主动管理
	public static final List<String> AmFrontRcvSettleBusType = new ArrayList<String>();
	//理财资管产品后台付款类业务--主动管理
	public static final List<String> AmBackPaymentSettleBusType = new ArrayList<String>();
	//理财资管产品后台收款类业务--主动管理
	public static final List<String> AmBackRcvSettleBusType = new ArrayList<String>();
	static {
		// 不需要资金结算的业务
		NoCashSettleBusType.add(DictConstants.TrdType.DepositToBankFixedTransfer);
		NoCashSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_INTEDIFF);
		NoCashSettleBusType.add(DictConstants.TrdType.CurrentToBankRcv_INTEDIFF);
		NoCashSettleBusType.add(DictConstants.TrdType.Assetplan_MANUALTRANS);
		// NoCashSettleBusType.add(DictConstants.TrdType.ManualBooking);
		// 核算支持券指令类型
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.IN);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.OUT);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.FST);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.END);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.INTE_IN);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.INTE_OUT);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.INTE_DIFF);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.EXTEND_TERM);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.PRE_TERMINAL);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.OVER_TERM);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.BUY);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.SELL);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.MIDDLE_IN);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.ACCOUNTING_CANCEL);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.PURCHASE);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.SUBSCRIBE);
		AccountingSupportSecuInstrType.add(DictConstants.SettleSecuInstBussType.REDEEM);

		// 核算可允许不处理的券指令类型
		AccountingCanSkipSecuInstrType.add(DictConstants.TrdType.BondFreeze);
		AccountingCanSkipSecuInstrType.add(DictConstants.TrdType.BondUnfreeze);
		// 核算支持资金指令类型
		AccountingSupportCashInstrType.add(DictConstants.SettleCashInstBussType.PAY);
		AccountingSupportCashInstrType.add(DictConstants.SettleCashInstBussType.RECEIVE);

		// 非前台发起交易业务类型
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_OVEREND);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_PREEND);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_RCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_MANUALRCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_INTEDIFF);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_END);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_AUTODEPO);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositFromBankFixed_END);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositFromBankFixed_PAYINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositFromBankFixed_AUTODEPO);
		NoTradeSettleBusType.add(DictConstants.TrdType.SellRepoPledged_END);
		NoTradeSettleBusType.add(DictConstants.TrdType.ReverseRepoPledged_END);
		NoTradeSettleBusType.add(DictConstants.TrdType.DepositFromBankFixed_AUTODEPO);
		NoTradeSettleBusType.add(DictConstants.TrdType.Bond_RCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.UnderLyingRcvInte);
		NoTradeSettleBusType.add(DictConstants.TrdType.Bond_END);
		NoTradeSettleBusType.add(DictConstants.TrdType.CurrentFromBankPayInte);
		NoTradeSettleBusType.add(DictConstants.TrdType.CurrentToBankRcvInte);
		NoTradeSettleBusType.add(DictConstants.TrdType.CurrentToBankManualRcvInte);
		NoTradeSettleBusType.add(DictConstants.TrdType.CurrentToBankRcv_INTEDIFF);
		NoTradeSettleBusType.add(DictConstants.TrdType.ManualBooking);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_MANUALRCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_RCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.MoneyEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_PREEND);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_OVEREND);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_MANUALRCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_RCVINTE);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlanEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_PREEND);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_OVEREND);
		NoTradeSettleBusType.add(DictConstants.TrdType.Assetplan_MANUALTRANS);
		NoTradeSettleBusType.add(DictConstants.TrdType.IbBorrowingEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.IbLendingEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.IBCDIssueEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.SellRepoOutrightEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.ReverseRepoOutrightEnd);

		NoTradeSettleBusType.add(DictConstants.TrdType.ChargesIn);
		NoTradeSettleBusType.add(DictConstants.TrdType.ChargesOut);
		
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_OVEREND);
		NoTradeSettleBusType.add(DictConstants.TrdType.AssetPlan_PREEND);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_OVEREND);
		NoTradeSettleBusType.add(DictConstants.TrdType.Money_PREEND);
		
		NoTradeSettleBusType.add(DictConstants.TrdType.CustomEnd);
		NoTradeSettleBusType.add(DictConstants.TrdType.CustomAdvanceMaturity);
		NoTradeSettleBusType.add(DictConstants.TrdType.CustomMultiResale);
		NoTradeSettleBusType.add(DictConstants.TrdType.AccountTradeOffset);
		NoTradeSettleBusType.add(DictConstants.TrdType.CustomCashFlowChange);
		NoTradeSettleBusType.add(DictConstants.TrdType.CustomRcvInte);
		

		// 非合约、交易类业务类型
		NoTradePosBusType.add(DictConstants.TrdType.Bond_RCVINTE);
		NoTradePosBusType.add(DictConstants.TrdType.Bond_END);
		NoTradePosBusType.add(DictConstants.TrdType.CurrentFromBankPayInte);
		NoTradePosBusType.add(DictConstants.TrdType.CurrentToBankRcvInte);
		NoTradePosBusType.add(DictConstants.TrdType.CurrentToBankManualRcvInte);
		NoTradePosBusType.add(DictConstants.TrdType.CurrentToBankRcv_INTEDIFF);
		NoTradePosBusType.add(DictConstants.TrdType.ManualBooking);

		NoTradePosBusType.add(DictConstants.TrdType.Money_MANUALRCVINTE);
		NoTradePosBusType.add(DictConstants.TrdType.AssetPlan_MANUALRCVINTE);
		NoTradePosBusType.add(DictConstants.TrdType.Assetplan_MANUALTRANS);

		// 活期业务类型
		IBCurrentBusType.add(DictConstants.TrdType.CurrentFromBankIn);
		IBCurrentBusType.add(DictConstants.TrdType.CurrentFromBankPayInte);
		IBCurrentBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		IBCurrentBusType.add(DictConstants.TrdType.CurrentToBankBack);
		IBCurrentBusType.add(DictConstants.TrdType.CurrentToBankAccOff);
		IBCurrentBusType.add(DictConstants.TrdType.CurrentToBankRcvInte);

		// 定期业务

		// 付款业务
		PaymentSettleBusType.add(DictConstants.TrdType.CurrentFromBankPayInte);
		PaymentSettleBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		PaymentSettleBusType.add(DictConstants.TrdType.BondBuy);
		PaymentSettleBusType.add(DictConstants.TrdType.DepositToBankFixed);
		PaymentSettleBusType.add(DictConstants.TrdType.SellRepoPledged_END);
		PaymentSettleBusType.add(DictConstants.TrdType.ReverseRepoPledged);
		PaymentSettleBusType.add(DictConstants.TrdType.MoneyBuy);
		PaymentSettleBusType.add(DictConstants.TrdType.AssetPlanBuy);
		PaymentSettleBusType.add(DictConstants.TrdType.IbLending);
		PaymentSettleBusType.add(DictConstants.TrdType.ReverseRepoOutright);
		PaymentSettleBusType.add(DictConstants.TrdType.DistributeBuy);

		// 自动转存业务类型
		AutoReDepositBusType.add(DictConstants.TrdType.DepositFromBankFixed_AUTODEPO);
		AutoReDepositBusType.add(DictConstants.TrdType.DepositToBankFixed_AUTODEPO);

		// 非合约类资产
		NoContractPosition.add(DictConstants.AType.Bond);
		NoContractPosition.add(DictConstants.AType.BondAbs);
		NoContractPosition.add(DictConstants.AType.Money);

		// 需前置抹账检查的业务
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.BondBuy);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.BondSell);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.SellRepoPledged_END);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.BondUnfreeze);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.DepositToBankFixed_END);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.CurrentToBankManualRcvInte);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.CurrentToBankRcv_INTEDIFF);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.CurrentToBankRcvInte);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.CurrentToBankBack);
		NeedEarsePreCheckBusType.add(DictConstants.TrdType.DistributeBuy);
		
		//可进行指令延期的业务类型
		SettleDelayBusType.add(DictConstants.TrdType.Bond_END);
		SettleDelayBusType.add(DictConstants.TrdType.Bond_RCVINTE);
		SettleDelayBusType.add(DictConstants.TrdType.ReverseRepoPledged_END);
		SettleDelayBusType.add(DictConstants.TrdType.SellRepoPledged_END);
		SettleDelayBusType.add(DictConstants.TrdType.SellRepoOutrightEnd);
		SettleDelayBusType.add(DictConstants.TrdType.ReverseRepoOutrightEnd);
		SettleDelayBusType.add(DictConstants.TrdType.IbBorrowingEnd);
		SettleDelayBusType.add(DictConstants.TrdType.IbLendingEnd);
		SettleDelayBusType.add(DictConstants.TrdType.UnderLyingRcvInte);
		SettleDelayBusType.add(DictConstants.TrdType.MoneyEnd);
		SettleDelayBusType.add(DictConstants.TrdType.Money_RCVINTE);
		SettleDelayBusType.add(DictConstants.TrdType.AssetPlanEnd);
		SettleDelayBusType.add(DictConstants.TrdType.AssetPlan_RCVINTE);
		
		//同业定期
		DepositToBankFixedManualBusType.add(DictConstants.TrdType.DepositToBankFixed_PREEND);
		DepositToBankFixedManualBusType.add(DictConstants.TrdType.DepositToBankFixed_OVEREND);
		DepositToBankFixedManualBusType.add(DictConstants.TrdType.DepositToBankFixed_INTEDIFF);
		DepositToBankFixedManualBusType.add(DictConstants.TrdType.DepositToBankFixed_MANUALRCVINTE);
		//同业活期
		CurrentToBankManualBusType.add(DictConstants.TrdType.CurrentToBankManualRcvInte);
		CurrentToBankManualBusType.add(DictConstants.TrdType.CurrentToBankRcv_INTEDIFF);
        //理财投资
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.Money_MANUALRCVINTE);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.AssetPlan_MANUALRCVINTE);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.Assetplan_MANUALTRANS);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.Money_OVEREND);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.Money_PREEND);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.AssetPlan_OVEREND);
		AssetPlanMoneyManualBusType.add(DictConstants.TrdType.AssetPlan_PREEND);
		
		//场内业务
		BankInBusType.add(DictConstants.TrdType.BondBuy);
		BankInBusType.add(DictConstants.TrdType.BondSell);
		BankInBusType.add(DictConstants.TrdType.SellRepoPledged);
		BankInBusType.add(DictConstants.TrdType.ReverseRepoPledged);
		BankInBusType.add(DictConstants.TrdType.IbBorrowing);
		BankInBusType.add(DictConstants.TrdType.IbLending);
		BankInBusType.add(DictConstants.TrdType.SellRepoOutright);
		BankInBusType.add(DictConstants.TrdType.ReverseRepoOutright);
		
		//大额支付业务
		HvpsBusType.add(DictConstants.TrdType.DepositToBankFixed);
		HvpsBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		HvpsBusType.add(DictConstants.TrdType.MoneyBuy);
		HvpsBusType.add(DictConstants.TrdType.AssetPlanBuy);
		HvpsBusType.add(DictConstants.TrdType.DistributeBuy);
		HvpsBusType.add(DictConstants.TrdType.IbBorrowingEnd); 
		HvpsBusType.add(DictConstants.TrdType.IbLending);
		
		SettleBusType.add(DictConstants.TrdType.CustomAdvanceMaturity);
		SettleBusType.add(DictConstants.TrdType.Custom);
		SettleBusType.add(DictConstants.TrdType.AccountTradeOffset);
		SettleBusType.add(DictConstants.TrdType.CustomMultiResale);
		SettleBusType.add(DictConstants.TrdType.BondBuy);
		SettleBusType.add(DictConstants.TrdType.BondSell);
		SettleBusType.add(DictConstants.TrdType.SellRepoPledged);
		SettleBusType.add(DictConstants.TrdType.ReverseRepoPledged);
		SettleBusType.add(DictConstants.TrdType.SellRepoOutright);
		SettleBusType.add(DictConstants.TrdType.ReverseRepoOutright);
		SettleBusType.add(DictConstants.TrdType.IbBorrowing);
		SettleBusType.add(DictConstants.TrdType.IbLending);
		SettleBusType.add(DictConstants.TrdType.DistributeBuy);
		SettleBusType.add(DictConstants.TrdType.DepositToBankFixed);
		SettleBusType.add(DictConstants.TrdType.DepositToBankFixedTransfer);
		SettleBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		SettleBusType.add(DictConstants.TrdType.CurrentToBankBack);
		SettleBusType.add(DictConstants.TrdType.CurrentToBankAccOff);
		SettleBusType.add(DictConstants.TrdType.AssetPlanSell);
		SettleBusType.add(DictConstants.TrdType.AssetPlanBuy);
		SettleBusType.add(DictConstants.TrdType.MoneyProdPurchase);
		SettleBusType.add(DictConstants.TrdType.MoneyProdSubscribe);
		SettleBusType.add(DictConstants.TrdType.MoneyProdPayInte);
		SettleBusType.add(DictConstants.TrdType.MoneyProdRedeem);
		SettleBusType.add(DictConstants.TrdType.MoneyProdPreTerminate);
		SettleBusType.add(DictConstants.TrdType.EntrustAssetPlan);
		SettleBusType.add(DictConstants.TrdType.EntrustAssetPlanConfig);
		SettleBusType.add(DictConstants.TrdType.MoneyProdTransfer);
		SettleBusType.add(DictConstants.TrdType.BenefitManage);
		
		//资管产品类业务
		AmMoneyProdBusType.add(DictConstants.TrdType.MoneyProdSubscribe);
		AmMoneyProdBusType.add(DictConstants.TrdType.MoneyProdPurchase);
		AmMoneyProdBusType.add(DictConstants.TrdType.MoneyProdRedeem);
		AmMoneyProdBusType.add(DictConstants.TrdType.BenefitManage);
		
		//资管前台付款类业务
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.CurrentFromBankPayInte);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.CurrentToBankTransfer);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.BondBuy);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.DepositToBankFixed);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.ReverseRepoPledged);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.MoneyBuy);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.AssetPlanBuy);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.IbLending);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.ReverseRepoOutright);
		AmFrontPaymentSettleBusType.add(DictConstants.TrdType.DistributeBuy);
		//资管前台收款类业务
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.CurrentToBankAccOff);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.CurrentToBankBack);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.BondSell);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.AssetPlanSell);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.SellRepoOutright);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.SellRepoPledged);
		AmFrontRcvSettleBusType.add(DictConstants.TrdType.IbBorrowing);
		//资管后台收款类业务
		AmBackRcvSettleBusType.add(DictConstants.TrdType.Bond_END);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.Bond_RCVINTE);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.ReverseRepoOutrightEnd);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.ReverseRepoPledged_END);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.IbLendingEnd);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_END);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_RCVINTE);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_MANUALRCVINTE);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_PREEND);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.DepositToBankFixed_OVEREND);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.CurrentToBankManualRcvInte);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.CurrentToBankRcvInte);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.AssetPlanEnd);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.AssetPlan_RCVINTE);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.AssetPlan_MANUALRCVINTE);
		AmBackRcvSettleBusType.add(DictConstants.TrdType.AssetPlan_PREEND);
		//资管后台付款类业务
		AmBackPaymentSettleBusType.add(DictConstants.TrdType.SellRepoPledged_END);
		AmBackPaymentSettleBusType.add(DictConstants.TrdType.SellRepoOutrightEnd);
		AmBackPaymentSettleBusType.add(DictConstants.TrdType.IbBorrowingEnd);
	}

	// 持仓结算

	// 金融工具票面
	public static double getFaceValueByInstrumentId(InstrumentId instrumentId) {
		double faceValue = 0;
		if (instrumentId.getA_type().equals(DictConstants.AType.Bond) || instrumentId.getA_type().equals(DictConstants.AType.BondAbs)) {
			MktBondService mktBondService = SpringContextHolder.getBean("mktBondService");
			IBond bond = mktBondService.getMktIBond(instrumentId);
			if (bond != null) {
				faceValue = bond.getB_face_value();}
			else {
				faceValue = 100;}
		} else {
			faceValue = 1;
		}
		return faceValue;
	}

	public static String getSecuKJAccIdBySecuAccId(String secuInAccId) {
		AccInSecuService accInSecuService = SpringContextHolder.getBean("accInSecuService");
		TtAccInSecu accInSecu = accInSecuService.getAcc(secuInAccId);
		if (accInSecu != null) {
			return accInSecu.getBookkeepAccid();}
		else {
			return "";}
	}
}
