package com.singlee.capital.settle.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TT_SETTLE_INST_CASH      
 * @author   Libo
 * Create Time  2013-01-24 15:20:03
 */
@Entity
@Table(name = "TT_SETTLE_INST_CASH")
public class TtSetInstCash implements Cloneable{


    /**  资金指令序号  */
	@Id
    private String cash_instr_id;

    /**  主指令序号  */
    private String instr_id;

    /**  1:付款
2:收款  */
    private String buss_type;

    /**  资金指令状态  */
    private String status;

    /**  资金指令合并序号  */
    private String p_cash_instrid;

    /**  0:大额支付 1:票据支付 2:系统内转账 3:DVP  */
    private String transfer_type;

    /**  币种  */
    private String cny;

    /**  0-现金流未确定，1-现金流已确定  */
    private String is_cashflow_fixed;

    /**  变动金额  */
    private double chg_amount;

    /**  变动数量正负标记  */
    private double chgamount_io;

    /**  本方计算变动数量  */
    private double self_chgamount;

    /**  外部资金流水号  */
    private String ext_cahsserialno;

    /**  本方银行帐号   */
    private String self_bankacccode;

    /**  本方账户名称   */
    private String self_bankaccname;

    /**  本方开户行行号   */
    private String self_bankcode;

    /**  本方开户行名称   */
    private String self_bankname;

    /**  本方内部资金账户  */
    private String self_cash_in_accid;

    /**  本方外部资金账户  */
    private String self_cash_out_accid;

    /**  对手方计算变动数量  */
    private double party_chgamount;

    /**  对手方银行帐号   */
    private String party_bankacccode;

    /**  对手方账户名称   */
    private String party_bankaccname;

    /**  对手方开户行行号   */
    private String party_bankcode;

    /**  对手方开户行名称   */
    private String party_bankname;

    /**  对手方内部资金账户  */
    private String party_cash_in_accid;

    /**  对手方外部资金账户  */
    private String party_cash_out_accid;

    /**  资金划拨单号  */
    private String cash_transfer_no;

    /**  复核人  */
    private String accounting_user;

    /**  复核时间  */
    private String accounting_time;

    /**    */
    private String memo;

    /**  创建时间  */
    private String create_time;

    /**  创建人  */
    private String create_user;

    /**  最后修改时间  */
    private String update_time;

    /**  最后修改人  */
    private String update_user;

    /**  变动日期  */
    private String chg_date;

         /**-------  Generate Getter And Setter   --------**/


    public String getCash_instr_id(){
          return this.cash_instr_id;
    }

    public void setCash_instr_id(String cash_instr_id){
          this.cash_instr_id=cash_instr_id;
    }

    public String getInstr_id(){
          return this.instr_id;
    }

    public void setInstr_id(String instr_id){
          this.instr_id=instr_id;
    }

    public String getBuss_type(){
          return this.buss_type;
    }

    public void setBuss_type(String buss_type){
          this.buss_type=buss_type;
    }

    public String getStatus(){
          return this.status;
    }

    public void setStatus(String status){
          this.status=status;
    }

    public String getP_cash_instrid(){
          return this.p_cash_instrid;
    }

    public void setP_cash_instrid(String p_cash_instrid){
          this.p_cash_instrid=p_cash_instrid;
    }

    public String getTransfer_type(){
          return this.transfer_type;
    }

    public void setTransfer_type(String transfer_type){
          this.transfer_type=transfer_type;
    }

    public String getCny(){
          return this.cny;
    }

    public void setCny(String cny){
          this.cny=cny;
    }

    public String getIs_cashflow_fixed(){
          return this.is_cashflow_fixed;
    }

    public void setIs_cashflow_fixed(String is_cashflow_fixed){
          this.is_cashflow_fixed=is_cashflow_fixed;
    }

    public double getChg_amount(){
          return this.chg_amount;
    }

    public void setChg_amount(double chg_amount){
          this.chg_amount=chg_amount;
    }

    public double getChgamount_io(){
          return this.chgamount_io;
    }

    public void setChgamount_io(double chgamount_io){
          this.chgamount_io=chgamount_io;
    }

    public double getSelf_chgamount(){
          return this.self_chgamount;
    }

    public void setSelf_chgamount(double self_chgamount){
          this.self_chgamount=self_chgamount;
    }

    public String getExt_cahsserialno(){
          return this.ext_cahsserialno;
    }

    public void setExt_cahsserialno(String ext_cahsserialno){
          this.ext_cahsserialno=ext_cahsserialno;
    }

    public String getSelf_bankacccode(){
          return this.self_bankacccode;
    }

    public void setSelf_bankacccode(String self_bankacccode){
          this.self_bankacccode=self_bankacccode;
    }

    public String getSelf_bankaccname(){
          return this.self_bankaccname;
    }

    public void setSelf_bankaccname(String self_bankaccname){
          this.self_bankaccname=self_bankaccname;
    }

    public String getSelf_bankcode(){
          return this.self_bankcode;
    }

    public void setSelf_bankcode(String self_bankcode){
          this.self_bankcode=self_bankcode;
    }

    public String getSelf_bankname(){
          return this.self_bankname;
    }

    public void setSelf_bankname(String self_bankname){
          this.self_bankname=self_bankname;
    }

    public String getSelf_cash_in_accid(){
          return this.self_cash_in_accid;
    }

    public void setSelf_cash_in_accid(String self_cash_in_accid){
          this.self_cash_in_accid=self_cash_in_accid;
    }

    public String getSelf_cash_out_accid(){
          return this.self_cash_out_accid;
    }

    public void setSelf_cash_out_accid(String self_cash_out_accid){
          this.self_cash_out_accid=self_cash_out_accid;
    }

    public double getParty_chgamount(){
          return this.party_chgamount;
    }

    public void setParty_chgamount(double party_chgamount){
          this.party_chgamount=party_chgamount;
    }

    public String getParty_bankacccode(){
          return this.party_bankacccode;
    }

    public void setParty_bankacccode(String party_bankacccode){
          this.party_bankacccode=party_bankacccode;
    }

    public String getParty_bankaccname(){
          return this.party_bankaccname;
    }

    public void setParty_bankaccname(String party_bankaccname){
          this.party_bankaccname=party_bankaccname;
    }

    public String getParty_bankcode(){
          return this.party_bankcode;
    }

    public void setParty_bankcode(String party_bankcode){
          this.party_bankcode=party_bankcode;
    }

    public String getParty_bankname(){
          return this.party_bankname;
    }

    public void setParty_bankname(String party_bankname){
          this.party_bankname=party_bankname;
    }

    public String getParty_cash_in_accid(){
          return this.party_cash_in_accid;
    }

    public void setParty_cash_in_accid(String party_cash_in_accid){
          this.party_cash_in_accid=party_cash_in_accid;
    }

    public String getParty_cash_out_accid(){
          return this.party_cash_out_accid;
    }

    public void setParty_cash_out_accid(String party_cash_out_accid){
          this.party_cash_out_accid=party_cash_out_accid;
    }

    public String getCash_transfer_no(){
          return this.cash_transfer_no;
    }

    public void setCash_transfer_no(String cash_transfer_no){
          this.cash_transfer_no=cash_transfer_no;
    }

    public String getAccounting_user(){
          return this.accounting_user;
    }

    public void setAccounting_user(String accounting_user){
          this.accounting_user=accounting_user;
    }

    public String getAccounting_time(){
          return this.accounting_time;
    }

    public void setAccounting_time(String accounting_time){
          this.accounting_time=accounting_time;
    }

    public String getMemo(){
          return this.memo;
    }

    public void setMemo(String memo){
          this.memo=memo;
    }

    public String getCreate_time(){
          return this.create_time;
    }

    public void setCreate_time(String create_time){
          this.create_time=create_time;
    }

    public String getCreate_user(){
          return this.create_user;
    }

    public void setCreate_user(String create_user){
          this.create_user=create_user;
    }

    public String getUpdate_time(){
          return this.update_time;
    }

    public void setUpdate_time(String update_time){
          this.update_time=update_time;
    }

    public String getUpdate_user(){
          return this.update_user;
    }

    public void setUpdate_user(String update_user){
          this.update_user=update_user;
    }
    
    public String getChg_date() {
		return chg_date;
	}

	public void setChg_date(String chg_date) {
		this.chg_date = chg_date;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSetInstCash [cash_instr_id=");
		builder.append(cash_instr_id);
		builder.append(", instr_id=");
		builder.append(instr_id);
		builder.append(", buss_type=");
		builder.append(buss_type);
		builder.append(", status=");
		builder.append(status);
		builder.append(", p_cash_instrid=");
		builder.append(p_cash_instrid);
		builder.append(", transfer_type=");
		builder.append(transfer_type);
		builder.append(", cny=");
		builder.append(cny);
		builder.append(", is_cashflow_fixed=");
		builder.append(is_cashflow_fixed);
		builder.append(", chg_amount=");
		builder.append(chg_amount);
		builder.append(", chgamount_io=");
		builder.append(chgamount_io);
		builder.append(", self_chgamount=");
		builder.append(self_chgamount);
		builder.append(", ext_cahsserialno=");
		builder.append(ext_cahsserialno);
		builder.append(", self_bankacccode=");
		builder.append(self_bankacccode);
		builder.append(", self_bankaccname=");
		builder.append(self_bankaccname);
		builder.append(", self_bankcode=");
		builder.append(self_bankcode);
		builder.append(", self_bankname=");
		builder.append(self_bankname);
		builder.append(", self_cash_in_accid=");
		builder.append(self_cash_in_accid);
		builder.append(", self_cash_out_accid=");
		builder.append(self_cash_out_accid);
		builder.append(", party_chgamount=");
		builder.append(party_chgamount);
		builder.append(", party_bankacccode=");
		builder.append(party_bankacccode);
		builder.append(", party_bankaccname=");
		builder.append(party_bankaccname);
		builder.append(", party_bankcode=");
		builder.append(party_bankcode);
		builder.append(", party_bankname=");
		builder.append(party_bankname);
		builder.append(", party_cash_in_accid=");
		builder.append(party_cash_in_accid);
		builder.append(", party_cash_out_accid=");
		builder.append(party_cash_out_accid);
		builder.append(", cash_transfer_no=");
		builder.append(cash_transfer_no);
		builder.append(", accounting_user=");
		builder.append(accounting_user);
		builder.append(", accounting_time=");
		builder.append(accounting_time);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", create_time=");
		builder.append(create_time);
		builder.append(", create_user=");
		builder.append(create_user);
		builder.append(", update_time=");
		builder.append(update_time);
		builder.append(", update_user=");
		builder.append(update_user);
		builder.append(", chg_date=");
		builder.append(chg_date);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}