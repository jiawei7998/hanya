package com.singlee.capital.settle.large.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.settle.large.mapper.TtSettleLargeInfoMapper;
import com.singlee.capital.settle.large.model.TtSettleLargeInfo;
import com.singlee.capital.settle.large.model.TtSettleLargeInfoMap;
import com.singlee.capital.settle.large.service.SettleLargeInfoMapService;
import com.singlee.capital.settle.large.service.SettleLargeInfoService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.UserParamService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理service实现
 * 
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
@Service("settleLargeInfoService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SettleLargeInfoServiceImpl implements SettleLargeInfoService {

	/* 用户mapper层 */
	@Autowired
	private TtSettleLargeInfoMapper ttSettleLargeInfoMapper;
	/* 映射service */
	@Autowired
	public SettleLargeInfoMapService settleLargeInfoMapService;
	@Autowired
	public UserParamService userParamService;
	@Autowired
	public DayendDateService dayendDateService;

	@Override
	public Page<TtSettleLargeInfo> pageSettleLargeInfos(
			Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TtSettleLargeInfo> result = ttSettleLargeInfoMapper
				.pageSettleLargeInfos(params, rowBounds);
		return result;
	}

	@Override
	public List<TtSettleLargeInfo> getSettleLargeInfos(
			Map<String, Object> params) {
		List<TtSettleLargeInfo> result = ttSettleLargeInfoMapper
				.searchSettleLargeInfo(params);
		return result;
	}

	@Override
	@AutoLogMethod(value = "创建大额来往帐")
	public void createLargeInfo(TtSettleLargeInfo largeInfo) throws RException {
		largeInfo.setLeft_amount(largeInfo.getAmount());
		largeInfo.setMatch_status(DictConstants.LargeStatus.New);
		largeInfo.setCreate_time(DateUtil.getCurrentDateTimeAsString());
		largeInfo.setUpdate_time(largeInfo.getCreate_time());
		largeInfo.setUpdate_user(largeInfo.getCreate_user());
		ttSettleLargeInfoMapper.insert(largeInfo);
	}

	@Override
	public void updateLargeInfo(TtSettleLargeInfo largeInfo) {
		TtSettleLargeInfo u = ttSettleLargeInfoMapper
				.selectByPrimaryKey(largeInfo);
		if (u == null) {
			JY.raiseRException("查不到对应的回款信息");
		}
		largeInfo.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
		ttSettleLargeInfoMapper.updateByPrimaryKey(largeInfo);
	}

	@Override
	public void deleteLargeInfo(String[] largeIds) {
		for (String largeId : largeIds) {
			TtSettleLargeInfo u = ttSettleLargeInfoMapper
					.selectByPrimaryKey(largeId);
			if (u == null) {
				JY.raiseRException("查不到对应的回款信息");
			}
			ttSettleLargeInfoMapper.deleteByPrimaryKey(largeId);
		}
	}

	@Override
	public void sendLargeInfo(String[] largeIds, String opUserId) {
		for (String largeId : largeIds) {
			TtSettleLargeInfo u = ttSettleLargeInfoMapper
					.selectByPrimaryKey(largeId);
			if (u == null) {
				JY.raiseRException("查不到对应的回款信息");
			}
			u.setMatch_status(DictConstants.LargeStatus.WaitSure);
			u.setUpdate_user(opUserId);
			u.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
			ttSettleLargeInfoMapper.updateByPrimaryKey(u);
		}
	}

	@Override
	public void confirmLargeInfo(String largeId,
			List<TtSettleLargeInfoMap> largeInfoMaps, String opUserId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("large_id", largeId);
		map.put("is_lock", "true");
		List<TtSettleLargeInfo> largeInfos = ttSettleLargeInfoMapper
				.searchSettleLargeInfo(map);
		if (largeInfos == null || largeInfos.size() == 0) {
			JY.raiseRException("查不到对应的回款信息");
		}
		TtSettleLargeInfo largeInfo = largeInfos.get(0);
		if (!(DictConstants.LargeStatus.WaitSure.equals(largeInfo
				.getMatch_status()) || DictConstants.LargeStatus.PartSured
				.equals(largeInfo.getMatch_status()))) {
			JY.raiseRException("回款状态已改变，请刷新再试");
		}
		settleLargeInfoMapService.createLargeInfoMaps(largeId, largeInfoMaps);
		// 更新回款信息
		double totalMatchAmount = 0;
		if (largeInfoMaps != null && largeInfoMaps.size() > 0) {
			for (TtSettleLargeInfoMap largeInfoMap : largeInfoMaps) {
				totalMatchAmount += largeInfoMap.getMatch_amount();
			}
			if (MathUtil.isZero(totalMatchAmount)) {
				largeInfo.setMatch_status(DictConstants.LargeStatus.WaitSure);
			}
			if (MathUtil.isEqual(totalMatchAmount, largeInfo.getAmount(), 2,
					BigDecimal.ROUND_HALF_UP)) {
				largeInfo.setMatch_status(DictConstants.LargeStatus.Sured);
			} else if (MathUtil.compare(totalMatchAmount,
					largeInfo.getAmount(), 2) < 0) {
				largeInfo.setMatch_status(DictConstants.LargeStatus.PartSured);
			} else {
				JY.raiseRException("确认金额大于回款金额，请确认");
			}
		} else {
			largeInfo.setMatch_status(DictConstants.LargeStatus.WaitSure);
		}
		largeInfo.setMatch_amount(totalMatchAmount);
		largeInfo.setLeft_amount(MathUtil.round(largeInfo.getAmount()
				- totalMatchAmount, 2));
		largeInfo.setUpdate_user(opUserId);
		largeInfo.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
		ttSettleLargeInfoMapper.updateByPrimaryKey(largeInfo);
	}

	@Override
	public void returnLargeInfos(String remark, List<String> largeIds,
			String opUserId) {
		JY.require(largeIds != null && largeIds.size() > 0, "回款编号信息为空，请确认");
		for (String largeId : largeIds) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("large_id", largeId);
			map.put("is_lock", "true");
			List<TtSettleLargeInfo> largeInfos = ttSettleLargeInfoMapper
					.searchSettleLargeInfo(map);
			if (largeInfos == null || largeInfos.size() == 0) {
				JY.raiseRException("查不到对应的回款信息");
			}
			TtSettleLargeInfo largeInfo = largeInfos.get(0);
			largeInfo.setMatch_status(DictConstants.LargeStatus.Returned);
			largeInfo.setRemark(remark);
			largeInfo.setUpdate_user(opUserId);
			largeInfo.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
			ttSettleLargeInfoMapper.updateByPrimaryKey(largeInfo);
		}
	}

	@Override
	public void cancelLargeInfos(String remark, List<String> largeIds,
			String opUserId) {
		JY.require(largeIds != null && largeIds.size() > 0, "回款编号信息为空，请确认");
		// 删除映射信息
		settleLargeInfoMapService.deleteLargeInfoMap(largeIds
				.toArray(new String[] {}));
		for (String largeId : largeIds) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("large_id", largeId);
			map.put("is_lock", "true");
			List<TtSettleLargeInfo> largeInfos = ttSettleLargeInfoMapper
					.searchSettleLargeInfo(map);
			if (largeInfos == null || largeInfos.size() == 0) {
				JY.raiseRException("查不到对应的回款信息");
			}
			TtSettleLargeInfo largeInfo = largeInfos.get(0);
			largeInfo.setMatch_status(DictConstants.LargeStatus.Canceled);
			largeInfo.setRemark(remark);
			largeInfo.setUpdate_user(opUserId);
			largeInfo.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
			ttSettleLargeInfoMapper.updateByPrimaryKey(largeInfo);
		}
	}

	@Override
	public void synLargeInfos(String opUserId, String instId) {
		
		// TODO Auto-generated method stub
		
	}
}
