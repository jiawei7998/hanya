package com.singlee.capital.settle.service.impl.clear;

import com.singlee.capital.common.util.Constants;
import com.singlee.capital.settle.pojo.SettleInstCashVo;
import com.singlee.capital.settle.pojo.TrdBalanceCashVo;
import com.singlee.capital.settle.pojo.TrdBalanceSecuVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTrdClear {
	/**
	 * 日间交易证券结算清算接口
	 * @param trade    交易
	 * @return 
	 */
	public abstract List<TrdBalanceSecuVo> daySecuSettleClearing(TrdTradeVo trade);
	/**
	 * 日间交易资金结算清算接口-- 默认资金清算处理
	 * @param trade
	 * @return
	 */
	public abstract List<TrdBalanceCashVo> dayCashSettleClearing(TrdTradeVo trade);
	/**
	 * 日间交易资金结算清算接口
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public abstract List<TrdBalanceCashVo> dayCashSettleClearing(TrdTradeVo trade,List<SettleInstCashVo> cashInstList);
	/**
	 * 日间交易资金结算清算接口 限额
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public abstract List<TrdBalanceCashVo> dayCashSettleClearing4limit(TrdTradeVo trade,List<SettleInstCashVo> cashInstList);
	/**
	 * 日间交易证券抹账清算接口 -- 默认资金清算处理
	 * @param trade    交易
	 * @return 
	 */
	public abstract List<TrdBalanceSecuVo> daySecuEarseClearing(TrdTradeVo trade);
	/**
	 * 日间交易资金抹账清算接口
	 * @param trade
	 * @return
	 */
	public abstract List<TrdBalanceCashVo> dayCashEarseClearing(TrdTradeVo trade);
	/**
	 * 日间交易资金抹账清算接口
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public abstract List<TrdBalanceCashVo> dayCashEarseClearing(TrdTradeVo trade,List<SettleInstCashVo> cashInstList);
	/**
	 * 日间交易资金抹账清算接口 限额模拟清算使用
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public abstract List<TrdBalanceCashVo> dayCashEarseClearing4limit(TrdTradeVo trade,List<SettleInstCashVo> cashInstList);
	/**
	 * 日间交易资金清算公共接口
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @param isPartyAccOwner 是否对手账户所有者
	 * @return 
	 */
	public List<TrdBalanceCashVo> dayCashClearingBase(TrdTradeVo trade,List<SettleInstCashVo> cashInstList,boolean isPartyAccOwner, String settleOperateType){
		List<TrdBalanceCashVo> cashBalanceList = new ArrayList<TrdBalanceCashVo>();
		if(cashInstList == null || cashInstList.size() == 0) {
			return cashBalanceList;}
		if(settleOperateType.equals(Constants.settleOperateType.accounting)){
			if(trade.getActSettleDays() == 0){
				for(SettleInstCashVo cashInst : cashInstList){
					TrdBalanceCashVo cashBalance = new TrdBalanceCashVo();
					cashBalance.setIncashaccid(trade.getSelf_incashaccid());
					cashBalance.setOutcashaccid(cashInst.getSelf_cash_out_accid());
					cashBalance.setR_amount(cashInst.getChg_amount() * cashInst.getChgamount_io());
					cashBalance.setR_avaamount(cashInst.getChg_amount() * cashInst.getChgamount_io());
					cashBalanceList.add(cashBalance);
				}
			}
			else if(trade.getActSettleDays() == 1){
				for(SettleInstCashVo cashInst : cashInstList){
					TrdBalanceCashVo cashBalance = new TrdBalanceCashVo();
					cashBalance.setIncashaccid(trade.getSelf_incashaccid());
					cashBalance.setOutcashaccid(cashInst.getSelf_cash_out_accid());
					if(cashInst.getChgamount_io() > 0) {
						cashBalance.setR_rec_amount(cashInst.getChg_amount());}
					else {
						cashBalance.setR_pay_amount(cashInst.getChg_amount());}
					cashBalanceList.add(cashBalance);
				}
			}
		}
		else if(settleOperateType.equals(Constants.settleOperateType.earsing)){
			if(trade.getActSettleDays() == 0){
				for(SettleInstCashVo cashInst : cashInstList){
					TrdBalanceCashVo cashBalance = new TrdBalanceCashVo();
					cashBalance.setIncashaccid(trade.getSelf_incashaccid());
					cashBalance.setOutcashaccid(cashInst.getSelf_cash_out_accid());
					cashBalance.setR_amount(-cashInst.getChg_amount() * cashInst.getChgamount_io());
					cashBalance.setR_avaamount(-cashInst.getChg_amount() * cashInst.getChgamount_io());
					cashBalanceList.add(cashBalance);
				}
			}
			else if(trade.getActSettleDays() == 1){
				for(SettleInstCashVo cashInst : cashInstList){
					TrdBalanceCashVo cashBalance = new TrdBalanceCashVo();
					cashBalance.setIncashaccid(trade.getSelf_incashaccid());
					cashBalance.setOutcashaccid(cashInst.getSelf_cash_out_accid());
					if(cashInst.getChgamount_io() > 0) {
						cashBalance.setR_rec_amount(-cashInst.getChg_amount());}
					else {
						cashBalance.setR_pay_amount(-cashInst.getChg_amount());}
					cashBalanceList.add(cashBalance);
				}
			}
		}
		return cashBalanceList;
	}
	/**
	 * 日间交易证券结算清算接口
	 * @param trade    交易
	 * @return 
	 */
	public List<Object> daySecuExtSettleClearing(TrdTradeVo trade){
		return null;
	}
	/**
	 * 日间交易证券抹账清算接口 -- 默认资金清算处理
	 * @param trade    交易
	 * @return 
	 */
	public List<Object> daySecuExtEarseClearing(TrdTradeVo trade){
		return null;
	}
}
