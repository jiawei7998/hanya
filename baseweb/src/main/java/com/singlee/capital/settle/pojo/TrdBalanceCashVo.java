package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtTrdBalanceCash;


public class TrdBalanceCashVo extends TtTrdBalanceCash implements Cloneable {
	/** 银行账号 **/
	private String bankacc;
	/** 内部账号 **/
	private String inner_acccode;
	/** 核心余额  **/
	private double coreBalance;
	
	
	public String getBankacc() {
		return bankacc;
	}

	public void setBankacc(String bankacc) {
		this.bankacc = bankacc;
	}

	
	public String getInner_acccode() {
		return inner_acccode;
	}

	public void setInner_acccode(String inner_acccode) {
		this.inner_acccode = inner_acccode;
	}

	public double getCoreBalance() {
		return coreBalance;
	}

	public void setCoreBalance(double coreBalance) {
		this.coreBalance = coreBalance;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrdBalanceCashVo [bankacc=");
		builder.append(bankacc);
		builder.append(", inner_acccode=");
		builder.append(inner_acccode);
		builder.append(", coreBalance=");
		builder.append(coreBalance);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
