package com.singlee.capital.settle.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TT_SETTLE_INSTRUCTION
 * 
 * @author Libo Create Time 2013-01-24 15:20:03
 */
@Entity
@Table(name = "TT_SETTLE_INSTRUCTION")
public class TtSetInst {

	/** 指令状态 */
	private String status;

	/** 清算日期 */
	private String settle_date;

	/** 内部交易号 */
	private String intordid;

	/** 业务类别 */
	private String bus_type;

	/** 外部资金账户 */
	private String cash_out_accid;

	/** 外部证券账户 */
	private String secu_out_accid;

	/** 内部资金账户 */
	private String cash_in_accid;

	/** 内部证券账户 */
	private String secu_in_accid;

	/** 交易单编号 */
	private String tradeid;

	/**    */
	private int trade_version;

	/** 父指令编号 */
	private String p_instr_id;

	/** 备注 */
	private String memo;

	/** 最后修改时间 */
	private String update_time;

	/** 最后修改人 */
	private String update_user;

	/** 创建时间 */
	private String create_time;

	/** 创建人 */
	private String create_user;

	/** 结算完成时间 */
	private String settle_finish_time;

	/** 核算交易组合 */
	private String settle_grpid;

	/** 结算对手 */
	private String settle_partyid;

	/** 结算交易员 */
	private String settle_trader;

	/** 父金融工具市场类型 */
	private String p_m_type;

	/** 父金融工具资产类型 */
	private String p_a_type;

	/** 父金融工具代码 */
	private String p_i_code;

	/** 结算撤单标记 0-允许 1-不允许 */
	private String withdraw_flag;

	/** 结算对手 */
	private String partyname;

	/** 结算方式 见券付款(PAD)见款付券(DAP)纯券过户(FOP)券款对付(DVP) */
	private String settle_type;

	/** 交易对手简称 */
	private String party_shortname;

	/** 交易对手编号 */
	private String partycode;

	/** 是否理论付息日 */
	private String is_theory_payment;

	/** 计算截止日期 */
	private String cal_date;

	/** 结算指令序号 */
	@Id
	private String instr_id;

	/** 指令类型 */
	private String instr_type;

	private String book_market;

	private String book_flag;

	private String exe_market;

	private String extdealid;

	private String settle_finish_date;

	private String inst_id;
	/** 经办时间 */
	private String manage_time;

	/** 经办柜员 */
	private String manage_user;

	/** 复核时间 */
	private String verfy_time;

	/** 复核柜员 */
	private String verfy_user;

	/** 记账机构号 **/
	private String book_inst_id;

	/** 保留字1 **/
	private String reserve1;

	/** 保留字2 **/
	private String reserve2;

	/** 主金融工具代码 **/
	private String i_code;

	/** 主资产类型 **/
	private String a_type;

	/** 主市场类型 **/
	private String m_type;

	/** 主产品类型 **/
	private String p_type;

	/** 主产品分类 **/
	private String p_class;

	/** 结算金额**/
	private double settle_amount;
	/** ------- Generate Getter And Setter -------- **/

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSettle_date() {
		return this.settle_date;
	}

	public void setSettle_date(String settle_date) {
		this.settle_date = settle_date;
	}

	public String getIntordid() {
		return this.intordid;
	}

	public void setIntordid(String intordid) {
		this.intordid = intordid;
	}

	public String getBus_type() {
		return this.bus_type;
	}

	public void setBus_type(String bus_type) {
		this.bus_type = bus_type;
	}

	public String getCash_out_accid() {
		return this.cash_out_accid;
	}

	public void setCash_out_accid(String cash_out_accid) {
		this.cash_out_accid = cash_out_accid;
	}

	public String getSecu_out_accid() {
		return this.secu_out_accid;
	}

	public void setSecu_out_accid(String secu_out_accid) {
		this.secu_out_accid = secu_out_accid;
	}

	public String getCash_in_accid() {
		return this.cash_in_accid;
	}

	public void setCash_in_accid(String cash_in_accid) {
		this.cash_in_accid = cash_in_accid;
	}

	public String getSecu_in_accid() {
		return this.secu_in_accid;
	}

	public void setSecu_in_accid(String secu_in_accid) {
		this.secu_in_accid = secu_in_accid;
	}

	public String getTradeid() {
		return this.tradeid;
	}

	public void setTradeid(String tradeid) {
		this.tradeid = tradeid;
	}

	public int getTrade_version() {
		return this.trade_version;
	}

	public void setTrade_version(int trade_version) {
		this.trade_version = trade_version;
	}

	public String getP_instr_id() {
		return this.p_instr_id;
	}

	public void setP_instr_id(String p_instr_id) {
		this.p_instr_id = p_instr_id;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getUpdate_time() {
		return this.update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getUpdate_user() {
		return this.update_user;
	}

	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}

	public String getCreate_time() {
		return this.create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_user() {
		return this.create_user;
	}

	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}

	public String getSettle_finish_time() {
		return this.settle_finish_time;
	}

	public void setSettle_finish_time(String settle_finish_time) {
		this.settle_finish_time = settle_finish_time;
	}

	public String getSettle_grpid() {
		return this.settle_grpid;
	}

	public void setSettle_grpid(String settle_grpid) {
		this.settle_grpid = settle_grpid;
	}

	public String getSettle_partyid() {
		return this.settle_partyid;
	}

	public void setSettle_partyid(String settle_partyid) {
		this.settle_partyid = settle_partyid;
	}

	public String getSettle_trader() {
		return this.settle_trader;
	}

	public void setSettle_trader(String settle_trader) {
		this.settle_trader = settle_trader;
	}

	public String getP_m_type() {
		return this.p_m_type;
	}

	public void setP_m_type(String p_m_type) {
		this.p_m_type = p_m_type;
	}

	public String getP_a_type() {
		return this.p_a_type;
	}

	public void setP_a_type(String p_a_type) {
		this.p_a_type = p_a_type;
	}

	public String getP_i_code() {
		return this.p_i_code;
	}

	public void setP_i_code(String p_i_code) {
		this.p_i_code = p_i_code;
	}

	public String getWithdraw_flag() {
		return this.withdraw_flag;
	}

	public void setWithdraw_flag(String withdraw_flag) {
		this.withdraw_flag = withdraw_flag;
	}

	public String getPartyname() {
		return this.partyname;
	}

	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}

	public String getSettle_type() {
		return this.settle_type;
	}

	public void setSettle_type(String settle_type) {
		this.settle_type = settle_type;
	}

	public String getParty_shortname() {
		return this.party_shortname;
	}

	public void setParty_shortname(String party_shortname) {
		this.party_shortname = party_shortname;
	}

	public String getPartycode() {
		return this.partycode;
	}

	public void setPartycode(String partycode) {
		this.partycode = partycode;
	}

	public String getIs_theory_payment() {
		return this.is_theory_payment;
	}

	public void setIs_theory_payment(String is_theory_payment) {
		this.is_theory_payment = is_theory_payment;
	}

	public String getCal_date() {
		return this.cal_date;
	}

	public void setCal_date(String cal_date) {
		this.cal_date = cal_date;
	}

	public String getInstr_id() {
		return this.instr_id;
	}

	public void setInstr_id(String instr_id) {
		this.instr_id = instr_id;
	}

	public String getInstr_type() {
		return this.instr_type;
	}

	public void setInstr_type(String instr_type) {
		this.instr_type = instr_type;
	}

	public String getBook_market() {
		return book_market;
	}

	public void setBook_market(String book_market) {
		this.book_market = book_market;
	}

	public String getBook_flag() {
		return book_flag;
	}

	public void setBook_flag(String book_flag) {
		this.book_flag = book_flag;
	}

	public String getExe_market() {
		return exe_market;
	}

	public void setExe_market(String exe_market) {
		this.exe_market = exe_market;
	}

	public String getExtdealid() {
		return extdealid;
	}

	public void setExtdealid(String extdealid) {
		this.extdealid = extdealid;
	}

	public String getSettle_finish_date() {
		return settle_finish_date;
	}

	public void setSettle_finish_date(String settle_finish_date) {
		this.settle_finish_date = settle_finish_date;
	}

	public String getInst_id() {
		return inst_id;
	}

	public void setInst_id(String inst_id) {
		this.inst_id = inst_id;
	}

	public String getManage_time() {
		return manage_time;
	}

	public void setManage_time(String manage_time) {
		this.manage_time = manage_time;
	}

	public String getManage_user() {
		return manage_user;
	}

	public void setManage_user(String manage_user) {
		this.manage_user = manage_user;
	}

	public String getVerfy_time() {
		return verfy_time;
	}

	public void setVerfy_time(String verfy_time) {
		this.verfy_time = verfy_time;
	}

	public String getVerfy_user() {
		return verfy_user;
	}

	public void setVerfy_user(String verfy_user) {
		this.verfy_user = verfy_user;
	}

	public String getBook_inst_id() {
		return book_inst_id;
	}

	public void setBook_inst_id(String book_inst_id) {
		this.book_inst_id = book_inst_id;
	}

	public String getReserve1() {
		return reserve1;
	}

	public void setReserve1(String reserve1) {
		this.reserve1 = reserve1;
	}

	public String getReserve2() {
		return reserve2;
	}

	public void setReserve2(String reserve2) {
		this.reserve2 = reserve2;
	}

	public String getI_code() {
		return i_code;
	}

	public void setI_code(String i_code) {
		this.i_code = i_code;
	}

	public String getA_type() {
		return a_type;
	}

	public void setA_type(String a_type) {
		this.a_type = a_type;
	}

	public String getM_type() {
		return m_type;
	}

	public void setM_type(String m_type) {
		this.m_type = m_type;
	}

	public String getP_type() {
		return p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public String getP_class() {
		return p_class;
	}

	public void setP_class(String p_class) {
		this.p_class = p_class;
	}

	public double getSettle_amount() {
		return settle_amount;
	}

	public void setSettle_amount(double settle_amount) {
		this.settle_amount = settle_amount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSetInst [status=");
		builder.append(status);
		builder.append(", settle_date=");
		builder.append(settle_date);
		builder.append(", intordid=");
		builder.append(intordid);
		builder.append(", bus_type=");
		builder.append(bus_type);
		builder.append(", cash_out_accid=");
		builder.append(cash_out_accid);
		builder.append(", secu_out_accid=");
		builder.append(secu_out_accid);
		builder.append(", cash_in_accid=");
		builder.append(cash_in_accid);
		builder.append(", secu_in_accid=");
		builder.append(secu_in_accid);
		builder.append(", tradeid=");
		builder.append(tradeid);
		builder.append(", trade_version=");
		builder.append(trade_version);
		builder.append(", p_instr_id=");
		builder.append(p_instr_id);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", update_time=");
		builder.append(update_time);
		builder.append(", update_user=");
		builder.append(update_user);
		builder.append(", create_time=");
		builder.append(create_time);
		builder.append(", create_user=");
		builder.append(create_user);
		builder.append(", settle_finish_time=");
		builder.append(settle_finish_time);
		builder.append(", settle_grpid=");
		builder.append(settle_grpid);
		builder.append(", settle_partyid=");
		builder.append(settle_partyid);
		builder.append(", settle_trader=");
		builder.append(settle_trader);
		builder.append(", p_m_type=");
		builder.append(p_m_type);
		builder.append(", p_a_type=");
		builder.append(p_a_type);
		builder.append(", p_i_code=");
		builder.append(p_i_code);
		builder.append(", withdraw_flag=");
		builder.append(withdraw_flag);
		builder.append(", partyname=");
		builder.append(partyname);
		builder.append(", settle_type=");
		builder.append(settle_type);
		builder.append(", party_shortname=");
		builder.append(party_shortname);
		builder.append(", partycode=");
		builder.append(partycode);
		builder.append(", is_theory_payment=");
		builder.append(is_theory_payment);
		builder.append(", cal_date=");
		builder.append(cal_date);
		builder.append(", instr_id=");
		builder.append(instr_id);
		builder.append(", instr_type=");
		builder.append(instr_type);
		builder.append(", book_market=");
		builder.append(book_market);
		builder.append(", book_flag=");
		builder.append(book_flag);
		builder.append(", exe_market=");
		builder.append(exe_market);
		builder.append(", extdealid=");
		builder.append(extdealid);
		builder.append(", settle_finish_date=");
		builder.append(settle_finish_date);
		builder.append(", inst_id=");
		builder.append(inst_id);
		builder.append(", manage_time=");
		builder.append(manage_time);
		builder.append(", manage_user=");
		builder.append(manage_user);
		builder.append(", verfy_time=");
		builder.append(verfy_time);
		builder.append(", verfy_user=");
		builder.append(verfy_user);
		builder.append(", book_inst_id=");
		builder.append(book_inst_id);
		builder.append(", reserve1=");
		builder.append(reserve1);
		builder.append(", reserve2=");
		builder.append(reserve2);
		builder.append(", i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", p_type=");
		builder.append(p_type);
		builder.append(", p_class=");
		builder.append(p_class);
		builder.append(", settle_amount=");
		builder.append(settle_amount);
		builder.append("]");
		return builder.toString();
	}
}