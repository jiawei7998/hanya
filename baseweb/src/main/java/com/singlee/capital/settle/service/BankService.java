package com.singlee.capital.settle.service;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.model.BankTrade;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;
/**
 * @projectName 理财资产管理系统
 * @className 银行查询接口
 * @author wzm
 * @createDate 2016-11-14
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface BankService {
	
	/**
	 * 查询银行列表
	 * @return
	 * @throws Exception 
	 */
	public Page<BankTrade> queryBankList(@RequestBody Map<String,Object> params);
}

