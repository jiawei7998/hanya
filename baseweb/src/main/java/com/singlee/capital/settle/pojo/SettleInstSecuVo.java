package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtSetInstSecu;


public class SettleInstSecuVo extends TtSetInstSecu implements Cloneable{
	/** 债券信息：托管市场 **/
	private String host_market;
	
	/** 理财信息：产品类型 **/
	private String p_type;
	
	/** 理财信息：保本类型 **/
	private String money_p2;
	
	/** 资管信息：资管类型 **/
	private String assetplan_p1;
	
	/** 理财资管信息 ：兑付日**/
	private String ib_paydate;
	
	/** 同业存单代码 **/
	private String ibcd_i_code;
	
	/** 同业存单名称 **/
	private String ibcd_i_name;
	
	/** 利率  **/
	private double rate;
	
	/** 起息日 **/
	private String value_date;
	
	/** 到期日 **/
	private String mature_date;
	
	/** 交易金额 **/
	private String trade_amount;
	
	/** 付息频率  **/
	private String pay_freq;
	
	/** 计息频率 **/
	private String cal_freq;
	
	/** 计息基准 **/
	private String day_counter;
	
	/** 交易对手简称 **/
	private String party_shortname;
	
	/** 投资收益 **/
	private double investIncome;
	
	/** 收息类型**/
	private String int_type;
	
	/** 业务审批单号**/
	private String order_id;
	
	public String getIb_paydate() {
		return ib_paydate;
	}

	public void setIb_paydate(String ib_paydate) {
		this.ib_paydate = ib_paydate;
	}

	public String getAssetplan_p1() {
		return assetplan_p1;
	}

	public void setAssetplan_p1(String assetplan_p1) {
		this.assetplan_p1 = assetplan_p1;
	}

	public String getP_type() {
		return p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public String getMoney_p2() {
		return money_p2;
	}

	public void setMoney_p2(String money_p2) {
		this.money_p2 = money_p2;
	}

	public String getHost_market() {
		return host_market;
	}

	public void setHost_market(String host_market) {
		this.host_market = host_market;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	

	public String getIbcd_i_code() {
		return ibcd_i_code;
	}

	public void setIbcd_i_code(String ibcd_i_code) {
		this.ibcd_i_code = ibcd_i_code;
	}

	public String getIbcd_i_name() {
		return ibcd_i_name;
	}

	public void setIbcd_i_name(String ibcd_i_name) {
		this.ibcd_i_name = ibcd_i_name;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getValue_date() {
		return value_date;
	}

	public void setValue_date(String value_date) {
		this.value_date = value_date;
	}

	public String getMature_date() {
		return mature_date;
	}

	public void setMature_date(String mature_date) {
		this.mature_date = mature_date;
	}

	public String getTrade_amount() {
		return trade_amount;
	}

	public void setTrade_amount(String trade_amount) {
		this.trade_amount = trade_amount;
	}

	public String getPay_freq() {
		return pay_freq;
	}

	public void setPay_freq(String pay_freq) {
		this.pay_freq = pay_freq;
	}

	public String getCal_freq() {
		return cal_freq;
	}

	public void setCal_freq(String cal_freq) {
		this.cal_freq = cal_freq;
	}

	public String getDay_counter() {
		return day_counter;
	}

	public void setDay_counter(String day_counter) {
		this.day_counter = day_counter;
	}
	

	public String getParty_shortname() {
		return party_shortname;
	}

	public void setParty_shortname(String party_shortname) {
		this.party_shortname = party_shortname;
	}
	
	public double getInvestIncome() {
		return investIncome;
	}

	public void setInvestIncome(double investIncome) {
		this.investIncome = investIncome;
	}

	public String getInt_type() {
		return int_type;
	}

	public void setInt_type(String int_type) {
		this.int_type = int_type;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SettleInstSecuVo [host_market=");
		builder.append(host_market);
		builder.append(", p_type=");
		builder.append(p_type);
		builder.append(", money_p2=");
		builder.append(money_p2);
		builder.append(", assetplan_p1=");
		builder.append(assetplan_p1);
		builder.append(", ib_paydate=");
		builder.append(ib_paydate);
		builder.append(", ibcd_i_code=");
		builder.append(ibcd_i_code);
		builder.append(", ibcd_i_name=");
		builder.append(ibcd_i_name);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", value_date=");
		builder.append(value_date);
		builder.append(", mature_date=");
		builder.append(mature_date);
		builder.append(", trade_amount=");
		builder.append(trade_amount);
		builder.append(", pay_freq=");
		builder.append(pay_freq);
		builder.append(", cal_freq=");
		builder.append(cal_freq);
		builder.append(", day_counter=");
		builder.append(day_counter);
		builder.append(", party_shortname=");
		builder.append(party_shortname);
		builder.append(", investIncome=");
		builder.append(investIncome);
		builder.append(", int_type=");
		builder.append(int_type);
		builder.append(", order_id=");
		builder.append(order_id);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
