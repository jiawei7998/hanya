package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtTrdIbCurrentRateAdj;


/**
 * 活期 利率调整 vo
 * @author LyonChen
 *
 */
public class DepositCurrentRateAdjVo extends TtTrdIbCurrentRateAdj {
	
	// 交易对手
	private String counterparty_code;
	// 交易对手
	private String counterparty_name;
	
	// 银行 账户
	private String bankacc;
	// 银行 账户名称
	private String accname;
	private String creator_name;
	private String approver_name;
	
	private String maxDate;
	private double maxDateRate = Double.NaN;
	
	public String getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}
	public double getMaxDateRate() {
		return maxDateRate;
	}
	public void setMaxDateRate(double maxDateRate) {
		this.maxDateRate = maxDateRate;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getCreator_name() {
		return creator_name;
	}
	public void setCreator_name(String creator_name) {
		this.creator_name = creator_name;
	}
	public String getApprover_name() {
		return approver_name;
	}
	public void setApprover_name(String approver_name) {
		this.approver_name = approver_name;
	}
	public String getBankacc() {
		return bankacc;
	}
	public void setBankacc(String bankacc) {
		this.bankacc = bankacc;
	}
	public String getCounterparty_code() {
		return counterparty_code;
	}
	public void setCounterparty_code(String counterparty_code) {
		this.counterparty_code = counterparty_code;
	}
	public String getCounterparty_name() {
		return counterparty_name;
	}
	public void setCounterparty_name(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	

}
