package com.singlee.capital.settle.service;

import com.singlee.capital.settle.model.TtSetInstCashCheck;

import java.util.List;
import java.util.Map;

/**
 * 结算资金流水单 服务
 * @author lihb
 *
 */
public interface SettleInstCashCheckService {
	/**
	 * 根据资金指令ID查询资金流水单信息
	 * @param cashInstrId  资金指令
	 * @return
	 */
	public List<TtSetInstCashCheck> getCashInstCheckByCashInstrId(String cashInstrId);

	/**
	 * 根据条件查询资金流水单信息
	 * @param map
	 * 
	 * map内参数：【都可为空】
	 * @return
	 */
	public List<TtSetInstCashCheck> getCashInstCheckList(Map<String, Object> mapO);
	
	/**
	 * 根据资金流水单编号查询资金流水单
	 * @param cashInstCheckId 资金流水单编号 不允许为空
	 * @return
	 */
	public TtSetInstCashCheck getCashInstCheckByCheckId(String cashInstCheckId);
	
	/**
	 * 保存资金流水单信息
	 * @param cashInstCheck 资金流水单对象
	 * @return
	 */
	public TtSetInstCashCheck addCashInstCheckInfo(TtSetInstCashCheck cashInstCheck);
	
	/**
	 * 修改资金流水单
	 * @param cashInstCheck
	 */
	public void modifyCashInstCheck(TtSetInstCashCheck cashInstCheck);
	
	/**
	 * 删除资金流水单
	 * @param cashInstrId   资金指令号
	 */
	public void delCashInstCheckByCashInstrId(String cashInstrId);
	/**
	 * 根据主指令号删除资金流水单
	 * @param instrId   主指令ID
	 */
	public void delInstCashCheckByInstrId(String instrId);
	/**
	 * 根据来账主机流水查询该笔来账是否已绑定
	 * @param hostFlowNo
	 * @return
	 */
	public String getLarteRcvStatusByHostFlowNo(String hostFlowNo,double largeRcvAmount);
	/**
	 * 根据主机流水查询大额剩余额度
	 * @param hostFlowNo
	 * @return
	 */
	public double getLarteRcvMatchedAmountByHostFlowNo(String hostFlowNo);
}
