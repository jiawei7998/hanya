package com.singlee.capital.settle.large.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TT_SETTLE_LARGEINFO")
public class TtSettleLargeInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_SETTLE_LARGE.NEXTVAL FROM DUAL")
	private String large_id;
	/** 大额来往帐日期 **/
	private String large_date;
	/** 主机交易流水号 **/
	private String hostflowno;
	/** 来账金额 **/
	private double amount;
	/** 剩余金额 **/
	private double left_amount;
	/** 匹配类型 **/
	private String match_type;
	/** 已匹配金额 **/
	private double match_amount;
	/** 付款人账号 **/
	private String payaccountno;
	/** 付款行行号 **/
	private String paybankno;
	/** 付款行行名 **/
	private String paybankname;
	/** 付款人户名 **/
	private String payaccountusername;
	/** 收款人账号 **/
	private String receiveaccountno;
	/** 收款人户名 **/
	private String receiveaccountusername;
	/** 收款人行号 **/
	private String receivebankno;
	/** 收款行行名 **/
	private String receivebankname;
	/** 附言 **/
	private String remark;
	/** 匹配状态 **/
	private String match_status;
	/** 大额类型 **/
	private String large_type;

	/** 支付交易序号 */
	private String trans_seq;

	/**
	 * 币种
	 */
	private String currency;

	/**
	 * 业务种类
	 */
	private String buss_type;

	/**
	 * 前置日期
	 */
	private String front_date;
	/**
	 * 前置流水
	 */
	private String front_seq;
	/** 创建人 **/
	private String create_user;
	/** 创建时间 **/
	private String create_time;
	/** 更新人 **/
	private String update_user;
	/** 更新时间 **/
	private String update_time;
	
	@Transient
	private String create_user_name;
	
	@Transient
	private String update_user_name;
	
	public String getLarge_id() {
		return large_id;
	}
	public void setLarge_id(String large_id) {
		this.large_id = large_id;
	}
	public String getLarge_date() {
		return large_date;
	}
	public void setLarge_date(String large_date) {
		this.large_date = large_date;
	}
	public String getHostflowno() {
		return hostflowno;
	}
	public void setHostflowno(String hostflowno) {
		this.hostflowno = hostflowno;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getLeft_amount() {
		return left_amount;
	}
	public void setLeft_amount(double left_amount) {
		this.left_amount = left_amount;
	}
	public String getMatch_type() {
		return match_type;
	}
	public void setMatch_type(String match_type) {
		this.match_type = match_type;
	}
	public double getMatch_amount() {
		return match_amount;
	}
	public void setMatch_amount(double match_amount) {
		this.match_amount = match_amount;
	}
	public String getPayaccountno() {
		return payaccountno;
	}
	public void setPayaccountno(String payaccountno) {
		this.payaccountno = payaccountno;
	}
	public String getPaybankno() {
		return paybankno;
	}
	public void setPaybankno(String paybankno) {
		this.paybankno = paybankno;
	}
	public String getPayaccountusername() {
		return payaccountusername;
	}
	public void setPayaccountusername(String payaccountusername) {
		this.payaccountusername = payaccountusername;
	}
	public String getReceiveaccountno() {
		return receiveaccountno;
	}
	public void setReceiveaccountno(String receiveaccountno) {
		this.receiveaccountno = receiveaccountno;
	}
	public String getReceiveaccountusername() {
		return receiveaccountusername;
	}
	public void setReceiveaccountusername(String receiveaccountusername) {
		this.receiveaccountusername = receiveaccountusername;
	}
	public String getReceivebankno() {
		return receivebankno;
	}
	public void setReceivebankno(String receivebankno) {
		this.receivebankno = receivebankno;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getMatch_status() {
		return match_status;
	}
	public void setMatch_status(String match_status) {
		this.match_status = match_status;
	}
	public String getLarge_type() {
		return large_type;
	}
	public void setLarge_type(String large_type) {
		this.large_type = large_type;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getTrans_seq() {
		return trans_seq;
	}
	public void setTrans_seq(String trans_seq) {
		this.trans_seq = trans_seq;
	}
	public String getBuss_type() {
		return buss_type;
	}
	public void setBuss_type(String buss_type) {
		this.buss_type = buss_type;
	}
	public String getFront_date() {
		return front_date;
	}
	public void setFront_date(String front_date) {
		this.front_date = front_date;
	}
	public String getFront_seq() {
		return front_seq;
	}
	public void setFront_seq(String front_seq) {
		this.front_seq = front_seq;
	}
	
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	
	public String getCreate_user_name() {
		return create_user_name;
	}
	public void setCreate_user_name(String create_user_name) {
		this.create_user_name = create_user_name;
	}
	public String getUpdate_user_name() {
		return update_user_name;
	}
	public void setUpdate_user_name(String update_user_name) {
		this.update_user_name = update_user_name;
	}
	public String getPaybankname() {
		return paybankname;
	}
	public void setPaybankname(String paybankname) {
		this.paybankname = paybankname;
	}
	public String getReceivebankname() {
		return receivebankname;
	}
	public void setReceivebankname(String receivebankname) {
		this.receivebankname = receivebankname;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSettleLargeInfo [large_id=");
		builder.append(large_id);
		builder.append(", large_date=");
		builder.append(large_date);
		builder.append(", hostflowno=");
		builder.append(hostflowno);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", left_amount=");
		builder.append(left_amount);
		builder.append(", match_type=");
		builder.append(match_type);
		builder.append(", match_amount=");
		builder.append(match_amount);
		builder.append(", payaccountno=");
		builder.append(payaccountno);
		builder.append(", paybankno=");
		builder.append(paybankno);
		builder.append(", payaccountusername=");
		builder.append(payaccountusername);
		builder.append(", receiveaccountno=");
		builder.append(receiveaccountno);
		builder.append(", receiveaccountusername=");
		builder.append(receiveaccountusername);
		builder.append(", receivebankno=");
		builder.append(receivebankno);
		builder.append(", remark=");
		builder.append(remark);
		builder.append(", match_status=");
		builder.append(match_status);
		builder.append(", large_type=");
		builder.append(large_type);
		builder.append(", trans_seq=");
		builder.append(trans_seq);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", buss_type=");
		builder.append(buss_type);
		builder.append(", front_date=");
		builder.append(front_date);
		builder.append(", front_seq=");
		builder.append(front_seq);
		builder.append(", create_user=");
		builder.append(create_user);
		builder.append(", create_time=");
		builder.append(create_time);
		builder.append(", update_user=");
		builder.append(update_user);
		builder.append(", update_time=");
		builder.append(update_time);
		builder.append(", update_user_name=");
		builder.append(update_user_name);
		builder.append(", create_user_name=");
		builder.append(create_user_name);
		builder.append(", paybankname=");
		builder.append(paybankname);
		builder.append(", receivebankname=");
		builder.append(receivebankname);
		builder.append("]");
		return builder.toString();
	}
	
}
