package com.singlee.capital.settle.pojo;

import java.util.List;



/**
 * 指令包
 * @author LyonChen
 *
 */
public class SettleInstPackage {

	private SettleInstVo instMain;//主指令
	private List<SettleInstSecuVo> setSecuList ;//券指令列表
	private List<SettleInstCashVo> setCashList;//资金指令列表
	
	public SettleInstVo getInstMain() {
		return instMain;
	}
	public void setInstMain(SettleInstVo instMain) {
		this.instMain = instMain;
	}
	public List<SettleInstSecuVo> getSetSecuList() {
		return setSecuList;
	}
	public void setSetSecuList(List<SettleInstSecuVo> setSecuList) {
		this.setSecuList = setSecuList;
	}
	public List<SettleInstCashVo> getSetCashList() {
		return setCashList;
	}
	public void setSetCashList(List<SettleInstCashVo> setCashList) {
		this.setCashList = setCashList;
	}

}
