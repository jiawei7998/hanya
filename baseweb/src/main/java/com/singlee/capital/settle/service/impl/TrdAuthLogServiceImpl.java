package com.singlee.capital.settle.service.impl;

import com.singlee.capital.settle.mapper.TrdAuthLogMapper;
import com.singlee.capital.settle.model.TtTrdAuthLog;
import com.singlee.capital.settle.service.TrdAuthLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("trdAuthLogService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrdAuthLogServiceImpl implements TrdAuthLogService {
	
	@Autowired
	private TrdAuthLogMapper trdAuthLogMapper;

	@Override
	public List<TtTrdAuthLog> getTrdAuthLogList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return trdAuthLogMapper.select(map);
	}

	@Override
	public TtTrdAuthLog addTrdAuthLog(TtTrdAuthLog trdAuthLog) {
		// TODO Auto-generated method stub
		trdAuthLogMapper.insert(trdAuthLog);
		return trdAuthLog;
	}
	@Override
	public void modifyTrdAuthLog(TtTrdAuthLog trdAuthLog) {
		// TODO Auto-generated method stub
		trdAuthLogMapper.update(trdAuthLog);
	}

	@Override
	public TtTrdAuthLog getTrdAuthLogById(String authLogId) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("auth_id", authLogId);
		List<TtTrdAuthLog> logList = getTrdAuthLogList(map);
		if(logList != null && logList.size() == 1){
			return logList.get(0);
		}
		return null;
	}
	
}
