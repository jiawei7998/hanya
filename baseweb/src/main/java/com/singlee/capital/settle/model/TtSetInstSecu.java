package com.singlee.capital.settle.model;

/**
 * TT_SETTLE_INST_SECU      
 * @author   Libo
 * Create Time  2013-01-24 15:20:02
 */
public class TtSetInstSecu {

    /**  当前实际全价成本  */
    private double r_cost_fullprice_actual;

    /**  券指令序号  */
    private String secu_instr_id;

    /**  状态  */
    private String status;

    /**  主结算指令序号  */
    private String instr_id;

    /**  业务类型  */
    private String buss_type;

    /**  金融工具代码  */
    private String i_code;

    /**  资产类型  */
    private String a_type;

    /**  市场类型  */
    private String m_type;

    /**  债券简称  */
    private String i_name;

    /**  产品分类  */
    private String p_class;

    /**  中债簿记标志位：XXXX 4位 1位-基本指令确认; 2位-付款确认;  3位-收款确认 ; 4位-合同成功  */
    private String book_flag;

    /**  挂销帐顺序号  */
    private String handorclose_no;

    /**  抹账状态  */
    private String erase_status;

    /**  外部确认编号  */
    private String ext_confirmno;

    /**  有效日期  */
    private String beg_date;

    /**  失效日期  */
    private String end_date;

    /**  费用成本变动  */
    private double r_cost_fee;

    /**  当前应计利息成本变动  */
    private double r_cost_ai;

    /**  当前净价成本变动  */
    private double r_cost_netprice;

    /**  当前实际应计利息  */
    private double r_cost_ai_actual;

    /**  当前实际净价成本  */
    private double r_cost_netprice_actual;

    /**  当前全价成本变动  */
    private double r_cost_fullprice;

    /**  当前余额变动  */
    private double r_amount;

    /**  当前历史实收利息  */
    private double r_cost_ai_his_actual;

    /**  核算业务分类 IN：收券 OUT:付券  */
    private String accounting_busstype;

    /**  复核人  */
    private String accounting_user;

    /**  复核时间  */
    private String accounting_time;

    /**  对手中债登托管账号  */
    private String party_zzd_accid;

    /**  本方托管账户  */
    private String self_zzd_accid;

    /**    */
    private String memo;

    /**  最后修改时间  */
    private String update_time;

    /**  最后修改人  */
    private String update_user;

    /**  创建时间  */
    private String create_time;

    /**  创建人  */
    private String create_user;
    
    /** 内部证券账户 **/
    private String secu_in_accid;
    
    /** 外部证券账户 **/
    private String secu_out_accid;
    
    /** 首期交易编号 **/
    private String p_trade_id;
    
    /** 保留字1 **/
    private String p1;
    
    /** 保留字2 **/
    private String p2;

         /**-------  Generate Getter And Setter   --------**/

    
    public double getR_cost_fullprice_actual(){
          return this.r_cost_fullprice_actual;
    }

    public String getP_trade_id() {
			return p_trade_id;
		}

		public void setP_trade_id(String p_trade_id) {
			this.p_trade_id = p_trade_id;
		}

		public String getP1() {
			return p1;
		}

		public void setP1(String p1) {
			this.p1 = p1;
		}

		public String getP2() {
			return p2;
		}

		public void setP2(String p2) {
			this.p2 = p2;
		}

	public void setR_cost_fullprice_actual(double r_cost_fullprice_actual){
          this.r_cost_fullprice_actual=r_cost_fullprice_actual;
    }

    public String getSecu_instr_id(){
          return this.secu_instr_id;
    }

    public void setSecu_instr_id(String secu_instr_id){
          this.secu_instr_id=secu_instr_id;
    }

    public String getStatus(){
          return this.status;
    }

    public void setStatus(String status){
          this.status=status;
    }

    public String getInstr_id(){
          return this.instr_id;
    }

    public void setInstr_id(String instr_id){
          this.instr_id=instr_id;
    }

    public String getBuss_type(){
          return this.buss_type;
    }

    public void setBuss_type(String buss_type){
          this.buss_type=buss_type;
    }

    public String getI_code(){
          return this.i_code;
    }

    public void setI_code(String i_code){
          this.i_code=i_code;
    }

    public String getA_type(){
          return this.a_type;
    }

    public void setA_type(String a_type){
          this.a_type=a_type;
    }

    public String getM_type(){
          return this.m_type;
    }

    public void setM_type(String m_type){
          this.m_type=m_type;
    }

    public String getI_name(){
          return this.i_name;
    }

    public void setI_name(String i_name){
          this.i_name=i_name;
    }

    public String getP_class(){
          return this.p_class;
    }

    public void setP_class(String p_class){
          this.p_class=p_class;
    }

    public String getBook_flag(){
          return this.book_flag;
    }

    public void setBook_flag(String book_flag){
          this.book_flag=book_flag;
    }

    public String getHandorclose_no(){
          return this.handorclose_no;
    }

    public void setHandorclose_no(String handorclose_no){
          this.handorclose_no=handorclose_no;
    }

    public String getErase_status(){
          return this.erase_status;
    }

    public void setErase_status(String erase_status){
          this.erase_status=erase_status;
    }

    public String getExt_confirmno(){
          return this.ext_confirmno;
    }

    public void setExt_confirmno(String ext_confirmno){
          this.ext_confirmno=ext_confirmno;
    }

    public String getBeg_date(){
          return this.beg_date;
    }

    public void setBeg_date(String beg_date){
          this.beg_date=beg_date;
    }

    public String getEnd_date(){
          return this.end_date;
    }

    public void setEnd_date(String end_date){
          this.end_date=end_date;
    }

    public double getR_cost_fee(){
          return this.r_cost_fee;
    }

    public void setR_cost_fee(double r_cost_fee){
          this.r_cost_fee=r_cost_fee;
    }

    public double getR_cost_ai(){
          return this.r_cost_ai;
    }

    public void setR_cost_ai(double r_cost_ai){
          this.r_cost_ai=r_cost_ai;
    }

    public double getR_cost_netprice(){
          return this.r_cost_netprice;
    }

    public void setR_cost_netprice(double r_cost_netprice){
          this.r_cost_netprice=r_cost_netprice;
    }

    public double getR_cost_ai_actual(){
          return this.r_cost_ai_actual;
    }

    public void setR_cost_ai_actual(double r_cost_ai_actual){
          this.r_cost_ai_actual=r_cost_ai_actual;
    }

    public double getR_cost_netprice_actual(){
          return this.r_cost_netprice_actual;
    }

    public void setR_cost_netprice_actual(double r_cost_netprice_actual){
          this.r_cost_netprice_actual=r_cost_netprice_actual;
    }

    public double getR_cost_fullprice(){
          return this.r_cost_fullprice;
    }

    public void setR_cost_fullprice(double r_cost_fullprice){
          this.r_cost_fullprice=r_cost_fullprice;
    }

    public double getR_amount(){
          return this.r_amount;
    }

    public void setR_amount(double r_amount){
          this.r_amount=r_amount;
    }

    public double getR_cost_ai_his_actual(){
          return this.r_cost_ai_his_actual;
    }

    public void setR_cost_ai_his_actual(double r_cost_ai_his_actual){
          this.r_cost_ai_his_actual=r_cost_ai_his_actual;
    }

    public String getAccounting_busstype(){
          return this.accounting_busstype;
    }

    public void setAccounting_busstype(String accounting_busstype){
          this.accounting_busstype=accounting_busstype;
    }

    public String getAccounting_user(){
          return this.accounting_user;
    }

    public void setAccounting_user(String accounting_user){
          this.accounting_user=accounting_user;
    }

    public String getAccounting_time(){
          return this.accounting_time;
    }

    public void setAccounting_time(String accounting_time){
          this.accounting_time=accounting_time;
    }

    public String getParty_zzd_accid(){
          return this.party_zzd_accid;
    }

    public void setParty_zzd_accid(String party_zzd_accid){
          this.party_zzd_accid=party_zzd_accid;
    }

    public String getSelf_zzd_accid(){
          return this.self_zzd_accid;
    }

    public void setSelf_zzd_accid(String self_zzd_accid){
          this.self_zzd_accid=self_zzd_accid;
    }

    public String getMemo(){
          return this.memo;
    }

    public void setMemo(String memo){
          this.memo=memo;
    }

    public String getUpdate_time(){
          return this.update_time;
    }

    public void setUpdate_time(String update_time){
          this.update_time=update_time;
    }

    public String getUpdate_user(){
          return this.update_user;
    }

    public void setUpdate_user(String update_user){
          this.update_user=update_user;
    }

    public String getCreate_time(){
          return this.create_time;
    }

    public void setCreate_time(String create_time){
          this.create_time=create_time;
    }

    public String getCreate_user(){
          return this.create_user;
    }

    public void setCreate_user(String create_user){
          this.create_user=create_user;
    }

	public String getSecu_in_accid() {
		return secu_in_accid;
	}

	public void setSecu_in_accid(String secu_in_accid) {
		this.secu_in_accid = secu_in_accid;
	}

	public String getSecu_out_accid() {
		return secu_out_accid;
	}

	public void setSecu_out_accid(String secu_out_accid) {
		this.secu_out_accid = secu_out_accid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtSetInstSecu [r_cost_fullprice_actual=");
		builder.append(r_cost_fullprice_actual);
		builder.append(", secu_instr_id=");
		builder.append(secu_instr_id);
		builder.append(", status=");
		builder.append(status);
		builder.append(", instr_id=");
		builder.append(instr_id);
		builder.append(", buss_type=");
		builder.append(buss_type);
		builder.append(", i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", i_name=");
		builder.append(i_name);
		builder.append(", p_class=");
		builder.append(p_class);
		builder.append(", book_flag=");
		builder.append(book_flag);
		builder.append(", handorclose_no=");
		builder.append(handorclose_no);
		builder.append(", erase_status=");
		builder.append(erase_status);
		builder.append(", ext_confirmno=");
		builder.append(ext_confirmno);
		builder.append(", beg_date=");
		builder.append(beg_date);
		builder.append(", end_date=");
		builder.append(end_date);
		builder.append(", r_cost_fee=");
		builder.append(r_cost_fee);
		builder.append(", r_cost_ai=");
		builder.append(r_cost_ai);
		builder.append(", r_cost_netprice=");
		builder.append(r_cost_netprice);
		builder.append(", r_cost_ai_actual=");
		builder.append(r_cost_ai_actual);
		builder.append(", r_cost_netprice_actual=");
		builder.append(r_cost_netprice_actual);
		builder.append(", r_cost_fullprice=");
		builder.append(r_cost_fullprice);
		builder.append(", r_amount=");
		builder.append(r_amount);
		builder.append(", r_cost_ai_his_actual=");
		builder.append(r_cost_ai_his_actual);
		builder.append(", accounting_busstype=");
		builder.append(accounting_busstype);
		builder.append(", accounting_user=");
		builder.append(accounting_user);
		builder.append(", accounting_time=");
		builder.append(accounting_time);
		builder.append(", party_zzd_accid=");
		builder.append(party_zzd_accid);
		builder.append(", self_zzd_accid=");
		builder.append(self_zzd_accid);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", update_time=");
		builder.append(update_time);
		builder.append(", update_user=");
		builder.append(update_user);
		builder.append(", create_time=");
		builder.append(create_time);
		builder.append(", create_user=");
		builder.append(create_user);
		builder.append(", secu_in_accid=");
		builder.append(secu_in_accid);
		builder.append(", secu_out_accid=");
		builder.append(secu_out_accid);
		builder.append(", p_trade_id=");
		builder.append(p_trade_id);
		builder.append(", p1=");
		builder.append(p1);
		builder.append(", p2=");
		builder.append(p2);
		builder.append("]");
		return builder.toString();
	}
    
}