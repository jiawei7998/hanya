package com.singlee.capital.settle.mapper;

import com.singlee.capital.settle.model.TtSetInstCashCheck;
import com.singlee.capital.settle.pojo.SettleInstCashCheckVo;

import java.util.List;
import java.util.Map;


/**
 * 结算 资金指令 dao
 * @author LyonChen
 *
 */
public interface SettleInstCashCheckMapper {
	
	List<TtSetInstCashCheck> searchSetInstCashCheck(Map<String,Object> map);

	List<SettleInstCashCheckVo> searchCashCheckVos(Map<String,Object> map);
	/**
	 * 增加
	 * @param mainSetInst
	 */
	void insertSetInstCashCheck(TtSetInstCashCheck cashCheck);

	/**
	 * 删除
	 * @param mainSetInst
	 */
	void deleteSetInstCashCheck(String cashInstId);
	/**
	 * 删除
	 * @param instrId 主指令ID
	 */
	void delInstCashCheckByInstrId(String instrId);
	/**
	 * 修改
	 * @param cashCheck
	 */
	void updateSetInstCashCheck(TtSetInstCashCheck cashCheck);
}
