package com.singlee.capital.settle.service.impl.clear;

import com.singlee.capital.base.util.KeyValuePair;
import com.singlee.capital.common.util.JY;

import java.util.ArrayList;
import java.util.List;

/**
 * 指令 工厂 负责 确定具体的AbstractInstruction实现，并调用实现的SaveInstruction()
 * 
 * @author LyonChen
 * 
 */
public class TrdClearFactory {
	public static final List<KeyValuePair<List<String>, Class<? extends AbstractTrdClear>>> CLEAR_CONFIG = new ArrayList<KeyValuePair<List<String>, Class<? extends AbstractTrdClear>>>();
	/**
	 * 单态
	 * 
	 * @author LyonChen
	 * 
	 */
	private static class SingletonHolder {
		private static final TrdClearFactory INSTANCE = new TrdClearFactory();
	}

	private TrdClearFactory() {
	}

	public static final TrdClearFactory getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public AbstractTrdClear getTrdClearObj(String bussType) {
		AbstractTrdClear ret = null;
		for (KeyValuePair<List<String>, Class<? extends AbstractTrdClear>> pair : CLEAR_CONFIG) {
			if (pair.key().contains(bussType)) {
				Class<? extends AbstractTrdClear> clazz = pair.value();
				try {
					ret = clazz.newInstance();
				} catch (InstantiationException e) {
					JY.raise("无法实例化" + clazz.getName() + ": " + e.getMessage());
				} catch (IllegalAccessException e) {
					JY.raise("无法实例化" + clazz.getName() + ": " + e.getMessage());
				}
				break;
			}
		}
		if (ret == null) {
			JY.raise("无法实例化指令对象，交易类型" + bussType + "没有被支持");
		}
		// 日志记录
		return ret;
	}
}
