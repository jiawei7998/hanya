package com.singlee.capital.settle.large.service;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.large.model.TtSettleLargeInfoMap;

import java.util.List;
import java.util.Map;
/**
 * 大额来往账映射service
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
public interface SettleLargeInfoMapService {


	/**
	 * 根据参数map分页查询列表
	 * 
	 * @param params - 参数map
	 * @return Page<TtSettleLargeInfoMap> 分页大额来往账对象列表
	 * @author lihb
	 * @date 2016-7-21
	 */
	Page<TtSettleLargeInfoMap> pageSettleLargeInfoMaps(Map<String,Object> params);
	
	/**
	 * 根据参数map分页查询列表
	 * 
	 * @param params - 参数map
	 * @return List<TtSettleLargeInfoMap> 分页大额来往账映射对象列表
	 * @author lihb
	 * @date 2016-7-21
	 */
	List<TtSettleLargeInfoMap> searchSettleLargeInfoMaps(Map<String,Object> params);
	
	/**
	 * 新增用户
	 * 
	 * @param largeInfoMap - 用户对象
	 * @author lihb
	 * @date 2016-7-21
	 */
	void createLargeInfoMap(TtSettleLargeInfoMap largeInfoMap);
	
	/**
	 * 新增用户
	 * 
	 * @param largeInfoMap - 用户对象
	 * @author lihb
	 * @date 2016-7-21
	 */
	void createLargeInfoMaps(String largeId,List<TtSettleLargeInfoMap> largeInfoMaps);
	
	/**
	 * 修改用户
	 * 
	 * @param largeInfoMap - 用户对象
	 * @author lihb
	 * @date 2016-7-21
	 */
	void updateLargeInfoMap(TtSettleLargeInfoMap largeInfoMap);
	
	/**
	 * 删除用户
	 * 
	 * @param largeId - 主键
	 * @author lihb
	 * @date 2016-7-21
	 */
	void deleteLargeInfoMap(String[] largeIds);
}
