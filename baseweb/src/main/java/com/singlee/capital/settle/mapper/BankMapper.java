package com.singlee.capital.settle.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.model.BankTrade;
import org.apache.ibatis.session.RowBounds;
import org.springframework.web.bind.annotation.RequestBody;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface BankMapper extends Mapper<BankTrade>{
	/**
	 * 查询银行信息
	 * @author wzm
	 * @date 2016-11-14
	 * @return
	 */
	public Page<BankTrade> queryBankList(@RequestBody Map<String,Object> params,RowBounds rb);
}
