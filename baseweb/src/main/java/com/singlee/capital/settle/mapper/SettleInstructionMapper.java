package com.singlee.capital.settle.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.model.TtSetInst;
import com.singlee.capital.settle.pojo.SettleInstVo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;


/**
 * 结算主指令 dao
 * 
 * @author LyonChen
 * 
 */
public interface SettleInstructionMapper extends Mapper<TtSetInst>{

	/**
	 * 结算指令查询，根据参数查询结算指令信息
	 * 
	 * @return
	 */
	List<SettleInstVo> searchSetInst(Map<String, Object> param);

	int countSetInst(Map<String, Object> param);

	/**
	 * 获得主指令序列号
	 * 
	 * @return
	 */
	String getSetInstSequenceId();

	/**
	 * 增加
	 * 
	 * @param mainSetInst
	 */
	void insertSetInst(TtSetInst mainSetInst);

	/**
	 * 修改
	 * 
	 * @param mainSetInst
	 */
	void updateSetInst(TtSetInst mainSetInst);

	/**
	 * 删除
	 * 
	 * @param instrId
	 *            主指令ID
	 */
	void deleteMainInstByInstrId(String instrId);

	/**
	 * 前台结算列表查询
	 * @param map参数：
	 */
	List<SettleInstVo> selectSettleInstList(Map<String, Object> map);

	/** 记录数，参数同select */
	int count(Map<String, Object> map);

	/**
	 * 主指令加锁
	 * @param instrIdList 主指令ID列表
	 */
	List<SettleInstVo> selectForLock(Map<String, Object> map);
	/**
	 * 查询券前置指令
	 * @param map
	 * @return
	 */
	List<SettleInstVo> selectPreInst(Map<String, Object> map);
	/**
	 * 查询活期收息前置指令
	 * @param map
	 * @return
	 */
	List<SettleInstVo> selectPreIBCurrentInst(Map<String, Object> map);
	
	/**
	 * 带券金融工具的主指令查询
	 * @param map
	 * @return
	 */
	List<SettleInstVo> searchSetInstWithInstrument(Map<String, Object> map);
	
	/**
	 * 分页查询结算指令
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SettleInstVo> pageSettleInsts(Map<String, Object> map, RowBounds rb);
	/**
	 * 
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SettleInstVo> pageVerifySettles(Map<String, Object> map, RowBounds rb);
}
