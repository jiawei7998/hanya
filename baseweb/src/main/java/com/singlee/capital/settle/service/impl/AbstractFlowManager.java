package com.singlee.capital.settle.service.impl;

import com.singlee.capital.settle.pojo.SettleData;
import com.singlee.capital.settle.service.impl.flow.ISettleFlow;

import java.util.List;

public abstract class AbstractFlowManager {
	
	
	/**
	 * 生成结算/抹账流程
	 * @param data 结算数据
	 * @return
	 */
	public abstract List<ISettleFlow> generateSettleFlows(SettleData data);
	
	/**
	 * 执行结算/抹账流程
	 * @param data 结算数据
	 * @return
	 */
	public abstract void executeSettleFlows(List<ISettleFlow> flowList,SettleData data);
	
	
}
