package com.singlee.capital.settle.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.model.TtSetInstSecu;
import com.singlee.capital.settle.pojo.SettleInstSecuVo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 结算券指令 dao
 * @author LyonChen
 *
 */
public interface SettleInstSecuMapper {

	/**
	 * map 参数内部
	 * 	inst_id	主指令号 允许为空
	 * begin_num 开始位置
	 * end_num 结束位置
	 * @return
	 */
	List<SettleInstSecuVo> searchSetInstSecu(Map<String, Object> param);
	int countSetInstSecu(Map<String, Object> param);
	/**
	 * 获得券指令序列号
	 * @return
	 */
	String getSetInstSecuSequenceId();

	/**
	 * 增加
	 * @param mainSetInst
	 */
	void insertSetInstSecu(TtSetInstSecu secuSetInsts);

	/**
	 * 修改
	 * @param mainSetInst
	 */
	void updateSetInstSecu(TtSetInstSecu secuSetInsts);
	/**
	 * 删除
	 * @param instrId
	 */
	void delSecuInstByInstrId(String instrId);
	/**
	 * 券指令加锁
	 * @param secuInstrIdList 券指令ID列表
	 */
	List<SettleInstSecuVo> selectForLock(Map<String, Object> map);
	
	/**
	 * 分页查询结算指令
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SettleInstSecuVo> pageSettleInstSecus(Map<String, Object> map, RowBounds rb);
}
