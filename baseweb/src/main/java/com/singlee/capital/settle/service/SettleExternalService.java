package com.singlee.capital.settle.service;
import com.singlee.capital.settle.pojo.SettleData;
import com.singlee.capital.settle.pojo.SettleInstSecuVo;
import com.singlee.capital.settle.pojo.SettleInstVo;
import com.singlee.capital.settle.pojo.TrdBalanceSecuVo;
import com.singlee.capital.trade.model.TtTrdTrade;

public interface SettleExternalService {
	
	void beforeInstrSettleHandle(SettleData settleData);
	
	void afterInstrSettleHandle(SettleData settleData);
	
	void beforeInstrEarseHandle(SettleData settleData);
	
	void afterInstrEarseHandle(SettleData settleData);
	
	String getBookInstId(SettleData settleData);
	
	boolean isSettleManageRefuse(SettleInstVo settleInst);
	
	boolean isSettle(TtTrdTrade ttTrdTrade);
	
	void beforeBalanceAdjust(SettleData settleData,String settleOperateType);
	
	void afterBalanceAdjust(SettleData settleData,String settleOperateType);
	
	void accountingExt(SettleData settleData);
	
	void generateRecordsExt(SettleData settleData);
	
	void unAccountingExt(SettleData settleData);
	
	void unGenerateRecordsExt(SettleData settleData);
	
	void instructionClearExt(SettleInstVo settleInst, String settleOperateType,SettleInstSecuVo secuInst,TrdBalanceSecuVo secuBalance);
	
}
