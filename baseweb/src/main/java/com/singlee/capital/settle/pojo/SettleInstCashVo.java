package com.singlee.capital.settle.pojo;

import com.singlee.capital.settle.model.TtSetInstCash;


public class SettleInstCashVo extends TtSetInstCash {
	/** 实际收付款日期 **/
	private String settle_date;
	/** 理论收付款日期 **/
	private String cal_date;
	/** 实际结算日期 **/
	private String settle_finish_date;
	/** 实际结算时间 **/
	private String settle_finish_time;
	/** 来账金额 **/
	private double lrcv_amount;
	/** 结算手续费 **/
	private double lrcv_fee;
	
	/** 内部账号 **/
	private String inner_acccode;
	/** 内部户名 **/
	private String inner_accname;
	
	public String getSettle_date() {
		return settle_date;
	}

	public void setSettle_date(String settle_date) {
		this.settle_date = settle_date;
	}

	public String getSettle_finish_time() {
		return settle_finish_time;
	}

	public void setSettle_finish_time(String settle_finish_time) {
		this.settle_finish_time = settle_finish_time;
	}

	public String getSettle_finish_date() {
		return settle_finish_date;
	}

	public void setSettle_finish_date(String settle_finish_date) {
		this.settle_finish_date = settle_finish_date;
	}

	public String getCal_date() {
		return cal_date;
	}

	public void setCal_date(String cal_date) {
		this.cal_date = cal_date;
	}


	public double getLrcv_amount() {
		return lrcv_amount;
	}

	public void setLrcv_amount(double lrcv_amount) {
		this.lrcv_amount = lrcv_amount;
	}

	public double getLrcv_fee() {
		return lrcv_fee;
	}

	public void setLrcv_fee(double lrcv_fee) {
		this.lrcv_fee = lrcv_fee;
	}


	public String getInner_acccode() {
		return inner_acccode;
	}

	public void setInner_acccode(String inner_acccode) {
		this.inner_acccode = inner_acccode;
	}

	public String getInner_accname() {
		return inner_accname;
	}

	public void setInner_accname(String inner_accname) {
		this.inner_accname = inner_accname;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SettleInstCashVo [settle_date=");
		builder.append(settle_date);
		builder.append(", cal_date=");
		builder.append(cal_date);
		builder.append(", settle_finish_date=");
		builder.append(settle_finish_date);
		builder.append(", settle_finish_time=");
		builder.append(settle_finish_time);
		builder.append(", lrcv_amount=");
		builder.append(lrcv_amount);
		builder.append(", lrcv_fee=");
		builder.append(lrcv_fee);
		builder.append(", inner_acccode=");
		builder.append(inner_acccode);
		builder.append(", inner_accname=");
		builder.append(inner_accname);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
