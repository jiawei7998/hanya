package com.singlee.capital.settle.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.settle.model.TtInstrumentCashFlow;
import com.singlee.capital.settle.pojo.InstrumentCashflowVo;

import java.util.List;
import java.util.Map;

public interface InstrumentCashflowService {
	/**
	 * 根据条件查询金融工具现金流
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * 
	 * @return
	 */
	Page<InstrumentCashflowVo> getInstrumentCashflowList(Map<String, Object> map, int pageNum, int pageSize);
	/**
	 * 根据条件查询金融工具现金流
	 * @return
	 */
	List<InstrumentCashflowVo> getCashflowForSettle(String iCode,String aType,String mType,String curDate);
	/**
	 * 调整金融工具现金流
	 * 
	 */
	void adjustInstrumentCashflow(InstrumentId instrumentId,String begDate,String endDate,boolean isManualFlag);
	/**
	 * 保存金融工具现金流
	 * @param instrumentCashflow 金融工具现金流对象
	 * @return
	 */
	TtInstrumentCashFlow addInstrumentCashFlow(TtInstrumentCashFlow instrumentCashflow);
	
	/**
	 * 修改金融工具现金流
	 * @param instrumentCashflow
	 */
	void modifyInstrumentCashFlow(TtInstrumentCashFlow instrumentCashflow);
	
	/**
	 * 删除金融工具现金流
	 * @param instrumentCashflow   金融工具现金流ID
	 */
	void delInstrumentCashFlow(TtInstrumentCashFlow instrumentCashflow);
}
