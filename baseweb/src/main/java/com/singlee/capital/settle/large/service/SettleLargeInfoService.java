package com.singlee.capital.settle.large.service;

import com.github.pagehelper.Page;
import com.singlee.capital.settle.large.model.TtSettleLargeInfo;
import com.singlee.capital.settle.large.model.TtSettleLargeInfoMap;

import java.util.List;
import java.util.Map;
/**
 * 大额来往账service
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
public interface SettleLargeInfoService {


	/**
	 * 根据参数map分页查询列表
	 * 
	 * @param params - 参数map
	 * @return Page<TtSettleLargeInfo> 分页大额来往账对象列表
	 * @author lihb
	 * @date 2016-7-21
	 */
	Page<TtSettleLargeInfo> pageSettleLargeInfos(Map<String,Object> params);
	/**
	 * 根据参数map查询列表
	 * 
	 * @param params - 参数map
	 * @return List<TtSettleLargeInfo> 分页大额来往账对象列表
	 * @author lihb
	 * @date 2016-7-21
	 */
	List<TtSettleLargeInfo> getSettleLargeInfos(Map<String,Object> params);
	
	/**
	 * 新增用户
	 * 
	 * @param largeInfo - 用户对象
	 * @author lihb
	 * @date 2016-7-21
	 */
	void createLargeInfo(TtSettleLargeInfo largeInfo);
	
	/**
	 * 修改用户
	 * 
	 * @param largeInfo - 用户对象
	 * @author lihb
	 * @date 2016-7-21
	 */
	void updateLargeInfo(TtSettleLargeInfo largeInfo);
	
	/**
	 * 删除用户
	 * 
	 * @param largeId - 主键
	 * @author lihb
	 * @date 2016-7-21
	 */
	void deleteLargeInfo(String[] largeIds);
	/**
	 * 发送前台确认
	 * 
	 * @param largeIds - 主键
	 * @author lihb
	 * @date 2016-7-21
	 */
	void sendLargeInfo(String[] largeIds,String opUserId);
	/**
	 * 前台确认
	 * @param largeId
	 * @param largeInfoMaps
	 * @param opUserId
	 */
	void confirmLargeInfo(String largeId,List<TtSettleLargeInfoMap> largeInfoMaps,String opUserId);
	/**
	 * 回退
	 * @param remark
	 * @param largeIds
	 * @param opUserId
	 */
	void returnLargeInfos(String remark,List<String> largeIds,String opUserId);
	/**
	 * 作废
	 * @param remark
	 * @param largeIds
	 * @param opUserId
	 */
	void cancelLargeInfos(String remark,List<String> largeIds,String opUserId);
	/**
	 * 同步
	 */
	void synLargeInfos(String opUserId,String instId);
}
