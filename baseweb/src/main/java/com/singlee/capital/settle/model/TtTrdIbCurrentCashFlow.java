package com.singlee.capital.settle.model;

/**
 * TT_TRD_IBCURRENT_CASHFLOW      活期现金流表
 * @author   Libo
 * Create Time  2013-05-28 16:09:03
 */
public class TtTrdIbCurrentCashFlow {

	/**  现金流ID  */
	private String cashflowid;

	/**  账户ID  */
	private String accid;

	/**  实际付息日  */
	private String pay_date;
	
	/**  理论付息日  */
	private String cal_date;

	/**  金额  */
	private double aiamount;

	/**  利率类型  */
	private String rate_type;

	/**  收付方向:PAY-付 RECEIVE-收  */
	private String buss_type;

	/**  业务类型  */
	private String trdtype;

	/**-------  Generate Getter And Setter   --------**/

	public String getCashflowid() {
		return this.cashflowid;
	}

	public void setCashflowid(String cashflowid) {
		this.cashflowid = cashflowid;
	}

	public String getAccid() {
		return this.accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getCal_date() {
		return this.cal_date;
	}

	public void setCal_date(String cal_date) {
		this.cal_date = cal_date;
	}

	public double getAiamount() {
		return this.aiamount;
	}

	public void setAiamount(double aiamount) {
		this.aiamount = aiamount;
	}

	public String getRate_type() {
		return this.rate_type;
	}

	public void setRate_type(String rate_type) {
		this.rate_type = rate_type;
	}

	public String getBuss_type() {
		return this.buss_type;
	}

	public void setBuss_type(String buss_type) {
		this.buss_type = buss_type;
	}

	public String getTrdtype() {
		return this.trdtype;
	}

	public void setTrdtype(String trdtype) {
		this.trdtype = trdtype;
	}

	public String getPay_date() {
		return pay_date;
	}

	public void setPay_date(String pay_date) {
		this.pay_date = pay_date;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtTrdIbCurrentCashFlow [cashflowid=");
		builder.append(cashflowid);
		builder.append(", accid=");
		builder.append(accid);
		builder.append(", pay_date=");
		builder.append(pay_date);
		builder.append(", cal_date=");
		builder.append(cal_date);
		builder.append(", aiamount=");
		builder.append(aiamount);
		builder.append(", rate_type=");
		builder.append(rate_type);
		builder.append(", buss_type=");
		builder.append(buss_type);
		builder.append(", trdtype=");
		builder.append(trdtype);
		builder.append("]");
		return builder.toString();
	}


	
	
}