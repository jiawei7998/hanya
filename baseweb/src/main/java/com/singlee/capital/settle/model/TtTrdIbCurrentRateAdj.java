package com.singlee.capital.settle.model;


/**
 * TT_TRD_IBCURRENT_RATE_ADJ 同业活期利率调整表
 * 
 * @author Libo Create Time 2013-08-30 11:44:25
 */

public class TtTrdIbCurrentRateAdj {
	
	private String pkid;
	
	/** 产品分类--存放同业，同业存放 */
	private String p_type;

	/** 账户编号 */
	private String accid;

	/** 活期利率 */
	private double rate;

	/** 开始日期 */
	private String beg_date;

	/** 结束日期 缺省即为2099-12-31*/
	private String end_date ;

	/** 状态 */
	private String approve_status;

	/** 创建者 */
	private String createuser;

	/** 创建时间 */
	private String createtime;

	/** 审核者 */
	private String approver;

	/** 审核时间 */
	private String approvetime;

	/** ------- Generate Getter And Setter -------- **/

	public String getPkid() {
		return pkid;
	}

	public void setPkid(String pkid) {
		this.pkid = pkid;
	}

	public String getP_type() {
		return this.p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public String getAccid() {
		return this.accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public double getRate() {
		return this.rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getBeg_date() {
		return this.beg_date;
	}

	public void setBeg_date(String beg_date) {
		this.beg_date = beg_date;
	}

	public String getEnd_date() {
		return this.end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getApprove_status() {
		return this.approve_status;
	}

	public void setApprove_status(String approve_status) {
		this.approve_status = approve_status;
	}

	public String getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(String createuser) {
		this.createuser = createuser;
	}

	public String getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getApprover() {
		return this.approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getApprovetime() {
		return this.approvetime;
	}

	public void setApprovetime(String approvetime) {
		this.approvetime = approvetime;
	}

}