package com.singlee.capital.settle.service.impl.clear;

import com.singlee.capital.common.util.Constants;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.settle.pojo.SettleInstCashVo;
import com.singlee.capital.settle.pojo.TrdBalanceCashVo;
import com.singlee.capital.settle.pojo.TrdBalanceSecuVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 交易余额调整服务
 * @author lihb
 */
@Service("trdClearManager")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrdClearManager {
	/**
	 * 日间交易证券清算接口
	 * @param trade    交易
	 * @return 
	 */
	public List<TrdBalanceSecuVo> dayClearingForSecu(TrdTradeVo trade,String settleOperateType){
		AbstractTrdClear trdClear = getTrdClear(trade);
		if(trdClear == null) {
			return null;}
		if(settleOperateType.equals(Constants.settleOperateType.accounting)) {
			return trdClear.daySecuSettleClearing(trade);}
		else if(settleOperateType.equals(Constants.settleOperateType.earsing)) {
			return trdClear.daySecuEarseClearing(trade);}
		else {
			return null;}
	}
	/**
	 * 日间交易资金清算接口--通过资金指令进行清算
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public List<TrdBalanceCashVo> dayClearingForCash(TrdTradeVo trade,List<SettleInstCashVo> cashInstList,String settleOperateType){
		AbstractTrdClear trdClear = getTrdClear(trade);
		if(trdClear == null) {
			return null;}
		if(settleOperateType.equals(Constants.settleOperateType.accounting)) {
			return trdClear.dayCashSettleClearing(trade,cashInstList);}
		else if(settleOperateType.equals(Constants.settleOperateType.earsing)) {
			return trdClear.dayCashEarseClearing(trade,cashInstList);}
		else {
			return null;}
	}
	
	private AbstractTrdClear getTrdClear(TrdTradeVo trade){
		if(!StringUtil.isNullOrEmpty(trade.getTrdtype())) {
			return TrdClearFactory.getInstance().getTrdClearObj(trade.getTrdtype());}
		return null;
	}
	/**
	 * 日间交易资金清算接口--通过资金指令进行清算 限额使用
	 * @param trade    交易
	 * @param cashInstList   资金指令列表
	 * @return 
	 */
	public List<TrdBalanceCashVo> dayClearingForCash4limit(TrdTradeVo trade,List<SettleInstCashVo> cashInstList,String settleOperateType){
		AbstractTrdClear trdClear = getTrdClear(trade);
		if(trdClear == null) {
			return null;}
		if(settleOperateType.equals(Constants.settleOperateType.accounting)) {
			return trdClear.dayCashSettleClearing4limit(trade, cashInstList);}
		else if(settleOperateType.equals(Constants.settleOperateType.earsing)) {
			return trdClear.dayCashEarseClearing4limit(trade,cashInstList);}
		else {
			return null;}
	}
	/**
	 * 日间交易证券清算接口
	 * @param trade    交易
	 * @return 
	 */
	public List<Object> dayClearingForSecuExt(TrdTradeVo trade,String settleOperateType){
		AbstractTrdClear trdClear = getTrdClear(trade);
		if(trdClear == null) {
			return null;}
		if(settleOperateType.equals(Constants.settleOperateType.accounting)) {
			return trdClear.daySecuExtSettleClearing(trade);}
		else if(settleOperateType.equals(Constants.settleOperateType.earsing)) {
			return trdClear.daySecuExtEarseClearing(trade);}
		else {
			return null;}
	}
}
