package com.singlee.capital.settle.pojo;

public class SecuPositionKey {
	
	public SecuPositionKey(String iCode,String aType,String mType,String inSecuAccId,String outSecuAccId,String hICode,String hAType,String hMType){
		this.iCode = iCode;
		this.aType = aType;
		this.mType = mType;
		this.inSecuAccId = inSecuAccId;
		this.outSecuAccId = outSecuAccId;
		this.hICode = hICode;
		this.hAType = hAType;
		this.mType = hMType;
	}
	public SecuPositionKey(TrdBalanceSecuVo balance){
		aType = balance.getA_type();
		iCode = balance.getI_code();
		mType = balance.getM_type();
		inSecuAccId = balance.getInsecuaccid();
		outSecuAccId = balance.getOutsecuaccid();
		hICode = balance.getH_i_code();
		hAType = balance.getH_a_type();
		hMType = balance.getH_m_type();
	}
	public SecuPositionKey(String keyStr){
		String[] key = keyStr.split(",",-1);
		iCode = "".equals(key[0])?null:key[0];
		aType = "".equals(key[1])?null:key[1];
		mType = "".equals(key[2])?null:key[2];
		inSecuAccId = "".equals(key[3])?null:key[3];
		outSecuAccId = "".equals(key[4])?null:key[4];
		hICode = "".equals(key[5])?null:key[5];
		hAType = "".equals(key[6])?null:key[6];
		hMType = "".equals(key[7])?null:key[7];
	}
	private String aType;
	private String iCode;
	private String mType;
	private String inSecuAccId;
	private String outSecuAccId;
	private String hICode;
	private String hAType;
	private String hMType;
	
	public String gethICode() {
		return hICode;
	}
	public void sethICode(String hICode) {
		this.hICode = hICode;
	}
	public String gethAType() {
		return hAType;
	}
	public void sethAType(String hAType) {
		this.hAType = hAType;
	}
	public String gethMType() {
		return hMType;
	}
	public void sethMType(String hMType) {
		this.hMType = hMType;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String getInSecuAccId() {
		return inSecuAccId;
	}
	public void setInSecuAccId(String inSecuAccId) {
		this.inSecuAccId = inSecuAccId;
	}
	public String getOutSecuAccId() {
		return outSecuAccId;
	}
	public void setOutSecuAccId(String outSecuAccId) {
		this.outSecuAccId = outSecuAccId;
	}
	public String getKeyStr(){
		return new StringBuilder(this.iCode==null?"":this.iCode).append(",").append(this.aType==null?"":this.aType).
				append(",").append(this.mType==null?"":this.mType).append(",").append(this.inSecuAccId==null?"":this.inSecuAccId).append(",").
				append(this.outSecuAccId==null?"":this.outSecuAccId).append(",").append(this.hICode==null?"":this.hICode).append(",").
				append(this.hAType==null?"":this.hAType).append(",").append(this.hMType==null?"":this.hMType).toString();
	}
}
