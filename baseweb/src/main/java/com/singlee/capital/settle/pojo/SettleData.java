package com.singlee.capital.settle.pojo;

import java.io.Serializable;
import java.util.List;

public class SettleData implements Cloneable,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 核算日期 **/
	private String settleDate;
	/** 核算任务 **/
	private String settleTaskId;
	/** 是否日间核算 **/
	private boolean isDaySettle;
	/** 是否核算到实际 **/
	private boolean isActualSettle;
	/** 结算平台 **/
	private String settlePlatform;
	/** 资金流动方向 **/
	private String cashBussType;
	/** 操作员 **/
	private String operUser;
	/** 操作员所在机构 **/
	private String operInstId;
	/** 授权员 **/
	private String authUser;
	/** 记账机构代码 **/
	private String bookKeepingInstId;
	/** 记账柜员 **/
	private String bookKeepingUserId;
	/** 分录机构代码 **/
	private String mappingInstId;
	/** 结算指令包 **/
	private List<SettleInstPackage> settlePackageList;
	/** 允许发送支付的资金指令 **/
	private List<SettleInstCashVo> cashInstList;
	/** 是否预核算 **/
	private boolean isPreAccounting;
	
	
	public String getSettleDate() {
		return settleDate;
	}


	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}


	public String getSettleTaskId() {
		return settleTaskId;
	}


	public void setSettleTaskId(String settleTaskId) {
		this.settleTaskId = settleTaskId;
	}


	public boolean isDaySettle() {
		return isDaySettle;
	}


	public void setDaySettle(boolean isDaySettle) {
		this.isDaySettle = isDaySettle;
	}


	public boolean isActualSettle() {
		return isActualSettle;
	}


	public void setActualSettle(boolean isActualSettle) {
		this.isActualSettle = isActualSettle;
	}


	public String getSettlePlatform() {
		return settlePlatform;
	}


	public void setSettlePlatform(String settlePlatform) {
		this.settlePlatform = settlePlatform;
	}


	public String getCashBussType() {
		return cashBussType;
	}


	public void setCashBussType(String cashBussType) {
		this.cashBussType = cashBussType;
	}


	public String getOperUser() {
		return operUser;
	}


	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}


	public String getOperInstId() {
		return operInstId;
	}


	public void setOperInstId(String operInstId) {
		this.operInstId = operInstId;
	}


	public String getAuthUser() {
		return authUser;
	}


	public void setAuthUser(String authUser) {
		this.authUser = authUser;
	}


	public String getBookKeepingInstId() {
		return bookKeepingInstId;
	}


	public void setBookKeepingInstId(String bookKeepingInstId) {
		this.bookKeepingInstId = bookKeepingInstId;
	}


	public String getMappingInstId() {
		return mappingInstId;
	}


	public void setMappingInstId(String mappingInstId) {
		this.mappingInstId = mappingInstId;
	}


	public List<SettleInstPackage> getSettlePackageList() {
		return settlePackageList;
	}


	public void setSettlePackageList(List<SettleInstPackage> settlePackageList) {
		this.settlePackageList = settlePackageList;
	}


	public List<SettleInstCashVo> getCashInstList() {
		return cashInstList;
	}


	public void setCashInstList(List<SettleInstCashVo> cashInstList) {
		this.cashInstList = cashInstList;
	}


	public String getBookKeepingUserId() {
		return bookKeepingUserId;
	}


	public void setBookKeepingUserId(String bookKeepingUserId) {
		this.bookKeepingUserId = bookKeepingUserId;
	}

	public boolean isPreAccounting() {
		return isPreAccounting;
	}


	public void setPreAccounting(boolean isPreAccounting) {
		this.isPreAccounting = isPreAccounting;
	}


	@Override
	public Object clone() throws CloneNotSupportedException {
//		try {
//			//序列化当前对象到字节数组
//			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//			ObjectOutputStream oos = new ObjectOutputStream(bos);
//			oos.writeObject(this);
//			//反序列化
//			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
//			ObjectInputStream ois = new ObjectInputStream(bis);
//			SettleData settleData = (SettleData)ois.readObject();
//			return settleData;
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new CloneNotSupportedException("流io错误" + e.getMessage());
//		}
		return super.clone();
	}
	
}
