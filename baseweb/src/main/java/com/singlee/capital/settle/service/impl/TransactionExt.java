package com.singlee.capital.settle.service.impl;

import com.singlee.capital.common.spring.SpringContextHolder;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class TransactionExt {
	private static String dbType;
	private int propagationBehavior;
	private DefaultTransactionDefinition transDef;
	private DataSourceTransactionManager transactionManager;
	
	static{
		dbType = "ORACLE";
	}
	public TransactionExt(){
		this.propagationBehavior = TransactionDefinition.PROPAGATION_REQUIRED;
		transDef = new DefaultTransactionDefinition(propagationBehavior);
		transactionManager = (DataSourceTransactionManager) SpringContextHolder.getBean("transactionManager");
		transDef.setPropagationBehavior(propagationBehavior);
	}
	public TransactionExt(int propagationBehavior){
		this.propagationBehavior = propagationBehavior;
		if("ORACLE".equals(dbType)){
			transDef = new DefaultTransactionDefinition(propagationBehavior);
			transactionManager = (DataSourceTransactionManager) SpringContextHolder.getBean("transactionManager");
			transDef.setPropagationBehavior(propagationBehavior);
		}
	}
	
	public void commit(TransactionStatus transactionStatus){
		transactionManager.commit(transactionStatus);
	}
	
	public void rollback(TransactionStatus transactionStatus){
		transactionManager.rollback(transactionStatus);
	}
	
	public int getPropagationBehavior() {
		return propagationBehavior;
	}
	
	public TransactionStatus getTransactionStatus(){
		return transactionManager.getTransaction(transDef);
	}
}
