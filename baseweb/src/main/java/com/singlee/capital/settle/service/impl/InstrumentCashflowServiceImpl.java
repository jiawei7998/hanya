package com.singlee.capital.settle.service.impl;

import com.github.pagehelper.Page;
import com.joyard.jc.cashflow.CashFlow;
import com.joyard.jc.cashflow.SpecialInterestCashFlow;
import com.joyard.jc.instrument.InstrumentFactory;
import com.joyard.jc.instrument.InstrumentInterface;
import com.joyard.jc.time.Date;
import com.joyard.jc.util.QLUtils;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.settle.mapper.InstrumentCashflowMapper;
import com.singlee.capital.settle.model.TtInstrumentCashFlow;
import com.singlee.capital.settle.pojo.InstrumentCashflowVo;
import com.singlee.capital.settle.service.InstrumentCashflowService;
import com.singlee.capital.system.dict.DictConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("instrumentCashflowService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class InstrumentCashflowServiceImpl implements InstrumentCashflowService {
	@Autowired
	private InstrumentCashflowMapper instrumentCashflowDao;

	@Override
	public Page<InstrumentCashflowVo> getInstrumentCashflowList(
			Map<String, Object> map, int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		Page<InstrumentCashflowVo> ph = new Page<InstrumentCashflowVo>();
		instrumentCashflowDao.count(map);
		return ph;
	}
	
	@Override
	public List<InstrumentCashflowVo> getCashflowForSettle(String iCode,
			String aType, String mType, String curDate) {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("i_code", iCode);
		map.put("a_type", aType);
		map.put("m_type", mType);
		map.put("curDate", curDate);
		return instrumentCashflowDao.selectCashflowForSettle(map);
	}

	@Override
	public void adjustInstrumentCashflow(InstrumentId instrumentId, String begDate, String endDate, boolean isManualFlag) {
		// TODO Auto-generated method stub
		List<InstrumentCashflowVo> cashflowList = getCashflowDataFromJCompute(instrumentId,begDate,isManualFlag);
		for(InstrumentCashflowVo cashflowData : cashflowList){
			if((cashflowData.getPay_date().compareTo(begDate) >= 0 && cashflowData.getPay_date().compareTo(endDate) <= 0) || 
			   (cashflowData.getCal_date().compareTo(begDate) >= 0 && cashflowData.getCal_date().compareTo(endDate) <= 0)){
				//保存现金流
				saveInstrumentCashflow(cashflowData);
			}
		}
	}
	public void saveInstrumentCashflow(InstrumentCashflowVo cashflowVo){
		//判断当前现金流是否存在
		if(isExist(cashflowVo)){
			modifyInstrumentCashFlow(cashflowVo);
		}
		else{
			addInstrumentCashFlow(cashflowVo);
		}
	}
	public boolean isExist(InstrumentCashflowVo cashflowVo){
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("i_code", cashflowVo.getI_code());
		map.put("a_type", cashflowVo.getA_type());
		map.put("m_type", cashflowVo.getM_type());
		map.put("pay_date", cashflowVo.getPay_date());
		Page<InstrumentCashflowVo> ph = getInstrumentCashflowList(map,1,1000);
		if(ph == null) {
			return false;}
		if(ph.getTotal() > 0) {
			return true;}
		else {
			return false;}
	}
	public List<InstrumentCashflowVo> getCashflowDataFromJCompute(InstrumentId instrumentId, String valueDate,boolean isManualFlag){
		List<InstrumentCashflowVo> retList = new ArrayList<InstrumentCashflowVo>();
		try{
			//调用计算接口获取现金流信息
			InstrumentInterface instrumentExt = InstrumentFactory.createInstrument(instrumentId);
			Date baseDate = QLUtils.parseDate(DateUtil.dateAdjust(valueDate, 1));
			if(instrumentExt.isTheoryCashFlowDate(baseDate)){
				CashFlow cashflow = instrumentExt.cashFlow(baseDate); 
				boolean isTerminalFlag = instrumentExt.isMaturityDate(baseDate);
				InstrumentCashflowVo cashflowData = new InstrumentCashflowVo();
				if(cashflow == null) {  
					throw new RException("金融工具现金流计算返回为空，金融工具编号:" + instrumentId.getI_code());}
				if(instrumentId.getA_type().equals(DictConstants.AType.IBFixed) ||
					instrumentId.getA_type().equals(DictConstants.AType.Money) ||
					instrumentId.getA_type().equals(DictConstants.AType.Assetplan)||
					instrumentId.getA_type().equals(DictConstants.AType.IBLb)){
					cashflowData.setI_code(instrumentId.getI_code());
					cashflowData.setA_type(instrumentId.getA_type());
					cashflowData.setM_type(instrumentId.getM_type());
					cashflowData.setPay_date(cashflow.getPaymentDate().toString());
					cashflowData.setCal_date(cashflow.getTheoryPaymentDate().toString());
					/** 
					 * 利息前置 部分的现金流处理 add by cz
					 * SpecialInterestCashFlow类型的现金流，需要考虑利息前置，实际应该发生的利息应该是剩余的利息。
					 * 
					 */
					cashflowData.setEst_interest(cashflow.getInterest()); // 预期利息部分
					cashflowData.setEst_principal(cashflow.getRepayPrincipal()); // 预期应付本金部分
					
					if(cashflow instanceof SpecialInterestCashFlow){
						SpecialInterestCashFlow sicf = (SpecialInterestCashFlow) cashflow;
						cashflowData.setAct_interest(sicf.getRemainInterest()); //这里是剩余的应付利息部分
					}else{
						cashflowData.setAct_interest(cashflow.getInterest()); //这里是应付利息部分
					}
					cashflowData.setAct_principal(cashflow.getRepayPrincipal()); // 这里应付本金部分
					
					cashflowData.setIs_manual(isManualFlag?"1":"0");
					cashflowData.setIs_terminal(isTerminalFlag?"1":"0");
					cashflowData.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
					retList.add(cashflowData);
				}
				else if(instrumentId.getA_type().equals(DictConstants.AType.Bond) || instrumentId.getA_type().equals(DictConstants.AType.BondAbs)){
					cashflowData.setI_code(instrumentId.getI_code());
					cashflowData.setA_type(instrumentId.getA_type());
					cashflowData.setM_type(instrumentId.getM_type());
					cashflowData.setPay_date(cashflow.getPaymentDate().toString());
					cashflowData.setCal_date(cashflow.getTheoryPaymentDate().toString());
					cashflowData.setAct_principal(cashflow.getRepayPrincipal()); // 这里应付本金部分
					cashflowData.setAct_interest(cashflow.getInterest()); //这里是应付利息部分
					cashflowData.setIs_manual(isManualFlag?"1":"0");
					cashflowData.setIs_terminal(isTerminalFlag?"1":"0");
					cashflowData.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
					retList.add(cashflowData);
				}
				else if(instrumentId.getA_type().equals(DictConstants.AType.Repo)){
					cashflowData.setI_code(instrumentId.getI_code());
					cashflowData.setA_type(instrumentId.getA_type());
					cashflowData.setM_type(instrumentId.getM_type());
					cashflowData.setPay_date(cashflow.getPaymentDate().toString());
					cashflowData.setCal_date(cashflow.getTheoryPaymentDate().toString());
					cashflowData.setAct_principal(cashflow.getRepayPrincipal()); // 这里应付本金部分
					cashflowData.setAct_interest(cashflow.getInterest()); //这里是应付利息部分
					cashflowData.setIs_manual(isManualFlag?"1":"0");
					cashflowData.setIs_terminal(isTerminalFlag?"1":"0");
					cashflowData.setUpdate_time(DateUtil.getCurrentDateTimeAsString());
					retList.add(cashflowData);
				}
			}
		}catch(Exception e){
			JY.warn(e);
			throw new RException(String.format("金融工具[I_CODE:{0},A_TYPE：{1}]获取现金流数据失败!错误信息：{2}",instrumentId.getI_code(),instrumentId.getA_type(),e.getMessage()));
		}
		return retList;
	}
	@Override
	public TtInstrumentCashFlow addInstrumentCashFlow(
			TtInstrumentCashFlow instrumentCashflow) {
		// TODO Auto-generated method stub
		instrumentCashflowDao.insert(instrumentCashflow);
		return instrumentCashflow;
	}
	@Override
	public void modifyInstrumentCashFlow(TtInstrumentCashFlow instrumentCashflow) {
		// TODO Auto-generated method stub
		instrumentCashflowDao.update(instrumentCashflow);
	}
	@Override
	public void delInstrumentCashFlow(TtInstrumentCashFlow instrumentCashflow) {
		// TODO Auto-generated method stub
		instrumentCashflowDao.delete(instrumentCashflow);
	}
}
