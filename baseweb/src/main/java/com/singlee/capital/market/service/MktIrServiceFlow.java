package com.singlee.capital.market.service;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.market.model.TtMktIrSeries;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * 
 */
@Service("mktIrServiceFlow")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MktIrServiceFlow {

	@Autowired
	private MktIrService mktIrService;
	
	public void create(String refId, String type, TtMktIrSeries t, String user,
			String inst) {
		refIdCheck(refId, type, t);
		if(StringUtils.isEmpty(refId)){
			refId = mktIrService.getSeriesSeq();
		}
		refId = null;
	}
	
	public void createAndOperate(String refId, String type, TtMktIrSeries t,
			String user, String inst) {
		refIdCheck(refId, type, t);
		if(StringUtils.isEmpty(refId)){
			refId = mktIrService.getSeriesSeq();
		}
	}

	private void refIdCheck(String refId, String type, TtMktIrSeries t){
		if("add".equals(type)){
		}else if("edit".equals(type)){
			JY.require(StringUtil.isNotEmpty(refId), "业务ID不能为空");
			JY.require(refId.equals(t.getS_id()), "传入的业务ID与实际ID不匹配");
		}
	}
	
	
}
