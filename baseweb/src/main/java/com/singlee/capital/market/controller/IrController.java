
package com.singlee.capital.market.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.market.model.TtMktIr;
import com.singlee.capital.market.model.TtMktIrSeries;
import com.singlee.capital.market.service.MktIrService;
import com.singlee.capital.market.service.MktIrServiceFlow;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 基本利率
 * 
 * @author cz
 * 
 */
@Controller
@RequestMapping(value = "/IrController")
public class IrController extends CommonController {

	/** 基准利率服务层 */
	@Autowired
	private MktIrService irService;
	
	/** 基准利率经办复核实现类 */
	@Autowired
	private MktIrServiceFlow mktIrServiceFlow;

	/**
	 * 基准利率分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIr")
	public RetMsg<PageInfo<TtMktIr>> searchPageIr(@RequestBody Map<String,String> params) {
		PageInfo<TtMktIr> page = irService.page(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 基准利率保存
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIr")
	public RetMsg<Serializable> saveIr(@RequestBody Map<String,String> params) {
		irService.save(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率删除
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIr")
	public RetMsg<Serializable> deleteIr(@RequestBody Map<String,String> params) {
		irService.delete(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率详情
	 * 
	 * @param map
	 * @return 用户对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectIr")
	public RetMsg<TtMktIr> selectIr(@RequestBody Map<String,String> params) {
		TtMktIr mktIr= irService.select(params);
		return RetMsgHelper.ok(mktIr);
	}
	
	/**
	 * 基准利率Series分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIrSeries")
	public RetMsg<PageInfo<TtMktIrSeries>> searchPageIrSeries(@RequestBody Map<String,String> params) {
		PageInfo<TtMktIrSeries> page = irService.pageSeries(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 基准利率Series保存
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIrSeries")
	public RetMsg<Serializable> saveIrSeries(@RequestBody Map<String,Object> params) {
		irService.saveSeries(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率Series删除
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIrSeries")
	public RetMsg<Serializable> deleteIrSeries(@RequestBody Map<String,Object> params) {
		irService.deleteSeries(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率Series详情
	 * 
	 * @param map
	 * @return 用户对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectIrSeries")
	public RetMsg<TtMktIrSeries> selectIrSeries(@RequestBody Map<String,Object> params) {
		TtMktIrSeries mktIrSeries= irService.selectSeries(params);
		return RetMsgHelper.ok(mktIrSeries);
	}
	
	/**
	 * 基准利率Series建立经办
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/createSeries")
	public RetMsg<Serializable> createSeries(@RequestBody Map<String,Object> params)  {
		TtMktIrSeries ttMktIrSeries = new TtMktIrSeries();
		ParentChildUtil.HashMapToClass(params, ttMktIrSeries);
		String refId = ttMktIrSeries.getS_id();
		String type = ParameterUtil.getString(params, "type", "");
		mktIrServiceFlow.create(refId, type, ttMktIrSeries, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId());
		return RetMsgHelper.ok();
		
	}
	
	
	/**
	 * 基准利率Series建立经办并提交复核
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/createAndOperateSeries")
	public RetMsg<Serializable> createAndOperateSeries(@RequestBody Map<String,Object> params) {
		TtMktIrSeries ttMktIrSeries = new TtMktIrSeries();
		ParentChildUtil.HashMapToClass(params, ttMktIrSeries);
		String refId = ttMktIrSeries.getS_id();
		String type = ParameterUtil.getString(params, "type", "");
		mktIrServiceFlow.createAndOperate(refId, type, ttMktIrSeries, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId());
		return RetMsgHelper.ok();
	}

}
