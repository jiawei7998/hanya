package com.singlee.capital.fibs.crbc;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.List;

/**
 * 字符串工具类
 * 
 * @author Gaolb
 */

public class FileUtil   {

	public static <T> void generateFile(String filepath, String filename, FileLineFmt<T> fmt, List<T> list, FileLineFmt<Integer> endFmt){
		File file = new File(filepath + File.separator + filename);
		Writer output = null;
		try {
			output = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
		} catch (IOException e) {
			IOUtils.closeQuietly(output);
			JY.raise("无法获得file的写操作 %s", filepath + File.separator  + filename, e.getMessage());
		}
		for(int i=0;i<list.size();i++){
			T o = list.get(i);
			String data = fmt.fmt(o);
			try {
				IOUtils.write(data, output);
				IOUtils.write("\r\n", output);
			} catch (IOException e) {
				IOUtils.closeQuietly(output);
				JY.raise("写入 %s 时第%d行发生 异常 %s ", filepath + File.separator  + filename, i+1, e.getMessage());
			}
		}

		String data = endFmt.fmt(list.size());
		if(!(StringUtil.isNullOrEmpty(data))){
			try {
				IOUtils.write(data, output);
				IOUtils.write("\r\n", output);
			} catch (IOException e) {
				IOUtils.closeQuietly(output);
				JY.raise("写入 %s 最后一行时 发生 异常 %s ", filepath + File.separator  + filename, e.getMessage());
			}
		}
		
		try {
			file.setReadable(false,false); 	//文件权限先清空
			file.setWritable(false,false);
			file.setExecutable(false,false);
			file.setReadable(true,false);// 让所有人都可以读！
			file.setWritable(true, true);
			output.flush();
		} catch (IOException e) {
			JY.raise("输出到文件时 异常 %s ", filepath + File.separator  + filename, e.getMessage());
		} finally{
			IOUtils.closeQuietly(output);
		}
		
	}
}