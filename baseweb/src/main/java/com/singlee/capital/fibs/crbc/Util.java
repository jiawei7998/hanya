package com.singlee.capital.fibs.crbc;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



public class Util {

	public String oamInstallPath = null;
	public String oamCookieName = "ObSSOCookie";
	public String USER_UM_ID = "userid";
	public String EMP_ID = "empid";
	public String checkCookie = "yes";
	public String isDebug = "yes";
	private String LOGINED_TIMES = "UM_LOGINED_TIMES";
	private boolean needcheckcookie = false;

	public void clearLoginedTimes(HttpSession session) {
		session.removeAttribute(LOGINED_TIMES);
	}
	
	public boolean ifLoginedToMainTimes(HttpSession session) {
		boolean flag = false;
		Object times = session.getAttribute(LOGINED_TIMES);
		if(times==null) {
			session.setAttribute(LOGINED_TIMES, 1);
		}else {
			int t = (Integer)times;
			if(t>10) {//超过10次，不再偿试登录
				flag=true;
			}else {
				t++;
				session.setAttribute(LOGINED_TIMES, t);
			}
		}
		return flag ;
	}
	
	public String getOamCookie(HttpServletRequest servletRequest) {
		String oamCookie = null;
		Cookie[] acookie = servletRequest.getCookies();
		if (acookie != null && acookie.length != 0) {
			for (int i1 = 0; i1 < acookie.length; i1++) {
				Cookie cookie = acookie[i1];
				if (oamCookieName.equalsIgnoreCase(cookie.getName())) {
					oamCookie = cookie.getValue();
					break;
				}
			}
		}
		return oamCookie;
	}

	public boolean checkOamCookie(String cookieValue, String userDN) {
		if (!needcheckcookie) {
			return true;}
		return needcheckcookie;
		
	}
}
