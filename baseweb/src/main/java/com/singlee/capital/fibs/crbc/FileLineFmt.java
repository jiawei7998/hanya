package com.singlee.capital.fibs.crbc;

public interface FileLineFmt<T> {
	
	String fmt(T o);

}
