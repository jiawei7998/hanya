package com.singlee.capital.trust.service.impl;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.trust.mapper.TrustInterfaceMapper;
import com.singlee.capital.trust.model.TtTrdTrust;
import com.singlee.capital.trust.service.TrustInterfaceService;
import com.singlee.capital.trust.service.TrustManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * 委托单接口管理
 * @author kevin_gm
 *
 */
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service("trustInterfaceService")
public class TrustInterfaceServiceImpl implements TrustInterfaceService {
	
	/** 委托单管理业务层 **/
	@Autowired
	private TrustManageService trustManageService;
	
	@Autowired
	private TrustInterfaceMapper trustInterfaceDao;
	
	/**
	 * 根据审批单编号查询委托单明细
	 * @param map
	 * @return
	 */
	@Override
	public HashMap<String,Object> selectTrustInfoByOrderId(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("order_id"))), "缺少必须的参数order_id.");
		TtTrdTrust ttTrdTrust = trustInterfaceDao.selectTrustByOrderID(map);
		
		JY.require(ttTrdTrust != null,"不存在的委托单据.");
		//1.查询审批单明细信息
		HashMap<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("folder", map.get("folder"));
		paramMap.put("trust_id", ttTrdTrust.getTrust_id());
		paramMap.put("version_number", ttTrdTrust.getVersion_number());
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(paramMap.get("trust_id"))), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(paramMap.get("version_number"))), "缺少必须的参数version_number.");
		
		return trustManageService.selectTrustInfo(paramMap);
	}
}
