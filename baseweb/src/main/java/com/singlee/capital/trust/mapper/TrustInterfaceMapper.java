package com.singlee.capital.trust.mapper;

import com.singlee.capital.trust.model.TtTrdTrust;

import java.util.HashMap;

public interface TrustInterfaceMapper {

	public TtTrdTrust selectTrustByOrderID(HashMap<String, Object> map);
}
