package com.singlee.capital.trust.controller;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.service.TrustManageService;
import com.singlee.slbpm.pojo.vo.ApproveLogVo;
import com.singlee.slbpm.service.FlowLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 委托单管理控制层
 * 
 * @author kevin_gm
 * 
 */
@Controller
@RequestMapping(value = "/TrustManageController")
public class TrustManageController extends CommonController {

	/** 委托单管理业务层 **/
	@Autowired
	private TrustManageService trustManageService;
	
	@Autowired
	private FlowLogService flowLogService;

	/**
	 * 审批单列表查询
	 * 
	 * @param request
	 *            参数 ： pageNumber 页码 pageSize 记录
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTrustList")
	public Object searchTrustList(@RequestBody HashMap<String, Object> map) throws IOException {
		return trustManageService.searchTrustList(map);
	}

	/**
	 * 审批单明细查询
	 * 
	 * @param request
	 *            参数 ： pageNumber 页码 pageSize 记录
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTrust")
	public HashMap<String, Object> selectTrust(@RequestBody HashMap<String, Object> map) throws IOException {
		return trustManageService.selectTrustInfo(map);
	}

	/**
	 * 审批单保存
	 * 
	 * @param request
	 *            参数 ： pageNumber 页码 pageSize 记录
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTrust")
	public void saveTrust(@RequestBody HashMap<String, Object> map) throws IOException {
		trustManageService.saveTrustInfo(map);
	}

	/**
	 * 删除委托单
	 * 
	 * @param trust_id
	 *            委托单号
	 * @param version
	 *            版本号
	 */
	@SuppressWarnings("deprecation")
	@ResponseBody
	@RequestMapping(value = "/cancelTrust")
	public void cancelTrust(@RequestBody HashMap<String, Object> map) throws IOException {
		String trust_id = ParameterUtil.getString(map, "trust_id", "");
		String memo = ParameterUtil.getString(map, "memo", "");
		int version_number = ParameterUtil.getInt(map, "version_number", -1);
		String folder = ParameterUtil.getString(map, "folder", "");

		String order_id = trustManageService.cancelTrust(trust_id, version_number, folder, memo);

		// 消息发送
		List<ApproveLogVo> list = flowLogService.searchFlowLog("", "", order_id, "", "", "", false);
		for (ApproveLogVo approveLogVo : list) {
			// 最后审批通过时user_id的编号为空格，需过滤该数据
			if (StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim())) {
				continue;
			}
//			msgService.puslishTrustReject(trust_id, approveLogVo.getUser_id(), SessionManager.getCurrentUserVo().getUser_name(), memo);
		}
	}

	/**
	 * 委托单生成交易单
	 * 
	 * @param request
	 *            参数 ： pageNumber 页码 pageSize 记录
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/createTrade")
	public TtTrdTrade createTrade(@RequestBody HashMap<String, Object> map) throws IOException {
		return trustManageService.createTrade(map);
	}
}
