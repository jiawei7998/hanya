package com.singlee.capital.trust.mapper;

import com.singlee.capital.trust.model.TtTrdTrust;
import com.singlee.capital.trust.pojo.TrustEditVo;

import java.util.HashMap;
import java.util.List;


/**
 * 委托单模板管理dao
 * @author kevin_gm
 *
 */
public interface TrdTrustMapper {
 
	/**
	 * 根据委托单编号查询委托记录对象，此方法带行级锁
	 * @param map
	 * @return
	 */
	public TtTrdTrust selectTrustForTrustId(HashMap<String,Object> map);
	
	/**
	 * 查询时显示委托单明细信息
	 * @param map
	 * @return
	 */
	public TrustEditVo selectTrustEditForTrustId(HashMap<String,Object> map);
	
	/**
	 * 获取委托单编号
	 * @return
	 */
	public String getTrustId(HashMap<String,Object> map);

	/**
	 * 委托单插入
	 * @param TtTrdTrust
	 */
	public void insertTrust(TtTrdTrust ttTrdTrust);
	
	/**
	 * 委托单更新
	 * @param TtTrdTrust
	 */
	public void updateTrust(TtTrdTrust ttTrdTrust);
	
	/**
	 * 
	* @date 2015-6-30
	* @Description TODO
	* @author shm
	* @returnType List<TrustListForMatching>
	 */
	public List<TrustEditVo> searchTrustList(HashMap<String, Object> map);
	
	/**
	 * 
	* @date 2015-6-30
	* @Description TODO
	* @author shm
	* @returnType int
	 */
	public int searchTrustListCount(HashMap<String, Object> map);
}
