package com.singlee.capital.trust.service;

import java.util.HashMap;

public interface TrustInterfaceService {

	/**
	 * 根据审批单编号查询委托单明细
	 * @param map
	 * @return
	 */
	public HashMap<String,Object> selectTrustInfoByOrderId(HashMap<String,Object> map);
	
}
