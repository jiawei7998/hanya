package com.singlee.capital.trust.model;
import java.io.Serializable;
/**
 * 交易委托单      
 * @author gm   
 * Create Time  2013-01-04 11:02:13
 */
public class TtTrdTrust implements Serializable {
	private static final long serialVersionUID = 5220101937433493780L;
	
	private String trust_id;	//交易编号 
	private int version_number;	//版本号 
	private String trust_date;	//委托单创建日期 
	private String trust_time;	//委托单创建时间 
	private String trust_user;	//委托单创建人
	private String confirm_date;	//确认日期
	private String confirm_time;	//确认时间
	private String confirm_user;  //确认人
	private String order_id;	//审批单号
	private String self_zzdaccid;	//本方中债托管帐号 
	private String party_zzdaccid;	//对手方中债托管账号
	private String trdtype;	//交易类型
	private String party_id;	//交易对手
	private String i_code;	//金融工具代码 
	private String a_type;	//资产类型 
	private String m_type;	//市场类型 BANK-银行间 
	private String i_name;	//金融工具名称 
	private String err_code;	//错误代码
	private String err_info;	//错误信息
	private String group_id;	//组合号 
	private String ref_tradedate;	//附属交易日期
	private String ref_tradeid;	//附属交易id
	private String exe_market;	//01:银行间场内 02:银行间场外 
	private String trust_source;	//交易来源 1=内部交易 2=COMPLEX 
	private String imp_time;	//导入时间
	private String fst_settle_type;	//首期结算方式
	private String end_settle_type;	//到期结算方式 
	private String fst_settle_date;	//首期结算日期 YYYY-MM-DD 
	private String end_settle_date;	//到期结算日期 YYYY-MM-DD 
	private double term_days;	//期限（天）
	private String settle_days;	//清算速度
	private String party_type;	//交易对手类型
	private double totalamount;	//券面总额 
	private double totalcount;	//券面数量 
	private double ytm;	//到期收益率 质押式回购填回购利率 
	private double netprice;	//净价(元) 
	private double fullprice;	//全价(元) 
	private double aiprice;	//应计利息(元) 
	private double netamount;	//净价金额(元) 
	private double fullamount;	//全价金额(元) 
	private double aiamount;	//利息金额(元) 
	private String self_traderid;	//本方交易员编号 
	private String self_tradername;	//本方交易员名称 
	private String self_bankacccode;	//本方银行帐号 
	private String self_bankaccname;	//本方账户名称 
	private String self_bankcode;	//本方开户行行号 
	private String self_bankname;	//本方开户行名称 
	private String self_largepaymentcode;	//本方大额支付号 
	private String self_insecuaccid;	//本方内部证券账户编码 
	private String self_outsecuaccid;	//本方外部证券账户编码 
	private String self_incashaccid;	//本方内部资金账户编码 
	private String self_outcashaccid;	//本方外部资金账户编码 
	private String party_traderid;	//对手方交易员编号 
	private String party_tradername;	//对手方交易员名称 
	private String party_bankacccode;	//对手方银行帐号 
	private String party_bankaccname;	//对手方账户名称 
	private String party_bankcode;	//对手方开户行行号 
	private String party_bankname;	//对手方开户行名称 
	private String party_largepaymentcode;	//对手方大额支付号 
	private String party_insecuaccid;	//对手方内部证券账户编码 
	private String party_outsecuaccid;	//对手方外部证券账户编码 
	private String party_incashaccid;	//对手方内部资金账户编码 
	private String party_outcashaccid;	//对手方外部资金账户编码 
	private String trust_status;	//0:待确认 
	private String operator_id;	//操作员编号 
	private String operate_time;	//操作时间
	private String memo;	//备注信息 
	private double zzd_netprice;	//中债估值净价
	private double zzd_ytm;	//中债收益率
	private double pretrust_amount;	//预成交额度
	private double used_amount;	//实际占用额度
	private double remain_amount;	//剩余可用额度
	private String is_active;	//启用标示
	private double trade_fee;	//交易费用
	private double settle_fee;	//结算费用
	private double fst_settle_amount;//首期结算金额
	private double end_settle_amount;//到期结算金额
	private String execid;//下行数据编号
	private double cal_days;//计息天数

    public double getCal_days() {
		return cal_days;
	}
	public void setCal_days(double cal_days) {
		this.cal_days = cal_days;
	}
	/** 行权收益率 **/
    private double strike_yield;
	
	public String getExecid() {
		return execid;
	}
	public void setExecid(String execid) {
		this.execid = execid;
	}
	public double getStrike_yield() {
		return strike_yield;
	}
	public void setStrike_yield(double strike_yield) {
		this.strike_yield = strike_yield;
	}
	public double getFst_settle_amount() {
		return fst_settle_amount;
	}
	public void setFst_settle_amount(double fst_settle_amount) {
		this.fst_settle_amount = fst_settle_amount;
	}
	public double getEnd_settle_amount() {
		return end_settle_amount;
	}
	public void setEnd_settle_amount(double end_settle_amount) {
		this.end_settle_amount = end_settle_amount;
	}
	public String getTrust_user() {
		return trust_user;
	}
	public void setTrust_user(String trust_user) {
		this.trust_user = trust_user;
	}
	public String getConfirm_user() {
		return confirm_user;
	}
	public void setConfirm_user(String confirm_user) {
		this.confirm_user = confirm_user;
	}
	public String getTrust_id() {
		return trust_id;
	}
	public void setTrust_id(String trust_id) {
		this.trust_id = trust_id;
	}
	public int getVersion_number() {
		return version_number;
	}
	public void setVersion_number(int version_number) {
		this.version_number = version_number;
	}
	public String getTrust_date() {
		return trust_date;
	}
	public void setTrust_date(String trust_date) {
		this.trust_date = trust_date;
	}
	public String getTrust_time() {
		return trust_time;
	}
	public void setTrust_time(String trust_time) {
		this.trust_time = trust_time;
	}
	public String getConfirm_date() {
		return confirm_date;
	}
	public void setConfirm_date(String confirm_date) {
		this.confirm_date = confirm_date;
	}
	public String getConfirm_time() {
		return confirm_time;
	}
	public void setConfirm_time(String confirm_time) {
		this.confirm_time = confirm_time;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getSelf_zzdaccid() {
		return self_zzdaccid;
	}
	public void setSelf_zzdaccid(String self_zzdaccid) {
		this.self_zzdaccid = self_zzdaccid;
	}
	public String getParty_zzdaccid() {
		return party_zzdaccid;
	}
	public void setParty_zzdaccid(String party_zzdaccid) {
		this.party_zzdaccid = party_zzdaccid;
	}
	public String getTrdtype() {
		return trdtype;
	}
	public void setTrdtype(String trdtype) {
		this.trdtype = trdtype;
	}
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
	public String getI_code() {
		return i_code;
	}
	public void setI_code(String i_code) {
		this.i_code = i_code;
	}
	public String getA_type() {
		return a_type;
	}
	public void setA_type(String a_type) {
		this.a_type = a_type;
	}
	public String getM_type() {
		return m_type;
	}
	public void setM_type(String m_type) {
		this.m_type = m_type;
	}
	public String getI_name() {
		return i_name;
	}
	public void setI_name(String i_name) {
		this.i_name = i_name;
	}
	public String getErr_code() {
		return err_code;
	}
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}
	public String getErr_info() {
		return err_info;
	}
	public void setErr_info(String err_info) {
		this.err_info = err_info;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getRef_tradedate() {
		return ref_tradedate;
	}
	public void setRef_tradedate(String ref_tradedate) {
		this.ref_tradedate = ref_tradedate;
	}
	public String getRef_tradeid() {
		return ref_tradeid;
	}
	public void setRef_tradeid(String ref_tradeid) {
		this.ref_tradeid = ref_tradeid;
	}
	public String getExe_market() {
		return exe_market;
	}
	public void setExe_market(String exe_market) {
		this.exe_market = exe_market;
	}
	public String getTrust_source() {
		return trust_source;
	}
	public void setTrust_source(String trust_source) {
		this.trust_source = trust_source;
	}
	public void setParty_bankaccname(String party_bankaccname) {
		this.party_bankaccname = party_bankaccname;
	}
	public String getImp_time() {
		return imp_time;
	}
	public void setImp_time(String imp_time) {
		this.imp_time = imp_time;
	}
	public String getFst_settle_type() {
		return fst_settle_type;
	}
	public void setFst_settle_type(String fst_settle_type) {
		this.fst_settle_type = fst_settle_type;
	}
	public String getEnd_settle_type() {
		return end_settle_type;
	}
	public void setEnd_settle_type(String end_settle_type) {
		this.end_settle_type = end_settle_type;
	}
	public String getFst_settle_date() {
		return fst_settle_date;
	}
	public void setFst_settle_date(String fst_settle_date) {
		this.fst_settle_date = fst_settle_date;
	}
	public String getEnd_settle_date() {
		return end_settle_date;
	}
	public void setEnd_settle_date(String end_settle_date) {
		this.end_settle_date = end_settle_date;
	}
	public double getTerm_days() {
		return term_days;
	}
	public void setTerm_days(double term_days) {
		this.term_days = term_days;
	}
	public String getSettle_days() {
		return settle_days;
	}
	public void setSettle_days(String settle_days) {
		this.settle_days = settle_days;
	}
	public String getParty_type() {
		return party_type;
	}
	public void setParty_type(String party_type) {
		this.party_type = party_type;
	}
	public double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}
	public double getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(double totalcount) {
		this.totalcount = totalcount;
	}
	public double getYtm() {
		return ytm;
	}
	public void setYtm(double ytm) {
		this.ytm = ytm;
	}
	public double getNetprice() {
		return netprice;
	}
	public void setNetprice(double netprice) {
		this.netprice = netprice;
	}
	public double getFullprice() {
		return fullprice;
	}
	public void setFullprice(double fullprice) {
		this.fullprice = fullprice;
	}
	public double getAiprice() {
		return aiprice;
	}
	public void setAiprice(double aiprice) {
		this.aiprice = aiprice;
	}
	public double getNetamount() {
		return netamount;
	}
	public void setNetamount(double netamount) {
		this.netamount = netamount;
	}
	public double getFullamount() {
		return fullamount;
	}
	public void setFullamount(double fullamount) {
		this.fullamount = fullamount;
	}
	public double getAiamount() {
		return aiamount;
	}
	public void setAiamount(double aiamount) {
		this.aiamount = aiamount;
	}
	public String getSelf_traderid() {
		return self_traderid;
	}
	public void setSelf_traderid(String self_traderid) {
		this.self_traderid = self_traderid;
	}
	public String getSelf_tradername() {
		return self_tradername;
	}
	public void setSelf_tradername(String self_tradername) {
		this.self_tradername = self_tradername;
	}
	public String getSelf_bankacccode() {
		return self_bankacccode;
	}
	public void setSelf_bankacccode(String self_bankacccode) {
		this.self_bankacccode = self_bankacccode;
	}
	public String getSelf_bankaccname() {
		return self_bankaccname;
	}
	public void setSelf_bankaccname(String self_bankaccname) {
		this.self_bankaccname = self_bankaccname;
	}
	public String getSelf_bankcode() {
		return self_bankcode;
	}
	public void setSelf_bankcode(String self_bankcode) {
		this.self_bankcode = self_bankcode;
	}
	public String getSelf_bankname() {
		return self_bankname;
	}
	public void setSelf_bankname(String self_bankname) {
		this.self_bankname = self_bankname;
	}
	public String getSelf_largepaymentcode() {
		return self_largepaymentcode;
	}
	public void setSelf_largepaymentcode(String self_largepaymentcode) {
		this.self_largepaymentcode = self_largepaymentcode;
	}
	public String getSelf_insecuaccid() {
		return self_insecuaccid;
	}
	public void setSelf_insecuaccid(String self_insecuaccid) {
		this.self_insecuaccid = self_insecuaccid;
	}
	public String getSelf_outsecuaccid() {
		return self_outsecuaccid;
	}
	public void setSelf_outsecuaccid(String self_outsecuaccid) {
		this.self_outsecuaccid = self_outsecuaccid;
	}
	public String getSelf_incashaccid() {
		return self_incashaccid;
	}
	public void setSelf_incashaccid(String self_incashaccid) {
		this.self_incashaccid = self_incashaccid;
	}
	public String getSelf_outcashaccid() {
		return self_outcashaccid;
	}
	public void setSelf_outcashaccid(String self_outcashaccid) {
		this.self_outcashaccid = self_outcashaccid;
	}
	public String getParty_traderid() {
		return party_traderid;
	}
	public void setParty_traderid(String party_traderid) {
		this.party_traderid = party_traderid;
	}
	public String getParty_tradername() {
		return party_tradername;
	}
	public void setParty_tradername(String party_tradername) {
		this.party_tradername = party_tradername;
	}
	public String getParty_bankacccode() {
		return party_bankacccode;
	}
	public void setParty_bankacccode(String party_bankacccode) {
		this.party_bankacccode = party_bankacccode;
	}
	public String getParty_bankaccname() {
		return party_bankaccname;
	}
	public void Party(String party_bankaccname) {
		this.party_bankaccname = party_bankaccname;
	}
	public String getParty_bankcode() {
		return party_bankcode;
	}
	public void setParty_bankcode(String party_bankcode) {
		this.party_bankcode = party_bankcode;
	}
	public String getParty_bankname() {
		return party_bankname;
	}
	public void setParty_bankname(String party_bankname) {
		this.party_bankname = party_bankname;
	}
	public String getParty_largepaymentcode() {
		return party_largepaymentcode;
	}
	public void setParty_largepaymentcode(String party_largepaymentcode) {
		this.party_largepaymentcode = party_largepaymentcode;
	}
	public String getParty_insecuaccid() {
		return party_insecuaccid;
	}
	public void setParty_insecuaccid(String party_insecuaccid) {
		this.party_insecuaccid = party_insecuaccid;
	}
	public String getParty_outsecuaccid() {
		return party_outsecuaccid;
	}
	public void setParty_outsecuaccid(String party_outsecuaccid) {
		this.party_outsecuaccid = party_outsecuaccid;
	}
	public String getParty_incashaccid() {
		return party_incashaccid;
	}
	public void setParty_incashaccid(String party_incashaccid) {
		this.party_incashaccid = party_incashaccid;
	}
	public String getParty_outcashaccid() {
		return party_outcashaccid;
	}
	public void setParty_outcashaccid(String party_outcashaccid) {
		this.party_outcashaccid = party_outcashaccid;
	}
	public String getTrust_status() {
		return trust_status;
	}
	public void setTrust_status(String trust_status) {
		this.trust_status = trust_status;
	}
	public String getOperator_id() {
		return operator_id;
	}
	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}
	public String getOperate_time() {
		return operate_time;
	}
	public void setOperate_time(String operate_time) {
		this.operate_time = operate_time;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getZzd_netprice() {
		return zzd_netprice;
	}
	public void setZzd_netprice(double zzd_netprice) {
		this.zzd_netprice = zzd_netprice;
	}
	public double getZzd_ytm() {
		return zzd_ytm;
	}
	public void setZzd_ytm(double zzd_ytm) {
		this.zzd_ytm = zzd_ytm;
	}
	public double getPretrust_amount() {
		return pretrust_amount;
	}
	public void setPretrust_amount(double pretrust_amount) {
		this.pretrust_amount = pretrust_amount;
	}
	public double getUsed_amount() {
		return used_amount;
	}
	public void setUsed_amount(double used_amount) {
		this.used_amount = used_amount;
	}
	public double getRemain_amount() {
		return remain_amount;
	}
	public void setRemain_amount(double remain_amount) {
		this.remain_amount = remain_amount;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}
	public double getTrade_fee() {
		return trade_fee;
	}
	public void setTrade_fee(double trade_fee) {
		this.trade_fee = trade_fee;
	}
	public double getSettle_fee() {
		return settle_fee;
	}
	public void setSettle_fee(double settle_fee) {
		this.settle_fee = settle_fee;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtTrdTrust [trust_id=");
		builder.append(trust_id);
		builder.append(", version_number=");
		builder.append(version_number);
		builder.append(", trust_date=");
		builder.append(trust_date);
		builder.append(", trust_time=");
		builder.append(trust_time);
		builder.append(", trust_user=");
		builder.append(trust_user);
		builder.append(", confirm_date=");
		builder.append(confirm_date);
		builder.append(", confirm_time=");
		builder.append(confirm_time);
		builder.append(", confirm_user=");
		builder.append(confirm_user);
		builder.append(", order_id=");
		builder.append(order_id);
		builder.append(", self_zzdaccid=");
		builder.append(self_zzdaccid);
		builder.append(", party_zzdaccid=");
		builder.append(party_zzdaccid);
		builder.append(", trdtype=");
		builder.append(trdtype);
		builder.append(", party_id=");
		builder.append(party_id);
		builder.append(", i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", i_name=");
		builder.append(i_name);
		builder.append(", err_code=");
		builder.append(err_code);
		builder.append(", err_info=");
		builder.append(err_info);
		builder.append(", group_id=");
		builder.append(group_id);
		builder.append(", ref_tradedate=");
		builder.append(ref_tradedate);
		builder.append(", ref_tradeid=");
		builder.append(ref_tradeid);
		builder.append(", exe_market=");
		builder.append(exe_market);
		builder.append(", trust_source=");
		builder.append(trust_source);
		builder.append(", imp_time=");
		builder.append(imp_time);
		builder.append(", fst_settle_type=");
		builder.append(fst_settle_type);
		builder.append(", end_settle_type=");
		builder.append(end_settle_type);
		builder.append(", fst_settle_date=");
		builder.append(fst_settle_date);
		builder.append(", end_settle_date=");
		builder.append(end_settle_date);
		builder.append(", term_days=");
		builder.append(term_days);
		builder.append(", settle_days=");
		builder.append(settle_days);
		builder.append(", party_type=");
		builder.append(party_type);
		builder.append(", totalamount=");
		builder.append(totalamount);
		builder.append(", totalcount=");
		builder.append(totalcount);
		builder.append(", ytm=");
		builder.append(ytm);
		builder.append(", netprice=");
		builder.append(netprice);
		builder.append(", fullprice=");
		builder.append(fullprice);
		builder.append(", aiprice=");
		builder.append(aiprice);
		builder.append(", netamount=");
		builder.append(netamount);
		builder.append(", fullamount=");
		builder.append(fullamount);
		builder.append(", aiamount=");
		builder.append(aiamount);
		builder.append(", self_traderid=");
		builder.append(self_traderid);
		builder.append(", self_tradername=");
		builder.append(self_tradername);
		builder.append(", self_bankacccode=");
		builder.append(self_bankacccode);
		builder.append(", self_bankaccname=");
		builder.append(self_bankaccname);
		builder.append(", self_bankcode=");
		builder.append(self_bankcode);
		builder.append(", self_bankname=");
		builder.append(self_bankname);
		builder.append(", self_largepaymentcode=");
		builder.append(self_largepaymentcode);
		builder.append(", self_insecuaccid=");
		builder.append(self_insecuaccid);
		builder.append(", self_outsecuaccid=");
		builder.append(self_outsecuaccid);
		builder.append(", self_incashaccid=");
		builder.append(self_incashaccid);
		builder.append(", self_outcashaccid=");
		builder.append(self_outcashaccid);
		builder.append(", party_traderid=");
		builder.append(party_traderid);
		builder.append(", party_tradername=");
		builder.append(party_tradername);
		builder.append(", party_bankacccode=");
		builder.append(party_bankacccode);
		builder.append(", party_bankaccname=");
		builder.append(party_bankaccname);
		builder.append(", party_bankcode=");
		builder.append(party_bankcode);
		builder.append(", party_bankname=");
		builder.append(party_bankname);
		builder.append(", party_largepaymentcode=");
		builder.append(party_largepaymentcode);
		builder.append(", party_insecuaccid=");
		builder.append(party_insecuaccid);
		builder.append(", party_outsecuaccid=");
		builder.append(party_outsecuaccid);
		builder.append(", party_incashaccid=");
		builder.append(party_incashaccid);
		builder.append(", party_outcashaccid=");
		builder.append(party_outcashaccid);
		builder.append(", trust_status=");
		builder.append(trust_status);
		builder.append(", operator_id=");
		builder.append(operator_id);
		builder.append(", operate_time=");
		builder.append(operate_time);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", zzd_netprice=");
		builder.append(zzd_netprice);
		builder.append(", zzd_ytm=");
		builder.append(zzd_ytm);
		builder.append(", pretrust_amount=");
		builder.append(pretrust_amount);
		builder.append(", used_amount=");
		builder.append(used_amount);
		builder.append(", remain_amount=");
		builder.append(remain_amount);
		builder.append(", is_active=");
		builder.append(is_active);
		builder.append(", trade_fee=");
		builder.append(trade_fee);
		builder.append(", settle_fee=");
		builder.append(settle_fee);
		builder.append(", fst_settle_amount=");
		builder.append(fst_settle_amount);
		builder.append(", end_settle_amount=");
		builder.append(end_settle_amount);
		builder.append(", execid=");
		builder.append(execid);
		builder.append(", cal_days=");
		builder.append(cal_days);
		builder.append(", strike_yield=");
		builder.append(strike_yield);
		builder.append("]");
		return builder.toString();
	}
} 