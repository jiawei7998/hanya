package com.singlee.capital.trust.service;

import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.model.TtTrdTrust;

import java.util.HashMap;
import java.util.Map;


/**
 * 委托单业务层接口，所有业务必须实现该接口
 * @author kevin_gm
 *
 */
public interface TrustServiceListener {

	/**
	 * 审批单查询
	 * @param map
	 * @return
	 */
	Object searchTrustList(HashMap<String,Object> map,int pageNumber,int pageSize);
	
	/**
	 * 查询审批单明细信息
	 * @param map
	 * @return
	 */
	void selectTrustInfo(HashMap<String,Object> map);
	
	/**
	 * 审批单保存方法
	 * @param map：前台传入参数
	 * @param ttTrdOrder：默认赋值后的TtTrdOrder对象
	 * @param isAdd：true 表示当前执行新增操作
	 */
	void saveTrust(HashMap<String, Object> map,TtTrdTrust ttTrdTrust);
	/**
	 * 注销委托单
	 * @param trust_id
	 * @param version_number
	 * @param folder
	 */
	void cancelTrust(TtTrdTrust ttTrdTrust);
	
	/**
	 * 委托单下行匹配
	 * @param trust_id
	 * @param version_number
	 * @param folder
	 */
	void confirmTrust(TtTrdTrust ttTrdTrust);
	/**
	 * 委托单生成交易单
	 * @param map
	 * @return
	 */
	void createTrade(HashMap<String, Object> map,TtTrdTrade ttTrdTrade,TtTrdTrust ttTrdTrustOld);
	/**
	 * 
	* @date 2015-9-11
	* @Description TODO
	* @author Administrator
	* @returnType boolean
	 */
//	void createTrustMatching(TtTrdTrust ttTrdTrust,Map<String, Object> map,ExecutionReportCSTP exec);
	/**
	 * 
	* @date 2015-9-11
	* @Description TODO
	* @author Administrator
	* @returnType void
	 */
//	boolean autoTrustMatching(HashMap<String, Object> map,TtTrdTrust ttTrdTrust, ExecutionReportCSTP executionReportCSTP);
	/**
	 * 
	* @date 2015-9-11
	* @Description TODO
	* @author Administrator
	* @returnType List<ExecutionReportCSTP>
	 */
	Object searchExecutionReportCSTP(Map<String, Object> map);
	
	/**
	 * 
	* @date 2015-9-11
	* @Description 匹配交易单
	* @author Administrator
	* @returnType void
	 */
//	boolean autoCFETSMatching(Map<String, Object> map, TtTrdTrust ttTrdTrust,ExecutionReportCSTP executionReportCSTP);
		
	
}
