package com.singlee.capital.trust.service.impl;

import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveInfoService;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.util.SettleConstants;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SysParamService;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trust.mapper.TrdTrustMapper;
import com.singlee.capital.trust.model.TtTrdTrust;
import com.singlee.capital.trust.pojo.TrustEditVo;
import com.singlee.capital.trust.service.TrustManageService;
import com.singlee.capital.trust.service.TrustServiceListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * 委托单管理业务层
 * @author kevin_gm
 *
 */
@Service("trustManageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrustManageServiceImpl implements TrustManageService {

	/** 交易单管理dao **/
	@Autowired
	private TrdTradeMapper tradeManageDao;
	
	/** 委托单管理dao **/
	@Autowired
	private TrdTrustMapper trustManageDao;

	/** 审批单dao **/
	@Autowired
	private TrdOrderMapper approveManageDao;

	/** 审批单业务层 **/
	@Autowired
	private ApproveManageService approveManageService;

//	/** 质押券dao **/
//	@Autowired
//	private PledgeBondDao pledgeBondDao;
	
	/** 系统参数表 **/
	@Autowired
	private SysParamService sysParamService;

	/** 下行数据对象dao **/
//	@Autowired
//	private ExecutionReportCSTPDao executionReportCSTPDao;
	
	
	/** 用于判断单据是否允许录入 **/
	@Autowired
	private ApproveInfoService approveInfoService;
	

	/** 消息发送 **//*
	@Autowired
	private MsgService msgService;
	
	*//**审批流程**//*
	@Autowired
	private FlowService flowService;*/
	
//	@Autowired
//	private ExecutionReportService executionReportService;
//	
	@Autowired
	private UserService userService;
	@Autowired
	private DayendDateService dayendDateService;

	@Autowired
	private UserParamService userParamService;
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	@Override
	public Object searchTrustList(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),DictConstants.BillsType.Trust);
		
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		int pageNumber =ParameterUtil.getInt(map,"pageNumber",1);
		int pageSize = ParameterUtil.getInt(map,"pageSize",12);
		String trdtype_search = ParameterUtil.getString(map,"trdtype_search","");
		String trust_status_search = ParameterUtil.getString(map,"trust_status_search","");
		map.put("trdtype_search", StringUtil.checkEmptyNull(trdtype_search)?null:trdtype_search.split(","));
		map.put("trust_status_search", StringUtil.checkEmptyNull(trust_status_search)?null:trust_status_search.split(","));
		map.put("user_id", ParameterUtil.getString(map, "opUserId", ""));
		return trustServiceListener.searchTrustList(map, pageNumber, pageSize);
	}
	
	/**
	 * 查询委托单明细信息
	 * @param map
	 * @return
	 */
	@Override
	public HashMap<String,Object> selectTrustInfo(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("trust_id"))), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("version_number"))), "缺少必须的参数version_number.");

		//1.查询审批单明细信息
		TrustEditVo trustEditVo = trustManageDao.selectTrustEditForTrustId(map);
		HashMap<String,Object> objMap = ParentChildUtil.ClassToHashMap(trustEditVo);

		//2.调用其他业务接口
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),DictConstants.BillsType.Trust);
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		trustServiceListener.selectTrustInfo(objMap);
		
		return objMap;
	}
	
	/**
	 * 保存委托单信息(委托单不存在新增业务,但存在新增逻辑)
	 * @param map
	 */
	@Override
	public TtTrdTrust saveTrustInfo(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("trust_id"))), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("version_number"))), "缺少必须的参数version_number.");

//		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		
		//1.锁定原委托单
		HashMap<String,Object> mapOld = new HashMap<String, Object>();
		mapOld.put("trust_id", map.get("trust_id"));
		mapOld.put("version_number", map.get("version_number"));
		mapOld.put("is_locked", true);
		TtTrdTrust ttTrdTrust = trustManageDao.selectTrustForTrustId(mapOld);
		//增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdTrust.getM_type(), ttTrdTrust.getTrust_date());
		
		double totalamountOld = ttTrdTrust.getTotalamount();
		
		//2.验证规则
		String opUserId = ParameterUtil.getString(map, "opUserId", "");
		JY.require(DictConstants.TrustStatus.NEW.equals(ttTrdTrust.getTrust_status()),"仅允许修改状态为[新增]的委托单据.");
		JY.require(opUserId.equals(ttTrdTrust.getOperator_id()),"仅允许修改本人所属单据.");
		
		//3.更新原交易单的is_active
		ttTrdTrust.setIs_active(DictConstants.YesNo.NO);
		trustManageDao.updateTrust(ttTrdTrust);
		
		//4.生成新委托单对象
		ttTrdTrust.setVersion_number(ttTrdTrust.getVersion_number() + 1);
		ttTrdTrust.setTotalamount(ParameterUtil.getDouble(map, "totalamount", -1));
		ttTrdTrust.setNetamount(ParameterUtil.getDouble(map, "netamount", -1));
		ttTrdTrust.setFullamount(ParameterUtil.getDouble(map, "fullamount", -1));
		ttTrdTrust.setAiamount(ParameterUtil.getDouble(map, "aiamount", -1));
		ttTrdTrust.setTotalcount(ParameterUtil.getDouble(map, "totalcount", -1));
		ttTrdTrust.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrust.setOperator_id(opUserId);
		ttTrdTrust.setPretrust_amount(ttTrdTrust.getTotalamount());
		ttTrdTrust.setUsed_amount(0);
		ttTrdTrust.setRemain_amount(ttTrdTrust.getTotalamount());
		ttTrdTrust.setMemo(ParameterUtil.getString(map, "memo", ""));
		ttTrdTrust.setIs_active(DictConstants.YesNo.YES);

		//5.调用其他业务接口
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),DictConstants.BillsType.Trust);
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		trustServiceListener.saveTrust(map, ttTrdTrust);
		
		//6.委托单写入
		trustManageDao.insertTrust(ttTrdTrust);
		
		//3.1 锁定审批单
		HashMap<String, Object> mapOrder = new HashMap<String, Object>();
		mapOrder.put("order_id",ttTrdTrust.getOrder_id());
		mapOrder.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(map);
		
		//3.2 更新审批单
		approveManageService.updateApproveRemainAmount(ttTrdOrder, totalamountOld, ttTrdTrust.getTotalamount(),true,opUserId);
		return ttTrdTrust;
	}
	
	/**
	 * 注销委托单
	 * @param trust_id
	 * @param version_number
	 * @param folder
	 */
	@Override
	public String cancelTrust(String trust_id,int version_number,String folder, String memo){
		JY.require(!StringUtil.checkEmptyNull(trust_id), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(version_number)), "缺少必须的参数version_number.");
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");

//		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		
		//1.锁定原委托单
		HashMap<String,Object> map = new HashMap<String, Object>();
		map.put("trust_id", trust_id);
		map.put("version_number", version_number);
		map.put("is_locked", true);
		TtTrdTrust ttTrdTrust = trustManageDao.selectTrustForTrustId(map);
		
		//2.委托单验证
		JY.require(ttTrdTrust != null, "选中交易单对象为空或不存在.");
		JY.require(DictConstants.TrustStatus.NEW.equals(ttTrdTrust.getTrust_status()), "删除的交易单状态必须为[待确认]状态.");
		JY.require(DictConstants.YesNo.YES.equals(ttTrdTrust.getTrust_status()), "该委托单状态已为禁用.");

		//3.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trust);
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		trustServiceListener.cancelTrust(ttTrdTrust);
		
		//4 锁定审批单
		map = new HashMap<String, Object>();
		map.put("order_id",ttTrdTrust.getOrder_id());
		map.put("is_locked", true);
		TtTrdOrder ttTrdOrder = approveManageDao.selectOrderForOrderId(map);

		// 4.5 设置memo
		String memos = StringUtil.isNullOrEmpty(ttTrdOrder.getMemo())?memo:(ttTrdOrder.getMemo() +"\r\n"+memo);
		ttTrdOrder.setMemo(memos);
		String opUserId = ParameterUtil.getString(map, "opUserId", "");
		//5 更新审批单余额
		approveManageService.updateApproveRemainAmount(ttTrdOrder, ttTrdTrust.getTotalamount(), 0,false,opUserId);
		
		//6.更新委托单信息
		ttTrdTrust.setIs_active(DictConstants.YesNo.NO);
		ttTrdTrust.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrust.setOperator_id(opUserId);
		trustManageDao.updateTrust(ttTrdTrust);
		
		return ttTrdTrust.getOrder_id();
	}
	
	/**
	 * 委托单上行发送
	 * @param trust_id
	 * @param version_number
	 * @param folder
	 */
	@Override
	public void confirmTrust(String trust_id,int version_number,String folder,String opUserId){
		JY.require(!StringUtil.checkEmptyNull(trust_id), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(version_number)), "缺少必须的参数version_number.");
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");

//		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
//		
		//1.锁定原委托单
		HashMap<String,Object> map = new HashMap<String, Object>();
		map.put("trust_id", trust_id);
		map.put("version_number", version_number);
		map.put("is_locked", true);
		TtTrdTrust ttTrdTrust = trustManageDao.selectTrustForTrustId(map);

		//增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdTrust.getM_type(), ttTrdTrust.getTrust_date());
		
		//2.委托单验证
		JY.require(ttTrdTrust != null, "选中交易单对象为空或不存在.");
		JY.require(DictConstants.TrustStatus.NEW.equals(ttTrdTrust.getTrust_status()), "仅允许确认状态为[新建]的委托单.");
		JY.require(DictConstants.YesNo.YES.equals(ttTrdTrust.getTrust_status()), "仅允许确认状态为[启用]的委托单.");
		
		//3.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trust);
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		trustServiceListener.cancelTrust(ttTrdTrust);

		//4.更新委托单状态
		ttTrdTrust.setConfirm_date(dayendDateService.getSettlementDate());
		ttTrdTrust.setConfirm_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrust.setConfirm_user(opUserId);
		ttTrdTrust.setTrust_status(DictConstants.TrustStatus.SENDED);
		ttTrdTrust.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrust.setOperator_id(opUserId);
		trustManageDao.updateTrust(ttTrdTrust);
		
		//演示模式,直接生成交易单（正式环境不开放该功能）
		if(userParamService.getBooleanSysParamByName("system.isCreateTradeForDemonstrate", false)){
			createTradeForDemonstrate(ttTrdTrust);
			return;
		}
		
		//5.委托单上行数据发送
//		if(SystemProperties.isSendToCstp){
//			HashMap<String, Object> packMap = new HashMap<String, Object>();
//			packMap.put("tt_trd_trust", ttTrdTrust);
//			//5.1如果为质押式回购时，则需要将质押式券对象传入
//			map = new HashMap<String, Object>();
//			map.put("tradeid", trust_id);
//			map.put("trade_version", version_number);
//			List<TtTrdPledgebond> pledgeList = pledgeBondDao.searchPledgeBondLocked(map);
//			packMap.put("tt_trd_pledgebond", pledgeList);
//			
//			DataPackage dataPackage = new  DataPackage(Function.SendTrust,packMap);
//			DataPackage responsePackage = ExchangeManager.getInstance().sendMessage(dataPackage);
//			if(responsePackage == null){
//				JY.raise("ESB连接超时.");
//			}
//			if(responsePackage.getHeader().getErrorCode() != 0){
//				JY.raise("委托上行接口处理异常："+responsePackage.getHeader().getErrorMsg());
//			}
//		}
	}
	
	/**
	 * 演示环境下，委托单直接生成交易单
	 */
	private void createTradeForDemonstrate(TtTrdTrust ttTrdTrust){
		String trdtype = ttTrdTrust.getTrdtype();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trust_id", ttTrdTrust.getTrust_id());
		map.put("version_number", ttTrdTrust.getVersion_number());
		//不同业务创建交易单时需要构建出不同数据
		if(DictConstants.TrdType.BondBuy.equals(trdtype) || DictConstants.TrdType.BondSell.equals(trdtype)){
			map.put("totalamount", ttTrdTrust.getTotalamount());
			map.put("totalcount", ttTrdTrust.getTotalcount());
			map.put("netamount", ttTrdTrust.getNetamount());
			map.put("fullamount", ttTrdTrust.getFullamount());
			map.put("aiamount", ttTrdTrust.getAiamount());
			map.put("fst_settle_amount", ttTrdTrust.getFst_settle_amount());
		}else if(DictConstants.TrdType.SellRepoPledged.equals(trdtype) || DictConstants.TrdType.ReverseRepoPledged.equals(trdtype)){
			map.put("fst_settle_amount", ttTrdTrust.getFst_settle_amount());
			map.put("end_settle_amount", ttTrdTrust.getEnd_settle_amount());
		}else{
			JY.raise("演示环境下委托单自动生成交易单时.错误的业务类型["+trdtype+"].");
		}
		createTrade(map);
	}
	@Override
	public TtTrdTrade createTrade(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
//		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		TtTrdTrade ttTrdTrade=createCFETSTrade(map);
		return ttTrdTrade;
	}
	
	/**
	 * 委托单生成交易单
	 * @param map
	 * @return
	 */
	@Override
	public TtTrdTrade createCFETSTrade(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("trust_id"))), "缺少必须的参数trust_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("version_number"))), "缺少必须的参数version_number.");
		//1.锁定原委托单
		HashMap<String,Object> mapOld = new HashMap<String, Object>();
		mapOld.put("trust_id", StringUtil.toString(map.get("trust_id")));
		mapOld.put("version_number", StringUtil.toString(map.get("version_number")));
		mapOld.put("is_locked", true);
		TtTrdTrust ttTrdTrustOld = trustManageDao.selectTrustForTrustId(mapOld);
		
		//2.委托单验证
		JY.require(ttTrdTrustOld != null, "选中交易单对象为空或不存在.");
		JY.require(DictConstants.TrustStatus.NEW.equals(ttTrdTrustOld.getTrust_status()) || DictConstants.TrustStatus.SENDED.equals(ttTrdTrustOld.getTrust_status()) || DictConstants.TrustStatus.PARTIALCONFIRMED.equals(ttTrdTrustOld.getTrust_status()) || DictConstants.TrustStatus.SINCONFIRMED.equals(ttTrdTrustOld.getTrust_status()) ,"仅允许状态为[新建]或[部分匹配]或[全部匹配(多交易)]或[全部匹配(单交易)]或[执行中]的审批单据执行生成委托单操作.");
		ttTrdTrustOld.setExecid(ParameterUtil.getString(map, "dealNumber", ""));
		
		//4.交易单对象转换
		TtTrdTrade ttTrdTrade = trustTransTrade(ttTrdTrustOld);
		//增加交易日判断
		approveInfoService.processToJudgeTradeDate(ttTrdTrade.getM_type(), ttTrdTrade.getTrade_date());
		
		//5.调用其他业务接口
		String folder = ParameterUtil.getString(map, "folder", "");
		if(StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))){
			folder = sysParamService.selectSysParam("000002", ttTrdTrustOld.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trust);
		TrustServiceListener trustServiceListener = SpringContextHolder.getBean(beanName);
		trustServiceListener.createTrade(map,ttTrdTrade,ttTrdTrustOld);
		//取交易员
		String mgTradeUser=(String)map.get("mgTradeUser");
		TaUser userVo=userService.getUserById(mgTradeUser);
		if(null!=userVo){
			ttTrdTrade.setOperator_id(userVo.getUserId());
		}
		//mod by lihuabing 20161215
		//交易单状态置为核实中
		ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Verifying);
		
		//6.交易单写入
		tradeManageDao.insertTrade(ttTrdTrade);
		JY.info("更新委托单号       要匹配的委托单号:"+ttTrdTrustOld.getTrust_id()+"  要匹配的成交编号"+ttTrdTrustOld.getExecid());
		//7.更新审批单额度
		updateTrustRemainAmount(ttTrdTrustOld,0,ttTrdTrade.getTotalamount(),userVo.getUserId());
		return ttTrdTrade;
	}
	
	private TtTrdTrade trustTransTrade(TtTrdTrust ttTrdTrust){
		TtTrdTrade ttTrdTrade = new TtTrdTrade();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trdtype", ttTrdTrust.getTrdtype());
		ttTrdTrade.setTrade_id(tradeManageDao.getTradeId(map));		
		
		ttTrdTrade.setVersion_number(0);
		ttTrdTrade.setTrade_date(ttTrdTrust.getTrust_date());
		ttTrdTrade.setTrade_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setTrade_user(ttTrdTrust.getTrust_user());
//		ttTrdTrade.setConfirm_date(ttTrdOrder.getConfirm_date());
//		ttTrdTrade.setConfirm_time(ttTrdOrder.getConfirm_time());
//		ttTrdTrade.setConfirm_user(ttTrdOrder.getConfirm_user());
		ttTrdTrade.setOrder_id(ttTrdTrust.getOrder_id());
		ttTrdTrade.setTrust_id(ttTrdTrust.getTrust_id());
		ttTrdTrade.setTrust_version(ttTrdTrust.getVersion_number());
//		ttTrdTrade.setIn_tradeid(ttTrdOrder.getIn_tradeid());
//		ttTrdTrade.setOut_tradeid(ttTrdOrder.getOut_tradeid());
//		ttTrdTrade.setCust_tradeid(ttTrdOrder.getCust_tradeid());
		ttTrdTrade.setSelf_zzdaccid(ttTrdTrust.getSelf_zzdaccid());
		ttTrdTrade.setParty_zzdaccid(ttTrdTrust.getParty_zzdaccid());
		ttTrdTrade.setTrdtype(ttTrdTrust.getTrdtype());
		ttTrdTrade.setParty_id(ttTrdTrust.getParty_id());
		ttTrdTrade.setI_code(ttTrdTrust.getI_code());
		ttTrdTrade.setA_type(ttTrdTrust.getA_type());
		ttTrdTrade.setM_type(ttTrdTrust.getM_type());
		ttTrdTrade.setI_name(ttTrdTrust.getI_name());
		ttTrdTrade.setErr_code(ttTrdTrust.getErr_code());
		ttTrdTrade.setErr_info(ttTrdTrust.getErr_info());
		ttTrdTrade.setGroup_id(ttTrdTrust.getGroup_id());
//		ttTrdTrade.setRef_tradedate(ttTrdOrder.getRef_tradedate());
//		ttTrdTrade.setRef_tradeid(ttTrdOrder.getRef_tradeid());
		ttTrdTrade.setExe_market(ttTrdTrust.getExe_market());
		ttTrdTrade.setTrade_source(DictConstants.TrustSource.SysTrust);
		ttTrdTrade.setImp_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setFst_settle_type(ttTrdTrust.getFst_settle_type());
		ttTrdTrade.setEnd_settle_type(ttTrdTrust.getEnd_settle_type());
		ttTrdTrade.setFst_settle_date(ttTrdTrust.getFst_settle_date());
		ttTrdTrade.setEnd_settle_date(ttTrdTrust.getEnd_settle_date());
		ttTrdTrade.setTerm_days(ttTrdTrust.getTerm_days());
		ttTrdTrade.setSettle_days(ttTrdTrust.getSettle_days());
		ttTrdTrade.setParty_type(ttTrdTrust.getParty_type());
		ttTrdTrade.setTotalamount(ttTrdTrust.getTotalamount());
		ttTrdTrade.setTotalcount(ttTrdTrust.getTotalcount());
		ttTrdTrade.setYtm(ttTrdTrust.getYtm());
		ttTrdTrade.setNetprice(ttTrdTrust.getNetprice());
		ttTrdTrade.setFullprice(ttTrdTrust.getFullprice());
		ttTrdTrade.setAiprice(ttTrdTrust.getAiprice());
		ttTrdTrade.setNetamount(ttTrdTrust.getNetamount());
		ttTrdTrade.setFullamount(ttTrdTrust.getFullamount());
		ttTrdTrade.setAiamount(ttTrdTrust.getAiamount());
		ttTrdTrade.setSelf_traderid(ttTrdTrust.getSelf_traderid());
		ttTrdTrade.setSelf_tradername(ttTrdTrust.getSelf_tradername());
		ttTrdTrade.setSelf_bankacccode(ttTrdTrust.getSelf_bankacccode());
		ttTrdTrade.setSelf_bankaccname(ttTrdTrust.getSelf_bankaccname());
		ttTrdTrade.setSelf_bankcode(ttTrdTrust.getSelf_bankcode());
		ttTrdTrade.setSelf_bankname(ttTrdTrust.getSelf_bankname());
		ttTrdTrade.setSelf_largepaymentcode(ttTrdTrust.getSelf_largepaymentcode());
		ttTrdTrade.setSelf_insecuaccid(ttTrdTrust.getSelf_insecuaccid());
		ttTrdTrade.setSelf_outsecuaccid(ttTrdTrust.getSelf_outsecuaccid());
		ttTrdTrade.setSelf_incashaccid(ttTrdTrust.getSelf_incashaccid());
		ttTrdTrade.setSelf_outcashaccid(ttTrdTrust.getSelf_outcashaccid());
		ttTrdTrade.setParty_traderid(ttTrdTrust.getParty_traderid());
		ttTrdTrade.setParty_tradername(ttTrdTrust.getParty_tradername());
		ttTrdTrade.setParty_bankacccode(ttTrdTrust.getParty_bankacccode());
		ttTrdTrade.setParty_bankaccname(ttTrdTrust.getParty_bankaccname());
		ttTrdTrade.setParty_bankcode(ttTrdTrust.getParty_bankcode());
		ttTrdTrade.setParty_bankname(ttTrdTrust.getParty_bankname());
		ttTrdTrade.setParty_largepaymentcode(ttTrdTrust.getParty_largepaymentcode());
		ttTrdTrade.setParty_insecuaccid(ttTrdTrust.getParty_insecuaccid());
		ttTrdTrade.setParty_outsecuaccid(ttTrdTrust.getParty_outsecuaccid());
		ttTrdTrade.setParty_incashaccid(ttTrdTrust.getParty_incashaccid());
		ttTrdTrade.setParty_outcashaccid(ttTrdTrust.getParty_outcashaccid());
		ttTrdTrade.setTrade_status(DictConstants.TradeStatus.NEW);
		
		ttTrdTrade.setOperator_id(ttTrdTrust.getOperator_id());
		
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setMemo(ttTrdTrust.getMemo());
		ttTrdTrade.setZzd_netprice(ttTrdTrust.getZzd_netprice());
		ttTrdTrade.setZzd_ytm(ttTrdTrust.getZzd_ytm());
		ttTrdTrade.setTradeamount(ttTrdTrust.getFst_settle_amount());
		ttTrdTrade.setIs_active(ttTrdTrust.getIs_active());
		ttTrdTrade.setTrade_fee(ttTrdTrust.getTrade_fee());
		ttTrdTrade.setSettle_fee(ttTrdTrust.getSettle_fee());
		ttTrdTrade.setSettleamount(ttTrdTrust.getFst_settle_amount() + ttTrdTrust.getSettle_fee() + ttTrdTrust.getTrade_fee());
		ttTrdTrade.setFst_settle_amount(ttTrdTrust.getFst_settle_amount());
		ttTrdTrade.setEnd_settle_amount(ttTrdTrust.getEnd_settle_amount());
		ttTrdTrade.setStrike_yield(ttTrdTrust.getStrike_yield());
		ttTrdTrade.setExecid(ttTrdTrust.getExecid());
		return ttTrdTrade;
	}

	/**
	 * 更新审批单余额信息
	 * @param order_id
	 * @param addAmount
	 * @param subAmount
	 */
	@Override
	public void updateTrustRemainAmount(TtTrdTrust ttTrdTrust,double oldAmount,double newAmount,String opUserId){
		//1.计算审批单额度
		double used_amount = ttTrdTrust.getUsed_amount();
		double remain_amount = ttTrdTrust.getRemain_amount();
		double pretrust_amount = ttTrdTrust.getPretrust_amount();
		used_amount = used_amount - oldAmount + newAmount;
		remain_amount = remain_amount + oldAmount - newAmount;
		//JY.require(remain_amount >= 0, "委托单["+ttTrdTrust.getOrder_id()+"]剩余额度不足.");
		JY.info("委托单信息:" + ttTrdTrust.toString());
		JY.info("used_amount:" + used_amount + ",remain_amount:" + remain_amount + ",pretrust_maount:" + pretrust_amount);
		
		ttTrdTrust.setRemain_amount(remain_amount);
		ttTrdTrust.setUsed_amount(used_amount);
		
		//2.审批单状态判断
		String trust_status = "";
		if(MathUtil.isEqual(BigDecimal.ROUND_HALF_UP, remain_amount, pretrust_amount)){
			//演示模式,退回至新建状态（正式环境不开放该功能）
			if(userParamService.getBooleanSysParamByName("system.isCreateTradeForDemonstrate",false)){
				trust_status = DictConstants.TrustStatus.NEW;
			}else{
				trust_status = DictConstants.TrustStatus.SENDED;
			}
			//当已用金额重置为0时，判断是否关联下行数据。当关联下行数据则需要取消关联关系
			if(!StringUtil.checkEmptyNull(ttTrdTrust.getExecid())){
				JY.info("当已用金额重置为0时，判断是否关联下行数据。当关联下行数据则需要取消关联关系");
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("dealNumber", ttTrdTrust.getExecid());
//				map.put("matchStatus", DictConstants.MatchingType.New);
				map.put("updateTime", DateUtil.getCurrentDateTimeAsString());
				map.put("updateUser", opUserId);
				map.put("matchType2", "true");
//				executionReportService.updateByExecId(map);
				ttTrdTrust.setExecid("");
			}
		}else if(used_amount == pretrust_amount){
			trust_status = DictConstants.TrustStatus.SINCONFIRMED;
		}else{
			// 设置为 全部匹配。不存在部分匹配过程
			trust_status = DictConstants.TrustStatus.SINCONFIRMED;
		}
		ttTrdTrust.setTrust_status(trust_status);
		ttTrdTrust.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrust.setOperator_id(opUserId);
		//3.更新纪录
		trustManageDao.updateTrust(ttTrdTrust);
	}

}
