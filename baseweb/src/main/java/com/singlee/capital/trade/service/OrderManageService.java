package com.singlee.capital.trade.service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;

import java.util.HashMap;

/**
 * 交易单管理业务层
 * @author thinkpad
 *
 */
public interface OrderManageService {

	/**
	 * 不区分业务查询交易对手分页
	 * @param map
	 * @return TrdTradeVo
	 */
	Page<TtTrdOrder> getOrderVoPage(HashMap<String,Object> map);

	
}
