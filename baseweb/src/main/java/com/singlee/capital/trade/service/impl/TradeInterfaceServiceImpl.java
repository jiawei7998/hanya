package com.singlee.capital.trade.service.impl;

import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TradeEditVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.TradeInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 交易单对内接口
 * @author kevin_gm
 *
 */
@Service("tradeInterfaceService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeInterfaceServiceImpl implements TradeInterfaceService {
	
	/**交易单dao**/
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	/**
	 * 保存交易单对象
	 * @param ttTrdTrade
	 */
	@Override
	public TtTrdTrade saveTrdTrade(TtTrdTrade ttTrdTrade){

//		//增加逻辑：机构轧帐完成后，判断是否允许保存交易单
//		approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		
		if(!DictConstants.TrdType.BondExercise.equals(ttTrdTrade.getTrdtype())){
			JY.raise("执行交易单保存接口传入业务类型错误.");
		}
		if(StringUtil.checkEmptyNull(ttTrdTrade.getTrade_id())){
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("trdtype", ttTrdTrade.getTrdtype());
			String trade_id = tradeManageMapper.getTradeId(map);
			ttTrdTrade.setTrade_id(trade_id);
			ttTrdTrade.setVersion_number(0);
			tradeManageMapper.insertTrade(ttTrdTrade);
		}else{
			tradeManageMapper.updateTrade(ttTrdTrade);
		}
		return ttTrdTrade;
	}
	
	/**
	 * 读取交易单对象接口
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	@Override
	public TtTrdTrade selectTrdTrade(String trade_id,double version_number){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("version_number", version_number);
		JY.require(!StringUtil.checkEmptyNull(trade_id), "交易单编号不允许为空.");
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		return ttTrdTrade;
	}
	/**
	 * 读取交易单对象接口
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	@Override
	public TrdTradeVo selectTrdTradeVo(String trade_id,double version_number){
		TtTrdTrade ttTrdTrade = selectTrdTrade(trade_id, version_number);
		TrdTradeVo trdTradeVo = new TrdTradeVo();
		ParentChildUtil.fatherToChildWithoutException(ttTrdTrade, trdTradeVo);
		return trdTradeVo;
	}
	
	/**
	 * 读取交易单对象接口
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	@Override
	public TrdTradeVo selectTrdTradeVoByMap(HashMap<String, Object> map){
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		TrdTradeVo trdTradeVo = new TrdTradeVo();
		ParentChildUtil.fatherToChildWithoutException(ttTrdTrade, trdTradeVo);
		return trdTradeVo;
	}
	
	/**
	 * 合约类交易通过金融工具查找交易单
	 * @param i_code
	 * @param a_type
	 * @param m_type
	 * @param version_number
	 * @return
	 */
	@Override
	public TrdTradeVo selectTtTrdTradeFromInstrument(String i_code,String a_type,String m_type,double version_number){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("i_code", i_code);
		map.put("a_type", a_type);
		map.put("m_type", m_type);
		map.put("version_number", version_number);
		map.put("is_active", DictConstants.YesNo.YES);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		if(ttTrdTrade == null) {
			return null;}
		TrdTradeVo trdTradeVo = new TrdTradeVo();
		ParentChildUtil.fatherToChildWithoutException(ttTrdTrade, trdTradeVo);
		return trdTradeVo;
	}
	/**
	 * 非合约类交易通过金融工具查找交易记录
	 * @param i_code
	 * @param a_type
	 * @param m_type
	 * @param version_number
	 * @return
	 */
	@Override
	public List<TtTrdTrade> selectTtTrdTrade(HashMap<String, Object> map){
		return tradeManageMapper.selectTtTrdTrade(map);
	}
	@Override
	public TrdTradeVo selectTtTrdTradeFromInstrumentId(InstrumentId instrumentId) {
		if(instrumentId == null) {
			return null;}
		//获取金融工具
		//TrdTradeVo trade = null;
//		if(instrumentId.getA_type().equals(DictConstants.AType.Repo)){
//			TtTrdIblbInstrument orgInstrument = (TtTrdIblbInstrument)instrumentGen.getT();
//			if(orgInstrument == null)
//				return null;
//			//获取交易
//			trade = selectTtTrdTradeFromInstrument(orgInstrument.getI_code(),orgInstrument.getA_type(),orgInstrument.getM_type(),orgInstrument.getTrade_version());
//		}
//		else if(instrumentId.getA_type().equals(DictConstants.AType.IBFixed)){
//			TtTrdIbftInstrument orgInstrument = (TtTrdIbftInstrument)instrumentGen.getT();
//			if(orgInstrument == null)
//				return null;
//			//获取交易
//			trade = selectTtTrdTradeFromInstrument(orgInstrument.getI_code(),orgInstrument.getA_type(),orgInstrument.getM_type(),orgInstrument.getTrade_version());
//		}
		return null;
	}
	
//	@Override
//	public List<TrdTradeVo> selectTradeList4Credit(InstrumentId instrumentId) {
//		if(instrumentId == null)
//			return null;
//		return trade;
//	}

	/**
	 * 查询交易单bo对象
	 * @param map
	 * @return
	 */
	@Override
	public List<TtTrdTrade> selectTtTrdTrade4Limit(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		return tradeManageMapper.selectTtTrdTrade4Limit(map);
	}
	
	/**
	 * 查询交易单对象
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	@Override
	public TradeEditVo selectTradeEditForTradeId(String trade_id,int version_number){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("version_number", version_number);
		JY.require(!StringUtil.checkEmptyNull(trade_id), "交易单编号不允许为空.");
		return tradeManageMapper.selectTradeEditForTradeId(map);
	}
	
	@Override
	public TtTrdTrade selectTrade4creditInit(String trdType, String inSecuAccId, String outCashAccid, InstrumentId instrumentId){
		Map<String, Object> params = new HashMap<String, Object>();
		if(DictConstants.TrdType.BondBuy.equals(trdType) || DictConstants.TrdType.MoneyBuy.equals(trdType) ||
				DictConstants.TrdType.AssetPlanBuy.equals(trdType)){
			params.put("selfInSecuAccid", inSecuAccId);
			params.put("iCode", instrumentId.getI_code());
			params.put("aType", instrumentId.getA_type());
			params.put("mType", instrumentId.getM_type());
		}
		else if(DictConstants.TrdType.CurrentToBankTransfer.equals(trdType)){
			params.put("partyOutCashAccid", outCashAccid);
		}
		params.put("trdType", trdType);
		List<TtTrdTrade> trades = tradeManageMapper.selectTtTrdTrade4Credit(params);
		return trades == null || trades.size() == 0?null:trades.get(0);
	}
	
}
