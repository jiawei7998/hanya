package com.singlee.capital.trade.pojo;

import com.singlee.capital.trade.model.TtTrdTrade;

public class TrdTradeVo extends TtTrdTrade{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//交易扩展信息
	/** 实际结算天数 **/
	private int actSettleDays;
	/**
	 * 交易对手名称
	 */
	private String party_name;
	/**
	 * 产品名称
	 * @return
	 */
	private String prd_name;
	/**
	 * 产品种类
	 */
	private String prd_type;
	
	
	public int getActSettleDays() {
		return actSettleDays;
	}
	public void setActSettleDays(int actSettleDays) {
		this.actSettleDays = actSettleDays;
	}
	public String getParty_name() {
		return party_name;
	}
	public void setParty_name(String party_name) {
		this.party_name = party_name;
	}
	public String getPrd_name() {
		return prd_name;
	}
	public void setPrd_name(String prd_name) {
		this.prd_name = prd_name;
	}
	public String getPrd_type() {
		return prd_type;
	}
	public void setPrd_type(String prd_type) {
		this.prd_type = prd_type;
	}


}
