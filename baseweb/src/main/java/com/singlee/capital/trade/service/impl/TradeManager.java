package com.singlee.capital.trade.service.impl;

import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.pojo.TrdTradeVo;

/**
 * 交易管理类
 * @author Lihb
 *
 */
public class TradeManager {

    private	DayendDateService dayendDateService = SpringContextHolder.getBean("dayendDateService");

	/**
	 * 审批单转换临时交易
	 * @param order			审批单对象
	 * @return TtTrdTrade  交易对象
	 */
    public TrdTradeVo getTradeByOrder(TtTrdOrder order)
    {
        if (order == null) {
        	return null;}
        //交易对象
        TrdTradeVo trade = new TrdTradeVo();
        trade.setTrade_id("-1");
        trade.setVersion_number(0);
        trade.setI_code(order.getI_code());
        trade.setA_type(order.getA_type());
        trade.setM_type(order.getM_type());
        trade.setOrder_id(order.getOrder_id());
        trade.setIn_tradeid(order.getIn_orderid());
        trade.setOperator_id(order.getUser_id());
        trade.setTrdtype(order.getTrdtype());
        trade.setTrade_user(order.getSelf_traderid());
        //账户信息
        trade.setSelf_insecuaccid(order.getSelf_insecuaccid());
        trade.setSelf_incashaccid(order.getSelf_incashaccid());
        trade.setParty_insecuaccid(order.getParty_insecuaccid());
        trade.setParty_incashaccid(order.getParty_incashaccid());
        trade.setSelf_outsecuaccid(order.getSelf_outsecuaccid());
        trade.setSelf_outcashaccid(order.getSelf_outcashaccid());
        trade.setParty_outcashaccid(order.getParty_outcashaccid());
        trade.setSelf_traderid(order.getSelf_traderid());
        trade.setTrade_date(order.getOrder_date());
        trade.setSettle_days(order.getSettle_days());
        trade.setFst_settle_date(order.getFst_settle_date());
        trade.setEnd_settle_date(order.getEnd_settle_date());
        trade.setFst_settle_amount(order.getFst_settle_amount());
        trade.setEnd_settle_amount(order.getEnd_settle_amount());
        trade.setTradeamount(order.getFst_settle_amount());
        trade.setExe_market(order.getExe_market());
        trade.setTotalamount(order.getTotalamount());
        trade.setTotalcount(order.getTotalcount());
        trade.setAiamount(order.getAiamount());
        trade.setAiprice(order.getAiprice());
        trade.setNetprice(order.getNetprice());
        trade.setFst_settle_type(order.getFst_settle_type());
        trade.setEnd_settle_type(order.getEnd_settle_type());
        trade.setI_name(order.getI_name());
        trade.setParty_id(order.getParty_id());
        trade.setSelf_bankacccode(order.getSelf_bankacccode());
        trade.setSelf_bankaccname(order.getSelf_bankaccname());
        trade.setSelf_bankcode(order.getSelf_bankcode());
        trade.setSelf_bankname(order.getSelf_bankname());
        trade.setSelf_largepaymentcode(order.getSelf_largepaymentcode());
        trade.setParty_bankacccode(order.getParty_bankacccode());
        trade.setParty_bankaccname(order.getParty_bankaccname());
        trade.setParty_bankcode(order.getParty_bankcode());
        trade.setParty_bankname(order.getParty_bankname());
        trade.setParty_largepaymentcode(order.getParty_largepaymentcode());
        trade.setParty_zzdaccid(order.getParty_zzdaccid());
        trade.setZzd_netprice(order.getZzd_netprice());
        trade.setZzd_ytm(order.getZzd_ytm());
        trade.setYtm(order.getYtm());
        trade.setGroup_id(order.getGroup_id());
        trade.setTrade_status(DictConstants.ApproveStatus.Executed);
        trade.setTerm_days(order.getTerm_days());
        trade.setActSettleDays(DateUtil.daysBetween(dayendDateService.getSettlementDate(),trade.getTrade_date()));
        return trade;
    }
    public TrdTradeVo getTradeByOrder(TtTrdOrder order, boolean isPart){
    	TrdTradeVo trade = getTradeByOrder(order);
//    	if(isPart){
//	    	InstrumentId id = new InstrumentId(trade.getI_code(),trade.getA_type(),trade.getM_type());
//			if(Constants.isBond(id)){
//				trade.setTotalamount(order.getRemain_amount());
//				trade.setTotalcount(order.getRemain_amount()/SettleCommonInfo.getFaceValueByInstrumentId(id));
//				trade.setNetamount(trade.getNetprice() * trade.getTotalcount());
//				trade.setAiamount(trade.getAiprice() * trade.getTotalcount());
//				trade.setFullamount(trade.getFullprice()* trade.getTotalcount());
//				trade.setTradeamount(trade.getNetamount() + trade.getAiamount());
//				trade.setFst_settle_amount(trade.getTradeamount());
//			}
//			else{
//				trade.setTotalamount(order.getRemain_amount());
//				trade.setTotalcount(order.getRemain_amount());
//				trade.setTradeamount(trade.getTotalamount());
//				trade.setFst_settle_amount(trade.getTotalamount());
//			}
//    	}
		return trade;
    }
}
