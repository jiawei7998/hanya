package com.singlee.capital.trade.service;

import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TrdTradeVo;

import java.util.HashMap;
import java.util.Map;


/**
 * 交易单业务层接口，所有业务必须实现该接口
 * @author kevin_gm
 *
 */
public interface TradeServiceListener {

	/**
	 * 交易单查询
	 * @param map
	 * @return
	 */
	public Object searchTradeList(HashMap<String,Object> map,int pageNum,int pageSize);
	
	/**
	 * 查询交易单明细信息
	 * @param map
	 * @return
	 */
	public void selectTradeInfo(HashMap<String,Object> map);
	
	/**
	 * 注销交易单信息
	 * @param trade_id
	 */
	public void cancelTrade(TtTrdTrade ttTrdTrade);
	
	/**
	 * 确认交易单信息
	 * @param trade_id
	 */
	public void dealConfirmTrade(TtTrdTrade ttTrdTrade);
	
	/**
	 * 取消确认交易单信息
	 * @param trade_id
	 */
	public void withdrawTrade(TrdTradeVo trdTradeVo);
	
	/**
	 * 交易单保存方法
	 * @param map：前台传入参数
	 * @param ttTrdTrade：默认赋值后的ttTrdTrade对象
	 * @param isAdd：true 表示当前执行新增操作
	 */
	public void saveTrade(HashMap<String, Object> map,TtTrdTrade ttTrdTrade, boolean isAdd);
	
	/**
	 * 交易单审批拒绝
	 * @param ttTrdOrder
	 */
	public void noPassApprove(TtTrdTrade ttTrdTrade);
	
	
	
//	public ExecutionReportCSTP parseTradeImport(Workbook wb,HashMap<String, Object> map);
//	/**
//	 * 新增加一条数据main表（金融工具表）
//	 * @param dealNo
//	 */
//	public void modifyApproveMainVersion(Map<String,Object> map);
	void beforeSave(Map<String,Object> map);
	
	void afterSave(Map<String,Object> map,TtTrdTrade trade);
	
}
