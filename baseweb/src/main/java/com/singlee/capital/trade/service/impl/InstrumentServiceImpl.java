package com.singlee.capital.trade.service.impl;

import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.pojo.InstrumentIdGeneric;
import com.singlee.capital.trade.service.InstrumentService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 
 * @author LyonChen
 *
 */
@Service("instrumentService")
public class InstrumentServiceImpl implements InstrumentService {

//	@Autowired
//	private IbftInstrumentService ibftInstrumentService;
//	@Autowired
//	private IblbInstrumentService iblbInstrumentService;
//
//	@Autowired
//	private IBondCache iBondCache;
	
	@Override
	public <T extends Serializable> InstrumentIdGeneric<T> getInstrument(InstrumentId id) {
		if (id == null) { return null;}
		
		InstrumentIdGeneric<T> ret = new InstrumentIdGeneric<T>(id);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("i_code", id.getI_code());
		map.put("a_type", id.getA_type());
		map.put("m_type", id.getM_type());
		if(id.getVer_num() != -1) {
			map.put("version_number", id.getVer_num());}
//		if (DictConstants.AType.IBFixed.compare(id.getA_type())) {
//			TtTrdIbftInstrument ibft = ibftInstrumentService.selectIbftInstrument(map);
//			ret.setT((T) ibft);
//		}else if (DictConstants.AType.Repo.compare(id.getA_type())) {
//			TtTrdIblbInstrument iblb = iblbInstrumentService.selectTtTrdIblbInstrumentForTradeId(map);			
//			ret.setT((T) iblb);
//		}else if (DictConstants.AType.Bond.compare(id.getA_type())){
//			IBond bond = iBondCache.get(id);
//			ret.setT((T)bond);
//		}
		return ret;
	}

}
