package com.singlee.capital.trade.service;

import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TradeEditVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;

import java.util.HashMap;
import java.util.List;

/**
 * 交易单对内接口
 * @author kevin_gm
 *
 */
public interface TradeInterfaceService {
	/**
	 * 保存交易单对象
	 * @param ttTrdTrade
	 */
	public TtTrdTrade saveTrdTrade(TtTrdTrade ttTrdTrade);
	
	/**
	 * 读取交易单对象接口
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	public TtTrdTrade selectTrdTrade(String trade_id,double version_number);
	
	/**
	 * 读取交易单对象接口
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	public TrdTradeVo selectTrdTradeVo(String trade_id,double version_number);
	/**
	 * 合约类交易通过金融工具查找交易单
	 * @param i_code
	 * @param a_type
	 * @param m_type
	 * @param version_number
	 * @return
	 */
	public TrdTradeVo selectTtTrdTradeFromInstrument(String i_code,String a_type,String m_type,double version_number);
	/**
	 * 合约类交易通过金融工具Id查找交易单
	 * @param instrumentId
	 * @return
	 */
	public TrdTradeVo selectTtTrdTradeFromInstrumentId(InstrumentId instrumentId);
	/**
	 * 查询交易单bo对象
	 * @param map
	 * @return
	 */
	public List<TtTrdTrade> selectTtTrdTrade4Limit(HashMap<String, Object> map);
	/**
	 * 查询交易单对象
	 * @param trade_id
	 * @param version_number
	 * @return
	 */
	public TradeEditVo selectTradeEditForTradeId(String trade_id,int version_number);

	TtTrdTrade selectTrade4creditInit(String trdType, String inSecuAccId, String outCashAccid, InstrumentId instrumentId);

	TrdTradeVo selectTrdTradeVoByMap(HashMap<String, Object> map);

	List<TtTrdTrade> selectTtTrdTrade(HashMap<String, Object> map);
	
}
