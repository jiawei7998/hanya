package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

/**
 *存量数据移植
 */
public interface BaseExcelService {
	
	/**
	 * 基础资产信息导入
	 * @param baseAssetList
	 * @author wuyc
	 */
	public void initBaseAsset(List<Map<String, Object>> baseAssetList);
	
	/**
	 * 增信措施
	 * @param ICList
	 */
	public void initIncreaseCredit(List<Map<String,Object>> ICList);
	
	/**
	 * 风险资产
	 * @param raList
	 */
	public void initRiskAasset(List<Map<String,Object>> raList);
	
	/**
	 * 保证金
	 * @param raList
	 */
	public void initRiskSlowRelease(List<Map<String,Object>> raList);

	/**
	 * 票据基础资产
	 * @param raList
	 */
	public void initBaseAssetBill(List<Map<String,Object>> raList);
	
	/**
	 * 股票信息
	 * @param raList
	 */
	public void initStockInfo(List<Map<String,Object>> siList);
	
	/**
	 * 交叉营销
	 * @param raList
	 */
	public void initCrossMarket(List<Map<String,Object>> cmList);

}
