package com.singlee.capital.trade.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TradeEditVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 交易单dao
 * @author kevin_gm
 *
 */
public interface TrdTradeMapper {

	/**
	 * 根据审批单编号查询审批记录对象，此方法带行级锁
	 * @param map
	 * @return
	 */
	public TtTrdTrade selectTradeForTradeId(HashMap<String,Object> map);
	
	/**
	 * 交易单编辑时读取交易单对象
	 * @param map
	 * @return
	 */
	public TradeEditVo selectTradeEditForTradeId(HashMap<String,Object> map);
	
	/**
	 * 获取审批单编号
	 * @return
	 */
	public String getTradeId(HashMap<String,Object> map);

	/**
	 * 审批单插入
	 * @param ttTrdOrder
	 */
	public void insertTrade(TtTrdTrade ttTrdTrade);
	
	/**
	 * 审批单更新
	 * @param ttTrdOrder
	 */
	public void updateTrade(TtTrdTrade ttTrdTrade);
	/**
	 * 审批单更新
	 * @param ttTrdOrder
	 */
	public void updateImportTrade(TtTrdTrade ttTrdTrade);
	
	/**
	 * 交易单Bo对象查询
	 * @param ttTrdTrade
	 */
	public List<TtTrdTrade> selectTtTrdTrade(HashMap<String,Object> map);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易单列表
	 * @author Hunter
	 * @date 2016-9-23
	 */
	public Page<TtTrdTrade> selectTtTrdTrade(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 交易单Bo对象分页查询
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TrdTradeVo> getTradeVoPage(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 交易单Bo对象查询
	 * @param ttTrdTrade
	 */
	public List<TradeEditVo> searchTradeListForCharges(HashMap<String,Object> map);
	/**
	 * 交易单Bo对象查询 限额专用
	 * @param ttTrdTrade
	 */
	public List<TtTrdTrade> selectTtTrdTrade4Limit(HashMap<String,Object> map);
	
	List<TtTrdTrade> selectTtTrdTrade4Credit(Map<String,Object> map);
	/**
	 * 现券买卖交易单查询
	 * @param map
	 * 	order_id_search：审批单号
	 *  trdtype_search：业务类型（数组）
	 *  order_status_search：审批单状态（数组）
	 *  begin_search：交易日期开始
	 *  end_search：交易日期结束
	 *  part_name_search：交易对手名称
	 *  user_name_search：提交人名称
	 *  login_user_id：当前登录柜员id
	 *  begin_num 开始记录数
	 * 	end_num 结束记录数
	 * user_id：操作员
	 * @return
	 */
	public List<TradeEditVo> searchTradeList(HashMap<String, Object> map);

	/**
	 * 现券买卖委托单查询(数量)
	 * @param map (同 searchApproveList 方法)
	 * @return
	 */
	public int searchTradeListCount(HashMap<String, Object> map);
	/**
	 * 现券买卖委托单查询(数量)
	 * @param map (同 searchApproveList 方法)
	 * @return
	 */
	public int searchTradeListForChargesCount(HashMap<String, Object> map);
	
	/**
	 * 根据trade_id或order_id查询激活状态交易单
	 * @return
	 */
	public TtTrdTrade selectOneTradeIdorOrderId(Map<String,Object> map);
	
	/**
	 * 删除交易单
	 * @param map
	 * @return
	 */
	public int delTrade(Map<String,Object> map);
	
	
}
