package com.singlee.capital.trade.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.trade.service.OrderManageService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;

/**
 * 交易单管理业务层
 * @author thinkpad
 *
 */
@Service("orderManageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OrderManageServiceImpl implements OrderManageService{
	
	/** 审批单dao **/
	@Autowired
	private TrdOrderMapper trdOrderMapper;

	@Override
	public Page<TtTrdOrder> getOrderVoPage(HashMap<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		String productType = ParameterUtil.getString(map, "custom_product_search", "");
		if(StringUtil.isNotEmpty(productType)){
			String[] PrdTypeArr =productType.split(",");
			map.put("PrdTypeList", Arrays.asList(PrdTypeArr));
		}
		return trdOrderMapper.getOrderVoPage(map, rowBounds);
	}
	

	
}
