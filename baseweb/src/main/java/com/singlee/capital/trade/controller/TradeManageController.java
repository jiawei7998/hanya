package com.singlee.capital.trade.controller;

import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TradeEditVo;
import com.singlee.capital.trade.service.TradeInterfaceService;
import com.singlee.capital.trade.service.TradeManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
/**
 * @projectName 同业业务管理系统
 * @className 存续期管理控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-22 下午19:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/TradeManageController")
public class TradeManageController extends CommonController {
	
	@Autowired
	TradeManageService tradeManageService;
	@Autowired
	private TradeInterfaceService tradeInterfaceService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-22
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTrade")
	public RetMsg<?> searchPageApprove(@RequestBody Map<String,Object> params){
		return RetMsgHelper.ok(tradeManageService.searchTradeList((HashMap<String, Object>) params));
	}
	
	/**
	 * 不区分业务条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author wang
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeWithoutFolder")
	public RetMsg<?> searchPageTradeWithoutFolder(@RequestBody Map<String,Object> params){
		params.put("sponInst", SlSessionHelper.getInstitutionId());
		return RetMsgHelper.ok(tradeManageService.searchTradeListWithoutFolder((HashMap<String, Object>) params));
	}
	
	/**
	 * 不区分业务条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author wang
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/getTradeVoPage")
	public RetMsg<?> getTradeVoPage(@RequestBody Map<String,Object> params){
		params.put("sponInst", SlSessionHelper.getInstitutionId());
		return RetMsgHelper.ok(tradeManageService.getTradeVoPage((HashMap<String, Object>) params));
	}
	
	/**
	 * 根据tradeid和version查询
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTrdTradeByTradeId")
	public RetMsg<TradeEditVo> getTrdTradeByTradeId(@RequestBody Map<String,Object> params){
		String tradeId= ParameterUtil.getString(params, "tradeId", "");
		int version = ParameterUtil.getInt(params, "version", 0);
		return RetMsgHelper.ok(tradeInterfaceService.selectTradeEditForTradeId(tradeId,version));
	}
	/**
	 * 交易单保存
	 * @param map
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTrade")
	public TtTrdTrade saveTrade(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("opUserId", SlSessionHelper.getUserId());
		map.put("accInCash", SlSessionHelperExt.getInCashAccId());
		String tradeId=ParameterUtil.getString(map, "dealNo", "");
		HashMap<String,Object> cMap = new HashMap<String,Object>();
		cMap.put("trade_id", tradeId);
		TtTrdTrade trd = tradeManageService.searchTradeInfo(cMap);
		if(null != trd) {
			map.putAll(BeanUtil.beanToMap(trd));}
		return tradeManageService.saveTradeInfo(map);
	}
}
