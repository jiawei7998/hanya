package com.singlee.capital.trade.pojo;

import com.singlee.capital.trade.model.TtTrdTrade;

/**
 * 交易单编辑时查询对象
 * @author kevin_gm
 *
 */
public class TradeEditVo extends TtTrdTrade {

	private static final long serialVersionUID = -6277705410914439596L;
	
	private String self_insecuaccid_name;
	private String self_outcashaccid_name;
	private String insider_party_id_name;
	private String outsider_party_id_name;
	private String outsider_user_code_name;
	
	private String b_name;
	private String accname;
	private String user_name;
	private String party_shortname;
	private String v_i_code;
	private String v_i_name;
	private String u_i_code;
	private String u_i_name;
	public String getSelf_insecuaccid_name() {
		return self_insecuaccid_name;
	}
	public void setSelf_insecuaccid_name(String self_insecuaccid_name) {
		this.self_insecuaccid_name = self_insecuaccid_name;
	}
	public String getSelf_outcashaccid_name() {
		return self_outcashaccid_name;
	}
	public void setSelf_outcashaccid_name(String self_outcashaccid_name) {
		this.self_outcashaccid_name = self_outcashaccid_name;
	}
	public String getInsider_party_id_name() {
		return insider_party_id_name;
	}
	public void setInsider_party_id_name(String insider_party_id_name) {
		this.insider_party_id_name = insider_party_id_name;
	}
	public String getOutsider_party_id_name() {
		return outsider_party_id_name;
	}
	public void setOutsider_party_id_name(String outsider_party_id_name) {
		this.outsider_party_id_name = outsider_party_id_name;
	}
	public String getOutsider_user_code_name() {
		return outsider_user_code_name;
	}
	public void setOutsider_user_code_name(String outsider_user_code_name) {
		this.outsider_user_code_name = outsider_user_code_name;
	}
	public String getB_name() {
		return b_name;
	}
	public void setB_name(String b_name) {
		this.b_name = b_name;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getParty_shortname() {
		return party_shortname;
	}
	public void setParty_shortname(String party_shortname) {
		this.party_shortname = party_shortname;
	}
	public String getV_i_code() {
		return v_i_code;
	}
	public void setV_i_code(String vICode) {
		v_i_code = vICode;
	}
	public String getV_i_name() {
		return v_i_name;
	}
	public void setV_i_name(String vIName) {
		v_i_name = vIName;
	}
	public String getU_i_code() {
		return u_i_code;
	}
	public void setU_i_code(String uICode) {
		u_i_code = uICode;
	}
	public String getU_i_name() {
		return u_i_name;
	}
	public void setU_i_name(String uIName) {
		u_i_name = uIName;
	}
	
}
