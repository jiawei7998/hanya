package com.singlee.capital.trade.service;

/**
 * 存续期接口
 * @author x220
 *
 */
public interface DurationService {
	
	/**
	 * 删除提前到期
	 * @param dealNo
	 */
	public int delAdvanceMaturity(String dealNo);
	
	/**
	 * 删除资产卖断
	 * @param dealNo
	 * @return
	 */
	public int delMultiResale(String dealNo);

}
