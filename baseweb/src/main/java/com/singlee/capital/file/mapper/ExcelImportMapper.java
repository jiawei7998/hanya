package com.singlee.capital.file.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 核算配置 dao 接口定义
 * @author ZLF
 *
 */
public interface ExcelImportMapper {
	
	/**
	 * 插入数据到表
	 * @author zlf
	 */
	public void commonDbInsert(Map<String,String> sql);
	
	/**
	 * 更新表
	 * @author zlf
	 */
	public void commonDbUpdate(Map<String,String> sql);
	
	/**
	 * 删除表数据
	 * @author zlf
	 */
	public void commonDbDelete(Map<String,String> sql);
	
	
	/**
	 * 查询配置字典表
	 * @author zlf
	 */
	public List<HashMap<String, String>> queryConfigSetting();
	/**
	 * 分页查询配置内容表
	 * @author zlf
	 */
	
	public Page<HashMap<String, String>> queryEntrySetting(Map<String,String> map, RowBounds rb);
	

	/**
	 * 查询列名
	 * @author zlf
	 */
	public List<HashMap<String, String>> queryGridColumn();
}
