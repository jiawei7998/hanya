package com.singlee.capital.cashflow.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowFee;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TtCashflowFeeMapper extends Mapper<TtCashflowFee> {
	
	/**
	 * 分页查询列表
	 * @param map 
	 * @param rb - rowBounds
	 * @return Page<TtCashflowFee>
	 * @date 2016-9-8
	 */
	Page<TtCashflowFee> PageCFFee(Map<String, Object> map, RowBounds rb);
	
	
}