package com.singlee.capital.cashflow.service;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import com.singlee.capital.cashflow.model.TtCashflowDailyInterest;
import com.singlee.capital.cashflow.model.TtCashflowFee;
import com.singlee.capital.cashflow.model.TtCashflowInterest;

import java.util.List;
import java.util.Map;

public interface CashflowService {

	/**
	 * 根据对象查询现金流列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	public Page<TtCashflowCapital> pageCfCapital(Map<String,Object> params);
	
	/**
	 * 根据对象查询现金流(利息)列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	public Page<TtCashflowInterest> pageCfInterest(Map<String,Object> params);
	
	/**
	 * 获取本金现金流列表
	 * @param params
	 * @return
	 */
	public List<TtCashflowCapital> getCfCapitalList(Map<String, Object> params);
	/**
	 * 获取利息现金流列表
	 * @param params
	 * @return
	 */
	public List<TtCashflowInterest> getCfInterestList(Map<String, Object> params);

	/**
	 * 获取date日是理论收付日的本金现金流列表
	 * @param data
	 * @return
	 */
	public List<TtCashflowCapital> getCfCapitalList(String date);
	/**
	 * 获取date日是理论收付日的利息现金流列表
	 * @param params
	 * @return
	 */
	public List<TtCashflowInterest> getCfInterestList(String date);
	

	/**
	 * 获得 需要重新定盘的记录 
	 * @param date
	 * @return
	 */
	public List<TtCashflowInterest> getCfInterestListByRefixing(String date) ;
	
	/**
	 * 删除现金流
	 * @param i
	 * @param a
	 * @param m
	 * @return
	 */
	public int deleteCashFlow(String i,String a,String m);
	
	/**
	 * 新增现金流
	 * @param list1
	 * @param list2
	 */
	public void insertCashFlow(List<TtCashflowInterest> list1,List<TtCashflowCapital> list2);
	
	/**
	 * 新增中间业务收入现金流
	 * @map
	 */
	public void insertCashFlowFee(TtCashflowFee fee);
	
	/**
	 * 查询每日计提现金流信息
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	public Page<TtCashflowDailyInterest> pageCfDailyInterest(Map<String,Object> params);

}
