package com.singlee.capital.cashflow.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowDailyInterest;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TtCashflowDailyInterestMapper extends Mapper<TtCashflowDailyInterest> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 获取每日计提利息现金流列表
	 * @param map
	 * @return
	 */
	Page<TtCashflowDailyInterest> PageCfDailyInterestList(Map<String, Object> map, RowBounds rb);
	/**
	 * 删除计提现金流
	 * @param map
	 */
	void deleteTtCashflowDailyInterest(Map<String, Object> map);

}