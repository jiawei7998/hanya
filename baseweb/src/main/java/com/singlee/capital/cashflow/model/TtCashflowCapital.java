package com.singlee.capital.cashflow.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 现金流(利息) 类
 * 
 * @author wang
 * 
 */

@Entity
@Table(name = "TT_CASHFLOW_CAPITAL")
public class TtCashflowCapital implements Serializable,Comparable<TtCashflowCapital> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_CAPITAL.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	private String refBeginDate;// 开始日
	private String refEndDate;// 结束日
	private String theoryPaymentDate;// 理论收付日期
	private String paymentDate;// 实际收付日期
	
	private String cfType;// 现金流类型
	private String payDirection;// 收付方向
	private double endPrincipal;// 期末支付本金
	private double principal; // 本金
	private String iCode;
	private String aType;
	private String mType;
	private String seqNumber;// 序列
	private String cfEvent;// 现金流事件
	
	
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public String getTheoryPaymentDate() {
		return theoryPaymentDate;
	}
	public void setTheoryPaymentDate(String theoryPaymentDate) {
		this.theoryPaymentDate = theoryPaymentDate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public double getEndPrincipal() {
		return endPrincipal;
	}
	public void setEndPrincipal(double endPrincipal) {
		this.endPrincipal = endPrincipal;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getCfEvent() {
		return cfEvent;
	}
	public void setCfEvent(String cfEvent) {
		this.cfEvent = cfEvent;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	@Override
	public int compareTo(TtCashflowCapital o) {
		int c1 = this.getTheoryPaymentDate().compareTo(o.getTheoryPaymentDate());
		if(c1 == 0 ){
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			if(c2==0){
				int c3 =this.getRefEndDate().compareTo(o.getRefEndDate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}
	
}
