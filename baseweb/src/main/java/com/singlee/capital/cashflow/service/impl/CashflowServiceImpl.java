package com.singlee.capital.cashflow.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.TtCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TtCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TtCashflowFeeMapper;
import com.singlee.capital.cashflow.mapper.TtCashflowInterestMapper;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import com.singlee.capital.cashflow.model.TtCashflowDailyInterest;
import com.singlee.capital.cashflow.model.TtCashflowFee;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import com.singlee.capital.cashflow.service.CashflowService;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 现金流 服务层
 * 
 * @author Lyon Chen
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CashflowServiceImpl implements CashflowService {
	
	@Autowired
	private TtCashflowCapitalMapper cfCapitalMapper;
	
	@Autowired
	private TtCashflowInterestMapper cfInterestMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TtCashflowFeeMapper cfFeeMapper;
	@Autowired
	private TtCashflowDailyInterestMapper ttCashflowDailyInterestMapper;
	
	
	/**
	 * 根据对象查询现金流列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@Override
	public Page<TtCashflowCapital> pageCfCapital(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TtCashflowCapital> result = cfCapitalMapper.pageCfCapital(params, rowBounds);
		return result;
	}

	/**
	 * 根据对象查询现金流(利息)列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@Override
	public Page<TtCashflowInterest> pageCfInterest(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TtCashflowInterest> result = cfInterestMapper.pageCfInterest(params, rowBounds);
		return result;
	}
	@Override
	public List<TtCashflowCapital> getCfCapitalList(Map<String, Object> params){
		String iCode = ParameterUtil.getString(params, "iCode", "");
		if(StringUtil.isEmpty(iCode)){
			return new ArrayList<TtCashflowCapital>();
		}
		return cfCapitalMapper.getCfCapitalList(params);
	}
	@Override
	public List<TtCashflowInterest> getCfInterestList(Map<String, Object> params){
		String iCode = ParameterUtil.getString(params, "iCode", "");
		if(StringUtil.isEmpty(iCode)){
			return new ArrayList<TtCashflowInterest>();
		}
		return cfInterestMapper.getCfInterestList(params);
	}

	@Override
	public List<TtCashflowCapital> getCfCapitalList(String date) {
		return cfCapitalMapper.sumByTheoryPaymentDate(date);
	}

	@Override
	public List<TtCashflowInterest> getCfInterestList(String date) {
		return cfInterestMapper.sumByTheoryPaymentDate(date);
	}

	@Override
	public List<TtCashflowInterest> getCfInterestListByRefixing(String date) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("fixingDate", date);
		return cfInterestMapper.getCfInterestListByFixingDate(map);
	}
	
	/**
	 * 删除旧的现金流
	 */
	@Override
	public int deleteCashFlow(String i, String a, String m) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("iCode", i);
		map.put("aType", a);
		map.put("mType", m);
		int c =cfInterestMapper.deleteCfInterst(map);
		int b =cfCapitalMapper.deleteCfCapital(map);
		if(c>0 && b>0){
			return 1;
		}
		return 0;
	}

	/**
	 * 生成新的现金流
	 */
	@Override
	public void insertCashFlow(List<TtCashflowInterest> list1, List<TtCashflowCapital> list2) {
		batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowInterestMapper.insert", list1);
		batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowCapitalMapper.insert", list2);
		
	}

	/**
	 * 生成中间业务收入现金流
	 */
	@Override
	public void insertCashFlowFee(TtCashflowFee fee) {
		cfFeeMapper.insert(fee);
		
	}

	@Override
	public Page<TtCashflowDailyInterest> pageCfDailyInterest(
			Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		return ttCashflowDailyInterestMapper.PageCfDailyInterestList(params, rowBounds);
	}
	

}
