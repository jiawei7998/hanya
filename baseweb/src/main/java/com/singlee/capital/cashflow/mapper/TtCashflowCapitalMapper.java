package com.singlee.capital.cashflow.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtCashflowCapitalMapper extends Mapper<TtCashflowCapital> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 分页查询列表
	 * @param map 
	 * @param rb - rowBounds
	 * @return Page<TtCashflowCapital>
	 * @date 2016-9-8
	 */
	Page<TtCashflowCapital> pageCfCapital(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 获取本金现金流列表
	 * @param map
	 * @return
	 */
	List<TtCashflowCapital> getCfCapitalList(Map<String, Object> map);
	
	/**
	 * 删除本金现金流
	 * @param map
	 */
	int deleteCfCapital(Map<String, Object> map);

	/**
	 * 
	 * @param date
	 * @return
	 */
	List<TtCashflowCapital> sumByTheoryPaymentDate(String date);
	/**
	 * 获取实际支付日为节假日的本金现金流
	 * @param map
	 * @return
	 */
	List<TtCashflowCapital> getNeedResetPayMentDateCaptial(Map<String, Object> map);
	
	
}