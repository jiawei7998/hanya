package com.singlee.capital.cashflow.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 每日计提利息
 * 
 * @author xuhui
 * 
 */

@Entity
@Table(name = "TT_CASHFLOW_DAILY_INTEREST")
public class TtCashflowDailyInterest implements Serializable, Comparable<TtCashflowDailyInterest> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_DAILY_INTEREST.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	private String dayCounter; //计息基准
	private String refBeginDate;// 开始日
	private String refEndDate;// 结束日
	private double principal;// 本金
	private double interest;// 应计利息
	private String iCode;
	private String aType;
	private String mType;
	private String seqNumber; //期号
	

	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	public String getDayCounter() {
		return dayCounter;
	}
	public void setDayCounter(String dayCounter) {
		this.dayCounter = dayCounter;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}
	@Override
	public int compareTo(TtCashflowDailyInterest o) {
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			return c2 ; 
	}
	
}
