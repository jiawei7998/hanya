package com.singlee.capital.cashflow.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 中间业务收入现金流
 * @author Administrator
 *
 */
@Entity
@Table(name = "TT_CASHFLOW_FEE")
public class TtCashflowFee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_FEE.NEXTVAL FROM DUAL")
	private Integer  cashflowId;		//序号
	private String theoryPaymentDate;	//理论收付日
	private String cfType;				//中收类型品种
	private String payDirection;		//收付方向
	private String iCode;				//合约号
	private String aType;				//资产类型
	private String mType;				//市场类型
	private Integer seqNumber;			//期数
	private String cfEvent;				//中收标志字符串
	private double principal;			//费率（本金）
	private double rate;				//费率（利率）
	private double amt;					//费率（计算出），固定（直接填）
	private String daycounter;			//计息基础
	private String currency;			//币种
	
	public Integer getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(Integer cashflowId) {
		this.cashflowId = cashflowId;
	}
	public String getTheoryPaymentDate() {
		return theoryPaymentDate;
	}
	public void setTheoryPaymentDate(String theoryPaymentDate) {
		this.theoryPaymentDate = theoryPaymentDate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public Integer getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(Integer seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getCfEvent() {
		return cfEvent;
	}
	public void setCfEvent(String cfEvent) {
		this.cfEvent = cfEvent;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getDaycounter() {
		return daycounter;
	}
	public void setDaycounter(String daycounter) {
		this.daycounter = daycounter;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Override
	public String toString() {
		return "TtCashflowFee [cashflowId=" + cashflowId + ", theoryPaymentDate=" + theoryPaymentDate + ", cfType="
				+ cfType + ", payDirection=" + payDirection + ", iCode=" + iCode + ", aType=" + aType + ", mType="
				+ mType + ", seqNumber=" + seqNumber + ", cfEvent=" + cfEvent + ", principal=" + principal + ", rate="
				+ rate + ", amt=" + amt + ", daycounter=" + daycounter + ", currency=" + currency + "]";
	}
	

}
