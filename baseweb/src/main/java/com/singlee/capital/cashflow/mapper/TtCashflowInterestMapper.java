package com.singlee.capital.cashflow.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtCashflowInterestMapper extends Mapper<TtCashflowInterest> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 分页查询列表
	 * @param map 
	 * @param rb - rowBounds
	 * @return Page<TtCashflowCapital>
	 * @date 2016-9-8
	 */
	Page<TtCashflowInterest> pageCfInterest(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
	List<TtCashflowInterest> getCfInterestList(Map<String, Object> map);
	
	/**
	 * 根据定盘日获取现金流列表
	 * @param map
	 * @return
	 */
	List<TtCashflowInterest> getCfInterestListByFixingDate(Map<String, Object> map);
	
	/**
	 * 删除利息现金流
	 * @param map
	 */
	int deleteCfInterst(Map<String, Object> map);

	/**
	 * 
	 * @param date
	 * @return
	 */
	List<TtCashflowInterest> sumByTheoryPaymentDate(String date);
	
	void updateByIAMFixingDate(TtCashflowInterest interest);
	
	/**
	 * 获取实际支付日为节假日的利息现金流
	 * @param date
	 * @return
	 */
	List<TtCashflowInterest> getNeedResetPayMentDateInterest(String date);

}