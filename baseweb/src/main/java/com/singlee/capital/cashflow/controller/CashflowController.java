package com.singlee.capital.cashflow.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import com.singlee.capital.cashflow.model.TtCashflowDailyInterest;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import com.singlee.capital.cashflow.service.CashflowService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 用户相关
 * @author Lyon Chen 
 */
@Controller
@RequestMapping(value = "/CashflowController")
public class CashflowController extends CommonController {

	@Autowired
	private CashflowService cashflowService;
	
	/**
	 * 根据对象查询现金流列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCfCapital")
	public RetMsg<PageInfo<TtCashflowCapital>> searchPageCfCapital(@RequestBody Map<String,Object> params) throws RException {
		Page<TtCashflowCapital> page = cashflowService.pageCfCapital(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据对象查询现金流(利息)列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCfInterest")
	public RetMsg<PageInfo<TtCashflowInterest>> searchPageCfInterest(@RequestBody Map<String,Object> params) throws RException {
		Page<TtCashflowInterest> page = cashflowService.pageCfInterest(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCfInterestList")
	public RetMsg<List<TtCashflowInterest>> searchCfInterestList(@RequestBody Map<String,Object> params) throws RException {
		List<TtCashflowInterest> list = cashflowService.getCfInterestList(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchCfCapitalList")
	public RetMsg<List<TtCashflowCapital>> searchCfCapitalList(@RequestBody Map<String,Object> params) throws RException {
		List<TtCashflowCapital> list = cashflowService.getCfCapitalList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 根据对象查询现金流列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCfDailyInterest")
	public RetMsg<PageInfo<TtCashflowDailyInterest>> searchPageCfDailyInterest(@RequestBody Map<String,Object> params) throws RException {
		Page<TtCashflowDailyInterest> page = cashflowService.pageCfDailyInterest(params);
		return RetMsgHelper.ok(page);
	}
}
