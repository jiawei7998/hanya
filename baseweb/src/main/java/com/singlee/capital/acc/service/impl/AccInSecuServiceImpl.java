package com.singlee.capital.acc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.mapper.AccInSecuMapper;
import com.singlee.capital.acc.mapper.AccMapMapper;
import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.model.TtAccMap;
import com.singlee.capital.acc.service.AccInSecuService;
import com.singlee.capital.common.util.Constants;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.UserAccInSecuMapper;
import com.singlee.capital.system.model.TtAccInSecuVisit;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 内部证券账户Service
 * @author Libo
 *
 */
@Service("accInSecuService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccInSecuServiceImpl implements AccInSecuService {

	@Autowired
	private AccInSecuMapper accInSecuMapper;	//内部证券账户dao
	
	@Autowired
	private AccMapMapper accMapMapper;		//账户映射关系dao
	
	
	@Autowired
	private UserAccInSecuMapper userAccInSecuMapper;//用户账户权限dao
	
	
	/**
	 * 新增内部证券账户
	 * @param acc			账户对象
	 * @param accOutSecuZzd	外部证券账户--中债登账号
	 * @param accOutSecuSqs 外部证券账户--上清所账号
	 */
	@Override
	public void add(TtAccInSecu acc, String accOutSecuZzd, String accOutSecuSqs) {
		 
//		/*会计账户*/
//		TtAccFisca accFisca = new TtAccFisca();
//		accFisca.setAccId(acc.getBookkeepAccid());
//		accFisca.setAccName("abc");
//		accFiscaMapper.insertTtAccFisca(accFisca);
//		
//		/*内部证券账户*/
//		acc.setBookkeepAccid(accFisca.getAccId());
		accInSecuMapper.insertTtAccInSecu(acc);
		
		/*与中债登账号关联关系*/
		TtAccMap accMapZzd = new TtAccMap();
		accMapZzd.setInAccId(acc.getAccId());
		accMapZzd.setOutAccId(StringUtil.isNullOrEmpty(accOutSecuZzd)?"221":accOutSecuZzd);
		accMapZzd.setCashOrSecu(Constants.SECURITY);
		accMapMapper.insertTtAccMap(accMapZzd);
		
		/*上清所账号账号关联关系*/
		TtAccMap accMapSqs = new TtAccMap();
		accMapSqs.setInAccId(acc.getAccId());
		accMapSqs.setOutAccId(StringUtil.isNullOrEmpty(accOutSecuSqs)?"222":accOutSecuSqs);
		accMapSqs.setCashOrSecu(Constants.SECURITY);
		accMapMapper.insertTtAccMap(accMapSqs);
		
		/*设置柜员对应账户树列表关系*/
		TtAccInSecuVisit accInSecuVisit = new TtAccInSecuVisit();
		accInSecuVisit.setUserId(acc.getOwner());
		accInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
		accInSecuVisit.setAccid(acc.getAccId());
		
		userAccInSecuMapper.insertTtAccInSecuVisit(accInSecuVisit);
		 
	}
	
	/**
	 * 修改账户
	 * @param acc
	 */
	@Override
	public void modify(TtAccInSecu acc) {
		accInSecuMapper.update(acc);
	}

	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * 
	 * map内参数：【都可为空】
	 * accId			:账户id
	 * accName			:账户名称
	 * cashAccid		:内部资金账户
	 * owner			:所有者
	 * trdKind			:交易目的
	 * trdGrpId			:交易组
	 * bookkeepAccid	:会计账户id
	 * status			:账户状态
	 * isAutoCreateGroup:是否自动创建交易组
	 * isLock			:是否锁定
	 * lockStatus		:锁定状态
	 * accfiscasubject  :财务科目分类	
	 * inst_id			:机构ID
	 * @return
	 */
	@Override
	public Page<TtAccInSecu> getAccList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TtAccInSecu> result = accInSecuMapper.search(map, rowBounds);
		return result;
	}

	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	@Override
	public Page<TtAccInSecu> getAccList(TtAccInSecu acc) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("accId",acc.getAccId());
		map.put("accName",acc.getAccName());
		map.put("cashAccid",acc.getCashAccid());
		map.put("owner",acc.getOwner());
		map.put("trdKind",acc.getTrdKind());
		map.put("trdGrpId",acc.getTrdGrpId());
		map.put("bookkeepAccid",acc.getBookkeepAccid());
		map.put("isAutoCreateGroup",acc.getIsAutoCreateGroup());
		map.put("isLock",acc.getIsLock());
		map.put("lockStatus",acc.getLockStatus());
		map.put("accfiscasubject",acc.getAccfiscasubject());
		map.put("inst_id",acc.getInstId());
		return (Page<TtAccInSecu>)getAccList(map);
	}

	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	@Override
	public TtAccInSecu getAcc(String accId) {
		return accInSecuMapper.get(accId);
	}

}
