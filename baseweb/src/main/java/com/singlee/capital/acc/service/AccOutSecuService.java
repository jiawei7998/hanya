package com.singlee.capital.acc.service;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutSecu;

import java.util.List;
import java.util.Map;


/**
 * 外部证券账户 服务层接口
 * @author Libo
 *
 */
public interface AccOutSecuService {
	/**
	 * 新增账户
	 * @param acc
	 */
	public void add(TtAccOutSecu acc);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modify(TtAccOutSecu acc);
	
	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * map内参数：
	 * accId     ：账户id 
	 * accName   ：账户名称
	 * status    ：账户状态
	 * currency  ：币种
	 * iId       ：机构ID
	 * @return
	 */
	public Page<TtAccOutSecu> getAccList(Map<String, Object> map);
	
	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	public Page<TtAccOutSecu> getAccList(TtAccOutSecu acc);
	
	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	public TtAccOutSecu getAcc(String accId);
	
	
	
	

	/**
	 * 校验 zzd sqs 2个参数账户是否在外部证券账户 除 inst_id存在了
	 * @param zzd
	 * @param sqs
	 * @return
	 */
	public String checkHostAccId(String zzd, String sqs, String inst_id);

	
	/**
	 * 获得 指定机构 inst_id 的外部证券账户列表
	 * @param inst_id
	 * @return
	 */
	public List<TtAccOutSecu> listByInstId(String inst_id);
	/**
	 * 根据内部证券账户ID获取外部证券账户列表
	 * @param inSecuAccId
	 * @return
	 */
	public List<TtAccOutSecu> getOutAccSecuListByInAccSecuId(String inSecuAccId);
}
