package com.singlee.capital.acc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.mapper.AccOutSecuMapper;
import com.singlee.capital.acc.model.TtAccOutSecu;
import com.singlee.capital.acc.service.AccOutSecuService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("accOutSecuService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccOutSecuServiceImpl implements AccOutSecuService {

	@Autowired
	private AccOutSecuMapper accOutSecuMapper;
	
	/**
	 * 新增账户
	 * @param acc
	 */
	@Override
	public void add(TtAccOutSecu acc) {
		accOutSecuMapper.insertTtAccOutSecu(acc);
	}
	
	/**
	 * 修改账户
	 * @param acc
	 */
	@Override
	public void modify(TtAccOutSecu acc) {
		accOutSecuMapper.update(acc);
	}

	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * map内参数：
	 * accId		:账户id
	 * accName		:账户名称
	 * outCashAccid :外部 资金账户
	 * accType		:账户类型
	 * status		:账户状态
	 * execMode	    :执行模式
	 * grade		:债券账户等级
	 * inst_id			:机构ID
	 * hostAccid	:托管场所 托管的账户
	 * @return
	 */
	@Override
	public Page<TtAccOutSecu> getAccList(Map<String, Object> map) {
		RowBounds rb =new RowBounds();	
		Page<TtAccOutSecu> result = accOutSecuMapper.search(map,rb);
		return result;
	}

	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	@Override
	public Page<TtAccOutSecu> getAccList(TtAccOutSecu acc) {
		Map<String,Object> map = new HashMap<String,Object>();
		map =BeanUtil.beanToMap(acc);
		return getAccList(map);
	}

	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	@Override
	public TtAccOutSecu getAcc(String accId) {
		return accOutSecuMapper.get(accId);
	}

	
	/**
	 * 校验 zzd sqs 2个参数账户是否在外部证券账户 除 inst_id存在了
	 * @param zzd_accid
	 * @param sqs_accid
	 * @param inst_id 
	 * @return
	 */
	@Override
	public String checkHostAccId(String zzd_accid, String sqs_accid, String inst_id){
		Map<String, String> param = new HashMap<String,String>();
		param.put("zzd_accid", zzd_accid);
		param.put("sqs_accid", sqs_accid);
		param.put("inst_id",inst_id);
		List<String> list = accOutSecuMapper.checkHostAccId(param);
		return list == null || list.size()==0 ? null : StringUtil.join(list.iterator(), ",");
	}
	

	/**
	 * 获得 指定机构 inst_id 的外部证券账户列表
	 * @param inst_id
	 * @return
	 */
	@Override
	public List<TtAccOutSecu> listByInstId(String instId){
		return accOutSecuMapper.listOutSecuAccByInstId(instId);
	}

	@Override
	public List<TtAccOutSecu> getOutAccSecuListByInAccSecuId(String inSecuAccId) {
		if(StringUtil.checkEmptyNull(inSecuAccId)) {
			return null;
		}
		return accOutSecuMapper.listOutSecuAccByInSecuAccId(inSecuAccId);
	}
	
}
