package com.singlee.capital.acc.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtTrdAccAdjust;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * 清算路径
 * @author 
 *
 */
public interface TtTrdAccAdjustMapper extends Mapper<TtTrdAccAdjust>{

	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccAdjust> getAccAdjustList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccAdjust> getAccAdjustListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccAdjust> getAccAdjustListMine(Map<String, Object> params, RowBounds rb);
	
}