package com.singlee.capital.acc.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 外部资金账户 定期账户
 * 
 * @author Lihb
 * 
 */
@Entity
@Table(name = "TT_ACC_OUT_CASH_FIXED")
public class TtAccOutCashFixed implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 账户代码 */
	private String accid;

	/** 账户名称 */
	private String accname;

	/** 交易市场 */
	private String market;

	/** 账号 */
	private String bankacc;

	/**
	 * 0：创建中, 1：已启用, 2：停用中 3：已停用
	 */
	private String status;

	/** 机构号 */
	private String instId;

	/** 币种 */
	private String cny;

	/** 开户行大额支付行号 */
	private String bankLargeCode;

	/** 开户行名称 */
	private String bankName;

	/** 客户编号 */
	private String customerNo;

	/** 客户名称 */
	private String customerName;

	/**
	 * 账户性质：1-自营 2-理财 3-其他
	 */
	private String accNature;

	/** 记账机构 */
	private String bookInstId;

	/** 内部账号名称 */
	private String innerAccname;

	/** 内部账号 */
	private String innerAcccode;

	/** 记账科目名称 */
	private String bookSubjectName;
	
	/**更新人**/
	private String updateUser;
	
	/**更新时间**/
	private String updateTime;
	
	/**创建人**/
	private String createUser;
	
	/**创建时间**/
	private String createTime;

	/** ------- Generate Getter And Setter -------- **/

	

	public String getAccid() {
		return accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getAccname() {
		return accname;
	}

	public void setAccname(String accname) {
		this.accname = accname;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getBankacc() {
		return bankacc;
	}

	public void setBankacc(String bankacc) {
		this.bankacc = bankacc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getCny() {
		return cny;
	}

	public void setCny(String cny) {
		this.cny = cny;
	}

	public String getBankLargeCode() {
		return bankLargeCode;
	}

	public void setBankLargeCode(String bankLargeCode) {
		this.bankLargeCode = bankLargeCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAccNature() {
		return accNature;
	}

	public void setAccNature(String accNature) {
		this.accNature = accNature;
	}

	public String getBookInstId() {
		return bookInstId;
	}

	public void setBookInstId(String bookInstId) {
		this.bookInstId = bookInstId;
	}

	public String getInnerAccname() {
		return innerAccname;
	}

	public void setInnerAccname(String innerAccname) {
		this.innerAccname = innerAccname;
	}

	public String getInnerAcccode() {
		return innerAcccode;
	}

	public void setInnerAcccode(String innerAcccode) {
		this.innerAcccode = innerAcccode;
	}

	public String getBookSubjectName() {
		return bookSubjectName;
	}

	public void setBookSubjectName(String bookSubjectName) {
		this.bookSubjectName = bookSubjectName;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtAccOutCashFixed [accid=");
		builder.append(accid);
		builder.append(", accname=");
		builder.append(accname);
		builder.append(", market=");
		builder.append(market);
		builder.append(", bankacc=");
		builder.append(bankacc);
		builder.append(", status=");
		builder.append(status);
		builder.append(", instId=");
		builder.append(instId);
		builder.append(", cny=");
		builder.append(cny);
		builder.append(", bankLargeCode=");
		builder.append(bankLargeCode);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", customerNo=");
		builder.append(customerNo);
		builder.append(", customerName=");
		builder.append(customerName);
		builder.append(", accNature=");
		builder.append(accNature);
		builder.append(", bookInstId=");
		builder.append(bookInstId);
		builder.append(", innerAccname=");
		builder.append(innerAccname);
		builder.append(", innerAcccode=");
		builder.append(innerAcccode);
		builder.append(", bookSubjectName=");
		builder.append(bookSubjectName);
		builder.append(", updateUser=");
		builder.append(updateUser);
		builder.append(", updateTime=");
		builder.append(updateTime);
		builder.append(", createUser=");
		builder.append(createUser);
		builder.append(", createTime=");
		builder.append(createTime);
		builder.append("]");
		return builder.toString();
	}

}