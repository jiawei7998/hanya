package com.singlee.capital.acc.service.impl;

import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionListener;
import org.springframework.beans.factory.annotation.Autowired;



/**
 * 
 * @author x230i
 *
 */
public class AccInstitutionListener implements InstitutionListener{
//	@Autowired
//	private AccInCashMapper accInCashMapper;
	/** 外部证券账户服务 spring注入 
	@Autowired
	private AccOutSecuService accOutSecuService;*/

	/** 内部资金账户服务 spring注入 */
	@Autowired
	private AccInCashService accInCashService;
	@Override
	public void addAfter(TtInstitution add) {
		// 增加 外部证券托管账户
//		addAccOutSecu(add);
		// 增加内部资金账户
		addAccInCash(add);		
	}

	@Override
	public void modAfter(TtInstitution mod) {
		TtAccInCash accInCash = accInCashService.getAccByInstId(mod.getInstId());
		if(accInCash==null){
			// 增加内部资金账户
			addAccInCash(mod);
		}
	}

	@Override
	public void remAfter(TtInstitution rem) {
		
	}


	/**
	 * 增加 机构 所属的 内部资金 账户
	 * 	1 ACCID 序列获取
		2 ACCNAME 统一为"内部资金账户"
		3 STATUS 默认2-已启用 000031 
		4 REMARK 备注
		5 CNY 币种 CNY-人民币 USD-美元
		6 INST_ID 机构号 当前机构 
	 * @param inst
	 */
	private void addAccInCash(TtInstitution inst) {
		TtAccInCash inCash = new TtAccInCash();
		inCash.setAccname("内部资金账户");
		inCash.setStatus(DictConstants.AccStatus.Enabled);
		inCash.setRemark("");
		inCash.setCny(DictConstants.Currency.CNY);
		inCash.setInstId(inst.getInstId());
		accInCashService.add(inCash);
	}
	
}
