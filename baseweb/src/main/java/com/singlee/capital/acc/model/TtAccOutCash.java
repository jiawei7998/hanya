package com.singlee.capital.acc.model;

import javax.persistence.*;

/**
 * 外部资金账户
 * 
 * @author Lihb
 * 
 */
@Entity
@Table(name = "TT_ACC_OUT_CASH")
public class TtAccOutCash implements java.io.Serializable {

	private static final long serialVersionUID = -7099671072066591003L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_ACC_OUT_CASH.NEXTVAL FROM DUAL")
	private String accid;	// 账户代码
	private String accname;	// 账户名称
	private String market;	// 交易市场
	private String bankacc;	// 账号
	private String password;// 账户密码
	private String status;	// 0：创建中,1：已启用,2：停用中 3：已停用
	private String acctype;	// 账户类型：0 清算账户 1 存放人行 2 存放同业 3 保证金
	private String instId;	// 机构号
	private String cny;		// 币种
	private double currRate;// 活期利率
	private String bankLargeCode;	// 开户行大额支付行号
	private String bankName;		// 开户行名称
	private String openDate;		// 开户日期
	private String dvpFlag;		// 1 是 0 否
	private String customerNo;		// 客户编号
	private String customerName;	// 客户名称
	//private String inner_accid;		// 内部账号
	private String couponType;		// 结息方式 0-按季结息 1-不结息
	private String accNature;		// 账户性质：1-自营2-理财 3-其他
	private String bookInstId;	// 记账机构
	private String innerAccname;	// 内部账号名称
	private String innerAcccode;	// 内部账号
	private String bookSubjectName;//记账科目
	/**更新人**/
	private String updateUser;
	
	/**更新时间**/
	private String updateTime;
	
	/**创建人**/
	private String createUser;
	
	/**创建时间**/
	private String createTime;
	
	@Transient
	private String cashAccType;
	@Transient
	private String instName;
	@Transient
	private String createUserName;
	@Transient
	private String updateUserName;
	@Transient
	private String authUserName;
	

	public String getAccid() {
		return accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getAccname() {
		return accname;
	}

	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}

	public String getBankacc() {
		return bankacc;
	}

	public void setBankacc(String bankacc) {
		this.bankacc = bankacc;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getAcctype() {
		return acctype;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getCny() {
		return cny;
	}
	public void setCny(String cny) {
		this.cny = cny;
	}

	public double getCurrRate() {
		return currRate;
	}

	public void setCurrRate(double currRate) {
		this.currRate = currRate;
	}

	public String getBankLargeCode() {
		return bankLargeCode;
	}

	public void setBankLargeCode(String bankLargeCode) {
		this.bankLargeCode = bankLargeCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	public String getDvpFlag() {
		return dvpFlag;
	}
	public void setDvpFlag(String dvpFlag) {
		this.dvpFlag = dvpFlag;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getAccNature() {
		return accNature;
	}
	public void setAccNature(String accNature) {
		this.accNature = accNature;
	}
	public String getBookInstId() {
		return bookInstId;
	}

	public void setBookInstId(String bookInstId) {
		this.bookInstId = bookInstId;
	}

	public String getInnerAccname() {
		return innerAccname;
	}

	public void setInnerAccname(String innerAccname) {
		this.innerAccname = innerAccname;
	}

	public String getInnerAcccode() {
		return innerAcccode;
	}

	public void setInnerAcccode(String innerAcccode) {
		this.innerAcccode = innerAcccode;
	}

	public String getBookSubjectName() {
		return bookSubjectName;
	}

	public void setBookSubjectName(String bookSubjectName) {
		this.bookSubjectName = bookSubjectName;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCashAccType() {
		return cashAccType;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public String getAuthUserName() {
		return authUserName;
	}

	public void setAuthUserName(String authUserName) {
		this.authUserName = authUserName;
	}

	public void setCashAccType(String cashAccType) {
		this.cashAccType = cashAccType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtAccOutCash [accid=");
		builder.append(accid);
		builder.append(", accname=");
		builder.append(accname);
		builder.append(", market=");
		builder.append(market);
		builder.append(", bankacc=");
		builder.append(bankacc);
		builder.append(", password=");
		builder.append(password);
		builder.append(", status=");
		builder.append(status);
		builder.append(", acctype=");
		builder.append(acctype);
		builder.append(", instId=");
		builder.append(instId);
		builder.append(", cny=");
		builder.append(cny);
		builder.append(", currRate=");
		builder.append(currRate);
		builder.append(", bankLargeCode=");
		builder.append(bankLargeCode);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", openDate=");
		builder.append(openDate);
		builder.append(", dvpFlag=");
		builder.append(dvpFlag);
		builder.append(", customerNo=");
		builder.append(customerNo);
		builder.append(", customerName=");
		builder.append(customerName);
		builder.append(", couponType=");
		builder.append(couponType);
		builder.append(", accNature=");
		builder.append(accNature);
		builder.append(", bookInstId=");
		builder.append(bookInstId);
		builder.append(", innerAccname=");
		builder.append(innerAccname);
		builder.append(", innerAcccode=");
		builder.append(innerAcccode);
		builder.append(", bookSubjectName=");
		builder.append(bookSubjectName);
		builder.append(", updateUser=");
		builder.append(updateUser);
		builder.append(", updateTime=");
		builder.append(updateTime);
		builder.append(", createUser=");
		builder.append(createUser);
		builder.append(", createTime=");
		builder.append(createTime);
		builder.append("]");
		return builder.toString();
	}

}
