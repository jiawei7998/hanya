package com.singlee.capital.acc.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutCash;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 外部资金账户DAO
 * @author Libo
 *
 */
public interface AccOutCashMapper extends Mapper<TtAccOutCash> {
	
	/** 
	 * 查找外部资金账户
	 * map参数：
	 * 		accId         :账户代码
	 * 		innerCode     :
	 */
	public int check(Map<String,Object> map);
	
	/** 修改外部资金账户 */
	public void update(TtAccOutCash acc);
	
	/** 查询外部资金账户 */ 
	public TtAccOutCash get(String accId);
	/**
	 * 新增
	 * @param acc
	 */
	public void insertTtAccOutCash(TtAccOutCash acc);
	
	public List<TtAccOutCash> selectAccListByAccInCashId(Map<String,Object> map);
	/**
	 * 查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TtAccOutCash> search(Map<String,Object> map,RowBounds rb);

	/**
	 * 外部资金账户查询
	 * @param params
	 * @return
	 */
	public Page<TtAccOutCash> searchAccOutCashPage(Map<String, Object> params, RowBounds rb);
	
	/** 查询外部资金账户 */ 
	public TtAccOutCash getByInnerCode(String inner_acccode);
	
	/**
	 * 拼接理财子账户id使用
	 * @return
	 */
	public String getMoneySpecialAccSeq();
	
	/**
	 * 删除理财子账户列表
	 * @return
	 */
	public void deleteMoneySpecialAcc(Map<String, Object> params);
}