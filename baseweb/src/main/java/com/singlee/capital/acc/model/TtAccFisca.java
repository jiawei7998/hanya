package com.singlee.capital.acc.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 财务账户表
 * @author LyonChen
 *
 */

@Entity
@Table(name = "TT_ACC_FISCA")
public class TtAccFisca implements java.io.Serializable{

	private static final long serialVersionUID = -1506498988547078570L;

	private String accId;	 	//	会计账户代码
	private String accName;	 	 //会计账户名称
	
	public TtAccFisca() {
		
	}

	public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	@Override
	public String toString() {
		return "TtAccFisca [accId=" + accId + ", accName=" + accName + "]";
	}

}
