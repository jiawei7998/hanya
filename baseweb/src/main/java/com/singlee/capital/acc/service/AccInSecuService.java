package com.singlee.capital.acc.service;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccInSecu;

import java.util.Map;


/**
 * 内部证券账户 服务层接口
 * @author Libo
 *
 */
public interface AccInSecuService {

	/**
	 * 新增账户
	 * @param acc			账户对象
	 * @param accOutSecuZzd	中债登账号
	 * @param accOutSecuSqs 上清所账号
	 */
	public void add(TtAccInSecu acc, String accOutSecuZzd, String accOutSecuSqs);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modify(TtAccInSecu acc);
	
	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * map内参数：
	 * accId     ：账户id 
	 * accName   ：账户名称
	 * status    ：账户状态
	 * currency  ：币种
	 * iId       ：机构ID
	 * @return
	 */
	public Page<TtAccInSecu> getAccList(Map<String, Object> map);
	
	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	public Page<TtAccInSecu> getAccList(TtAccInSecu acc);
	
	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	public TtAccInSecu getAcc(String accId);
	
}
