package com.singlee.capital.acc.mapper;

import com.singlee.capital.acc.model.TtAccFisca;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 财务账户表DAO
 * @author Libo
 *
 */
public interface AccFiscaMapper extends Mapper<TtAccFisca>{
	
	/** 
	 * 查找会计账户
	 * 参数：
	 * accId		:会计账户id
	 */
	public List<TtAccFisca> search(Map<String, String> param);
	
	
	/** 删除会计账户 */
	public void deleteByAccId(String accId);
	
	public void insertTtAccFisca(TtAccFisca ttAccFisca);
	
}