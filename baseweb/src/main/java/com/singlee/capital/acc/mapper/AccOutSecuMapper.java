package com.singlee.capital.acc.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutSecu;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;



/**
 * 外部证券账户DAO
 * @author Libo
 *
 */
public interface AccOutSecuMapper extends Mapper<TtAccOutSecu>{

	/** 
	 * 查找内部资金账户
	 * map参数：
	 * 		accId		:账户id
	 * 		accName		:账户名称
	 * 		outCashAccid:外部 资金账户
	 * 		accType		:账户类型
	 * 		status		:账户状态
	 * 		execMode	:执行模式
	 * 		grade		:债券账户等级
	 * 		inst_id		:机构ID
	 * 		hostAccid	:托管场所托管的账户
	 * 		begin_num 	：起始记录数【必输】
	 * 		end_num   	: 结束记录数【必输】
	 */
	public Page<TtAccOutSecu> search(Map<String,Object> map,RowBounds rb);
	
	
	/** 修改外部证券账户 */
	public void update(TtAccOutSecu acc);	 
	
	/** 查询外部证券账户 */ 
	public TtAccOutSecu get(String accId);
	
	
	/**	
	 * 检查账户
	 * map参数
	 * 		zzd 	中债登托管账户【可为空】
	 * 		sqs 	上清所托管账户,
	 * 		inst_id 机构id 	为空表示查找所有的机构。不为空则表示查找不为inst_id的机构是否有
	 * 		注意 是！= inst_id的 
	 * @return
	 */
	public List<String> checkHostAccId(Map<String,String> map);
	
	/**
	 * 新增
	 * @param acc
	 */
	public void insertTtAccOutSecu(TtAccOutSecu acc);

	/**
	 * 获得指定机构关联的所有外部证券账户
	 * @param inst_id
	 * @return
	 */
	public List<TtAccOutSecu> listOutSecuAccByInstId(String instId);
	/**
	 * 获得指定内部证券账户关联的所有外部证券账户
	 * @param inst_id
	 * @return
	 */
	public List<TtAccOutSecu> listOutSecuAccByInSecuAccId(String inSecuAccId);
	
}