package com.singlee.capital.acc.service;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.model.TtAccOutCashFixed;

import java.util.List;
import java.util.Map;


/**
 * 外部资金账户 服务层接口
 * @author Libo
 *
 */
public interface AccOutCashService {

	/**
	 * 新增账户
	 * @param acc
	 */
	public void add(TtAccOutCash acc,String authLogId);

	/**
	 * 新增账户
	 * @param acc
	 */
	public void add(TtAccOutCashFixed acc,String authLogId);

	/**
	 * 新增理财子账户
	 * @param acc
	 */
	public void addMoneySpecialAcc(TtAccOutCash acc,String authLogId);
	
	/**
	 * 新增账户
	 * @param acc
	 */
	public void addMoneySpecialAcc(TtAccOutCashFixed acc,String authLogId);
	
	/**
	 * 检查 
	 * 1 :内部账户 是否已经存在
	 * @param cashAccType 定期活期
	 * @param accid
	 * @param inner_acccode
	 */
	public void check(String cashAccType, String accid, String inner_acccode);
	
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modifyMoneySpecialAcc(TtAccOutCash acc);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modifyMoneySpecialAcc(TtAccOutCashFixed acc);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modify(TtAccOutCash acc);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modify(TtAccOutCashFixed acc);
	
	/**
	 * 根据条件查询账户
	 * @param map
	 * map内参数：
	 * accId     ：账户id 
	 * accName   ：账户名称
	 * bankName 	:开户行名称  模糊查询
	 * status    ：账户状态
	 * currency  ：币种
	 * iId       ：机构ID
	 * @return
	*/
	public Page<TtAccOutCash> getAccList(Map<String, Object> map); 

	


	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	public TtAccOutCash getAcc(String accId);
	
	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	public TtAccOutCashFixed getAccFixed(String accId);

	/**
	 * 根据innerAccCode查找指定账户,若查询不到则返回Null
	 * @param innerAccCode
	 * @return
	 */
	public TtAccOutCashFixed getAccFixedByInnerAccCode(String innerAccCode);
	
	/**
	 * 停用/启用 指定 accid 的外部资金账户 
	 * @param accid
	 */
	public void active(String accid);

	/**
	 * 停用/启用 指定 accid 的外部资金账户 
	 * @param accid
	 */
	public void activeFixed(String accid);
	
	/**
	 * 根据内部资金账户获取外部资金账户列表 
	 * @param accid
	 */
	public List<TtAccOutCash> getAccListByAccInCashId(String accInCashId);

	/**
	 * 根据银行行号 + 银行账号 获取外部资金账户列表 
	 * @param accid
	 */
	public TtAccOutCash getPartyOutCashBankAcc(String bankCode, String bankAccCode);
	
	/**
	 * 根据机构，对手，获得定期 的外部资金账户
	 * @param inst_id 机构
	 * @param partyCode 对手
	 * @return
	 */
	public List<TtAccOutCashFixed> getAccOutCashFixedByPartyCode(String inst_id, String partyCode,String bookingInstId);
	
	/**
	 * 根据机构，对手，获得活期 的外部资金账户
	 * @param inst_id 机构
	 * @param partyCode 对手
	 * @return
	*/
	public List<TtAccOutCash> getAccOutCashCurrentByPartyCode(String inst_id, String partyCode); 
	
	
	/**
	 * 根据外部资金账户判断转账方式
	 * @param accOutCashId
	 * @return
	 */
	public String getTransferTypeByExtAcc(String accOutCashId);
	/**
	 * 根据外部资金账户获取记账机构
	 * @param accOutCashId
	 * @return
	 */
	public String getBookInstIdByOutCashAccId(String accOutCashId);
	
	/**
	 * 同业定期记账机构号通过定期账号获取
	 * @param accOutCashFixedId
	 * @return
	
	public String getIBFixedBookInstIdBySettleInst(SettleInstVo settleInst); */
	

	/**
	 * 获取活期账号
	 * @param innerAccCode
	 * @return
	 */
	public TtAccOutCash getAccByInnerAccCode(String innerAccCode);
	
	/**
	 * 根据银行账号、开户行行号查找指定账户,若查询不到则返回Null
	 * @param bankAccCode
	 * @param bankCode
	 * @return List<TtAccOutCash> 账户列表
	 */
	List<TtAccOutCash> getAcc(String bankAccCode,String bankCode);

	/**
	 * 外部资金账户查询
	 * @param params
	 * @return
	 */
	public Page<TtAccOutCash> searchAccOutCashPage(Map<String, Object> params);
	
	/**
	 * 删除理财子账户列表
	 * @param params
	 * @return
	 */
	public void deleteMoneySpecialAcc(Map<String,Object> params);
}
