package com.singlee.capital.acc.mapper;

import com.singlee.capital.acc.model.TtAccOutCashFixed;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 外部资金账户DAO 定期
 * 
 * @author Libo
 * 
 */
public interface AccOutCashFixedMapper extends Mapper<TtAccOutCashFixed>{


	/** */
	public List<TtAccOutCashFixed> search(Map<String,Object> map);
	
	/** 修改外部资金账户 */
	public void update(TtAccOutCashFixed acc);	
	
	/** 查询外部资金账户 */ 
	public TtAccOutCashFixed get(String accId);
	
	/** 查询外部资金账户 */ 
	public TtAccOutCashFixed getByInnerCode(String inner_acccode);
	
	/**
	 * 新增
	 * @param acc
	 */
	public void insertTtAccOutCashFixed(TtAccOutCashFixed acc);

	/** 
	 * 查找外部资金账户
	 * map参数：
	 * 		accId         :账户代码
	 * 		innerCode     :
	 */
	public int check(Map<String,Object> map);
	
}