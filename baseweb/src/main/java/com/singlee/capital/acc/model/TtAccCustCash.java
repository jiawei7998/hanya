package com.singlee.capital.acc.model;
/**
 * 同业存放活期账户表
 * @author kevin_gm
 *
 */
public class TtAccCustCash implements java.io.Serializable {

	private static final long serialVersionUID = 4705544635347063463L;
	private String accid;//内部账户代码
	private String accname;//账户名称
	private String status;//状态
	private String cny;//币种
	private String rate;//活期利率
	private String coupon_type;//计息方式
	private String open_date;//开户日期
	private String cust_id;//客户编码
	private String cust_name;//客户名称
	private String cash_serial_no;//资金流水号
	private String bankacccode;//银行账号
	private String inst_id;//机构代码
	private String ai_calc_type;//活期利息计算方式：默认1-普通日积数
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCny() {
		return cny;
	}
	public void setCny(String cny) {
		this.cny = cny;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public String getOpen_date() {
		return open_date;
	}
	public void setOpen_date(String open_date) {
		this.open_date = open_date;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCash_serial_no() {
		return cash_serial_no;
	}
	public void setCash_serial_no(String cash_serial_no) {
		this.cash_serial_no = cash_serial_no;
	}
	public String getBankacccode() {
		return bankacccode;
	}
	public void setBankacccode(String bankacccode) {
		this.bankacccode = bankacccode;
	}
	public String getInst_id() {
		return inst_id;
	}
	public void setInst_id(String inst_id) {
		this.inst_id = inst_id;
	}
	public String getAi_calc_type() {
		return ai_calc_type;
	}
	public void setAi_calc_type(String ai_calc_type) {
		this.ai_calc_type = ai_calc_type;
	}
	
	
}
