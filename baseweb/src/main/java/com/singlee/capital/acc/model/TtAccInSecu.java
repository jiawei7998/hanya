package com.singlee.capital.acc.model;

import javax.persistence.*;

/**
 * 内部证券账户
 * @author Libo
 *
 */
@Entity
@Table(name = "TT_ACC_IN_SECU")
public class TtAccInSecu implements java.io.Serializable{
	
	private static final long serialVersionUID = -7099671072066591003L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_ACC_IN_SECU.NEXTVAL FROM DUAL")
	private String accId;		 	 //账户id
	private String accName;		 	 //账户名称
	private String cashAccid;	 	 //内部资金账户
	private String owner;		 	 //所有者
	private String isPublic;		 //是否公用账户
	private String trdKind;		 	 //交易目的
	private String trdGrpId;	 	 //交易组
	private String bookkeepAccid;	 //会计账户id
	private String status;		 	 //账户状态
	private String isAutoCreateGroup;//是否自动创建交易组
	private String isLock;			 //是否锁定
	private String lockStatus;	     //锁定状态
	private String accfiscasubject;	 //财务科目分类
	private String instId;			 //机构ID
	private String trdType;          //业务种类
	
	
	/**  内部资金账户名称  */
	@Transient
	private String accInCashName;
	
	/**  机构名称  */
	@Transient
	private String instName;

	/**  柜员名称 */
	@Transient
	private String ownerName;
	@Transient
	private String userId;
	@Transient
	private String visitId;
	@Transient
	private String visitLevel;
	@Transient
	private String packageId;
	@Transient
	private String packageType;
	@Transient
	private String packageName;
	@Transient
	private String packagePid;
	@Transient
	private String accOutSecuId;//外部证券账户
	@Transient
	private String hostAccid;//中债登托管账户
	@Transient
	private String childInSecuIds;//该节点下所有内部证券账户子节点集合
	@Transient
	private String accStatus;//账户节点的状态
	
	
	public TtAccInSecu() {
	}
	public String getAccId() {
		return accId;
	}
	public void setAccId(String accId) {
		this.accId = accId;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public String getCashAccid() {
		return cashAccid;
	}
	public void setCashAccid(String cashAccid) {
		this.cashAccid = cashAccid;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getTrdKind() {
		return trdKind;
	}
	public void setTrdKind(String trdKind) {
		this.trdKind = trdKind;
	}
	public String getTrdGrpId() {
		return trdGrpId;
	}
	public void setTrdGrpId(String trdGrpId) {
		this.trdGrpId = trdGrpId;
	}
	public String getBookkeepAccid() {
		return bookkeepAccid;
	}
	public void setBookkeepAccid(String bookkeepAccid) {
		this.bookkeepAccid = bookkeepAccid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsAutoCreateGroup() {
		return isAutoCreateGroup;
	}
	public void setIsAutoCreateGroup(String isAutoCreateGroup) {
		this.isAutoCreateGroup = isAutoCreateGroup;
	}
	public String getIsLock() {
		return isLock;
	}
	public void setIsLock(String isLock) {
		this.isLock = isLock;
	}
	public String getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getAccfiscasubject() {
		return accfiscasubject;
	}
	public void setAccfiscasubject(String accfiscasubject) {
		this.accfiscasubject = accfiscasubject;
	}
	public String getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getAccInCashName() {
		return accInCashName;
	}
	public void setAccInCashName(String accInCashName) {
		this.accInCashName = accInCashName;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getVisitLevel() {
		return visitLevel;
	}
	public void setVisitLevel(String visitLevel) {
		this.visitLevel = visitLevel;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageType() {
		return packageType;
	}
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackagePid() {
		return packagePid;
	}
	public void setPackagePid(String packagePid) {
		this.packagePid = packagePid;
	}
	public String getAccOutSecuId() {
		return accOutSecuId;
	}
	public void setAccOutSecuId(String accOutSecuId) {
		this.accOutSecuId = accOutSecuId;
	}
	public String getHostAccid() {
		return hostAccid;
	}
	public void setHostAccid(String hostAccid) {
		this.hostAccid = hostAccid;
	}
	public String getChildInSecuIds() {
		return childInSecuIds;
	}
	public void setChildInSecuIds(String childInSecuIds) {
		this.childInSecuIds = childInSecuIds;
	}
	public String getAccStatus() {
		return accStatus;
	}
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	public String getTrdType() {
		return trdType;
	}
	public void setTrdType(String trdType) {
		this.trdType = trdType;
	}
	
}
