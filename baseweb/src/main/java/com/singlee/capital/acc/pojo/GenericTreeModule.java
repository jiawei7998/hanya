package com.singlee.capital.acc.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 支持泛型树对象
 * @author gm
 * @param <T>
 *
 */
public class GenericTreeModule<T> implements Serializable {

	private static final long serialVersionUID = -1357480417417561809L;
	private String id;
	private String text;
	private T attributes;
	private List<GenericTreeModule<T>> children;
	private String pid;
	private String iconCls;
	
	public List<GenericTreeModule<T>> getChildren() {
		return children;
	}
	public void setChildren(List<GenericTreeModule<T>> children) {
		this.children = children;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public T getAttributes() {
		return attributes;
	}
	public void setAttributes(T attributes) {
		this.attributes = attributes;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	
}
