package com.singlee.capital.acc.service;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccInCash;

import java.util.Map;


/**
 * 内部资金账户 服务层接口
 * @author Libo
 *
 */
public interface AccInCashService {

	/**
	 * 新增账户
	 * @param acc
	 */
	public void add(TtAccInCash acc);
	
	/**
	 * 修改账户
	 * @param acc
	 */
	public void modify(TtAccInCash acc);
	
	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * map内参数：
	 * accId     ：账户id 
	 * accName   ：账户名称
	 * status    ：账户状态
	 * currency  ：币种
	 * inst_id   ：机构ID
	 * @return
	 */
	public Page<TtAccInCash> getAccList(Map<String, Object> map);
	
	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	public Page<TtAccInCash> getAccList(TtAccInCash acc);
	
	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	public TtAccInCash getAcc(String accId);

	
	/**
	 * 根据 inst_id 获得 内部资金账户对象
	 * @param inst_id
	 * @return
	 */
	public TtAccInCash getAccByInstId(String inst_id);
	/**
	 * 根据内部资金账户获取记账机构号
	 * @param inCashAccid
	 * @return
	 */
	public String getBookInstIdByInCashAccId(String inCashAccid);
}
