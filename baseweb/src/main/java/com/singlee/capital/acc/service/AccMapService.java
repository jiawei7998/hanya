package com.singlee.capital.acc.service;

import com.singlee.capital.acc.model.TtAccMap;

import java.util.List;


/**
 * 账户关联关系 服务层接口
 * @author Libo
 *
 */
public interface AccMapService {

	/**
	 * 新增账户关联关系
	 * @param acc
	 */
	public void add(TtAccMap accRel);

	/**
	 * 删除账户关联关系
	 * @param acc
	 * @author LyonChen
	 */
	public void remove(TtAccMap accRel);
	/**
	 * 查找账户关联关系
	 * @param inAccid     内部账户id
	 * @param OutAccid	    外部账户id 
	 * @param CashOrSecu  资金or券
	 * @return
	 */
	public List<TtAccMap> getAccRelList(String inAccid, String OutAccid, String CashOrSecu);

	public List<TtAccMap> getAccRelListByOutCashAccId(String outAccId, String CashOrSecu);
	
}
