package com.singlee.capital.acc.controller;

import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.model.TtAccInSecuPackage;
import com.singlee.capital.acc.pojo.GenericTreeModule;
import com.singlee.capital.acc.service.TtAccInSecuPackageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.TreeInterface;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

 
/**
 * 账户树
 * @author dingzy
 */
@Controller
@RequestMapping(value="/TtAccInSecuPackageController")
public class TtAccInSecuPackageController extends CommonController {
	
	/** 账户树服务接口 spring 负责注入 */
	@Autowired
	private TtAccInSecuPackageService ttAccInSecuPackageService;
	
	/**
	 * 查询账户树
	 * @param request
	 * @return
	 * @throws IOException
	 */
//	@RequestMapping(value = "/searchAccTree",method={RequestMethod.GET, RequestMethod.POST})
//	@ResponseBody
//	public List<GenericTreeModule<TtAccInSecuPackage>> searchAccInSecuTreeModuleForAll() throws IOException{
//		HashMap<String, Object> map = new HashMap<String, Object>();
//		return ttAccInSecuPackageService.searchAccInSecuTreeModuleForAll(map);
//	}
	@RequestMapping(value = "/searchAccTree",method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public List<TreeInterface> searchAccInSecuTreeModuleForAll(HashMap<String, Object> map) throws IOException{
		String instId = SlSessionHelper.getInstitutionId();
		map.put("instId", instId);
		return ttAccInSecuPackageService.searchAccTree(map);
	}
	
	@RequestMapping(value = "/searchTtAccInSecuByTrdType",method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public List<TreeInterface> searchTtAccInSecuByTrdType(@RequestBody HashMap<String, Object> map) throws IOException{
		String instId = SlSessionHelper.getInstitutionId();
		map.put("instId", instId);
		return ttAccInSecuPackageService.searchTtAccInSecuByTrdType(map);
	}
	/**
	 * 内部证券账户（两层树）
	 */
	@RequestMapping(value = "/searchAccInSecuTwoTree")
	@ResponseBody
	public List<GenericTreeModule<TtAccInSecu>> searchAccInSecuTwoTree(@RequestBody HashMap<String, Object> map) throws Exception{
		if(map.get("instId") == null || "".equals(map.get("instId"))){
			map.put("instId", SlSessionHelper.getInstitutionId());
		}
		return ttAccInSecuPackageService.searchAccInSecuTwoTree(map);
	}
	
	
	/**
	 * 新增子节点包
	 * @param taUserParam
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/addAccInSecuPackage")
	@ResponseBody
	public TtAccInSecuPackage addAccInSecuPackage(String parentId) throws IOException {
		String userId = SlSessionHelper.getUserId();
		return ttAccInSecuPackageService.insertTtAccInSecuPackage(parentId,userId);
	}
	
	/**
	 * 修改子节点包
	 * @param taUserParam
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateTtAccInSecuPackage")
	@ResponseBody
	public TtAccInSecuPackage updateTtAccInSecuPackage(String id,String text){
		String userId = SlSessionHelper.getUserId();
		return ttAccInSecuPackageService.updateTtAccInSecuPackage(id, text,userId);
	}
	
	/**
	 * 删除子节点包
	 * @param taUserParam
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/delAccInSecuPackage")
	@ResponseBody
	public RetMsg<Serializable> delAccInSecuPackage(String id){
		ttAccInSecuPackageService.delAccInSecuPackage(id);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 拖动子节点包
	 * @param request
	 * @param response
	 * @return
	 * @throws RException
	 */
	@RequestMapping(value = "/dndTtAccInSecuPackage")
	@ResponseBody
	public RetMsg<Serializable> dndTtAccInSecuPackage(String id,String targetId ,String point){
		String userId = SlSessionHelper.getUserId();
		ttAccInSecuPackageService.dndTtAccInSecuPackage(id, targetId, point, userId);
		return RetMsgHelper.ok();
	} 
	
}