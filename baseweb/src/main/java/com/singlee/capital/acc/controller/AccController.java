package com.singlee.capital.acc.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.model.TtAccOutCashFixed;
import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.AccInSecuService;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.UserAccInSecuService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Map;

/**
 * 账户管理控制器 包括内外部资金、内外部券 请求前缀 /AccController/
 * @author Libo
 * 
 */
@Controller
@RequestMapping(value = "/AccController", method = RequestMethod.POST)
public class AccController extends CommonController {

	/** 内部证券账户接口 */
	@Autowired
	private AccInSecuService accInSecuService;

	@Autowired
	private AccInCashService accInCashService;
	
	@Autowired
	private UserAccInSecuService userAccInSecuService;
	/** 外部资金账户接口 */
	@Autowired
	private AccOutCashService accOutCashService;
	
	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;

	/**
	 * 查询内部证券账户列表
	 * 
	 * map
	 * @return
	 */
	@RequestMapping(value = "/searchAccInSecuInfo")
	@ResponseBody
	public RetMsg<PageInfo<TtAccInSecu>> searchAccInSecuInfo(@RequestBody Map<String,Object> params) throws IOException {
		Page<TtAccInSecu> page = accInSecuService.getAccList(params);
		return RetMsgHelper.ok(page);
	}

	
	/**
	 * 保存、修改内部证券账户信息
	 * 
	 * map内参数：【新增时accId可不输，其余全都必输】
	 * 		  types    			操作类型   【 Edit：修改，其他：新增】
	 *        accId	     		 账户ID   【新增时为空，修改时必输】
	 *        accName  			账户名称   
	 *        cashAccid 		资金账户ID
	 *        owner				所有者
	 *        status			状态
	 *        isLock			是否锁定
	 *        lockStatus		锁定状态
	 *        accfiscasubject	财务科目分类
	 *        acc_out_secu_sqs	上清所账号
	 *        acc_out_secu_zzd	中债登账户
	 *        isPublic			是否公用
	 *        inst_id			机构号
	 *        
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/saveAccInSecuInfo")
	@ResponseBody
	public TtAccInSecu saveAccInSecuInfo(@RequestBody Map<String,Object> params) throws IOException {
		
		String accOutSecuZzd = ParameterUtil.getString(params, "accOutSecuZzd", "");
		String accOutSecuSqs = ParameterUtil.getString(params, "accOutSecuSqs", "");

		TtAccInSecu acc = new TtAccInSecu();
		ParentChildUtil.HashMapToClass(params, acc);
		if(DictConstants.YesNo.YES.equals(acc.getIsPublic())){
			acc.setOwner(SlSessionHelper.getInstitutionId());
		}
		//根据所选机构查询内部资金账户
		TtAccInCash accInCash = accInCashService.getAccByInstId(acc.getInstId());
		if(accInCash == null){
			JY.raiseRException("无法找到该机构对应的内部资金账户");
		}
		acc.setCashAccid(accInCash.getAccid());
		
		if ("edit".equals(params.get("types"))) {
			accInSecuService.modify(acc);
		} else  if("add".equals(params.get("types"))) {
			accInSecuService.add(acc, accOutSecuZzd, accOutSecuSqs);
		}else{
			JY.raiseRException("操作方式错误");
		}
		//edit by gm 当账户类型为公共账户时，增加公共账户与所有角色之间的对应关系
		if(DictConstants.YesNo.YES.equals(acc.getIsPublic())){
			userAccInSecuService.savePublicTradeAcc(acc.getAccId(), "");
		}else{
			JY.require(!StringUtil.checkEmptyNull(acc.getAccId()), "删除账户对应关系时,账户编号不能为空.");
			userAccInSecuService.deleteAccInSecuVisit(acc.getAccId());
			userAccInSecuService.savePublicTradeAcc(acc.getAccId(), acc.getOwner());//非公用账户保存 当所有者 与 账户对应关系
		}
		return acc;
	}
	

	/**
	 * 查询外部资金账户列表
	 * @param accOutCashService 
	 * 
	 * @param acc_out_cash_id
	 *            :
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchAccOutCashInfo")
	@ResponseBody
	public RetMsg<PageInfo<TtAccOutCash>> searchAccOutCashInfo(@RequestBody Map<String,Object> params) {
		Page<TtAccOutCash> page =  accOutCashService.getAccList(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 查询理财资金账户列表
	 * @param accOutCashService 
	 * 
	 * @param acc_out_cash_id
	 *            :
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchMoneySpecialAccList")
	@ResponseBody
	public RetMsg<PageInfo<TtAccOutCash>> searchMoneySpecialAccList(@RequestBody Map<String,Object> params) {
		params.put("acctype", DictConstants.AccOutCashType.Money);
		Page<TtAccOutCash> page =  accOutCashService.getAccList(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 保存、修改外部资金账户信息
	 * 	map 
	 * 			inner_acccode	内部账户代码
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/saveAccOutCashInfo")
	@ResponseBody
	public void saveAccOutCashInfo(@RequestBody Map<String,Object> params) throws IOException {
		// 资金账户类型
		String cashAccType = ParameterUtil.getString(params, "cashAccType", "");
		String type = ParameterUtil.getString(params, "types", null);
		// 资金账户类型
		if(DictConstants.cashAccType.current.equals(cashAccType)){
			TtAccOutCash ttAccOutCash = new TtAccOutCash();
			ParentChildUtil.HashMapToClass(params, ttAccOutCash);
			if(StringUtils.isNotEmpty(ttAccOutCash.getOpenDate())){
				String settlementDate = dayendDateService.getSettlementDate();
			JY.require(DateUtil.daysBetween(ttAccOutCash.getOpenDate(), settlementDate) >= 0,
					"账户开户日期%s不应该晚于业务日%s", ttAccOutCash.getOpenDate(), settlementDate);
			}
			accOutCashService.check(ttAccOutCash.getCashAccType(), ttAccOutCash.getAccid(),ttAccOutCash.getInnerAcccode());
			realSave(type,ttAccOutCash,"");
		}else{
			TtAccOutCashFixed accf = new TtAccOutCashFixed();
			ParentChildUtil.HashMapToClass(params, accf);
			accOutCashService.check(cashAccType, accf.getAccid(),accf.getInnerAcccode());
			realSave(type,accf,"");
		}
	}
	
	/**
	 * 保存、修改理财子账户信息
	 * 	map 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/saveMoneySpecialAcc")
	@ResponseBody
	public void saveMoneySpecialAcc(@RequestBody Map<String,Object> params) throws IOException {
		// 资金账户类型
		String cashAccType = ParameterUtil.getString(params, "cashAccType", "");
		String type = ParameterUtil.getString(params, "types", null);
		// 资金账户类型
		if(DictConstants.cashAccType.current.equals(cashAccType)){
			TtAccOutCash ttAccOutCash = new TtAccOutCash();
			ParentChildUtil.HashMapToClass(params, ttAccOutCash);
			if(StringUtils.isNotEmpty(ttAccOutCash.getOpenDate())){
				String settlementDate = dayendDateService.getSettlementDate();
			JY.require(DateUtil.daysBetween(ttAccOutCash.getOpenDate(), settlementDate) >= 0,
					"账户开户日期%s不应该晚于业务日%s", ttAccOutCash.getOpenDate(), settlementDate);
			}
			accOutCashService.check(ttAccOutCash.getCashAccType(), ttAccOutCash.getAccid(),ttAccOutCash.getInnerAcccode());
			realSaveMoneySpecialAcc(type,ttAccOutCash,"");
		}else{
			TtAccOutCashFixed accf = new TtAccOutCashFixed();
			ParentChildUtil.HashMapToClass(params, accf);
			accOutCashService.check(cashAccType, accf.getAccid(),accf.getInnerAcccode());
			realSaveMoneySpecialAcc(type,accf,"");
		}
	}
	
	private void realSaveMoneySpecialAcc(String type,TtAccOutCashFixed acc,String authLogId){
		if ("edit".equals(type)) {
			acc.setUpdateUser(SlSessionHelper.getUserId());
			acc.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.modify(acc);
		} else if ("add".equals(type)) {
			acc.setCreateUser(SlSessionHelper.getUserId());
			acc.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.add(acc,authLogId);
		}else{
			JY.raiseRException("操作方式错误");
		}
	}
	
	private void realSaveMoneySpecialAcc(String type, TtAccOutCash acc,String authLogId){
		if ("edit".equals(type)) {
			acc.setUpdateUser(SlSessionHelper.getUserId());
			acc.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.modify(acc);
		} else if ("add".equals(type)) {
			acc.setUpdateUser(SlSessionHelper.getUserId());
			acc.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			acc.setCreateUser(SlSessionHelper.getUserId());
			acc.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.addMoneySpecialAcc(acc,authLogId);
		}else{
			JY.raiseRException("操作方式错误");
		}
	}
	
	private void realSave(String type, TtAccOutCash acc,String authLogId){
		if ("edit".equals(type)) {
			acc.setUpdateUser(SlSessionHelper.getUserId());
			acc.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.modify(acc);
		} else if ("add".equals(type)) {
			acc.setCreateUser(SlSessionHelper.getUserId());
			acc.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.add(acc,authLogId);
		}else{
			JY.raiseRException("操作方式错误");
		}
	}
	
	private void realSave(String type,TtAccOutCashFixed acc,String authLogId){
		if ("edit".equals(type)) {
			acc.setUpdateUser(SlSessionHelper.getUserId());
			acc.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.modify(acc);
		} else if ("add".equals(type)) {
			acc.setCreateUser(SlSessionHelper.getUserId());
			acc.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accOutCashService.add(acc,authLogId);
		}else{
			JY.raiseRException("操作方式错误");
		}
	}
	
	@RequestMapping(value = "/activeAccOutCashInfo")
	@ResponseBody
	public void activeAccOutCashInfo(@RequestBody Map<String,Object> params) {
		// 资金账户类型
		String cashAccType = ParameterUtil.getString(params, "cashAccType", "");
		String accid = ParameterUtil.getString(params, "accid", "");
		if(DictConstants.cashAccType.current.equals(cashAccType)){
			accOutCashService.active(accid);
		}else{
			accOutCashService.activeFixed(accid);
		}
	}

	/**
	 * 外部资金账户查询
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/searchAccOutCashPage")
	@ResponseBody
	public RetMsg<PageInfo<TtAccOutCash>> searchAccOutCashPage(@RequestBody Map<String,Object> params) {
	//	params.put("acctype", DictConstants.AccOutCashType.Money);
		Page<TtAccOutCash> page =  accOutCashService.searchAccOutCashPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除理财子账户列表
	 */
	@RequestMapping(value = "/deleteMoneySpecialAcc")
	@ResponseBody
	public void deleteMoneySpecialAcc(@RequestBody Map<String,Object> params) {
		accOutCashService.deleteMoneySpecialAcc(params);
	}
}