package com.singlee.capital.acc.model;

import com.singlee.capital.common.util.TreeInterface;
import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * 内部证券账户树对象
 * @author dingzy
 *
 */
@Entity
@Table(name = "TT_ACC_IN_SECU_PACKAGE")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtAccInSecuPackage implements Serializable,TreeInterface {

	private static final long serialVersionUID = 1L;
	@Id
	private String package_id;//包编号（PACKAGE_TYPE=1时，存放账户编号）
	private String package_name;//包名称（PACKAGE_TYPE=1时，存放账户名称）
	private String status;//状态
	private String last_date;//最后更新时间yyyy-MM-ddhh:mi:ss
	private String last_userid;//最后更新人员编号
	private Integer package_sortno;
	private String package_pid;//父节点ID
	private String package_type;//包类型.0:包;1：账户;2:机构
	private String package_serno;//流水号
	@Transient
	private List<TreeInterface> children;
	
	public String getPackage_serno() {
		return package_serno;
	}
	public void setPackage_serno(String package_serno) {
		this.package_serno = package_serno;
	}
	public String getPackage_id() {
		return package_id;
	}
	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLast_date() {
		return last_date;
	}
	public void setLast_date(String last_date) {
		this.last_date = last_date;
	}
	public void setPackage_sortno(Integer package_sortno) {
		this.package_sortno = package_sortno;
	}
	public String getLast_userid() {
		return last_userid;
	}
	public void setLast_userid(String last_userid) {
		this.last_userid = last_userid;
	}
	
	public int getPackage_sortno() {
		return package_sortno;
	}
	public void setPackage_sortno(int package_sortno) {
		this.package_sortno = package_sortno;
	}
	public String getPackage_pid() {
		return package_pid;
	}
	public void setPackage_pid(String package_pid) {
		this.package_pid = package_pid;
	}
	public String getPackage_type() {
		return package_type;
	}
	public void setPackage_type(String package_type) {
		this.package_type = package_type;
	}
	@Override
	public String getId() {
		return getPackage_id();
	}
	@Override
	public String getParentId() {
		return getPackage_pid();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TreeInterface>) list);
	}
	@Override
	public String getText() {
		return package_name;
	}
	@Override
	public String getValue() {
		return package_id;
	}
	public List<TreeInterface> getChildren() {
		return children;
	}
	public void setChildren(List<TreeInterface> children) {
		this.children = children;
	}
	@Override
	public int getSort() {
		return package_sortno;
	}
	@Override
	public List<? extends TreeInterface> children() {
		return children;
	}
	@Override
	public String getIconCls() {
		if("0".equals(package_type)){
			return "";
		}else if("1".equals(package_type)){
			return "";
		}
		return null;
	}
}
