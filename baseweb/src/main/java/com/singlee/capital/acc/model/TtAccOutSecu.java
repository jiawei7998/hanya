package com.singlee.capital.acc.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 外部证券账户
 * @author Libo
 *
 */
@Entity
@Table(name = "TT_ACC_OUT_SECU")
public class TtAccOutSecu implements java.io.Serializable{

	private static final long serialVersionUID = -7422105775515708207L;
	@Id
	private String accId;		 //	账户id
	private String accName;		 //	账户名称
	private String accType;	 	 //	账户类型 账户类型   ZZD：中债登   SQS：上清所
	private String market; 		 //	执行市场，银行间 x_cnbd
	private String status;		 //	账户状态 字典 000031
	private String execMode;	 // 执行模式
	private String grade;		 // 债券账户等级
	private String instId;	     // 机构ID
	private String outCashAccid; //	外部 资金账户
	private String hostAccid;	 // 托管场所托管的账户
	
	
	public TtAccOutSecu() {
	}


	public String getAccId() {
		return accId;
	}


	public void setAccId(String accId) {
		this.accId = accId;
	}


	public String getAccName() {
		return accName;
	}


	public void setAccName(String accName) {
		this.accName = accName;
	}


	public String getOutCashAccid() {
		return outCashAccid;
	}


	public void setOutCashAccid(String outCashAccid) {
		this.outCashAccid = outCashAccid;
	}



	public String getAccType() {
		return accType;
	}


	public void setAccType(String accType) {
		this.accType = accType;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getExecMode() {
		return execMode;
	}


	public void setExecMode(String execMode) {
		this.execMode = execMode;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getHostAccid() {
		return hostAccid;
	}

	public void setHostAccid(String hostAccid) {
		this.hostAccid = hostAccid;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	@Override
	public String toString() {
		return "TtAccOutSecu [accId=" + accId + ", accName=" + accName + ", outCashAccid=" + outCashAccid + ", accType=" + accType + ", status=" + status + ", execMode=" + execMode + ", grade=" + grade + ", instId=" + instId + ", hostAccid=" + hostAccid + "]";
	}


	
}
