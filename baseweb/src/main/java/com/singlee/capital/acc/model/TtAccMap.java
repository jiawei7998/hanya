package com.singlee.capital.acc.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 账户关联关系表
 * @author Libo
 *
 */
@Entity
@Table(name = "TT_ACC_MAP")
public class TtAccMap implements java.io.Serializable{

	private static final long serialVersionUID = -1506498988547078570L;

	private String outAccId;	 //	 外部账户id
	private String inAccId;	 	 //	 内部账户id
	private String cashOrSecu;	 //	 类型   券账户/资金账户
	
	public TtAccMap() {
		
	}

	public String getOutAccId() {
		return outAccId;
	}

	public void setOutAccId(String outAccId) {
		this.outAccId = outAccId;
	}

	public String getInAccId() {
		return inAccId;
	}

	public void setInAccId(String inAccId) {
		this.inAccId = inAccId;
	}

	public String getCashOrSecu() {
		return cashOrSecu;
	}

	public void setCashOrSecu(String cashOrSecu) {
		this.cashOrSecu = cashOrSecu;
	}

	@Override
	public String toString() {
		return "TtAccRel [outAccId=" + outAccId + ", inAccId=" + inAccId + ", cashOrSecu=" + cashOrSecu + "]";
	}

}
