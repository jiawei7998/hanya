package com.singlee.capital.acc.model;

import javax.persistence.*;

/**
 * 内部资金账户
 * @author cz
 *
1 ACCID 序列获取
2 ACCNAME 统一为"内部资金账户"
3 STATUS 默认1-已启用
4 REMARK 备注
5 CNY 币种 CNY-人民币 USD-美元
6 INST_ID 机构号 当前机构
 */
@Entity
@Table(name = "TT_ACC_IN_CASH")
public class TtAccInCash implements java.io.Serializable{
	
	private static final long serialVersionUID = 4597542643522557108L;
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_ACC_IN_CASH.NEXTVAL FROM DUAL")
	 private String accid;// 账户代码
	 private String accname;// 账户名称
	 private String status;// 账户状态	0：停用 	1：已启用
	 private String remark;// 备注
	 private String cny;// 币种
	 private String instId;// 机构编号

	
	
	public TtAccInCash() {
		
	}
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCny() {
		return cny;
	}

	public void setCny(String cny) {
		this.cny = cny;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}

	@Override
	public String toString() {
		return "TtAccInCash [accid=" + accid + ", accname=" + accname + ", status=" + status + ", remark=" + remark + ", cny=" + cny + ", instId=" + instId + "]";
	}




}
