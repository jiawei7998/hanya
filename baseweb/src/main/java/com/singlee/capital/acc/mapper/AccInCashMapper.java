package com.singlee.capital.acc.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccInCash;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * 内部资金账户DAO
 * @author Libo
 *
 */
public interface AccInCashMapper extends Mapper<TtAccInCash>{
	
	/** 
	 * 查找内部资金账户
	 * map参数：
	 * 		accId     ：账户id 
	 * 		accName   ：账户名称
	 * 		status    ：账户状态
	 * 		currency  ：币种
	 * 		inst_id   ：机构ID
	 * 		begin_num ：起始记录数【必输】
	 * 		end_num   : 结束记录数【必输】
	 */
	public Page<TtAccInCash> search(Map<String,Object> map,RowBounds rb);
	
	/** 
	 * 查找内部资金账户
	 * map参数：
	 * 		accId     ：账户id 
	 * 		accName   ：账户名称
	 * 		status    ：账户状态
	 * 		currency  ：币种
	 * 		inst_id   ：机构ID
	 * 		begin_num ：起始记录数【必输】
	 * 		end_num   : 结束记录数【必输】
	 */
	public int count(Map<String,Object> map);
	
	/** 修改内部资金账户 */
	public void update(TtAccInCash acc);
 
	/** 查询内部资金账户 */ 
	public TtAccInCash get(String accId);
	
	/**
	 * 根据机构inst_id查找指定账户,若查询不到则返回Null
	 */
	public TtAccInCash getByInstId(String instId);
	
}