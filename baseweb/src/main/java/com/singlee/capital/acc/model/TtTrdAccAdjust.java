package com.singlee.capital.acc.model;

import javax.persistence.*;

@Entity
@Table(name = "TT_TRD_ACC_ADJUST")
public class TtTrdAccAdjust {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_TRD_ACC_ADJUST.NEXTVAL FROM DUAL")
	private String adjId;
	
	private String partyId;
	private String openBankCodeOld;
	private String openBankNameOld;
	private String bankaccidOld;
	private String bankaccnameOld;
	private String openBankCode;
	private String openBankName;
	private String bankaccid;
	private String bankaccname;
	private String isActive;
	private String createUser;
	private String createTime;
	private String updateUser;
	private String updateTime;
	private String status;
	private String memo;
	private String partyCode;
	private String partyName;
	private String operationType;
	private String currency;

	@Transient
	private String createUserName;
	@Transient
	private String updateUserName;
	@Transient
	private String taskId;
	
	public String getAdjId() {
		return adjId;
	}
	
	public void setAdjId(String adjId) {
		this.adjId = adjId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getOpenBankCodeOld() {
		return openBankCodeOld;
	}

	public void setOpenBankCodeOld(String openBankCodeOld) {
		this.openBankCodeOld = openBankCodeOld;
	}

	public String getOpenBankNameOld() {
		return openBankNameOld;
	}

	public void setOpenBankNameOld(String openBankNameOld) {
		this.openBankNameOld = openBankNameOld;
	}

	public String getBankaccidOld() {
		return bankaccidOld;
	}

	public void setBankaccidOld(String bankaccidOld) {
		this.bankaccidOld = bankaccidOld;
	}

	public String getBankaccnameOld() {
		return bankaccnameOld;
	}

	public void setBankaccnameOld(String bankaccnameOld) {
		this.bankaccnameOld = bankaccnameOld;
	}

	public String getOpenBankCode() {
		return openBankCode;
	}

	public void setOpenBankCode(String openBankCode) {
		this.openBankCode = openBankCode;
	}

	public String getOpenBankName() {
		return openBankName;
	}

	public void setOpenBankName(String openBankName) {
		this.openBankName = openBankName;
	}

	public String getBankaccid() {
		return bankaccid;
	}

	public void setBankaccid(String bankaccid) {
		this.bankaccid = bankaccid;
	}

	public String getBankaccname() {
		return bankaccname;
	}

	public void setBankaccname(String bankaccname) {
		this.bankaccname = bankaccname;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
