package com.singlee.capital.acc.service;

import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.system.session.impl.SlSessionHelper;

public class SlSessionHelperExt extends SlSessionHelper{
	
	/**
	 * @return
	 */
	private static final String KeyInCashAcc = "InCashAcc";
	
	public static String getInCashAccId(){
		TtAccInCash getInCashAcc = 	getInCashAcc();
		return getInCashAcc ==null ? "" : getInCashAcc.getAccid();
	}
	
	public static TtAccInCash getInCashAcc(){
		SlSession session = getSlSession();
		return session==null?null:(TtAccInCash)session.get(KeyInCashAcc);
	}
	
	public static void setAccInCash(SlSession session, TtAccInCash accByInstId) {
		session.put(KeyInCashAcc, accByInstId);
	}
	
}

