package com.singlee.capital.acc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.mapper.TtTrdAccAdjustMapper;
import com.singlee.capital.acc.model.TtTrdAccAdjust;
import com.singlee.capital.acc.service.AccAdjustService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Map;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccAdjustdServiceImpl implements AccAdjustService {

	@Autowired
	private TtTrdAccAdjustMapper adjMapper;
	
	@Override
	public TtTrdAccAdjust getAccAdjustById(String adjId) {
		return adjMapper.selectByPrimaryKey(adjId);
	}

	@Override
	public Page<TtTrdAccAdjust> getAccAdjustPage(Map<String, Object> params, int isFinished) {
		String approveStatus = ParameterUtil.getString(params, "status", null);
		if(approveStatus != null && !"".equals(approveStatus)){
			String [] status = approveStatus.split(",");
			params.put("status", Arrays.asList(status));
		}
		if(isFinished == 1){
			return adjMapper.getAccAdjustList(params, ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			return adjMapper.getAccAdjustListFinished(params, ParameterUtil.getRowBounds(params));
		}else {
			return adjMapper.getAccAdjustListMine(params, ParameterUtil.getRowBounds(params));
		}
	}

	@Override
	@AutoLogMethod(value = "创建客户清算路径表")
	public void createAccAdjust(Map<String, Object> params) {
		adjMapper.insert(AccAdjust(params));
	}
	
	@Override
	@AutoLogMethod(value = "修改客户清算路径表")
	public void updateAccAdjust(Map<String, Object> params) {
		adjMapper.updateByPrimaryKey(AccAdjust(params));
	}
	
	private TtTrdAccAdjust AccAdjust(Map<String, Object> params){
		// map转实体类
		TtTrdAccAdjust accAdjust = new TtTrdAccAdjust();
		try {
			BeanUtil.populate(accAdjust, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		String type = ParameterUtil.getString(params, "type", "");

		// 初始化表数据
        accAdjust.setStatus(DictConstants.ApproveStatus.New);
        accAdjust.setIsActive(DictConstants.YesNo.YES);
       
		//审批完成，为交易单添加一条数据
		if("add".equals(type)){
			accAdjust.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accAdjust.setCreateUser(SlSessionHelper.getUserId());
		}
		accAdjust.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		accAdjust.setUpdateUser(SlSessionHelper.getUserId());
		return accAdjust;
	}
	
	/**
	 * 删除
	 */
	@Override
	public void deleteAccAdjust(String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			adjMapper.deleteByPrimaryKey(ids[i]);
		}
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
