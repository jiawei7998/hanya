package com.singlee.capital.acc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.mapper.AccInCashMapper;
import com.singlee.capital.acc.mapper.AccMapMapper;
import com.singlee.capital.acc.mapper.AccOutCashFixedMapper;
import com.singlee.capital.acc.mapper.AccOutCashMapper;
import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.acc.model.TtAccMap;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.model.TtAccOutCashFixed;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.common.util.Constants;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 外部资金账户服务
 * 
 * @author LyonChen
 * 
 */
@Service("accOutCashService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccOutCashServiceImpl implements AccOutCashService {
	@Autowired
	private AccOutCashMapper accOutCashMapper;
	@Autowired
	private AccInCashMapper accInCashMapper;
	@Autowired
	private AccMapMapper accMapMapper;
	@Autowired
	private AccOutCashFixedMapper accOutCashFixedMapper;

	@Override
	public void add(TtAccOutCash acc, String authLogId) {
		accOutCashMapper.insertTtAccOutCash(acc);
		TtAccInCash accInCash = accInCashMapper.getByInstId(acc.getInstId());
		if (accInCash == null) {
			JY.raiseRException(String.format("外部资金账户(%s)的机构(%s)对应的内部资金账户不存在", acc.getAccid(), acc.getInstId()));
		}
		TtAccMap accRel = new TtAccMap();
		accRel.setCashOrSecu(Constants.CASH);
		accRel.setOutAccId(acc.getAccid());
		accRel.setInAccId(accInCash.getAccid());
		accMapMapper.insertTtAccMap(accRel);
		// 更新授权日志表
		// updateAuthLogId(authLogId,acc.getAccid());

	}

	@Override
	public void addMoneySpecialAcc(TtAccOutCash acc, String authLogId) {
		String innerAcccode = acc.getInnerAcccode();
		int n = 4;
		int m = innerAcccode.length();
		String subInnerAcccode = innerAcccode.substring(m - n, m);
		String seqid = accOutCashMapper.getMoneySpecialAccSeq();
		acc.setBankacc(subInnerAcccode + seqid);
		// acc.setBankacc(acc.getInnerAcccode());
		acc.setAccname(acc.getInnerAccname());
		accOutCashMapper.insertTtAccOutCash(acc);
		TtAccInCash accInCash = accInCashMapper.getByInstId(acc.getInstId());
		if (accInCash == null) {
			JY.raiseRException(String.format("外部资金账户(%s)的机构(%s)对应的内部资金账户不存在", acc.getAccid(), acc.getInstId()));
		}
		TtAccMap accRel = new TtAccMap();
		accRel.setCashOrSecu(Constants.CASH);
		accRel.setOutAccId(acc.getAccid());
		accRel.setInAccId(accInCash.getAccid());
		accMapMapper.insertTtAccMap(accRel);
		// 更新授权日志表
		// updateAuthLogId(authLogId,acc.getAccid());

	}

	@Override
	public void add(TtAccOutCashFixed acc, String authLogId) {
		accOutCashFixedMapper.insertTtAccOutCashFixed(acc);
		// 更新授权日志表
		// updateAuthLogId(authLogId,acc.getAccid());

	}

	@Override
	public void addMoneySpecialAcc(TtAccOutCashFixed acc, String authLogId) {
		accOutCashFixedMapper.insertTtAccOutCashFixed(acc);
		// 更新授权日志表
		// updateAuthLogId(authLogId,acc.getAccid());

	}

	@Override
	public void check(String cashAccType, String accid, String inner_acccode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyMoneySpecialAcc(TtAccOutCash acc) {
		accOutCashMapper.update(acc);

	}

	@Override
	public void modifyMoneySpecialAcc(TtAccOutCashFixed acc) {
		accOutCashFixedMapper.update(acc);

	}

	@Override
	public void modify(TtAccOutCash acc) {
		accOutCashMapper.update(acc);

	}

	@Override
	public void modify(TtAccOutCashFixed acc) {
		accOutCashFixedMapper.update(acc);

	}

	@Override
	public TtAccOutCash getAcc(String accId) {
		return accOutCashMapper.get(accId);
	}

	@Override
	public TtAccOutCashFixed getAccFixed(String accId) {
		return accOutCashFixedMapper.get(accId);
	}

	@Override
	public TtAccOutCashFixed getAccFixedByInnerAccCode(String innerAccCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void active(String accid) {
		TtAccOutCash acc = getAcc(accid);
		if (acc == null) {
			JY.raiseRException(String.format("外部资金账号(%s)没有找到", accid));
		}
		String a = acc.getStatus();
		String ret = null;
		if (DictConstants.AccStatus.Disabled.equals(a)) {
			JY.raise("选中的外部资金账户已销户，不允许操作！");
		}
		if (DictConstants.AccStatus.Disable.equals((a))) {
			// 停用改成启用
			acc.setStatus(DictConstants.AccStatus.Enabled); 
			ret = "启用";
		} else if (DictConstants.AccStatus.Enabled.equals((a))) {
			// 启用改成停用
			acc.setStatus(DictConstants.AccStatus.Disable);
			ret = "停用";
		}
		if (ret != null) {
			try {
				accOutCashMapper.update(acc);
				ret += "外部资金账户(" + acc.getAccid() + ")成功";
			} catch (Exception e) {
				ret += "外部资金账户(" + acc.getAccid() + ")到数据库发生错误：" + e.getMessage();
			}
		} else {
			ret = "外部资金账户(" + acc.getAccid() + ")原始的状态" + a + "错误";
		}

	}

	@Override
	public void activeFixed(String accid) {
		TtAccOutCashFixed acc = getAccFixed(accid);
		if (acc == null) {
			JY.raiseRException(String.format("外部资金定期账号(%s)没有找到", accid));
		}
		String a = acc.getStatus();
		String ret = null;
		if (DictConstants.AccStatus.Disabled.equals(a)) {
			JY.raise("选中的外部资金账户已销户，不允许操作！");
		}
		if (DictConstants.AccStatus.Disable.equals((a))) {
			// 停用改成启用
			acc.setStatus(DictConstants.AccStatus.Enabled); 
			ret = "启用";
		} else if (DictConstants.AccStatus.Enabled.equals((a))) {
			// 启用改成停用
			acc.setStatus(DictConstants.AccStatus.Disable);
			ret = "停用";
		}
		if (ret != null) {
			try {
				accOutCashFixedMapper.update(acc);
				ret += "外部资金定期账户(" + acc.getAccid() + ")成功";
			} catch (Exception e) {
				ret += "外部资金定期账户(" + acc.getAccid() + ")到数据库发生错误：" + e.getMessage();
			}
		} else {
			ret = "外部资金定期账户(" + acc.getAccid() + ")原始的状态" + a + "错误";
		}

	}

	@Override
	public List<TtAccOutCash> getAccListByAccInCashId(String accInCashId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("accincashid", accInCashId);
		param.put("cashmaptype", Constants.CASH);
		return accOutCashMapper.selectAccListByAccInCashId(param);
	}

	@Override
	public TtAccOutCash getPartyOutCashBankAcc(String bankCode, String bankAccCode) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pageSize", 100);
		map.put("pageNumber", 1);
		RowBounds rb = ParameterUtil.getRowBounds(map);
		map.put("bankacc", bankAccCode);
		map.put("bank_large_code", bankCode);
		map.put("cashacctype", DictConstants.cashAccType.current);
		Page<TtAccOutCash> ph = accOutCashMapper.search(map, rb);
		if(ph != null && ph.getTotal() > 0) {
			return ph.getResult().get(0);
		}
		return null;
	}

	@Override
	public List<TtAccOutCashFixed> getAccOutCashFixedByPartyCode(String inst_id, String partyCode, String bookingInstId) {
		if(StringUtil.checkEmptyNull(inst_id) || StringUtil.checkEmptyNull(partyCode)){
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("instId", inst_id);
		map.put("customerNo", partyCode);
		map.put("status", DictConstants.AccStatus.Enabled);
//		map.put("book_inst_id", bookingInstId);
		List<TtAccOutCashFixed> list = accOutCashFixedMapper.search(map);
		return  list;
	}

	@Override
	public String getTransferTypeByExtAcc(String accOutCashId) {
		TtAccOutCash accOutCash = getAcc(accOutCashId);
		if(accOutCash == null) {
			return DictConstants.TransferType.BillPayment;
		}
		if(DictConstants.AccOutCashType.Settle.equals(accOutCash.getAcctype()) || DictConstants.AccOutCashType.SettleHis.equals(accOutCash.getAcctype())) {
			return DictConstants.TransferType.HVPS;
		} else {
			return DictConstants.TransferType.BillPayment;
		}
	}

	@Override
	public String getBookInstIdByOutCashAccId(String accOutCashId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TtAccOutCash getAccByInnerAccCode(String innerAccCode) {
		return accOutCashMapper.getByInnerCode(innerAccCode);
	}

	@Override
	public Page<TtAccOutCash> getAccList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TtAccOutCash> result = accOutCashMapper.search(map, rb);
		return result;
	}

	@Override
	public List<TtAccOutCash> getAcc(String bankAccCode, String bankCode) {
		JY.require(!StringUtil.isNullOrEmpty(bankAccCode), "银行账号不能为空！");
		RowBounds rb = new RowBounds();
		Map<String, Object> accMap = new HashMap<String, Object>();
		accMap.put("bankacc", bankAccCode);
		if (!StringUtil.isNullOrEmpty(bankCode)) {
			accMap.put("bankcode", bankCode);
		}
		Page<TtAccOutCash> result = accOutCashMapper.search(accMap, rb);
		return result.getResult();
	}

	@Override
	public Page<TtAccOutCash> searchAccOutCashPage(Map<String, Object> params) {
		RowBounds rb = new RowBounds();
		Page<TtAccOutCash> ttAccOutCashPage = accOutCashMapper.searchAccOutCashPage(params, rb);
		return ttAccOutCashPage;
	}

	@Override
	public void deleteMoneySpecialAcc(Map<String, Object> params) {
		accOutCashMapper.deleteMoneySpecialAcc(params);
	}

	@Override
	public List<TtAccOutCash> getAccOutCashCurrentByPartyCode(String inst_id, String partyCode) {
		if (StringUtil.checkEmptyNull(inst_id) || StringUtil.checkEmptyNull(partyCode)) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageNum", 1);
		map.put("pageSize", 10000);
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);

		// 增加过渡户部分
		map.put("acctype", DictConstants.AccOutCashType.Settle);
		map.put("status", DictConstants.AccStatus.Enabled);
		map.put("inst_id", inst_id);
		List<TtAccOutCash> list = accOutCashMapper.search(map, rowBounds);
		map.clear();
		map.put("cashacctype", DictConstants.cashAccType.current);
		map.put("inst_id", inst_id);
		map.put("customer_no", partyCode);
		map.put("status", DictConstants.AccStatus.Enabled);
		list.addAll(accOutCashMapper.search(map, rowBounds));
		return list;
	}
}
