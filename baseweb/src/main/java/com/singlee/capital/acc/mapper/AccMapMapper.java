package com.singlee.capital.acc.mapper;

import com.singlee.capital.acc.model.TtAccMap;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 内外部账户映射关系DAO
 * @author Libo
 *
 */
public interface AccMapMapper extends Mapper<TtAccMap>{
	
	/** 
	 * 查找映射关系
	 * map参数：
	 * 		outAccId		:外部账户id
	 * 		inAccId	    	:内部账户id
	 * 		cashOrSecu		:类型   券账户/资金账户
	 */
	public List<TtAccMap> search(Map<String,Object> map);
	
	public void insertTtAccMap(TtAccMap ttAccMap);
	
}