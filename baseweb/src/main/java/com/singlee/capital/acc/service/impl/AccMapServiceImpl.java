package com.singlee.capital.acc.service.impl;

import com.singlee.capital.acc.mapper.AccMapMapper;
import com.singlee.capital.acc.model.TtAccMap;
import com.singlee.capital.acc.service.AccMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service("accRelService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccMapServiceImpl implements AccMapService {

	@Autowired
	private AccMapMapper accRelDao;

	@Override
	public void add(TtAccMap accRel) {
		accRelDao.insert(accRel);
	}


	@Override
	public void remove(TtAccMap accRel) {
		accRelDao.delete(accRel);		
	}
	@Override
	public List<TtAccMap> getAccRelList(String inAccId, String outAccId, String CashOrSecu) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("inAccId",inAccId);
		map.put("outAccId",outAccId);
		map.put("CashOrSecu",CashOrSecu);
		return accRelDao.search(map);
	}
	@Override
	public List<TtAccMap> getAccRelListByOutCashAccId(String outAccId, String CashOrSecu) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("outAccId",outAccId);
		map.put("CashOrSecu",CashOrSecu);
		return accRelDao.search(map);
	}

}
