package com.singlee.capital.acc.service.impl;

import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.common.session.LoginListener;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;

public class AccLoginListener implements LoginListener{
	
	@Autowired
	private AccInCashService accInCashService;
	@Override
	public void loginOk(SlSession session) {
		SlSessionHelperExt.setAccInCash(session,accInCashService.getAccByInstId(SlSessionHelper.getInstitutionId()));
	}	
}
