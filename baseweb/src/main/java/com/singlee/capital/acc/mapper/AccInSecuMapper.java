package com.singlee.capital.acc.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccInSecu;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;



/**
 * 内部证券账户DAO
 * @author Libo
 *
 */
public interface AccInSecuMapper extends Mapper<TtAccInSecu>{
	
	/** 
	 * 查找内部资金账户
	 */
	public Page<TtAccInSecu> search(Map<String,Object> map, RowBounds rb);
	
	
	
	/** 修改内部证券账户 */
	public void update(TtAccInSecu acc);	
	
	/** 查询内部证券账户 */ 
	public TtAccInSecu get(String accId);
	/**
	 * 新增
	 * @param acc
	 */
	public void insertTtAccInSecu(TtAccInSecu acc);
	
	/**查询所有公共账户信息 **/
	public List<TtAccInSecu> searchTtAccInSecuForPublic(Map<String,Object> map);
	
	
	public List<TtAccInSecu> searchAccInSecu(Map<String,Object> map);
	
}