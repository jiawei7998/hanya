package com.singlee.capital.acc.service;

import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.model.TtAccInSecuPackage;
import com.singlee.capital.acc.pojo.GenericTreeModule;
import com.singlee.capital.common.util.TreeInterface;

import java.util.HashMap;
import java.util.List;

public interface TtAccInSecuPackageService {
	
	/**
	 * 查询所有账户树的数据
	 * @return
	 */
	public List<GenericTreeModule<TtAccInSecuPackage>> searchAccInSecuTreeModuleForAll(HashMap<String, Object> map);
	
	public List<TreeInterface> searchAccTree(HashMap<String, Object> map);
	
	public List<TreeInterface> searchTtAccInSecuByTrdType(HashMap<String, Object> map);
	
	/**
	 * 查询所有内部证券账户树
	 */
	public List<GenericTreeModule<TtAccInSecu>> searchAccInSecuTwoTree(HashMap<String, Object> map);

	/**
	 * 添加节点包(package_id拼接)
	 * @param accInSecuPackage
	 * @return
	 */
	public TtAccInSecuPackage insertTtAccInSecuPackage(String parentId,String userId);
	
	/**
	 * 修改节点包
	 * @param accInSecuPackage
	 * @return
	 */
	public TtAccInSecuPackage updateTtAccInSecuPackage(String id,String text,String userId);
	
	/**
	 * 删除节点包
	 * @param packageSerNo
	 */
	public void delAccInSecuPackage(String id);
	
	/**
	 * 拖动修改
	 */
	public void dndTtAccInSecuPackage(String id,String targetId,String point,String userId);
	
}
