package com.singlee.capital.acc.pojo;

import com.singlee.capital.acc.model.TtAccOutCash;



/**
 * 外部资金账户 vo 对象
 * 
 * @author LyonChen
 *
 */
public class AccOutCashVo extends TtAccOutCash{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cashAccType;
	
	
	@Override
	public String getCashAccType() {
		return cashAccType;
	}
	@Override
	public void setCashAccType(String cashAccType) {
		this.cashAccType = cashAccType;
	}

	//所属机构的 名称
	private String inst_name;
	
	private String create_user_name;
	
	private String update_user_name;
	
	private String auth_user_name;
	
	//簿记机构 名称
	// private String book_inst_name;

	
	
	public AccOutCashVo() {
	}

//	public String getBook_inst_name() {
//		return book_inst_name;
//	}
//
//	public void setBook_inst_name(String book_inst_name) {
//		this.book_inst_name = book_inst_name;
//	}

	public String getInst_name() {
		return inst_name;
	}

	public void setInst_name(String inst_name) {
		this.inst_name = inst_name;
	}

	public String getCreate_user_name() {
		return create_user_name;
	}

	public void setCreate_user_name(String create_user_name) {
		this.create_user_name = create_user_name;
	}

	public String getUpdate_user_name() {
		return update_user_name;
	}

	public void setUpdate_user_name(String update_user_name) {
		this.update_user_name = update_user_name;
	}

	public String getAuth_user_name() {
		return auth_user_name;
	}

	public void setAuth_user_name(String auth_user_name) {
		this.auth_user_name = auth_user_name;
	}

}
