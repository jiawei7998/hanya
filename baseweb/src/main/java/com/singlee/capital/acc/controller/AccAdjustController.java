package com.singlee.capital.acc.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtTrdAccAdjust;
import com.singlee.capital.acc.service.AccAdjustService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 存续期控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午16:16:28	
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/AccAdjustController")
public class AccAdjustController extends CommonController {
	
	@Autowired
	private AccAdjustService accAdjustService;

	
	/**
	 * 条件查询列表(客户清算路径)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAccAdjustFinished")
	public RetMsg<PageInfo<TtTrdAccAdjust>> searchPageAccAdjustFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("createUser", SlSessionHelper.getUserId());
		Page<TtTrdAccAdjust> page = accAdjustService.getAccAdjustPage(params, 2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(客户清算路径)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAccAdjustUnfinished")
	public RetMsg<PageInfo<TtTrdAccAdjust>> searchPageAccAdjustUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TtTrdAccAdjust> page = accAdjustService.getAccAdjustPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(客户清算路径)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAccAdjustMine")
	public RetMsg<PageInfo<TtTrdAccAdjust>> searchPageAccAdjustMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TtTrdAccAdjust> page = accAdjustService.getAccAdjustPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 保存审批流程
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/saveAccAdjust")
	public RetMsg<Serializable> saveAccAdjust(@RequestBody Map<String,Object> params){
		String type = ParameterUtil.getString(params, "type", "");
		if("add".equals(type)){
			accAdjustService.createAccAdjust(params);
		}else if("edit".equals(type)){
			accAdjustService.updateAccAdjust(params);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除流程 仅限新建
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteAccAdjust")
	public RetMsg<Serializable> deleteAccAdjust(@RequestBody String[] adjIds){
		accAdjustService.deleteAccAdjust(adjIds);
		return RetMsgHelper.ok();
	}
}
