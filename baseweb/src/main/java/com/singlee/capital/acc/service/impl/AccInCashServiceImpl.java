package com.singlee.capital.acc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.mapper.AccInCashMapper;
import com.singlee.capital.acc.model.TtAccInCash;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.service.AccInCashService;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.dict.DictConstants;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("accInCashService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccInCashServiceImpl implements AccInCashService {

	@Autowired
	private AccInCashMapper accInCashMapper;
	@Autowired
	private AccOutCashService accOutCashService;
	
	/**
	 * 新增账户
	 * @param acc
	 */
	@Override
	public void add(TtAccInCash acc) {
		accInCashMapper.insert(acc);
	}
	
	/**
	 * 修改账户
	 * @param acc
	 */
	@Override
	public void modify(TtAccInCash acc) {
		accInCashMapper.update(acc);
	}

	/**
	 * 根据条件查询账户
	 * @param map
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * map内参数：
	 * accId     ：账户id 
	 * accName   ：账户名称
	 * status    ：账户状态
	 * cny  ：币种
	 * inst_id       ：机构ID
	 * @return
	 */
	@Override
	public Page<TtAccInCash> getAccList(Map<String, Object> map) {
		RowBounds rb =new RowBounds();
		Page<TtAccInCash> result = accInCashMapper.search(map,rb);
		return result;
	}

	/**
	 * 根据示例对象上的属性查找账户
	 * @param acc
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	@Override
	public Page<TtAccInCash> getAccList(TtAccInCash acc) {
		Map<String,Object> map = BeanUtil.beanToMap(acc);
		return getAccList(map);
	}

	/**
	 * 根据accId查找指定账户,若查询不到则返回Null
	 * @param accId
	 * @return
	 */
	@Override
	public TtAccInCash getAcc(String accId) {
		return accInCashMapper.get(accId);
	}
	/**
	 * 根据机构inst_id查找指定账户,若查询不到则返回Null
	 * @param inst_id
	 * @return
	 */
	@Override
	public TtAccInCash getAccByInstId(String instId) {
		return accInCashMapper.getByInstId(instId);
	}

	@Override
	public String getBookInstIdByInCashAccId(String inCashAccid) {
		//获取当前内部资金账户
		TtAccInCash accInCash = getAcc(inCashAccid);
		if(accInCash == null) {
			JY.raiseRException("内部资金账户不能为空，请确认!");
		}
		List<TtAccOutCash> outCashList = accOutCashService.getAccListByAccInCashId(inCashAccid);
		if(outCashList != null){
			for(TtAccOutCash item : outCashList){
				if(DictConstants.AccOutCashType.Settle.equals(item.getAcctype())){
					return item.getBookInstId();
				}
			}
		}
		return accInCash.getInstId();
	}
	
}
