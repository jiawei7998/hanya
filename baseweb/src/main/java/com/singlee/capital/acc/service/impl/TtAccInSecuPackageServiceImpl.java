package com.singlee.capital.acc.service.impl;


import com.singlee.capital.acc.mapper.AccInSecuMapper;
import com.singlee.capital.acc.mapper.TtAccInSecuPackageMapper;
import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.model.TtAccInSecuPackage;
import com.singlee.capital.acc.pojo.GenericTreeModule;
import com.singlee.capital.acc.service.TtAccInSecuPackageService;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.TreeInterface;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TtAccInSecuPackageServiceImpl implements TtAccInSecuPackageService {

	@Autowired
	private TtAccInSecuPackageMapper ttAccInSecuPackageMapper;
	
	@Autowired
	private AccInSecuMapper ttAccInSecuMapper;
	
	@Autowired
	private InstitutionService institutionService;
	
	/**
	 * 查询所有账户树数据（方法一）
	 */
	@Override
	public List<TreeInterface> searchAccTree(HashMap<String, Object> map){
		List<TreeInterface> treeList = new ArrayList<TreeInterface>();
		List<TtAccInSecuPackage> accList = ttAccInSecuPackageMapper.searchTtAccInSecuPackage(map);
		for(int i=0;i<accList.size();i++){
			if("PACK0".equals(accList.get(i).getPackage_id())){
				accList.remove(i);
			}
		}
		List<TtInstitution> instList = institutionService.searchCanInstitution(map);
		treeList.addAll(accList);
		treeList.addAll(instList);
		return TreeUtil.mergeChildrenList(treeList, "-1");
	}

	/**
	 * 查询所有账户树的数据（方法二）
	 */
	@Override
	public List<GenericTreeModule<TtAccInSecuPackage>> searchAccInSecuTreeModuleForAll(HashMap<String, Object> map) {
		List<TtAccInSecuPackage> list = ttAccInSecuPackageMapper.searchTtAccInSecuPackage(map);
		return createModleList(list);
	}
	
	public List<GenericTreeModule<TtAccInSecuPackage>> createModleList(List<TtAccInSecuPackage> hashList){
		//1.将数组构建为hashMap<String,List<TreeModuleBo>> 对象
		HashMap<String, List<GenericTreeModule<TtAccInSecuPackage>>> treeModuleHashMap = new HashMap<String, List<GenericTreeModule<TtAccInSecuPackage>>>();
		for (TtAccInSecuPackage ttAccInSecuPackage : hashList) {
			String pID = ttAccInSecuPackage.getPackage_pid();
			List<GenericTreeModule<TtAccInSecuPackage>> list = new ArrayList<GenericTreeModule<TtAccInSecuPackage>>();
			//判断treeModuleHashMap是否已存在pId的对象。存在时从hashmap中读取到list，再项list中添加数据
			if(treeModuleHashMap.get(pID) != null){
				list = treeModuleHashMap.get(pID);
			}
			list.add(TtAccInSecuPackageToGenericTreeModuleBo(ttAccInSecuPackage));
			treeModuleHashMap.put(pID, list);
		}
		//2.从String = 0 的数组开始迭代，读取hashMap中module_id = PId 的数据
		List<GenericTreeModule<TtAccInSecuPackage>> TaModuleRoots = treeModuleHashMap.get("0");
		if(TaModuleRoots != null){
			for (GenericTreeModule<TtAccInSecuPackage> treeModuleBo : TaModuleRoots) {
				SetTreeModuleBoChildren(treeModuleHashMap,treeModuleBo);
			}
		}
		return TaModuleRoots;
	}
	
	/**
	 * 设置TreeModuleBo对象中children属性
	 * @param taModuleHashMap
	 * @param treeModuleBo
	 */
	private void SetTreeModuleBoChildren(HashMap<String, List<GenericTreeModule<TtAccInSecuPackage>>> treeModuleHashMap,GenericTreeModule<TtAccInSecuPackage> treeModuleBo){
		String id = treeModuleBo.getId();
		if(treeModuleHashMap.get(id) != null){
			List<GenericTreeModule<TtAccInSecuPackage>> list = treeModuleHashMap.get(id);
			for (GenericTreeModule<TtAccInSecuPackage> treeModuleBo2 : list) {
				SetTreeModuleBoChildren(treeModuleHashMap,treeModuleBo2);
			}
			treeModuleBo.setChildren(list);
		}
	}
	
	/**
	 * 将TtAccInSecuPackage对象转换为TreeModule对象
	 * @param TaModules
	 * @return
	 */
	private GenericTreeModule<TtAccInSecuPackage> TtAccInSecuPackageToGenericTreeModuleBo(TtAccInSecuPackage ttAccInSecuPackage){
		GenericTreeModule<TtAccInSecuPackage> treeModuleBo = new GenericTreeModule<TtAccInSecuPackage>();
		treeModuleBo.setId(ttAccInSecuPackage.getPackage_id());
		treeModuleBo.setText(ttAccInSecuPackage.getPackage_name());
		treeModuleBo.setAttributes(ttAccInSecuPackage);
		treeModuleBo.setPid(ttAccInSecuPackage.getPackage_pid());
		//Package_type = 0表示包，1 表示账户节点 2 部门节点
//		if(!DictConstants.AccPackageType.AccInSecu.compare(ttAccInSecuPackage.getPackage_type())){
//			treeModuleBo.setIconCls("icon-department");
//		}
		return treeModuleBo;
	}

	/**
	 * 内部证券账户创建两层树
	 */
	@Override
	public List<GenericTreeModule<TtAccInSecu>> searchAccInSecuTwoTree(HashMap<String, Object> map) {
		List<GenericTreeModule<TtAccInSecu>> list = new ArrayList<GenericTreeModule<TtAccInSecu>>();
		GenericTreeModule<TtAccInSecu> module = new GenericTreeModule<TtAccInSecu>();
		//1.创建根结点
		module.setId("0");
		module.setText("账户列表");
		module.setIconCls("");
		
		//2.读取数据，创建二级节点
		List<TtAccInSecu> accInSecusList = ttAccInSecuMapper.searchAccInSecu(map);
		module.setChildren(ttAccInSecuToTreeModuleList(accInSecusList));
		
		list.add(module);
		return list;
	}
	
	
	/**
	 * 把对象转换成TreeObject
	 * @param accInSecusList
	 * @return
	 */
	private List<GenericTreeModule<TtAccInSecu>> ttAccInSecuToTreeModuleList(List<TtAccInSecu> accInSecusList){
		List<GenericTreeModule<TtAccInSecu>> list = new ArrayList<GenericTreeModule<TtAccInSecu>>();
		for (TtAccInSecu ttAccInSecu : accInSecusList) {
			GenericTreeModule<TtAccInSecu> treeModule = new GenericTreeModule<TtAccInSecu>();
			treeModule.setId(ttAccInSecu.getAccId());
			treeModule.setText(ttAccInSecu.getAccName()+" [ "+ttAccInSecu.getInstName()+"&"+ttAccInSecu.getTrdType()+" ]");
			treeModule.setIconCls("");
			list.add(treeModule);
		}
		return list;
	}

	/**
	 * 添加节点包
	 */
	@Override
	public TtAccInSecuPackage insertTtAccInSecuPackage(String parentId,String userId) {
		TtAccInSecuPackage tpackage = new TtAccInSecuPackage();
		tpackage.setPackage_name("");
		tpackage.setStatus("2");
		tpackage.setLast_date(DateTimeUtil.getLocalDateTime());
		tpackage.setLast_userid(userId);
		tpackage.setPackage_sortno(getSortNo(parentId));
		tpackage.setPackage_pid(parentId);
		tpackage.setPackage_type("0");
		ttAccInSecuPackageMapper.insertTtAccInSecuPackage(tpackage);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("package_serno", tpackage.getPackage_serno());
		TtAccInSecuPackage secuPackage = ttAccInSecuPackageMapper.searchTtAccInSecuPackageOne(map);
		return secuPackage;
	}

	/**
	 * 修改节点包
	 */
	@Override
	public TtAccInSecuPackage updateTtAccInSecuPackage(String id,String text,String userId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("package_id", id);
		TtAccInSecuPackage secuPackage = ttAccInSecuPackageMapper.searchTtAccInSecuPackageOne(map);
		secuPackage.setPackage_name(text);
		secuPackage.setLast_date(DateTimeUtil.getLocalDateTime());
		secuPackage.setLast_userid(userId);
		ttAccInSecuPackageMapper.updateTtAccInSecuPackage(secuPackage);
		return secuPackage;
	}
	/**
	 * 删除节点包
	 */
	@Override
	public void delAccInSecuPackage(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("package_id", id);
		ttAccInSecuPackageMapper.deleteNodeWithAllChildren(map);
	}
	
	/**
	 * 拖拽节点修改
	 */
	@Override
	public void dndTtAccInSecuPackage(String id, String targetId, String point,String userId) {
		//修改拖拽节点到新节点（父节点，到新父节点下的排序，旧父节点下的排序）
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("package_id", id);
		TtAccInSecuPackage secuPackage = ttAccInSecuPackageMapper.searchTtAccInSecuPackageOne(map);
		if(secuPackage ==null){ //账户列表节点拖动
			TtAccInSecu accInSecu = ttAccInSecuMapper.get(id);
			TtAccInSecuPackage tpackage = new TtAccInSecuPackage();
			tpackage.setPackage_id(id);
			tpackage.setPackage_name(accInSecu.getAccName());
			tpackage.setStatus("2");
			tpackage.setLast_date(DateTimeUtil.getLocalDateTime());
			tpackage.setLast_userid(userId);
			tpackage.setPackage_sortno(getSortNo(targetId));
			tpackage.setPackage_pid(targetId);
			tpackage.setPackage_type("1");
			ttAccInSecuPackageMapper.insertAccInSecu(tpackage);
		} else {//账户树节点拖动
			map.clear();
			map.put("package_pid", secuPackage.getPackage_pid());
			map.put("sart_no", secuPackage.getSort());
			map.put("end_no", getSortNo(secuPackage.getPackage_pid()));
			if(getSortNo(secuPackage.getPackage_pid())>0){
				ttAccInSecuPackageMapper.updatePackageSortNoReduce(map);
			}
			secuPackage.setPackage_pid(targetId);
			secuPackage.setPackage_sortno(getSortNo(targetId));
			ttAccInSecuPackageMapper.updateTtAccInSecuPackage(secuPackage);
		}
		
	}
	
	
	/**
	 * 获取某个父节点下子节点的个数
	 * @return
	 */
	public int getSortNo(String id){
		HashMap<String,Object> map = new HashMap<String, Object>();
		map.put("package_pid", id);
		List<TtAccInSecuPackage> list = ttAccInSecuPackageMapper.searchTtAccInSecuPackage(map);
		int num = list.size();
		return num;
	}
	
	/**
	 * 根据产品类型来查询数据构建树
	 */
	@Override
	public List<TreeInterface> searchTtAccInSecuByTrdType(HashMap<String, Object> map) {
		List<TreeInterface> treeList = new ArrayList<TreeInterface>();
		List<TtAccInSecuPackage> accList = ttAccInSecuPackageMapper.searchTtAccInSecuByTrdType(map);
		List<TtInstitution> instList = institutionService.searchCanInstitution(map);
		for(int i=0;i<accList.size();i++){
			if("PACK0".equals(accList.get(i).getPackage_id())){
				accList.remove(i);
			}
		}
		treeList.addAll(accList);
		treeList.addAll(instList);
		return TreeUtil.mergeChildrenList(treeList, "-1");
	}

}
