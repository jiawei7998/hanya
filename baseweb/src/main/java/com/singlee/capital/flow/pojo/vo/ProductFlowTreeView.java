package com.singlee.capital.flow.pojo.vo;

import com.singlee.capital.common.util.TreeInterface;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程定义的Vo类 
 * @author Libo
 *
 */

public class ProductFlowTreeView implements Serializable,TreeInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 306862396536179390L;
	/**
	 * 产品种类编号 序列自动生成
	 */
	private String prd_type_no;
	/**
	 * 父产品种类编号
	 */
	private String sup_prd_type_no;
	/**
	 * 产品种类名称
	 */
	private String prd_type_name;
	/**
	 * 产品种类描述
	 */
	private String prd_type_desc;
	
	private String dict_key;

	@Transient
	/** 子节点 */
	private List<ProductFlowTreeView> children = new ArrayList<ProductFlowTreeView>();
	
	@Override
	public String getId() {
		return prd_type_no;
	}

	@Override
	public String getParentId() {
		return sup_prd_type_no;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		this.setChildren((List<ProductFlowTreeView>)list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return this.getChildren();
	}

	@Override
	public String getText() {
		return prd_type_name;
	}

	@Override
	public String getValue() {
		return prd_type_no;
	}

	@Override
	public int getSort() {
		return 0;
	}

	@Override
	public String getIconCls() {
		return null;
	}

	//--------------------
	public String getPrd_type_no() {
		return prd_type_no;
	}

	public void setPrd_type_no(String prd_type_no) {
		this.prd_type_no = prd_type_no;
	}

	public String getSup_prd_type_no() {
		return sup_prd_type_no;
	}

	public void setSup_prd_type_no(String sup_prd_type_no) {
		this.sup_prd_type_no = sup_prd_type_no;
	}

	public String getPrd_type_name() {
		return prd_type_name;
	}

	public void setPrd_type_name(String prd_type_name) {
		this.prd_type_name = prd_type_name;
	}

	public String getPrd_type_desc() {
		return prd_type_desc;
	}

	public void setPrd_type_desc(String prd_type_desc) {
		this.prd_type_desc = prd_type_desc;
	}

	public String getDict_key() {
		return dict_key;
	}

	public void setDict_key(String dict_key) {
		this.dict_key = dict_key;
	}

	public List<ProductFlowTreeView> getChildren() {
		return children;
	}

	public void setChildren(List<ProductFlowTreeView> children) {
		this.children = children;
	}
	
	
}
