/**
 * 
 */
package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaDataGram;
import tk.mybatis.mapper.common.Mapper;

/**
 * esb报文对象持久层
 */
public interface  TaDataGramMapper extends Mapper<TaDataGram> {

}
