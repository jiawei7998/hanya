package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.market.mapper.CalendarDao;
import com.singlee.capital.market.model.TtCalendarDate;
import com.singlee.capital.system.service.CalendarDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CalendarDateServiceImpl implements CalendarDateService {
	@Autowired
	private CalendarDao calendarDao;

	@Override
	public Page<TtCalendarDate> pageCalendar(Map<String, String> pageData) {
		return calendarDao.pageCalendarDate(pageData,ParameterUtil.getRowBounds(pageData));
	}

}
