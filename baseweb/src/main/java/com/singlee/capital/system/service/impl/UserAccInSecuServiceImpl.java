package com.singlee.capital.system.service.impl;

import com.singlee.capital.acc.mapper.AccInSecuMapper;
import com.singlee.capital.acc.model.TtAccInSecu;
import com.singlee.capital.acc.pojo.GenericTreeModule;
import com.singlee.capital.acc.service.AccInSecuService;
import com.singlee.capital.base.pojo.AccInSecuTreeAttributesVo;
import com.singlee.capital.base.pojo.UserAccInSecuVisitVo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.UserAccInSecuMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtAccInSecuVisit;
import com.singlee.capital.system.service.UserAccInSecuService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 柜员内部证券账户树权限管理 业务层
 * @author gm
 *
 */
@Service("userAccInSecuService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserAccInSecuServiceImpl implements UserAccInSecuService {

	/**
	 * 员账户权限关系表
	 */
	@Autowired
	public UserAccInSecuMapper userAccInSecuMapper;
	
	@Autowired
	public AccInSecuMapper accInSecuMapper;
	@Autowired
	public AccInSecuService accInSecuService;
	@Autowired
	public TaUserMapper taUserMapper;
	@Autowired
	public TaSysParamMapper taSysParamMapper;
	
	/**
	 * 用于保存公共账户的对应关系
	 * @param accid
	 * @param user_id
	 */
	@Override
	public void savePublicTradeAcc(String accid,String userId){
		if(StringUtil.checkEmptyNull(accid) && !StringUtil.checkEmptyNull(userId)){
			//传入账户为空时，则将当前用户与所有公共账户的对应关系保存到表中
			List<TtAccInSecu> publicAccList = accInSecuMapper.searchTtAccInSecuForPublic(new HashMap<String, Object>());
			if(publicAccList == null){return;}
			for (TtAccInSecu ttAccInSecu : publicAccList) {
				TaUser userVo = taUserMapper.selectUser(userId);
				if(!userVo.getInstId().equals(ttAccInSecu.getInstId())){
					continue;
				}
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("userId", userId);
				map.put("accid", ttAccInSecu.getAccId());
				TtAccInSecuVisit ttAccInSecuVisit = userAccInSecuMapper.selectTtAccInSecuVisit(map);
				if(ttAccInSecuVisit == null){
					ttAccInSecuVisit = new TtAccInSecuVisit();
					ttAccInSecuVisit.setAccid(ttAccInSecu.getAccId());
					ttAccInSecuVisit.setUserId(userId);
					ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
					userAccInSecuMapper.insertTtAccInSecuVisit(ttAccInSecuVisit);
				}else{
					ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
					userAccInSecuMapper.update(ttAccInSecuVisit);
				}
			}
		}else if(StringUtil.checkEmptyNull(userId) && !StringUtil.checkEmptyNull(accid)){
			//传入用户为空时，则将当前账户与所有用户的对应关系保存到表中
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("instId", accInSecuService.getAcc(accid).getInstId());
			List<TaUser> allUserList = taUserMapper.searchUser(paramMap);
			if(allUserList == null){return;}
			for (TaUser taUser : allUserList) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("userId", taUser.getUserId());
				map.put("accid", accid);
				TtAccInSecuVisit ttAccInSecuVisit = userAccInSecuMapper.selectTtAccInSecuVisit(map);
				if(ttAccInSecuVisit == null){
					ttAccInSecuVisit = new TtAccInSecuVisit();
					ttAccInSecuVisit.setAccid(accid);
					ttAccInSecuVisit.setUserId(taUser.getUserId());
					ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
					userAccInSecuMapper.insertTtAccInSecuVisit(ttAccInSecuVisit);
				}else{
					ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
					userAccInSecuMapper.update(ttAccInSecuVisit);
				}
			}
		}else if(!StringUtil.checkEmptyNull(userId) && !StringUtil.checkEmptyNull(accid)){
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("userId", userId);
			map.put("accid", accid);
			TtAccInSecuVisit ttAccInSecuVisit = userAccInSecuMapper.selectTtAccInSecuVisit(map);
			if(ttAccInSecuVisit == null){
				ttAccInSecuVisit = new TtAccInSecuVisit();
				ttAccInSecuVisit.setAccid(accid);
				ttAccInSecuVisit.setUserId(userId);
				ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
				userAccInSecuMapper.insertTtAccInSecuVisit(ttAccInSecuVisit);
			}else{
				ttAccInSecuVisit.setVisitLevel(DictConstants.UserAccInSecuType.Trade);
				userAccInSecuMapper.update(ttAccInSecuVisit);
			}
		}else{
			JY.raise("设置公共账户对应关系时,账户编号(accid)或用户编号(user_id)不能都为空.");
		}
	}
	
	/**
	 * 读取柜员账户树的权限数据
	 * @param userId
	 * @return
	 */
	@Override
	public List<UserAccInSecuVisitVo> searchUserAccInSecuForVo(String userId, String instId, String accName_search, String visit_level_search){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("instId", instId);
		map.put("accName_search", accName_search);
		map.put("visit_level_search", visit_level_search);
		List<UserAccInSecuVisitVo> list = userAccInSecuMapper.searchUserAccInSecu(map);
		return list;
	}
	
	/**
	 * 保存柜员内部证券账户权限
	 * @param ttAccInSecuVisit
	 * visit_id = null and visit_level != 0，执行insert方法
	 * visit_id != null and visit_level != 0 ，执行update方法
	 * visit_id != null and visit_level != 0 ，执行delete方法
	 */
	@Override
	public void saveTtAccInSecuVisit(List<TtAccInSecuVisit> ttAccInSecuVisitList){
		if(ttAccInSecuVisitList == null){return;}
		for (TtAccInSecuVisit ttAccInSecuVisit : ttAccInSecuVisitList) {
			if(StringUtil.checkEmptyNull(ttAccInSecuVisit.getVisitId()) && !DictConstants.UserAccInSecuType.Non.equals(ttAccInSecuVisit.getVisitLevel())){
				//visit_id = null and visit_level != 0，执行insert方法
				userAccInSecuMapper.insertTtAccInSecuVisit(ttAccInSecuVisit);
			}else if(!StringUtil.checkEmptyNull(ttAccInSecuVisit.getVisitId()) && !DictConstants.UserAccInSecuType.Non.equals(ttAccInSecuVisit.getVisitLevel())){
				//visit_id != null and visit_level != 0 ，执行update方法
				userAccInSecuMapper.update(ttAccInSecuVisit);
			}else if(!StringUtil.checkEmptyNull(ttAccInSecuVisit.getVisitId()) && DictConstants.UserAccInSecuType.Non.equals(ttAccInSecuVisit.getVisitLevel())){
				//visit_id != null and visit_level != 0 ，执行delete方法
				userAccInSecuMapper.deleteByVisitId(ttAccInSecuVisit.getVisitId());
			}
		}
	}
	
	/**
	 * 根据用户id及权限级别读取到有权限的内部证券账户树
	 * @return
	*/
	@Override
	public List<GenericTreeModule<AccInSecuTreeAttributesVo>> searchAccInSecuTreeModuleBuyUserIdAndLevel(Map<String, Object> map){
		// 根据folder转业务类型
		if(map != null){
			String folder = ParameterUtil.getString(map, "folder", "");
			if(StringUtils.isNotEmpty(folder)){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("p_code","000002");
				param.put("p_prop1",folder);
				List<TaSysParam> sysParamList = taSysParamMapper.selectTaSysParam(param);
				if(sysParamList != null && sysParamList.size() > 0){
					String[] trdTypeArr = new String[sysParamList.size()];
					int j = 0;
					for(int i = 0; i < sysParamList.size(); i++){
						String pValue = sysParamList.get(i).getP_value();
						if(StringUtils.isNotEmpty(pValue)){
							trdTypeArr[i] = pValue;
							j++;
						}
					}
					if(j > 0){
						map.put("trdTypeArr", trdTypeArr);
					}
				}
			}
		}
		//增加逻辑 当level = 0 时需要读取所有账户树
		List<AccInSecuTreeAttributesVo> list = userAccInSecuMapper.searchAccInSecuVisitByUserIdAndLevel(map);
		return createModleList(list);
	}
	 
	
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * @param hashList
	 * @return
	 */
	private List<GenericTreeModule<AccInSecuTreeAttributesVo>> createModleList(List<AccInSecuTreeAttributesVo> hashList){
		//1.将数组构建为hashMap<String,List<TreeModuleBo>> 对象
		HashMap<String, List<GenericTreeModule<AccInSecuTreeAttributesVo>>> treeModuleHashMap = new HashMap<String, List<GenericTreeModule<AccInSecuTreeAttributesVo>>>();
		for (AccInSecuTreeAttributesVo accInSecuTreeAttributesVo : hashList) {
			String pID = accInSecuTreeAttributesVo.getPackage_pid();
			List<GenericTreeModule<AccInSecuTreeAttributesVo>> list = new ArrayList<GenericTreeModule<AccInSecuTreeAttributesVo>>();
			//判断treeModuleHashMap是否已存在pId的对象。存在时从hashmap中读取到list，再项list中添加数据
			if(treeModuleHashMap.get(pID) != null){
				list = treeModuleHashMap.get(pID);
			}
			list.add(AccInSecuTreeAttributesVoToGenericTreeModuleBo(accInSecuTreeAttributesVo));
			treeModuleHashMap.put(pID, list);
		}
		//2.从String = 0 的数组开始迭代，读取hashMap中module_id = PId 的数据
		List<GenericTreeModule<AccInSecuTreeAttributesVo>> TaModuleRoots = treeModuleHashMap.get("0");
		if(TaModuleRoots != null){
			for (GenericTreeModule<AccInSecuTreeAttributesVo> treeModuleBo : TaModuleRoots) {
				SetTreeModuleBoChildren(treeModuleHashMap,treeModuleBo);
			}
		}
		return TaModuleRoots;
	}
	
	
	/**
	 * 设置TreeModuleBo对象中children属性
	 * @param taModuleHashMap
	 * @param treeModuleBo
	 */
	private void SetTreeModuleBoChildren(HashMap<String, List<GenericTreeModule<AccInSecuTreeAttributesVo>>> treeModuleHashMap,GenericTreeModule<AccInSecuTreeAttributesVo> treeModuleBo){
		String id = treeModuleBo.getId();
		if(treeModuleHashMap.get(id) != null){
			List<GenericTreeModule<AccInSecuTreeAttributesVo>> list = treeModuleHashMap.get(id);
			for (GenericTreeModule<AccInSecuTreeAttributesVo> treeModuleBo2 : list) {
				SetTreeModuleBoChildren(treeModuleHashMap,treeModuleBo2);
			}
			treeModuleBo.setChildren(list);
		}
	}
	/**
	 * 将TtAccInSecuPackage对象转换为TreeModule对象
	 * @param TaModules
	 * @return
	 */
	private GenericTreeModule<AccInSecuTreeAttributesVo> AccInSecuTreeAttributesVoToGenericTreeModuleBo(AccInSecuTreeAttributesVo accInSecuTreeAttributesVo){
		GenericTreeModule<AccInSecuTreeAttributesVo> treeModuleBo = new GenericTreeModule<AccInSecuTreeAttributesVo>();
		treeModuleBo.setId(accInSecuTreeAttributesVo.getPackage_id());
		treeModuleBo.setText(accInSecuTreeAttributesVo.getPackage_name());
		treeModuleBo.setAttributes(accInSecuTreeAttributesVo);
		treeModuleBo.setPid(accInSecuTreeAttributesVo.getPackage_pid());
		//Package_type = 0表示包，1 表示账户节点 2 部门节点
		if(!DictConstants.AccPackageType.AccInSecu.equals(accInSecuTreeAttributesVo.getPackage_type())){
			treeModuleBo.setIconCls("");
		} 
		if(DictConstants.AccPackageType.AccInSecu.equals(accInSecuTreeAttributesVo.getPackage_type()) && DictConstants.AccStatus.Enabled.equals(accInSecuTreeAttributesVo.getAcc_status())){
			treeModuleBo.setIconCls("");
		}
		return treeModuleBo;
	}
	
	/**
	 * 根据账户id删除该账户的所有对应关系
	 * @param accid
	 */
	@Override
	public void deleteAccInSecuVisit(String accid){
		userAccInSecuMapper.deleteAccInSecuVisit(accid);
	}

}
