package com.singlee.capital.system.pojo;

import com.singlee.capital.acc.model.TtAccInSecu;

import java.io.Serializable;


public class TaAccInSecuVO extends TtAccInSecu implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**  内部资金账户名称  */
	private String accInCashName;
	
	/**  机构名称  */
	private String instName;

	/**  柜员名称 */
	private String ownerName;
	@Override
	public String getAccInCashName() {
		return accInCashName;
	}
	@Override
	public void setAccInCashName(String accInCashName) {
		this.accInCashName = accInCashName;
	}
	@Override
	public String getInstName() {
		return instName;
	}
	@Override
	public void setInstName(String instName) {
		this.instName = instName;
	}
	@Override
	public String getOwnerName() {
		return ownerName;
	}
	@Override
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
}
