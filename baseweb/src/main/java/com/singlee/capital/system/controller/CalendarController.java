package com.singlee.capital.system.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.market.model.TtCalendarDate;
import com.singlee.capital.system.service.CalendarDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
/**
 * 角色控制器层
 * @author lihuabing
 *
 */
@Controller
@RequestMapping(value = "/CalendarController")
public class CalendarController  extends CommonController {
	@Autowired
	private CalendarDateService calendarDateService;
	/**
	 * 分页查询角色列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pageCalendarDate")
	public RetMsg<PageInfo<TtCalendarDate>> pageCalendarDate(@RequestBody Map<String, String> map){
		Page<TtCalendarDate> list = calendarDateService.pageCalendar(map);
		return RetMsgHelper.ok(list);
	}
	

}
