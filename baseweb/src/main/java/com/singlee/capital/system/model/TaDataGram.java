package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 接收报文对象
 * @author xuhui
 *
 */
@Entity
@Table(name = "TA_DATA_GRAM")
public class TaDataGram implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private String createDate;      //时间
    private String gramContent;   //报文内容
    private String transCode; //交易码
    private String systemName;  //系统名称
    private String direction;   //方向  0--发出去    1--接收进来
    
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getGramContent() {
		return gramContent;
	}
	public void setGramContent(String gramContent) {
		this.gramContent = gramContent;
	}
	public String getTransCode() {
		return transCode;
	}
	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
}
