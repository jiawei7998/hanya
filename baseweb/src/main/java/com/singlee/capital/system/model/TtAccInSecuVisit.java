package com.singlee.capital.system.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 柜员对应账户树列表关系
 * @author gm
 *
 */
@Entity
@Table(name = "TT_ACC_IN_SECU_VISIT")
public class TtAccInSecuVisit implements Serializable {

	private static final long serialVersionUID = -5509880840027084204L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_ACC_IN_SECU_VISIT.NEXTVAL FROM DUAL")
	private String visitId;//访问序列号
	private String userId;//柜员ID
	private String visitLevel;//访问权限级别 0-无权限 1-可浏览 2-可交易
	private String accid;//内部证券账户
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getVisitLevel() {
		return visitLevel;
	}
	public void setVisitLevel(String visitLevel) {
		this.visitLevel = visitLevel;
	}
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	

	
}
