package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.market.model.TtCalendarDate;

import java.util.Map;

/**
 * 日历服务-接口
 * @author lihuabing
 *
 */
public interface CalendarDateService {


	
	/**
	 * 分页查询
	 * @param pageData  查询条件
	 * @return
	 */
	Page<TtCalendarDate> pageCalendar(Map<String, String> pageData);
	
	
}
