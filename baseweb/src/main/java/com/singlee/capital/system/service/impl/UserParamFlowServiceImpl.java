package com.singlee.capital.system.service.impl;


import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaUserParamMapper;
import com.singlee.capital.system.model.TaUserParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户参数double kill
 * @author thinkpad x220
 *
 */
@Service(value="userParamFlowService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserParamFlowServiceImpl  {

	@Autowired
	private TaUserParamMapper userParamMapper;

	public void create(String refId, String type, TaUserParam t, String user,
			String inst) {
		refIdCheck(refId, type, t);
		if(StringUtils.isEmpty(refId)){
			refId = t.getP_id();
		}
		refId = null;
	}
	
	public void createAndOperate(String refId, String type, TaUserParam t,
			String user, String inst) {
		refIdCheck(refId, type, t);
		if(StringUtils.isEmpty(refId)){
			refId = t.getP_id();
		}
	}

	private void refIdCheck(String refId, String type, TaUserParam t){
		if("add".equals(type)){
		}else if("edit".equals(type)){
			JY.require(StringUtil.isNotEmpty(refId), "业务ID不能为空");
			JY.require(refId.equals(t.getP_id()), "传入的业务ID与实际ID不匹配");
		}
	}
}
