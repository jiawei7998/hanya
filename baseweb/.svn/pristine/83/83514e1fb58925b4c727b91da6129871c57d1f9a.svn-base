<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.invest.mapper.TtTrdInvestFollowupMapper">

	<!-- 投后报告映射规则 -->
	<resultMap id="TtTrdInvestFollowupMap" type="com.singlee.capital.invest.model.TtTrdInvestFollowup">
		<id property="dealNo" column="DEAL_NO" />
		<result property="refNo" column="REF_NO" />
		<result property="dealType" column="DEAL_TYPE" />
		<result property="sponsor" column="SPONSOR" />
		<result property="sponInst" column="SPONINST" />
		<result property="remark" column="REMARK" />
		<result property="lastUpdate" column="LAST_UPDATE" />
		<result property="investStatus" column="INVEST_STATUS" />
		<result property="task_id" column="TASK_ID" />
		<result property="explain" column="EXPLAIN" />
		<result property="prdName" column="CON_TITLE" />
		<association property="counterParty" javaType="com.singlee.capital.counterparty.model.TtCounterParty">
			<id property="party_id" column="PARTY_ID" />
			<result property="party_name" column="PARTY_NAME" />
		</association>
		<association property="user" javaType="com.singlee.capital.system.model.TaUser">
			<id property="userId" column="SPONSOR" />
			<result property="userName" column="USER_NAME" />
		</association>
		<association property="institution" javaType="com.singlee.capital.system.model.TtInstitution">
			<id property="instId" column="SPON_INST" />
			<result property="instName" column="INST_NAME" />
		</association>
	</resultMap>
	
	<!-- 查询我发起的投后报告列表 -->
	<select id="selectTtTrdInvestFollowup" parameterType="java.util.Map" resultMap="TtTrdInvestFollowupMap">
		Select t.Deal_No,
		       t.Ref_No,
		       t.Deal_Type,
		       Tpam.Con_Title,
		       ttc.party_id,
		       Ttc.Party_Name,
		       t.Invest_Status,
		       t.Last_Update,
		       t.Remark,
		       t.Sponsor,
		       Ta.User_Name,
		       t.Spon_Inst,
		       Ti.Inst_Name,
		       t.explain
		  From Tt_Trd_Invest_Followup t
		  Left Join td_product_approve_main tpam
		    On tpam.deal_no = t.Ref_No
		  Left Join Tt_Trd_Counterparty Ttc
		    On Ttc.Party_Id = tpam.c_no
		  Left Join Ta_User Ta
		    On Ta.User_Id = t.sponsor
		  Left Join Tt_Institution Ti
		    On Ti.Inst_Id = t.spon_inst
		  <where>
		  		and Tpam.Is_Active = '1'
		  	<if test="dealNo !=null and dealNo !=''">
		  		and t.deal_no = #{dealNo}
		  	</if>
		  	<if test="refNo !=null and refNo !=''">
		  		and t.ref_no = #{refNo}
		  	</if>
		  	<if test="dealType !='' and dealType !=null">
		  		and t.deal_type = #{dealType}
		  	</if>
		  	<if test="sponsor !='' and sponsor !=null">
		  		and t.sponsor = #{sponsor}
		  	</if>
		  	<if test="explain !=null and explain !=''">
		  		and t.explain like '%'|| #{explain} ||'%'
		  	</if>
		  	<if test="party_name !=null and party_name !=''">
		  		and Ttc.Party_Name like '%'|| #{party_name} ||'%'
		  	</if>
		  	<if test="investStatus !='' and investStatus !=null">
		  		and t.invest_status = #{investStatus}
		  	</if>
		  </where>
		  Order By t.Last_Update Desc
	</select>
	
	<!-- 查询待审批投后报告列表 -->
	<select id="selectTtTrdInvestFollowupApprove" parameterType="java.util.Map" resultMap="TtTrdInvestFollowupMap">
		Select t.Deal_No,
		       t.Ref_No,
		       t.Deal_Type,
		       Tpam.Con_Title,
		       ttc.party_id,
		       Ttc.Party_Name,
		       t.Invest_Status,
		       t.Last_Update,
		       t.Remark,
		       t.Sponsor,
		       Ta.User_Name,
		       t.Spon_Inst,
		       Ti.Inst_Name,
		       Task.Task_Id,
		       t.explain
		  From tt_trd_invest_followup t
		  Left Join (Select Regexp_Substr(Jt.Name_, '[^.]+', 1, 4) Serial_No,
		                    Regexp_Substr(Jt.Name_, '[^.]+', 1, 5) Flow_Type,
		                    Jt.Dbid_ As Task_Id,
		                    Jt.Name_ As Task_Name,
		                    Jt.Assignee_ As User_Id,
		                    Jt.Supertask_ Super_Task,
		                    Jt.Execution_ Execution,
		                    Jt.Execution_Id_ Execution_Name
		               From Jbpm4_Task Jt
		              Where Jt.Supertask_ Is Not Null) Task
		    On To_Char(t.deal_no) = Task.Serial_No
		    Left Join td_product_approve_main tpam
		    On tpam.deal_no = t.Ref_No
		  Left Join Tt_Trd_Counterparty Ttc
		    On Ttc.Party_Id = tpam.c_no
		  Left Join Ta_User Ta
		    On Ta.User_Id = t.sponsor
		  Left Join Tt_Institution Ti
		    On Ti.Inst_Id = t.spon_inst
		  <where>
		  		and Tpam.Is_Active = '1'
		  	<if test="dealNo !=null and dealNo !=''">
		  		and t.deal_no = #{dealNo}
		  	</if>
		  	<if test="refNo !=null and refNo !=''">
		  		and t.ref_no = #{refNo}
		  	</if>
		  	<if test="dealType !='' and dealType !=null">
		  		and t.deal_type = #{dealType}
		  	</if>
		  	<if test="sponsor !='' and sponsor !=null">
		  		and task.user_id = #{sponsor}
		  	</if>
		  	<if test="party_name !=null and party_name !=''">
		  		and Ttc.Party_Name like '%'|| #{party_name} ||'%'
		  	</if>
		  	<if test="investStatus !='' and investStatus !=null">
		  		and t.invest_status = #{investStatus}
		  	</if>
		  	<if test="explain !=null and explain !=''">
		  		and t.explain like '%'|| #{explain} ||'%'
		  	</if>
		  </where>
		  Order By t.Last_Update Desc
	</select>
	
	<!-- 查询已审批投后报告列表 -->
	<select id="selectTtTrdInvestFollowupFinished" parameterType="java.util.Map" resultMap="TtTrdInvestFollowupMap">
		Select t.Deal_No,
		       t.Ref_No,
		       t.Deal_Type,
		       Tpam.Con_Title,
		       ttc.party_id,
		       Ttc.Party_Name,
		       t.Invest_Status,
		       t.Last_Update,
		       t.Remark,
		       t.Sponsor,
		       Ta.User_Name,
		       t.Spon_Inst,
		       Ti.Inst_Name,
		       Task.Task_Id,
		       t.explain
		  From Tt_Trd_Invest_Followup t
		  Left Join (Select Distinct Substr(Tfl.Instance_Id,
		                                    Instr(Tfl.Instance_Id, '.') + 1,
		                                    Length(Tfl.Instance_Id)) As Serial_No,
		                             Tfl.Flow_Type,
		                             '' As Task_Id,
		                             Tfl.User_Id
		               From Tt_Flow_Log Tfl
		              Where Tfl.Flow_Type = '9'
		                And Trim(User_Id) Is Not Null) Task
		    On To_Char(t.Deal_No) = Task.Serial_No
		  Left Join Td_Product_Approve_Main Tpam
		    On Tpam.Deal_No = t.Ref_No
		  Left Join Tt_Trd_Counterparty Ttc
		    On Ttc.Party_Id = Tpam.c_No
		  Left Join Ta_User Ta
		    On Ta.User_Id = t.Sponsor
		  Left Join Tt_Institution Ti
		    On Ti.Inst_Id = t.Spon_Inst
		  <where>
		  		and Tpam.Is_Active = '1'
		  	<if test="dealNo !=null and dealNo !=''">
		  		and t.deal_no = #{dealNo}
		  	</if>
		  	<if test="refNo !=null and refNo !=''">
		  		and t.ref_no = #{refNo}
		  	</if>
		  	<if test="dealType !='' and dealType !=null">
		  		and t.deal_type = #{dealType}
		  	</if>
		  	<if test="sponsor !='' and sponsor !=null">
		  		and task.user_id = #{sponsor}
		  	</if>
		  	<if test="party_name !=null and party_name !=''">
		  		and Ttc.Party_Name like '%'|| #{party_name} ||'%'
		  	</if>
		  	<if test="investStatus !='' and investStatus !=null">
		  		and t.invest_status = #{investStatus}
		  	</if>
		  	<if test="explain !=null and explain !=''">
		  		and t.explain like '%'|| #{explain} ||'%'
		  	</if>
		  </where>
		  Order By t.Last_Update Desc
	</select>
	
	<!-- 查询投后报告流水号 -->
	<select id="getInvestFollowupSeq" resultType="int">
		SELECT SEQ_INVEST_FOLLOWUP_ID.NEXTVAL FROM DUAL
	</select>
	
</mapper>
