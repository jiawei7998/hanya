package com.singlee.capital.system.mapper;

import com.singlee.capital.base.pojo.AccInSecuTreeAttributesVo;
import com.singlee.capital.base.pojo.UserAccInSecuVisitVo;
import com.singlee.capital.system.model.TtAccInSecuVisit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 柜员账户权限关系表
 * @author gm
 */
public interface UserAccInSecuMapper {

	/**
	 * 根据柜员ID查询出整个账户树结构的权限关系
	 * @param map
	 * user_id:柜员ID（不允许为空）
	 * @return
	*/
	public List<UserAccInSecuVisitVo> searchUserAccInSecu(HashMap<String, Object> map); 
	
	/**
	 * 查询用户与内部证券账户对应关系
	 * @param map
	 * @return
	 */
	public TtAccInSecuVisit selectTtAccInSecuVisit(HashMap<String, Object> map);

	/**
	 * 修改
	 * @param ttAccInSecuVisit
	 */
	public void update(TtAccInSecuVisit ttAccInSecuVisit);
	
	/**
	 * 根据内部证券账户编码的删除所有对应关系
	 * @param accid
	 */
	public void deleteAccInSecuVisit(String accid);
	
	public void deleteByVisitId(String visitId);
	
	public void insertTtAccInSecuVisit(TtAccInSecuVisit ttAccInSecuVisit);
	
	/**
	 * 根据用户id和权限级别返回有内部证券账户列表
	 * @param map
	 * user_id: 柜员编号
	 * visit_level: 访问级别
	 * @return
	*/
	public List<AccInSecuTreeAttributesVo> searchAccInSecuVisitByUserIdAndLevel(Map<String, Object> map);
}
