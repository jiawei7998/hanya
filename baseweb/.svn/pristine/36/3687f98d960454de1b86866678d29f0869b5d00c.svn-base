package com.singlee.capital.base.util;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.converter.ExcelToHtmlConverter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author 黎化冰
 *
 */
@SuppressWarnings({"deprecation"})
public class PoiUtil {
	
	/**
	 * 定位 行 
	 * @param sheet
	 * @param key
	 * @return
	 */
	public static final Cell locateCell(Sheet sheet, String key){
		JY.require(sheet!=null, "sheet不允许为空！");
		Cell cell = findCell(sheet,key);
		return cell;
	}
	
	/**
	 * 从一个sheet中查找是否含有key值的表格
	 * @param sheet
	 * @param key
	 * @return
	 */
	public static final Cell findCell(Sheet sheet, String key){
		for(int i=sheet.getFirstRowNum();i<=sheet.getLastRowNum();i++){
			Row row = sheet.getRow(i);
			if(null != row){
				Cell cell = findCell(row, key);
				if(null != cell) {
					return cell;}
			}
		}
		return null;
	}
	
	/**
	 * 从一行中查找是否含有key值的单元格
	 * @param row
	 * @param key
	 * @return
	 */
	public static final Cell findCell(Row row,String key){
		for(int i=row.getFirstCellNum();i<row.getLastCellNum();i++){
			Cell cell = row.getCell(i);
			String cellValue = getCellStringValue(cell);
			if(cellValue.equals(key)) {
				return cell;}
		}
		return null;
	}
	/**
	 * 获取单元格的字符串值
	 * @param cell
	 * @return
	 */
	public static String getCellStringValue(Cell cell){
		String stringCellValue = null;
		if(null != cell){
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				stringCellValue = cell.getRichStringCellValue().getString();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if(DateUtil.isCellDateFormatted(cell)){
					stringCellValue = com.singlee.capital.common.util.DateUtil.format(cell.getDateCellValue());
				}
				else{
					stringCellValue = String.valueOf(cell.getNumericCellValue());
				}
				break;
			case Cell.CELL_TYPE_BLANK:
				stringCellValue = null;
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				stringCellValue = String.valueOf(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_ERROR:
				stringCellValue = null;
				break;
			case Cell.CELL_TYPE_FORMULA:
				stringCellValue = getStringFormulaValue(cell.getSheet().getWorkbook(),cell);
				break;
			default:
				break;
			}
		}
		return stringCellValue;
	}
	/**
	 * 获取公式的值，string表示
	 * @param wb
	 * @param cell
	 * @return
	 */
	public static String getStringFormulaValue(Workbook wb, Cell cell){
		String stringCellValue = null;
		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		CellValue cellValue = evaluator.evaluate(cell);
		switch (cellValue.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			stringCellValue = String.valueOf(cellValue.getBooleanValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			stringCellValue = String.valueOf(cellValue.getNumberValue());
			break;
		case Cell.CELL_TYPE_STRING:
			stringCellValue = String.valueOf(cellValue.getStringValue());
			break;
		default:
			break;
		}
		return stringCellValue;
	}
	
	public static final Cell getCell(Sheet sheet,int columnNum,int rowNum){
		Cell cell = null;
		JY.require(sheet!=null, "sheet不允许为空！");
		Row row = sheet.getRow(rowNum);
		if(null != row) {
			cell = row.getCell(columnNum);}
		return cell;
	}
	/**
	 * 获取Cell的字符串值
	 * @param sheet
	 * @param column
	 * @param row
	 * @param def
	 * @return
	 */
	public static final String getString(Sheet sheet, int column, int row, String def){
		JY.require(sheet!=null, "sheet不允许为空！");
		Cell cell = getCell(sheet,column, row);
		String val = "";
		if(cell != null){
			val = getCellStringValue(cell);
		}
		if(StringUtils.isEmpty(val)){
			return def;
		}else{
			return val.trim();
		}
	}
	
	/**
	 * 获取Cell的double值
	 * @param sheet
	 * @param column
	 * @param row
	 * @param def
	 * @return
	 */
	public static final Double getDouble(Sheet sheet, int column, int row, Double def){
		String val = getString(sheet,column, row, null);
		if(StringUtils.isEmpty(val)){
			return def;
		}else{
			try{
				val = val.replace(",", "");
				if(val.contains("(") && val.contains(")")){
				val = val.replace("(", "");
				val = val.replace(")", "");
				val="-"+val;
				}
				return Double.parseDouble(val);
			}catch(Exception e){
				JY.raise("%s sheet [%d,%d]数据(%s)不能解析为double！",sheet.getSheetName(), column+1,row+1, val);
				return def;
			}
		}
	}
	
	
    /**
     * 获取内容为value的cell 同行的下一个cell的String类型内容
     * @param sheet
     * @param value
     * @param def 为空后的默认值
     * @return
     */
	public static final String getNextColumnCellString(Sheet sheet, String value, String def){
		Cell cell = locateCell(sheet, value);
		JY.require(cell!=null, "内容为 \"%s\" 的Cell不存在！",value);
		String val = getString(sheet, cell.getColumnIndex()+1, cell.getRowIndex(), def);
		if(StringUtils.isEmpty(val)){
			val = getString(sheet, cell.getColumnIndex()+2, cell.getRowIndex(), def);
		}
		return val;
	}
	
	/**
	 * 获取内容为value的cell 同行的下一个cell的Double类型内容
	 * @param sheet
	 * @param value
	 * @param def  为空后的默认值
	 * @return
	 */
	public static final Double getNextColumnCellDouble(Sheet sheet, String value, Double def){
		Cell cell = locateCell(sheet, value);
		JY.require(cell!=null, "内容为 \"%s\" 的Cell不存在！",value);
		return getDouble(sheet, cell.getColumnIndex()+1, cell.getRowIndex(), def);
	}

	/**
	 * 获取Cell中分隔符sep后的字符串
	 * @param sheet
	 * @param column
	 * @param row
	 * @param sep
	 * @param def
	 * @return
	 */
	public static final String getSepString(Sheet sheet, int column, int row, String sep, String def){
		String val = getString(sheet,column,row,"");
		if(val.indexOf(sep)>=0 && (val.length()>val.indexOf(sep)+1)){
			return val.substring(val.indexOf(sep)+1);
		}else{
			return def;
		}
	}
	
	/**
     * 功能：拷贝sheet
     * 实际调用     copySheet(targetSheet, sourceSheet, targetWork, sourceWork, true)
     * @param targetSheet
     * @param sourceSheet
     * @param targetWork
     * @param sourceWork                                                                   
     */
    public static void copySheet(Sheet targetSheet, Sheet sourceSheet,
            Workbook targetWork, Workbook sourceWork) throws Exception{
        if(targetSheet == null || sourceSheet == null || targetWork == null || sourceWork == null){
            throw new IllegalArgumentException("调用PoiUtil.copySheet()方法时，targetSheet、sourceSheet、targetWork、sourceWork都不能为空，故抛出该异常！");
        }
        copySheet(targetSheet, sourceSheet, targetWork, sourceWork, true);
    }
 
    /**
     * 功能：拷贝sheet
     * @param targetSheet
     * @param sourceSheet
     * @param targetWork
     * @param sourceWork
     * @param copyStyle                 boolean 是否拷贝样式
     */
    public static void copySheet(Sheet targetSheet, Sheet sourceSheet,
    		Workbook targetWork, Workbook sourceWork, boolean copyStyle)throws Exception {
         
        if(targetSheet == null || sourceSheet == null || targetWork == null || sourceWork == null){
            throw new IllegalArgumentException("调用PoiUtil.copySheet()方法时，targetSheet、sourceSheet、targetWork、sourceWork都不能为空，故抛出该异常！");
        }
         
        //复制源表中的行
        int maxColumnNum = 0;
        Map<String,Object> styleMap = (copyStyle) ? new HashMap<String,Object>() : null;
         
        Drawing patriarch = targetSheet.createDrawingPatriarch(); //用于复制注释
        //sourceSheet.getLastRowNum()获取当前工作表的最后一行，这个值为有效行数-1，故需要<=遍历
        for (int i = sourceSheet.getFirstRowNum(); i <= sourceSheet.getLastRowNum(); i++) {
            Row sourceRow = sourceSheet.getRow(i);
            Row targetRow = targetSheet.createRow(i);
            if (sourceRow != null) {
                copyRow(targetRow, sourceRow,
                        targetWork, sourceWork,patriarch, styleMap);
                if (sourceRow.getLastCellNum() > maxColumnNum) {
                    maxColumnNum = sourceRow.getLastCellNum();
                }
            }
        }
        //复制源表中的合并单元格
        mergerRegion(targetSheet, sourceSheet);
        //设置目标sheet的列宽
        for (int i = 0; i <= maxColumnNum; i++) {
            targetSheet.setColumnWidth(i, sourceSheet.getColumnWidth(i));
        }
        //复制打印参数
        targetSheet.setMargin(HSSFSheet.TopMargin,sourceSheet.getMargin(HSSFSheet.TopMargin));// 页边距（上）  
        targetSheet.setMargin(HSSFSheet.BottomMargin,sourceSheet.getMargin(HSSFSheet.BottomMargin));// 页边距（下）  
        targetSheet.setMargin(HSSFSheet.LeftMargin,sourceSheet.getMargin(HSSFSheet.LeftMargin) );// 页边距（左）  
        targetSheet.setMargin(HSSFSheet.RightMargin,sourceSheet.getMargin(HSSFSheet.RightMargin));// 页边距（右  

        PrintSetup sourcePs = sourceSheet.getPrintSetup();  
        PrintSetup targetPs = targetSheet.getPrintSetup();  
        targetPs.setLandscape(sourcePs.getLandscape()); // 打印方向，true：横向，false：纵向(默认)  
        targetPs.setVResolution(sourcePs.getVResolution());  
        targetPs.setPaperSize(sourcePs.getPaperSize()); //纸张类型     HSSFPrintSetup.A4_PAPERSIZE
        
    }
     
    /**
     * 功能：拷贝row
     * @param targetRow
     * @param sourceRow
     * @param styleMap
     * @param targetWork
     * @param sourceWork
     * @param targetPatriarch
     */
    public static void copyRow(Row targetRow, Row sourceRow,
            Workbook targetWork, Workbook sourceWork,Drawing targetPatriarch, Map<String,Object> styleMap) throws Exception {
        if(targetRow == null || sourceRow == null || targetWork == null || sourceWork == null || targetPatriarch == null){
            throw new IllegalArgumentException("调用PoiUtil.copyRow()方法时，targetRow、sourceRow、targetWork、sourceWork、targetPatriarch都不能为空，故抛出该异常！");
        }
         
        //设置行高
        targetRow.setHeight(sourceRow.getHeight());
        //如果改行一个单元格都没有，则直接返回
        if(sourceRow.getFirstCellNum() == -1) {
        	return;}
        //sourceRow.getLastCellNum()获取当前行最后的一列，数据为当前行有效列的列数
        for (int i = sourceRow.getFirstCellNum(); i < sourceRow.getLastCellNum(); i++) {
            Cell sourceCell = sourceRow.getCell(i);
            Cell targetCell = targetRow.getCell(i);
            if (sourceCell != null) {
                if (targetCell == null) {
                    targetCell = targetRow.createCell(i);
                }
                 
                //拷贝单元格，包括内容和样式
                copyCell(targetCell, sourceCell, targetWork, sourceWork, styleMap);
                 
                //如果为2003则拷贝单元格注释，否则暂时不拷贝注释
                if(sourceWork instanceof HSSFWorkbook) {
                	copyComment(targetCell,sourceCell,targetPatriarch);}
            }
        }
    }
     
    
    /**
     * 功能：拷贝cell，依据styleMap是否为空判断是否拷贝单元格样式
     * @param targetCell            不能为空
     * @param sourceCell            不能为空
     * @param targetWork            不能为空
     * @param sourceWork            不能为空
     * @param styleMap              可以为空                
     */
    public static void copyCell(Cell targetCell, Cell sourceCell, Workbook targetWork, Workbook sourceWork,Map<String,Object> styleMap) {
        if(targetCell == null || sourceCell == null || targetWork == null || sourceWork == null ){
            throw new IllegalArgumentException("调用PoiUtil.copyCell()方法时，targetCell、sourceCell、targetWork、sourceWork都不能为空，故抛出该异常！");
        }
         
        //处理单元格样式
        if(styleMap != null){
            if (targetWork == sourceWork) {
            	//同一个excel内复制可以直接使用targetCell.setCellStyle(sourceCell.getCellStyle());
            	//如果不是同一个excel则不能使用
                targetCell.setCellStyle(sourceCell.getCellStyle());
            } else {
                String stHashCode = "" + sourceCell.getCellStyle().hashCode();
                CellStyle targetCellStyle = (CellStyle) styleMap.get(stHashCode);
                if (targetCellStyle == null) {
                    targetCellStyle = targetWork.createCellStyle();
                    targetCellStyle.cloneStyleFrom(sourceCell.getCellStyle());
                    styleMap.put(stHashCode, targetCellStyle);
                }
                //不是同一个excel需要使用克隆的方式复制样式，否则会报错
                targetCell.setCellStyle(targetCellStyle);
            }
        }
         
        //处理单元格内容
        switch (sourceCell.getCellType()) {
        case Cell.CELL_TYPE_STRING:
            targetCell.setCellValue(sourceCell.getRichStringCellValue());
            break;
        case Cell.CELL_TYPE_NUMERIC:
			if(DateUtil.isCellDateFormatted(sourceCell)){
				targetCell.setCellValue(sourceCell.getDateCellValue());
			}else{
				targetCell.setCellValue(sourceCell.getNumericCellValue());
			}
			break;
        case Cell.CELL_TYPE_BLANK:
            targetCell.setCellType(Cell.CELL_TYPE_BLANK);
            break;
        case Cell.CELL_TYPE_BOOLEAN:
            targetCell.setCellValue(sourceCell.getBooleanCellValue());
            break;
        case Cell.CELL_TYPE_ERROR:
            targetCell.setCellErrorValue(sourceCell.getErrorCellValue());
            break;
        case Cell.CELL_TYPE_FORMULA:
            targetCell.setCellFormula(sourceCell.getCellFormula());
            break;
        default:
            break;
        }
    }
     
    /**
     * 功能：拷贝comment
     * @param targetCell
     * @param sourceCell
     * @param targetPatriarch
     */
    public static void copyComment(Cell targetCell,Cell sourceCell,Drawing targetPatriarch)throws Exception{
        if(targetCell == null || sourceCell == null || targetPatriarch == null){
            throw new IllegalArgumentException("调用PoiUtil.copyCommentr()方法时，targetCell、sourceCell、targetPatriarch都不能为空，故抛出该异常！");
        }
         
        //处理单元格注释
        HSSFComment comment = (HSSFComment) sourceCell.getCellComment();
        if(comment != null){
            HSSFComment newComment = ((HSSFPatriarch)targetPatriarch).createComment(new HSSFClientAnchor());
            newComment.setAuthor(comment.getAuthor());
            newComment.setColumn(comment.getColumn());
            newComment.setFillColor(comment.getFillColor());
            newComment.setHorizontalAlignment(comment.getHorizontalAlignment());
            newComment.setLineStyle(comment.getLineStyle());
            newComment.setLineStyleColor(comment.getLineStyleColor());
            newComment.setLineWidth(comment.getLineWidth());
            newComment.setMarginBottom(comment.getMarginBottom());
            newComment.setMarginLeft(comment.getMarginLeft());
            newComment.setMarginTop(comment.getMarginTop());
            newComment.setMarginRight(comment.getMarginRight());
            newComment.setNoFill(comment.isNoFill());
            newComment.setRow(comment.getRow());
            newComment.setShapeType(comment.getShapeType());
            newComment.setString(comment.getString());
            newComment.setVerticalAlignment(comment.getVerticalAlignment());
            newComment.setVisible(comment.isVisible());
            targetCell.setCellComment(newComment);
        }
    }
    
     
    /**
     * 功能：复制原有sheet的合并单元格到新创建的sheet
     * 
     * @param sheetCreat
     * @param sourceSheet
     */
    public static void mergerRegion(Sheet targetSheet, Sheet sourceSheet)throws Exception {
        if(targetSheet == null || sourceSheet == null){
            throw new IllegalArgumentException("调用PoiUtil.mergerRegion()方法时，targetSheet或者sourceSheet不能为空，故抛出该异常！");
        }
         
        for (int i = 0; i < sourceSheet.getNumMergedRegions(); i++) {
            CellRangeAddress oldRange = sourceSheet.getMergedRegion(i);
            CellRangeAddress newRange = new CellRangeAddress(
                    oldRange.getFirstRow(), oldRange.getLastRow(),
                    oldRange.getFirstColumn(), oldRange.getLastColumn());
            targetSheet.addMergedRegion(newRange);
        }
    }
 
    /**
     * 功能：重新定义HSSFColor.PINK的色值
     * 
     * @param workbook
     * @return
     */
    public static HSSFColor setMBorderColor(HSSFWorkbook workbook) {
        HSSFPalette palette = workbook.getCustomPalette();
        HSSFColor hssfColor = null;
        byte[] rgb = { (byte) 0, (byte) 128, (byte) 192 };
        try {
            hssfColor = palette.findColor(rgb[0], rgb[1], rgb[2]);
            if (hssfColor == null) {
                palette.setColorAtIndex(HSSFColor.PINK.index, rgb[0], rgb[1],
                        rgb[2]);
                hssfColor = palette.getColor(HSSFColor.PINK.index);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            throw new RException(e);
        }
        return hssfColor;
    }
    /**
     * 将excel转成html
     * @param excelFilePath
     * @param htmlFilePath
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
	public static void convertExcel2Html(String excelFilePath, String htmlFilePath) throws IOException, ParserConfigurationException, TransformerException {
		File excelFile = new File(excelFilePath);
		File htmlFile = new File(htmlFilePath);
		File htmlFileParent = new File(htmlFile.getParent());
		InputStream is = null;
		OutputStream out = null;
		StringWriter writer = null;
		try {
			if (excelFile.exists()) {
				if (!htmlFileParent.exists()) {
					htmlFileParent.mkdirs();
				}
				is = new FileInputStream(excelFile);
				HSSFWorkbook workBook = new HSSFWorkbook(is);
				ExcelToHtmlConverter converter = new ExcelToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());

				converter.processWorkbook(workBook);

				writer = new StringWriter();
				Transformer serializer = TransformerFactory.newInstance().newTransformer();
				serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				serializer.setOutputProperty(OutputKeys.INDENT, "yes");
				serializer.setOutputProperty(OutputKeys.METHOD, "html");
				serializer.transform(new DOMSource(converter.getDocument()), new StreamResult(writer));
				out = new FileOutputStream(htmlFile);
				out.write(writer.toString().getBytes("UTF-8"));
				out.flush();
				out.close();
				writer.close();
			}
		} finally {
			try {
				if (is != null) {
					is.close();
				}
				if (out != null) {
					out.close();
				}
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				//e.printStackTrace();
				throw new RException(e);
			}
		}
	}
}
