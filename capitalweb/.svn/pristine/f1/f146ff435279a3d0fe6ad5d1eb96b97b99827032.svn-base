﻿<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.hrbreport.mapper.G22ReportMapper">
	
	<select id="getG22ReportDlAmt" parameterType="map" resultType="map">
		SELECT 
		    SUM(ABS(CASE WHEN CCYAMT IS NULL THEN 0 ELSE CCYAMT END)) CCYAMT,
		    AL
		FROM ${OPICS}.DLDT, ${OPICS}.CUST
		WHERE DLDT.BR = #{br}
		      AND VERIND IS NOT NULL
		      AND DLDT.CNO = CUST.CNO
		      AND REVDATE IS NULL
		      AND DLDT.CCY = 'CNY'
		      AND <![CDATA[DLDT.MDATE - TO_DATE(#{date},'YYYYMMDD') <= 30]]>
		      AND <![CDATA[DLDT.MDATE - TO_DATE(#{date},'YYYYMMDD') > 0]]>
		      AND <![CDATA[DLDT.VDATE <= TO_DATE(#{date},'YYYYMMDD')]]>
		      AND CUST.ACCTNGTYPE IN ('POLICYBK', 'COMMBK-D', 'COMMBK-F', 'OTHERDEFIN','OTHNODEFIN','ZGS','ZGSCZYH', 'ZGSJRGS')
		GROUP BY AL
	</select>
	
	<select id="getG22ReportRepoAmt" parameterType="map" resultType="map">
		SELECT 
		     PS,
		     SUM(ABS(CASE WHEN COMPROCDAMT IS NULL THEN 0 ELSE COMPROCDAMT END)) NOTAMT
		FROM ${OPICS}.RPRH, ${OPICS}.CUST
		WHERE RPRH.BR = #{br}
		      AND VERIND IS NOT NULL
		      AND RPRH.CNO = CUST.CNO
		      AND REVDATE IS NULL
		      AND <![CDATA[RPRH.MATDATE - TO_DATE(#{date},'YYYYMMDD') <= 30]]>
		      AND <![CDATA[RPRH.MATDATE - TO_DATE(#{date},'YYYYMMDD') > 0]]>
		      AND <![CDATA[RPRH.VDATE <= TO_DATE(#{date},'YYYYMMDD')]]>
		      AND RPRH.CCY = 'CNY'
		      AND CUST.ACCTNGTYPE IN ('POLICYBK','COMMBK-D','COMMBK-F', 'OTHERDEFIN', 'OTHNODEFIN', 'ZGS', 'ZGSCZYH', 'ZGSJRGS')
		GROUP BY PS
	</select>
	
	<select id="getG22ReportBond" parameterType="map" resultType="map">
		SELECT 
		    SUM(ACCROUTST) ACCROUTSTTPOS
		FROM (SELECT 
		          ABS(TPOS.ACCROUTST) ACCROUTST,
		          TPOS.INVTYPE,
		          TPOS.PRODTYPE,
		          SECM.MDATE,
		          SECS.IPAYDATE,
		          CASE WHEN IPAYDATE IS NULL THEN SECM.MDATE ELSE IPAYDATE END LASTMDATE
		          FROM ${OPICS}.SL_TPOS_H TPOS
		          LEFT JOIN ${OPICS}.SECM ON SECM.SECID = TPOS.SECID
		          LEFT JOIN (SELECT 
		                         MIN(SECS.IPAYDATE) IPAYDATE, 
		                         SECID
		                     FROM ${OPICS}.SECS
		                     WHERE <![CDATA[IPAYDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>
		                           AND <![CDATA[IPAYDATE - TO_DATE(#{date},'YYYY-MM-DD') <= 30]]>
		                           GROUP BY SECID
		                     ) SECS ON SECS.SECID = TPOS.SECID
		         WHERE TPOS.BR = #{br}
		               AND TPOS.CCY = 'CNY'
		               AND <![CDATA[SECM.MDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>
		               AND TPOS.POSTDATE = TO_DATE(#{date}, 'YYYY-MM-DD')
		               AND <![CDATA[TPOS.SETTQTY <> 0]]>
		               <if test="sqlType == 2">
		               		AND TPOS.INVTYPE IN ('A','H') AND TPOS.PRODTYPE = 'IM'
		               </if>
		) WHERE LASTMDATE - TO_DATE('20141210', 'YYYY-MM-DD') BETWEEN 0 AND 30
	</select>
	
	<select id="getG22ReportDlRate" parameterType="map" resultType="map">
		SELECT SUM(ABS(CASE WHEN <![CDATA[DLDT.MDATE <= TO_DATE(#{date},'YYYY-MM-DD')]]> THEN  
		                  SCHD.TOTPAYAMT - DLDT.CCYAMT - FIND.YTDPINCEXP
		                 ELSE
		                  FIND.ACCROUTST - FIND.YTDPINCEXP
		               END)
		           ) ACCROUTSTDLDT,
		       DLDT.AL
		FROM ${OPICS}.DLDT
		LEFT JOIN ${OPICS}.SL_FIND_H FIND ON TRIM(FIND.DEALNO) = TRIM(DLDT.DEALNO)
		     AND TRIM(DLDT.BR) = TRIM(FIND.BR) 
		     AND <![CDATA[FIND.POSTDATE = TO_DATE(#{date},'YYYY-MM-DD')]]>
		     AND TRIM(DLDT.PRODUCT) = TRIM(FIND.PRODUCT)
		     AND TRIM(DLDT.PRODTYPE) = TRIM(FIND.PRODTYPE)
		     AND TRIM(DLDT.CCY) = TRIM(FIND.CCY)
		     AND TRIM(DLDT.SEQ) = TRIM(FIND.SEQ)
		     AND FIND.INCEXPTYPE = 'A'
		     AND FIND.ACCROUTST != 0
		LEFT JOIN ${OPICS}.SCHD ON TRIM(SCHD.DEALNO) = TRIM(DLDT.DEALNO)
		     AND TRIM(SCHD.PRODTYPE) = TRIM(DLDT.PRODTYPE)
		     AND TRIM(DLDT.PRODUCT) = TRIM(SCHD.PRODUCT)
		     AND SCHDTYPE = 'M'
		     AND SCHD.BR = DLDT.BR
		     AND SCHD.SEQ = DLDT.SEQ
		WHERE DLDT.BR = #{br}
		      AND DLDT.REVDATE IS NULL
		      AND DLDT.VERDATE IS NOT NULL
		      AND TRIM(DLDT.CCY) = 'CNY'
		      AND <![CDATA[DLDT.MDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>      
		      AND <![CDATA[DLDT.VDATE <= TO_DATE(#{date},'YYYY-MM-DD')]]>      
		      AND <![CDATA[DLDT.MDATE - TO_DATE(#{date},'YYYY-MM-DD') > 0]]>
		      AND <![CDATA[DLDT.MDATE - TO_DATE(#{date},'YYYY-MM-DD') <= 30]]>
		      GROUP BY DLDT.AL
	</select>
	
	<select id="getG22ReportRepoRate" parameterType="map" resultType="map">
		SELECT SUM(ABS(CASE WHEN <![CDATA[RPRH.MATDATE > TO_DATE(#{date},'YYYY-MM-DD')]]> THEN    
		                   FIND.ACCROUTST - FIND.YTDPINCEXP
		                ELSE
		                   RPRH.TOTALINT - FIND.YTDPINCEXP
		                END)) ACCROUTSTRPRH,
		        CASE WHEN RPRH.PRODTYPE IN ('VB', 'VP') THEN 'VBP'
		             ELSE 'RBP' END TYPERPRH
		FROM ${OPICS}.RPRH
		LEFT JOIN ${OPICS}.SL_FIND_H FIND  ON TRIM(FIND.DEALNO) = TRIM(RPRH.DEALNO)
		     AND TRIM(RPRH.BR) = TRIM(FIND.BR)
		     AND FIND.POSTDATE = TO_DATE(#{date}, 'YYYY-MM-DD')
		     AND TRIM(RPRH.PRODUCT) = TRIM(FIND.PRODUCT)
		     AND TRIM(RPRH.PRODTYPE) = TRIM(FIND.PRODTYPE)
		     AND TRIM(RPRH.CCY) = TRIM(FIND.CCY)
		     AND TRIM(RPRH.SEQ) = TRIM(FIND.SEQ)
		     AND FIND.INCEXPTYPE = 'A'
		WHERE RPRH.BR = #{br}
		    AND RPRH.REVDATE IS NULL
		    AND RPRH.VERDATE IS NOT NULL
		    AND TRIM(RPRH.CCY) = 'CNY'
		    AND <![CDATA[RPRH.MATDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>
		    AND <![CDATA[RPRH.VDATE <= TO_DATE(#{date},'YYYY-MM-DD')]]>
		    AND <![CDATA[RPRH.MATDATE - TO_DATE(#{date},'YYYY-MM-DD') > 0]]>
		    AND <![CDATA[RPRH.MATDATE - TO_DATE(#{date},'YYYY-MM-DD') <= 30]]>
		    AND RPRH.PRODTYPE IN ('VB', 'VP', 'RB', 'RP')
		GROUP BY CASE WHEN RPRH.PRODTYPE IN ('VB', 'VP') THEN  'VBP' ELSE  'RBP' END
	</select>
	
	<select id="getG22ReportBondHat" parameterType="map" resultType="map">
		SELECT SUM(CASE WHEN TRIM(AT) IS NULL THEN 0 ELSE AT END) + 
		       SUM(CASE WHEN TRIM(AA) IS NULL THEN 0 ELSE AA END) + 
		       SUM(CASE WHEN TRIM(AH) IS NULL THEN 0 ELSE AH END) +
		       SUM(CASE WHEN TRIM(AA1) IS NULL THEN 0 ELSE AA1 END) + 
		       SUM(CASE WHEN TRIM(AH1) IS NULL THEN 0 ELSE AH1 END) BAL
		FROM (
		     SELECT T.SECID,
		            CASE WHEN TRIM(T.INVTYPE) = 'T' THEN
		                  ABS(T.SETTAVGCOST) + T.TDYMTM END AT,
		            CASE WHEN TRIM(T.INVTYPE) = 'A' AND S.PRODTYPE = 'IM' THEN
		                  ABS(T.PRINAMT) + ABS(T.ACCROUTST) + T.UNAMORTAMT + T.TDYMTM END AA,
		            CASE WHEN TRIM(T.INVTYPE) = 'A' AND S.PRODTYPE != 'IM' THEN
		                  ABS(T.PRINAMT) + T.UNAMORTAMT + T.TDYMTM END AA1,
		            CASE WHEN TRIM(T.INVTYPE) = 'H' AND S.PRODTYPE = 'IM' THEN
		                  ABS(T.PRINAMT) + ABS(T.ACCROUTST) - (T.UNAMORTAMT * -1) END AH,
		            CASE WHEN TRIM(T.INVTYPE) = 'H' AND S.PRODTYPE != 'IM' THEN
		                  ABS(T.PRINAMT) - (T.UNAMORTAMT * -1) END AH1
		     FROM ${OPICS}.SL_TPOS_H T
		     LEFT JOIN ${OPICS}.SECM S ON T.SECID = S.SECID
		     WHERE T.BR = #{br}
		           AND <![CDATA[MDATE > TO_DATE(#{date},'YYYYMMDD')]]>
		           AND T.POSTDATE = TO_DATE(#{date},'YYYYMMDD')
		           AND <![CDATA[T.SETTQTY <> 0]]>
		           AND T.INVTYPE = 'H'
		           AND <![CDATA[S.MDATE - TO_DATE(#{date},'YYYYMMDD') <= 30]]>
		)	
	</select>
	
	<select id="getG22ReportBondPledge" parameterType="map" resultType="map">
		SELECT 
		     CASE WHEN PRINAMTS IS NULL OR PRINAMTS = 0 THEN 0 ELSE BAL / PRINAMTP * PRINAMTS END BAL
		FROM (
		     SELECT 
		          SUM(PRINAMTP) PRINAMTP, 
		          SUM(PRINAMTS) PRINAMTS, 
		          SUM(BAL) BAL
		     FROM (
		          SELECT 
		               SUM(ABS(RPDT.PRINAMT)) PRINAMTP, 
		               RPDT.SECID
		          FROM ${OPICS}.RPDT
		          WHERE RPDT.BR = 01
		                AND RPDT.PRODTYPE IN ('RP', 'RB')
		                AND RPDT.REVDATE IS NULL
		                AND RPDT.VERDATE IS NOT NULL
		                AND <![CDATA[RPDT.MDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>
		                AND <![CDATA[RPDT.MDATE - TO_DATE(#{date},'YYYY-MM-DD') <= 30]]>
		                AND RPDT.CCY = 'CNY'
		                <if test="sqlType == 2">
		                	<![CDATA[AND RPDT.VDATE <=TO_DATE(#{date},'YYYY-MM-DD')]]>
		                </if>
		                GROUP BY RPDT.SECID
		     ) RPDT
		     LEFT JOIN (
		               <if test="sqlType == 1">
			               SELECT SECID,
			                      SUM(ABS(PRINAMT)) PRINAMTS,
			                      SUM(AT) + SUM(AA) + SUM(AH) + SUM(AA1) + SUM(AH1) BAL
			               FROM (
				               SELECT <!-- TPOS.ID, -->
				                      TPOS.PRINAMT,
				                      TPOS.SECID,
				                      CASE WHEN TRIM(TPOS.INVTYPE) = 'T' THEN 
				                           ABS(TPOS.SETTAVGCOST) + TPOS.TDYMTM ELSE 0 END AT,
				                      CASE WHEN TRIM(TPOS.INVTYPE) = 'A' AND SECM.PRODTYPE = 'IM' THEN 
				                           ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT + TPOS.TDYMTM ELSE 0 END AA,
				                      CASE WHEN TRIM(TPOS.INVTYPE) = 'H' AND SECM.PRODTYPE = 'IM' THEN
				                           ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) - (TPOS.UNAMORTAMT * -1)  ELSE 0 END AH,
				                      CASE WHEN TRIM(TPOS.INVTYPE) = 'A' AND SECM.PRODTYPE != 'IM' THEN
				                           ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT + TPOS.TDYMTM ELSE 0 END AA1,
				                      CASE WHEN TRIM(TPOS.INVTYPE) = 'H' AND SECM.PRODTYPE != 'IM' THEN
				                           ABS(TPOS.PRINAMT) -(TPOS.UNAMORTAMT * -1) ELSE 0 END AH1
				                      FROM ${OPICS}.SL_TPOS_H TPOS
				                      LEFT JOIN ${OPICS}.SECM ON SECM.SECID = TPOS.SECID
				                      WHERE TPOS.POSTDATE = TO_DATE(#{date},'YYYY-MM-DD') 
				                            AND <![CDATA[SECM.MDATE > TO_DATE(#{date},'YYYY-MM-DD')]]>
				                            AND <![CDATA[SECM.MDATE - TO_DATE(#{date},'YYYY-MM-DD') <= 30]]>
				                            AND <![CDATA[TPOS.SETTQTY <> 0]]>
				                            AND TPOS.INVTYPE = 'H'
				                ) GROUP BY SECID
		                </if>
		                <if test="sqlType == 2">
		                   SELECT SECID,
			                      SUM(ABS(PRINAMT)) PRINAMTS,
			                      SUM(MKTVAL) BAL
			               FROM (
			                    SELECT <!-- TPOS.ID, -->
			                           TPOS.PRINAMT,
			                           TPOS.SECID,
			                           ABS(MKTVAL) MKTVAL
			                    FROM ${OPICS}.SL_TPOS_H TPOS
			                    LEFT JOIN ${OPICS}.SECM ON SECM.SECID = TPOS.SECID
			                    WHERE TPOS.POSTDATE = TO_DATE(#{date}, 'YYYY-MM-DD') 
			                          AND TPOS.INVTYPE IN ('T', 'A') 
			                          AND <![CDATA[SECM.MDATE -TO_DATE(#{date}, 'YYYY-MM-DD') > 30]]>
			                          AND <![CDATA[TPOS.SETTQTY <> 0]]>
			                ) GROUP BY SECID
		                </if>
		     ) TPOS ON TPOS.SECID = RPDT.SECID
		     WHERE TPOS.SECID IS NOT NULL
		)
	</select>
	
	<select id="getG22ReportSec" parameterType="map" resultType="map">
		SELECT SUM(ABS(CASE WHEN (TPOS.INVTYPE IN ('T', 'A') AND <![CDATA[MDATE - TO_DATE(#{date},'YYYYMMDD') <= 30)]]>
		                     THEN  0  ELSE TPOS.MKTVAL END)) BAL
		FROM ${OPICS}.SL_TPOS_H TPOS
		LEFT JOIN ${OPICS}.SECM ON SECM.SECID = TPOS.SECID
		WHERE TPOS.BR = #{br}
		   AND TPOS.POSTDATE = TO_DATE(#{date},'YYYYMMDD')
		   AND <![CDATA[TPOS.SETTQTY <> 0]]>
		   AND <![CDATA[TPOS.MKTVAL <> 0]]>
		   AND TPOS.INVTYPE IN ('T', 'A')
	</select>
	
</mapper>