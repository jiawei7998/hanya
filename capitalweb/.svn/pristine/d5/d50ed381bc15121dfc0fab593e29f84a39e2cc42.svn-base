<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.ifs.mapper.IfsBondPledgeMapper">

    <sql id="columns">
        ${alias}.TICKET_ID,
        ${alias}.BOND_CODE,
        ${alias}.PORT,
        ${alias}.COST,
        ${alias}.INVTYPE,
        ${alias}.DESCR,
        ${alias}.FACE_VALUE,
        ${alias}.CLEAN_PRICE,
        ${alias}.VDATE,
        ${alias}.MDATE
    </sql>

    <resultMap id="ifsBondPledge" type="com.singlee.ifs.model.IfsBondPledgeBean">
        <result column="TICKET_ID" property="ticketId" />
        <result column="BOND_CODE" property="bondCode"  />
        <result column="PORT" property="port" />
        <result column="COST" property="cost" />
        <result column="INVTYPE" property="invType" />
        <result column="DESCR" property="descr" />
        <result column="FACE_VALUE" property="faceValue" />
        <result column="CLEAN_PRICE" property="cleanPrice" />
        <result column="VDATE" property="vdate" />
        <result column="MDATE" property="mdate" />
    </resultMap>

    <select id="selectBondPledgeList" parameterType="map" resultMap="ifsBondPledge">
        select <include refid="columns"><property name="alias" value="T"/></include>
        from IFS_BOND_PLEDGE T
        <where>
            1=1
            <if test="ticketId!=null and ticketId!=''">
               and TICKET_ID = #{ticketId}
            </if>
            <if test="bondCode!=null and bondCode!=''">
               and BOND_CODE = #{bondCode}
            </if>
            <if test="descr!=null and descr!=''">
               and DESCR like '%'||#{descr}||'%'
            </if>
            <if test="startDate!=null and startDate!=''">
               and MDATE >= to_date(#{startDate},'yyyy-mm-dd')
            </if>
            <if test="endDate!=null and endDate!=''">
               and VDATE &lt;= to_date(#{endDate},'yyyy-mm-dd')
            </if>
        </where>
        order by TICKET_ID,BOND_CODE,PORT,COST,INVTYPE desc
    </select>

    <select id="selectBondPledgeByPrimaryKey" parameterType="map" resultMap="ifsBondPledge">
        select <include refid="columns"><property name="alias" value="T"/></include>
        from IFS_BOND_PLEDGE T
        <where>
            1=1
            <if test="ticketId!=null and ticketId!=''">
                and TICKET_ID = #{ticketId}
            </if>
            <if test="bondCode!=null and bondCode!=''">
                and BOND_CODE = #{bondCode}
            </if>
            <if test="port!=null and port!=''">
                and PORT = #{port}
            </if>
            <if test="cost!=null and cost!=''">
                and COST = #{cost}
            </if>
            <if test="invType!=null and invType!=''">
                and INVTYPE = #{invType}
            </if>
        </where>
    </select>

    <insert id="insert" parameterType="com.singlee.ifs.model.IfsBondPledgeBean">
        INSERT INTO  IFS_BOND_PLEDGE
            (TICKET_ID,BOND_CODE,PORT,COST,INVTYPE,DESCR,FACE_VALUE,CLEAN_PRICE,VDATE,MDATE)
        VALUES
            (#{ticketId},#{bondCode},#{port},#{cost},#{invType},#{descr},#{faceValue},#{cleanPrice},to_date(#{vdate},'yyyy-mm-dd'),to_date(#{mdate},'yyyy-mm-dd'))
    </insert>

    <delete id="deleteByPrimaryKey" parameterType="com.singlee.ifs.model.IfsBondPledgeBean">
        delete from IFS_BOND_PLEDGE
        where TICKET_ID = #{ticketId} and BOND_CODE = #{bondCode} and PORT = #{port}
          and COST = #{cost} and INVTYPE = #{invType}
    </delete>

    <update id="updateByPrimaryKey" parameterType="com.singlee.ifs.model.IfsBondPledgeBean">
        UPDATE IFS_BOND_PLEDGE SET
                DESCR = #{descr},FACE_VALUE = #{faceValue},CLEAN_PRICE = #{cleanPrice},VDATE= to_date(#{vdate},'yyyy-mm-dd'),MDATE = to_date(#{mdate},'yyyy-mm-dd')
        WHERE TICKET_ID = #{ticketId} AND  BOND_CODE = #{bondCode} AND PORT = #{port} AND COST = #{cost} AND INVTYPE = #{invType}
    </update>
	
	
	<delete id="deleteSecmStatus" parameterType="java.lang.String">
        delete from SL_SECM_STATUS
        where SECID = #{secid} and TABLENAME = #{tableName}
    </delete>
    
    <insert id="insertSecmStatus" parameterType="com.singlee.ifs.model.SecmStatus">
		INSERT INTO SL_SECM_STATUS@opics
			(SECID,
			CHOIS_REF_NO ,
			CHOIS_HIS_NO,
			SENDTIME,
			ISSEND,
			ISRESEND,
			SENDRESULT,
			SENDMSG,
			TABLENAME,
			UPCOUNT,
			UPOWN,
			UPTIME,
			INPUTTIME,
			RECVMSG)
			 VALUES
			 (#{secid},
			 #{choisRefNo},
			 #{choisHisNo},
			 #{sendtime},
			 #{issend},
			 #{isresend},
			 #{sendresult},
			 #{sendmsg},
			 #{tablename},
			 #{upcount},
			 #{upown},
			 #{uptime},
			 #{inputtime},
			 #{recvmsg})
	</insert>
	
	<select id="querySecmStatusByDealnoSeq" parameterType="java.lang.String" resultType="com.singlee.ifs.model.SecmStatus">
	select *
	from SL_SECM_STATUS@opics
	where
	SECID = #{secid}
    </select>
    
    <update id="updateSecmStatusByDealnoSeq" parameterType="com.singlee.ifs.model.SecmStatus">
		UPDATE SL_SECM_STATUS@opics SET CHOIS_REF_NO=#{choisRefNo},CHOIS_HIS_NO=#{choisHisNo},SENDTIME=#{sendtime},ISSEND=#{issend},
		ISRESEND=#{isresend},SENDRESULT=#{sendresult},SENDMSG=#{sendmsg},UPCOUNT=#{upcount},UPOWN=#{upown},UPTIME=#{uptime} WHERE SECID=#{secid}
	</update>
	
	<insert id="insertCust" parameterType="com.singlee.capital.choistrd.model.CustRecieve" statementType="CALLABLE">
		{call SL_SP_HANA_CUST(
				#{FOIQ11_CIX_NO,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_BIC_CD,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_CTRY_CD,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_CUST_SNM,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_CUST_ENM,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_ADDR_ENM,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_POST_NO,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_LST_IL,mode=IN,jdbcType=VARCHAR},
				#{FOIQ11_HEAD_CTRY_CD,mode=IN,jdbcType=VARCHAR})
		}
	</insert>
	
	<select id="queryAllSecm" parameterType="java.util.HashMap" resultType="com.singlee.capital.choistrd.model.Secm">

	SELECT 'FBO1000' AS FXFIG_SVCN,
	'FB10' AS FXFIG_IBCD,
	'FB' AS FXFIG_GWAM,
	'10' AS FXFIG_GEOR,
	'OPCS088' AS FXFIG_OPNO,
	TRIM(S.SECID) AS FIFB10_SECURITY_ID,
	(CASE
	WHEN TRIM(S.INTCALCRULE) = 'DIS' THEN
	'1'
	ELSE
	'2'
	END) AS FIFB10_COUPON_STYLE,
	'99' AS FIFB10_BOND_TYPE,
	to_char(S.ISSDATE, 'yyyy-MM-dd') AS FIFB10_ISSUE_IL,
	to_char(S.MDATE, 'yyyy-MM-dd') AS FIFB10_EXP_IL,
	'1' AS FIFB10_ISSUE_MTD,
	'UAT' AS FIFB10_ISSUE_MARKET_NM,
	'1' AS FIFB10_ISSUE_MKT_NATION_CD,
	'1' AS FIFB10_REPAY_PRIORITY,
	(SELECT NVL((CASE
	WHEN BONDCREDITRATING = 'A-2' THEN
	'2'
	WHEN BONDCREDITRATING = 'A' THEN
	'3'
	WHEN BONDCREDITRATING = 'A-' THEN
	'2'
	WHEN BONDCREDITRATING = 'BBB' THEN
	'1'
	WHEN BONDCREDITRATING = 'AAA' THEN
	'8'
	WHEN BONDCREDITRATING = 'BBB+' THEN
	'1'
	WHEN BONDCREDITRATING = 'AA+' THEN
	'7'
	WHEN BONDCREDITRATING = 'A+' THEN
	'4'
	WHEN BONDCREDITRATING = 'A-1' THEN
	'2'
	WHEN BONDCREDITRATING = 'AA' THEN
	'6'
	WHEN BONDCREDITRATING = 'A-' THEN
	'2'
	ELSE
	TRIM(BONDCREDITRATING)
	END),
	' ')
	FROM SL_ORG_ISEC@opics
	WHERE SL_ORG_ISEC.BNDCD = S.SECID
	and rownum = 1) AS FIFB10_CREDIT_AGENCY,
	(SELECT to_number(ISSUEVOL) * 100000000
	FROM SL_ORG_ISEC@opics
	WHERE SL_ORG_ISEC.BNDCD = S.SECID
	and rownum = 1) AS FIFB10_TOTAL_ISSUE_AMT,
	(CASE
	WHEN S.CCY = 'CNY' THEN
	'RMB'
	ELSE
	S.CCY
	END) AS FIFB10_TOTAL_ISSUE_CCY,
	'1' AS FIFB10_REPAY_MTD,
	to_char(S.MDATE, 'yyyy-MM-dd') AS FIFB10_FST_REPAY_IL,
	S.REDEMPAMT AS FIFB10_REPAY_PRICE,
	(SELECT to_number(ISSUEVOL) * 100000000
	FROM SL_ORG_ISEC@opics
	WHERE SL_ORG_ISEC.BNDCD = S.SECID
	and rownum = 1) AS FIFB10_REPAY_AMT,
	(CASE
	WHEN S.RATECODE IS NOT NULL AND S.RATECODE != '' THEN
	'2'
	ELSE
	'1'
	END) AS FIFB10_FIX_FLT_GB,
	'0' AS FIFB10_COUPON_SPREAD_RT,
	'0' AS FIFB10_CAP_RT_YN,
	'0' AS FIFB10_CAP_RT,
	'0' AS FIFB10_FLOOR_RT_YN,
	'0' AS FIFB10_FLOOR_RT,
	'2' AS FIFB10_ADV_ARR_GB,
	(CASE
	WHEN S.CCY = 'CNY' THEN
	'RMB'
	ELSE
	S.CCY
	END) AS FIFB10_RATE_CCY,
	(CASE
	WHEN S.RATECODE IS NOT NULL AND S.RATECODE != '' THEN
	(SELECT (CASE
	WHEN R.DESMAT = '7D' THEN
	'2'
	WHEN R.DESMAT = '1Y' THEN
	'8'
	WHEN R.DESMAT = '1M' THEN
	'3'
	WHEN R.DESMAT = '2M' THEN
	'4'
	WHEN R.DESMAT = '3M' THEN
	'5'
	WHEN R.DESMAT = '6M' THEN
	'6'
	WHEN R.DESMAT = '9M' THEN
	'7'
	ELSE
	'1'
	END)
	FROM RATE@opics R
	WHERE R.RATECODE = S.RATECODE
	and rownum = 1)
	ELSE
	'0'
	END) AS FIFB10_RATE_FREQ,
	(CASE
	WHEN INTPAYCYCLE = 'A' THEN
	'4'
	WHEN INTPAYCYCLE = 'S' THEN
	'3'
	WHEN INTPAYCYCLE = 'Q' THEN
	'2'
	WHEN INTPAYCYCLE = 'M' THEN
	'1'
	ELSE
	'9'
	END) AS FIFB10_INT_FREQ,
	'N' AS FIFB10_COUPON_ADJ_YN,
	'1' AS FIFB10_BSNS_DAY_RULE,
	(CASE
	WHEN S.BASIS = 'ACTUAL' THEN
	'1'
	WHEN S.BASIS = 'A365' THEN
	'4'
	WHEN S.BASIS = 'A360' THEN
	'2'
	WHEN S.BASIS = 'ACT365' THEN
	'4'
	ELSE
	''
	END) AS FIFB10_ACCR_TYPE,
	'1' AS FIFB10_SINGLE_BOTH_TYPE,
	to_char(C.INTSTRTDTE, 'yyyy-MM-dd') AS FIFB10_BASE_IL,
	substr(TRIM(S.ISSUER), -6) AS FIFB10_ISSUE_ID
	FROM SECM@opics S
	LEFT JOIN SECS@opics C
	ON S.SECID = C.SECID
	WHERE DESCR NOT LIKE '%HANA GO LIVE CONVERSION DATA'
	AND ((to_date(#{branprcdate}, 'yyyy-MM-dd') >= INTSTRTDTE AND
	C.INTENDDTE >= to_date(#{branprcdate}, 'yyyy-MM-dd')) OR
	(S.INTCALCRULE = 'DIS'))
	AND NOT EXISTS
	(SELECT *
	FROM SL_SECM_STATUS A
	WHERE S.SECID = A.SECID
	AND ISSEND = 'Y')
	AND (EXISTS (SELECT *
	FROM ISEC@opics I
	LEFT JOIN INTH@opics N
	ON I.FEDEALNO = N.FEDEALNO
	WHERE I.SECID = S.SECID
	AND N.STATCODE = '-8'
	AND N.SERVER = 'PENDINGSECUR') OR NOT EXISTS
	(SELECT *
	FROM ISEC@opics I
	WHERE I.SECID = S.SECID
	AND I.SERVER = 'PENDINGSECUR'))
	</select>
	
	<select id="queryBrps" resultType="java.lang.String">
		SELECT to_char(BRANPRCDATE,'yyyy-MM-dd') FROM BRPS@opics WHERE BR = '01'
    </select>
    
    <select id="queryAllSlNupdStatus" resultType="com.singlee.capital.choistrd.model.SlNupdStatus">
		SELECT * from SL_NUPD_STATUS@OPICS where ADFLAG = 'A'
    </select>
    
    <update id="updateSlNupdStatusByDealNo" parameterType="java.util.HashMap">
		UPDATE SL_NUPD_STATUS@OPICS SET
		ref_no = #{ref_no}, his_no = #{his_no}
		WHERE dealno = #{dealno}
    </update>
</mapper>