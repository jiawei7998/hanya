package com.singlee.capital.trade.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.AdvanceMaturityService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 提前到期
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AdvanceMaturityServiceImpl extends AbstractDurationService implements AdvanceMaturityService {

	@Autowired
	private TdAdvanceMaturityMapper advanceMaturityMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	
	@Autowired 
	private TdRateRangeMapper rateRangeMapper;
	@Autowired
	private DayendDateService dateService;
	@Autowired
	private TdCashflowCapitalMapper cashflowCapitalMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private CashflowFeeService cashflowFeeService;
	@Autowired
	private TdFeesPassAgewayService feesPassAgewayService;
	
	@Override
	public TdAdvanceMaturity getAdvanceMaturityById(String dealNo) {
		return advanceMaturityMapper.getAdvanceMaturityById(dealNo);
	}

	@Override
	public Page<TdAdvanceMaturity> getAdvanceMaturityPage(Map<String, Object> params, int isFinished) {
		Page<TdAdvanceMaturity> page= new Page<TdAdvanceMaturity>();
		if(isFinished == 1){
			page=advanceMaturityMapper.getAdvanceMaturityList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=advanceMaturityMapper.getAdvanceMaturityListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {
			page=advanceMaturityMapper.getAdvanceMaturityListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		List<TdAdvanceMaturity> list =page.getResult();
		for(TdAdvanceMaturity tm :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(tm.getRefNo());
			if(main!=null){
				tm.setProduct(main.getProduct());
				tm.setParty(main.getCounterParty());
				tm.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建提前到期金融工具表")
	public void createAdvanceMaturity(Map<String, Object> params) {
		advanceMaturityMapper.insert(advanceMaturity(params));
	}

	@Override
	@AutoLogMethod(value = "修改提前到期金融工具表")
	public void updateAdvanceMaturity(Map<String, Object> params) {
		advanceMaturityMapper.updateByPrimaryKey(advanceMaturity(params));
	}

	
	private TdAdvanceMaturity advanceMaturity(Map<String, Object> params){
		// map转实体类
		TdAdvanceMaturity advanceMaturity = new TdAdvanceMaturity();
		try {
			BeanUtil.populate(advanceMaturity, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
        if(advanceMaturity.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(advanceMaturity.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		advanceMaturity.setVersion(productApproveMain.getVersion());
        }
    	advanceMaturity.setSponsor(SlSessionHelper.getUserId());
    	advanceMaturity.setSponInst(SlSessionHelper.getInstitutionId());
		advanceMaturity.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		advanceMaturity.setCfType(DictConstants.TrdType.CustomAdvanceMaturity);
		return advanceMaturity;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteAdvanceMaturity(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			advanceMaturityMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	@Override
	public TdAdvanceMaturity ifExistDuration(Map<String, Object> map) {
		return advanceMaturityMapper.ifExistDuration(map);
	}

	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 提起到期核实单号
	 */
	@Override
	public void dealVersionAndCashFlow(String trade_id) {}

	/**
	 * 计算获取应计利息
	 */
	@Override
	public double getAmInt(Map<String, Object> map) {
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(ParameterUtil.getString(map, "refNo", ""))){
        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(map, "refNo", "")));
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double amInt = 0.00;
		String postdate = dateService.getDayendDate();
		/**
		 *	如果是先收息，则查找摊销计划表
		 *  后收息则计算计提差额
		 */
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeBefore))
		{
			map.put("startDate", postdate);
			map.put("endDate", "".equals(ParameterUtil.getString(map, "amDate", postdate))?"":ParameterUtil.getString(map, "amDate", postdate).substring(0, 10));
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("version", productApproveMain.getVersion());
			//替換為后收息的模式20171029
			//List<CashflowAmor> cashflowAmors = cashflowAmorMapper.pageDailyCashflowAmorList(map, new RowBounds(0,99999999)).getResult();
			List<CashflowDailyInterest> cashflowAmors= cashflowDailyInterestMapper.pageDailyInterestList(map);
			for(CashflowDailyInterest cashflowAmor:cashflowAmors) {
				amInt = PlaningTools.add(amInt, cashflowAmor.getInterest());
			}
		}else{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("dealNo", productApproveMain.getDealNo());
			params.put("startDate", postdate);
			params.put("endDate", "".equals(ParameterUtil.getString(map, "amDate", postdate))?"":ParameterUtil.getString(map, "amDate", postdate).substring(0, 10));
			params.put("version", productApproveMain.getVersion());
			amInt = cashflowDailyInterestMapper.selectInterestByDealNoAndDate(params);
		}
		
		//是否减去通道计提
		if("true".equals(ParameterUtil.getString(map, "isFees", "").trim())) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("refNo", ParameterUtil.getString(map, "refNo", ""));
			params.put("refBeginDate", postdate);
			params.put("refEndDate", "".equals(ParameterUtil.getString(map, "amDate", postdate))?"":ParameterUtil.getString(map, "amDate", postdate).substring(0, 10));
			params.put("intCal", "1");
			BigDecimal sumInterest = tdFeesPassAgewayService.getSumInterest(params);
			amInt = PlaningTools.sub(amInt, sumInterest.doubleValue());
		}
		
		return amInt;
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdAdvanceMaturity advanceMaturity = new TdAdvanceMaturity();
		BeanUtil.populate(advanceMaturity, params);
		advanceMaturity = advanceMaturityMapper.searchAdvanceMaturity(advanceMaturity);
		return productApproveService.getProductApproveActivated(advanceMaturity.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdAdvanceMaturity advanceMaturity = new TdAdvanceMaturity();
		BeanUtil.populate(advanceMaturity, params);
		if(advanceMaturity.getDealNo() == null){
			advanceMaturity.setDealNo(ParameterUtil.getString(params, "trade_id", null));
		}
		advanceMaturity = advanceMaturityMapper.searchAdvanceMaturity(advanceMaturity);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(advanceMaturity.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(advanceMaturity.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(advanceMaturity.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomAdvanceMaturity);//利率变更
		tdTrdEvent.setEventTable("TD_ADVANCE_MATURITY");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		TdAdvanceMaturity advanceMaturity = new TdAdvanceMaturity();
		BeanUtil.populate(advanceMaturity, params);
		/**
		 * 根据提前到期的交易数据进行数据清分
		 */
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(advanceMaturity.getRefNo());
		JY.require(productApproveMain!=null, "原交易流水不存在！异常！");
		 
		/**
		 * 得到提前到期交易的起息日，获得满足的还本计划   TD_CASHFLOW_CAPITAL.REPAYMENT_TDATE IS NULL
		 * 找到，那么判断该条计划是否未还本金，是，那么更新期预期换本金额=该条计划原换本金额-提前到期金额；
		 */
		Map<String, Object> paramsMap2 = new HashMap<String, Object>();
		paramsMap2.put("refNo", advanceMaturity.getRefNo());
		List<TdCashflowCapital> capitals = cashflowCapitalMapper.getCaplitalsIsNotRecive(paramsMap2);
		
		TdCashflowCapital tempCashflowCapital = null;
		TdCashflowCapital capital = null;
		if(capitals.size()>0){
			Collections.sort(capitals);//升序
			double remainAmt = advanceMaturity.getAmAmt();
			for(int i=(capitals.size()-1);i>=0;i--)//按最大计划的金额递减
			{
				tempCashflowCapital = capitals.get(i);
				remainAmt = PlaningTools.sub(remainAmt, tempCashflowCapital.getRepaymentSamt());
				if(remainAmt>=0){
					tempCashflowCapital.setRepaymentSamt(0);
					cashflowCapitalMapper.updateByPrimaryKey(tempCashflowCapital);
				}else
				{
					tempCashflowCapital.setRepaymentSamt(Math.abs(remainAmt));
					cashflowCapitalMapper.updateByPrimaryKey(tempCashflowCapital);
					break;
				}				
			}
			
			try {
				capital = new TdCashflowCapital();
				capital.setSeqNumber(capitals.size() > 0 ? capitals.get(capitals.size() - 1).getSeqNumber() + 1 : 1);
				capital.setRepaymentSamt(advanceMaturity.getAmAmt());
				capital.setRepaymentSdate(advanceMaturity.getAmDate());
				capital.setCfType("Principal");
				capital.setPayDirection("Recevie");
				capital.setDealNo(productApproveMain.getDealNo());
				capital.setVersion(productApproveMain.getVersion());
				
				Map<String, Object> paramsMap_1 = new HashMap<String, Object>();
				paramsMap_1.put("dealNo", productApproveMain.getDealNo());
				paramsMap_1.put("repaymentSdate", advanceMaturity.getAmDate());
				List<CashflowCapital> cashflowCapitals = cashflowCapitalMapper.getCapitalList(paramsMap_1);
				
				if(cashflowCapitals.size() > 0){
					capital.setCashflowId(cashflowCapitals.get(0).getCashflowId());
					capital.setRepaymentSamt(PlaningTools.add(capital.getRepaymentSamt(), cashflowCapitals.get(0).getRepaymentSamt()));
					cashflowCapitalMapper.updateByPrimaryKey(capital);
					
				}else{
					capital.setRepaymentTamt(advanceMaturity.getAmAmt());
					capital.setRepaymentTdate(advanceMaturity.getAmDate());
					HashMap<String, Object> params_1 = BeanUtil.beanToHashMap(capital);
					cashflowCapitalMapper.insertAllProperty(params_1);
					
				}
			} catch (Exception e) {
				e.printStackTrace();
				JY.error("保存还本计划出错", e);
			}
		}
		if(StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType()))//前收息
		{
			return;
		}
		//变更相应的利息计划
		//获得界面还本计划 TABLE列表数据
		List<CashflowCapital> cashflowCapitals=cashflowCapitalMapper.getCapitalList(BeanUtil.beanToMap(productApproveMain));
		
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		/**
		 * 筛选出所有的 利率区间
		 */
		List<TdRateRange> ranges = rateRangeMapper.getRateRanges(params);
		InterestRange interestRange = null;
		for(TdRateRange tdRateRange:ranges){
			interestRange = new InterestRange();
			interestRange.setStartDate(tdRateRange.getBeginDate());
			interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(tdRateRange.getEndDate(), Frequency.DAY, -1));
			interestRange.setRate(tdRateRange.getExecRate());
			
			interestRanges.add(interestRange);
		}
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;
		double tempAmt =0f;
		int count =0 ; //将本金计划翻译成本金剩余区间
		Collections.sort(cashflowCapitals);
		HashMap<String, PrincipalInterval> prHashMap = new HashMap<String, PrincipalInterval>();
		String endDate = null;
		for(CashflowCapital tmCashflowCapital : cashflowCapitals){
			endDate = PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1);
			if(prHashMap.get(endDate) != null)
			{
				prHashMap.get(endDate).setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
				
			}else{
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(count==0?productApproveMain.getvDate():cashflowCapitals.get(count-1).getRepaymentSdate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
				principalIntervals.add(principalInterval);

				prHashMap.put(principalInterval.getEndDate(), principalInterval);
			}
			
			tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
			count++;
		}
		//利息明细
		List<CashflowDailyInterest> tdCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		//获取利息计划(没有做过收息的交易)
		List<TdCashflowInterest>  cashflowInterests = cashflowInterestMapper.getInterestListForApprove(BeanUtil.beanToMap(productApproveMain));
		if(cashflowInterests == null || cashflowInterests.size() == 0)
		{
			return;
		}
		Collections.sort(cashflowInterests);
		
		//重新计算通道计划 每日通道计提利息
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
		try {
			String effdate = cashflowInterests.get(0).getRefBeginDate();
			passAwayMap = feesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals, productApproveMain, effdate, "act");
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("重新计算通道计划错误", e);
		}
		List<TdFeesPassAgewayDaily> fAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
		if(passAwayMap != null){
			fAgewayDailies.addAll(passAwayMap.values());
		}
		
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("dealNo", productApproveMain.getDealNo());
		paramsMap.put("version", productApproveMain.getVersion());
		
		//如果是全部提前还款 将未确认的收息计划都设置为已确认 由提前还款收息计划收取
		boolean all = false;
		if(advanceMaturity.getAmAmt() == advanceMaturity.getRemainAmt()){
			all = true;
		}
		
		for(TdCashflowInterest cashflowInterest:cashflowInterests){
			try {
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges, 
						principalIntervals, 
						tdCashflowDailyInterests, 
						cashflowInterest, cashflowInterests.get(0).getRefBeginDate(), 
						DayCountBasis.equal(cashflowInterest.getDayCounter()), fAgewayDailies);
				
				
				if(all){
					cashflowInterest.setActualDate(advanceMaturity.getAmDate());
					cashflowInterest.setActualRamt(cashflowInterest.getInterestAmt());
				}
				
				//更新该条收息计划数据
				cashflowInterestMapper.updateByPrimaryKey(cashflowInterest);
				cashflowInterestMapper.updateCashflowInterestById(cashflowInterest);
				/**
				 * <delete id="deleteCashflowDailyInterest" parameterType="map">
						delete from TD_CASHFLOW_DAILY_INTEREST WHERE 
						DEAL_NO=#{dealNo} AND VERSION=#{version}
						<if test="startDate != null and startDate != ''">
					    	<![CDATA[ and T.REF_BEGIN_DATE>=#{startDate}]]>
					    </if>
					    <if test="endDate != null and endDate != ''">
					    	<![CDATA[ and T.REF_END_DATE<#{endDate}]]>
					    </if>
					</delete>
				 */
				paramsMap.put("startDate", cashflowInterest.getRefBeginDate());
				paramsMap.put("endDate", cashflowInterest.getRefEndDate());
				//删除该区间的每日预计提数据
				cashflowDailyInterestMapper.deleteCashflowDailyInterest(paramsMap);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("重新计算应计利息异常"+e.getMessage());
			}
		}
		if(tdCashflowDailyInterests.size() > 0)
		{
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", tdCashflowDailyInterests);
		}
		//费用计划修改
		cashflowFeeService.updateByHandleCaptitalAmt(BeanUtil.beanToMap(productApproveMain), principalIntervals);
	}
	
	@Override
	public Page<TdAdvanceMaturity> getAdvanceMaturityForDealNoPage(
			Map<String, Object> params) {
		return this.advanceMaturityMapper.getAllAdvanceMaturitysByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}
	
	@Override
	public Integer getDurationUnfinishCount(Map<String,Object> map){
		return advanceMaturityMapper.getDurationUnfinishCount(map);
	}
}

