package com.singlee.capital.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 基金存续期转换表
 * @author SL_LXY
 *
 */
@Entity
@Table(name = "TD_DURATION_FUND_TRANS")
public class TdDurationFundTrans implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//审批单编号 自动生成
	private String prdNo;//产品编号 序列自动生成
	private int version;//版本
	private String sponsor;//审批发起人
	private String sponInst;//审批发起机构
	private String aDate;//审批开始日期
	
	private String outFundCode;//转出基金代码
	private String cNo;//转出基金交易对手
	private String inFundCode;//转入基金代码
	private String opType;//操作标识-4基金转换
	private BigDecimal outUtQty;//转出基金当前份额
	private BigDecimal inUtQty;//转入基金当前份额
	private BigDecimal turnInQty;//转入基金份额
	private BigDecimal turnOutQty;//转出基金份额
	private BigDecimal trunInPrice;//转入基金价格
	private BigDecimal trunOutPrice;//转出基金价格
	private String trunInDate;//转入日期
	private String vDate;//转入生效日期
	private BigDecimal turnInAmt;//转入份额对应金额
	private BigDecimal turnOutAmt;//转出份额对应金额
	private String remark;//备注
	private String approveStatus;//审批单状态
	private String outInvType;//转出基金投资类型
	private String inInvType;//转入基金投资类型
	@Transient
	private String flowType;
	@Transient
	private String taskId;
	@Transient
	private String taskName;
	@Transient
	private String procInst;
	@Transient
	private String executionName;
	@Transient
	private String fundType;//债基类型
	@Transient
	private String cnName;//客户名称
	@Transient
	private String prdName;//产品名称
	@Transient
	private String userName;//用户名称
	@Transient
	private String instName;//机构名称
	@Transient
	private String invType;//会计四分类
	private String ccy;
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getOutFundCode() {
		return outFundCode;
	}
	public void setOutFundCode(String outFundCode) {
		this.outFundCode = outFundCode;
	}
	public String getInFundCode() {
		return inFundCode;
	}
	public void setInFundCode(String inFundCode) {
		this.inFundCode = inFundCode;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public BigDecimal getOutUtQty() {
		return outUtQty;
	}
	public void setOutUtQty(BigDecimal outUtQty) {
		this.outUtQty = outUtQty;
	}
	public BigDecimal getInUtQty() {
		return inUtQty;
	}
	public void setInUtQty(BigDecimal inUtQty) {
		this.inUtQty = inUtQty;
	}
	public BigDecimal getTurnInQty() {
		return turnInQty;
	}
	public void setTurnInQty(BigDecimal turnInQty) {
		this.turnInQty = turnInQty;
	}
	public BigDecimal getTurnOutQty() {
		return turnOutQty;
	}
	public void setTurnOutQty(BigDecimal turnOutQty) {
		this.turnOutQty = turnOutQty;
	}
	public BigDecimal getTrunInPrice() {
		return trunInPrice;
	}
	public void setTrunInPrice(BigDecimal trunInPrice) {
		this.trunInPrice = trunInPrice;
	}
	public BigDecimal getTrunOutPrice() {
		return trunOutPrice;
	}
	public void setTrunOutPrice(BigDecimal trunOutPrice) {
		this.trunOutPrice = trunOutPrice;
	}
	public String getTrunInDate() {
		return trunInDate;
	}
	public void setTrunInDate(String trunInDate) {
		this.trunInDate = trunInDate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public BigDecimal getTurnInAmt() {
		return turnInAmt;
	}
	public void setTurnInAmt(BigDecimal turnInAmt) {
		this.turnInAmt = turnInAmt;
	}
	public BigDecimal getTurnOutAmt() {
		return turnOutAmt;
	}
	public void setTurnOutAmt(BigDecimal turnOutAmt) {
		this.turnOutAmt = turnOutAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getProcInst() {
		return procInst;
	}
	public void setProcInst(String procInst) {
		this.procInst = procInst;
	}
	public String getExecutionName() {
		return executionName;
	}
	public void setExecutionName(String executionName) {
		this.executionName = executionName;
	}
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getOutInvType() {
		return outInvType;
	}
	public void setOutInvType(String outInvType) {
		this.outInvType = outInvType;
	}
	public String getInInvType() {
		return inInvType;
	}
	public void setInInvType(String inInvType) {
		this.inInvType = inInvType;
	}
	
}
