package com.singlee.capital.base.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 消息表
 * @author lxy
 *
 */
@Entity
@Table(name = "TA_MESSAGE")
public class TaMessage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 序号*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TA_MESSAGE.NEXTVAL FROM DUAL")
	private int serNo;
	/** 交易编号 */
	private String dealNo;
	/** 产品编号*/
	private String prdNo;
	/** 审批类型*/
	private String dealType;
	/** 审批状态*/
	private int approveStatus;
	/** 审批发起人*/
	private String sponsor;
	/** 审批发起机构*/
	private String sponInst;
	/** 金额*/
	private BigDecimal amt;
	/** 交易日期*/
	private String dealDate;
	/** 审批开始日期*/
	private String aDate;
	/** 操作类型*/
	private String opType;
	/** 是否已读 0-未读 1-已读*/
	private int isRead;
	/** 是否已处理 0-未处理 1-已处理*/
	private int isProcessed;
	/** 审批处理人*/
	private String approveUser;
	/** 流程ID*/
	private String flowId;
	/** 任务ID*/
	private String taskId;
	/** 最后修改时间*/
	private String lstDate;
	/**产品名称*/
	@Transient
	private String prdName;
	/**流程名称*/
	@Transient
	private String flowName;
	/**任务名称*/
	@Transient
	private String taskName;
	/**流程类型*/
	@Transient
	private String flowType;
	@Transient
	private String vDate;//起息日
	@Transient
	private String mDate;//到期日
	@Transient
	private double contractRate;//投资利率
	@Transient
	private String basis;//计息基础
	@Transient
	private String intFreName;//收益计付频率
	@Transient
	private double enterpCostRate;//企业融资成本_%
	@Transient
	private String spvTypeName;
	@Transient
	private String sponInstName;
	@Transient
	private String sponsorName;
	@Transient
	private String prdNos;
	
	public String getPrdNos() {
		return prdNos;
	}
	public void setPrdNos(String prdNos) {
		this.prdNos = prdNos;
	}
	public String getSponsorName() {
		return sponsorName;
	}
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	public String getSponInstName() {
		return sponInstName;
	}
	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getIntFreName() {
		return intFreName;
	}
	public void setIntFreName(String intFreName) {
		this.intFreName = intFreName;
	}
	public double getEnterpCostRate() {
		return enterpCostRate;
	}
	public void setEnterpCostRate(double enterpCostRate) {
		this.enterpCostRate = enterpCostRate;
	}
	public String getSpvTypeName() {
		return spvTypeName;
	}
	public void setSpvTypeName(String spvTypeName) {
		this.spvTypeName = spvTypeName;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public double getContractRate() {
		return contractRate;
	}
	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}
	public int getSerNo() {
		return serNo;
	}
	public void setSerNo(int serNo) {
		this.serNo = serNo;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public int getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public int getIsRead() {
		return isRead;
	}
	public void setIsRead(int isRead) {
		this.isRead = isRead;
	}
	public int getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(int isProcessed) {
		this.isProcessed = isProcessed;
	}
	public String getApproveUser() {
		return approveUser;
	}
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getLstDate() {
		return lstDate;
	}
	public void setLstDate(String lstDate) {
		this.lstDate = lstDate;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	
	private String prdType;
	private String fundCode;
	private String fundName;
	private double qty;
	private double cashAmt;

	public String getPrdType() {
		return prdType;
	}
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public double getCashAmt() {
		return cashAmt;
	}
	public void setCashAmt(double cashAmt) {
		this.cashAmt = cashAmt;
	}
	
}
