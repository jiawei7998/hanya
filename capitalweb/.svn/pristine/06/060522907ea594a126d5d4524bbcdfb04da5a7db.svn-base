package com.singlee.hrbcap.service.impl;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.hrbcap.service.Ifrs9BookkeepingEngineService;
import com.singlee.hrbcap.util.Ifrs9Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/***
 * 账务模块-回调方法
 */
@Service("hrbAccountCallback")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbAccountCallback {
    @Autowired
    Ifrs9BookkeepingEngineService ifrs9BookkeepingEngineService;
    /***
     * 账务模块审批通过后生成会计分录的方法
     * abeanMap 为当前业务实体类转成的map，另外需要put进一个事件ID表示当前你是出那种账务
     * 例如eventId为VDATE为放款，DINT为每日计提，RINT为收付息，RAMT为还本
     * 出账涉及的维度信息封装在abeanMap
     */
    public String createAcups(Map<String, Object> abeanMap) {
        Map<String, Object> amtMap = new HashMap<String, Object>();
        String 	eventId= ParameterUtil.getString(abeanMap, "eventId", "");
        if(Ifrs9Constants.Ifrs9EventCode.Event_VDATE.equals(eventId)||Ifrs9Constants.Ifrs9EventCode.Event_VERIFY.equals(eventId)){//放款账
            amtMap.put(DurationConstants.AccType.LOAN_AMT, ParameterUtil.getString(abeanMap, "amt", "0"));
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_RINT.equals(eventId)){//收付息
            amtMap.put(DurationConstants.AccType.REV_PROCAMT, ParameterUtil.getString(abeanMap, "amt", "0"));
            amtMap.put(DurationConstants.AccType.INTEREST_AMOR, ParameterUtil.getString(abeanMap, "interestAmt", "0"));
            amtMap.put(DurationConstants.AccType.DIS_INTAMT, ParameterUtil.getString(abeanMap, "disIntAmt", "0"));
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_RAMT.equals(eventId)){//收本
            amtMap.put(DurationConstants.AccType.REV_PROCAMT, ParameterUtil.getString(abeanMap, "amt", "0"));
            amtMap.put(DurationConstants.AccType.REVAL_AMT, ParameterUtil.getString(abeanMap, "revalAmt", "0"));//冲该笔交易的估值余额
        }else if(Ifrs9Constants.Ifrs9EventCode.Event_EINT.equals(eventId)||Ifrs9Constants.Ifrs9EventCode.Event_SDATE.equals(eventId)||Ifrs9Constants.Ifrs9EventCode.Event_SETT.equals(eventId)){//资产转让、到期后收息
            amtMap.put(DurationConstants.AccType.REV_PROCAMT, ParameterUtil.getString(abeanMap, "amt", "0"));
        }
        InstructionPackage instructionPackage = new InstructionPackage();
        instructionPackage.setInfoMap(abeanMap);
        instructionPackage.setAmtMap(amtMap);
        String flowId = ifrs9BookkeepingEngineService.generateEntryIFRS9Entrance(instructionPackage, null);
        return flowId;
    }

    /**
     * 组装账务参数
     * @author
     * @param infoMap
     *        infoMap传的参数（维度参数）：eventId会计事件
     *                       prdNo：产品编号
     *                       instaffiliation记账机构（如果有记账机构，必须传）
     *                       sponinst：交易机构（选传）
     *                       ccy:币种（必填）
     *                       others：如果有科目转换规则表达式，则需要传递表达式中需要的参数【如：key1与key2在map中获取，由key1与key2拼接或转换得到的科目，则需传递key1、key2】
     * @param amtMap  AmtMap参数说明：需传分录对应的金额表达式对应的value
     * @return 分录流水号
     */
    public String packageAcupParams(Map<String, Object> infoMap,Map<String,Object> amtMap) {
        InstructionPackage instructionPackage = new InstructionPackage();
        instructionPackage.setInfoMap(infoMap);
        instructionPackage.setAmtMap(amtMap);
        String flowId = ifrs9BookkeepingEngineService.generateEntryIFRS9Entrance(instructionPackage, null);
        return flowId;
    }

    /**
     * @author guomeijuan 20191025
     * 生成起息日至账务日期的利息帐以及收息帐
     * @param params 出账参数，必须包含单号dealNo、利率rate、计息基础basis、起息日期vdate、到期日期mdate、本金amt以及账务所需的必要的参数
     * @return sysTotalInt 未收利息总和  actTotalInt 已收利息总和
     */
    public Map<String,Object> generatePreInt(Map<String,Object> params) {
        return ifrs9BookkeepingEngineService.generatePreInt(params);
    }
    /**
     * 系统内资金往来倒起息
     * @param params 出账参数，必须包含单号dealNo、利率rate、计息基础basis、起息日期vdate、到期日期mdate、本金amt以及账务所需的必要的参数
     * @return sysTotalInt 未收利息总和  actTotalInt 已收利息总和
     **/
    public Map<String,Object> generatePreXTNInt(Map<String,Object> params) {
        return ifrs9BookkeepingEngineService.generatePreXTNInt(params);
    }
}
