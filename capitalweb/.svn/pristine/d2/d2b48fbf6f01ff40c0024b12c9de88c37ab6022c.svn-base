package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.ifs.model.IfsCurDeposit;
import com.singlee.ifs.model.IfsCurrentDepositOut;
import com.singlee.ifs.service.IfsCurrentDepositOutService;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Controller
@RequestMapping(value = "/CurrentDepositController")
public class CurrentDepositController {

    @Autowired
    IfsCurrentDepositOutService currentDepositOutService;

    /**
     * 分页查询
     */
    @ResponseBody
    @RequestMapping(value="/getCurrentDepositPage")
    public RetMsg<PageInfo<IfsCurDeposit>> getCurrentDepositPage(@RequestBody Map<String,Object> params){
        Page<IfsCurDeposit> currentDepositOutPages = currentDepositOutService.getCurrentDepositOutPages(params);
        return RetMsgHelper.ok(currentDepositOutPages);
    }


    /**
     * 交易导入
     */
    @ResponseBody
    @RequestMapping(value="/importCurrentDeposit", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String importCurrentDeposit(MultipartHttpServletRequest request, HttpServletResponse response){


        // 1.文件获取
        List<UploadedFile> uploadedFileList = null;
        String ret = "导入失败";
        Workbook wb = null;
        List<IfsCurDeposit> depositOuts = new ArrayList<>();
        try {
            uploadedFileList = FileManage.requestExtractor(request);
            // 2.校验并处理成excel
            List<Workbook> excelList = new LinkedList<Workbook>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType())) {
                    return ret = "上传文件类型错误,仅允许上传xls格式文件";
                }
                Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(
                        uploadedFile.getBytes()));
                excelList.add(book);
            }
            if (excelList.size() != 1) {
                return ret = "只能处理一份excel";
            }
            wb = excelList.get(0);
            Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
            String postdate= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
            for (Sheet sheet : sheets) {
                int rowCount = sheet.getRows();// 行数
                String dealType = sheet.getName();
                DecimalFormat instance = (DecimalFormat)NumberFormat.getInstance();

                for (int i = 1; i < rowCount; i++) { // 从第二行开始
                    IfsCurDeposit currentDepositOut = new IfsCurDeposit();
                    if(StringUtils.isBlank(sheet.getCell(1, i).getContents())){
                        continue;
                    }
                    currentDepositOut.setBranch(StringUtils.trimToEmpty(sheet.getCell(1, i).getContents()));//分行名称
                    currentDepositOut.setSubBranch(StringUtils.trimToEmpty(sheet.getCell(2, i).getContents()));//支行名称
                    currentDepositOut.setCustName(StringUtils.trimToEmpty(sheet.getCell(3, i).getContents()));//交易对手总部全称
                    currentDepositOut.setSfn(StringUtils.trimToEmpty(sheet.getCell(4, i).getContents()));//交易对手（全称）
                    currentDepositOut.setCustArea(StringUtils.trimToEmpty(sheet.getCell(5, i).getContents()));//交易对手地域
                    currentDepositOut.setCurFix(StringUtils.trimToEmpty(sheet.getCell(6, i).getContents()));//活期/定期
                    currentDepositOut.setVdate(StringUtils.trimToEmpty(sheet.getCell(7, i).getContents()));//业务开始日期
                    currentDepositOut.setMdate(StringUtils.trimToEmpty(sheet.getCell(8, i).getContents()));//业务结束日期
                    currentDepositOut.setRate(StringUtils.trimToEmpty(sheet.getCell(9, i).getContents()));//利率
                    String amtStr = "".equals(StringUtils.trimToEmpty(sheet.getCell(10, i).getContents())) ? "0":StringUtils.trimToEmpty(sheet.getCell(10, i).getContents());
                    BigDecimal bigDecimal = new BigDecimal(instance.parse(amtStr).toString());
                    currentDepositOut.setAmt(bigDecimal);//余额
                    currentDepositOut.setSubject(StringUtils.trimToEmpty(sheet.getCell(11, i).getContents()));//记帐科目
                    currentDepositOut.setCustType(StringUtils.trimToEmpty(sheet.getCell(12, i).getContents()));//交易对手类型
                    currentDepositOut.setDealType(dealType);
                    depositOuts.add(currentDepositOut);
                }
            }
            //插入存放同业数据
            ret = currentDepositOutService.importCurrentDeposit(depositOuts);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RException(e);
        } finally {
            if (null != wb) {
                wb.close();
            }
        }
        return ret;// 返回前台展示配置
    }


    /**
     * 导出模板
     */
    @ResponseBody
    @RequestMapping("/exportcurrentDepositoutTemplate")
    public void exportcurrentDepositoutTemplate(HttpServletRequest request,HttpServletResponse response){
        OutputStream out = null;
        FileInputStream in = null;

        try {
            String fileName = "存放同业(活期)";
            // 读取模板
            String excelPath = request.getSession().getServletContext().getRealPath("standard/PriceDailyExcel/CurDeposit.xls");

            fileName = URLEncoder.encode(fileName, "UTF-8");

            response.reset();
            // 追加时间
            response.addHeader("Content-Disposition", "attachment;filename="
                    + fileName + ".xls");
            response.setContentType("application/octet-stream;charset=UTF-8");

            out = response.getOutputStream();
            in = new FileInputStream(excelPath);

            byte[] b = new byte[1024];
            int len;

            while ((len = in.read(b)) > 0) {
                response.getOutputStream().write(b, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                out = null;
            }
        }
    }

}
