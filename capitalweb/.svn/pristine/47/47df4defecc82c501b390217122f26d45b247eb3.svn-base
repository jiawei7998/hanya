package com.singlee.hrbnewport.model;

import java.math.BigDecimal;

/**
    * 本币交易情况表
    */
public class TbRmbDeal {
    /**
    * 交易类
    */
    private String accountType;

    /**
    * 业务类型
    */
    private String businessType;

    /**
    * 组合类型
    */
    private String combinationType;

    /**
    * 交易类型
    */
    private String dealType;
    /**
     * 交割日期
     */
    private String dealdate;

    /**
    * 买入交易笔数
    */
    private BigDecimal buyCount;

    /**
    * 卖出交易笔数
    */
    private BigDecimal sellCount;

    /**
    * 总交易笔数
    */
    private BigDecimal dealCount;

    /**
    * 单笔金额大于10亿小于等于15亿
    */
    private BigDecimal buyThanTen;

    /**
    * 大于15亿
    */
    private BigDecimal buyThanFifteen;

    /**
    * 买入交易量(万元)
    */
    private BigDecimal buyTotal;

    /**
    * 卖出交易量(万元)
    */
    private BigDecimal sellTotal;

    /**
    * 总交易量(万元)
    */
    private BigDecimal total;


    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCombinationType() {
        return combinationType;
    }

    public void setCombinationType(String combinationType) {
        this.combinationType = combinationType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealdate() {
        return dealdate;
    }

    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }

    public BigDecimal getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(BigDecimal buyCount) {
        this.buyCount = buyCount;
    }

    public BigDecimal getSellCount() {
        return sellCount;
    }

    public void setSellCount(BigDecimal sellCount) {
        this.sellCount = sellCount;
    }

    public BigDecimal getDealCount() {
        return dealCount;
    }

    public void setDealCount(BigDecimal dealCount) {
        this.dealCount = dealCount;
    }

    public BigDecimal getBuyThanTen() {
        return buyThanTen;
    }

    public void setBuyThanTen(BigDecimal buyThanTen) {
        this.buyThanTen = buyThanTen;
    }

    public BigDecimal getBuyThanFifteen() {
        return buyThanFifteen;
    }

    public void setBuyThanFifteen(BigDecimal buyThanFifteen) {
        this.buyThanFifteen = buyThanFifteen;
    }

    public BigDecimal getBuyTotal() {
        return buyTotal;
    }

    public void setBuyTotal(BigDecimal buyTotal) {
        this.buyTotal = buyTotal;
    }

    public BigDecimal getSellTotal() {
        return sellTotal;
    }

    public void setSellTotal(BigDecimal sellTotal) {
        this.sellTotal = sellTotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", accountType=").append(accountType);
        sb.append(", businessType=").append(businessType);
        sb.append(", combinationType=").append(combinationType);
        sb.append(", dealType=").append(dealType);
        sb.append(", dealdate=").append(dealdate);
        sb.append(", buyCount=").append(buyCount);
        sb.append(", sellCount=").append(sellCount);
        sb.append(", dealCount=").append(dealCount);
        sb.append(", buyThanTen=").append(buyThanTen);
        sb.append(", buyThanFifteen=").append(buyThanFifteen);
        sb.append(", buyTotal=").append(buyTotal);
        sb.append(", sellTotal=").append(sellTotal);
        sb.append(", total=").append(total);
        sb.append("]");
        return sb.toString();
    }
}