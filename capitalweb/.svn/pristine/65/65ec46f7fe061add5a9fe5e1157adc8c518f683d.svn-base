<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.singlee.ifs.mapper.IfsRevIotdMapper" >
    <resultMap id="BaseResultMap" type="com.singlee.ifs.model.IfsRevIotd" >
        <id column="TICKET_ID" property="ticketId" jdbcType="VARCHAR" />
        <result column="DEAL_TYPE" property="dealType" jdbcType="VARCHAR" />
        <result column="ADATE" property="adate" jdbcType="VARCHAR" />
        <result column="SPONSOR" property="sponsor" jdbcType="VARCHAR" />
        <result column="CHECKER" property="checker" jdbcType="VARCHAR" />
        <result column="BR" property="br" jdbcType="VARCHAR" />
        <result column="FEDEALNO" property="fedealno" jdbcType="VARCHAR" />
        <result column="SEQ" property="seq" jdbcType="VARCHAR" />
        <result column="INOUTIND" property="inoutind" jdbcType="VARCHAR" />
        <result column="SERVER" property="server" jdbcType="VARCHAR" />
        <result column="DEALNO" property="dealNo" jdbcType="VARCHAR" />
        <result column="REVREASON" property="revreason" jdbcType="VARCHAR" />
        
        <result column="SPON_INST"      property="sponInst" jdbcType="VARCHAR" />
        <result column="SPON_DATE"      property="sponDate" jdbcType="VARCHAR" />
        <result column="APPROVE_STATUS" property="approveStatus" jdbcType="VARCHAR" />
        <result column="TASK_ID"        property="taskId"   jdbcType="VARCHAR"/>
        <result column="INST_NAME" property="instName" jdbcType="VARCHAR"/>
    </resultMap>
    
    <sql id="join">
    	<!-- 审批发起人   -->
		LEFT OUTER JOIN TA_USER U
		  ON ${alias}.SPONSOR = U.USER_ID
		<!-- 审批发起机构    -->
		LEFT OUTER JOIN TT_INSTITUTION I
		  ON ${alias}.SPON_INST = I.INST_ID
	</sql>

    <sql id="Base_Column_List" >
        ${alias}.TICKET_ID, ${alias}.DEAL_TYPE, ${alias}.ADATE, ${alias}.SPONSOR, ${alias}.CHECKER, ${alias}.BR, ${alias}.FEDEALNO, ${alias}.SEQ, ${alias}.INOUTIND, 
        ${alias}.SERVER, ${alias}.DEALNO, ${alias}.REVREASON, ${alias}.SPON_INST, ${alias}.SPON_DATE, ${alias}.APPROVE_STATUS,I.INST_NAME
    </sql>

    <select id="searchPageOptParam" resultMap="BaseResultMap" parameterType="map" >
        SELECT 
        <include refid="Base_Column_List"><property name="alias" value="T"/></include>
        FROM IFS_REV_IOTD T
        <where>
			<if test="ticketId != null and ticketId != '' ">
			   AND T.TICKET_ID LIKE '%${ticketId}%'
			</if>
			<if test="dealno != null and dealno != '' ">
			   AND T.DEALNO LIKE '%${dealno}%'
			</if>
		</where>
		ORDER BY T.TICKET_ID DESC
    </select>
    
    <select id="getId" resultType="java.lang.String">
		SELECT 'OP'||SEQ_IFS_REV_IOTD.NEXTVAL FROM DUAL
	</select>

    <delete id="deleteById" parameterType="java.lang.String" >
        delete from IFS_REV_IOTD
        where TICKET_ID = #{ticketId}
    </delete>

    <insert id="insert" parameterType="com.singlee.ifs.model.IfsRevIotd" >
        insert into IFS_REV_IOTD (TICKET_ID, DEAL_TYPE, ADATE, 
            SPONSOR, CHECKER, 
            BR, FEDEALNO, SEQ, 
            INOUTIND, SERVER, DEALNO, 
            REVREASON,
             SPON_INST, SPON_DATE, APPROVE_STATUS)
        values (#{ticketId,jdbcType=VARCHAR}, #{dealType,jdbcType=VARCHAR}, #{adate,jdbcType=VARCHAR}, 
            #{sponsor,jdbcType=VARCHAR}, #{checker,jdbcType=VARCHAR}, 
            #{br,jdbcType=VARCHAR}, #{fedealno,jdbcType=VARCHAR}, #{seq,jdbcType=VARCHAR}, 
            #{inoutind,jdbcType=VARCHAR}, #{server,jdbcType=VARCHAR}, #{dealNo,jdbcType=VARCHAR}, 
            #{revreason,jdbcType=VARCHAR}, #{sponInst,jdbcType=VARCHAR}, #{sponDate,jdbcType=VARCHAR}, #{approveStatus,jdbcType=VARCHAR})
    </insert>
    
     <update id="updateById" parameterType="com.singlee.ifs.model.IfsRevIotd" >
        update IFS_REV_IOTD
        set DEAL_TYPE = #{dealType,jdbcType=VARCHAR},
            ADATE = #{adate,jdbcType=VARCHAR},
            SPONSOR = #{sponsor,jdbcType=VARCHAR},
            CHECKER = #{checker,jdbcType=VARCHAR},
            BR = #{br,jdbcType=VARCHAR},
            FEDEALNO = #{fedealno,jdbcType=VARCHAR},
            SEQ = #{seq,jdbcType=VARCHAR},
            INOUTIND = #{inoutind,jdbcType=VARCHAR},
            SERVER = #{server,jdbcType=VARCHAR},
            DEALNO = #{dealNo,jdbcType=VARCHAR},
            REVREASON = #{revreason,jdbcType=VARCHAR},
            SPON_INST = #{sponInst},
            SPON_DATE = #{sponDate},  
            APPROVE_STATUS = #{approveStatus}
        where TICKET_ID = #{ticketId,jdbcType=VARCHAR}
    </update>
  
    <!-- 我发起的 -->
    <select id="getRevIotdMine" parameterType="map" resultMap="BaseResultMap">
    	SELECT 
			<include refid="Base_Column_List"><property name="alias" value="T"/></include>
		FROM IFS_REV_IOTD T  
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="ticketId != null and ticketId != '' ">
			   AND T.TICKET_ID LIKE '%${ticketId}%'
			</if>
			<if test="dealno != null and dealno != '' ">
			   AND T.DEALNO LIKE '%${dealno}%'
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
				AND I.BRANCH_ID = #{branchId}
			</if>
			<if test="approveStatus != null and approveStatus != ''">
				AND T.APPROVE_STATUS = #{approveStatus}
			</if>
			<if test="userId != null and userId != ''">
				AND T.SPONSOR = #{userId}
			</if>
		</where>
		ORDER BY  T.TICKET_ID DESC
    </select>
    
    <!-- 待审批 -->
    <select id="getRevIotdUnfinished" parameterType="map" resultMap="BaseResultMap">
    	SELECT 
			<include refid="Base_Column_List"><property name="alias" value="T"/></include>
			,task.task_id
		FROM
		   	(<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) task
		     INNER JOIN IFS_REV_IOTD T ON task.serial_no = T.TICKET_ID 
		  	<include refid="join"><property name="alias" value="T"/></include>
  		<where>
  			<if test="ticketId != null and ticketId != '' ">
			   AND T.TICKET_ID LIKE '%${ticketId}%'
			</if>
			<if test="dealno != null and dealno != '' ">
			   AND T.DEALNO LIKE '%${dealno}%'
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
				AND I.BRANCH_ID = #{branchId}
			</if>
			<if test="approveStatus != null and approveStatus != ''">
				AND T.APPROVE_STATUS = #{approveStatus}
			</if>
			<if test="approveStatusNo != null and approveStatusNo !=''">
				AND T.APPROVE_STATUS NOT IN
				<foreach item="item" index="index" collection="approveStatusNo"
					open="(" separator="," close=")">
					#{item}
				</foreach>
			</if>
		</where>
		ORDER BY  T.TICKET_ID DESC
    </select>
   
   <!-- 已审批 -->
   <select id="getRevIotdFinished" parameterType="map" resultMap="BaseResultMap">
   		SELECT 
			<include refid="Base_Column_List"><property name="alias" value="T"/></include>
		  FROM  (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_completed_task"></include>) U2
		  INNER JOIN IFS_REV_IOTD T ON U2.serial_no = T.TICKET_ID
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="ticketId != null and ticketId != '' ">
			   AND T.TICKET_ID LIKE '%${ticketId}%'
			</if>
			<if test="dealno != null and dealno != '' ">
			   AND T.DEALNO LIKE '%${dealno}%'
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
				AND I.BRANCH_ID = #{branchId}
			</if>
			<if test="approveStatus != null and approveStatus != ''">
				AND T.APPROVE_STATUS = #{approveStatus}
			</if>
			<if test="userId != null and userId != ''">
				AND U2.USER_ID = #{userId}
			</if>
			
			AND U2.USER_ID != T.SPONSOR
			
			<if test="approveStatusNo != null and approveStatusNo !=''">
				AND T.APPROVE_STATUS IN
				<foreach item="item" index="index" collection="approveStatusNo"
					open="(" separator="," close=")">
					#{item}
				</foreach>
			</if>
		</where>
		ORDER BY  T.TICKET_ID DESC
   </select>
   
   <!-- 根据id修改  审批状态 、审批发起时间   -->
   <update id="updateIotdStatusByID">
   	update IFS_REV_IOTD set APPROVE_STATUS=#{1},SPON_DATE=#{2} where TICKET_ID=#{0}
   </update> 
   
   <select id="searchById" parameterType="map" resultMap="BaseResultMap">
		SELECT 
		<include refid="Base_Column_List"><property name="alias" value="T"/></include>
		from IFS_REV_IOTD T
		<include refid="join"><property name="alias" value="T"/></include>
		<where>
	    	<if test="ticketId !='' and ticketId!=null ">
				AND T.TICKET_ID=#{ticketId}
			</if>
			<if test="branchId != null and branchId != ''">
					AND U.BRANCH_ID = #{branchId}
					AND I.BRANCH_ID = #{branchId}
			</if>
	    </where>
	</select> 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
</mapper>