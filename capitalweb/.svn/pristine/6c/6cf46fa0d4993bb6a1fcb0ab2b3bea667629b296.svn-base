package com.singlee.rules.test.airco;

import com.singlee.rules.api.Facts;
import com.singlee.rules.api.Rule;
import com.singlee.rules.api.Rules;
import com.singlee.rules.api.RulesEngine;
import com.singlee.rules.core.InferenceRulesEngine;
import com.singlee.rules.core.RuleBuilder;

import static com.singlee.rules.test.airco.DecreaseTemperatureAction.decreaseTemperature;
import static com.singlee.rules.test.airco.HighTemperatureCondition.itIsHot;

public class Launcher {

    public static void main(String[] args) {
        // define facts
        Facts facts = new Facts();
        facts.put("temperature", 30);

        // define rules
        Rule airConditioningRule = new RuleBuilder()
                .name("air conditioning rule")
                .when(itIsHot())
                .then(decreaseTemperature())
                .build();
        Rules rules = new Rules();
        rules.register(airConditioningRule);

        // fire rules on known facts
        RulesEngine rulesEngine = new InferenceRulesEngine();
        rulesEngine.fire(rules, facts);
    }

}