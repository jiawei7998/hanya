package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 货币掉期正式交易审批Controller
 * 
 * ClassName: IfsCurrencyController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午07:29:06 <br/>
 * 
 * @author huangx
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping("IfsCurrencyController")
public class IfsCurrencyController {
	@Autowired
	private IFSService iFSService;

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcySwapPage")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchCcySwapPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxCswap> page = iFSService.searchCcySwapPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcySwap")
	public RetMsg<Serializable> deleteCcySwap(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteCcySwap(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcySwap")
	public String addCcySwap(@RequestBody IfsCfetsfxCswap map) {
		String message = iFSService.addCcySwap(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcySwap")
	public RetMsg<Serializable> editCcySwap(@RequestBody IfsCfetsfxCswap map) {
		iFSService.editCcySwap(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcySwap")
	public RetMsg<IfsCfetsfxCswap> searchCcySwap(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetsfxCswap ifsCfetsfxCswap = iFSService.searchCcySwap(ticketid);
		return RetMsgHelper.ok(ifsCfetsfxCswap);
	}

	/**
	 * 根据主键查询成交单(返回值增加 流程id)
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCcySwapAndFlowIdById")
	public RetMsg<IfsCfetsfxCswap> searchCcySwapAndFlowIdById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxCswap ccySwap = iFSService.getCcySwapAndFlowIdById(key);
		return RetMsgHelper.ok(ccySwap);
	}

	/**
	 * 货币掉期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapMine")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchPageCcySwapMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxCswap> page = iFSService.getCcySwapMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchPageCcySwapUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxCswap> page = iFSService.getCcySwapMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 货币掉期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcySwapFinished")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchPageCcySwapFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxCswap> page = iFSService.getCcySwapMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxCswapPassed")
	public RetMsg<PageInfo<IfsCfetsfxCswap>> searchPageApprovefxCswapPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxCswap> page = iFSService.getApprovefxCswapPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外币拆借
	 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyLendingPage")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchCcyLendingPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxLend> page = iFSService.searchCcyLendingPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcyLending")
	public RetMsg<Serializable> deleteCcyLending(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteCcyLending(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcyLending")
	public RetMsg<Serializable> addCcyLending(@RequestBody IfsCfetsfxLend map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		message = iFSService.addCcyLending(map);
		return StringUtil.containsIgnoreCase(message, "保存成功")?RetMsgHelper.ok(message):RetMsgHelper.fail(message);
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcyLending")
	public RetMsg<Serializable> editCcyLending(@RequestBody IfsCfetsfxLend map) {
		String message = iFSService.checkCust(map.getDealTransType(),map.getCounterpartyInstId(),null,map.getCfetscn(), map.getProduct(), map.getProdType());
		if(StringUtil.isNotEmpty(message)) {
			return RetMsgHelper.fail(message);
		}
		iFSService.editCcyLending(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyLending")
	public RetMsg<IfsCfetsfxLend> searchCcyLending(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetsfxLend ifsCfetsfxLend = iFSService.searchCcyLending(ticketid);
		return RetMsgHelper.ok(ifsCfetsfxLend);
	}

	/**
	 * 外币拆借-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingMine")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchPageCcyLendingMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxLend> page = iFSService.getCcyLendingMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币拆借-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchPageCcyLendingUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxLend> page = iFSService.getCcyLendingMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币拆借-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyLendingFinished")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchPageCcyLendingFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxLend> page = iFSService.getCcyLendingMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxLendPassed")
	public RetMsg<PageInfo<IfsCfetsfxLend>> searchPageApprovefxLendPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxLend> page = iFSService.getApprovefxLendPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外汇对即期
	 */

	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyOnSpotPage")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchCcyOnSpotPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxOnspot> page = iFSService.searchCcyOnSpotPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteCcyOnSpot")
	public RetMsg<Serializable> deleteCcyOnSpot(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteCcyOnSpot(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addCcyOnSpot")
	public String addCcyOnSpot(@RequestBody IfsCfetsfxOnspot map) {
		String message = iFSService.addCcyOnSpot(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editCcyOnSpot")
	public RetMsg<Serializable> editCcyOnSpot(@RequestBody IfsCfetsfxOnspot map) {
		iFSService.editCcyOnSpot(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchCcyOnSpot")
	public RetMsg<IfsCfetsfxOnspot> searchCcyOnSpot(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetsfxOnspot ifsCfetsfxOnspot = iFSService.searchCcyOnSpot(ticketid);
		return RetMsgHelper.ok(ifsCfetsfxOnspot);
	}

	/**
	 * 外汇对即期-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotMine")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchPageCcyOnSpotMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOnspot> page = iFSService.getCcyOnSpotMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外汇对即期-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchPageCcyOnSpotUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOnspot> page = iFSService.getCcyOnSpotMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外汇对即期-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCcyOnSpotFinished")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchPageCcyOnSpotFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxOnspot> page = iFSService.getCcyOnSpotMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxAllotPassed")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchPageApprovefxAllotPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxAllot> page = iFSService.getApprovefxAllotPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外币头寸调拨
	 */
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsAllocatePage")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchIfsAllocatePage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxAllot> page = iFSService.searchIfsAllocatePage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteIfsAllocate")
	public RetMsg<Serializable> deleteIfsAllocate(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteIfsAllocate(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addIfsAllocate")
	public String addIfsAllocate(@RequestBody IfsCfetsfxAllot map) {
		String message = iFSService.addIfsAllocate(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editIfsAllocate")
	public RetMsg<Serializable> editIfsAllocate(@RequestBody IfsCfetsfxAllot map) {
		iFSService.editIfsAllocate(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsAllocate")
	public RetMsg<IfsCfetsfxAllot> searchIfsAllocate(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetsfxAllot ifsCfetsfxAllot = iFSService.searchIfsAllocate(ticketid);
		return RetMsgHelper.ok(ifsCfetsfxAllot);
	}

	/**
	 * 外币头寸调拨-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateMine")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchPageAllocateMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxAllot> page = iFSService.getAllocateMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币头寸调拨-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchPageAllocateUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxAllot> page = iFSService.getAllocateMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币头寸调拨-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAllocateFinished")
	public RetMsg<PageInfo<IfsCfetsfxAllot>> searchPageAllocateFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxAllot> page = iFSService.getAllocateMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApproveOnspotPassed")
	public RetMsg<PageInfo<IfsCfetsfxOnspot>> searchPageApproveOnspotPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxOnspot> page = iFSService.getApproveOnspotPassedPage(params);
		return RetMsgHelper.ok(page);
	}

	/*
	 * 外币债
	 */
	// 分页查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsDebtPage")
	public RetMsg<PageInfo<IfsCfetsfxDebt>> searchIfsDebtPage(@RequestBody Map<String, Object> map) {
		Page<IfsCfetsfxDebt> page = iFSService.searchIfsDebtPage(map);
		return RetMsgHelper.ok(page);
	}

	// 删除
	@ResponseBody
	@RequestMapping(value = "/deleteIfsDebt")
	public RetMsg<Serializable> deleteIfsDebt(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketid").toString();
		iFSService.deleteIfsDebt(ticketid);
		return RetMsgHelper.ok();
	}

	// 新增
	@ResponseBody
	@RequestMapping(value = "/addIfsDebt")
	public String addIfsDebt(@RequestBody IfsCfetsfxDebt map) {
		String message = iFSService.addIfsDebt(map);
		return message;
	}

	// 修改
	@ResponseBody
	@RequestMapping(value = "/editIfsDebt")
	public RetMsg<Serializable> editIfsDebt(@RequestBody IfsCfetsfxDebt map) {
		iFSService.editIfsDebt(map);
		return RetMsgHelper.ok();
	}

	// 查询
	@ResponseBody
	@RequestMapping(value = "/searchIfsDebt")
	public RetMsg<IfsCfetsfxDebt> searchIfsDebt(@RequestBody Map<String, Object> map) {
		String ticketid = map.get("ticketId").toString();
		IfsCfetsfxDebt ifsCfetsfxDebt = iFSService.searchIfsDebt(ticketid);
		return RetMsgHelper.ok(ifsCfetsfxDebt);
	}

	/**
	 * 外币债-查询我发起的
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtMine")
	public RetMsg<PageInfo<IfsCfetsfxDebt>> searchPageIfsDebtMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxDebt> page = iFSService.getDebtMinePage(params, 3);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币债-查询 待审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtUnfinished")
	public RetMsg<PageInfo<IfsCfetsfxDebt>> searchPageIfsDebtUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxDebt> page = iFSService.getDebtMinePage(params, 1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 外币债-查询 已审批
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2018-3-23
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIfsDebtFinished")
	public RetMsg<PageInfo<IfsCfetsfxDebt>> searchPageIfsDebtFinished(@RequestBody Map<String, Object> params) {
		String approveStatusNo = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatusNo)) {
			approveStatusNo = DictConstants.ApproveStatus.ApprovedPass;
			approveStatusNo += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatusNo += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatusNo += "," + DictConstants.ApproveStatus.Verified;
			approveStatusNo += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatusNo += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatusNo.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsCfetsfxDebt> page = iFSService.getDebtMinePage(params, 2);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据主键关联流程查询成交单
	 * 
	 * @param Key
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchCurrencyAndFlowById")
	public RetMsg<IfsCfetsfxDebt> searchCurrencyAndFlowById(@RequestBody Map<String, Object> key) {
		IfsCfetsfxDebt gold = iFSService.getDebtAndFlowById(key);
		return RetMsgHelper.ok(gold);
	}

	/**
	 * 勾兑时，查询表中审批通过的数据
	 * 
	 * @param
	 * 
	 * */
	@ResponseBody
	@RequestMapping(value = "/searchPageApprovefxDebtPassed")
	public RetMsg<PageInfo<IfsCfetsfxDebt>> searchPageApprovefxDebtPassed(@RequestBody Map<String, Object> params) {
		Page<IfsCfetsfxDebt> page = iFSService.getApprovefxDebtPassedPage(params);
		return RetMsgHelper.ok(page);
	}
	// 修改
	@ResponseBody
	@RequestMapping(value = "/rebackData")
	public RetMsg<Serializable> rebackData(@RequestBody Map<String, Object> params) {
		iFSService.rebackData(params);
		return RetMsgHelper.ok();
	}
}
