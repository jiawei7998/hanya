package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 额度交易操作日志表
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_ED_DEAL_LOG")
public class TdEdDealLog extends EdInParams implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_ED_CUST_LOG_ID.NEXTVAL FROM DUAL")
	private String dealLogId;
	
	public String getDealLogId() {
		return dealLogId;
	}
	public void setDealLogId(String dealLogId) {
		this.dealLogId = dealLogId;
	}
	
	private int seq;//序号
	
	private String batchId;//额度操作批次
	
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}

	/**
	 * 额度唯一主键
	 */
	private String custCreditId;
	/**
	 * 额度中类
	 */
	private String creditId;
	/**
	 * 额度期限
	 */
	private String termId;
	/**
	 * 期限类型
	 */
	private String termType;
	/**
	 * 风险级别
	 */
	private int riskLevel;
	
	/**
	 * 优先级
	 */
	private String propertyL;
	/**
	 * 根据额度期限，类型，对年对月对日 时刻到期日
	 */
	private String edMdate;
	/**
	 * ED_MDATE-M_DATE 天数
	 */
	private int edMdays;
	/**
	 * 当前系统时间
	 */
	private String postDate;
	/**
	 * 额度到期日
	 */
	private String creditMdate;
	/**
	 * CREDIT_MDATE-系统时间 天数
	 */
	private int creditMdays;
	/**
	 * 操作时点可用额度
	 */
	private double edAvlamt;
	/**
	 * 额度操作类型
	 */
	private String edOpType;
	/**
	 * 操作时点
	 */
	private String edOpTime;
	/**
	 * 额度操作金额
	 */
	private double edOpAmt;
	
	/**
	 * 操作时点可用额度不占用权重
	 */
	private double edAvlamtAll;
	/**
	 * 操作时点可用额度不占用权重
	 */
	private double edOpAmtAll;
	
	/**
	 * 客户名称
	 */
	@Transient
	private String cnName;
	/**
	 * 额度中类
	 */
	@Transient
	private String creditName;
	/**
	 * 产品名称
	 */
	@Transient
	private String prdName;
	
	/**
	 * 客户可用授信额度总额
	 */
	private double avlAmtEcif;
	/**
	 * 授信产品可用授信额度
	 */
	private double avlAmtCredit;
	/**
	 * 授信产品可用授信额度总额
	 */
	private double loanAmtEcif;
	/**
	 * 授信产品可用额度总额
	 */
	private double loanAmtCredit;
	
	private String isActive;
	
	//是否新发起交易
	@Transient
	private String isNewDeal;
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType;
	}
	public int getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}
	
	public String getPropertyL() {
		return propertyL;
	}
	public void setPropertyL(String propertyL) {
		this.propertyL = propertyL;
	}
	public String getEdMdate() {
		return edMdate;
	}
	public void setEdMdate(String edMdate) {
		this.edMdate = edMdate;
	}
	public int getEdMdays() {
		return edMdays;
	}
	public void setEdMdays(int edMdays) {
		this.edMdays = edMdays;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getCreditMdate() {
		return creditMdate;
	}
	public void setCreditMdate(String creditMdate) {
		this.creditMdate = creditMdate;
	}
	
	public int getCreditMdays() {
		return creditMdays;
	}
	public void setCreditMdays(int creditMdays) {
		this.creditMdays = creditMdays;
	}
	public double getEdAvlamt() {
		return edAvlamt;
	}
	public void setEdAvlamt(double edAvlamt) {
		this.edAvlamt = edAvlamt;
	}
	public String getEdOpType() {
		return edOpType;
	}
	public void setEdOpType(String edOpType) {
		this.edOpType = edOpType;
	}
	public String getEdOpTime() {
		return edOpTime;
	}
	public void setEdOpTime(String edOpTime) {
		this.edOpTime = edOpTime;
	}
	public double getEdOpAmt() {
		return edOpAmt;
	}
	public void setEdOpAmt(double edOpAmt) {
		this.edOpAmt = edOpAmt;
	}

	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
	
	@Transient
	private String sponName;
	@Transient
	private String sponInst;

	
	public String getSponName() {
		return sponName;
	}
	public void setSponName(String sponName) {
		this.sponName = sponName;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public double getEdAvlamtAll() {
		return edAvlamtAll;
	}
	public void setEdAvlamtAll(double edAvlamtAll) {
		this.edAvlamtAll = edAvlamtAll;
	}
	public double getEdOpAmtAll() {
		return edOpAmtAll;
	}
	public void setEdOpAmtAll(double edOpAmtAll) {
		this.edOpAmtAll = edOpAmtAll;
	}
	@Override
	public String getIsNewDeal() {
		return isNewDeal;
	}
	@Override
	public void setIsNewDeal(String isNewDeal) {
		this.isNewDeal = isNewDeal;
	}
	public double getAvlAmtEcif() {
		return avlAmtEcif;
	}
	public void setAvlAmtEcif(double avlAmtEcif) {
		this.avlAmtEcif = avlAmtEcif;
	}
	public double getAvlAmtCredit() {
		return avlAmtCredit;
	}
	public void setAvlAmtCredit(double avlAmtCredit) {
		this.avlAmtCredit = avlAmtCredit;
	}
	public double getLoanAmtEcif() {
		return loanAmtEcif;
	}
	public void setLoanAmtEcif(double loanAmtEcif) {
		this.loanAmtEcif = loanAmtEcif;
	}
	public double getLoanAmtCredit() {
		return loanAmtCredit;
	}
	public void setLoanAmtCredit(double loanAmtCredit) {
		this.loanAmtCredit = loanAmtCredit;
	}
	
	
}
