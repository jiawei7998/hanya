package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBalance;

public interface TdBalanceMapper extends Mapper<TdBalance>{
	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdBalance getAdvanceMaturityById(String dealNo);
	
	
	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdBalance getAdvanceMaturityByMap(Map<String, Object> params);
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdBalance> getBalanceList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdBalance> getBalanceListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdBalance> getBalanceListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param advanceMaturity
	 * @return
	 */
	public TdBalance searchAdvanceMaturity(TdBalance advanceMaturity);
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	public TdBalance ifExistDuration(Map<String,Object> map);
	/**
	 * 获取提前到期数据 供账务使用
	 * @param map
	 * @return
	 */
	public List<TdBalance> getAdvanceMaturitiesForAcc(Map<String,Object> map);

	public Page<TdBalance> getTdBalance(Map<String, Object> params, RowBounds rowBounds);
	

}
