<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.trade.mapper.TdRateChangeMapper">
	<sql id="columns">
		${alias}.DEAL_NO,
		${alias}.REF_NO,
		${alias}.DEAL_TYPE,
		CASE 
        	WHEN TTO.ORDER_STATUS IS NULL THEN TTT.TRADE_STATUS
        	WHEN TTT.TRADE_STATUS IS NULL THEN TTO.ORDER_STATUS
        	ELSE NULL
       	END AS APPROVE_STATUS,
		${alias}.SPONSOR,
		U.USER_NAME,
		${alias}.SPON_INST,
		I.INST_NAME,
		${alias}.A_DATE,
		${alias}.DEAL_DATE,
		${alias}.RATE,
		${alias}.CHANGERATE,
		${alias}.CHANGEDATE,
		${alias}.CHANGEREASON,
		${alias}.EFFECTDATE,
		${alias}.LAST_UPDATE,
		${alias}.ADJINTAMT,
		${alias}.VERSION
	</sql>
	
	<sql id="join">
		LEFT OUTER JOIN TA_USER U
		  ON ${alias}.SPONSOR = U.USER_ID
		LEFT OUTER JOIN TT_INSTITUTION I
		  ON ${alias}.SPON_INST = I.INST_ID
		LEFT OUTER JOIN TT_TRD_ORDER TTO
		  ON ${alias}.DEAL_NO = TTO.ORDER_ID
		LEFT OUTER JOIN TT_TRD_TRADE TTT
		  ON ${alias}.DEAL_NO = TTT.TRADE_ID
	</sql>
	
	<resultMap id="rateChnageMap" type="com.singlee.capital.trade.model.TdRateChange">
		<id property="dealNo" column="DEAL_NO" />
		<result property="refNo" column="REF_NO" />
		<result property="dealType" column="DEAL_TYPE" />
		<result property="approveStatus" column="APPROVE_STATUS"/>
		<result property="sponsor" column="SPONSOR" />
		<result property="sponInst" column="SPON_INST" />
		<result property="aDate" column="A_DATE" />
		<result property="dealDate" column="DEAL_DATE" />
		<result property="rate" column="RATE" />
		<result property="changeRate" column="CHANGERATE" />
		<result property="changeDate" column="CHANGEDATE" />
		<result property="changeReason" column="CHANGEREASON" />
		<result property="effectDate" column="EFFECTDATE" />
		<result property="lastUpdate" column="LAST_UPDATE" />
		<result property="adjintamt" column="ADJINTAMT" />
		<result property="version" column="VERSION" />
		<result property="taskId" column="TASK_ID" />
		<association property="user" javaType="com.singlee.capital.system.model.TaUser">
			<id property="userId" column="SPONSOR" />
			<result property="userName" column="USER_NAME" />
		</association>
		<association property="institution" javaType="com.singlee.capital.system.model.TtInstitution">
			<id property="instId" column="SPON_INST" />
			<result property="instName" column="INST_NAME" />
		</association>
	</resultMap>

	<select id="getRateChangeById" parameterType="String" resultMap="rateChnageMap">
		SELECT
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_RATE_CHANGE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		 WHERE DEAL_NO = #{dealNo}
	</select>

	<select id="getRateChangeList" parameterType="map" resultMap="rateChnageMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
			,null as task_id
		  FROM TD_RATE_CHANGE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="RCStatus != null">
				AND (TTO.ORDER_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR
				TTT.TRADE_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
			<if test="userId != null and userId != ''">
				AND T.SPONSOR = #{userId}
			</if>
		</where>
		union all
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
			,task.task_id
		  FROM
		   (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) task
		    INNER JOIN TD_RATE_CHANGE T ON task.serial_no = T.DEAL_NO
		  	<include refid="join"><property name="alias" value="T"/></include>
		ORDER BY
		<choose>
			<when test="sort != null and sort != ''">
				${sort} ${order}
			</when>
			<otherwise>
				LAST_UPDATE DESC, DEAL_NO DESC
			</otherwise>
		</choose>
	</select>
	
	<!-- 查询待审批列表 -->
	<select id="getRateChangeApproveList" parameterType="map" resultMap="rateChnageMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
			,task.task_id
		  FROM
		   (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>
			) task
			INNER JOIN TD_RATE_CHANGE T ON task.serial_no = T.DEAL_NO
			<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
			</if>
			<if test="RCStatus != null">
				AND (TTO.ORDER_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR
				TTT.TRADE_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
			<if test="approveStatusNo != null and approveStatusNo !=''">
				AND (TTO.ORDER_STATUS NOT IN
				<foreach item="item" index="index" collection="approveStatusNo"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR TTT.TRADE_STATUS NOT IN
				<foreach item="item" index="index" collection="approveStatusNo"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
		</where>
			ORDER BY
			<choose>
				<when test="sort != null and sort != ''">
					${sort} ${order}
				</when>
				<otherwise>
					LAST_UPDATE DESC, DEAL_NO DESC
				</otherwise>
			</choose>
	</select>
	
	<!-- 查询已审批列表 -->
	<select id="getRateChangeListFinish" parameterType="map" resultMap="rateChnageMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM  (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_completed_task"></include>) U2
		 INNER JOIN TD_RATE_CHANGE T ON U2.serial_no = T.DEAL_NO
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			AND U2.USER_ID != T.SPONSOR
			<if test="userId != null and userId != ''">
				AND U2.USER_ID = #{userId}
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
			</if>
			<if test="RCStatus != null">
				AND (TTO.ORDER_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR
				TTT.TRADE_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
			<if test="approveStatus != null and approveStatus !=''">
				AND (TTO.ORDER_STATUS IN
				<foreach item="item" index="index" collection="approveStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR TTT.TRADE_STATUS IN
				<foreach item="item" index="index" collection="approveStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
		</where>
		ORDER BY
		<choose>
			<when test="sort != null and sort != ''">
				${sort} ${order}
			</when>
			<otherwise>
				T.LAST_UPDATE DESC, T.DEAL_NO DESC
			</otherwise>
		</choose>
	</select>
	
	<!-- 我发起列表 -->
	<select id="getRateChangeMine" parameterType="map" resultMap="rateChnageMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_RATE_CHANGE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
			</if>
			<if test="RCStatus != null">
				AND (TTO.ORDER_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>
				OR
				TTT.TRADE_STATUS IN
				<foreach item="item" index="index" collection="RCStatus"
					open="(" separator="," close=")">
					#{item}
				</foreach>)
			</if>
			<if test="userId != null and userId != ''">
				AND T.SPONSOR = #{userId}
			</if>
		</where>
		ORDER BY
		T.LAST_UPDATE DESC, T.DEAL_NO DESC
	</select>
	
	<select id="searchRateChange" parameterType="com.singlee.capital.trade.model.TdRateChange" resultMap="rateChnageMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_RATE_CHANGE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="dealType !=null and dealType !=''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
		</where>
	</select>
	<select id="getAllRateChangesByDealnoVersion" parameterType="map" resultMap="rateChnageMap">
		SELECT 
		<include refid="columns"><property name="alias" value="T"/></include>
		 FROM TD_RATE_CHANGE T
		 <include refid="join"><property name="alias" value="T"/></include>
		 WHERE T.REF_NO=#{dealNo} AND TTT.TRADE_STATUS='12'
		<if test="version != null and version != ''">
	    <![CDATA[ AND T.VERSION=#{version}]]>
	    </if>
		<if test="startDate != null and startDate != ''">
	    <![CDATA[ and T.EFFECTDATE>=#{startDate}]]>
	    </if>
	    <if test="endDate != null and endDate != ''">
	    <![CDATA[ and T.EFFECTDATE<#{endDate}]]>
	    </if>
	</select>	
</mapper>