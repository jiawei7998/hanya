package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.model.IfsCfetsfxLend;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 外币拆借
 */
@Service("IfsPrintLendServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintLendServiceImpl implements IfsPrintService {

    @Resource
    IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(id);
        if (ifsCfetsfxLend != null) {
            String myDir = ifsCfetsfxLend.getDirection();
            if("P".equals(myDir)){
                mapEnd.put("instId", ifsCfetsfxLend.getInstfullname());
                mapEnd.put("dealer", ifsCfetsfxLend.getMyUserName());
                mapEnd.put("counterpartyInstId", ifsCfetsfxLend.getCno());
                mapEnd.put("counterpartyDealer", ifsCfetsfxLend.getCounterpartyDealer());
            }else if("S".equals(myDir)){
                mapEnd.put("instId", ifsCfetsfxLend.getCno());
                mapEnd.put("dealer", ifsCfetsfxLend.getCounterpartyDealer());
                mapEnd.put("counterpartyInstId", ifsCfetsfxLend.getInstfullname());
                mapEnd.put("counterpartyDealer", ifsCfetsfxLend.getMyUserName());
            }

            mapEnd.put("forDate", ifsCfetsfxLend.getForDate());
            mapEnd.put("forTime", ifsCfetsfxLend.getForTime());
            mapEnd.put("rate", ifsCfetsfxLend.getRate().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("basis", ifsCfetsfxLend.getBasis());

            mapEnd.put("tenor", ifsCfetsfxLend.getTenor());
            mapEnd.put("valueDate", ifsCfetsfxLend.getValueDate());
            mapEnd.put("maturityDate", ifsCfetsfxLend.getMaturityDate());
            mapEnd.put("occupancyDays", ifsCfetsfxLend.getOccupancyDays());
            mapEnd.put("interest", ifsCfetsfxLend.getInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("amount", ifsCfetsfxLend.getAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("repaymentAmount", ifsCfetsfxLend.getRepaymentAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            List<TaDictVo> list =  taDictVoMapper.getTadictTree("Currency");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCfetsfxLend.getCurrency())){
                    mapEnd.put("currency", t.getDict_value());
                }
            }
        }
        return mapEnd;
    }
}
