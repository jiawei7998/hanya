<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.trade.mapper.TdInvFiipApproveMapper">
	<sql id="columns">
		${alias}.DEAL_NO,
		${alias}.REF_NO,
		${alias}.DEAL_TYPE,
		CASE 
        	WHEN TTO.ORDER_STATUS IS NULL THEN TTT.TRADE_STATUS
        	WHEN TTT.TRADE_STATUS IS NULL THEN TTO.ORDER_STATUS
        	ELSE NULL
       	END AS APPROVE_STATUS,
		${alias}.SPONSOR,
		U.USER_NAME,
		${alias}.SPON_INST,
		I.INST_NAME,
		${alias}.A_DATE,
		${alias}.LAST_UPDATE,
		${alias}.VERSION,
		${alias}.PL_CHANGE,
		${alias}.CASH_FLOOR,
		${alias}.CASH_UP,
		${alias}.CG_FLOOR,
		${alias}.CG_UP,
		${alias}.PEOPLEBOC_FLOOR,
		${alias}.PEOPLEBOC_UP,
		${alias}.POLICYBOC_FLOOR,
		${alias}.POLICYBOC_UP,
		${alias}.LGOC_FLOOR,
		${alias}.LGOC_UP,
		${alias}.PSEC_FLOOR,
		${alias}.PSEC_UP,
		${alias}.CBIC_FLOOR,
		${alias}.CBIC_UP,
		${alias}.IS_SD,
		${alias}.ACCG_FLOOR,
		${alias}.ACCG_UP,
		${alias}.IS_BONDST,
		${alias}.OTHERFI_FLOOR,
		${alias}.OTHERFI_UP,
		${alias}.GE_FLOOR,
		${alias}.GE_UP,
		${alias}.OTHER,
		${alias}.POSTDATE,
		${alias}.QUOARTER
	</sql>
	
	<sql id="join">
		LEFT OUTER JOIN TA_USER U
		  ON ${alias}.SPONSOR = U.USER_ID
		LEFT OUTER JOIN TT_INSTITUTION I
		  ON ${alias}.SPON_INST = I.INST_ID
		LEFT OUTER JOIN TT_TRD_ORDER TTO
		  ON ${alias}.DEAL_NO = TTO.ORDER_ID
		LEFT OUTER JOIN TT_TRD_TRADE TTT
		  ON ${alias}.DEAL_NO = TTT.TRADE_ID
	</sql>
	
	<resultMap id="transforScheduleMap" type="com.singlee.capital.trade.model.TdInvFiipApprove">
		<id property="dealNo" column="DEAL_NO" />
		<result property="refNo" column="REF_NO" />
		<result property="dealType" column="DEAL_TYPE" />
		<result property="approveStatus" column="APPROVE_STATUS"/>
		<result property="sponsor" column="SPONSOR" />
		<result property="sponInst" column="SPON_INST" />
		<result property="aDate" column="A_DATE" />
		<result property="lastUpdate" column="LAST_UPDATE" />
		<result property="version" column="VERSION" />
		
		<result property="plChange" column="PL_CHANGE" />
		<result property="cashFloor" column="CASH_FLOOR" />
		<result property="cashUp" column="CASH_UP" />
		<result property="cgFloor" column="CG_FLOOR" />
		<result property="cgUp" column="CG_UP" />
		<result property="peoplebocFloor" column="PEOPLEBOC_FLOOR" />
		<result property="peoplebocUp" column="PEOPLEBOC_UP" />
		<result property="policybocFloor" column="POLICYBOC_FLOOR" />
		<result property="policybocUp" column="POLICYBOC_UP" />
		<result property="lgocFloor" column="LGOC_FLOOR" />
		<result property="lgocUp" column="LGOC_UP" />
		<result property="psecFloor" column="PSEC_FLOOR" />
		<result property="psecUp" column="PSEC_UP" />
		<result property="cbicFloor" column="CBIC_FLOOR" />
		<result property="cbicUp" column="CBIC_UP" />
		<result property="isSd" column="IS_SD" />
		<result property="accgFloor" column="ACCG_FLOOR" />
		<result property="accgUp" column="ACCG_UP" />
		
		<result property="isBondst" column="IS_BONDST" />
		<result property="otherfiFloor" column="OTHERFI_FLOOR" />
		<result property="otherfiUp" column="OTHERFI_UP" />
		<result property="geFloor" column="GE_FLOOR" />
		<result property="geUp" column="GE_UP" />
		<result property="other" column="OTHER" />
		<result property="postdate" column="POSTDATE" />
		<result property="quoarter" column="QUOARTER" />
		
		<result property="taskId" column="TASK_ID" />
		<association property="user" javaType="com.singlee.capital.system.model.TaUser">
			<id property="userId" column="SPONSOR" />
			<result property="userName" column="USER_NAME" />
		</association>
		<association property="institution" javaType="com.singlee.capital.system.model.TtInstitution">
			<id property="instId" column="SPON_INST" />
			<result property="instName" column="INST_NAME" />
		</association>
	</resultMap>
	
	
	<select id="getTdInvFiipApproveById" parameterType="String" resultMap="transforScheduleMap">
		SELECT
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_INV_FIIP_APPROVE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		 WHERE DEAL_NO = #{dealNo}
	</select>

	<select id="getTdInvFiipApproveList" parameterType="map" resultMap="transforScheduleMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
			,task.task_id
		  FROM
		   (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) task
		    INNER JOIN TD_INV_FIIP_APPROVE T ON task.serial_no = T.DEAL_NO
		  	<include refid="join"><property name="alias" value="T"/></include>
  			<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="sponInst != null and sponInst != ''">
				AND T.SPON_INST = #{sponInst}
			</if>
		</where>
		ORDER BY
		<choose>
			<when test="sort != null and sort != ''">
				${sort} ${order}
			</when>
			<otherwise>
				LAST_UPDATE DESC, DEAL_NO DESC
			</otherwise>
		</choose>
	</select>
	
	<select id="getTdInvFiipApproveListFinished" parameterType="map" resultMap="transforScheduleMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM  (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_completed_task"></include>) U2
		    INNER JOIN TD_INV_FIIP_APPROVE T ON U2.serial_no = T.DEAL_NO
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="userId != null and userId != ''">
				AND U2.USER_ID = #{userId}
			</if>
			<if test="sponInst != null and sponInst != ''">
				AND T.SPON_INST = #{sponInst}
			</if>
		</where>
		ORDER BY
		<choose>
			<when test="sort != null and sort != ''">
				${sort} ${order}
			</when>
			<otherwise>
				T.LAST_UPDATE DESC, T.DEAL_NO DESC
			</otherwise>
		</choose>
	</select>
	
	<select id="getTdInvFiipApproveListMine" parameterType="map" resultMap="transforScheduleMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_INV_FIIP_APPROVE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="refNo != null and refNo != ''">
				AND T.REF_NO = #{refNo}
			</if>
			<if test="dealType != null and dealType != ''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
			<if test="sponInst != null and sponInst != ''">
				AND T.SPON_INST = #{sponInst}
			</if>
			<if test="userId != null and userId != ''">
				AND T.SPONSOR = #{userId}
			</if>
		</where>
		ORDER BY
			<choose>
				<when test="sort != null and sort != ''">
					${sort} ${order}
				</when>
				<otherwise>
					LAST_UPDATE DESC, DEAL_NO DESC
				</otherwise>
			</choose>
	</select>	
	
	<select id="searchTdInvFiipApprove" parameterType="com.singlee.capital.trade.model.TdInvFiipApprove" resultMap="transforScheduleMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TD_INV_FIIP_APPROVE T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="dealNo != null and dealNo != ''">
				AND T.DEAL_NO = #{dealNo}
			</if>
			<if test="dealType !=null and dealType !=''">
				AND T.DEAL_TYPE = #{dealType}
			</if>
		</where>
	</select>
</mapper>