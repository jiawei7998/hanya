package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbCbtMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 债券投资交易审批单
 */
@Service("IfsPrintCbtTzServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintCbtTzServiceImpl implements IfsPrintService {

	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;

	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;

	@Autowired
	TaDictVoMapper taDictVoMapper;

	@Override
	public Map<String, Object> getData(String id) {

		Map<String, Object> mapEnd = new HashMap<String, Object>();
		IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(id);

		IfsOpicsBond ifsOpicsBond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());
		if(ifsOpicsBond!=null){
			mapEnd.put("bondperiod", new BigDecimal(ifsOpicsBond.getBondperiod() == null ? "0" : ifsOpicsBond.getBondperiod()).divide(BigDecimal.valueOf(360), 4, BigDecimal.ROUND_HALF_UP));
			mapEnd.put("issuer", ifsOpicsBond.getIssuer());
			mapEnd.put("couponrate", ifsOpicsBond.getCouponrate());
			mapEnd.put("bondrating", ifsOpicsBond.getBondrating());
			mapEnd.put("interestPaymentMethod", ifsOpicsBond.getIntpaycircle());
			int remainDay = DateUtil.daysBetween(DateUtil.getCurrentDateAsString(), ifsOpicsBond.getMatdt());
			BigDecimal remainYear = new BigDecimal(remainDay).divide(BigDecimal.valueOf(360), 4, BigDecimal.ROUND_HALF_UP);
			mapEnd.put("remainYear", remainYear);
			mapEnd.put("issudt", ifsOpicsBond.getIssudt());
			mapEnd.put("matdt", ifsOpicsBond.getMatdt());
		}

		mapEnd.put("bondName", ifsCfetsrmbCbt.getBondName());
		mapEnd.put("bondCode", ifsCfetsrmbCbt.getBondCode());
		mapEnd.put("totalFaceValue", ifsCfetsrmbCbt.getTotalFaceValue());
		mapEnd.put("invtype", ifsCfetsrmbCbt.getInvtype());
		mapEnd.put("cleanPrice", ifsCfetsrmbCbt.getCleanPrice());
		mapEnd.put("yield", ifsCfetsrmbCbt.getYield());
		mapEnd.put("note", ifsCfetsrmbCbt.getNote());
		mapEnd.put("date", DateUtil.getCurrentDateAsString());

		return mapEnd;
	}
}
