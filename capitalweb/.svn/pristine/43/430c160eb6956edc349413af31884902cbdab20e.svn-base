package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbBondfwdMapper;
import com.singlee.ifs.model.IfsCfetsrmbBondfwd;
import com.singlee.ifs.service.IfsCfetsrmbBondfwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCfetsrmbBondfwdServiceImpl implements IfsCfetsrmbBondfwdService {

	@Autowired
	private IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	@Resource
	IFSMapper ifsMapper;
	
	@Override
	public Page<IfsCfetsrmbBondfwd> searchICBFPage(Map<String, Object> params, Integer isFinished) {
		Page<IfsCfetsrmbBondfwd> page = new Page<IfsCfetsrmbBondfwd>();
		
		if (isFinished == 1) {// 待审批
			page = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdFinished(params, ParameterUtil.getRowBounds(params));
		} else {//我发起的
			page = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public List<IfsCfetsrmbBondfwd> searchICBFList(Map<String, Object> map) {
		return ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdList(map);
	}

	@Override
	public IfsCfetsrmbBondfwd searchICBF(Map<String, Object> map) {
		return ifsCfetsrmbBondfwdMapper.selectFlowIdByPrimaryKey(map);
	}

	@Override
	public void saveICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd) {
		ifsCfetsrmbBondfwd.setTicketId(getTicketId("BondFwd"));
		ifsCfetsrmbBondfwd.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsrmbBondfwdMapper.insertIfsCfetsRMBBound(ifsCfetsrmbBondfwd);
	}

	@Override
	public int updateICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd) {
		return ifsCfetsrmbBondfwdMapper.updateByTicketId(ifsCfetsrmbBondfwd);
	}

	@Override
	public int deleteICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd) {
		Map<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("dealTransType",ifsCfetsrmbBondfwd.getDealTransType());
		hashMap.put("ticketId",ifsCfetsrmbBondfwd.getTicketId());
		return ifsCfetsrmbBondfwdMapper.deleteIfsCfetsrmbBondfwd(hashMap);
	}

	@Override
	public IfsCfetsrmbBondfwd getBondById(IfsCfetsrmbBondfwd key) {
		Map<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("dealTransType",key.getDealTransType());
		hashMap.put("ticketId",key.getTicketId());
		IfsCfetsrmbBondfwd Bondfwd=ifsCfetsrmbBondfwdMapper.selectByPrimaryKeyId(hashMap);
		return Bondfwd;
	}


	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

	@Override
	public IfsCfetsrmbBondfwd getIfsCfetsrmbBondfwdByTicketId(Map<String, Object> map){
		IfsCfetsrmbBondfwd result = ifsCfetsrmbBondfwdMapper.getIfsCfetsrmbBondfwdByTicketId(map);
		return result;
	}
}