<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.credit.mapper.TcCustCreditDealMapper">
	<sql id="columns">
		${alias}.deal_No,
		${alias}.cust_No,
		${alias}.cust_Name,
		${alias}.product_code,
		${alias}.product_name,
		${alias}.institution,
		${alias}.vdate,
		${alias}.mdate,
		${alias}.amt,
		${alias}.reason,
		${alias}.remark1,
		${alias}.old_amt,
		${alias}.old_Deal_No,
		${alias}.state,
		${alias}.ioper,
		${alias}.off_line,
		${alias}.approve_Status,
		${alias}.input_date,
		${alias}.weight,
		I.INST_FULLNAME AS spon_Inst_Name
	</sql>

	<sql id="join">
		LEFT JOIN TT_INSTITUTION I ON T.institution=I.INST_ID
	</sql>
	
	<resultMap id="DealMap" type="com.singlee.capital.credit.pojo.CustCreditDealBean">
		<result property="dealNo" column="deal_no" />
		<result property="custNo" column="cust_no" />
		<result property="custName" column="cust_name" />
		<result property="productCode" column="product_code" />
		<result property="productName" column="product_name" />
		<result property="institution" column="institution" />
		<result property="vdate" column="vdate" />
		<result property="mdate" column="mdate" />
		<result property="amt" column="amt" />
		<result property="reason" column="reason" />
		<result property="remark1" column="remark1" />
		<result property="oldAmt" column="old_Amt" />
		<result property="oldDealNo" column="old_Deal_No" />
		<result property="state" column="state" />
		<result property="approveStatus" column="approve_status" />
		<result property="taskId" column="task_id" />
		<result property="ioper" column="ioper" />
		<result property="offLine" column="off_line" />
		<result property="inputDate" column="input_date" />
		<result property="sponInstName" column="spon_Inst_Name" />
		<result property="weight" column="weight" />
	</resultMap>
	
	<select id="getCreditDealListApprove" parameterType="map" resultMap="DealMap">
		select 
		<include refid="columns">
			<property name="alias" value="T" />
		</include>
		,S.TASK_ID
		FROM TC_CUST_CREDIT_Deal T
		<include refid="join"><property name="alias" value="T"/></include>
		INNER JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) S
		ON T.DEAL_NO = S.SERIAL_NO
		<where>
			NVL(T.off_Line,'N') = #{offLine} AND (
			1=1
			<if test="approveStatus != null and approveStatus != ''">
				or approve_status = #{approveStatus}
			</if>
			<if test="approveStatus_Approving != null and approveStatus_Approving != ''">
				or T.approve_status = #{approveStatus_Approving}
			</if>)
			<if test="searchStatus != null and searchStatus != ''">
				and T.approve_Status = #{searchStatus}
			</if>
			<if test="dealNo != null and dealNo != ''">
				and T.deal_No like '%' || #{dealNo} || '%'
			</if>
			<if test="oldDealNo != null and oldDealNo != ''">
				and T.old_deal_No like '%' || #{oldDealNo} || '%'
			</if>
			<if test="partyName != null and partyName != ''">
				and T.cust_Name like '%' || #{partyName} || '%'
			</if>
			<if test="state != null and state != ''">
				and T.state = #{state}
			</if>
			<!-- 20180529增加条件branchId -->
			<if test="branchId != null and branchId != ''">
				and I.BRANCH_ID = #{branchId}
			</if>
		</where>
	</select>
	
	<select id="getCreditDealListFinish" parameterType="map" resultMap="DealMap">
		select 
		<include refid="columns">
			<property name="alias" value="T" />
		</include>
		FROM TC_CUST_CREDIT_Deal T
		<include refid="join"><property name="alias" value="T"/></include>
		INNER JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_completed_task"></include>) S
		ON T.DEAL_NO = S.SERIAL_NO
		<where>
			NVL(T.off_Line,'N') = #{offLine} AND (
			1=1
			<if test="approveStatus != null and approveStatus != ''">
				or approve_status = #{approveStatus}
			</if>
			<if test="approveStatus_Approving != null and approveStatus_Approving != ''">
				or T.approve_status = #{approveStatus_Approving}
			</if>)
			<if test="searchStatus != null and searchStatus != ''">
				and T.approve_Status = #{searchStatus}
			</if>
			<if test="dealNo != null and dealNo != ''">
				and T.deal_No like '%' || #{dealNo} || '%'
			</if>
			<if test="oldDealNo != null and oldDealNo != ''">
				and T.old_deal_No like '%' || #{oldDealNo} || '%'
			</if>
			<if test="partyName != null and partyName != ''">
				and T.cust_Name like '%' || #{partyName} || '%'
			</if>
			<if test="state != null and state != ''">
				and T.state = #{state}
			</if>
			<!-- 20180529增加条件branchId -->
			<if test="branchId != null and branchId != ''">
				and I.BRANCH_ID = #{branchId}
			</if>
		</where>
	</select>
	
	<select id="getCreditDealListMine" parameterType="map" resultMap="DealMap">
		select 
		<include refid="columns">
			<property name="alias" value="T" />
		</include>
		FROM TC_CUST_CREDIT_Deal T
		<include refid="join"><property name="alias" value="T"/></include>
		<where>
			NVL(T.off_Line,'N') = #{offLine} AND (
			1=1
			<if test="approveStatus != null and approveStatus != ''">
				or approve_status = #{approveStatus}
			</if>
			<if test="approveStatus_Approving != null and approveStatus_Approving != ''">
				or T.approve_status = #{approveStatus_Approving}
			</if>)
			<if test="searchStatus != null and searchStatus != ''">
				and T.approve_Status = #{searchStatus}
			</if>
			<if test="dealNo != null and dealNo != ''">
				and T.deal_No like '%' || #{dealNo} || '%'
			</if>
			<if test="oldDealNo != null and oldDealNo != ''">
				and T.old_deal_No like '%' || #{oldDealNo} || '%'
			</if>
			<if test="partyName != null and partyName != ''">
				and T.cust_Name like '%' || #{partyName} || '%'
			</if>
			<if test="state != null and state != ''">
				and T.state = #{state}
			</if>
			<if test="userId != null and userId != ''">
				and T.IOPER = #{userId}
			</if>
			<!-- 20180529增加条件branchId -->
			<if test="branchId != null and branchId != ''">
				and I.BRANCH_ID = #{branchId}
			</if>
		</where>
	</select>
	
	
	<select id="getTicketIdOld" parameterType="String" resultType="String">
		select T.OLD_DEAL_NO as oldDealNo FROM TC_CUST_CREDIT_Deal T
		<where>
			<if test="serial_no != null and serial_no != ''">
				and T.DEAL_NO = #{serial_no}
			</if>
		</where>
	</select>
	
	
	<select id="getCreditDealByDealNo" parameterType="String" resultMap="DealMap">
		select 
			T.deal_No,
			T.cust_No,
			T.cust_Name,
			T.product_code,
			T.product_name,
			T.institution,
			T.vdate,
			T.mdate,
			T.amt,
			T.reason,
			T.remark1,
			T.old_amt,
			T.old_Deal_No,
			T.state,
			T.ioper,
			T.off_line,
			T.approve_Status,
			T.input_date,
			T.weight
		from TC_CUST_CREDIT_Deal T
		where deal_No = #{dealNo}
	</select>
	
	<select id="getCreditDealAll" parameterType="map" resultMap="DealMap">
		SELECT TC.DEAL_NO,
	       TC.PRODUCT_CODE,
	       P.PRD_NAME PRODUCT_NAME,
	       TC.CUST_NO,
	       TC.CUST_NAME,
	       TC.INSTITUTION,
	       TC.VDATE,
	       TC.MDATE,
	       TC.AMT,
	       TC.REASON,
	       TC.IOPER
	  FROM TC_CUST_CREDIT_DEAL TC,TC_PRODUCT P WHERE TC.PRODUCT_CODE = P.PRD_NO
	  <if test="dealNo != null and dealNo != ''">
	    AND TC.DEAL_NO = #{dealNo}
	  </if>
		UNION ALL (SELECT TD.DEAL_NO,
                   TD.PRD_NO,
                   PP.PRD_NAME PRODUCT_NAME,
                   TD.C_NO,
                   TT.CN_NAME PARTY_NAME,
                   TD.SPON_INST,
                   TD.V_DATE,
                   TD.M_DATE,
                   TD.AMT,
                   TD.REMARK_NOTE,
                   TD.SPONSOR
     	FROM TD_PRODUCT_APPROVE_MAIN TD, td_cust_ecif TT,TC_PRODUCT PP
        WHERE TD.C_NO = TT.CUSTMER_CODE AND TD.PRD_NO = PP.PRD_NO
      <if test="dealNo != null and dealNo != ''">
	  	AND TD.DEAL_NO = #{dealNo}
	  </if>)
	</select>
	
	<update id="updateByMap" parameterType="map">
		update tc_cust_credit_Deal set
		cust_no = #{custNo},cust_name = #{custName},product_Code = #{productCode},
		product_Name = #{productName},institution = #{institution},
		vdate = #{vdate},mdate = #{mdate},amt = #{amt},reason = #{reason},
		remark1 = #{remark1},state = #{state},
		modify_Date = #{modifyDate},weight = #{weight}
		where deal_no = #{dealNo}
	</update>
	
	<update id="statusChange" parameterType="String">
		update tc_cust_credit_Deal set
		approve_Status = #{approveStatus},modify_Date = #{modifyDate}
		where deal_no = #{dealNo}
	</update>
	
	<select id="queryCreditReleaseDeal" parameterType="map" resultMap="DealMap">
		SELECT T.DEALNO,T.CUST_NO,MAX(C.CN_NAME) custName,MAX(T.PRODUCT_CODE) PRODUCT_CODE,MAX(P.PRD_NAME) PRODUCT_NAME,
      	SUM(T.USE_AMT) AMT,MAX(VDATE) VDATE,MAX(MDATE) MDATE 
    	FROM TC_CUST_CREDIT_DEAL_RELATION T,TC_CUST_CREDIT_MAIN_LOG M,TC_PRODUCT P,td_cust_ecif C
		WHERE T.DEALNO = M.DEAL_NO  
		  AND 1=1 
		  AND M.OPER_TYPE = 1
		  AND T.PRODUCT_CODE = P.PRD_NO
		  AND T.CUST_NO = C.CUSTMER_CODE
		<if test="dealNo != null and dealNo != ''">
		  AND T.DEALNO like '%' || #{dealNo} || '%'
		</if> 
		<if test="productName != null and productName != ''">
		  AND P.PRD_NAME like '%' || #{productName} || '%'
		</if> 
		<if test="custName != null and custName != ''">
		  AND C.CN_NAME like '%' || #{custName} || '%'
		</if> 
		GROUP BY T.DEALNO,T.CUST_NO HAVING(SUM(T.USE_AMT) > 0)
	</select>
	
	<select id="queryCreditReleaseDealByDealno" parameterType="map" resultMap="DealMap">
		SELECT T.DEALNO,T.CUST_NO,MAX(C.CN_NAME) custName,MAX(T.PRODUCT_CODE) PRODUCT_CODE,MAX(P.PRD_NAME) PRODUCT_NAME,
        SUM(T.USE_AMT) AMT,MAX(VDATE) VDATE,MAX(MDATE) MDATE 
		FROM TC_CUST_CREDIT_DEAL_RELATION T,TC_CUST_CREDIT_MAIN_LOG M,TC_PRODUCT P,td_cust_ecif C
		WHERE T.DEALNO = M.DEAL_NO  
		  AND 1=1
		  AND M.OPER_TYPE = 1
		  AND T.PRODUCT_CODE = P.PRD_NO
		  AND T.CUST_NO = C.CUSTMER_CODE
		<if test="dealNo != null and dealNo != ''">
		  AND T.DEALNO = #{dealNo}
		</if>  
		<if test="custNo != null and custNo != ''">
		  AND T.CUST_NO = #{custNo}
		</if>  
		GROUP BY T.DEALNO,T.CUST_NO HAVING(SUM(T.USE_AMT) > 0)
	</select>
	
	<sql id="columns1">
		${alias}.dealno,
		${alias}.custno,
		${alias}.custname,
		${alias}.ioper,
		${alias}.currency,
		${alias}.loanamt,
		${alias}.vdate,
		${alias}.mdate,
		${alias}.state,
		${alias}.prodcode,
		${alias}.weight,
		${alias}.instname,
		${alias}.orgno
	</sql>
	
	<resultMap id="DetailMap" type="com.singlee.financial.bean.LoanBean">
		<result property="custcreditid" column="dealno" />
		<result property="custno" column="custno" />
		<result property="custname" column="custname" />
		<result property="ioper" column="ioper" />
		<result property="ccy" column="currency" />
		<result property="loanamt" column="loanamt" />
		<result property="vdate" column="vdate" />
		<result property="mdate" column="mdate" />
		<result property="state" column="state" />
		<result property="productname" column="prodcode" />
		<result property="weight" column="weight" />
		<result property="institution" column="instname" />
		<result property="orgno" column="orgno" />
	</resultMap>
	
	<!-- 联合查询 -->
	<select id="getDetailEd" resultMap="DetailMap" >
		SELECT distinct d.*,q.currency,g.amt as loanamt,f.EX_DATE as mdate,h.loanmapcode as prodcode,
	    f.weight,g.amt_all as amt FROM 
	    (select a.deal_no as dealno,a.ecif_no as custno, e.cliname as custname,v.v_date as vdate,v.ioper as ioper,v.INST_ID as orgno from td_ed_cust_batch_status a
	    LEFT JOIN ifs_opics_cust e on a.ecif_no=e.clino
	    LEFT JOIN td_ed_cust t on a.ecif_no=t.ecif_num
	    LEFT JOIN V_ALL_PRODUCT_LIMIT v on a.deal_no=v.deal_no) d LEFT JOIN td_ed_cust q ON d.custno=q.ecif_num 
	    LEFT JOIN V_ALL_PRODUCT_LIMIT f on d.dealno=f.deal_no
	    LEFT JOIN TD_ED_CUST_BATCH_STATUS g ON d.dealno=g.deal_no
	    LEFT JOIN TC_PRODUCT h ON f.prd_no = h.prd_no
	</select>
	
	<!-- 交易金额增加权重，金额值为 金额*权重/100 -->
	<select id="getCreditDealByDealNoAddWeight" parameterType="String" resultMap="DealMap">
		select 
			T.deal_No,
			T.cust_No,
			T.cust_Name,
			T.product_code,
			T.product_name,
			T.institution,
			T.vdate,
			T.mdate,
			T.amt*(NVL(T.weight,100)/100) as amt,
			T.reason,
			T.remark1,
			T.old_amt,
			T.old_Deal_No,
			T.state,
			T.ioper,
			T.off_line,
			T.approve_Status,
			T.input_date,
			T.weight
		from TC_CUST_CREDIT_Deal T
		where deal_No = #{dealNo}
	</select>
</mapper>