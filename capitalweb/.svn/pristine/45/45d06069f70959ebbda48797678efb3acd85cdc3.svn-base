package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.singlee.capital.common.util.TreeInterface;

/**
 * @projectName 同业业务管理系统
 * @className 附件树关联关系表
 * @description TODO
 * @author 刘小燚
 * @createDate 2017-6-4 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
public class TcAttachTreeRefVo implements Serializable,TreeInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String refId;
	private String refName;
	private String treeId;
	private String treeName;
	private String nodeId;
	private String nodeName;
	private String nodeType;
	private String parId;
	
	@Transient
	private List<TcAttachTreeRefVo> children;
	
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getRefName() {
		return refName;
	}
	public void setRefName(String refName) {
		this.refName = refName;
	}
	public String getTreeId() {
		return treeId;
	}
	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public String getNodeType() {
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	public String getParId() {
		return parId;
	}
	public void setParId(String parId) {
		this.parId = parId;
	}
	
	@Override
	public String getId() {
		return getNodeId();
	} 
	@Override
	public String getParentId() {
		return getParId();
	}
	public List<TcAttachTreeRefVo> getChildren() {
		return children;
	}
	public void setChildren(List<TcAttachTreeRefVo> children) {
		Collections.sort(children, new Comparator<TcAttachTreeRefVo>() {
			@Override
			public int compare(TcAttachTreeRefVo arg0, TcAttachTreeRefVo arg1) {
				return arg0.getSort() > arg1.getSort() ? 1 : 0;
			}
			
		});
		this.children = children;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TcAttachTreeRefVo>) list);
	}
	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	@Override
	public String getText() {
		return nodeName;
	}
	@Override
	public String getValue() {
		return getNodeId();
	}
	@Override
	public int getSort() {
		return StringUtils.length(getNodeId());
	}
	@Override
	public String getIconCls() {
		return "";
	}
	
}

