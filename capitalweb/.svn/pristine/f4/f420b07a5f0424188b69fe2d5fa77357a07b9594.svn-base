<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.singlee.capital.reports.mapper.ReportCommMapper">

	<resultMap id="resMap" type = "com.singlee.cap.base.vo.TradingAccountVo">
		<result property="dealNo" column="DEAL_NO" />
		<result property="subjCode" column="SUBJ_CODE" />
		<result property="debitValue" column="DEBIT_VALUE" />
		<result property="creditValue" column="CREDIT_VALUE" />
	</resultMap>
	
	<select id="selectAllTradingAccount" resultMap ="resMap">
		SELECT 
		T.DEAL_NO,T.SUBJ_CODE,T.DEBIT_VALUE,T.CREDIT_VALUE
		FROM TBK_DEAL_BALANCE T 
		LEFT JOIN TBK_SUBJECT_DEF A ON T.SUBJ_CODE = A.SUBJ_CODE 
		WHERE A.SUBJ_NATURE = '1'
	</select>
	
	<!-- 同业投资情况表 -->
	<select id="getReportInterbankAccount" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.ReportInterbankInVo">
		SELECT P.PRD_NAME as prdName,
		       SUM(CASE WHEN A.CCY = 'CNY'
		       <if test="deanDate != NULL and deanDate != '' "> AND A.DEAL_DATE LIKE #{deanDate} </if>
		       THEN A.AMT ELSE 0 END) AS sumAmtCcy,
		       SUM(CASE WHEN A.CCY = 'USD' 
		       <if test="deanDate != NULL and deanDate != '' "> AND A.DEAL_DATE LIKE #{deanDate} </if>
		       THEN A.AMT ELSE 0 END) AS sumAmtUsd
		  FROM TD_PRODUCT_APPROVE_MAIN A
		  LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
		  LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID 
		 WHERE TR.TRADE_STATUS='12'
		   AND A.SPON_INST IN (Select p.Inst_Id
		                           From Tt_Institution p
		                          Start With p.Inst_Id = 'CN0019001'
		                         Connect By Prior p.Inst_Id = p.p_Inst_Id)
		 GROUP BY P.PRD_NAME ORDER BY P.PRD_NAME
	</select>
	
	<!-- 同业投资情况合计 -->
	<select id="getReportInterbankhjAccount" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.ReportInterbankInVo">
		  <![CDATA[
		  SELECT P.PRD_NAME as prdName,
	             SUM(CASE WHEN A.CCY='CNY' AND A.DEAL_DATE LIKE #{deanDate} 
	             		  THEN A.AMT
	             		  WHEN A.CCY <> 'CNY' AND A.DEAL_DATE LIKE #{deanDate} 
	             		  THEN (A.AMT*A.VERIFY_RATE8)
	                      ELSE 0 
	                  END ) AS sumAmt
	        FROM TD_PRODUCT_APPROVE_MAIN A
	        LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
	        LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID 
	       WHERE TR.TRADE_STATUS='12'
		     AND A.SPON_INST IN (Select p.Inst_Id
	                                 From Tt_Institution p
	                                Start With p.Inst_Id = 'CN0019001'
	                               Connect By Prior p.Inst_Id = p.p_Inst_Id)
	       GROUP BY P.PRD_NAME ORDER BY P.PRD_NAME
	       ]]>
	</select>
	<!-- 较上月本外合计 -->
	<select id="getReportInterbankdifferAccount" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.ReportInterbankInVo">
		  <![CDATA[
		  SELECT ST.prdName,
		  		 (ST.SUMAMT-SUMUPAMT) AS differAmt 
		  	FROM (SELECT P.PRD_NAME as prdName,
	             SUM(CASE WHEN A.CCY='CNY' AND A.DEAL_DATE LIKE #{beginDate} 
	             		  THEN A.AMT 
	             		  WHEN A.CCY <> 'CNY' AND A.DEAL_DATE LIKE #{beginDate} 
	             		  THEN (A.AMT*A.VERIFY_RATE8)
	             		  ELSE 0
					  END) AS sumAmt,
	             SUM(CASE WHEN A.CCY='CNY' AND A.DEAL_DATE LIKE #{update} 
	             		  THEN A.AMT 
	             		  WHEN A.CCY <> 'CNY' AND A.DEAL_DATE LIKE #{update}
	             		  THEN (A.AMT*A.VERIFY_RATE8)
	             		  ELSE 0 
	             	  END) AS sumUpAmt
	        FROM TD_PRODUCT_APPROVE_MAIN A
	        LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
	        LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
	       WHERE TR.TRADE_STATUS='12'
		     AND A.SPON_INST IN (Select p.Inst_Id
	                                 From Tt_Institution p
	                                Start With p.Inst_Id = 'CN0019001'
	                               Connect By Prior p.Inst_Id = p.p_Inst_Id)
	       GROUP BY P.PRD_NAME ORDER BY P.PRD_NAME) ST ORDER BY ST.PRDNAME
	       ]]>
	</select>
	
	
	
	<!-- 信用资产负债率指标 -->
	<select id="getRepAssetLiabilityRatioAccount" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.RepAssetLiabilityRatioVo">
		  <![CDATA[
		  SELECT * FROM (
		  	SELECT ST.prdName,
	             (ST.sumAmt/#{maxDate}) AS avgAmt, 
	             st.sumMonthEndAmt,
	             st.sumYearEndAmt,
	             st.sumYearStarAmt,
	             ((ST.sumAmt/#{maxDate}) - st.sumYearStarAmt) AS defferAmt
	        FROM (SELECT P.PRD_NAME as prdName,
		             SUM(CASE WHEN A.CCY = 'CNY'
		                       AND A.DEAL_DATE LIKE #{reDate}
		                      THEN A.AMT 
	                        WHEN A.CCY <> 'CNY'
	                         AND A.DEAL_DATE LIKE #{reDate}
	                        THEN (A.AMT*A.VERIFY_RATE8) 
	                        ELSE 0 END) AS sumAmt,
		             SUM(CASE WHEN A.CCY='CNY'
		                       AND A.DEAL_DATE = #{lastDay} 
		                      THEN A.AMT 
	                        WHEN A.CCY <> 'CNY'
		                       AND A.DEAL_DATE = #{lastDay}
	                        THEN (A.AMT*A.VERIFY_RATE8)
	                        ELSE 0 END) AS sumMonthEndAmt,
	               SUM(CASE WHEN A.CCY='CNY'
		                       AND A.DEAL_DATE = #{upDate}
		                      THEN A.AMT 
	                        WHEN A.CCY<>'CNY'
	                         AND A.DEAL_DATE = #{upDate}
	                        THEN (A.AMT*A.VERIFY_RATE8)
	                        ELSE 0 END) AS sumYearEndAmt,
	               SUM(CASE WHEN A.CCY='CNY'
		                       AND A.DEAL_DATE = #{starYearDate}
	                        THEN A.AMT 
	                        WHEN A.CCY='CNY'
		                       AND A.DEAL_DATE = #{starYearDate}
	                        THEN (A.AMT*A.VERIFY_RATE8)
		                      ELSE 0 END) AS sumYearStarAmt
		        FROM TD_PRODUCT_APPROVE_MAIN A
		        LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
		        LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
		       WHERE TR.TRADE_STATUS='12'
		     	 AND A.SPON_INST IN (Select p.Inst_Id
		                                 From Tt_Institution p
		                                Start With p.Inst_Id = 'CN0019001'
		                               Connect By Prior p.Inst_Id = p.p_Inst_Id)
		       GROUP BY P.PRD_NAME ORDER BY P.PRD_NAME) ST ORDER BY ST.PRDNAME)
		       Union all
	           select * from (select 
		           '合计' as prdName,
		           sum(avgAmt) as avgAmt,
		           sum(sumMonthEndAmt) as sumMonthEndAmt,
		           sum(sumYearEndAmt) as sumYearEndAmt,
		           sum(sumYearStarAmt) as sumYearStarAmt,
		           sum(defferAmt) as defferAmt
		          from
		           (SELECT   ST.prdName,
				             (ST.sumAmt/#{maxDate}) AS avgAmt, 
				             st.sumMonthEndAmt,
				             st.sumYearEndAmt,
				             st.sumYearStarAmt,
				             ((ST.sumAmt/#{maxDate}) - st.sumYearStarAmt) AS defferAmt
				        FROM (SELECT P.PRD_NAME as prdName,
					             SUM(CASE WHEN A.CCY = 'CNY'
					                       AND A.DEAL_DATE LIKE #{reDate}
					                      THEN A.AMT 
				                        WHEN A.CCY <> 'CNY'
				                         AND A.DEAL_DATE LIKE #{reDate}
				                        THEN (A.AMT*A.VERIFY_RATE8) 
				                        ELSE 0 END) AS sumAmt,
					             SUM(CASE WHEN A.CCY='CNY'
					                       AND A.DEAL_DATE = #{lastDay} 
					                      THEN A.AMT 
				                        WHEN A.CCY <> 'CNY'
					                       AND A.DEAL_DATE = #{lastDay}
				                        THEN (A.AMT*A.VERIFY_RATE8)
				                        ELSE 0 END) AS sumMonthEndAmt,
				               SUM(CASE WHEN A.CCY='CNY'
					                       AND A.DEAL_DATE = #{upDate}
					                      THEN A.AMT 
				                        WHEN A.CCY<>'CNY'
				                         AND A.DEAL_DATE = #{upDate}
				                        THEN (A.AMT*A.VERIFY_RATE8)
				                        ELSE 0 END) AS sumYearEndAmt,
				               SUM(CASE WHEN A.CCY='CNY'
					                       AND A.DEAL_DATE = #{starYearDate}
				                        THEN A.AMT 
				                        WHEN A.CCY='CNY'
					                       AND A.DEAL_DATE = #{starYearDate}
				                        THEN (A.AMT*A.VERIFY_RATE8)
					                      ELSE 0 END) AS sumYearStarAmt
					        FROM TD_PRODUCT_APPROVE_MAIN A
					        LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
					        LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
					       WHERE TR.TRADE_STATUS='12'
		     	 		     AND A.SPON_INST IN (Select p.Inst_Id
					                                 From Tt_Institution p
					                                Start With p.Inst_Id = 'CN0019001'
					                               Connect By Prior p.Inst_Id = p.p_Inst_Id)
					       GROUP BY P.PRD_NAME ORDER BY P.PRD_NAME) ST ORDER BY ST.PRDNAME) t)
		]]>
	</select>
	
	
	<!--经营单位中收明细表-->
	<select id="queryInCharge" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.InChargeVo">
		SELECT
                ST.instName,
                ST.sameMonSumgwf,
                ST.totalSumgwf,
                ST.sameMonSumtgf,
                ST.totalSumtgf,
                ST.sameMonSumfwf,
                ST.totalSumfwf,
                (ST.sameMonSumgwf + ST.sameMonSumtgf + ST.sameMonSumfwf) AS sameMonSumhj,
                (ST.totalSumgwf + ST.totalSumtgf + ST.totalSumfwf) AS totalSumhj
          FROM 
          (SELECT INST.INST_NAME AS instName,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.FINA_FEEAMT 
                          ELSE 0 END) AS sameMonSumgwf,
                 SUM(A.FINA_FEEAMT) AS totalSumgwf,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.CUSTOD_FEEAMT 
                          ELSE 0 END) AS sameMonSumtgf,
                 SUM(A.CUSTOD_FEEAMT) AS totalSumtgf,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.SERVICE_FEEAMT 
                          ELSE 0 END) AS sameMonSumfwf,
                 SUM(A.SERVICE_FEEAMT) AS totalSumfwf
            FROM TD_PRODUCT_APPROVE_MAIN A
            LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
            LEFT JOIN TT_INSTITUTION INST ON A.SPON_INST = INST.INST_ID
            LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
           WHERE TR.TRADE_STATUS='12'
 		     AND A.SPON_INST IN (Select p.Inst_Id
                                     From Tt_Institution p
                                    Start With p.Inst_Id = 'CN0019001'
                                   Connect By Prior p.Inst_Id = p.p_Inst_Id)
           GROUP BY INST.INST_NAME ORDER BY INST.INST_NAME) ST
           Union all
           SELECT
                ST.instName,
                ST.sameMonSumgwf,
                ST.totalSumgwf,
                ST.sameMonSumtgf,
                ST.totalSumtgf,
                ST.sameMonSumfwf,
                ST.totalSumfwf,
                (ST.sameMonSumgwf + ST.sameMonSumtgf + ST.sameMonSumfwf) AS sameMonSumhj,
                (ST.totalSumgwf + ST.totalSumtgf + ST.totalSumfwf) AS totalSumhj
          FROM 
          (SELECT '合计' AS instName,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.FINA_FEEAMT 
                          ELSE 0 END) AS sameMonSumgwf,
                 SUM(A.FINA_FEEAMT) AS totalSumgwf,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.CUSTOD_FEEAMT 
                          ELSE 0 END) AS sameMonSumtgf,
                 SUM(A.CUSTOD_FEEAMT) AS totalSumtgf,
                 SUM(CASE WHEN A.DEAL_DATE LIKE #{deanDate}
                          THEN A.SERVICE_FEEAMT 
                          ELSE 0 END) AS sameMonSumfwf,
                 SUM(A.SERVICE_FEEAMT) AS totalSumfwf
            FROM TD_PRODUCT_APPROVE_MAIN A
            LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
            LEFT JOIN TT_INSTITUTION INST ON A.SPON_INST = INST.INST_ID
            LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
           WHERE TR.TRADE_STATUS='12'
 		     AND A.SPON_INST IN (Select p.Inst_Id
                                     From Tt_Institution p
                                    Start With p.Inst_Id = 'CN0019001'
                                   Connect By Prior p.Inst_Id = p.p_Inst_Id)
           ORDER BY INST.INST_NAME) ST
	</select>
	
	
	<!-- 综合经营指标-指标数表 -->
	<select id="queryComprehensiveOper" parameterType="java.util.HashMap" resultType="com.singlee.capital.reports.model.ComprehensiveOperVo">
		 <![CDATA[
		 SELECT
                ST.instName,
                TRUNC(ST.sumgsyty/100000000,2) AS sumgsyty,
                TRUNC(ST.sumtyhq/100000000,2) AS sumtyhq,
                TRUNC(ST.sumzjyw/10000,2) AS sumzjyw,
                TRUNC(ST.sumpj/10000,2) AS sumpj
          FROM 
          (SELECT INST.INST_NAME AS instName,
                 SUM(CASE WHEN A.PRD_NO in ('308','907','306','288','299','295','293','291','290','287','246','286','292','298','903','904','905','906') AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}  
                          THEN A.AMT 
                          ELSE 0 END) AS sumgsyty,
                 SUM(CASE WHEN A.PRD_NO ='300' AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.AMT 
                          ELSE 0 END) AS sumtyhq,
                 SUM(CASE WHEN A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.FWFJS 
                          ELSE 0 END) AS sumzjyw,
                 SUM(CASE WHEN A.PRD_NO ='294' AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.AMT 
                          ELSE 0 END) AS sumpj
            FROM 
                 (SELECT NVL((SELECT SUM(CASE
                         WHEN FEE.REF_BEGIN_DATE < #{reportDate} AND FEE.REF_END_DATE > #{reportDate} 
                         THEN
                          (MA.AMT * FEE.EXECUTE_RATE * (to_date(#{reportDate},'yyyy-mm-dd')-to_date(FEE.REF_BEGIN_DATE,'yyyy-mm-dd'))) / CASE WHEN FEE.DAY_COUNTER='Actual/360' THEN 360 ELSE 365 END
                         WHEN FEE.REF_BEGIN_DATE < #{reportDate} AND FEE.REF_END_DATE < #{reportDate} 
                         THEN
                          (MA.AMT * FEE.EXECUTE_RATE * (to_date(FEE.REF_END_DATE,'yyyy-mm-dd')-to_date(FEE.REF_BEGIN_DATE,'yyyy-mm-dd'))) / CASE WHEN FEE.DAY_COUNTER='Actual/360' THEN 360 ELSE 365 END
                         ELSE
                          0
                       END) AS FWFJS
              FROM TD_CASHFLOW_FEE FEE
             WHERE FEE.DEAL_NO = MA.DEAL_NO
               AND FEE.VERSION = '0'),0) AS FWFJS,
                 MA.*
            FROM TD_PRODUCT_APPROVE_MAIN MA) A
            LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
            LEFT JOIN TT_INSTITUTION INST ON A.SPON_INST = INST.INST_ID
            LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
           WHERE TR.TRADE_STATUS='12'
 		     AND A.SPON_INST IN (Select p.Inst_Id
                                     From Tt_Institution p
                                    Start With p.Inst_Id = 'CN0019001'
                                   Connect By Prior p.Inst_Id = p.p_Inst_Id)             
           GROUP BY INST.INST_NAME ORDER BY INST.INST_NAME) ST
           
           Union all
           
           SELECT
                ST.instName,
                TRUNC(ST.sumgsyty/100000000,2) AS sumgsyty,
                TRUNC(ST.sumtyhq/100000000,2) AS sumtyhq,
                TRUNC(ST.sumzjyw/10000,2) AS sumzjyw,
                TRUNC(ST.sumpj/10000,2) AS sumpj
          FROM 
          (SELECT '合计' AS instName,
                 SUM(CASE WHEN A.PRD_NO in ('308','907','306','288','299','295','293','291','290','287','246','286','292','298','903','904','905','906') AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}  
                          THEN A.AMT 
                          ELSE 0 END) AS sumgsyty,
                 SUM(CASE WHEN A.PRD_NO ='300' AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.AMT 
                          ELSE 0 END) AS sumtyhq,
                 SUM(CASE WHEN A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.FWFJS 
                          ELSE 0 END) AS sumzjyw,
                 SUM(CASE WHEN A.PRD_NO ='294' AND A.DEAL_DATE <= #{reportDate} AND A.DEAL_DATE >= #{starDate}
                          THEN A.AMT 
                          ELSE 0 END) AS sumpj
            FROM 
                 (SELECT NVL((SELECT SUM(CASE
                         WHEN FEE.REF_BEGIN_DATE < #{reportDate} AND FEE.REF_END_DATE > #{reportDate} 
                         THEN
                          (MA.AMT * FEE.EXECUTE_RATE * (to_date(#{reportDate},'yyyy-mm-dd')-to_date(FEE.REF_BEGIN_DATE,'yyyy-mm-dd'))) / CASE WHEN FEE.DAY_COUNTER='Actual/360' THEN 360 ELSE 365 END
                         WHEN FEE.REF_BEGIN_DATE < #{reportDate} AND FEE.REF_END_DATE < #{reportDate} 
                         THEN
                          (MA.AMT * FEE.EXECUTE_RATE * (to_date(FEE.REF_END_DATE,'yyyy-mm-dd')-to_date(FEE.REF_BEGIN_DATE,'yyyy-mm-dd'))) / CASE WHEN FEE.DAY_COUNTER='Actual/360' THEN 360 ELSE 365 END
                         ELSE
                          0
                       END) AS FWFJS
              FROM TD_CASHFLOW_FEE FEE
             WHERE FEE.DEAL_NO = MA.DEAL_NO
               AND FEE.VERSION = '0'),0) AS FWFJS,
                 MA.*
            FROM TD_PRODUCT_APPROVE_MAIN MA) A
            LEFT JOIN TC_PRODUCT P ON A.PRD_NO = P.PRD_NO
            LEFT JOIN TT_INSTITUTION INST ON A.SPON_INST = INST.INST_ID
            LEFT JOIN TT_TRD_TRADE TR ON A.DEAL_NO = TR.TRADE_ID
           WHERE TR.TRADE_STATUS='12'
 		     AND A.SPON_INST IN (Select p.Inst_Id
                                     From Tt_Institution p
                                    Start With p.Inst_Id = 'CN0019001'
                                   Connect By Prior p.Inst_Id = p.p_Inst_Id)             
           ORDER BY INST.INST_NAME) ST
           
           ]]>
	</select>
</mapper>