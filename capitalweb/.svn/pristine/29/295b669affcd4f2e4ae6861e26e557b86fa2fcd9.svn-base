package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixDepositsAndLoans;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.PayFrequency;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.component.SettMode;
import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.IfsCfetsfxLend;
import com.singlee.ifs.model.IfsCfetsrmbIbo;
import com.singlee.ifs.model.IfsRmbDepositin;
import com.singlee.ifs.model.IfsRmbDepositout;
import com.singlee.ifs.service.IfsRmbDepOutService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 信用拆借业务处理类
 * <p>
 * ClassName: CfetsImixMM <br/>
 * Function: 落地信用拆借业务处理数据. <br/>
 * Reason: 审批+处理后送入OPICS系统. <br/>
 * date: 2018-5-30 下午03:59:25 <br/>
 *
 * @author shenzl
 * @since JDK 1.6
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class CfetsImixMM implements ICfetsImixDepositsAndLoans {
    @Autowired
    private IfsCfetsrmbIboMapper cfetsrmbIboMapper;
    @Autowired
    private IfsCfetsfxLendMapper cfetsfxLendMapper;
    @Autowired
    private IfsRmbDepositinMapper ifsRmbDepositinMapper;
    @Autowired
    private IfsRmbDepositoutMapper ifsRmbDepositoutMapper;

    @Autowired
    private CfetsRmbService cfetsService;
    @Autowired
    private IfsRmbDepOutService ifsRmbDepOutService;
    @Autowired
    private IfsCfetsPackMsgMapper cfetsPackMsgMapper;

    @Autowired
    private TradeSettlsMapper tradeSettlsMapper;

    private static Logger logger = LoggerFactory.getLogger(CfetsImixMM.class);

    @Override
    public RetBean send(DepositsAndLoansBean mmBean, CfetsPackMsgBean cfetsPackMsg) {

        try {
            logger.info("保存下行交易IBO报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易IBO报文异常:", e1);
        }

        RetBean rtb = null;
        logger.info("拆借输入对象：" + mmBean);
        try {
            // 信用拆借
            IfsCfetsrmbIbo ibo = new IfsCfetsrmbIbo();

            SettBaseBean borrow = mmBean.getSettInfo();
            SettBaseBean remove = mmBean.getContraPartySettInfo();

            ibo.setMyDir(mmBean.getPs() == null ? "" : mmBean.getPs().toString());
            ibo.setOppoDir(ibo.getMyDir().equals(PsEnum.P.name()) ? PsEnum.S.name() : PsEnum.P.name());

            // 交易模式
            if (mmBean.getDealMode() != null) {
                if (StringUtil.isNotEmpty(mmBean.getDealMode().toString())) {
                    if ("Enquiry".equals(mmBean.getDealMode().toString())) {// 询价
                        ibo.setTradingModel("02");
                    } else if ("Competition".equals(mmBean.getDealMode().toString())) {// 竟价
                        ibo.setTradingModel("01");
                    } else if ("Married".equals(mmBean.getDealMode().toString())) {
                        ibo.setTradingModel("05");
                    }
                }
            }

            // 交易状态
            if (mmBean.getStatus() != null) {
                ibo.setDealTransType(StringUtils.trimToEmpty(mmBean.getStatus().toString()));
            } else {
                ibo.setDealTransType("1");
            }
            // 外汇即期
            ibo.setDealSource("01");
            // 成交单编号
            ibo.setTicketId(cfetsService.getTicketId("MM"));
            // 成交单编号
            ibo.setContractId(mmBean.getExecId());
            // 交易日期
            ibo.setForDate(DateUtil.format(mmBean.getDealDate()));
            // 本方机构
            ibo.setBorrowInst(cfetsService.getLocalInstitution(ibo, mmBean.getInstitutionInfo().getInstitutionId(), null));
            // 本方交易员
            ibo.setBorrowTrader(mmBean.getInstitutionInfo().getTradeId());
            // 对手方机构
            ibo.setRemoveInst(
                    cfetsService.getLocalInstitution(ibo, mmBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
            // 对方交易员
            ibo.setRemoveTrader(mmBean.getContraPatryInstitutionInfo().getTradeId());
            // 录入时间
            ibo.setInputTime(mmBean.getTradeTime());
            // 金额
            ibo.setAmt(mmBean.getAmt().divide(BigDecimal.valueOf(new Double(10000))));
            // 利率
            ibo.setRate(mmBean.getRate());
            // 首次结算日
            ibo.setFirstSettlementDate(DateUtil.format(mmBean.getValueDate()));
            // 到期结算日
            ibo.setSecondSettlementDate(DateUtil.format(mmBean.getMaturityDate()));
            // 期限
            ibo.setTenor(mmBean.getTenor());

            //付息频率 付息方式
            ibo.setScheduleType("D");
            //利率差
            ibo.setSpread8(new BigDecimal(0));

            // 占款天数
            ibo.setOccupancyDays(BigDecimal.valueOf(new Double(mmBean.getHoldingDays().toString())));
            // 利息金额
            ibo.setAccuredInterest(mmBean.getInterestAmt());
            // 结算金额
            ibo.setSettlementAmount(mmBean.getMaturityAmt());
            // 交易品种
            ibo.setTradingProduct(mmBean.getKindId());
            // 清算方式
            ibo.setNettingStatus(
                    (SettMode.Netting.equals(mmBean.getSettMode()) || SettMode.Central.equals(mmBean.getSettMode()))
                            ? DictConstants.YesNo.YES
                            : DictConstants.YesNo.NO);
            // 拆入方信息
            ibo.setBorrowAccname(borrow.getAcctName());
            ibo.setBorrowOpenBank(borrow.getBankName());
            ibo.setBorrowAccnum(borrow.getAcctNo());
            ibo.setBorrowPsnum(borrow.getBankAcctNo());
            // 拆出方信息
            ibo.setRemoveAccname(remove.getAcctName());
            ibo.setRemoveOpenBank(remove.getBankName());
            ibo.setRemoveAccnum(remove.getAcctNo());
            ibo.setRemovePsnum(remove.getBankAcctNo());
            ibo.setApproveStatus("3");
            ibo.setApplyProd(TradeConstants.MarketIndicator.INTER_BANK_OFFERING);

            TaUser user = cfetsService.userMapping(ibo, ibo.getBorrowTrader());
            if (null == user) {
                ibo.setBorrowTrader(SystemProperties.cfetsDefautUserId);
                ibo.setBorrowInst(SystemProperties.cfetsDefautUserInstId);
            } else {
                ibo.setBorrowTrader(user.getUserId());
                ibo.setBorrowInst(user.getInstId());
            }
            ibo.setfPrdCode(TradeConstants.ProductCode.IBO);

            cfetsrmbIboMapper.insert(ibo);

            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.error("拆借下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }
        logger.info("拆借输出对象：" + rtb);

        return rtb;
    }

    @Override
    public RetBean send(DepositsAndLoansFxBean mmBean, CfetsPackMsgBean cfetsPackMsg)
            throws RemoteConnectFailureException, Exception {

        try {
            logger.info("保存下行交易FXLEND报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易FXLEND报文异常:", e1);
        }

        RetBean rtb = null;
        logger.info("拆借输入对象：" + mmBean);
        try {
            // 外币拆借
            IfsCfetsfxLend lend = new IfsCfetsfxLend();

            lend.setDirection(mmBean.getPs() == null ? "" : mmBean.getPs().toString());
            lend.setOppoDir(lend.getDirection().equals(PsEnum.P.name()) ? PsEnum.S.name() : PsEnum.P.name());
            // 交易模式
            if (mmBean.getDealMode() != null) {
                if (StringUtil.isNotEmpty(mmBean.getDealMode().toString())) {
                    if ("Enquiry".equals(mmBean.getDealMode().toString())) {// 询价
                        lend.setTradingModel("02");
                    } else if ("Competition".equals(mmBean.getDealMode().toString())) {// 竟价
                        lend.setTradingModel("01");
                    } else if ("Married".equals(mmBean.getDealMode().toString())) {
                        lend.setTradingModel("05");
                    }
                }
            }
            // 交易方式
            if (mmBean.getTradeMethod() != null) {
                String tradingType = StringUtils.trimToEmpty(mmBean.getTradeMethod().toString());
                lend.setTradingType(tradingType.equals("RFQ_NDM") ? "RFQ" : tradingType);
            }
            // 交易状态
            if (mmBean.getStatus() != null) {
                lend.setDealTransType(StringUtils.trimToEmpty(mmBean.getStatus().toString()));
            } else {
                lend.setDealTransType("1");
            }
            //期限代码
            lend.setTenorCode(mmBean.getSettlType());
            // 外汇即期
            lend.setDealSource("02");
            // 成交单编号
            lend.setTicketId(cfetsService.getTicketId("FXMM"));
            // 成交单编号
            lend.setContractId(mmBean.getExecId());
            //交易后确认标识
            lend.setCfetsCnfmIndicator(mmBean.getCfetsCnfmIndicator());

            // 交易日期
            lend.setForDate(DateUtil.format(mmBean.getDealDate()));
            lend.setForTime(mmBean.getTradeTime() == null ? DateUtil.getCurrentTimeAsString() : mmBean.getTradeTime());
            // 本方机构
            lend.setInstId(cfetsService.getLocalInstitution(lend, mmBean.getInstitutionInfo().getInstitutionId(), null));
            // 本方交易员
            lend.setDealer(mmBean.getInstitutionInfo().getTradeName());
            // 对手方机构
            lend.setCounterpartyInstId(
                    cfetsService.getLocalInstitution(lend, mmBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
            // 对方交易员
            lend.setCounterpartyDealer(mmBean.getContraPatryInstitutionInfo().getTradeName());
            // 录入时间
            lend.setInputTime(mmBean.getTradeTime());
            // 金额
            lend.setAmount(mmBean.getCurrencyInfo().getAmt());
            // 利率
            lend.setRate(mmBean.getCurrencyInfo().getRate());
            // 首次结算日
            lend.setValueDate(DateUtil.format(mmBean.getValueDate()));
            // 到期结算日
            lend.setMaturityDate(DateUtil.format(mmBean.getMatuitydate()));
            // 期限
            // lend.setTenor(mmBean.getSettlType());
            int days = DateUtil.daysBetween(mmBean.getValueDate(), mmBean.getMatuitydate());
            lend.setTenor(String.valueOf(days));

            // 占款天数
            lend.setOccupancyDays(BigDecimal.valueOf(new Double(mmBean.getCashHoldingDays().toString())));
            // 利息金额
            lend.setInterest(mmBean.getAccruedInterestTotal());
            // 结算金额
            lend.setRepaymentAmount(mmBean.getSettlCurrAmt2());
            if (mmBean.getSettlCurrAmt2() == null) {
                lend.setRepaymentAmount(mmBean.getCurrencyInfo().getAmt().add(mmBean.getAccruedInterestTotal()));
            }
            // 交易品种
//			 lend.setsetTradingProduct(mmBean.getKindId());
            // 计息基础
            lend.setBasis(mmBean.getBasis().toString());
            // 币种
            lend.setCurrency(mmBean.getCurrencyInfo().getCcy());

            // 结算方式
            lend.setDeliveryType(mmBean.getDeliveryType());


            // 拆入方信息
            // 本方清算币种
            lend.setBuyCurreny(mmBean.getCurrencyInfo().getCcy());
            // 开户行名称 110
            lend.setBankName1(mmBean.getSettlement().getBankName());
            // 开户行BIC 138
            lend.setBankBic1(mmBean.getSettlement().getBankOpenNo());
            // 收款行名称 23
            lend.setDueBank1(mmBean.getSettlement().getAcctName());
            // 收款行BIC CODE 16
            lend.setDueBankName1(mmBean.getSettlement().getAcctOpenNo());
            // 收款账号 15
            lend.setDueBankAccount1(mmBean.getSettlement().getAcctNo());
            // 中间行名称 209
            lend.setIntermediaryBankName1(
                    StringUtils.defaultIfEmpty(mmBean.getSettlement().getIntermediaryBankName(), ""));
            lend.setIntermediaryBank1(StringUtils.defaultIfEmpty(mmBean.getSettlement().getIntermediaryBankName(), ""));
            // 中间行BIC 211
            lend.setIntermediaryBankBicCode1(
                    StringUtils.defaultIfEmpty(mmBean.getSettlement().getIntermediaryBankBicCode(), ""));
            lend.setIntermediaryBicCode1(
                    StringUtils.defaultIfEmpty(mmBean.getSettlement().getIntermediaryBankBicCode(), ""));
            // 中间行行号 213
            lend.setIntermediaryBankAcctNo1(
                    StringUtils.defaultIfEmpty(mmBean.getSettlement().getIntermediaryBankAcctNo(), ""));
            // 资金开户行 207
            lend.setCapitalBank1(mmBean.getSettlement().getAcctName());
            lend.setBankAccount1(mmBean.getSettlement().getBankAcctNo());
            // 资金账户户名110
            lend.setCapitalBankName1(mmBean.getSettlement().getAcctName());
            // 支付系统行号
            lend.setCnapsCode1(mmBean.getSettlement().getAcctNo());
            // 资金账号
            lend.setCapitalAccount1(mmBean.getSettlement().getAcctNo());

            // 对手方清算币种
            lend.setSellCurreny(mmBean.getCurrencyInfo().getCcy());
            // 开户行名称 110
            lend.setBankName2(mmBean.getCounterPartySettlement().getBankName());
            // 开户行BIC 138
            lend.setBankBic2(mmBean.getCounterPartySettlement().getBankOpenNo());
            // 收款行名称 23
            lend.setDueBank2(mmBean.getCounterPartySettlement().getAcctName());
            // 收款行BIC CODE 16
            lend.setDueBankName2(mmBean.getCounterPartySettlement().getAcctOpenNo());
            // 收款账号 15
            lend.setDueBankAccount2(mmBean.getCounterPartySettlement().getAcctNo());
            // 中间行名称 209
            lend.setIntermediaryBankName2(
                    StringUtils.defaultIfEmpty(mmBean.getCounterPartySettlement().getIntermediaryBankName(), ""));
            lend.setIntermediaryBank2(
                    StringUtils.defaultIfEmpty(mmBean.getCounterPartySettlement().getIntermediaryBankName(), ""));
            // 中间行BIC 211
            lend.setIntermediaryBankBicCode2(
                    StringUtils.defaultIfEmpty(mmBean.getCounterPartySettlement().getIntermediaryBankBicCode(), ""));
            lend.setIntermediaryBicCode2(
                    StringUtils.defaultIfEmpty(mmBean.getCounterPartySettlement().getIntermediaryBankBicCode(), ""));
            // 中间行行号 213
            lend.setIntermediaryBankAcctNo2(
                    StringUtils.defaultIfEmpty(mmBean.getCounterPartySettlement().getIntermediaryBankAcctNo(), ""));
            // 资金开户行 207
            lend.setCapitalBank2(mmBean.getCounterPartySettlement().getAcctName());
            lend.setBankAccount2(mmBean.getCounterPartySettlement().getBankAcctNo());
            // 资金账户户名110
            lend.setCapitalBankName2(mmBean.getCounterPartySettlement().getAcctName());
            // 支付系统行号
            lend.setCnapsCode2(mmBean.getCounterPartySettlement().getBankAcctNo());
            // 资金账号
            lend.setCapitalAccount2(mmBean.getCounterPartySettlement().getAcctNo());

            lend.setApproveStatus("3");
            //外币拆借产品号
            lend.setfPrdCode(TradeConstants.ProductCode.CCYLENDING);
            TaUser user = cfetsService.userMapping(lend, lend.getDealer());
            if (null == user) {
                lend.setDealer(SystemProperties.cfetsDefautUserId);
                lend.setInstId(SystemProperties.cfetsDefautUserInstId);
            } else {
                lend.setDealer(user.getUserId());
                lend.setInstId(user.getInstId());
            }
            cfetsfxLendMapper.addCcyLending(lend);

            //cfets 清算路径保存
            TradeSettlsBean settlsBean = tradeSettls(mmBean);
            settlsBean.setDealNo(lend.getTicketId());
            settlsBean.setTradeType("LEND");
            tradeSettlsMapper.insert(settlsBean);

            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.error("拆借下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }
        logger.info("拆借输出对象：" + rtb);

        return rtb;
    }

    /**
     * 拆借清算路径
     *
     * @param mmBean
     * @return
     */
    private TradeSettlsBean tradeSettls(DepositsAndLoansFxBean mmBean) {
        TradeSettlsBean bean = new TradeSettlsBean();
        //近端清算货币
        bean.setSettccy(mmBean.getCurrencyInfo().getCcy());
        // 远端清算币种
        bean.setcSettccy(mmBean.getCurrencyInfo().getContraCcy());

        if (SettMode.Netting.equals(mmBean.getSettMode())) {
            // 1-双边净额清算
            bean.setNetGrossInd(String.valueOf(1));
        } else if (SettMode.Normal.equals(mmBean.getSettMode())) {
            // 2-双边全额清算
            bean.setNetGrossInd(String.valueOf(2));
        } else if (SettMode.Central.equals(mmBean.getSettMode())) {
            // 3-集中清算
            bean.setNetGrossInd(String.valueOf(3));
        } else if (SettMode.SGE.equals(mmBean.getSettMode())) {
            // 4-应急录入
            bean.setNetGrossInd(String.valueOf(4));
        }


        //市场标识
        bean.setMarketIndicator("21");
        //成交单号
        bean.setExecId(mmBean.getExecId());
        // 本方机构 21位码,
        bean.setInstitutionId(mmBean.getInstitutionInfo().getInstitutionId());
        // 本方交易员编号
        bean.setTradeId(mmBean.getInstitutionInfo().getTradeId());
        // 本方交易员名称
        bean.setTradeName(mmBean.getInstitutionInfo().getTradeName());
        // 本方机构简称
        bean.setShortName(mmBean.getInstitutionInfo().getShortName());
        // 本方机构全称
        bean.setFullName(mmBean.getInstitutionInfo().getFullName());
        // 本方机构角色
        bean.setMarkerTakerTole(mmBean.getSettlement().getMarkerTakerRole() == null ? "" : mmBean.getSettlement().getMarkerTakerRole().toString());

        // 对手方机构 21位码,
        bean.setcInstitutionId(mmBean.getContraPatryInstitutionInfo().getInstitutionId());
        // 对手方交易员编号
        bean.setcIradeId(mmBean.getContraPatryInstitutionInfo().getTradeId());
        // 对手交易员名称
        bean.setcIradeName(mmBean.getContraPatryInstitutionInfo().getTradeName());
        // 对手机构简称
        bean.setcShortName(mmBean.getContraPatryInstitutionInfo().getShortName());
        // 对手机构全称
        bean.setcFullName(mmBean.getContraPatryInstitutionInfo().getFullName());
        // 对手机构角色
        bean.setcMarkerTakerTole(mmBean.getCounterPartySettlement().getMarkerTakerRole() == null ? "" : mmBean.getCounterPartySettlement().getMarkerTakerRole().toString());
        bean.setDealNo("");

        //近端 清算信息
        covertNear(bean, mmBean.getSettlement(), mmBean.getCounterPartySettlement());

        return bean;
    }

    /**
     * 近端转化
     *
     * @param bean
     * @param settlement
     * @param counterPartySettlement
     */
    private static void covertNear(TradeSettlsBean bean, FxSettBean settlement, FxSettBean counterPartySettlement) {

        //近端 本方 开户行SWIFT CODE(外币)/开户行行号(人民币)
        bean.setBankopenno(settlement.getBankOpenNo());
        //近端 本方 账户行SWIFT CODE/支付系统行号
        bean.setAcctopenno(settlement.getAcctOpenNo());
        //近端 本方 中间行名称
        bean.setIntermediarybankname(settlement.getIntermediaryBankName());
        //近端 本方 中间行SWIFT CODE
        bean.setIntermediarybankbiccode(settlement.getIntermediaryBankBicCode());
        //近端 本方 中间行在开户行的资金账户
        bean.setIntermediarybankacctno(settlement.getIntermediaryBankAcctNo());
        //近端 本方 附言
        bean.setRemark(settlement.getRemark());
        //近端 本方 开户行名称
        bean.setBankname(settlement.getBankName());
        //近端 本方 支付系统行号
        bean.setBankacctno(settlement.getBankAcctNo());
        //近端 本方 账户名称
        bean.setAcctname(settlement.getAcctName());
        //近端 本方 资金账号
        bean.setAcctno(settlement.getAcctNo());


        // 近端 对手方 开户行SWIFT CODE(外币)/开户行行号(人民币)
        bean.setcBankopenno(counterPartySettlement.getBankOpenNo());
        //近端 对手方 账户行SWIFT CODE/支付系统行号
        bean.setcAcctopenno(counterPartySettlement.getAcctOpenNo());
        //近端 对手方 中间行名称
        bean.setcIntermediarybankname(counterPartySettlement.getIntermediaryBankName());
        //近端 对手方 中间行SWIFT CODE
        bean.setcIntermediarybankbiccode(counterPartySettlement.getIntermediaryBankBicCode());
        //近端 对手方 中间行在开户行的资金账户
        bean.setcIntermediarybankacctno(counterPartySettlement.getIntermediaryBankAcctNo());
        //近端 对手方 附言
        bean.setcRemark(counterPartySettlement.getRemark());

        //近端 对手方 开户行名称
        bean.setcBankname(counterPartySettlement.getBankName());
        //近端 对手方 支付系统行号
        bean.setcBankacctno(counterPartySettlement.getBankAcctNo());
        //近端 对手方 账户名称
        bean.setcAcctname(counterPartySettlement.getAcctName());
        //近端 对手方 资金账号
        bean.setcAcctno(counterPartySettlement.getAcctNo());
    }


    @Override
    public RetBean send(InterBankDepositBean mmBean, CfetsPackMsgBean cfetsPackMsg)
            throws RemoteConnectFailureException, Exception {
        try {
            logger.info("保存下行交易同业存款报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易同业存款报文异常:", e1);
        }

        RetBean rtb = null;
        logger.info("同业存款输入对象：" + mmBean);
        try {
            if (mmBean.getPs().equals(PsEnum.P)) {//本方存入  同业存放
                IfsRmbDepositin irdin = new IfsRmbDepositin();

                // 交易状态
                if (mmBean.getStatus() != null) {
                    irdin.setDealTransType(StringUtils.trimToEmpty(mmBean.getStatus().toString()));
                } else {
                    irdin.setDealTransType("1");
                }

                // 成交单编号
                irdin.setTicketId(cfetsService.getTicketId("IBD"));
                // 成交单编号
                irdin.setContractId(mmBean.getExecId());
                // 本方交易员
                irdin.setDealer(mmBean.getInstitutionInfo().getTradeName());
                // 对手方机构
                irdin.setCustId(cfetsService.getLocalInstitution(irdin, mmBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
                // 交易日期
                irdin.setForDate(DateUtil.format(mmBean.getDealDate()));
                // 利率
                irdin.setRate(mmBean.getRate());
                //币种
                irdin.setCurrency("CNY");
                //点差
                irdin.setBenchmarkSpread(new BigDecimal("0"));
                // 计息基础
                irdin.setBasis("A360");
                // 协议开始日期
                irdin.setFirstSettlementDate(DateUtil.format(mmBean.getValueDate()));
                // 协议到期日期
                irdin.setSecondSettlementDate(DateUtil.format(mmBean.getMaturityDate()));
                // 存款期限
                irdin.setTenor(mmBean.getTenor());
                // 实际占款天数
                irdin.setOccupancyDays(mmBean.getHoldingDays());
                //本金
                irdin.setAmt(mmBean.getAmt());
                // 到期金额(元)
                irdin.setSettlementAmount(mmBean.getMaturityAmt());
                // 到期利息(元)
                irdin.setAccuredInterest(mmBean.getInterestAmt());
                //付息类型
                if (mmBean.getPayFrequency().equals(PayFrequency.ONCE)) {
                    irdin.setScheduleType("D");//到期一次性还本
                } else {
                    irdin.setScheduleType("IR");//按期付息
                    //付息频率
                    irdin.setPaymentFrequency(mmBean.getPayFrequency().name());
                }
                // 首次结息日
                irdin.setFirstSettlDay(mmBean.getFirstSettleDate());
                // 首次付息日
                irdin.setFirstPayDay(mmBean.getFirstPayDate());
                // 结息日约定
                irdin.setSettlDayAgreement(mmBean.getSettlDayAgreement());
                // 付息日约定
                irdin.setPayDayAgreement(mmBean.getPayDayAgreement());

                // 提前支取约定
                irdin.setAdvAgreement(mmBean.getAdvanceWithDrawAgreement());
                // 提前支取预期利率
                if (!"1".equals(mmBean.getAdvanceWithDrawAgreement())) { //不为 1不允许提前支取
                    irdin.setAdvRate(mmBean.getExpYield());
                } else {
                    irdin.setAdvRate(new BigDecimal("0"));
                }

                // 付款方信息
                irdin.setSelfAcccode(mmBean.getSettInfo().getAcctNo());
                irdin.setSelfAccname(mmBean.getSettInfo().getAcctName());
                irdin.setSelfBankcode(mmBean.getSettInfo().getBankAcctNo());
                irdin.setSelfBankname(mmBean.getSettInfo().getBankName());

                // 收款方信息
                irdin.setPartyAcccode(mmBean.getContraPartySettInfo().getAcctNo());
                irdin.setPartyAccname(mmBean.getContraPartySettInfo().getAcctName());
                irdin.setPartyBankcode(mmBean.getContraPartySettInfo().getBankAcctNo());
                irdin.setPartyBankname(mmBean.getContraPartySettInfo().getBankName());


                irdin.setApproveStatus("3");
                //同业存放产品号
                irdin.setfPrdCode(TradeConstants.ProductCode.RMBDEPIN);
                TaUser user = cfetsService.userMapping(irdin, irdin.getDealer());
                if (null == user) {
                    irdin.setDealer(SystemProperties.cfetsDefautUserId);
                    irdin.setInstId(SystemProperties.cfetsDefautUserInstId);
                } else {
                    irdin.setDealer(user.getUserId());
                    irdin.setInstId(user.getInstId());
                }
                ifsRmbDepositinMapper.insertSelective(irdin);


            } else if (mmBean.getPs().equals(PsEnum.S)) {//本方存出 存放同业
                IfsRmbDepositout irdout = new IfsRmbDepositout();

                // 交易状态
                if (mmBean.getStatus() != null) {
                    irdout.setDealTransType(StringUtils.trimToEmpty(mmBean.getStatus().toString()));
                } else {
                    irdout.setDealTransType("1");
                }

                // 成交单编号
                irdout.setTicketId(cfetsService.getTicketId("IBD"));
                // 成交单编号
                irdout.setContractId(mmBean.getExecId());
                // 本方交易员
                irdout.setDealer(mmBean.getInstitutionInfo().getTradeName());
                // 对手方机构
                irdout.setCustId(cfetsService.getLocalInstitution(irdout, mmBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
                // 交易日期
                irdout.setForDate(DateUtil.format(mmBean.getDealDate()));
                // 利率
                irdout.setRate(mmBean.getRate());
                //点差
                irdout.setBenchmarkSpread(new BigDecimal("0"));
                //币种
                irdout.setCurrency("CNY");
                // 计息基础
                irdout.setBasis("A360");
                // 协议开始日期
                irdout.setFirstSettlementDate(DateUtil.format(mmBean.getValueDate()));
                // 协议到期日期
                irdout.setSecondSettlementDate(DateUtil.format(mmBean.getMaturityDate()));
                // 存款期限
                irdout.setTenor(mmBean.getTenor());
                // 实际占款天数
                irdout.setOccupancyDays(mmBean.getHoldingDays());
                //本金
                irdout.setAmt(mmBean.getAmt());
                // 到期金额(元)
                irdout.setSettlementAmount(mmBean.getMaturityAmt());
                // 到期利息(元)
                irdout.setAccuredInterest(mmBean.getInterestAmt());
                //付息类型
                if (mmBean.getPayFrequency().equals(PayFrequency.ONCE)) {
                    irdout.setScheduleType("D");//到期一次性还本
                } else {
                    irdout.setScheduleType("IR");//按期付息
                    //付息频率
                    irdout.setPaymentFrequency(mmBean.getPayFrequency().name());
                }
                // 首次结息日
                irdout.setFirstSettlDay(mmBean.getFirstSettleDate());
                // 首次付息日
                irdout.setFirstPayDay(mmBean.getFirstPayDate());
                // 结息日约定
                irdout.setSettlDayAgreement(mmBean.getSettlDayAgreement());
                // 付息日约定
                irdout.setPayDayAgreement(mmBean.getPayDayAgreement());
                // 提前支取约定
                irdout.setAdvAgreement(mmBean.getAdvanceWithDrawAgreement());
                // 提前支取预期利率
                if (!"1".equals(mmBean.getAdvanceWithDrawAgreement())) { //不为 1不允许提前支取
                    irdout.setAdvRate(mmBean.getExpYield());
                } else {
                    irdout.setAdvRate(new BigDecimal("0"));
                }

                // 付款方信息
                irdout.setSelfAcccode(mmBean.getSettInfo().getAcctNo());
                irdout.setSelfAccname(mmBean.getSettInfo().getAcctName());
                irdout.setSelfBankcode(mmBean.getSettInfo().getBankAcctNo());
                irdout.setSelfBankname(mmBean.getSettInfo().getBankName());

                // 收款方信息
                irdout.setPartyAcccode(mmBean.getContraPartySettInfo().getAcctNo());
                irdout.setPartyAccname(mmBean.getContraPartySettInfo().getAcctName());
                irdout.setPartyBankcode(mmBean.getContraPartySettInfo().getBankAcctNo());
                irdout.setPartyBankname(mmBean.getContraPartySettInfo().getBankName());


                irdout.setApproveStatus("3");
                //同业存放产品号
                irdout.setfPrdCode(TradeConstants.ProductCode.RMBDEPOUT);
                TaUser user = cfetsService.userMapping(irdout, irdout.getDealer());
                if (null == user) {
                    irdout.setDealer(SystemProperties.cfetsDefautUserId);
                    irdout.setInstId(SystemProperties.cfetsDefautUserInstId);
                } else {
                    irdout.setDealer(user.getUserId());
                    irdout.setInstId(user.getInstId());
                }

                ifsRmbDepositoutMapper.insertSelective(irdout);

                //生成现金流
                ifsRmbDepOutService.generateCashFlow(irdout);
            }


            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.error("同业存款下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }
        logger.info("同业存款输出对象：" + rtb);
        return rtb;
    }

    @Override
    public RetBean send(InterBankLoansBean mmBean, CfetsPackMsgBean cfetsPackMsg)
            throws RemoteConnectFailureException, Exception {
        RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        return rtb;
    }
}
