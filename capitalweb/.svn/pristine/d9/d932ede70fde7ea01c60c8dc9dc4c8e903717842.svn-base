﻿<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.hrbreport.mapper.DldtReportMapper">
	
	<resultMap id="resultMap" type="com.singlee.hrbreport.model.DldtPo" >
		<id column ="DEALNO"  property="dealNo" jdbcType="VARCHAR" />
		<result column ="PRODTYPE"  property="prodType" jdbcType="VARCHAR" />
		<result column ="COSTDESC"  property="costDesc" jdbcType="VARCHAR" />
		<result column ="CFN1"  property="cname" jdbcType="VARCHAR" />
		<result column ="ACCTDESC"  property="acctDesc" jdbcType="VARCHAR" />
		<result column ="ACCTTYPE"  property="acctType" jdbcType="VARCHAR" />
		<result column ="DESCR"  property="descr" jdbcType="VARCHAR" />
		<result column ="VDATE"  property="vDate" jdbcType="VARCHAR" />
		<result column ="TERM"  property="term" jdbcType="VARCHAR" />
		<result column ="MDATE"  property="mDate" jdbcType="VARCHAR" />
		<result column ="INTRATE"  property="intrate" jdbcType="VARCHAR" />
		<result column ="CCYAMTABS"  property="ccyAmtAbs" jdbcType="VARCHAR" />
		<result column ="CCYAMT"  property="ccyAmt" jdbcType="VARCHAR" />
		<result column ="TOTPAYAMT"  property="totpayAmt" jdbcType="VARCHAR" />
		<result column ="TOTALINT"  property="totaLint" jdbcType="VARCHAR" />
		<result column ="OCCUPYAMT"  property="occupyAmt" jdbcType="VARCHAR" />
		<result column ="OVERTERM"  property="overTerm" jdbcType="VARCHAR" />
		<result column ="RPLINT"  property="rpLint" jdbcType="VARCHAR" />
		<result column ="OVERLINT"  property="overLint" jdbcType="VARCHAR" />
		<result column ="PROLOSS"  property="proLoss" jdbcType="VARCHAR" />

		<result column ="CCY"  property="ccy" jdbcType="VARCHAR" />
		<result column ="ACCROUTST"  property="accroutst" jdbcType="VARCHAR" />
		
	</resultMap>
	
	<select id="getDldtReportPage" parameterType="map" resultMap="resultMap">
		SELECT DL.DEALNO,
		       DL.PRODTYPE,
		       TRIM(CO.COSTDESC) COSTDESC,
		       TRIM(CU.CFN1) CFN1,
		       AC.ACCTDESC,
			   DL.CCY,
		       CASE WHEN CO.COSTDESC LIKE '%流动性%' THEN '流动性'
		            WHEN CO.COSTDESC LIKE '%投资性%' THEN '投资性' ELSE ''
		       END AS ACCTTYPE,
		       TRIM(TY.DESCR) DESCR,
		       TO_CHAR(DL.VDATE, 'yyyy-MM-dd') AS VDATE,
		       TO_CHAR((DL.MDATE - DL.VDATE), 'FM99,999,999,999,999,999,990') AS TERM,
		       TO_CHAR(DL.MDATE, 'yyyy-MM-dd') AS MDATE,
		       TO_CHAR(DL.INTRATE, 'FM99,999,999,999,999,999,990.0000') AS INTRATE,
		       ABS(DL.CCYAMT) AS CCYAMTABS,
		       (-DL.CCYAMT) CCYAMT,
		       SC.TOTPAYAMT,
		       (SC.TOTPAYAMT - DL.CCYAMT) AS TOTALINT,
		       ABS(CASE WHEN TO_DATE(#{yrMonth}, 'MM-dd') - TO_DATE('01-01', 'MM-dd') + 1 = 0 THEN  DL.CCYAMT * (DL.MDATE1 - DL.VDATE1)
		                ELSE ((DL.CCYAMT * (DL.MDATE - DL.VDATE)) / (TO_DATE(#{yrMonth}, 'MM-dd') - TO_DATE('01-01', 'MM-dd') + 1))
		       END) AS OCCUPYAMT,
		       CASE WHEN <![CDATA[(DL.MDATE - TO_DATE(#{edate}, 'yyyy-MM-dd') < 0) OR 
		                 (DL.MDATE - TO_DATE(#{edate}, 'yyyy-MM-dd') - 1 < 0) THEN 0]]>
		            ELSE DL.MDATE - TO_DATE(#{edate}, 'yyyy-MM-dd') - 1
		       END AS OVERTERM,
		       FI.ACCROUTST,
		       CASE WHEN <![CDATA[DL.MDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd') THEN 0 ]]>
		            ELSE FI.ACCROUTST
		       END AS RPLINT,
		       CASE WHEN <![CDATA[MDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd') THEN  0]]> 
		            ELSE (SC.TOTPAYAMT - DL.CCYAMT - FI.ACCROUTST)
		       END AS OVERLINT,
		       CASE WHEN <![CDATA[MDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd') THEN (SC.TOTPAYAMT - DL.CCYAMT - FI.YTDPINCEXP)]]>
		            ELSE (FI.ACCROUTST - FI.YTDPINCEXP)
		       END AS PROLOSS
		FROM (
		     SELECT DEALNO,COST,CNO,PRODUCT,CCY,REVDATE,BR,SEQ,PRODTYPE,VDATE,MDATE,INTRATE,CCYAMT,
		            CASE WHEN <![CDATA[VDATE < TO_DATE(#{sdate}, 'yyyy-MM-dd')]]> THEN TO_DATE(#{sdate}, 'yyyy-MM-dd')
		                 ELSE VDATE END AS VDATE1,
		            CASE WHEN <![CDATA[MDATE > TO_DATE(#{edate}, 'yyyy-MM-dd')]]> THEN TO_DATE(#{edate}, 'yyyy-MM-dd')
		                 ELSE MDATE  END AS MDATE1
		     FROM ${OPICS}.DLDT
		     WHERE <![CDATA[(VDATE >= TO_DATE(#{sdate}, 'yyyy-MM-dd') AND  VDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd'))
		            OR (MDATE >= TO_DATE(#{sdate}, 'yyyy-MM-dd') AND MDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd'))
		            OR (VDATE < TO_DATE(#{sdate}, 'yyyy-MM-dd') AND MDATE > TO_DATE(#{edate}, 'yyyy-MM-dd'))]]>
		) DL LEFT JOIN (SELECT * FROM ${OPICS}.SCHD WHERE SCHDTYPE = 'M') SC ON DL.DEALNO = SC.DEALNO
		LEFT JOIN ${OPICS}.COST CO ON DL.COST = CO.COSTCENT
		LEFT JOIN ${OPICS}.CUST CU ON DL.CNO = CU.CNO
		LEFT JOIN ${OPICS}.ACTY AC ON CU.ACCTNGTYPE = AC.ACCTNGTYPE
		LEFT JOIN ${OPICS}.TYPE TY ON DL.PRODTYPE = TY.TYPE AND DL.PRODUCT = TY.PRODCODE
		LEFT JOIN ${OPICS}.SL_FIND_H FI ON TRIM(DL.BR) = TRIM(FI.BR)
		          AND TRIM(DL.DEALNO) = TRIM(FI.DEALNO)
		          AND TRIM(DL.SEQ) = TRIM(FI.SEQ)
		          AND TRIM(DL.PRODUCT) = TRIM(FI.PRODUCT)
		          AND TRIM(DL.PRODTYPE) = TRIM(FI.PRODTYPE)
		          AND TRIM(FI.INCEXPTYPE) = 'A'
		          AND TRIM(FI.CCY) = #{ccy}
		          AND FI.POSTDATE = TO_DATE(#{edate}, 'yyyy-MM-dd')
		WHERE DL.CCY = #{ccy}
		      AND DL.REVDATE IS NULL
			  <if test="type != null and type != ''">
		      		AND DL.PRODTYPE = #{type}
			  </if>
			  <if test="br != null and br != ''">
		     		AND DL.BR = #{br}	 
			  </if> 
	</select>
	
	<select id="getRateList" parameterType="map" resultType="map">
		SELECT *
		FROM (
		     SELECT *
		     FROM ${OPICS}.K_WABACUS_TAXATION
		     WHERE <![CDATA[CREATEDATE <= TO_DATE(#{edate}, 'yyyy-MM-dd')]]>
		           ORDER BY CREATEDATE DESC
		) WHERE ROWNUM = 1
	</select>
	
</mapper>