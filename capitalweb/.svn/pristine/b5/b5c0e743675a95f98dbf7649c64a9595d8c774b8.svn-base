<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.ordersorting.mapper.TdOrdersortingApproveMapper">
	<resultMap id="ref-map"
		type="com.singlee.capital.ordersorting.model.TdOrdersortingApprove">
		<id property="dealNo" column="DEAL_NO" />
		<result property="prdNo" column="PRD_NO" />
		<result property="dealType" column="DEAL_TYPE" />
		<result property="sponsor" column="SPONSOR" />
		<result property="sponInst" column="SPON_INST" />
		<result property="aDate" column="A_DATE" />
		<result property="versio" column="VERSIO" />
		<result property="approveStatus" column="APPROVE_STATUS" />
		<result property="refNo" column="REF_NO" />
		<result property="cno" column="CNO" />
		<result property="refPrd" column="REF_PRD" />
		<result property="refAmt" column="REF_AMT" />
		<result property="flowType" column="FLOW_TYPE" />
		<result property="taskId" column="TASK_ID" />
		<result property="taskName" column="TASK_NAME" />
		<result property="cDealNo" column="C_DEAL_NO" />
		<result property="lstDate" column="LST_DATE" />
		<result property="prdName" column="PRD_NAME" />
		<result property="cnName" column="PARTY_NAME" />
		<result property="ccy" column="CCY" />
		<association property="user" javaType="com.singlee.capital.system.model.TaUser">
			<id property="userId" column="SPONSOR" />
			<result property="userName" column="USER_NAME" />
		</association>
		<association property="institution" javaType="com.singlee.capital.system.model.TtInstitution">
			<id property="instId" column="SPON_INST" />
			<result property="instName" column="INST_NAME" />
		</association>
	</resultMap>
	
	<sql id="columns">
		${alias}.*,
		P.PRD_NAME,
		U.USER_NAME,
		I.INST_NAME,
		TTC.PARTY_NAME
	</sql>
	
	<sql id="join">
		LEFT OUTER JOIN TC_PRODUCT P
		  ON ${alias}.REF_PRD = P.PRD_NO
		LEFT OUTER JOIN TA_USER U
		  ON ${alias}.SPONSOR = U.USER_ID
		LEFT OUTER JOIN TT_INSTITUTION I
		  ON ${alias}.SPON_INST = I.INST_ID
		LEFT OUTER JOIN TT_TRD_COUNTERPARTY TTC
		  ON ${alias}.CNO = TTC.PARTY_ID
	</sql>
	
	<select id="searchTdOrdersortingApprove" parameterType="java.util.Map" resultMap="ref-map">
		SELECT * FROM(
		  SELECT <include refid="columns"><property name="alias" value="T"/></include>,S.TASK_ID,S.FLOW_TYPE
		  FROM TD_ORDERSORTING_APPROVE T
		  	INNER JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) S
			ON T.DEAL_NO = S.SERIAL_NO
		  	<include refid="join"><property name="alias" value="T"/></include>
		WHERE 1=1
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
		<!-- 新增的branchId 条件查询 pjx （所有的branchId都是新添加的）-->
		<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
		</if>
		<if test="aDate != null and aDate != ''">
			AND T.A_DATE=#{aDate}
		</if>
		<if test="approveStatus != null and approveStatus != ''">
			AND	T.APPROVE_STATUS in
			<foreach item="item" index="index" collection="approveStatus"
				open="(" separator="," close=")">
				#{item}
			</foreach>
		</if>
		<if test="institutionList != null and institutionList != ''">
			AND	T.SPON_INST in
			<foreach item="item" index="index" collection="institutionList"
				open="(" separator="," close=")">
				#{item}
			</foreach>
		</if>)
		ORDER BY LST_DATE DESC
	</select>
	
	<select id="searchTdOrdersortingApproveFinished" parameterType="map" resultMap="ref-map">
		SELECT * FROM(
			 SELECT <include refid="columns"><property name="alias" value="T"/></include>
		  	 FROM TD_ORDERSORTING_APPROVE T
			INNER JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_completed_task"></include>) S
			ON T.DEAL_NO = S.SERIAL_NO
			<include refid="join"><property name="alias" value="T"/></include>
		WHERE 1 = 1
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
		<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
		</if>
		<if test="aDate != null and aDate != ''">
			AND T.A_DATE =#{dDate}
		</if>
		<if test="refNo !=null and refNo !=''">
		    AND T.REF_NO =#{refNo}
		</if>
		<if test="institutionList != null and institutionList != ''">
		  AND T.SPON_INST IN
         	 <foreach collection="institutionList" index="index" item="item" open="(" separator="," close=")">
		 		#{item}
			 </foreach>
		</if>
		<if test="approveStatus != null and approveStatus != ''">
			AND	T.APPROVE_STATUS in
			<foreach item="item" index="index" collection="approveStatus"
				open="(" separator="," close=")">
				#{item}
			</foreach>
		</if>
		<if test="type != null and type != ''">
			AND EXISTS (SELECT 1 FROM TD_ORDERSORTING_DETAIL WHERE DEAL_NO = T.DEAL_NO AND rev_Type=#{type})
		</if>)
		ORDER BY LST_DATE DESC
	</select>
	
	<select id="searchTdOrdersortingApproveMySelf" parameterType="map" resultMap="ref-map">
		SELECT * FROM (
			SELECT <include refid="columns"><property name="alias" value="T"/></include>
		  	FROM TD_ORDERSORTING_APPROVE T
			<include refid="join"><property name="alias" value="T"/></include>
			WHERE 1=1
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
		<if test="branchId != null and branchId != ''">
				AND U.BRANCH_ID = #{branchId}
		</if>
		<if test="institutionList != null and institutionList != ''">
		  AND T.SPON_INST IN
         	 <foreach collection="institutionList" index="index" item="item" open="(" separator="," close=")">
		 		#{item}
			 </foreach>
		</if>
		<if test="aDate != null and aDate != ''">
			AND T.A_DATE =#{aDate}
		</if>
		<if test="userId != null and userId != ''">
			AND T.SPONSOR LIKE  '%'||#{userId}||'%'
		</if>
		<if test="refNo !=null and refNo !=''">
		    AND T.REF_NO =#{refNo}
		</if>
		<if test="type != null and type != ''">
			AND EXISTS (SELECT 1 FROM TD_ORDERSORTING_DETAIL WHERE DEAL_NO = T.DEAL_NO AND (rev_Type=#{type} or rev_Type='4' ))
		</if>
		)
		ORDER BY LST_DATE DESC
	</select>
	
	<update id="updateTdOrdersortingApproveStatus" parameterType="map">
		UPDATE TD_ORDERSORTING_APPROVE T SET
		T.APPROVE_STATUS = #{approveStatus}
		WHERE T.DEAL_NO  = #{dealNo}
	</update>
	
	<select id="getTdOrdersortingApproveById" parameterType="map" resultMap="ref-map">
		SELECT * FROM TD_ORDERSORTING_APPROVE T WHERE 1=1
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
	</select>
	
	<select id="searchTdOrdersortingApproveByType" parameterType="map" resultMap="ref-map">
		SELECT a.* FROM(
		SELECT <include refid="columns"><property name="alias" value="T"/></include>
		FROM TD_ORDERSORTING_APPROVE T 
			LEFT JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) S
			ON T.DEAL_NO = S.SERIAL_NO
			<include refid="join"><property name="alias" value="T"/></include>
		WHERE 1=1
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
		<if test="dDate != null and dDate != ''">
			AND T.DEAL_DATE LIKE  '%'||#{dDate}||'%'
		</if>
		<if test="approveStatus != null and approveStatus != ''">
			AND	T.APPROVE_STATUS in
			<foreach item="item" index="index" collection="approveStatus"
				open="(" separator="," close=")">
				#{item}
			</foreach>
		</if>)a left join Td_Ordersorting_Detail b on a.deal_no=b.deal_no where b.rev_Type=#{type}
	</select>
	
	<select id="searchTdOrdersortingApproveUnfinished" parameterType="map" resultMap="ref-map">
		SELECT * FROM(
		SELECT <include refid="columns"><property name="alias" value="T"/></include>,S.TASK_ID,S.FLOW_TYPE
		FROM TD_ORDERSORTING_APPROVE T 
			INNER JOIN (<include refid="com.singlee.slbpm.mapper.TtFlowTaskMapper.flow_task"></include>) S
			ON T.DEAL_NO = S.SERIAL_NO
			<include refid="join"><property name="alias" value="T"/></include>
		WHERE 1=1
		<if test="refNo != null and refNo != ''">
			AND T.REF_NO LIKE  '%'||#{refNo}||'%'
		</if>
		<if test="dealNo != null and dealNo != ''">
			AND T.DEAL_NO LIKE  '%'||#{dealNo}||'%'
		</if>
		<if test="aDate != null and aDate != ''">
			AND T.A_DATE = #{aDate}
		</if>
		<if test="approveStatus != null and approveStatus != ''">
			AND	T.APPROVE_STATUS in
			<foreach item="item" index="index" collection="approveStatus"
				open="(" separator="," close=")">
				#{item}
			</foreach>
		</if>
		<if test="type != null and type != ''">
			AND EXISTS (SELECT 1 FROM TD_ORDERSORTING_DETAIL WHERE DEAL_NO = T.DEAL_NO AND (rev_Type=#{type} or rev_Type='4'))
		</if>
		)
		order by LST_DATE desc
	</select>
</mapper>