package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 出账交易冲销
 * @author 倪航
 * @createDate 2016-9-25 上午12:00:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_TRADE_OFFSET")
public class TdTradeOffset implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3720610986116412801L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;

	/**
	 * 账务日期
	 */
	private String accountingDate;

	/**
	 * 撤销原因
	 */
	private String offsetReason;
	/**
	 * 影像资料
	 */
	private String image;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	/**
	 * 合约号
	 */
	private String iCode;
	/**
	 * 资产类型
	 */
	private String aType;
	/**
	 * 市场类型
	 */
	private String mType;
	/**
	 * 版本号
	 */
	private int version;

	/**
	 * 提前到期备注
	 */
	private String remark;
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getAccountingDate() {
		return accountingDate;
	}
	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}
	public String getOffsetReason() {
		return offsetReason;
	}
	public void setOffsetReason(String offsetReason) {
		this.offsetReason = offsetReason;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
}
