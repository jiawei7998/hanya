<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.singlee.capital.base.mapper.TcFormFieldsetMapper">
	<sql id="columns">
		${alias}.FORM_NO,
		${alias}.FDS_NO,
		${alias}.FORM_INDEX,
		FS.FDS_NAME,
		FS.FDS_TYPE,
       	FF.FLD_NO,
       	FF.FORM_INDEX FLD_INDEX,
       	FF.FORM_NAME,
		FF.FORM_TIPS,
       	FF.IS_MANDATORY,
       	FF.IS_DISABLED,
       	FF.DEF_KEY,
       	FF.DEF_VALUE,
       	FF.DEF_TEXT,
       	FF.ATTACH_TREE_TEMP,
       	F.FLD_NAME,
		F.FLD_TYPE,
		F.FORM_TYPE,
		F.COMPONET,
		F.FORM_COLS,
		F.FORM_ROWS,
		F.FORM_LENGTH,
		F.FORM_MULTIPLE,
		F.FORM_MIN,
		F.FORM_MAX,
		F.FORM_PRECISION,
		F.FORM_SPLIT,
		F.FORM_TIMES,
		F.FORM_VALID,
		F.VALID_PARAM,
		F.DEF_URL
	</sql>
	
	<sql id="join">
		LEFT JOIN TC_FIELDSET FS
		  ON ${alias}.FDS_NO = FS.FDS_NO
		LEFT JOIN TC_FORM_FIELDSET_FIELD_MAP FF
		  ON ${alias}.FORM_NO = FF.FORM_NO
		  AND FS.FDS_NO = FF.FDS_NO
		LEFT JOIN TC_FIELD F
	      ON FF.FLD_NO = F.FLD_NO
	</sql>
	
	<resultMap id="formFieldsetMap" type="com.singlee.capital.base.model.TcFieldset">
		<id property="fdsNo" column="FDS_NO" />
		<result property="fdsName" column="FDS_NAME" />
		<collection property="fields" ofType="com.singlee.capital.base.model.TcField">
			<id property="fldNo" column="FLD_NO" />
			<result property="fldName" column="FLD_NAME" />
			<result property="fldType" column="FLD_TYPE" />
			<result property="formName" column="FORM_NAME" />
			<result property="formTips" column="FORM_TIPS" />
			<result property="formType" column="FORM_TYPE" />
			<result property="componet" column="COMPONET" />
			<result property="formCols" column="FORM_COLS" />
			<result property="formRows" column="FORM_ROWS" />
			<result property="formLength" column="FORM_LENGTH" />
			<result property="formMultiple" column="FORM_MULTIPLE" />
			<result property="formMin" column="FORM_MIN" />
			<result property="formMax" column="FORM_MAX" />
			<result property="formPrecision" column="FORM_PRECISION" />
			<result property="formSplit" column="FORM_SPLIT" />
			<result property="formTimes" column="FORM_TIMES" />
			<result property="formValid" column="FORM_VALID" />
			<result property="validParam" column="VALID_PARAM" />
			<result property="isMandatory" column="IS_MANDATORY" />
			<result property="isDisabled" column="IS_DISABLED" />
			<result property="defKey" column="DEF_KEY" />
			<result property="defUrl" column="DEF_URL" />
			<result property="defValue" column="DEF_VALUE" />
			<result property="defText" column="DEF_TEXT" />
			<result property="attachTreeTemp" column="ATTACH_TREE_TEMP" />
		</collection>
	</resultMap>

	<select id="getFormFieldsetList" parameterType="map" resultMap="formFieldsetMap">
		SELECT 
			<include refid="columns"><property name="alias" value="T"/></include>
		  FROM TC_FORM_FIELDSET_MAP T
		  	<include refid="join"><property name="alias" value="T"/></include>
		<where>
			<if test="formNo != null and formNo != ''">
				AND T.FORM_NO = #{formNo}
			</if>
			<if test="fdsType != null and fdsType != ''">
				AND FDS_TYPE = #{fdsType}
			</if>
		</where>
		ORDER BY
		<choose>
			<when test="sort != null and sort != ''">
				${sort} ${order}
			</when>
			<otherwise>
				T.FORM_INDEX, FF.FORM_INDEX
			</otherwise>
		</choose>
	</select>
	
	<delete id="deleteFormFieldsetByFormId">
		DELETE FROM TC_FORM_FIELDSET_MAP WHERE FORM_NO = #{formNo}
	</delete>
</mapper>