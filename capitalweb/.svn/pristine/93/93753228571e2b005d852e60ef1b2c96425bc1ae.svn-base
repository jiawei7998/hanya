package com.singlee.hrbcap.model.acup;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_ENTRY")
public class TbEntry implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**  分录id          */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT TO_CHAR(SYSDATE,'YYYYMMDD')||LPAD(SEQ_ACCOUNTING_TEST.NEXTVAL,'8','0') FROM DUAL")
	private String entryId;

	/**  帐套id          */
	private String taskId;

	/**  分录日期        */
	private String postDate;

	/**  流水号          */
	private String flowId;

	/**  流水序号          */
	private int flowSeq;

	/**  交易流水号          */
	private String dealNo;

	/**  产品代码            */
	private String prdNo;

	/**  交易机构        */
	private String sponInst;

	/**  记账机构        */
	private String bkkpgOrgId;
	/**  会计事件        */
	private String eventId;

	/**  科目码          */
	private String subjCode;

	/**  科目码名称         */
	private String subjName;

	/**  内部帐序号      */
	private String innerAcctSn;

	/**  借贷标识        */
	private String debitCreditFlag;

	/**  对手方资金账号  */
	private String partyCashAcctId;

	/**  发生额          */
	private double value;

	/**  币种            */
	private String ccy;

	/**  状态            */
	private String state;

	/**  更新时间        */
	private String updateTime;

	/**  核心账号        */
	private String coreAcctCode;

	/**  备注        */
	private String remark;
	/**  红蓝字【00-蓝字,01-红字】        */
	private String redFlag;
	/**  入账优先级        */
	private String seqFlag;
	/**  发送核心标志【0-默认,1-成功,2-失败,3-待重发,4-重发成功】        */
	private String sendFlag;
	/**  前置日期        */
	private String qzDate;
	/**  前置流水号        */
	private String qzDealno;
	/**  柜员流水号        */
	private String operDealno;
	/**  核心返回码       */
	private String retCode;
	/**  核心返回信息       */
	private String retMsg;

	/**导出用中文名**/
	@Transient
	private String sendFlagName;
	@Transient
	private String debitCreditFlagName;


	public String getSendFlagName() {
		return sendFlagName;
	}
	public void setSendFlagName(String sendFlagName) {
		this.sendFlagName = sendFlagName;
	}
	public String getDebitCreditFlagName() {
		return debitCreditFlagName;
	}
	public void setDebitCreditFlagName(String debitCreditFlagName) {
		this.debitCreditFlagName = debitCreditFlagName;
	}
	public String getSetno() {
		return setno;
	}
	public void setSetno(String setno) {
		this.setno = setno;
	}
	/**  清算流水号      */
	private String setno;

	/**
	 * 是否有效
	 */
	private String errCode;
	/**
	 * 异常信息
	 */
	private String errMsg;

	/**
	 * 基金代码
	 */
	private String fundCode;


	@Transient
	private String expression;
	@Transient
	private String entry_date_begin;
	@Transient
	private String entry_date_end;
	@Transient
	private String eventName;
	@Transient
	private String taskName;
	@Transient
	private String prdName;

	@Transient
	private double amtValue;
	/**
	 * 	核心流水-核心流水
	 */
	@Transient
	private String coreCoresn;
	/**
	 * 	核心日期-核心日期
	 */
	@Transient
	private String coreCoredate;
	/**
	 * 	平台记账日期-账务日期
	 */
	@Transient
	private String corePostdate;
	/**
	 * 	核心状态-核心状态 0-正常,1-被冲帐,2-冲帐
	 */
	@Transient
	private String coreCorestatus;

	@Transient
	private String thirdPartyId;//核心记账机构
	@Transient
	private String bInstId;//OPICS记账机构


	public double getAmtValue() {
		return amtValue;
	}
	public void setAmtValue(double amtValue) {
		this.amtValue = amtValue;
	}
	/**-------  Generate Getter And Setter   --------**/
	public String getEntryId() {
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getBkkpgOrgId() {
		return bkkpgOrgId;
	}
	public void setBkkpgOrgId(String bkkpgOrgId) {
		this.bkkpgOrgId = bkkpgOrgId;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getInnerAcctSn() {
		return innerAcctSn;
	}
	public void setInnerAcctSn(String innerAcctSn) {
		this.innerAcctSn = innerAcctSn;
	}
	public String getDebitCreditFlag() {
		return debitCreditFlag;
	}
	public void setDebitCreditFlag(String debitCreditFlag) {
		this.debitCreditFlag = debitCreditFlag;
	}
	public String getPartyCashAcctId() {
		return partyCashAcctId;
	}
	public void setPartyCashAcctId(String partyCashAcctId) {
		this.partyCashAcctId = partyCashAcctId;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getCoreAcctCode() {
		return coreAcctCode;
	}
	public void setCoreAcctCode(String coreAcctCode) {
		this.coreAcctCode = coreAcctCode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRedFlag() {
		return redFlag;
	}
	public void setRedFlag(String redFlag) {
		this.redFlag = redFlag;
	}
	public String getSeqFlag() {
		return seqFlag;
	}
	public void setSeqFlag(String seqFlag) {
		this.seqFlag = seqFlag;
	}
	public String getSendFlag() {
		return sendFlag;
	}
	public void setSendFlag(String sendFlag) {
		this.sendFlag = sendFlag;
	}
	public String getQzDate() {
		return qzDate;
	}
	public void setQzDate(String qzDate) {
		this.qzDate = qzDate;
	}
	public String getQzDealno() {
		return qzDealno;
	}
	public void setQzDealno(String qzDealno) {
		this.qzDealno = qzDealno;
	}
	public String getOperDealno() {
		return operDealno;
	}
	public void setOperDealno(String operDealno) {
		this.operDealno = operDealno;
	}
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public int getFlowSeq() {
		return flowSeq;
	}
	public void setFlowSeq(int flowSeq) {
		this.flowSeq = flowSeq;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEntry_date_begin() {
		return entry_date_begin;
	}
	public void setEntry_date_begin(String entryDateBegin) {
		entry_date_begin = entryDateBegin;
	}
	public String getEntry_date_end() {
		return entry_date_end;
	}
	public void setEntry_date_end(String entryDateEnd) {
		entry_date_end = entryDateEnd;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	@Transient
	private String sponInstName;
	public String getSponInstName() {
		return sponInstName;
	}
	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}
	public String getCoreCoresn() {
		return coreCoresn;
	}
	public void setCoreCoresn(String coreCoresn) {
		this.coreCoresn = coreCoresn;
	}
	public String getCoreCoredate() {
		return coreCoredate;
	}
	public void setCoreCoredate(String coreCoredate) {
		this.coreCoredate = coreCoredate;
	}
	public String getCorePostdate() {
		return corePostdate;
	}
	public void setCorePostdate(String corePostdate) {
		this.corePostdate = corePostdate;
	}
	public String getCoreCorestatus() {
		return coreCorestatus;
	}
	public void setCoreCorestatus(String coreCorestatus) {
		this.coreCorestatus = coreCorestatus;
	}


	public String getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(String thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public String getbInstId() {
		return bInstId;
	}

	public void setbInstId(String bInstId) {
		this.bInstId = bInstId;
	}
}