package com.singlee.hrbextra.wind;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.mapper.IfsWindSecLevelMapper;
import com.singlee.ifs.model.IfsWindSecLevelBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 万得债券评级数据
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WindSecLevelService {

	@Autowired
	BatchDao batchDao;
	@Autowired
	IfsWindSecLevelMapper IfsWindSecLevelMapper;

	
	private static Logger logManager = LoggerFactory.getLogger(WindSecLevelService.class);

	public Boolean run() throws Exception {
		logManager.info("==================开始执行 读取Excel==============================");
		// 组装文件名
		Date spotDate = new Date();

		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);

		String postDates = new SimpleDateFormat("yyyyMMdd HH").format(spotDate);


		logManager.info("==================在本地创建文件夹==============================");

		//本地文件文件保存目录
		String filePath = SystemProperties.marketdataTargetpath;
		//远程下载文件名
		String fileName = "";
		if ("08".equals(postDates.substring(9))||"09".equals(postDates.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecLevel + "0830";
		} else if ("10".equals(postDates.substring(9))||"11".equals(postDates.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecLevel + "1030";
		} else if ("14".equals(postDates.substring(9))||"15".equals(postDates.substring(9))) {
			fileName = SystemProperties.marketdataLocalfilenameSecLevel + "1430";

		}
		Map<String ,Object> map=new HashMap<>();
		//远程文件名(不含文件后缀名)
		map.put("fileName",fileName);
		map.put("postDate",postDate);
		// 下载文件 （远程下载目录、远程文件名、本地保存目录、本地保存文件名）
		boolean flag=WindUtils.sftp(map);
		if(flag) {

			String fullName= (String) map.get("fullName");
			//本地文件绝对路径
			String excel_location = filePath + postDate + "/" + fullName;
			// 读取文件
			logManager.debug("开始读取文件：" + excel_location);
			File file = new File(excel_location);
			if (!file.exists()) {
				logManager.info("========" + excel_location + ":文件不存在=========");
				return true;
			}
			// 解析结果集
			List<IfsWindSecLevelBean> wslbs = new ArrayList<IfsWindSecLevelBean>();

			InputStream is = new FileInputStream(file);
			ExcelUtil excelUtil = new ExcelUtil(is);
			Sheet sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			// 开始读取数据
			for (int i = 1; i <= rowNum; i++) {
				IfsWindSecLevelBean wslb = new IfsWindSecLevelBean();
				Row row = sheet.getRow(i);
				wslb = readExcel(row, wslb, spotDate);
				wslbs.add(wslb);
			}
			logManager.info("==================执行完成 读取Excel==============================");

			logManager.info("==================开始执行 批量处理前置==============================");
			try {
				for (IfsWindSecLevelBean wslb : wslbs) {
					logManager.info("=====债券ID:" + wslb.getBondCode() + "=====");
					IfsWindSecLevelBean selectByPrimaryKey = IfsWindSecLevelMapper.selectByPrimaryKey(wslb.getBondCode());
					if (selectByPrimaryKey == null) {
						IfsWindSecLevelMapper.insertSelective(wslb);
					} else {
						IfsWindSecLevelMapper.updateByPrimaryKeySelective(wslb);
					}

				}
			} catch (Exception e) {
				logManager.error("债券评级数据批量插入前置异常!", e);
			}
			logManager.info("==================执行完成 批量处理前置==============================");

			return true;
		}else
		{
			logManager.info("==================文件下载失败，请重试==============================");
			logManager.info("=================="+flag+"==============================");

			return false;
		}
	}

	/**
	 * 解析表格
	 * 
	 * @param row
	 * @param wslb
	 * @return
	 */
	public static IfsWindSecLevelBean readExcel(Row row, IfsWindSecLevelBean wslb, Date spotDate) {
		wslb.setBondName(WindUtils.getString(row, 0));
		wslb.setBondCode(WindUtils.getString(row, 1));
		wslb.setIfName(WindUtils.getString(row, 2));
		wslb.setFxZtLevel(WindUtils.getString(row, 3));
		wslb.setFxZtDate(WindUtils.getString(row, 4));
		wslb.setZxZtLevel(WindUtils.getString(row, 5));
		wslb.setFxZxLevel(WindUtils.getString(row, 6));
		wslb.setFxZxDate(WindUtils.getString(row, 7));
		wslb.setZxZxLevel(WindUtils.getString(row, 8));
		wslb.setZtInst(WindUtils.getString(row, 9));
		wslb.setZxInst(WindUtils.getString(row, 10));
		wslb.setSpotDate(spotDate);
		return wslb;
	}

//	/**
//	 * 格式换数据
//	 *
//	 * @param row
//	 * @return
//	 */
//	public static String WindUtils.getString(Row row, int i) {
//		if ("".equals(row.getCell(i).WindUtils.getStringCellValue()) || null == row.getCell(i).WindUtils.getStringCellValue()) {
//			return "";
//		} else {
//			return row.getCell(i).WindUtils.getStringCellValue().trim();
//		}
//	}
}
