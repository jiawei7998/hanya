package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.credit.mapper.TcProductCreditMapper;
import com.singlee.capital.credit.model.TcProductCredit;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import com.singlee.rules.util.DateUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsRmbCbtService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbCbtService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;
	@Autowired
	ISecurServer securServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	// 债券
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;

	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Autowired
	TcProductCreditMapper tcProductCreditMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbCbtMapper.updateRmbCbtByID(serial_no, status, date);
		IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(serial_no);
		IfsOpicsBond ifsOpicsBond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());
		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsrmbCbt) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("P".equals(ifsCfetsrmbCbt.getMyDir())) {//买入
					hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				}else{
					if ("CNY".equals(ifsOpicsBond.getCcy())) {
						if ("8300002001".equals(ifsCfetsrmbCbt.getCost())) {
							LimitOccupy(serial_no, TradeConstants.ProductCode.MBS);
						}else{
							LimitOccupy(serial_no, TradeConstants.ProductCode.CBT);
						}
					}
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
//		// 2.交易审批中状态,需要将交易纳入监控审批
//		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {// 6的状态为结算完成
//			// 释放额度,重新占用
//			List<EdInParams> creditListLast = new ArrayList<EdInParams>();
//			creditListLast = ifsCfetsfxLendMapper.searchAllInfo(serial_no);
//			for (int i = 0; i < creditListLast.size(); i++) {
//				String prdNo = creditListLast.get(i).getPrdNo();
//				String direction = creditListLast.get(i).getDirection();
//				if ("S".equals(direction)) {// 卖出
//					Map<String, String> paramMap = new HashMap<String, String>();
//					paramMap.put("serial_no", serial_no);
//					paramMap.put("product_type", prdNo);
//					paramMap.put("pureFlag", "1");// 清算之后卖出的限额进行限制
//					edCustManangeService.chooseOccupy(paramMap);
//
//					// 所有的限额都在交易员提交时限制
//					// paramMap.put("code", "0");
//					// paramMap.put("product_type", prdNo);
//					// String inFlag = "1";//作为判断标识符，为0时是审批台发起，为1为清算后发起
//					// edCustManangeService.limitBondExchange(paramMap,inFlag);
//
//				}
//			}
//		}
		String prdNo = ifsCfetsrmbCbt.getfPrdCode();
		// /////以下将审批中的记录插入审批监控表
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbCbt.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbCbt.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbCbt.getApproveStatus());// 设置监控表-审批状态
			// 判断是现券买卖还是外币债（人民币或非人民币）
			// 债券信息实体类

			if (ifsOpicsBond == null) {
				JY.raise("交易的券不存在......");
			}
			if (ifsOpicsBond.getCcy() == null) {
				JY.raise("交易的券的币种不存在......");
			}
			if ("CNY".equals(ifsOpicsBond.getCcy())) {
				if(TradeConstants.ProductCode.MBS.equals(prdNo)) {
					ifsFlowMonitor.setPrdName("提前偿还");// 设置监控表-产品名称
					ifsFlowMonitor.setPrdNo("mbs");// 设置监控表-产品编号：作为分类
					prdNo = TradeConstants.ProductCode.MBS;
				}else if(TradeConstants.ProductCode.CBT.equals(prdNo)){
					ifsFlowMonitor.setPrdName("现券买卖");// 设置监控表-产品名称
					ifsFlowMonitor.setPrdNo("cbt");// 设置监控表-产品编号：作为分类
					prdNo = TradeConstants.ProductCode.CBT;
				}

			} else {
				ifsFlowMonitor.setPrdName("外币债");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("debt");// 设置监控表-产品编号：作为分类
			}
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbCbt.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbCbt.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbCbt.getSellInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_CBT");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CBT where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbCbtMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("P".equals(ifsCfetsrmbCbt.getMyDir())) {//买入
						if(!"0".equals(ifsCfetsrmbCbt.getQuotaOccupyType())){
							LimitOccupy(serial_no, prdNo);
						}
					}

				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			try {
				if("S".equals(ifsCfetsrmbCbt.getMyDir())) {//卖出
					if(!"0".equals(ifsCfetsrmbCbt.getQuotaOccupyType())){
						LimitRelesse(serial_no, prdNo);
					}
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> mapEntity = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(mapEntity);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			// 3.3调用java代码
			SlOutBean result = null;
			// 获取当前账务日期
			Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbCbt.getBr());
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				IfsOpicsBond bond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());
				if (bond.getIntcalcrule() != null && "MBS".equals(bond.getIntcalcrule())) {
					// MBS
					SlImbdBean slImbdBean = getMbsEntry(ifsCfetsrmbCbt, branchDate);
					result = securServer.imbd(slImbdBean);
				} else {
					// 现券买卖
					SlFixedIncomeBean slFixedIncomeBean = getFiEntry(ifsCfetsrmbCbt, branchDate);
					result = securServer.fi(slFixedIncomeBean);
				}
			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {

			} else {
				// 将实体对象转换成map
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbCbt);
				result = securServer.bredProc(map);// 现券买卖存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，执行存储过程失败......");
				}
			}
			// 判断释放处理成功
			if (!result.getRetStatus().equals(RetStatusEnum.S)) {
				JY.raise("审批未通过，插入opics失败......");
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("ticketId", ifsCfetsrmbCbt.getTicketId());
				map.put("satacode", "-1");
				ifsCfetsrmbCbtMapper.updateStatcodeByTicketId(map);
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbCbt.getCustNo());
		remap.put("custType", ifsCfetsrmbCbt.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbCbt.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbCbt.getBuyInst());
		BigDecimal amt = calcuAmt(ifsCfetsrmbCbt.getTradeAmount(), ifsCfetsrmbCbt.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbCbt.getForDate());
		remap.put("mdate", ifsCfetsrmbCbt.getSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	public void LimitRelesse(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(serial_no);
		Map<String, Object> remap = new HashMap<>();

		IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(ifsCfetsrmbCbt.getCustNo());
		if (ifsOpicsCust == null ) {
			JY.raiseRException("未找到" + ifsCfetsrmbCbt.getCustNo() + "授信主体");
		}
		remap.put("productCode", prd_no);
		TcProductCredit tcProductCredit = tcProductCreditMapper.selectCreditProductOne(remap);
		if (tcProductCredit == null ) {
			JY.raiseRException("未找到" + prd_no + "产品类型");
		}

		remap.put("custNo", ifsCfetsrmbCbt.getCustNo());
		remap.put("custType", ifsCfetsrmbCbt.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种

		Map<String, Object> map = new HashMap<>();
		map.put("custName", ifsOpicsCust.getSn());
		map.put("custNo", ifsCfetsrmbCbt.getCustNo());
		map.put("productCode", prd_no);
		map.put("productName", tcProductCredit.getProductName());
		map.put("currency", hrbCreditCustQuota.getCurrency());
		BigDecimal amt = calcuAmt(ifsCfetsrmbCbt.getTradeAmount(), ifsCfetsrmbCbt.getWeight());
		map.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		map.put("institution", "999999999");
		map.put("creTypeName", ifsCfetsrmbCbt.getCustType());
		map.put("weight", ifsCfetsrmbCbt.getWeight());
		map.put("dealNo", serial_no);
		hrbCreditLimitOccupyService.LimitReleaseInsert(map);
	}

	/**
	 * 获取债券交易实体
	 * 
	 * @param ifsCfetsrmbCbt
	 * @return
	 */
	private SlFixedIncomeBean getFiEntry(IfsCfetsrmbCbt ifsCfetsrmbCbt, Date branchDate) {
		SlFixedIncomeBean slFixedIncomeBean = new SlFixedIncomeBean(ifsCfetsrmbCbt.getBr(), SlDealModule.FI.SERVER,
				SlDealModule.FI.TAG, SlDealModule.FI.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbCbt.getSellInst());
		slFixedIncomeBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slFixedIncomeBean.setSecsacct(ifsCfetsrmbCbt.getBuyCustname());
		/************* 设置交易方向 ***************************/
		if (ifsCfetsrmbCbt.getMyDir().endsWith("P")) {
			slFixedIncomeBean.setPs(PsEnum.P);
		}
		if (ifsCfetsrmbCbt.getMyDir().endsWith("S")) {
			slFixedIncomeBean.setPs(PsEnum.S);
		}
		slFixedIncomeBean.setInvtype(ifsCfetsrmbCbt.getInvtype());
		// 结算金额
		slFixedIncomeBean.setOrigamt(String
				.valueOf(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsrmbCbt.getSettlementAmount() + ""), 2)));
		/************* 设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbCbt.getBuyTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbCbt.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FI.IOPER);
		institutionInfo.setTradeId(trad);
		slFixedIncomeBean.getInthBean().setIoper(trad);
		slFixedIncomeBean.setInstitutionInfo(institutionInfo);
		/************* 设置债券信息 ***************************/
		SecurityInfoBean securityInfo = new SecurityInfoBean();
		securityInfo.setSecurityId(ifsCfetsrmbCbt.getBondCode());
		slFixedIncomeBean.setSecurityInfo(securityInfo);
		slFixedIncomeBean.setValueDate(DateUtil.parse(ifsCfetsrmbCbt.getSettlementDate()));
		slFixedIncomeBean.setDealDate(DateUtil.parse(ifsCfetsrmbCbt.getForDate()));
		
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsrmbCbt.getCost());
		externalBean.setPort(ifsCfetsrmbCbt.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbCbt.getContractId());
		externalBean.setVerind(SlDealModule.FI.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbCbt.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbCbt.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbCbt.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbCbt.getCtrsacct());
		slFixedIncomeBean.setVerdate(branchDate);
		slFixedIncomeBean.setCcysmeans(ifsCfetsrmbCbt.getCcysmeans());
		slFixedIncomeBean.setCcysacct(ifsCfetsrmbCbt.getCcysacct());
		slFixedIncomeBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFixedIncomeBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbCbt.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFixedIncomeBean.setInthBean(inthBean);
		slFixedIncomeBean.setStandinstr("Y");
		slFixedIncomeBean.setUsualid("");
		// 判断是否延期交易
		IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbCbt.getBondCode());
		String settleDays = "0";
		if (ifsOpicsBond != null) {
			settleDays = ifsOpicsBond.getSettdays();
			settleDays = settleDays.trim();
		}
		if ("".equals(settleDays)) {
			settleDays = "0";
		}
		int betweendays = DateUtils.sumDays(ifsCfetsrmbCbt.getForDate(), ifsCfetsrmbCbt.getSettlementDate(), "1");
		int IsettleDays = Integer.valueOf(settleDays);
		if (betweendays > IsettleDays) {
			slFixedIncomeBean.setDelaydelivind("Y");
		} else {
			slFixedIncomeBean.setDelaydelivind("N");
		}
		
		// 交易金额
		String unit = ifsOpicsBond != null && null != ifsOpicsBond.getSecunit() && !"".equals(ifsOpicsBond.getSecunit()) ? 
		        ifsOpicsBond.getSecunit() : "FMT"; 
		if("FMT".equals(unit)) {
		    slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
	                Double.parseDouble(ifsCfetsrmbCbt.getTotalFaceValue().multiply(new BigDecimal(10000)) + ""), 2)));
		}else {
		    //
		    slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
                    Double.parseDouble(ifsCfetsrmbCbt.getTotalFaceValue().multiply(new BigDecimal(1)) + ""), 2)));
		}
        
		
		return slFixedIncomeBean;
	}

	/**
	 * 获取MBS交易实体
	 * 
	 * @param ifsCfetsrmbCbt
	 * @return
	 */
	private SlImbdBean getMbsEntry(IfsCfetsrmbCbt ifsCfetsrmbCbt, Date branchDate) {
		SlImbdBean slImbdBean = new SlImbdBean(ifsCfetsrmbCbt.getBr(), SlDealModule.IMBD.SERVER, SlDealModule.IMBD.TAG,
				SlDealModule.IMBD.DETAIL);
		IfsOpicsBond bond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());
		if (bond.getIntcalcrule() != null && "MBS".equals(bond.getIntcalcrule())) {
			InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
			contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbCbt.getSellInst());
			slImbdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
			slImbdBean.setSecsacct(ifsCfetsrmbCbt.getBuyCustname());
			/************* 设置交易方向 ***************************/
			if (ifsCfetsrmbCbt.getMyDir().endsWith("P")) {
				slImbdBean.setPs(PsEnum.P);
			}
			if (ifsCfetsrmbCbt.getMyDir().endsWith("S")) {
				slImbdBean.setPs(PsEnum.S);
			}
			slImbdBean.setInvtype(ifsCfetsrmbCbt.getInvtype());
			// 结算金额
			slImbdBean.setOrigamt(String
					.valueOf(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsrmbCbt.getSettlementAmount() + ""), 2)));
			/************* 设置本方交易员 ***************************/
			InstitutionBean institutionInfo = new InstitutionBean();
			Map<String, Object> userMap = new HashMap<String, Object>();
			userMap.put("userId", ifsCfetsrmbCbt.getBuyTrader());// 交易员id
			userMap.put("branchId", ifsCfetsrmbCbt.getBranchId());// 机构
			TaUser user = taUserMapper.queryUserByMap(userMap);
			institutionInfo.setTradeId(user.getOpicsTrad());
			slImbdBean.setInstitutionInfo(institutionInfo);
			/************* 设置债券信息 ***************************/
			SecurityInfoBean securityInfo = new SecurityInfoBean();
			securityInfo.setSecurityId(ifsCfetsrmbCbt.getBondCode());
			slImbdBean.setSecurityInfo(securityInfo);
			slImbdBean.setValueDate(DateUtil.parse(ifsCfetsrmbCbt.getSettlementDate()));
			slImbdBean.setDealDate(DateUtil.parse(ifsCfetsrmbCbt.getForDate()));
			// price
			SecurityAmtDetailBean securityAmt = new SecurityAmtDetailBean();
			securityAmt.setPrice(ifsCfetsrmbCbt.getCleanPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
			slImbdBean.setSecurityAmt(securityAmt);
			// 交易金额
			slImbdBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
					Double.parseDouble(ifsCfetsrmbCbt.getTotalFaceValue().multiply(new BigDecimal(10000)) + ""), 2)));
			SlExternalBean externalBean = new SlExternalBean();
			externalBean.setCost(ifsCfetsrmbCbt.getCost());
			externalBean.setPort(ifsCfetsrmbCbt.getPort());
			externalBean.setBroker(SlDealModule.BROKER);
			externalBean.setDealtext(ifsCfetsrmbCbt.getContractId());
			externalBean.setVerind(SlDealModule.IMBD.VERIND);
			// 设置清算路径1
			externalBean.setCcysmeans(ifsCfetsrmbCbt.getCcysmeans());
			// 设置清算账户1
			externalBean.setCcysacct(ifsCfetsrmbCbt.getCcysacct());
			// 设置清算路径2
			externalBean.setCtrsmeans(ifsCfetsrmbCbt.getCtrsmeans());
			// 设置清算账户2
			externalBean.setCtrsacct(ifsCfetsrmbCbt.getCtrsacct());
			slImbdBean.setVerdate(branchDate);
			slImbdBean.setCcysmeans(ifsCfetsrmbCbt.getCcysmeans());
			slImbdBean.setCcysacct(ifsCfetsrmbCbt.getCcysacct());
			slImbdBean.setExternalBean(externalBean);
			SlInthBean inthBean = slImbdBean.getInthBean();
			inthBean.setSeq("0");
			inthBean.setFedealno(ifsCfetsrmbCbt.getTicketId());
			inthBean.setLstmntdate(branchDate);
			slImbdBean.setInthBean(inthBean);
			slImbdBean.setStandinstr("Y");
			// 判断是否延期交易
			IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbCbt.getBondCode());
			String settleDays = "0";
			if (ifsOpicsBond != null) {
				settleDays = ifsOpicsBond.getSettdays();
				settleDays = settleDays.trim();
			}
			if ("".equals(settleDays)) {
				settleDays = "0";
			}
			int betweendays = DateUtils.sumDays(ifsCfetsrmbCbt.getForDate(), ifsCfetsrmbCbt.getSettlementDate(), "1");
			int IsettleDays = Integer.valueOf(settleDays);
			if (betweendays > IsettleDays) {
				slImbdBean.setDelaydelivind("Y");
			} else {
				slImbdBean.setDelaydelivind("N");
			}
			slImbdBean.setAuthind("Y");
			slImbdBean.setAuthdate(DateUtil.parse(ifsCfetsrmbCbt.getForDate()));
		}
		return slImbdBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		String id = (String)paramer.get("id");

		String[] costcent = {"8300002001"};
		String costind = null;
		if("jymbs".equals(id)) {
			costind = "MBS";
		}else if("jyxqmm".equals(id)) {
			paramer.put("applyProd", TradeConstants.MarketIndicator.CASH_BOND);
		}
		paramer.put("cost", costcent);
		paramer.put("costind", costind);

		List<IfsCfetsrmbCbt> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbCbtMapper.getRmbCbtMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbCbtMapper.getRmbCbtMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbCbtMapper.getRmbCbtMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> tradingList = taDictVoMapper.getTadictTree("trading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> invtypeList = taDictVoMapper.getTadictTree("invtype").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> settlementMethodList = taDictVoMapper.getTadictTree("SettlementMethod").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbCbt ifsCfetsrmbCbt: list) {
			ifsCfetsrmbCbt.setMyDir(tradingList.get(ifsCfetsrmbCbt.getMyDir()));
			ifsCfetsrmbCbt.setInvtype(invtypeList.get(ifsCfetsrmbCbt.getInvtype()));
			ifsCfetsrmbCbt.setSettlementMethod(settlementMethodList.get(ifsCfetsrmbCbt.getSettlementMethod()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
