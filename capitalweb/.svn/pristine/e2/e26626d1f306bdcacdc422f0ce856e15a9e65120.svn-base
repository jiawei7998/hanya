package com.singlee.capital.trade.acc.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyAmorVo;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyVo;
import com.singlee.capital.trade.model.TdAdvanceMaturity;

/**
 * 账务计提的入口函数
 * @author SINGLEE
 *
 */
public interface AccForTradeAccessService {
	/**
	**	筛选需要计提的交易数据
		数据逻辑：从TD_TRD_TPOS获得还有剩余本金的持仓数据
		关联交易核实单TD_PRODUCT_APPROVE_MAIN  TRD开头交易
		关联利率区间表TT_RATE_RANGE 获得实时的利率
		关联收付息计划表TD_CASHFLOW_INTEREST 获得区间收息起始日和截止日
		关联账务日期表TT_DAYEND_DATE 明确计提日（账务日）
		SELECT 'interest_Normal',T.DEAL_NO,T.VERSION,P.PRD_NO,P.INVTYPE,T.SETTL_AMT,
	    P.BASIS,R.EXEC_RATE,D.CUR_DATE,C.REF_BEGIN_DATE,C.REF_END_DATE 
	    FROM TD_TRD_TPOS T
	    LEFT JOIN TD_PRODUCT_APPROVE_MAIN P ON T.DEAL_NO = P.DEAL_NO AND T.VERSION=P.VERSION 
	    LEFT JOIN TT_RATE_RANGE R ON T.DEAL_NO=R.DEAL_NO AND T.VERSION = R.VERSION
	    LEFT JOIN TD_CASHFLOW_INTEREST C ON T.DEAL_NO=C.DEAL_NO AND T.VERSION=C.VERSION
	    ,TT_DAYEND_DATE D
	    WHERE ((R.BEGIN_DATE<=D.CUR_DATE AND D.CUR_DATE<R.END_DATE)
	    OR (R.BEGIN_DATE=D.CUR_DATE AND D.CUR_DATE=R.END_DATE))
	    AND (
	    (C.REF_BEGIN_DATE<=D.CUR_DATE AND D.CUR_DATE<C.REF_END_DATE)
	    OR (C.REF_BEGIN_DATE=D.CUR_DATE AND D.CUR_DATE=C.REF_END_DATE)
	    )
	    AND NOT EXISTS (SELECT * FROM TD_ACC_TRD_DAILY ACC WHERE ACC.POST_DATE=D.CUR_DATE
	    AND ACC.DEAL_NO = T.DEAL_NO AND ACC.ACC_TYPE='interest_Normal')
	   
	   'INTEREST'
		DEAL_NO
		VERSION
		PRD_NO
		INVTYPE
		SETTL_AMT
		BASIS
		EXEC_RATE
		CUR_DATE
		REF_BEGIN_DATE
		REF_END_DATE
		采用后一天减去前一天的利息计提算法
		ROUND(SETTL_AMT*EXEC_RATE*(CUR_DATE+1-REF_BEGIN_DATE)/BASIS,2)
		-
		ROUND(SETTL_AMT*EXEC_RATE*(CUR_DATE-REF_BEGIN_DATE)/BASIS,2)
		遇到CUR_DATE=REF_END_DATE-1时,需要改变算法
		(固息，中间没有改变利率)
		ROUND(SETTL_AMT*EXEC_RATE*(REF_END_DATE-REF_BEGIN_DATE)/BASIS,2)
		-
		SELECT SUM(INTEREST_AMT) FROM TD_ACC_TRD_DAILY ACC WHERE ACC.POST_DATE>= REF_BEGIN_DATE
		AND ACC.POST_DATE<REF_END_DATE
		(中间改变了利率）
		TT_RATE_RANGE分区段计算加总之和
		-
		SELECT SUM(INTEREST_AMT) FROM TD_ACC_TRD_DAILY ACC WHERE ACC.POST_DATE>= REF_BEGIN_DATE
		AND ACC.POST_DATE<REF_END_DATE
	 */
	public void getNeedForAccDrawingDatasStepOne(String dealNo);
	/**
	**	筛选需要计提的活期交易数据
		数据逻辑：从TD_TRD_TPOS获得还有剩余本金的持仓数据
		关联交易核实单TD_PRODUCT_APPROVE_MAIN  TRD开头交易
		关联账务日期表TT_DAYEND_DATE 明确计提日（账务日）
		SELECT 'interest_Normal' AS ACCTYPE,
		       T.DEAL_NO,
		       T.VERSION,
		       P.PRD_NO,
		       P.INVTYPE,
		       T.SETTL_AMT,
		       P.BASIS,
		       P.CONTRACT_RATE,
		       D.CUR_DATE,
		       P.V_DATE AS REF_BEGIN_DATE
		  FROM TD_TRD_TPOS T
		 INNER JOIN TD_PRODUCT_APPROVE_MAIN P
		    ON T.DEAL_NO = P.DEAL_NO
		   AND T.VERSION = P.VERSION
		  , TT_DAYEND_DATE D
		 WHERE P.INT_TYPE = '2'
		   AND P.V_DATE <= D.CUR_DATE 
		   AND P.PRD_NO = '907'
		   AND T.SETTL_AMT <> 0
		   AND NOT EXISTS (SELECT *
		          FROM TD_ACC_TRD_DAILY ACC
		         WHERE ACC.POST_DATE = D.CUR_DATE
		           AND ACC.DEAL_NO = T.DEAL_NO
		           AND ACC.ACC_TYPE = 'interest_Normal')
	   
	   'INTEREST'
		DEAL_NO
		VERSION
		PRD_NO
		INVTYPE
		SETTL_AMT
		BASIS
		EXEC_RATE
		CUR_DATE
		REF_BEGIN_DATE
		REF_END_DATE
		采用后一天减去前一天的利息计提算法
		ROUND(SETTL_AMT*EXEC_RATE*(CUR_DATE+1-REF_BEGIN_DATE)/BASIS,2)
		-
		ROUND(SETTL_AMT*EXEC_RATE*(CUR_DATE-REF_BEGIN_DATE)/BASIS,2)
	 */
	public void getNeedForCurrentAccDrawingDatasStepOne(String dealNo);
	
	/**
	 * 批量出费用计提/摊销
	 */
	public void getNeedForAccDrawingDataFees();
	/**
	 * 
	   SELECT 'interest_Overdue' AS ACCTYPE,
       T.DEAL_NO,
       T.VERSION,
       P.PRD_NO,
       P.INVTYPE,
       T.SETTL_AMT,
       P.BASIS,
       R.EXEC_RATE,
       D.CUR_DATE
	   FROM TD_TRD_TPOS T
	   LEFT JOIN TD_PRODUCT_APPROVE_MAIN P
	    ON T.DEAL_NO = P.DEAL_NO
	   AND T.VERSION = P.VERSION
	   LEFT JOIN TD_OVERDUE_CONFIRM C
	   ON C.REF_NO=P.DEAL_NO
	   LEFT JOIN TT_TRD_TRADE TTT ON TTT.TRADE_ID = C.DEAL_NO AND TTT.TRADE_STATUS='12'
	   LEFT JOIN TT_RATE_RANGE R
	    ON T.DEAL_NO = R.DEAL_NO
	   AND T.VERSION = R.VERSION, TT_DAYEND_DATE D
	   WHERE ((R.BEGIN_DATE <= D.CUR_DATE AND D.CUR_DATE < R.END_DATE) OR
	       (R.BEGIN_DATE = D.CUR_DATE AND D.CUR_DATE = R.END_DATE))
	   AND NOT EXISTS (SELECT *
	          FROM TD_ACC_TRD_DAILY ACC
	         WHERE ACC.POST_DATE = D.CUR_DATE
	           AND ACC.DEAL_NO = T.DEAL_NO
	           AND ACC.ACC_TYPE = 'interest_Overdue')
	 * @param dealNo  逾期
	 */
	public void getNeedForAccDrawingDatesForOverDueStepTow(String dealNo);
	
	/**
	 * SELECT 'amor_Normal' as ACC_TYPE,T.DEAL_NO,T.VERSION,P.PRD_NO,P.INVTYPE,T.SETTL_AMT,
		P.BASIS,P.CONTRACT_RATE,D.CUR_DATE,P.PROC_AMT,T.DIS_AMT,C.ACT_AMOR_AMT,C.CASHFLOW_ID
		FROM TD_TRD_TPOS T
		LEFT JOIN TD_PRODUCT_APPROVE_MAIN P ON T.DEAL_NO = P.DEAL_NO AND T.VERSION=P.VERSION 
		LEFT JOIN TD_CASHFLOW_AMOR C ON T.DEAL_NO=C.DEAL_NO AND T.VERSION=C.VERSION 
		,TT_DAYEND_DATE D
		WHERE C.POSTDATE = D.CUR_DATE AND NOT EXISTS (SELECT * FROM TD_ACC_TRD_DAILY ACC WHERE ACC.POST_DATE=D.CUR_DATE
		AND ACC.DEAL_NO = T.DEAL_NO AND ACC.ACC_TYPE='amor_Normal')
	 * @param params
	 * @return
	 */
	public void getNeedForAccDrawingDatesForAmorStepThree(String dealNo);
	
	public Page<TdAccTrdDailyVo> getTdAccTrdDailyVos(Map<String, Object> params);

	public List<TdAccTrdDaily> getTdAccTrdDaily(Map<String, Object> params);
	
	
	/**
	 * 
	 * SELECT M.*
	  FROM TD_ADVANCE_MATURITY M
	  LEFT JOIN TD_TRD_TPOS T
	    ON M.DEAL_NO = T.DEAL_NO
	  LEFT JOIN TT_TRD_TRADE TT
	    ON M.DEAL_NO = TT.TRADE_ID
	   AND TT.TRADE_STATUS = '12', TT_DAYEND_DATE D
	  WHERE M.AM_DATE = D.CUR_DATE
	  AND NOT EXISTS(SELECT * FROM TD_ACC_TRD_DAILY ACC WHERE NVL(ACC.REF_NO,'X')=M.DEAL_NO AND ACC.POST_DATE=D.CUR_DATE
	  AND ACC.ACC_TYPE like 'am_%')
	 * @param dealNo-提前到期的流水号
	 * @param refNo-提前到期关联的原交易流水
	 * 
	 * 对接来账分拣
	 * amtParams
	 * KEY=DurationConstants.AccType.AM_AMT VALUE=清分提前还款本金
	 * KEY=DurationConstants.AccType.AM_RETURN_IAMT  VALUE=清分提前还款归还利息
	 * KEY=DurationConstants.AccType.AM_PENALTY  VALUE=清分提前还款罚息
	 * KEY=DurationConstants.AccType.AM_INT   VALUE=清分提前还款利息
	 * 
	 * 前台手工测试  amtParams为null值
	 */
	public void getNeedForAccAdvanceMaturity(String dealNo,String refNo,Map<String, Object> amtParams);
	
	/**
	 * 活期交易提前还款
	 * SELECT M.*
	  FROM TD_ADVANCE_MATURITY_CURRENT M
	  LEFT JOIN TD_TRD_TPOS T
	    ON M.DEAL_NO = T.DEAL_NO
	  LEFT JOIN TT_TRD_TRADE TT
	    ON M.DEAL_NO = TT.TRADE_ID
	   AND TT.TRADE_STATUS = '12', TT_DAYEND_DATE D
	  WHERE M.AM_DATE = D.CUR_DATE
	  AND NOT EXISTS(SELECT * FROM TD_ACC_TRD_DAILY ACC WHERE NVL(ACC.REF_NO,'X')=M.DEAL_NO AND ACC.POST_DATE=D.CUR_DATE
	  AND ACC.ACC_TYPE like 'am_%')
	 * @param dealNo-提前到期的流水号
	 * @param refNo-提前到期关联的原交易流水
	 * 
	 * 对接来账分拣
	 * amtParams
	 * KEY=DurationConstants.AccType.AM_AMT VALUE=清分提前还款本金
	 * KEY=DurationConstants.AccType.AM_RETURN_IAMT  VALUE=清分提前还款归还利息
	 * KEY=DurationConstants.AccType.AM_INT   VALUE=清分提前还款利息
	 * 
	 * 前台手工测试  amtParams为null值
	 */
	public void getNeedForAccAdvanceMaturityCurrent(String dealNo, String refNo,Map<String, Object> amtParams);

	public Page<TdAdvanceMaturity> getTdAdvanceMaturity( Map<String, Object> params);
	
	public Page<TdAccTrdDailyAmorVo> getAccTrdDailyAmor(
			Map<String, Object> params);
	
	/**
	**	判断当天是否为结息日，如果是，
	*	筛选需要结息的交易数据
		数据逻辑：从TD_TRD_TPOS获得还有剩余本金的持仓数据
		关联交易核实单TD_PRODUCT_APPROVE_MAIN  TRD开头交易
		关联利率区间表TT_RATE_RANGE 获得实时的利率
		关联收付息计划表TD_CASHFLOW_INTEREST 获得区间收息起始日和截止日
		关联账务日期表TT_DAYEND_DATE 明确计提日（账务日）
	 */
	public void getNeedForSettleDatasStepOne(String dealNo);
	
	/**
	 * 判断逾期确认日期是否等于当前账务日期，如果是，需要把计提进行冲销
	 * @param dealNo
	 */
	public void getNeedFeeWriteOffDatesForOverdueStepOne(String dealNo);
	
	
	
	public void getNeedForMtmDatasStepOne(String dealNo);
	
}
