package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.PdAttrInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.ifs.model.*;

import java.util.List;
import java.util.Map;

public interface IfsLimitService {

	// 查询
	public Page<IfsLimitCondition> searchPageLimit(Map<String, Object> map);

	// 修改
	public void update(IfsLimitCondition entity);

	// 新增
	public void insert(IfsLimitCondition entity);

	// 删除
	public void deleteById(String id);

	public IfsLimitCondition searchIfsLimitCondition(String id);

	// 根据限额字段 查询出所有 的 字典值 等 具体信息（额度值）
	public List<Map<String, Object>> searchColumsValue(Map<String, String> map);

	List<Map<String, Object>> searchResult(Map<String, String> map);

	// 20180531新增：查询交易员的总授信额度、总可用额度
	Page<List<Map<String, Object>>> searchTraderTotalAndAvlAmt(Map<String, Object> map);

	// 20180531新增：查询交易员的分授信额度、分可用额度
	Page<IfsLimitCondition> searchTraderEdu(Map<String, Object> map);

	// 外汇限额新增
	public void insertLimit(Map<String, Object> map);

	// 查询
	public Page<ifsLimitTemplate> searchTemplateLimit(Map<String, Object> map);

	// 修改外汇限额
	public void updateLimit(Map<String, Object> map);

	// 删除
	public void deleteIfsLimit(Map<String, Object> map);

	// 外汇限额新增
	public void insertBond(Map<String, Object> map);

	// 查询
	public Page<BondTemplate> searchTemplateBond(Map<String, Object> map);

	// 删除
	public void deleteIfsBond(Map<String, Object> map);

	// 修改外汇限额
	public void updateBond(Map<String, Object> map);

	/**
	 * 占用限额
	 * 
	 * @param map
	 * @return
	 */
	public Map<String, Object> occupyLimit(Map<String, Object> map);

	/**
	 * 查询所有成本中心
	 * 
	 * @return
	 */
	public List<IfsOpicsCost> searchCostList(Map<String, Object> params);

	/**
	 * 取消限额
	 * 
	 * @param map
	 * @return
	 */
	public Map<String, Object> revokeLimit(Map<String, Object> map);

	/**
	 * 查询限额
	 * 
	 * @param map
	 * @return
	 */
	public double queryLimit(Map<String, Object> map);

	/**
	 * 插入产品属性信息表
	 */
	public void insertCredit(Map<String, Object> map);

	/**
	 * 查询所有
	 */
	Page<PdAttrInfoArray> searchAllProd(Map<String, Object> map);

	/**
	 * 查询是否有当前审批件编号
	 */
	List<PdAttrInfoArray> searchProd(String approveFlNo);

	/**
	 * 插入业务种类信息表
	 */
	public void insertBussType(Map<String, Object> map);

	/**
	 * 已经通过授信的主体加入到准入名单
	 */
	public void insertLimitToAccess(Map<String, Object> map);

	/**
	 * 插入业务种类信息表(黑名单)
	 */
	public void insertBussTypeBk(Map<String, Object> map);

	/**
	 * 查询是否有当前业务种类
	 */
	List<SlIfsBean> searchBuss(Map<String, Object> map);

	/**
	 * 查询所有业务种类
	 */
	Page<BussVrtyInfoArray> searchAllBuss(Map<String, Object> map);

	/**
	 * 查询所有业务种类(黑名单)
	 */
	Page<BussVrtyInfoArray> searchAllBussBk(Map<String, Object> map);

	/**
	 * 根据ECIF客户号更新某一条数据
	 */
	void updateByEcifNo(Map<String, Object> map);

	/**
	 * 根据ECIF客户号更新某一条数据(黑名单)
	 */
	void updateByEcifNoBk(Map<String, Object> map);

	/**
	 * 根据ECIF客户号删除某一条数据
	 */
	void deleteByEcifNo(Map<String, Object> map);

	/**
	 * 根据ECIF客户号删除某一条数据(黑名单)
	 */
	void deleteByEcifNoBk(Map<String, Object> map);

	/**
	 * 客户准入 先查询，后插入
	 */
	String doBussExcelPropertyToDataBase(List<IfsTradeAccess> slIfsBean);

	/**
	 * 根据审批件编号更新某一条数据
	 */
	void updateByAppNo(Map<String, Object> map);

	/**
	 * 根据审批件编号删除某一条数据
	 */
	void deleteByAppNo(Map<String, Object> map);

	/**
	 * 风险审查 先查询，后插入
	 */
	String doSIngleExcelPropertyToDataBase(List<PdAttrInfoArray> pdAttrInfo);

	/**
	 * 查询所有业务种类(返回list)
	 */
	List<SlIfsBean> searchAllData();

	/**
	 * 同业准入 查询本地数据
	 */
	Page<SlIfsBean> searchLocalInfo(Map<String, String> map);

	/**
	 * 同业准入 查询本地数据(黑名单)
	 */
	List<SlIfsBean> searchLocalInfoBk(Map<String, String> map);

	/**
	 * 风险审查 查询本地数据
	 */
	List<PdAttrInfoArray> searchLocalData(Map<String, String> map);

	/**
	 * 风险审查 查询所有数据
	 */
	List<PdAttrInfoArray> searchAllDataRisk();

	// 外汇限额
	public Page<IfsLimitTemplateDetail> searchAllTemplateDetail(Map<String, Object> map);

	// 债券限额
	public Page<IfsLimitBondDetail> searchAllBondDetail(Map<String, Object> map);

	/**
	 * 同业准入 查询是否有此条记录
	 */
	List<SlIfsBean> queryBussInfo(Map<String, Object> map);

	/**
	 * 同业准入(黑名单) 查询是否有此条记录
	 */
	List<SlIfsBean> queryBussInfoBk(Map<String, Object> map);

	/**
	 * 查询历史明细
	 */
	Page<IfsTradeAccess> searchLocalDetail(Map<String, Object> map);

	// 债券限额
	public Page<InvestLimtlog> getBondInvestmentLimit(Map<String, Object> map);

	// 外币明细限额
	public Page<ForeignLimitLog> getForeignLimit(Map<String, Object> map);

	// 外币明细查询当日更新
	void updateForeignLimit();

	// 更新当日债券报表明细
	void updateBondLimit();
}