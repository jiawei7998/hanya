package com.singlee.cap.base.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Range;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.base.mapper.TbkEntrySettingMapper;
import com.singlee.cap.base.model.TbkEntrySetting;
import com.singlee.cap.base.model.TbkEntrySettingBr;
import com.singlee.cap.base.service.TbkEntrySettingService;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;

/*
 * 会计分录表serviceIMpl
 */
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service
public class TbkEntrySettingServiceImpl implements TbkEntrySettingService {
	@Autowired
	private TbkEntrySettingMapper tbkEntrySettingMapper;
	@Autowired
	BatchDao batchDao;

	@Override
	public TbkEntrySetting save(TbkEntrySetting tbkEntrySetting) {
		tbkEntrySettingMapper.insert(tbkEntrySetting);
		return tbkEntrySetting;
	}

	@Override
	public TbkEntrySetting update(TbkEntrySetting tbkEntrySetting) {
		tbkEntrySettingMapper.updateByPrimaryKey(tbkEntrySetting);
		return tbkEntrySetting;
	}

	@Override
	public void delTbkEntrySetting(String string) {
		tbkEntrySettingMapper.delTbkEntrySetting(string);
	}

	/**
	 * 
	 */
	@Override
	public List<TbkEntrySetting> getEntrySettingList(String sceneId, String eventId) {
		return tbkEntrySettingMapper.getSettingList(sceneId, eventId);
	}

	/**
	 * 
	 */
	@Override
	public List<TbkEntrySetting> getEntrySettingList(TbkEntrySetting tbkEntrySetting) {
		return tbkEntrySettingMapper.select(tbkEntrySetting);
	}

	/**
	 * 查询entrySetting VO对象
	 */
	@Override
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String, Object> map, RowBounds rb) {
		return tbkEntrySettingMapper.getBkEntrySettingList(map, rb);
	}

	/**
	 * 保存维度信息
	 * 
	 * @param TbkEntrySetting
	 * @author huqin
	 * @date 2017-4-19
	 */
	@Override
	public void saveEntrySetting(Map<String, Object> map) {
		TbkEntrySetting tbkEntrySetting = new TbkEntrySetting();
		BeanUtil.populate(tbkEntrySetting, map);
		// 修改settingGroupId
		if (tbkEntrySetting.getSettingGroupId() != null && !"".equals(tbkEntrySetting.getSettingGroupId())) {
			// 修改时先删除原数据，再重新插入数据
			tbkEntrySettingMapper.deleteBySettingGroupId(tbkEntrySetting.getSettingGroupId());
		} else {// 新增
			tbkEntrySetting.setSettingGroupId(tbkEntrySettingMapper.querySettingGroupId());
		}
		tbkEntrySetting.setEventId(tbkEntrySetting.getEventId());
		// 插入数据
		String constact_listJson = ParameterUtil.getString(map, "constact_list", "");
		List<TbkEntrySettingBr> conList = FastJsonUtil.parseArrays(constact_listJson, TbkEntrySettingBr.class);
		if (conList.size() > 0) {
			for (int i = 0; i < conList.size(); i++) {
				tbkEntrySetting.setDebitCreditFlag(conList.get(i).getDebitCreditFlag()); // 借贷标识
				tbkEntrySetting.setSubjCode(conList.get(i).getSubjCode());// 科目代码
				tbkEntrySetting.setSubjName(conList.get(i).getSubjName());// 科目名称
				tbkEntrySetting.setExpression(conList.get(i).getExpression());
				tbkEntrySetting.setSort(i+1);
				tbkEntrySettingMapper.insert(tbkEntrySetting);
			}
		}
	}

	@Override
	public Page<TbkEntrySetting> getBkEntrySettingList(Map<String, Object> map) {
		return tbkEntrySettingMapper.getBkEntrySettingList(map);
	}

	@Override
	public TbkEntrySetting selectEntrySetting(Map<String, Object> params) {
		TbkEntrySetting acct = new TbkEntrySetting();
		List<TbkEntrySetting> list = getBkEntrySettingList(params, ParameterUtil.getRowBounds(params)).getResult();
		if (list.size() > 0) {
			acct.setSettingGroupId(list.get(0).getSettingGroupId());
			acct.setSettingDesc(list.get(0).getSettingDesc());
			List<TbkEntrySettingBr> volist = new ArrayList<TbkEntrySettingBr>();
			TbkEntrySettingBr vo = null;
			for (TbkEntrySetting setacctDef : list) {
				HashMap<String, Object> map = BeanUtil.beanToHashMap(setacctDef);
				vo = new TbkEntrySettingBr();
				BeanUtil.populate(vo, map);
				volist.add(vo);
			}
			acct.setConstact_list(volist);
		}
		return acct;
	}

	/**
	 * 删除
	 * 
	 * @author huqin
	 * @date 2017-4-20
	 */
	@Override
	public void deleteEntrySetting(String[] settingGroupId) {
		for (int i = 0; i < settingGroupId.length; i++) {
			tbkEntrySettingMapper.deleteBySettingGroupId(settingGroupId[i]);
		}
	}

	/**
	 * 导入配置文件后，更新配置数据
	 */
	@Override
	public String uploadDefExcel(Workbook wb, boolean rebuild) {
		StringBuffer sb = new StringBuffer();
		try {
			TbkEntrySetting setacctDef = null;
			Sheet sheet = wb.getSheet(0);
			Range[] rangeCell = sheet.getMergedCells();
			String acctdesc = "";
			String acctdesc1 = "";
			List<TbkEntrySetting> settingList = new ArrayList<TbkEntrySetting>();
			String settingGroupId = "";
			System.out.println(sheet.getRows());
			for (int i = 0; i < sheet.getRows(); i++) {
				setacctDef = new TbkEntrySetting();
				System.out.println("第" + i + "行");
				for (int j = 0; j < sheet.getColumns(); j++) {
					String returnStr = "";
					returnStr = sheet.getCell(j, i).getContents();
					for (Range r : rangeCell) {
						// 判断是否具有合并单元格
						if (i > r.getTopLeft().getRow() && i <= r.getBottomRight().getRow()
								&& j >= r.getTopLeft().getColumn() && j <= r.getBottomRight().getColumn()) {
							returnStr = sheet.getCell(r.getTopLeft().getColumn(), r.getTopLeft().getRow())
									.getContents();
						}
					}
					returnStr = returnStr.replaceAll("\n", "");
//					if (returnStr == null || returnStr.equals("")) {
//						break;
//					}
					if (j == 0) {
						setacctDef.setSettingDesc(returnStr);
					} else if (j == 1) {
						acctdesc = setacctDef.getSettingDesc() + returnStr.split("/")[0];
						setacctDef.setEventId(returnStr != null ? returnStr.split("/")[0] : "");// 会计事件
						if (!acctdesc.equals(acctdesc1)) {
							settingGroupId = tbkEntrySettingMapper.querySettingGroupId();
							acctdesc1 = acctdesc;
						}
					} else if (j == 2) {
						setacctDef.setSort(Integer.valueOf(returnStr));
					} else if (j == 3) {
						setacctDef.setDebitCreditFlag(returnStr);
					} else if (j == 4) { 
						setacctDef.setSubjCode(returnStr); // 科目号
					} else if (j == 5) { // 金额代码
						setacctDef.setExpression(returnStr != null && !"".equals(returnStr) ? returnStr.split("/")[1] : "");
					} else if (j == 6) {// 科目名称
						setacctDef.setSubjName(returnStr);
					}
				}
				System.out.println(settingGroupId);
				setacctDef.setSettingGroupId(settingGroupId);// 套号
				settingList.add(setacctDef);
				tbkEntrySettingMapper.insert(setacctDef);
			}
//			batchDao.batch("com.singlee.cap.base.mapper.TbkEntrySettingMapper.insert", settingList);
			sb.append(String.format("会计分录导入(%d)成功!", sheet.getRows()));
		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return sb.toString();
	}

	@Override
	public Page<TbkEntrySetting> getScentEntrySettingList(
			Map<String, Object> map, RowBounds rb) {
		return tbkEntrySettingMapper.getScentEntrySettingList(map, rb);
	}

}
