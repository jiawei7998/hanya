package com.singlee.capital.trade.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdLiabilitiesDuartionMapper;
import com.singlee.capital.trade.model.TdLiabilitiesDuration;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.LiabilitiesDurationService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 负债存续期
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class LiabilitiesDurationServiceImpl implements LiabilitiesDurationService {

	@Autowired
	private TdLiabilitiesDuartionMapper LiabilitiesDurationMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;

	@Autowired
	private FlowOpService flowOpService;
	
	@Override
	public TdLiabilitiesDuration getLiabilitiesDurationById(String dealNo) {
		return LiabilitiesDurationMapper.getLiabilitiesDurationById(dealNo);
	}

	@Override
	public Page<TdLiabilitiesDuration> getLiabilitiesDurationPage(
			Map<String, Object> params, int isFinished) {
		String approveStatus = ParameterUtil.getString(params, "AMStatus", null);
		Page<TdLiabilitiesDuration> page= new Page<TdLiabilitiesDuration>();
		if(approveStatus != null && !"".equals(approveStatus)){
			String [] AMStatus = approveStatus.split(",");
			params.put("AMStatus", Arrays.asList(AMStatus));
		}
		if(isFinished == 1){
			page=LiabilitiesDurationMapper.getLiabilitiesDurationList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=LiabilitiesDurationMapper.getLiabilitiesDurationListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {
			page=LiabilitiesDurationMapper.getLiabilitiesDurationListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		List<TdLiabilitiesDuration> list =page.getResult();
		for(TdLiabilitiesDuration tm :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(tm.getRefNo());
			if(main!=null){
				tm.setProduct(main.getProduct());
				tm.setParty(main.getCounterParty());
				tm.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建负债存续期金融工具表")
	public void createLiabilitiesDuration(Map<String, Object> params) {
		LiabilitiesDurationMapper.insert(LiabilitiesDuration(params));
	}

	@Override
	@AutoLogMethod(value = "修改负债存续期金融工具表")
	public void updateLiabilitiesDuration(Map<String, Object> params) {
		LiabilitiesDurationMapper.updateByPrimaryKey(LiabilitiesDuration(params));
	}

	
	private TdLiabilitiesDuration LiabilitiesDuration(Map<String, Object> params){
		// map转实体类
		TdLiabilitiesDuration liabilitiesDuration = new TdLiabilitiesDuration();
		try {
			BeanUtil.populate(liabilitiesDuration, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}


		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdLiabilitiesDuration duartion = new TdLiabilitiesDuration();
        duartion.setDealNo(String.valueOf(params.get("dealNo")));
        duartion.setDealType(liabilitiesDuration.getDealType());
        duartion = LiabilitiesDurationMapper.searchLiabilitiesDuration(duartion);

        if(liabilitiesDuration.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(liabilitiesDuration.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
            
            liabilitiesDuration.setVersion(productApproveMain.getVersion());
        }
       

    	liabilitiesDuration.setSponsor(SlSessionHelper.getUserId());
    	liabilitiesDuration.setSponInst(SlSessionHelper.getInstitutionId());
        liabilitiesDuration.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return liabilitiesDuration;
	}
	
	/**
	 * 删除（负债存续期+order）
	 */
	@Override
	public void deleteLiabilitiesDuration(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			LiabilitiesDurationMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	/**
	 * 升版本 
	 * @param order_id
	 */
	@Override
	public void dealVersionAndCashFlow(String order_id) {
		
		//1.升版本
		TdLiabilitiesDuration liabilitiesDuration = LiabilitiesDurationMapper.getLiabilitiesDurationById(order_id);
		JY.require(liabilitiesDuration!=null, "交易编号%s对应的负债存续期信息不存在!");
		TdProductApproveMain approveMain = new TdProductApproveMain();
		approveMain = productApproveService.getProductApproveActivated(liabilitiesDuration.getiCode());
		JY.require(liabilitiesDuration.getVersion() == approveMain.getVersion(), "版本不同，该笔交易无法通过！");
		productApproveService.updateProductApproveMainVersion(approveMain);
		
		//2.修改新版本金额
		//double newAmt = approveMain.getAmt() - Double.valueOf(liabilitiesDuration.getAmAmt());
		//approveMain.setAmt(newAmt);
		int i = productApproveService.modifyProductApproveMain(approveMain);	//修改数据
		JY.require(i==1, "更新原交易金额有误！");
	}
	
	
	/***
	 * ----以上为对外服务-------------------------------
	 */
}
