package com.singlee.rules.core;

import com.singlee.rules.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Inference {@link RulesEngine} implementation.
 *
 * Rules are selected based on given facts and fired according to their natural order which is priority by default.
 *
 * The engine continuously selects and fires rules until no more rules are applicable.
 *
 * @author Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 */
public final class InferenceRulesEngine implements RulesEngine {

    private static final Logger LOGGER = LoggerFactory.getLogger(InferenceRulesEngine.class);

    private RulesEngineParameters parameters;
    private List<RuleListener> ruleListeners;
    private List<RulesEngineListener> rulesEngineListeners;
    private DefaultRulesEngine delegate;

    /**
     * Create a new inference rules engine with default parameters.
     */
    public InferenceRulesEngine() {
        this(new RulesEngineParameters());
    }

    /**
     * Create a new inference rules engine.
     *
     * @param parameters of the engine
     */
    public InferenceRulesEngine(RulesEngineParameters parameters) {
        this.parameters = parameters;
        delegate = new DefaultRulesEngine(parameters);
        ruleListeners = new ArrayList<RuleListener>();
        rulesEngineListeners = new ArrayList<RulesEngineListener>();
    }

    @Override
    public RulesEngineParameters getParameters() {
        return parameters;
    }

    @Override
    public List<RuleListener> getRuleListeners() {
        return ruleListeners;
    }

    @Override
    public List<RulesEngineListener> getRulesEngineListeners() {
        return rulesEngineListeners;
    }

    @Override
    public void fire(Rules rules, Facts facts) {
        Set<Rule> selectedRules;
        do {
            LOGGER.info("Selecting candidate rules based on the following facts: {}", facts);
            selectedRules = selectCandidates(rules, facts);
            if(!selectedRules.isEmpty()) {
                delegate.doFire(new Rules(selectedRules), facts);
            } else {
                LOGGER.info("No candidate rules found for facts: {}", facts);
            }
        } while (!selectedRules.isEmpty());
    }

    private Set<Rule> selectCandidates(Rules rules, Facts facts) {
        Set<Rule> candidates = new TreeSet<Rule>();
        for (Rule rule : rules) {
            if (rule.evaluate(facts)) {
                candidates.add(rule);
            }
        }
        return candidates;
    }

    @Override
    public Map<Rule, Boolean> check(Rules rules, Facts facts) {
        return delegate.check(rules, facts);
    }

    /**
     * Register a rule listener.
     * @param ruleListener to register
     */
    public void registerRuleListener(RuleListener ruleListener) {
        ruleListeners.add(ruleListener);
        delegate.registerRuleListener(ruleListener);
    }

    /**
     * Register a list of rule listener.
     * @param ruleListeners to register
     */
    public void registerRuleListeners(List<RuleListener> ruleListeners) {
        this.ruleListeners.addAll(ruleListeners);
        delegate.registerRuleListeners(ruleListeners);
    }

    /**
     * Register a rules engine listener.
     * @param rulesEngineListener to register
     */
    public void registerRulesEngineListener(RulesEngineListener rulesEngineListener) {
        rulesEngineListeners.add(rulesEngineListener);
        delegate.registerRulesEngineListener(rulesEngineListener);
    }

    /**
     * Register a list of rules engine listener.
     * @param rulesEngineListeners to register
     */
    public void registerRulesEngineListeners(List<RulesEngineListener> rulesEngineListeners) {
        this.rulesEngineListeners.addAll(rulesEngineListeners);
        delegate.registerRulesEngineListeners(rulesEngineListeners);
    }
}
