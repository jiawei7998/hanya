package com.singlee.ifs.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.model.IfsRevBred;
import com.singlee.ifs.service.IfsRevBredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 债券类Controller
 * 
 * @author jiangzx
 * @date: 2018年7月24日 下午3:18:53 
 * @version
 * @since JDK 1.6
 */

@Controller
@RequestMapping(value = "/IfsRevBredController")
public class IfsRevBredController {
	@Autowired
	IfsRevBredService ifsRevBredService;
	
	/**
	 * 新增
	 */
	/*@ResponseBody
	@RequestMapping(value = "/saveRevBredAdd")
	public RetMsg<Serializable> saveRevBredAdd(@RequestBody IfsRevBred entity) {
		ifsRevBredService.insert(entity);
		return RetMsgHelper.ok();
	}*/

	/**
	 * 修改
	 */
	/*@ResponseBody
	@RequestMapping(value = "/saveRevBredEdit")
	public RetMsg<Serializable> saveRevBredEdit(@RequestBody IfsRevBred entity) {
		ifsRevBredService.updateByPrimaryKey(entity);
		return RetMsgHelper.ok();
	}*/
	
	/**
	 * 保存 债券类要素
	 */
	@ResponseBody
	@RequestMapping(value = "/saveRevBredParam")
	public RetMsg<Serializable> saveForexParam(@RequestBody IfsRevBred entity) {
		String ticketId = entity.getTicketId();
		if (null == ticketId || "".equals(ticketId)) {// 新增
			ifsRevBredService.insert(entity);
		} else {// 修改
			ifsRevBredService.updateByPrimaryKey(entity);
		}
		return RetMsgHelper.ok(); 
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteRevBred")
	public RetMsg<Serializable> deleteRevBred(@RequestBody Map<String, String> map) {
		String ticketId = map.get("ticketId");
		ifsRevBredService.deleteByPrimaryKey(ticketId);
		return RetMsgHelper.ok();
	}
	
	
	
	/**
	 * 通过ticketId查找单条数据
	 */
	@ResponseBody
	@RequestMapping(value = "/selectRevBredByid")
	public RetMsg<IfsRevBred> selectByPrimaryKey(@RequestBody Map<String, Object> map) {
		IfsRevBred ifsRevBred = ifsRevBredService.selectByPrimaryKey(map);
		return RetMsgHelper.ok(ifsRevBred);
	}
	
	/**
	 * 分页查询
	 * ticketId 审批单流水号(可选)
	 * dealno 交易流水号(可选)
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRevBred")
	public RetMsg<PageInfo<IfsRevBred>> searchPageType(@RequestBody Map<String, Object> map) {
		Page<IfsRevBred> page = ifsRevBredService.searchPageRevBred(map);
		return RetMsgHelper.ok(page);
	}
	
/***************************************************************************************/
	/**
	 * 查询债券冲销     我发起的
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevBredMine")
	public RetMsg<PageInfo<IfsRevBred>> searchRevBredMine(@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevBred> page = ifsRevBredService.getRevBredPage(params, 3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询 债券冲销       待审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevBredUnfinished")
	public RetMsg<PageInfo<IfsRevBred>> searchRevBredUnfinished(@RequestBody Map<String, Object> params) {
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + ","
				+ DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevBred> page = ifsRevBredService.getRevBredPage(params, 1);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询 债券冲销      已审批
	 * 
	 * @param params
	 * @return
	 * @author lij
	 * @date 2018-08-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRevBredFinished")
	public RetMsg<PageInfo<IfsRevBred>> searchRevBredFinished(@RequestBody Map<String, Object> params) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if (StringUtil.isNullOrEmpty(approveStatus)) {
			approveStatus = DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			approveStatus += "," + DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<IfsRevBred> page = ifsRevBredService.getRevBredPage(params, 2);
		return RetMsgHelper.ok(page);
	}
}
