package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSetmBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsSetmMapper;
import com.singlee.ifs.model.IfsOpicsSetm;
import com.singlee.ifs.service.IfsOpicsSetmService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 清算方式
 * 
 * @author yanming
 */

@Service
public class IfsOpicsSetmServiceImpl implements IfsOpicsSetmService {

	@Resource
	IfsOpicsSetmMapper ifsOpicsSetmMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsSetm entity) {

		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsSetmMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsSetm entity) {

		entity.setLstmntdte(new Date());
		ifsOpicsSetmMapper.updateById(entity);
	}

	@Override
	public void deleteById(IfsOpicsSetm entity) {

		ifsOpicsSetmMapper.deleteById(entity);

	}

	@Override
	public Page<IfsOpicsSetm> searchPageOpicsSetm(Map<String, Object> map) {

		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsSetm> result = ifsOpicsSetmMapper.searchPageOpicsSetm(map, rb);
		return result;
	}

	@Override
	public IfsOpicsSetm searchById(IfsOpicsSetm entity) {

		return ifsOpicsSetmMapper.searchById(entity);
	}

	@Override
	public void updateStatus(IfsOpicsSetm entity) {

		ifsOpicsSetmMapper.updateStatus(entity);

	}

	@Override
	public SlOutBean batchCalibration(List<IfsOpicsSetm> list) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从opics库查询所有的数据
			List<SlSetmBean> listSetmOpics = iStaticServer.synchroSetm();

			if (list.size() > 0) {
				/***** 对选中的行进行校准 ****************************/
				for (int i = 0; i < list.size(); i++) {
					for (int j = 0; j < listSetmOpics.size(); j++) {
						// 若opics库里有相同的机构分支和清算方式,就将本地库的更新成opics里的,其中备注还是用本地的,不更新
						if (listSetmOpics.get(j).getBr().trim().equals(list.get(i).getBr()) && listSetmOpics.get(j).getSettlmeans().trim().equals(list.get(i).getSettlmeans())) {
							IfsOpicsSetm entity = ifsOpicsSetmMapper.searchById(list.get(i));
							entity.setBr(listSetmOpics.get(j).getBr().trim());
							entity.setSettlmeans(listSetmOpics.get(j).getSettlmeans().trim());
							String descr = (String) listSetmOpics.get(j).getDescr();
							if ("".equals(descr) || descr == null) {
								entity.setDescr("");
							} else {
								entity.setDescr(descr.trim());
							}

							String checkdigit = listSetmOpics.get(j).getCheckdigit();
							if ("".equals(checkdigit) || checkdigit == null) {
								entity.setCheckdigit("");
							} else {
								entity.setCheckdigit(checkdigit.trim());
							}

							String genledgno = listSetmOpics.get(j).getGenledgno();
							if ("".equals(genledgno) || genledgno == null) {
								entity.setGenledgno("");
							} else {
								entity.setGenledgno(genledgno.trim());
							}

							String acctval = listSetmOpics.get(j).getAcctval();
							if ("".equals(acctval) || acctval == null) {
								entity.setAcctval("");
							} else {
								entity.setAcctval(acctval.trim());
							}

							String valacc = listSetmOpics.get(j).getValacc();
							if ("".equals(valacc) || valacc == null) {
								entity.setValacc("");
							} else {
								entity.setValacc(valacc.trim());
							}

							Date lstmntdte = listSetmOpics.get(j).getLstmntdte();
							if ("".equals(lstmntdte) || lstmntdte == null) {
								entity.setLstmntdte(new Date());
							} else {
								entity.setLstmntdte(lstmntdte);
							}

							String extintflag = listSetmOpics.get(j).getExtintflag();
							if ("".equals(extintflag) || extintflag == null) {
								entity.setExtintflag("");
							} else {
								entity.setExtintflag(extintflag.trim());
							}

							String offacct = listSetmOpics.get(j).getOffacct();
							if ("".equals(offacct) || offacct == null) {
								entity.setOffacct("");
							} else {
								entity.setOffacct(offacct.trim());
							}

							String autoauthorize = listSetmOpics.get(j).getAutoauthorize();
							if ("".equals(autoauthorize) || autoauthorize == null) {
								entity.setAutoauthorize("");
							} else {
								entity.setAutoauthorize(autoauthorize.trim());
							}

							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");
							ifsOpicsSetmMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listSetmOpics.size() - 1) {
							ifsOpicsSetmMapper.updateStatus0ById(list.get(i));
						}

					}
				}
			} else {
				/***** 全部进行校准 ****************************/
				// 从本地库查询所有的数据
				List<IfsOpicsSetm> listSetmLocal = ifsOpicsSetmMapper.searchAllOpicsSetm();
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listSetmOpics.size(); i++) {
					IfsOpicsSetm entity = new IfsOpicsSetm();
					entity.setBr(listSetmOpics.get(i).getBr().trim());
					entity.setSettlmeans(listSetmOpics.get(i).getSettlmeans().trim());
					String descr = (String) listSetmOpics.get(i).getDescr();
					if ("".equals(descr) || descr == null) {
						entity.setDescr("");
					} else {
						entity.setDescr(descr.trim());
					}

					String checkdigit = listSetmOpics.get(i).getCheckdigit();
					if ("".equals(checkdigit) || checkdigit == null) {
						entity.setCheckdigit("");
					} else {
						entity.setCheckdigit(checkdigit.trim());
					}

					String genledgno = listSetmOpics.get(i).getGenledgno();
					if ("".equals(genledgno) || genledgno == null) {
						entity.setGenledgno("");
					} else {
						entity.setGenledgno(genledgno.trim());
					}

					String acctval = listSetmOpics.get(i).getAcctval();
					if ("".equals(acctval) || acctval == null) {
						entity.setAcctval("");
					} else {
						entity.setAcctval(acctval.trim());
					}

					String valacc = listSetmOpics.get(i).getValacc();
					if ("".equals(valacc) || valacc == null) {
						entity.setValacc("");
					} else {
						entity.setValacc(valacc.trim());
					}

					Date lstmntdte = listSetmOpics.get(i).getLstmntdte();
					// 若opics系统此条工业代码没有设置最后维护时间,就设置最新时间
					if ("".equals(lstmntdte) || lstmntdte == null) {
						entity.setLstmntdte(new Date());
					} else {
						entity.setLstmntdte(lstmntdte);
					}

					String extintflag = listSetmOpics.get(i).getExtintflag();
					if ("".equals(extintflag) || extintflag == null) {
						entity.setExtintflag("");
					} else {
						entity.setExtintflag(extintflag.trim());
					}

					String offacct = listSetmOpics.get(i).getOffacct();
					if ("".equals(offacct) || offacct == null) {
						entity.setOffacct("");
					} else {
						entity.setOffacct(offacct.trim());
					}

					String autoauthorize = listSetmOpics.get(i).getAutoauthorize();
					if ("".equals(autoauthorize) || autoauthorize == null) {
						entity.setAutoauthorize("");
					} else {
						entity.setAutoauthorize(autoauthorize.trim());
					}

					entity.setOperator(SlSessionHelper.getUserId());
					entity.setStatus("1"); // 设置为已同步

					if (listSetmLocal.size() == 0) { // 本地库无数据
						ifsOpicsSetmMapper.insert(entity);
					} else { // 本地库有数据
						for (int j = 0; j < listSetmLocal.size(); j++) {
							// opics库的机构分支和清算方式与本地的相等，就更新本地的其他的内容
							if (listSetmOpics.get(i).getBr().trim().equals(listSetmLocal.get(j).getBr()) && listSetmOpics.get(i).getSettlmeans().trim().equals(listSetmLocal.get(j).getSettlmeans())) {
								ifsOpicsSetmMapper.updateById(entity);
								break;
							}
							// 若没有，就插入本地库
							if (j == listSetmLocal.size() - 1) {
								ifsOpicsSetmMapper.insert(entity);
							}
						}
					}
				}
				// 2.遍历完opics库之后，从本地库再次查询所有的数据,若本地库的数据多于opics库，遍历本地库，若本地库里有，opics库里没有的，状态要改为未同步
				List<IfsOpicsSetm> listSetmLocal2 = ifsOpicsSetmMapper.searchAllOpicsSetm();
				if (listSetmLocal2.size() > listSetmOpics.size()) {
					for (int i = 0; i < listSetmLocal2.size(); i++) {
						for (int j = 0; j < listSetmOpics.size(); j++) {
							if (listSetmOpics.get(j).getBr().trim().equals(listSetmLocal2.get(i).getBr()) && listSetmOpics.get(j).getSettlmeans().trim().equals(listSetmLocal2.get(i).getSettlmeans())) {
								break;
							}
							if (j == listSetmOpics.size() - 1) {
								listSetmLocal2.get(i).setLstmntdte(new Date());
								listSetmLocal2.get(i).setStatus("0");
								ifsOpicsSetmMapper.updateById(listSetmLocal2.get(i));
							}
						}
					}
				}

			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

	@Override
	public Page<IfsOpicsSetm> searchAllOpicsSetm() {

		return null;
	}
}
