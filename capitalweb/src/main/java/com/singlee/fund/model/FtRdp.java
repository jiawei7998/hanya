package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_RDP")
public class FtRdp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private String dealNo;

    private String prdNo;

    private String dealType;

    private String adate;

    private String sponsor;

    private String sponinst;

    private String cno;

    private String cname;

    private String contact;

    private String contactPhone;

    private String fundCode;
    @Transient
    private String fundName;

    private String ccy;

    private String tdate;

    private String vdate;

    private BigDecimal shareAmt;

    private String approveStatus;
    
    private String isConfirm;
    
    private BigDecimal reShareAmt;
    
    private String invType;//会计类型
    
    @Transient
    private BigDecimal canShareAmt;//可以赎回的份额
    @Transient
    private BigDecimal amtOld;//点击编辑时刻的原赎回份额
   
    public BigDecimal getCanShareAmt() {
		return canShareAmt;
	}

	public void setCanShareAmt(BigDecimal canShareAmt) {
		this.canShareAmt = canShareAmt;
	}

	public BigDecimal getAmtOld() {
		return amtOld;
	}

	public void setAmtOld(BigDecimal amtOld) {
		this.amtOld = amtOld;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	@Transient
    private String taskId;
    
    public String getTaskId() {
		return taskId;
	}

	public String getIsConfirm() {
		return isConfirm;
	}

	public void setIsConfirm(String isConfirm) {
		this.isConfirm = isConfirm;
	}

	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
   
	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo == null ? null : prdNo.trim();
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType == null ? null : dealType.trim();
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate == null ? null : adate.trim();
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor == null ? null : sponsor.trim();
    }

    public String getSponinst() {
        return sponinst;
    }

    public void setSponinst(String sponinst) {
        this.sponinst = sponinst == null ? null : sponinst.trim();
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone == null ? null : contactPhone.trim();
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode == null ? null : fundCode.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate == null ? null : tdate.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public BigDecimal getShareAmt() {
        return shareAmt;
    }

    public void setShareAmt(BigDecimal shareAmt) {
        this.shareAmt = shareAmt;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus == null ? null : approveStatus.trim();
    }
}