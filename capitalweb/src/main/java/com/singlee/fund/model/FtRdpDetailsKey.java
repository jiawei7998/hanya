package com.singlee.fund.model;

import java.io.Serializable;

public class FtRdpDetailsKey implements Serializable {
    private String tradeNo;

    private String dealNo;

    private static final long serialVersionUID = 1L;
    
	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }
}