package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="FT_INFO")
public class FtInfo implements Serializable {
	@Id
    private String fundCode;

    private String fundName;

    private String fundFullName;

    private String fundType;

    private String invType;

    private String estDate;

    private String term;

    private String managComp;
    
    private String managCompNm;

    private String custodian;

    private String managerMen;
    
    private String managerMenPhone;

    private String infoDiscloser;

    private String issueType;

    private String distCoordinator;

    private BigDecimal totalQty;

    private BigDecimal trueQty;

    private String ccy;

    private BigDecimal managRate;

    private BigDecimal trustRate;

    private BigDecimal applyRate;

    private BigDecimal redeemRate;

    private String coverMode;

    private String coverDay;

    private String remark;
    
    private String vType;

    private static final long serialVersionUID = 1L;
    
    /**
     * 划款路径
     * @return
     */
    
    private String partyAcccode;

    private String partyAccname;

    private String partyBankcode;

    private String partyBankname;
    
    
    public String getvType() {
		return vType;
	}

	public void setvType(String vType) {
		this.vType = vType;
	}

	public String getPartyAcccode() {
		return partyAcccode;
	}

	public void setPartyAcccode(String partyAcccode) {
		this.partyAcccode = partyAcccode;
	}

	public String getPartyAccname() {
		return partyAccname;
	}

	public void setPartyAccname(String partyAccname) {
		this.partyAccname = partyAccname;
	}

	public String getPartyBankcode() {
		return partyBankcode;
	}

	public void setPartyBankcode(String partyBankcode) {
		this.partyBankcode = partyBankcode;
	}

	public String getPartyBankname() {
		return partyBankname;
	}

	public void setPartyBankname(String partyBankname) {
		this.partyBankname = partyBankname;
	}

	public String getManagCompNm() {
		return managCompNm;
	}

	public void setManagCompNm(String managCompNm) {
		this.managCompNm = managCompNm;
	}

	public String getManagerMenPhone() {
		return managerMenPhone;
	}

	public void setManagerMenPhone(String managerMenPhone) {
		this.managerMenPhone = managerMenPhone;
	}

	public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode == null ? null : fundCode.trim();
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName == null ? null : fundName.trim();
    }

    public String getFundFullName() {
        return fundFullName;
    }

    public void setFundFullName(String fundFullName) {
        this.fundFullName = fundFullName == null ? null : fundFullName.trim();
    }

    public String getFundType() {
        return fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType == null ? null : fundType.trim();
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType == null ? null : invType.trim();
    }

    public String getEstDate() {
        return estDate;
    }

    public void setEstDate(String estDate) {
        this.estDate = estDate == null ? null : estDate.trim();
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term == null ? null : term.trim();
    }

    public String getManagComp() {
        return managComp;
    }

    public void setManagComp(String managComp) {
        this.managComp = managComp == null ? null : managComp.trim();
    }

    public String getCustodian() {
        return custodian;
    }

    public void setCustodian(String custodian) {
        this.custodian = custodian == null ? null : custodian.trim();
    }

    public String getManagerMen() {
        return managerMen;
    }

    public void setManagerMen(String managerMen) {
        this.managerMen = managerMen == null ? null : managerMen.trim();
    }

    public String getInfoDiscloser() {
        return infoDiscloser;
    }

    public void setInfoDiscloser(String infoDiscloser) {
        this.infoDiscloser = infoDiscloser == null ? null : infoDiscloser.trim();
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType == null ? null : issueType.trim();
    }

    public String getDistCoordinator() {
        return distCoordinator;
    }

    public void setDistCoordinator(String distCoordinator) {
        this.distCoordinator = distCoordinator == null ? null : distCoordinator.trim();
    }

    public BigDecimal getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(BigDecimal totalQty) {
        this.totalQty = totalQty;
    }

    public BigDecimal getTrueQty() {
        return trueQty;
    }

    public void setTrueQty(BigDecimal trueQty) {
        this.trueQty = trueQty;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public BigDecimal getManagRate() {
        return managRate;
    }

    public void setManagRate(BigDecimal managRate) {
        this.managRate = managRate;
    }

    public BigDecimal getTrustRate() {
        return trustRate;
    }

    public void setTrustRate(BigDecimal trustRate) {
        this.trustRate = trustRate;
    }

    public BigDecimal getApplyRate() {
        return applyRate;
    }

    public void setApplyRate(BigDecimal applyRate) {
        this.applyRate = applyRate;
    }

    public BigDecimal getRedeemRate() {
        return redeemRate;
    }

    public void setRedeemRate(BigDecimal redeemRate) {
        this.redeemRate = redeemRate;
    }

    public String getCoverMode() {
        return coverMode;
    }

    public void setCoverMode(String coverMode) {
        this.coverMode = coverMode == null ? null : coverMode.trim();
    }

    public String getCoverDay() {
        return coverDay;
    }

    public void setCoverDay(String coverDay) {
        this.coverDay = coverDay == null ? null : coverDay.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}