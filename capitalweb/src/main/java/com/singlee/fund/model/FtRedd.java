package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * FT_REDD 债基分红
 * @author louhuanqing
 *
 */
@Entity
@Table(name="FT_REDD")
public class FtRedd implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private String dealNo;//交易编号

    private String prdNo;//产品名称

    private String dealType;//单据类型

    private String adate;//审批时间

    private String sponsor;//审批发起人

    private String sponinst;//审批发起机构

    private String cno;//交易对手

    private String cname;//交易对手名称

    private String contact;//联系人

    private String contactPhone;//联系电话

    private String fundCode;//基金代码

    private String ccy;//基金代码

    private BigDecimal bshareAmt;//分红时刻持仓

    private BigDecimal revalAmt;//分红时刻未实现损益

    private String vdate;//分红宣告日期
    
    private  String tdate;//分红来账日期
    
    @Transient
    private String approveStatus;//审批状态
    
    private BigDecimal amt;//分红金额
    @Transient
    private List<FtReddDetails> reddDetailsList;
    
    @Transient
    private String taskId;//
    @Transient
    private String fundName;//基金名称
    
    private BigDecimal price;//分红后的净值
    
    private String invType;
    
	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public List<FtReddDetails> getReddDetailsList() {
		return reddDetailsList;
	}

	public void setReddDetailsList(List<FtReddDetails> reddDetailsList) {
		this.reddDetailsList = reddDetailsList;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponinst() {
		return sponinst;
	}

	public void setSponinst(String sponinst) {
		this.sponinst = sponinst;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getBshareAmt() {
		return bshareAmt;
	}

	public void setBshareAmt(BigDecimal bshareAmt) {
		this.bshareAmt = bshareAmt;
	}

	public BigDecimal getRevalAmt() {
		return revalAmt;
	}

	public void setRevalAmt(BigDecimal revalAmt) {
		this.revalAmt = revalAmt;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "FtRedd [dealNo=" + dealNo + ", prdNo=" + prdNo + ", dealType=" + dealType + ", adate=" + adate
				+ ", sponsor=" + sponsor + ", sponinst=" + sponinst + ", cno=" + cno + ", cname=" + cname + ", contact="
				+ contact + ", contactPhone=" + contactPhone + ", fundCode=" + fundCode + ", ccy=" + ccy
				+ ", bshareAmt=" + bshareAmt + ", revalAmt=" + revalAmt + ", vdate=" + vdate + ", approveStatus="
				+ approveStatus + ", amt=" + amt + ", taskId=" + taskId + "]";
	}

    
}