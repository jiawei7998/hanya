package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_RDP_DETAILS_CONF")
public class FtRdpDetailsConf implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	
    private String tradeNo;//当前赎回确认的审批单据

    private String dealNo;//赎回确认审批对应的申购完成的单据号

    private BigDecimal qty;//赎回数量

    private BigDecimal amt;//赎回金额

    private BigDecimal intamt;//赎回利息

    @Transient
    private BigDecimal shareAmt;
    @Transient
    private BigDecimal reShareAmt;
    @Transient
    private BigDecimal ftpprice;
    
    public BigDecimal getShareAmt() {
		return shareAmt;
	}

	public void setShareAmt(BigDecimal shareAmt) {
		this.shareAmt = shareAmt;
	}

	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public BigDecimal getFtpprice() {
		return ftpprice;
	}

	public void setFtpprice(BigDecimal ftpprice) {
		this.ftpprice = ftpprice;
	}

	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getIntamt() {
        return intamt;
    }

    public void setIntamt(BigDecimal intamt) {
        this.intamt = intamt;
    }

	@Override
	public String toString() {
		return "FtRdpDetailsConf [tradeNo=" + tradeNo + ", dealNo=" + dealNo + ", qty=" + qty + ", amt=" + amt
				+ ", intamt=" + intamt + "]";
	}
    
}