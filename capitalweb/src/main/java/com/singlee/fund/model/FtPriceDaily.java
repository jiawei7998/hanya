package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_PRICE_DAILY")
public class FtPriceDaily implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String fundCode;
	@Id
	private String postdate;
	
	private BigDecimal fundPrice;
	
	@Transient
	private String fundName;
	
	@Transient
	private String fundType;
	
	@Transient
	private String ccy;
	
	@Transient
	private String managCompNm;
	

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public BigDecimal getFundPrice() {
		return fundPrice;
	}

	public void setFundPrice(BigDecimal fundPrice) {
		this.fundPrice = fundPrice;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getManagCompNm() {
		return managCompNm;
	}

	public void setManagCompNm(String managCompNm) {
		this.managCompNm = managCompNm;
	}

	@Override
	public String toString() {
		return "FtPriceDaily [fundCode=" + fundCode + ", postdate=" + postdate + ", fundPrice=" + fundPrice
				+ ", fundName=" + fundName + ", fundType=" + fundType + ", ccy=" + ccy + ", managCompNm=" + managCompNm
				+ "]";
	}
	
}
