package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_REIN_DETAILS")
public class FtReddDetails implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	
    private String tradeNo;//当前转投确认的审批单据

    private String dealNo;//转投确认审批对应的申购完成的单据号

    private BigDecimal amt;//转投金额
    
    private BigDecimal reShareAmt;//当前转投时刻持仓
    @Transient
    private BigDecimal shareAmt;//申购交易对应的原始确认份额
    @Transient
    private BigDecimal ftpprice;//
    private BigDecimal reAmt;//分红时刻本金
    
    private BigDecimal qty;//转投份额
    
    private BigDecimal cPrice;//分红后净值

	public BigDecimal getcPrice() {
		return cPrice;
	}
	
	public BigDecimal getReAmt() {
		return reAmt;
	}


	public void setReAmt(BigDecimal reAmt) {
		this.reAmt = reAmt;
	}


	public void setcPrice(BigDecimal cPrice) {
		this.cPrice = cPrice;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public BigDecimal getShareAmt() {
		return shareAmt;
	}

	public void setShareAmt(BigDecimal shareAmt) {
		this.shareAmt = shareAmt;
	}

	public BigDecimal getFtpprice() {
		return ftpprice;
	}

	public void setFtpprice(BigDecimal ftpprice) {
		this.ftpprice = ftpprice;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "FtReddDetails [tradeNo=" + tradeNo + ", dealNo=" + dealNo + ", amt=" + amt + ", reShareAmt="
				+ reShareAmt + ", shareAmt=" + shareAmt + ", ftpprice=" + ftpprice + ", qty=" + qty + "]";
	}
    
    
}