package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_TPOS")
public class FtTpos implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
		 *  产品代码
		 *  四分类
			基金代码
			客户编号
			币种
		 */
		private String prdNo;
		private String invtype;
		private String fundCode;
		@Transient
		private String fundName;
		private String cNo;
		private String ccy;
		private String sponInst;//机构号
		
		
		private BigDecimal utQty;//持仓份额
		private BigDecimal utAmt;//持仓金额
		
		private BigDecimal ncvAmt;//累计已转投金额（货币基金）
		private BigDecimal yield_8;//万份收益率（货币基金）净值（债券基金）
		private BigDecimal shAmt;//累计已分红金额（债券基金）
		private String postdate;//账务日期
		
		private String revalAmt;//估值
		
		private BigDecimal yUnplAmt;//截止昨日未实现损益
		private BigDecimal tUnplAmt;//截止当日未实现损益
		private BigDecimal newQty;//赎回创建占用
		private BigDecimal appQty;//审批流程中占用
		
		
		@Transient
		private String cName;
		@Transient
		private String managComp;
		@Transient
		private String managCompNm;
		
		public String getManagComp() {
			return managComp;
		}
		public void setManagComp(String managComp) {
			this.managComp = managComp;
		}
		public String getManagCompNm() {
			return managCompNm;
		}
		public void setManagCompNm(String managCompNm) {
			this.managCompNm = managCompNm;
		}
		public BigDecimal getNewQty() {
			return newQty;
		}
		public void setNewQty(BigDecimal newQty) {
			this.newQty = newQty;
		}
		public BigDecimal getAppQty() {
			return appQty;
		}
		public void setAppQty(BigDecimal appQty) {
			this.appQty = appQty;
		}
		public String getPrdNo() {
			return prdNo;
		}
		public void setPrdNo(String prdNo) {
			this.prdNo = prdNo;
		}
		public String getInvtype() {
			return invtype;
		}
		public void setInvtype(String invtype) {
			this.invtype = invtype;
		}
		public String getFundCode() {
			return fundCode;
		}
		public void setFundCode(String fundCode) {
			this.fundCode = fundCode;
		}
		
		public String getFundName() {
			return fundName;
		}
		public void setFundName(String fundName) {
			this.fundName = fundName;
		}
		public BigDecimal getUtQty() {
			return utQty;
		}
		public void setUtQty(BigDecimal utQty) {
			this.utQty = utQty;
		}
		public BigDecimal getUtAmt() {
			return utAmt;
		}
		public void setUtAmt(BigDecimal utAmt) {
			this.utAmt = utAmt;
		}
		public BigDecimal getNcvAmt() {
			return ncvAmt;
		}
		public void setNcvAmt(BigDecimal ncvAmt) {
			this.ncvAmt = ncvAmt;
		}
		public BigDecimal getYield_8() {
			return yield_8;
		}
		public void setYield_8(BigDecimal yield_8) {
			this.yield_8 = yield_8;
		}
		public BigDecimal getShAmt() {
			return shAmt;
		}
		public void setShAmt(BigDecimal shAmt) {
			this.shAmt = shAmt;
		}
		public String getPostdate() {
			return postdate;
		}
		public void setPostdate(String postdate) {
			this.postdate = postdate;
		}
		public String getSponInst() {
			return sponInst;
		}
		public void setSponInst(String sponInst) {
			this.sponInst = sponInst;
		}
		public String getRevalAmt() {
			return revalAmt;
		}
		public void setRevalAmt(String revalAmt) {
			this.revalAmt = revalAmt;
		}
		public String getcNo() {
			return cNo;
		}
		public void setcNo(String cNo) {
			this.cNo = cNo;
		}
		public String getcName() {
			return cName;
		}
		public void setcName(String cName) {
			this.cName = cName;
		}
		public String getCcy() {
			return ccy;
		}
		public void setCcy(String ccy) {
			this.ccy = ccy;
		}
		public BigDecimal getyUnplAmt() {
			return yUnplAmt;
		}
		public void setyUnplAmt(BigDecimal yUnplAmt) {
			this.yUnplAmt = yUnplAmt;
		}
		public BigDecimal gettUnplAmt() {
			return tUnplAmt;
		}
		public void settUnplAmt(BigDecimal tUnplAmt) {
			this.tUnplAmt = tUnplAmt;
		}
		@Override
		public String toString() {
			return "FtTpos [prdNo=" + prdNo + ", invtype=" + invtype + ", fundCode=" + fundCode + ", fundName="
					+ fundName + ", cNo=" + cNo + ", ccy=" + ccy + ", sponInst=" + sponInst + ", utQty=" + utQty
					+ ", utAmt=" + utAmt + ", ncvAmt=" + ncvAmt + ", yield_8=" + yield_8 + ", shAmt=" + shAmt
					+ ", postdate=" + postdate + ", revalAmt=" + revalAmt + ", yUnplAmt=" + yUnplAmt + ", tUnplAmt="
					+ tUnplAmt + ", cName=" + cName + "]";
		}
		
}
