package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_AFP_CONF")
public class FtAfpConf implements Serializable {
	@Id
    private String tradeNo;
    
    private String dealNo;

    private String prdNo;

    private String dealType;

    private String adate;

    private String sponsor;

    private String sponinst;

    private String cno;

    private String cname;

    private String contact;

    private String contactPhone;

    private String fundCode;

    private String ccy;

    private BigDecimal shareAmt;

    private BigDecimal amt;

    private BigDecimal price;

    private String tdate;

    private String vdate;

    private BigDecimal ftpprice;

    private String invType;

    private String platformInv;

    private String eventuallyInv;

    private String remark;

    private String selfAcccode;

    private String selfAccname;

    private String selfBankcode;

    private String selfBankname;

    private String partyAcccode;

    private String partyAccname;

    private String partyBankcode;

    private String partyBankname;

    private String approveStatus;
    
    private String isConfirm;

    private static final long serialVersionUID = 1L;
    
    @Transient
    private String taskId;
    @Transient
    private String fundName;
    
    @Transient
    private BigDecimal reAmt;//剩余可用于分配的金额
    @Transient
    private BigDecimal totalamt;//关联申购交易的总需要确认的本金
    @Transient
    private BigDecimal amtOld;//界面上记录这笔交易原始的分配确认金额
    
    private String handleAmt;//手续费
    /**
     * RE_SHARE_AMT	NUMBER(20,0)	Yes		35	剩余可赎回金额
	   RE_END_FLAG	VARCHAR2(1 BYTE)	Yes		36	是否赎回完毕
     */
    
    private BigDecimal reShareAmt;
    private String reEndFlag;
    
	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public String getReEndFlag() {
		return reEndFlag;
	}

	public void setReEndFlag(String reEndFlag) {
		this.reEndFlag = reEndFlag;
	}

	public String getHandleAmt() {
		return handleAmt;
	}

	public void setHandleAmt(String handleAmt) {
		this.handleAmt = handleAmt;
	}

	public BigDecimal getAmtOld() {
		return amtOld;
	}

	public void setAmtOld(BigDecimal amtOld) {
		this.amtOld = amtOld;
	}

	public BigDecimal getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(BigDecimal totalamt) {
		this.totalamt = totalamt;
	}

	public BigDecimal getReAmt() {
		return reAmt;
	}

	public void setReAmt(BigDecimal reAmt) {
		this.reAmt = reAmt;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
    public String getIsConfirm() {
		return isConfirm;
	}

	public void setIsConfirm(String isConfirm) {
		this.isConfirm = isConfirm;
	}

	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo == null ? null : prdNo.trim();
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType == null ? null : dealType.trim();
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate == null ? null : adate.trim();
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor == null ? null : sponsor.trim();
    }

    public String getSponinst() {
        return sponinst;
    }

    public void setSponinst(String sponinst) {
        this.sponinst = sponinst == null ? null : sponinst.trim();
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone == null ? null : contactPhone.trim();
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode == null ? null : fundCode.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public BigDecimal getShareAmt() {
        return shareAmt;
    }

    public void setShareAmt(BigDecimal shareAmt) {
        this.shareAmt = shareAmt;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate == null ? null : tdate.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public BigDecimal getFtpprice() {
        return ftpprice;
    }

    public void setFtpprice(BigDecimal ftpprice) {
        this.ftpprice = ftpprice;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType == null ? null : invType.trim();
    }

    public String getPlatformInv() {
        return platformInv;
    }

    public void setPlatformInv(String platformInv) {
        this.platformInv = platformInv == null ? null : platformInv.trim();
    }

    public String getEventuallyInv() {
        return eventuallyInv;
    }

    public void setEventuallyInv(String eventuallyInv) {
        this.eventuallyInv = eventuallyInv == null ? null : eventuallyInv.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getSelfAcccode() {
        return selfAcccode;
    }

    public void setSelfAcccode(String selfAcccode) {
        this.selfAcccode = selfAcccode == null ? null : selfAcccode.trim();
    }

    public String getSelfAccname() {
        return selfAccname;
    }

    public void setSelfAccname(String selfAccname) {
        this.selfAccname = selfAccname == null ? null : selfAccname.trim();
    }

    public String getSelfBankcode() {
        return selfBankcode;
    }

    public void setSelfBankcode(String selfBankcode) {
        this.selfBankcode = selfBankcode == null ? null : selfBankcode.trim();
    }

    public String getSelfBankname() {
        return selfBankname;
    }

    public void setSelfBankname(String selfBankname) {
        this.selfBankname = selfBankname == null ? null : selfBankname.trim();
    }

    public String getPartyAcccode() {
        return partyAcccode;
    }

    public void setPartyAcccode(String partyAcccode) {
        this.partyAcccode = partyAcccode == null ? null : partyAcccode.trim();
    }

    public String getPartyAccname() {
        return partyAccname;
    }

    public void setPartyAccname(String partyAccname) {
        this.partyAccname = partyAccname == null ? null : partyAccname.trim();
    }

    public String getPartyBankcode() {
        return partyBankcode;
    }

    public void setPartyBankcode(String partyBankcode) {
        this.partyBankcode = partyBankcode == null ? null : partyBankcode.trim();
    }

    public String getPartyBankname() {
        return partyBankname;
    }

    public void setPartyBankname(String partyBankname) {
        this.partyBankname = partyBankname == null ? null : partyBankname.trim();
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus == null ? null : approveStatus.trim();
    }
}