package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_REIN")
public class FtRein implements Serializable {
	@Id
    private String dealNo;

    private String prdNo;

    private String dealType;

    private String adate;

    private String sponsor;

    private String sponinst;

    private String cno;

    private String cname;

    private String contact;

    private String contactPhone;

    private String fundCode;
    @Transient
    private String fundName;

    private String ccy;
    
    private BigDecimal eshareAmt;

	private BigDecimal price;

    private BigDecimal eAmt;
    
    private static final long serialVersionUID = 1L;
    
    @Transient
    private List<FtReinDetails> reinDetailsList;
    @Transient
    private String approveStatus;//审批状态
    
    private String vdate;
    
    private String invType;
    
    public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public List<FtReinDetails> getReinDetailsList() {
		return reinDetailsList;
	}

	public void setReinDetailsList(List<FtReinDetails> reinDetailsList) {
		this.reinDetailsList = reinDetailsList;
	}

	@Transient
    private String taskId;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponinst() {
		return sponinst;
	}

	public void setSponinst(String sponinst) {
		this.sponinst = sponinst;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getEshareAmt() {
		return eshareAmt;
	}

	public void setEshareAmt(BigDecimal eshareAmt) {
		this.eshareAmt = eshareAmt;
	}

	public BigDecimal geteAmt() {
		return eAmt;
	}

	public void seteAmt(BigDecimal eAmt) {
		this.eAmt = eAmt;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "FtRein{" +
				"dealNo='" + dealNo + '\'' +
				", prdNo='" + prdNo + '\'' +
				", dealType='" + dealType + '\'' +
				", adate='" + adate + '\'' +
				", sponsor='" + sponsor + '\'' +
				", sponinst='" + sponinst + '\'' +
				", cno='" + cno + '\'' +
				", cname='" + cname + '\'' +
				", contact='" + contact + '\'' +
				", contactPhone='" + contactPhone + '\'' +
				", fundCode='" + fundCode + '\'' +
				", fundName='" + fundName + '\'' +
				", ccy='" + ccy + '\'' +
				", eshareAmt=" + eshareAmt +
				", price=" + price +
				", eAmt=" + eAmt +
				", reinDetailsList=" + reinDetailsList +
				", approveStatus='" + approveStatus + '\'' +
				", vdate='" + vdate + '\'' +
				", invType='" + invType + '\'' +
				", taskId='" + taskId + '\'' +
				'}';
	}
}