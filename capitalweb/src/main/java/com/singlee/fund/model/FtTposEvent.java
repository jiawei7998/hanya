package com.singlee.fund.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="FT_TPOS_EVENT")
public class FtTposEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String tradeNo;
	@Id
	private String eventType;
	
	private String postdate;
	
	private String opTime;

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getOpTime() {
		return opTime;
	}

	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}

	@Override
	public String toString() {
		return "FtTposEvent [tradeNo=" + tradeNo + ", eventType=" + eventType + ", postdate=" + postdate + ", opTime="
				+ opTime + "]";
	}
	
}
