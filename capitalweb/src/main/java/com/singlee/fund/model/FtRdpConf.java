package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_RDP_CONF")
public class FtRdpConf implements Serializable {
    private String tradeNo;

    private String dealNo;

    private String prdNo;

    private String dealType;

    private String adate;

    private String sponsor;

    private String sponinst;

    private String cno;

    private String cname;

    private String contact;

    private String contactPhone;

    private String fundCode;
    @Transient
    private String fundName;

    private String ccy;

    private String tdate;

    private String vdate;

    private BigDecimal shareAmt;

    private BigDecimal amt;

    private BigDecimal intamt;

    private BigDecimal handleAmt;

    private BigDecimal handleRate;

    private String approveStatus;
    
    @Transient
    private List<FtRdpDetailsConf> rdpDetailsConfList;
    
    @Transient
    private String taskId;
    
    private String isConfirm;

    private String isNetWorth;
    
    @Transient
    private BigDecimal totalqty;//关联赎回申请交易的总需要确认的份额
    @Transient
    private BigDecimal qtyOld;//界面上记录这笔交易原始的分配确认份额
    @Transient
    private String reShareAmt;
    
    private BigDecimal price;//赎回时刻净值
    
    public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	private String invType;//会计类型
    
    
    public String getIsConfirm() {
		return isConfirm;
	}

	public void setIsConfirm(String isConfirm) {
		this.isConfirm = isConfirm;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}
    
    public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(String reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public BigDecimal getTotalqty() {
		return totalqty;
	}

	public void setTotalqty(BigDecimal totalqty) {
		this.totalqty = totalqty;
	}

	public BigDecimal getQtyOld() {
		return qtyOld;
	}

	public void setQtyOld(BigDecimal qtyOld) {
		this.qtyOld = qtyOld;
	}

	private static final long serialVersionUID = 1L;

    public List<FtRdpDetailsConf> getRdpDetailsConfList() {
		return rdpDetailsConfList;
	}

	public void setRdpDetailsConfList(List<FtRdpDetailsConf> rdpDetailsConfList) {
		this.rdpDetailsConfList = rdpDetailsConfList;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo == null ? null : prdNo.trim();
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType == null ? null : dealType.trim();
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate == null ? null : adate.trim();
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor == null ? null : sponsor.trim();
    }

    public String getSponinst() {
        return sponinst;
    }

    public void setSponinst(String sponinst) {
        this.sponinst = sponinst == null ? null : sponinst.trim();
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone == null ? null : contactPhone.trim();
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode == null ? null : fundCode.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate == null ? null : tdate.trim();
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate == null ? null : vdate.trim();
    }

    public BigDecimal getShareAmt() {
        return shareAmt;
    }

    public void setShareAmt(BigDecimal shareAmt) {
        this.shareAmt = shareAmt;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getIntamt() {
        return intamt;
    }

    public void setIntamt(BigDecimal intamt) {
        this.intamt = intamt;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus == null ? null : approveStatus.trim();
    }

    public String getIsNetWorth() {
        return isNetWorth;
    }

    public void setIsNetWorth(String isNetWorth) {
        this.isNetWorth = isNetWorth;
    }

    public BigDecimal getHandleAmt() {
        return handleAmt;
    }

    public void setHandleAmt(BigDecimal handleAmt) {
        this.handleAmt = handleAmt;
    }

    public BigDecimal getHandleRate() {
        return handleRate;
    }

    public void setHandleRate(BigDecimal handleRate) {
        this.handleRate = handleRate;
    }
}