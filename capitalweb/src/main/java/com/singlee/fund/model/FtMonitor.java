package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用于界面监控和代办数据汇总
 * @author louhuanqing
 *
 */
public class FtMonitor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tableId;//表识别码
	private String dealNo;//交易编号
	private String prdNo;//产品代码
	private String prdNm;//产品名称
	private String dealType;//审批类型
	private String adate;//审批日
	private String sponsor;//发起人
	private String sponinst;//发起机构
	private String cno;//交易对手
	private String cname;//名称
	private String fundCode;//基金代码
	private String fundName;
	private String ccy;//货币代码
	private String invType;//会计类型
	private BigDecimal amt;//金额
	private String markId;//识别要素
	private String instName;
	private String approveStatus;
	private String userName;
	private String trdtype;
	
	public String getTrdtype() {
		return trdtype;
	}
	public void setTrdtype(String trdtype) {
		this.trdtype = trdtype;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdNm() {
		return prdNm;
	}
	public void setPrdNm(String prdNm) {
		this.prdNm = prdNm;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getAdate() {
		return adate;
	}
	public void setAdate(String adate) {
		this.adate = adate;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponinst() {
		return sponinst;
	}
	public void setSponinst(String sponinst) {
		this.sponinst = sponinst;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getMarkId() {
		return markId;
	}
	public void setMarkId(String markId) {
		this.markId = markId;
	}
	@Override
	public String toString() {
		return "FtMonitor [tableId=" + tableId + ", dealNo=" + dealNo + ", prdNo=" + prdNo + ", prdNm=" + prdNm
				+ ", dealType=" + dealType + ", adate=" + adate + ", sponsor=" + sponsor + ", sponinst=" + sponinst
				+ ", cno=" + cno + ", cname=" + cname + ", fundCode=" + fundCode + ", ccy=" + ccy + ", invType="
				+ invType + ", amt=" + amt + ", markId=" + markId + "]";
	}
	
	
}
