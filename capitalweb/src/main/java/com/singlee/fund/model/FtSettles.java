package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="FT_SETTLES")
public class FtSettles implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String sn;
	private String product;// 产品代码
	private String prodtype;//产品类型
	private String dealno;//流水号
	private String seq;//分序号
	private String server;//来源
	private String fedealno;
	private String cno;//交易对手号
	private String cname;//客户名称
	private String vdate;//付款日期
	private String payrecind;//收付方向
	private String ccy;// 币种
	private BigDecimal amount;//金额
	private String cdate;
	private String ctime;
	private String payBankid;//付款行行号
	private String payBankname;//付款行行名
	private String payUserid;//付款账号
	private String payUsername;//付款账号名称
	private String recBankid;//收款行行号
	private String recBankname;// 收款行行名
	private String recUserid;//收款账号
	private String recUsername;//收款账号名称
	private String yingyeBr;
	private String hvpType;//业务种类(10-现金汇款,11-普通汇兑,12-网银支付...)
	private String hvpType1;//汇兑类型(0-正常)
	private String pzType;//凭证种类
	private String hurryLevel;//加急等级
	private String remarkFy;//附言
	private String dealflag;//处理状态(0,1,2,3,4,5,6,8,X)
	
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getPayrecind() {
		return payrecind;
	}
	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getCtime() {
		return ctime;
	}
	public void setCtime(String ctime) {
		this.ctime = ctime;
	}
	public String getPayBankid() {
		return payBankid;
	}
	public void setPayBankid(String payBankid) {
		this.payBankid = payBankid;
	}
	public String getPayBankname() {
		return payBankname;
	}
	public void setPayBankname(String payBankname) {
		this.payBankname = payBankname;
	}
	public String getPayUserid() {
		return payUserid;
	}
	public void setPayUserid(String payUserid) {
		this.payUserid = payUserid;
	}
	public String getPayUsername() {
		return payUsername;
	}
	public void setPayUsername(String payUsername) {
		this.payUsername = payUsername;
	}
	public String getRecBankid() {
		return recBankid;
	}
	public void setRecBankid(String recBankid) {
		this.recBankid = recBankid;
	}
	public String getRecBankname() {
		return recBankname;
	}
	public void setRecBankname(String recBankname) {
		this.recBankname = recBankname;
	}
	public String getRecUserid() {
		return recUserid;
	}
	public void setRecUserid(String recUserid) {
		this.recUserid = recUserid;
	}
	public String getRecUsername() {
		return recUsername;
	}
	public void setRecUsername(String recUsername) {
		this.recUsername = recUsername;
	}
	public String getYingyeBr() {
		return yingyeBr;
	}
	public void setYingyeBr(String yingyeBr) {
		this.yingyeBr = yingyeBr;
	}
	public String getHvpType() {
		return hvpType;
	}
	public void setHvpType(String hvpType) {
		this.hvpType = hvpType;
	}
	public String getHvpType1() {
		return hvpType1;
	}
	public void setHvpType1(String hvpType1) {
		this.hvpType1 = hvpType1;
	}
	public String getPzType() {
		return pzType;
	}
	public void setPzType(String pzType) {
		this.pzType = pzType;
	}
	public String getHurryLevel() {
		return hurryLevel;
	}
	public void setHurryLevel(String hurryLevel) {
		this.hurryLevel = hurryLevel;
	}
	public String getRemarkFy() {
		return remarkFy;
	}
	public void setRemarkFy(String remarkFy) {
		this.remarkFy = remarkFy;
	}
	public String getDealflag() {
		return dealflag;
	}
	public void setDealflag(String dealflag) {
		this.dealflag = dealflag;
	}
	
}
