package com.singlee.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="FT_REDD_DETAILS")
public class FtReinDetails implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	
    private String tradeNo;//当前分红确认的审批单据

    private String dealNo;//分红确认审批对应的申购完成的单据号

    private BigDecimal amt;//分红金额

    private BigDecimal cPrice;//分红后单位净值
    
    private BigDecimal reShareAmt;//当前分红时刻持仓
    @Transient
    private BigDecimal shareAmt;//申购交易对应的原始确认份额
    @Transient
    private BigDecimal ftpprice;//
    
    private BigDecimal reAmt;//分红时刻的成本金额
    
    private BigDecimal qty;
    
	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getReShareAmt() {
		return reShareAmt;
	}

	public void setReShareAmt(BigDecimal reShareAmt) {
		this.reShareAmt = reShareAmt;
	}

	public BigDecimal getShareAmt() {
		return shareAmt;
	}

	public void setShareAmt(BigDecimal shareAmt) {
		this.shareAmt = shareAmt;
	}

	public BigDecimal getFtpprice() {
		return ftpprice;
	}

	public void setFtpprice(BigDecimal ftpprice) {
		this.ftpprice = ftpprice;
	}

	public BigDecimal getReAmt() {
		return reAmt;
	}

	public void setReAmt(BigDecimal reAmt) {
		this.reAmt = reAmt;
	}

	public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo == null ? null : dealNo.trim();
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

	public BigDecimal getcPrice() {
		return cPrice;
	}

	public void setcPrice(BigDecimal cPrice) {
		this.cPrice = cPrice;
	}

	@Override
	public String toString() {
		return "FtReddDetails [tradeNo=" + tradeNo + ", dealNo=" + dealNo + ", amt=" + amt + ", cPrice=" + cPrice + "]";
	}
    
    
}