package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRein;

public interface FtReinMapper {
    int deleteByPrimaryKey(String dealNo);

    int insert(FtRein record);

    int insertSelective(FtRein record);

    FtRein selectByPrimaryKey(String dealNo);

    int updateByPrimaryKeySelective(FtRein record);

    int updateByPrimaryKey(FtRein record);
    
    Page<FtRein> getFtReinMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRein> getFtReinMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRein> getFtReinMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
}