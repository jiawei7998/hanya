package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRedd;
/**
 * 债基分红Mapper
 * @author louhuanqing
 *
 */
public interface FtReddMapper {
	/**
	 * 根据审批单号　dealNo删除
	 * @param dealNo
	 * @return
	 */
    int deleteByPrimaryKey(String dealNo);
    /**
     * FtRedd javabean新增
     * @param record
     * @return
     */
    int insert(FtRedd record);
    /**
     * 条件选择性 非空 则插入
     * @param record
     * @return
     */
    int insertSelective(FtRedd record);
    /**
     * 根据审批单号查询获得唯一数据
     * @param dealNo
     * @return
     */
    FtRedd selectByPrimaryKey(String dealNo);
    /**
     * 根据 条件选择性 非空 则更新
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(FtRedd record);
    /**
     * 根据审批单号  更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(FtRedd record);
    /**
     * 查询我的列表
     * @param params
     * @param rowBounds
     * @return
     */
    Page<FtRedd> getFtReddMineList(Map<String, Object> params,RowBounds rowBounds);
    /**
     * 查询我完成审批的列表
     * @param params
     * @param rowBounds
     * @return
     */
	Page<FtRedd> getFtReddMineListFinished(Map<String, Object> params,RowBounds rowBounds);
	/**
	 * 查询需要我审批的列表
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	Page<FtRedd> getFtReddMineListMine(Map<String, Object> params,RowBounds rowBounds);
}