package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtTpos;

import tk.mybatis.mapper.common.Mapper;
/**
 * 头寸操作类
 * @author louhuanqing
 *
 */
public interface FtTposMapper extends Mapper<FtTpos>{
	/**
	 * 根据条件获得基金的头寸信息
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<FtTpos>  getTposList(Map<String, Object> params, RowBounds rb);
	
	FtTpos geFtTposByBusinessKey(Map<String, Object> params);

	FtTpos geFtTposByCode(@Param("fundCode") String fundCode);
	
	int updateByPrimaryKeySelectiveAll(FtTpos ftTpos);
}
