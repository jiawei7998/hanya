package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRdpConf;

public interface FtRdpConfMapper {
    int deleteByPrimaryKey(String tradeNo);

    int insert(FtRdpConf record);

    int insertSelective(FtRdpConf record);

    FtRdpConf selectByPrimaryKey(String dealNo);

    int updateByPrimaryKeySelective(FtRdpConf record);

    int updateByPrimaryKey(FtRdpConf record);
    
    Page<FtRdpConf> getFtRdpConfMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRdpConf> getFtRdpConfMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRdpConf> getFtRdpConfMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
}