package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtPriceDaily;

import tk.mybatis.mapper.common.Mapper;

public interface FtPriceDailyMapper extends Mapper<FtPriceDaily>{
	/**
	 * 根据条件获得市场数据
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<FtPriceDaily> searchInfoPage(Map<String, Object> map,RowBounds rowBounds);
	
}
