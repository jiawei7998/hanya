package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtMonitor;

public interface FtMonitorMapper {
	/**
	 * 根据条件获得集合
	 * @param map
	 * @return
	 */
	public Page<FtMonitor> getFtMonitorPage(Map<String, Object> map,RowBounds rowBounds);
}
