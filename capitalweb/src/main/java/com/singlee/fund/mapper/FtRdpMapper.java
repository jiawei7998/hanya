package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRdp;

public interface FtRdpMapper {
    int deleteByPrimaryKey(String dealNo);

    int insert(FtRdp record);

    int insertSelective(FtRdp record);

    FtRdp selectByPrimaryKey(String dealNo);

    int updateByPrimaryKeySelective(FtRdp record);

    int updateByPrimaryKey(FtRdp record);
    
    Page<FtRdp> getFtRdpMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRdp> getFtRdpMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtRdp> getFtRdpMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
}