package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtAfp;

public interface FtAfpMapper {
    int deleteByPrimaryKey(String dealNo);

    int insert(FtAfp record);

    int insertSelective(FtAfp record);

    FtAfp selectByPrimaryKey(String dealNo);

    int updateByPrimaryKeySelective(FtAfp record);

    int updateByPrimaryKey(FtAfp record);
    
    Page<FtAfp> getFtAfpMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtAfp> getFtAfpMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtAfp> getFtAfpMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
}