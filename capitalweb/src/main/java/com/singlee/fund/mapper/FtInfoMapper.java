package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtInfo;

public interface FtInfoMapper {
    int deleteByPrimaryKey(String fundCode);

    int insert(FtInfo record);

    int insertSelective(FtInfo record);

    FtInfo selectByPrimaryKey(String fundCode);

    int updateByPrimaryKeySelective(FtInfo record);

    int updateByPrimaryKey(FtInfo record);
    
    Page<FtInfo> searchInfoPage(Map<String, Object> map,RowBounds rowBounds);

    String  selectFundNameByFundCode(String fundCode);
}