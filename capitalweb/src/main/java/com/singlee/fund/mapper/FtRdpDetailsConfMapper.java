package com.singlee.fund.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.fund.model.FtRdpDetailsConf;

public interface FtRdpDetailsConfMapper {
    int deleteByPrimaryKey(String tradeNo);

    int insert(FtRdpDetailsConf record);

    int insertSelective(FtRdpDetailsConf record);

    FtRdpDetailsConf selectByPrimaryKey(Map<String, Object> map);

    int updateByPrimaryKeySelective(FtRdpDetailsConf record);

    int updateByPrimaryKey(FtRdpDetailsConf record);
    
    List<FtRdpDetailsConf> selectDetailsList(String tradeNo);
}