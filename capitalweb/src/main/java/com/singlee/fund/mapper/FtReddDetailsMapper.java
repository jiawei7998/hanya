package com.singlee.fund.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.fund.model.FtReddDetails;

public interface FtReddDetailsMapper {
	
    int deleteByPrimaryKey(String tradeNo);

    int insert(FtReddDetails record);

    int insertSelective(FtReddDetails record);

    FtReddDetails selectByPrimaryKey(Map<String, Object> map);

    int updateByPrimaryKeySelective(FtReddDetails record);

    int updateByPrimaryKey(FtReddDetails record);
    
    List<FtReddDetails> selectDetailsList(String tradeNo);
}