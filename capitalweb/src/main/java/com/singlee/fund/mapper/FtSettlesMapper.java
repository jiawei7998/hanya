package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtSettles;

import tk.mybatis.mapper.common.Mapper;

public interface FtSettlesMapper extends Mapper<FtSettles>{
	/**
	 * 根据业务Key获得唯一的数据
	 * dealno payrecind
	 * @return
	 */
	FtSettles getFtSettlesByBusinessKey(Map<String, Object> map);
	
	/**
	 * 返回分页结果
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<FtSettles> getFtSettelsPage(Map<String, Object> map,RowBounds rowBounds);
}
