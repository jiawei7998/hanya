package com.singlee.fund.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.fund.model.FtReinDetails;

public interface FtReinDetailsMapper {
	
    int deleteByPrimaryKey(String tradeNo);

    int insert(FtReinDetails record);

    int insertSelective(FtReinDetails record);

    FtReinDetails selectByPrimaryKey(Map<String, Object> map);

    int updateByPrimaryKeySelective(FtReinDetails record);

    int updateByPrimaryKey(FtReinDetails record);
    
    List<FtReinDetails> selectDetailsList(String tradeNo);
}