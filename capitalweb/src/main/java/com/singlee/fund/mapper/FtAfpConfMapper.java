package com.singlee.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtAfpConf;

public interface FtAfpConfMapper {
    int deleteByPrimaryKey(String tradeNo);

    int insert(FtAfpConf record);

    int insertSelective(FtAfpConf record);

    FtAfpConf selectByPrimaryKey(String tradeNo);

    int updateByPrimaryKeySelective(FtAfpConf record);

    int updateByPrimaryKey(FtAfpConf record);
    
    Page<FtAfpConf> getFtAfpConfMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtAfpConf> getFtAfpConfMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<FtAfpConf> getFtAfpConfMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
	
	Page<FtAfpConf> getFtAfpConfMineListUnfinished(Map<String, Object> params,
			RowBounds rowBounds);
	/**
	 * 可用用于赎回的申购交易
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	Page<FtAfpConf> getFtAfpConfMineListFinishedForRdp(Map<String, Object> params,
			RowBounds rowBounds);
	/** 
	 * 可用于分红的数据持仓（申购列表）
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	Page<FtAfpConf> getFtAfpConfMineListFinishedForRedd(Map<String, Object> params,
			RowBounds rowBounds);
	/**
	 * 完成审批后 需要将可赎回的份额和标志为进行更新操作
	 * 
	 */
	void updateFtAfpConf(Map<String, Object> params);
}