package com.singlee.fund.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.fund.model.FtTpos;
import com.singlee.fund.service.FtTposService;

/**
 * 基金持仓操作类
 * @author louhuanqing
 *
 */
@Controller
@RequestMapping(value="/FundTposController")
public class FundTposController extends CommonController{
	@Autowired
	FtTposService ftTposService;
	@ResponseBody
	@RequestMapping(value="/getFtTposList")
	public RetMsg<PageInfo<FtTpos>> getFtTposList(@RequestBody Map<String, Object> params){
		Page<FtTpos> ftTpos=ftTposService.getTposList(params);
		return RetMsgHelper.ok(ftTpos);
	}
	
	@ResponseBody
	@RequestMapping(value="/getFtTposForPair")
	public RetMsg<Pair<List<String>, List<Double>>> getFtTposForPair(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(ftTposService.getTposListForPair(params));
	}
}
