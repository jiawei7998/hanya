package com.singlee.fund.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.model.FtMonitor;
import com.singlee.fund.model.FtPriceDaily;
import com.singlee.fund.model.FtRdpDetailsConf;
import com.singlee.fund.model.FtReddDetails;
import com.singlee.fund.model.FtReinDetails;
import com.singlee.fund.service.FtMonitorService;
import com.singlee.fund.service.FundMktService;
import com.singlee.fund.service.FundRdpConfService;
import com.singlee.fund.service.FundReddService;
import com.singlee.fund.service.FundReinService;

@Controller
@RequestMapping(value="/FundController")
public class FundController {
	@Autowired
	private FundRdpConfService rdpConfService;
	@Autowired
	private FundReddService reddService;
	@Autowired
	private FundReinService reinService;
	@Autowired
	private FundMktService mktService;
	@Autowired
	private FtMonitorService monitorService;
	/**
	 *查询得到 FtRdpDetailsConf
	 */
	@ResponseBody
	@RequestMapping(value="/searchRdpConfDetails")
	public RetMsg<List<FtRdpDetailsConf>> searchRdpConfDetails(@RequestBody Map<String, Object> map){
		List<FtRdpDetailsConf> rdps=rdpConfService.searchRdpConfDetails(map);
		return RetMsgHelper.ok(rdps);
	}
	
	@ResponseBody
	@RequestMapping(value="/searchReddDetails")
	public RetMsg<List<FtReddDetails>> searchReddDetails(@RequestBody Map<String, Object> map){
		List<FtReddDetails> rdps=reddService.searchFtReddDetails(map);
		return RetMsgHelper.ok(rdps);
	}
	
	@ResponseBody
	@RequestMapping(value="/searchReinDetails")
	public RetMsg<List<FtReinDetails>> searchReinDetails(@RequestBody Map<String, Object> map){
		List<FtReinDetails> rdps=reinService.searchFtReinDetails(map);
		return RetMsgHelper.ok(rdps);
	}
	
	@ResponseBody
	@RequestMapping(value="/searchFundMktList")
	public RetMsg<PageInfo<FtPriceDaily>> searchFundMktList(@RequestBody Map<String, Object> map){
		String instId = SlSessionHelper.getInstitutionId();
		map.put("sponInst", instId);
		Page<FtPriceDaily> priceDailies=mktService.searchInfoList(map);
		return RetMsgHelper.ok(priceDailies);
	}
	/**
	 * 市场价格数据添加
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveFtPriceDaily")
	public RetMsg<Serializable> saveFtPriceDaily(@RequestBody Map<String,Object> map){
		FtPriceDaily priceDaily = new FtPriceDaily();
		BeanUtil.populate(priceDaily, map);
		mktService.saveFtPriceDaily(priceDaily);
		return RetMsgHelper.ok();
	}
	/**
	 * 根据主键获得市场数据
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getFtPriceDailyByKey")
	public RetMsg<FtPriceDaily> getFtPriceDailyByKey(@RequestBody Map<String,Object> map){
		FtPriceDaily priceDaily = new FtPriceDaily();
		BeanUtil.populate(priceDaily, map);
		return RetMsgHelper.ok(mktService.getFtPriceDailyByKey(priceDaily));
	}
	/**
	 * 获得当前用户下需要审批的基金列表
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getFtMonitorPage")
	public RetMsg<PageInfo<FtMonitor>> getFtMonitorPage(@RequestBody Map<String, Object> map){
		String instId = SlSessionHelper.getInstitutionId();
		map.put("sponInst", instId);
		map.put("userId", SlSessionHelper.getUserId());
		Page<FtMonitor> monitors=monitorService.getFtMonitorPage(map);
		return RetMsgHelper.ok(monitors);
	}
	
}
