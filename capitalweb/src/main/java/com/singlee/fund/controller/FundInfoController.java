package com.singlee.fund.controller;

import java.io.Serializable;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.fund.model.FtInfo;
import com.singlee.fund.service.FundInfoService;
/**
 * 基金信息controller控制类
 * @author louhuanqing
 *
 */
@Controller
@RequestMapping(value="/FundInfoController")
public class FundInfoController {
	@Autowired
	private FundInfoService fundInfoService;
	
	/**
	 * 查询基金基础信息(List)列表
	 * 需要传入的是基金类型（1-货币基金 2-债券基金）
	 */
	@ResponseBody
	@RequestMapping(value="/seachInfoPage")
	public RetMsg<PageInfo<FtInfo>> seachInfoPage(@RequestBody Map<String, Object> map){
		Page<FtInfo> infos=fundInfoService.searchInfoList(map);
		return RetMsgHelper.ok(infos);
	}
	/**
	 * 根据fundCode获取基金信息
	 * @param map 
	 * @return FtInfo
	 */
	@ResponseBody
	@RequestMapping(value="/seachInfoByCode")
	public RetMsg<Object> seachInfoByCode(@RequestBody Map<String, Object> map){
		
		return RetMsgHelper.ok(fundInfoService.selectApproveById(map));
	}
	@ResponseBody
	@RequestMapping(value="/saveInfo")
	public RetMsg<Serializable> saveInfo(@RequestBody Map<String,Object> map){
		
		Object obj=fundInfoService.selectApproveById(map);
		if(obj==null){
			fundInfoService.saveApprove(map);
		}else{
			fundInfoService.updateApprove(map);
		}
		return RetMsgHelper.ok();
	}
}
