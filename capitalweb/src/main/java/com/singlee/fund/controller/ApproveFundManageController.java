package com.singlee.fund.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.service.ApproveFundManageService;
@Controller
@RequestMapping(value = "/ApproveFundManageController")
public class ApproveFundManageController extends CommonController {
	/** 审批单管理 **/
	@Autowired
	private ApproveFundManageService approveFundManageService;
	/**
	 * 审批单保存
	 * @param request
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/saveApprove")
	public Object saveApprove(@RequestBody HashMap<String, Object> map) throws IOException{
		map.put("opUserId", SlSessionHelper.getUserId());
		return approveFundManageService.saveApproveInfo(map);
	}
	/**
	 * 删除成交单
	 */
	@ResponseBody
	@RequestMapping(value="/deleteApprove")
	public RetMsg<Serializable> deleteApprove(@RequestBody Map<String, Object> map){
		approveFundManageService.delApproveInfo(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 查询方法统一入口
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/searchFundDealsPage")
	public RetMsg<?> searchFundDealsPage(@RequestBody Map<String, Object> map){
		Page<?> resultList=approveFundManageService.searchApproveList(map);
		return RetMsgHelper.ok(resultList);
	}
	/**
	 * 获得唯一审批单据
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getApproveById")
	public RetMsg<Object> getApproveById(@RequestBody Map<String, Object> map){
		Object result = approveFundManageService.getApproveById(map);
		return RetMsgHelper.ok(result);
	}
}
