package com.singlee.fund.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.fund.model.FtSettles;
import com.singlee.fund.service.FtSettlesService;

@Controller
@RequestMapping(value="/FundSettelsController")
public class FundSettelsController {
	
	@Autowired
	private FtSettlesService settlesService;
	
	@ResponseBody
	@RequestMapping(value="/getFtSettlesList")
	public RetMsg<PageInfo<FtSettles>> getFtSettlesList(@RequestBody Map<String, Object> params){
		Page<FtSettles> settles=settlesService.getFtSettelsPage(params);
		return RetMsgHelper.ok(settles);
	}

}
