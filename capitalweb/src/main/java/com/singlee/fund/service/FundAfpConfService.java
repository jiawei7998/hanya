package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtAfpConf;

/**
 * 基金申购确认接口
 * @author 
 *
 */
public interface FundAfpConfService extends FundCustomerService {
	/**
	 * 分页查询申购确认信息
	 */
	Page<FtAfpConf> searchAfpConfPage(Map<String, Object> map,int isFinished);
	/**
	 * 查询我发起的未审批通过的确认单
	 */
	Page<FtAfpConf> searchAfpConfPageMine(Map<String, Object> map);
	/**
	 * 更新申购确认的栏位
	 * @param map
	 */
	void updateTfAfpConf(Map<String, Object> map);
}
