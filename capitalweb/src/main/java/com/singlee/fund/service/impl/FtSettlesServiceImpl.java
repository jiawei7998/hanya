package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.fund.mapper.FtSettlesMapper;
import com.singlee.fund.model.FtSettles;
import com.singlee.fund.service.FtSettlesService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FtSettlesServiceImpl implements FtSettlesService {
	@Autowired
	private FtSettlesMapper ftSettlesMapper;
	@Override
	public void saveFtSettles(FtSettles ftSettles) {
		// TODO Auto-generated method stub
		FtSettles temp = ftSettlesMapper.getFtSettlesByBusinessKey(BeanUtil.beanToMap(ftSettles));
		if(null != temp) {
            ftSettlesMapper.deleteByPrimaryKey(temp);
        }
		ftSettlesMapper.insert(ftSettles);
	}
	@Override
	public Page<FtSettles> getFtSettelsPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ftSettlesMapper.getFtSettelsPage(map, ParameterUtil.getRowBounds(map));
	}

}
