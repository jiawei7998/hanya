package com.singlee.fund.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.fund.mapper.FtTposEventMapper;
import com.singlee.fund.model.FtAfp;
import com.singlee.fund.model.FtAfpConf;
import com.singlee.fund.model.FtRdp;
import com.singlee.fund.model.FtRdpConf;
import com.singlee.fund.model.FtRdpDetailsConf;
import com.singlee.fund.model.FtRedd;
import com.singlee.fund.model.FtReddDetails;
import com.singlee.fund.model.FtRein;
import com.singlee.fund.model.FtSettles;
import com.singlee.fund.model.FtTpos;
import com.singlee.fund.model.FtTposEvent;
import com.singlee.fund.service.ApproveFundServiceListener;
import com.singlee.fund.service.FtSettlesService;
import com.singlee.fund.service.FtTposService;
import com.singlee.fund.service.FundAfpConfService;
import com.singlee.fund.service.FundAfpService;
import com.singlee.fund.service.FundRdpConfService;
import com.singlee.fund.service.FundRdpService;
import com.singlee.fund.service.FundReddService;
import com.singlee.fund.service.FundReinService;
import com.singlee.fund.service.FundTposService;

/**
 * （货币、债券）基金处理总类
 * 任何交易需要维持一个处理总类
 * 由交易类别去区分该类业务是否需要单独建立存放内容的service
 * 不得改变处理总类的原则：
 * 事前审批需要从tt_trd_order入口  作为总的记录头信息；
 * 原因：再操作交易的时候需要锁定业务信息，不能去锁定业务所属交易表，必须统一锁定头信息
 * @author louhuanqing
 *
 */
@Service("approveFundCustomServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveFundCustomServiceImpl implements ApproveFundServiceListener{
	@Autowired
	private FundAfpService fundAfpService;//申购操作
	@Autowired
	private FundAfpConfService fundAfpConfService;
	@Autowired
	private FundRdpService fundRdpService;
	@Autowired
	private FundRdpConfService fundRdpConfService;
	@Autowired
	private FtTposService ftTposService;
	@Autowired
	DayendDateService dayendDateService;
	@Autowired
	private FundReddService fundReddService;
	@Autowired
	FundTposService fundTposService;
	@Autowired
	FundReinService fundReinService;
	@Autowired
	FtTposEventMapper tposEventMapper;
	@Autowired
	FtSettlesService settlesService;
	@Override
	public void beforeSave(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		//转换order_id
		if(DictConstants.DealType.Approve.equals(ParameterUtil.getString(map, "dealType", DictConstants.DealType.Approve))) {
			String orderId = ParameterUtil.getString(map, "dealNo", "");
			map.put("order_id", orderId);
		}else//转换 trade_id;
		{
			String tradeId = ParameterUtil.getString(map, "tradeNo", "");
			map.put("trade_id", tradeId);
		}
	}

	@Override
	public void cancelApprove(TtTrdOrder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createTrade(HashMap<String, Object> map, TtTrdTrade ttTrdTrade) {
		// TODO Auto-generated method stub
		String trdType = ParameterUtil.getString(map, "trdtype", "");
		if(DictConstants.FundTrdType.Afp.equals(trdType))//申购--申购确认
		{
			map.put("dealNo", ParameterUtil.getString(map, "order_id", ""));
			FtAfp ftAfp = (FtAfp) fundAfpService.selectApproveById(map);
			map = BeanUtil.beanToHashMap(ftAfp);
			map.put("dealNo",ftAfp.getDealNo());//挂钩事前审批单
			map.put("tradeNo", ttTrdTrade.getTrade_id());
			map.put("dealType", DictConstants.DealType.Verify);//确认单
			ttTrdTrade.setTrdtype(DictConstants.FundTrdType.Afp);
			map.put("shareAmt", PlaningTools.div(ftAfp.getAmt().doubleValue(), ftAfp.getPrice().compareTo(new BigDecimal(0))>0?ftAfp.getPrice().doubleValue():1));//默认是全额确认
			map.put("vdate", DateUtil.dateAdjust(ParameterUtil.getString(map, "adate", ""), 1));//默认T+1
			map.put("reAmt", 0.00);//默认全部确认
			fundAfpConfService.saveApprove(map);
		}
	}

	//判断是否需要生成确认表（默认申购、赎回、红利转投、分红）会自动生成确认交易信息
	@Override
	public boolean need2Trade(TtTrdOrder ttTrdOrder) {
		// TODO Auto-generated method stub
		//申购 、赎回
		if(DictConstants.FundTrdType.Afp.equals(ttTrdOrder.getTrdtype()) ){//|| DictConstants.FundTrdType.Rdp.equals(ttTrdOrder.getTrdtype())
			return true;
		}
		return false;
	}

	@Override
	public void noPassApprove(TtTrdOrder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveApprove(HashMap<String, Object> map, Object ttTrdOrder, boolean isAdd) {
		// TODO Auto-generated method stub
		//将业务单号放进map
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		if(dealType.equals(DictConstants.DealType.Approve)) {
            map.put("dealNo", ((TtTrdOrder)ttTrdOrder).getOrder_id());
        } else {
            map.put("tradeNo",((TtTrdTrade)ttTrdOrder).getTrade_id());
        }
		map.put("dealType", dealType);
		String trdType=dealType.equals(DictConstants.DealType.Approve)?((TtTrdOrder)ttTrdOrder).getTrdtype():((TtTrdTrade)ttTrdOrder).getTrdtype();
		String postdate = dayendDateService.getDayendDate();
		if(isAdd){
			if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {//申购
				fundAfpService.saveApprove(map);
			}else
			if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify)) {//申购确认
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持确认日期大于账务日期的操作！");
				fundAfpConfService.saveApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {//赎回
				fundRdpService.saveApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify)) {//赎回确认
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持赎回确认日期大于账务日期的操作！");
				fundRdpConfService.saveApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdd.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {//债基分红
				fundReddService.saveApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdi.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {//货币基金再投
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持红利再投日期大于账务日期的操作！");
				fundReinService.saveApprove(map);
			}
		}else {
			if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
				fundAfpService.updateApprove(map);
			}else
			if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify)) {
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持确认日期大于账务日期的操作！");
				fundAfpConfService.updateApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
				fundRdpService.updateApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify)) {
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持赎回确认日期大于账务日期的操作！");
				fundRdpConfService.updateApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdd.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
				fundReddService.updateApprove(map);
			}else
			if (DictConstants.FundTrdType.Rdi.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
				JY.require(DateUtil.daysBetween(ParameterUtil.getString(map, "vdate", ""), postdate)>=0, "不支持红利再投日期大于账务日期的操作！");
				fundReinService.updateApprove(map);
			}
			
		}
	}

	@Override
	public Page<?> searchApproveList(Map<String, Object> map,int isFinished){
		// TODO Auto-generated method stub
		map.put("sponInst", SlSessionHelper.getInstitutionId());
		map.put("userId", SlSessionHelper.getUserId());
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		if (DictConstants.FundTrdType.Afp.equals(ParameterUtil.getString(map, "trdtype", ""))&& dealType.equals(DictConstants.DealType.Approve)) {
			return fundAfpService.searchAfpList(map, isFinished);
		}else
		if (DictConstants.FundTrdType.Afp.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Verify)) {
			return fundAfpConfService.searchAfpConfPage(map, isFinished);
		}
		else if (DictConstants.FundTrdType.Rdp.equals(ParameterUtil.getString(map, "trdtype", ""))&& dealType.equals(DictConstants.DealType.Approve)) {
			return fundRdpService.searchRdpList(map, isFinished);
		}else
		if (DictConstants.FundTrdType.Rdp.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Verify)) {
			return fundRdpConfService.searchRdpConfPage(map, isFinished);
		}else
		if (DictConstants.FundTrdType.Rdd.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Approve)) {
			return fundReddService.searchReddList(map, isFinished);
		}else
		if (DictConstants.FundTrdType.Rdi.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Approve)) {
			return fundReinService.searchReinList(map, isFinished);
		}
			
		return null;
	}

	@Override
	public Object selectApproveInfo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("sponInst", SlSessionHelper.getInstitutionId());
		map.put("userId", SlSessionHelper.getUserId());
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		if (DictConstants.FundTrdType.Afp.equals(ParameterUtil.getString(map, "trdtype", ""))&& dealType.equals(DictConstants.DealType.Approve)) {
			Page<FtAfp> result = fundAfpService.searchAfpList(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}else
		if (DictConstants.FundTrdType.Afp.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Verify)) {
			Page<FtAfpConf> result = fundAfpConfService.searchAfpConfPage(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}
		else if (DictConstants.FundTrdType.Rdp.equals(ParameterUtil.getString(map, "trdtype", ""))&& dealType.equals(DictConstants.DealType.Approve)) {
			Page<FtRdp> result =  fundRdpService.searchRdpList(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}else
		if (DictConstants.FundTrdType.Rdp.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Verify)) {
			Page<FtRdpConf> result = fundRdpConfService.searchRdpConfPage(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}else
		if (DictConstants.FundTrdType.Rdd.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Approve)) {
			Page<FtRedd> result = fundReddService.searchReddList(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}else
		if (DictConstants.FundTrdType.Rdi.equals(ParameterUtil.getString(map, "trdtype", "")) && dealType.equals(DictConstants.DealType.Approve)) {
			Page<FtRein> result = fundReinService.searchReinList(map, 2);
			return (null!=result && null != result.getResult() && result.getResult().size()>0)?result.getResult().get(0):null;
		}
		return null;
	}

	@Override
	public void delApprove(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		String trdType = ParameterUtil.getString(map, "trdtype", "");
		if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
			fundAfpService.deleteAppove(map);
		}else
		if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify))  {
			fundAfpConfService.deleteAppove(map);
		}else
		if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
			fundRdpService.deleteAppove(map);
		}else
		if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify))  {
			fundRdpConfService.deleteAppove(map);
		}else
		if (DictConstants.FundTrdType.Rdd.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
			fundReddService.deleteAppove(map);
		}else
		if (DictConstants.FundTrdType.Rdi.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
			fundReinService.deleteAppove(map);
		}
	}

	@Override
	public boolean need2DoVerifyOhters(TtTrdTrade ttTrdTrade) {
		// TODO Auto-generated method stub
		String postdate = dayendDateService.getDayendDate();
		if (DictConstants.FundTrdType.Afp.equals(ttTrdTrade.getTrdtype())) {//申购确认完成(需要更新可赎回的份额+状态标志）
			Map<String,Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("tradeNo", ttTrdTrade.getTrade_id());
			paramsMap.put("reEndFlag", "0");
			FtAfpConf ftAfpConf = (FtAfpConf) fundAfpConfService.selectApproveById(paramsMap);
			paramsMap.put("reShareAmt", ftAfpConf.getShareAmt());
			fundAfpConfService.updateTfAfpConf(paramsMap);
			//首先得判定是否可以更新当前持仓头寸？ 必须是确认日期<=当前账务日期
			if(DateUtil.daysBetween(ftAfpConf.getVdate(), postdate)<0) {
                return true;
            }
			//将确认的数据进行FT_TPOS的处理
			/**
			 * * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
			 */
			FtTposEvent ftTposEvent = new FtTposEvent();
			ftTposEvent.setTradeNo(ttTrdTrade.getTrade_id());
			ftTposEvent.setEventType("TPOS");
			ftTposEvent.setPostdate(postdate);
			ftTposEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
			
			tposEventMapper.insert(ftTposEvent);
			paramsMap.put("prdNo", ftAfpConf.getPrdNo());
			paramsMap.put("sponInst", ftAfpConf.getSponinst());
			paramsMap.put("fundCode", ftAfpConf.getFundCode());
			paramsMap.put("invtype", ftAfpConf.getInvType());
			FtTpos tpos  = ftTposService.geFtTposByBusinessKey(paramsMap);
			if(null == tpos)
			{
				tpos = new FtTpos();
				tpos.setPrdNo(ftAfpConf.getPrdNo());
				tpos.setInvtype(ftAfpConf.getInvType());
				tpos.setFundCode(ftAfpConf.getFundCode());
				tpos.setUtQty(ftAfpConf.getShareAmt());
				//tpos.setUtAmt(ftAfpConf.getAmt());
				tpos.setSponInst(ftAfpConf.getSponinst());
				tpos.setcNo(ftAfpConf.getCno());
				tpos.setCcy(ftAfpConf.getCcy());
				tpos.setPostdate(postdate);
				tpos.setNcvAmt(BigDecimal.ZERO);
				tpos.setYield_8(BigDecimal.ZERO);
				tpos.setShAmt(BigDecimal.ZERO);
				tpos.setRevalAmt("0");
				tpos.setyUnplAmt(BigDecimal.ZERO);
				tpos.settUnplAmt(BigDecimal.ZERO);
				tpos.setNewQty(BigDecimal.ZERO);
				tpos.setAppQty(BigDecimal.ZERO);
				ftTposService.saveFtTpos(tpos);
			}else {
				tpos.setUtQty(tpos.getUtQty().add(ftAfpConf.getShareAmt()));
				//tpos.setUtAmt(tpos.getUtAmt().add(ftAfpConf.getAmt()));
				tpos.setPostdate(dayendDateService.getDayendDate());
				ftTposService.updateFtTpos(tpos);
			}
		}else if (DictConstants.FundTrdType.Rdp.equals(ttTrdTrade.getTrdtype())){//由于赎回确认通过的时候，赎回确认单与申请赎回单的金额并非一致；确认少的那部分需要释放回持仓 FT_TPOS
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("tradeNo", ttTrdTrade.getTrade_id());
			FtRdpConf ftRdpConf = (FtRdpConf) fundRdpConfService.selectApproveById(paramsMap);
			JY.require(null != ftRdpConf, "找不到赎回确认交易："+ttTrdTrade.getTrade_id());
			paramsMap.put("dealNo", ftRdpConf.getDealNo());
			FtRdp ftRdp = (FtRdp) fundRdpService.selectApproveById(paramsMap);
			JY.require(null != ftRdp, "找不到赎回确认交易对应的赎回申请交易："+ftRdpConf.getDealNo());
			/**
			 * * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
			 */
			paramsMap.put("prdNo", ftRdpConf.getPrdNo());
			paramsMap.put("sponInst", ftRdpConf.getSponinst());
			paramsMap.put("fundCode", ftRdpConf.getFundCode());
			paramsMap.put("invtype", ftRdpConf.getInvType());
			FtTpos tpos  = ftTposService.geFtTposByBusinessKey(paramsMap);
			JY.require(null != tpos, ftRdpConf.getFundCode()+"基金的持仓头寸异常，找不到！");
			if(DictConstants.YesNo.YES.equals(ftRdpConf.getIsConfirm()))
			{
				BigDecimal rdpAmt = ftRdp.getReShareAmt();//赎回申请剩余份额
				if(rdpAmt.compareTo(new BigDecimal(0))>0) {
					tpos.setNewQty(tpos.getNewQty().subtract(ftRdp.getShareAmt()).add(rdpAmt));//原TPOS里面申请赎回份额-实际发生赎回份额（审批赎回份额-剩余的份额）
				}
			}
			tpos.setUtQty(tpos.getUtQty().subtract(ftRdpConf.getShareAmt()));
			//JY.require(tpos.getUtAmt().subtract(ftRdpConf.getAmt()).compareTo(new BigDecimal(0))>=0, "赎回的本金超过持仓基金份额对应的本金！");
			//tpos.setUtAmt(tpos.getUtAmt().subtract(ftRdpConf.getAmt()));
			fundTposService.updateByPrimaryKeySelectiveAll(tpos);
			
		}else if (DictConstants.FundTrdType.Rdd.equals(ttTrdTrade.getTrdtype())){//分红完成 需要
			
		}
		return true;
	}

	@Override
	public boolean need2DoApproveOhters(TtTrdOrder ttTrdOrder) {
		// TODO Auto-generated method stub
		if (DictConstants.FundTrdType.Rdp.equals(ttTrdOrder.getTrdtype())){//赎回申请审批结束，需要更新赎回交易的可用赎回份额
			
		}
		return false;
	}

	@Override
	public boolean createPayRevDeals(String dealNoOrTradeNo,String dealType,String trdType) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		if (DictConstants.FundTrdType.Afp.equals(trdType) && dealType.equals(DictConstants.DealType.Approve))  {
			map.put("dealNo", dealNoOrTradeNo);
			FtAfp afpConf  = (FtAfp) fundAfpService.selectApproveById(map);
			FtSettles ftSettles = new FtSettles();
			ftSettles.setAmount(afpConf.getAmt());
			ftSettles.setCcy(afpConf.getCcy());
			ftSettles.setCdate(DateUtil.getCurrentDateAsString());
			ftSettles.setCtime(DateUtil.getCurrentDateTimeAsString());
			ftSettles.setDealflag(DictConstants.YesNo.NO);
			ftSettles.setDealno(afpConf.getDealNo());
			ftSettles.setFedealno(afpConf.getFundCode());
			ftSettles.setHurryLevel(DictConstants.YesNo.NO);
			ftSettles.setHvpType("0");
			ftSettles.setHvpType1("0");
			ftSettles.setPayBankid(afpConf.getSelfBankcode());
			ftSettles.setPayBankname(afpConf.getSelfBankname());
			ftSettles.setPayUserid(afpConf.getSelfAcccode());
			ftSettles.setPayUsername(afpConf.getSelfAccname());
			ftSettles.setPayrecind(DictConstants.payRecive.P);
			ftSettles.setProdtype("FT_AFP_CONF");
			ftSettles.setProduct(afpConf.getPrdNo());
			ftSettles.setPzType("0");
			ftSettles.setRecBankid(afpConf.getPartyBankcode());
			ftSettles.setRecBankname(afpConf.getPartyBankname());
			ftSettles.setRecUserid(afpConf.getPartyAcccode());
			ftSettles.setRecUsername(afpConf.getPartyAccname());
			ftSettles.setSeq("0");
			ftSettles.setServer("FT");
			ftSettles.setVdate(afpConf.getTdate());
			ftSettles.setYingyeBr(afpConf.getSponinst());
			ftSettles.setSn(afpConf.getDealNo());
			ftSettles.setCno(afpConf.getCno());
			ftSettles.setCname(afpConf.getCname());
			settlesService.saveFtSettles(ftSettles);
		}else
		if (DictConstants.FundTrdType.Rdp.equals(trdType) && dealType.equals(DictConstants.DealType.Verify))  {
			
			map.put("tradeNo", dealNoOrTradeNo);
			FtRdpConf rdpConf = (FtRdpConf) fundRdpConfService.selectApproveById(map);
			List<FtRdpDetailsConf>  ftRdpDetailsConfs = fundRdpConfService.searchRdpConfDetails(map);
			int count =0;
			for(FtRdpDetailsConf ftRdpDetailsConf : ftRdpDetailsConfs) {
			map.put("dealNo", ftRdpDetailsConf.getDealNo());
			FtAfp afpConf  = (FtAfp) fundAfpService.selectApproveById(map);
			FtSettles ftSettles = new FtSettles();
			ftSettles.setAmount(ftRdpDetailsConf.getAmt().add(ftRdpDetailsConf.getIntamt()));//赎回本金+红利
			ftSettles.setCcy(rdpConf.getCcy());
			ftSettles.setCdate(DateUtil.getCurrentDateAsString());
			ftSettles.setCtime(DateUtil.getCurrentDateTimeAsString());
			ftSettles.setDealflag(DictConstants.YesNo.NO);
			ftSettles.setDealno(ftRdpDetailsConf.getTradeNo());
			ftSettles.setFedealno(rdpConf.getFundCode());
			ftSettles.setHurryLevel(DictConstants.YesNo.NO);
			ftSettles.setHvpType("0");
			ftSettles.setHvpType1("0");
			ftSettles.setPayrecind(DictConstants.payRecive.R);
			ftSettles.setProdtype("FT_RDP_CONF");
			ftSettles.setProduct(rdpConf.getPrdNo());
			ftSettles.setPzType("0");
			ftSettles.setSeq(String.valueOf(count));
			ftSettles.setServer("FT");
			ftSettles.setVdate(rdpConf.getVdate());
			ftSettles.setSn(rdpConf.getTradeNo());
			ftSettles.setCno(rdpConf.getCno());
			ftSettles.setCname(rdpConf.getCname());
			ftSettles.setYingyeBr(rdpConf.getSponinst());
			
			ftSettles.setPayBankid(afpConf.getSelfBankcode());
			ftSettles.setPayBankname(afpConf.getSelfBankname());
			ftSettles.setPayUserid(afpConf.getSelfAcccode());
			ftSettles.setPayUsername(afpConf.getSelfAccname());
			ftSettles.setRecBankid(afpConf.getPartyBankcode());
			ftSettles.setRecBankname(afpConf.getPartyBankname());
			ftSettles.setRecUserid(afpConf.getPartyAcccode());
			ftSettles.setRecUsername(afpConf.getPartyAccname());
			
			settlesService.saveFtSettles(ftSettles);
			count++;
			}
		}else
		if (DictConstants.FundTrdType.Rdd.equals(trdType) && dealType.equals(DictConstants.DealType.Approve)) {
			map.put("dealNo", dealNoOrTradeNo);
			FtRedd redd = (FtRedd) fundReddService.selectApproveById(map);
			List<FtReddDetails>  ftReddDetails = fundReddService.searchFtReddDetails(map);
			int count =0;
			for(FtReddDetails ftRdpDetailsConf : ftReddDetails) {
			map.put("dealNo", ftRdpDetailsConf.getDealNo());
			FtAfp afpConf  = (FtAfp) fundAfpService.selectApproveById(map);
			FtSettles ftSettles = new FtSettles();
			ftSettles.setAmount(ftRdpDetailsConf.getAmt());//红利
			ftSettles.setCcy(redd.getCcy());
			ftSettles.setCdate(DateUtil.getCurrentDateAsString());
			ftSettles.setCtime(DateUtil.getCurrentDateTimeAsString());
			ftSettles.setDealflag(DictConstants.YesNo.NO);
			ftSettles.setDealno(ftRdpDetailsConf.getTradeNo());
			ftSettles.setFedealno(redd.getFundCode());
			ftSettles.setHurryLevel(DictConstants.YesNo.NO);
			ftSettles.setHvpType("0");
			ftSettles.setHvpType1("0");
			ftSettles.setPayrecind(DictConstants.payRecive.R);
			ftSettles.setProdtype("FT_REDD");
			ftSettles.setProduct(redd.getPrdNo());
			ftSettles.setPzType("0");
			ftSettles.setSeq(String.valueOf(count));
			ftSettles.setServer("FT");
			ftSettles.setVdate(redd.getTdate());
			ftSettles.setSn(redd.getDealNo());
			ftSettles.setCno(redd.getCno());
			ftSettles.setCname(redd.getCname());
			ftSettles.setYingyeBr(redd.getSponinst());
			
			ftSettles.setPayBankid(afpConf.getSelfBankcode());
			ftSettles.setPayBankname(afpConf.getSelfBankname());
			ftSettles.setPayUserid(afpConf.getSelfAcccode());
			ftSettles.setPayUsername(afpConf.getSelfAccname());
			ftSettles.setRecBankid(afpConf.getPartyBankcode());
			ftSettles.setRecBankname(afpConf.getPartyBankname());
			ftSettles.setRecUserid(afpConf.getPartyAcccode());
			ftSettles.setRecUsername(afpConf.getPartyAccname());
			
			settlesService.saveFtSettles(ftSettles);
			count++;
			}
		}
		return true;
	}

}
