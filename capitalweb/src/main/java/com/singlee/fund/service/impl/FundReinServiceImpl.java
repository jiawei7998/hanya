package com.singlee.fund.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtReinDetailsMapper;
import com.singlee.fund.mapper.FtReinMapper;
import com.singlee.fund.model.FtRein;
import com.singlee.fund.model.FtReinDetails;
import com.singlee.fund.service.FundReinService;
@Service(value="fundReinService")
public class FundReinServiceImpl implements FundReinService{
	@Autowired
	private FtReinMapper reinMapper;
	@Autowired
	private FtReinDetailsMapper ftReinDetailsMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRein rein=new FtRein();
		BeanUtil.populate(rein, map);
		reinMapper.insert(rein);
		List<FtReinDetails> confs=JSON.parseArray(rein.getReinDetailsList().toString(), FtReinDetails.class);
		for(FtReinDetails reddDetails:confs){
			reddDetails.setTradeNo(rein.getDealNo());
			ftReinDetailsMapper.insert(reddDetails);
		}
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRein rein=new FtRein();
		BeanUtil.populate(rein, map);
		reinMapper.updateByPrimaryKey(rein);
		List<FtReinDetails> confs=JSON.parseArray(rein.getReinDetailsList().toString(), FtReinDetails.class);
		ftReinDetailsMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
		for(FtReinDetails reddDetails:confs){
			reddDetails.setTradeNo(rein.getDealNo());
			ftReinDetailsMapper.insert(reddDetails);
		}
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		reinMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
		ftReinDetailsMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
	}

	@Override
	public FtRein searchReinByNo(Map<String, Object> map) {
		FtRein rein=reinMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return rein;
	}

	@Override
	public Page<FtRein> searchReinList(Map<String, Object> map, int isFinished) {
		Page<FtRein> reinPage=new Page<FtRein>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){//我发起的
			reinPage=reinMapper.getFtReinMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){//待审批
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
								   DictConstants.ApproveStatus.WaitVerify + "," + 
								   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			reinPage=reinMapper.getFtReinMineList(map, ParameterUtil.getRowBounds(map));
		}else{//已审批的
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
			 					   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			reinPage=reinMapper.getFtReinMineListFinished(map, ParameterUtil.getRowBounds(map));
		}
		return reinPage;
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtRein rein=reinMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return rein;
	}

	@Override
	public List<FtReinDetails> searchFtReinDetails(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String tradeNo = ParameterUtil.getString(map, "tradeNo", "");
		return ftReinDetailsMapper.selectDetailsList(tradeNo);
	}
}
