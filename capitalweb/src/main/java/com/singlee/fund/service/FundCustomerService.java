package com.singlee.fund.service;

import java.util.Map;


/**
 * 基金公共接口  
 * 基金交易必须继承该接口
 */
public interface FundCustomerService {
	/**
	 * 添加审批单
	 */
	 void saveApprove(Map<String, Object> map);
	/**
	 * 修改审批单
	 */
	 void updateApprove(Map<String, Object> map);
	/**
	 * 删除审批单
	 */
	 void deleteAppove(Map<String, Object> map);
	/**
	 * 根据id查询成交单
	 */
	 Object selectApproveById(Map<String, Object> map);
}
