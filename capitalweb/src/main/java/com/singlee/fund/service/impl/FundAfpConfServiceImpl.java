package com.singlee.fund.service.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtAfpConfMapper;
import com.singlee.fund.mapper.FtAfpMapper;
import com.singlee.fund.model.FtAfp;
import com.singlee.fund.model.FtAfpConf;
import com.singlee.fund.service.FundAfpConfService;
@Service(value="fundAfpConfService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundAfpConfServiceImpl implements FundAfpConfService {
	@Autowired
	private FtAfpConfMapper confMapper;
	@Autowired
	private FtAfpMapper afpMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtAfpConf afpConf=new FtAfpConf();
		BeanUtil.populate(afpConf,map);
		confMapper.insert(afpConf);
		//更新申购交易余额
		FtAfp afp=afpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		if(null != afp) {
			afp.setReAmt(new BigDecimal(map.get("reAmt").toString()));
			afp.setIsConfirm(String.valueOf(map.get("isComfirm")));
			afpMapper.updateByPrimaryKey(afp);
		}
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtAfpConf afpConf=new FtAfpConf();
		BeanUtil.populate(afpConf, map);
		confMapper.updateByPrimaryKey(afpConf);
		//更新申购交易余额
		FtAfp afp=afpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		//实时再次校验数据库是否真的可以确认交易单的金额
		BigDecimal nowReAmt = afp.getReAmt();//剩余可用于分配的金额(实时）
		BigDecimal amtOld = afpConf.getAmtOld();//操作交易之前的Old金额
		BigDecimal timeAmt = afpConf.getAmt();//界面上的剩余可分配金额（时刻）
		//nowReAmt-timeAmt+amtOld>=0表示可以分配
		JY.require((nowReAmt.add(amtOld).subtract(timeAmt)).compareTo(new BigDecimal("0.00"))>-1,"无法确认:"+timeAmt.doubleValue()+";最多可以确认："+nowReAmt.add(amtOld).doubleValue());
		afp.setReAmt(afpConf.getReAmt());
		//afp.setReShareAmt(afpConf.getReAmt());
		afp.setIsConfirm(ParameterUtil.getString(map, "isConfirm", "0"));
		afpMapper.updateByPrimaryKey(afp);
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		//更新申购交易余额
		FtAfpConf afpConf=confMapper.selectByPrimaryKey(String.valueOf(map.get("tradeNo")));
		FtAfp afp=afpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		afp.setReAmt(afp.getReAmt().add(afpConf.getAmt()));
		//afp.setReShareAmt(afp.getReShareAmt().add(afpConf.getShareAmt()));
		afp.setIsConfirm(DictConstants.YesNo.NO);
		afpMapper.updateByPrimaryKey(afp);
		confMapper.deleteByPrimaryKey(String.valueOf(map.get("tradeNo")));
		

	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtAfpConf afpConf=confMapper.selectByPrimaryKey(String.valueOf(map.get("tradeNo")));
		return afpConf;
	}
	
	@Override
	public Page<FtAfpConf> searchAfpConfPage(Map<String, Object> map,int isFinished) {
		Page<FtAfpConf> confPage=new Page<FtAfpConf>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){
			confPage=confMapper.getFtAfpConfMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
			   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
			   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
			   DictConstants.ApproveStatus.WaitVerify + "," + 
			   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			confPage=confMapper.getFtAfpConfMineList(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==4){
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
					   DictConstants.ApproveStatus.Verified+",";//审批通过+确认通过的状态下的交易可以用于赎回
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			confPage=confMapper.getFtAfpConfMineListFinishedForRdp(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==5){
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
					   DictConstants.ApproveStatus.Verified+",";//审批通过+确认通过的状态下的交易可以用于赎回
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			confPage=confMapper.getFtAfpConfMineListFinishedForRedd(map, ParameterUtil.getRowBounds(map));
		}
		else {
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			confPage=confMapper.getFtAfpConfMineListFinished(map, ParameterUtil.getRowBounds(map));
		}
		return confPage;
	}

	@Override
	public Page<FtAfpConf> searchAfpConfPageMine(Map<String, Object> map) {
		Page<FtAfpConf> confs=confMapper.getFtAfpConfMineListUnfinished(map, ParameterUtil.getRowBounds(map));
		return confs;
	}

	@Override
	public void updateTfAfpConf(Map<String, Object> map) {
		// TODO Auto-generated method stub
		this.confMapper.updateFtAfpConf(map);
	}

}
