package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtAfpMapper;
import com.singlee.fund.model.FtAfp;
import com.singlee.fund.service.FundAfpService;
@Service(value="fundAfpService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundAfpServiceImpl implements  FundAfpService{
	@Autowired
	private FtAfpMapper afpMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		map.put("isConfirm", "0");
		String dealNo = ParameterUtil.getString(map, "dealNo", "");
		if(StringUtil.checkEmptyNull(dealNo)) {
            map.put("dealNo", DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100000)));
        }
		FtAfp  afp=new FtAfp();
		BeanUtil.populate(afp, map);
		afpMapper.insert(afp);
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		map.put("isConfirm", "0");
		FtAfp  afp=new FtAfp();
		BeanUtil.populate(afp, map);
		afpMapper.updateByPrimaryKey(afp);
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		afpMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
	}

	@Override
	public FtAfp searchAfpByNo(Map<String, Object> map) {
		FtAfp afp=afpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return afp;
	}

	@Override
	public Page<FtAfp> searchAfpList(Map<String, Object> map,int isFinished) {
		Page<FtAfp> afpPage=new Page<FtAfp>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){//我发起的
			afpPage=afpMapper.getFtAfpMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){//待审批
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
								   DictConstants.ApproveStatus.WaitVerify + "," + 
								   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			afpPage=afpMapper.getFtAfpMineList(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==3){//已审批的
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
			 					   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			afpPage=afpMapper.getFtAfpMineListFinished(map, ParameterUtil.getRowBounds(map));
		}else{
			map.put("userId", "");
			afpPage=afpMapper.getFtAfpMineListMine(map, ParameterUtil.getRowBounds(map));
		}
		return afpPage;
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtAfp afp=afpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return  afp;
	}
	

}
