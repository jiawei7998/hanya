package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.fund.mapper.FtMonitorMapper;
import com.singlee.fund.model.FtMonitor;
import com.singlee.fund.service.FtMonitorService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FtMonitorServiceImpl implements FtMonitorService {
	@Autowired
	private FtMonitorMapper monitorService;
	@Override
	public Page<FtMonitor> getFtMonitorPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return monitorService.getFtMonitorPage(map, ParameterUtil.getRowBounds(map));
	}

}
