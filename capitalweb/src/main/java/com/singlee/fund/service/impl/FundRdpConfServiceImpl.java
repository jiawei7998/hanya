package com.singlee.fund.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtAfpConfMapper;
import com.singlee.fund.mapper.FtRdpConfMapper;
import com.singlee.fund.mapper.FtRdpDetailsConfMapper;
import com.singlee.fund.mapper.FtRdpMapper;
import com.singlee.fund.model.FtAfpConf;
import com.singlee.fund.model.FtRdp;
import com.singlee.fund.model.FtRdpConf;
import com.singlee.fund.model.FtRdpDetailsConf;
import com.singlee.fund.model.FtRdpDetailsConfVo;
import com.singlee.fund.service.FundRdpConfService;
@Service(value="fundRdpConfService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundRdpConfServiceImpl implements FundRdpConfService{
	@Autowired
	private FtRdpConfMapper confMapper;
	@Autowired
	private FtRdpDetailsConfMapper detailsConfMapper;
	@Autowired
	private FtRdpMapper rdpMapper;
	
	@Autowired
	private FtAfpConfMapper afpConfMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRdpConf conf=new FtRdpConf();
		BeanUtil.populate(conf, map);
		confMapper.insert(conf);
		List<FtRdpDetailsConfVo> confs=JSON.parseArray(conf.getRdpDetailsConfList().toString(), FtRdpDetailsConfVo.class);
		for(FtRdpDetailsConfVo rdpConf:confs){
			rdpConf.setTradeNo(conf.getTradeNo());
			FtRdpDetailsConf conf2 = new FtRdpDetailsConf();
			BeanUtil.copyNotEmptyProperties(conf2, rdpConf);
			detailsConfMapper.insert(conf2);
			//针对每一笔确认申购完成的影响头寸的交易 更新其余额
			FtAfpConf ftAfpConf = afpConfMapper.selectByPrimaryKey(rdpConf.getDealNo());
			JY.require((rdpConf.getReShareAmt().subtract(rdpConf.getQty())).doubleValue()>=0, ftAfpConf.getTradeNo()+"持仓的可用份额不足："+rdpConf.getShareAmt());
			Map<String,Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("tradeNo", ftAfpConf.getTradeNo());
			paramsMap.put("reEndFlag", "0");
			paramsMap.put("reShareAmt", rdpConf.getReShareAmt().subtract(rdpConf.getQty()));
			afpConfMapper.updateFtAfpConf(paramsMap);
		}
		//更新主表剩余赎回余额
		FtRdp ftRdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		//shareAmt(本次可确认份额）  reShareAmt（本次确认之前的可确认份额）
		JY.require(new BigDecimal(map.get("reShareAmt").toString()).subtract(conf.getShareAmt()).doubleValue()>=0, String.valueOf(map.get("dealNo"))+"赎回申请的可用份额不足："+conf.getShareAmt());
		ftRdp.setReShareAmt(new BigDecimal(map.get("reShareAmt").toString()).subtract(conf.getShareAmt()));
		ftRdp.setIsConfirm(ParameterUtil.getString(map, "isConfirm", "0"));
		rdpMapper.updateByPrimaryKey(ftRdp);
		
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRdpConf conf=new FtRdpConf();
		BeanUtil.populate(conf, map);
		confMapper.updateByPrimaryKey(conf);
		List<FtRdpDetailsConfVo> confs=JSON.parseArray(conf.getRdpDetailsConfList().toString(), FtRdpDetailsConfVo.class);
		detailsConfMapper.deleteByPrimaryKey(String.valueOf(map.get("tradeNo")));
		for(FtRdpDetailsConfVo rdpConf:confs){
			rdpConf.setTradeNo(conf.getTradeNo());
			FtRdpDetailsConf conf2 = new FtRdpDetailsConf();
			BeanUtil.copyNotEmptyProperties(conf2, rdpConf);
			detailsConfMapper.insert(conf2);
			//针对每一笔确认申购完成的影响头寸的交易 更新其余额
			FtAfpConf ftAfpConf = afpConfMapper.selectByPrimaryKey(rdpConf.getDealNo());
			JY.require((rdpConf.getReShareAmt().subtract(rdpConf.getQty())).doubleValue()>=0, ftAfpConf.getTradeNo()+"持仓的可用份额不足："+rdpConf.getShareAmt());
			Map<String,Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("tradeNo", ftAfpConf.getTradeNo());
			paramsMap.put("reEndFlag", "0");
			paramsMap.put("reShareAmt", rdpConf.getReShareAmt().subtract(rdpConf.getQty()));
			afpConfMapper.updateFtAfpConf(paramsMap);
		}
		//更新主表剩余赎回余额
		FtRdp ftRdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		//shareAmt(本次可确认份额）  reShareAmt（本次确认之前的可确认份额）
		JY.require(new BigDecimal(map.get("reShareAmt").toString()).subtract(conf.getShareAmt()).doubleValue()>=0, String.valueOf(map.get("dealNo"))+"赎回申请的可用份额不足："+conf.getShareAmt());
		ftRdp.setReShareAmt(new BigDecimal(map.get("reShareAmt").toString()).subtract(conf.getShareAmt()));
		ftRdp.setIsConfirm(ParameterUtil.getString(map, "isConfirm", "0"));
		rdpMapper.updateByPrimaryKey(ftRdp);
		
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		List<FtRdpDetailsConf> detailsConfs=detailsConfMapper.selectDetailsList(String.valueOf(map.get("tradeNo")));
		BigDecimal sumAmt = new BigDecimal(0);
		for(FtRdpDetailsConf detailsConf:detailsConfs){
			Map<String,Object> paramsMap = new HashMap<String, Object>();
			FtAfpConf ftAfpConf = afpConfMapper.selectByPrimaryKey(detailsConf.getDealNo());
			paramsMap.put("tradeNo", detailsConf.getDealNo());
			paramsMap.put("reEndFlag", "0");
			paramsMap.put("reShareAmt", ftAfpConf.getReShareAmt().add(detailsConf.getQty()));//重新加上去
			afpConfMapper.updateFtAfpConf(paramsMap);
			sumAmt=sumAmt.add(detailsConf.getQty());
		}
		//ft_rdp 需要增加
		//更新主表剩余赎回余额
		FtRdp ftRdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		ftRdp.setReShareAmt(ftRdp.getReShareAmt().add(sumAmt));
		ftRdp.setIsConfirm("0");
		rdpMapper.updateByPrimaryKey(ftRdp);
		confMapper.deleteByPrimaryKey(String.valueOf(map.get("tradeNo")));
		detailsConfMapper.deleteByPrimaryKey(String.valueOf(map.get("tradeNo")));
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return confMapper.selectByPrimaryKey(ParameterUtil.getString(map, "tradeNo", ""));
	}

	@Override
	public Page<FtRdpConf> searchRdpConfPage(Map<String, Object> map,
			int isFinished) {
		Page<FtRdpConf> rdpConfPage=new Page<FtRdpConf>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){
			rdpConfPage=confMapper.getFtRdpConfMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
			   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
			   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
			   DictConstants.ApproveStatus.WaitVerify + "," + 
			   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			rdpConfPage=confMapper.getFtRdpConfMineList(map, ParameterUtil.getRowBounds(map));
		}else {
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			rdpConfPage=confMapper.getFtRdpConfMineListFinished(map, ParameterUtil.getRowBounds(map));
		}
		return rdpConfPage;
	}

	@Override
	public List<FtRdpDetailsConf> searchRdpConfDetails(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String tradeNo = ParameterUtil.getString(map, "tradeNo", "");
		return detailsConfMapper.selectDetailsList(tradeNo);
	}

}
