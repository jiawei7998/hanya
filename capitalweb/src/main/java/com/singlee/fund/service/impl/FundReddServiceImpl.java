package com.singlee.fund.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtReddDetailsMapper;
import com.singlee.fund.mapper.FtReddMapper;
import com.singlee.fund.model.FtRedd;
import com.singlee.fund.model.FtReddDetails;
import com.singlee.fund.service.FundReddService;
@Service(value="fundReddService")
public class FundReddServiceImpl implements FundReddService{
	@Autowired
	private FtReddMapper reddMapper;
	@Autowired
	private FtReddDetailsMapper ftReddDetailsMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRedd redd=new FtRedd();
		BeanUtil.populate(redd, map);
		reddMapper.insert(redd);
		List<FtReddDetails> confs=JSON.parseArray(redd.getReddDetailsList().toString(), FtReddDetails.class);
		for(FtReddDetails reddDetails:confs){
			reddDetails.setTradeNo(redd.getDealNo());
			ftReddDetailsMapper.insert(reddDetails);
		}
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		FtRedd redd=new FtRedd();
		BeanUtil.populate(redd, map);
		reddMapper.updateByPrimaryKey(redd);
		List<FtReddDetails> confs=JSON.parseArray(redd.getReddDetailsList().toString(), FtReddDetails.class);
		ftReddDetailsMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
		for(FtReddDetails ftReddDetails:confs){
			ftReddDetails.setTradeNo(redd.getDealNo());
			ftReddDetailsMapper.insert(ftReddDetails);
		}
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		reddMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
		ftReddDetailsMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
	}

	@Override
	public FtRedd searchReddByNo(Map<String, Object> map) {
		FtRedd Redd=reddMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return Redd;
	}

	@Override
	public Page<FtRedd> searchReddList(Map<String, Object> map, int isFinished) {
		Page<FtRedd> ReddPage=new Page<FtRedd>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){//我发起的
			ReddPage=reddMapper.getFtReddMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){//待审批
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
								   DictConstants.ApproveStatus.WaitVerify + "," + 
								   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			ReddPage=reddMapper.getFtReddMineList(map, ParameterUtil.getRowBounds(map));
		}else{//已审批的
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
			 					   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			ReddPage=reddMapper.getFtReddMineListFinished(map, ParameterUtil.getRowBounds(map));
		}
		return ReddPage;
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtRedd Redd=reddMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return Redd;
	}
	
	@Override
	public List<FtReddDetails> searchFtReddDetails(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String tradeNo = ParameterUtil.getString(map, "tradeNo", "");
		return ftReddDetailsMapper.selectDetailsList(tradeNo);
	}

}
