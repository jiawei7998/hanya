package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtInfo;

/**
 * 货币基金基础信息
 * @author 
 *
 */
public interface FundInfoService extends FundCustomerService {
	/**
	 * 分页查询货币基金基础信息
	 */
	Page<FtInfo> searchInfoList(Map<String, Object> map);
}
