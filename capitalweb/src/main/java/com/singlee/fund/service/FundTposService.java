package com.singlee.fund.service;

import com.singlee.fund.model.FtTpos;
/**
 * FtTpos服务类
 * @author louhuanqing
 *
 */
public interface FundTposService {
	/**
	 * 选择性有值的属性更新
	 * @param ftTpos
	 */
	void updateByPrimaryKeySelectiveAll(FtTpos ftTpos);
}
