package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtAfp;

/**
 * 基金申购接口
 * @author 
 *
 */
public interface FundAfpService extends FundCustomerService {
	/**
	 * 根据编号查询申购交易
	 */
	FtAfp searchAfpByNo(Map<String, Object> map);
	/**
	 * 分页查询申购交易
	 */
	Page<FtAfp> searchAfpList(Map<String, Object> map,int isFinished);
	
}
