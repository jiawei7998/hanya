package com.singlee.fund.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.fund.mapper.FtTposMapper;
import com.singlee.fund.model.FtTpos;
import com.singlee.fund.service.FundTposService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundTposServiceImpl implements FundTposService {
	@Autowired
	private FtTposMapper ftTposMapper;
	@Override
	public void updateByPrimaryKeySelectiveAll(FtTpos ftTpos) {
		// TODO Auto-generated method stub
		ftTposMapper.updateByPrimaryKeySelectiveAll(ftTpos);
	}

}
