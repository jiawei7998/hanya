package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRdp;
/**
 * 基金赎回
 * @author 
 *
 */
public interface FundRdpService extends FundCustomerService {
	/**
	 * 根据编号查询申购交易
	 */
	FtRdp searchRdpByNo(Map<String, Object> map);
	/**
	 * 分页查询申购交易
	 */
	Page<FtRdp> searchRdpList(Map<String, Object> map,int isFinished);
}
