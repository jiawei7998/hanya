package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.fund.mapper.FtRdpMapper;
import com.singlee.fund.mapper.FtTposMapper;
import com.singlee.fund.model.FtRdp;
import com.singlee.fund.model.FtTpos;
import com.singlee.fund.service.FundRdpService;
@Service(value="fundRdpService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundRdpServiceImpl implements FundRdpService{
	@Autowired
	private FtRdpMapper rdpMapper;
	@Autowired
	private FtTposMapper ftTposMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		map.put("isConfirm", "0");
		FtRdp rdp=new FtRdp();
		BeanUtil.populate(rdp, map);
		//保存基金赎回主表信息
		rdp.setReShareAmt(rdp.getShareAmt());
		rdpMapper.insert(rdp);
		/**
		 * 限制FtTpos可赎回的Qty
		 */
		/**
		 * * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
		 */
		map.put("prdNo", rdp.getPrdNo());
		map.put("sponInst", rdp.getSponinst());
		map.put("fundCode", rdp.getFundCode());
		map.put("invtype", rdp.getInvType());
		FtTpos tpos  = ftTposMapper.geFtTposByBusinessKey(map);
		if(null != tpos) {
			tpos.setNewQty(tpos.getNewQty().add(rdp.getShareAmt()));//更新到NEW创建的赎回数量
			ftTposMapper.updateByPrimaryKeySelectiveAll(tpos);
		}
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		map.put("isConfirm", "0");
		FtRdp rdp=new FtRdp();
		BeanUtil.populate(rdp, map);
		rdp.setReShareAmt(rdp.getShareAmt());
		rdpMapper.updateByPrimaryKey(rdp);
		/**
		 * * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
		 */
		map.put("prdNo", rdp.getPrdNo());
		map.put("sponInst", rdp.getSponinst());
		map.put("fundCode", rdp.getFundCode());
		map.put("invtype", rdp.getInvType());
		FtTpos tpos  = ftTposMapper.geFtTposByBusinessKey(map);
		if(null != tpos) {
			/**
			 * 这里需要得到原交易的ShareAmt才能得出调整值
			 */
			tpos.setNewQty(tpos.getNewQty().add(rdp.getShareAmt()).subtract(rdp.getAmtOld()));//更新到NEW创建的赎回数量
			ftTposMapper.updateByPrimaryKeySelectiveAll(tpos);
		}
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		//删除主表
		FtRdp rdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		if(null != rdp) {
			rdpMapper.deleteByPrimaryKey(String.valueOf(map.get("dealNo")));
			/**
			 * * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
			 */
			map.put("prdNo", rdp.getPrdNo());
			map.put("sponInst", rdp.getSponinst());
			map.put("fundCode", rdp.getFundCode());
			map.put("invtype", rdp.getInvType());
			FtTpos tpos  = ftTposMapper.geFtTposByBusinessKey(map);
			if(null != tpos) {
				tpos.setNewQty(tpos.getNewQty().subtract(rdp.getShareAmt()));//更新到NEW创建的赎回数量
				ftTposMapper.updateByPrimaryKeySelectiveAll(tpos);
			}
		}
	}

	@Override
	public FtRdp searchRdpByNo(Map<String, Object> map) {
		FtRdp rdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return rdp;
	}

	@Override
	public Page<FtRdp> searchRdpList(Map<String, Object> map,int isFinished) {
		Page<FtRdp> rdpPage=new Page<FtRdp>();
		map.put("userId", SlSessionHelper.getUserId());
		if(isFinished==1){//我发起的
			rdpPage=rdpMapper.getFtRdpMineListMine(map, ParameterUtil.getRowBounds(map));
		}else if(isFinished==2){//待审批
			String approveStatus = DictConstants.ApproveStatus.New + "," + 
								   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.VerifyRefuse+ "," + 
								   DictConstants.ApproveStatus.WaitVerify + "," + 
								   DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			rdpPage=rdpMapper.getFtRdpMineList(map, ParameterUtil.getRowBounds(map));
		}else{//已审批的
			String approveStatus = DictConstants.ApproveStatus.ApprovedPass+","+
			 					   DictConstants.ApproveStatus.ApprovedNoPass + "," + 
								   DictConstants.ApproveStatus.Cancle+","+
								   DictConstants.ApproveStatus.Executing + "," + 
								   DictConstants.ApproveStatus.Executed+","+
								   DictConstants.ApproveStatus.Verified+","+
								   DictConstants.ApproveStatus.Accounting + "," + 
								   DictConstants.ApproveStatus.Accounted+","+
								   DictConstants.ApproveStatus.Approving;
			String[] orderStatus = approveStatus.split(",");
			map.put("approveStatusNo", orderStatus);
			rdpPage=rdpMapper.getFtRdpMineListFinished(map, ParameterUtil.getRowBounds(map));
		}
		return rdpPage;
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtRdp rdp=rdpMapper.selectByPrimaryKey(String.valueOf(map.get("dealNo")));
		return  rdp;
	}
	
}
