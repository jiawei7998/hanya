package com.singlee.fund.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.base.util.SettleConstants;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SysParamService;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.fund.service.ApproveFundManageService;
import com.singlee.fund.service.ApproveFundServiceListener;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
/**
 * 基金操作管理类（统一入口）
 * @author louhuanqing
 *
 */
@Service("approveFundManageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveFundManageServiceImpl implements ApproveFundManageService, SlbpmCallBackInteface,TaskEventInterface {
	/** 系统参数表 **/
	@Autowired
	private SysParamService sysParamService;//系统类处理参数维护
	@Autowired
	private UserParamService userParamService;//个性化用户使用银行的设置
	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;
	/** 审批单管理dao **/
	@Autowired
	private TrdOrderMapper approveManageDao;
	/** 交易单dao **/
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	@Override
	public Page<?> searchApproveList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String trdType = ParameterUtil.getString(map, "trdtype", "");//交易类别
		String folder = sysParamService.selectSysParam("000002", trdType).getP_prop1();
		//可以根据处理类别  找到真实的处理类
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		return approveServiceInterface.searchApproveList(map, ParameterUtil.getInt(map, "approveType", 1));
	}

	@Override
	public HashMap<String, Object> selectApproveInfo(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object saveApproveInfo(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		String trdType = ParameterUtil.getString(map, "trdtype", "");//交易类别
		//String type = ParameterUtil.getString(map, "type" , "");//审批类别 1-事前审批  2-核实确认
		// 获取具体业务接口  可能是直接再界面指定的。
		String folder = ParameterUtil.getString(map, "folder", "");
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		//没有的话需要根据交易类别查询数据库获得
		if (StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))) {
			folder = sysParamService.selectSysParam("000002", trdType).getP_prop1();
		}
		if (StringUtil.checkEmptyNull(folder)) {//如果没有再界面指定
			TaSysParam sysParam = new TaSysParam();
			sysParam.setP_code("000002");//默认系统类别，以后可以根据该参数自由选择处理类
			sysParam.setP_editabled(DictConstants.YesNo.YES);//必须启用，用来支持多个银行的基金处理类；可以不同
			sysParam.setP_prop1("60");//基金默认管理类标示
			sysParam.setP_value(trdType);//用交易类别指定
			sysParam.setP_type(trdType);//用交易类别指定
			sysParamService.add(sysParam);
			folder = sysParam.getP_prop1();//具体的操作类别
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		//可以根据处理类别  找到真实的处理类
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);//此处为默认审批类
		ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		// 1.在保存之前处理
		approveServiceInterface.beforeSave(map);
		// 2.增加逻辑，当为非测试环境下，当业务日期 不等于 自然日期时不允许保存审批单
		if (userParamService.getBooleanSysParamByName("system.isJudgeSysDateAndBusDate",false)) {
			String settlementDate = dayendDateService.getSettlementDate();
			JY.require(DateUtil.getCurrentDateAsString().equals(settlementDate), "业务日历不等于当前日期,无法执行保存操作.");
		}
		// 3.增加交易日判断  假期不允许保存交易
		
		boolean isAdd = true;
		if(dealType.equals(DictConstants.DealType.Approve)) {
			TtTrdOrder ttTrdOrder = new TtTrdOrder();
			// 4.将hashmap转换为TtTrdOrder对象
			ParentChildUtil.HashMapToClass(map, ttTrdOrder);
			// 5.当order_id存在时，锁定order记录,当order_id不存在时，则预先取得orader_id的值
			String opUserId = SlSessionHelper.getUserId();
			if (!StringUtil.checkEmptyNull(ttTrdOrder.getOrder_id())) {//判断审批投标order_id是否为空
				isAdd = false;
				HashMap<String, Object> mapRow = new HashMap<String, Object>();
				mapRow.put("order_id", map.get("order_id"));
				mapRow.put("is_locked", true);//加锁锁定交易
				TtTrdOrder ttTrdOrderOld = approveManageDao.selectOrderForOrderId(mapRow);
				JY.require(StringUtil.isNullOrEmpty(ttTrdOrderOld.getUser_id()) || opUserId.equals(ttTrdOrderOld.getUser_id()), "仅允许修改本人所属单据.");
	
			} else {
				isAdd = true;//表示新增交易
				String order_id = "";//主审批单据号为空
				HashMap<String, Object> mapSel = new HashMap<String, Object>();
				mapSel.put("trdtype", map.get("trdtype"));
				order_id = approveManageDao.getOrderId(mapSel);//获得审批单据
				ttTrdOrder.setOrder_id(order_id);
				ttTrdOrder.setOrder_time(DateUtil.getCurrentDateTimeAsString());
				ttTrdOrder.setUser_id(opUserId);
			}
			// 6.默认属性赋值
			ttTrdOrder.setIs_active(DictConstants.YesNo.YES);
			ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.New);
			ttTrdOrder.setOperate_time(DateUtil.getCurrentDateTimeAsString());
			ttTrdOrder.setOperator_id(opUserId);
			
			// 7.调用其他业务接口（分发到其他的具体保存交易的类）
			approveServiceInterface.saveApprove(map, ttTrdOrder, isAdd);
			// 8.根据order_id判断执行新增或修改操作
			if (isAdd) {
				approveManageDao.insert(ttTrdOrder);
			} else {
				approveManageDao.updateApprove(ttTrdOrder);
			}
			return ttTrdOrder;
		}else {
			TtTrdTrade trade = new TtTrdTrade();
			// 4.将hashmap转换为TtTrdOrder对象
			ParentChildUtil.HashMapToClass(map, trade);
			// 5.当order_id存在时，锁定order记录,当order_id不存在时，则预先取得orader_id的值
			String opUserId = SlSessionHelper.getUserId();
			if (!StringUtil.checkEmptyNull(trade.getTrade_id())) {//判断审批投标order_id是否为空
				isAdd = false;
				HashMap<String, Object> mapRow = new HashMap<String, Object>();
				mapRow.put("trade_id", map.get("trade_id"));
				mapRow.put("is_locked", true);//加锁锁定交易
				TtTrdTrade ttTrdTradeOld = tradeManageMapper.selectTradeForTradeId(mapRow);
				JY.require(StringUtil.isNullOrEmpty(ttTrdTradeOld.getTrade_user()) || opUserId.equals(ttTrdTradeOld.getTrade_user()), "仅允许修改本人所属单据.");
	
			} else {
				isAdd = true;//表示新增交易
				String order_id = "";//主审批单据号为空
				HashMap<String, Object> mapSel = new HashMap<String, Object>();
				mapSel.put("trdtype", map.get("trdtype"));
				order_id = tradeManageMapper.getTradeId(mapSel);//获得审批单据
				trade.setTrade_id(order_id);
				trade.setTrade_time(DateUtil.getCurrentDateTimeAsString());
				trade.setTrade_user(opUserId);
			}
			// 6.默认属性赋值
			trade.setIs_active(DictConstants.YesNo.YES);
			trade.setTrade_status(DictConstants.ApproveStatus.New);
			trade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
			trade.setOperator_id(opUserId);
			
			// 7.调用其他业务接口（分发到其他的具体保存交易的类）
			approveServiceInterface.saveApprove(map, trade, isAdd);
			// 8.根据order_id判断执行新增或修改操作
			if (isAdd) {
				tradeManageMapper.insertTrade(trade);
			} else {
				tradeManageMapper.updateTrade(trade);
			}
			return trade;
		}
		
	}

	@Override
	public TtTrdTrade createTrade(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public void cancelApprove(String order_id, String folder, String opUserId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void noPassApprove(TtTrdOrder ttTrdOrder, String folder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delTrdOrderOrTrader(Object order) {
		// TODO Auto-generated method stub
		if(order instanceof TtTrdOrder) {
            approveManageDao.deleteByPrimaryKey(order);
        } else if(order instanceof TtTrdTrade) {
            tradeManageMapper.delTrade(BeanUtil.beanToMap(order));
        }
	}

	@Override
	public TtTrdOrder getTrdorderOrTrader(Object order) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateOrderOrTrader(Object order) {
		// TODO Auto-generated method stub

	}

	@Override
	public Map<String, Object> fireEvent(String arg0, String arg1, String arg2, String arg3) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getBizObj(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * 流程状态变更(用于审批流程函数回调)
	 * 
	 * @param flow_id
	 *            流程id
	 * @param serial_no
	 *            序号
	 * @param status
	 *            最新审批状态
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {
		//1、锁定审批单据的头信息，避免重复操作
		HashMap<String, Object> mapRow = new HashMap<String, Object>();TtTrdOrder ttTrdOrder = null;TtTrdTrade ttTrdTradeTemp = null;
		if(DictConstants.FlowType.BusinessApproveFlow.equals(flow_type) || DictConstants.FlowType.RdpApproveFlow.equals(flow_type)
				|| DictConstants.FlowType.RddApproveFlow.equals(flow_type)| DictConstants.FlowType.RdiApproveFlow.equals(flow_type)) {
			mapRow.put("order_id", serial_no);
			mapRow.put("is_locked", true);
			ttTrdOrder = approveManageDao.selectOrderForOrderId(mapRow);
			ttTrdOrder.setOrder_status(status);
			approveManageDao.updateApprove(ttTrdOrder);
		}else {
			mapRow.remove("order_id");
			mapRow.put("trade_id", serial_no);
			ttTrdTradeTemp = tradeManageMapper.selectTradeForTradeId(mapRow);
			ttTrdTradeTemp.setTrade_status(status);
			tradeManageMapper.updateTrade(ttTrdTradeTemp);
		}
		if (DictConstants.ApproveStatus.New.equals(status)) {//新建时候触发
			
		} else if (DictConstants.ApproveStatus.Cancle.equals(status)) {//撤销时候触发
			
		} else if (DictConstants.ApproveStatus.WaitApprove.equals(status)) {//等待审批
			
		} else if (DictConstants.ApproveStatus.ApprovedNoPass.equals(status)) {//拒绝时候触发
			
		} else if (DictConstants.ApproveStatus.ApprovedPass.equals(status)) {//审批完成
			if(DictConstants.FlowType.BusinessApproveFlow.equals(flow_type)) {//事前审批触发事件处理
				String folder = sysParamService.selectSysParam("000002", ttTrdOrder.getTrdtype()).getP_prop1();//找到处理类参数标记
				//可以根据处理类别  找到真实的处理类
				String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
				ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
				if (approveServiceInterface.need2Trade(ttTrdOrder)) {//判断当前的业务是否需要生成确认单
					TtTrdTrade ttTrdTrade = new TtTrdTrade();
					HashMap<String,Object> map = new HashMap<String, Object>();
					map = BeanUtil.beanToHashMap(ttTrdOrder);
					String trade_id = tradeManageMapper.getTradeId(map);
			        String opUserId = ParameterUtil.getString(map, "opUserId", ttTrdOrder.getUser_id());
					ttTrdTrade.setTrade_time(DateUtil.getCurrentDateTimeAsString());
					ttTrdTrade.setTrade_id(trade_id);
					ttTrdTrade.setIs_active(DictConstants.YesNo.YES);
					ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.New);
					ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
					ttTrdTrade.setOperator_id(opUserId);
					ttTrdTrade.setTrade_user(ParameterUtil.getString(map, "trade_user", opUserId));
					approveServiceInterface.createTrade(map, ttTrdTrade);
					approveServiceInterface.need2DoApproveOhters(ttTrdOrder);
					tradeManageMapper.insertTrade(ttTrdTrade);
				}
				/**
				 * 支付指令和收款指令处理
				 */
				approveServiceInterface.createPayRevDeals(ttTrdOrder.getOrder_id(), DictConstants.DealType.Approve, ttTrdOrder.getTrdtype());
			}
			if(DictConstants.FlowType.VerifyApproveFlow.equals(flow_type) || DictConstants.FlowType.RdpVerifyFlow.equals(flow_type)) {//确认完成
				String folder = sysParamService.selectSysParam("000002", ttTrdTradeTemp.getTrdtype()).getP_prop1();//找到处理类参数标记
				String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
				ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
				approveServiceInterface.need2DoVerifyOhters(ttTrdTradeTemp);
				/**
				 * 支付指令和收款指令处理
				 */
				approveServiceInterface.createPayRevDeals(ttTrdTradeTemp.getTrade_id(), DictConstants.DealType.Verify, ttTrdTradeTemp.getTrdtype());
			}
			
		}
		
	}

	@Override
	public void delApproveInfo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String trdType = ParameterUtil.getString(map, "trdtype", "");//交易类别
		String folder = sysParamService.selectSysParam("000002", trdType).getP_prop1();
		String dealType = ParameterUtil.getString(map, "dealType", DictConstants.BillsType.Approve);//判断审批类型
		
		/**
		 * 判断是否是当前用户自己维护的交易
		 */
		String opUserId = SlSessionHelper.getUserId();
		HashMap<String, Object> mapRow = new HashMap<String, Object>();
		mapRow.put("order_id", map.get("dealNo"));
		mapRow.put("trade_id", map.get("tradeNo"));
		mapRow.put("is_locked", true);//加锁锁定交易
		if(dealType.equals(DictConstants.DealType.Approve)) {
			TtTrdOrder ttTrdOrderOld = approveManageDao.selectOrderForOrderId(mapRow);
			JY.require(StringUtil.isNullOrEmpty(ttTrdOrderOld.getUser_id()) || opUserId.equals(ttTrdOrderOld.getUser_id()), "仅允许本人删除所属单据.");
			TtTrdOrder ttTrdOrder = new TtTrdOrder();
			ttTrdOrder.setOrder_id(ParameterUtil.getString(map, "dealNo", ""));
			ttTrdOrder.setTrdtype(trdType);
			this.delTrdOrderOrTrader(ttTrdOrder);
		}else {
			mapRow.remove("order_id");
			mapRow.put("trade_id", map.get("tradeNo"));
			TtTrdTrade ttTradeOld = tradeManageMapper.selectTradeForTradeId(mapRow);
			JY.require(StringUtil.isNullOrEmpty(ttTradeOld.getTrade_user()) || opUserId.equals(ttTradeOld.getTrade_user()), "仅允许本人删除所属单据.");
			TtTrdTrade ttTrdTrade = new TtTrdTrade();
			ttTrdTrade.setTrade_id(ParameterUtil.getString(map, "dealNo", ""));
			ttTrdTrade.setTrdtype(trdType);
			this.delTrdOrderOrTrader(ttTrdTrade);
		}
		
		//可以根据处理类别  找到真实的处理类
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		approveServiceInterface.delApprove(map);
		
	}

	@Override
	public Object getApproveById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String trdType = ParameterUtil.getString(map, "trdtype", "");//交易类别
		String folder = sysParamService.selectSysParam("000002", trdType).getP_prop1();
		//可以根据处理类别  找到真实的处理类
		String beanName = SettleConstants.getBeanFromTrdType(folder, DictConstants.BillsType.Approve);
		ApproveFundServiceListener approveServiceInterface = SpringContextHolder.getBean(beanName);
		return approveServiceInterface.selectApproveInfo(map);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

}
