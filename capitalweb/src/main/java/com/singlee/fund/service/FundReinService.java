package com.singlee.fund.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRein;
import com.singlee.fund.model.FtReinDetails;

/**
 * 红利再投接口
 * @author 
 *
 */
public interface FundReinService extends FundCustomerService {
	/**
	 * 根据编号查询交易
	 */
	FtRein searchReinByNo(Map<String, Object> map);
	/**
	 * 分页查询交易
	 */
	Page<FtRein> searchReinList(Map<String, Object> map,int isFinished);
	
	/**
	 * 明细分红信息
	 * @param map
	 * @return
	 */
	public List<FtReinDetails> searchFtReinDetails(Map<String, Object> map);
}
