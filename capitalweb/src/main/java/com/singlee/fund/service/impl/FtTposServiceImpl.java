package com.singlee.fund.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.fund.mapper.FtTposMapper;
import com.singlee.fund.model.FtTpos;
import com.singlee.fund.service.FtTposService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FtTposServiceImpl implements FtTposService {
	@Autowired
	FtTposMapper ftTposMapper;
	@Override
	public Page<FtTpos> getTposList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		RowBounds rb = ParameterUtil.getRowBounds(params);
		return ftTposMapper.getTposList(params, rb);
	}
	@Override
	public Pair<List<String>,List<Double>> getTposListForPair(Map<String, Object> params) {
		// TODO Auto-generated method stub
		RowBounds rb = ParameterUtil.getRowBounds(params);
		List<FtTpos> ftTposList = ftTposMapper.getTposList(params, rb);
		List<String> left = new ArrayList<String>();
		List<Double> right = new ArrayList<Double>();
		Pair<List<String>,List<Double>> pair = new Pair<List<String>, List<Double>>(left, right);
		for(FtTpos ftTpos:ftTposList) {
			left.add(ftTpos.getFundName()+"*"+ftTpos.getFundCode());
			right.add(PlaningTools.div(ftTpos.getUtQty().doubleValue(), 10000, 2));
		}
		pair.setLeft(left);pair.setRight(right);
		return pair;
	}
	@Override
	public FtTpos geFtTposByBusinessKey(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return ftTposMapper.geFtTposByBusinessKey(params);
	}
	@Override
	public void saveFtTpos(FtTpos ftTpos) {
		// TODO Auto-generated method stub
		this.ftTposMapper.insert(ftTpos);
	}
	@Override
	public void updateFtTpos(FtTpos ftTpos) {
		// TODO Auto-generated method stub
		ftTposMapper.updateByPrimaryKeySelectiveAll(ftTpos);
	}
}
