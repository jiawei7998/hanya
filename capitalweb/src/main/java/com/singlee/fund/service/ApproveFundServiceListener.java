package com.singlee.fund.service;

import java.util.HashMap;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.trade.model.TtTrdTrade;

public interface ApproveFundServiceListener {
	/**
	 * 审批单查询
	 * @param map
	 * @return
	 */
	public Page<?> searchApproveList(Map<String, Object> map,int isFinished);
	
	/**
	 * 查询审批单明细信息
	 * @param map
	 * @return
	 */
	public Object selectApproveInfo(Map<String,Object> map);
	
	/**
	 * 审批单保存方法
	 * @param map：前台传入参数
	 * @param ttTrdOrder：默认赋值后的TtTrdOrder对象
	 * @param isAdd：true 表示当前执行新增操作
	 */
	public void saveApprove(HashMap<String, Object> map,Object ttTrdOrder, boolean isAdd);
	
	/**
	 * 审批单生成交易单
	 * @param map：前台返回的hashmap对象
	 * @param ttTrdTrade：原审批单生成的交易单对象，可根据实际需求修改该对象
	 * @return
	 */
	public void createTrade(HashMap<String,Object> map,TtTrdTrade ttTrdTrade);

	/**
	 * 注销审批单
	 * @param ttTrdOrder
	 */
	public void cancelApprove(TtTrdOrder ttTrdOrder);
	/**
	 * 删除单据
	 * @param ttTrdOrder/ttTrdTrade
	 */
	public void delApprove(Map<String, Object> map );
	
	/**
	 * 审批单审批拒绝
	 * @param ttTrdOrder
	 */
	public void noPassApprove(TtTrdOrder ttTrdOrder);
	
	
	void beforeSave(HashMap<String,Object> map);
	

	public boolean need2Trade(TtTrdOrder ttTrdOrder);
	/**
	 * 根据不同的情况进行相应交易审批通过的处理（确认）
	 * @return
	 */
	public boolean need2DoVerifyOhters(TtTrdTrade ttTrdTrade);
	/**
	 * 审批结束需要触发的事件
	 * @param ttTrdOrder
	 * @return
	 */
	public boolean need2DoApproveOhters(TtTrdOrder ttTrdOrder);
	/**
	 * 收付款指令
	 * @return
	 */
	public  boolean createPayRevDeals(String dealNoOrTradeNo,String dealType,String trdType);
	
}
