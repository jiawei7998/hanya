package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtPriceDaily;

/**
 * 货币基金基础信息
 * @author 
 *
 */
public interface FundMktService{
	/**
	 * 分页查询货币基金基础信息
	 */
	Page<FtPriceDaily> searchInfoList(Map<String, Object> map);
	/**
	 * 保存市场数据
	 * @param priceDaily
	 */
	void saveFtPriceDaily(FtPriceDaily priceDaily);
	/**
	 * 根据 fundCode + postdate 获得唯一的数据
	 * @param priceDaily
	 * @return
	 */
	FtPriceDaily getFtPriceDailyByKey(FtPriceDaily priceDaily);
}
