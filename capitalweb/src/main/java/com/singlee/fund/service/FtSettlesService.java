package com.singlee.fund.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtSettles;

/**
 * 基金支付/收款信息服务类
 * @author louhuanqing
 *
 */
public interface FtSettlesService {
	/**
	 * 根据
	 */
	void saveFtSettles(FtSettles ftSettles);
	/**
	 * 查询分页信息
	 * @param map
	 * @return
	 */
	Page<FtSettles> getFtSettelsPage(Map<String, Object> map);
}
