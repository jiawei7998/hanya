package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.fund.mapper.FtPriceDailyMapper;
import com.singlee.fund.model.FtPriceDaily;
import com.singlee.fund.service.FundMktService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FundMktServiceImpl implements FundMktService {
	@Autowired
	private FtPriceDailyMapper ftPriceDailyMapper;
	@Override
	public Page<FtPriceDaily> searchInfoList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ftPriceDailyMapper.searchInfoPage(map, ParameterUtil.getRowBounds(map));
	}
	@Override
	public void saveFtPriceDaily(FtPriceDaily priceDaily) {
		// TODO Auto-generated method stub
		FtPriceDaily  temp = ftPriceDailyMapper.selectByPrimaryKey(priceDaily);
		if(null != temp)
		{
			ftPriceDailyMapper.deleteByPrimaryKey(priceDaily);
		}
		ftPriceDailyMapper.insert(priceDaily);
	}
	@Override
	public FtPriceDaily getFtPriceDailyByKey(FtPriceDaily priceDaily) {
		// TODO Auto-generated method stub
		return ftPriceDailyMapper.selectByPrimaryKey(priceDaily);
	}

}
