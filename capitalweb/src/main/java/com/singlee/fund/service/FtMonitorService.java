package com.singlee.fund.service;
/**
 * 监控列表的服务类
 * @author louhuanqing
 *
 */

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtMonitor;

public interface FtMonitorService {

	public Page<FtMonitor> getFtMonitorPage(Map<String, Object> map);
}
