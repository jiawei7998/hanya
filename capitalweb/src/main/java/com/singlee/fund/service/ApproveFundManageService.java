package com.singlee.fund.service;

import java.util.HashMap;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.trade.model.TtTrdTrade;

public interface ApproveFundManageService {

	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	Page<?> searchApproveList(Map<String,Object> map);
	/**
	 * 查询审批单明细信息
	 * @param map
	 * @return
	 */
	HashMap<String,Object> selectApproveInfo(HashMap<String,Object> map);
	/**
	 * 保存审批单信息
	 * @param map
	 */
	Object saveApproveInfo(HashMap<String,Object> map);
	/**
	 * 删除单据
	 * @param map
	 */
	void delApproveInfo(Map<String,Object> map);
	/**
	 * 审批单生成交易单
	 * @param map
	 */
	TtTrdTrade createTrade(HashMap<String,Object> map);
	/**
	 * 注销审批单信息
	 * @param order_id
	 */
	void cancelApprove(String order_id,String folder,String opUserId);
	
	/**
	 * 审批单审批拒绝
	 * @param order_id
	 */
	void noPassApprove(TtTrdOrder ttTrdOrder,String folder);
	
	/**
	 * 删除order数据
	 * @param order
	 */
	void delTrdOrderOrTrader(Object order);
	
	/**
	 * 获取order详细对象
	 * @param order
	 * @return
	 */
	Object getTrdorderOrTrader(Object order);
	
	/**
	 * 修改审批单
	 * @param order
	 */
	public void updateOrderOrTrader(Object order);
	/**
	 * 通过单据唯一dealNo/TradeNo获得数据
	 * @param map
	 * @return
	 */
	public Object getApproveById(Map<String,Object> map);
}
