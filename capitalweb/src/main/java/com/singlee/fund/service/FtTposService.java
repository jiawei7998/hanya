package com.singlee.fund.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.fund.model.FtTpos;

/**
 * Ft的头寸操作服务接口
 * @author louhuanqing
 *
 */
public interface FtTposService {
	/**
	 * 根据条件获得需要的基金持仓信息
	 * @param params
	 * @return
	 */
	Page<FtTpos>  getTposList(Map<String, Object> params);
	/**
	 * 返回双子 用于X Y轴
	 * @param params
	 * @return
	 */
	Pair<List<String>,List<Double>> getTposListForPair(Map<String, Object> params);
	/**
	 * 根据prdNo invtype fundCode sponInst 获得持仓TPOS
	 * 由于每只基金的fundCode唯一，决定了cno和其他ccy等唯一性，所以无需再进行key的约束查询了。
	 * @param params
	 * @return
	 */
	FtTpos geFtTposByBusinessKey(Map<String, Object> params);
	/**
	 * 保存ftTpos
	 * @param ftTpos
	 */
	void  saveFtTpos(FtTpos ftTpos);
	/**
	 * 不为空的属性进行选择性更新
	 * @param ftTpos
	 */
	void  updateFtTpos(FtTpos ftTpos);
}
