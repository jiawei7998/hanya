package com.singlee.fund.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.fund.model.FtRdpConf;
import com.singlee.fund.model.FtRdpDetailsConf;

/**
 * 基金赎回确认接口
 * @author 
 *
 */
public interface FundRdpConfService extends FundCustomerService {
	/**
	 * 分页查询申购确认信息
	 */
	Page<FtRdpConf> searchRdpConfPage(Map<String, Object> map,int isFinished);
	/**
	 * 根据tradeNo查询所关联的赎回明细
	 * @param map
	 * @return
	 */
	List<FtRdpDetailsConf> searchRdpConfDetails(Map<String, Object> map);
}
