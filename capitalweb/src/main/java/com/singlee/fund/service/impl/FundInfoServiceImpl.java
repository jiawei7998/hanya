package com.singlee.fund.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.fund.mapper.FtInfoMapper;
import com.singlee.fund.model.FtInfo;
import com.singlee.fund.service.FundInfoService;
@Service(value="fundInfoService")
public class FundInfoServiceImpl implements FundInfoService {
	@Autowired
	private FtInfoMapper infoMapper;
	@Override
	public void saveApprove(Map<String, Object> map) {
		FtInfo fundInfo=new FtInfo();
		BeanUtil.populate(fundInfo, map);
		infoMapper.insert(fundInfo);
	}

	@Override
	public void updateApprove(Map<String, Object> map) {
		FtInfo fundInfo=new FtInfo();
		BeanUtil.populate(fundInfo, map);
		infoMapper.updateByPrimaryKey(fundInfo);
	}

	@Override
	public void deleteAppove(Map<String, Object> map) {
		infoMapper.deleteByPrimaryKey(String.valueOf(map.get("fundCode")));
	}

	@Override
	public Object selectApproveById(Map<String, Object> map) {
		FtInfo info=infoMapper.selectByPrimaryKey(String.valueOf(map.get("fundCode")));
		return info;
	}

	@Override
	public Page<FtInfo> searchInfoList(Map<String, Object> map) {
		Page<FtInfo> infoPage=infoMapper.searchInfoPage(map, ParameterUtil.getRowBounds(map));
		return infoPage;
	}

}
