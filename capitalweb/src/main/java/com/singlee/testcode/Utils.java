package com.singlee.testcode;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;
 
public abstract class Utils {

	public static String getTime() {
		return getDate("HHmmss");
	}

	public static String getDate() {
		return getDate("yyyyMMdd");
	}
	
	public static String getDateTime(){
		return getDate("yyyyMMdd hhmmss");
	}
	
	public static String getDate(String dateFormat){
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Calendar cal = Calendar.getInstance();
		return format.format(cal.getTime());
	}
	
	public static String dateFormat(String date, String fromFormat, String toFormat){
		SimpleDateFormat dffrom = new SimpleDateFormat(fromFormat);
		SimpleDateFormat dfto = new SimpleDateFormat(toFormat);
		String s = null;		
		try {
			Date today = dffrom.parse(date);
			s = dfto.format(today);
		}catch(ParseException e){
			e.printStackTrace();
		}
		return s;
	}
	
	/*
	 * byte[] b1 뒤에 byte[] b2를 붙인다. 
	 */
	public static byte[] concatByte( byte[] b1, byte[] b2 ) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		baos.write( b1 );
		baos.write( b2 );
		
		return baos.toByteArray();
		
	}
	
	/* @param num  
	* 대상 Number형 숫자  
	* @param size  
	* 원하는 자릿수  
	* @return 대상문자열에 자릿수 만큼의 '0'를 붙인 문자열  
	*/  
	public static String zerofill(Number num, int size) {  
	 String zero = "";  
	 for (int i = 0; i < size; i++) {  
	  zero += "0";  
	 }  
	 DecimalFormat df = new DecimalFormat(zero);  
	 return df.format(num);  
	}
	
	
	/* @param num  
	* 대상 Number형 숫자  
	* @param size  
	* 원하는 자릿수  
	* @return 대상문자열에 자릿수 만큼의 '0'를 붙인 문자열  
	*/  
	public static String zerofill(String num, int size) {  
	 String zero = "";  
	 for (int i = 0; i < size; i++) {  
	  zero += "0";  
	 }  
	 DecimalFormat df = new DecimalFormat(zero);  
	 return df.format(num);  
	}
	
	
	/**
	 * s1과 s2를 비교한다.
	 */
	public  static int strCmp(String s1, String s2) {
		
		int ret = -1;
		
		if ((s1 == null) && (s2 == null)) {
			ret = 0;
		}
		else if ((s1 != null) && (s2 != null)) {
			ret = s1.compareTo(s2);
		}
		else if ((s1 == null) && (s2 != null)) {
			if (s2.equals("")) {
				ret = 0;
			}
		}
		else if ((s1 != null) && (s2 == null)) {
			if (s1.equals("")) {
				ret = 0;
			}
		}
		
		return ret;
	}	
	
	/**
	 * <b> double d를 type형태로 변환한다.
	 */
	public  static String numFormat(double d, String type) {
		DecimalFormat df = new DecimalFormat(type);
		return df.format(d);
	}
	
	/**
	 * String s를 n위치에서 len자리만큼 자른다.
	 */
	public  static String subStr(String s, int n, int len) {
		if (s == null) {
			return null;
		}
		
		if ((n + len - 1) > s.getBytes().length) {
			return null;
		}
		
		byte[] b = s.getBytes();
		byte[] v = new byte[len];
		if (b.length > 0) {
			for (int i = 0; i < len; i++) {
				v[i] = b[i+n-1];
			}
			return new String(v).toString();
		}
		else {
			return null;
		}
	}
	
	/**
	 * Date Format형식으로 변환
	 */
	public  static String dateFormat(java.sql.Date d, String type) {
		SimpleDateFormat fmt = new SimpleDateFormat(type);
		return fmt.format(d).toString();
	}
	 
	/**
	 * Date Format형식으로 변환
	 */
	public  static String dateFormat(java.util.Date d, String type) {
		SimpleDateFormat fmt = new SimpleDateFormat(type);
		return fmt.format(d).toString();
	}
	
	public static Date stringToDate(String str) 
	{
		
		
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = null;
			try{
				 date = formatter.parse(str);
			}catch (Exception e)
			{
				
				
			}
			
			return date;
	}
	
	
	 /**
     * yyyyMMdd 형식 SimpleDateFormat
     * ex) "20081030"
     */
    public static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");

    /**
     * HHmmss 형식 SimpleDateFormat
     * ex) "235633"
     */
    public static final SimpleDateFormat HHmmss = new SimpleDateFormat("HHmmss");

    /**
     * HHmmss 형식 SimpleDateFormat
     * ex) "235633"
     */
    public static final SimpleDateFormat HHmm = new SimpleDateFormat("HH:mm");
    
    /**
     * yyyyMMdd HH:mm:ss 형식 SimpleDateFormat
     * ex) "20081030 23:56:33"
     */
    public static final SimpleDateFormat yyyyMMdd_HH$mm$ss = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    
    public static String nvl(String src, String def) {
		if (src == null || "".equals(src.trim())) {
			return def;
		} else {
			return src;
		}
	}
    
    public static String getCurrentDate(String fmt) {
		SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.format(new Date());
	}
    
    public static Date strToDate(String value, String fmt) {
    	SimpleDateFormat sdf = new SimpleDateFormat(fmt);
    	Date result = null;
    	try {
    		result = sdf.parse(value);
    	}catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	return result;
    }
    
    
    public static String dateToStr(Date date, String fmt) {
    	SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.format(date);
    }
    
    
//    public static double convertAmtDouble(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD * 100;
//    	
//    	return tempAmtD;
//    }
//    
//    public static double convertAmtDouble1000000(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD * 1000000;
//    	
//    	return tempAmtD;
//    }
//    
//    
//    public static String convertAmtString(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD / 100;
//    	
//    	return Double.toString(tempAmtD);
//    }
//    
//    public static double convertAmtDoubleDiv100(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD / 100;
//    	
//    	return tempAmtD;
//    }
//    
//    public static double convertAmtDoubleDiv1000000(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD / 1000000;
//    	
//    	return tempAmtD;
//    }
//    
//    
    public static double convertAmtDoubleDivPoint12(String amt)
    {
    	double tempAmtD = new Double(amt);
    	
    	tempAmtD = tempAmtD / 1000000;
    	
    	tempAmtD = tempAmtD / 1000000;
    	
    	return tempAmtD;
    }
//    
//    
//    
//    public static double convertAmtDoubleNoneDiv100(String amt)
//    {
//    	double tempAmtD = new Double(amt);
//    	
//    	tempAmtD = tempAmtD ;
//    	
//    	return tempAmtD;
//    }
    
    public static  String statusCdName(String status)
	{
		String statusNm ="";
		
		if(StringUtils.isEmpty(status))
			status = "";
		
		if(status.equals("1"))
			statusNm = "Required";
		else if(status.equals("2"))
			statusNm = "Complete";
		else if(status.equals("3"))
			statusNm = "Abnormal";
		else if(status.equals("9"))
			statusNm = "Etc";
		
		return statusNm;
	}
    
//    public static String decryptString(GibCrypto crypto,String hexStr) throws Exception
//    {
//		
//    	return crypto.Decrypt(hexStr);
//    	
//    }
//    
//    public static String encryptString(GibCrypto crypto,String hexStr) throws Exception
//    {
//		
//    	return crypto.Encrypt(hexStr);
//    	
//    }
    
    public static double parseDouble(String arg)
    {
    	if(StringUtils.isEmpty(arg))
    	{
    		return 0d;
    	}
    	else
    	{
    		return Double.parseDouble( arg ) ;
    	}
    	
    }
    
    
    public static String currencyDbltoString(double amt)
    {
    	
    	if(amt == 0.00d)
    		return "0.00";
    	
    	DecimalFormat df = new DecimalFormat("################0.00#");
    	String samt = df.format(amt);
    	
    	return samt;
    }
    
    public static String longStringtoString(Long amt)
    {
   	
    	DecimalFormat df = new DecimalFormat("################0");
    	String samt = df.format(amt);
    	
    	return samt;
    }    
//    
//    
//    public static String recvCurrencyDbltoString(double amt)
//    {
//    	
//    	if(amt == 0.00d)
//    		return "0.00";
//    	
//    	DecimalFormat df = new DecimalFormat("################0.00#");
//    	String samt = df.format(amt);
//    	
//    	return samt;
//    }
//    
//    public static String recvCurrencyStringToString(String amt)
//    {
//    	
//    	DecimalFormat df = new DecimalFormat("################0.00#");
//    	String samt = df.format(amt);
//    	
//    	return samt;
//    }
//    
//    public static String sendCurrencyDbltoStringPoint6(double amt)
//    {
//    	
//    	
//    	DecimalFormat df = new DecimalFormat("###########0");
//    	String samt = df.format(amt);
//    	
//    	return samt;
//    }  
//     
//    public static String sendCurrencyDbltoString(double amt)
//    {
//    	
//    	
//    	DecimalFormat df = new DecimalFormat("###########0");
//    	String samt = df.format(amt);
//    	
//    	return samt;
//    }
    

    
    
    public static String toDecimalStr( double value, int max, int min ) {
    	NumberFormat nf= NumberFormat.getInstance();
    	nf.setGroupingUsed(false);
    	nf.setMaximumFractionDigits(max);
    	nf.setMinimumFractionDigits(min);
    	
    	return nf.format(value );
    }
    
    public static String toDecimalStr( String value, int max, int min ) {
    	return toDecimalStr(Double.parseDouble(value), max, min ); 	
    }
    
    /* 10000-> 100.00 hobis->bt->pt */
    public static double toDecimalNumAmt( String value ) {  		
		return  Double.parseDouble(value)/100 ;
    }
    
    /* 10000-> 100.00 hobis->bt->pt */
    public static String toDecimalStrAmt( String value ) {  		
		return toDecimalStr( toDecimalNumAmt(value) , 2, 2 );
    }
    
    /* 100.00-> 100.00 hobis->bt->pt */
    public static String toDecimalStrAmt( double value ) {  		
		return toDecimalStr( value , 2, 2 );
    }
    
    /* 100.00 -> 10000 pt->bt->hobis */
	public static double toFixedNumAmt( String value ) {
		return Double.parseDouble(value)*100;
    }
	
    /* 100.00 -> 10000 pt->bt->hobis */
	public static String toFixedStrAmt( String value ) {
		value = Utils.nvl(value, "0");
		return toDecimalStr( toFixedNumAmt(value) , 0, 0 );
    }
	
	/* 100.000000 -> 10000000000 pt->bt->hobis */
	public static String toFixedStrRate( String value ) {
		return toDecimalStr( Double.parseDouble(value)*1000000, 0, 0 );
    }
	
	/* 10000 -> 100.000000 pt->bt->hobis */
	public static String toDecimalStrRate( String value ) {
		return toDecimalStr( Double.parseDouble(value)/1000000, 6, 6 );
    }
    
	/**
	 * 일수 연산 하여 가감된 일자를 리턴함
	 * @param inputDate
	 * @param days
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static String dateCalcu(String inputDate, int days)
	{
		DateFormat sdFormat = new SimpleDateFormat("yyyyMMdd");
		Date tempDate=null;
		
		try {
			tempDate = sdFormat.parse(inputDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		Calendar cal = Calendar.getInstance();
		cal.setTime(tempDate);
		cal.add(cal.DATE,days);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat ("yyyyMMdd"); 
		
		
		return dateFormatter.format ( cal.getTime() );
		
	}
	
	public static String LPAD(String str, int len, char pad) {
		return PAD(str, len, pad, true);
	}

	public static String PAD(String str, int len, char pad, boolean left) {
		if (str == null)
			str = "";
		if (str.getBytes().length >= len) {
			return new String(str.getBytes(), 0, str.getBytes().length);
		}
		String result = str;
		StringBuffer sb = new StringBuffer();
		int templen = len - result.getBytes().length;

		for (int i = 0; i < templen; i++) {
			sb.append(pad);
		}
		if (left) {
			return sb.toString() + result;
		} else {
			return result + sb.toString();
		}
	} 	
	
	@SuppressWarnings("rawtypes")
	public static Class[] getClasses(String packageName)
	        throws ClassNotFoundException, IOException {
	    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    assert classLoader != null;
	    String path = packageName.replace('.', '/');
	    Enumeration<URL> resources = classLoader.getResources(path);
	    List<File> dirs = new ArrayList<File>();
	    while (resources.hasMoreElements()) {
	        URL resource = resources.nextElement();
	        dirs.add(new File(resource.getFile()));
	    }
	    ArrayList<Class> classes = new ArrayList<Class>();
	    for (File directory : dirs) {
	        classes.addAll(findClasses(directory, packageName));
	    }
	    return classes.toArray(new Class[classes.size()]);
	}
	
	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 *
	 * @param directory   The base directory
	 * @param packageName The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
	    List<Class> classes = new ArrayList<Class>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    for (File file : files) {
	        if (file.isDirectory()) {
	            assert !file.getName().contains(".");
	            classes.addAll(findClasses(file, packageName + "." + file.getName()));
	        } else if (file.getName().endsWith(".class")) {
	            classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
	        }
	    }
	    return classes;
	}
	
//	public static String prettyXml(String src) {
//		
//		StreamResult xmlOutput = null;
//		String result = "";
//		try {
//	        Source xmlInput = new StreamSource(new StringReader(src));
//	        StringWriter stringWriter = new StringWriter();
//	        xmlOutput = new StreamResult(stringWriter);
//	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//	        transformerFactory.setAttribute("indent-number", 2);
//	        //Fortify 권고로 추가
//	        transformerFactory.setFeature("http://javax.xml.XMLConstants/property/accessExternalDTD", false);
//	        
//	        Transformer transformer = transformerFactory.newTransformer(); 
//	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");	        	        	        	      
//	        transformer.transform(xmlInput, xmlOutput);
//	        
//	        result = xmlOutput.getWriter().toString();
//	        xmlOutput.getWriter().close();   	        	       
//	    } catch (Exception e) {
//	        throw new RuntimeException(e); // simple exception handling, please review it
//	    } finally {
//	    	if (xmlOutput.getWriter() != null) try {xmlOutput.getWriter().close();}catch(Exception e){}
//	    }
//		return result;
//	}
	
//	public static <T extends Object, Y extends Object> void copyProtoFields(T from, Y too) {
//	    Class<? extends Object> fromClass = from.getClass();
//	    Field[] fromFields = fromClass.getDeclaredFields();
//
//	    Class<? extends Object> tooClass = too.getClass();
//	    Field[] tooFields = tooClass.getDeclaredFields();
//
//	    if (fromFields != null && tooFields != null) {
//	        for (Field tooF : tooFields) {
//	            
//	            try {
//	                Field fromF = fromClass.getDeclaredField(tooF.getName()+"_");
//	                fromF.setAccessible(true);
//	                tooF.setAccessible(true);
//	                if("int".equals(tooF.getType().getName())) {
//	                	String v = Utils.nvl((String) fromF.get(from), "");
//	                	if (!"".equals(v)) {
//	                		tooF.setInt(too, fromF.getInt(Integer.parseInt(v)));
//	                	}
//	                }else {
//	                	tooF.set(too, fromF.get(from));
//	                }
//	            } catch (Exception e) {
//	                e.printStackTrace();
//	            } 
//	        }
//	    }
//	}
	
	public static <T extends Object, Y extends Object> void copyEntityToProto(T entity, Y proto) {
		try {
			for(PropertyDescriptor propertyDescriptor : 
			    Introspector.getBeanInfo(entity.getClass()).getPropertyDescriptors()){
			    
				String getter = "get"
						+ propertyDescriptor.getName().substring(0, 1).toUpperCase()
						+ propertyDescriptor.getName().substring(1);
			    
			    String setter = "set"
						+ propertyDescriptor.getName().substring(0, 1).toUpperCase()
						+ propertyDescriptor.getName().substring(1);
			    
			    try {
				    Method getMethod = entity.getClass().getMethod(getter);
				    Object getValue = getMethod.invoke(entity);
				    
				    Method setMethod = proto.getClass().getMethod(setter, String.class);
				    if(getValue==null) getValue = "";
				    setMethod.invoke(proto, String.valueOf(getValue));
			    }catch(Exception e) {
			    	// skip
			    }
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String convertHobisRateFormat(String value) {
		
		if(StringUtils.isEmpty(value)) value = "0";
		
		double dVal = Double.parseDouble(value.replaceAll(",", "")) * 1000000;
		
		return String.format("%012d", (int)dVal);
	}	
	
	/* 자주쓰는 계좌에서 이체 방법 결정*/
    public static String svcIdConvertTranferId(String svcId, String gubun)
	{
		String id="";
       
		
			
		
		if(svcId.equals("SGIB0501A001"))
		{	
			id = "1";  /*OVERBOOKING*/
		}/* 타행이체 */
		else if(svcId.equals("SGIB0502A001"))
		{	
			
			if(! StringUtils.isEmpty(gubun))
			{
				if(gubun.equals("1"))
				{
					 id = "2";   /*LLG*/
				}else if(gubun.equals("2"))
				{
					id = "3";    /*RTGS*/
				}
			}
			else
			{
				id ="";
			}
		}
		else if(svcId.equals("SGIB0503A001"))
		{	
			
			
			if(! StringUtils.isEmpty(gubun))
			{
				if(gubun.equals("01"))
				{
					 id = "1";    /*당행*/
				}else if(gubun.equals("02"))
				{
					id = "2";    /*LLG*/
				}else if(gubun.equals("03"))
				{
					id = "3";    /*RTGS*/
				}else if(gubun.equals("04"))
				{
					id = "4";    /*TRANSFER ONLINE*/
				}
			}
			else
			{
				id ="";
			}
		}
		else if(svcId.equals("SGIB0504A001"))
		{	
			id = "4";
		
		}
		return id;
		
	}
    
	public static String objToStr(Object obj) {
		
		if(obj == null || "".equals(obj.toString()))
			return "";
		else 
			return obj.toString();
	}
	
	 /* 거래내역 에서 이체 방법 결정*/
    public static String svcIdConvertFncTrscId(String svcId, String gubun)
	{
		String id="";
       
	
		
		if(svcId.equals("SGIB0501A001"))
		{	
			id = "0";  /*OVERBOOKING*/
		}/* 타행이체 */
		else if(svcId.equals("SGIB0502A001"))
		{	
			if(! StringUtils.isEmpty(gubun))
			{
				if(gubun.equals("1"))
				{
					 id = "1";   /*LLG*/
				}else if(gubun.equals("2"))
				{
					id = "2";    /*RTGS*/
				}
			}else
			{
				id="";
			}
			
		}else if(svcId.equals("SGIB0507A001"))/* 타행이체 */
		{	
			id = "3";    /*Online*/
		}
		else if(svcId.equals("SGIB0503A001"))/* 예약이체 */
		{	
			if(! StringUtils.isEmpty(gubun))
			{
			
				if(gubun.equals("01"))
				{
					 id = "0";    /*당행*/
				}else if(gubun.equals("02"))
				{
					id = "1";    /*LLG*/
				}else if(gubun.equals("03"))
				{
					id = "2";    /*RTGS*/
				}else if(gubun.equals("04"))
				{
					id = "3";    /*TRANSFER ONLINE*/
				}
			}
			else
			{
				id ="";
			}
		}
		else if(svcId.equals("SGIB0504A001"))
		{	
			id = "3";
		
		}
		return id;
		
	}
  //sendAmt = realAmt*100
	public static String makeAmt(String src) { 
		if (src==null || "".equals(src)) return "";
		return Utils.toFixedStrAmt(Utils.nvl(src, "0"));
	}
	
	//realAmt = reveiveAmt/100
	public static String reduceAmt(String src) {
		if (src==null || "".equals(src) || "null".equals(src)) return "";
		return Utils.toDecimalStr(Utils.toDecimalNumAmt(src) , 2, 2);
	}   
   
}