package com.singlee.testcode;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.adpt.util.EAIException;
import com.hanafn.eai.client.stdtmsg.StdTMsg;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.hanafn.eai.client.stdtmsg.StdTMsgValidator;
import com.hanafn.eai.client.stdtmsg.util.StdTMsgException;

import com.singlee.capital.common.util.PropertiesUtil;

public class test {

	 private String str;

	    private int byteNum;
	
	public static void main(String[] args) throws Exception {
		
		/*File[] list = new File("C:\\Users\\hspcadmin\\Desktop\\韩亚银行OPICS升级\\市场数据新模板").listFiles();
        for(File file : list)
        {
           if(file.isFile())
           {
               if (file.getName().contains("BONDDATA20221130")) {
                   // 就输出该文件的绝对路径
                   System.out.println(file.getName());
               }
 
           }
        }*/
		String s="adadeeq";
		int i=5;
		System.out.println(s.substring(0,i));
		// TODO Auto-generated method stub
		/*String trscDv = "S";
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		//EAI接口ID
		String intfId = "OPS_CHS_DSS00001";

		if (args.length >= 1) {
			trscDv = args[0];
			
		}

		if (args.length >= 2) {
			eaiDv = args[1];
		}

		if (args.length >= 3) {
			intfId = args[2];
		}

		test sample = new test();
		trscDv="XX";
        
		if ("S".equals(trscDv)) {
			sample.sendSychMsg(eaiDv, intfId);
		} else if ("A".equals(trscDv)) {
			sample.sendAsynMsg(eaiDv, intfId);
		} else if ("H".equals(trscDv)) {
			sample.sendHealthCheckMsg(eaiDv);
		}else {
			sample.sendLogin(eaiDv, intfId);
		}
		
		
		
		try {
			System.out.println(new String(sample.makeLoginRequstStdMsg(intfId)));
		} catch (StdTMsgException e) {
			e.printStackTrace();
		}
		*/
		//getFile("eai.properties");
		
		/*String s="9313.92"	;	
		String s1=s.substring(0, s.indexOf("."));;
		String s2=s.substring(s1.length()+1);;
		String s3=s1+s2;
		boolean b=s.contains(".");
		if(b) {
			System.out.println("bao");
		}else {
			System.out.println("n");
		}*/
		/*int len=s.length();
		if(len<16) {
			 while(len<16){
				 StringBuffer buffer=new StringBuffer();
				 buffer.append("").append(s);
				 s=buffer.toString();
				
			 }
			
		}*/
		 //System.out.println(s1);
		//String s2="COMMCNSHXXX115500722           交通银行股份有限公司                                        301290000007交通银行股份有限公司                                        BANK OF COMMUNICATIONS                                                ";
		/*System.out.println(s2.substring(31, 91).substring(0,s2.substring(31, 91).length()-12));
	System.out.println(s2.substring(31, 91).substring(s2.substring(31, 91).length()-12,s2.substring(31, 91).length()));
		//System.out.println(s2.substring(s2.substring(0, 91).length(),s2.length()));
		System.out.println(s2.substring(s2.substring(0, 91).length(),s2.length()).substring(0, s2.substring(s2.substring(0, 91).length(),s2.length()).length()-70));
		System.out.println(s2.substring(s2.substring(0, 91).length(),s2.length()).substring(s2.substring(s2.substring(0, 91).length(),s2.length()).length()-70, s2.substring(s2.substring(0, 91).length(),s2.length()).length()));
		*/
		//String reg = "[^\u4e00-\u9fa5]";
		//韩元(CNY/KRW)

				//s2 = s2.replaceAll(reg, " ");
		/*String s2="7.94810000";		
		String s1="7.94810000";	
		System.out.println(Double.toString((Double.parseDouble(s1)+Double.parseDouble(s2))/2));
		*/
	}
	
	/**
	 * 判断字符是不是中文字符
	 * @param c
	 * @return
	 */
	public static boolean isChinese(char c) {
	    int ascii = (int)c;
	    if(ascii >= 0 && ascii <= 255)
	        return false;
	    return true;
	}

	/**
	 * 获取字符串的字节长度,中文按2字节算
	 * @param chstring
	 * @return
	 */
	public static int length(String chstring) {
	    int length = 0;
	    if(null == chstring || chstring.equals("") || chstring.length() == 0)
	        return 0;
	    for(int i = 0; i < chstring.length(); i++) {
	        char c = chstring.charAt(i);
	        if(isChinese(c))
	            length += 2;
	        else
	            length += 1;
	    }
	    return length;
	}

	/**
	 * 字符串截取，支持中文
	 * @param chstring
	 * @param offset
	 * @param length
	 * @return
	 */
	public static String subChString(String chstring, int offset, int length) {
	    if(null == chstring || chstring.equals("") || chstring.length() == 0)
	        return chstring;
	    int num = 0;
	    int index = -1;
	    StringBuffer sb = new StringBuffer();
	    for(int i = 0; i < chstring.length(); i++) {
	        char c = chstring.charAt(i);
	        int move = 0;
	        if(isChinese(c))
	            move = 2;
	        else
	            move = 1;
	        index += move;
	        if(index >= offset) {
	            sb.append(c);
	            num += move;
	        }
	        if(num >= length)
	            break;
	    }
	    return sb.toString();
	}
	public static int len(String s) {
		int in=0;
		String ch="[\u0391-\uFFE5]";
		for(int i=0;i<s.length();i++) {
			String temp=s.substring(i, i+1);
			if(temp.matches(ch)) {
				in +=2;
			}else {
				in +=1;
			}
		}
		
		return in;
	}
	
	public static String subStr16Char(BigDecimal args) {
		String reslut = null;
		if(args != null) {
			//转换成字符串类型
			String args1 = String.valueOf(args);
			if(args1.indexOf(".")>0) {
				//截取小数点前的字符
				String args2 = args1.substring(0, args1.indexOf("."));
				//获取需要补齐的长度
				int args3 = 16-args1.length()+args2.length();
				//小数点前补齐后的数据
				String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
				//结果reslut	
				String s=args1.substring(args1.indexOf("."));;
				reslut = args4+s;
			}else{
				reslut = String.format("%016d", Integer.parseInt(args1));
			}
			
		}else {
			reslut = String.format("%016d", Integer.parseInt("0"));
		}
		return reslut;
	}
	
	public static  String fillZero(String num, int n,int m,boolean left,boolean right){
		String amt1=null;
		String amt2=null;
		String amt3=null;
		String fstr=null;
		String str=null;
		String result=null;
		 int rnum=0;		    
		int len=0;
		int zoreNum=0;
		if(num.contains("-")) {
			fstr=num.substring(0, 1);
			if(num.contains(".")) {
				 amt1=num.substring(0, num.indexOf("."));
				 amt3=amt1.substring(1, amt1.length());
				 amt2=num.substring(amt1.length()+1, num.length());
				 len=amt2.length();
				
				 
				 //if(len>m) {
					 zoreNum = n - amt3.length()-m-1;
				 /*}else {
					 zoreNum = n - amt3.length()-len-1;
				 }*/
				
			}else {
				amt3=num.substring(1, num.length());
				rnum=m;
				amt2="";
				zoreNum = n - amt3.length()-m-1;
			}
		  
		    if(n < 0){
		        throw new RuntimeException("补零后的长度不能为负数");
		    }
		    if(n < num.length()){
		        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
		    }
		    if(n == num.length()){
		        return num;
		    }
		   
		    if(m==len) {
		    	rnum=0;
		    }else if(m>len){
		    	rnum=m-len;
		    }else {
		    	str=amt2.substring(0,m);
		    	rnum=0;
		    	amt2=str;
		    }
		    StringBuilder sb1 = new StringBuilder("");
		    for(int i = 0; i < rnum; i++){
		        sb1.append("0");
		    }
		    if(right){
		    	 sb1.insert(0,amt2);
		      
		    }else{
		    	  sb1.append(amt2);
		    }
		    
		   
		    StringBuilder sb = new StringBuilder("");
		    for(int i = 0; i < zoreNum; i++){
		        sb.append("0");
		    }
		    if(left){
		        sb.append(amt3);
		    }else{
		        sb.insert(0,amt3);
		    }
		    result=fstr+sb.toString()+sb1.toString();
		}else {
			if(num.contains(".")) {
				 amt1=num.substring(0, num.indexOf("."));
				 
				 amt2=num.substring(amt1.length()+1, num.length());
				 len=amt2.length();
				  zoreNum = n - amt1.length()-m;
				
				  
				  /*if(len>m) {
						 zoreNum = n - amt1.length()-m;
					 }else {
						 zoreNum = n - amt1.length()-len;
					 }*/
			}else {
				amt1=num;
				rnum=m;
				amt2="";
				zoreNum = n - amt1.length()-m;
			}
		  
		    if(n < 0){
		        throw new RuntimeException("补零后的长度不能为负数");
		    }
		    if(n < num.length()){
		        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
		    }
		    if(n == num.length()){
		        return num;
		    }
		   
		    if(m==len) {
		    	rnum=0;
		    }else if(m>len){
		    	rnum=m-len;
		    }else {
		    	str=amt2.substring(0,m);
		    	rnum=0;
		    	amt2=str;
		    }
		    StringBuilder sb1 = new StringBuilder("");
		    for(int i = 0; i < rnum; i++){
		        sb1.append("0");
		    }
		    if(right){
		    	 sb1.insert(0,amt2);
		      
		    }else{
		    	  sb1.append(amt2);
		    }
		    
		  
		    StringBuilder sb = new StringBuilder("");
		    for(int i = 0; i < zoreNum; i++){
		        sb.append("0");
		    }
		    if(left){
		        sb.append(amt1);
		    }else{
		        sb.insert(0,amt1);
		    }
		    result=sb.toString()+sb1.toString();
		}
		
		
	    return result;
	 
	}
	
	 private static File getFile(String fileName)
			    throws EAIException
			  {
			    String filePath = null;
			    URL propertiesFileUrl = ClassLoader.getSystemResource(fileName);
			    if ((propertiesFileUrl != null) && (propertiesFileUrl.getPath() != null)) {
			      filePath = propertiesFileUrl.getPath();
			    } else {
			      String configDir = System.getProperty("config.dir");
			      configDir = configDir != null ? configDir : "config";
			      filePath = configDir + File.separator + fileName;
			    }

			    if (filePath == null) {
			      throw new EAIException(
			        "PROPRETY File Path 값이  존재하지 않습니다 .");
			    }
			    File file = new File(filePath);
			    if ((file == null) || (!file.exists())) {
			      throw new EAIException(
			        "PROPRETY File[" + filePath + "]이 존재하지 않습니다 .");
			    }
			    return file;
			  }
	public void sendLogin(String eaiDv, String intfId) {
		try {
			//String serviceUrl = "172.28.65.30";
			//最初发送系统代码
			String serviceUrl = "OPS";
			byte[] testData = makeLoginRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			System.out.println("eaiDv:--"+eaiDv);
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, testData, serviceUrl);
			System.out.println("返回信息："+new String(returnData, "UTF-8"));
			int len=(new String(returnData, "UTF-8")).length();
			String remsg=(new String(returnData, "UTF-8")).substring(len-220,len-218);
			System.out.println("remsg："+remsg);
			if(remsg.equals("EM")) {
				System.out.println("报文有误："+remsg);
			}else {
				String sess=(new String(returnData, "UTF-8")).substring(len-28,len-2);
				System.out.println("返回session："+sess);
			}
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private byte[] makeLoginRequstStdMsg(String intfId) throws StdTMsgException, Exception {
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		msg.setEaiIntfId(intfId);
		msg.setTrscSyncDvCd("S");
		msg.setStdTmsgPrgrNo("01");//转发系统节点代码
		msg.setRecvSvcCd("EIO1100");//接收服务代码
		msg.setExtnCrctNo("OPCS");//外部线路号
		Date date =new Date();
		SimpleDateFormat sdfdate = new SimpleDateFormat("yyyyMMdd");
		//SimpleDateFormat sdftime = new SimpleDateFormat("yyyyMMddhhmmss");
		SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
		String msgTypeCode=null;//标准报文数据种类代码
		String  msgTypeCodeLen;//标准报文数据长度
		int  msgTypeCodeLen1;//标准报文数据长度
		String sysName;//系统姓名
		String handleCode;//处理代码
		String channelType;//渠道类型
		String channelId;//渠道ID
		String gainInst;//获取机构
		String cdate;//日期
		String ctime;//时间
		String keyTrade;//KEY1, 交易
		String keyTradeNo;//KEY2 , 参考号
		String serveHandleCode;//服务处理代码
		
		/**柜员登录***/
		String tradeType;//交易类型
		String name;//用户名
		String pwd;//密码
		String dist;//区分
		String pwdLock;//密码锁定
		PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		msgTypeCode=stdtmsgSysValue.getString("msgTypeCode");
		msgTypeCodeLen=stdtmsgSysValue.getString("msgTypeCodeLen");
		msgTypeCodeLen1=stdtmsgSysValue.getInt("msgTypeCodeLen1");
		sysName=stdtmsgSysValue.getString("sysName");
		handleCode=stdtmsgSysValue.getString("handleCode");
		channelType=stdtmsgSysValue.getString("channelType");
		channelId=stdtmsgSysValue.getString("channelId");
		gainInst=stdtmsgSysValue.getString("gainInst");
		cdate=sdfdate.format(date);
		ctime=sdftimes.format(date);
		keyTrade=stdtmsgSysValue.getString("keyTrade");
		keyTradeNo=stdtmsgSysValue.getString("keyTradeNo");
		serveHandleCode=stdtmsgSysValue.getString("serveHandleCode");
		tradeType=stdtmsgSysValue.getString("tradeType");
		name=stdtmsgSysValue.getString("name");
		pwd=stdtmsgSysValue.getString("pwd");
		dist=stdtmsgSysValue.getString("dist");
		pwdLock=stdtmsgSysValue.getString("pwdLock");
		byte[] headerPacket = generator.makeHeader(msg);
		byte[] dataPacket=(msgTypeCode+msgTypeCodeLen+msgTypeCodeLen1+sysName+handleCode+channelType+channelId+gainInst+"                  "+cdate+ctime+keyTrade+keyTradeNo+"  "+serveHandleCode+tradeType+name+pwd+"       "+dist+pwdLock).getBytes();
		byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
		return totalPacket;
		

	}
	public void sendSychMsg(String eaiDv, String intfId) {

		try {
			String serviceUrl = "OPS";
			byte[] testData = makeSampleRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, testData, serviceUrl);
			System.out.println(new String(returnData, "UTF-8"));
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendAsynMsg(String eaiDv, String intfId) {

		try {
			String serviceUrl = "OPS";
			byte[] testData = makeSampleRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			httpCilent.sendAsychMsg(eaiDv, testData, serviceUrl);
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		}
	}

	public void sendHealthCheckMsg(String eaiDv) {

		try {
			String serviceUrl = "OPS";
			byte[] reqData = makeSampleReqHealthCheckMsg();
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			byte[] result = httpCilent.sendHealthCheckMsg(eaiDv, reqData, serviceUrl);
			System.out.println(">>> Health Checking:"+new String(result, "UTF-8"));
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
			eaiExp.getMessage();
			Throwable cause = eaiExp.getCause();
			if (cause != null) {
				cause.printStackTrace();
			}
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private byte[] makeSampleRequstStdMsg(String intfId) throws StdTMsgException {
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		msg.setEaiIntfId(intfId);
		byte[] headerPacket = generator.makeHeader(msg);
		byte[] dataPacket = "Dummy Data".getBytes();
		byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
		return totalPacket;

	}

	private byte[] makeSampleReqHealthCheckMsg() throws StdTMsgException {
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		new StdTMsgValidator().validateAll(msg);
		byte[] packet = generator.makeReqHealthCheckMsg(msg);
		return packet;
	}
	

}
