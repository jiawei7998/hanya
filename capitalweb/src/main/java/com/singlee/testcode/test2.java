package com.singlee.testcode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson.JSON;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.ifs.model.IfsIntfcIrevBean;

public class test2 {

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		/*Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());
		String fileName ="E:\\hnsftp\\zhongzhai\\"+sdate+"\\"+"zhongzhaiguzhi20230704.xlsx";
		 EasyExcel.read(fileName, getSecl.class, new DemoDataListener()).sheet().doRead();

	        // 一个文件一个reader
	        try (ExcelReader excelReader = EasyExcel.read(fileName, getSecl.class, new DemoDataListener()).build()) {
	            // 构建一个sheet 这里可以指定名字或者no
	            ReadSheet readSheet = EasyExcel.readSheet(1).build();
	            // 读取一个sheet
	            excelReader.read(readSheet);
	        }
		
		EasyExcel.read(fileName, getSecl.class, new PageReadListener<getSecl>(dataList -> {
            for (getSecl demoData : dataList) {
            	//System.out.println("读取到一条数据{}"+ JSON.toJSONString(demoData));
            	System.out.println("债券号:"+ demoData.getSecid());
            	
            }
            System.out.println("数量:"+ dataList.size());
        })).sheet().doRead();*/
		ImportLIrevByExcel();
	}
	 public static String ImportLIrevByExcel() throws Exception {
	        
		 
	        String ret = "导入失败";
	       
	        List<IfsIntfcIrevBean> irevBean = new ArrayList<>();
	        Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			//calendar.add(Calendar.DATE, -1);
			String sdate = null;
			//sdate = format.format(calendar.getTime());
			sdate = "20240131";
	        PropertiesConfiguration stdtmsgSysValue;
			stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
			String lsLocalPath=null;
			String name=null;
			name= stdtmsgSysValue.getString("MKTDATA_NEW");
			lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
			
			 InputStreamReader isr = null;
		     BufferedReader br = null;
		     String lstmntdte= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
		     
		     File[] list = new File(lsLocalPath).listFiles();
		     for(File file : list)
		       {
		    	 if(file.isFile())
		          {
		    		 if (file.getName().contains(name+sdate+".csv")) {
		    			//MKTDATA
		    			 File mkdata = new File(lsLocalPath + file.getName());
		    			 mkdata.setReadable(true);
		    			 mkdata.setWritable(true);
		    			 if (!mkdata.exists()) {
		    					//logManager.info("========" + mkdata + ":文件不存在=========");
		    		            
		    			}else {
		    				  try {
		    			           
		    					  isr = new InputStreamReader(new FileInputStream(mkdata), "UTF-8");
		    			          br = new BufferedReader(isr);
		    			          String line = "";
		    				    
		    			          String tradedate= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
		    			           
		    			          IfsIntfcIrevBean IfsIntfcIrevbean = null;
		    			          while ((line = br.readLine()) != null) {
		    			        	  String[] names = line.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
		 				            	
		    			            	
		    			            	if(names[0].equals("REUTERS MKT DATA") || names[0].equals("") || names[0].equals("Instrument ID")) {
		    			                	//logManager.info("========names:" + names + "=========");
		    			                }else {
		    			            	 Map<String, Object> map = new HashMap<String, Object>();
		    				        	 map.put("operid", names[2].trim());
		    				            
		    				             IfsIntfcIrevbean = new IfsIntfcIrevBean();                    
		    			                
		    			                 IfsIntfcIrevbean.setBr("01");
		    			               
		    			                String ccy=null;
		    			                String ccycn=null;
		    			                String ccyp=null;
		    			               
		    			                	
		    			                	if(names[0].trim().equals("CNY=SAEC") || names[0].trim().equals("EURCNY=SAEC") || names[0].trim().equals("HKDCNY=SAEC")|| names[0].trim().equals("JPYCNY=SAEC")|| names[0].trim().equals("GBPCNY=SAEC")|| names[0].trim().equals("CNYKRW=SAEC")|| names[0].trim().equals("CHFCNY=SAEC")|| names[0].trim().equals("CNYSEK=SAEC") || names[0].trim().equals("CADCNY=R") || names[0].trim().equals("AUDCNY=")) {
		    			                		 if (StringUtils.trimToEmpty(names[0].trim()).indexOf("KRW")>0||
		    					                         	StringUtils.trimToEmpty(names[0].trim()).indexOf("SEK")>0) {
		    					                         	//IfsIntfcIrevbean.setCcyp(StringUtils.trimToEmpty(sheet.getCell(0, i).getContents()).substring(0, 6));//C
		    					                         	ccy=StringUtils.trimToEmpty(names[0].trim()).substring(3, 6);         
		    					                    }else {
		    					                 	   ccy=StringUtils.trimToEmpty(names[0].trim()).substring(0, 3);         
		    					                    }
		    					                	 ccyp=StringUtils.trimToEmpty(names[0].trim()).substring(0, 6);
		    					                    
		    					                	 if(names[0].trim().equals("CNY=SAEC")) {
		    					                		 ccy="USD";
		    							                	ccycn="美元";
		    							                	ccyp="USDCNY";
		    					                	 }
		    					                     
		    					                     if(ccy.equals("HKD")) {
		    					                     	ccycn="港元";
		    					                     }else if(ccy.equals("JPY")) {
		    					                     	ccycn="日元";
		    					                     }else if(ccy.equals("GBP")) {
		    					                     	ccycn="英镑";
		    					                     }else if(ccy.equals("CAD")) {
		    					                     	ccycn="加拿大元";
		    					                     }else if(ccy.equals("CHF")) {
		    					                     	ccycn="瑞士法郎";
		    					                     }else if(ccy.equals("SEK")) {
		    					                     	ccycn="瑞典克朗";
		    					                     }else if(ccy.equals("AUD")) {
		    					                     	ccycn="澳元";
		    					                     }else if(ccy.equals("EUR")) {
		    					                     	ccycn="欧元";
		    					                     }else if(ccy.equals("KRW")) {
		    					                     	ccycn="韩元";
		    					                     }
		    					                IfsIntfcIrevbean.setCcycn(ccycn);
		    					                IfsIntfcIrevbean.setCcyp(ccyp);//C
		    					                IfsIntfcIrevbean.setCcy(ccy);
		    					             
		    					                String rate1=StringUtils.trimToEmpty(names[5].trim().replace("\"", "").replace(",", ""));
		    					                String rate2=StringUtils.trimToEmpty(names[6].trim().replace("\"", "").replace(",", ""));
		    					                //float rate=Float.parseFloat(rate1)+Float.parseFloat(rate2);
		    					                //String spotrate=Float.toString(rate/2);
		    					                String spotrate=Float.toString((Float.parseFloat(rate1)+Float.parseFloat(rate2))/2);
		    					                
		    					                BigDecimal num = new BigDecimal("100");
		    					                BigDecimal spotrate1;
		    					                if(ccy.equals("JPY")){
		    					                	 spotrate1 = (new BigDecimal(spotrate)).divide(num);
		    					                }else {
		    					                	spotrate1 = (new BigDecimal(spotrate));
		    					                }
		    					                IfsIntfcIrevbean.setSpotrate(spotrate1.toString());//D
		    					                IfsIntfcIrevbean.setFiletime(StringUtils.trimToEmpty(DateUtil.getCurrentTimeAsString()));
		    					                IfsIntfcIrevbean.setRemark("");
		    					                IfsIntfcIrevbean.setTradedate(StringUtils.trimToEmpty(tradedate));
		    					                
		    					                
		    					                irevBean.add(IfsIntfcIrevbean);
		    			                	}
		    			                	 //ret = ifsIntfcIrevService.doBussExcelPropertyToDataBase(irevBean);
		    			                }
		    			          }
		    			          
		    			           
		    			           

		    			        } catch (Exception e) {
		    			            e.printStackTrace();
		    			            throw new RException(e);
		    			        } 
		    			} 
		    		 }
		          }
		       }
			
	      
	        return ret;// 返回前台展示配置
	    }
}
