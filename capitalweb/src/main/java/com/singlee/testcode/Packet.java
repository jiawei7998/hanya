package com.singlee.testcode;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 

@SuppressWarnings({"rawtypes", "unchecked"})
public class Packet {
	protected Packet packet;
	
	private static final Logger logger = LoggerFactory.getLogger(Packet.class);
	
	
	List parts = new ArrayList();
	HashMap partMap = new HashMap();
	
	String id;
	String method;
	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void addPart(Part part) {
		parts.add(part);
		if (partMap.containsKey(part.getName())) {
			throw new RuntimeException("동일한 이름이 존재합니다 : ["+part.getName()+"]");
		}
		partMap.put(part.getName(), part);
	}
	
	public Part getPart(int index) {
		return (Part) parts.get(index);
	}
	
	public Part getPart(String name) {
		for (int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			if(name.equals(part.getName())) {
				return part;
			}
		}
		throw new RuntimeException("항목을 찾을 수 없습니다 : ["+name+"]");
	}
	
	
//	public String getPacket() {
//		StringBuffer s = new StringBuffer();
//		for(int i=0; i<parts.size(); i++) {
//			Part part = (Part) parts.get(i);
//			s.append(part.raw());
//		}
//		return s.toString();
//	}
	
	public void setCount(String name, int value) {
		setValue(name, ""+value);
	}
	
	public void setValue(String name, String value) {
		Part part = (Part) partMap.get(name);
		part.setValue(value);
	}
	
	public void setArrValue(String name, List<String> value) {
		Part part = (Part) partMap.get(name);
		part.setArrValue(value);
	}
	
	public void setArrValue(String name, String[] value) {
		Part part = (Part) partMap.get(name);
		
		List arrValue = new ArrayList();
		for(int i=0; i<value.length; i++) {
			arrValue.add(value[i]);
		}
		part.setArrValue(arrValue);
	}
	
	String _getLengthValue(String value, int length) {
		if(length > 0 && value.getBytes().length > length) {
			byte[] t = new byte[length];
			System.arraycopy(value.getBytes(), 0, t, 0, length);
			return new String(t);
		}
		return value;
	}
	
	public void addArrValue(String name, String value) {
		if(value==null){ value = "";}
		Part part = (Part) partMap.get(name);
		if(!"A".equals(part.getType())) {
			throw new RuntimeException("["+name+"] is not array part.");
		}
		List arrValue = part.getArrValue();
		arrValue.add(_getLengthValue(value, part.getLength()));
	}
	
	public String getValue(String name) {
		if (partMap.containsKey(name)) {
			Part part = (Part) partMap.get(name);
			String result = part.getValue();
			
			if(result==null) return "";
			return result.trim();
		}else {
			return "";
		}
	}	
	
	//byte array 처리를 위해 추가
	public byte[] getByteArrayValue(String name) {
		if (partMap.containsKey(name)) {
			Part part = (Part) partMap.get(name);
			byte[] result = part.getByteArrayValue();
			
			if(result==null) return result;
			return result;
		}else {
			return null;
		}
	}
		
	//byte array 처리를 위해 추가.
	public void setByteArrayValue(String name, byte[] byteArrayValue) {
		Part part = (Part) partMap.get(name);
		part.setByteArrayValue(byteArrayValue);
	}
	
	public List<String> getList(String name) {
		if (partMap.containsKey(name)) {
			Part part = (Part) partMap.get(name);
			return part.getArrValue();
		}else {
			return null;
		}
	}
	
	
	public int getIntValue(String name) {
		String s = getValue(name);
		return Integer.parseInt(s);
	}
	
	public int size() {
		int result = 0;
		for(int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			result += part.getLength();
		}
		return result;
	}
	
	public String setPartValue(byte[] data, Part part, int pos, boolean isArray) {
		byte[] t = new byte[part.getLength()];
		try {
			System.arraycopy(data, pos, t, 0, part.getLength());
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			System.err.println("전문데이터가 짧아서 파싱할 수 없음 : "+part.toString());
			throw new RuntimeException(e);
		}
		// String temp = new String(t);
		//default  UTF-8 
		String temp;
		
		 if("msgCtt".equals(part.getName())){    
			 try {
					temp = new String(t,"GBK");
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					temp = new String(t);
				}  
		 }else{
			 temp = new String(t);
		 }
		
		 
	 
		temp = Utils.nvl(temp, "").trim();
		if(isArray) {
			part.getArrValue().add(temp);
		}else {
			if (part.isEbcdic()) {
				try {
					if(part.isPack()) {
						part.setValue(""+PackedDecimal.parse(t));
					}else {
						part.setValue(new String(t, "CP933"));
					}
				}catch(Exception e) {
					logger.error("",e);
					part.setValue("");
				}
			} else {
				part.setValue(temp);			
			}
		}
		return temp;
	}
	
//	public void checkRequiredEven() {
//		for(int i=0; i<parts.size(); i++) {
//			Part part = (Part) parts.get(i);
//			if("".equals(PacketTools.nvl(part.getType(), "")) || "N".equals(part.getType())) {
//				if(part.getRequiredEven()!=null) {
//					String[] requiredEven = part.getRequiredEven();
//					boolean hasData = false;
//					for(int k=0; k<requiredEven.length; k++) {
//						hasData = !"".equals(PacketTools.nvl(getValue(requiredEven[k]), "").trim());
//						if(hasData) break;
//					}
//					if(!hasData) {
//						throw new RuntimeException("requiredEven check failed!");
//					}
//				}
//			}
//		}
//	}
	
	public byte[] parse(byte[] data) {
		int pos = 0;
		int count = 0;
		
		for(int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			/*
			 * normal type 'N' or Null
			 */
			if("".equals(PacketTools.nvl(part.getType(), "")) || "N".equals(part.getType())) {
				setPartValue(data, part, pos, false);
				pos += part.getLength();
				
				/*
				 * required check.
				 */
//				if (part.isRequired() && "".equals(PacketTools.nvl(part.getValue(), "").trim())) {
//					throw new RuntimeException("required item error, part :"+part.toString());
//				}
			}
			
			/*
			 * count type 'C'
			 */
			else if("C".equals(part.getType())) {
				count = Integer.parseInt(setPartValue(data, part, pos, false));
				pos += part.getLength();
			}			
			
			/*
			 * Array type 'A'
			 */
			else if ("A".equals(part.getType())) {
				int next = 0;
				while(count > 0) {
					if ((i+next) >= parts.size()) {
						count--;
						next = 0;
						continue;
					}
					
					Part arrPart = (Part) parts.get(i+ next);
					if(!"A".equals(arrPart.getType())) {
						count--;
						next = 0;
						continue;
					}
					
					setPartValue(data, arrPart, pos, true);
					pos += arrPart.getLength();
					next++;
				}
			}
		}
		
		byte[] rest = new byte[data.length-pos];
		System.arraycopy(data, pos, rest, 0, data.length-pos);
		return rest;
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		for(int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			s.append(part.toString());
			s.append("\n");
		}
		return s.toString();
	}
	
	private String getArrayRaw(int cnt) {
		StringBuffer s = new StringBuffer();
		for (int j=0; j<cnt; j++) {
			for(int i=0; i<parts.size(); i++) {
				Part part = (Part) parts.get(i);
				if(!"A".equals(part.getType())) {
					continue;
				}
				if(part.getArrValue().size()==0) {
					s.append(part.raw(""));
				}else {
					String value = (String) part.getArrValue().get(j);
					s.append(part.raw(value));
				}
			}
		}
		return s.toString();
	}
	
	public String raw() {
		StringBuffer s = new StringBuffer();
		
		int cnt = 0;
		boolean isArrayStart = false;
		for(int i=0; i<parts.size(); i++) {
			
			Part part = (Part) parts.get(i);
			if ("C".equals(part.getType())) {
				cnt = Integer.parseInt(part.getValue());
			}
			
			if (!isArrayStart && "A".equals(part.getType())) {
				isArrayStart = true;
				s.append(getArrayRaw(cnt));
			}
			
			if ("A".equals(part.getType())) {
				continue;
			}
			
			s.append(part.raw());
		}
		return s.toString();
	}
	
//	public String rawByDelimeter(String delimeter, boolean lastAppend) {
//		StringBuffer s = new StringBuffer();
//		for(int i=0; i<parts.size(); i++) {
//			Part part = (Part) parts.get(i);
//			s.append(part.raw());
//			if(i < parts.size()-1) {
//				s.append(delimeter);
//			}else if(lastAppend) { // last item.
//				s.append(delimeter);
//			}
//		}
//		return s.toString();
//	}

//	public void parseByDelimeter(String data, String delimeter) {
//		String[] elements = data.split(delimeter, -1);
//		if (elements.length < parts.size()) {
//			throw new RuntimeException("데이터와 파싱정보가 불일치 합니다 : ["+data+"]");
//		}
//		for(int i=0; i<parts.size(); i++) {
//			Part part = (Part) parts.get(i);
//			part.setValue(elements[i]);
//		}
//	}
	
	public void parseD(String data) {		
		String[] elements = data.split("\\x1A", -1);
		if (elements.length < parts.size()) {
			throw new RuntimeException("데이터와 파싱정보가 불일치 합니다 : ["+data+"]");
		}
		
		int count = 0;
		for(int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			
			/*
			 * normal type 'N' or Null
			 */
			if("".equals(PacketTools.nvl(part.getType(), "")) || "N".equals(part.getType())) {
				part.setValue(elements[i]);
				count = 0;
			}
			
			/*
			 * count type 'C'
			 */
			else if("C".equals(part.getType())) {
				String t = elements[i];
				if ("".equals(t)) {
					t = "0";
				}
				count = Integer.parseInt(t);
				part.setValue(elements[i]);
			}
			
			/*
			 * Array type 'A'
			 */
			else if ("A".equals(part.getType())) {
				List arrValue = new ArrayList();
				part.setArrValue(arrValue);
				for(int j=0; j<count; j++) {
					arrValue.add("");
				}
				String[] arr = elements[i].split("\\x1B", -1);
				for(int j=0; j<arr.length; j++) {
					arrValue.set(j, arr[j]);
				}
			}
		}
	}
	
	public String rawD() {
		return join((char)0x1A, (char)0x1B);
	}
	
	public String join(char c1, char c2) {
		StringBuffer result = new StringBuffer();
		for(int i=0; i<parts.size(); i++) {
			Part part = (Part) parts.get(i);
			if(part.getType()==null || "N".equals(part.getType())) {
					result.append(part.getValue());
			} else if("C".equals(part.getType())) {
					result.append(part.getValue());				
			} else if("A".equals(part.getType())) {
				List t = part.getArrValue();
				for(int j=0; j<t.size(); j++) {
					String repeat = (String) t.get(j);
					result.append(repeat);
					if(j<t.size()-1) {
						result.append(c2);
					}
				}
			}
			if(i<parts.size()-1) {
				result.append(c1);
			}
		}
		return result.toString();
	}

	public List getParts() {
		return parts;
	}
	
}
