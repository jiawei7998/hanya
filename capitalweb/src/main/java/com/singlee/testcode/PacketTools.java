package com.singlee.testcode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;

public class PacketTools {
	
	public static void errorLog(Logger log, Exception e) {
		StackTraceElement[] ste = e.getStackTrace();
		String className = ste[0].getClassName();
		String methodName = ste[0].getMethodName();
		int lineNumber = ste[0].getLineNumber();
		String fileName = ste[0].getFileName();
		log.error("Exception : " + e.getMessage());
		log.error(className + "." + methodName + " " + fileName + " "
				+ lineNumber + " line ...");
	}

	public static String nvl(String src, String def) {
		if (src == null || "".equals(src)) {
			return def;
		} else {
			return src;
		}
	}

	public static String RPAD(String str, int len, char pad) {
		return PAD(str, len, pad, false);
	}

	public static String LPAD(String str, int len, char pad) {
		return PAD(str, len, pad, true);
	}

	public static String PAD(String str, int len, char pad, boolean left) {
		if (str == null)
			str = "";
		if (str.getBytes().length >= len) {
			return new String(str.getBytes(), 0, str.getBytes().length);
		}
		String result = str;
		StringBuffer sb = new StringBuffer();
		int templen = len - result.getBytes().length;

		for (int i = 0; i < templen; i++) {
			sb.append(pad);
		}
		if (left) {
			return sb.toString() + result;
		} else {
			return result + sb.toString();
		}
	} // end of RPAD();

	public static String getCurrentDate(String fmt) {
		SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.format(new Date());
	}

//	public static void makeFile(File f, String data) throws Exception {
//		File parentDir = new File(f.getParent());
//		if (!parentDir.exists()) {
//			parentDir.mkdirs();
//		}
//		FileWriter fw = null;
//		
//		try {
//		     fw = new FileWriter(f); 
//		     fw.write(data);
//		     fw.close();
//		} catch(Exception e) {
//			throw new Exception(e);
//		} finally {
//			if (fw != null) try { fw.close(); } catch(Exception e) {}
//		}
//	}
//
//	public static void makeFile(File f, String data, String fileType)
//			throws Exception {
//		File parentDir = new File(f.getParent());
//		if (!parentDir.exists()) {
//			parentDir.mkdirs();
//		}
//
//		Writer out = null;
//		
//		try {
//		     out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), fileType)); // UTF8, UTF16,,
//		     out.write(data);
//		     out.close();
//		} catch(Exception e) {
//			throw new Exception(e);
//		} finally {
//			if (out != null) try { out.close(); } catch(Exception e) {}
//		}
//		     
//	}

	public static void copyFile(File in, File out) throws IOException {
		FileChannel inChannel = null;
		FileChannel outChannel = null;
		try {
			 inChannel = new FileInputStream(in).getChannel();
			 outChannel = new FileOutputStream(out).getChannel();
					
			 inChannel.transferTo(0, inChannel.size(), outChannel);
		} catch (IOException e) {
			throw e;
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

	public static String toHalfChar(String src) {
		StringBuffer strBuf = new StringBuffer();
		char c = 0;
		int nSrcLength = src.length();

		for (int i = 0; i < nSrcLength; i++) {
			c = src.charAt(i);
			// 영문이거나 특수 문자 일경우.
			if (c >= '！' && c <= '～') {
				c -= 0xfee0;
			} else if (c == '　') {
				c = 0x20;
			}
			// 문자열 버퍼에 변환된 문자를 쌓는다
			strBuf.append(c);
		}
		return strBuf.toString();
	}

	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		String aResult = result.toString();
		byte[] src = aResult.getBytes();
		if (src.length > 2000) {
			byte[] t = new byte[2000];
			System.arraycopy(src, 0, t, 0, 2000);
			return new String(t);
		} else {
			return aResult;
		}
	}

	public static String getAgoDate(String date, int ago) {
		if (date == null || "".equals(date))
			return date;
		if (date.length() < 8)
			return date;
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(year), Integer.parseInt(month) - 1,
				Integer.parseInt(day));
		c.add(Calendar.DAY_OF_MONTH, -ago);
		Date agoDate = c.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(agoDate);
	}
	
//	public static String getGlobalId() {
//		String yyyymmdd = PacketTools.getCurrentDate("yyyyMMdd");
//		String dbmsType = "shlret21";
//		String systemCd = "WA";
//		String hhmm = PacketTools.getCurrentDate("hhmm");
//		String seq = PacketTools.LPAD(""+new java.util.Random().nextInt(100000000), 8, '0');
//		String progress = "01";
//		return yyyymmdd + dbmsType + systemCd + hhmm + seq + progress;
//	}
}
