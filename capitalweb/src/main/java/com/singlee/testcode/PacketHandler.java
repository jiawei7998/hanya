package com.singlee.testcode;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.hanafn.eai.client.stdtmsg.StdTMsg;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;

import com.hanafn.eai.client.stdtmsg.util.StdTMsgException;
import com.hanafn.eai.client.stdtmsg.util.StdTMsgGlobId;
 

public class PacketHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(PacketHandler.class);
	public static final String[] noNeedChoisBiz = new String[] {
		"Sgib0704a001In",
	};
	String tlgmId;
	String subPrcsCd;
	int retryCount;
	
	String opno;             //电子柜员
	boolean reLoginFlag ;    //重新登录标示
	/*
	 * Input
	 */
	Packet inSysCmnpt;		// GLI Header
	Packet inSysBizHead;	// GLI Header
	Packet inBizCmnpt;		// Chois Header (초이스 공통전문)
	Packet inBizChois;		// Chois Header (초이스 공통전문 - 초이스로그인내역, 포함안되는 경우도 있다)
	Packet inData;
	
	/*
	 * Output
	 */
	Packet outSysCmnpt;		// GLI Header
	Packet outSysBizHead;	// GLI Header
	Packet outSysMessage;	// GLI Header (When error)
	Packet outBizCmnpt;		// Chois Header (초이스 공통전문)
	Packet outData;
	
	ArrayList<String> noNeedBizList;	// 초이스 공통전문 중 로그인 내역 필요없는 서비스목록
	
	/**
	 * PacketHandler
	 * 
	 * @param tlgmId XML 전문아이디
	 * @param subPrcsCd 서비스코드
	 * @throws BusinessException
	 */
	public PacketHandler(String tlgmId, String subPrcsCd) throws  Exception {
		this.tlgmId = tlgmId;
		this.subPrcsCd = subPrcsCd;
		this.retryCount = 0;
		
		//initPacket();
	}
	
 
	
	public PacketHandler(String subPrcsCd) throws Exception {
		//String svcId = getSystemCommonDTO().getSvcId();
		this.tlgmId = "Sgib"+subPrcsCd.toLowerCase();
		this.subPrcsCd = subPrcsCd;
		this.retryCount = 0;
		
		//initPacket();
	}
	
	/*
	 * 대외거래중 XML파일이 없는 경우
	 */
	/*public PacketHandler(int num) throws  BusinessException {
		
		this.subPrcsCd = "EAI" + num;		
		this.retryCount = 0;
		
		try {
			 XMLParser sysHeader = XMLParser.create("SysCmnpt.xml");
			 XMLParser bizHeader = XMLParser.create("BizCmnpt.xml");			 
			
			 
			  * Create In Packet
			  
			 this.inSysCmnpt = sysHeader.createPacket("SysCmnpt");
			 this.inSysBizHead = sysHeader.createPacket("SysBizHead");
			 this.inBizCmnpt = bizHeader.createPacket("BizCmnpt");
			 this.inBizChois = bizHeader.createPacket("BizChois");
			 			
			 
			  * Create Out Packet
			  
			 this.outSysCmnpt = sysHeader.createPacket("SysCmnpt");
			 this.outSysBizHead = sysHeader.createPacket("SysBizHead");
			 this.outSysMessage = sysHeader.createPacket("SysMessage");
			 this.outBizCmnpt = bizHeader.createPacket("BizCmnpt");		
					
		} catch(Exception e) {
			throw new BusinessException(
	    			"PacketHandler", 
	    			ErrorSeverity.ERROR, e.getMessage(), e);
		}
	}*/
	
	/*private void initPacket() throws BusinessException {
		 
		 * Chois Header: BizChois 
		 
		this.noNeedBizList = new ArrayList<String>();
		for(String _biz: noNeedChoisBiz) {
			noNeedBizList.add(_biz);
		}
		
		try {
			XMLParser sysHeader = XMLParser.create("SysCmnpt.xml");
			XMLParser bizHeader = XMLParser.create("BizCmnpt.xml");
			XMLParser data = XMLParser.create(tlgmId+".xml");
			
			
			 * Create In Packet
			 
			this.inSysCmnpt = sysHeader.createPacket("SysCmnpt");
			this.inSysBizHead = sysHeader.createPacket("SysBizHead");
			this.inBizCmnpt = bizHeader.createPacket("BizCmnpt");
			this.inBizChois = bizHeader.createPacket("BizChois");
			this.inData = data.createPacket(tlgmId+"In");
			
			
			 * Create Out Packet
			 
			this.outSysCmnpt = sysHeader.createPacket("SysCmnpt");
			this.outSysBizHead = sysHeader.createPacket("SysBizHead");
			this.outSysMessage = sysHeader.createPacket("SysMessage");
			this.outBizCmnpt = bizHeader.createPacket("BizCmnpt");
			this.outData = data.createPacket(tlgmId+"Out");
			
		
		}catch(Exception e) {
			throw new BusinessException(
	    			"PacketHandler", 
	    			ErrorSeverity.ERROR, e.getMessage(), e);
		}
	}*/
	
	
	/**
	 * 초이스로 송신
	 * @throws BusinessException
	 */
	public void send()  {
		try {
			long  trscUnqNo = 0L;
			/*
			 * 전문일련번호 기록
			 */
			inBizCmnpt.setValue("sysTrcrAdtNo", Utils.zerofill(trscUnqNo, 6));
			inBizCmnpt.setValue("rtvlRefNo", Utils.zerofill(trscUnqNo, 12));
			 /*
			 * 송신전문 생성 (초이스 공통헤더 + 입력데이터)
			 */
			 byte[] in = makeInputData();  // commBiz+choisBiz
			/*
			 * 거래내역로그를 위한 서비스코드등록
			 */
			
			/*
			 * GLI 헤더작성 시작
			 */
		    StdTMsgGenerator generator = new StdTMsgGenerator();
			StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
			msg.setProcRsltDvCd("0");
			msg.setEaiIntfId(  "CMS_CHS_DSS00001" );
			msg.setRecvSvcCd("EIO1100");  						// 수신서비스코드  ,인터페이스 아이디 ,
			msg.setTrscSyncDvCd("S");
			msg.setChnlTypCd("CMS");                   			// 채넝부분에 신청 
			StdTMsgGlobId globid = StdTMsgGlobId.generate();  	// 글로벌 아이디 자동생성 샘플
			msg.setGlobId(globid.toString());
			//msg.setExtnCrctNo("GIB "+getSgibCommonDTO().getBizCd()+"   ");
			msg.setLnggDvCd("zh");								//???????????	
			/*
			 * 최초 10바이트 설정
			 */
			StringBuffer sb = new StringBuffer();

			sb.append("10");   							// tmsgDatKindCd  2자리 "10" 표준전문데이터종류코드			
			sb.append(Utils.zerofill(in.length, 8));   	// tmsgDatLen 8자리 표준전문데이터길이
			/* 
			 * 최종 전문 
			 */
			byte[] totalPacket = generator.makeWholeMsg(new byte[][] {
					generator.makeHeader(msg), 
					sb.toString().getBytes(),
					in});	 
			/*
			 * Gli 헤더파싱
			 */
			 inSysBizHead.parse(inSysCmnpt.parse(totalPacket));
			/*
			 * 송신전문 로그출력
			 */
			  printInPacket(trscUnqNo);
			
			/*
			 * 전문 송신
			 */
			long startTime = System.currentTimeMillis();
	 
		 
			// byte[] out = EAIHttpAdapterSample.sendSychMsg(totalPacket);;
	 
			logger.info("Total elapsed time:[{}ms]", System.currentTimeMillis()-startTime );
			 
			/*
			 * 수신전문 파싱
			 */
			//out = outSysCmnpt.parse(out);
			//out = outSysBizHead.parse(out);
			logger.info("procRsltDvCd=",outSysCmnpt.getValue("procRsltDvCd"));
			if (!"0".equals(outSysCmnpt.getValue("procRsltDvCd"))) {
		    	/* 
		    	 * GLI 또는 Chois 응답오류
		    	 */
				if ("EM".equals(outSysBizHead.getValue("tmsgDatKindCd"))) {	
				//	out = outSysMessage.parse(out);
					logger.error("Error <<SysCmnpt>>\n{}", outSysCmnpt.toString());
					logger.error("Error <<SysMessage>>\n{}", outSysMessage.toString());
			    	String rspsMsgCtt = outSysMessage.getValue("rspsMsgCtt").trim();
			    	String msgCtt = outSysMessage.getValue("msgCtt").trim();
			    	
			    	String errorCode = Utils.nvl(outSysCmnpt.getValue("stdTmsgErrCd"), "");
			    	logger.error("errorCode:[{}]", errorCode);
			    	if("".equals(errorCode)||errorCode == null) {
			    		errorCode = outSysMessage.getValue("msgCd").trim();
			    	}
			    	
		    	}
		    }
			//out = outBizCmnpt.parse(out);
			//out = outData.parse(out);
			/*
			 * 수신전문 출력
			 */
			printOutPacket(trscUnqNo);
			

		}catch(Exception e) {
			try {
				throw new Exception(
						 e);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	/**
	 * GLI 헤더를 제외한 송신전문 생성
	 * inBizChois는 요청시마다 조회하여 값을 세팅함
	 * 
	 * @return 송신전문 
	 * @throws BusinessException
	 */
	private byte[] makeInputData()  {
		
		setOpNo();
		
		StringBuffer result = new StringBuffer();
		/*
		 * Chois Header: BizCmnpt
		 */
		Date trscDt = null;
		if(trscDt == null) {
			trscDt = new Date();
		}
		inBizCmnpt.setValue("subPrcsCd", this.subPrcsCd);
		inBizCmnpt.setValue("swcId", "GIBI");
		inBizCmnpt.setValue("prcsCd", "PT1100");
		inBizCmnpt.setValue("tmlTp", "GIBK");	
		inBizCmnpt.setValue("tmlId", "90000001");
		inBizCmnpt.setValue("acqInst", "0484");					
		
		inBizCmnpt.setValue("trscDt", Utils.yyyyMMdd.format(trscDt));
		inBizCmnpt.setValue("trscTm", Utils.HHmmss.format(new Date()));	
		inBizCmnpt.setValue("rspsCd", "  " );
 
		result.append(inBizCmnpt.raw());
		
		//String mainNode = "120";            //INB+MainNode+[0-9] 或其他电子柜员号
		//String opno = getLogonNo(mainNode); // 登陆电子柜员   ????? 电子柜员号获取还需要考虑下。
		//opno = "CMS1200";
		//
		// 从登陆控制队列中查看是否已经登陆，如果为null或者为0，则表示未登陆过或者还需要重新登陆
	 	
	
	 if(!noNeedBizList.contains(this.tlgmId+"In")) { 
		  // UsrSessService	usrSessService =(UsrSessService) getCtx().getBean("usrSessService");
		// UsrSess usrSess = usrSessService.getUsrSessByOpno(opno);
		/* if(usrSess == null){
			 usrSess = getNewUsrSess(opno);
		 }*/
		inBizChois.setValue("thid",	 "");
		inBizChois.setValue("ibil",	"");
		inBizChois.setValue("ssid",	 "");
		//inBizChois.setValue("ssid",	 "I3");
		inBizChois.setValue("ssil",	 "");
		inBizChois.setValue("thgb",  "P");
		inBizChois.setValue("type",  "T");  //T验证session  M不验证
		inBizChois.setValue("jumCd", "");
		inBizChois.setValue("cjum", "");
		inBizChois.setValue("ajum", "");
		inBizChois.setValue("sjum",  "");
		inBizChois.setValue("opno",  opno);
		inBizChois.setValue("opgb",  "");
		inBizChois.setValue("tell", "");
		inBizChois.setValue("ccy",   "");
		inBizChois.setValue("sess",  "");
	    inBizChois.setValue("sessChk", "Y"); 
	 
		 	/*inBizChois.setValue("thid",	 "");
			inBizChois.setValue("ibil",	 Utils.yyyyMMdd.format(trscDt));
			inBizChois.setValue("ssid",	 "I3");
			inBizChois.setValue("ssil",	 Utils.yyyyMMdd.format(trscDt));
			inBizChois.setValue("thgb",  "P");
			inBizChois.setValue("type",  "M");  //T验证session  M不验证
			inBizChois.setValue("jumCd", "00");
			inBizChois.setValue("cjum",  "210");
			inBizChois.setValue("ajum", "210");
			inBizChois.setValue("sjum",  "210");
			inBizChois.setValue("opno",  opno);
			inBizChois.setValue("opgb",  "9");
			inBizChois.setValue("tell", "998");
			inBizChois.setValue("ccy",   "RMB");
			inBizChois.setValue("sess",  "");
		    inBizChois.setValue("sessChk", "Y");  */
		 
		
		result.append(inBizChois.raw());
		} 
		result.append(inData.raw());
		return result.toString().getBytes();
	}
	//get common information  
	/*public static UsrSess getNewUsrSess(String opno) throws BusinessException {
		try {
			PacketHandler ph = new PacketHandler("Sgib0704a001", "0704A01");
			Packet in = ph.getInData();
			in.setValue("opGb", "1");
			in.setValue("userId", opno);
			in.setValue("password", "1");
			in.setValue("rejectLogin", "Y");
			in.setValue("passwdLock", "0");
			ph.send();
			Packet out = ph.getOutData();
			UsrSess usrSess = new UsrSess();
			usrSess.setOpno(opno);
			usrSess.setCjum(out.getValue("cjum"));
			usrSess.setAjum(out.getValue("ajum"));
			usrSess.setSjum(out.getValue("sjum"));
			usrSess.setTell(out.getValue("tell"));
			usrSess.setOpgb(out.getValue("opGb"));
			usrSess.setCcy(out.getValue("ccy"));
			usrSess.setJumcd(out.getValue("jumCd"));
			usrSess.setThid(out.getValue("thid"));
			usrSess.setIbil(out.getValue("ibil"));
			usrSess.setSsid(out.getValue("ssid"));
			usrSess.setSsil(out.getValue("ssil"));
			usrSess.setBra1(out.getValue("bra1"));
			usrSess.setBrtn(out.getValue("brtn"));
			usrSess.setBrfn(out.getValue("brfn"));
			usrSess.setSfcd(out.getValue("sfcd"));
			usrSess.setSess(out.getValue("sess"));
			UsrSessService usrSessService  = (UsrSessService) getCtx().getBean("usrSessService");
			usrSessService.insertUsrSess(usrSess);
			return usrSess;

		}catch(Exception e) {
			
			logger.error("", e);
			
			throw new BusinessException(
	    			"PacketHandler", 
	    			ErrorSeverity.ERROR, e.getMessage(), e);
		}
	}
	*/
	private void printInPacket(long seq) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("<<inSysCmnpt>>\n");sb.append(inSysCmnpt.toString());
		sb.append("<<inSysBizHead>>\n");sb.append(inSysBizHead.toString());
		sb.append("<<inBizCmnpt>>\n");sb.append(inBizCmnpt.toString());
		sb.append("<<inBizChois>>\n");sb.append(inBizChois.toString());
		sb.append("<<inData>>\n");sb.append(inData.toString());
		
		StringBuffer result = new StringBuffer();
		String[] lines = sb.toString().trim().split("\\n", -1);
		for(String line: lines) {
			result.append("["+seq+"] -> "+line).append("\n");
		}
		logger.info(result.toString().trim());
	}
	
	private void printOutPacket(long seq) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("<<outSysCmnpt>>\n");sb.append(outSysCmnpt.toString());
		sb.append("<<outSysBizHead>>\n");sb.append(outSysBizHead.toString());
		sb.append("<<outSysMessage>>\n");sb.append(outSysMessage.toString());
		sb.append("<<outBizCmnpt>>\n");sb.append(outBizCmnpt.toString());
		sb.append("<<outData>>\n");sb.append(outData.toString());
		
		StringBuffer result = new StringBuffer();
		String[] lines = sb.toString().trim().split("\\n", -1);
		for(String line: lines) {
			result.append("["+seq+"] -> "+line).append("\n");
		}
		logger.info(result.toString().trim());
	}
	
	/**
	 * <b>功能说明</br> 获取电子柜员号
	 * 
	 * @param ctx
	 * @return 电子柜员号
	 */
	private String getLogonNo(String accountMainNode) {
		String logon = "INB"; // 电子柜员默认前缀
		String mainNode = accountMainNode;
		if ((mainNode == null || "".equals(mainNode))) {
			mainNode = "210"+String.valueOf((int)Math.floor((Math.random()*10)/2)+5); // 交易账号
		}else{
			mainNode = mainNode + String.valueOf((int)Math.floor((Math.random()*10)/2)+5);
		}
		logon = logon + mainNode;
		return logon;
	}

	  private static WebApplicationContext getCtx(){
			WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
			ServletContext servletContext = webApplicationContext.getServletContext();
			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
			return ctx;
	  }

	private void setOpNo(){
		if(!reLoginFlag){
			this.opno = getLogonNo(inBizChois.getValue("cjum"));	
		} 
	}
	
	public String getTlgmId() {
		return tlgmId;
	}

	public void setTlgmId(String tlgmId) {
		this.tlgmId = tlgmId;
	}

	public String getSubPrcsCd() {
		return subPrcsCd;
	}

	public void setSubPrcsCd(String subPrcsCd) {
		this.subPrcsCd = subPrcsCd;
	}

	public Packet getInSysCmnpt() {
		return inSysCmnpt;
	}

	public void setInSysCmnpt(Packet inSysCmnpt) {
		this.inSysCmnpt = inSysCmnpt;
	}

	public Packet getInSysBizHead() {
		return inSysBizHead;
	}

	public void setInSysBizHead(Packet inSysBizHead) {
		this.inSysBizHead = inSysBizHead;
	}

	public Packet getInBizCmnpt() {
		return inBizCmnpt;
	}

	public void setInBizCmnpt(Packet inBizCmnpt) {
		this.inBizCmnpt = inBizCmnpt;
	}

	public Packet getInBizChois() {
		return inBizChois;
	}

	public void setInBizChois(Packet inBizChois) {
		this.inBizChois = inBizChois;
	}

	public Packet getInData() {
		return inData;
	}

	public void setInData(Packet inData) {
		this.inData = inData;
	}

	public Packet getOutSysCmnpt() {
		return outSysCmnpt;
	}

	public void setOutSysCmnpt(Packet outSysCmnpt) {
		this.outSysCmnpt = outSysCmnpt;
	}

	public Packet getOutSysBizHead() {
		return outSysBizHead;
	}

	public void setOutSysBizHead(Packet outSysBizHead) {
		this.outSysBizHead = outSysBizHead;
	}

	public Packet getOutSysMessage() {
		return outSysMessage;
	}

	public void setOutSysMessage(Packet outSysMessage) {
		this.outSysMessage = outSysMessage;
	}

	public Packet getOutBizCmnpt() {
		return outBizCmnpt;
	}

	public void setOutBizCmnpt(Packet outBizCmnpt) {
		this.outBizCmnpt = outBizCmnpt;
	}

	public Packet getOutData() {
		return outData;
	}

	public void setOutData(Packet outData) {
		this.outData = outData;
	}
	
	
	
}
