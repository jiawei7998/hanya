package com.singlee.testcode;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.jar.Attributes;



import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.GetMapping;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.Order;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.singlee.capital.chois.util.LutouUtil;
import com.singlee.capital.chois.util.SubStrChinese;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.PropertiesUtil;

import jcifs.UniAddress;
import jcifs.smb.NtlmAuthenticator;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbSession;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import net.sf.jxls.transformer.Row;


public class test1 {
	static SubStrChinese subStrChinese=new SubStrChinese();
	 public static final int BLOCK_SIZE = 1000;
	@SuppressWarnings("resource")
	public static void main(String[] args) throws UnsupportedEncodingException, ParseException {
		
		//ReadMethod();
		
		/*Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//calendar.add(Calendar.DATE, -1);
		String sdate = null;
		sdate = format.format(calendar.getTime());//readCsvByBufferedReader("C:\\Users\\hspcadmin\\Desktop\\韩亚银行OPICS升级\\二期文档\\市场数据新模板\\HANA_IntradaySample.csv");
		LutouUtil lutouUtil=new LutouUtil();
		lutouUtil.sftp("BONDDATA_NEW "+sdate+".csv");*/
	//String s="10d";
	//System.out.println(s.toUpperCase());
		/*String s="-00000050000";
		int integer=Integer.valueOf(s);
		float i=(float)integer/1000000;
		String str=String.valueOf(i);
		System.out.println(str);*/
		//smbGet();
		/* String url = "smb://172.28.3.9/Scan/路透/收益率_曲线_汇率/20230620/";
	        String userName = "080";
	        String password = "bnm";
	        String domain = null;
	        NtlmPasswordAuthentication auth = 
	              new NtlmPasswordAuthentication(null, userName, password);
	        try {
	            doRecursiveLookup(new SmbFile(url, auth));
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        }*/
		/* try {
			SmbFile sourceFile = new SmbFile("smb://" + "080" + ":" + "bnm" + "@" + "\\172.28.3.9\\Scan\\路透\\收益率_曲线_汇率\\20230620" + "收益率曲线20230620.xlsx");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//String s='"'+"weqwqq"+'"';
		//System.out.println(s.replace("\"", "" ));
		
	/*	try {
			
			File file=new File("E:\\hnsftp\\zhongzhai\\zhongzhaiguzhi20230704.xlsx");
			 InputStream is = new FileInputStream(file);
			XSSFWorkbook wb;
			wb = new XSSFWorkbook(is);
			XSSFSheet sheet=wb.getSheetAt(0);
			int row=sheet.getLastRowNum();
			String mid=sheet.getRow(2).getCell(0).toString();
			
			System.out.println("row-----"+mid);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		/*for (int i = 0; i < 100; i++) {
			  
			 try {
				processOneSheet("E:\\\\hnsftp\\\\zhongzhai\\\\债券估值20230627.xlsx");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		*/
		
		//read("E:\\hnsftp\\zhongzhai\\债券估值20230620.xlsx");
		
	}
	
    public static void read(String filePath){
        String file = filePath;
        UserLisener<getSecl> userUserLisener = new UserLisener<>();
        EasyExcel.read(file, getSecl.class,userUserLisener).sheet("债券估值").doRead();
        List<getSecl> datas = userUserLisener.getDatas();
        System.out.println(datas);
    }
   
	public static void processOneSheet(String filename) throws Exception {
		  OPCPackage pkg = OPCPackage.open(filename);
		  XSSFReader r = new XSSFReader(pkg);
		  SharedStringsTable sst = r.getSharedStringsTable();
		 
		  XMLReader parser = fetchSheetParser(sst);
		  InputStream sheet2 = r.getSheet("rId1");
		  InputSource sheetSource = new InputSource(sheet2);
		  parser.parse(sheetSource);
		  sheet2.close();
		 }
		 
		 public static XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
		 // XMLReader parser = XMLReaderFactory.createXMLReader();
			 //XMLReader parser = XMLReaderFactory.createXMLReader("javax.xml.parsers.SAXParser");
		  XMLReader parser = XMLReaderFactory.createXMLReader("com.sun.org.apache.xerces.internal.parsers.SAXParser");
		  ContentHandler handler = new SheetHandler(sst);
		  parser.setContentHandler(handler);
		  return parser;
		 }
		 
		 private static class SheetHandler extends DefaultHandler {
		  private SharedStringsTable sst;
		  private String lastContents;
		  private boolean nextIsString;
		 
		  private SheetHandler(SharedStringsTable sst) {
		   this.sst = sst;
		  }
		 
		  public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		   if (name.equals("c")) {
		    System.out.print(attributes.getValue("r") + " - ");
		    String cellType = attributes.getValue("t");
		    if (cellType != null && cellType.equals("s")) {
		     nextIsString = true;
		    } else {
		     nextIsString = false;
		    }
		   }
		   lastContents = "";
		  }
		 
		  public void endElement(String uri, String localName, String name) throws SAXException {
		   if (nextIsString) {
		    int idx = Integer.parseInt(lastContents);
		    lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
		    nextIsString = false;
		   }
		 
		   if (name.equals("v")) {
		    System.out.println(lastContents);
		   }
		  }
		 
		  public void characters(char[] ch, int start, int length) throws SAXException {
		   lastContents += new String(ch, start, length);
		  }
		 }
	public static void doRecursiveLookup(SmbFile smb) {
        try {
            if (smb.isDirectory()) {
                System.out.println(smb.getName());
                for (SmbFile f : smb.listFiles()) {
                    if (f.isDirectory()) {
                        doRecursiveLookup(f);
                    } else {
                        System.out.println("\t:" + f.getName());
                    }
                }
            } else {
                System.out.println("\t:" + smb.getName());
            }
        } catch (SmbException e) {
            e.printStackTrace();
        }
    }
	public static String split(String s) {
		String result=null;
		for(int i=0;i<s.length();i++) {
			//String ss=s.charAt(i);
			if(String.valueOf(s.charAt(i))!="") {
				result+=s.charAt(i);
				
			}
		}
		return result;
	}
	
	public static ArrayList<String> readCsvByBufferedReader(String filePath) {
        File csv = new File(filePath);
        csv.setReadable(true);
        csv.setWritable(true);
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            isr = new InputStreamReader(new FileInputStream(csv), "UTF-8");
            br = new BufferedReader(isr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String line = "";
        ArrayList<String> records = new ArrayList<>();
        try {
            while ((line = br.readLine()) != null) {
            	String[] names = line.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
            	
                System.out.println(line);
                System.out.println("names[0]:"+names[0].trim());
                System.out.println("names[1]:"+names[1].trim());
                System.out.println("names[2]:"+names[2].trim());
                System.out.println("names[7]:"+names[7].trim());
                System.out.println("names[18]:"+names[18].trim());
                System.out.println("names[14]:"+names[14].trim());
                records.add(line);
            }
            System.out.println("csv表格读取行数：" + records.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return records;
    }

	 public static  void smbGet() {
	    	
	    	// 输入流
	        InputStream in = null;
	        // 输出流
	        OutputStream out = null;

	        try {
	        	 String sylyurl;
	        	 String	sylna;
	        	 String sylpwd;
	        	 String	sylpath;
	        	 String	sylLocalPath;
	        	 String	sylname;
	        	String lstmntdte= DateUtil.getCurrentDateAsString("yyyyMMdd");
	        	PropertiesConfiguration stdtmsgSysValue;
	    		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
	    		sylyurl= stdtmsgSysValue.getString("sylyurl");
	    		sylna= stdtmsgSysValue.getString("sylna");
	    		sylpwd= stdtmsgSysValue.getString("sylpwd");
	    		sylpath= stdtmsgSysValue.getString("sylpath");
	    		sylLocalPath= stdtmsgSysValue.getString("sylLocalPath");
	    		//sylname=stdtmsgSysValue.getString("sylname")+lstmntdte+".xlsx";
	    		 NtlmPasswordAuthentication auth = 
	   	              new NtlmPasswordAuthentication(null, sylna, sylpwd);
	            // 初始化远端共享盘路径
	            //SmbFile sourceFile = new SmbFile("smb://" + sylna + ":" + sylpwd + "@" + sylpath+lstmntdte+"//" + sylname);
	    		String url="smb://"+sylyurl+lstmntdte+"/"+"收益率曲线"+lstmntdte+".xlsx";
	    		System.out.println(url);
	    		 SmbFile sourceFile = new SmbFile("smb://"+sylyurl+"路透/收益率_曲线_汇率/"+lstmntdte+"/"+"收益率曲线"+lstmntdte+".xlsx",auth);
	    		
	    		if (!sourceFile.exists()) {
	                return;
	            }
	            // 获取远端file名称
	            String sourceFileName = sourceFile.getName();
	            // 初始化本地文件对象
	            File targetFile = new File(sylLocalPath + File.separator + sourceFileName);
	            // 创建本地路径
	            if (!targetFile.exists()) {
	                targetFile.getParentFile().mkdirs();
	            }
	            // 输入流
	            in = new BufferedInputStream(new SmbFileInputStream(sourceFile));
	            // 输出流
	            out = new BufferedOutputStream(new FileOutputStream(targetFile));
	            byte[] buffer = new byte[1024];
	            while (in.read(buffer) != -1) {
	                out.write(buffer);
	                buffer = new byte[1024];
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                out.close();
	                in.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	public static List<String[]> ReadMethod(){
        String File = "C:\\Users\\hspcadmin\\Desktop\\韩亚银行OPICS升级\\二期文档\\市场数据新模板\\BONDDATA.csv";
        String line = "";
        String SplitBy = " ";
        String[] Line;
        List<String[]> BikeDataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(File))) {
            while ((line = br.readLine()) != null) {
                Line = line.split(SplitBy);
                BikeDataList.add(Line);
                System.out.println("signal "+Line[0]+" {\n\tsite 1 { pogo = "+Line[1]+"; }" +
                                   "\n\tsite 2 { pogo = "+Line[2]+"; }\n}");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BikeDataList;
    }

	
	public static String subStrToString(String args,int num) {
		String reslut = null;
		if(args != null) {
			//判断字符串中是否含有中文（不含中文）
			if(args.length() == args.getBytes().length) {
				if(args.length() == num) {
					reslut = args;
				}else {
					if(num < 0) {
						throw new RuntimeException("补空后的总长度不能为负数");
					}
					if(num < args.length()) {
						throw new RuntimeException("所给字符串长度大于补空后字符串长度，补零失败");
					}
					reslut = StringUtils.rightPad(args, num, "");
				}
				
			}else {
				if(num < 0) {
					throw new RuntimeException("补空后的总长度不能为负数");
				}
				if(num < args.getBytes().length) {
					throw new RuntimeException("所给字符串长度大于补空后字符串长度，补零失败");
				}
				int i=num-args.length()-args.length();
				reslut = StringUtils.rightPad(args, i, "");
			}
			
		}else {
			reslut = StringUtils.rightPad("", num, "");
		}
		return reslut;
	}
	
	public static String subStrOrNumber(String args,int num,int m) {
		String reslut = null;
		
		if(args != null) {
			if(num < 0) {
				throw new RuntimeException("补零后的总长度不能为负数");
			}
			if(num < m) {
				throw new RuntimeException("补零后的总长度不能小于小数点后的位数");
			}
			if(num < args.length()) {
				throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
			}
			//第一步，判断数值为正负
			if(args.contains("-")) {
				String fh = "-";
				//判断数值是否为整数，即小数点后是否有值
				if(args.contains(".")) {
					//截取小数点前的字符
					String args1 = args.substring(args.indexOf("-")+1, args.indexOf("."));
					//字符串小数点前左补0
					String args11 = StringUtils.leftPad(args1, num-m-1, "0");
					//截取小数点后的字符
					String args2 = args.substring(args.indexOf(".")+1,args.length());
					//字符串小数点后右补0
					String args21 = StringUtils.rightPad(args2, m, "0");
					//结果reslut	
					reslut = fh+args11+args21;
				}else{
					//整数值时，先左补0
					String args1 = args.substring(args.indexOf("-")+1, args.length());
					
					String args11 = StringUtils.leftPad(args1, num-m-1, "0");
					//根据左补0后的值再右补0
					String args2 = StringUtils.rightPad(args11, num-1, "0");
					//结果reslut
					reslut = fh+args2;
				}
			}else {
				//判断数值是否为整数，即小数点后是否有值
				if(args.contains(".")) {
					//截取小数点前的字符
					String args1 = args.substring(0, args.indexOf("."));
					//字符串小数点前左补0
					String args11 = StringUtils.leftPad(args1, num-m, "0");
					//截取小数点后的字符
					String args2 = args.substring(args.indexOf(".")+1,args.length());
					//字符串小数点后右补0
					String args21 = StringUtils.rightPad(args2, m, "0");
					//结果reslut	
					reslut = args11+args21;
				}else{
					//整数值时，先左补0
					String args1 = StringUtils.leftPad(args, num-m, "0");
					//根据左补0后的值再右补0
					String args2 = StringUtils.rightPad(args1, num, "0");
					//结果reslut
					reslut = args2;
				}
			}			
		}else{
			reslut = StringUtils.rightPad("", num, "");
		}
		
		return reslut;
	}
	/**
	 * 设置字符长度  不足者 右侧添加 指定字符
	 * @param str1 元字符
	 * @param lenth 指定长度
	 * @param st2 指定字符
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public static String strAppendStr(String str1, int lenth, String st2) throws UnsupportedEncodingException  {
		StringBuilder strb1 = new StringBuilder(str1);
		lenth = lenth - getChineseLength(str1, "UTF-8");
		while (lenth >= 0) {
			lenth--;
			strb1.append(st2);
		}
		return strb1.toString();
	}

	/**
	 * 计算中文字符长度
	 * @param name 字符
	 * @param endcoding 编码方式
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public static int getChineseLength(String name, String endcoding) throws UnsupportedEncodingException  {
		int len = 0; //定义返回的字符串长度
		int j = 0;
		//按照指定编码得到byte[]
		byte[] b_name = name.getBytes(endcoding);
		do {
			short tmpst = (short) (b_name[j] & 0xF0);
			if (tmpst >= 0xB0) {
				if (tmpst < 0xC0) {
					j += 2;
					len += 2;
				} else if ((tmpst == 0xC0) || (tmpst == 0xD0)) {
					j += 2;
					len += 2;
				} else if (tmpst == 0xE0) {
					j += 3;
					len += 2;
				} else {
					short tmpst0 = (short) (((short) b_name[j]) & 0x0F);
					if (tmpst0 == 0) {
						j += 4;
						len += 2;
					} else if (tmpst0 < 12) {
						j += 5;
						len += 2;
					} else {
						j += 6;
						len += 2;
					}
				}
			} else {
				j += 1;
				len += 1;
			}
		} while (j <= b_name.length - 1);
		return len;
	}
	
	
	
}
