package com.singlee.testcode;

import java.io.Serializable;

import com.alibaba.excel.annotation.ExcelProperty;

public class getSecl  implements Serializable{
	
	private static final long serialVersionUID = -6809015731102988151L;
	 
	@ExcelProperty("债券简称")
	  String secname;
	@ExcelProperty("债券代码")
	  String secid; 
	@ExcelProperty("估值日期")
	  String gdate;
	@ExcelProperty("流通场所")
	  String lpath;
	@ExcelProperty("待偿期(年)")
	  String dyear;
	@ExcelProperty("日间估价全价(元)")
	  String fullprice;
	@ExcelProperty("日间应计利息(元)")
	  String lx;
	@ExcelProperty("估价净价(元)")
	  String clprice;
	public String getSecname() {
		return secname;
	}
	public void setSecname(String secname) {
		this.secname = secname;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getGdate() {
		return gdate;
	}
	public void setGdate(String gdate) {
		this.gdate = gdate;
	}
	public String getLpath() {
		return lpath;
	}
	public void setLpath(String lpath) {
		this.lpath = lpath;
	}
	public String getDyear() {
		return dyear;
	}
	public void setDyear(String dyear) {
		this.dyear = dyear;
	}
	public String getFullprice() {
		return fullprice;
	}
	public void setFullprice(String fullprice) {
		this.fullprice = fullprice;
	}
	public String getLx() {
		return lx;
	}
	public void setLx(String lx) {
		this.lx = lx;
	}
	public String getClprice() {
		return clprice;
	}
	public void setClprice(String clprice) {
		this.clprice = clprice;
	}
	
	
}
