package com.singlee.quota;

import java.util.List;
import java.util.Map;

/***
 * 限额接口
 * @author lijing
 *
 */
public interface Limit {
	
	/***
	 * 
	 * @param list  限额维度
	 * @param map  成交单交易数据
	 * @param limitCondition 限额条件名称（即实体对象的属性）
	 * @param tradeAmt 交易金额字段，即交易中要限制金额的字段
	 * @param availAmt 可用金额字段，即限额维度中的可用金额字段
	 * @param limitType 限额类型字段
	 * @return Map<String,Object>   "flag":true--限额成功  false--限额失败
	 * 								"desc":List<Map<String, Object>> 限额维度以及每个维度是否限额成功
	 * 注意：调用此限额接口，限额条件表现形式应该为json字符串形式；
	 *      限额类型：1：单笔        2：累计
	 */
	Map<String,Object> limitAmt(List<Map<String,Object>> list,Map<String,Object> map,String limitCondition,
					 String tradeAmt,String availAmt,String limitType);

}
