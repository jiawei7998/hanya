package com.singlee.quota.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.ifs.mapper.IfsLimitConditionMapper;
import com.singlee.ifs.model.IfsLimitCondition;
import com.singlee.quota.Limit;
@Service
public class LimitImpl implements Limit {
	
	@Autowired
	IfsLimitConditionMapper ifsLimitConditionMapper;
	
	
	/***
	 * 
	 * @param list  限额维度
	 * @param map  成交单交易数据
	 * @param limitCondition 限额条件字段
	 * @param tradeAmt 交易金额字段，即交易中要限制金额的字段
	 * @param availAmt 可用金额字段，即限额维度中的可用金额字段
	 * @param limitType 限额类型字段
	 * @return Map<String,Object>  "flag":true--限额成功  false--限额失败
	 * 								"desc":List<Map<String, Object>> 限额维度以及每个维度是否限额成功
	 */
	@Override
	public Map<String,Object> limitAmt(List<Map<String, Object>> list,Map<String, Object> map,String limitCondition,String tradeAmt,String availAmt,String limitType) {
		//存放返回值
		Map<String,Object> returnMap = new HashMap<String,Object>();
		//存放返回值中map的 desc中
		List<Map<String, Object>> returnList =new ArrayList<Map<String, Object>>();
		
		//quotaPass 存放限额条件相同的数据
		List<Map<String, Object>> quotaPass =new ArrayList<Map<String, Object>>();
		//1.找出限额条件相同的
		for(int i=0;i<list.size();i++){
			Map<String, Object> limitDimension = BeanUtil.beanToMap(list.get(i));//获取单个限额维度
			boolean flag = limitDimension.containsKey(limitCondition); //判断limitDimension中是否存在key值
			if(flag != true){
				JY.raise("限额维度中不包含key值:"+limitCondition);
				//return false;
			}
			String limitCon = (String)limitDimension.get(limitCondition);//获取限额条件
			if(StringUtil.isNullOrEmpty(limitCon)){
				JY.raise("限额条件为null或空");
			}
			
			boolean ifStr = isJSONValid(limitCon) ;//判断字符串limitCon是否是json字符串
			if(ifStr!=true){
				JY.raise("限额条件表现形式需要以json字符串表现形式存在");
				//return false;
			}
			
			Map<String,Object> limitMap = (Map<String,Object>)JSON.parse(limitCon);//将限额条件以map形式表现
			
			boolean ifMap = compareTwoMap(limitMap,map);//比较限额条件map与成交单map是否匹配
			if(ifMap==true){//如果匹配，将此限额维度放在quotaPass中
				
				quotaPass.add(limitDimension);
				
			}else{//如果不匹配，继续比较下一个限额条件
				continue;
			}
			
		}
		
		if(quotaPass.size()==0){//成交单数据与所有的限额条件不匹配,不匹配就通过，返回true
			for(int i=0;i<list.size();i++){
				list.get(i).put("flag", "false");
				returnList.add(list.get(i));
			}
			returnMap.put("flag", "true");
			returnMap.put("desc", returnList);
			return returnMap;
		}
		
		
		//2.众多 相同限额条件中找'可用金额'最小的限额维度，与交易金额进行比对
		Map<String,Object> AmtSmallMap = findAvailAmtSmall(quotaPass,availAmt);
		
		//3.比较可用金额与交易金额
		String availAmt1 = (String) AmtSmallMap.get(availAmt);//可用金额
		String tradeAmt1 =  map.get(tradeAmt).toString();//交易金额
		Map<String,Object> ifbig = compareTwoStr(availAmt1,tradeAmt1);
		if("true".equals(ifbig.get("flg"))){//可用金额>=交易金额
			boolean flag = AmtSmallMap.containsKey(limitType); //判断AmtSmallMap中是否存在key值
			if(flag != true){
				JY.raise("此条限额维度中不包含key值:"+limitType);
				//return false;
			}
			//4.限额通过的情况下，再判断限额类型，若限额类型为'累计',则要操作数据库，将可用金额-交易金额
			if("2".equals(AmtSmallMap.get(limitType))){
				AmtSmallMap.put(availAmt, ifbig.get("leftAvailAmt"));
				ifsLimitConditionMapper.updateMap(AmtSmallMap);
				
				
				
			}
			//遍历传过来的限额维度
			for(int i=0;i<list.size();i++){
				
				if(list.get(i).get("id") != AmtSmallMap.get("id")){
					list.get(i).put("flag", "false");
					returnList.add(list.get(i));
				}
				
			}
			AmtSmallMap.put("flag", "true");
			returnList.add(AmtSmallMap);
			returnMap.put("flag", "true");
			returnMap.put("desc", returnList);
			 return returnMap;
			
		}
		
		/////////////////////////////////////
		for(int i=0;i<list.size();i++){
			list.get(i).put("flag", "false");
			returnList.add(list.get(i));
		}
		returnMap.put("flag", "false");
		returnMap.put("desc", returnList);
		return returnMap;
	}
	
	
	/**
     * 暴力解析:Alibaba fastjson
     * 判断字符串test是否是json字符串
     * @param test
     * @return
     */
	private final static boolean isJSONValid(String test) {
        try {
            JSONObject.parseObject(test);
        } catch (JSONException ex) {
            try {
                JSONObject.parseArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
    
    /***
     * 比较两个map是否相等,若map1中的所有的键在map2中能找到，并且值相等，即可认为map1与map2相等，返回true
     * @return true 相等
     */
    
    private boolean compareTwoMap(Map<String,Object> map1,Map<String,Object> map2){
    	boolean b=true;
    	Iterator<Entry<String, Object>> it1 = map1.entrySet().iterator();  
        while(it1.hasNext()){  
            Entry<String, Object> entry1 = it1.next();  
            String obj = map2.get(entry1.getKey()).toString();  
            if(obj == null||(!(obj.equals(entry1.getValue().toString())))){  
                b = false;  
                break;  
            }  
        }  
    	return b;
    	
    }
	/***
	 * 比较两个数字字符串
	 * @param str1
	 * @param str2
	 * @return map: flg为true 表示可用金额str1 >= 交易金额 str2
	 */
    private Map<String,Object> compareTwoStr(String str1,String str2){
    	double d1=0;
    	double d2=0;
    	Map<String,Object> map = new HashMap<String,Object>();
    	try {
			  d1 = Double.parseDouble(str1);
			  d2 = Double.parseDouble(str2);
		} catch (NumberFormatException e) {
			throw e;
		}
		
		if((d1-d2) >= 0 ){
			map.put("flg", "true");
			map.put("leftAvailAmt", String.valueOf(d1-d2));//剩余可用金额
			
		}else{
			map.put("flg", "false");
			map.put("leftAvailAmt", String.valueOf(d1));
			
		}
    	
    	return map;
    }
    
    
    /***
     * 
     * @param quotaPass  限额条件相同的限额维度
     * @param availAmt  限额维度中的可用金额字段
     * @return Map<String,Object> 返回可用金额最小的限额维度
     */
    private Map<String,Object> findAvailAmtSmall(List<Map<String, Object>> quotaPass,String availAmt){
    	
    	Map<String,Object> amtSmallMap = new HashMap<String,Object>();
    	String temp = "";
    	
    	for(int i=0;i<quotaPass.size();i++){
    		Map<String, Object>  tempMap = quotaPass.get(i);
    		boolean flag = tempMap.containsKey(availAmt); //判断tempMap中是否存在key值
			if(flag != true){
				if(i==quotaPass.size()-1){
					JY.raise("限额维度不包含可用金额字段:"+availAmt);
				}
				continue;
			}
			
			if(i==0){
				temp = (String) tempMap.get(availAmt);
				amtSmallMap = quotaPass.get(i);
			}else{
				Map<String,Object> map =  compareTwoStr(temp,(String)tempMap.get(availAmt));
				if("true".equals(map.get("flg"))){
					temp = (String) tempMap.get(availAmt);
					amtSmallMap.clear();
					amtSmallMap = quotaPass.get(i);
				}
			}
			
    		
    	}
    	
    	
    	return amtSmallMap;
    }
    
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
