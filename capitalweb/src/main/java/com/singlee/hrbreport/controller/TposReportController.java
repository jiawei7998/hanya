package com.singlee.hrbreport.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.TposPo;
import com.singlee.hrbreport.service.TposReportService;

/**
 * 合并持仓分析报表
 *
 */
@Controller
@RequestMapping(value = "/TposReportController")
public class TposReportController extends CommonController {

	@Autowired
	private TposReportService tposReportService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTposReportPage")
	public RetMsg<PageInfo<TposPo>> searchRmbReportPage(@RequestBody Map<String, String> map) {
		String sdate = ParameterUtil.getString(map,"sdate","");
		String edate = ParameterUtil.getString(map,"edate","");
		if("".equals(sdate) || "".equals(edate)) {
			JY.raise("缺少参数......");
		}
		Page<TposPo> repoReportPage = tposReportService.getTposReportPage(map);
		return RetMsgHelper.ok(repoReportPage);
	}

}
