package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.RmbNostroVO;
import com.singlee.hrbreport.service.RmbNostroService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 人民币同业余额
 * @author zk
 */
@Controller
@RequestMapping("/RmbNostroController")
public class RmbNostroController extends CommonController {
    
    @Autowired
    RmbNostroService rmbNostor;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<RmbNostroVO>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<RmbNostroVO> reportPage = rmbNostor.getReportPage(map);
        return RetMsgHelper.ok(reportPage);
    }
    

}
