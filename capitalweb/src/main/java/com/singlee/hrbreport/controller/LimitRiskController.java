package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.LimitRisk;
import com.singlee.hrbreport.service.impl.LimitRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/10/30 17:37
 * @description：
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping("/LimitRiskController")
public class LimitRiskController extends CommonController {

    @Autowired
    LimitRiskService limitRiskService;

    @ResponseBody
    @RequestMapping(value = "/searchPageLimit")
    public RetMsg<PageInfo<LimitRisk>> searchPageLimit(@RequestBody Map<String, Object> map) {
        Page<LimitRisk> page = limitRiskService.getLimitPage(map);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping(value = "/saveLimitRisk")
    public RetMsg<Serializable> saveLimitRisk(@RequestBody List<LimitRisk> list) {
        if (list.size()>0){
          for (LimitRisk limitRisk:list){
              limitRiskService.updateByPrimaryKey(limitRisk);
          }
        }
        return RetMsgHelper.ok();

    }
}
