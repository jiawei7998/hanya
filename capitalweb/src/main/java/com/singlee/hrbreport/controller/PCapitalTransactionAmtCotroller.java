package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.PCapitalTransactionAmtVO;
import com.singlee.hrbreport.service.PCapitalTransactionAmtService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/PCapitalTransactionAmtCotroller")
public class PCapitalTransactionAmtCotroller extends CommonController {
    
    @Autowired
    PCapitalTransactionAmtService pCapitalTransactionAmtService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<PCapitalTransactionAmtVO>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Date postdate = null;
        try {
            postdate = new SimpleDateFormat("yyyy-MM-dd").parse(map.get("queryDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        map.put("postdate", postdate);
        Page<PCapitalTransactionAmtVO> page = pCapitalTransactionAmtService.selectPCapitalTransactionAmt(map);
        return RetMsgHelper.ok(page);
    }
}
