package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.service.CapitalTurnoverService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

@Controller
@RequestMapping("/CapitalTurnoverController")
public class CapitalTurnoverController extends CommonController {
    
    @Autowired
    CapitalTurnoverService capitalTurnoverService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<Map<String,Object>>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        map.put("postdate", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));
        Page<Map<String,Object>> page = capitalTurnoverService.getCapitalTurnover(map);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping("/call")
    public RetMsg<Serializable> call(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));

        capitalTurnoverService.getCapitalData(map);
        return RetMsgHelper.ok();
    }
}
