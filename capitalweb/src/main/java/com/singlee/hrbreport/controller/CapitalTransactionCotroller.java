package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.CapitalTransactionVO;
import com.singlee.hrbreport.service.CapitalTransactionService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;



@Controller
@RequestMapping("/CapitalTransactionCotroller")
public class CapitalTransactionCotroller extends CommonController {
    
    @Autowired
    CapitalTransactionService capitalTransactionService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<CapitalTransactionVO>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Date postdate = null;
        try {
            postdate = new SimpleDateFormat("yyyy-MM-dd").parse(map.get("queryDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        map.put("postdate", postdate);
        Page<CapitalTransactionVO> page = capitalTransactionService.selectGovernmentBond(map);
        return RetMsgHelper.ok(page);
    }
    
        
    
    
    
    @RequestMapping("/exportExcel")
    @ResponseBody
    public void exportExcel(HttpServletResponse response,CapitalTransactionVO ca,HttpSession session) throws Exception {
        String filename = "资金交易报表.xlsx"; // 文件名
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            throw new RException(e1.getMessage());
        }
      
        response.setContentType("application/vnd.ms-excel");
        response.addHeader("Content-Disposition", "attachment;fileName="+encode_filename);
        try {
            ExcelUtil e = downloadExcel(response,ca,session);
            OutputStream ouputStream = response.getOutputStream();
            e.getWb().write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            throw new RException(e);
        }
    }
    
    
    
    
    
    @SuppressWarnings("unused")
    public ExcelUtil downloadExcel(HttpServletResponse response,CapitalTransactionVO ca,HttpSession session) throws Exception {
       
        
        ExcelUtil e = null;
        try {
            /* 先查数据，再设置表格属性以及内容 */
            Page<CapitalTransactionVO> page1 = new Page<CapitalTransactionVO>();

              Map<String, Object> map =new HashMap<>();
              map.put("pageNumber", "1");
              map.put("pageSize", "999"); 
              page1 = capitalTransactionService.selectGovernmentBond(map);
            // 表头
            e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
            CellStyle center = e.getDefaultCenterStrCellStyle();
            int sheet = 0;
            int row = 0;
            // 表格sheet名称
            e.getWb().createSheet("资金交易报表");
            // 设置表头字段名
            e.writeStr(sheet, row, "A", center,"银行机构代码");
            e.writeStr(sheet, row, "B", center,"内部机构号");
            e.writeStr(sheet, row, "C", center,"银行机构名称");
            e.writeStr(sheet, row, "D", center,"交易编号");
            e.writeStr(sheet, row, "E", center,"资金交易类型");
            e.writeStr(sheet, row, "F", center,"帐户类型");
            e.writeStr(sheet, row, "G", center,"币种");
            e.writeStr(sheet, row, "H", center,"交易柜员");
            e.writeStr(sheet, row, "I", center,"审核人");
            e.writeStr(sheet, row, "J", center,"交易对手代码");
            e.writeStr(sheet, row, "K", center, "交易对手名称");
            e.writeStr(sheet, row, "L", center, "交易日期");
            e.writeStr(sheet, row, "M", center, "起始日期");
            e.writeStr(sheet, row, "N", center, "到期日期");
            e.writeStr(sheet, row, "O", center, "买卖标志");
            e.writeStr(sheet, row, "P", center, "即远期标志");
            e.writeStr(sheet, row, "Q", center, "买入币种");
            e.writeStr(sheet, row, "R", center, "买入金额");
            e.writeStr(sheet, row, "S", center, "卖出币种");
            e.writeStr(sheet, row, "T", center, "卖出金额");
            e.writeStr(sheet, row, "U", center, "复核日期");
            e.writeStr(sheet, row, "V", center, "取消日期");
            e.writeStr(sheet, row, "W", center, "实际交割日期");
            e.writeStr(sheet, row, "X", center, "清算标志");
            // 设置列的宽度
            e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);  
            e.getSheetAt(sheet).setColumnWidth(10, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(11, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(12, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(13, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(14, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(15, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(10, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(11, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(12, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(13, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(14, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(15, 20 * 256);
            e.getSheetAt(sheet).setColumnWidth(16, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(17, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(18, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(19, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(20, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(21, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(22, 20 * 256); 
            e.getSheetAt(sheet).setColumnWidth(23, 20 * 256); 
            
          
           
            
           
            for (int i = 0; i < page1.size(); i++) {
                ca = page1.get(i);
                    row++;
                   
                    
                    //插入数据
                    e.writeStr(sheet, row, "A", center, ca.getBr());
                    e.writeStr(sheet, row, "B", center, ca.getBr2());
                    e.writeStr(sheet, row, "C", center, ca.getBanksn());
                    e.writeStr(sheet, row, "D", center, ca.getDealno());
                    e.writeStr(sheet, row, "E", center, ca.getType());
                    e.writeStr(sheet, row, "F", center, ca.getAcctngtype());
                    e.writeStr(sheet, row, "G", center, ca.getCcy());
                    e.writeStr(sheet, row, "H", center, ca.getTrad());
                    e.writeStr(sheet, row, "I", center, ca.getVoper());
                    e.writeStr(sheet, row, "J", center, ca.getCno());     
                    e.writeStr(sheet, row, "K", center, ca.getSn());
                    e.writeStr(sheet, row, "L", center, ca.getDealdate());
                    e.writeStr(sheet, row, "M", center, ca.getVdate());
                    e.writeStr(sheet, row, "N", center, ca.getMdate());
                    e.writeStr(sheet, row, "O", center, ca.getPs());
                    e.writeStr(sheet, row, "P", center, ca.getSpotfwdind());
                    e.writeStr(sheet, row, "Q", center, ca.getCcy2());
                    e.writeStr(sheet, row, "R", center, ca.getCcyamt());
                    e.writeStr(sheet, row, "S", center, ca.getCtrccy());
                    e.writeStr(sheet, row, "T", center, ca.getCtramt());
                    e.writeStr(sheet, row, "U", center, ca.getVerdate());
                    e.writeStr(sheet, row, "V", center, ca.getRevdate());
                    e.writeStr(sheet, row, "W", center, ca.getCcysettdate());
                    e.writeStr(sheet, row, "X", center, ca.getNetsi());
                }
           
            
            return e;
        } catch (Exception e1) {
            throw new RException(e1);
        }
    }

    
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    

