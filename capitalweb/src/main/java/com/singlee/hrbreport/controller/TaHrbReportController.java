package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.TaUserInfoService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.model.TaHrbReport;
import com.singlee.hrbreport.service.TaHrbReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Map;

/**
 * 报表模板
 */
@Controller
@RequestMapping("/TaHrbReportController")
public class TaHrbReportController extends CommonController {
    @Autowired
    TaHrbReportService taHrbReportService;
    @Autowired
    TaUserInfoService UserInfoService;

    @ResponseBody
    @RequestMapping(value = "/searchTaHrbReportList")
    public RetMsg<PageInfo<TaHrbReport>> searchTaHrbReport(@RequestBody Map<String, Object> params){
        params.put("isActive", DictConstants.YesNo.YES);
        params.put("instId", SlSessionHelper.getInstitutionId());

        String instName = SlSessionHelper.getInstitution().getInstName();
        String roles = UserInfoService.selectUserInfoById(SlSessionHelper.getUserId()).getRoles();
        if(!roles.contains("HR0000000")){
            String[] role = roles.split(",");
            params.put("reportInst", instName);
            params.put("reportRole", Arrays.asList(role));
        }

        Page<TaHrbReport> reports = taHrbReportService.searchTaHrbReportPage(params);
        return RetMsgHelper.ok(reports);
    }

}
