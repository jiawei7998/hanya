package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbreport.service.ChinaRussiaReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/15 16:32
 * @description：
 * @modified By：
 * @version:
 * 中俄月报
 */
@Controller
@RequestMapping("/ChinaRussiaReportController")
public class ChinaRussiaReportController {

    @Autowired
    ChinaRussiaReportService chinaRussiaReportService;

    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<Serializable> search(@RequestBody Map<String ,Object> map){
        Page page=  chinaRussiaReportService.getChinaRussiaPage(map);

        return RetMsgHelper.ok(page);
    }



    @ResponseBody
    @RequestMapping("/call")
    public RetMsg<Serializable> call(@RequestBody Map<String ,Object> map){
        map.put("intodates", ParameterUtil.getString(map, "queryDate", DateUtil.getCurrentDateAsString()));

        chinaRussiaReportService.getChinaRussiaDate(map);
        return RetMsgHelper.ok();
    }
}
