package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.CurAssetVO;
import com.singlee.hrbreport.service.CurAssetService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
/**
 * _优质流动性资产台账
 * @author zk
 */
@Controller
@RequestMapping("/CurAssetController")
public class CurAssetController extends CommonController {
    
    @Autowired
    CurAssetService curAssetService;
    
    @ResponseBody
    @RequestMapping("/searchCurrentAssets")
    public RetMsg<PageInfo<CurAssetVO>> searchCurrentAssets(@RequestBody  Map<String,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<CurAssetVO> queryCurrentAssetsPage = curAssetService.queryCurrentAssetsPage(map);
        return RetMsgHelper.ok(queryCurrentAssetsPage);
    }
    
}
