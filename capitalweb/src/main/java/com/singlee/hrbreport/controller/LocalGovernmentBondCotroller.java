package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;
import com.singlee.hrbreport.service.LocalGovernmentBondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/LocalGovernmentBondCotroller")
public class LocalGovernmentBondCotroller extends CommonController {

    @Autowired
    LocalGovernmentBondService localGovernmentBondService;


    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<LocalGovernmentBondVO>> search(@RequestBody Map<String, Object> map) {
        map.put("OPICS", SystemProperties.hrbcbUserName);
        map.put("HRBCB", SystemProperties.opicsUserName);
        Page<LocalGovernmentBondVO> page = localGovernmentBondService.searchGovernmentBondPage(map);


        System.out.println(RetMsgHelper.ok(page));
        return RetMsgHelper.ok(page);
    }


}
