package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.RepoPo;
import com.singlee.hrbreport.service.RepoReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 回购台账报表
 *
 */
@Controller
@RequestMapping(value = "/RepoReportController")
public class RepoReportController extends CommonController {

	@Autowired
	private RepoReportService repoReportService;
	
	@ResponseBody
	@RequestMapping(value = "/searchRepoReportPage")
	public RetMsg<PageInfo<RepoPo>> searchRepoReportPage(@RequestBody Map<String, String> map) {
		String sdate = ParameterUtil.getString(map,"sdate","");
		String edate = ParameterUtil.getString(map,"edate","");
		if("".equals(sdate) || "".equals(edate)) {
			JY.raise("缺少参数......");
		}
		map.put("yrMonth", edate.substring(5));
		Page<RepoPo> repoReportPage = repoReportService.getRepoReportPage(map);
		return RetMsgHelper.ok(repoReportPage);
	}

}
