package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.model.PCapitalTransactionVO;
import com.singlee.hrbreport.service.PCapitalTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/PCapitalTransactionCotroller")
public class PCapitalTransactionCotroller extends CommonController {
    
    @Autowired
    PCapitalTransactionService pCapitalTransactionService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<PCapitalTransactionVO>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", SystemProperties.hrbcbUserName);
        map.put("HRBCB", SystemProperties.opicsUserName);
        Date postdate = null;
        try {
            postdate = new SimpleDateFormat("yyyy-MM-dd").parse(map.get("queryDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        map.put("postdate", postdate);
        Page<PCapitalTransactionVO> page = pCapitalTransactionService.selectPCapitalTransactionPage(map);
        return RetMsgHelper.ok(page);
    }
}
