package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.ExchangeRepoVO;
import com.singlee.hrbreport.service.ExchangeRepoService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * _交易所回购
 * @author zk
 */
@Controller
@RequestMapping("/ExchangeRepoController")
public class ExchangeRepoController extends CommonController {
    
    @Autowired
    ExchangeRepoService exchangeRepoService;
    
    @ResponseBody
    @RequestMapping("/searchExchangeRepo")
    public RetMsg<PageInfo<ExchangeRepoVO>> searchExchangeRepo(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<ExchangeRepoVO> exchangeRepoPage = exchangeRepoService.getExchangeRepoPage(map);
        return RetMsgHelper.ok(exchangeRepoPage);
    }
}
