package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.DepositAndBorrowingBranchUploadVO;
import com.singlee.hrbreport.model.DepositAndBorrowingForeInvestorVO;
import com.singlee.hrbreport.model.DepositAndBorrowingSummarizingVO;
import com.singlee.hrbreport.service.DepAndBorrService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * _存款含银行同业和联行存放负债
 * @author zk
 *
 */
@Controller
@RequestMapping("/DepositAndBorrowingController")
public class DepositAndBorrowingController extends CommonController {
    
    @Autowired
    DepAndBorrService depAndBorrService;
    
    @ResponseBody
    @RequestMapping("/searchForeInvestor")
    public RetMsg<PageInfo<DepositAndBorrowingForeInvestorVO>> searchForeInvestor(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<DepositAndBorrowingForeInvestorVO> foreInvestorReportPage = depAndBorrService.getForeInvestorReportPage(map);
        return RetMsgHelper.ok(foreInvestorReportPage);
    }
    
    @ResponseBody
    @RequestMapping("/searchBranchUpload")
    public RetMsg<PageInfo<DepositAndBorrowingBranchUploadVO>> searchBranchUpload(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<DepositAndBorrowingBranchUploadVO> branchUploadReportPage = depAndBorrService.getBranchUploadReportPage(map);
        return RetMsgHelper.ok(branchUploadReportPage);
    }
    
    @ResponseBody
    @RequestMapping("/searchSummarizing")
    public RetMsg<PageInfo<DepositAndBorrowingSummarizingVO>> searchSummarizing(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<DepositAndBorrowingSummarizingVO> summarizingReportPage = depAndBorrService.getSummarizingReportPage(map);
        return RetMsgHelper.ok(summarizingReportPage);
    }
    
}
