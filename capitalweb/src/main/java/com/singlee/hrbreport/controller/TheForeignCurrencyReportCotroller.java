package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.TheForeignCurrencyVO;
import com.singlee.hrbreport.model.TheForeignCurrencyVO2;
import com.singlee.hrbreport.service.TheForeignCurrencyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/TheForeignCurrencyReportCotroller")
public class TheForeignCurrencyReportCotroller extends CommonController {
    
    @Autowired
    TheForeignCurrencyReportService theForeignCurrencyReportService;
    
    @ResponseBody
    @RequestMapping("/search1")
    public RetMsg<PageInfo<TheForeignCurrencyVO>> search1(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
       
        Page<TheForeignCurrencyVO> page = theForeignCurrencyReportService.selectTheForeignCurrency1(map);
        return RetMsgHelper.ok(page);
    }
    
    @ResponseBody
    @RequestMapping("/search2")
    public RetMsg<PageInfo<TheForeignCurrencyVO>> search2(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
        
       
        Page<TheForeignCurrencyVO> page = theForeignCurrencyReportService.selectTheForeignCurrency2(map);
        return RetMsgHelper.ok(page);
    }
    
    
    @ResponseBody
    @RequestMapping("/search3")
    public RetMsg<PageInfo<TheForeignCurrencyVO2>> search3(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
       
        Page<TheForeignCurrencyVO2> page = theForeignCurrencyReportService.selectTheForeignCurrency3(map);
        return RetMsgHelper.ok(page);
    }
    
    
    @ResponseBody
    @RequestMapping("/search4")
    public RetMsg<PageInfo<TheForeignCurrencyVO2>> search4(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
       
        Page<TheForeignCurrencyVO2> page = theForeignCurrencyReportService.selectTheForeignCurrency4(map);
        return RetMsgHelper.ok(page);
    }
    
    
    @ResponseBody
    @RequestMapping("/search5")
    public RetMsg<PageInfo<TheForeignCurrencyVO2>> search5(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
      
       
        Page<TheForeignCurrencyVO2> page = theForeignCurrencyReportService.selectTheForeignCurrency5(map);
        return RetMsgHelper.ok(page);
    }
    
    @ResponseBody
    @RequestMapping("/search6")
    public RetMsg<PageInfo<TheForeignCurrencyVO2>> search6(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
       
        Page<TheForeignCurrencyVO2> page = theForeignCurrencyReportService.selectTheForeignCurrency6(map);
        return RetMsgHelper.ok(page);
    }
    
    @ResponseBody
    @RequestMapping("/search7")
    public RetMsg<PageInfo<TheForeignCurrencyVO>> search7(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
        Page<TheForeignCurrencyVO> page = theForeignCurrencyReportService.selectTheForeignCurrency7(map);
        return RetMsgHelper.ok(page);
    }
   
    @ResponseBody
    @RequestMapping("/search8")
    public RetMsg<PageInfo<TheForeignCurrencyVO>> search8(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        
       
       
        Page<TheForeignCurrencyVO> page = theForeignCurrencyReportService.selectTheForeignCurrency8(map);
        return RetMsgHelper.ok(page);
    }
    
    
}
