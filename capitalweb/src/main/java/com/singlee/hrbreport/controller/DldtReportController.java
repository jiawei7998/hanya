package com.singlee.hrbreport.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.DldtPo;
import com.singlee.hrbreport.service.DldtReportService;

/**
 * 拆借台账报表
 *
 */
@Controller
@RequestMapping(value = "/DldtReportController")
public class DldtReportController extends CommonController {

	@Autowired
	private DldtReportService dldtReportService;
	
	@ResponseBody
	@RequestMapping(value = "/searchDldtReportPage")
	public RetMsg<PageInfo<DldtPo>> searchDldtReportPage(@RequestBody Map<String, String> map) {
		String sdate = ParameterUtil.getString(map,"sdate","");
		String edate = ParameterUtil.getString(map,"edate","");
		if("".equals(sdate) || "".equals(edate)) {
			JY.raise("缺少参数......");
		}
		map.put("yrMonth", edate.substring(5));
		Page<DldtPo> repoReportPage = dldtReportService.getDldtReportPage(map);
		return RetMsgHelper.ok(repoReportPage);
	}

}
