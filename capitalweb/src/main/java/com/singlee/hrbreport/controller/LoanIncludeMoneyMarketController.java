package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketCounGroupVO;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketVO;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * _贷款拆放含拆放银行同业及联行资产表
 * @author zk
 *
 */
@Controller
@RequestMapping("/LoanIncludeMoneyMarketController")
public class LoanIncludeMoneyMarketController extends CommonController {

    @Autowired
    private LoanInSameService loanService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<LoanIncludeMoneyMarketVO>> search(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<LoanIncludeMoneyMarketVO> loanReportData = loanService.getLoanReportData(map);
        return RetMsgHelper.ok(loanReportData);
    }
    
    @ResponseBody
    @RequestMapping("/searchCoun")
    public RetMsg<PageInfo<LoanIncludeMoneyMarketCounGroupVO>> searchCoun(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<LoanIncludeMoneyMarketCounGroupVO> loanReportData = loanService.getLoanCounGroupReportData(map);
        return RetMsgHelper.ok(loanReportData);
    }
    
}
