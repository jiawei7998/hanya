package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.*;
import com.singlee.hrbreport.service.InBankDealService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/InBankDealController")
public class InBankDealController extends CommonController {

    @Autowired
    InBankDealService inBankDealService;
    
    @ResponseBody
    @RequestMapping("/queryRepoDetail")
    public RetMsg<PageInfo<InBankDealRepoVO>> queryRepoDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealRepoVO> queryRepoPage = inBankDealService.queryRepoPage(map);
        return RetMsgHelper.ok(queryRepoPage);
    }
    
    @ResponseBody
    @RequestMapping("/querySecPosDetail")
    public RetMsg<PageInfo<InBankDealSecVO>> querySecPosDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealSecVO> querySecPosDetailPage = inBankDealService.querySecPosDetailPage(map);
        return RetMsgHelper.ok(querySecPosDetailPage);
    }

    @ResponseBody
    @RequestMapping("/queryBrLoDetail")
    public RetMsg<PageInfo<InBankDealBrLoVO>> queryBrLoDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealBrLoVO> queryBrLoPosDetailPage = inBankDealService.queryBrLoPosDetailPage(map);
        return RetMsgHelper.ok(queryBrLoPosDetailPage);
    }
    
    @ResponseBody
    @RequestMapping("/queryCDSEDetail")
    public RetMsg<PageInfo<InBankDealCDSeVO>> queryCDSEDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealCDSeVO> queryCDSEDetailPage = inBankDealService.queryCDSEDetailPage(map);
        return RetMsgHelper.ok(queryCDSEDetailPage);
    }
    
    @ResponseBody
    @RequestMapping("/queryRHCDSEDetail")
    public RetMsg<PageInfo<InBankDealCDSeVO>> queryRHCDSEDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealCDSeVO> queryCDSEDetailPage = inBankDealService.queryRHCDSEDetailPage(map);
        return RetMsgHelper.ok(queryCDSEDetailPage);
    }
    
    @ResponseBody
    @RequestMapping("/queryFundDetail")
    public RetMsg<PageInfo<InBankDealFundVO>> queryFundDetail(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<InBankDealFundVO> queryFundDetailPage = inBankDealService.queryFundDetailPage(map);
        return RetMsgHelper.ok(queryFundDetailPage);
    }
    
}
