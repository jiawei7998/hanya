package com.singlee.hrbreport.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.model.ForeDebtContractVO;
import com.singlee.hrbreport.service.ForeDebtContractReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;



@Controller
@RequestMapping(value = "/ForeDebtContractController")
public class ForeDebtContractController extends CommonController {

	@Autowired
	ForeDebtContractReportService foreDebtContractReportService;
	
	//外债签约
	@ResponseBody
	@RequestMapping(value = "/getDataSet")
	public RetMsg<PageInfo<ForeDebtContractVO>> getDataSet(@RequestBody Map<String, Object> reMap) throws RemoteConnectFailureException, Exception  {

	    reMap.put("OPICS",  SystemProperties.hrbcbUserName);
	    reMap.put("HRBCB", SystemProperties.opicsUserName);
        Date postdate = null;
        try {
            postdate = new SimpleDateFormat("yyyy-MM-dd").parse(reMap.get("queryDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        reMap.put("postdate",postdate);
       Page<ForeDebtContractVO> page = foreDebtContractReportService.getDataSet(reMap);
   
        
        
       System.out.println(RetMsgHelper.ok(page));
        return RetMsgHelper.ok(page);
	    
	    
	    
	    
	}
	
}
