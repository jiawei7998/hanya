package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.EstSecPosVO;
import com.singlee.hrbreport.service.EstSecPosService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * _其他形式流入房地产
 * @author zk
 */
@Controller
@RequestMapping("/EstSecPosController")
public class EstSecPosController extends CommonController {
    @Autowired
    EstSecPosService estSecPosService;
    
    @ResponseBody
    @RequestMapping("/searchEstateSec")
    public RetMsg<PageInfo<EstSecPosVO>> searchEstateSec(@RequestBody  Map<String,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<EstSecPosVO> queryEstateSecBalancePage = estSecPosService.queryEstateSecBalancePage(map);
        return RetMsgHelper.ok(queryEstateSecBalancePage);
    }
}
