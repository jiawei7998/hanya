package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.SlCdBalanceVO;
import com.singlee.hrbreport.service.SlCdBalanceService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
/**
 * _同业存单发行情况
 * @author zk
 */
@Controller
@RequestMapping("/SlCdBalanceController")
public class SlCdBalanceController extends CommonController {
    
    @Autowired
    SlCdBalanceService slCdBalanceService;
    
    @ResponseBody
    @RequestMapping("/searchSlcdBala")
    public RetMsg<PageInfo<SlCdBalanceVO>> searchSlcdBala(@RequestBody Map<String, Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<SlCdBalanceVO> searchSlcdBalaPage = slCdBalanceService.searchSlcdBalaPage(map);
        return RetMsgHelper.ok(searchSlcdBalaPage);
    }
}
