package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.AccrualPrincilalChangeVO;
import com.singlee.hrbreport.service.AccrualPriChgService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * _计提本金情况变动表
 * @author zk
 *
 */
@Controller
@RequestMapping("/AccrualPrincipalChangeController")
public class AccrualPrincipalChangeController extends CommonController {
    
    @Autowired
    AccrualPriChgService accrual;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<AccrualPrincilalChangeVO>> search(@RequestBody Map<String ,Object> map){
        
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Page<AccrualPrincilalChangeVO> accrualData = accrual.getReportPage(map);
        return RetMsgHelper.ok(accrualData);
    }
    
    
    
}
