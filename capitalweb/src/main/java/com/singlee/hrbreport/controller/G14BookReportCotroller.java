package com.singlee.hrbreport.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.hrbreport.model.G14BookReportVO;
import com.singlee.hrbreport.service.G14BookReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/G14BookReportCotroller")
public class G14BookReportCotroller extends CommonController {
    
    @Autowired
    G14BookReportService g14BookReportService;
    
    @ResponseBody
    @RequestMapping("/search")
    public RetMsg<PageInfo<G14BookReportVO>> search(@RequestBody Map<String ,Object> map){
        map.put("OPICS", HrbReportUtils.OPICS);
        map.put("HRBCB", HrbReportUtils.HRBCB);
        Date postdate = null;
        try {
            postdate = new SimpleDateFormat("yyyy-MM-dd").parse(map.get("queryDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        map.put("postdate", postdate);
        Page<G14BookReportVO> page = g14BookReportService.getReportMap(map);
        return RetMsgHelper.ok(page);
    }
}
