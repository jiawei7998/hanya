package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.SlCdBalanceVO;

public interface SlCdBalanceMapper {
    
    public List<SlCdBalanceVO> searchSlcdBala(Map<String, Object> map);
    
}
