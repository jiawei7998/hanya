package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface AccrualPrincipalChangeMapper {
    
    public  List<Map<String,Object>> queryHoliday(Map<String,Object> map);
    
    public  List<Map<String,Object>> queryAcctNo(Map<String,Object> map);
    
    public  List<Map<String,Object>> queryPrincipal1(Map<String,Object> map);
    
    public  List<Map<String,Object>> queryPrincipal2(Map<String,Object> map);
    

}
