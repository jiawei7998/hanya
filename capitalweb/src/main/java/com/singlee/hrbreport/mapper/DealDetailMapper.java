package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.MutualFundsVO;
import com.singlee.hrbreport.model.RepoDealDetailVO;
import com.singlee.hrbreport.model.SecDetailVO;
import com.singlee.hrbreport.model.SecPurchaseVO;

/**
 * _业务明细_人行
 * @author zk
 *
 */
public interface DealDetailMapper {
    
    List<RepoDealDetailVO> queryReverseRepoDetail(Map<String ,Object> map);
    
    List<RepoDealDetailVO> queryRepoDetail(Map<String ,Object> map);
    
    List<SecDetailVO> querySecDetail(Map<String ,Object> map);
    
    List<MutualFundsVO> queryFundDealDetail(Map<String ,Object> map);
    
    List<SecPurchaseVO> querySecPurchaseDetail(Map<String ,Object> map);
    
    List<Map<String,Object>> queryHoliday(Map<String,Object> map);
    
}
