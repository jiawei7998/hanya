package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.RmbNostroVO;

/**
 * 人民币同业余额
 * @author zk
 */
public interface RmbNostroMapper {
    
    List<RmbNostroVO> queryAllFixed(Map<String ,Object> map);
    
    List<RmbNostroVO> queryAcdw(Map<String ,Object> map);
    
    List<RmbNostroVO> queryDldt(Map<String ,Object> map);
    
    String getStaticFiled(Map<String ,Object> map);
    
}
