package com.singlee.hrbreport.mapper;

import java.util.Map;

public interface AssetandLoanMapper {
    
    public Map<String , Object> queryAssetandLoan(Map<String , Object> map);
    
}
