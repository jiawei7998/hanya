package com.singlee.hrbreport.mapper;

import java.util.Map;

/**
 * G14 大额风险暴露统计表 
 * @author zk
 */
public interface G1401ReportMapper {
    
    public Map<String , Object> queryUnsame(Map<String , Object> map);
    
    public Map<String , Object> queryUnsameMax(Map<String , Object> map);
    
    public Map<String , Object> querySame(Map<String , Object> map);
    
    public Map<String , Object> querySameMax(Map<String , Object> map);
    
}
