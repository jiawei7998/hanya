package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.ExchangeRepoVO;

/**
 * _交易所回购
 * @author zk
 */
public interface ExchangeRepoMapper {
    
    public List<ExchangeRepoVO> queryExchangeRepo(Map<String ,Object> map);
    
}
