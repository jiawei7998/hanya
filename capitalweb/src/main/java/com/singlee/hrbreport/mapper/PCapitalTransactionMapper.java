package com.singlee.hrbreport.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;

import com.singlee.hrbreport.model.PCapitalTransactionVO;

public interface PCapitalTransactionMapper {
    
    
    
    public List<PCapitalTransactionVO> searchPCapitalTransaction(Map<String, Object> map);
    

}
