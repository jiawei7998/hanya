package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;
/**
 * g52地方政府融资平台及支出责任债务持有情况统计表
 * @author zk
 *
 */
public interface G52ReportMapper {
    
    public List<Map<String , Object>> queryAmt(Map<String ,Object> map);
    
}
