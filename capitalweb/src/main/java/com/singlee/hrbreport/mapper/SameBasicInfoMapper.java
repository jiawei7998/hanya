package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.SameBasicInfoVO;

public interface SameBasicInfoMapper {
    public List<SameBasicInfoVO> queryBasicInfo(Map<String ,Object> map);
}
