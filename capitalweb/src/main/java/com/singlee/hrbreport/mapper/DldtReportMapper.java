package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.hrbreport.model.DldtPo;

public interface DldtReportMapper {
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<DldtPo> getDldtReportPage(Map<String, String> map);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> getRateList(Map<String, String> map);
}
