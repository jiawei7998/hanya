package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.EstSecPosVO;

/**
 * _其他形式流入房地产
 * @author zk
 */
public interface EstSecPosMapper {
    public List<EstSecPosVO> queryEstateSecBalance(Map<String , Object> param);
}
