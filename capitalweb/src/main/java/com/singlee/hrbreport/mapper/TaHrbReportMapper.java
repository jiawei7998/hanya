package com.singlee.hrbreport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.TaHrbReport;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TaHrbReportMapper extends Mapper<TaHrbReport> {
    Page<TaHrbReport> searchTaHrbReportPage(Map<String, Object> params, RowBounds rowBounds);

    TaHrbReport searchTaHrbReport(Map<String, Object> params);
}