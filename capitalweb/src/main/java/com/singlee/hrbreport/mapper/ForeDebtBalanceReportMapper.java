package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.ForeDebtBalanceVO;

public interface ForeDebtBalanceReportMapper {
    
    public List<ForeDebtBalanceVO> getCusBalanceList(Map<String, Object> map);
    
}
