package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G11ReportMapper {
	
	public  List<Map<String,Object>> getG11Reportsql1(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG11Reportsql2(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG11Reportsql3(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG11Reportsql5(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG11Reportsql5r(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG11Reportsql5d(Map<String,Object> map);
	
	
}
