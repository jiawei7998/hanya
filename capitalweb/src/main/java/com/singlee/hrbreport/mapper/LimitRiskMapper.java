package com.singlee.hrbreport.mapper;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.LimitRisk;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/10/30 17:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface LimitRiskMapper {
    /**
     * delete by primary key
     * @param secmType primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String secmType);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(LimitRisk record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(LimitRisk record);

    /**
     * select by primary key
     * @param secmType primary key
     * @return object by primary key
     */
    LimitRisk selectByPrimaryKey(String secmType);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(LimitRisk record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(LimitRisk record);

    Page<LimitRisk> selectPageLimit(Map<String,Object> map, RowBounds rb);

    List<LimitRisk> selectPageLimit(Map<String,Object> map);


    List<Map<String,Object>> selectListLimit(Map<String,Object> map);

}