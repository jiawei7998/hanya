package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.refund.model.FundAfpReport;

public interface FundAfpReportMapper {
	
	/**
	 * 基金申购报表
	 * @param map
	 * @return
	 */
	List<FundAfpReport> searchFundAfpTradeCount(Map<String, Object> map);
	
}