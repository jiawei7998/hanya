package com.singlee.hrbreport.mapper;

import com.github.pagehelper.Page;

import java.util.Map;

/**
 * @author copysun
 */
public interface CommonExportMapper {

	/**
	 * 202XXXXX基金报表
	 * @param map
	 * @return
	 */
	Map<String,Object> getFundData(Map<String,Object> map);

	/**
	 *对公客户信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getCustPage(Map<String,Object> map);

	/**
	 * 同业存款基础信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankDepositsBasePage(Map<String,Object> map);


	/**
	 * 同业存款余额信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankDepositsBalancePage(Map<String,Object> map);


	/**
	 * 同业存款发生额信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankDepositsAmountPage(Map<String,Object> map);

	/**
	 * 同业借贷基础信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankBorrowBasePage(Map<String,Object> map);

	/**
	 * 同业借贷余额表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankBorrowBalancePage(Map<String,Object> map);

	/**
	 * 同业借贷发生额信息表
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> getInterbankBorrowAmountPage(Map<String,Object> map);





}
