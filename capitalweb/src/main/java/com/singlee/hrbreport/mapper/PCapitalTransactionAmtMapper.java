package com.singlee.hrbreport.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;

import com.singlee.hrbreport.model.PCapitalTransactionAmtVO;

public interface PCapitalTransactionAmtMapper {
    
 
    
   
    public List<PCapitalTransactionAmtVO> searchPCapitalTransactionAmt(Map<String, Object> map);

}
