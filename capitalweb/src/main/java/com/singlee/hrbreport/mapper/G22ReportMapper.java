package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G22ReportMapper {
	
	/**
	 * 拆入拆出本金
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportDlAmt(Map<String, String> map);
	
	/**
	 * 正逆回购本金
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportRepoAmt(Map<String, String> map);
	
	
	
	/**
	 * 债券的利息
	 * sqlType 1:债券的利息  
	 * 		   2:债券的利息 A H IM 
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportBond(Map<String, String> map);
	
	
	
	/**
	 * 拆放同业利息
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportDlRate(Map<String, String> map);
	
	/**
	 * 回购利息
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportRepoRate(Map<String, String> map);
	
	
	/**
	 * 一个月内到期的债券投资--债券 H A T
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportBondHat(Map<String, String> map);
	
	/**
	 * 一个月内到期的债券投资 --被质押
	 * sqlType 1:被质押
	 * 		   2:	
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportBondPledge(Map<String, String> map);

	/**
	 * 在国内外二级市场上可随时变现的证券投资（不包括项目1.7的有关项目）
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getG22ReportSec(Map<String, String> map);
}
