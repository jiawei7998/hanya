package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface CapitalTurnoverMapper {
    
      List<Map<String,Object>> searchInvest(Map<String,Object> map);


      List<Map<String,Object>> searchLiabilities(Map<String,Object> map);


      void  callCapitalTurnover(Map<String,Object> map);
}
