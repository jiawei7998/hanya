package com.singlee.hrbreport.mapper;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface ChinaRussiaReportMapper {
    
      List<Map<String,Object>> getsql1(Map<String,Object> map);
    
      List<Map<String,Object>> getsql2(Map<String,Object> map);
    
      List<Map<String,Object>> getsql3(Map<String,Object> map);
    
      List<Map<String,Object>> getsql4(Map<String,Object> map);
    
      List<Map<String,Object>> getsql5(Map<String,Object> map);
    
      List<Map<String,Object>> getsql6(Map<String,Object> map);
    
      List<Map<String,Object>> getsql7(Map<String,Object> map);
    
      List<Map<String,Object>> getsql8(Map<String,Object> map);

      void getChinaRussiaData(Map<String,Object> map);

      Page<Map<String,Object>> searchData(Map<String,Object> map, RowBounds rb);

      List<Map<String,Object>> searchDataList(Map<String,Object> map);


    
    

}
