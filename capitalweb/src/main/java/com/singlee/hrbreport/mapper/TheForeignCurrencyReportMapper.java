package com.singlee.hrbreport.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;


import com.singlee.hrbreport.model.TheForeignCurrencyVO;
import com.singlee.hrbreport.model.TheForeignCurrencyVO2;

public interface TheForeignCurrencyReportMapper {
    //本币同业拆入
    public  List<TheForeignCurrencyVO> getFsql1(Map<String,Object> map);
    
    //本币同业拆出
    public  List<TheForeignCurrencyVO> getFsql2(Map<String,Object> map);
    
    //外币同业存放
    public  List<TheForeignCurrencyVO2> getFsql3(Map<String,Object> map);
    
    //外币存放同业
    public  List<TheForeignCurrencyVO2> getFsql4(Map<String,Object> map);
    
    //外币同业拆入
    public  List<TheForeignCurrencyVO2> getFsql5(Map<String,Object> map);
    
    //外币拆放同业
    public  List<TheForeignCurrencyVO2> getFsql6(Map<String,Object> map);
    
    //本币同业存放
    public  List<TheForeignCurrencyVO> getFsql7(Map<String,Object> map);
    
    //本币存放同业
    public  List<TheForeignCurrencyVO> getFsql8(Map<String,Object> map);
    
    
    
  

}
