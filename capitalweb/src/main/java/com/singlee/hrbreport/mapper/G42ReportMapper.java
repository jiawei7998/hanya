package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G42ReportMapper {
	
	/**
	 * sqlType  1:对我国中央政府投资的公用企业的债权 33
	 * 			2:对其他公用企业的债权 c
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportSec(Map<String, String> map);
	
	/**
	 * sqlType  1:411
	 * 			2:612
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportRPRH(Map<String, String> map);
	
	/**
	 * sqlType  1:412
	 * 			2:611
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportDLDT(Map<String, String> map);
	
	/**
	 * 对我国商业银行的债权    不含次级债, 公共部门  
	 * 原始期限四个月以上  432
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportTradeRPRH(Map<String, String> map);
	
	/**
	 * 对我国商业银行的债权    不含次级债, 公共部门  
	 * 原始期限四个月以下 431
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportTradeDLDT(Map<String, String> map);
	
	/**
	 * 对我国其他金融机构的债权 
	 * sqlType    1:433
	 * 			  2:613
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportOtherSec(Map<String, String> map);
	
	/**
	 * eg
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG42ReportEG(Map<String, String> map);
	
	
}
