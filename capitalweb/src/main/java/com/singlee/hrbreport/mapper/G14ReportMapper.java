package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G14ReportMapper {
	
	public  List<Map<String,Object>> getG14Reportsql1(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG14Reportsql2(Map<String,Object> map);
	

	
	
}
