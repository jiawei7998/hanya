package com.singlee.hrbreport.mapper;

import java.util.Map;

/**
 * _房地产融资风险检测表
 * @author zk
 */
public interface EstateComSecMapper {
    public Map<String , Object> queryEstateMBSBalance(Map<String , Object> param);
    
    public Map<String , Object> queryEstateMBSLastBalance(Map<String , Object> param);
    
}
