package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G1102ReportMapper {
	
	public  List<Map<String,Object>> getG1102Reportsql1(Map<String,Object> map);
	//拆放同业
	public  List<Map<String,Object>> getG1102Reportsql2(Map<String,Object> map);
	//存放同业
	public  List<Map<String,Object>> getG1102Reportsql3(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG1102Reportsql4(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG1102Reportsql5(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG1102Reportsql5r(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG1102Reportsql5d(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG1102Reportsql6s(Map<String,Object> map);
	
	
}
