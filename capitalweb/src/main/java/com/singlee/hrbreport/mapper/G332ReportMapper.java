package com.singlee.hrbreport.mapper;

import java.util.Map;

public interface G332ReportMapper {
	
	/**
	 * 金融机构 部分
	 * sqlType	1：金融机构间融资形成的资产
	 * 			2：计息的各项贷款
	 * 			3：金融机构间融资形成的负债
	 * 			4：其他生息资产
	 * @param map
	 * @return
	 */
	public String getG332ReportCust(Map<String, String> map);
	
	/**
	 * 债券投资 部分
	 * @param map
	 * @return
	 */
	public String getG332ReportBond(Map<String, String> map);
	
	/**
	 * 利率掉期 部分
	 * @param map
	 * @return
	 */
	public String getG332ReportSwap(Map<String, String> map);
	
	/**
	 * 其他负债 部分
	 * @param map
	 * @return
	 */
	public String getG332ReportDebt(Map<String, String> map);
}
