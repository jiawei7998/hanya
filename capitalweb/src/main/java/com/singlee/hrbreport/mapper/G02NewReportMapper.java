package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G02NewReportMapper {
	
	public List<Map<String,Object>>   getSql1(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql2(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql3(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql4(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql5(Map<String,Object> map);
	
	public List<Map<String,Object>>   getSql6(Map<String,Object> map);
}
