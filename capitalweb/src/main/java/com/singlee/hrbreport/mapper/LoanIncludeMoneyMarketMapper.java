package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.LoanIncludeMoneyMarketVO;

public interface LoanIncludeMoneyMarketMapper {
    List<LoanIncludeMoneyMarketVO> getLastMonthLoanReportData(Map<String,Object> map);
    
    List<LoanIncludeMoneyMarketVO> getThisMonthLoanReportData(Map<String,Object> map);
    
    List<LoanIncludeMoneyMarketVO> getTeroReportData(Map<String,Object> map);
    
    List<LoanIncludeMoneyMarketVO> getThisMonthBalanceReportData(Map<String,Object> map);
    
    List<LoanIncludeMoneyMarketVO> getThisMonthInterestReportData(Map<String,Object> map);
    
    String getStaticFiled(Map<String,Object> map);
    
    String getCoun(Map<String,Object> map);
    
    List<Map<String,Object>> queryHoliday(Map<String,Object> map);
    
}
