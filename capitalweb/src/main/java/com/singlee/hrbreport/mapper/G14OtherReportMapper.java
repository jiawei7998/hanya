package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.G14OtherVO;
/**
 * _大额风险暴露统计表
 * @author zk
 */
public interface G14OtherReportMapper {
    
    public List<G14OtherVO> queryUnSameList(Map<String , Object> map);
    
    public List<G14OtherVO> querySameList(Map<String , Object> map);
    
}
