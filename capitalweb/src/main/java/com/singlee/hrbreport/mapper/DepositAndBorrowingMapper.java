package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.DepositAndBorrowingDTO;

/**
 * _存款含银行同业和联行存放负债
 * @author zk
 *
 */
public interface DepositAndBorrowingMapper {
    
    List<DepositAndBorrowingDTO> querySql1(Map<String,Object> map);
    
    List<DepositAndBorrowingDTO> querySqlNow(Map<String,Object> map);
    
    List<DepositAndBorrowingDTO> querySql2(Map<String,Object> map);
    
    List<DepositAndBorrowingDTO> querySql3(Map<String,Object> map);
    
    List<DepositAndBorrowingDTO> querySql4(Map<String,Object> map);
    
    List<DepositAndBorrowingDTO> queryAcctnoSql(Map<String,Object> map);
    
    String getStaticFiled(Map<String,Object> map);
    
    String getCoun(Map<String,Object> map);
    
    List<Map<String,Object>> queryHoliday(Map<String,Object> map);
    
    List<Map<String , Object>> getNo(Map<String ,Object> map);
    
    List<Map<String , Object>> getBranchUpload(Map<String ,Object> map);
    
    List<Map<String , Object>> getGroup(Map<String ,Object> map);
}
