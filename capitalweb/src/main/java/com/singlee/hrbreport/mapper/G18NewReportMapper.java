package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G18NewReportMapper {
	
	public  List<Map<String,Object>> getBalance(Map<String,Object> map);
	
	public  List<Map<String,Object>> getLimitBlY1(Map<String,Object> map);
	
	public  List<Map<String,Object>> getLimitBlY5(Map<String,Object> map);
	
	public  List<Map<String,Object>> getLimitBlY10(Map<String,Object> map);
	
	public  List<Map<String,Object>> getLimitBlY10D(Map<String,Object> map);
	
	
	

	
	
	
	
	
}
