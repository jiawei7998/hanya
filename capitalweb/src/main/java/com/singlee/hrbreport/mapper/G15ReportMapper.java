package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G15ReportMapper {
	
	public  List<Map<String,Object>> getG15Reportsql1(Map<String,Object> map);
	
	public  List<Map<String,Object>> getG15Reportsql2(Map<String,Object> map);
	

	
	
}
