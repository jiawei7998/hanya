package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.hrbreport.model.TposPo;

public interface TposReportMapper {
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<TposPo> getTposReportPage(Map<String, String> map);
	
}
