package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface S60ReportMapper {
	
	public  List<Map<String,Object>> getS60ReporCost(Map<String,Object> map);
	
	public  List<Map<String,Object>> getS60ReporCostf(Map<String,Object> map);
	
	
}
