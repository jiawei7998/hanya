package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G2502ReportMapper {
    public List<Map<String,Object>> countShortAndLCRFirstSecurity(Map<String,Object> map);
    
    public List<Map<String,Object>> countGuaranteeForRepo(Map<String,Object> map);
    
    public List<Map<String,Object>> countOtherBond(Map<String,Object> map);
    
    public List<Map<String,Object>> countOtherLoan(Map<String,Object> map);
}
