package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.G24Po;

public interface G24ReportMapper {

	/**
	 * AMT1--同业拆借
	 * AMT2--同业借款
	 * AMT3--同业存放
	 * AMT4--卖出回购
	 * AMT5--委托方同业代付
	 * AMT6--结算性同业存款
	 * AMT7--发行的同业存单
	 * @param map
	 * @return
	 */
	public List<G24Po> getG24ReportDldt(Map<String, String> map);
	
}
