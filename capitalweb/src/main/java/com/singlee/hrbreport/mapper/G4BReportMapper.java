package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G4BReportMapper {
    
    public List<Map<String,Object>> countCreditRight(Map<String,Object> map);//sql1
    
    public List<Map<String,Object>> countCreditRightFromPublicDepartment(Map<String,Object> map);//sql2
    
    public List<Map<String,Object>> countLendingPrincipalFromPolicyBank(Map<String,Object> map);//sql41
    
    public List<Map<String,Object>> countReverseRepoPrincipalFromPolicyBank(Map<String,Object> map);//sql42
    
    public List<Map<String,Object>> countBondPrincipalAndInterestFromPolicyBank(Map<String,Object> map);//sql43
    
    public List<Map<String,Object>> countSubCreditRightFromPolicyBank(Map<String,Object> map);//V412
    
    public List<Map<String,Object>> countLendingPrincipalFromComercialBank(Map<String,Object> map);//sql431
    
    public List<Map<String,Object>> countReverseRepoPrincipalFromComercialBank(Map<String,Object> map);//sql432
    
    public List<Map<String,Object>> countBondPrincipalAndInterestFromComercialBank(Map<String,Object> map);//sql433
    
    public List<Map<String,Object>> countSubCreditRightFromComercialBank(Map<String,Object> map);//sql44
    
    public List<Map<String,Object>> countReverseRepoPrincipalFromOther(Map<String,Object> map);//sql4561
    
    public List<Map<String,Object>> countLendingPrincipalFromOther(Map<String,Object> map);//sql4562
    
    public List<Map<String,Object>> countBondPrincipalAndInterestFromOther(Map<String,Object> map);//sql4563
    
    public List<Map<String,Object>> countEG(Map<String,Object> map);//sqlEG
    
}
