package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.CurAssetVO;
/**
 * _优质流动性资产台账
 * @author zk
 */
public interface CurAssetMapper {
    
    public List<CurAssetVO> queryCurrentAssets(Map<String , Object> map);
    
}
