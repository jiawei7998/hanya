package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G21ReportMapper {
	
	public  List<Map<String,Object>> getTmp(Map<String,Object> map);
	
	public  List<Map<String,Object>> getCCYAMT(Map<String,Object> map);
	
	public  List<Map<String,Object>> getNOTAMT(Map<String,Object> map);
	
	public  List<Map<String,Object>> getPSNOTAMT(Map<String,Object> map);
	
	public  List<Map<String,Object>> getAA(Map<String,Object> map);
	
}
