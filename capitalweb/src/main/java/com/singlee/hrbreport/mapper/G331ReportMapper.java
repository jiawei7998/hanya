package com.singlee.hrbreport.mapper;

import java.util.Map;

public interface G331ReportMapper {
	
	/**
	 * 债券投资部分
	 * @param map
	 * @return
	 */
	public String getG331ReportBond(Map<String, String> map);
	
	/**
	 * 利率掉期部分
	 * @param map
	 * @return
	 */
	public String getG331ReportSwap(Map<String, String> map);
}
