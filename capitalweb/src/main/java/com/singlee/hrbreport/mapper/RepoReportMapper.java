package com.singlee.hrbreport.mapper;

import com.singlee.hrbreport.model.RepoPo;
import com.singlee.hrbreport.model.RepoSecPo;

import java.util.List;
import java.util.Map;

public interface RepoReportMapper {
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<RepoPo> getRepoReportPage(Map<String, String> map);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<RepoSecPo> getRepoSecPoList(Map<String, String> map);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> getRateList(Map<String, String> map);
}
