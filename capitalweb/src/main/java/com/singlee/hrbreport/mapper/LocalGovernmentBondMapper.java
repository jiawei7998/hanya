package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;

public interface LocalGovernmentBondMapper {
    
//    public Page<LocalGovernmentBondVO> getBondList(Map<String, Object> map,RowBounds rowBounds);
   
    
  public List<LocalGovernmentBondVO> getBondList(Map<String, Object> map);
}
