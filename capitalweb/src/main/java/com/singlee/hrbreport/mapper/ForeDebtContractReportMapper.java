package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.page.PageInfo;
import com.singlee.hrbreport.model.ForeDebtContractVO;

public interface ForeDebtContractReportMapper {
    
	public  List<Map<String,Object>> getGReportsql1(Map<String,Object> map);
	
	public  List<Map<String,Object>> getGReportsql2(Map<String,Object> map);
	
	void  insertsql(Map<String,Object> map);
	
	void insert2(Map<String,Object> map);
	
    void delete(Map<String,Object> map);
    
    void update1(Map<String,Object> map);
    
    
    void update3(Map<String,Object> map);
     
   
    public  List<Map<String,Object>> getGReportsql3(Map<String,Object> map);
    
	public  List<Map<String,Object>> getGReportsql4(Map<String,Object> map);
	

	//查询债券余额报表
	public  List<ForeDebtContractVO> getGReportsql6(Map<String,Object> map,RowBounds rowBounds);
	
	public  List<Map<String,Object>> getGReportsql7(Map<String,Object> map);
	
	public  List<Map<String,Object>> getGReportsql8(Map<String,Object> map);
	
	public  List<Map<String,Object>> getGReportsql9(Map<String,Object> map);
	
	
	
	public  List<Map<String,Object>> getStaticFiled(Map<String,Object> map);
	
	public  String getCoun(Map<String,Object> map);
	
	public  String getCountry(Map<String,Object> map);
	
	public  String getCountry2(Map<String,Object> map);

	
	
}
