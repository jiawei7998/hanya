package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G31ReportMapper {
	
	/**
	 * 债券投资部分
	 * sqlType  1:对于secm中ratecode为空的记录，利息类型为定息VARFIX = 'F'
	 * 			2:定息债券投资
	 * 			3:浮息债券投资
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportBond(Map<String, String> map);
	
	/**
	 * 历史成本(摊余成本）
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportHamt(Map<String, String> map);
	
	/**
	 * 新版本债券投资部分 穿透前
	 * sqlType 1：国内债券
	 * 		   2：国外债券
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportBondTypeD(Map<String, String> map);
	
	/**
	 * 新版本债券投资部分 穿透后
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportBondTypeF(Map<String, String> map);
	
	/**
	 * 新版本债券评级部分 穿透前
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportBondLevelD(Map<String, String> map);
	
	/**
	 * 新版本债券评级部分 穿透后
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportBondLevelF(Map<String, String> map);
	
	/**
	 * 基金投资部分
	 * sqlType 1:货币基金
	 * 		   2：债券基金
	 * @param map
	 * @return
	 */
	public List<Map<String,  Object>> getG31ReportFund(Map<String, String> map);
}
