package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.InBankDealBrLoVO;
import com.singlee.hrbreport.model.InBankDealCDSeVO;
import com.singlee.hrbreport.model.InBankDealFundVO;
import com.singlee.hrbreport.model.InBankDealRepoVO;
import com.singlee.hrbreport.model.InBankDealSecVO;

public interface InBankDealMapper {
    
    public List<InBankDealRepoVO> queryRepoDetail(Map<String, Object> param);
    
    public List<String> queryPledge(Map<String, Object> param);
    
    public List<InBankDealSecVO> querySecPosDetail(Map<String, Object> param);
    
    public List<InBankDealSecVO> querySecCDPosDetail(Map<String, Object> param);
    
    public List<InBankDealBrLoVO> queryBrLoDetail(Map<String, Object> param);
    
    public List<InBankDealCDSeVO> queryCDSEDetail(Map<String, Object> param);
    
    public List<InBankDealCDSeVO> queryRHCDSEDetail(Map<String, Object> param);
    
    public List<InBankDealFundVO> queryFundDetail(Map<String, Object> param);
    
}
