package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

public interface G2501ReportMapper {

	public List<Map<String,Object>> countNoRiskSecurity(Map<String,Object> map);
	public List<Map<String,Object>> countSecLevelAsset(Map<String,Object> map);
	public List<Map<String,Object>> countExpireSecurity(Map<String,Object> map);
	public List<Map<String,Object>> countGuarantee(Map<String,Object> map);
	public List<Map<String,Object>> countGuaranteeWithCB(Map<String,Object> map);
	public List<Map<String,Object>> countGuaranteeBy2B(Map<String,Object> map);
	public List<Map<String,Object>> countGuaranteeByOthers(Map<String,Object> map);
	public List<Map<String,Object>> countOtherGuarantee(Map<String,Object> map);
	public List<Map<String,Object>> countDeposit(Map<String,Object> map);
	
}
