package com.singlee.hrbreport.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName ManageMonthMapper.java
 * @Description 累计经营月报
 * @createTime 2021年10月11日 14:14:00
 */
public interface ManageMonthMapper {

    List<Map<String, Object>> TopsSql(Map<String, Object> map);

    List<Map<String, Object>> getTopMax(Map<String, Object> map);

    List<Map<String, Object>> avgSql(Map<String, Object> map);

    List<Map<String, Object>> repoSql(Map<String, Object> map);

    List<Map<String, Object>> dldtSql(Map<String, Object> map);
    
    List<Map<String, Object>> spshSql(Map<String, Object> map);

}
