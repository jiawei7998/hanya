package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;

import com.singlee.hrbreport.model.G14OtherVO;
/**
 * _大额风险暴露统计表
 * @author zk
 */
public interface G14OtherReportService extends ReportService {
    public List<G14OtherVO> getRportList(Map<String, Object> paramer);
}
