package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.TheForeignCurrencyReportMapper;
import com.singlee.hrbreport.model.TheForeignCurrencyVO;
import com.singlee.hrbreport.model.TheForeignCurrencyVO2;
import com.singlee.hrbreport.service.TheForeignCurrencyReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 哈尔滨银行本外币同业台账
 * @author cg
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TheForeignCurrencyReportServiceImpl implements TheForeignCurrencyReportService {

    @Autowired
    TheForeignCurrencyReportMapper foreign;

    @Override
  //本币-同业拆入
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList1(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
 
        paraMap.put("OPICS", SystemProperties.opicsUserName);
        paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        return foreign.getFsql1(paraMap);
    }

    @Override
  //本币-同业拆出
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList2(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql2(paraMap);
    }

    @Override
  //外币-同业存放
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList3(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql3(paraMap);
    }

    @Override
    //外币-存放同业
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList4(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql4(paraMap);
    }

    @Override
  //外币-同业拆入
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList5(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql5(paraMap);
    }

    @Override
    //外币-拆放同业
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList6(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql6(paraMap);
    }

   
  

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> mapEnd=new HashMap<>();
       
        
        List<TheForeignCurrencyVO> list1 =selectTheForeignCurrencyList1(paramer);
           
       
   
        List<TheForeignCurrencyVO> list2 =selectTheForeignCurrencyList2(paramer);
            
     
      
        List<TheForeignCurrencyVO> list7 =selectTheForeignCurrencyList7(paramer);
          
      
       
        List<TheForeignCurrencyVO> list8 =selectTheForeignCurrencyList8(paramer);
          
      
        List<TheForeignCurrencyVO2> list3 =selectTheForeignCurrencyList3(paramer);
          

        List<TheForeignCurrencyVO2> list4 =selectTheForeignCurrencyList4(paramer);
           
        
        List<TheForeignCurrencyVO2> list5 =selectTheForeignCurrencyList5(paramer);
          
        
        List<TheForeignCurrencyVO2> list6 =selectTheForeignCurrencyList6(paramer);
        mapEnd.put("list1", list1);
        mapEnd.put("list2", list2);
        mapEnd.put("list3", list3);
        mapEnd.put("list4", list4);
        mapEnd.put("list5", list5);
        mapEnd.put("list6", list6);
        mapEnd.put("list7", list7);
        mapEnd.put("list8", list8);
        
       
        return mapEnd;
    }

    @Override
  //本币-同业存放
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList7(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql7(paraMap);
    }

    @Override
  //本币-存放同业
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList8(Map<String, Object> param) {
        Map<String , Object> paraMap = new HashMap<String,Object>();
        
        
        paraMap.put("OPICS", SystemProperties.opicsUserName);
         paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
        
        return foreign.getFsql8(paraMap);
    }

    @Override
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency1(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO> list1 = selectTheForeignCurrencyList1(param);
      
        Page <TheForeignCurrencyVO> page1 =HrbReportUtils.producePage(list1, pageNum, pageSize);
        return page1;
    }

    @Override
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency2(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO> list2 = selectTheForeignCurrencyList2(param);
      
        Page <TheForeignCurrencyVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency3(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO2> list2 = selectTheForeignCurrencyList3(param);
        
        Page <TheForeignCurrencyVO2> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency4(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
  List<TheForeignCurrencyVO2> list2 = selectTheForeignCurrencyList4(param);
        
        Page <TheForeignCurrencyVO2> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency5(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO2> list2 = selectTheForeignCurrencyList5(param);
        
        Page <TheForeignCurrencyVO2> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency6(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO2> list2 = selectTheForeignCurrencyList6(param);
        
        Page <TheForeignCurrencyVO2> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency7(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO> list2 = selectTheForeignCurrencyList7(param);
        
        Page <TheForeignCurrencyVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency8(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<TheForeignCurrencyVO> list2 = selectTheForeignCurrencyList8(param);
        
        Page <TheForeignCurrencyVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    

}
