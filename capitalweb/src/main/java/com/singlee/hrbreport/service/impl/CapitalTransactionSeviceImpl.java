package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.CapitalTransactionMapper;
import com.singlee.hrbreport.model.CapitalTransactionVO;
import com.singlee.hrbreport.service.CapitalTransactionService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * 资金交易情况
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CapitalTransactionSeviceImpl implements CapitalTransactionService  {

    @Autowired
    CapitalTransactionMapper ca;

    @Override
    public List<CapitalTransactionVO> selectGovernmentBondList(Map<String, Object> param) {
        HashMap<String,Object> remap =new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName);
        remap.put("HRBCB", SystemProperties.hrbcbUserName);
    
        return ca.searchCapitalTransaction(remap);
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        HashMap<String,Object> mapEnd =new HashMap<>();
        List<CapitalTransactionVO> list =selectGovernmentBondList(paramer);
        mapEnd.put("list", list);
        return mapEnd;
    }


    @Override
    public Page<CapitalTransactionVO> selectGovernmentBond(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<CapitalTransactionVO> list2 = selectGovernmentBondList(param);
      
        Page <CapitalTransactionVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }
}
    
    
    
    

