package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G331利率重新定价风险情况报表第1部分：交易账户
 *
 */
public interface G331ReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
}
