package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G4BReportMapper;
import com.singlee.hrbreport.service.G4BReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G4B-1表内信用风险加权资产计算表（权重法）
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G4BReportServiceImpl implements G4BReportService {

    @Autowired
    G4BReportMapper g4b;
    
    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        HashMap<String,Object> mapEnd = new HashMap<String,Object>();
        String br = paramer.get("BR");
        String RPTDate = HrbReportUtils.getDate(postdate);
        String SRPTDate = HrbReportUtils.dateToMap(postdate);

        mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
        mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
        mapEnd.put("RPTDate", SRPTDate);//报表日期
        
        Map<String , Object> paraMap = new HashMap<String,Object>();
        paraMap.put("BR", br);
        paraMap.put("RPTDate", RPTDate);
        paraMap.put("OPICS", paramer.get("OPICS") ); 
        paraMap.put("HRBCB", paramer.get("HRBCB") ); 
        
        
        //债权
        List<Map<String,Object>> list = g4b.countCreditRight(paraMap);
        Object V21=0.00;
        Object V22=0.00;
        Object V32=0.00;    
        if(list.size()>0&&list!=null){
            for (Map<String, Object> map : list) {//DOMLOGOVSE
                    if("DOMGOVSE".equals(map.get("ACCTNGTYPE").toString())){
                        V21=map.get("VAL");
                    }
                    if("DOMCENBKSE".equals(map.get("ACCTNGTYPE").toString())){
                        V22=map.get("VAL");
                    }        
                    if("DOMLOGOVSE".equals(map.get("ACCTNGTYPE").toString())){
                        V32=map.get("VAL");
                    }  
                    mapEnd.put("V21",(new BigDecimal(V21.toString()).add(new BigDecimal(V32.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
                    mapEnd.put("V22",new BigDecimal(V22.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
                    mapEnd.put("V32",new BigDecimal(V32.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
            }
        }else{
            mapEnd.put("V21",V21);
            mapEnd.put("V22",V22);
            mapEnd.put("V32",V32);
        }
        
        //公共部门发行的债券
        List<Map<String, Object>> list2 =  g4b.countCreditRightFromPublicDepartment(paraMap);
        if(list2!=null&&list2.size() > 0&&list2.get(0) != null && list2.get(0).get("VAL")!=null){
               mapEnd.put("V312",new BigDecimal(list2.get(0).get("VAL").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        }else {
               mapEnd.put("V312",0.00);
        }
        //拆出本金(政策新银行)
        List<Map<String, Object>> list41 =  g4b.countLendingPrincipalFromPolicyBank(paraMap);
        //逆回购本金(政策新银行)
        List<Map<String, Object>> list42 =  g4b.countReverseRepoPrincipalFromPolicyBank(paraMap);
        //债券本金及应计利息(政策新银行)
        List<Map<String, Object>> list43 =  g4b.countBondPrincipalAndInterestFromPolicyBank(paraMap);
        Object V41=0.00;    Object V42=0.00;    Object V43=0.00;
        if(list41!=null&&list41.size() > 0&&list41.get(0) != null && list41.get(0).get("VAL")!=null){
            V41=list41.get(0).get("VAL");
        }
        if(list42!=null&&list42.size() > 0&&list42.get(0) != null && list42.get(0).get("VAL")!=null){
            V42=list42.get(0).get("VAL");
        }
        if(list43!=null&&list43.size() > 0&&list43.get(0) != null && list43.get(0).get("VAL")!=null){
            V43=list43.get(0).get("VAL");
        }
        mapEnd.put("V411",(new BigDecimal(V41.toString()).add(new BigDecimal(V42.toString())).add(new BigDecimal(V43.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        //对政策性银行的次级债权
        List<Map<String, Object>> list412 =  g4b.countSubCreditRightFromPolicyBank(paraMap);
        Object V412V=0.00;
        if(list412!=null&&list412.size() > 0&&list412.get(0) != null && list412.get(0).get("VAL")!=null){
            mapEnd.put("V412",new BigDecimal(list412.get(0).get("VAL").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
            V412V=list412.get(0).get("VAL");
        }else{
            mapEnd.put("V412",0.00);
        }
        mapEnd.put("V41",new BigDecimal(V41.toString()).add(new BigDecimal(V42.toString()).add(new BigDecimal(V43.toString())).add(new BigDecimal(V412V.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        //拆出本金(商业银行)
        List<Map<String, Object>> list431 =  g4b.countLendingPrincipalFromComercialBank(paraMap);
        //逆回购本金(商业银行)
        List<Map<String, Object>> list432 =  g4b.countReverseRepoPrincipalFromComercialBank(paraMap);
        //债券本金及应计利息(商业银行)
        List<Map<String, Object>> list433 =  g4b.countBondPrincipalAndInterestFromComercialBank(paraMap);
        Object V4311=0.00;  Object V4321=0.00;  Object V4331=0.00;
        Object V4312=0.00;  Object V4322=0.00;  Object V4332=0.00;
        if(list431.size()>0&&list431!=null){
            for (Map<String, Object> map : list431) {
                if(map == null) {
                    continue;
                }
                if(map.get("TENOR")!=null&&map.get("VAL")!=null){
                    if("1".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="1"){
                        V4311=map.get("VAL");
                    }
                    if("2".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="2"){
                        V4312=map.get("VAL");
                    }  
                }
            }
        }
         
        if(list432.size()>0&&list432!=null){
            for (Map<String, Object> map : list432) {
                if(map == null) {
                    continue;
                }
                if(map.get("TENOR")!=null&&map.get("VAL")!=null){
                    if("1".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="1"){
                         V4321=map.get("VAL");
                    }
                    if("2".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="2"){
                        V4322=map.get("VAL");
                    }
                }
            }
        }
        
        if(list433.size()>0&&list433!=null){
            for (Map<String, Object> map : list433) {
                if(map == null) {
                    continue;
                }
                if(map.get("TENOR")!=null&&map.get("VAL")!=null){
                    if("1".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="1"){
                        V4331=map.get("VAL");
                    }
                    if("2".equals(map.get("TENOR").toString())||map.get("TENOR").toString()=="2"){
                        V4332=map.get("VAL");
                    } 
                }
            }
        }
        mapEnd.put("V431",(new BigDecimal(V4311.toString()).add(new BigDecimal(V4321.toString())).add(new BigDecimal(V4331.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("V432",(new BigDecimal(V4312.toString()).add(new BigDecimal(V4322.toString())).add(new BigDecimal(V4332.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("V43",(new BigDecimal(V4311.toString()).add(new BigDecimal(V4321.toString())).add(new BigDecimal(V4331.toString())).add(new BigDecimal(V4312.toString())).add(new BigDecimal(V4322.toString())).add(new BigDecimal(V4332.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        
        //对商业银行的次级债权
        List<Map<String, Object>> list44 =  g4b.countSubCreditRightFromComercialBank(paraMap);
        if(list44!=null&&list44.size() > 0&&list44.get(0) != null && list44.get(0).get("VAL")!=null){
            mapEnd.put("V44",new BigDecimal(list44.get(0).get("VAL").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        }else{
            mapEnd.put("V44",0.00);
        }
        
        //逆回购本金(其他)
        List<Map<String, Object>> list4561 =  g4b.countReverseRepoPrincipalFromOther(paraMap);
        //拆出本金(其他)
        List<Map<String, Object>> list4562 =  g4b.countLendingPrincipalFromOther(paraMap);
        //债券本金及应计利息(其他)
        List<Map<String, Object>> list4563 =  g4b.countBondPrincipalAndInterestFromOther(paraMap);
        BigDecimal bal45=new BigDecimal(0.00);
        BigDecimal bal5=new BigDecimal(0.00);
        BigDecimal bal6=new BigDecimal(0.00);
        if(list4561.size()>0&&list4561!=null){
            for (Map<String, Object> map : list4561) {
                if(map == null) {
                    continue;
                }
                if(map.get("TYPE")!=null){
                    if("1".equals(map.get("TYPE").toString())){
                        bal45=bal45.add(new BigDecimal(map.get("VAL").toString()));  
                    }
                    if("2".equals(map.get("TYPE").toString())){
                        bal5=bal5.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                    if("3".equals(map.get("TYPE").toString())){
                        bal6=bal6.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                }
            }
        }
        if(list4562.size()>0&&list4562!=null){
            for (Map<String, Object> map : list4562) {
                if(map == null) {
                    continue;
                }
                if(map.get("TYPE")!=null){
                    if("1".equals(map.get("TYPE").toString())){
                        bal45=bal45.add(new BigDecimal(map.get("VAL").toString()));  
                    }
                    if("2".equals(map.get("TYPE").toString())){
                        bal5=bal5.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                    if("3".equals(map.get("TYPE").toString())){
                        bal6=bal6.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                }
            }
        }
        if(list4563.size()>0&&list4563!=null){
            for (Map<String, Object> map : list4563) {
                if(map == null) {
                    continue;
                }
                if(map.get("TYPE")!=null){
                    if("1".equals(map.get("TYPE").toString())){
                        bal45=bal45.add(new BigDecimal(map.get("VAL").toString()));  
                    }
                    if("2".equals(map.get("TYPE").toString())){
                        bal5=bal5.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                    if("3".equals(map.get("TYPE").toString())){
                        bal6=bal6.add(new BigDecimal(map.get("VAL").toString()));
                    } 
                }
                
            }
        }
        mapEnd.put("V45", bal45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("V5", bal5.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("V6", bal6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        
        List<Map<String, Object>> listEG =  g4b.countEG(paraMap);
        
        BigDecimal BALE411=new  BigDecimal(0.00);
        BigDecimal BALF411=new  BigDecimal(0.00);
        BigDecimal BALG411=new  BigDecimal(0.00);
        BigDecimal BALH411=new  BigDecimal(0.00);
        BigDecimal BALI411=new  BigDecimal(0.00);
        
        BigDecimal BALE412=new  BigDecimal(0.00);
        BigDecimal BALF412=new  BigDecimal(0.00);
        BigDecimal BALG412=new  BigDecimal(0.00);
        BigDecimal BALH412=new  BigDecimal(0.00);
        BigDecimal BALI412=new  BigDecimal(0.00);
        
        BigDecimal BALE431=new  BigDecimal(0.00);
        BigDecimal BALF431=new  BigDecimal(0.00);
        BigDecimal BALG431=new  BigDecimal(0.00);
        BigDecimal BALH431=new  BigDecimal(0.00);
        BigDecimal BALI431=new  BigDecimal(0.00);
        
        BigDecimal BALE432=new  BigDecimal(0.00);
        BigDecimal BALF432=new  BigDecimal(0.00);
        BigDecimal BALG432=new  BigDecimal(0.00);
        BigDecimal BALH432=new  BigDecimal(0.00);
        BigDecimal BALI432=new  BigDecimal(0.00);

        BigDecimal BALE44=new  BigDecimal(0.00);
        BigDecimal BALF44=new  BigDecimal(0.00);
        BigDecimal BALG44=new  BigDecimal(0.00);
        BigDecimal BALH44=new  BigDecimal(0.00);
        BigDecimal BALI44=new  BigDecimal(0.00);
        
        BigDecimal BALE45=new  BigDecimal(0.00);
        BigDecimal BALF45=new  BigDecimal(0.00);
        BigDecimal BALG45=new  BigDecimal(0.00);
        BigDecimal BALH45=new  BigDecimal(0.00);
        BigDecimal BALI45=new  BigDecimal(0.00);
        
        BigDecimal BALE6=new  BigDecimal(0.00);
        BigDecimal BALF6=new  BigDecimal(0.00);
        BigDecimal BALG6=new  BigDecimal(0.00);
        BigDecimal BALH6=new  BigDecimal(0.00);
        BigDecimal BALI6=new  BigDecimal(0.00);
        
        
        if(listEG.size()>0&&listEG!=null){
            for (Map<String, Object> map : listEG) {
                if(map == null) {
                    continue;
                }
                if(map.get("ACCTNGTYPE")!=null&&map.get("TENOR")!=null&&map.get("BAL")!=null){
                    if("POLICYBK".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE411=BALE411.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF411=BALF411.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG411=BALG411.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH411=BALH411.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI411=BALI411.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("POLICYBKC".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE412=BALE412.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF412=BALF412.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG412=BALG412.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH412=BALH412.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI412=BALI412.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("COMMBK-DL".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE431=BALE431.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF431=BALF431.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG431=BALG431.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH431=BALH431.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI431=BALI431.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("COMMBK-DM".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE432=BALE432.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF432=BALF432.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG432=BALG432.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH432=BALH432.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI432=BALI432.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("COMMBK-DC".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE44=BALE44.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF44=BALF44.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG44=BALG44.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH44=BALH44.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI44=BALI44.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("OTHER".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE45=BALE45.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF45=BALF45.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG45=BALG45.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH45=BALH45.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI45=BALI45.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                    if("ENTERPRISE".equals(map.get("ACCTNGTYPE").toString())){
                        if("1".equals(map.get("TENOR").toString())){
                            BALE6=BALE6.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("2".equals(map.get("TENOR").toString())){
                            BALF6=BALF6.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("3".equals(map.get("TENOR").toString())){
                            BALG6=BALG6.add(new BigDecimal(map.get("BAL").toString()));
                        }
                        if("4".equals(map.get("TENOR").toString())){
                            BALH6=BALH6.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                        if("5".equals(map.get("TENOR").toString())){
                            BALI6=BALI6.add(new BigDecimal(map.get("BAL").toString()));
                        } 
                    }
                }
            }
        }
        mapEnd.put("BALE411", BALE411.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF411", BALF411.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG411", BALG411.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH411", BALH411.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI411", BALH411.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE412", BALE412.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF412", BALF412.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG412", BALG412.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH412", BALH412.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI412", BALH412.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE431", BALE431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF431", BALF431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG431", BALG431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH431", BALH431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI431", BALH431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE432", BALE432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF432", BALF432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG432", BALG432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH432", BALH432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI432", BALH432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE44", BALE44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF44", BALF44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG44", BALG44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH44", BALH44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI44", BALH44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE45", BALE45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF45", BALF45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG45", BALG45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH45", BALH45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI45", BALH45.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        mapEnd.put("BALE6", BALE6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALF6", BALF6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALG6", BALG6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("BALH6", BALH6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));           
        mapEnd.put("BALI6", BALH6.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        return mapEnd;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }

}
