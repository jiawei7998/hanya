package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.RepoPo;

import java.util.Date;
import java.util.Map;

/**
 * 回购台账报表
 *
 */
public interface RepoReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
	/**
	 * @param paramer
	 * @return
	 */
	public Page<RepoPo>  getRepoReportPage(Map<String, String> paramer) ;
}
