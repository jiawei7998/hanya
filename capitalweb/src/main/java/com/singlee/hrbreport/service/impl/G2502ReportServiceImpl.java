package com.singlee.hrbreport.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G2502ReportMapper;
import com.singlee.hrbreport.service.G2502ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
/**
 * G25流动性覆盖率和净稳定资金比例情况表
 * @author zk
 *
 */
@Service
public class G2502ReportServiceImpl implements G2502ReportService {

    @Autowired
    G2502ReportMapper g2502;
    
    public Map<String, Object> getReport(Map<String, Object> paramer, Date postdate) {
        HashMap<String,Object> mapEnd = new HashMap<String,Object>();
        String br = ParameterUtil.getString(paramer,"BR",SlSessionHelper.getUser().getOpicsBr());
        String RPTDate = HrbReportUtils.getDate(postdate);
        String SRPTDate = HrbReportUtils.dateToMap(postdate);

        mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
        mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
        mapEnd.put("RPTDate", SRPTDate);//报表日期
        
        Map<String , Object> paraMap = new HashMap<String,Object>();
        paraMap.put("BR", br);
        paraMap.put("RPTDate", RPTDate);
        paraMap.put("OPICS", paramer.get("OPICS") ); 
        paraMap.put("HRBCB", paramer.get("HRBCB") ); 
        
        // 货币市场工具、短期工具和LCR一级资产定义的证券
        countShortAndLCRFirstSecurity(paraMap,mapEnd);
        
        // 买断式逆回购中作为抵押品的证券    担保借款和负债
        countGuaranteeForRepo(paraMap,mapEnd);
        
        // 其他债券
        countOtherBond(paraMap,mapEnd);
        
        // 金融机构及其他负债    金融机构贷款    
        countOtherLoan(paraMap,mapEnd);
        
        return mapEnd;
    }




    /**
     * 2. 货币市场工具和短期无担保工具（到期日＜1年）  RPT1
       5.1 符合LCR一级资产定义的证券                               RPT4(<1年小计)    RPT5(>=1年或无到期日)
     * @param paraMap
     * @param mapEnd
     */
    private void countShortAndLCRFirstSecurity(Map<String, Object> paraMap, HashMap<String, Object> mapEnd) {
        Object ORPT1=0.00; //小于一年
        Object ORPT12=0.00; //大于一年
        Object ORPT51=0.00;//  //小于一年--   5.1 符合LCR一级资产定义的证券  账面余额（债券--地方政府债  \债券--政策性金融债）
        Object ORPT52=0.00; //大于一年
        List<Map<String,Object>> list = g2502.countShortAndLCRFirstSecurity(paraMap);
        if(list!=null&&list.size()>0){
            for (Map<String, Object> map : list) {
                if(map== null ) {
                    continue;
                }
                if("GOV".equals(map.get("SECMTYPE"))&& "LESS".equals(map.get("TYPE"))){
                    ORPT1=map.get("AMT");
                }
                if("GOV".equals(map.get("SECMTYPE"))&& "MORE".equals(map.get("TYPE"))){
                    ORPT12=map.get("AMT");
                }
                if("LGOV".equals(map.get("SECMTYPE"))&& "LESS".equals(map.get("TYPE"))){
                    ORPT51=map.get("AMT");
                }
                if("LGOV".equals(map.get("SECMTYPE"))&& "MORE".equals(map.get("TYPE"))){
                    ORPT52=map.get("AMT");
                }
                
            }
        }
        mapEnd.put("RPT1", new BigDecimal(ORPT1.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));//小于一年 国债、央票 
        mapEnd.put("RPT4", (new BigDecimal(ORPT51.toString()).add(new BigDecimal(ORPT1.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));//小于一年债券--地方政府债  \债券--政策性金融债       
        mapEnd.put("RPT5",(new BigDecimal(ORPT52.toString()).add(new BigDecimal(ORPT12.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );//大于一年  ：债券--国内中央银行\债券--国债  \债券--地方政府债  \债券--政策性金融债
        
    }
    
    /**
     *  3. 买断式逆回购中作为抵押品的证券     RPT2       RPT3
        9. 担保借款和负债                   ORPTRBP2  ORPTRBP3
     * @param paraMap
     * @param mapEnd
     */
    private void countGuaranteeForRepo(Map<String, Object> paraMap, HashMap<String, Object> mapEnd) {
        Object ORPT2=0.00;Object ORPT3=0.00; Object ORPTRBP2=0.00;Object ORPTRBP3=0.00;  
        
        List<Map<String,Object>> list = g2502.countGuaranteeForRepo(paraMap);
        if(list!=null&&list.size()>0){
            for (Map<String, Object> map : list) {
                if(map == null) {
                    continue;
                }
                if("less".equals(map.get("TYPE").toString())&& "VB".equals(map.get("PRODTYPE").toString())){
                    ORPT2=map.get("AMT");
                }
                if("more".equals(map.get("TYPE").toString())&& "VB".equals(map.get("PRODTYPE").toString())){
                    ORPT3=map.get("AMT");
                }
                if("less".equals(map.get("TYPE").toString())&& "RBP".equals(map.get("PRODTYPE").toString())){
                    ORPTRBP2=map.get("AMT");
                }
                if("more".equals(map.get("TYPE").toString())&& "RBP".equals(map.get("PRODTYPE").toString())){
                    ORPTRBP3=map.get("AMT");
                }
            }           
        }
        
        mapEnd.put("RPT2", new BigDecimal(ORPT2.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("RPT3", new BigDecimal(ORPT3.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) ); 
        mapEnd.put("ORPTRBP2", new BigDecimal(ORPTRBP2.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("ORPTRBP3", new BigDecimal(ORPTRBP3.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) ); 
    }
    
    
    /**
     * 5.2 符合LCR二级资产定义的证券       PRT6   PRT7
       5.3 非金融公司债（评级为A-至A+）  PRT8   PRT9
       5.4 非自身发行的资产担保债券（Covered Bond）（评级为A-至A+）       PRT100   PRT101
       5.5 其他    ELSE0   ELSE1
     * @param paraMap
     * @param mapEnd
     */
    private void countOtherBond(Map<String, Object> paraMap, HashMap<String, Object> mapEnd) {
        Object ORPT6=0.00;Object ORPT7=0.00;        
        Object ORPT8=0.00;Object ORPT9=0.00;// 5.3 非金融公司债（评级为A-至A+
        Object ORPT100=0.00;Object ORPT101=0.00;//  5.4 非自身发行的资产担保债券（Covered Bond）（评级为A-至A+）
        Object OELSE0=0.00;Object OELSE1=0.00;//     5.5 其他
        Object OOTHER0=0.00;Object OOTHER1=0.00;//      不在以上分类的
        
        List<Map<String,Object>> list = g2502.countOtherBond(paraMap);
        
        if(list!=null&&list.size()>0){
            for (Map<String, Object> map : list) {
                if(map == null) {
                    continue;
                }
                 if("LESS".equals(map.get("TYPE"))){
                     if("AA".equals(map.get("LEVETYPE"))){
                         //（.SD LIKE '%公共部门实体（收入来源于中央财政）%'+%中央政府投资的公用事业企业%+）+评级AA-及以上的非金融公司债 （非金融债）
                         if("1".equals(map.get("SECMTYPE"))|| "2".equals(map.get("SECMTYPE"))){
                             ORPT6=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT6.toString()));
                         } else if("4".equals(map.get("SECMTYPE"))){
                             //债券-商业银行金融债+次级债SDsd+非银行金融机构债（DOMNBFINSE）
                             OELSE0=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OELSE0.toString()));
                         }else{
                             OOTHER0=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER0.toString()));
                         }
                         
                     }else if("A".equals(map.get("LEVETYPE"))){
                          //非金融债-评级为A-至A+
                         if("2".equals(map.get("SECMTYPE"))){
                             ORPT8=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT8.toString()));
                         }else  if("3".equals(map.get("SECMTYPE"))){
                         // 5.4 非自身发行的资产担保债券（Covered Bond）（评级为A-至A+）
                             ORPT100=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT100.toString()));
                         }else  if("4".equals(map.get("SECMTYPE"))){
                             //债券-商业银行金融债+次级债SDsd+非银行金融机构债（DOMNBFINSE）
                             OELSE0=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OELSE0.toString()));
                         }else{
                             OOTHER0=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER0.toString()));
                         }
                     }else{
                         OOTHER0=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER0.toString()));
                     }
                 }else{
                     if("AA".equals(map.get("LEVETYPE"))){
                        //（.SD LIKE '%公共部门实体（收入来源于中央财政）%'+%中央政府投资的公用事业企业%+）+评级AA-及以上的非金融公司债 （非金融债）
                         if("1".equals(map.get("SECMTYPE"))|| "2".equals(map.get("SECMTYPE"))){
                             ORPT7=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT7.toString()));
                         } else if("4".equals(map.get("SECMTYPE"))){
                             //债券-商业银行金融债+次级债SDsd+非银行金融机构债（DOMNBFINSE）
                             OELSE1=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OELSE1.toString()));
                         }else{
                             OOTHER1=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER1.toString()));
                         }
                     }else if("A".equals(map.get("LEVETYPE"))){
                         //5.3 非金融公司债（评级为A-至A+
                         if("2".equals(map.get("SECMTYPE"))){
                             ORPT9=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT9.toString()));
                         }else if("3".equals(map.get("SECMTYPE"))){
                            //     5.4 非自身发行的资产担保债券（Covered Bond）（评级为A-至A+）
                             ORPT100=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(ORPT100.toString()));
                         }else  if("4".equals(map.get("SECMTYPE"))){
                             //债券-商业银行金融债+次级债SDsd+非银行金融机构债（DOMNBFINSE）
                             OELSE1=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OELSE1.toString()));
                         }else{
                             OOTHER1=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER1.toString()));
                         }
                     }else{
                         OOTHER1=new BigDecimal(map.get("AMT").toString()).add(new BigDecimal(OOTHER1.toString()));
                     }
                 } 
            }
        }
        
        mapEnd.put("RPT6",new BigDecimal(ORPT6.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("RPT7",new BigDecimal(ORPT7.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("RPT8",new BigDecimal(ORPT8.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("RPT9",new BigDecimal(ORPT9.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );   
        mapEnd.put("RPT100",new BigDecimal(ORPT100.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("RPT101",new BigDecimal(ORPT101.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );   
        mapEnd.put("ELSE0",(new BigDecimal(OELSE0.toString()).add(new BigDecimal(OOTHER0.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("ELSE1",(new BigDecimal(OELSE1.toString()).add(new BigDecimal(OOTHER1.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );   
    }
    
    
    /**
     * 7.3 金融机构及其他    DLDTCCLESS  DLDTCCMORE
       4. 金融机构贷款    DLDTCRMORE  DLDTCRLESS
     * @param paraMap
     * @param mapEnd
     */
    private void countOtherLoan(Map<String, Object> paraMap, HashMap<String, Object> mapEnd) {
        Object DLDTCRLESS=0.00;Object DLDTCCLESS=0.00;
        Object DLDTCRMORE=0.00;Object DLDTCCMORE=0.00;
        List<Map<String,Object>> DLDTlist = g2502.countOtherLoan(paraMap);
        if(DLDTlist!=null&&DLDTlist.size()>0){
            for (Map<String, Object> map : DLDTlist) {
                if(map.get("CCYAMT")!=null&& map.get("PRODTYPE")!=null){
                    if(map == null) {
                        continue;
                    }
                    if("CR".equals(map.get("PRODTYPE"))){
                        if("LESS".equals(map.get("TYPE"))){
                            DLDTCRLESS=map.get("CCYAMT");
                        }
                        if("MORE".equals(map.get("TYPE"))){
                            DLDTCRMORE=map.get("CCYAMT");
                        }
                    }
                    if("CC".equals(map.get("PRODTYPE"))){
                        if("LESS".equals(map.get("TYPE"))){
                            DLDTCCLESS=map.get("CCYAMT");
                        }
                        if("MORE".equals(map.get("TYPE"))){
                            DLDTCCMORE=map.get("CCYAMT");
                        }
                    }
                }
            }
        }
        
        mapEnd.put("DLDTCCLESS",new BigDecimal(DLDTCCLESS.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("DLDTCCMORE",new BigDecimal(DLDTCCMORE.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("DLDTCRMORE",new BigDecimal(DLDTCRMORE.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
        mapEnd.put("DLDTCRLESS",new BigDecimal(DLDTCRLESS.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
    }
    
    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }
    
    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
        Date date = DateUtil.parse(queryDate);
        Map<String, Object> report = getReport(paramer,date);
        return report;
    }
    
}
