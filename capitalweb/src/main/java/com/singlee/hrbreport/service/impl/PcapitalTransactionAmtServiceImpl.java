package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;

import com.singlee.hrbreport.mapper.PCapitalTransactionAmtMapper;

import com.singlee.hrbreport.model.PCapitalTransactionAmtVO;
import com.singlee.hrbreport.service.PCapitalTransactionAmtService;
import com.singlee.hrbreport.util.HrbReportUtils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 自营资金业务余额表
 */
@Service
public class PcapitalTransactionAmtServiceImpl implements PCapitalTransactionAmtService  {

    @Autowired
    PCapitalTransactionAmtMapper pa;

   

    @Override
    public List<PCapitalTransactionAmtVO> selectPCapitalTransactionAmtList(Map<String, Object> param) {
        HashMap<String,Object> remap =new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName); 
        remap.put("HRBCB", SystemProperties.hrbcbUserName);
        remap.put("startdate", param.get("queryDate"));    
        return pa.searchPCapitalTransactionAmt(remap);
      
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<PCapitalTransactionAmtVO> list =selectPCapitalTransactionAmtList(paramer);
        mapEnd.put("list", list);
        return mapEnd;
    }


    @Override
    public Page<PCapitalTransactionAmtVO> selectPCapitalTransactionAmt(Map<String, Object> param) {

        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<PCapitalTransactionAmtVO> list2 = selectPCapitalTransactionAmtList(param);
      
        Page <PCapitalTransactionAmtVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }
}



   
 

    
    
    
    

