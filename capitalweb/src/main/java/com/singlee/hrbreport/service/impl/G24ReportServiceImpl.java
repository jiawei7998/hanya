package com.singlee.hrbreport.service.impl;

import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G24ReportMapper;
import com.singlee.hrbreport.model.G24Po;
import com.singlee.hrbreport.service.G24ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * G24最大百家金额机构同业融入情况表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G24ReportServiceImpl implements G24ReportService {

	@Autowired
	private G24ReportMapper g24ReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		String day = date.substring(6, 8);
		if ("0".equals((date.substring(6, 8)).substring(0, 1))) {
			day = date.substring(7, 8);
		} else {
			day = date.substring(6, 8);
		}
		String yrMonth = date.substring(0, 6);

		Map<String, String> mapPar = new HashMap<String, String>();
		
		Date parse = DateUtil.parse(date,"yyyyMMdd");
		mapPar.put("sdate", DateUtil.format(CalendarUtil.getFirstDayOfMonth(parse),"yyyyMMdd"));
		Calendar cal = Calendar.getInstance();
	    cal.setTime(parse);//设置起时间
	    cal.add(Calendar.MONTH, 2);//增加两个月  
		mapPar.put("edate", DateUtil.format(CalendarUtil.getLastDayOfMonth(cal.getTime()),"yyyyMMdd"));
		
		mapPar.put("br", ParameterUtil.getString(paramer, "br", "01"));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);
		mapPar.put("term", "TERMS" + day);
		mapPar.put("spot", "SPOTRATE" + day + "_8");
		mapPar.put("yrMonth", yrMonth);
		List<G24Po> list = g24ReportMapper.getG24ReportDldt(mapPar);
		
		for (int i = 0; i < list.size(); i++) {
			G24Po g24Po = list.get(i);
			g24Po.setNumber(i+1);
			g24Po.setAmt1((g24Po.getAmt1() == null ? new BigDecimal(0) : g24Po.getAmt1()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt2((g24Po.getAmt2() == null ? new BigDecimal(0) : g24Po.getAmt2()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt3((g24Po.getAmt3() == null ? new BigDecimal(0) : g24Po.getAmt3()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt4((g24Po.getAmt4() == null ? new BigDecimal(0) : g24Po.getAmt4()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt5((g24Po.getAmt5() == null ? new BigDecimal(0) : g24Po.getAmt5()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt6((g24Po.getAmt6() == null ? new BigDecimal(0) : g24Po.getAmt6()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			g24Po.setAmt7((g24Po.getAmt7() == null ? new BigDecimal(0) : g24Po.getAmt7()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			
		}

		mapEnd.put("list", list);
		return mapEnd;
	}

}
