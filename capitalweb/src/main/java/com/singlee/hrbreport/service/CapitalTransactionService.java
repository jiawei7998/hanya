package com.singlee.hrbreport.service;


import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.CapitalTransactionVO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * 自营资金交易信息表
 * @author cg
 *
 */
@Service
public interface CapitalTransactionService  extends ReportService{

    public Page<CapitalTransactionVO> selectGovernmentBond(Map<String, Object> param);
    
    public List<CapitalTransactionVO> selectGovernmentBondList(Map<String, Object> param);
    
}
