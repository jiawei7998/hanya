package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;


import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;

/**
 *地方政府债明细
 */


public interface LocalGovernmentBondService extends ReportService{

    public List<LocalGovernmentBondVO> selectGovernmentBond(Map<String, Object> param) ;

    public Page<LocalGovernmentBondVO> searchGovernmentBondPage(Map<String , Object> param);
   
    
}
