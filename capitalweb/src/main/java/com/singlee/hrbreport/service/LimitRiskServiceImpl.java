package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbnewport.service.CommonExportService;
import com.singlee.hrbreport.mapper.LimitRiskMapper;
import com.singlee.hrbreport.model.LimitRisk;
import com.singlee.hrbreport.service.impl.LimitRiskService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/10/30 17:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class LimitRiskServiceImpl implements LimitRiskService, CommonExportService {

    @Resource
    private LimitRiskMapper limitRiskMapper;

    @Override
    public Page<LimitRisk> getLimitPage(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<LimitRisk> result = limitRiskMapper.selectPageLimit(map, rb);
        return result;
    }

    @Override
    public int deleteByPrimaryKey(String secmType) {
        return limitRiskMapper.deleteByPrimaryKey(secmType);
    }

    @Override
    public int insert(LimitRisk record) {
        return limitRiskMapper.insert(record);
    }

    @Override
    public int insertSelective(LimitRisk record) {
        return limitRiskMapper.insertSelective(record);
    }

    @Override
    public LimitRisk selectByPrimaryKey(String secmType) {
        return limitRiskMapper.selectByPrimaryKey(secmType);
    }

    @Override
    public int updateByPrimaryKeySelective(LimitRisk record) {
        return limitRiskMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(LimitRisk record) {
        return limitRiskMapper.updateByPrimaryKey(record);
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        List<Map<String, Object>> list =limitRiskMapper.selectListLimit(paramer);
        Map<String,Object> map=new HashMap<>();
        map.put("list",list);
        return  map;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Map<String, Object>> mapList = limitRiskMapper.selectListLimit(map);
        for(int i =0;i<mapList.size();i++){
            result.put("map"+(i+1),mapList.get(i));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}
