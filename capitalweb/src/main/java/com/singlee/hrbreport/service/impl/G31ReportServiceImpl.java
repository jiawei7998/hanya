package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.common.util.DataUtils;
import com.singlee.hrbreport.mapper.G31ReportMapper;
import com.singlee.hrbreport.service.G31ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * G31有价证券及投资情况表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G31ReportServiceImpl implements G31ReportService {

	@Autowired
	private G31ReportMapper g31ReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("date", date);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", "01"));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		mapPar.put("sqlType", "1");
		List<Map<String, Object>> listTypeD1 = g31ReportMapper.getG31ReportBondTypeD(mapPar);
		Object BALD01 = 0.00;
		Object BALD02 = 0.00;
		Object BALD03 = 0.00;
		Object BALD04 = 0.00;
		Object BALD05 = 0.00;
		Object BALD06 = 0.00;
		Object BALD07 = 0.00;
		Object BALD08 = 0.00;
		Object BALD09 = 0.00;
		Object BALD10 = 0.00;
		for (Map<String, Object> map : listTypeD1) {
			if (map.get("ACCTNGTYPE") != null) {
				if ("DOMGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD01 = map.get("BAL");
				}
				if ("DOMLOGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD02 = map.get("BAL");
				}
				if ("DOMCENBKSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD03 = map.get("BAL");
				}
				if ("DOMLOGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD04 = map.get("BAL");
				}
				if ("DOMFINSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD06 = map.get("BAL");
				}
				if ("DOMNOFINSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALD07 = map.get("BAL");
				}
			}
		}
		BigDecimal BALD11 = new BigDecimal("0.00");
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> listTypeD2 = g31ReportMapper.getG31ReportBondTypeD(mapPar);
		for (Map<String, Object> map : listTypeD2) {
			if (map.get("ACCTNGTYPE") != null) {
				BALD11 = BALD11.add((BigDecimal) map.get("BAL"));
			}
		}
		mapEnd.put("BALD01",
				(new BigDecimal(BALD01.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD02",
				(new BigDecimal(BALD02.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD03",
				(new BigDecimal(BALD03.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD04",
				(new BigDecimal(BALD04.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD05",
				(new BigDecimal(BALD05.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD06",
				(new BigDecimal(BALD06.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD07",
				(new BigDecimal(BALD07.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD08",
				(new BigDecimal(BALD08.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD09",
				(new BigDecimal(BALD09.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD10",
				(new BigDecimal(BALD10.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD11", (BALD11.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));

		mapPar.put("sqlType", "1");
		List<Map<String, Object>> listTypeF1 = g31ReportMapper.getG31ReportBondTypeF(mapPar);
		Object BALF01 = 0.00;
		Object BALF02 = 0.00;
		Object BALF03 = 0.00;
		Object BALF04 = 0.00;
		Object BALF05 = 0.00;
		Object BALF06 = 0.00;
		Object BALF07 = 0.00;
		Object BALF08 = 0.00;
		Object BALF09 = 0.00;
		Object BALF10 = 0.00;
		for (Map<String, Object> map : listTypeF1) {
			if (map.get("ACCTNGTYPE") != null) {
				if ("DOMGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF01 = map.get("BAL");
				}
				if ("DOMLOGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF02 = map.get("BAL");
				}
				if ("DOMCENBKSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF03 = map.get("BAL");
				}
				if ("DOMLOGOVSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF04 = map.get("BAL");
				}
				if ("DOMFINSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF06 = map.get("BAL");
				}
				if ("DOMNOFINSE".equals(map.get("ACCTNGTYPE").toString())) {
					BALF07 = map.get("BAL");
				}
			}
		}
		BigDecimal BALF11 = new BigDecimal("0.00");
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> listTypeF2 = g31ReportMapper.getG31ReportBondTypeF(mapPar);
		for (Map<String, Object> map : listTypeF2) {
			if (map.get("ACCTNGTYPE") != null) {
				BALF11 = BALF11.add((BigDecimal) map.get("BAL"));
			}
		}
		mapEnd.put("BALF01",
				(new BigDecimal(BALF01.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF02",
				(new BigDecimal(BALF02.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF03",
				(new BigDecimal(BALF03.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF04",
				(new BigDecimal(BALF04.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF05",
				(new BigDecimal(BALF05.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF06",
				(new BigDecimal(BALF06.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF07",
				(new BigDecimal(BALF07.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF08",
				(new BigDecimal(BALF08.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF09",
				(new BigDecimal(BALF09.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF10",
				(new BigDecimal(BALF10.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF11", (BALF11.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));

		List<Map<String, Object>> listLevelD = g31ReportMapper.getG31ReportBondLevelD(mapPar);
		BigDecimal BALD12 = new BigDecimal("0.00");
		BigDecimal BALD13 = new BigDecimal("0.00");
		BigDecimal BALD14 = new BigDecimal("0.00");
		for (Map<String, Object> map : listLevelD) {
			if (map.get("LEVE") != null) {
				String level = map.get("LEVE").toString();
				String[] list1 = { "AA+", "AAA-", "AAA" };
				String[] list2 = { "AA", "A2", "AA-", "A+", "A", "A-", "BBB+", "BBB", "BBB-", "BB+", "BB", "BB-", "B" };
				if (Arrays.asList(list1).contains(level)) {
					BALD12 = BALD12.add((BigDecimal) map.get("BAL"));
				}
				if (Arrays.asList(list2).contains(level)) {
					BALD13 = BALD13.add((BigDecimal) map.get("BAL"));
				}
				if ("0".equals(level)) {
					BALD14 = BALD14.add((BigDecimal) map.get("BAL"));
				}
			}
		}
		mapEnd.put("BALD12", (BALD12.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD13", (BALD13.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALD14", (BALD14.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));

		List<Map<String, Object>> listLevelF = g31ReportMapper.getG31ReportBondLevelF(mapPar);
		BigDecimal BALF12 = new BigDecimal("0.00");
		BigDecimal BALF13 = new BigDecimal("0.00");
		BigDecimal BALF14 = new BigDecimal("0.00");
		for (Map<String, Object> map : listLevelF) {
			if (map.get("LEVE") != null) {
				String level = map.get("LEVE").toString();
				String[] list1 = { "AA+", "AAA-", "AAA" };
				String[] list2 = { "AA", "A2", "AA-", "A+", "A", "A-", "BBB+", "BBB", "BBB-", "BB+", "BB", "BB-", "B" };
				if (Arrays.asList(list1).contains(level)) {
					BALF12 = BALF12.add((BigDecimal) map.get("BAL"));
				}
				if (Arrays.asList(list2).contains(level)) {
					BALF13 = BALF13.add((BigDecimal) map.get("BAL"));
				}
				if ("0".equals(level)) {
					BALF14 = BALF14.add((BigDecimal) map.get("BAL"));
				}
			}
		}
		mapEnd.put("BALF12", (BALF12.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF13", (BALF13.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("BALF14", (BALF14.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));

		String tdate = DataUtils.formatDate(postdate, 1, "yyyy-MM-dd");
		mapPar.put("tdate", tdate);
		BigDecimal BALD15 = new BigDecimal("0.00");
		mapPar.put("sqlType", "2");
		mapPar.put("prdNo", "802");
		List<Map<String, Object>> listFund1 = g31ReportMapper.getG31ReportFund(mapPar);
		for (Map<String, Object> map : listFund1) {
			if (map.get("FUNDCODE") != null) {
				//String fundCode = map.get("FUNDCODE").toString();
				BigDecimal price = new BigDecimal("2.00");
				BigDecimal utQty = new BigDecimal((String) map.get("UTQTY"));
				BALD15 = BALD15.add(price.multiply(utQty));
			}
		}
		mapEnd.put("BALD15", BALD15.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));

		mapPar.put("sqlType", "1");
		mapPar.put("prdNo", "801");
		List<Map<String, Object>> listFund2 = g31ReportMapper.getG31ReportFund(mapPar);
		if (listFund2.size() > 0 && listFund2 != null) {
			if (listFund2.get(0) != null && listFund2.get(0).get("UTQTY") != null) {
				mapEnd.put("BALD16", new BigDecimal(listFund2.get(0).get("UTQTY").toString())
						.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			} else {
				mapEnd.put("BALD16", 0.00);
			}
		} else {
			mapEnd.put("BALD16", 0.00);
		}

		mapEnd.put("BALF15", 0.00);
		mapEnd.put("BALF16", 0.00);

		return mapEnd;
	}

}
