package com.singlee.hrbreport.service;



import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


//地方法人金融机构月度监测表
@Service
public interface LegalMonitoringService  extends ReportService{
	
    
    
    
    public List<Map<String, Object>> getReportMap(Map<String, Object> paramer);
}
 