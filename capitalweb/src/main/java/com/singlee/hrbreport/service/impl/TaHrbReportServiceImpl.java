package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbreport.mapper.TaHrbReportMapper;
import com.singlee.hrbreport.model.TaHrbReport;
import com.singlee.hrbreport.service.TaHrbReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * 模板查询
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaHrbReportServiceImpl implements TaHrbReportService {
    @Autowired
    TaHrbReportMapper taHrbReportMapper;
    @Override
    public Page<TaHrbReport> searchTaHrbReportPage(Map<String, Object> params) {
        return taHrbReportMapper.searchTaHrbReportPage(params, ParameterUtil.getRowBounds(params));
    }

    @Override
    public TaHrbReport searchTaHrbReport(Map<String, Object> params) {
        return taHrbReportMapper.searchTaHrbReport(params);
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
