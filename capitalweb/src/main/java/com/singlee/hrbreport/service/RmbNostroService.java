package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.RmbNostroVO;

import java.util.List;
import java.util.Map;

/**
 * 人民币同业余额
 * @author zk
 */
public interface RmbNostroService extends ReportService{
    
    public Page<RmbNostroVO> getReportPage(Map<String ,Object> param);
    
    public List<RmbNostroVO> getReportList(Map<String ,Object> param);
    
}
