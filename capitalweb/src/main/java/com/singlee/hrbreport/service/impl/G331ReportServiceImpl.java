package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G331ReportMapper;
import com.singlee.hrbreport.service.G331ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * G331利率重新定价风险情况报表第1部分：交易账户
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G331ReportServiceImpl implements G331ReportService {

	@Autowired
	private G331ReportMapper g331ReportMapper;
	
	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String,Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);//报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());//工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());//姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		String sdate = "";
		String edate = "";
		String fixfloatind = "";
		
		Map<String,String> map = new HashMap<String, String>();
		map.put("date", date);
		map.put("br", ParameterUtil.getString(paramer, "br", "01"));
		map.put("OPICS", SystemProperties.opicsUserName);
		map.put("HRBCB", SystemProperties.hrbcbUserName);
		map.put("sqlType", "1");
		
		for (int i = 0; i < 6; i++) {
			if(i == 0) {sdate = "0"; edate = "1"; }//隔夜
			if(i == 1) {sdate = "2"; edate = "30"; }//隔夜到1月
			if(i == 2) {sdate = "31"; edate = "90"; }//1月到3月
			if(i == 3) {sdate = "91"; edate = "180"; }//3月到6月
			if(i == 4) {sdate = "181"; edate = "270"; }//6月到9月
			if(i == 5) {sdate = "271"; edate = "370"; }//9月到1年
			
			map.put("sdate", sdate);
			map.put("edate", edate);
			
			String workBond = "v" + String.valueOf(i) + "al";
			String amtBond = g331ReportMapper.getG331ReportBond(map);//连接数据库
			if(amtBond != null) {
				mapEnd.put(workBond, new BigDecimal(amtBond.toString()).divide(new BigDecimal("10000.0"),6,BigDecimal. ROUND_HALF_UP));
			}else {
				mapEnd.put(workBond, new BigDecimal("0.000000"));
			}
			
			for (int j = 4; j < 6; j++) {
				if(j == 4) {fixfloatind = "R"; }//利率掉期合约多头
				if(j == 5) {fixfloatind = "P"; }//利率掉期合约空头
				
				map.put("fixfloatind", fixfloatind);
				
				String workSwap = "v" + String.valueOf(i) + "al" + fixfloatind;
				String amtSwap = g331ReportMapper.getG331ReportSwap(map);//连接数据库
				if(amtSwap != null) {
					mapEnd.put(workSwap, new BigDecimal(amtSwap.toString()).divide(new BigDecimal("10000.0"),6,BigDecimal. ROUND_HALF_UP));
				}else {
					mapEnd.put(workSwap, new BigDecimal("0.000000"));
				}
			}
		}

		map.put("sqlType", "2");
		
		for (int i = 6; i < 19; i++) {
			if(i == 6) {sdate = "12"; edate = "18"; }//1年到1.5年
			if(i == 7) {sdate = "18"; edate = "24"; }//1.5年到2
			if(i == 8) {sdate = "24"; edate = "36"; }//2年到3年
			if(i == 9) {sdate = "36"; edate = "48"; }//3年到4年
			if(i == 10) {sdate = "48"; edate = "60"; }//4年到5年
			if(i == 11) {sdate = "60"; edate = "72"; }//5年到6年
			if(i == 12) {sdate = "72"; edate = "84"; }//6年到7年
			if(i == 13) {sdate = "84"; edate = "96"; }//7年到8年
			if(i == 14) {sdate = "96"; edate = "108"; }//8年到9年
			if(i == 15) {sdate = "108"; edate = "120"; }//9年到10年
			if(i == 16) {sdate = "120"; edate = "180"; }//10年到15年
			if(i == 17) {sdate = "180"; edate = "240"; }//15年到20年
			if(i == 18) {sdate = "240"; edate = "240000"; }//20年以上
			
			map.put("sdate", sdate);
			map.put("edate", edate);
			
			String workBond = "v" + String.valueOf(i) + "al";
			String amtBond = g331ReportMapper.getG331ReportBond(map);//连接数据库
			if(amtBond != null) {
				mapEnd.put(workBond, new BigDecimal(amtBond.toString()).divide(new BigDecimal("10000.0"),6,BigDecimal. ROUND_HALF_UP));
			}else {
				mapEnd.put(workBond, new BigDecimal("0.000000"));
			}
			
			for (int j = 4; j < 6; j++) {
				if(j == 4) {fixfloatind = "R"; }//利率掉期合约多头
				if(j == 5) {fixfloatind = "P"; }//利率掉期合约空头
				
				map.put("fixfloatind", fixfloatind);
				
				String workSwap = "v" + String.valueOf(i) + "al" + fixfloatind;
				String amtSwap = g331ReportMapper.getG331ReportSwap(map);//连接数据库
				if(amtSwap != null) {
					mapEnd.put(workSwap, new BigDecimal(amtSwap.toString()).divide(new BigDecimal("10000.0"),6,BigDecimal. ROUND_HALF_UP));
				}else {
					mapEnd.put(workSwap, new BigDecimal("0.000000"));
				}
			}
		}
		
		return mapEnd;
	}

}
