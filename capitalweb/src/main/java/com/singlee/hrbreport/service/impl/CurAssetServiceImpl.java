package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbreport.mapper.CurAssetMapper;
import com.singlee.hrbreport.model.CurAssetVO;
import com.singlee.hrbreport.service.CurAssetService;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * _优质流动性资产台账
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CurAssetServiceImpl implements CurAssetService {
    @Autowired
    CurAssetMapper curAssetMapper;
    @Autowired
    LoanInSameService loanService;
    
    @Override
    public Page<CurAssetVO> queryCurrentAssetsPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<CurAssetVO> queryCurrentAssetsList = queryCurrentAssetsList(param);
        return HrbReportUtils.producePage(queryCurrentAssetsList, pageNum, pageSize);
    }

    @Override
    public List<CurAssetVO> queryCurrentAssetsList(Map<String, Object> param) {
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = sf.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = sf.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("queryDate", DateUtil.format(queryDay));
        
        //传入SICO
        String sico = "";
        String sicInd = param.get("seSicType")!=null?param.get("seSicType").toString():"";
        if("1".equals(sicInd)) {
            //国债
            sico = "SE-GZ";
        }
        if("2".equals(sicInd)) {
            //政金债
            sico = "SE-ZCYHJRZ";
        }
        if("3".equals(sicInd)) {
            //地方政府债
            sico = "SE-DFZ";
        }
        if("4".equals(sicInd)) {
            //企业债
            sico = "SE-QYZBMST','SE-QYZFYH','SE-QYZGQ','SE-QYZQT','SE-QYZQTGY','SE-QYZSYQY','SE-QYZYQ";
        }
        if("5".equals(sicInd)) {
            //短期融资券
            sico = "SE-DRBMST','SE-DRFYH','SE-DRGQ','SE-DRQT','SE-DRQTGY','SE-DRSYQY','SE-DRYQ";
        }
        if("6".equals(sicInd)) {
            //中期票据
            sico = "SE-ZPFYH','SE-ZPGQ','SE-ZPQT','SE-ZPYQ";
        }
        param.put("SICO", sico);
        
        List<CurAssetVO> queryCurrentAssets = curAssetMapper.queryCurrentAssets(param);
        
        return queryCurrentAssets;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        
        paramer.put("seSicType", "1");
        List<CurAssetVO> gzList = queryCurrentAssetsList(paramer);
        paramer.put("seSicType", "2");
        List<CurAssetVO> zjList = queryCurrentAssetsList(paramer);
        paramer.put("seSicType", "3");
        List<CurAssetVO> dfList = queryCurrentAssetsList(paramer);
        paramer.put("seSicType", "4");
        List<CurAssetVO> qyList = queryCurrentAssetsList(paramer);
        paramer.put("seSicType", "5");
        List<CurAssetVO> dqList = queryCurrentAssetsList(paramer);
        paramer.put("seSicType", "6");
        List<CurAssetVO> zqList = queryCurrentAssetsList(paramer);
        
        mapend.put("gzList",gzList);
        mapend.put("zjList",zjList);
        mapend.put("dfList",dfList);
        mapend.put("qyList",qyList);
        mapend.put("dqList",dqList);
        mapend.put("zqList",zqList);
        mapend.put("queryDate",ParameterUtil.getString(paramer,"queryDate", DateUtil.getCurrentDateAsString()));
        
        return mapend;
    }
    
}
