package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G24最大百家金额机构同业融入情况表
 *
 */
public interface G24ReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
}
