package com.singlee.hrbreport.service;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName AcctLossLimitServiceImpl.java
 * @Description 交易帐户浮亏止损限额表
 * @createTime 2021年10月29日 09:44:00
 */
public interface AcctLossLimitService extends ReportService{
}
