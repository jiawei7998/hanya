package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.hrbreport.mapper.DepositAndBorrowingMapper;
import com.singlee.hrbreport.model.DepositAndBorrowingBranchUploadVO;
import com.singlee.hrbreport.model.DepositAndBorrowingDTO;
import com.singlee.hrbreport.model.DepositAndBorrowingForeInvestorVO;
import com.singlee.hrbreport.model.DepositAndBorrowingSummarizingVO;
import com.singlee.hrbreport.service.DepAndBorrService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * _存款含银行同业和联行存放负债
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DepAndBorrServiceImpl implements DepAndBorrService{

    @Autowired
    DepositAndBorrowingMapper depositMapper;
    @Autowired
    DayendDateService dayendDateService;

    @Override
    public Page<DepositAndBorrowingForeInvestorVO> getForeInvestorReportPage(Map<String, Object> param) {
        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        List<DepositAndBorrowingForeInvestorVO> foreInvestorReportList = getForeInvestorReportList(param);
        return HrbReportUtils.producePage(foreInvestorReportList, pageNum, pageSize);
    }

    @Override
    public Page<DepositAndBorrowingBranchUploadVO> getBranchUploadReportPage(Map<String, Object> param) {
        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        List<DepositAndBorrowingBranchUploadVO> branchUploadReportList = getBranchUploadReportList(param);
        return HrbReportUtils.producePage(branchUploadReportList, pageNum, pageSize);
    }

    @Override
    public Page<DepositAndBorrowingSummarizingVO> getSummarizingReportPage(Map<String, Object> param) {
        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        List<DepositAndBorrowingSummarizingVO> summarizingReportList = getSummarizingReportList(param);
        return HrbReportUtils.producePage(summarizingReportList, pageNum, pageSize);
    }
    
    /**
     * _境外机构
     * @param param
     * @return
     */
    public List<DepositAndBorrowingForeInvestorVO> getForeInvestorReportList(Map<String, Object> param) {
        
        List<DepositAndBorrowingForeInvestorVO> reportList = new ArrayList<DepositAndBorrowingForeInvestorVO>();
        
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString("yyyy-MM"));
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay));
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        startDate = getLastDate(startDate);
        endDate = getLastDate(getNextDate(endDate));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("queryDate", queryDate);
        
        List<DepositAndBorrowingDTO> querySql1List = depositMapper.querySql1(param);
        List<DepositAndBorrowingDTO> querySqlNowList = depositMapper.querySqlNow(param);
        List<DepositAndBorrowingDTO> querySql2List = depositMapper.querySql2(param);
        List<DepositAndBorrowingDTO> querySql3List = depositMapper.querySql3(param);
        List<DepositAndBorrowingDTO> querySql4List = depositMapper.querySql4(param);
        List<DepositAndBorrowingDTO> queryAcctnoSqlList = depositMapper.queryAcctnoSql(param);
        
        List<DepositAndBorrowingDTO> lastquery = new ArrayList<DepositAndBorrowingDTO>();
        for(DepositAndBorrowingDTO depositDTO : querySql1List) {
            DepositAndBorrowingDTO depositDO = new DepositAndBorrowingDTO();
            if(depositDTO != null && depositDTO.getAcctNo() != null) {
                BigDecimal saccroutst = new BigDecimal(depositDTO.getSaccroutst().toString());
                BigDecimal scurbalance = new BigDecimal(depositDTO.getScurBalance().toString());
                
                depositDO.setAccountNo(depositDTO.getAcctNo());
                depositDO.setSaccroutst(saccroutst.toString());
                depositDO.setScurBalance(scurbalance.toString());
            }else if(depositDTO != null && depositDTO.getAccountNo() != null) {
                BigDecimal accroutst = new BigDecimal(depositDTO.getFaccroutst());
                
                depositDO.setAccountNo(depositDTO.getAccountNo());
                depositDO.setAccroutst(accroutst.toString());
                depositDO.setScurBalance("0");
            }
            lastquery.add(depositDO);
        }
        
        List<DepositAndBorrowingDTO> nowquery = new ArrayList<DepositAndBorrowingDTO>();
        for(DepositAndBorrowingDTO depositDTO : querySqlNowList) {
            DepositAndBorrowingDTO depositDO = new DepositAndBorrowingDTO();
            if(depositDTO != null && depositDTO.getAcctNo() != null) {
                BigDecimal effordamt = new BigDecimal(depositDTO.getSaccroutst().toString());
                BigDecimal effordedamt = new BigDecimal(depositDTO.getMtdincexp().toString());
                BigDecimal scurbalance = new BigDecimal(depositDTO.getScurBalance().toString());
                
                depositDO.setAccountNo(depositDTO.getAcctNo());
                depositDO.setEffordamt(effordamt.toString());
                depositDO.setEffordedamt(effordedamt.toString());
                depositDO.setScurBalance(scurbalance.toString());
            }else if(depositDTO != null && depositDTO.getAccountNo() != null) {
                BigDecimal effordamt = new BigDecimal(depositDTO.getFaccroutst().toString());
                BigDecimal effordedamt = new BigDecimal(depositDTO.getFintpaid().toString());
                
                depositDO.setAccountNo(depositDTO.getAcctNo());
                depositDO.setEffordamt(effordamt.toString());
                depositDO.setEffordedamt(effordedamt.toString());
                depositDO.setScurBalance("0");
            }
            nowquery.add(depositDO);
        }
        
        String ORGNO = getStaticFiled("CKCFFZ", "填报机构代码",param.get("OPICS").toString());
        String DESC = getStaticFiled("CKCFFZ", "备注",param.get("OPICS").toString());
        
        String coun = "";
        String OAREA = "";
        
        Integer i = 0;//计数
        for(DepositAndBorrowingDTO allno : queryAcctnoSqlList) {
            DepositAndBorrowingForeInvestorVO depositVO = new DepositAndBorrowingForeInvestorVO();
            depositVO.setAccroutst("0");
            depositVO.setAmount("0");
            depositVO.setEffordamt("0");
            depositVO.setBalanceAmt("0");
            depositVO.setEffordedamt("0");
            
            Map<String ,Object> map = new HashMap<String ,Object>();
            map.put("COUN", allno.getCoun());
            map.put("OPICS", HrbReportUtils.OPICS);
            OAREA = depositMapper.getCoun(map);
            
            String dataNo = ORGNO.trim() + "aaaaaaD05-1" + queryDate.replace("-", "") + fullWithZero(i.toString(), 7);
            depositVO.setDataNo(dataNo);
            depositVO.setOrgNo(ORGNO);
            depositVO.setRepMonth(queryDate);
            depositVO.setCcy(allno.getCcy());
            depositVO.setCoun(OAREA);
            depositVO.setDesc(DESC);
            depositVO.setAccountno(allno.getAccountNo());
            
            String cndeNo = getNo(allno.getAccountNo());
            depositVO.setCndeNo(cndeNo);
            
            for(DepositAndBorrowingDTO depositDTO : lastquery) {
                if(allno.getAccountNo() != null && allno.getAccountNo().equals(depositDTO.getAccountNo())) {
                    depositVO.setAccroutst(depositDTO.getAccroutst());
                    BigDecimal balanceamt = new BigDecimal(depositDTO.getScurBalance()).negate();
                    depositVO.setBalanceAmt(balanceamt.toString());
                }
            }
            for(DepositAndBorrowingDTO depositDTO : nowquery) {
                if(allno.getAccountNo() != null && allno.getAccountNo().equals(depositDTO.getAccountNo())) {
                    depositVO.setEffordamt(depositDTO.getEffordamt());
                    depositVO.setEffordedamt(depositDTO.getEffordedamt());
                    
                    BigDecimal balanceamt = new BigDecimal(depositDTO.getScurBalance());
                    depositVO.setAmount(balanceamt.toString());
                    if(depositDTO.getScurBalance() != null) {
                        balanceamt = balanceamt.add(new BigDecimal(depositDTO.getScurBalance()));
                    }
                    depositVO.setBalanceAmt(balanceamt.abs().toString());
                }
            }
            for(DepositAndBorrowingDTO depositDTO : querySql2List) {
                if(allno.getAccountNo() != null && allno.getAccountNo().equals(depositDTO.getAccountNo())) {
                    BigDecimal amount = new BigDecimal(depositDTO.getAmt());
                    if(depositVO.getAmount() != null) {
                        amount = amount.add(new BigDecimal(depositVO.getAmount()));
                    }
                    depositVO.setAmount(amount.toString());
                }
            }
            for(DepositAndBorrowingDTO depositDTO : querySql3List) {
                if(allno.getAccountNo() != null && allno.getAccountNo().equals(depositDTO.getAccountNo())) {
                    BigDecimal balanceAmt = new BigDecimal(depositDTO.getAmt()).negate();
                    if(depositVO.getAmount() != null) {
                        balanceAmt = balanceAmt.add(new BigDecimal(depositVO.getBalanceAmt()));
                    }
                    depositVO.setBalanceAmt(balanceAmt.toString());
                }
            }
            for(DepositAndBorrowingDTO depositDTO : querySql4List) {
                if(allno.getAccountNo() != null && allno.getAccountNo().equals(depositDTO.getAccountNo())) {
                    BigDecimal balanceAmt = new BigDecimal(depositDTO.getAmt());
                    if(depositVO.getAmount() != null) {
                        balanceAmt = balanceAmt.add(new BigDecimal(depositVO.getBalanceAmt()));
                    }
                    depositVO.setBalanceAmt(balanceAmt.toString());
                }
            }
            reportList.add(depositVO);
            i++;
        }
        return reportList;
    }
    
    /**
     * _分行上传
     * @param param
     * @return
     */
    public List<DepositAndBorrowingBranchUploadVO> getBranchUploadReportList(Map<String, Object> param) {
        List<DepositAndBorrowingBranchUploadVO> list = new ArrayList<DepositAndBorrowingBranchUploadVO>();
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString("yyyy-MM"));
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay));
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        startDate = getLastDate(startDate);
        endDate = getLastDate(getNextDate(endDate));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<Map<String,Object>> branchUpload = depositMapper.getBranchUpload(param);
        
        for (Map<String,Object> map : branchUpload) {
            DepositAndBorrowingBranchUploadVO branchVO = new DepositAndBorrowingBranchUploadVO();
            branchVO.setDataNo(map.get("DATANO").toString());
            branchVO.setOrgNo(map.get("ORGNO").toString());
            branchVO.setCoun(map.get("COUN").toString());
            branchVO.setCcy(map.get("CCY").toString());
            branchVO.setCndeNo(map.get("CNDENO").toString());
            
            branchVO.setAccroutst(map.get("ACCROUTST").toString());
            branchVO.setAmount(map.get("AMOUNT").toString());
            branchVO.setEffordamt(map.get("EFFORDAMT").toString());
            branchVO.setBalanceamt(map.get("BALANCEAMT").toString());
            branchVO.setEffordedamt(map.get("EFFORDEDAMT").toString());
            branchVO.setRepMonth(map.get("REPMONTH").toString().substring(0, 7));
            
            String desc = map.get("DESCS") != null ? map.get("DESCS").toString() : "";
            branchVO.setDescs(desc);
            
            list.add(branchVO);
        }
        return list;
    }
    
    /**
     * _汇总
     * @param param
     * @return
     */
    public List<DepositAndBorrowingSummarizingVO> getSummarizingReportList(Map<String, Object> param) {
        List<DepositAndBorrowingSummarizingVO> list = new ArrayList<DepositAndBorrowingSummarizingVO>();
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString("yyyy-MM"));
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay));
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        startDate = getLastDate(startDate);
        endDate = getLastDate(getNextDate(endDate));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<DepositAndBorrowingForeInvestorVO> foreInvestorReportList = getForeInvestorReportList(param);
        List<DepositAndBorrowingBranchUploadVO> branchUploadReportList = getBranchUploadReportList(param);
        
        Set<String> groupSet = new TreeSet<String>();
        for(DepositAndBorrowingForeInvestorVO depositVO: foreInvestorReportList) {
            param.put("accountno", depositVO.getAccountno());
            List<Map<String,Object>> groupList = depositMapper.getGroup(param);
            depositVO.setCcy(groupList.get(0).get("CCY").toString());
            String group = depositVO.getCoun().toString() + ":" +groupList.get(0).get("CCY").toString();
            groupSet.add(group);
        }
        for(DepositAndBorrowingBranchUploadVO depositVO: branchUploadReportList) {
            String group = depositVO.getCoun() + ":" + depositVO.getCcy();
            groupSet.add(group);
        }
        
        for(String groupStr : groupSet) {
            DepositAndBorrowingSummarizingVO summarizingVO = new DepositAndBorrowingSummarizingVO();
            
            BigDecimal accroutst = new BigDecimal(0);
            BigDecimal amount = new BigDecimal(0);
            BigDecimal effordamt = new BigDecimal(0);
            BigDecimal balanceamt = new BigDecimal(0);
            BigDecimal effordedamt = new BigDecimal(0);
            
            summarizingVO.setAccroutst("0");
            summarizingVO.setAmount("0");
            summarizingVO.setEffordamt("0");
            summarizingVO.setBalanceAmt("0");
            summarizingVO.setEffordedAmt("0");
            
            for(DepositAndBorrowingForeInvestorVO depositVO : foreInvestorReportList) {
                if(groupStr.equals(depositVO.getCoun().toString() + ":" +depositVO.getCcy())) {
                    if(depositVO.getAccroutst() != null) {
                        accroutst = accroutst.add(new BigDecimal(depositVO.getAccroutst()));
                    }
                    if(depositVO.getAmount() != null) {
                        amount = amount.add(new BigDecimal(depositVO.getAmount()));
                    }
                    if(depositVO.getEffordamt() != null) {
                        effordamt = effordamt.add(new BigDecimal(depositVO.getEffordamt()));
                    }
                    if(depositVO.getBalanceAmt() != null) {
                        balanceamt = balanceamt.add(new BigDecimal(depositVO.getBalanceAmt()));
                    }
                    if(depositVO.getEffordedamt() != null) {
                        effordedamt = effordedamt.add(new BigDecimal(depositVO.getEffordedamt()));
                    }
                    summarizingVO.setCoun(depositVO.getCoun());
                    summarizingVO.setCcy(depositVO.getCcy());
                    summarizingVO.setRepMonth(queryDate);
                    summarizingVO.setAccroutst(accroutst.toString());
                    summarizingVO.setAmount(amount.toString());
                    summarizingVO.setEffordamt(effordamt.toString());
                    summarizingVO.setBalanceAmt(balanceamt.toString());
                    summarizingVO.setEffordedAmt(effordedamt.toString());
                }
            }
            for(DepositAndBorrowingBranchUploadVO depositVO : branchUploadReportList) {
                if(groupStr.equals(depositVO.getCoun().toString() + ":" +depositVO.getCcy())) {
                    if(depositVO.getAccroutst() != null) {
                        accroutst = accroutst.add(new BigDecimal(depositVO.getAccroutst()));
                    }
                    if(depositVO.getAmount() != null) {
                        amount = amount.add(new BigDecimal(depositVO.getAmount()));
                    }
                    if(depositVO.getEffordamt() != null) {
                        effordamt = effordamt.add(new BigDecimal(depositVO.getEffordamt()));
                    }
                    if(depositVO.getBalanceamt() != null) {
                        balanceamt = balanceamt.add(new BigDecimal(depositVO.getBalanceamt()));
                    }
                    if(depositVO.getEffordedamt() != null) {
                        effordedamt = effordedamt.add(new BigDecimal(depositVO.getEffordedamt()));
                    } 
                    summarizingVO.setCoun(depositVO.getCoun());
                    summarizingVO.setCcy(depositVO.getCcy());
                    summarizingVO.setRepMonth(queryDate);
                    summarizingVO.setAccroutst(accroutst.toString());
                    summarizingVO.setAmount(amount.toString());
                    summarizingVO.setEffordamt(effordamt.toString());
                    summarizingVO.setBalanceAmt(balanceamt.toString());
                    summarizingVO.setEffordedAmt(effordedamt.toString());
                }
            }
            list.add(summarizingVO);
        }
        
        return list;
    }
    
    
    /**
     * _补全位数
     * @param num
     * @param length
     * @return
     */
    private String fullWithZero(String num, int length) {
        if (num.length() < length) {
            String temp = "";
            for (int i = 0; i < length - num.length(); i++) {
                temp = temp + "0";
            }
            return (temp + num).trim();
        }
        return "";
    }
    
    /**
     * holidate传入节假日,返回节假日前的第一个工作日
     * @return
     */
    private String getLastDate(String holidate) {
        do {
            holidate = getPreDay(holidate);
        }
        while (isHolidDate(holidate));
        return holidate;
    }
    
    /**
     * holidate传入节假日,返回节假日后的第一个工作日
     * @return
     */
    private String getNextDate(String holidate) {
        do {
            holidate = getNextDay(holidate);
        }
        while (isHolidDate(holidate));
        return holidate;
    }

    
    /**
     * _获取前一天
     * @param date
     * @return
     */
    private String getPreDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, -1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    
    /**
     * _获取第二天的日期
     * 
     * @return
     */
    private String getNextDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    
    /**
     * _ 判断传入的日期是否是节假日
     * 
     * @param date
     * @return
     */
    private boolean isHolidDate(String date) {
        Map<String, Object> map = new HashMap<>();
        map.put("calDate",date);

        boolean flag =dayendDateService.queryJJR(map)>0?true:false;
        return flag;
    }
    
    /**
     * _获取数据库配置数据
     * @param tableId
     * @param tableValue
     * @return
     */
    private String getStaticFiled(String tableId,String tableValue,String opics) {
        HashMap<String,Object> param = new HashMap<String,Object>();
        param.put("tableId", tableId);
        param.put("tableValue", tableValue);
        param.put("OPICS", opics);
        String text = depositMapper.getStaticFiled(param);
        return text;
    }
    
    
    private String getNo(String accountno) {
        String result = "";
        Map map = new HashMap();
        map.put("ACCOUNTNO", "%" + accountno + "%");
        map.put("OPICS", HrbReportUtils.OPICS);
        List<Map<String, Object>> list = depositMapper.getNo(map);
        if ((list != null) && (list.size() > 0)) {
            result = list.get(0).get("NOTES").toString();
        }
        return result;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> mapend = new HashMap<String, Object>();
        List<DepositAndBorrowingForeInvestorVO> foreInvestorReportList = getForeInvestorReportList(paramer);
        List<DepositAndBorrowingBranchUploadVO> branchUploadReportList = getBranchUploadReportList(paramer);
        List<DepositAndBorrowingSummarizingVO> summarizingReportList = getSummarizingReportList(paramer);
        
        //存款含银行-境外机构存款
        mapend.put("foreList",foreInvestorReportList);
        //存款含银行-分行上传
        mapend.put("branList",branchUploadReportList);
        //存款含银行-汇总
        mapend.put("summarizingList",summarizingReportList);
        
        return mapend;
    }
}
