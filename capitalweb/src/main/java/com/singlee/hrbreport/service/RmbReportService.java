package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.RmbPo;

/**
 * 本币交易情况报表
 *
 */
public interface RmbReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
	/**
	 * @param paramer
	 * @return
	 */
	public Page<RmbPo>  getRmbReportPage(Map<String, String> paramer) ;
}
