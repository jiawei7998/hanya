package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G14 大额风险暴露统计表 
 * @author zk
 */
public interface G1401ReportService extends ReportService {
    
    public Map<String, Object> getReport(Map<String, Object> paramer, Date postdate);
    
}
