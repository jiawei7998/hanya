package com.singlee.hrbreport.service;



import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.TheForeignCurrencyVO;
import com.singlee.hrbreport.model.TheForeignCurrencyVO2;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 本外币同业台账
 * @author cg
 *
 */
public interface TheForeignCurrencyReportService extends ReportService{
    
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList1(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList2(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList3(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList4(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList5(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO2> selectTheForeignCurrencyList6(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList7(Map<String,Object> param);
    
    public List<TheForeignCurrencyVO> selectTheForeignCurrencyList8(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency1(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency2(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency3(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency4(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency5(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO2> selectTheForeignCurrency6(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency7(Map<String,Object> param);
    
    public Page<TheForeignCurrencyVO> selectTheForeignCurrency8(Map<String,Object> param);
    
    
}
