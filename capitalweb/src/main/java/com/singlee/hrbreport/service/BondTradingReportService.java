package com.singlee.hrbreport.service;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName BondTradingReportService.java
 * @Description 现券买卖内部成交单
 * @createTime 2021年10月12日 10:08:00
 */
public interface BondTradingReportService extends ReportService{
}
