package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.CurAssetVO;
/**
 * _优质流动性资产台账
 * @author zk
 */
public interface CurAssetService extends ReportService {
    
    public Page<CurAssetVO> queryCurrentAssetsPage(Map<String , Object> param);
    
    public List<CurAssetVO> queryCurrentAssetsList(Map<String , Object> param);
    
}
