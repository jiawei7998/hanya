package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.hrbreport.mapper.PCapitalTransactionMapper;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;
import com.singlee.hrbreport.model.PCapitalTransactionVO;
import com.singlee.hrbreport.service.PCapitalTransactionService;
import com.singlee.hrbreport.util.HrbReportUtils;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 自营基金交易表
 */
@Service
public class PcapitalTransactionServiceImpl implements PCapitalTransactionService  {

    @Autowired
    PCapitalTransactionMapper pca;

  
    @Override
    public List<PCapitalTransactionVO> selectPCapitalTransaction(Map<String, Object> param) {
        HashMap<String,Object> remap =new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName); 
        remap.put("HRBCB", SystemProperties.hrbcbUserName);
        remap.put("startDate", param.get("startDate"));
        remap.put("endDate", param.get("endDate"));
        
        return pca.searchPCapitalTransaction(remap);
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<PCapitalTransactionVO> list =selectPCapitalTransaction(paramer);
        mapEnd.put("list", list);
        return mapEnd;
    }


    @Override
    public Page<PCapitalTransactionVO> selectPCapitalTransactionPage(Map<String, Object> param) {

        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<PCapitalTransactionVO> list2 = selectPCapitalTransaction(param);
      
        Page <PCapitalTransactionVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }
}


    
    
    
    

