package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.mapper.AcctLossLimitMapper;
import com.singlee.hrbreport.service.AcctLossLimitService;
import com.singlee.refund.model.CFtAfpConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName AcctLossLimitServiceImpl.java
 * @Description 交易帐户浮亏止损限额表
 * @createTime 2021年10月12日 10:30:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AcctLossLimitServiceImpl implements AcctLossLimitService {
    @Autowired
    AcctLossLimitMapper acctLossLimitMapper;

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        List<Object> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        return map;
    }
}
