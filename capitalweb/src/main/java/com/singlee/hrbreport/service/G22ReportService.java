package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G22流动性比例监测表
 *
 */
public interface G22ReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
}
