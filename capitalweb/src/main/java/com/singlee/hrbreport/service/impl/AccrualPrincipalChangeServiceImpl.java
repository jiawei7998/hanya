package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.AccrualPrincipalChangeMapper;
import com.singlee.hrbreport.model.AccrualPrincilalChangeVO;
import com.singlee.hrbreport.service.AccrualPriChgService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 本金计提变动表
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccrualPrincipalChangeServiceImpl implements AccrualPriChgService {

    @Autowired
    AccrualPrincipalChangeMapper accruMapper;
    @Autowired
    DayendDateService dayendDateService;

    /**
     * _返回查询结果page
     */
    @Override
    public Page<AccrualPrincilalChangeVO> getReportPage(Map<String, Object> paramer){
        
        int pageNum = Integer.parseInt(paramer.get("pageNum").toString());
        int pageSize = Integer.parseInt(paramer.get("pageSize").toString());
        
        Page<AccrualPrincilalChangeVO> page = HrbReportUtils.producePage(getReportList(paramer),pageNum,pageSize);
        return page;
    }

    /**
     * _返回查询结果List
     */
    @Override
    public List<AccrualPrincilalChangeVO> getReportList(Map<String, Object> paramer) {
        List<AccrualPrincilalChangeVO> accList = new ArrayList<AccrualPrincilalChangeVO>();
        String ccy = paramer.get("CCY") != null ? paramer.get("CCY").toString() : "CNY";
        
        String RPTDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString("yyyy-MM")); 
        RPTDate = "".equals(RPTDate)?DateUtil.getCurrentDateAsString("yyyy-MM"):RPTDate;
        Date queryDay = DateUtil.parse(RPTDate.substring(0,7),"yyyy-MM");
        
        Map<String, Object> para = new HashMap<String, Object>();
        para.put("CCY", ccy);
        para.put("OPICS", SystemProperties.opicsUserName);
        para.put("HRBCB", SystemProperties.hrbcbUserName);

        NumberFormat nf = NumberFormat.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        nf.setGroupingUsed(false);
        List noList = new ArrayList();
        if ((queryDay != null) && (!"".equals(queryDay))) {
            String startDate = sf.format(CalendarUtil.getFirstDayOfMonth(queryDay));
            String endDate = sf.format(CalendarUtil.getLastDayOfMonth(queryDay));
            // 获取当月的天数作为下面循环的次数
            int count = Integer.parseInt(endDate.substring(6)) - Integer.parseInt(startDate.substring(6)) + 1;
            BigDecimal countb = new BigDecimal(count);
            // 获取acctnoList
            noList = queryAcctNo(startDate, para);
            String[][] data = new String[count][noList.size()];

            for (int i = 0; i < count; i++) {
                List<Map<String, Object>> temp = queryData(startDate, para, noList);
                int j = 0;
                for (Map<String, Object> map : temp) {
                    if (map.get("CURBALANCE") == null) {
                        data[i][j] = (map.get("CMNE") + "-" + "0");
                    }
                    else {
                        data[i][j] = (map.get("CMNE") + "-" + map.get("CURBALANCE")).toString();
                    }
                    j++;
                }
                startDate = getNextDay(startDate);
            }

            for (int i = 0; i < noList.size(); i++) {
                BigDecimal sumAmt = new BigDecimal("0.0");
                BigDecimal balAmt = new BigDecimal("0.0");
                AccrualPrincilalChangeVO accVO = new AccrualPrincilalChangeVO();

                for (int j = 0; j < count; j++) {
                    String[] temp = data[j][i].split("-");
                    if(accVO != null && accVO.getCmne() == null) {
                        accVO.setCmne(temp[0]);
                    }
                    BigDecimal temp1 = new BigDecimal(temp[1]);
                    try {

                        Method method = accVO.getClass().getDeclaredMethod("setDate" + (j + 1),BigDecimal.class);
                        method.invoke(accVO, temp1);

                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    if ((temp[1] == null) || (temp[1].toString() == null)) {
                        sumAmt = sumAmt.add(new BigDecimal("0.0"));
                    } else {
                        sumAmt = sumAmt.add(temp1);
                    }
                    balAmt = sumAmt.divide(countb,10,BigDecimal.ROUND_HALF_UP);

                }
                accVO.setSumamt(sumAmt.abs());
                accVO.setBalamt(balAmt);
                accList.add(accVO);
            }
        }
        return accList;
    }

    /**
     * _ 查询queryHoliday 和 queryAcctNo
     * 
     * @param startdate
     * @param acctPara
     * @return
     */
    private List<Map<String, Object>> queryAcctNo(String startdate, Map<String, Object> acctPara) {
        // 1.跳过节假日
        if (isHolidDate(startdate, acctPara)) {
            startdate = getLastDate(startdate, acctPara);
        }
        // 2.调用mapper,查询结果
        acctPara.put("RPTDate", startdate);
        List<Map<String, Object>> acctList = accruMapper.queryAcctNo(acctPara);
        return acctList;
    }

    /**
     * _ 查询queryPrincipal1 和 queryPrincipal2
     * 
     * @param startdate
     * @param para
     * @param allNo
     * @return
     */
    private List<Map<String, Object>> queryData(String startdate, Map<String, Object> para,
            List<Map<String, Object>> allNo) {

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        if (isHolidDate(startdate, para)) {
            startdate = getLastDate(startdate, para);
        }
        para.put("RPTDate", startdate);

        List<Map<String, Object>> queryList = accruMapper.queryPrincipal1(para);
        List<Map<String, Object>> strList = accruMapper.queryPrincipal2(para);

        for (Map map : allNo) {
            for (Map queryMap : queryList) {
                if (map.get("CMNE").equals(queryMap.get("CMNE"))) {
                    BigDecimal queryb = new BigDecimal(queryMap.get("CURBALANCE").toString());
                    map.put("CURBALANCE", queryb);
                }
            }
            for (Map strMap : strList) {
                if (map.get("CMNE").equals(strMap.get("CMNE"))) {
                    BigDecimal strb = new BigDecimal(strMap.get("CURBALANCE").toString());
                    map.put("CURBALANCE", strb);
                }
            }
            dataList.add(map);
        }
        return dataList;
    }

    /**
     * _ 判断传入的日期是否是节假日
     * 
     * @param date
     * @return
     */
    private boolean isHolidDate(String date, Map<String, Object> map) {
        map.put("calDate",date);

        boolean flag =dayendDateService.queryJJR(map)>0?true:false;
        return flag;
    }

    /**
     * holidate传入节假日,返回节假日前的最后一个工作日
     * 
     * @return
     */
    private String getLastDate(String holidate, Map<String, Object> map) {

        do {
            holidate = getLastDay(holidate);
        }
        while (isHolidDate(holidate, map));
        return holidate;
    }

    /**
     * _获取前一天的日期
     * 
     * @return
     */
    private String getLastDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, -1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    
    /**
     * _获取后一天的日期
     * 
     * @return
     */
    private String getNextDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> mapend = new HashMap<String, Object>();
        List<AccrualPrincilalChangeVO> reportList = getReportList(paramer);
        mapend.put("list", reportList);
        return mapend;
    }
}
