package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.service.BondTradingReportService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName BondTradingReportServiceImpl.java
 * @Description 现券买卖内部成交单
 * @createTime 2021年10月12日 10:09:00
 */
@Service
public class BondTradingReportServiceImpl implements BondTradingReportService {


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
