package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G31有价证券及投资情况表
 *
 */
public interface G31ReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
}
