package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.SlCdBalanceVO;
/**
 * _同业存单发行情况
 * @author zk
 */
public interface SlCdBalanceService extends ReportService {
    
    public Page<SlCdBalanceVO> searchSlcdBalaPage(Map<String, Object> param);
    
    public List<SlCdBalanceVO> searchSlcdBalaList(Map<String, Object> param);
    
}
