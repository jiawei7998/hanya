package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G42ReportMapper;
import com.singlee.hrbreport.service.G42ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G42表内加权风险资产计算表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G42ReportServiceImpl implements G42ReportService {
	
	@Autowired
	private G42ReportMapper g42ReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String,Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);//报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());//工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());//姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		Map<String,String> mapPar = new HashMap<String, String>();
		mapPar.put("date", date);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", "01"));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);
		
		mapPar.put("sqlType", "1");
		List<Map<String, Object>> listSec1 = g42ReportMapper.getG42ReportSec(mapPar);//33
		Object BALC33 = 0.00;
		Object BALC34 = 0.00;
		if (listSec1.size() > 0 && listSec1 != null) {
			for (Map<String, Object> map : listSec1) {
				if ("33".equals(map.get("TYPE").toString())) {
					BALC33 = map.get("BAL");
				}
				if ("34".equals(map.get("TYPE").toString())) {
					BALC34 = map.get("BAL");
				}
			}
		}
		mapEnd.put("BALC33", new BigDecimal(BALC33.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC34", new BigDecimal(BALC34.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> listSec2 = g42ReportMapper.getG42ReportSec(mapPar);//c
		Object BALC21 = 0.00;Object BALC23 = 0.00;
		Object BALC721 = 0.00;Object BALC722 = 0.00;
		if (listSec2.size() > 0 && listSec2 != null) {
			for (Map<String, Object> map : listSec2) {
				if ("1".equals(map.get("TYPE").toString())) {
					BALC21 = map.get("BAL");
				}
				if ("2".equals(map.get("TYPE").toString())) {
					BALC23 = map.get("BAL");
					mapEnd.put("BALC23", new BigDecimal(map.get("BAL").toString()).divide(new BigDecimal("10000.0"), 2,BigDecimal.ROUND_HALF_UP));
				}
				if ("3".equals(map.get("TYPE").toString())) {
					mapEnd.put("BALC721", new BigDecimal(map.get("BAL").toString()).divide(new BigDecimal("10000.0"), 2,BigDecimal.ROUND_HALF_UP));
					BALC721 = map.get("BAL");
				}
				if ("4".equals(map.get("TYPE").toString())) {
					BALC722 = map.get("BAL");
					mapEnd.put("BALC722", new BigDecimal(map.get("BAL").toString()).divide(new BigDecimal("10000.0"), 2,BigDecimal.ROUND_HALF_UP));
				}
			}
		}
		mapEnd.put("BALC21", new BigDecimal (BALC21.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC23", new BigDecimal (BALC23.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC721", new BigDecimal (BALC721.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC722", new BigDecimal (BALC722.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));

		mapPar.put("sqlType", "1");
		List<Map<String, Object>> rprh1 = g42ReportMapper.getG42ReportRPRH(mapPar);//411
		Object BALC411=0.00;
		if(rprh1!=null&&rprh1.size() > 0&&rprh1.get(0) != null && rprh1.get(0).get("BAL")!=null){
			BALC411=rprh1.get(0).get("BAL");
		}
		
		List<Map<String, Object>> dldt1 = g42ReportMapper.getG42ReportDLDT(mapPar);//412
		Object BALC412=0.00;
		if(dldt1!=null&&dldt1.size() > 0&&dldt1.get(0) != null && dldt1.get(0).get("BAL")!=null){
			BALC412=dldt1.get(0).get("BAL");
		}

		BigDecimal BALC44= new BigDecimal(0);
		List<Map<String, Object>> tdldt = g42ReportMapper.getG42ReportTradeDLDT(mapPar);//431
		Object BALC4311=0.00;Object BALC4312=0.00;
		if (tdldt.size() > 0 && tdldt != null) {
			for (Map<String, Object> map : tdldt) {

				if ("COMMBK-D".equals(map.get("ACCTNGTYPE").toString())) {
					if ("1".equals(map.get("TENOR").toString())) {
						BALC4312 = map.get("BAL");
					}
					if ("2".equals(map.get("TENOR").toString())) {
						BALC4311 = map.get("BAL");
					}
				}
				if ("OTHER".equals(map.get("ACCTNGTYPE").toString())) {
					BALC44 = BALC44.add(new BigDecimal(map.get("BAL").toString()));
				}

			}
		}

		List<Map<String, Object>> trprh = g42ReportMapper.getG42ReportTradeRPRH(mapPar);//432
		Object BALC4321=0.00;Object BALC4322=0.00;
		if (trprh.size() > 0 && trprh != null) {
			for (Map<String, Object> map : trprh) {
				if ("COMMBK-D".equals(map.get("ACCTNGTYPE"))) {
					if ("1".equals(map.get("TENOR").toString())) {
						BALC4322 = map.get("BAL");
					}
					if ("2".equals(map.get("TENOR").toString())) {
						BALC4321 = map.get("BAL");
					}
				}
				if ("OTHER".equals(map.get("ACCTNGTYPE").toString())) {
					BALC44 = BALC44.add(new BigDecimal(map.get("BAL").toString()));
				}

			}
		}

		Object BALC4331=0.00;Object BALC4332=0.00;
		List<Map<String, Object>> otherSec1 = g42ReportMapper.getG42ReportOtherSec(mapPar);//433
		if (otherSec1.size() > 0 && otherSec1 != null) {
			for (Map<String, Object> map : otherSec1) {
				if (map.get("ACCTNGTYPE") != null && map.get("TENOR") != null && map.get("BAL") != null) {
					if ("DOMFINSE".equals(map.get("ACCTNGTYPE").toString())) {
						if ("1".equals(map.get("TENOR").toString())) {
							BALC4332 = map.get("BAL");
						}
						if ("2".equals(map.get("TENOR").toString())) {
							BALC4331 = map.get("BAL");
						}
					}
				}
				if ("OTHER".equals(map.get("ACCTNGTYPE").toString())) {
					BALC44 = BALC44.add(new BigDecimal(map.get("BAL").toString()));
				}
			}
		}
		

		BigDecimal BAL61=new BigDecimal(0.00);
		Object BALC413=0.00;//DOMPOLICSE债券-政策性金融债
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> dldt2 = g42ReportMapper.getG42ReportDLDT(mapPar);//611
		if (dldt2!=null&&dldt2.size() > 0&&dldt2.get(0) != null && dldt2.get(0).get("BAL") != null) {
			BAL61 = BAL61.add(new BigDecimal(dldt2.get(0).get("BAL").toString()));
		}
		
		List<Map<String, Object>> rprh2 = g42ReportMapper.getG42ReportRPRH(mapPar);//612
		if (rprh2!=null&&rprh2.size() > 0&&rprh2.get(0) != null && rprh2.get(0).get("BAL") != null) {
			BAL61 = BAL61.add(new BigDecimal(rprh2.get(0).get("BAL").toString()));
		}
		
		List<Map<String, Object>> otherSec2 = g42ReportMapper.getG42ReportOtherSec(mapPar);//613
		if (otherSec2.size() > 0 && otherSec2 != null) {
			for (Map<String, Object> map : otherSec2) {
				if (map.get("ACCTNGTYPE") != null) {
					if (map.get("BAL") != null) {
						if ("DOMNOFINSE".equals(map.get("ACCTNGTYPE").toString())) {
							BAL61 = BAL61.add(new BigDecimal(map.get("BAL").toString()));
						}
						if ("DOMPOLICSE".equals(map.get("ACCTNGTYPE").toString())) {
							BALC413 = map.get("BAL");
						}
					}

				}

			}
		}
		
		mapEnd.put("BALC61", BAL61.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC41", (new BigDecimal(BALC411.toString()).add(new BigDecimal(BALC412.toString())).add(new BigDecimal(BALC413.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC431", (new BigDecimal(BALC4311.toString()).add(new BigDecimal(BALC4321.toString())).add(new BigDecimal(BALC4331.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC432", (new BigDecimal(BALC4312.toString()).add(new BigDecimal(BALC4322.toString())).add(new BigDecimal(BALC4332.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALC44", BALC44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		List<Map<String, Object>> eg = g42ReportMapper.getG42ReportEG(mapPar);//eg
		BigDecimal BALE41=new  BigDecimal(0.00);BigDecimal BALF41=new  BigDecimal(0.00);
		BigDecimal BALG41=new  BigDecimal(0.00);BigDecimal BALH41=new  BigDecimal(0.00);BigDecimal BALJ41=new  BigDecimal(0.00);
		
		BigDecimal BALE431=new  BigDecimal(0.00);BigDecimal BALF431=new  BigDecimal(0.00);
		BigDecimal BALG431=new  BigDecimal(0.00);BigDecimal BALH431=new  BigDecimal(0.00);BigDecimal BALJ431=new  BigDecimal(0.00);
		
		
		BigDecimal BALE432=new  BigDecimal(0.00);BigDecimal BALF432=new  BigDecimal(0.00);
		BigDecimal BALG432=new  BigDecimal(0.00);BigDecimal BALH432=new  BigDecimal(0.00);BigDecimal BALJ432=new  BigDecimal(0.00);
		
		 
		BigDecimal BALE44=new  BigDecimal(0.00);BigDecimal BALF44=new  BigDecimal(0.00);
		BigDecimal BALG44=new  BigDecimal(0.00);BigDecimal BALH44=new  BigDecimal(0.00);BigDecimal BALJ44=new  BigDecimal(0.00);
		
		BigDecimal BALE62=new  BigDecimal(0.00);BigDecimal BALF62=new  BigDecimal(0.00);
		BigDecimal BALG62=new  BigDecimal(0.00);BigDecimal BALH62=new  BigDecimal(0.00);BigDecimal BALJ62=new  BigDecimal(0.00);
		
		if (eg.size() > 0 && eg != null) {
			for (Map<String, Object> map : eg) {
				if (map.get("ACCTNGTYPE") != null) {
					if ("POLICYBK".equals(map.get("ACCTNGTYPE"))) {
						if (map.get("TENOR") != null) {
							if ("1".equals(map.get("TENOR").toString())) {
								BALE41 = BALE41.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("2".equals(map.get("TENOR").toString())) {
								BALF41 = BALF41.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("3".equals(map.get("TENOR").toString())) {
								BALG41 = BALG41.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("4".equals(map.get("TENOR").toString())) {
								BALH41 = BALH41.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("5".equals(map.get("TENOR").toString())) {
								BALJ41 = BALJ41.add(new BigDecimal(map.get("BAL").toString()));
							}
						}
					}
					if ("COMMBK-D1".equals(map.get("ACCTNGTYPE"))) {
						if (map.get("TENOR") != null) {
							if ("1".equals(map.get("TENOR").toString())) {
								BALE431 = BALE431.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("2".equals(map.get("TENOR").toString())) {
								BALF431 = BALF431.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("3".equals(map.get("TENOR").toString())) {
								BALG431 = BALG431.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("4".equals(map.get("TENOR").toString())) {
								BALH431 = BALH431.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("5".equals(map.get("TENOR").toString())) {
								BALJ431 = BALJ431.add(new BigDecimal(map.get("BAL").toString()));
							}
						}
					}
					if ("COMMBK-D2".equals(map.get("ACCTNGTYPE"))) {
						if (map.get("TENOR") != null) {
							if ("1".equals(map.get("TENOR").toString())) {
								BALE432 = BALE432.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("2".equals(map.get("TENOR").toString())) {
								BALF432 = BALF432.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("3".equals(map.get("TENOR").toString())) {
								BALG432 = BALG432.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("4".equals(map.get("TENOR").toString())) {
								BALH432 = BALH432.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("5".equals(map.get("TENOR").toString())) {
								BALJ432 = BALJ432.add(new BigDecimal(map.get("BAL").toString()));
							}
						}
					}
					if ("OTHER".equals(map.get("ACCTNGTYPE"))) {
						if (map.get("TENOR") != null) {
							if ("1".equals(map.get("TENOR").toString())) {
								BALE44 = BALE44.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("2".equals(map.get("TENOR").toString())) {
								BALF44 = BALF44.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("3".equals(map.get("TENOR").toString())) {
								BALG44 = BALG44.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("4".equals(map.get("TENOR").toString())) {
								BALH44 = BALH44.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("5".equals(map.get("TENOR").toString())) {
								BALJ44 = BALJ44.add(new BigDecimal(map.get("BAL").toString()));
							}
						}
					}
					if ("ENTERPRISE".equals(map.get("ACCTNGTYPE"))) {
						if (map.get("TENOR") != null) {
							if ("1".equals(map.get("TENOR").toString())) {
								BALE62 = BALE62.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("2".equals(map.get("TENOR").toString())) {
								BALF62 = BALF62.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("3".equals(map.get("TENOR").toString())) {
								BALG62 = BALG62.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("4".equals(map.get("TENOR").toString())) {
								BALH62 = BALH62.add(new BigDecimal(map.get("BAL").toString()));
							}
							if ("5".equals(map.get("TENOR").toString())) {
								BALJ62 = BALJ62.add(new BigDecimal(map.get("BAL").toString()));
							}
						}
					}
				}
			}
		}
		
		mapEnd.put("BALE41", BALE41.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALF41", BALF41.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALG41", BALG41.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALH41", BALH41.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALJ41", BALJ41.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		mapEnd.put("BALE431", BALE431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALF431", BALF431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALG431", BALG431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALH431", BALH431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALJ431", BALJ431.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		mapEnd.put("BALE432", BALE432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALF432", BALF432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALG432", BALG432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALH432", BALH432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALJ432", BALJ432.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		mapEnd.put("BALE44", BALE44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALF44", BALF44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALG44", BALG44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALH44", BALH44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALJ44", BALJ44.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		mapEnd.put("BALE62", BALE62.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALF62", BALF62.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALG62", BALG62.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALH62", BALH62.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("BALJ62", BALJ62.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		
		return mapEnd;
	}

}
