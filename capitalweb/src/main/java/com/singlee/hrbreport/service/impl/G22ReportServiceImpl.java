package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G22ReportMapper;
import com.singlee.hrbreport.service.G22ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G22流动性比例监测表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G22ReportServiceImpl implements G22ReportService {

	@Autowired
	private G22ReportMapper g22ReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("date", date);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", "01"));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		Object RPT141 = 0.00;
		Object RPT142 = 0.00;
		Object RPT231 = 0.00;
		Object RPT232 = 0.00;

		Object RPT15 = 0.00; // 债券的利息
		Object RPT151 = 0.00;// 逆回购的利息
		Object RPT152 = 0.00;// 拆放同业的利息
		Object RPT251 = 0.00;
		Object RPT252 = 0.00;

		List<Map<String, Object>> dlAmtMap = g22ReportMapper.getG22ReportDlAmt(mapPar);
		if (dlAmtMap.size() > 0 && dlAmtMap != null) {
			for (Map<String, Object> map : dlAmtMap) {
				if (map.get("AL") != null && map.get("CCYAMT") != null) {
					if ("A".equals(map.get("AL"))) {
						RPT141 = map.get("CCYAMT");
					}
					if ("L".equals(map.get("AL"))) {
						RPT231 = map.get("CCYAMT");
					}
				}

			}
		}
		List<Map<String, Object>> repoAmtMap = g22ReportMapper.getG22ReportRepoAmt(mapPar);
		if (repoAmtMap.size() > 0 && repoAmtMap != null) {
			for (Map<String, Object> map : repoAmtMap) {
				if (map.get("PS") != null) {
					if ("P".equals(map.get("PS"))) {
						if (map.get("NOTAMT") != null) {
							RPT142 = map.get("NOTAMT");
						}
					}
					if ("S".equals(map.get("PS"))) {
						if (map.get("NOTAMT") != null) {
							RPT232 = map.get("NOTAMT");
						}
					}
				}

			}
		}
		BigDecimal RPT14 = new BigDecimal(RPT141.toString()).add(new BigDecimal(RPT142.toString()));
		BigDecimal RPT23 = new BigDecimal(RPT231.toString()).add(new BigDecimal(RPT232.toString()));
		if (RPT14.compareTo(RPT23) >= 0) {
			mapEnd.put("RPT14", (RPT14.subtract(RPT23)).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			mapEnd.put("RPT23", 0.00);
		} else {
			mapEnd.put("RPT23", (RPT23.subtract(RPT14)).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			mapEnd.put("RPT14", 0.00);
		}

		mapPar.put("sqlType", "1");
		List<Map<String, Object>> bond1 = g22ReportMapper.getG22ReportBond(mapPar);
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> bond2 = g22ReportMapper.getG22ReportBond(mapPar);
		if (bond1 != null && bond1.size() > 0 && bond2 != null && bond2.size() > 0 && bond1.get(0) != null && bond2.get(0) != null) {
			if (bond1.get(0).get("ACCROUTSTTPOS") != null) {
				RPT15 = new BigDecimal(bond1.get(0).get("ACCROUTSTTPOS").toString())
						.add(new BigDecimal(RPT15.toString()));
			}
			if (bond2.get(0).get("ACCROUTSTTPOS") != null) {
				RPT15 = new BigDecimal(RPT15.toString())
						.subtract(new BigDecimal(bond2.get(0).get("ACCROUTSTTPOS").toString()));
			}
		}

		List<Map<String, Object>> dlRateMap = g22ReportMapper.getG22ReportDlRate(mapPar);
		if (dlRateMap.size() > 0 && dlRateMap != null) {
			for (Map<String, Object> map : dlRateMap) {
				if (map.get("AL") != null && map.get("ACCROUTSTDLDT") != null) {
					if ("A".equals(map.get("AL"))) {
						RPT151 = map.get("ACCROUTSTDLDT");
					} else {
						RPT251 = map.get("ACCROUTSTDLDT");
					}
				}
			}
		}
		List<Map<String, Object>> repoRateMap = g22ReportMapper.getG22ReportRepoRate(mapPar);
		if (repoRateMap.size() > 0 && repoRateMap != null) {
			for (Map<String, Object> map : repoRateMap) {
				if (map.get("TYPERPRH") != null && map.get("ACCROUTSTRPRH") != null) {
					if ("VBP".equals(map.get("TYPERPRH"))) {
						RPT152 = map.get("ACCROUTSTRPRH");
					} else {
						RPT252 = map.get("ACCROUTSTRPRH");
					}
				}
			}
		}
		mapEnd.put("RPT15",
				(new BigDecimal(RPT15.toString()).add(new BigDecimal(RPT151.toString()))
						.add(new BigDecimal(RPT152.toString()))).divide(new BigDecimal("10000.0"), 2,
								BigDecimal.ROUND_HALF_UP));
		mapEnd.put("RPT25", (new BigDecimal(RPT251.toString()).add(new BigDecimal(RPT252.toString())))
				.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));

		Object RPT17 = 0.00;
		Object RPT18 = 0.00;
		Object RPT171 = 0.00;
		Object RPT181 = 0.00;
		List<Map<String, Object>> bondHat = g22ReportMapper.getG22ReportBondHat(mapPar);
		if (bondHat.size() > 0 && bondHat != null&&bondHat.get(0) != null && bondHat.get(0).get("BAL") != null) {
			RPT17 = bondHat.get(0).get("BAL");
		}
		List<Map<String, Object>> sec = g22ReportMapper.getG22ReportSec(mapPar);
		if (sec.size() > 0 && sec != null&&sec.get(0) != null && sec.get(0).get("BAL") != null) {
			RPT18 = sec.get(0).get("BAL");
		}

		mapPar.put("sqlType", "1");
		List<Map<String, Object>> bp1 = g22ReportMapper.getG22ReportBondPledge(mapPar);
		if (bp1.size() > 0 && bp1 != null&&bp1.get(0) != null && bp1.get(0).get("BAL") != null) {
			RPT171 = bp1.get(0).get("BAL");
		}
		mapPar.put("sqlType", "2");
		List<Map<String, Object>> bp2 = g22ReportMapper.getG22ReportBondPledge(mapPar);
		if (bp2.size() > 0 && bp2 != null&&bp2.get(0) != null && bp2.get(0).get("BAL") != null) {
			RPT181 = bp2.get(0).get("BAL");
		}
		mapEnd.put("RPT17", ((new BigDecimal(RPT17.toString()).subtract(new BigDecimal(RPT171.toString())))
				.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));
		mapEnd.put("RPT18", ((new BigDecimal(RPT18.toString()).subtract(new BigDecimal(RPT181.toString())))
				.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP)));

		return mapEnd;
	}

}
