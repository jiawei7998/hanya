package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbreport.mapper.EstSecPosMapper;
import com.singlee.hrbreport.model.EstSecPosVO;
import com.singlee.hrbreport.service.EstSecPosService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * _其他形式流入房地产
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EstSecPosServiceImpl implements EstSecPosService {
    
    @Autowired
    EstSecPosMapper estSecPosMapper;
    
    @Override
    public Page<EstSecPosVO> queryEstateSecBalancePage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<EstSecPosVO> queryEstateSecBalanceList = queryEstateSecBalanceList(param);
        return HrbReportUtils.producePage(queryEstateSecBalanceList, pageNum, pageSize);
    }

    @Override
    public List<EstSecPosVO> queryEstateSecBalanceList(Map<String, Object> param) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = sf.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = sf.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<EstSecPosVO> queryEstateSecBalance = estSecPosMapper.queryEstateSecBalance(param);
        return queryEstateSecBalance;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        List<EstSecPosVO> queryEstateSecBalanceList = queryEstateSecBalanceList(paramer);
        mapend.put("list", queryEstateSecBalanceList);
        return mapend;
    }
    
    
}
