package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.TaHrbReport;

import java.util.Map;

/**
 * 模板查询
 */
public interface TaHrbReportService extends ReportService{
    Page<TaHrbReport> searchTaHrbReportPage(Map<String, Object> params);

    TaHrbReport searchTaHrbReport(Map<String, Object> params);
}
