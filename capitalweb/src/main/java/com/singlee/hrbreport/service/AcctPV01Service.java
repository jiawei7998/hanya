package com.singlee.hrbreport.service;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName AcctPV01Service.java
 * @Description 交易帐户组合PV01和组合久期限额情况表
 * @createTime 2021年10月12日 11:17:00
 */
public interface AcctPV01Service extends ReportService{
}
