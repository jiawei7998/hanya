package com.singlee.hrbreport.service;

import java.util.Map;
/**
 * g52地方政府融资平台及支出责任债务持有情况统计表
 * @author zk
 *
 */
public interface G52ReportService extends ReportService {
    
    public Map<String , Object> query(Map<String , Object> param);
    
}
