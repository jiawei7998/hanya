package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G18ReportMapper;
import com.singlee.hrbreport.service.G18ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;



/**
 * @author Administrator
 *地方政府自主发行债券持有情况统计表
 */

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G18ReportServiceImpl implements G18ReportService {
	
	@Autowired
	private G18ReportMapper g18;

	@Override
	public HashMap<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		HashMap<String,Object> mapEnd = new HashMap<String,Object>();
		String br = paramer.get("BR");
		String RPTDate = HrbReportUtils.getDate(postdate);
		String SRPTDate = HrbReportUtils.dateToMap(postdate);

		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", RPTDate.substring(0, 4)+"年"+RPTDate.substring(4, 6)+"月"+RPTDate.substring(6, 8)+"日");

		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("BR", br);
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", paramer.get("OPICS"));
		paraMap.put("HRBCB", paramer.get("HRBCB"));
		
		
		try {
			
			List<Map<String, Object>> list = g18.getsql(paraMap);
			for (Map<String, Object> map : list) {		
				paraMap.put("ACCTNGTYPE",map.get("ACCTNGTYPE" ) );
				//2：地方政府债   3：国内中央银行  5：政策性金融债   15：外国政府
				List<Map<String, Object>> list1 = new ArrayList<Map<String,Object>>();
				if("2".equals(map.get("NO").toString())|| "3".equals(map.get("NO").toString())|| "5".equals(map.get("NO").toString())|| "15".equals(map.get("NO").toString())){
					list1 = g18.getsql1(paraMap);
         	 } 
				//国债
            	 if("1".equals(map.get("NO").toString())){
            		 list1 = g18.getsql2(paraMap);
           
 	 } 
            	 if("4".equals(map.get("NO").toString())){
            		   list1 = g18.getsql3(paraMap);
            		 
       	 } 
            	 if("6".equals(map.get("NO").toString())){
            		   list1 = g18.getsql4(paraMap);
            	 }
            	 //     1.3.3其中:非银行金融债券
            	 
            	 /*ACTY:DOMNBFINSE
            	 OR --sd like 短期融资券, 中期票据
            	 非银行金融机构债*/
            	 if("7".equals(map.get("NO").toString())){
            		   list1 = g18.getsql5(paraMap);
            		 
         	 }
                //  1.4非金融企业债券
            	 if("8".equals(map.get("NO").toString())){
            		   list1 = g18.getsql6(paraMap);
            	 }
            	 /*ACTY:DOMNOFINSE   债券-非金融债
            	 SICO.SD:存在“企业”、“企业债”*/
            	 //    1.4.1其中:企业债
            	 if("9".equals(map.get("NO").toString())){
            		   list1 = g18.getsql7(paraMap);
            		 
           	 }
            	 /*wy: ACTY:DOMNOFINSE   债券-非金融债 SICO.SD:存在“企业”、“公司债”*/
            	 if("10".equals(map.get("NO").toString())){
            		   list1 = g18.getsql8(paraMap);
			}
            	 /*wy: ACTY:DOMNOFINSE   债券-非金融债  SICO.SD:存在“企业”、“短期融资券”*/
            	 if("11".equals(map.get("NO").toString())){
            		   list1 = g18.getsql9(paraMap);
     	 }
            	 /*wy: ACTY:DOMNOFINSE   债券-非金融债 SICO.SD:存在“企业”、“中期票据”*/
            	 if("12".equals(map.get("NO").toString())){
            		   list1 = g18.getsql10(paraMap);
            		 
           	 }
            	 /*wy: ACTY:DOMNOFINSE   债券-非金融债 SICO.SD:存在“企业”、“中期票据”*/
            	 if("13".equals(map.get("NO").toString())){
            		   list1 = g18.getsql11(paraMap);
				}
            	 if("14".equals(map.get("NO").toString())){
            		   list1 = g18.getsql12(paraMap);
            		 
					}
            	 if("16".equals(map.get("NO").toString())){
            		   list1 = g18.getsql13(paraMap);
            		 
       	 }
            	if("18".equals(map.get("NO").toString())|| "19".equals(map.get("NO").toString())|| "20".equals(map.get("NO").toString())){
            		
            		  list1 = g18.getsql14(paraMap);
              
            	}
            	
            	if("23".equals(map.get("NO").toString())){
            		  list1 = g18.getsql15(paraMap);
           		}
            	/***
            	 * SECM.MDATE - 报告日 >= 1年 SECM.MDATE - 报告日 < 5年
            	 */
            	if("24".equals(map.get("NO").toString())){
            		  list1 = g18.getsql16(paraMap);
      	}
            	/***
            	 * SECM.MDATE - 报告日 >= 5年 SECM.MDATE - 报告日 < 10年
            	 */
            	if("25".equals(map.get("NO").toString())){
            		  list1 = g18.getsql17(paraMap);
      	}
            	/***
            	 * SECM.MDATE - 报告日 >= 5年 SECM.MDATE - 报告日 > 10年
            	 */
            	if("26".equals(map.get("NO").toString())){
            		  list1 = g18.getsql18(paraMap);
       	}
        	 	if("21".equals(map.get("NO").toString())|| "17".equals(map.get("NO").toString())|| "22".equals(map.get("NO").toString())|| "27".equals(map.get("NO").toString())){
        	 		
        	 	}else{
        	 		
                  System.out.println(map.get("NO").toString());
                 
                  System.out.println(list1);
					if (list1 != null && list1.size() > 0) {

//						if (list1.get(0) == null) {
//							list1.add(0, paraMap);
//							System.out.println("list1的第一个数据是空的 ");
//						}
						if (list1.get(0).get("BAL") != null) {

							map.put("BAL", list1.get(0).get("BAL").toString());
						}
						if (list1.get(0).get("PROFITLOSS") != null) {
							map.put("PROFITLOSS", list1.get(0).get("PROFITLOSS").toString());
						}
					}
        	 	}
            	 	if(map.get("BAL")!=null){
            	 		mapEnd.put("B"+map.get("NO").toString()+"AL", new BigDecimal(map.get("BAL").toString()).divide(new BigDecimal("10000.0"),6, BigDecimal.ROUND_HALF_UP));	
            	 	}else{
        	 			mapEnd.put("B"+map.get("NO").toString()+"AL", 0.000000);
        	 		}
            	 	//估值损益---万元计数--两位小数
            	 	if(map.get("PROFITLOSS")!=null){
            	 		mapEnd.put("V"+map.get("NO").toString()+"AL", new BigDecimal(map.get("PROFITLOSS").toString()).divide(new BigDecimal("10000.0"), 6, BigDecimal.ROUND_HALF_UP));
            	 	}else{
            	 		mapEnd.put("V"+map.get("NO").toString()+"AL",0.000000);
            	 	}   
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapEnd; 
		
		
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		return null;
	}


}
