package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.InBankDealMapper;
import com.singlee.hrbreport.model.*;
import com.singlee.hrbreport.service.InBankDealService;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
/**
 * _同业业务管理报告
 * @author zk
 */
@Service
public class InBankDealServiceImpl implements InBankDealService {

    @Autowired
    InBankDealMapper inBankDealMapper;
    
    /**
     * repo
     */
    @Override
    public Page<InBankDealRepoVO> queryRepoPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealRepoVO> queryRepoList = queryRepoList(param);
        return HrbReportUtils.producePage(queryRepoList, pageNum, pageSize);
    }

    @Override
    public List<InBankDealRepoVO> queryRepoList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        String dealType = ParameterUtil.getString(param, "dealtype", "");
        
        if("REVREPO".equals(dealType)) {
            param.put("ps", "P");
        }
        if("REPO".equals(dealType)) {
            param.put("ps", "S");
        }
        
        List<InBankDealRepoVO> queryRepoDetail = inBankDealMapper.queryRepoDetail(param);
        for(InBankDealRepoVO repoV: queryRepoDetail) {
            //查询质押债
            param.put("dealno", repoV.getDealno());
            List<String> queryPledge = inBankDealMapper.queryPledge(param);
            StringBuffer sb = new StringBuffer("");
            for(String desc:queryPledge) {
                sb.append(desc);
                sb.append(";");
            }
            repoV.setPledge(sb.toString());
        }
        return queryRepoDetail;
    }
    
    /**
     * sec position
     */
    @Override
    public Page<InBankDealSecVO> querySecPosDetailPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealSecVO> querySecPosDetailList = querySecPosDetailList(param);
        return HrbReportUtils.producePage(querySecPosDetailList, pageNum, pageSize);
    }

    @Override
    public List<InBankDealSecVO> querySecPosDetailList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        String dealType = ParameterUtil.getString(param, "dealtype", "");
        List<InBankDealSecVO> querySecPosDetail = new ArrayList<InBankDealSecVO>();
        if("SEC".equals(dealType)) {
            querySecPosDetail = inBankDealMapper.querySecPosDetail(param);
        }else {
            querySecPosDetail = inBankDealMapper.querySecCDPosDetail(param);
        }
        return querySecPosDetail;
    }

    /**
     * borrowing & loan
     */
    @Override
    public Page<InBankDealBrLoVO> queryBrLoPosDetailPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealBrLoVO> queryBrLoPosDetailList = queryBrLoPosDetailList(param);
        return HrbReportUtils.producePage(queryBrLoPosDetailList, pageNum, pageSize);
    }

    @Override
    public List<InBankDealBrLoVO> queryBrLoPosDetailList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<InBankDealBrLoVO> queryBrLoPosDetail = inBankDealMapper.queryBrLoDetail(param);
        return queryBrLoPosDetail;
    }
    
    /**
     * sec issue
     */
    @Override
    public Page<InBankDealCDSeVO> queryCDSEDetailPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealCDSeVO> queryCDSEDetailPage = queryCDSEDetailList(param);
        return HrbReportUtils.producePage(queryCDSEDetailPage, pageNum, pageSize);
    }

    @Override
    public List<InBankDealCDSeVO> queryCDSEDetailList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        param.put("issuer", SystemProperties.hrbReportCno);
        
        List<InBankDealCDSeVO> queryCDSEDetail = inBankDealMapper.queryCDSEDetail(param);
        return queryCDSEDetail;
    }
    
    /**
     * cd issue (人行报表)
     */
    @Override
    public Page<InBankDealCDSeVO> queryRHCDSEDetailPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealCDSeVO> queryCDSEDetailPage = queryRHCDSEDetailList(param);
        return HrbReportUtils.producePage(queryCDSEDetailPage, pageNum, pageSize);
    }
    
    @Override
    public List<InBankDealCDSeVO> queryRHCDSEDetailList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        param.put("issuer", SystemProperties.hrbReportCno);
        
        List<InBankDealCDSeVO> queryCDSEDetail = inBankDealMapper.queryRHCDSEDetail(param);
        return queryCDSEDetail;
    }
    
    /**
     * fund
     */
    @Override
    public Page<InBankDealFundVO> queryFundDetailPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<InBankDealFundVO> queryFundDetailList = queryFundDetailList(param);
        return HrbReportUtils.producePage(queryFundDetailList, pageNum, pageSize);
    }
    
    @Override
    public List<InBankDealFundVO> queryFundDetailList(Map<String, Object> param) {
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        param.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay)) ;
        String endDate = DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<InBankDealFundVO> queryFundDetail = inBankDealMapper.queryFundDetail(param);
        return queryFundDetail;
    }
    
    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }
    
    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        String id = ParameterUtil.getString(paramer, "id", "");
        
        //逆回购
        paramer.put("dealtype", "REVREPO");
        List<InBankDealRepoVO> revRepoList = queryRepoList(paramer);
        //正回购
        paramer.put("dealtype", "REPO");
        List<InBankDealRepoVO> repoList = queryRepoList(paramer);
        
        //投资存单
        paramer.put("dealtype", "SEC");
        List<InBankDealSecVO> secPosDList = querySecPosDetailList(paramer);
        //存单申购
        paramer.put("dealtype", "CD");
        List<InBankDealSecVO> secCDlList = querySecPosDetailList(paramer);
        
        //存放同业
        paramer.put("dealtype", "CT");
        List<InBankDealBrLoVO> ctList = queryBrLoPosDetailList(paramer);
        //同业存放
        paramer.put("dealtype", "DP");
        List<InBankDealBrLoVO> dpList = queryBrLoPosDetailList(paramer);
        //拆放同业
        paramer.put("dealtype", "LD");
        List<InBankDealBrLoVO> ldList = queryBrLoPosDetailList(paramer);
        //同业拆入
        paramer.put("dealtype", "BR");
        List<InBankDealBrLoVO> brList = queryBrLoPosDetailList(paramer);
        
        //存单发行
        paramer.put("dealtype", "CDSE");
        List<InBankDealCDSeVO> cdseList = queryCDSEDetailList(paramer);
        if("yjty".equals(id)) {
            cdseList = queryCDSEDetailList(paramer);
        }else if ("rhty".equals(id)) {
            cdseList = queryRHCDSEDetailList(paramer);
        }
        
        //公募基金
        paramer.put("dealtype", "FUND");
        List<InBankDealFundVO> fundList = queryFundDetailList(paramer);
        
        mapend.put("revRepoList", revRepoList);
        mapend.put("repoList", repoList);
        mapend.put("secPosDList", secPosDList);
        mapend.put("secCDlList", secCDlList);
        mapend.put("ctList", ctList);
        mapend.put("dpList", dpList);
        mapend.put("ldList", ldList);
        mapend.put("brList", brList);
        mapend.put("cdseList", cdseList);
        mapend.put("fundList", fundList);
        
        String queryDate = ParameterUtil.getString(paramer,"queryDate", DateUtil.getCurrentDateAsString());
        mapend.put("date", queryDate.substring(0, 7));
        return mapend;
    }
    
    
}
