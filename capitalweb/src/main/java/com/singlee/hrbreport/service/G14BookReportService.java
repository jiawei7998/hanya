package com.singlee.hrbreport.service;






import com.github.pagehelper.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;



import com.singlee.hrbreport.model.G14BookReportVO;


@Service
public interface G14BookReportService  extends ReportService{
	
    public Page<G14BookReportVO> getReportMap(Map<String, Object> paramer);
    
    public List<G14BookReportVO> getReportMapList(Map<String, Object> paramer);
    
    
}
 