package com.singlee.hrbreport.service;


import com.github.pagehelper.Page;
import com.singlee.hrbnewport.service.CommonExportService;

import java.util.List;
import java.util.Map;






/**
 * 各部门资产投放周报表
 * @author cg
 *
 */




public interface CapitalTurnoverService extends CommonExportService {

     //母行投资
     Page<Map<String,Object>> getCapitalTurnover(Map<String, Object> paramer);
    //母行投资
     List<Map<String,Object>> getReportMap2(Map<String, Object> paramer);

     void getCapitalData(Map<String, Object> paramer);

    
}
