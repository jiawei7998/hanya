package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.SameBasicInfoVO;

import java.util.List;
import java.util.Map;

public interface SameBasicInfoService extends ReportService{
    
    public Page<SameBasicInfoVO> queryBasicInfoPage(Map<String ,Object> param);
    
    public List<SameBasicInfoVO> queryBasicInfoList(Map<String ,Object> param);
    

}
