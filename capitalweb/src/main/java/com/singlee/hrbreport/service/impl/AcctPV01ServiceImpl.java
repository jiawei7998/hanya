package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.service.AcctPV01Service;
import com.singlee.refund.model.CFtAfpConf;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName AcctPV01ServiceImpl.java
 * @Description 交易帐户组合PV01和组合久期限额情况表
 * @createTime 2021年10月12日 11:17:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AcctPV01ServiceImpl implements AcctPV01Service {

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        List<Object> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        return map;
    }
}
