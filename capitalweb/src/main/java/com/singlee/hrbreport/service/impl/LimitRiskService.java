package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.LimitRisk;
import com.singlee.hrbreport.service.ReportService;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/10/30 17:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface LimitRiskService extends ReportService {

    Page<LimitRisk> getLimitPage(Map<String, Object> map);

    int deleteByPrimaryKey(String secmType);

    int insert(LimitRisk record);

    int insertSelective(LimitRisk record);

    LimitRisk selectByPrimaryKey(String secmType);

    int updateByPrimaryKeySelective(LimitRisk record);

    int updateByPrimaryKey(LimitRisk record);

}
