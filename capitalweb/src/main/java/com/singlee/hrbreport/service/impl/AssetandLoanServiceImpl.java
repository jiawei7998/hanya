package com.singlee.hrbreport.service.impl;

import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.AssetandLoanMapper;
import com.singlee.hrbreport.service.AssetandLoanService;
import com.singlee.hrbreport.service.LoanInSameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AssetandLoanServiceImpl implements AssetandLoanService {
    
    public final static Map<String, Object> prodMap = new HashMap();
    static {
        prodMap.put("LD" , "MM");
        prodMap.put("DP" , "MM");
        prodMap.put("BR" , "MM");
        prodMap.put("RB','RP" , "REPO");
        prodMap.put("VB','VP" , "REPO");
        prodMap.put("T" , "SEC");
        prodMap.put("A" , "SEC");
        prodMap.put("H" , "SEC");
        prodMap.put("CD" , "SEC");
        prodMap.put("DOMNBFINSE','DOMPOLICSE','DOMCENBKSE ','DOMFINSE ','DOMGOVSE','DOMLOGOVSE" , "SECSE");
        prodMap.put("DOMCD" , "SECSE");
        prodMap.put("FUND", "FUND");
    }
    
    public final static Map<String, Object> actyMap = new HashMap();
    static {
        actyMap.put("FC" , "YHYFCKLJG" ); //境内银行业非存款类金融机构
        actyMap.put("BX" , "BXY" ); //境内保险业金融机构
        actyMap.put("JR" , "JRKGGS" ); //境内金融控股公司
        actyMap.put("JS" , "JYJJSJG" ); //境内交易及结算类金融机构
        actyMap.put("ZQ" , "ZQY" ); //境内证券业金融机构
        actyMap.put("QT" , "QTJRJG" ); //境内其他金融机构
        actyMap.put("TS" , "TSMDZT" ); //境内特殊目的载体
        actyMap.put("FQT" , "FQTJRJG" ); //境外其他金融机构
        actyMap.put("CK" , "FYHYCKLJG" ); //境外银行业存款类金融机构
    }
    public final static List<String> dateInd = new ArrayList();
    static {
        dateInd.add("N");
        dateInd.add("L");
    }

    
    @Autowired
    AssetandLoanMapper assetandLoanMapper;
    @Autowired
    LoanInSameService loanService;
    
    @Override
    public Map<String, Object> queryAssetandLoan(Map<String, Object> param) {
        Map<String, Object> mapend = new HashMap<String, Object>();
        
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        mapend.put("queryDate", queryDate);
        //获取月初 月末日期
        String startDate = loanService.getLastDate(DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay))) ;
        String endDate = loanService.getLastDate(loanService.getNextDate(DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay))));
        
        
        for(Map.Entry<String, Object> e1:prodMap.entrySet()) {
            StringBuffer se1 = new StringBuffer();
            String product = e1.getValue().toString();
            String prodType = e1.getKey().toString();
            param.put("PRODUCT", product);
            param.put("PRODTYPE", prodType);
            param.put("issuer", SystemProperties.hrbReportCno);
            //回购,债券发行,基金的prodtype过长,重新赋值
            if("REPO".equals(product)) {
                if("RB','RP".equals(prodType)) {
                    //正回购(卖出回购)
                    prodType = "";
                }else {
                    //逆回购(买入返售)
                    prodType = "V";
                }
            }
            if("SECSE".equals(product)) {
                if("DOMCD".equals(prodType)) {
                    prodType="CD";
                }else {
                    prodType="";
                }
            }
            if("FUND".equals(product)) {
                prodType="";
            }
            se1.append(product);
            se1.append(prodType);
            for(Map.Entry<String, Object> e2:actyMap.entrySet()) {
                StringBuffer se2 = new StringBuffer();
                String acty = e2.getKey().toString();
                if("SECSE".equals(product) || "FUND".equals(product)) {
                    //债券发行,基金不区分交易对手类别
                    acty = "";
                }
                param.put("acty", e2.getValue());
                se2.append(se1);
                se2.append(acty);
                for(String ind:dateInd) {
                    StringBuffer se3 = new StringBuffer();
                    se3.append(se2);
                    if("N".equals(ind)) {
                        //当期余额
                        param.put("queryDate", endDate);
                        se3.append(ind);
                        if(mapend.get(se3.toString())!=null) {
                            continue;
                        }
                        Map<String, Object> queryAssetandLoan = assetandLoanMapper.queryAssetandLoan(param);
                        mapend.put(se3.toString(), queryAssetandLoan.get("AMT"));
                    }
                    if("L".equals(ind)) {
                        //上期余额
                        param.put("queryDate", startDate);
                        se3.append(ind);
                        if(mapend.get(se3.toString())!=null) {
                            continue;
                        }
                        Map<String, Object> queryAssetandLoan = assetandLoanMapper.queryAssetandLoan(param);
                        mapend.put(se3.toString(), queryAssetandLoan.get("AMT"));
                    }
                }
            }
        }
        return mapend;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> AssetandLoan = queryAssetandLoan(paramer);
        for(Map.Entry<String, Object> e:AssetandLoan.entrySet()) {
            System.out.println(e.getKey() + ":" + e.getValue());
        }
        return AssetandLoan;
    }
    
}
