package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G02NewReportMapper;
import com.singlee.hrbreport.service.G02NewReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * @author Administrator
 *银行业金融机构衍生产品交易业务情况表
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G02NewReportServiceImpl  implements G02NewReportService {

	@Autowired
	private G02NewReportMapper  g02;



	@Override
	public HashMap<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
	
		HashMap<String, Object> mapEnd= new HashMap<String,Object>();
		String RPTDate =HrbReportUtils.getDate(postdate);
		int lastyear =Integer.parseInt(RPTDate.toString().substring(0,4))-1;
		String RPTEDate =String.valueOf(lastyear)+"1231";
		String day= RPTDate.substring(6,8);
		if("0".equals((RPTDate.substring(6,8)).substring(0,1))) {
			day=RPTDate.substring(7,8);
	
		}
		String br =HrbReportUtils.getBr();

		
		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", RPTDate);//报表日期
		
		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("day", day);
		paraMap.put("RPTEDate", RPTEDate);
		paraMap.put("br", br);
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", paramer.get("OPICS"));
	   
		
	    
		try{
			
			
			//表一：业务存量
			List<Map<String,Object>>	list1= g02.getSql1(paraMap);
		Object BAL114A=0.00;
		Object BAL112A=0.00;
		Object BAL114C=0.00;
		Object BAL112C=0.00;
		if(list1.size()>0&&list1!=null) {
			for(Map<String,Object> map :list1) {
			if (map.get("CCYTYPE")!=null&&map.get("COST")!=null) {
				if("CCY".equals(map.get("CCYTYPE"))) {
					if("8200001045".equals(map.get("COST").toString())&&map.get("BALL114A")!=null) {
						BAL114C=map.get("BALL114A");
					}
					if("8200001045".equals(map.get("COST").toString())&&map.get("BALL124A")!=null) {
						BAL112C=map.get("BALL124A");
					}
					if("8200001045".equals(map.get("COST").toString())&&map.get("BALL114A")!=null) {
						BAL114A=map.get("BALL114A");
					}
					if("8200001045".equals(map.get("COST").toString())&&map.get("BALL114A")!=null) {
						BAL112A=map.get("BALL124A");
					}
					
						
						
				}
			}
				
			}
			
		}
		mapEnd.put("BAL114C", HrbReportUtils.tranBigde(BAL114C.toString()));
		mapEnd.put("BAL112C", HrbReportUtils.tranBigde(BAL112C.toString()));
		mapEnd.put("BAL114A", HrbReportUtils.tranBigdeAddTmp(BAL114A.toString(), BAL114C.toString()));
		mapEnd.put("BAL124A", HrbReportUtils.tranBigdeAddTmp(BAL112A.toString(), BAL112C.toString()));
//		
		//表二
	
		List<Map<String,Object>>	list2 =g02.getSql2(paraMap);
		
		Object bal115A=0.00;
		Object bal125A=0.00;
		Object bal115C=0.00;
		Object bal125C=0.00;
		if(list2.size()>0&&list2!=null) {
		for(Map<String, Object> map : list2) {	
			if (map.get("PORT")!=null&&map.get("CCYTYPE")!=null) {
				if("CCY".equals(map.get("CCYTYPE"))) {
					if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null) {
						bal115C=map.get("BAL");
					}
					if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null) {
						bal125C=map.get("BAL");
					}
					if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null) {
						bal115A=map.get("BAL");
					}
					if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null) {
						bal125A=map.get("BAL");
					}
					
						
						
				}
			}
			
		}
		}
		mapEnd.put("BAL1115C", HrbReportUtils.tranBigde(bal115C.toString()));
		mapEnd.put("BAL1125C", HrbReportUtils.tranBigde(bal125C.toString()));
		mapEnd.put("BAL1115A", HrbReportUtils.tranBigdeAddTmp(bal115C.toString(), bal115A.toString()));
		mapEnd.put("BAL1125A", HrbReportUtils.tranBigdeAddTmp(bal125C.toString(), bal125A.toString()));
		
//		//表三：业务发生额
		
		List<Map<String,Object>>
		list3 =g02.getSql3(paraMap);
		Object BAL314A=0.00;			
		Object BAL312A=0.00;
		Object BAL314C=0.00;			
		Object BAL312C=0.00;
		if(list3.size()>0&&list3!=null){
			for (Map<String, Object> map : list3) {
				if(map.get("COST")!=null&&map.get("CCYTYPE")!=null){
					if("CCY".equals(map.get("CCYTYPE"))){
						if("8200001045".equals(map.get("COST").toString())&&map.get("BAL314A")!=null){
							 BAL314C=map.get("BAL314A");
						}
						if("8300001045".equals(map.get("COST").toString())&&map.get("BAL3124A")!=null){
							BAL312C=map.get("BAL3124A");
						}
					}else{
						if("8200001045".equals(map.get("COST").toString())&&map.get("BAL314A")!=null){
							 BAL314A=map.get("BAL314A");
						}
						if("8300001045".equals(map.get("COST").toString())&&map.get("BAL3124A")!=null){
							BAL312A=map.get("BAL3124A");
						}
					}
					
				}
			}
		}
		mapEnd.put("BAL314C", HrbReportUtils.tranBigde(BAL314C.toString()));
		mapEnd.put("BAL324C", HrbReportUtils.tranBigde(BAL312C.toString()));
		mapEnd.put("BAL314A", HrbReportUtils.tranBigdeAddTmp(BAL314A.toString(), BAL314C.toString()));
		mapEnd.put("BAL324A", HrbReportUtils.tranBigdeAddTmp(BAL312A.toString(), BAL312C.toString()));
		//表四
		
		List<Map<String,Object>>	list4 =g02.getSql4(paraMap);
		Object bal315A=0.00;			
		Object bal325A=0.00;
		Object bal315C=0.00;			
		Object bal325C=0.00;
		if(list4.size()>0&&list4!=null){
			for (Map<String, Object> map : list4) {
				if(map.get("PORT")!=null&&map.get("CCYTYPE")!=null){	
					if("CCY".equals(map.get("CCYTYPE"))){
						if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null){
							bal315C=map.get("BAL");
						}
						if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null){
							bal325C=map.get("BAL");
						}
					}else{
						if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null){
							bal315A=map.get("BAL");
						}
						if(("FXZS".equals(map.get("PORT").toString())|| "FXZY".equals(map.get("PORT").toString()))&&map.get("BAL")!=null){
							bal325A=map.get("BAL");
						}
					}
					
				}
			}
		}
		mapEnd.put("BAL315C", HrbReportUtils.tranBigde(bal315C.toString()));
		mapEnd.put("BAL325C", HrbReportUtils.tranBigde(bal325C.toString()));
		mapEnd.put("BAL315A", HrbReportUtils.tranBigdeAddTmp(bal315A.toString(), bal315C.toString()));
		mapEnd.put("BAL325A", HrbReportUtils.tranBigdeAddTmp(bal325A.toString(), bal325C.toString()));
		
		Object balBK=0.00;
		Object balNBK=0.00;
		Object balFBK=0.00;
		Object balEBK=0.00;
		//表五
		
		List<Map<String,Object>>list5 =g02.getSql5(paraMap);
		if(list5.size()>0&&list5!=null){
			for (Map<String, Object> map : list5){
				if(map.get("BAL")!=null&&map.get("BKTYPE")!=null){
					if("BK".equals(map.get("BKTYPE"))){
						balBK=map.get("BAL");
					}
					if("NBK".equals(map.get("BKTYPE"))){
						balNBK=map.get("BAL");
					}
					if("FBK".equals(map.get("BKTYPE"))){
						balFBK=map.get("BAL");
					}
					if("EBK".equals(map.get("BKTYPE"))){
						balEBK=map.get("BAL");
					}
				}
			}
		}
		
		//表六
	
		List<Map<String,Object>>list6 =g02.getSql6(paraMap);
		if(list6.size()>0&&list6!=null){
			for (Map<String, Object> map : list6){
				if(map.get("BAL")!=null&&map.get("BKTYPE")!=null){
					if("BK".equals(map.get("BKTYPE"))){
						balBK=new BigDecimal(balBK.toString()).add(new BigDecimal( map.get("BAL").toString())) ;
					}
					if("NBK".equals(map.get("BKTYPE"))){
						balNBK=new BigDecimal(balNBK.toString()).add(new BigDecimal( map.get("BAL").toString())) ;
					}
					if("FBK".equals(map.get("BKTYPE"))){
						balFBK=new BigDecimal(balFBK.toString()).add(new BigDecimal( map.get("BAL").toString())) ;
					}
					if("EBK".equals(map.get("BKTYPE"))){
						balEBK=new BigDecimal(balEBK.toString()).add(new BigDecimal( map.get("BAL").toString())) ;
					}
				}
			}
		}
		
		mapEnd.put("BALBK", HrbReportUtils.tranBigde(balBK.toString()));
		mapEnd.put("BALNBK", HrbReportUtils.tranBigde(balNBK.toString()));
		mapEnd.put("BALFBK", HrbReportUtils.tranBigde(balBK.toString()));
		mapEnd.put("BALEBK", HrbReportUtils.tranBigde(balNBK.toString()));
		
		
	
	} catch (Exception e) {
		e.printStackTrace();
	}	
	return mapEnd; 
		
	
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		return null;
	}


}
