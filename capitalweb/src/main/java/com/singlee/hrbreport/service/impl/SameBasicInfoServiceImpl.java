package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.SameBasicInfoMapper;
import com.singlee.hrbreport.model.SameBasicInfoVO;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.service.SameBasicInfoService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SameBasicInfoServiceImpl implements SameBasicInfoService{
    
    @Autowired
    SameBasicInfoMapper sameBasicInfoMapper;
    @Autowired
    LoanInSameService loanService;
    
    @Override
    public Page<SameBasicInfoVO> queryBasicInfoPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        List<SameBasicInfoVO> queryBasicInfoList = queryBasicInfoList(param);
        return HrbReportUtils.producePage(queryBasicInfoList, pageNum, pageSize);
    }

    @Override
    public List<SameBasicInfoVO> queryBasicInfoList(Map<String, Object> param) {
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = loanService.getLastDate(sf.format(CalendarUtil.getFirstDayOfMonth(queryDay))) ;
        String endDate = loanService.getLastDate(loanService.getNextDate(sf.format(CalendarUtil.getLastDayOfMonth(queryDay))));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<SameBasicInfoVO> queryBasicInfo = sameBasicInfoMapper.queryBasicInfo(param);
        return queryBasicInfo;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        paramer.put("OPICS", SystemProperties.opicsUserName);
        paramer.put("HRBCB", SystemProperties.hrbcbUserName);
        List<SameBasicInfoVO> queryBasicInfoList = queryBasicInfoList(paramer);
        mapend.put("list", queryBasicInfoList);
        return mapend;
    }
}
