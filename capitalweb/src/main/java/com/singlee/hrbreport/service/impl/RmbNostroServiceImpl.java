package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.hrbreport.mapper.RmbNostroMapper;
import com.singlee.hrbreport.model.RmbNostroVO;
import com.singlee.hrbreport.service.RmbNostroService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 人民币同业余额
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RmbNostroServiceImpl implements RmbNostroService{
    
    @Autowired
    RmbNostroMapper rmbNostro;
    
    @Override
    public Page<RmbNostroVO> getReportPage(Map<String, Object> param) {
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        if(param.get("queryDate") == null || "".equals(param.get("queryDate"))) {
            String queryStr = DateUtil.getCurrentDateAsString();
            param.put("queryDate", queryStr);
        }
        List<RmbNostroVO> reportList = getReportList(param);
        return HrbReportUtils.producePage(reportList, pageNum, pageSize);
    }
    
    @Override
    public List<RmbNostroVO> getReportList(Map<String, Object> param) {
        List<RmbNostroVO> dataList = new ArrayList<RmbNostroVO>();
        
        List<RmbNostroVO> allFixedList = rmbNostro.queryAllFixed(param);
        List<RmbNostroVO> acdwList = rmbNostro.queryAcdw(param);
        List<RmbNostroVO> dldtList = rmbNostro.queryDldt(param);
        
        String typeStr = getStaticFiled("TYYEXX","明细操作类型",param.get("OPICS").toString());
        
        for(RmbNostroVO rmbNostro:acdwList) {
            if(rmbNostro == null || rmbNostro.getAcctNo() == null) {
                continue;
            }
            rmbNostro.setType(typeStr);
            dataList.add(rmbNostro);
        }
        
        boolean ind = true;
        for(RmbNostroVO rmbNostro1 : allFixedList) {
            for(RmbNostroVO rmbNostro2 : dataList) {
                if( rmbNostro1 != null && rmbNostro2 != null && rmbNostro1.getAcctNo() != null 
                        && rmbNostro1.getAcctNo().equals(rmbNostro2.getAcctNo())) {
                    ind = false;
                    break;
                }
            }
            if(ind) {
                rmbNostro1.setType(typeStr);
                dataList.add(rmbNostro1);
            }
        }
        
        for(RmbNostroVO rmbNostro : dldtList) {
            rmbNostro.setType(typeStr);
            dataList.add(rmbNostro);
        }
        
        return dataList;
    }
    
    
    /**
     * _获取数据库配置数据
     * @param tableId
     * @param tableValue
     * @return
     */
    private String getStaticFiled(String tableId,String tableValue,String opics) {
        HashMap<String,Object> param = new HashMap<String,Object>();
        param.put("tableId", tableId);
        param.put("tableValue", tableValue);
        param.put("OPICS", opics);
        String text = rmbNostro.getStaticFiled(param);
        return text;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> mapend = new HashMap<String, Object>();
        List<RmbNostroVO> reportList = getReportList(paramer);
        mapend.put("list", reportList);
        return mapend;
    }
}
