package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.reports.util.ReportUtils;
import com.singlee.hrbreport.mapper.ManageMonthMapper;
import com.singlee.hrbreport.service.ManageMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName ManageMonthServiceImpl.java
 * @Description 累计经营月报
 * @createTime 2021年10月11日 14:08:00
 */
@Service
public class ManageMonthServiceImpl implements ManageMonthService {
    @Autowired
    ManageMonthMapper manageMonthMapper;

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        HashMap<String,Object> mapEnd=new HashMap<String,Object>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tranDate = ParameterUtil.getString(paramer,"queryDate",sdf.format(new Date()));
        int lastyear=Integer.parseInt(tranDate.toString().substring(0, 4))-1;
        String lastyearday=String.valueOf(lastyear)+"1231";
        paramer.put("TranDate",tranDate);
        paramer.put("lastyearday",lastyearday);

        //TPOS.TDYMTM-TPOS.PYEMTM
        Object TPRINAMT=0.00; Object TAVGPRINAMT=0.00; Object TPRICEDIFF=0.00;  Object TRATEINCOME=0.00;   Object TTANXIAO=0.00;  Object TTDYMTM=0.00;
        Object APRINAMT=0.00; Object AAVGPRINAMT=0.00; Object APRICEDIFF=0.00;  Object ARATEINCOME=0.00; Object ATANXIAO=0.00; Object ATDYMTM=0.00;
        Object HPRINAMT=0.00; Object HAVGPRINAMT=0.00; Object HPRICEDIFF=0.00;  Object HRATEINCOME=0.00; Object HTANXIAO=0.00; Object  HTDYMTM=0.00;

        List<Map<String, Object>> list = manageMonthMapper.TopsSql(paramer);
        if(list.size() > 0){
            for (Map<String, Object> map : list) {
                if(map.get("INVTYPE")!=null){
                    mapEnd.put(map.get("INVTYPE")+"INVTYPE", map.get("INVTYPE"));
                    if(map.get("INVTYPE").equals("T")){
                        mapEnd.put(map.get("INVTYPE")+"INVTYPE", "交易性");
                        if(map.get("PRINAMT")!=null){
                            TPRINAMT=map.get("PRINAMT");//余额
                        }
                        if(map.get("PRICEDIFF")!=null){
                            TPRICEDIFF=map.get("PRICEDIFF");//价差
                        }
                        if(map.get("RATEINCOME")!=null){
                            //利息收入
                            TRATEINCOME=new BigDecimal(TRATEINCOME.toString()).add(new BigDecimal(map.get("RATEINCOME").toString()));
                        }
                        if(map.get("TANXIAO")!=null){
                            TRATEINCOME=new BigDecimal(TRATEINCOME.toString()).add(new BigDecimal(map.get("TANXIAO").toString()));
                        }

                        if(map.get("TDYMTM")!=null){
                            TTDYMTM=map.get("TDYMTM");//公允价值变动
                        }
                    }
                    if(map.get("INVTYPE").equals("A")){
                        mapEnd.put(map.get("INVTYPE")+"INVTYPE", "可供出售");
                        if(map.get("PRINAMT")!=null){
                            APRINAMT=map.get("PRINAMT");//余额
                        }
                        if(map.get("PRICEDIFF")!=null){
                            APRICEDIFF=map.get("PRICEDIFF");//价差
                        }
                        if(map.get("RATEINCOME")!=null){
                            ARATEINCOME=new BigDecimal(ARATEINCOME.toString()).add(new BigDecimal(map.get("RATEINCOME").toString())) ;//利息收入
                        }
                        if(map.get("TANXIAO")!=null){
                            ARATEINCOME=new BigDecimal(ARATEINCOME.toString()).add(new BigDecimal(map.get("TANXIAO").toString())) ;//利息收入
                        }
                        if(map.get("TDYMTM")!=null){
                            //ATDYMTM=map.get("TDYMTM");//公允价值变动
                            ATDYMTM=0;
                        }
                    }
                    if(map.get("INVTYPE").equals("H")){
                        mapEnd.put(map.get("INVTYPE")+"INVTYPE", "持有至到期");
                        if(map.get("PRINAMT")!=null){
                            HPRINAMT=map.get("PRINAMT");//余额
                        }
                        if(map.get("PRICEDIFF")!=null){
                            HPRICEDIFF=map.get("PRICEDIFF");//价差
                        }
                        if(map.get("RATEINCOME")!=null){
                            HRATEINCOME=new BigDecimal(HRATEINCOME.toString() ).add(new BigDecimal( map.get("RATEINCOME").toString()) );//利息收入
                        }
                        if(map.get("TANXIAO")!=null){
                            //HTANXIAO=map.get("TANXIAO");//累计摊销
                            HRATEINCOME=new BigDecimal(HRATEINCOME.toString() ).add(new BigDecimal( map.get("TANXIAO").toString()) );//利息收入
                        }
                        if(map.get("TDYMTM")!=null){
                            //HTDYMTM=map.get("TDYMTM");//公允价值变动
                            HTDYMTM=0;
                        }

                    }
                }
            }
        }

        //获取最大日期
        String lastyeardayn=lastyearday;
        paramer.put("lastyeardayn",lastyeardayn);
        List<Map<String, Object>> lastdayL = manageMonthMapper.getTopMax(paramer);

        if(lastdayL.size()>0&&lastdayL.get(0).get("CREATEDATE")!=null){
            lastyeardayn=lastdayL.get(0).get("CREATEDATE").toString();
        }
        paramer.put("lastyeardayn",lastyeardayn);
        //日均额
        List<Map<String, Object>> list2 = manageMonthMapper.avgSql(paramer);
        if (list2.size() > 0) {
            for (Map<String, Object> map2 : list2) {
                if(map2.get("INVTYPE")!=null){
                    if(map2.get("AVGPRINAMT")!=null){
                        if(map2.get("INVTYPE").toString().equals("T")){
                            TAVGPRINAMT= map2.get("AVGPRINAMT");
                        }
                        if(map2.get("INVTYPE").toString().equals("A")){
                            AAVGPRINAMT=map2.get("AVGPRINAMT");
                        }
                        if(map2.get("INVTYPE").toString().equals("H")){
                            HAVGPRINAMT=map2.get("AVGPRINAMT");
                        }
                    }
                }
            }
        }

        //价差	利息收入	累计摊销	公允价值变动	小计
        Object TSUM=0.00; Object ASUM=0.00;Object HSUM=0.00;
        TSUM=new BigDecimal(TPRICEDIFF.toString()).add(new BigDecimal(TRATEINCOME.toString())).add(new BigDecimal(TTDYMTM.toString()));
        ASUM=new BigDecimal(APRICEDIFF.toString()).add(new BigDecimal(ARATEINCOME.toString())).add(new BigDecimal(ATANXIAO.toString())).add(new BigDecimal(ATDYMTM.toString()));
        HSUM=new BigDecimal(HPRICEDIFF.toString()).add(new BigDecimal(HRATEINCOME.toString())).add(new BigDecimal(HTANXIAO.toString()));
        //收益率/付息率
        Object TRATE=0.00; Object ARATE=0.00;Object HRATE=0.00;
        //年初至报告期天数
        int DAYBT = DateUtil.daysBetween(tranDate,lastyearday);

        if(TAVGPRINAMT.equals("0.00")||TAVGPRINAMT.equals("0")||TAVGPRINAMT.equals("0.0")||TAVGPRINAMT.equals(0.0)){
            TRATE=0.00;
        }else{
            try {
                TRATE=new BigDecimal(TSUM.toString()).
                        divide((new BigDecimal(DAYBT) ),8,BigDecimal.ROUND_HALF_UP)
                        .multiply(new BigDecimal("365")).
                        divide(new BigDecimal(TAVGPRINAMT.toString()),8,BigDecimal.ROUND_HALF_UP)
                        .multiply(new BigDecimal(100));
            } catch (Exception e) {
            }
        }

        if(AAVGPRINAMT.equals("0.00")||AAVGPRINAMT.equals("0")||AAVGPRINAMT.equals("0.0")||AAVGPRINAMT.equals(0.0)){
            ARATE=0.00;
        }else{
            ARATE=new BigDecimal(ASUM.toString()).divide((new BigDecimal(DAYBT) ),8,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("365")).divide(new BigDecimal(AAVGPRINAMT.toString()),8,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
        }
        if(HAVGPRINAMT.equals("0.00")||HAVGPRINAMT.equals("0")||HAVGPRINAMT.equals("0.0")||HAVGPRINAMT.equals(0.0)){
            HRATE=0.00;
        }else{
            HRATE=new BigDecimal(HSUM.toString()).divide((new BigDecimal(DAYBT) ),8,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("365")).divide(new BigDecimal(HAVGPRINAMT.toString()),8,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
        }

        //中间业务收入	承分销手续费收入 --p付900301，s收 900300
			/*--900300收 --买券  realrecvfee有值
		     --900301  --卖券   realpayfee 有值*/
        // Object APLANFEES=0.00;
        Object PLANFEE=0.00; Object APLANFEE=0.00;

        //回购
        //RPRH.MDATE>出表日 RPRH.TYPE=RP,取值：RPRH.COMPROCDAMT	年初到现在的RPRH.COMPROCDAMT
        //逆回购	 正回购
        List<Map<String, Object>> list3 = manageMonthMapper.repoSql(paramer);
        Object NOTAMTVPB=0.00;
        Object NOTAMTRPB=0.00;
        Object ANOTAMTVPB=0.00;
        Object ANOTAMTRPB=0.00;
        Object YTDPINCEXPVPB=0.00;
        Object YTDPINCEXPRPB=0.00;
        Object SBALVPB=0.00;
        Object SBALRPB=0.00;
        //质押式正回购:RPRH.TYPE=RP,取值：RPRH.COMPROCDAMT
        //买断式正回购:RPRH.TYPE=RB,取值：RPRH.COMPROCDAMT
        Object COMPROCDAMTRP=0.00;
        Object COMPROCDAMTRB=0.00;
        Object ACOMPROCDAMTRP=0.00;
        Object ACOMPROCDAMTRB=0.00;
        Object CNTRP=0.00;
        Object CNTRB=0.00;
        //质押式逆回购		RPRH.MDATE>出表日 RPRH.TYPE=VP,取值：RPRH.COMPROCDAMT
        //买断式逆回购		RPRH.MDATE>出表日 RPRH.TYPE=VB,取值：RPRH.COMPROCDAMT
        Object COMPROCDAMTVP=0.00;
        Object COMPROCDAMTVB=0.00;
        Object ACOMPROCDAMTVP=0.00;
        Object ACOMPROCDAMTVB=0.00;
        Object CNTVP=0.00;
        Object CNTVB=0.00;
        if(list3.size()>0){
            for (Map<String, Object> map3 : list3) {
                if(map3.get("PRODTYPE")!=null){
                    if(map3.get("COMPROCDAMTL")!=null){
                        if(map3.get("PRODTYPE").toString().equals("VB")||map3.get("PRODTYPE").toString().equals("VP")){
                            NOTAMTVPB=new BigDecimal(NOTAMTVPB.toString()).add(new BigDecimal(map3.get("COMPROCDAMTL").toString()));
                        }
                        if(map3.get("PRODTYPE").toString().equals("RB")||map3.get("PRODTYPE").toString().equals("RP")){
                            NOTAMTRPB=new BigDecimal(NOTAMTRPB.toString()).add(new BigDecimal(map3.get("COMPROCDAMTL").toString()));
                        }
                    }
                    if(map3.get("ACOMPROCDAMTA")!=null){
                        if(map3.get("PRODTYPE").toString().equals("VB")||map3.get("PRODTYPE").toString().equals("VP")){
                            ANOTAMTVPB=new BigDecimal(ANOTAMTVPB.toString()).add(new BigDecimal(map3.get("ACOMPROCDAMTA").toString()));
                        }
                        if(map3.get("PRODTYPE").toString().equals("RB")||map3.get("PRODTYPE").toString().equals("RP")){
                            ANOTAMTRPB=new BigDecimal(ANOTAMTRPB.toString()).add(new BigDecimal(map3.get("ACOMPROCDAMTA").toString()));
                        }
                    }
                    if(map3.get("YTDPINCEXP")!=null){
                        if(map3.get("PRODTYPE").toString().equals("VB")||map3.get("PRODTYPE").toString().equals("VP")){
                            YTDPINCEXPVPB=new BigDecimal(YTDPINCEXPVPB.toString()).add(new BigDecimal(map3.get("YTDPINCEXP").toString()));
                        }
                        if(map3.get("PRODTYPE").toString().equals("RB")||map3.get("PRODTYPE").toString().equals("RP")){
                            YTDPINCEXPRPB=new BigDecimal(YTDPINCEXPRPB.toString()).add(new BigDecimal(map3.get("YTDPINCEXP").toString()));
                        }
                    }

                    if(map3.get("PRODTYPE").toString().equals("RP")){
                        if(map3.get("COMPROCDAMTL")!=null){
                            COMPROCDAMTRP=map3.get("COMPROCDAMTL");
                            ACOMPROCDAMTRP=map3.get("COMPROCDAMTA");
                            CNTRP=map3.get("CNT");
                        }
                    }
                    if(map3.get("PRODTYPE").toString().equals("RB")){
                        if(map3.get("COMPROCDAMTL")!=null){
                            COMPROCDAMTRB=map3.get("COMPROCDAMTL");
                            ACOMPROCDAMTRB=map3.get("COMPROCDAMTA");
                            CNTRB=map3.get("CNT");
                        }
                    }
                    if(map3.get("PRODTYPE").toString().equals("VP")){
                        if(map3.get("COMPROCDAMTL")!=null){
                            COMPROCDAMTVP=map3.get("COMPROCDAMTL");
                            ACOMPROCDAMTVP=map3.get("COMPROCDAMTA");
                            CNTVP=map3.get("CNT");
                        }
                    }
                    if(map3.get("PRODTYPE").toString().equals("VB")){
                        if(map3.get("COMPROCDAMTL")!=null){
                            COMPROCDAMTVB=map3.get("COMPROCDAMTL");
                            ACOMPROCDAMTVB=map3.get("COMPROCDAMTA");
                            CNTVB=map3.get("CNT");
                        }
                    }

                }

            }
        }

        if(ANOTAMTVPB==null||ANOTAMTVPB.toString().equals("0.0")){
            SBALVPB=0.00;
        }else {
            SBALVPB=new BigDecimal(YTDPINCEXPVPB.toString()).divide(new BigDecimal(DAYBT),2).multiply(new BigDecimal("365")).divide(new BigDecimal(ANOTAMTVPB.toString()),2).multiply(new BigDecimal(100));

        }
        if(ANOTAMTRPB==null||ANOTAMTRPB.toString().equals("0.0")){
            SBALRPB=0.00;
        }else {
            SBALRPB=new BigDecimal(YTDPINCEXPRPB.toString()).divide(new BigDecimal(DAYBT),2).multiply(new BigDecimal("365")).divide(new BigDecimal(ANOTAMTRPB.toString()),2).multiply(new BigDecimal(100));

        }

        //信用拆出	信用拆入
        Object CCYAMTCC=0.00;
        Object ACCYAMTCC=0.00;
        Object ACCYAMTCCA=0.00;
        Object YTDPINCEXPCC=0.00;
        Object SBALCC=0.00;
        Object CCYAMTCR=0.00;
        Object ACCYAMTCR=0.00;
        Object ACCYAMTCRA=0.00;
        Object YTDPINCEXPCR=0.00;
        Object SBALCR=0.00;
        Object CNTDAYCR=0.00;
        Object CNTDAYCC=0.00;

        List<Map<String, Object>> list4 = manageMonthMapper.dldtSql(paramer);

        if(list4.size()>0){
            for (Map<String, Object> map4 : list4) {
                if(map4.get("PRODTYPE")!=null){
                    if(map4.get("PRODTYPE").toString().equals("CC")){
                        CCYAMTCC=map4.get("CCYAMTL");
                        ACCYAMTCC=map4.get("CCYAMTA");
                        ACCYAMTCCA=map4.get("ACCYAMTA");
                        YTDPINCEXPCC=map4.get("YTDPINCEXP");
                        //SBALCC=map4.get("SBAL");
                        CNTDAYCC=map4.get("CNTDAY");
                    }
                    if(map4.get("PRODTYPE").toString().equals("CR")){
                        CCYAMTCR=map4.get("CCYAMTL");
                        ACCYAMTCR=map4.get("CCYAMTA");
                        ACCYAMTCRA=map4.get("ACCYAMTA");
                        YTDPINCEXPCR=map4.get("YTDPINCEXP");
                        //SBALCR=map4.get("SBAL");
                        CNTDAYCR=map4.get("CNTDAY");
                    }
                }
            }
        }
        if(ACCYAMTCCA==null||ACCYAMTCCA.toString().equals("0.0")||ACCYAMTCCA.toString().equals("0")){
            SBALCC="0.00";
        }else{
            SBALCC=new BigDecimal(YTDPINCEXPCC.toString()).divide(new BigDecimal(DAYBT),2).multiply(new BigDecimal("365")).multiply(new BigDecimal(100)).divide(new BigDecimal(ACCYAMTCCA.toString()),2);
        }

        if(ACCYAMTCRA==null||ACCYAMTCRA.toString().equals("0.0")||ACCYAMTCRA.toString().equals("0")){
            SBALCR="0.00";
        }else{
            SBALCR=new BigDecimal(YTDPINCEXPCR.toString()).divide(new BigDecimal(DAYBT),2).multiply(new BigDecimal("365")).multiply(new BigDecimal(100)).divide(new BigDecimal(ACCYAMTCRA.toString()),2);

        }

        //质押式正回购	买断式正回购	 信用拆入	  小计
        Object  RPBCRCOMPROCD=new BigDecimal(COMPROCDAMTRP.toString()).add(new BigDecimal(COMPROCDAMTRB.toString())).add(new BigDecimal(CCYAMTCR.toString()));
        Object  RPBCRACOMPROCD=new BigDecimal(ACOMPROCDAMTRP.toString()).add(new BigDecimal(ACOMPROCDAMTRB.toString())).add(new BigDecimal(ACCYAMTCR.toString()));
        Object  RPBCRCNT=new BigDecimal(CNTRP.toString()).add(new BigDecimal(CNTRB.toString())).add(new BigDecimal( CNTDAYCR.toString()));

        //质押式逆回购  	买断式逆回购 	 信用拆出	  小计
        Object  VPBCCCOMPROCD=new BigDecimal(COMPROCDAMTVP.toString()).add(new BigDecimal(COMPROCDAMTVB.toString())).add(new BigDecimal(CCYAMTCC.toString()));
        Object  VPBCCACOMPROCD=new BigDecimal(ACOMPROCDAMTVP.toString()).add(new BigDecimal(ACOMPROCDAMTVB.toString())).add(new BigDecimal(ACCYAMTCC.toString()));
        Object  VPBCCCNT=new BigDecimal(CNTVP.toString()).add(new BigDecimal(CNTVB.toString())).add(new BigDecimal( CNTDAYCC.toString()));


        Object PRINAMTA=0.00;
        Object FACEAMTB=0.00;
        Object FACEAMTA=0.00;
        Object CNTDAYB=0.00;
        Object CNTDAYA=0.00;
        List<Map<String, Object>> list6 = manageMonthMapper.spshSql(paramer);

        if(list6.size()>0){
            for (Map<String, Object> map6 : list6) {
                if(map6.get("TYPE")!=null){
                    if(map6.get("FACEAMT")!=null){
                        if(map6.get("TYPE").toString().equals("CASH")){
                            FACEAMTB=map6.get("FACEAMT");
                        }
                        if(map6.get("TYPE").toString().equals("AGENT")){
                            FACEAMTA=map6.get("FACEAMT");
                        }
                    }
                    if(map6.get("CNTDAY")!=null){
                        if(map6.get("TYPE").toString().equals("CASH")){
                            CNTDAYB=map6.get("CNTDAY");
                        }
                        if(map6.get("TYPE").toString().equals("AGENT")){
                            CNTDAYA=map6.get("CNTDAY");
                        }
                    }
                }
            }
        }

        FACEAMTB=new BigDecimal(FACEAMTB.toString()).add(new BigDecimal(FACEAMTA.toString()));
        CNTDAYB=new BigDecimal(CNTDAYB.toString()).add(new BigDecimal(CNTDAYA.toString()));
        Object STBAL=new BigDecimal(RPBCRCOMPROCD.toString()).add(new BigDecimal(VPBCCCOMPROCD.toString())) ;
        Object STABAL=new BigDecimal(RPBCRACOMPROCD.toString()).add(new BigDecimal(VPBCCACOMPROCD.toString())).add(new BigDecimal(FACEAMTB.toString()));
        Object STCNT=new BigDecimal(RPBCRCNT.toString()).add(new BigDecimal(VPBCCCNT.toString())).add(new BigDecimal(CNTDAYB.toString()));
        //合计

        mapEnd.put("TPRINAMT", ReportUtils.ObjtoStr(TPRINAMT));
        mapEnd.put("TAVGPRINAMT", ReportUtils.ObjtoStr(TAVGPRINAMT));
        mapEnd.put("TPRICEDIFF", ReportUtils.ObjtoStr(TPRICEDIFF));
        mapEnd.put("TRATEINCOME", ReportUtils.ObjtoStr(TRATEINCOME));
        mapEnd.put("TTDYMTM", ReportUtils.ObjtoStr(TTDYMTM));
        mapEnd.put("TSUM", ReportUtils.ObjtoStr(TSUM));
        mapEnd.put("TRATE",ReportUtils.ObjtoStr(TRATE));

        mapEnd.put("APRINAMT", ReportUtils.ObjtoStr(APRINAMT));
        mapEnd.put("AAVGPRINAMT",  ReportUtils.ObjtoStr( AAVGPRINAMT));
        mapEnd.put("APRICEDIFF", ReportUtils.ObjtoStr( APRICEDIFF));
        mapEnd.put("ARATEINCOME", ReportUtils.ObjtoStr( ARATEINCOME));
        mapEnd.put("ATANXIAO", ReportUtils.ObjtoStr(ATANXIAO));
        mapEnd.put("ATDYMTM", ReportUtils.ObjtoStr( ATDYMTM));
        mapEnd.put("ASUM", ReportUtils.ObjtoStr( ASUM));
        mapEnd.put("ARATE", ReportUtils.ObjtoStr( ARATE));


        mapEnd.put("HPRINAMT", ReportUtils.ObjtoStr(HPRINAMT));
        mapEnd.put("HAVGPRINAMT", ReportUtils.ObjtoStr(  HAVGPRINAMT));
        mapEnd.put("HPRICEDIFF", ReportUtils.ObjtoStr(HPRICEDIFF));
        mapEnd.put("HRATEINCOME", ReportUtils.ObjtoStr(HRATEINCOME));
        mapEnd.put("HTANXIAO", ReportUtils.ObjtoStr(HTANXIAO));
        mapEnd.put("HSUM", ReportUtils.ObjtoStr(HSUM));
        mapEnd.put("HRATE", ReportUtils.ObjtoStr(HRATE));

        mapEnd.put("PLANFEE",  ReportUtils.ObjtoStr(PLANFEE));
        mapEnd.put("APLANFEE",  ReportUtils.ObjtoStr(APLANFEE));

        mapEnd.put("PRINAMTN", ReportUtils.ObjtoStr(new BigDecimal(TPRINAMT.toString()).add(new BigDecimal(APRINAMT.toString())).add(new BigDecimal(HPRINAMT.toString()))));
        mapEnd.put("AVGPRINAMTN", ReportUtils.ObjtoStr(new BigDecimal(TAVGPRINAMT.toString()).add(new BigDecimal(AAVGPRINAMT.toString())).add(new BigDecimal(HAVGPRINAMT.toString()))));
        mapEnd.put("PRICEDIFFN", ReportUtils.ObjtoStr(new BigDecimal(TPRICEDIFF.toString()).add(new BigDecimal(APRICEDIFF.toString())).add(new BigDecimal(HPRICEDIFF.toString()))));
        mapEnd.put("RATEINCOMEN", ReportUtils.ObjtoStr(new BigDecimal(TRATEINCOME.toString()).add(new BigDecimal(ARATEINCOME.toString())).add(new BigDecimal(HRATEINCOME.toString()))));
        mapEnd.put("TANXIAON", ReportUtils.ObjtoStr((new BigDecimal(ATANXIAO.toString())).add(new BigDecimal(HTANXIAO.toString()))));
        mapEnd.put("TDYMTMN", ReportUtils.ObjtoStr(new BigDecimal(TTDYMTM.toString()).add(new BigDecimal(ATDYMTM.toString()))));
        mapEnd.put("SUMN", ReportUtils.ObjtoStr(new BigDecimal(TSUM.toString()).add(new BigDecimal(ASUM.toString())).add(new BigDecimal(HSUM.toString()))));
        Object Avg=new BigDecimal(TAVGPRINAMT.toString()).add(new BigDecimal(AAVGPRINAMT.toString())).add(new BigDecimal(HAVGPRINAMT.toString()));
        Object ASUMN=new BigDecimal(TSUM.toString()).add(new BigDecimal(ASUM.toString())).add(new BigDecimal(HSUM.toString()));


        if(Avg==null||Avg.toString().equals("0.0") ){
            mapEnd.put("RATEN", "0.00");
        }else{
            mapEnd.put("RATEN", ReportUtils.ObjtoStr(  new BigDecimal(ASUMN.toString()).divide((new BigDecimal(DAYBT)),4).multiply(new BigDecimal(365)).divide(new BigDecimal(Avg.toString()),4).multiply(new BigDecimal("100"))));
        }

        Object ALLS =0.00; Object ALLRateS =0.00;
        ALLS=new BigDecimal(TSUM.toString()).add(new BigDecimal(ASUM.toString())).add(new BigDecimal(HSUM.toString())).add(new BigDecimal(PLANFEE.toString()));
        if(Avg==null||Avg.toString().equals("0.0")){
            mapEnd.put("ALLRateS", "0.00");
        }else{
            mapEnd.put("ALLRateS", ReportUtils.ObjtoStr( ( new BigDecimal(ASUMN.toString()).add(new BigDecimal(PLANFEE.toString()))).divide(new BigDecimal(DAYBT) ,4).multiply(new BigDecimal(365)).divide(new BigDecimal(Avg.toString()) ,4).multiply(new BigDecimal("100")) ));
        }


        mapEnd.put("ALLS",ReportUtils.ObjtoStr(ALLS));

        mapEnd.put("PRINAMT", ReportUtils.ObjtoStr(new BigDecimal(TPRINAMT.toString()).add(new BigDecimal(APRINAMT.toString())).add(new BigDecimal(HPRINAMT.toString())).add(new BigDecimal(PLANFEE.toString()))));
        mapEnd.put("AVGPRINAMT", ReportUtils.ObjtoStr(new BigDecimal(TAVGPRINAMT.toString()).add(new BigDecimal(AAVGPRINAMT.toString())).add(new BigDecimal(HAVGPRINAMT.toString())).add(new BigDecimal(APLANFEE.toString()))));
        mapEnd.put("PRICEDIFF", ReportUtils.ObjtoStr(new BigDecimal(TPRICEDIFF.toString()).add(new BigDecimal(APRICEDIFF.toString())).add(new BigDecimal(HPRICEDIFF.toString()))));
        mapEnd.put("RATEINCOME", ReportUtils.ObjtoStr(new BigDecimal(TRATEINCOME.toString()).add(new BigDecimal(ARATEINCOME.toString())).add(new BigDecimal(HRATEINCOME.toString()))));
        mapEnd.put("TANXIAO", ReportUtils.ObjtoStr((new BigDecimal(ATANXIAO.toString())).add(new BigDecimal(HTANXIAO.toString()))));
        mapEnd.put("TDYMTM", ReportUtils.ObjtoStr(new BigDecimal(TTDYMTM.toString()).add(new BigDecimal(ATDYMTM.toString()))));
        mapEnd.put("SUM", ReportUtils.ObjtoStr(new BigDecimal(TSUM.toString()).add(new BigDecimal(ASUM.toString())).add(new BigDecimal(HSUM.toString()))));
        mapEnd.put("RATE", ReportUtils.ObjtoStr(new BigDecimal(TRATE.toString()).add(new BigDecimal(ARATE.toString())).add(new BigDecimal(HRATE.toString()))));

        mapEnd.put("NOTAMTVPB", ReportUtils.ObjtoStr(NOTAMTVPB));
        mapEnd.put("ANOTAMTVPB", ReportUtils.ObjtoStr(ANOTAMTVPB));
        mapEnd.put("YTDPINCEXPVPB", ReportUtils.ObjtoStr(YTDPINCEXPVPB));
        mapEnd.put("SBALVPB", ReportUtils.ObjtoStr(SBALVPB));



        mapEnd.put("NOTAMTRPB", ReportUtils.ObjtoStr(NOTAMTRPB));
        mapEnd.put("ANOTAMTRPB", ReportUtils.ObjtoStr(ANOTAMTRPB));
        mapEnd.put("YTDPINCEXPRPB", ReportUtils.ObjtoStr(YTDPINCEXPRPB));
        mapEnd.put("SBALRPB", ReportUtils.ObjtoStr(SBALRPB));

        mapEnd.put("CCYAMTCC", ReportUtils.ObjtoStr(CCYAMTCC));
        mapEnd.put("ACCYAMTCC", ReportUtils.ObjtoStr(ACCYAMTCC));
        mapEnd.put("YTDPINCEXPCC", ReportUtils.ObjtoStr(YTDPINCEXPCC));
        mapEnd.put("SBALCC", ReportUtils.ObjtoStr(SBALCC));

        mapEnd.put("CCYAMTCR", ReportUtils.ObjtoStr(CCYAMTCR));
        mapEnd.put("ACCYAMTCR", ReportUtils.ObjtoStr(ACCYAMTCR));
        mapEnd.put("YTDPINCEXPCR", ReportUtils.ObjtoStr(YTDPINCEXPCR));
        mapEnd.put("SBALCR", ReportUtils.ObjtoStr(SBALCR));


        mapEnd.put("COMPROCDAMTRP", ReportUtils.ObjtoStr(new BigDecimal(COMPROCDAMTRP.toString())));
        mapEnd.put("ACOMPROCDAMTRP", ReportUtils.ObjtoStr(new BigDecimal(ACOMPROCDAMTRP.toString())));
        mapEnd.put("CNTRP", ReportUtils.ObjtoStr(CNTRP));




        mapEnd.put("COMPROCDAMTRB", ReportUtils.ObjtoStr(new BigDecimal(COMPROCDAMTRB.toString())));
        mapEnd.put("ACOMPROCDAMTRB", ReportUtils.ObjtoStr(new BigDecimal(ACOMPROCDAMTRB.toString())));
        mapEnd.put("CNTRB", ReportUtils.ObjtoStr(CNTRB));


        mapEnd.put("CCYAMTCR", ReportUtils.ObjtoStr(new BigDecimal(CCYAMTCR.toString())));
        mapEnd.put("ACCYAMTCR", ReportUtils.ObjtoStr(new BigDecimal(ACCYAMTCR.toString())));
        mapEnd.put("CNTDAYCR", ReportUtils.ObjtoStr(CNTDAYCR));


        mapEnd.put("RPBCRCOMPROCD", ReportUtils.ObjtoStr(new BigDecimal(RPBCRCOMPROCD.toString())));
        mapEnd.put("RPBCRACOMPROCD", ReportUtils.ObjtoStr(new BigDecimal(RPBCRACOMPROCD.toString())));
        mapEnd.put("RPBCRCNT", ReportUtils.ObjtoStr(RPBCRCNT));

        mapEnd.put("COMPROCDAMTVP", ReportUtils.ObjtoStr(new BigDecimal(COMPROCDAMTVP.toString())));
        mapEnd.put("ACOMPROCDAMTVP", ReportUtils.ObjtoStr(new BigDecimal(ACOMPROCDAMTVP.toString())));
        mapEnd.put("CNTVP", ReportUtils.ObjtoStr(CNTVP));


        mapEnd.put("COMPROCDAMTVB", ReportUtils.ObjtoStr(new BigDecimal(COMPROCDAMTVB.toString())));
        mapEnd.put("ACOMPROCDAMTVB", ReportUtils.ObjtoStr(new BigDecimal(ACOMPROCDAMTVB.toString())));
        mapEnd.put("CNTVB", ReportUtils.ObjtoStr(CNTVB));

        mapEnd.put("CCYAMTCC",  ReportUtils.ObjtoStr(new BigDecimal(CCYAMTCC.toString())));
        mapEnd.put("ACCYAMTCC", ReportUtils.ObjtoStr(new BigDecimal(ACCYAMTCC.toString())));
        mapEnd.put("CNTDAYCC", ReportUtils.ObjtoStr(CNTDAYCC));

        mapEnd.put("VPBCCCOMPROCD", ReportUtils.ObjtoStr(new BigDecimal(VPBCCCOMPROCD.toString())));
        mapEnd.put("VPBCCACOMPROCD", ReportUtils.ObjtoStr(new BigDecimal(VPBCCACOMPROCD.toString())));
        mapEnd.put("VPBCCCNT", ReportUtils.ObjtoStr(VPBCCCNT));

        mapEnd.put("FACEAMTB", ReportUtils.ObjtoStr(new BigDecimal(FACEAMTB.toString())));
        mapEnd.put("CNTDAYB", ReportUtils.ObjtoStr(CNTDAYB));


        mapEnd.put("STBAL", ReportUtils.ObjtoStr(new BigDecimal(STBAL.toString())));
        mapEnd.put("STABAL", ReportUtils.ObjtoStr(new BigDecimal(STABAL.toString())));
        mapEnd.put("STCNT", ReportUtils.ObjtoStr(STCNT));

        mapEnd.put("PRINAMTA", ReportUtils.ObjtoStr(new BigDecimal(PRINAMTA.toString())));
        mapEnd.put("FACEAMTA", ReportUtils.ObjtoStr(new BigDecimal(FACEAMTA.toString())));
        mapEnd.put("CNTDAYA", ReportUtils.ObjtoStr(CNTDAYA));



        mapEnd.put("ACCYAMTCRA", ReportUtils.ObjtoStr(ACCYAMTCRA));
        mapEnd.put("ACCYAMTCCA", ReportUtils.ObjtoStr(ACCYAMTCCA));
        mapEnd.put("TRANDATE",  tranDate);
        return mapEnd;
    }
}
