package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.ExchangeRepoVO;

import java.util.List;
import java.util.Map;

/**
 * _交易所回购
 * @author zk
 */
public interface ExchangeRepoService extends ReportService{
    
    public Page<ExchangeRepoVO> getExchangeRepoPage(Map<String ,Object> param);
    
    public List<ExchangeRepoVO> getExchangeRepoList(Map<String ,Object> param);

}
