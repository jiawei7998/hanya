package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.TposPo;

/**
 * 合并持仓分析报表
 *
 */
public interface TposReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
	/**
	 * @param paramer
	 * @return
	 */
	public Page<TposPo>  getTposReportPage(Map<String, String> paramer) ;
}
