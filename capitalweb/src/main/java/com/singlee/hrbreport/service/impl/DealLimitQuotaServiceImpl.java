package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.service.DealLimitQuotaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName DealLimitQuotaServiceImpl.java
 * @Description 交易限额情况表
 * @createTime 2021年10月12日 11:09:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DealLimitQuotaServiceImpl implements DealLimitQuotaService {

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
