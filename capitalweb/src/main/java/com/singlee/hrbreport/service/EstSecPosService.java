package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.EstSecPosVO;

/**
 * _其他形式流入房地产
 * @author zk
 */
public interface EstSecPosService extends ReportService {
    public Page<EstSecPosVO> queryEstateSecBalancePage(Map<String , Object> param);
    
    public List<EstSecPosVO> queryEstateSecBalanceList(Map<String , Object> param);
}
