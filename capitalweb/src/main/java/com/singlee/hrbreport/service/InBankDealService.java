package com.singlee.hrbreport.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.InBankDealBrLoVO;
import com.singlee.hrbreport.model.InBankDealCDSeVO;
import com.singlee.hrbreport.model.InBankDealFundVO;
import com.singlee.hrbreport.model.InBankDealRepoVO;
import com.singlee.hrbreport.model.InBankDealSecVO;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public interface InBankDealService extends ReportService {
    
    public Page<InBankDealRepoVO> queryRepoPage(Map<String , Object> param);
    
    public List<InBankDealRepoVO> queryRepoList(Map<String , Object> param);
    
    public Page<InBankDealSecVO> querySecPosDetailPage(Map<String, Object> param);
    
    public List<InBankDealSecVO> querySecPosDetailList(Map<String, Object> param);
    
    public Page<InBankDealBrLoVO> queryBrLoPosDetailPage(Map<String, Object> param);
    
    public List<InBankDealBrLoVO> queryBrLoPosDetailList(Map<String, Object> param);
    
    public Page<InBankDealCDSeVO> queryCDSEDetailPage(Map<String, Object> param);
    
    public List<InBankDealCDSeVO> queryCDSEDetailList(Map<String, Object> param);
    
    public Page<InBankDealCDSeVO> queryRHCDSEDetailPage(Map<String, Object> param);
    
    public List<InBankDealCDSeVO> queryRHCDSEDetailList(Map<String, Object> param);
    
    public Page<InBankDealFundVO> queryFundDetailPage(Map<String, Object> param);
    
    public List<InBankDealFundVO> queryFundDetailList(Map<String, Object> param);
    
}
