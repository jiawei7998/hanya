package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.G14BookReportMapper;
import com.singlee.hrbreport.model.G14BookReportVO;
import com.singlee.hrbreport.service.G14BookReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * @author Administrator
 *G14台账明细-XX部门
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G14BookReportServiceImpl implements G14BookReportService {

	
	@Autowired
	private G14BookReportMapper gb14;



    @Override
    public List<G14BookReportVO> getReportMapList(Map<String, Object> paramer) {
        HashMap<String,Object> remap =new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName); 
        remap.put("HRBCB", SystemProperties.hrbcbUserName);

         return gb14.getG14BookReportsql1(remap);
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> mapEnd=new HashMap<>();
        List<G14BookReportVO> list =getReportMapList(paramer);
        G14BookReportVO list2 =new  G14BookReportVO() ;
//        list2.setabsamt("11");
//        list2.setAcctngtype("22");
//        list2.setBood("33");
//        list2.setCfn1("44");
//        list2.setSettamt("55");
//        list2.setSic("66");
//        list2.setSn("77");
        list.add(list2);
        mapEnd.put("list", list);
        return mapEnd;
    }


    @Override
    public Page<G14BookReportVO> getReportMap(Map<String, Object> paramer) {
        int pageNum = (int)paramer.get("pageNum");
        int pageSize = (int)paramer.get("pageSize");
        List<G14BookReportVO> list2 = getReportMapList(paramer);
      
        Page <G14BookReportVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }

    
}