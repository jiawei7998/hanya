package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.RmbReportMapper;
import com.singlee.hrbreport.model.RmbPo;
import com.singlee.hrbreport.service.RmbReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 本币交易情况表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RmbReportServiceImpl implements RmbReportService {

	@Autowired
	private RmbReportMapper rmbReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();

		String SRPTDate = DateUtil.format(new Date(), "yyyy年MM月dd日");
		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名

		String sdate = ParameterUtil.getString(paramer, "sdate", "");
		String edate = ParameterUtil.getString(paramer, "edate", "");
		String type = ParameterUtil.getString(paramer, "type", "");

		if ("".equals(sdate) || "".equals(edate)) {
			String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
			Date parse = DateUtil.parse(queryDate, "yyyy-MM-dd");
			sdate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(parse));
			edate = DateUtil.format(CalendarUtil.getLastDayOfMonth(parse));
		}

		mapEnd.put("sdate", sdate);// 开始日期
		mapEnd.put("edate", edate);// 结束日期

		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("sdate", sdate);
		mapPar.put("edate", edate);
		mapPar.put("type", type);
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		List<RmbPo> list = rmbReportMapper.getRmbReport(mapPar);
		format(list);
		mapEnd.put("list", list);

		return mapEnd;
	}

	@Override
    public Page<RmbPo> getRmbReportPage(Map<String, String> paramer) {
		int pageNumber = ParameterUtil.getInt(paramer, "pageNumber", 10);
		int pageSize = ParameterUtil.getInt(paramer, "pageSize", 1);
		paramer.put("OPICS", SystemProperties.opicsUserName);
		paramer.put("HRBCB", SystemProperties.hrbcbUserName);

		List<RmbPo> list = rmbReportMapper.getRmbReport(paramer);
		format(list);
		return HrbReportUtils.producePage(list, pageNumber, pageSize);
	}

	/**
	 * 
	 */
	public void format(List<RmbPo> list) {
		for (RmbPo rmbPo : list) {
			rmbPo.setType(StringUtil.isEmpty(rmbPo.getType()) ? "" : rmbPo.getType().trim());
			rmbPo.setAcctdesc(StringUtil.isEmpty(rmbPo.getAcctdesc()) ? "" : rmbPo.getAcctdesc().trim());
			rmbPo.setCostdesc(StringUtil.isEmpty(rmbPo.getCostdesc()) ? "" : rmbPo.getCostdesc().trim());
			rmbPo.setSecname(StringUtil.isEmpty(rmbPo.getSecname()) ? "" : rmbPo.getSecname().trim());
			rmbPo.setCno(StringUtil.isEmpty(rmbPo.getCno()) ? "" : rmbPo.getCno());

			String faceamt = StringUtil.isEmpty(rmbPo.getFaceamt()) ? "0" : rmbPo.getFaceamt();
			rmbPo.setFaceamt((new BigDecimal(faceamt).divide(new BigDecimal("100000000"), 6, BigDecimal.ROUND_HALF_UP))
					.toString());
			rmbPo.setAmt(getFormatNum((new BigDecimal(rmbPo.getAmt())).toString()));
			rmbPo.setMdate(StringUtil.isEmpty(rmbPo.getMdate()) ? null : rmbPo.getMdate().substring(0, 10));
			rmbPo.setDealdate(StringUtil.isEmpty(rmbPo.getMdate()) ? null : rmbPo.getDealdate().substring(0, 10));

			rmbPo.setClprice(getFormatNum(StringUtil.isEmpty(rmbPo.getClprice()) ? "0" : rmbPo.getClprice()));
			rmbPo.setPrice(getFormatNum(StringUtil.isEmpty(rmbPo.getPrice()) ? "0" : rmbPo.getPrice()));
			rmbPo.setRate(getFormatNum(StringUtil.isEmpty(rmbPo.getRate()) ? "0" : rmbPo.getRate()));
		}
	}

	/**
	 * 格式化
	 * 
	 * @param num
	 * @return
	 */
	public String getFormatNum(String num) {
		if (num != null && !"".equals(num)) {
			if ("0".equals(num)) {
				return "0.00";
			} else {
				double parseDouble = Double.parseDouble(num);
				DecimalFormat df = new DecimalFormat("#,###.00");
				String data = "";
				if (parseDouble < 1 && parseDouble > 0) {
					data = "0" + df.format(parseDouble);
				} else {
					data = df.format(parseDouble);
				}
				return data;
			}
		} else {
			return "0.00";
		}
	}

}
