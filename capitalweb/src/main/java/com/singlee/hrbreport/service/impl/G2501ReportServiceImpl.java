package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G2501ReportMapper;
import com.singlee.hrbreport.service.G2501ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G25 第I部分 流动性覆盖率
 * @author zk
 *
 */
@Service
public class G2501ReportServiceImpl implements G2501ReportService {

    @Autowired
    private G2501ReportMapper g2501;

    public Map<String, Object> getReport(Map<String, Object> paramer, Date postdate) {
        HashMap<String,Object> mapEnd = new HashMap<String,Object>();
		String br = ParameterUtil.getString(paramer,"BR",SlSessionHelper.getUser().getOpicsBr());

		String RPTDate = HrbReportUtils.getDate(postdate);
		String SRPTDate = HrbReportUtils.dateToMap(postdate);

		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", SRPTDate);//报表日期
		
		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("BR", br);
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", paramer.get("OPICS")); 
		paraMap.put("HRBCB", paramer.get("HRBCB")); 
		
		//风险权重为零的证券
		countNoRiskSecurity(paraMap,mapEnd);
		
		//二级资产
		countSecLevelAsset(paraMap,mapEnd);
		
		//到期证券投资
		countExpireSecurity(paraMap,mapEnd);
		
		//押品流入流出
		countGuarantee(paraMap,mapEnd);
		
		//与央行进行的担保融资
		countGuaranteeWithCB(paraMap,mapEnd);
		
		//由2B级资产担保的融资交易
		countGuaranteeBy2B(paraMap,mapEnd);
		
		//由其他资产担保的融资交易
		countGuaranteeByOthers(paraMap,mapEnd);
		
		//存款
		countDeposit(paraMap,mapEnd);
		
		return mapEnd;
	}

    /**
	 *  1.1.3.1主权国家发行的 		   mapEnd.RPT1
	 *  1.1.3.2主权国家担保的 		   mapEnd.RPT2
	 *  1.1.3.3央行发行或担保的		   mapEnd.RPT3
	 *  1.1.3风险权重为零的证券		   mapEnd.RPT113
		SQL :RPT1
	 * @param paramMap
	 * @param mapEnd
	 */
    private void countNoRiskSecurity(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> noRiskList = g2501.countNoRiskSecurity(paramMap);
        Object ORPT1=0.00;Object ORPT2=0.00;Object ORPT3=0.00;
        if(noRiskList.size()>0&&noRiskList!=null){
            for (Map<String, Object> map : noRiskList) {
                if("DOMGOVSE".equals(map.get("TYPE").toString())){
                    ORPT1=map.get("AMT");
                }
                if("DOMLOGOVSE".equals(map.get("TYPE").toString())){
                    ORPT2=map.get("AMT");
                }
                if("DOMCENBKSE".equals(map.get("TYPE").toString())){
                    ORPT3=map.get("AMT");
                }
            }
        }
        mapEnd.put("RPT1", HrbReportUtils.tranBigde(ORPT1.toString()));
        mapEnd.put("RPT2", HrbReportUtils.tranBigde(ORPT2.toString()));
        mapEnd.put("RPT3", HrbReportUtils.tranBigde(ORPT3.toString()));
        mapEnd.put("RPT113", (new BigDecimal(ORPT1.toString()).add(new BigDecimal(ORPT2.toString())).add(new BigDecimal(ORPT3.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
    }
    	
	/**
	 * 1.2.1 2A资产-公司债         		        mapEnd.RPT2
	 * 1.2.2 2A资产担保债券                 			mapEnd.RPT4
	 * 1.2.3.4  公共部门实体发行或担保的                  mapEnd.RPT5
	 * 1.2.4 2B资产-公司债券				mapEnd.RPT6
		SQL:RPT2
	 * @param paramMap
	 * @param mapEnd
	 */
    private void countSecLevelAsset(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
		List<Map<String, Object>> secLevelList = g2501.countSecLevelAsset(paramMap);
		Object ORPT2A=0.00; Object ORPT4=0.00; Object ORPT5=0.00;  Object ORPT6=0.00;  Object  OELSE=0.00; 
		if(secLevelList.size()>0&&secLevelList!=null){
			 for (Map<String, Object> map:secLevelList) {
				 if("1".equals(map.get("LEVE"))){
					 //评级AA-及以上"
					 if("1".equals(map.get("TYPE"))){
						 /*(评级AA-及以上的非金融公司债)
						 债券-非金融债*/
						 ORPT2A=map.get("MKTVAL").toString();							 
					 }else if("2".equals(map.get("TYPE"))|| "4".equals(map.get("TYPE"))){
						/* "债券--商业银行金融债(DOMFINSE) +次级债sd+非银行金融机构债券
						 评级AA-及以上"*/
						 ORPT4=new BigDecimal(map.get("MKTVAL").toString()).add(new BigDecimal(ORPT4.toString()));							 
					 }else if("3".equals(map.get("TYPE"))){
						 //SD LIKE '%公共部门实体（收入来源于中央财政）%'+%中央政府投资的公用事业企业%+
						 ORPT5=new BigDecimal(map.get("MKTVAL").toString()).add(new BigDecimal(ORPT5.toString()));
					 }else{
						 OELSE=new BigDecimal(map.get("MKTVAL").toString()).add(new BigDecimal(OELSE.toString()));
					 }
				 }else{
					 //评级为BBB-至A+
					 if("3".equals(map.get("TYPE"))){
						 //SD LIKE '%公共部门实体（收入来源于中央财政）%'+%中央政府投资的公用事业企业%+
						ORPT5=new BigDecimal(map.get("MKTVAL").toString()).add(new BigDecimal(ORPT5.toString()));
					}else{
						 ORPT6=new BigDecimal(map.get("MKTVAL").toString()).add(new BigDecimal(ORPT6.toString()));
					}
				}
			}
		}
		
		
		mapEnd.put("RPT2A",  (new BigDecimal(ORPT2A.toString())).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
		mapEnd.put("RPT4", new BigDecimal(ORPT4.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("RPT5", new BigDecimal(ORPT5.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
		mapEnd.put("RPT6",  ((new BigDecimal(ORPT6.toString())).add(new BigDecimal(OELSE.toString()))).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
	}
    
    
    /**
     * 2.2.2.7  到期证券投资      mapEnd.RPT7
        SQL:RPT7
     * @param paramMap
     * @param mapEnd
     */
    private void countExpireSecurity(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> expireList = g2501.countExpireSecurity(paramMap);
        Object ORPT7=0.00;
        if(expireList.size()>0&&expireList!=null){
            if(expireList.get(0) != null && expireList.get(0).get("RPT7")!=null){
                ORPT7=expireList.get(0).get("RPT7").toString();
            }
        }
       mapEnd.put("RPT7",  (new BigDecimal(ORPT7.toString())).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
    }
    
    /**
     *  2.1.3.1.1 其中，以合格优质流动性资产为押品的融资               CTLRN
        2.1.3.1.1.1 一级资产押品市值                                CTLR1
        2.1.3.1.1.2 2A资产押品市值                                 CTLR2A
        2.1.3.1.1.3 2B资产押品市值                                CTLR2B
        2.1.3.2由一级资产担保的融资交易（与央行以外其他交易对手）            NCTLR1N
            2.1.3.2.1 押品市场价值                                    NCTLR1M
        2.1.3.3由2A级资产担保的融资交易（与央行以外其他交易对手）           NCTLR2AN
            2.1.3.3.1押品市场价值                                     NCTLR2AM
                   短期融资券：最新主题评级
                    其他：最新债项评级
        
        2.2.1.1.1  一级资产为担保                  VB1N
            2.2.1.1.1.1 押品市场价值              VB1M
        2.2.1.1.2  以2A资产为担保                 VB2AN
            2.2.1.1.2.1 押品市场价值              VB2AM
        2.2.1.1.3 以2B资产为担保                  VB2BN
            2.2.1.1.3.1 押品市场价值              VB2BM
        2.2.1.3 押品未再用于再抵押                   VP2N
        SQL:RPT8
     * @param paramMap
     * @param mapEnd
     */
    private void countGuarantee(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        BigDecimal CTLRN=new BigDecimal(0.00);// 2.1.3.1.1 其中，以合格优质流动性资产为押品的融资
        Object CTLR1=0.00;//2.1.3.1.1.1 一级资产押品市值
        Object CTLR2A=0.00;//2.1.3.1.1.2 2A资产押品市值
        Object CTLR2B=0.00;//2.1.3.1.1.3 2B资产押品市值
        Object NCTLR1N=0.00;//2.1.3.2由一级资产担保的融资交易（与央行以外其他交易对手）
        Object NCTLR1M=0.00;//2.1.3.2.1 押品市场价值
        Object NCTLR2BM=0.00;//2.1.3.2.1 押品市场价值
        Object NCTLR2AN=0.00;//2.1.3.3由2A级资产担保的融资交易（与央行以外其他交易对手）
        Object NCTLR2AM=0.00;//2.1.3.3.1押品市场价值
        Object NCTLR2BN=0.00;//2.1.3.4由2B级资产担保的融资交易（与央行以外其他交易对手）
        Object VB1N=0.00;//2.2.1.1.1以一级资产为担保
        Object VB1M=0.00;// 2.2.1.1.1.1押品市场价值
        Object VB2AN=0.00;// 2.2.1.1.2以2A资产为担保
        Object VB2AM=0.00;//2.2.1.1.2.1押品市场价值
        Object VB2BN=0.00;// 2.2.1.1.3以2B资产为担保
        Object VB2BM=0.00;//2.2.1.1.3.1押品市场价值
        Object VP2N=0.00;// 2.2.1.2押品未用于再抵押（质押式）
        Object ELSEO=0.00;//2.1.3.1.1.1 ELSE
        //NPRODTYPE, BKTYPE , TYPE,
        List<Map<String, Object>> guaranteeList=g2501.countGuarantee(paramMap);
        if(guaranteeList.size()>0&&guaranteeList!=null){
             for (Map<String, Object> map : guaranteeList) {
                 if (map.get("NPRODTYPE")!=null){
                     if("RBP".equals(map.get("NPRODTYPE").toString())){
                         if(map.get("TYPE")!=null){
                             if(map.get("BKTYPE")!=null){
                                 if("CTRLBK".equals(map.get("BKTYPE"))){
                                    //2.1.3.1与央行进行的担保融资
                                    /* 2.1.3.1.1.1 一级资产押品市值
                                     2.1.3.1.1.2 2A资产押品市值
                                     2.1.3.1.1.3 2B资产押品市值*/
                                     if(map.get("VAL")!=null){
                                         if("1".equals(map.get("TYPE"))){
                                             CTLR1=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(CTLR1.toString())); 
                                         }
                                         if("2A".equals(map.get("TYPE"))){
                                             CTLR2A=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(CTLR1.toString())); 
                                         }
                                         if("2B".equals(map.get("TYPE"))){
                                             CTLR2B=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(CTLR1.toString())); 
                                         }   
                                     }
                                    
                                 }else{
                                    /* 64          2.1.3.2由一级资产担保的融资交易（与央行以外其他交易对手）
                                     65      2.1.3.2.1 押品市场价值
                                     66        2.1.3.3由2A级资产担保的融资交易（与央行以外其他交易对手）
                                     67      2.1.3.3.1押品市场价值
                                     */
                                     if(map.get("VAL")!=null){
                                         if("1".equals(map.get("TYPE"))){
                                             NCTLR1M=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(NCTLR1M.toString()));
                                         }      
                                         if("2A".equals(map.get("TYPE"))){
                                             NCTLR2AM=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(NCTLR2AM.toString()));
                                         } 
                                     }
                                     if(map.get("NOTAMT")!=null){
                                         if("1".equals(map.get("TYPE"))){
                                             NCTLR1N=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(NCTLR1N.toString()));
                                         }
                                         if("2A".equals(map.get("TYPE"))){
                                             NCTLR2AN=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(NCTLR2AN.toString()));
                                         }
                                     }
                                 }
                             }
                         }
                         
                     } 
                 }
                // 2.2.1.1押品未用于再抵押（买断式）
                 if("VB".equals(map.get("NPRODTYPE").toString())){
                     if(map.get("AB")!=null){
                         if(map.get("VAL")!=null){
                             if("1".equals(map.get("TYPE"))){
                                 VB1M=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(VB1M.toString())); 
                             }
                             if("2A".equals(map.get("TYPE"))){
                                VB2AM=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(VB2AM.toString()));  
                             }
                             if("2B".equals(map.get("TYPE"))){
                                VB2BM=new BigDecimal(map.get("VAL").toString()).add(new BigDecimal(VB2BM.toString())); 
                             }
                         }
                         if(map.get("NOTAMT")!=null){
                             if("1".equals(map.get("TYPE"))){
                                 VB1N=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(VB1N.toString())); 
                             }
                             if("2A".equals(map.get("TYPE"))){
                                VB2AN=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(VB2AN.toString())); 
                             }
                             if("2B".equals(map.get("TYPE"))){
                                VB2BN=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(VB2BN.toString())); 
                             }
                         }
                     }
                 }
                 
            //   2.2.1.2押品未用于再抵押（质押式）
                 if("VP".equals(map.get("NPRODTYPE").toString())) {
                     if(map.get("NOTAMT")!=null) {
                         VP2N=new BigDecimal(map.get("NOTAMT").toString()).add(new BigDecimal(VP2N.toString()));
                     }
                 }
             } 
        }
        
        mapEnd.put("CTLRN",CTLRN.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR1",new BigDecimal(CTLR1.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR2A",new BigDecimal(CTLR2A.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR2B",new BigDecimal(CTLR2B.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR1N",new BigDecimal(NCTLR1N.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR1M",new BigDecimal(NCTLR1M.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR2BM",new BigDecimal(NCTLR2BM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR2AN",new BigDecimal(NCTLR2AN.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR2AM",new BigDecimal(NCTLR2AM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLR2BN",new BigDecimal(NCTLR2BN.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB1N",new BigDecimal(VB1N.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB1M",new BigDecimal(VB1M.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB2AN",new BigDecimal(VB2AN.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB2AM",new BigDecimal(VB2AM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB2BN",new BigDecimal(VB2BN.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VB2BM",new BigDecimal(VB2BM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("VP2N",new BigDecimal(VP2N.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        
        // 2.2.1.1.5 其他            VBE
        //RPT12
        List<Map<String, Object>> otherGuaranteeList = g2501.countOtherGuarantee(paramMap);
        Object OVB=0.00;
        Object OVBE=0.00;
        if(otherGuaranteeList.size()>0&&otherGuaranteeList!=null) {
            if(otherGuaranteeList.get(0) != null && otherGuaranteeList.get(0).get("NOTAMT")!=null) {
                OVB=otherGuaranteeList.get(0).get("NOTAMT").toString();
            }
        }
        OVBE=new BigDecimal(OVB.toString())
                .subtract(new BigDecimal(VB1N.toString())).subtract(new BigDecimal(VB2AN.toString())).subtract(new BigDecimal(VB2BN.toString()));
        mapEnd.put("VBE",new BigDecimal(OVBE.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
    }
    
    
    /**
     * 2.1.3.1与央行进行的担保融资         RBPN
     * RPT9
     * @param paramMap
     * @param mapEnd
     */
    private void countGuaranteeWithCB(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> guaranteeWithCBList = g2501.countGuaranteeWithCB(paramMap);
        if(guaranteeWithCBList!=null&&guaranteeWithCBList.size() > 0&&guaranteeWithCBList.get(0) != null &&guaranteeWithCBList.get(0).get("NOTAMT")!=null) {
            mapEnd.put("RBPN", new BigDecimal(guaranteeWithCBList.get(0).get("NOTAMT").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        } else {
            mapEnd.put("RBPN", 0.00);
        }
    }
    
    
    
    /*
       */
    //SQL:RPT10
    //由2B级资产担保的融资交易
    /**
     *  2.1.3.4由2B级资产担保的融资交易（与央行以外其他交易对手）    
       2.1.3.4.1交易对手为本国主权、多边开发银行、公共部门实体     CTLR2BN
        2.1.3.4.1.1押品市场价值                           CTLR2BM
       2.1.3.4.2其他交易对手                              CTLR2BON
        2.1.3.4.2.1押品市场价值                           CTLR2BOM
        RPT10
     * @param paramMap
     * @param mapEnd
     */
    private void countGuaranteeBy2B(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> guaranteeBy2BList = g2501.countGuaranteeBy2B(paramMap);
        Object CTLR2BN=0.00;
        Object CTLR2BM=0.00;
        Object CTLR2BON=0.00;
        Object CTLR2BOM=0.00;
        if(guaranteeBy2BList.size()>0&&guaranteeBy2BList!=null) {
            for (Map<String, Object> map : guaranteeBy2BList) {
                if(map.get("PUBTYPE")!=null) {
                    if(map.get("VAL")!=null) {
                        if("PUB".equals(map.get("PUBTYPE").toString())) {
                            CTLR2BM=map.get("NOTAMT");
                        }
                        if("NOPUB".equals(map.get("PUBTYPE").toString())) {
                            CTLR2BOM=map.get("NOTAMT");
                        }
                    }
                    if(map.get("NOTAMT")!=null) {
                        if("PUB".equals(map.get("PUBTYPE").toString())) {
                            CTLR2BN=map.get("BAL");
                        }
                        if("NOPUB".equals(map.get("PUBTYPE").toString())) {
                            CTLR2BON=map.get("BAL");
                        }
                    }
                }
            }
        }
        mapEnd.put("CTLR2BN", new BigDecimal(CTLR2BN.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR2BM", new BigDecimal(CTLR2BM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR2BON", new BigDecimal(CTLR2BON.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("CTLR2BOM", new BigDecimal(CTLR2BOM.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
    }
    
    /**
     *  2.1.3.5由其他资产担保的融资交易（与央行以外其他交易对手）
          2.1.3.5.1交易对手为本国主权、多边开发银行、公共部门实体               NCTLNP
          2.1.3.5.2其他交易对手                                 NCTLNE
        RPT11
     * @param paramMap
     * @param mapEnd
     */
    private void countGuaranteeByOthers(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> guaranteeByOthersList = g2501.countGuaranteeByOthers(paramMap);
        Object NCTLNP=0.00;
        Object NCTLNE=0.00;
        if(guaranteeByOthersList.size()>0&&guaranteeByOthersList!=null) {
            for (Map<String, Object> map : guaranteeByOthersList) {
                if(map.get("PUBTYPE")!=null){
                    if(map.get("NOTAMT")!=null){
                        if("PUB".equals(map.get("PUBTYPE"))) {
                            NCTLNP=map.get("NOTAMT");
                        }
                        if("NOPUB".equals(map.get("PUBTYPE"))) {
                            NCTLNE=map.get("NOTAMT");
                        }
                    }
                }
            }
        }
        mapEnd.put("NCTLNP", new BigDecimal(NCTLNP.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
        mapEnd.put("NCTLNE", new BigDecimal(NCTLNE.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
    }
    
    /**
     * 2.1.2.4.3 银行存款,有业务关系且无存款保险           DLDTCRBK
     * 2.1.2.4.6 其他金融机构存款,有业务关系且无存款保险   DLDTCRNBK
     * 2.2.2.6.1 有业务关系的款项                   DLDTCCBK
     * RPT13
     * @param paramMap
     * @param mapEnd
     */
    private void countDeposit(Map<String,Object> paramMap,Map<String,Object> mapEnd) {
        List<Map<String, Object>> depositList = g2501.countDeposit(paramMap);
        Object DLDTCRBK=0.00;
        Object DLDTCCBK=0.00;
        Object DLDTCRNBK=0.00; 
        if(depositList.size()>0) {
            for (Map<String, Object> map : depositList) {
                if(map.get("CCYAMT")!=null&& map.get("PRODTYPE")!=null) {
                    //2.1.2.4.3银行存款，有业务关系且无存款保险        30日内到期同业拆入
                    //2.1.2.4.6其他金融机构存款，有业务关系且无存款保险        30日内到期同业拆入
                    if("CR".equals(map.get("PRODTYPE"))) {
                        if("BK".equals(map.get("TYPE"))) {
                            DLDTCRBK=map.get("CCYAMT");
                        }
                        if("NBK".equals(map.get("TYPE"))) {
                            DLDTCRNBK=map.get("CCYAMT");
                        }
                        
                    }
                    //2.2.2.6.1有业务关系的款项 30日到期同业拆出
                    if("CC".equals(map.get("PRODTYPE"))) {
                        DLDTCCBK=new BigDecimal(map.get("CCYAMT").toString()).add(new BigDecimal(DLDTCCBK.toString()));
                    }
                }
            }
        }
         mapEnd.put("DLDTCCBK",new BigDecimal(DLDTCCBK.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
         mapEnd.put("DLDTCRBK",new BigDecimal(DLDTCRBK.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
         mapEnd.put("DLDTCRNBK",new BigDecimal(DLDTCRNBK.toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );  
    }
    
    
    
    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
        Date date = DateUtil.parse(queryDate);
        Map<String, Object> report = getReport(paramer,date);
        return report;
    }
}
