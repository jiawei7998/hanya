package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.PCapitalTransactionVO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * 自营资金交易信息表
 * @author cg
 *
 */
@Service
public interface PCapitalTransactionService  extends ReportService{
    
    public List<PCapitalTransactionVO> selectPCapitalTransaction(Map<String, Object> param);
    
    public Page<PCapitalTransactionVO> selectPCapitalTransactionPage(Map<String, Object> param);
}
