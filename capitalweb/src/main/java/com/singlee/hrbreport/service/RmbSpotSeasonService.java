package com.singlee.hrbreport.service;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName rmbSpotSeasonService.java
 * @Description 人民币外汇即期交易季报表
 * @createTime 2021年10月12日 09:17:00
 */
public interface RmbSpotSeasonService extends ReportService {
}
