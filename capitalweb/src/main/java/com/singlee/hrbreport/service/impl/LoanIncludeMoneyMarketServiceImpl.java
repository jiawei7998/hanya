package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.hrbreport.mapper.LoanIncludeMoneyMarketMapper;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketCounGroupVO;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketVO;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * _贷款拆放含拆放银行同业及联行资产表
 * @author zk
 *
 */
@Service
public class LoanIncludeMoneyMarketServiceImpl implements LoanInSameService {
    
    @Autowired
    LoanIncludeMoneyMarketMapper loanMapper;
    @Autowired
    DayendDateService dayendDateService;
    /**
     * _贷款拆放含拆放银行同业及联行资产表
     */
    @Override
    public Page<LoanIncludeMoneyMarketVO> getLoanReportData(Map<String,Object> param) {
        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        //查询数据
        List<LoanIncludeMoneyMarketVO> reportList = getLoanReportDataList(param);
        //分页
        Page<LoanIncludeMoneyMarketVO> page = HrbReportUtils.producePage(reportList,pageNum,pageSize);
        return page;
    }

    
    /**
     *_ 贷款拆放含拆放银行同业及联行资产表 按国别按币种
     */
    @Override
    public Page<LoanIncludeMoneyMarketCounGroupVO> getLoanCounGroupReportData(Map<String,Object> param) {
        //获取页码
        int pageNum = (int)param.get("pageNum");
        int pageSize = (int)param.get("pageSize");
        //调用按国别分组方法
        List<LoanIncludeMoneyMarketCounGroupVO> reportList = getCounGroupLoanReportData(param);
        //分页
        Page<LoanIncludeMoneyMarketCounGroupVO> page = HrbReportUtils.producePage(reportList,pageNum,pageSize);
        return page;
    }
    
    /**
     * _按国别分组
     * @param param
     * @return
     */
    private List<LoanIncludeMoneyMarketCounGroupVO> getCounGroupLoanReportData(Map<String, Object> param){
        List<LoanIncludeMoneyMarketCounGroupVO> reportList = new ArrayList();
        //获取贷款拆放数据
        List<LoanIncludeMoneyMarketVO> loanList = getLoanReportDataList(param);
        //按国别分组
        HashSet<String> counSet = new HashSet<String>();
        for(LoanIncludeMoneyMarketVO loan : loanList) {
            counSet.add(loan.getCtrArea() + ":" + loan.getCcy());
        }
        
        for(String counCcy:counSet) {
            LoanIncludeMoneyMarketCounGroupVO counGroup = new LoanIncludeMoneyMarketCounGroupVO();
            
            //变量初始化
            BigDecimal lastAmt = new BigDecimal(0);
            BigDecimal lastAccroutst = new BigDecimal(0);
            BigDecimal ccyAmt = new BigDecimal(0);
            BigDecimal loneYearAmt = new BigDecimal(0);
            BigDecimal accroutst = new BigDecimal(0);
            BigDecimal changeAmt = new BigDecimal(0);
            BigDecimal accroutstAmt = new BigDecimal(0);
            BigDecimal mtdIncexp = new BigDecimal(0);
            
            counGroup.setLastAmt(lastAmt.toString());
            counGroup.setLastAccroutst(lastAccroutst.toString());
            counGroup.setCcyAmt(ccyAmt.toString());
            counGroup.setLoneYearAmt(loneYearAmt.toString());
            counGroup.setAccroutst(accroutst.toString());
            counGroup.setChangeAmt(changeAmt.toString());
            counGroup.setAccroutstAmt(accroutstAmt.toString());
            counGroup.setMtdIncexp(mtdIncexp.toString());
            
            //分组计算
            for(LoanIncludeMoneyMarketVO loan  : loanList) {
                if(counCcy.equals(loan.getCtrArea() + ":" + loan.getCcy())) {
                    if(null != loan.getLastAmt() && !"".equals(loan.getLastAmt())) {
                        lastAmt = lastAmt.add(new BigDecimal(loan.getLastAmt()));
                    }
                    if(null != loan.getLastAccroutst() && !"".equals(loan.getLastAccroutst())) {
                        lastAccroutst = lastAccroutst.add(new BigDecimal(loan.getLastAccroutst()));
                    }
                    if(null != loan.getCcyAmt() && !"".equals(loan.getCcyAmt())) {
                        ccyAmt = ccyAmt.add(new BigDecimal(loan.getCcyAmt()));
                    }
                    if(null != loan.getLoneYearAmt() && !"".equals(loan.getLoneYearAmt())) {
                        loneYearAmt = loneYearAmt.add(new BigDecimal(loan.getLoneYearAmt()));
                    }
                    if(null != loan.getAccroutst() && !"".equals(loan.getAccroutst())) {
                        accroutst = accroutst.add(new BigDecimal(loan.getAccroutst()));
                    }
                    if(null != loan.getChangeAmt() && !"".equals(loan.getChangeAmt())) {
                        changeAmt = changeAmt.add(new BigDecimal(loan.getChangeAmt()));
                    }
                    if(null != loan.getAccroutstAmt() && !"".equals(loan.getAccroutstAmt())) {
                        accroutstAmt = accroutstAmt.add(new BigDecimal(loan.getAccroutstAmt()));
                    }
                    if(null != loan.getMtdIncexp() && !"".equals(loan.getMtdIncexp())) {
                        mtdIncexp = mtdIncexp.add(new BigDecimal(loan.getMtdIncexp()));
                    }
                }
            }
            
            //结果赋值
            String[] split = counCcy.split(":");
            counGroup.setCoun(split[0]);
            counGroup.setCcy(split[1]);
            counGroup.setRepMonth(param.get("queryDate").toString().trim());
            counGroup.setLastAmt(lastAmt.toString());
            counGroup.setLastAccroutst(lastAccroutst.toString());
            counGroup.setCcyAmt(ccyAmt.toString());
            counGroup.setLoneYearAmt(loneYearAmt.toString());
            counGroup.setAccroutst(accroutst.toString());
            counGroup.setChangeAmt(changeAmt.toString());
            counGroup.setAccroutstAmt(accroutstAmt.toString());
            counGroup.setMtdIncexp(mtdIncexp.toString());
            
            reportList.add(counGroup);
        }
        return reportList;
    }
    
    /**
     * _贷款拆放含拆放银行同业及联行资产表
     * @param param
     * @return
     */
    private List<LoanIncludeMoneyMarketVO> getLoanReportDataList(Map<String,Object> param) {
        HashSet<String> set = new HashSet<String>();
        List<LoanIncludeMoneyMarketVO> reportList = new ArrayList<LoanIncludeMoneyMarketVO>();
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString("yyyy-MM"));
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = getLastDate(DateUtil.format(CalendarUtil.getFirstDayOfMonth(queryDay))) ;
        String endDate = getLastDate(getNextDate(DateUtil.format(CalendarUtil.getLastDayOfMonth(queryDay))));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        List<LoanIncludeMoneyMarketVO> lastMonthList = loanMapper.getLastMonthLoanReportData(param);
        List<LoanIncludeMoneyMarketVO> thisMonthList = loanMapper.getThisMonthLoanReportData(param);
        List<LoanIncludeMoneyMarketVO> teroList = loanMapper.getTeroReportData(param);
        List<LoanIncludeMoneyMarketVO> thisBalanceList = loanMapper.getThisMonthBalanceReportData(param);
        List<LoanIncludeMoneyMarketVO> thisInterestList = loanMapper.getThisMonthInterestReportData(param);
        
        for(LoanIncludeMoneyMarketVO loan:lastMonthList) {
            set.add(loan.getCid().trim());
        }
        for(LoanIncludeMoneyMarketVO loan:thisMonthList) {
            set.add(loan.getCid().trim());
        }
        for(LoanIncludeMoneyMarketVO loan:teroList) {
            set.add(loan.getCid().trim());
        }
        for(LoanIncludeMoneyMarketVO loan:thisBalanceList) {
            set.add(loan.getCid().trim());
        }
        for(LoanIncludeMoneyMarketVO loan:thisInterestList) {
            set.add(loan.getCid().trim());
        }
        
        String Opics = param.get("OPICS").toString().trim();
        String ORGNO = getStaticFiled("DKCFZC", "填报机构代码",Opics);
        String CRSIGN = getStaticFiled("DKCFZC", "是否委托贷款",Opics);
        String AGENTDEP = getStaticFiled("DKCFZC", "委托所属部门",Opics);
        String CTRDEP = getStaticFiled("DKCFZC", "对方部门",Opics);
        String LINK = getStaticFiled("DKCFZC", "对方与本机构/委托人的关系",Opics);
        String CHANGEAMT = getStaticFiled("DKCFZC", "本月非交易变动",Opics);
        String DESC = getStaticFiled("DKCFZC", "备注",Opics);
        String OAREA = "";
        Integer i = 0;//计数
        for(String cid:set) {
            LoanIncludeMoneyMarketVO reportVO = new LoanIncludeMoneyMarketVO();
            reportVO.setCid(cid);
            reportVO.setLastAmt("0");
            reportVO.setLastAccroutst("0");
            reportVO.setCcyAmt("0");
            reportVO.setLoneYearAmt("0");
            reportVO.setAccroutst("0");
            reportVO.setChangeAmt("0");
            reportVO.setAccroutstAmt("0");
            reportVO.setMtdIncexp("0");
            reportVO.setLoneYearAmt("0");
            
            for(LoanIncludeMoneyMarketVO loan:lastMonthList) {
                if(cid.equals(loan.getCid().trim())){
                    reportVO.setCno(loan.getCno());
                    reportVO.setCcy(loan.getCcy());
                    
                    String lastAccroutst = loan.getLastAccroutst().toString();
                    String lastAmt = loan.getLastAmt().toString();
                    reportVO.setLastAccroutst(lastAccroutst);//上月末应收利息余额
                    reportVO.setLastAmt(lastAmt);//上月末本金余额
                    BigDecimal accroutstamt = new BigDecimal(lastAccroutst).add(new BigDecimal(lastAmt)) .negate();
                    reportVO.setAccroutstAmt(accroutstamt.toString());
                }
            }
            
            for(LoanIncludeMoneyMarketVO loan:thisMonthList) {
                if(cid.equals(loan.getCid().trim())) {
                    reportVO.setCno(loan.getCno());
                    reportVO.setCcy(loan.getCcy());
                    String accroutst = loan.getAccroutst();
                    String mtdIncexp = loan.getMtdIncexp();
                    String ccyAmt = loan.getCcyAmt();
                    reportVO.setAccroutst(accroutst);//本月末应收利息余额
                    reportVO.setMtdIncexp(mtdIncexp);//本月利息收入
                    reportVO.setCcyAmt(ccyAmt);//本月末本金余额
                    
                    String lastAccroutstAmt = reportVO.getAccroutstAmt();
                    BigDecimal accroutstAmt = new BigDecimal(accroutst).add(new BigDecimal(ccyAmt));
                    if(reportVO.getAccroutstAmt() != null) {
                        accroutstAmt = accroutstAmt.add(new BigDecimal(lastAccroutstAmt));
                    }
                    reportVO.setAccroutstAmt(accroutstAmt.toString());
                }
            }
            
            reportVO.setTero("1");
            for(LoanIncludeMoneyMarketVO loan:teroList) {
                if(cid.equals(loan.getCid().trim())) {
                    reportVO.setCno(loan.getCno());
                    reportVO.setCcy(loan.getCcy());
                  //期限超过1年的
                    reportVO.setTero("2");
                }
            }
            
            for(LoanIncludeMoneyMarketVO loan:thisBalanceList) {
                if(cid.equals(loan.getCid().trim())) {
                    reportVO.setCno(loan.getCno());
                    reportVO.setCcy(loan.getCcy());
                    //本月末本金余额:其中期限在一年及以下
                    reportVO.setLoneYearAmt(loan.getLoneYearAmt());
                }
            }
            
            for(LoanIncludeMoneyMarketVO loan:thisInterestList) {
                if(cid.equals(loan.getCid())) {
                    reportVO.setCno(loan.getCno());
                    reportVO.setCcy(loan.getCcy());
                    
                    //本月利息收入
                    String mtdincexp1 = reportVO.getMtdIncexp();
                    String mtdincexp2 = loan.getMtdIncexp();
                    BigDecimal mtdincexp = new BigDecimal(0);//本月利息收入
                    if(mtdincexp1 != null) {
                        mtdincexp = new BigDecimal(mtdincexp1).add(new BigDecimal(mtdincexp2));
                    }
                    reportVO.setMtdIncexp(mtdincexp.toString());
                }
            }
            
            reportVO.setOrgNo(ORGNO);
            reportVO.setDataNo(ORGNO.trim() + "aaaaaD02aaaaa" + queryDate.replace("-", "") + fullWithZero(i.toString(), 7));
            
            param.put("cno", reportVO.getCno() + " ");//DLDT.cno 与 CUST.cno长度不一致 , 这里用空格补长度
            OAREA = loanMapper.getCoun(param);
            reportVO.setCtrArea(OAREA);
            
            reportVO.setCrsign(CRSIGN);
            reportVO.setAgentDep(AGENTDEP);
            reportVO.setCtrDep(CTRDEP);
            reportVO.setLink(LINK);
            reportVO.setChangeAmt(CHANGEAMT);
            reportVO.setDesc(DESC);
            reportVO.setRepMonth(queryDate);
            
            reportList.add(reportVO);
            i++;
        }
        return reportList;
    }
    
    /**
     * _获取数据库配置数据
     * @param tableId
     * @param tableValue
     * @return
     */
    @Override
    public String getStaticFiled(String tableId, String tableValue, String opics) {
        HashMap<String,Object> param = new HashMap<String,Object>();
        param.put("tableId", tableId);
        param.put("tableValue", tableValue);
        param.put("OPICS", opics);
        String text = loanMapper.getStaticFiled(param);
        return text;
    }
    
    /**
     * _补全位数
     * @param num
     * @param length
     * @return
     */
    @Override
    public String fullWithZero(String num, int length) {
        if (num.length() < length) {
            String temp = "";
            for (int i = 0; i < length - num.length(); i++) {
                temp = temp + "0";
            }
            return (temp + num).trim();
        }
        return "";
    }
    
    /**
     * holidate传入节假日,返回节假日前的第一个工作日
     * @return
     */
    @Override
    public String getLastDate(String holidate) {
        do {
            holidate = getPreDay(holidate);
        }
        while (isHolidDate(holidate));
        return holidate;
    }
    
    /**
     * holidate传入节假日,返回节假日后的第一个工作日
     * @return
     */
    @Override
    public String getNextDate(String holidate) {
        do {
            holidate = getNextDay(holidate);
        }
        while (isHolidDate(holidate));
        return holidate;
    }

    
    /**
     * _获取前一天
     * @param date
     * @return
     */
    @Override
    public String getPreDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, -1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    
    /**
     * _获取第二天的日期
     * 
     * @return
     */
    @Override
    public String getNextDay(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(date));
            c.add(Calendar.DAY_OF_MONTH, 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    
    /**
     * _ 判断传入的日期是否是节假日
     * 
     * @param date
     * @return
     */
    @Override
    public boolean isHolidDate(String date) {
        Map<String, Object> map = new HashMap<>();
        map.put("calDate",date);

        boolean flag =dayendDateService.queryJJR(map)>0?true:false;
        return flag;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();

        paramer.put("OPICS", SystemProperties.opicsUserName);
        paramer.put("HRBCB", SystemProperties.hrbcbUserName);

        //贷款拆放含拆放银行同业及联行资产表
        List<LoanIncludeMoneyMarketVO> loanList = getLoanReportDataList(paramer);
        //贷款拆放含拆放银行同业及联行资产表 按国别按币种
        List<LoanIncludeMoneyMarketCounGroupVO> LoanGroupList = getCounGroupLoanReportData(paramer);

        mapend.put("loanList", loanList);
        mapend.put("LoanGroupList", LoanGroupList);

        return mapend;
    }
}
