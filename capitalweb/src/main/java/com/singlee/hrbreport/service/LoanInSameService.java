package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketCounGroupVO;
import com.singlee.hrbreport.model.LoanIncludeMoneyMarketVO;

import java.util.Map;


public interface LoanInSameService extends ReportService{
    
    public Page<LoanIncludeMoneyMarketVO> getLoanReportData(Map<String,Object> param);
    
    public Page<LoanIncludeMoneyMarketCounGroupVO> getLoanCounGroupReportData(Map<String,Object> param);
    
    public String fullWithZero(String num, int length);
    
    public String getStaticFiled(String tableId,String tableValue,String opics);
    
    public String getLastDate(String holidate);
    
    public String getNextDate(String holidate);
    
    public String getPreDay(String date);
    
    public String getNextDay(String date);
    
    public boolean isHolidDate(String date);
    
}
