package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.hrbreport.mapper.ForeDebtContractReportMapper;
import com.singlee.hrbreport.model.ForeDebtContractVO;
import com.singlee.hrbreport.service.ForeDebtContractReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;



/**
 * @author Administrator
 * 外债签约报表详情
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ForeDebtContractReportServiceImpl implements ForeDebtContractReportService {

	@Autowired
	ForeDebtContractReportMapper contract;

	private String queryDate = "";
	private static final List<Map<String, Object>> reportdata = new ArrayList();
	private List<Map<String, Object>> dataList = new ArrayList();
	private String text;
	public List<Map<String, Object>> noList;
	public List<Map<String, Object>> acctnoList;
	Map<String, Object> map =new HashMap<>();
	
	



	
	   @Override
	    public Page<ForeDebtContractVO> getDataSet(Map<String, Object> reMap) {
	       int pageNum = (int)reMap.get("pageNum");
	        int pageSize = (int)reMap.get("pageSize");
	        List<ForeDebtContractVO> list2 = getDataSetList(reMap);
	      
	        Page<ForeDebtContractVO> page2 =HrbReportUtils.producePage(list2, pageNum, pageSize);
	        return page2;
	      
	    }

	 //返回查询结果
	    @Override
	    public List<ForeDebtContractVO> getDataSetList(Map<String, Object> reMap) {
	        reMap.put("OPICS",  SystemProperties.opicsUserName);
	        reMap.put("HRBCB", SystemProperties.hrbcbUserName);
	        getRecordcount(reMap);         
          RowBounds rb = ParameterUtil.getRowBounds(reMap);
          List<ForeDebtContractVO> List = contract.getGReportsql6(reMap, rb);
          return List;
	    }
	


	//querydate是需要传过来的值
	@Override
	public void getRecordcount(Map<String, Object> reMap) {
		this.queryDate = reMap.get("queryDate").toString().trim();
         map.put("OPICS",  SystemProperties.opicsUserName);
	     map.put("HRBCB", SystemProperties.hrbcbUserName);
		createData();

		
	}
	 
	
	
	
	@SuppressWarnings("rawtypes")
	public void createData() {
		if (reportdata.size() != 0) {
			reportdata.clear();
		}

		//从PUBS表里拿到notes塞入datano
		updateDataNo();

		queryData();

		String ORGNO = getStaticFiled("WZQYXX", "债务人代码");
		String DEBTTYPE = getStaticFiled("WZQYXX", "债务类型");
		String SUBDEBTTYPE = getStaticFiled("WZQYXX", "二级债务类型");
		String TYPECODE = getStaticFiled("WZQYXX", "债权人类型代码");
		String SUBTYPECODE = getStaticFiled("WZQYXX", "债权人类型二级代码");
		String SIGN = getStaticFiled("WZQYXX", "跨境融资风险加权余额计算");
		String DEALTYPE = getStaticFiled("WZQYXX", "存款业务类型");
		String LINK = getStaticFiled("WZQYXX", "对方与本机构的关系");
		String SEDATE = getStaticFiled("WZQYXX", "原始期限");
		String DESCS = getStaticFiled("WZQYXX", "备注");
		String BASEAREA = "";
		String BASEAREA1 = "";
		String BASEAREA2 = "";
		String BASEAREA3 = "";
		String FACTORYAREA = "";
		String FACTORYAREA1 = "";
		String FACTORYAREA2 = "";
		String FACTORYAREA3 = "";

		@SuppressWarnings("unchecked")
		List<Map<String,Object>> rateList = new ArrayList();
		for (Map<String,Object> dataMap : this.dataList) {
		

			BASEAREA = (String) dataMap.get("BASEAREA");
			map.put("BASEAREA", BASEAREA);
			BASEAREA1 = contract.getCoun(map);
			map.put("BASEAREA", BASEAREA1.trim());
			BASEAREA2 = contract.getCountry(map);

			if ((BASEAREA2 == null) || ("".equals(BASEAREA2))) {
				map.put("BASEAREA", BASEAREA1);
				BASEAREA3 = contract.getCountry2(map);
			} else if (!BASEAREA2.substring(0, 1).matches("\\d+")) {
				map.put("BASEAREA", BASEAREA2);
				BASEAREA3 = contract.getCountry2(map);
			}
			dataMap.put("BASEAREA", BASEAREA3);

			FACTORYAREA = (String) dataMap.get("FACTORYAREA");
			map.put("BASEAREA", FACTORYAREA);
			FACTORYAREA1 = contract.getCoun(map);
			map.put("BASEAREA", FACTORYAREA1.trim());
			FACTORYAREA2 = contract.getCountry(map);
			if ((FACTORYAREA2 == null) || ("".equals(FACTORYAREA2))) {
				map.put("BASEAREA", FACTORYAREA1);
				dataMap.put("FACTORYAREA", FACTORYAREA3);
			} else if (!FACTORYAREA2.substring(0, 1).matches("\\d+")) {
				map.put("BASEAREA", FACTORYAREA2);
				FACTORYAREA3 = contract.getCountry2(map);
			}
			if (FACTORYAREA3.equals("")){
				dataMap.put("FACTORYAREA", FACTORYAREA2);
			}else dataMap.put("FACTORYAREA", FACTORYAREA3);


			if(BASEAREA3.equals("")){
				dataMap.put("BASEAREA", BASEAREA1);
			}else dataMap.put("BASEAREA", BASEAREA1);

			dataMap.put("DATANO", "");
			dataMap.put("ORGNO", ORGNO.trim());
			dataMap.put("DEBTTYPE", DEBTTYPE);
			dataMap.put("TYPECODE", TYPECODE);
			dataMap.put("SIGN", BASEAREA1);
			dataMap.put("DEALTYPE", DEALTYPE);
			dataMap.put("LINK", LINK);
			dataMap.put("SEDATE", SEDATE);
			dataMap.put("DESCS", DESCS);
			dataMap.put("SUBDEBTTYPE", SUBDEBTTYPE);
			dataMap.put("SUBTYPECODE", SUBTYPECODE);



			if (dataMap.get("CLOSEDATE") == null) {
				dataMap.put("CLOSEDATE", "");
			}

			try {

				if ("FIXED".equals(((String) dataMap.get("RATECODE")).trim())) {
					dataMap.put("RATESIGN", "否");
					dataMap.put("RATE", dataMap.get("RATE"));
				} else {
					dataMap.put("RATESIGN", "是");
					dataMap.put("RATE", "0");
					map.put("ACCOUNTNO", dataMap.get("ACCOUNTNO").toString().trim());
					//获取交易说明，利率
					rateList = contract.getGReportsql1(map);
					if ((rateList != null) && (rateList.size() > 0)) {
						dataMap.put("RATE", ((String) ((Map) rateList.get(0)).get("INTRATE")).trim());
					}
				}
				dataMap.put("OPENDATE", dataMap.get("OPENDATE").toString().trim());
				rateList = contract.getGReportsql2(map);
				if (rateList.size() <= 0) {
					HashMap<String,Object> dateMap2 = new HashMap<>();
					dateMap2.put("OPICS",  SystemProperties.opicsUserName);
					dateMap2.put("HRBCB", SystemProperties.hrbcbUserName);
					dateMap2.put("DATANO", dataMap.get("DATANO").toString().trim());
					dateMap2.put("ORGNO", dataMap.get("ORGNO").toString().trim());
					dateMap2.put("DEBTTYPE", dataMap.get("DEBTTYPE").toString().trim());
					dateMap2.put("SUBDEBTTYPE", dataMap.get("SUBDEBTTYPE").toString().trim());
					dateMap2.put("CCY", dataMap.get("CCY").toString().trim());
					dateMap2.put("OPENDATE", dataMap.get("OPENDATE").toString().trim());
					dateMap2.put("BASEAREA", dataMap.get("BASEAREA").toString().trim());
					dateMap2.put("FACTORYAREA", dataMap.get("FACTORYAREA").toString().trim());
					dateMap2.put("TYPECODE", dataMap.get("TYPECODE").toString().trim());
					dateMap2.put("SUBTYPECODE", dataMap.get("SUBTYPECODE").toString().trim());
					dateMap2.put("CODE", dataMap.get("CODE").toString().trim());
					dateMap2.put("CNNAME", dataMap.get("CNNAME"));
					dateMap2.put("SN", dataMap.get("SN").toString().trim());
					dateMap2.put("SIGN", dataMap.get("SIGN").toString().trim());
					dateMap2.put("RATESIGN", dataMap.get("RATESIGN").toString().trim());
					dateMap2.put("RATE", dataMap.get("RATE").toString().trim());
					dateMap2.put("DEALTYPE", dataMap.get("DEALTYPE").toString().trim());
					dateMap2.put("LINK", dataMap.get("LINK").toString().trim());
					dateMap2.put("SEDATE", dataMap.get("SEDATE").toString().trim());
					dateMap2.put("DESCS", dataMap.get("DESCS").toString().trim());
					dateMap2.put("CLOSEDATE", dataMap.get("CLOSEDATE").toString().trim());
					dateMap2.put("ACCOUNTNO", dataMap.get("ACCOUNTNO").toString().trim());
					dateMap2.put("SWIFTCOD",dataMap.get("ACCOUNTNO"));

                 	contract.insertsql(dateMap2);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {

			}

		}

		compareField();
	}

	
	//查询在该日期之前的记录
	public List<Map<String,Object>> queryData() {
		List<Map<String, Object>> queryList = new ArrayList();
		if (this.dataList.size() >= 0) {
            this.dataList.clear();
        }
		try {
		
			
			queryList = contract.getGReportsql3(map);
			for (Map<String,Object> map1 : queryList) {
                this.dataList.add(map1);
            }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
        return dataList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void compareField() {
		List<Map<String, Object>> datanoList = new ArrayList();
		List<Map<String, Object>> tempList = new ArrayList();
		String ORGNO = getStaticFiled("WZQYXX", "债务人代码");
		String DEBTTYPE = getStaticFiled("WZQYXX", "债务类型");
		String SUBDEBTTYPE = getStaticFiled("WZQYXX", "二级债务类型");
		String TYPECODE = getStaticFiled("WZQYXX", "债权人类型代码");
		String SUBTYPECODE = getStaticFiled("WZQYX ", "债权人类型二级代码");
		String SIGN = getStaticFiled("WZQYXX", "跨境融资风险加权余额计算");
		String DEALTYPE = getStaticFiled("WZQYXX", "存款业务类型");
		String LINK = getStaticFiled("WZQYXX", "对方与本机构的关系");
		String SEDATE = getStaticFiled("WZQYXX", "原始期限");
		String DESCS = getStaticFiled("WZQYXX", "备注");
		String BASEAREA = "";
		String BASEAREA1 = "";
		String BASEAREA2 = "";
		String BASEAREA3 = "";
		String FACTORYAREA = "";
		String FACTORYAREA1 = "";
		String FACTORYAREA2 = "";
		String FACTORYAREA3 = "";
		try {
		datanoList = contract.getGReportsql4(map);
			List rateList = new ArrayList();
			
			if(datanoList!=null&&datanoList.get(0)!=null ) {
			    
			
			for (Map dataMap : datanoList) {
				String dataNo = (String) dataMap.get("DATANO");
				tempList = getAcctNO(dataNo);
				if ((tempList != null) && (tempList.size() > 0)) {
				    Map<String,Object>		tempMap = (Map) tempList.get(0);
				BASEAREA = (String) dataMap.get("BASEAREA");
					map.put("BASEAREA", BASEAREA);
					BASEAREA1 = contract.getCoun(map);
					map.put("BASEAREA", BASEAREA1.trim());
					BASEAREA2 = contract.getCountry(map);

					if ((BASEAREA2 == null) || ("".equals(BASEAREA2))) {
						map.put("BASEAREA", BASEAREA1);
						BASEAREA3 = contract.getCountry2(map);
					} else if (!BASEAREA2.substring(0, 1).matches("\\d+")) {
						map.put("BASEAREA", BASEAREA2);
						BASEAREA3 = contract.getCountry2(map);
					}
					dataMap.put("BASEAREA", BASEAREA3);

					FACTORYAREA = (String) dataMap.get("FACTORYAREA");
					map.put("BASEAREA", FACTORYAREA);
					FACTORYAREA1 = contract.getCoun(map);

					map.put("BASEAREA", FACTORYAREA1.trim());
					FACTORYAREA2 = contract.getCountry(map);
					if ((FACTORYAREA2 == null) || ("".equals(FACTORYAREA2))) {
						map.put("BASEAREA", FACTORYAREA1);
						FACTORYAREA3 = contract.getCountry2(map);
					} else if (!FACTORYAREA2.substring(0, 1).matches("\\d+")) {
						map.put("BASEAREA", FACTORYAREA2);
						FACTORYAREA3 = contract.getCountry2(map);
					}
				
				
				tempMap.put("FACTORYAREA", FACTORYAREA3);
				tempMap.put("DATANO", dataNo);
				tempMap.put("ORGNO", ORGNO.trim());
				tempMap.put("DEBTTYPE", DEBTTYPE);
				tempMap.put("TYPECODE", TYPECODE);
				tempMap.put("SIGN", SIGN);
				tempMap.put("DEALTYPE", DEALTYPE);
				tempMap.put("LINK", LINK);
				tempMap.put("SEDATE", SEDATE);
				tempMap.put("DESCS", DESCS);
				tempMap.put("SUBDEBTTYPE", SUBDEBTTYPE);
				tempMap.put("SUBTYPECODE", SUBTYPECODE);
				
				if ("FIXED".equals(((String) tempMap.get("RATECODE")).trim())) {
					tempMap.put("RATESIGN", "否");
					tempMap.put("RATE", tempMap.get("RATE"));
				} else {
					tempMap.put("RATESIGN", "是");
					tempMap.put("RATE", "0");
				rateList = contract.getGReportsql1(tempMap);
					if ((rateList != null) && (rateList.size() > 0)) {
						tempMap.put("RATE", ((String) ((Map) rateList.get(0)).get("INTRATE")).trim());
					}
				}
				tempMap.put("OPENDATE", tempMap.get("OPENDATE"));

				if (!((String) dataMap.get("DATANO")).trim().equals(((String) tempMap.get("DATANO")).trim())) {
                    continue;
                }
				if ((((String) dataMap.get("DATANO")).trim().equals(((String) tempMap.get("DATANO")).trim()))
						&& (((String) dataMap.get("ORGNO")).trim().equals(((String) tempMap.get("ORGNO")).trim()))
						&& (((String) dataMap.get("DEBTTYPE")).trim().equals(((String) tempMap.get("DEBTTYPE")).trim()))
						&& (((String) dataMap.get("SUBDEBTTYPE")).trim()
								.equals(((String) tempMap.get("SUBDEBTTYPE")).trim()))
						&& (((String) dataMap.get("CCY")).trim().equals(((String) tempMap.get("CCY")).trim()))
						&& (((String) dataMap.get("OPENDATE")).trim().equals(((String) tempMap.get("OPENDATE")).trim()))
						&& (((String) dataMap.get("BASEAREA")).trim().equals(((String) tempMap.get("BASEAREA")).trim()))
						&& (((String) dataMap.get("FACTORYAREA")).trim()
								.equals(((String) tempMap.get("FACTORYAREA")).trim()))
						&& (((String) dataMap.get("TYPECODE")).trim().equals(((String) tempMap.get("TYPECODE")).trim()))
						&& (((String) dataMap.get("SUBTYPECODE")).trim()
								.equals(((String) tempMap.get("SUBTYPECODE")).trim()))
						&& (((String) dataMap.get("CODE")).trim().equals(((String) tempMap.get("CODE")).trim()))
						&& (((String) dataMap.get("CNNAME")).trim().equals(((String) tempMap.get("CNNAME")).trim()))
						&& (((String) dataMap.get("SN")).trim().equals(((String) tempMap.get("SN")).trim()))
						&& (((String) dataMap.get("SIGN")).trim().equals(((String) tempMap.get("SIGN")).trim()))
						&& (((String) dataMap.get("RATESIGN")).trim().equals(((String) tempMap.get("RATESIGN")).trim()))
						&& (((String) dataMap.get("RATE")).trim().equals(((String) tempMap.get("RATE")).trim()))
						&& (((String) dataMap.get("DEALTYPE")).trim().equals(((String) tempMap.get("DEALTYPE")).trim()))
						&& (((String) dataMap.get("LINK")).trim().equals(((String) tempMap.get("LINK")).trim()))
						&& (((String) dataMap.get("SEDATE")).trim().equals(((String) tempMap.get("SEDATE")).trim()))
						&& (((String) dataMap.get("CLOSEDATE")).trim()
								.equals(((String) tempMap.get("CLOSEDATE")).trim()))) {
					continue;
				}
			map.put("DATANO", dataMap.get("DATANO").toString().trim());
				contract.delete(map);

				this.queryDate = HrbReportUtils.getToday("yyyy-MM-dd");
			contract.update1(map);

				if (tempMap.get("CLOSEDATE") == null) {
					tempMap.put("CLOSEDATE", "");
				}

				HashMap<String,Object> tempMap2 =new HashMap<>();
				tempMap2.put("DATANO", tempMap.get("DATANO").toString().trim());
				tempMap2.put("ORGNO", tempMap.get("ORGNO").toString().trim());
				tempMap2.put("DEBTTYPE", tempMap.get("DEBTTYPE").toString().trim());
				tempMap2.put("SUBDEBTTYPE", tempMap.get("SUBDEBTTYPE").toString().trim());
				tempMap2.put("CCY", tempMap.get("CCY").toString().trim());
				tempMap2.put("OPENDATE", tempMap.get("OPENDATE").toString().trim());
				tempMap2.put("BASEAREA", tempMap.get("BASEAREA").toString().trim());
				tempMap2.put("FACTORYAREA", tempMap.get("FACTORYAREA").toString().trim());
				tempMap2.put("TYPECODE", tempMap.get("TYPECODE").toString().trim());
				tempMap2.put("SUBTYPECODE", tempMap.get("SUBTYPECODE").toString().trim());
				tempMap2.put("CODE", tempMap.get("CODE").toString().trim());
				tempMap2.put("CNNAME", tempMap.get("CNNAME").toString().trim());
				tempMap2.put("SN", tempMap.get("SN").toString().trim());
				tempMap2.put("SIGN", tempMap.get("SIGN").toString().trim());
				tempMap2.put("RATESIGN", tempMap.get("RATESIGN").toString().trim());
				tempMap2.put("RATE", tempMap.get("RATE").toString().trim());
				tempMap2.put("DEALTYPE", tempMap.get("DEALTYPE").toString().trim());
				tempMap2.put("LINK", tempMap.get("LINK").toString().trim());
				tempMap2.put("SEDATE", tempMap.get("SEDATE").toString().trim());
				tempMap2.put("DESCS", tempMap.get("DESCS").toString().trim());
				tempMap2.put("CLOSEDATE", tempMap.get("CLOSEDATE").toString().trim());
				tempMap2.put("ACCOUNTNO", tempMap.get("ACCOUNTNO").toString().trim());
			
				
				
				contract.insert2(tempMap2);
				}
			}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	
	public List<Map<String, Object>> getAcctNO(String datano) {
		try {
			
			map.put("queryDate", this.queryDate);
			map.put("datano", datano);
			this.acctnoList = contract.getGReportsql7(map);
			return this.acctnoList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	//更新外债签约的datano
	public void updateDataNo() {
		List NO = new ArrayList();
	
		List<Map<String,Object>> retList = new ArrayList();
		try {
			//获取ACCOUNTNO
	 	retList = contract.getGReportsql8(map);

			for (Map dataMap : retList) {
				String dataNo = (String) dataMap.get("ACCOUNTNO");
				NO = getNO(dataNo.trim());

				if ((NO != null) && (NO.size() > 0)) {
		map.put("dataNo", dataNo.trim());
			map.put("NO", (((Map) NO.get(0)).get("NOTES")).toString().trim());
					contract.update3(map);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public List<Map<String, Object>> getNO(String accountno) {
		try {
			map.put("accountno", accountno.trim());
			
			this.noList = contract.getGReportsql9(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.noList;


	}

	// 根据表名和值获取TEXT
	public String getStaticFiled(String tableId, String tableValue) {

	
		map.put("tableId", tableId);
		map.put("tableValue", tableValue);
		
		List<Map<String, Object>> retList = new ArrayList();
		try {
			this.text = "";

			retList = contract.getStaticFiled(map);
			if (retList.size() < 1) {
				this.text = "";
			}
			for (Map map : retList) {
				if (map!=null&&(map.get("TEXT") != null) && (!"".equals(map.get("TEXT")))) {
                    this.text = ((String) map.get("TEXT"));
                } else {
					this.text = "";
				}
				System.out.println(this.text);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		System.out.println(this.text);
		return this.text;
	}


	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
	    Map<String,Object> mapEnd=new HashMap<>();
        List<ForeDebtContractVO> list =getDataSetList(paramer);
        mapEnd.put("list", list);
        return mapEnd;
	}


}
