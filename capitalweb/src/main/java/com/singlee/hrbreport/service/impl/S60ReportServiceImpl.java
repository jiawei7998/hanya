package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.S60ReportMapper;
import com.singlee.hrbreport.service.S60ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * @author Administrator
 *最大十家衍生产品交易业务情况表
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class S60ReportServiceImpl implements S60ReportService {

	
	@Autowired
	private S60ReportMapper s60;

	@Override
	public HashMap<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		HashMap<String,Object> mapEnd = new HashMap<String,Object>();
		String br = paramer.get("BR");
		String RPTDate = HrbReportUtils.getDate(postdate);
		String SRPTDate = HrbReportUtils.dateToMap(postdate);
		String KINF ;

		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", SRPTDate);//报表日期
		
	
		
		BigDecimal tmp = new BigDecimal(0.00);
		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("BR", br);
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", paramer.get("OPICS"));
		paraMap.put("HRBCB", paramer.get("HRBCB"));
		
		
		
		try {
			//表一：业务存量 
			List<Map<String,Object>> list1 = s60.getS60ReporCost(paraMap);
			if(list1.size()>0&&list1!=null){
				 int i=0;
				for (Map<String, Object> map : list1) {
					mapEnd.put("BAL"+i+"B", map.get("FACEAMT"));
					mapEnd.put("NAME"+i+"B", map.get("CNAME"));
					i++;
				}
			}			
			List<Map<String,Object>> list2 = s60.getS60ReporCostf(paraMap);		
			if(list2.size()>0&&list2!=null){
				int i=0;
				for (Map<String, Object> map : list2) {
					mapEnd.put("BAL"+i+"C", map.get("FACEAMT"));
					mapEnd.put("NAME"+i+"C", map.get("CNAME"));	
					i++;
				}
			}
			mapEnd.put("RPTDate", RPTDate);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return mapEnd; 
		
		


	}

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        // TODO Auto-generated method stub
        return null;
    }}
	

	

