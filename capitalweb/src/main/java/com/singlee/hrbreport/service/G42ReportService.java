package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

/**
 * G42表内加权风险资产计算表
 *
 */
public interface G42ReportService extends ReportService {

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) ;
	
}
