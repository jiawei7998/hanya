package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G21ReportMapper;
import com.singlee.hrbreport.service.G21ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * G21 流动性期限缺口统计表
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G21ReportServiceImpl implements G21ReportService {
	
	@Autowired
	private G21ReportMapper g21;

	@Override
	public HashMap<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		HashMap<String,Object> mapEnd = new HashMap<String,Object>();
		String br = paramer.get("BR");
		String RPTDate = HrbReportUtils.getDate(postdate);
		String SRPTDate = HrbReportUtils.dateToMap(postdate);

		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", SRPTDate);//报表日期
		
		String daySt = "";
		String dayEnd = "";
		String yearSt = "";
		String yearEnd = "";
		String dateFlag = "";
		
		BigDecimal tmp = new BigDecimal(0.00);
		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("BR", br);
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", paramer.get("OPICS"));
		paraMap.put("HRBCB", paramer.get("HRBCB"));
		
		List<Map<String,Object>> tmpList = g21.getTmp(paraMap);
		for(Map<String,Object> map:tmpList) {
			if(map == null || map.get("BAL") == null){
				continue;
			}
			String tmpStr = String.valueOf(map.get("BAL"));
			tmp = new BigDecimal(tmpStr);
		}
		
		for(int i=0;i < 8;i++) {
			if(i==0) {daySt = "0";dayEnd="1";dateFlag = "D";}
			if(i==1) {daySt = "2";dayEnd="7";dateFlag = "D";}
			if(i==2) {daySt = "8";dayEnd="30";dateFlag = "D";}
			if(i==3) {daySt = "31";dayEnd="90";dateFlag = "D";}
			if(i==4) {daySt = "90";dayEnd="360"; yearSt="1"; dateFlag = "DY";}
			if(i==5) {daySt = "";dayEnd="";yearSt = "1";yearEnd = "5";dateFlag = "Y";}
			if(i==6) {daySt = "";dayEnd="";yearSt = "5";yearEnd = "10";dateFlag = "Y";}
			if(i==7) {daySt = "";dayEnd="";yearSt = "10";yearEnd = "";dateFlag = "Y";}
			
			paraMap.put("daySt", daySt);
			paraMap.put("dayEnd", dayEnd);
			paraMap.put("yearSt", yearSt);
			paraMap.put("yearEnd", yearEnd);
			paraMap.put("dateFlag", dateFlag);
			
			//拆借   
			List<Map<String,Object>> ccyList = g21.getCCYAMT(paraMap);
			String flagA = "N";String flagL = "N";
			if(ccyList != null && ccyList.size() > 0) {
				for(Map<String,Object> map:ccyList) {
					if("A".equals(map.get("AL"))) {//1.4拆放同业
						if(map!=null && map.get("AMT")!=null) {
							String amt = map.get("AMT").toString();
							mapEnd.put("VALA" + i, HrbReportUtils.tranBigde(amt));
							flagA = "Y";
						}
					}
					if("L".equals(map.get("AL"))) {//3.3同业拆入
						if(map!=null&&map.get("AMT")!=null) {
							String amt = map.get("AMT").toString();
							mapEnd.put("VALL" + i, HrbReportUtils.tranBigde(amt));
							flagL = "Y";
						}
					}
				}
			}
			if("N".equals(flagA)) {
				mapEnd.put("VALA"+i, 0.00);
			}
			if("N".equals(flagL)) {
				mapEnd.put("VALL"+i, 0.00);
			}
			
			
			List<Map<String,Object>> notAmtList = g21.getPSNOTAMT(paraMap);
			String flagP = "N";String flagS = "N";
			if(notAmtList != null && notAmtList.size() > 0) {
				for(Map<String,Object> map:notAmtList) {
					if(map!=null&&"P".equals(map.get("PS"))) {//1.5买入返售资产
						String amt = String.valueOf(map.get("AMT"));
						mapEnd.put("VALP" +i , HrbReportUtils.tranBigde(amt));
						flagP = "Y";
					}
					if("S".equals(map.get("PS"))) {//3.4卖出回购资产
						String amt = String.valueOf(map.get("AMT"));
						mapEnd.put("VALS" +i , HrbReportUtils.tranBigde(amt));
						flagS = "Y";
					}
				}
			}
			if("N".equals(flagP)) {
				mapEnd.put("VALP" + i, 0.00);
			}
			if("N".equals(flagS)) {
				mapEnd.put("VALS" + i, 0.00);
			}
			
			//1.7债券投资
			List<Map<String,Object>> PSNotAmtList = g21.getAA(paraMap);
			if(PSNotAmtList != null && PSNotAmtList.size() > 0) {
				for(Map<String,Object> map:PSNotAmtList) {
					if(map!=null&&map.get("AMT") == null) {
						if(i==1) {
							
							mapEnd.put("VAL" +i , HrbReportUtils.tranBigdeAddTmp(map.get("AMT").toString(),tmp));
						} else {
							mapEnd.put("VAL" +i , HrbReportUtils.tranBigde(map.get("AMT").toString()));
						}
					} else {
						mapEnd.put("VAL" + i, 0.00);
					}
				}
			}
			
			//3.5.1 定期存款(不含财政新存款)
			List<Map<String,Object>> AAList = g21.getNOTAMT(paraMap);
			if(AAList != null && AAList.size() > 0) {
				for(Map<String,Object> map:AAList) {
					if(map!=null&&map.get("AMT") != null) {
						mapEnd.put("VALGD" +i , HrbReportUtils.tranBigde(map.get("AMT").toString()));
					} else {
						mapEnd.put("VALGD" + i, 0.00);
					}
				}
			}
		}
		return mapEnd;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		return null;
	}


}
