package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G1102ReportMapper;
import com.singlee.hrbreport.service.G11ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Administrator
 * G11第Ⅱ部分：资产质量及准备金
 */
@Service
public class G1102ReportServiceImpl implements G11ReportService {


    @Autowired
    private G1102ReportMapper g1102;


    public HashMap<String, Object> getReportMap2(Map<String, Object> paramer, Date postdate) {
        HashMap<String, Object> mapEnd = new HashMap<String, Object>();

        String RPTDate = HrbReportUtils.getDate(postdate);
        String SRPTDate = HrbReportUtils.dateToMap(postdate);

        mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
        mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
        mapEnd.put("RPTDate", SRPTDate);//报表日期


        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("BR", SlSessionHelper.getUser().getBranchId());
        paraMap.put("RPTDate", RPTDate);
        paraMap.put("OPICS", SystemProperties.opicsUserName);
        paraMap.put("HRBCB", SystemProperties.hrbcbUserName);


        try {
            List<Map<String, Object>> list1 = g1102.getG1102Reportsql1(paraMap);
            System.out.println(list1);
            if (list1 != null && list1.size() > 0) {
                for (Map<String, Object> map1 : list1) {
                    //国债
                    if ("DOMGOVSE".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V1", map1.get("ACCTNGTYPE").toString());
                    }
                    //地方政府债
                    if ("DOMLOGOVSE".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V2", map1.get("AMT").toString());
                    }
                    //政策性金融债
                    if ("DOMPOLICSE".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V3", map1.get("AMT").toString());
                    }
                    //商业性金融债券
                    if ("DOMFINSE".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V4", map1.get("AMT").toString());
                    }
                    //非金融企业债券
                    if ("DOMNOFINSE".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V5", map1.get("AMT").toString());
                    }
                    //同业存单
                    if ("DOMCD".equals(map1.get("ACCTNGTYPE").toString())) {
                        mapEnd.put("V10", map1.get("AMT").toString());
                    }

                }
            }
            //存放同业
            List<Map<String, Object>> list2 = g1102.getG1102Reportsql2(paraMap);
            if (list2 != null && list2.size()>0) {
                for (Map<String, Object> map2 : list2) {

                    mapEnd.put("V8", map2.get("CCYAMT"));

                }
            }
            //拆放同业
            List<Map<String, Object>> list3 = g1102.getG1102Reportsql3(paraMap);
            if (list3 != null && list3.size()>0) {
                for (Map<String, Object> map3 : list3) {

                    mapEnd.put("V7", map3.get("CCYAMT").toString());

                }
            }
            //金融机构间买入返售资产
            List<Map<String, Object>> list4 = g1102.getG1102Reportsql4(paraMap);
            if (list4 != null && list4.size()>0) {
                for (Map<String, Object> map4 : list4) {
                    if (map4.get("AMT") != null) {
                        mapEnd.put("V9", map4.get("AMT").toString());
                    }

                }
            }
            BigDecimal v51 = new BigDecimal(0);//BigDecimal v51r=new BigDecimal(0);BigDecimal v51d=new BigDecimal(0);
            List<Map<String, Object>> list5 = g1102.getG1102Reportsql5(paraMap);
            if (list5.size() > 0 && list5 != null && list5.size()>0) {
                if (list5.get(0).get("TPOSACCROUTST") != null) {
                    v51 = new BigDecimal(list5.get(0).get("TPOSACCROUTST").toString());
                }
            }
            List<Map<String, Object>> list5r = g1102.getG1102Reportsql5r(paraMap);
            if (list5r.size() > 0 && list5r != null && list5r.size()>0) {
                if (list5r.get(0) == null) {
                    list5r.add(0, paraMap);
                    System.out.println("list5r这里面的第一个数据是空的 ");

                }
                if (list5r.get(0).get("RACCROUTST") != null) {
                    v51 = v51.add(new BigDecimal(list5r.get(0).get("RACCROUTST").toString()));
                }
            }
            System.out.println("v51 rprh=" + v51);
            List<Map<String, Object>> list5d = g1102.getG1102Reportsql5d(paraMap);
            if (list5d.size() > 0 && list5d != null && list5d.size()>0) {

                if (list5d.get(0) == null) {
                    list5d.add(0, paraMap);
                    System.out.println("list5r的第一个数据是空的 ");

                }

                if (list5d.get(0).get("DACCROUTST") != null) {
                    v51 = v51.add(new BigDecimal(list5d.get(0).get("DACCROUTST").toString()));
                }
            }
            System.out.println("v51 dldt=" + v51);
            mapEnd.put("V11", v51.divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));


            //ABS
            List<Map<String, Object>> list6 = g1102.getG1102Reportsql6s(paraMap);
            if (list6 != null && list6.size()>0) {
                for (Map<String, Object> map6 : list6) {

                    mapEnd.put("V6", map6.get("SETTAMT").toString());

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

//		System.out.println("mapEnd"  +mapEnd);

//		mapEnd.put("V6", "66666");
//		mapEnd.put("V1", "66666");
//		mapEnd.put("V2", "66666");
//		mapEnd.put("V3", "66666");
//		mapEnd.put("V4", "66666");
//		mapEnd.put("V5", "66666");
//		mapEnd.put("V7", "66666");

        return mapEnd;


    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        String postdate = (String) paramer.get("queryDate");

        Map<String, Object> result = new HashMap<>();
        Map<String, Object> mapEnd = getReportMap2(paramer, DateUtil.parse(postdate));
        mapEnd.put("postdate", paramer.get("queryDate"));
        result.put("mapEnd", mapEnd);
        return result;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        // TODO Auto-generated method stub
        return null;
    }


}
