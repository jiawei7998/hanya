package com.singlee.hrbreport.service;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName InBankiboTradingService.java
 * @Description 同业拆借业务交易情况统计表
 * @createTime 2021年10月12日 10:20:00
 */
public interface InBankiboTradingService extends ReportService {
}
