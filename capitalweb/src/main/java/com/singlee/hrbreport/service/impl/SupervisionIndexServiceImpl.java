package com.singlee.hrbreport.service.impl;


import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.SupervisionIndexMapper;
import com.singlee.hrbreport.service.SupervisionIndexService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * @author Administrator
 *2021年1月末城商行/民营银行主要监管指标情况表
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SupervisionIndexServiceImpl implements SupervisionIndexService {

    @Autowired
    SupervisionIndexMapper  index;
    
    @Override
    public List<Map<String, Object>> getReportMap(Map<String, Object> paramer) {
        HashMap<String,Object> remap =new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName); 
        remap.put("HRBCB", SystemProperties.hrbcbUserName);
    return index.getLegal1(remap);
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> result=new HashMap<>();
        Map<String,Object> mapEnd=new HashMap<>();
        List<Map<String, Object>> list =getReportMap(paramer);
      int i=0;
        for( Map<String,Object> remap : list) {
            mapEnd.put("AMT"+i, HrbReportUtils.tranBigdeAmt2(remap.get("AMT")));
            mapEnd.put("OAMT"+i, HrbReportUtils.tranBigdeAmt2(remap.get("OAMT")));
            mapEnd.put("ORASE"+i, HrbReportUtils.tranBigdeAmt2(remap.get("ORASE")));
            mapEnd.put("BEGUNAMT"+i, HrbReportUtils.tranBigdeAmt2(remap.get("BEGUNAMT")));
            mapEnd.put("BEGUNRASE"+i, HrbReportUtils.tranBigdeAmt2(remap.get("BEGUNRASE")));
            mapEnd.put("YEARAMT"+i, HrbReportUtils.tranBigdeAmt2(remap.get("YEARAMT")));
            mapEnd.put("YEARRASE"+i, HrbReportUtils.tranBigdeAmt2(remap.get("YEARRASE")));
           i++;
        }
        result.put("mapEnd", mapEnd);
        return result;
    }
}