package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.AccrualPrincilalChangeVO;

import java.util.List;
import java.util.Map;

/**
 * 本金计提变动表
 * @author zk
 *
 */
public interface AccrualPriChgService extends ReportService {
    
    public Page<AccrualPrincilalChangeVO> getReportPage(Map<String, Object> paramer);
    
    public List<AccrualPrincilalChangeVO> getReportList(Map<String, Object> paramer);
    
}
