package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.TposReportMapper;
import com.singlee.hrbreport.model.TposPo;
import com.singlee.hrbreport.service.TposReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 合并持仓分析报表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TposReportServiceImpl implements TposReportService {

	@Autowired
	private TposReportMapper tposReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();

		String SRPTDate = DateUtil.format(new Date(), "yyyy年MM月dd日");

		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名

		String sdate = ParameterUtil.getString(paramer, "sdate", "");
		String edate = ParameterUtil.getString(paramer, "edate", "");
		String type = ParameterUtil.getString(paramer, "type", "");

		if ("".equals(sdate) || "".equals(edate)) {
			String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
			// Date parse = DateUtil.parse(queryDate, "yyyy-MM-dd");
			// sdate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(parse));
			// edate = DateUtil.format(CalendarUtil.getLastDayOfMonth(parse));
			sdate = queryDate;
			edate = queryDate;
		}

		mapEnd.put("sdate", sdate);// 开始日期
		mapEnd.put("edate", edate);// 结束日期

		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("sdate", sdate);
		mapPar.put("date", sdate.replace("-", ""));
		mapPar.put("edate", edate);
		mapPar.put("type", type);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", ""));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		List<TposPo> list = tposReportMapper.getTposReportPage(mapPar);
		format(list);
		mapEnd.put("list", list);
		return mapEnd;
	}

	@Override
    public Page<TposPo> getTposReportPage(Map<String, String> paramer) {
		int pageNumber = ParameterUtil.getInt(paramer, "pageNumber", 10);
		int pageSize = ParameterUtil.getInt(paramer, "pageSize", 1);
		paramer.put("OPICS", SystemProperties.opicsUserName);
		paramer.put("HRBCB", SystemProperties.hrbcbUserName);

		List<TposPo> list = tposReportMapper.getTposReportPage(paramer);
		format(list);
		return HrbReportUtils.producePage(list, pageNumber, pageSize);
	}

	/**
	 * 
	 */
	public void format(List<TposPo> list) {
		for (TposPo tposPo : list) {
			tposPo.setInvType(tposPo.getInvType().substring(1, tposPo.getInvType().length()));
			tposPo.setJiuQi(getFormatNum(tposPo.getJiuQi()));
			tposPo.setPvbpx(getFormatNum(tposPo.getPvbpx()));
			tposPo.setPv01x(getFormatNum(tposPo.getPv01x()));
			tposPo.setVarx(getFormatNum(tposPo.getVarx()));
			tposPo.setFxzl(getFormatNum(tposPo.getFxzl()));
			tposPo.setPrinAmt(getFormatNum(tposPo.getPrinAmt()));
			tposPo.setCoupRate(getFormatNum(tposPo.getCoupRate()));
			tposPo.setCccb(getFormatNum(tposPo.getCccb()));
			tposPo.setYslx(getFormatNum(tposPo.getYslx()));
			tposPo.setZmjz(getFormatNum(tposPo.getZmjz()));
			tposPo.setYjlx(getFormatNum(tposPo.getYjlx()));
			tposPo.setDnljlxsr(getFormatNum(tposPo.getDnljlxsr()));
			tposPo.setSyljlxsr(getFormatNum(tposPo.getSyljlxsr()));
			tposPo.setTdymtm(getFormatNum(tposPo.getTdymtm()));
			tposPo.setTjpmtm(getFormatNum(tposPo.getTjpmtm()));
			tposPo.setAvgCost(getFormatNum(tposPo.getAvgCost()));
			tposPo.setSettAvgCost(getFormatNum(tposPo.getSettAvgCost()));
			// tposPo.setGz(getFormatNum(tposPo.getGz()));
			tposPo.setOpicsJjsz(getFormatNum(tposPo.getOpicsJjsz()));
			tposPo.setJjsz(getFormatNum(tposPo.getJjsz()));
			tposPo.setYzj(getFormatNum(tposPo.getYzj()));
			tposPo.setWindYzj(getFormatNum(tposPo.getWindYzj()));
			tposPo.setLxtz(getFormatNum(tposPo.getLxtz()));
			tposPo.setSyl(getFormatNum(tposPo.getSyl()));
			tposPo.setJqcbPer(getFormatNum(tposPo.getJqcbPer()));
			tposPo.setJjcbPer(getFormatNum(tposPo.getJjcbPer()));
			tposPo.setYslxPer(getFormatNum(tposPo.getYslxPer()));
			tposPo.setYjlxPer(getFormatNum(tposPo.getYjlxPer()));
			tposPo.setTyjj(getFormatNum(tposPo.getTyjj()));
			tposPo.setJjfy(getFormatNum(tposPo.getJjfy()));
			tposPo.setWindJjfy(getFormatNum(tposPo.getWindJjfy()));
			tposPo.setSynx(getFormatNum(tposPo.getSynx()));
			tposPo.setJzll(getFormatNum(tposPo.getJzll()));
			tposPo.setFdjd(getFormatNum(tposPo.getFdjd()));
			tposPo.setLc(getFormatNum(tposPo.getLc()));
			tposPo.setRbaMount(getFormatNum(tposPo.getRbaMount()));
			tposPo.setRpaMount(getFormatNum(tposPo.getRpaMount()));
			tposPo.setRdaMount(getFormatNum(tposPo.getRdaMount()));
			tposPo.setMe(getFormatNum(tposPo.getMe()));
			tposPo.setvDate(tposPo.getvDate() == null ? null : tposPo.getvDate().substring(0, 10));
			tposPo.setmDate(tposPo.getmDate() == null ? null : tposPo.getmDate().substring(0, 10));
			tposPo.setFxrq(tposPo.getFxrq() == null ? null : tposPo.getFxrq().substring(0, 10));
		}
	}

	/**
	 * 格式化
	 * 
	 * @param num
	 * @return
	 */
	public String getFormatNum(String num) {
		if (num != null && !"".equals(num)) {
			if ("0".equals(num)) {
				return "0.00";
			} else {
				double parseDouble = Double.parseDouble(num);
				DecimalFormat df = new DecimalFormat("#,###.00");
				String data = "";
				if (parseDouble < 1 && parseDouble > 0) {
					data = "0" + df.format(parseDouble);
				} else {
					data = df.format(parseDouble);
				}
				return data;
			}
		} else {
			return "0.00";
		}
	}

}
