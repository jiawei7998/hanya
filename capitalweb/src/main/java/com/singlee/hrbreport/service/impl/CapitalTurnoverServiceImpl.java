package com.singlee.hrbreport.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.CapitalTurnoverMapper;
import com.singlee.hrbreport.service.CapitalTurnoverService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 主要资产和负债业务周计划表
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class CapitalTurnoverServiceImpl implements CapitalTurnoverService {

    @Autowired
    CapitalTurnoverMapper ca;

    @Override
    public List<Map<String, Object>> getReportMap2(Map<String, Object> paramer) {
        HashMap<String, Object> remap = new HashMap<>();
        List<Map<String, Object>> list1 = null;
        remap.put("OPICS", SystemProperties.opicsUserName);
        remap.put("HRBCB", SystemProperties.hrbcbUserName);
        remap.put("postdate", paramer.get("queryDate"));
        if (paramer.get("FZ").equals("1")) {
            list1 = ca.searchLiabilities(remap);
        } else {
            list1 = ca.searchInvest(remap);
        }
        return list1;
    }

    //调用存储过程生成数据
    @Override
    public void getCapitalData(Map<String, Object> paramer) {
        ca.callCapitalTurnover(paramer);
    }


    @Override
    public Page<Map<String, Object>> getCapitalTurnover(Map<String, Object> paramer) {
        int pageNum = (int) paramer.get("pageNum");
        int pageSize = (int) paramer.get("pageSize");

        List<Map<String, Object>> list2 = getReportMap2(paramer);
        Page<Map<String, Object>> page2 = HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }


    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> mapEnd = new HashMap<>();
        map.put("OPICS", SystemProperties.opicsUserName);
        map.put("HRBCB", SystemProperties.hrbcbUserName);
        map.put("postdate", map.get("queryDate"));
        String sheetType = ParameterUtil.getString(map, "sheetType", "");
        if (sheetType.equals("fz")) {
            List<Map<String, Object>> list = ca.searchLiabilities(map);
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> paramer = list.get(i);
                mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);
            }
        } else {
            List<Map<String, Object>> list = ca.searchInvest(map);
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> paramer = list.get(i);
                mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);

            }
        }
            return mapEnd;

        }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }

}

    
    
    
    

