package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.DldtReportMapper;
import com.singlee.hrbreport.model.DldtPo;
import com.singlee.hrbreport.service.DldtReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拆借台账报表
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DldtReportServiceImpl implements DldtReportService {

	@Autowired
	private DldtReportMapper dldtReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();

		String SRPTDate = DateUtil.format(new Date(), "yyyy年MM月dd日");
		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名

		String sdate = ParameterUtil.getString(paramer, "sdate", "");
		String edate = ParameterUtil.getString(paramer, "edate", "");
		String type = ParameterUtil.getString(paramer, "type", "");

		if ("".equals(sdate) || "".equals(edate)) {
			String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
			Date parse = DateUtil.parse(queryDate, "yyyy-MM-dd");
			sdate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(parse));
			edate = DateUtil.format(CalendarUtil.getLastDayOfMonth(parse));
		}

		mapEnd.put("sdate", sdate);// 开始日期
		mapEnd.put("edate", edate);// 结束日期

		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("sdate", sdate);
		mapPar.put("edate", edate);
		mapPar.put("yrMonth", edate.substring(5));
		mapPar.put("type", type);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", ""));
		mapPar.put("ccy", ParameterUtil.getString(paramer, "ccy", "CNY").isEmpty() ? "CNY" : ParameterUtil.getString(paramer, "ccy", "CNY"));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		List<DldtPo> list = dldtReportMapper.getDldtReportPage(mapPar);
		List<Map<String, Object>> rateList = dldtReportMapper.getRateList(mapPar);
		format(list, rateList);

		mapEnd.put("list", list);
		return mapEnd;
	}

	@Override
	public Page<DldtPo> getDldtReportPage(Map<String, String> paramer) {
		int pageNumber = ParameterUtil.getInt(paramer, "pageNumber", 10);
		int pageSize = ParameterUtil.getInt(paramer, "pageSize", 1);
		paramer.put("OPICS", SystemProperties.opicsUserName);
		paramer.put("HRBCB", SystemProperties.hrbcbUserName);

		paramer.put("ccy", ParameterUtil.getString(paramer, "ccy", "CNY").isEmpty() ? "CNY" : ParameterUtil.getString(paramer, "ccy", "CNY"));
		List<DldtPo> list = dldtReportMapper.getDldtReportPage(paramer);
		List<Map<String, Object>> rateList = dldtReportMapper.getRateList(paramer);
		format(list, rateList);
		return HrbReportUtils.producePage(list, pageNumber, pageSize);
	}

	/**
	 * 合并交易下债券列表信息
	 */
	public void format(List<DldtPo> list, List<Map<String, Object>> RateList) {
		for (DldtPo dldtPo : list) {

			// 开始计算手续费
			double faceamt = Double.parseDouble(
					new BigDecimal(dldtPo.getCcyAmtAbs()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			int term = Integer.parseInt(dldtPo.getTerm());
			String prodType = dldtPo.getProdType();
			String amount = null;
			DecimalFormat df = new DecimalFormat("0.0000");
			if (RateList.size() > 0) {
				Double mountOne = Double.parseDouble((String) RateList.get(0).get("RZ1"));
				Double mountTwo = Double.parseDouble((String) RateList.get(0).get("RZ2"));
				if ((("CC").equals(prodType) || ("CR").equals(prodType) || ("LC").equals(prodType)) && term == 1) {
					amount = df.format((faceamt * mountOne));
				} else if ((("CC").equals(prodType) || ("CR").equals(prodType) || ("LC").equals(prodType))
						&& term > 1) {
					amount = df.format((faceamt * mountTwo));
				} else {
					amount = "0";
				}
			}
			dldtPo.setHandAmt(getFormatNum(amount));

			dldtPo.setCcyAmtAbs(getFormatNum(dldtPo.getCcyAmtAbs()));
			dldtPo.setCcyAmt(getFormatNum(dldtPo.getCcyAmt()));
			dldtPo.setTotpayAmt(getFormatNum(dldtPo.getTotpayAmt()));
			dldtPo.setTotaLint(getFormatNum(dldtPo.getTotaLint()));
			dldtPo.setOccupyAmt(getFormatNum(dldtPo.getOccupyAmt()));
			dldtPo.setRpLint(getFormatNum(dldtPo.getRpLint()));
			dldtPo.setOverLint(getFormatNum(dldtPo.getOverLint()));
			dldtPo.setProLoss(getFormatNum(dldtPo.getProLoss()));
		}
	}

	/**
	 * 
	 * @param parm1
	 * @param parm2
	 * @return
	 */
	public String add(String parm1, String parm2) {
		parm1 = parm1 == null ? "0" : parm1;
		parm2 = parm2 == null ? "0" : parm2;
		return new BigDecimal(parm1).add(new BigDecimal(parm2).setScale(4, BigDecimal.ROUND_HALF_UP)).toString();
	}

	/**
	 * 格式化
	 * 
	 * @param num
	 * @return
	 */
	public String getFormatNum(String num) {
		if (num != null && !"".equals(num)) {
			if ("0".equals(num)) {
				return "0.00";
			} else {
				DecimalFormat df = new DecimalFormat("#,###.00");
				String data = df.format(Double.parseDouble(num));
				return data;
			}
		} else {
			return "0.00";
		}
	}

}
