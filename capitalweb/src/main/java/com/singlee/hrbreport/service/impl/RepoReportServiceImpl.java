package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.RepoReportMapper;
import com.singlee.hrbreport.model.RepoPo;
import com.singlee.hrbreport.model.RepoSecPo;
import com.singlee.hrbreport.service.RepoReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 回购台账报表
 *
 */
@Service
public class RepoReportServiceImpl implements RepoReportService {

	@Autowired
	private RepoReportMapper repoReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String, Object> mapEnd = new HashMap<String, Object>();

		String SRPTDate = DateUtil.format(new Date(), "yyyy年MM月dd日");
		mapEnd.put("srptDate", SRPTDate);// 报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());// 工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());// 姓名

		String sdate = ParameterUtil.getString(paramer, "sdate", "");
		String edate = ParameterUtil.getString(paramer, "edate", "");
		String type = ParameterUtil.getString(paramer, "type", "");
		String ps = ParameterUtil.getString(paramer, "ps", "");

		if ("".equals(sdate) || "".equals(edate)) {
			String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
			Date parse = DateUtil.parse(queryDate, "yyyy-MM-dd");
			sdate = DateUtil.format(CalendarUtil.getFirstDayOfMonth(parse));
			edate = DateUtil.format(CalendarUtil.getLastDayOfMonth(parse));
		}

		mapEnd.put("sdate", sdate);// 开始日期
		mapEnd.put("edate", edate);// 结束日期

		Map<String, String> mapPar = new HashMap<String, String>();
		mapPar.put("sdate", sdate);
		mapPar.put("edate", edate);
		mapPar.put("yrMonth", edate.substring(5));
		mapPar.put("type", type);
		mapPar.put("ps", ps);
		mapPar.put("br", ParameterUtil.getString(paramer, "br", ""));
		mapPar.put("OPICS", SystemProperties.opicsUserName);
		mapPar.put("HRBCB", SystemProperties.hrbcbUserName);

		List<RepoPo> list = repoReportMapper.getRepoReportPage(mapPar);
		List<Map<String, Object>> rateList = repoReportMapper.getRateList(mapPar);
		format(list, rateList);

		mapEnd.put("list", list);
		return mapEnd;
	}

	@Override
    public Page<RepoPo> getRepoReportPage(Map<String, String> paramer) {
		int pageNumber = ParameterUtil.getInt(paramer, "pageNumber", 10);
		int pageSize = ParameterUtil.getInt(paramer, "pageSize", 1);
		paramer.put("OPICS", SystemProperties.opicsUserName);
		paramer.put("HRBCB", SystemProperties.hrbcbUserName);

		List<RepoPo> list = repoReportMapper.getRepoReportPage(paramer);
		List<Map<String, Object>> rateList = repoReportMapper.getRateList(paramer);
		format(list, rateList);
		return HrbReportUtils.producePage(list, pageNumber, pageSize);
	}

	/**
	 * 
	 */
	public void format(List<RepoPo> list, List<Map<String, Object>> RateList) {
		for (RepoPo repoPo : list) {
			Map<String, String> parm = new HashMap<String, String>();
			parm.put("dealNo", repoPo.getDelNo());
			parm.put("OPICS", SystemProperties.opicsUserName);
			parm.put("HRBCB", SystemProperties.hrbcbUserName);
			List<RepoSecPo> repoSecPoList = repoReportMapper.getRepoSecPoList(parm);
			for (RepoSecPo repoSecPo : repoSecPoList) {
				repoPo.setSecId(repoPo.getSecId() + ";" + repoSecPo.getSecId().trim());
				repoPo.setFaceAmt(repoPo.getFaceAmt() + ";" + getFormatNum(repoSecPo.getFaceAmt()));
				repoPo.setDeprice(repoPo.getDeprice() + ";" + getFormatNum(repoSecPo.getDeprice()));
				repoPo.setZlLevel(repoPo.getZlLevel() + ";" + repoSecPo.getZlLevel());

				repoPo.setAmt(add(repoPo.getAmt(), repoSecPo.getFaceAmt()));
			}

			// 开始计算手续费
			String repoType = repoPo.getRepoType();
			String safeKeepAcct = repoPo.getSafeKeepAcct();
			String text = repoPo.getDealText();
			DecimalFormat df = new DecimalFormat("0.0000");
			String dealtext = "";
			Double mountOne = null;
			Double mountTwo = null;
			String amount = null;
			Double zzMount = null;
			double amt = Double.parseDouble(new BigDecimal(repoPo.getAmt() == null ? "0" : repoPo.getAmt().toPlainString())
					.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			int term = Integer.parseInt(repoPo.getTerm());
			if (text != null && !"".equals(text)) {
				String[] textFiled = text.replaceAll("；", ";").split(";");
				if (textFiled.length >= 2) {
					dealtext = textFiled[1];
				} else {
					dealtext = "";
				}
			} else {
				dealtext = "";
			}
			if (RateList.size() > 0) {
				if ("CDTC".equals(safeKeepAcct)) {
					mountOne = Double.parseDouble((String) RateList.get(0).get("RZ1"));
					mountTwo = Double.parseDouble((String) RateList.get(0).get("RZ2"));
					if ("ASA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("REPO1"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("ASA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("REPO1"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("AMA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("REPO2"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("AMA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("REPO2"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("MD".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("BUYOUT"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("MD".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("BUYOUT"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("GD".equals(repoType)) {
						amount = "0";
					} else {
						amount = "0";
					}
				} else if ("SCH".equals(safeKeepAcct)) {
					mountOne = Double.parseDouble((String) RateList.get(0).get("RZ1"));
					mountTwo = Double.parseDouble((String) RateList.get(0).get("RZ2"));
					if ("ASA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQREPO1"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("ASA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQREPO1"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("AMA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQREPO2"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("AMA".equals(dealtext.toUpperCase()) && "ZY".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQREPO2"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("MD".equals(repoType) && term == 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQBUYOUT"));
						amount = df.format(zzMount + (amt * mountOne));
					} else if ("MD".equals(repoType) && term > 1) {
						zzMount = Double.parseDouble((String) RateList.get(0).get("SQBUYOUT"));
						amount = df.format(zzMount + (amt * mountTwo));
					} else if ("GD".equals(repoType)) {
						amount = "0";
					} else {
						amount = "0";
					}
				} else {
					amount = "0";
				}
			}
			if (repoPo.getSecId() != null && repoPo.getSecId().length() > 5) {
				repoPo.setSecId(repoPo.getSecId().substring(5, repoPo.getSecId().length()));
				repoPo.setFaceAmt(repoPo.getFaceAmt().substring(5, repoPo.getFaceAmt().length()));
				repoPo.setDeprice(repoPo.getDeprice().substring(5, repoPo.getDeprice().length()));
				repoPo.setZlLevel(repoPo.getZlLevel().substring(5, repoPo.getZlLevel().length()));
			}

			repoPo.setAmt(getFormatDecimal(repoPo.getAmt()));
			repoPo.setComProcdAmt(getFormatDecimal(repoPo.getComProcdAmt()));
			repoPo.setMatProcdAmt(getFormatDecimal(repoPo.getMatProcdAmt()));
			repoPo.setTotaLint(getFormatDecimal(repoPo.getTotaLint()));
			repoPo.setOccupyAmt(getFormatDecimal(repoPo.getOccupyAmt()));
			repoPo.setHandAmt(getFormatDecimal(new BigDecimal(amount)));
			repoPo.setRpLint(getFormatDecimal(repoPo.getRpLint()));
			repoPo.setOverLint(getFormatDecimal(repoPo.getOverLint()));
			repoPo.setProLoss(getFormatDecimal(repoPo.getProLoss()));

		}
	}

	/**
	 * 
	 * @param parm1
	 * @param parm2
	 * @return
	 */
	public BigDecimal add(BigDecimal parm1, String parm2) {
		parm1 = parm1 == null ? new BigDecimal("0") : parm1;
		parm2 = parm2 == null ? "0" : parm2;
		return parm1.add(new BigDecimal(parm2).setScale(4, BigDecimal.ROUND_HALF_UP));
	}

	/**
	 * 格式化
	 * 
	 * @param num
	 * @return
	 */
	public String getFormatNum(String num) {
		if (num != null && !"".equals(num)) {
			if ("0".equals(num)) {
				return "0.00";
			} else {
				DecimalFormat df = new DecimalFormat("#,###.00");
				String data = df.format(Double.parseDouble(num));
				return data;
			}
		} else {
			return "0.00";
		}
	}
	
	public BigDecimal getFormatDecimal(BigDecimal numb) {
        if (numb != null) {
            return numb.setScale(2, BigDecimal.ROUND_HALF_UP);
        } else {
            return new BigDecimal("0.00");
        }
    }

}
