package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbnewport.service.CommonExportService;

import java.util.List;
import java.util.Map;


/**
 *中俄月报表
 */





public interface ChinaRussiaReportService extends CommonExportService {






     void getChinaRussiaDate(Map<String, Object> reMap);

     Page<Map<String,Object>> getChinaRussiaPage(Map<String, Object> params);

     List<Map<String,Object>> getDate(Map<String,Object> map);
    
    
}
