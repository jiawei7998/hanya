package com.singlee.hrbreport.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G1401ReportMapper;
import com.singlee.hrbreport.service.G1401ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;

/**
 * G14 大额风险暴露统计表 
 * @author zk
 */
@Service
public class G1401ReportServiceImpl implements G1401ReportService {

    @Autowired
    G1401ReportMapper g1401ReportMapper;
    
    @Override
    public Map<String, Object> getReport(Map<String, Object> paramer, Date postdate) {
        Map<String, Object> mapend = new HashMap<String, Object>();
        Map<String, String> seasonBegEnd = HrbReportUtils.getSeasonBegEnd(postdate);
        paramer.put("startDate",seasonBegEnd.get("begin"));
        paramer.put("endDate",seasonBegEnd.get("end"));
        
        String queryDate = DateUtil.format(postdate);
        mapend.put("queryDate", queryDate);
        
        Map<String, Object> querySame = g1401ReportMapper.querySame(paramer);
        Map<String, Object> querySameMax = g1401ReportMapper.querySameMax(paramer);
        Map<String, Object> queryUnsame = g1401ReportMapper.queryUnsame(paramer);
        Map<String, Object> queryUnsameMax = g1401ReportMapper.queryUnsameMax(paramer);
        
        mapend.put("SAME", new BigDecimal(ParameterUtil.getString(querySame,"AMT","0")));
        mapend.put("SAMEMAX", new BigDecimal(ParameterUtil.getString(querySameMax,"AMT","0")));
        mapend.put("UNSAME", new BigDecimal(ParameterUtil.getString(queryUnsame,"AMT","0")));
        mapend.put("UNSAMEMAX", new BigDecimal(ParameterUtil.getString(queryUnsameMax,"AMT","0")));
        
        return mapend;
    }
    
    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate){
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        String queryDate = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString());
        Map<String, Object> report = getReport(paramer,DateUtil.parse(queryDate));
        return report;
    }
    
}
