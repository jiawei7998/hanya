package com.singlee.hrbreport.service;


import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;




/**
 *地方政府债情况报送
 */
public interface LocalGovernmentBondBillService extends ReportService{

    public Page<Map<String,Object>> getReportPage(Map<String, Object> paramer);
    
    public List<Map<String,Object>> getReportMap(Map<String, Object> paramer);
    
}
