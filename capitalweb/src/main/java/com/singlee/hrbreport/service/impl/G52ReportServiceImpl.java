package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G52ReportMapper;
import com.singlee.hrbreport.service.G52ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * g52地方政府融资平台及支出责任债务持有情况统计表
 * @author zk
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G52ReportServiceImpl implements G52ReportService {
    
    private final static Map<String,Object> map = new HashMap(32);
    static {
        map.put("BJ" , "%北京%" ) ;
        map.put("TJ" , "%天津%" ) ;
        map.put("HB" , "%河北%" ) ;
        map.put("SX1" , "%山西%" ) ;
        map.put("NMG" , "%内蒙古%" ) ;
        map.put("LN" , "%辽宁%" ) ;
        map.put("JL" , "%吉林%" ) ;
        map.put("HLJ" , "%黑龙江%" ) ;
        map.put("SH" , "%上海%" ) ;
        map.put("JS" , "%江苏%" ) ;
        map.put("ZJ" , "%浙江%" ) ;
        map.put("AH" , "%安徽%" ) ;
        map.put("FJ" , "%福建%" ) ;
        map.put("JX" , "%江西%" ) ;
        map.put("SD" , "%山东%" ) ;
        map.put("HN1" , "%河南%" ) ;
        map.put("HB" , "%湖北%" ) ;
        map.put("HN2" , "%湖南%" ) ;
        map.put("GD" , "%广东%" ) ;
        map.put("GX" , "%广西%" ) ;
        map.put("HN3" , "%海南%" ) ;
        map.put("CQ" , "%重庆%" ) ;
        map.put("SC" , "%四川%" ) ;
        map.put("GZ" , "%贵州%" ) ;
        map.put("YN" , "%云南%" ) ;
        map.put("XZ" , "%西藏%" ) ;
        map.put("SX2" , "%陕西%" ) ;
        map.put("GS" , "%甘肃%" ) ;
        map.put("QH" , "%青海%" ) ;
        map.put("NX" , "%宁夏%" ) ;
        map.put("XJ" , "%新疆%" ) ;
        map.put("XM" , "%厦门%" ) ;
        map.put("QD" , "%青岛%" ) ;
        map.put("DL" , "%大连%" ) ;
        map.put("SZ" , "%深圳%" ) ;
        map.put("NB" , "%宁波%" ) ;

    }
    
    @Autowired
    G52ReportMapper g52ReportMapper;

    @Override
    public Map<String, Object> query(Map<String, Object> param) {
        Map<String,Object> mapEnd = new HashMap<String,Object>(128);
        
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        queryDate = DateUtil.format(queryDay);
        
        Map<String, String> seasonBegEnd = HrbReportUtils.getSeasonBegEnd(queryDay);
        param.put("endDate",seasonBegEnd.get("end"));
        
        String yearSt = "";
        String yearEnd = "";
        for(int i = 1; i <= 4;i++) {
            if(i==1) {yearSt=null;yearEnd = "1"; }
            if(i==2) {yearSt = "1";yearEnd = "5";}
            if(i==3) {yearSt = "5";yearEnd = "10";}
            if(i==4) {yearSt = "10";yearEnd = null;}
            
            param.put("yearSt",yearSt);
            param.put("yearEnd",yearEnd);
            
            for(Map.Entry<String,Object> entry:map.entrySet()) {
                param.put("area",entry.getValue());
                List<Map<String,Object>> queryAmt = g52ReportMapper.queryAmt(param);
                
                for(Map<String,Object> map:queryAmt) {
                    String amt = map.get("AMT").toString();
                    mapEnd.put(entry.getKey() + i, new BigDecimal(amt));
                }
            }
        }
        
        return mapEnd;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> mapend = query(paramer);
        return mapend;
    }
    
}
