package com.singlee.hrbreport.service;

import java.util.Map;

public interface AssetandLoanService extends ReportService{
    
    public Map<String , Object> queryAssetandLoan(Map<String , Object> param);
    
}
