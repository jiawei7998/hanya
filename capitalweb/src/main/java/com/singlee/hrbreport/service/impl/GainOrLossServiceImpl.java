package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.mapper.GainOrLossMapper;
import com.singlee.hrbreport.service.GainOrLossService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName GainOrLossServiceImpl.java
 * @Description 损益分析
 * @createTime 2021年10月12日 10:00:00
 */
public class GainOrLossServiceImpl implements GainOrLossService {
    @Autowired
    GainOrLossMapper gainOrLossMapper;

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
