package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.ForeDebtBalanceVO;

/**
 * 外债余额
 * @author zk
 *
 */
public interface ForeDebtBalaService extends ReportService{
    
    public Page<ForeDebtBalanceVO> selectCustCreditPage(Map<String,Object> param,Date postdate);
    
    public List<ForeDebtBalanceVO> selectCustCreditList(Map<String,Object> param,Date postdate);
    
}
