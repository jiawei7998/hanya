package com.singlee.hrbreport.service.impl;

import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G14ReportMapper;
import com.singlee.hrbreport.service.G14ReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * @author Administrator
 *最大十家金融机构同业授信情况表
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G14ReportServiceImpl implements G14ReportService {

	
	@Autowired
	private G14ReportMapper g14;

	@Override
	public HashMap<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		HashMap<String,Object> mapEnd = new HashMap<String,Object>();
		
		String RPTDate = HrbReportUtils.getDate(postdate);
		String SRPTDate = HrbReportUtils.dateToMap(postdate);
		int lastyear=Integer.parseInt(RPTDate.toString().substring(0, 4))-1;
		String lastyearday=String.valueOf(lastyear)+"1231";
 		String day=RPTDate.substring(6, 8);
		if("0".equals((RPTDate.substring(6, 8)).substring(0, 1))){
			day=RPTDate.substring(7, 8);
		}else{
			day=RPTDate.substring(6, 8);
		}

		mapEnd.put("USERNAME", SlSessionHelper.getUserId());//工号
		mapEnd.put("FULLNAME", SlSessionHelper.getUser().getUserName());//姓名
		mapEnd.put("RPTDate", SRPTDate);//报表日期
			
		Map<String , Object> paraMap = new HashMap<String,Object>();
		paraMap.put("BR", SlSessionHelper.getUser().getBranchId());
		paraMap.put("RPTDate", RPTDate);
		paraMap.put("OPICS", SystemProperties.opicsUserName);
        paraMap.put("HRBCB", SystemProperties.hrbcbUserName);
        
		paraMap.put("day",day);
		paraMap.put("lastyearday", lastyearday);
		
		
		
		
		try {
          List<Map<String,Object>> list= g14.getG14Reportsql1(paraMap);
    
           int i=1;
			for (Map<String, Object> map : list) {
				System.out.println("这里面是"+map.get("RELACNO"));
				
            paraMap.put("RELACNO", map.get("RELACNO"));
				  //金融机构名称
				   if(map.get("RELACNO")!=null){
					   List<Map<String, Object>> list1 =  g14.getG14Reportsql2(paraMap);
					   System.out.println(list1);
							for (Map<String, Object> map1 : list1) {
							   if(map1.get("CFN1")!=null){
			            	 		mapEnd.put("CFN"+i+"", map1.get("CFN1").toString());
			            	 	}
			            	 	if(map1.get("AMOUNT")!=null){
			            	 		mapEnd.put("AMOUNT"+i+"", new BigDecimal(map1.get("AMOUNT").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP) );
			            	 	}else{
			            	 		mapEnd.put("AMOUNT"+i+"",0.00);
			            	 	}
			            	 	if(map1.get("AMOUNT1")!=null){
			            	 		mapEnd.put("AMOUNT1"+i+"", new BigDecimal(map1.get("AMOUNT1").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			            	 	}else{
			            	 		mapEnd.put("AMOUNT1"+i+"", 0.00);
			            	 	}
			            	 	if(map1.get("AMOUNT2")!=null){
			            	 		mapEnd.put("AMOUNT2"+i+"", new BigDecimal(map1.get("AMOUNT2").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			            	 	}else{
			            	 		mapEnd.put("AMOUNT2"+i+"", 0.00);
			            	 	}
			            	 	if(map1.get("AMOUNT3")!=null){
			            	 		mapEnd.put("AMOUNT3"+i+"", new BigDecimal(map1.get("AMOUNT3").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			            	 	}else{
			            	 		mapEnd.put("AMOUNT3"+i+"", 0.00);
			            	 	}
			            	 	if(map1.get("AMOUNT4")!=null){
			            	 		mapEnd.put("AMOUNT4"+i+"", new BigDecimal(map1.get("AMOUNT4").toString()).divide(new BigDecimal("10000.0"), 2, BigDecimal.ROUND_HALF_UP));
			            	 	}else {
			            	 		mapEnd.put("AMOUNT4"+i+"", 0.00);
			            	 	}
			            	 	if(map1.get("CCODE")!=null){
			            	 		mapEnd.put("CCODE"+i+"",  map1.get("CCODE").toString());
			            	 	}
						}
								
				   } 
			}
		
            	 	
            	 	i++;
          	 	System.out.println(i);
			
		}
	catch (Exception e) {
			e.printStackTrace();
	
		}	
		
		return mapEnd; 
		
		
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		return null;
	}


}
