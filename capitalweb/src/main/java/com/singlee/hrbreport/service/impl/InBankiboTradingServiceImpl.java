package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.mapper.InBankiboTradingMapper;
import com.singlee.hrbreport.service.InBankiboTradingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName InBankiboTradingServiceImpl.java
 * @Description 同业拆借业务交易情况统计表
 * @createTime 2021年10月12日 10:21:00
 */
@Service
public class InBankiboTradingServiceImpl implements InBankiboTradingService {
    @Autowired
    InBankiboTradingMapper inBankiboTradingMapper;

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
