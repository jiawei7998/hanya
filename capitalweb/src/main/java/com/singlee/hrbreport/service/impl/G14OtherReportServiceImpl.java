package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.hrbreport.mapper.G14OtherReportMapper;
import com.singlee.hrbreport.model.G14OtherVO;
import com.singlee.hrbreport.service.G14OtherReportService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * _大额风险暴露统计表
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G14OtherReportServiceImpl implements G14OtherReportService{
    
    
    @Autowired
    G14OtherReportMapper g14OtherReportMapper;

    @Override
    public List<G14OtherVO> getRportList(Map<String, Object> paramer) {
        List<G14OtherVO> list = new ArrayList();
        
        String queryDate = paramer.get("queryDate").toString().trim();
        Date queryDay = queryDate==null||"".equals(queryDate) ? new Date() : DateUtil.parse(queryDate);
        Map<String, String> seasonBegEnd = HrbReportUtils.getSeasonBegEnd(queryDay);
        paramer.put("endDate",seasonBegEnd.get("end"));
        
        String id = ParameterUtil.getString(paramer, "id", "");
        if("g1402".equals(id)) {
            list = g14OtherReportMapper.queryUnSameList(paramer);
        }
        if("g1405".equals(id)) {
            list = g14OtherReportMapper.querySameList(paramer);
        }
        return list;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        Map<String , Object> namemap = new HashMap<String , Object>();
        List<G14OtherVO> rportList = getRportList(paramer);
        String part = ParameterUtil.getString(paramer, "id", "");
        if("g1402".equals(part)) {
            namemap.put("part", "第II部分：大额风险暴露客户情况表");
        }
        if("g1405".equals(part)) {
            namemap.put("part", "第V部分：同业单一客户大额风险暴露情况表");//同业单一客户大额风险暴露情况表
        }
        mapend.put("map", namemap);
        mapend.put("list",rportList);
        return mapend;
    }
    
}
