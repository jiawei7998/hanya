package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.Map;

public interface ReportService {


	
	public Map<String,Object> getReportMap(Map<String,String> paramer, Date postdate);

	public Map<String,Object> getReportExcel(Map<String,Object> paramer);

}