package com.singlee.hrbreport.service;



import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


//2021年1月末城商行/民营银行主要监管指标情况表
@Service
public interface SupervisionIndexService  extends ReportService{
	
    
    
    
    public List<Map<String, Object>> getReportMap(Map<String, Object> paramer);
}
 