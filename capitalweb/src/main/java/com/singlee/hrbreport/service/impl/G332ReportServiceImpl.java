package com.singlee.hrbreport.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbreport.mapper.G332ReportMapper;
import com.singlee.hrbreport.service.G332ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * G332利率重新定价风险情况报表第二部分：银行账户
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class G332ReportServiceImpl implements G332ReportService {
	
	@Autowired
	private G332ReportMapper g332ReportMapper;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Map<String,Object> mapEnd = new HashMap<String, Object>();
		Date postdate = new Date();
		String SRPTDate = DateUtil.format(postdate, "yyyy年MM月dd日");
		
		mapEnd.put("srptDate", SRPTDate);//报表日期
		mapEnd.put("userId", SlSessionHelper.getUserId());//工号
		mapEnd.put("userName", SlSessionHelper.getUser().getUserName());//姓名
		String date = ParameterUtil.getString(paramer, "queryDate", DateUtil.getCurrentDateAsString()).replace("-", "");
		
		String br = ParameterUtil.getString(paramer,"br","01");
		String OPICS = SystemProperties.opicsUserName;
		String HRBCB = SystemProperties.hrbcbUserName;

		
		String sdate = "";
		String edate = "";
		
		for (int i = 0; i < 6; i++) {
			if(i == 0) {sdate = "0"; edate = "30"; }//1月
			if(i == 1) {sdate = "31"; edate = "90"; }//1月到3月
			if(i == 2) {sdate = "91"; edate = "180"; }//3月到6月
			if(i == 3) {sdate = "181"; edate = "370"; }//6月到1年
			
			mapEnd = switchCase(i, date, br, OPICS, HRBCB, sdate, edate, "1", mapEnd);
		}
		
		for (int i = 6; i < 19; i++) {
			if(i == 4) {sdate = "12"; edate = "24"; }//1年到2
			if(i == 5) {sdate = "24"; edate = "36"; }//2年到3年
			if(i == 6) {sdate = "36"; edate = "48"; }//3年到4年
			if(i == 7) {sdate = "48"; edate = "60"; }//4年到5年
			if(i == 8) {sdate = "60"; edate = "84"; }//5年到7年
			if(i == 9) {sdate = "84"; edate = "120"; }//7年到10年
			if(i == 10) {sdate = "120"; edate = "180"; }//10年到15年
			if(i == 11) {sdate = "180"; edate = "240"; }//15年到20年
			if(i == 12) {sdate = "240"; edate = "240000"; }//20年以上
			
			mapEnd = switchCase(i, date, br, OPICS, HRBCB, sdate, edate, "2", mapEnd);
			
		}
		
		return mapEnd;
	}

	/**
	 * 
	 * @param i				索引	
	 * @param date			时间
	 * @param br			机构
	 * @param userName		用户
	 * @param sdate			时间区域-开始时间
	 * @param edate			时间区域-结束时间
	 * @param sqlType		sql类型
	 * @param mapEnd		execl结果	
	 */
	public Map<String,Object> switchCase(int i,String date,String br,String OPICS,String HRBCB,String sdate,String edate,String sqlType,Map<String,Object> mapEnd) {

		Map<String,String> map = new HashMap<String, String>();
		map.put("date", date);
		map.put("br", br);
		map.put("OPICS", OPICS);
		map.put("HRBCB", HRBCB);
		map.put("sdate", sdate);
		map.put("edate", edate);
		map.put("sqlType", sqlType);
		
		for (int j = 0; j < 8; j++) {
			String result = null;
			switch (j) {
				case 0:
					map.put("dprodtype", "1");
					map.put("rprodtype", "1");
					map.put("acctngtype", "1");
					result = g332ReportMapper.getG332ReportCust(map);
					break;
				case 1:
					map.put("dprodtype", "1");
					map.put("rprodtype", "1");
					map.put("acctngtype", "3");
					result = g332ReportMapper.getG332ReportCust(map);
					break;
				case 2:
					result = g332ReportMapper.getG332ReportBond(map);
					break;	
				case 3:
					map.put("dprodtype", "2");
					map.put("rprodtype", "2");
					map.put("acctngtype", "1");
					result = g332ReportMapper.getG332ReportCust(map);
					break;	
				case 4:
					map.put("fixfloatind", "R");
					result = g332ReportMapper.getG332ReportSwap(map);
					break;	
				case 5:
					map.put("fixfloatind", "P");
					result = g332ReportMapper.getG332ReportSwap(map);
					break;	
				case 6:
					result = g332ReportMapper.getG332ReportDebt(map);
					break;	
				case 7:
					map.put("dprodtype", "1");
					map.put("rprodtype", "1");
					map.put("acctngtype", "4");
					result = g332ReportMapper.getG332ReportCust(map);
					break;		
			}
			
			String tempVal="v"+String.valueOf(i)+String.valueOf(j)+"al";
			if(result != null){
				mapEnd.put(tempVal, new BigDecimal(result.toString()).divide(new BigDecimal("10000.0"), 6, BigDecimal.ROUND_HALF_UP));
			}else{
				mapEnd.put(tempVal, 0);
			}
			
		}
		return mapEnd;
	}
}
