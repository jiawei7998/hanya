package com.singlee.hrbreport.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.LocalGovernmentBondMapper;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;
import com.singlee.hrbreport.service.LocalGovernmentBondService;
import com.singlee.hrbreport.util.HrbReportUtils;


/**
 * 地方政府债明细
 */
@Service
public class LocalGovernmentBondServiceImpl implements LocalGovernmentBondService {

    @Autowired
    LocalGovernmentBondMapper bond;


    @Override
    public List<LocalGovernmentBondVO> selectGovernmentBond(Map<String, Object> param) {
        param.put("OPICS", SystemProperties.opicsUserName);
        param.put("HRBCB", SystemProperties.hrbcbUserName);
        List<LocalGovernmentBondVO> GovernmentBondlist = bond.getBondList(param);
        return GovernmentBondlist;
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String, Object> mapEnd = new HashMap<>();
        List<LocalGovernmentBondVO> list = selectGovernmentBond(paramer);
        mapEnd.put("list", list);
        return mapEnd;
    }


    @Override
    public Page<LocalGovernmentBondVO> searchGovernmentBondPage(Map<String, Object> param) {

        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        List<LocalGovernmentBondVO> list2 = selectGovernmentBond(param);

        Page<LocalGovernmentBondVO> page2 = HrbReportUtils.producePage(list2, pageNum, pageSize);
        return page2;
    }


}