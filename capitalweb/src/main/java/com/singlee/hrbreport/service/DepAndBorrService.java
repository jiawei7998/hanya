package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.DepositAndBorrowingBranchUploadVO;
import com.singlee.hrbreport.model.DepositAndBorrowingForeInvestorVO;
import com.singlee.hrbreport.model.DepositAndBorrowingSummarizingVO;

import java.util.Map;

/**
 * _存款含银行同业和联行存放负债
 * @author zk
 *
 */
public interface DepAndBorrService extends ReportService{
    
    public Page<DepositAndBorrowingForeInvestorVO>  getForeInvestorReportPage(Map<String, Object> map);
    
    public Page<DepositAndBorrowingBranchUploadVO>  getBranchUploadReportPage(Map<String, Object> map);
    
    public Page<DepositAndBorrowingSummarizingVO>  getSummarizingReportPage(Map<String, Object> map);
    
}
