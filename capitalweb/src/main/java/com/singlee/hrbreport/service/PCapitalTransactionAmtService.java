package com.singlee.hrbreport.service;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.model.PCapitalTransactionAmtVO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;


/**
 * 自营资金交易余额
 * @author cg
 *
 */
@Service
public interface PCapitalTransactionAmtService  extends ReportService{
    
    public Page<PCapitalTransactionAmtVO> selectPCapitalTransactionAmt(Map<String, Object> param);
    
    
    public List<PCapitalTransactionAmtVO> selectPCapitalTransactionAmtList(Map<String, Object> param);
}
