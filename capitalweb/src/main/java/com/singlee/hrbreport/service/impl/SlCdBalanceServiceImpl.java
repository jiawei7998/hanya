package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.SlCdBalanceMapper;
import com.singlee.hrbreport.model.SlCdBalanceVO;
import com.singlee.hrbreport.service.LoanInSameService;
import com.singlee.hrbreport.service.SlCdBalanceService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * _同业存单发行情况
 * @author zk
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlCdBalanceServiceImpl implements SlCdBalanceService {
    
    @Autowired
    SlCdBalanceMapper slCdBalanceMapper;
    @Autowired
    LoanInSameService loanService;
    
    
    @Override
    public Page<SlCdBalanceVO> searchSlcdBalaPage(Map<String, Object> param) {
        int pageNum = (int) param.get("pageNum");
        int pageSize = (int) param.get("pageSize");
        List<SlCdBalanceVO> searchSlcdBalaList = searchSlcdBalaList(param);
        return HrbReportUtils.producePage(searchSlcdBalaList, pageNum, pageSize);
    }

    @Override
    public List<SlCdBalanceVO> searchSlcdBalaList(Map<String, Object> param) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String queryDate = ParameterUtil.getString(param,"queryDate", DateUtil.getCurrentDateAsString());
        Date queryDay = new Date();
        try {
            if(queryDate != null && !"".equals(queryDate)) {
                queryDay = sf.parse(queryDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取月初 月末日期
        String startDate = loanService.getLastDate(sf.format(CalendarUtil.getFirstDayOfMonth(queryDay))) ;
        String endDate = loanService.getLastDate(loanService.getNextDate(sf.format(CalendarUtil.getLastDayOfMonth(queryDay))));
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        
        param.put("issuer", SystemProperties.hrbReportCno);
        
        List<SlCdBalanceVO> searchSlcdBala = slCdBalanceMapper.searchSlcdBala(param);
        return searchSlcdBala;
    }

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String , Object> mapend = new HashMap<String , Object>();
        paramer.put("OPICS", SystemProperties.opicsUserName);
        paramer.put("HRBCB", SystemProperties.hrbcbUserName);
        List<SlCdBalanceVO> searchSlcdBalaList = searchSlcdBalaList(paramer);
        mapend.put("list", searchSlcdBalaList);
        //年份
        String startDate = ParameterUtil.getString(paramer, "startDate", DateUtil.getCurrentDateAsString());
        String year = startDate.substring(0,4);
        mapend.put("year", year);
        return mapend;
    }
    
}
