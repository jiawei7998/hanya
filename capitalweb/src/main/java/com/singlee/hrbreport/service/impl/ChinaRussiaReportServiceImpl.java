package com.singlee.hrbreport.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.ChinaRussiaReportMapper;
import com.singlee.hrbreport.service.ChinaRussiaReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.*;

/**
 * 中俄月报表
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ChinaRussiaReportServiceImpl implements ChinaRussiaReportService  {

    @Autowired
    ChinaRussiaReportMapper ChinaRussia;
    
   
//    private String postdate = "";
    private List<Map<String, Object>> dataList = new ArrayList();
//    private String startDate = "";
//    private String endDate = "";
//    private String area = "";

   
   
    
    

    public Map<String, Object> getReportExcel(Map<String, Object> map) {
        
        Map<String ,Object> result = new HashMap<>();
//        this.postdate =map.get("postdate").toString().trim();
//        this.area =map.get("area").toString().trim();
//        Map<String, Object> remap =new HashMap<>();
//        remap.put("postdate", map.get("postdate").toString().trim());
//      //地区：哈巴区、阿穆尔州、滨海边疆、后贝加尔区
//        remap.put("area", map.get("area").toString().trim());
        
        map.put("OPICS", SystemProperties.opicsUserName);
        map.put("HRBCB", SystemProperties.hrbcbUserName);
       
      try {
          Map<String ,Object> mapEnd   =createData(map);
          result.put("mapEnd", mapEnd);
      } catch (Exception e) {
        e.printStackTrace();
      }
   
     return  result;
    }


    public Map<String, Object> createData(Map<String, Object> map)throws Exception{
      Map<String ,Object> mapEnd = new HashMap<>();
      String  postdate =(String) map.get("queryDate");
      double startCcyAmt = 0.0D;
      double startUsdAmt = 0.0D;
      double crCcyAmt = 0.0D;
      double crUsdAmt = 0.0D;
      double endCcyAmt = 0.0D;
      double endUsdAmt = 0.0D;
      double drCcyAmt = 0.0D;
      double drUsdAmt = 0.0D;

      try
      {
        if ((postdate != null) && (!"".equals(postdate))) {
//          this.startDate = CalendarUtil.getMonthStartDate(this.postdate);
            //获取上一天的日期，如果是节假日，则再上一天
           String  startDate = getLastDate(postdate);
//          this.endDate = CalendarUtil.getNextDay(CalendarUtil.getMonthEndDate(this.postdate));
            //获取下一天的日期，如果是节假日，则再下一天
           String endDate = getNextDate(postdate);
           map.put("startDate",  startDate);
          
       
         // startCcyAmt = Math.abs(queryAmt("CNY", this.startDate));
          startCcyAmt = Math.abs(queryAmt("CNY", map));
          startUsdAmt = Math.abs(queryAmt("USD", map));

          map.put("startDate",  endDate);
          endCcyAmt = Math.abs(queryAmt("CNY", map));
          endUsdAmt = Math.abs(queryAmt("USD", map));

          map.put("startDate",  startDate);
          map.put("endDate",  endDate);
          crCcyAmt = Math.abs(queryCrAmt("CNY", map));
          crUsdAmt = Math.abs(queryCrAmt("USD", map));
          drCcyAmt = Math.abs(queryDrAmt("CNY", map));
          drUsdAmt = Math.abs(queryDrAmt("USD", map));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      
      mapEnd.put("startCcyAmt",startCcyAmt);
      mapEnd.put("startUsdAmt",startUsdAmt);
      

      mapEnd.put("drCcyAmt",drCcyAmt);
      mapEnd.put("drUsdAmt",drUsdAmt);

     
      
      mapEnd.put("crCcyAmt",crCcyAmt);
      mapEnd.put("crUsdAmt",crUsdAmt);


      mapEnd.put("endCcyAmt",endCcyAmt);
      mapEnd.put("endUsdAmt",endUsdAmt);

      mapEnd.put("queryCount1",queryCount("CNY"));
      mapEnd.put("queryCount2",queryCount("USD"));
      return mapEnd;
    
    }

    public double queryDrAmt(String ccy,  Map<String,Object> map) {
        map.put("ccy", ccy);
      List queryList = new ArrayList();
      double amt = 0.0D;
      if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
          this.dataList.clear();
      }
      try
      {

          this.dataList = ChinaRussia.getsql1(map);
        
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
          amt = Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
          this.dataList.clear();
        }


       
        this.dataList = ChinaRussia.getsql2(map);
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null)
        {
          
            amt += Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      } finally {
       
      }
      return amt;
    }

    public double queryCrAmt(String ccy, Map<String,Object> map) {
      map.put("ccy", ccy);
      List queryList = new ArrayList();
      double amt = 0.0D;
      if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
          this.dataList.clear();
      }
      try
      {
       

        
        this.dataList = ChinaRussia.getsql3(map);

        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
          System.out.println(((Map)this.dataList.get(0)).get("SUM_AMT"));
          
            amt = Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
            this.dataList.clear();
          

        }



       
        this.dataList = ChinaRussia.getsql4(map);
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null)
        {
          
            amt += Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      } finally {
      
      }
      return amt;
    }

    public int queryCount(String ccy)
    {
        HashMap<String,Object> reMap=new HashMap<>();
      List queryList = new ArrayList();
      if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
        this.dataList.clear();
      }
      int count = 0;
      try {
        


      
        this.dataList = ChinaRussia.getsql5(reMap);
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
            count = Integer.parseInt((String) (this.dataList.get(0)).get("CCYCOUNT"));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      } finally {
        
      }
      return count;
    }

    public double queryAmt(String ccy, Map<String,Object> map){
    
        map.put("ccy", ccy);
      List queryList = new ArrayList();
      double amt = 0.0D;
      if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null) {
          this.dataList.clear();
      }
      try
      {
        this.dataList = ChinaRussia.getsql6(map);
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null )
        {
          amt = Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
          this.dataList.clear();
        }
        this.dataList = ChinaRussia.getsql7(map);
        if (this.dataList!=null&&this.dataList.size() > 0&& this.dataList.get(0)!=null)
        {
          if (((Map)this.dataList.get(0)).get("SUM_AMT") != null) {
              amt += Double.parseDouble((String) (this.dataList.get(0)).get("SUM_AMT"));
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      } finally {
       
      }
      return amt;
    }


    public String getLastDate(String dealDate)
      throws Exception
    {
        HashMap<String,Object> reMap=new HashMap<>();
        reMap.put("OPICS", SystemProperties.opicsUserName);
        reMap.put("HRBCB", SystemProperties.hrbcbUserName);
        reMap.put("dealdate", dealDate);
      Date dealdate = DateUtil.parse(dealDate, "yyyy-MM-dd");
      do {
          dealdate = new Date(dealdate.getTime() - 86400000L);
      }
      while (isHolidDate(reMap));
      return DateUtil.format(dealdate);
    }

    public String getNextDate(String dealDate)
      throws Exception
    {
        HashMap<String,Object> reMap=new HashMap<>();
        reMap.put("OPICS", SystemProperties.opicsUserName);
        reMap.put("HRBCB", SystemProperties.hrbcbUserName);
        reMap.put("dealdate", dealDate);
       
      Date dealdate = DateUtil.parse(dealDate, "yyyy-MM-dd");
      
      do {
          dealdate = new Date(dealdate.getTime() + 86400000L);
      }
      while (
        isHolidDate(reMap));
      return DateUtil.format(dealdate);
    }

    public boolean isHolidDate(HashMap<String,Object> reMap) throws SQLException
    {
      try
      {
        
        List hldyList =ChinaRussia.getsql8(reMap);
        return (hldyList != null) && (hldyList.size() != 0);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      } finally {
        
      }
      return false;
    }



    @Override
    public void getChinaRussiaDate(Map<String, Object> reMap) {
        ChinaRussia.getChinaRussiaData(reMap);

    }

    @Override
    public Page<Map<String, Object>> getChinaRussiaPage(Map<String, Object> params) {
        return ChinaRussia.searchData(params, ParameterUtil.getRowBounds(params));
    }

    @Override
    public List<Map<String, Object>> getDate(Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, Object> getExportData(Map<String, Object> map) {
        Map<String, Object> mapEnd = new HashMap<>();
        List<Map<String, Object>> list =ChinaRussia.searchDataList(map);
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> paramer = list.get(i);
            mapEnd.put("mapEnd" + ParameterUtil.getString(paramer, "PX", ""), paramer);
        }
        return mapEnd;
    }

    @Override
    public List<Map<String, Object>> getExportListData(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> getExportPageData(Map<String, Object> map) {
        return null;
    }
}

 


  
    
    
    
    

