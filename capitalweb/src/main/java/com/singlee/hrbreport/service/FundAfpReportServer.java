package com.singlee.hrbreport.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.refund.model.FundAfpReport;


/**
 * 基金申购报表
 *
 */
public interface FundAfpReportServer extends ReportService{

	@Override
    Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate);
	
	public Page<FundAfpReport> getDataSetList(Map<String, Object> reMap) ;
}