package com.singlee.hrbreport.service.impl;

import com.singlee.hrbreport.mapper.RmbSpotSeasonMapper;
import com.singlee.hrbreport.service.RmbSpotSeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName RmbSpotSeasonServiceImpl.java
 * @Description 人民币外汇即期交易季报表
 * @createTime 2021年10月12日 09:18:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RmbSpotSeasonServiceImpl implements RmbSpotSeasonService {
    @Autowired
    RmbSpotSeasonMapper rmbSpotSeasonMapper;

    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        return null;
    }
}
