package com.singlee.hrbreport.service.impl;



import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbreport.mapper.LocalGovernmentBondBillMapper;
import com.singlee.hrbreport.model.LocalGovernmentBondVO;
import com.singlee.hrbreport.service.LocalGovernmentBondBillService;
import com.singlee.hrbreport.util.HrbReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * 地方政府债情况报送 
 */
@Service
public class LocalGovernmentBondBillServiceImpl implements LocalGovernmentBondBillService  {

    @Autowired
    LocalGovernmentBondBillMapper bill;

    @Override
    public Page<Map<String,Object>> getReportPage(Map<String, Object> paramer) {
        int pageNum = Integer.parseInt(paramer.get("pageNum").toString());
        int pageSize = Integer.parseInt(paramer.get("pageSize").toString());
        
        Page<Map<String, Object>> page = HrbReportUtils.producePage(getReportMap(paramer),pageNum,pageSize);
        return page;
    }

    @Override
    public List<Map<String,Object>> getReportMap(Map<String, Object> paramer) {
       HashMap<String,Object> remap =new HashMap<>();
       remap.put("OPICS", SystemProperties.opicsUserName);
       remap.put("HRBCB", SystemProperties.hrbcbUserName);
     
        return bill.getbill1(remap);
    }


    @Override
    public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
        return null;
    }

    @Override
    public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
        Map<String,Object> mapEnd=new HashMap<>();
        Map<String,Object> result=new HashMap<>();
        List<Map<String,Object>> list =getReportMap(paramer);
        for(int i=0;i<list.size();) {
        for( Map<String,Object> map :list) {
           mapEnd.put("val"+i, HrbReportUtils.tranBigdeAmt(map.get("SETTAMT"))) ;
           i++;
        }
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
        
        mapEnd.put("postdate", df.format(new Date()));
        result.put("mapEnd", mapEnd);
        return result;
    }
}