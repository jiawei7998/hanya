package com.singlee.hrbreport.model;

import java.math.BigDecimal;

/**
 * _优质流动性资产台账
 *
 * @author zk
 */
public class CurAssetVO {
    private String secid; //债券代码
    private String descr; //债券名称
    private String secType; //债券种类
    private String assetType; //LCR资产等级
    private BigDecimal prinamt; //面值（万元）
    private BigDecimal rate; //票面利率
    private String subjectrating; //主体评级
    private String crating; //债项评级
    private String vdate; //发行日
    private String mdate; //到期日
    private String exerciseyears; //剩余期限(年)
    private String pledgeState; //质押状态
    private BigDecimal unPleAmt; //未质押账面价值（万元）
    private BigDecimal pleAmt; //已质押账面价值（万元）
    private String pleExeYears; //已质押剩余期限(年) 
    private String pleMDate; //质押到期日
    private String pleRate; //人行借款 质押率（%）
    private BigDecimal val; //评估价值（万元）

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSecType() {
        return secType;
    }

    public void setSecType(String secType) {
        this.secType = secType;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public BigDecimal getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(BigDecimal prinamt) {
        this.prinamt = prinamt;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getSubjectrating() {
        return subjectrating;
    }

    public void setSubjectrating(String subjectrating) {
        this.subjectrating = subjectrating;
    }

    public String getCrating() {
        return crating;
    }

    public void setCrating(String crating) {
        this.crating = crating;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getExerciseyears() {
        return exerciseyears;
    }

    public void setExerciseyears(String exerciseyears) {
        this.exerciseyears = exerciseyears;
    }

    public String getPledgeState() {
        return pledgeState;
    }

    public void setPledgeState(String pledgeState) {
        this.pledgeState = pledgeState;
    }

    public BigDecimal getUnPleAmt() {
        return unPleAmt;
    }

    public void setUnPleAmt(BigDecimal unPleAmt) {
        this.unPleAmt = unPleAmt;
    }

    public BigDecimal getPleAmt() {
        return pleAmt;
    }

    public void setPleAmt(BigDecimal pleAmt) {
        this.pleAmt = pleAmt;
    }

    public String getPleExeYears() {
        return pleExeYears;
    }

    public void setPleExeYears(String pleExeYears) {
        this.pleExeYears = pleExeYears;
    }

    public String getPleMDate() {
        return pleMDate;
    }

    public void setPleMDate(String pleMDate) {
        this.pleMDate = pleMDate;
    }

    public String getPleRate() {
        return pleRate;
    }

    public void setPleRate(String pleRate) {
        this.pleRate = pleRate;
    }

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "CurAssetVO{" +
                "secid='" + secid + '\'' +
                ", descr='" + descr + '\'' +
                ", secType='" + secType + '\'' +
                ", assetType='" + assetType + '\'' +
                ", prinamt=" + prinamt +
                ", rate=" + rate +
                ", subjectrating='" + subjectrating + '\'' +
                ", crating='" + crating + '\'' +
                ", vdate='" + vdate + '\'' +
                ", mdate='" + mdate + '\'' +
                ", exerciseyears='" + exerciseyears + '\'' +
                ", pledgeState='" + pledgeState + '\'' +
                ", unPleAmt=" + unPleAmt +
                ", pleAmt=" + pleAmt +
                ", pleExeYears='" + pleExeYears + '\'' +
                ", pleMDate='" + pleMDate + '\'' +
                ", pleRate='" + pleRate + '\'' +
                ", val=" + val +
                '}';
    }
}
