package com.singlee.hrbreport.model;

public class SameBasicInfoVO {
    private String custName;//客户名称
    private String custNo;//客户代码
    private String orgCode;//组织机构代码
    private String custType;//客户类别
    private String coun;//国别及地区
    private String inLevel;//内部评级
    private String outLevel;//外部评级
    private String ldAmt;//拆放同业
    private String ctAmt;//存放同业
    private String revRepoAmt;//债券回购-买入返售
    private String loanBySecurity;//股票质押贷款
    private String revRepoAsset;//买入返售资产
    private String buyDiscount;//买断式转贴现
    private String secAmt;//持有债券
    private String stock;//股权投资
    private String payForOth;//同业代付
    private String otherAmt;//其他表内业务-存单投资
    public String getCustName() {
        return custName;
    }
    public void setCustName(String custName) {
        this.custName = custName;
    }
    public String getCustNo() {
        return custNo;
    }
    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }
    public String getOrgCode() {
        return orgCode;
    }
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
    public String getCustType() {
        return custType;
    }
    public void setCustType(String custType) {
        this.custType = custType;
    }
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getInLevel() {
        return inLevel;
    }
    public void setInLevel(String inLevel) {
        this.inLevel = inLevel;
    }
    public String getOutLevel() {
        return outLevel;
    }
    public void setOutLevel(String outLevel) {
        this.outLevel = outLevel;
    }
    public String getLdAmt() {
        return ldAmt;
    }
    public void setLdAmt(String ldAmt) {
        this.ldAmt = ldAmt;
    }
    public String getCtAmt() {
        return ctAmt;
    }
    public void setCtAmt(String ctAmt) {
        this.ctAmt = ctAmt;
    }
    public String getRevRepoAmt() {
        return revRepoAmt;
    }
    public void setRevRepoAmt(String revRepoAmt) {
        this.revRepoAmt = revRepoAmt;
    }
    public String getLoanBySecurity() {
        return loanBySecurity;
    }
    public void setLoanBySecurity(String loanBySecurity) {
        this.loanBySecurity = loanBySecurity;
    }
    public String getRevRepoAsset() {
        return revRepoAsset;
    }
    public void setRevRepoAsset(String revRepoAsset) {
        this.revRepoAsset = revRepoAsset;
    }
    public String getBuyDiscount() {
        return buyDiscount;
    }
    public void setBuyDiscount(String buyDiscount) {
        this.buyDiscount = buyDiscount;
    }
    public String getSecAmt() {
        return secAmt;
    }
    public void setSecAmt(String secAmt) {
        this.secAmt = secAmt;
    }
    public String getStock() {
        return stock;
    }
    public void setStock(String stock) {
        this.stock = stock;
    }
    public String getPayForOth() {
        return payForOth;
    }
    public void setPayForOth(String payForOth) {
        this.payForOth = payForOth;
    }
    public String getOtherAmt() {
        return otherAmt;
    }
    public void setOtherAmt(String otherAmt) {
        this.otherAmt = otherAmt;
    }
    @Override
    public String toString() {
        return "SameBasicInfoVO [custName=" + custName + ", custNo=" + custNo + ", orgCode=" + orgCode + ", custType="
                + custType + ", coun=" + coun + ", inLevel=" + inLevel + ", outLevel=" + outLevel + ", ldAmt=" + ldAmt
                + ", ctAmt=" + ctAmt + ", revRepoAmt=" + revRepoAmt + ", loanBySecurity=" + loanBySecurity
                + ", revRepoAsset=" + revRepoAsset + ", buyDiscount=" + buyDiscount + ", secAmt=" + secAmt + ", stock="
                + stock + ", payForOth=" + payForOth + ", otherAmt=" + otherAmt + "]";
    }
}
