package com.singlee.hrbreport.model;




//同业业务（外币币）
public class TheForeignCurrencyVO2  {
    
	
	
    private String br;//部门号
    private String sn;//交易对手全称
    private String cfn1;//交易对手落地机构
    private String ca4;//交易对手地域
    private String vDate;//业务开始日期
    private String mdate;//业务结束日期
    private String intrate;//利率
    private String ccy;//货币类型
    private String ccyamt;//原币金额
    private String spotrate_8;//汇率
    private String rmbamt;//折人民币金额（元）
    public String getBr() {
        return br;
    }
    public void setBr(String br) {
        this.br = br;
    }
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getCfn1() {
        return cfn1;
    }
    public void setCfn1(String cfn1) {
        this.cfn1 = cfn1;
    }
    public String getCa4() {
        return ca4;
    }
    public void setCa4(String ca4) {
        this.ca4 = ca4;
    }
    public String getvDate() {
        return vDate;
    }
    public void setvDate(String vDate) {
        this.vDate = vDate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getIntrate() {
        return intrate;
    }
    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getCcyamt() {
        return ccyamt;
    }
    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }
    public String getSpotrate_8() {
        return spotrate_8;
    }
    public void setSpotrate_8(String spotrate_8) {
        this.spotrate_8 = spotrate_8;
    }
    public String getRmbamt() {
        return rmbamt;
    }
    public void setRmbamt(String rmbamt) {
        this.rmbamt = rmbamt;
    }
    @Override
    public String toString() {
        return "TheForeignCurrencyVO2 [br=" + br + ", sn=" + sn + ", cfn1=" + cfn1 + ", ca4=" + ca4 + ", vDate=" + vDate
                + ", mdate=" + mdate + ", intrate=" + intrate + ", ccy=" + ccy + ", ccyamt=" + ccyamt + ", spotrate_8="
                + spotrate_8 + ", rmbamt=" + rmbamt + "]";
    }
    
    
    
    
    
    
  
    
  
	
}
