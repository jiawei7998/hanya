package com.singlee.hrbreport.model;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public class InBankDealCDSeVO {
    private String secid; //存单代码
    private String vdate; //起息日
    private String mdate; //到期日
    private String days; //期限
    private String rate; //到期收益率
    private double amt; //实际发行量/交易金额
    private String cust; //交易对手
    
    public String getSecid() {
        return secid;
    }
    public void setSecid(String secid) {
        this.secid = secid;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getDays() {
        return days;
    }
    public void setDays(String days) {
        this.days = days;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public double getAmt() {
        return amt;
    }
    public void setAmt(double amt) {
        this.amt = amt;
    }
    public String getCust() {
        return cust;
    }
    public void setCust(String cust) {
        this.cust = cust;
    }
    @Override
    public String toString() {
        return "InBankDealCDSeVO [secid=" + secid + ", vdate=" + vdate + ", mdate=" + mdate + ", days=" + days
                + ", rate=" + rate + ", amt=" + amt + ", cust=" + cust + "]";
    }
}
