package com.singlee.hrbreport.model;

public class LocalGovernmentBondVO {

    private String invtype;            //帐户类型
    private String acctngtype;             //债券类型
    private String issuer;        //债券发行人
    private String secid;         //债券代码
    private String descr;   //债券名称
    private String vadate;          //起息日
    private String tenor;            //期限
    private String mdate;     //到期日
    private String paramt;       //债券面值
    private String couprate_8;       //票面利率
    private String redempamt;    //账面价值
    private String yield_8;       //到期收益率
    public String getInvtype() {
        return invtype;
    }
    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }
    public String getAcctngtype() {
        return acctngtype;
    }
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }
    public String getIssuer() {
        return issuer;
    }
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
    public String getSecid() {
        return secid;
    }
    public void setSecid(String secid) {
        this.secid = secid;
    }
    public String getDescr() {
        return descr;
    }
    public void setDescr(String descr) {
        this.descr = descr;
    }
    public String getVadate() {
        return vadate;
    }
    public void setVadate(String vadate) {
        this.vadate = vadate;
    }
    public String getTenor() {
        return tenor;
    }
    public void setTenor(String tenor) {
        this.tenor = tenor;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getParamt() {
        return paramt;
    }
    public void setParamt(String paramt) {
        this.paramt = paramt;
    }
    public String getCouprate_8() {
        return couprate_8;
    }
    public void setCouprate_8(String couprate_8) {
        this.couprate_8 = couprate_8;
    }
    public String getRedempamt() {
        return redempamt;
    }
    public void setRedempamt(String redempamt) {
        this.redempamt = redempamt;
    }
    public String getYield_8() {
        return yield_8;
    }
    public void setYield_8(String yield_8) {
        this.yield_8 = yield_8;
    }
    @Override
    public String toString() {
        return "LocalGovernmentBondVO [invtype=" + invtype + ", acctngtype=" + acctngtype + ", issuer=" + issuer
                + ", secid=" + secid + ", descr=" + descr + ", vadate=" + vadate + ", tenor=" + tenor + ", mdate="
                + mdate + ", paramt=" + paramt + ", couprate_8=" + couprate_8 + ", redempamt=" + redempamt
                + ", yield_8=" + yield_8 + "]";
    }
   
}
