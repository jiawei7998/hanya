package com.singlee.hrbreport.model;

public class PCapitalTransactionAmtVO {

    
    private String sysday;//采集日期
    private String startdate;//存量日期
    private String br;//银行机构代码
    private String br2;//内部机构号
    private String banksn;//银行机构名称
    private String dealno;//交易编号
    private String acctngtype;//帐户类型
    private String ccy;//币种
    private String trad;//交易柜员
    private String voper;//审核人
    private String dealdate;//交易日期
    private String ps;//买卖标志
    private String ccyamt;//成交金额
    private String vdate;//起息日
    private String mdate;//到息日
    private String netsi;//清算标志
    private String prodtype;//银行产品
    private String intrate;//利率 
    public String getSysday() {
        return sysday;
    }
    public void setSysday(String sysday) {
        this.sysday = sysday;
    }
    public String getStartdate() {
        return startdate;
    }
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }
    public String getBr() {
        return br;
    }
    public void setBr(String br) {
        this.br = br;
    }
    public String getBr2() {
        return br2;
    }
    public void setBr2(String br2) {
        this.br2 = br2;
    }
    public String getBanksn() {
        return banksn;
    }
    public void setBanksn(String banksn) {
        this.banksn = banksn;
    }
    public String getDealno() {
        return dealno;
    }
    public void setDealno(String dealno) {
        this.dealno = dealno;
    }
    public String getAcctngtype() {
        return acctngtype;
    }
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getTrad() {
        return trad;
    }
    public void setTrad(String trad) {
        this.trad = trad;
    }
    public String getVoper() {
        return voper;
    }
    public void setVoper(String voper) {
        this.voper = voper;
    }
    public String getDealdate() {
        return dealdate;
    }
    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }
    public String getPs() {
        return ps;
    }
    public void setPs(String ps) {
        this.ps = ps;
    }
    public String getCcyamt() {
        return ccyamt;
    }
    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getNetsi() {
        return netsi;
    }
    public void setNetsi(String netsi) {
        this.netsi = netsi;
    }
    public String getProdtype() {
        return prodtype;
    }
    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }
    public String getIntrate() {
        return intrate;
    }
    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }
    @Override
    public String toString() {
        return "PCapitalTransactionAmtVO [sysday=" + sysday + ", startdate=" + startdate + ", br=" + br + ", br2=" + br2
                + ", banksn=" + banksn + ", dealno=" + dealno + ", acctngtype=" + acctngtype + ", ccy=" + ccy
                + ", trad=" + trad + ", voper=" + voper + ", dealdate=" + dealdate + ", ps=" + ps + ", ccyamt=" + ccyamt
                + ", vdate=" + vdate + ", mdate=" + mdate + ", netsi=" + netsi + ", prodtype=" + prodtype + ", intrate="
                + intrate + "]";
    }
    
}
