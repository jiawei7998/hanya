package com.singlee.hrbreport.model;




//同业拆借（本币）
public class TheForeignCurrencyVO  {
    
	
	
    private String br;//部门号
    private String sn;//交易对手全称
    private String vDate;//业务开始日期
    private String mdate;//业务结束日期
    private String intrate;//利率
    private String ccyamt;//金额
    public String getBr() {
        return br;
    }
    public void setBr(String br) {
        this.br = br;
    }
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getvDate() {
        return vDate;
    }
    public void setvDate(String vDate) {
        this.vDate = vDate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getIntrate() {
        return intrate;
    }
    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }
    public String getCcyamt() {
        return ccyamt;
    }
    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }
    @Override
    public String toString() {
        return "TheForeignCurrencyVO [br=" + br + ", sn=" + sn + ", vDate=" + vDate + ", mdate=" + mdate + ", intrate="
                + intrate + ", ccyamt=" + ccyamt + "]";
    }
   
}
