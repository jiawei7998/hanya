package com.singlee.hrbreport.model;

/**
 * 人民币同业往来账
 * @author zk
 *
 */
public class RmbNostroVO {
    
    public String type;//明细操作类型
    public String outBank;//境外参加行SWIFT BIC
    public String acctNo;//人民币同业往来账号账户
    public String curBalance;//账户日终余额
    public String sysDate;//系统更新时间
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getOutBank() {
        return outBank;
    }
    public void setOutBank(String outBank) {
        this.outBank = outBank;
    }
    public String getAcctNo() {
        return acctNo;
    }
    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }
    public String getCurBalance() {
        return curBalance;
    }
    public void setCurBalance(String curBalance) {
        this.curBalance = curBalance;
    }
    public String getSysDate() {
        return sysDate;
    }
    public void setSysDate(String sysDate) {
        this.sysDate = sysDate;
    }
    @Override
    public String toString() {
        return "RmbNostroVO [type=" + type + ", outBank=" + outBank + ", acctNo=" + acctNo + ", curBalance="
                + curBalance + ", sysDate=" + sysDate + "]";
    }
}
