package com.singlee.hrbreport.model;

public class G14BookReportVO {
    private String sn ;//客户全称
    private String settamt ;//余额/金额
    private String acctngtype ;//债券类型
    private String cfn1 ;//所属集团
    private String sic ;//客户类型
    private String bood ;//债券投资
    private String absamt ;//资产证券化产品(abs)
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getSettamt() {
        return settamt;
    }
    public void setSettamt(String settamt) {
        this.settamt = settamt;
    }
    public String getAcctngtype() {
        return acctngtype;
    }
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }
    public String getCfn1() {
        return cfn1;
    }
    public void setCfn1(String cfn1) {
        this.cfn1 = cfn1;
    }
    public String getSic() {
        return sic;
    }
    public void setSic(String sic) {
        this.sic = sic;
    }
    public String getBood() {
        return bood;
    }
    public void setBood(String bood) {
        this.bood = bood;
    }
    public String getabsamt() {
        return absamt;
    }
    public void setabsamt(String absamt) {
        this.absamt = absamt;
    }
    @Override
    public String toString() {
        return "G14BookReportVO [sn=" + sn + ", settamt=" + settamt + ", acctngtype=" + acctngtype + ", cfn1=" + cfn1
                + ", sic=" + sic + ", bood=" + bood + ", absamt=" + absamt + "]";
    }


}
