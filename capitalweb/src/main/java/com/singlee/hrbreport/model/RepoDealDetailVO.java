package com.singlee.hrbreport.model;
/**
 * _业务明细_人行  回购交易
 * @author zk
 *
 */
public class RepoDealDetailVO {
    
    private String custNo;//交易对手
    private String valueDate;//首期交割日
    private String tenor;//期限
    private String mDate;//到期交割日
    private String repoRate;//利率
    private Double amount;//首期结算额
    
    public String getCustNo() {
        return custNo;
    }
    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }
    public String getValueDate() {
        return valueDate;
    }
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }
    public String getTenor() {
        return tenor;
    }
    public void setTenor(String tenor) {
        this.tenor = tenor;
    }
    public String getmDate() {
        return mDate;
    }
    public void setmDate(String mDate) {
        this.mDate = mDate;
    }
    public String getRepoRate() {
        return repoRate;
    }
    public void setRepoRate(String repoRate) {
        this.repoRate = repoRate;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    @Override
    public String toString() {
        return "RepoDealDetailVO [custNo=" + custNo + ", valueDate=" + valueDate + ", tenor=" + tenor + ", mDate="
                + mDate + ", repoRate=" + repoRate + ", amount=" + amount + "]";
    }
}
