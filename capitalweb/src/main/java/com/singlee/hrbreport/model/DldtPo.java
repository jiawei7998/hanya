package com.singlee.hrbreport.model;

import java.io.Serializable;

public class DldtPo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dealNo;// 成交单号
	private String prodType;// 产品类型
	private String costDesc;// 成本中心描述 T2
	private String cname;// 客户全名T3
	private String acctDesc;// 对手属性T4
	private String acctType;// 账户类型T5
	private String descr;// 回购（拆借）方式T6
	private String vDate;// 首期交割日T7
	private String term;// 期限T8
	private String mDate;// 到期交割日T9
	private String intrate;// 利率T10
	private String overTerm;// 剩余期限T17
	private String accroutst;//

	private String handAmt;// 交易手续费T16 

	private String ccyAmtAbs;// 券面（拆借）总额T11 
	private String ccyAmt;// 首期结算额T12 
	private String totpayAmt;// 到期结算额T13
	private String totaLint;// 利息支额T14 
	private String occupyAmt;// 资金占用T15
	private String rpLint;// 应收（付）利息额T18
	private String overLint;// 剩余利息额T19
	private String proLoss;// 已实现损益T20 

	private String ccy;// 币种

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getCostDesc() {
		return costDesc;
	}

	public void setCostDesc(String costDesc) {
		this.costDesc = costDesc;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getAcctDesc() {
		return acctDesc;
	}

	public void setAcctDesc(String acctDesc) {
		this.acctDesc = acctDesc;
	}

	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getIntrate() {
		return intrate;
	}

	public void setIntrate(String intrate) {
		this.intrate = intrate;
	}

	public String getOverTerm() {
		return overTerm;
	}

	public void setOverTerm(String overTerm) {
		this.overTerm = overTerm;
	}

	public String getAccroutst() {
		return accroutst;
	}

	public void setAccroutst(String accroutst) {
		this.accroutst = accroutst;
	}

	public String getHandAmt() {
		return handAmt;
	}

	public void setHandAmt(String handAmt) {
		this.handAmt = handAmt;
	}

	public String getCcyAmtAbs() {
		return ccyAmtAbs;
	}

	public void setCcyAmtAbs(String ccyAmtAbs) {
		this.ccyAmtAbs = ccyAmtAbs;
	}

	public String getCcyAmt() {
		return ccyAmt;
	}

	public void setCcyAmt(String ccyAmt) {
		this.ccyAmt = ccyAmt;
	}

	public String getTotpayAmt() {
		return totpayAmt;
	}

	public void setTotpayAmt(String totpayAmt) {
		this.totpayAmt = totpayAmt;
	}

	public String getTotaLint() {
		return totaLint;
	}

	public void setTotaLint(String totaLint) {
		this.totaLint = totaLint;
	}

	public String getOccupyAmt() {
		return occupyAmt;
	}

	public void setOccupyAmt(String occupyAmt) {
		this.occupyAmt = occupyAmt;
	}

	public String getRpLint() {
		return rpLint;
	}

	public void setRpLint(String rpLint) {
		this.rpLint = rpLint;
	}

	public String getOverLint() {
		return overLint;
	}

	public void setOverLint(String overLint) {
		this.overLint = overLint;
	}

	public String getProLoss() {
		return proLoss;
	}

	public void setProLoss(String proLoss) {
		this.proLoss = proLoss;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
}
