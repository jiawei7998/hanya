package com.singlee.hrbreport.model;

public class ForeDebtContractVO {
    
	
	
    private String dataNo;//外债编号
    private String orgNo;//外债人代码
    private String debtType;//债务类型
    private String subDebtType;//二级债务类型
    private String ccy;//货币类型
    private String openDate;//起息日
    private String baseArea;//债权人总部所在国家(地区)代码
    private String factoryArea;//债权人经营地所在国家(地区)代码
    private String typeCode;//债权人类型代码
    private String subTypeCode;//债权人类型二级代码
    private String code;//债权人代码
    private String cnName;//债权人中文名
    private String sn;//债权人英文名
    private String sign;//是否不纳入跨境融资风险加权余额计算
    private String rateSign;//是否浮动利率
    private String rate;//年化利率值
    private String dealType;//存款业务类型
    private String link;//对方与本方的关系
    private String seDate;//原始期限
    private String desc;//备注
    private String swiftcod;//客户号
    public String getDataNo() {
        return dataNo;
    }
    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }
    public String getOrgNo() {
        return orgNo;
    }
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }
    public String getDebtType() {
        return debtType;
    }
    public void setDebtType(String debtType) {
        this.debtType = debtType;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getOpenDate() {
        return openDate;
    }
    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }
    public String getBaseArea() {
        return baseArea;
    }
    public void setBaseArea(String baseArea) {
        this.baseArea = baseArea;
    }
    public String getFactoryArea() {
        return factoryArea;
    }
    public void setFactoryArea(String factoryArea) {
        this.factoryArea = factoryArea;
    }
    public String getTypeCode() {
        return typeCode;
    }
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
    public String getSubTypeCode() {
        return subTypeCode;
    }
    public void setSubTypeCode(String subTypeCode) {
        this.subTypeCode = subTypeCode;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCnName() {
        return cnName;
    }
    public void setCnName(String cnName) {
        this.cnName = cnName;
    }
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }
    public String getRateSign() {
        return rateSign;
    }
    public void setRateSign(String rateSign) {
        this.rateSign = rateSign;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public String getDealType() {
        return dealType;
    }
    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getSeDate() {
        return seDate;
    }
    public void setSeDate(String seDate) {
        this.seDate = seDate;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSubDebtType() {
        return subDebtType;
    }

    public void setSubDebtType(String subDebtType) {
        this.subDebtType = subDebtType;
    }

    public String getSwiftcod() {
        return swiftcod;
    }

    public void setSwiftcod(String swiftcod) {
        this.swiftcod = swiftcod;
    }

    @Override
    public String toString() {
        return "ForeDebtContractVO{" +
                "dataNo='" + dataNo + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", debtType='" + debtType + '\'' +
                ", subDebtType='" + subDebtType + '\'' +
                ", ccy='" + ccy + '\'' +
                ", openDate='" + openDate + '\'' +
                ", baseArea='" + baseArea + '\'' +
                ", factoryArea='" + factoryArea + '\'' +
                ", typeCode='" + typeCode + '\'' +
                ", subTypeCode='" + subTypeCode + '\'' +
                ", code='" + code + '\'' +
                ", cnName='" + cnName + '\'' +
                ", sn='" + sn + '\'' +
                ", sign='" + sign + '\'' +
                ", rateSign='" + rateSign + '\'' +
                ", rate='" + rate + '\'' +
                ", dealType='" + dealType + '\'' +
                ", link='" + link + '\'' +
                ", seDate='" + seDate + '\'' +
                ", desc='" + desc + '\'' +
                ", swiftcod='" + swiftcod + '\'' +
                '}';
    }
}
