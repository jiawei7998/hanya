package com.singlee.hrbreport.model;

import java.io.Serializable;

public class RmbPo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;// 交易号
	private String mdate;// 交割日期
	private String product;// 产品类型
	private String type;// 业务类型
	private String acctdesc;// 债券类别
	private String costdesc;// 成本中心
	private String dealtype;// 交易类型
	private String market;// 市场
	private String secid;// 代码
	private String secname;// 名称
	private String ps;// 买卖方向
	private String smeans;// 结算方式
	private String faceamt;// 成交面额(亿)
	private String cno;// 交易对手
	private String dealno;// 成交单编号
	private String trad;// 交易员
	private String acctype;// 账户类型
	private String inacctype;// 会计账户类型
	private String dealdate;// 交易日期
	private String intsmeans;// 清算方式
	private String clprice;// 成交净价
	private String price;// 成交全价
	private String rate;// 收益率(%)
	private String amt;// 成交金额

//	private String invType;// 会计账户类型
//	private String costDesc;// 业务类型
//	private String acctngType;// 组合类型
//	private String port;// 交易类型
//	private String settDate;// 交割日期
//	private String pNum;// 买入交易笔数
//	private String sNum;// 卖出交易笔数
//	private String num;// 总交易笔数
//	private String num10_15y;// 单笔金额大于10亿小于等于15亿
//	private String num15y;// 大于15亿
//	private String pFaceAmt;// 买入交易量(万元)
//	private String sFaceAmt;// 卖出交易量(万元)
//	private String faceAmt;// 总交易量(万元)

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAcctdesc() {
		return acctdesc;
	}

	public void setAcctdesc(String acctdesc) {
		this.acctdesc = acctdesc;
	}

	public String getCostdesc() {
		return costdesc;
	}

	public void setCostdesc(String costdesc) {
		this.costdesc = costdesc;
	}

	public String getDealtype() {
		return dealtype;
	}

	public void setDealtype(String dealtype) {
		this.dealtype = dealtype;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getSecname() {
		return secname;
	}

	public void setSecname(String secname) {
		this.secname = secname;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getFaceamt() {
		return faceamt;
	}

	public void setFaceamt(String faceamt) {
		this.faceamt = faceamt;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getAcctype() {
		return acctype;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public String getInacctype() {
		return inacctype;
	}

	public void setInacctype(String inacctype) {
		this.inacctype = inacctype;
	}

	public String getDealdate() {
		return dealdate;
	}

	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}

	public String getIntsmeans() {
		return intsmeans;
	}

	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}

	public String getClprice() {
		return clprice;
	}

	public void setClprice(String clprice) {
		this.clprice = clprice;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

}
