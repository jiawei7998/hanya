package com.singlee.hrbreport.model;

import java.math.BigDecimal;

public class ForeDebtBalanceVO {
    
    private String type;//外债类型
    private String no;//外债编号
    private String bankNo;//银行账号
    private String changeNo;//变动编号
    private String changeDate;//变动日期
    private String curbalance;//外债余额
    private String desc;//备注
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    public String getBankNo() {
        return bankNo;
    }
    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }
    public String getChangeNo() {
        return changeNo;
    }
    public void setChangeNo(String changeNo) {
        this.changeNo = changeNo;
    }
    public String getChangeDate() {
        return changeDate;
    }
    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getCurbalance() {
        return curbalance;
    }
    public void setCurbalance(String curbalance) {
        this.curbalance = curbalance;
    }
    @Override
    public String toString() {
        return "ForeDebtBalanceVO [type=" + type + ", no=" + no + ", bankNo=" + bankNo + ", changeNo=" + changeNo
                + ", changeDate=" + changeDate + ", curbalance=" + curbalance + ", desc=" + desc + "]";
    }
    
}
