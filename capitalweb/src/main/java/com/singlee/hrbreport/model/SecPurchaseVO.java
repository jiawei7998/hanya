package com.singlee.hrbreport.model;

public class SecPurchaseVO {
    
    private String cust;//申购机构
    private String mdate;//到期日
    private String vdate;//起息日
    private Double amount;//成交量(亿元)
    private String fundInd;//申购机构是否是货币基金
    public String getCust() {
        return cust;
    }
    public void setCust(String cust) {
        this.cust = cust;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public String getFundInd() {
        return fundInd;
    }
    public void setFundInd(String fundInd) {
        this.fundInd = fundInd;
    }
    @Override
    public String toString() {
        return "SecPurchaseVO [cust=" + cust + ", mdate=" + mdate + ", vdate=" + vdate + ", amount=" + amount
                + ", fundInd=" + fundInd + "]";
    }
}
