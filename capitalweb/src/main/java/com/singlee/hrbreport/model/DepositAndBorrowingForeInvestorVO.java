package com.singlee.hrbreport.model;

/**
 * _存款含银行同业和联行存放负债-境外机构存款表
 * @author zk
 *
 */
public class DepositAndBorrowingForeInvestorVO {
    String dataNo;//数据自编码
    String orgNo;//填报机构代码
    String repMonth;//报告期
    String cndeNo;//外债编号
    String accroutst;//上月末应付利息余额
    String amount;//本月末本金余额其中剩余期限在一年及以下
    String effordamt;//本月末应付利息余额
    String balanceAmt;//本月净发生额
    String effordedamt;//本月利息支出
    String desc;//备注
    String coun;//国别
    String ccy;//币种
    String accountno;
    public String getDataNo() {
        return dataNo;
    }
    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }
    public String getOrgNo() {
        return orgNo;
    }
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }
    public String getRepMonth() {
        return repMonth;
    }
    public void setRepMonth(String repMonth) {
        this.repMonth = repMonth;
    }
    public String getCndeNo() {
        return cndeNo;
    }
    public void setCndeNo(String cndeNo) {
        this.cndeNo = cndeNo;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
    public String getEffordamt() {
        return effordamt;
    }
    public void setEffordamt(String effordamt) {
        this.effordamt = effordamt;
    }
    public String getBalanceAmt() {
        return balanceAmt;
    }
    public void setBalanceAmt(String balanceAmt) {
        this.balanceAmt = balanceAmt;
    }
    public String getEffordedamt() {
        return effordedamt;
    }
    public void setEffordedamt(String effordedamt) {
        this.effordedamt = effordedamt;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getAccountno() {
        return accountno;
    }
    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }
    
}
