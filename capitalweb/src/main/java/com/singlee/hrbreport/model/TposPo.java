package com.singlee.hrbreport.model;

import java.io.Serializable;

public class TposPo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String invType;// 会计账户类型
	private String sd;// 债券类型
	private String issuer;// 债券发行人
	private String cost;// 成本中心
	private String secId;// 债券代码
	private String port;// 投资组合
	private String jiuQi;// 久期
	private String pvbpx;// PVBP值
	private String pv01x;// PV01值
	private String varx;// VAR值
	private String fxzl;// 债券发行总量(万)
	private String sshy;// 债券所属行业
	private String ztLevel;// 最新主体评级
	private String zxLevel;// 最新债项评级
	private String fxZtLevel;// 发行主体评级
	private String fxZxLevel;// 发行债项评级
	private String sName;// 债券名称
	private String vDate;// 起息日
	private String tenor;// 期限
	private String mDate;// 到期日
	private String prinAmt;// 债券面值

	private String coupRate;// 票面利率
	private String acctngType;// 持仓类别
	private String cccb;// 持仓成本(元)
	private String yslx;// 应收利息(元)
	private String zmjz;// 账面价值(元)
	private String yjlx;// 应计利息(元)
	private String dnljlxsr;// 计算期利息收入(元)
	private String syljlxsr;// 持有期利息收入(元)
	private String tdymtm;// 公允价值变动(元)
	private String tjpmtm;// 公允价值变动损益(元)
	private String avgCost;// 债券买入成本
	private String settAvgCost;// 债券投资成本
	private String gz;// 估值
	private String opicsJjsz;// 账面净价市值(元)
	private String jjsz;// 净价市值(元)
	private String yzj;// OPICS_溢折价(元）
	private String windYzj;// 溢折价(元）
	private String lxtz;// 利息调整(元)
	private String syl;// 到期收益率
	private String jqcbPer;// 百元加权成本

	private String jjcbPer;// 百元净价成本
	private String yslxPer;// 百元应收利息
	private String yjlxPer;// 百元应计利息
	private String tyjj;// 摊余净价(元)
	private String jjfy;// OPICS_净价浮赢(元)
	private String windJjfy;// 净价浮赢(元)
	private String synx;// 剩余年限
	private String fxSyts;// 付息剩余天数
	private String cdjr;// 最近利率重定价日
	private String nx;// 年限
	private String fxrq;// 发行日期
	private String fxl;// 付息类
	private String fxpl;// 付息频率
	private String fdlx;// 浮动类型
	private String jzll;// 基准利率
	private String fdjd;// 浮动基点
	private String lc;// 利差

	private String hqz;// 含权类
	private String ifName;// 发行人
	private String ptz;// 是否平台债
	private String db;// 是否担保
	private String dbfs;// 担保方式
	private String dbName;// 担保人
	private String rbaMount;// 买断抵押面额(元)
	private String rpaMount;// 质押面额(元)
	private String rdaMount;// 国库定期存款质押面额
	private String me;// 可用面额(元)
	private String bz;// 备注

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getSd() {
		return sd;
	}

	public void setSd(String sd) {
		this.sd = sd;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getSecId() {
		return secId;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getJiuQi() {
		return jiuQi;
	}

	public void setJiuQi(String jiuQi) {
		this.jiuQi = jiuQi;
	}

	public String getPvbpx() {
		return pvbpx;
	}

	public void setPvbpx(String pvbpx) {
		this.pvbpx = pvbpx;
	}

	public String getPv01x() {
		return pv01x;
	}

	public void setPv01x(String pv01x) {
		this.pv01x = pv01x;
	}

	public String getVarx() {
		return varx;
	}

	public void setVarx(String varx) {
		this.varx = varx;
	}

	public String getFxzl() {
		return fxzl;
	}

	public void setFxzl(String fxzl) {
		this.fxzl = fxzl;
	}

	public String getSshy() {
		return sshy;
	}

	public void setSshy(String sshy) {
		this.sshy = sshy;
	}

	public String getZtLevel() {
		return ztLevel;
	}

	public void setZtLevel(String ztLevel) {
		this.ztLevel = ztLevel;
	}

	public String getZxLevel() {
		return zxLevel;
	}

	public void setZxLevel(String zxLevel) {
		this.zxLevel = zxLevel;
	}

	public String getFxZtLevel() {
		return fxZtLevel;
	}

	public void setFxZtLevel(String fxZtLevel) {
		this.fxZtLevel = fxZtLevel;
	}

	public String getFxZxLevel() {
		return fxZxLevel;
	}

	public void setFxZxLevel(String fxZxLevel) {
		this.fxZxLevel = fxZxLevel;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getPrinAmt() {
		return prinAmt;
	}

	public void setPrinAmt(String prinAmt) {
		this.prinAmt = prinAmt;
	}

	public String getCoupRate() {
		return coupRate;
	}

	public void setCoupRate(String coupRate) {
		this.coupRate = coupRate;
	}

	public String getAcctngType() {
		return acctngType;
	}

	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}

	public String getCccb() {
		return cccb;
	}

	public void setCccb(String cccb) {
		this.cccb = cccb;
	}

	public String getYslx() {
		return yslx;
	}

	public void setYslx(String yslx) {
		this.yslx = yslx;
	}

	public String getZmjz() {
		return zmjz;
	}

	public void setZmjz(String zmjz) {
		this.zmjz = zmjz;
	}

	public String getYjlx() {
		return yjlx;
	}

	public void setYjlx(String yjlx) {
		this.yjlx = yjlx;
	}

	public String getDnljlxsr() {
		return dnljlxsr;
	}

	public void setDnljlxsr(String dnljlxsr) {
		this.dnljlxsr = dnljlxsr;
	}

	public String getSyljlxsr() {
		return syljlxsr;
	}

	public void setSyljlxsr(String syljlxsr) {
		this.syljlxsr = syljlxsr;
	}

	public String getTdymtm() {
		return tdymtm;
	}

	public void setTdymtm(String tdymtm) {
		this.tdymtm = tdymtm;
	}

	public String getTjpmtm() {
		return tjpmtm;
	}

	public void setTjpmtm(String tjpmtm) {
		this.tjpmtm = tjpmtm;
	}

	public String getAvgCost() {
		return avgCost;
	}

	public void setAvgCost(String avgCost) {
		this.avgCost = avgCost;
	}

	public String getSettAvgCost() {
		return settAvgCost;
	}

	public void setSettAvgCost(String settAvgCost) {
		this.settAvgCost = settAvgCost;
	}

	public String getGz() {
		return gz;
	}

	public void setGz(String gz) {
		this.gz = gz;
	}

	public String getOpicsJjsz() {
		return opicsJjsz;
	}

	public void setOpicsJjsz(String opicsJjsz) {
		this.opicsJjsz = opicsJjsz;
	}

	public String getJjsz() {
		return jjsz;
	}

	public void setJjsz(String jjsz) {
		this.jjsz = jjsz;
	}

	public String getYzj() {
		return yzj;
	}

	public void setYzj(String yzj) {
		this.yzj = yzj;
	}

	public String getWindYzj() {
		return windYzj;
	}

	public void setWindYzj(String windYzj) {
		this.windYzj = windYzj;
	}

	public String getLxtz() {
		return lxtz;
	}

	public void setLxtz(String lxtz) {
		this.lxtz = lxtz;
	}

	public String getSyl() {
		return syl;
	}

	public void setSyl(String syl) {
		this.syl = syl;
	}

	public String getJqcbPer() {
		return jqcbPer;
	}

	public void setJqcbPer(String jqcbPer) {
		this.jqcbPer = jqcbPer;
	}

	public String getJjcbPer() {
		return jjcbPer;
	}

	public void setJjcbPer(String jjcbPer) {
		this.jjcbPer = jjcbPer;
	}

	public String getYslxPer() {
		return yslxPer;
	}

	public void setYslxPer(String yslxPer) {
		this.yslxPer = yslxPer;
	}

	public String getYjlxPer() {
		return yjlxPer;
	}

	public void setYjlxPer(String yjlxPer) {
		this.yjlxPer = yjlxPer;
	}

	public String getTyjj() {
		return tyjj;
	}

	public void setTyjj(String tyjj) {
		this.tyjj = tyjj;
	}

	public String getJjfy() {
		return jjfy;
	}

	public void setJjfy(String jjfy) {
		this.jjfy = jjfy;
	}

	public String getWindJjfy() {
		return windJjfy;
	}

	public void setWindJjfy(String windJjfy) {
		this.windJjfy = windJjfy;
	}

	public String getSynx() {
		return synx;
	}

	public void setSynx(String synx) {
		this.synx = synx;
	}

	public String getFxSyts() {
		return fxSyts;
	}

	public void setFxSyts(String fxSyts) {
		this.fxSyts = fxSyts;
	}

	public String getCdjr() {
		return cdjr;
	}

	public void setCdjr(String cdjr) {
		this.cdjr = cdjr;
	}

	public String getNx() {
		return nx;
	}

	public void setNx(String nx) {
		this.nx = nx;
	}

	public String getFxrq() {
		return fxrq;
	}

	public void setFxrq(String fxrq) {
		this.fxrq = fxrq;
	}

	public String getFxl() {
		return fxl;
	}

	public void setFxl(String fxl) {
		this.fxl = fxl;
	}

	public String getFxpl() {
		return fxpl;
	}

	public void setFxpl(String fxpl) {
		this.fxpl = fxpl;
	}

	public String getFdlx() {
		return fdlx;
	}

	public void setFdlx(String fdlx) {
		this.fdlx = fdlx;
	}

	public String getJzll() {
		return jzll;
	}

	public void setJzll(String jzll) {
		this.jzll = jzll;
	}

	public String getFdjd() {
		return fdjd;
	}

	public void setFdjd(String fdjd) {
		this.fdjd = fdjd;
	}

	public String getLc() {
		return lc;
	}

	public void setLc(String lc) {
		this.lc = lc;
	}

	public String getHqz() {
		return hqz;
	}

	public void setHqz(String hqz) {
		this.hqz = hqz;
	}

	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

	public String getPtz() {
		return ptz;
	}

	public void setPtz(String ptz) {
		this.ptz = ptz;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getDbfs() {
		return dbfs;
	}

	public void setDbfs(String dbfs) {
		this.dbfs = dbfs;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getRbaMount() {
		return rbaMount;
	}

	public void setRbaMount(String rbaMount) {
		this.rbaMount = rbaMount;
	}

	public String getRpaMount() {
		return rpaMount;
	}

	public void setRpaMount(String rpaMount) {
		this.rpaMount = rpaMount;
	}

	public String getRdaMount() {
		return rdaMount;
	}

	public void setRdaMount(String rdaMount) {
		this.rdaMount = rdaMount;
	}

	public String getMe() {
		return me;
	}

	public void setMe(String me) {
		this.me = me;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

}
