package com.singlee.hrbreport.model;

import java.math.BigDecimal;

public class CapitalTurnoverVO {

    private String      product;
    private BigDecimal  amt      ;
    private BigDecimal  tenor_1m ;
    private BigDecimal  tenor_2m ;
    private BigDecimal  tenor_3m ;
    private BigDecimal  tenor_4m ;
    private BigDecimal  tenor_5m ;
    private BigDecimal  tenor_6m ;
    private BigDecimal  tenor_7m ;
    private BigDecimal  tenor_8m ;
    private BigDecimal  tenor_9m ;
    private BigDecimal  tenor_10m;
    private BigDecimal  tenor_11m;
    private BigDecimal  tenor_12m;
    private BigDecimal  bzsye    ;
    private BigDecimal  xztfe    ;
    private BigDecimal  xzdqhse  ;
    private BigDecimal  xzjz     ;
    private BigDecimal  xzsye    ;
    private BigDecimal  bzsjtf   ;
    private BigDecimal  bzsjdqhs ;
    private BigDecimal  bzsjjz   ;
    private BigDecimal  szsye    ;
    private BigDecimal  ybdbztf  ;
    private BigDecimal  ybdbzdqhs;
    private BigDecimal  ybdbzjz  ;
    private BigDecimal  tfcy     ;
    private BigDecimal  hscy;
    private BigDecimal  jzcy;
    private String      cyyy;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getTenor_1m() {
        return tenor_1m;
    }

    public void setTenor_1m(BigDecimal tenor_1m) {
        this.tenor_1m = tenor_1m;
    }

    public BigDecimal getTenor_2m() {
        return tenor_2m;
    }

    public void setTenor_2m(BigDecimal tenor_2m) {
        this.tenor_2m = tenor_2m;
    }

    public BigDecimal getTenor_3m() {
        return tenor_3m;
    }

    public void setTenor_3m(BigDecimal tenor_3m) {
        this.tenor_3m = tenor_3m;
    }

    public BigDecimal getTenor_4m() {
        return tenor_4m;
    }

    public void setTenor_4m(BigDecimal tenor_4m) {
        this.tenor_4m = tenor_4m;
    }

    public BigDecimal getTenor_5m() {
        return tenor_5m;
    }

    public void setTenor_5m(BigDecimal tenor_5m) {
        this.tenor_5m = tenor_5m;
    }

    public BigDecimal getTenor_6m() {
        return tenor_6m;
    }

    public void setTenor_6m(BigDecimal tenor_6m) {
        this.tenor_6m = tenor_6m;
    }

    public BigDecimal getTenor_7m() {
        return tenor_7m;
    }

    public void setTenor_7m(BigDecimal tenor_7m) {
        this.tenor_7m = tenor_7m;
    }

    public BigDecimal getTenor_8m() {
        return tenor_8m;
    }

    public void setTenor_8m(BigDecimal tenor_8m) {
        this.tenor_8m = tenor_8m;
    }

    public BigDecimal getTenor_9m() {
        return tenor_9m;
    }

    public void setTenor_9m(BigDecimal tenor_9m) {
        this.tenor_9m = tenor_9m;
    }

    public BigDecimal getTenor_10m() {
        return tenor_10m;
    }

    public void setTenor_10m(BigDecimal tenor_10m) {
        this.tenor_10m = tenor_10m;
    }

    public BigDecimal getTenor_11m() {
        return tenor_11m;
    }

    public void setTenor_11m(BigDecimal tenor_11m) {
        this.tenor_11m = tenor_11m;
    }

    public BigDecimal getTenor_12m() {
        return tenor_12m;
    }

    public void setTenor_12m(BigDecimal tenor_12m) {
        this.tenor_12m = tenor_12m;
    }

    public BigDecimal getBzsye() {
        return bzsye;
    }

    public void setBzsye(BigDecimal bzsye) {
        this.bzsye = bzsye;
    }

    public BigDecimal getXztfe() {
        return xztfe;
    }

    public void setXztfe(BigDecimal xztfe) {
        this.xztfe = xztfe;
    }

    public BigDecimal getXzdqhse() {
        return xzdqhse;
    }

    public void setXzdqhse(BigDecimal xzdqhse) {
        this.xzdqhse = xzdqhse;
    }

    public BigDecimal getXzjz() {
        return xzjz;
    }

    public void setXzjz(BigDecimal xzjz) {
        this.xzjz = xzjz;
    }

    public BigDecimal getXzsye() {
        return xzsye;
    }

    public void setXzsye(BigDecimal xzsye) {
        this.xzsye = xzsye;
    }

    public BigDecimal getBzsjtf() {
        return bzsjtf;
    }

    public void setBzsjtf(BigDecimal bzsjtf) {
        this.bzsjtf = bzsjtf;
    }

    public BigDecimal getBzsjdqhs() {
        return bzsjdqhs;
    }

    public void setBzsjdqhs(BigDecimal bzsjdqhs) {
        this.bzsjdqhs = bzsjdqhs;
    }

    public BigDecimal getBzsjjz() {
        return bzsjjz;
    }

    public void setBzsjjz(BigDecimal bzsjjz) {
        this.bzsjjz = bzsjjz;
    }

    public BigDecimal getSzsye() {
        return szsye;
    }

    public void setSzsye(BigDecimal szsye) {
        this.szsye = szsye;
    }

    public BigDecimal getYbdbztf() {
        return ybdbztf;
    }

    public void setYbdbztf(BigDecimal ybdbztf) {
        this.ybdbztf = ybdbztf;
    }

    public BigDecimal getYbdbzdqhs() {
        return ybdbzdqhs;
    }

    public void setYbdbzdqhs(BigDecimal ybdbzdqhs) {
        this.ybdbzdqhs = ybdbzdqhs;
    }

    public BigDecimal getYbdbzjz() {
        return ybdbzjz;
    }

    public void setYbdbzjz(BigDecimal ybdbzjz) {
        this.ybdbzjz = ybdbzjz;
    }

    public BigDecimal getTfcy() {
        return tfcy;
    }

    public void setTfcy(BigDecimal tfcy) {
        this.tfcy = tfcy;
    }

    public BigDecimal getHscy() {
        return hscy;
    }

    public void setHscy(BigDecimal hscy) {
        this.hscy = hscy;
    }

    public BigDecimal getJzcy() {
        return jzcy;
    }

    public void setJzcy(BigDecimal jzcy) {
        this.jzcy = jzcy;
    }

    public String getCyyy() {
        return cyyy;
    }

    public void setCyyy(String cyyy) {
        this.cyyy = cyyy;
    }


}
