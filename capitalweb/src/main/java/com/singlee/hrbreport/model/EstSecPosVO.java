package com.singlee.hrbreport.model;
/**
 * _其他形式流入房地产
 * @author zk
 */
public class EstSecPosVO {
    private String secName; //债券名称
    private String cust; //交易对手
    private String rate; //利息率
    private String vdate; //起息日
    private String mdate; //到期日
    private String balance; //账面余额
    public String getSecName() {
        return secName;
    }
    public void setSecName(String secName) {
        this.secName = secName;
    }
    public String getCust() {
        return cust;
    }
    public void setCust(String cust) {
        this.cust = cust;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
}
