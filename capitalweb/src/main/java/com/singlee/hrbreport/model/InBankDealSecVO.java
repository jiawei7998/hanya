package com.singlee.hrbreport.model;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public class InBankDealSecVO {
    private String secid; //债券代码
    private String secName; //债券名称
    private String vdate; //起息日
    private String days; //期限
    private String mdate; //到期日
    private double faceAmt; //债券面值
    private String rate; //票面利率
    private String pledgeInd; //是否质押
    private String invtype; //债券类型
    public String getSecid() {
        return secid;
    }
    public void setSecid(String secid) {
        this.secid = secid;
    }
    public String getSecName() {
        return secName;
    }
    public void setSecName(String secName) {
        this.secName = secName;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getDays() {
        return days;
    }
    public void setDays(String days) {
        this.days = days;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public double getFaceAmt() {
        return faceAmt;
    }
    public void setFaceAmt(double faceAmt) {
        this.faceAmt = faceAmt;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public String getPledgeInd() {
        return pledgeInd;
    }
    public void setPledgeInd(String pledgeInd) {
        this.pledgeInd = pledgeInd;
    }
    public String getInvtype() {
        return invtype;
    }
    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }
    @Override
    public String toString() {
        return "InBankDealSecVO [secid=" + secid + ", secName=" + secName + ", vdate=" + vdate + ", days=" + days
                + ", mdate=" + mdate + ", faceAmt=" + faceAmt + ", rate=" + rate + ", pledgeInd=" + pledgeInd
                + ", invtype=" + invtype + "]";
    }
}
