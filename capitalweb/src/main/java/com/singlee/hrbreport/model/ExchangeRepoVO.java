package com.singlee.hrbreport.model;

/**
 * _交易所回购
 * @author zk
 */
public class ExchangeRepoVO {
    
    private String secName;//证券名称
    private String vdate;//首期结算日
    private String tenor;//期限
    private String matdate;//到期结算日
    private String comProcdAmt;//首期结算额
    private String rate;//利率
    private String matProcdAmt;//到期结算额
    private String cname;//交易对手
    public String getSecName() {
        return secName;
    }
    public void setSecName(String secName) {
        this.secName = secName;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMatdate() {
        return matdate;
    }
    public void setMatdate(String matdate) {
        this.matdate = matdate;
    }
    public String getComProcdAmt() {
        return comProcdAmt;
    }
    public void setComProcdAmt(String comProcdAmt) {
        this.comProcdAmt = comProcdAmt;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public String getMatProcdAmt() {
        return matProcdAmt;
    }
    public void setMatProcdAmt(String matProcdAmt) {
        this.matProcdAmt = matProcdAmt;
    }
    public String getCname() {
        return cname;
    }
    public void setCname(String cname) {
        this.cname = cname;
    }
    public String getTenor() {
        return tenor;
    }
    public void setTenor(String tenor) {
        this.tenor = tenor;
    }
    @Override
    public String toString() {
        return "ExchangeRepoVO [secName=" + secName + ", vdate=" + vdate + ", tenor=" + tenor + ", matdate=" + matdate
                + ", comProcdAmt=" + comProcdAmt + ", rate=" + rate + ", matProcdAmt=" + matProcdAmt + ", cname="
                + cname + "]";
    }
}

