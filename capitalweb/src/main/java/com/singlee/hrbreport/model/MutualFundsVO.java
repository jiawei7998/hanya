package com.singlee.hrbreport.model;

public class MutualFundsVO {
    
    private String manager;//基金管理人
    private String fundName;//基金简称
    private String fundType;//基金类型
    private String fundCode;//基金代码
    private String tradeWay;//交易方向
    private Double balance;//余额(万元)
    private String vdate;//申购日
    public String getManager() {
        return manager;
    }
    public void setManager(String manager) {
        this.manager = manager;
    }
    public String getFundType() {
        return fundType;
    }
    public void setFundType(String fundType) {
        this.fundType = fundType;
    }
    public String getFundCode() {
        return fundCode;
    }
    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }
    public String getTradeWay() {
        return tradeWay;
    }
    public void setTradeWay(String tradeWay) {
        this.tradeWay = tradeWay;
    }
    public Double getBalance() {
        return balance;
    }
    public void setBalance(Double balance) {
        this.balance = balance;
    }
    public String getFundName() {
        return fundName;
    }
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    @Override
    public String toString() {
        return "MutualFundsVO [manager=" + manager + ", fundName=" + fundName + ", fundType=" + fundType + ", fundCode="
                + fundCode + ", tradeWay=" + tradeWay + ", balance=" + balance + ", vdate=" + vdate + "]";
    }

}
