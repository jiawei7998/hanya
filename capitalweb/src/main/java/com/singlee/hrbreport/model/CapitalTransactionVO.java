package com.singlee.hrbreport.model;


public class CapitalTransactionVO {
  
    private String br;//银行机构代码
    private String br2;//内部机构号
    private String banksn;//银行机构名称
    private String dealno;//交易编号
    private String type;//资金交易类型
    private String acctngtype;//帐户类型
    private String ccy;//币种
    private String trad;//交易柜员
    private String voper;//审核人
    private String cno;//交易对手代码
    private String sn;//交易对手名称
    private String dealdate;//交易日期
    private String vdate;//起始日期
    private String mdate;//到期日期
    private String ps;//买卖标志
    private String spotfwdind;//即远期标志
    private String ccy2;//买入币种
    private String ccyamt;//买入金额
    private String ctrccy;//卖出币种
    private String ctramt;//卖出金额
    private String verdate;//复核日期
    private String revdate;//取消日期
    private String ccysettdate;//实际交割日期
    private String netsi;//清算标志
//    private String department;//借方账号
//    private String department;//贷方账号
//    private String department;//借方金额
//    private String department;//贷方金额
//    private String department;//借方币种
//    private String department;//贷方币种
//    private String department;//借方利率 
//    private String department;//贷方利率 
    public String getBr() {
        return br;
    }
    public void setBr(String br) {
        this.br = br;
    }
    public String getBr2() {
        return br2;
    }
    public void setBr2(String br2) {
        this.br2 = br2;
    }
    public String getBanksn() {
        return banksn;
    }
    public void setBanksn(String banksn) {
        this.banksn = banksn;
    }
    public String getDealno() {
        return dealno;
    }
    public void setDealno(String dealno) {
        this.dealno = dealno;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getAcctngtype() {
        return acctngtype;
    }
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getTrad() {
        return trad;
    }
    public void setTrad(String trad) {
        this.trad = trad;
    }
    public String getVoper() {
        return voper;
    }
    public void setVoper(String voper) {
        this.voper = voper;
    }
    public String getCno() {
        return cno;
    }
    public void setCno(String cno) {
        this.cno = cno;
    }
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getDealdate() {
        return dealdate;
    }
    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getPs() {
        return ps;
    }
    public void setPs(String ps) {
        this.ps = ps;
    }
    public String getSpotfwdind() {
        return spotfwdind;
    }
    public void setSpotfwdind(String spotfwdind) {
        this.spotfwdind = spotfwdind;
    }
    public String getCcy2() {
        return ccy2;
    }
    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2;
    }
    public String getCcyamt() {
        return ccyamt;
    }
    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }
    public String getCtrccy() {
        return ctrccy;
    }
    public void setCtrccy(String ctrccy) {
        this.ctrccy = ctrccy;
    }
    public String getCtramt() {
        return ctramt;
    }
    public void setCtramt(String ctramt) {
        this.ctramt = ctramt;
    }
    public String getVerdate() {
        return verdate;
    }
    public void setVerdate(String verdate) {
        this.verdate = verdate;
    }
    public String getRevdate() {
        return revdate;
    }
    public void setRevdate(String revdate) {
        this.revdate = revdate;
    }
    public String getCcysettdate() {
        return ccysettdate;
    }
    public void setCcysettdate(String ccysettdate) {
        this.ccysettdate = ccysettdate;
    }
    public String getNetsi() {
        return netsi;
    }
    public void setNetsi(String netsi) {
        this.netsi = netsi;
    }
    @Override
    public String toString() {
        return "CapitalTransactionVO [br=" + br + ", br2=" + br2 + ", banksn=" + banksn + ", dealno=" + dealno
                + ", type=" + type + ", acctngtype=" + acctngtype + ", ccy=" + ccy + ", trad=" + trad + ", voper="
                + voper + ", cno=" + cno + ", sn=" + sn + ", dealdate=" + dealdate + ", vdate=" + vdate + ", mdate="
                + mdate + ", ps=" + ps + ", spotfwdind=" + spotfwdind + ", ccy2=" + ccy2 + ", ccyamt=" + ccyamt
                + ", ctrccy=" + ctrccy + ", ctramt=" + ctramt + ", verdate=" + verdate + ", revdate=" + revdate
                + ", ccysettdate=" + ccysettdate + ", netsi=" + netsi + "]";
    }

   
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
