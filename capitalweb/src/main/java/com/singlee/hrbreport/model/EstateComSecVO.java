package com.singlee.hrbreport.model;

/**
 * _房地产融资风险检测表
 * @author zk
 */
public class EstateComSecVO {
    private  String     balance ; //    本期余额        private String balance; //本期余额
    private  String     lastBalance ; //    上期余额        private String lastBalance; //上期余额
    private  String     overdueAmt  ; //    逾期余额        private String overdueAmt; //逾期余额
    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
    public String getLastBalance() {
        return lastBalance;
    }
    public void setLastBalance(String lastBalance) {
        this.lastBalance = lastBalance;
    }
    public String getOverdueAmt() {
        return overdueAmt;
    }
    public void setOverdueAmt(String overdueAmt) {
        this.overdueAmt = overdueAmt;
    }
    @Override
    public String toString() {
        return "EstateComSecVO [balance=" + balance + ", lastBalance=" + lastBalance + ", overdueAmt=" + overdueAmt
                + "]";
    }
}
