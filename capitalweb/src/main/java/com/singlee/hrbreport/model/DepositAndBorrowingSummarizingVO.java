package com.singlee.hrbreport.model;

/**
 * _存款含银行同业和联行存放负债-汇总表
 * @author zk
 *
 */
public class DepositAndBorrowingSummarizingVO {
    String coun;//国别
    String ccy;//币种
    String repMonth;//报告期
    String accroutst;//上月末应付利息余额
    String amount;//本月末本金余额
    String effordamt;//本月末应付利息余额
    String balanceAmt;//本月净发生额
    String effordedAmt;//本月利息支出
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getRepMonth() {
        return repMonth;
    }
    public void setRepMonth(String repMonth) {
        this.repMonth = repMonth;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
    public String getEffordamt() {
        return effordamt;
    }
    public void setEffordamt(String effordamt) {
        this.effordamt = effordamt;
    }
    public String getBalanceAmt() {
        return balanceAmt;
    }
    public void setBalanceAmt(String balanceAmt) {
        this.balanceAmt = balanceAmt;
    }
    public String getEffordedAmt() {
        return effordedAmt;
    }
    public void setEffordedAmt(String effordedAmt) {
        this.effordedAmt = effordedAmt;
    }
    @Override
    public String toString() {
        return "DepositAndBorrowingSummarizingVO [coun=" + coun + ", ccy=" + ccy + ", repMonth=" + repMonth
                + ", accroutst=" + accroutst + ", amount=" + amount + ", effordamt=" + effordamt + ", balanceAmt="
                + balanceAmt + ", effordedAmt=" + effordedAmt + "]";
    }
}
