package com.singlee.hrbreport.model;

import java.io.Serializable;

public class RepoSecPo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String br;// 部门
	private String delNo;// 成交单号

	private String secId;// 债券IDT17 合并
	private String faceAmt;// 票面金额T18 合并
	private String deprice;// 全价T19 合并
	private String zlLevel;// 债券等级T20 合并

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDelNo() {
		return delNo;
	}

	public void setDelNo(String delNo) {
		this.delNo = delNo;
	}

	public String getSecId() {
		return secId;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

	public String getFaceAmt() {
		return faceAmt;
	}

	public void setFaceAmt(String faceAmt) {
		this.faceAmt = faceAmt;
	}

	public String getDeprice() {
		return deprice;
	}

	public void setDeprice(String deprice) {
		this.deprice = deprice;
	}

	public String getZlLevel() {
		return zlLevel;
	}

	public void setZlLevel(String zlLevel) {
		this.zlLevel = zlLevel;
	}

}
