package com.singlee.hrbreport.model;

/**
 * _贷款含拆放银行同业及联行资产表
 * @author zk
 *
 */
public class LoanIncludeMoneyMarketVO {
    private String dataNo;      //数据自编码
    private String orgNo;       //填报机构代码
    private String repMonth;    //报告期
    private String crsign;      //是否委托贷款
    private String agentDep;    //委托所属部门
    private String ctrArea;     //对方国家/地区
    private String ctrDep;      //对方部门
    private String link;        //对方与本机构/委托人关系
    private String tero;        //原始期限
    private String ccy;         //原始币种
    private String lastAmt;     //上月末本金余额
    private String lastAccroutst;//上月末应收利息余额
    private String ccyAmt;      //本月末本金余额
    private String loneYearAmt; //本月末本金余额:其中剩余期限在一年及以下
    private String accroutst;   //本月末应收利息余额
    private String changeAmt;   //本月非交易变动
    private String accroutstAmt; //本月净发生额
    private String mtdIncexp;   //本月利息收入
    private String desc;        //备注
    private String cid;         //唯一标识
    private String cno;         //交易对手编号
    
    public String getDataNo() {
        return dataNo;
    }
    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }
    public String getOrgNo() {
        return orgNo;
    }
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }
    public String getRepMonth() {
        return repMonth;
    }
    public void setRepMonth(String repMonth) {
        this.repMonth = repMonth;
    }
    public String getCrsign() {
        return crsign;
    }
    public void setCrsign(String crsign) {
        this.crsign = crsign;
    }
    public String getAgentDep() {
        return agentDep;
    }
    public void setAgentDep(String agentDep) {
        this.agentDep = agentDep;
    }
    public String getCtrArea() {
        return ctrArea;
    }
    public void setCtrArea(String ctrArea) {
        this.ctrArea = ctrArea;
    }
    public String getCtrDep() {
        return ctrDep;
    }
    public void setCtrDep(String ctrDep) {
        this.ctrDep = ctrDep;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getTero() {
        return tero;
    }
    public void setTero(String tero) {
        this.tero = tero;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getLastAmt() {
        return lastAmt;
    }
    public void setLastAmt(String lastAmt) {
        this.lastAmt = lastAmt;
    }
    public String getLastAccroutst() {
        return lastAccroutst;
    }
    public void setLastAccroutst(String lastAccroutst) {
        this.lastAccroutst = lastAccroutst;
    }
    public String getCcyAmt() {
        return ccyAmt;
    }
    public void setCcyAmt(String ccyAmt) {
        this.ccyAmt = ccyAmt;
    }
    public String getLoneYearAmt() {
        return loneYearAmt;
    }
    public void setLoneYearAmt(String loneYearAmt) {
        this.loneYearAmt = loneYearAmt;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getChangeAmt() {
        return changeAmt;
    }
    public void setChangeAmt(String changeAmt) {
        this.changeAmt = changeAmt;
    }
    public String getAccroutstAmt() {
        return accroutstAmt;
    }
    public void setAccroutstAmt(String accroutstAmt) {
        this.accroutstAmt = accroutstAmt;
    }
    public String getMtdIncexp() {
        return mtdIncexp;
    }
    public void setMtdIncexp(String mtdincexp) {
        this.mtdIncexp = mtdincexp;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getCid() {
        return cid;
    }
    public void setCid(String cid) {
        this.cid = cid;
    }
    public String getCno() {
        return cno;
    }
    public void setCno(String cno) {
        this.cno = cno;
    }
    @Override
    public String toString() {
        return "LoanIncludeMoneyMarketVO [dataNo=" + dataNo + ", orgNo=" + orgNo + ", repMonth=" + repMonth
                + ", crsign=" + crsign + ", agentDep=" + agentDep + ", ctrArea=" + ctrArea + ", ctrDep=" + ctrDep
                + ", link=" + link + ", tero=" + tero + ", CCY=" + ccy + ", lastAmt=" + lastAmt + ", lastAccroutst="
                + lastAccroutst + ", ccyAmt=" + ccyAmt + ", loneYearAmt=" + loneYearAmt + ", accroutst=" + accroutst
                + ", changeAmt=" + changeAmt + ", accroutstAmt=" + accroutstAmt + ", mtdIncexp=" + mtdIncexp + ", desc="
                + desc + ", cid=" + cid + ", cno=" + cno + "]";
    }
    
}
