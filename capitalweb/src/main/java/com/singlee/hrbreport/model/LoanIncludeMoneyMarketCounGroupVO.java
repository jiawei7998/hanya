package com.singlee.hrbreport.model;

/**
 * _贷款含拆放银行同业及联行资产按国别按币种汇总表
 * @author zk
 *
 */
public class LoanIncludeMoneyMarketCounGroupVO {
    private String coun;            //国别
    private String ccy;             //币种
    private String repMonth;        //报告期
    private String lastAmt;         //上月末本金余额
    private String lastAccroutst;   //上月末应收利息余额
    private String ccyAmt;          //本月末本金余额
    private String loneYearAmt;     //本月末本金余额:其中剩余期限在一年及以下
    private String accroutst;       //本月末应收利息余额
    private String changeAmt;       //本月非交易变动
    private String accroutstAmt;    //本月净发生额
    private String mtdIncexp;       //本月利息收入
    private String desc;            //备注
    
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getRepMonth() {
        return repMonth;
    }
    public void setRepMonth(String repMonth) {
        this.repMonth = repMonth;
    }
    public String getLastAmt() {
        return lastAmt;
    }
    public void setLastAmt(String lastAmt) {
        this.lastAmt = lastAmt;
    }
    public String getLastAccroutst() {
        return lastAccroutst;
    }
    public void setLastAccroutst(String lastAccroutst) {
        this.lastAccroutst = lastAccroutst;
    }
    public String getCcyAmt() {
        return ccyAmt;
    }
    public void setCcyAmt(String ccyAmt) {
        this.ccyAmt = ccyAmt;
    }
    public String getLoneYearAmt() {
        return loneYearAmt;
    }
    public void setLoneYearAmt(String loneYearAmt) {
        this.loneYearAmt = loneYearAmt;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getChangeAmt() {
        return changeAmt;
    }
    public void setChangeAmt(String changeAmt) {
        this.changeAmt = changeAmt;
    }
    public String getAccroutstAmt() {
        return accroutstAmt;
    }
    public void setAccroutstAmt(String accroutstAmt) {
        this.accroutstAmt = accroutstAmt;
    }
    public String getMtdIncexp() {
        return mtdIncexp;
    }
    public void setMtdIncexp(String mtdIncexp) {
        this.mtdIncexp = mtdIncexp;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    @Override
    public String toString() {
        return "LoanIncludeMoneyMarketByCCYVO [coun=" + coun + ", ccy=" + ccy + ", repMonth=" + repMonth + ", lastAmt="
                + lastAmt + ", lastAccroutst=" + lastAccroutst + ", ccyAmt=" + ccyAmt + ", loneYearAmt=" + loneYearAmt
                + ", accroutst=" + accroutst + ", changeAmt=" + changeAmt + ", accroutstAmt=" + accroutstAmt
                + ", mtdIncexp=" + mtdIncexp + ", desc=" + desc + "]";
    }
}
