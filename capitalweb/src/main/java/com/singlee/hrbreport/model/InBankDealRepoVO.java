package com.singlee.hrbreport.model;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public class InBankDealRepoVO {
    private String dealno; //交易单号
    private String ps; //交易方向
    private String vdate; //首次结算日
    private String days; //实际占款天数
    private String mdate; //到期结算日
    private String rate; //回购利率
    private String stanDays; //回购天数
    private double amt; //交易金额
    private String pledge; //质押物明细
    private String cust; //交易对手
    
    public String getPs() {
        return ps;
    }
    public void setPs(String ps) {
        this.ps = ps;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getDays() {
        return days;
    }
    public void setDays(String days) {
        this.days = days;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public String getStanDays() {
        return stanDays;
    }
    public void setStanDays(String stanDays) {
        this.stanDays = stanDays;
    }
    public double getAmt() {
        return amt;
    }
    public void setAmt(double amt) {
        this.amt = amt;
    }
    public String getPledge() {
        return pledge;
    }
    public void setPledge(String pledge) {
        this.pledge = pledge;
    }
    public String getDealno() {
        return dealno;
    }
    public void setDealno(String dealno) {
        this.dealno = dealno;
    }
    public String getCust() {
        return cust;
    }
    public void setCust(String cust) {
        this.cust = cust;
    }
    @Override
    public String toString() {
        return "InBankDealRepoVO [dealno=" + dealno + ", ps=" + ps + ", vdate=" + vdate + ", days=" + days + ", mdate="
                + mdate + ", rate=" + rate + ", stanDays=" + stanDays + ", amt=" + amt + ", pledge=" + pledge
                + ", cust=" + cust + "]";
    }
}
