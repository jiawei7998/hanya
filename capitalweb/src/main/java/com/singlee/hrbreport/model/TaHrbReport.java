package com.singlee.hrbreport.model;

import javax.persistence.*;

/**
    * 哈尔滨报表
    */
@Table(name = "TA_HRB_REPORT")
public class TaHrbReport {
    /**
     * 报表ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 报表名称
     */
    @Column(name = "REPORT_NMAE")
    private String reportNmae;

    /**
     * 报表使用机构对象
     */
    @Column(name = "REPORT_INST")
    private String reportInst;

    /**
     * 报表使用岗位对象
     */
    @Column(name = "REPORT_ROLE")
    private String reportRole;

    /**
     * 报表类型1-有list页面,0-无list页面
     */
    @Column(name = "REPORT_TYPE")
    private String reportType;

    /**
     * list页面地址
     */
    @Column(name = "MODULE_URL")
    private String moduleUrl;

    /**
     * 报表实现service
     */
    @Column(name = "REPORT_SERVER")
    private String reportServer;

    /**
     * 是否启用。1-启用，0-不启用
     */
    @Column(name = "ACTIVE_FLAG")
    private String activeFlag;

    /**
     * execl模板名称
     */
    @Column(name = "execl_name")
    private String execlName;

    /**
     * execl模板路径
     */
    @Column(name = "execl_dir")
    private String execlDir;

    /**
     * 报表编号
     */
    @Column(name="report_code")
    private String reportCode;


    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    /**
     * 获取报表ID
     *
     * @return ID - 报表ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置报表ID
     *
     * @param id 报表ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取报表名称
     *
     * @return REPORT_NMAE - 报表名称
     */
    public String getReportNmae() {
        return reportNmae;
    }

    /**
     * 设置报表名称
     *
     * @param reportNmae 报表名称
     */
    public void setReportNmae(String reportNmae) {
        this.reportNmae = reportNmae;
    }

    /**
     * 获取报表使用对象
     *
     * @return REPORT_INST - 报表使用对象
     */
    public String getReportInst() {
        return reportInst;
    }

    /**
     * 设置报表使用对象
     *
     * @param reportInst 报表使用对象
     */
    public void setReportInst(String reportInst) {
        this.reportInst = reportInst;
    }

    /**
     * 获取报表类型1-有list页面,0-无list页面
     *
     * @return REPORT_TYPE - 报表类型1-有list页面,0-无list页面
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * 设置报表类型1-有list页面,0-无list页面
     *
     * @param reportType 报表类型1-有list页面,0-无list页面
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * 获取list页面地址
     *
     * @return MODULE_URL - list页面地址
     */
    public String getModuleUrl() {
        return moduleUrl;
    }

    /**
     * 设置list页面地址
     *
     * @param moduleUrl list页面地址
     */
    public void setModuleUrl(String moduleUrl) {
        this.moduleUrl = moduleUrl;
    }

    /**
     * 获取报表实现service
     *
     * @return REPORT_SERVER - 报表实现service
     */
    public String getReportServer() {
        return reportServer;
    }

    /**
     * 设置报表实现service
     *
     * @param reportServer 报表实现service
     */
    public void setReportServer(String reportServer) {
        this.reportServer = reportServer;
    }

    /**
     * 获取是否启用。1-启用，0-不启用
     *
     * @return ACTIVE_FLAG - 是否启用。1-启用，0-不启用
     */
    public String getActiveFlag() {
        return activeFlag;
    }

    /**
     * 设置是否启用。1-启用，0-不启用
     *
     * @param activeFlag 是否启用。1-启用，0-不启用
     */
    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getExeclName() {
        return execlName;
    }

    public void setExeclName(String execlName) {
        this.execlName = execlName;
    }

    public String getExeclDir() {
        return execlDir;
    }

    public void setExeclDir(String execlDir) {
        this.execlDir = execlDir;
    }

    public String getReportRole() {
        return reportRole;
    }

    public void setReportRole(String reportRole) {
        this.reportRole = reportRole;
    }
}