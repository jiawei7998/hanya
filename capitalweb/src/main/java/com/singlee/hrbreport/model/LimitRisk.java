package com.singlee.hrbreport.model;

import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @author     ：chenguo
 * @date       ：Created in 2021/10/30 17:33
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Table(name = "LIMIT_RISK")
public class LimitRisk implements Serializable {
    private String secmType;

    private BigDecimal durationTest;

    private BigDecimal durationLimit;

    private BigDecimal bpTest;

    private BigDecimal bpLmit;

    private BigDecimal pvTest;

    private BigDecimal pvLimit;

    private static final long serialVersionUID = 1L;

    public String getSecmType() {
        return secmType;
    }

    public void setSecmType(String secmType) {
        this.secmType = secmType;
    }

    public BigDecimal getDurationTest() {
        return durationTest;
    }

    public void setDurationTest(BigDecimal durationTest) {
        this.durationTest = durationTest;
    }

    public BigDecimal getDurationLimit() {
        return durationLimit;
    }

    public void setDurationLimit(BigDecimal durationLimit) {
        this.durationLimit = durationLimit;
    }

    public BigDecimal getBpTest() {
        return bpTest;
    }

    public void setBpTest(BigDecimal bpTest) {
        this.bpTest = bpTest;
    }

    public BigDecimal getBpLmit() {
        return bpLmit;
    }

    public void setBpLmit(BigDecimal bpLmit) {
        this.bpLmit = bpLmit;
    }

    public BigDecimal getPvTest() {
        return pvTest;
    }

    public void setPvTest(BigDecimal pvTest) {
        this.pvTest = pvTest;
    }

    public BigDecimal getPvLimit() {
        return pvLimit;
    }

    public void setPvLimit(BigDecimal pvLimit) {
        this.pvLimit = pvLimit;
    }
}