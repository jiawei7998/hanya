package com.singlee.hrbreport.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RepoPo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String br;// 部门
	private String delNo;// 成交单号
	private String costDesc;// 成本中心描述 T2
	private String cname;// 客户全名T3
	private String acctDesc;// 对手属性T4
	private String acctType;// 账户类型T5
	private String descr;// 回购方式T6
	private String vDate;// 首期交割日T7
	private String term;// 期限T8
	private String matDate;// 到期交割日T9
	private String repoRate;// 回购利率T10
	private BigDecimal comProcdAmt;// 首期结算额T12
	private BigDecimal matProcdAmt;// 到期结算额T13
	private BigDecimal totaLint;// 利息支额T14
	private String dealText;// 交易摘要
	private String matDateC;// 交易计算起息日
	private String vDateC;// 交易计算到期日
	private BigDecimal occupyAmt;// 资金占用T15
	private String repoType;// 回购类型

	private BigDecimal amt;// 券面（拆借）总额T11 求和T18
	private BigDecimal handAmt;// 交易手续费T16

	/**
	 * 债券详情列表 合并成单条
	 */
	private String secId;// 债券IDT17 合并
	private String faceAmt;// 票面金额T18 合并
	private String deprice;// 全价T19 合并
	private String zlLevel;// 债券等级T20 合并

	private String overTerm;// 剩余期限T21
	private BigDecimal rpLint;// 应收（付）利息额T22
	private BigDecimal overLint;// 剩余利息额T23
	private BigDecimal proLoss;// 已实现损益T24
	private String safeKeepAcct;// 托管账户T25

	private List<RepoSecPo> repoDetails;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDelNo() {
		return delNo;
	}

	public void setDelNo(String delNo) {
		this.delNo = delNo;
	}

	public String getCostDesc() {
		return costDesc;
	}

	public void setCostDesc(String costDesc) {
		this.costDesc = costDesc;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getAcctDesc() {
		return acctDesc;
	}

	public void setAcctDesc(String acctDesc) {
		this.acctDesc = acctDesc;
	}

	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getMatDate() {
		return matDate;
	}

	public void setMatDate(String matDate) {
		this.matDate = matDate;
	}

	public String getRepoRate() {
		return repoRate;
	}

	public void setRepoRate(String repoRate) {
		this.repoRate = repoRate;
	}

	public String getDealText() {
		return dealText;
	}

	public void setDealText(String dealText) {
		this.dealText = dealText;
	}

	public String getMatDateC() {
		return matDateC;
	}

	public void setMatDateC(String matDateC) {
		this.matDateC = matDateC;
	}

	public String getvDateC() {
		return vDateC;
	}

	public void setvDateC(String vDateC) {
		this.vDateC = vDateC;
	}

	public String getRepoType() {
		return repoType;
	}

	public void setRepoType(String repoType) {
		this.repoType = repoType;
	}

	public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

	public String getSecId() {
		return secId;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

	public String getFaceAmt() {
		return faceAmt;
	}

	public void setFaceAmt(String faceAmt) {
		this.faceAmt = faceAmt;
	}

	public String getDeprice() {
		return deprice;
	}

	public void setDeprice(String deprice) {
		this.deprice = deprice;
	}

	public String getZlLevel() {
		return zlLevel;
	}

	public void setZlLevel(String zlLevel) {
		this.zlLevel = zlLevel;
	}

	public String getOverTerm() {
		return overTerm;
	}

	public void setOverTerm(String overTerm) {
		this.overTerm = overTerm;
	}

	public String getSafeKeepAcct() {
		return safeKeepAcct;
	}

	public void setSafeKeepAcct(String safeKeepAcct) {
		this.safeKeepAcct = safeKeepAcct;
	}

	public List<RepoSecPo> getRepoDetails() {
		return repoDetails;
	}

	public void setRepoDetails(List<RepoSecPo> repoDetails) {
		this.repoDetails = repoDetails;
	}

    public BigDecimal getComProcdAmt() {
        return comProcdAmt;
    }

    public void setComProcdAmt(BigDecimal comProcdAmt) {
        this.comProcdAmt = comProcdAmt;
    }

    public BigDecimal getMatProcdAmt() {
        return matProcdAmt;
    }

    public void setMatProcdAmt(BigDecimal matProcdAmt) {
        this.matProcdAmt = matProcdAmt;
    }

    public BigDecimal getTotaLint() {
        return totaLint;
    }

    public void setTotaLint(BigDecimal totaLint) {
        this.totaLint = totaLint;
    }

    public BigDecimal getOccupyAmt() {
        return occupyAmt;
    }

    public void setOccupyAmt(BigDecimal occupyAmt) {
        this.occupyAmt = occupyAmt;
    }

    public BigDecimal getHandAmt() {
        return handAmt;
    }

    public void setHandAmt(BigDecimal handAmt) {
        this.handAmt = handAmt;
    }

    public BigDecimal getRpLint() {
        return rpLint;
    }

    public void setRpLint(BigDecimal rpLint) {
        this.rpLint = rpLint;
    }

    public BigDecimal getOverLint() {
        return overLint;
    }

    public void setOverLint(BigDecimal overLint) {
        this.overLint = overLint;
    }

    public BigDecimal getProLoss() {
        return proLoss;
    }

    public void setProLoss(BigDecimal proLoss) {
        this.proLoss = proLoss;
    }

}
