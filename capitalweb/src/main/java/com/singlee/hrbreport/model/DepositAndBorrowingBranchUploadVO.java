package com.singlee.hrbreport.model;
/**
 * _存款含银行同业和联行存放负债-分行上传表
 * @author zk
 *
 */
public class DepositAndBorrowingBranchUploadVO {
    String dataNo;//数据自编码
    String orgNo;//填报机构代码
    String repMonth;//报告期
    String coun;//国别
    String ccy;//币种
    String cndeNo;//外债编号
    String accroutst;//上月末应付利息余额
    String amount;//本月本金余额
    String effordamt;//本月末应付利息余额
    String balanceamt;//本月净发生额
    String effordedamt;//本月利息支出
    String descs;//备注
    public String getDataNo() {
        return dataNo;
    }
    public void setDataNo(String dataNo) {
        this.dataNo = dataNo;
    }
    public String getOrgNo() {
        return orgNo;
    }
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }
    public String getRepMonth() {
        return repMonth;
    }
    public void setRepMonth(String repMonth) {
        this.repMonth = repMonth;
    }
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getCndeNo() {
        return cndeNo;
    }
    public void setCndeNo(String cndeNo) {
        this.cndeNo = cndeNo;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
    public String getEffordamt() {
        return effordamt;
    }
    public void setEffordamt(String effordamt) {
        this.effordamt = effordamt;
    }
    public String getBalanceamt() {
        return balanceamt;
    }
    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }
    public String getEffordedamt() {
        return effordedamt;
    }
    public void setEffordedamt(String effordedamt) {
        this.effordedamt = effordedamt;
    }
    public String getDescs() {
        return descs;
    }
    public void setDescs(String descs) {
        this.descs = descs;
    }
    @Override
    public String toString() {
        return "DepositAndBorrowingBranchUploadVO [dataNo=" + dataNo + ", orgNo=" + orgNo + ", repMonth=" + repMonth
                + ", coun=" + coun + ", ccy=" + ccy + ", cndeNo=" + cndeNo + ", accroutst=" + accroutst + ", amount="
                + amount + ", effordamt=" + effordamt + ", balanceamt=" + balanceamt + ", effordedamt=" + effordedamt
                + ", descs=" + descs + "]";
    }
}
