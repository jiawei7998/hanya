package com.singlee.hrbreport.model;

public class G14OtherVO {
    
    private String sn; //客户名称
    private String cno; //客户代码
    private double amt; //信用风险暴露
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getCno() {
        return cno;
    }
    public void setCno(String cno) {
        this.cno = cno;
    }
    public double getAmt() {
        return amt;
    }
    public void setAmt(double amt) {
        this.amt = amt;
    }
    @Override
    public String toString() {
        return "G14OtherVO [sn=" + sn + ", cno=" + cno + ", amt=" + amt + "]";
    }
    
    
}
