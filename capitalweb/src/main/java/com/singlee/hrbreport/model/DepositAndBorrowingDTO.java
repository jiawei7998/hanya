package com.singlee.hrbreport.model;
/**
 * _存款含银行同业和联行存放负债
 * @author zk
 *
 */
public class DepositAndBorrowingDTO {
    String acctNo;
    String accountNo;
    String saccroutst;
    String scurBalance;
    String faccroutst;
    String accroutst;
    String effordamt;
    String effordedamt;
    String mtdincexp;
    String fintpaid;
    String coun;
    String ccy;
    String amt;
    
    public String getAcctNo() {
        return acctNo;
    }
    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }
    public String getAccountNo() {
        return accountNo;
    }
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public String getScurBalance() {
        return scurBalance;
    }
    public void setScurBalance(String scurBalance) {
        this.scurBalance = scurBalance;
    }
    public String getFaccroutst() {
        return faccroutst;
    }
    public void setFaccroutst(String faccroutst) {
        this.faccroutst = faccroutst;
    }
    public String getSaccroutst() {
        return saccroutst;
    }
    public void setSaccroutst(String saccroutst) {
        this.saccroutst = saccroutst;
    }
    public String getAccroutst() {
        return accroutst;
    }
    public void setAccroutst(String accroutst) {
        this.accroutst = accroutst;
    }
    public String getEffordamt() {
        return effordamt;
    }
    public void setEffordamt(String effordamt) {
        this.effordamt = effordamt;
    }
    public String getEffordedamt() {
        return effordedamt;
    }
    public void setEffordedamt(String effordedamt) {
        this.effordedamt = effordedamt;
    }
    public String getMtdincexp() {
        return mtdincexp;
    }
    public void setMtdincexp(String mtdincexp) {
        this.mtdincexp = mtdincexp;
    }
    public String getFintpaid() {
        return fintpaid;
    }
    public void setFintpaid(String fintpaid) {
        this.fintpaid = fintpaid;
    }
    public String getCoun() {
        return coun;
    }
    public void setCoun(String coun) {
        this.coun = coun;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getAmt() {
        return amt;
    }
    public void setAmt(String amt) {
        this.amt = amt;
    }
    
}
