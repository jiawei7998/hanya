package com.singlee.hrbreport.model;

public class PCapitalTransactionVO {
    
    
    private String sysday;//采集日期
    private String startdate;//起始日期
    private String enddate;//到期日期
    private String br;//银行机构代码
    private String br2;//内部机构号
    private String banksn;//银行机构名称
    private String dealno;//交易编号
    private String acctngtype;//帐户类型
    private String ccy;//币种
    private String trad;//交易柜员
    private String voper;//审核人
    private String cno;//交易对手代码
    private String sn;//交易对手名称
    private String dealdate;//交易日期
    private String dealtime;//交易时间
    private String ps;//交易时间
    private String ccyamt;//成交金额
    private String vdate;//起息日
    private String mdate;//到息日
    private String verdate;//复核日期
    private String revdate;//取消日期
    private String ccysettdate;//实际交割日期
    private String netsi;//清算标志
    private String prodtype;//银行产品
    private String ccode;//交易对手国家
    private String sd;//交易对手行业
    private String intrate;//利率
    public String getSysday() {
        return sysday;
    }
    public void setSysday(String sysday) {
        this.sysday = sysday;
    }
    public String getStartdate() {
        return startdate;
    }
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }
    public String getEnddate() {
        return enddate;
    }
    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }
    public String getBr() {
        return br;
    }
    public void setBr(String br) {
        this.br = br;
    }
    public String getBr2() {
        return br2;
    }
    public void setBr2(String br2) {
        this.br2 = br2;
    }
    public String getBanksn() {
        return banksn;
    }
    public void setBanksn(String banksn) {
        this.banksn = banksn;
    }
    public String getDealno() {
        return dealno;
    }
    public void setDealno(String dealno) {
        this.dealno = dealno;
    }
    public String getAcctngtype() {
        return acctngtype;
    }
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }
    public String getCcy() {
        return ccy;
    }
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }
    public String getTrad() {
        return trad;
    }
    public void setTrad(String trad) {
        this.trad = trad;
    }
    public String getVoper() {
        return voper;
    }
    public void setVoper(String voper) {
        this.voper = voper;
    }
    public String getCno() {
        return cno;
    }
    public void setCno(String cno) {
        this.cno = cno;
    }
    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getDealdate() {
        return dealdate;
    }
    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }
    public String getDealtime() {
        return dealtime;
    }
    public void setDealtime(String dealtime) {
        this.dealtime = dealtime;
    }
    public String getPs() {
        return ps;
    }
    public void setPs(String ps) {
        this.ps = ps;
    }
    public String getCcyamt() {
        return ccyamt;
    }
    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getVerdate() {
        return verdate;
    }
    public void setVerdate(String verdate) {
        this.verdate = verdate;
    }
    public String getRevdate() {
        return revdate;
    }
    public void setRevdate(String revdate) {
        this.revdate = revdate;
    }
    public String getCcysettdate() {
        return ccysettdate;
    }
    public void setCcysettdate(String ccysettdate) {
        this.ccysettdate = ccysettdate;
    }
    public String getNetsi() {
        return netsi;
    }
    public void setNetsi(String netsi) {
        this.netsi = netsi;
    }
    public String getProdtype() {
        return prodtype;
    }
    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }
    public String getCcode() {
        return ccode;
    }
    public void setCcode(String ccode) {
        this.ccode = ccode;
    }
    public String getSd() {
        return sd;
    }
    public void setSd(String sd) {
        this.sd = sd;
    }
    public String getIntrate() {
        return intrate;
    }
    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }
    @Override
    public String toString() {
        return "PCapitalTransactionVO [sysday=" + sysday + ", startdate=" + startdate + ", enddate=" + enddate + ", br="
                + br + ", br2=" + br2 + ", banksn=" + banksn + ", dealno=" + dealno + ", acctngtype=" + acctngtype
                + ", ccy=" + ccy + ", trad=" + trad + ", voper=" + voper + ", cno=" + cno + ", sn=" + sn + ", dealdate="
                + dealdate + ", dealtime=" + dealtime + ", ps=" + ps + ", ccyamt=" + ccyamt + ", vdate=" + vdate
                + ", mdate=" + mdate + ", verdate=" + verdate + ", revdate=" + revdate + ", ccysettdate=" + ccysettdate
                + ", netsi=" + netsi + ", prodtype=" + prodtype + ", ccode=" + ccode + ", sd=" + sd + ", intrate="
                + intrate + "]";
    }
    
    
}
