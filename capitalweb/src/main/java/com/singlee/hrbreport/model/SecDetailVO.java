package com.singlee.hrbreport.model;

public class SecDetailVO {
    
    private String secId;//债券代码
    private String secDesc;//债券简称
    private String vdate;//起息日
    private String tendor;//期限
    private String mdate;//到期日
    private Double amount;//账面价值
    private String secRate;//预期收益
    
    public String getSecId() {
        return secId;
    }
    public void setSecId(String secId) {
        this.secId = secId;
    }
    public String getSecDesc() {
        return secDesc;
    }
    public void setSecDesc(String secDesc) {
        this.secDesc = secDesc;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getTendor() {
        return tendor;
    }
    public void setTendor(String tendor) {
        this.tendor = tendor;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public String getSecRate() {
        return secRate;
    }
    public void setSecRate(String secRate) {
        this.secRate = secRate;
    }
    @Override
    public String toString() {
        return "SecDetailVO [secId=" + secId + ", secDesc=" + secDesc + ", vdate=" + vdate + ", tendor=" + tendor
                + ", mdate=" + mdate + ", amount=" + amount + ", secRate=" + secRate + "]";
    }

}
