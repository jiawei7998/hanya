package com.singlee.hrbreport.model;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public class InBankDealFundVO {
    private String manager; //基金管理人
    private String fundName; //基金简称
    private String fundId; //基金代码
    private String ps; //交易方向
    private String dealDate; //交易日期
    private double amt; //交易金额
    public String getManager() {
        return manager;
    }
    public void setManager(String manager) {
        this.manager = manager;
    }
    public String getFundName() {
        return fundName;
    }
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }
    public String getFundId() {
        return fundId;
    }
    public void setFundId(String fundId) {
        this.fundId = fundId;
    }
    public String getPs() {
        return ps;
    }
    public void setPs(String ps) {
        this.ps = ps;
    }
    public String getDealDate() {
        return dealDate;
    }
    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }
    public double getAmt() {
        return amt;
    }
    public void setAmt(double amt) {
        this.amt = amt;
    }
    @Override
    public String toString() {
        return "InBankDealFundVO [manager=" + manager + ", fundName=" + fundName + ", fundId=" + fundId + ", ps=" + ps
                + ", dealDate=" + dealDate + ", amt=" + amt + "]";
    }
}
