package com.singlee.hrbreport.model;

/**
 *_ 同业业务管理报告
 * @author zk
 */
public class InBankDealBrLoVO {
    private String cust; //交易对手
    private String vdate; //业务开始日期
    private String mdate; //业务结束日期
    private String rate; //利率
    private double amt; //金额
    public String getCust() {
        return cust;
    }
    public void setCust(String cust) {
        this.cust = cust;
    }
    public String getVdate() {
        return vdate;
    }
    public void setVdate(String vdate) {
        this.vdate = vdate;
    }
    public String getMdate() {
        return mdate;
    }
    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
    public String getRate() {
        return rate;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public double getAmt() {
        return amt;
    }
    public void setAmt(double amt) {
        this.amt = amt;
    }
    @Override
    public String toString() {
        return "InBankDealBrLoVO [cust=" + cust + ", vdate=" + vdate + ", mdate=" + mdate + ", rate=" + rate + ", amt="
                + amt + "]";
    }
}
