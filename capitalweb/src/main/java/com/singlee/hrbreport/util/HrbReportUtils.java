package com.singlee.hrbreport.util;

import com.github.pagehelper.Page;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class HrbReportUtils {
	
	public static String HRBCB = SystemProperties.hrbcbUserName;//
	public static String OPICS = SystemProperties.opicsUserName;//
	//execl 多list
	public static String [] mutilList = {"ywmx","dkcf","ckty","yjty","yzldzc","rhty","bwbty"};
	
	/**
	 * 报表日期字段
	 * @param mapEnd
	 * @param postDate
	 */
	public static String dateToMap(Date postDate) {
		String RPTDate = getDate(postDate);
		String SRPTDate = RPTDate.substring(0,4) + "年" 
				+ RPTDate.substring(4,6) + "月" + RPTDate.substring(6,8) + "日";
		return SRPTDate;
	}
	
	/**
	 * 日期格式化
	 * @param date
	 * @return
	 */
	public static String getDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}
	

	
	/**
	 * 获取当前的BR号
	 * @return
	 */
	public static String getBr() {
		return SlSessionHelper.getUser().getOpicsBr();
	}
	
	/**
	 * 将入参转成BigDecimal
	 * @param value
	 * @return
	 */
	public static BigDecimal tranBigde(String value) {
		return new BigDecimal(value).divide(new BigDecimal("10000.000000"),2,BigDecimal.ROUND_HALF_UP);
	}
	
	
	
	public static BigDecimal tranBigdeAddTmp(String value,BigDecimal tmp) {
		return (new BigDecimal(value).add(tmp)).divide(new BigDecimal("10000.00000"),2,BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal tranBigde2(String value) {
        return new BigDecimal(value).divide(new BigDecimal("100000000.000000000"),2,BigDecimal.ROUND_HALF_UP);
    }
	
	
	public static Object tranBigdeAddTmp(String string, String string2) {
		return (new BigDecimal(string).add(new BigDecimal(string2))).divide(new BigDecimal("10000.00"),2,BigDecimal.ROUND_HALF_UP);
	}

	public static String getToday(String string) {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
	
//将不为空的map.value除10000
	public static Object tranBigdeAmt(Object value) {
	    if(value==null| "0".equals(value)){
	        return value;
        }
        else {
            String value2=value.toString();
            return   HrbReportUtils.tranBigde(value2);
        }
	     
	}
       
	//将不为空的map.value除一亿
    public static Object tranBigdeAmt2(Object value) {
        if(value==null| "0".equals(value)){
            return value;
        }
        else {
            String value2=value.toString();
            return   HrbReportUtils.tranBigde2(value2);
        }
         
    }
	
	
    /**
     * _将list转成page
     * @param <E>
     * @param list
     * @param pageNum
     * @param pageSize
     * @return
     */
    public static <E> Page<E> producePage(List<E> list,int pageNum,int pageSize){
        Page<E> page = new Page<E>(pageNum,pageSize);
        int total = list.size();
        page.setTotal(total);
        int startIndex = (pageNum - 1) * pageSize;
        int endIndex = Math.min(startIndex + pageSize , total);
        
        if(startIndex < total) {
            page.addAll(list.subList(startIndex, endIndex));
        }
        return page;
    }
    
    /**
     * _返回季度的第一天和最后一天
     * @param date
     * @return
     */
    public static Map<String , String> getSeasonBegEnd(Date date){
        if(date == null) {
            return null;
        }
        Map<String , String> map = new HashMap();
        String begin = "";
        String end = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calBegin = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        calBegin.setTime(date);
        calEnd.setTime(date);
        //计算季度首月和末月月份
        int month = calBegin.get(Calendar.MONTH) + 1;
        int beginM = (month-1)/3*3 + 1;
        int endM = (month+2)/3*3;
        
        calBegin.set(Calendar.MONTH, beginM - 1);
        calBegin.set(Calendar.DAY_OF_MONTH, 1);
        calEnd.set(Calendar.MONTH, endM );
        calEnd.set(Calendar.DAY_OF_MONTH, 0);
        
        begin= sdf.format(calBegin.getTime());
        end = sdf.format(calEnd.getTime());
        map.put("begin", begin);
        map.put("end", end);
        return map;
    }
}
