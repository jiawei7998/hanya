package com.singlee.hrbreport.util;

import com.singlee.capital.interfacex.qdb.util.PathUtils;

public enum HrbReports {

    jywhjq("jywhjq.xls","外汇即期报表"),
    jywhyq("jywhyq.xls","外汇远期报表"),
    jywhdq("jywhdq.xls","外汇掉期报表"),
    jywbcj("jywbcj.xls","外币拆借报表"),
    jyxycj("jyxycj.xls","信用拆借报表"),
    jytyjk("jytyjk.xls","同业借款报表"),
    jytyckxs("jytyckxs.xls","同业存款（线上）报表"),
    jytyckxx("jytyckxx.xls","同业存款（线下）报表"),
    jyxqmm("jyxqmm.xls","现券买卖报表"),
    jyzqjd("jyzqjd.xls","债券借贷报表"),
    jyzqfx("jyzqfx.xls","债券发行报表"),
    jycdfx("jycdfx.xls","存单发行报表"),
    jymbs("jymbs.xls","MBS报表"),
    jyzyshg("jyzyshg.xls","质押式回购报表"),
    jymdshg("jymdshg.xls","买断式回购报表"),
    jygkdqck("jygkdqck.xls","国库定期存款报表"),
    jyjyshg("jyjyshg.xls","交易所回购报表"),
    jycbjdbl("jycbjdbl.xls","常备借贷便利报表"),
    jyzqjdbl("jyzqjdbl.xls","中期借贷便利报表"),
    jyzxzdk("jyzxzdk.xls","线下押券报表"),
    jylvhh("jylvhh.xls","利率互换报表"),
    jyzqyq("jyzqyq.xls","债券远期报表"),
    jyzqjdsq("jyzqjdsq.xls","债券借贷事前审批报表"),
    jyhjsg("jyhjsg.xls","货基申购报表"),
    jyhjsgqr("jyhjsgqr.xls","货基申购确认报表"),
    jyhjsh("jyhjsh.xls","货基赎回报表"),
    jyhjshqr("jyhjshqr.xls","货基赎回确认报表"),
    jyzjsg("jyzjsg.xls","债基申购报表"),
    jyzjsgqr("jyzjsgqr.xls","债基申购确认报表"),
    jyzjsh("jyzjsh.xls","债基赎回报表"),
    jyzjshqr("jyzjshqr.xls","债基赎回确认报表"),
    jyzhjsg("jyzhjsg.xls","专基申购报表"),
    jyzhjsgqr("jyzhjsgqr.xls","专基申购确认报表"),
    jyzhjsh("jyzhjsh.xls","专基赎回报表"),
    jyzhjshqr("jyzhjshqr.xls","专基赎回确认报表"),
    jyxjfh("jyxjfh.xls","现金分红报表"),
    jyhlzt("jyhlzt.xls","红利再投报表")

    ;

    private String fileName;
    private String desc;

    private HrbReports(String fileName,String desc)
    {
        this.fileName = fileName;
        this.desc = desc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getReportModel(HrbReports reports){
        return PathUtils.getWebRootPath() +"/WEB-INF/classes/template/hrbreport/jy/"+ reports.fileName;
    }

    public static HrbReports getReportsByFileName(String fileName){
        for(HrbReports report:HrbReports.class.getEnumConstants()){
            if(report.getFileName().equals(fileName)){
                return report;
            }
        }
        return null;
    }
}
