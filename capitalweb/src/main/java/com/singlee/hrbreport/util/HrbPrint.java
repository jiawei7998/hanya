package com.singlee.hrbreport.util;

import com.singlee.capital.interfacex.qdb.util.PathUtils;

/**
 * 打印模板
 */
public enum HrbPrint {
    slbefApprove("SL.xls","债券借贷事前审批单");


    private String fileName;
    private String desc;

    private HrbPrint(String fileName,String desc)
    {
        this.fileName = fileName;
        this.desc = desc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getReportModel(HrbPrint reports){
        return PathUtils.getWebRootPath() +"/standard/hrbPrint/printTemplate/"+ reports.fileName;
    }

    public static HrbPrint getReportsByFileName(String fileName){
        for(HrbPrint report:HrbPrint.class.getEnumConstants()){
            if(report.getFileName().equals(fileName)){
                return report;
            }
        }
        return null;
    }
}
