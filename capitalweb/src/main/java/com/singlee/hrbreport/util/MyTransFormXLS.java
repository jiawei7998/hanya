package com.singlee.hrbreport.util;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.InputStream;
import java.util.*;

public class MyTransFormXLS {
	
	private static final int countSize= 65435;
	
	@SuppressWarnings("unchecked")
	public static Workbook myTransFormXLS(InputStream is, Map<String, Object> data) throws ParsePropertyException, InvalidFormatException {
	    XLSTransformer transformer = new XLSTransformer();
	    Workbook resultWorkbook =null;
	    Iterator<String> it=data.keySet().iterator();
		String key=null;
		while(it.hasNext()) {
			 key=it.next();
		}
	    List<Object> dataList = (List<Object>) data.get("list");

	    if(dataList!=null&&dataList.size()>0) {
	    List<List> sheets = new ArrayList<List>();//我们传入的对应每个Sheet的一个Java对象
	    List<String> sheetName=new ArrayList<String>();//即形成Excel文件的时候Sheet的Name

	    //计算拆分分多少sheet
	    int sizeNum=0;
	    int dataSize=dataList.size();
	    if(dataSize<countSize){
	    	sizeNum=1;
	    }else {
	    	sizeNum=dataSize%countSize==0?dataSize/countSize:dataSize/countSize+1;
	    }
	      //分段,比较countSize 和dataList长度
	      for(int i=1;i<=sizeNum;i++){
	        int formIndex = countSize*(i-1);//起始位置
	        int tolIndex = countSize*i>dataList.size()?dataSize%countSize:countSize;//终止位置
           // int toIndex = ((i+1) *countSize)< dataSize? ( i+1 ) * countSize : dataSize ;//0-sizeNum
	        Map<String,Object> param = new HashMap<String,Object>(1);
	        param.put(key,dataList.subList(formIndex,countSize*(i-1)+tolIndex));
	        sheets.add(dataList.subList(countSize*(i-1),countSize*(i-1)+tolIndex));
	        sheetName.add("sheet".concat(String.valueOf(i-1)));
	      }
	      resultWorkbook = transformer.transformMultipleSheetsList(is, sheets,sheetName,key,new HashMap<Object,Object>(),0);
	    return resultWorkbook;
	    
	    }else {
	    	return transformer.transformXLS(is, data);
	    }
	  }

	public static Workbook ywmx(InputStream is, Map<String, Object> data) throws ParsePropertyException, InvalidFormatException {
		XLSTransformer transformer = new XLSTransformer();
		Workbook resultWorkbook =null;


		List<String> nameList = new ArrayList<String>();


		ArrayList<List> dataList = new ArrayList<List>();//添加数据到objects中
		HashMap<String, Object> param2 = new HashMap<String, Object>();



		resultWorkbook = transformer.transformMultipleSheetsList(is, dataList ,
				nameList, "dataList", data, 0);
		return resultWorkbook;
	}
}