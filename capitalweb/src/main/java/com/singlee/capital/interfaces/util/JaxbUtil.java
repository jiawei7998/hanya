package com.singlee.capital.interfaces.util;
import java.io.StringReader;  
import java.io.StringWriter;  
 
import javax.xml.bind.JAXBContext;  
import javax.xml.bind.Marshaller;  
import javax.xml.bind.Unmarshaller;  

import com.singlee.capital.common.exception.RException;
 
/** 
* Jaxb2工具类 
* @author   
*/  
public class JaxbUtil {  
 
   /** 
    * JavaBean转换成xml 
    * 默认编码UTF-8 
    * @param obj 
    * @param writer 
    * @return  
    */  
   public static String convertToXml(Object obj) {  
       return convertToXml(obj, "UTF-8");  
   }  
 
   /** 
    * JavaBean转换成xml 
    * @param obj 
    * @param encoding  
    * @return  
    */  
   public static String convertToXml(Object obj, String encoding) {  
       String result = null;  
       try {  
           JAXBContext context = JAXBContext.newInstance(obj.getClass());  
           Marshaller marshaller = context.createMarshaller();  
           marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  //格式化輸出
           marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);  //自動編碼
 
           StringWriter writer = new StringWriter();  
           marshaller.marshal(obj, writer);  
           result = writer.toString();  
       } catch (Exception e) {  
    	 //e.printStackTrace();
			throw new RException(e);
       }  
 
       return result;  
   }  
 
   /** 
    * xml转换成JavaBean 
    * @param xml 
    * @param c 
    * @return 
    */  
   @SuppressWarnings("unchecked")
   public static <T> T converyToJavaBean(String xml, Class<T> c) {  
       T t = null;  
       try {  
           JAXBContext context = JAXBContext.newInstance(c);  
           Unmarshaller unmarshaller = context.createUnmarshaller();  
           t = (T) unmarshaller.unmarshal(new StringReader(xml));  
       } catch (Exception e) {  
    	 //e.printStackTrace();
			throw new RException(e);
       }  
 
       return t;  
   }  
}