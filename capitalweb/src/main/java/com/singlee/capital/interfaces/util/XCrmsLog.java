package com.singlee.capital.interfaces.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 检查条件，并将不符合条件的 情况下，扔出异常（RException运行期异常，CException编译器异常）
 */
public class XCrmsLog {


	private static final Logger log = LoggerFactory.getLogger("Crms");
	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final String message) {
		if (log != null) {
			log.error(message);
		} else {
			System.err.printf("ERROR: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final String message, final Throwable t) {
		//String err = message;// + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		if (log != null) {
			log.error(message, t);
		} else {
			String err = message  + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
			System.err.printf("ERROR: %s\n", err);
		}
	}
	public static void error(final String message, final Throwable t, Object... obj) {
		String err = String.format(message, obj) ;// + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		error(err,t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final Throwable t) {
		error("", t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final String message) {
		if (log != null) {
			log.warn(message);
		} else {
			System.err.printf("WARN: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final String message, final Throwable t) {
		String wrn = message + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		warn(wrn);
	}
	public static void warn(final String message, final Throwable t, Object... obj) {
		String wrn = String.format(message, obj)  + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		warn(wrn);
	}
	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final Throwable t) {
		warn("",t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void info(final String message) {
		if (log != null) {
			log.info(message);
		} else {
			System.err.printf("INFO: %s\n", message);
		}
	}

	public static void info(final String message, Object... obj) {
		info(String.format(message, obj));
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void debug(final String message) {
		if (log != null) {
			log.debug(message);
		} else {
			System.err.printf("DEBUG: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void debug(String message, Object... obj) {
		debug(String.format(message, obj));
	}

	/**
	 * 是否debug打开
	 * 
	 * @return
	 */
	public static boolean isLogDebugEnabled() {
		if (log != null) {
			return log.isDebugEnabled();
		} else {
			return true;
		}
	}
	
}
