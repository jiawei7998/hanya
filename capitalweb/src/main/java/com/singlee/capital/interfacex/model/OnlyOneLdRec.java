package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OnlyOneLdRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OnlyOneLdRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SchType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Num" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Frequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OnlyOneLdRec", propOrder = { "schType", "beginDt", "amount",
		"chargeCode", "num", "frequency", "rate" })
public class OnlyOneLdRec {

	@XmlElement(name = "SchType", required = true)
	protected String schType;
	@XmlElement(name = "BeginDt", required = true)
	protected String beginDt;
	@XmlElement(name = "Amount", required = true)
	protected String amount;
	@XmlElement(name = "ChargeCode", required = true)
	protected String chargeCode;
	@XmlElement(name = "Num", required = true)
	protected String num;
	@XmlElement(name = "Frequency", required = true)
	protected String frequency;
	@XmlElement(name = "Rate", required = true)
	protected String rate;

	/**
	 * Gets the value of the schType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSchType() {
		return schType;
	}

	/**
	 * Sets the value of the schType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSchType(String value) {
		this.schType = value;
	}

	/**
	 * Gets the value of the beginDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDt() {
		return beginDt;
	}

	/**
	 * Sets the value of the beginDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDt(String value) {
		this.beginDt = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the chargeCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * Sets the value of the chargeCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeCode(String value) {
		this.chargeCode = value;
	}

	/**
	 * Gets the value of the num property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNum() {
		return num;
	}

	/**
	 * Sets the value of the num property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNum(String value) {
		this.num = value;
	}

	/**
	 * Gets the value of the frequency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFrequency() {
		return frequency;
	}

	/**
	 * Sets the value of the frequency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFrequency(String value) {
		this.frequency = value;
	}

	/**
	 * Gets the value of the rate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * Sets the value of the rate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRate(String value) {
		this.rate = value;
	}

}
