package com.singlee.capital.interfacex.service;

//import com.ibm.mq.MQC;
//import com.ibm.mq.MQEnvironment;
//import com.ibm.mq.MQException;
//import com.ibm.mq.MQGetMessageOptions;
//import com.ibm.mq.MQMessage;
//import com.ibm.mq.MQQueue;
//import com.ibm.mq.MQQueueManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.socketservice.MessageQueueReceiver;

public class MQClient {
	
	
//	static MQQueueManager qMgr;
//	static int CCSID = 918;
//	static String queueString = "QL_IFBC_EXG_IFB";
//	
//	public static void connect1() throws MQException{
//		//209配置
////		MQEnvironment.hostname = "10.240.94.209";//209		
////		MQEnvironment.channel = "CS_EXG_S";//209		
////		MQEnvironment.port = 40190;//209		
////		qMgr = new MQQueueManager("S1QM_EXG_2");//209
//		
//		//208配置
//		MQEnvironment.hostname = "10.240.94.208";
//		MQEnvironment.channel = "CS_EXG_S3";//208
//		MQEnvironment.port = 40193;//208
//		qMgr = new MQQueueManager("S1QM_EXG_1");//208
//	}
//	
//	public static void connect2() throws MQException{
//		//209配置
//		MQEnvironment.hostname = "10.240.94.209";//209		
//		MQEnvironment.channel = "CS_EXG_S";//209		
//		MQEnvironment.port = 40190;//209		
//		qMgr = new MQQueueManager("S1QM_EXG_2");//209
//		
////		//208配置
////		MQEnvironment.hostname = "10.240.94.208";
////		MQEnvironment.channel = "CS_EXG_S3";//208
////		MQEnvironment.port = 40193;//208
////		qMgr = new MQQueueManager("S1QM_EXG_1");//208
//	}
//	
//	@SuppressWarnings("deprecation")
//	public static void receiveMsg(){
//		int openOptions =  MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
//		MQQueue queue = null;
//		try{
//			queue = qMgr.accessQueue(queueString, openOptions, null, null, null);
//			System.out.println(MQEnvironment.hostname+"队列深度为："+ queue.getCurrentDepth());
//			if(queue.getCurrentDepth()<1){return ;}
//			MQMessage msg = new MQMessage();
//			MQGetMessageOptions gmo = new MQGetMessageOptions();
//			
//			gmo.options = gmo.options + MQC.MQGMO_SYNCPOINT; // Get messages
//			// control(在同步点控制下获取消息);
//			gmo.options = gmo.options + MQC.MQGMO_WAIT; // Wait if no messages
//			// Queue(如果在队列上没有消息则等待);
//			gmo.options = gmo.options + MQC.MQGMO_FAIL_IF_QUIESCING; // Fail
//			// QeueManager :Sets the time limit for the
//			// Quiescing(如果队列管理器停顿则失败);
//			gmo.waitInterval = Integer.valueOf(60000); 
//			
//			queue.get(msg,gmo);		
//			byte[] rawData = new byte[msg.getMessageLength()];
//			msg.readFully(rawData);
//			String str = new String(rawData);
//			System.out.println("接收消息的长度: " + str.length());
//			System.out.println(" The Message from the Queue is : " + str);
//			Root root = MessageQueueReceiver.converyToJavaBean(str,Root.class);			
//			System.out.println(root.getTranAmt());			
//			System.out.println("---------------------------");
//		}catch(Exception e){
//			//e.printStackTrace();
//			throw new RException(e);
//		} finally {
//			if (queue != null){
//				try {
//					queue.close();
//					qMgr.disconnect();
//				} catch (MQException e){
//					//e.printStackTrace();
//					throw new RException(e);
//				}
//			}
//		}
//	}
//	
//	public static void start() throws MQException{
//		connect1();
//		receiveMsg();	
//		connect2();
//		receiveMsg();
//	}
	
	/*public static void main(String[] args) throws MQException {
		connect1();
		receiveMsg();	
		connect2();
		receiveMsg();
	}*/

}
