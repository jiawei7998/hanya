package com.singlee.capital.interfacex.quartzjob;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.service.MessageQueueService;

public class TsaMessageQueueQuartzJob  implements CronRunnable{

	/**
	 * MQ消息列表接口-动账通知(TSA触发)
	 * 来账分拣处理
	 */
	private MessageQueueService messageQueueService = SpringContextHolder.getBean("messageQueueService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		try {
			RetMsg<Object> retMsg = messageQueueService.TsaMessageQueueService();
			System.out.println(retMsg.getCode()+"|"+retMsg.getDesc());
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			throw new RException(e);
		}
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
