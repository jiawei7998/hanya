package com.singlee.capital.interfacex.qdb.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.interfacex.qdb.service.TdCashflowInterestService;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
@Service("cashflowCfPrintServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CashflowCfPrintServiceImpl implements AssemblyDataService{
	@Autowired
	private TdCashflowInterestService tdCashflowInterestService;
	
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	
	
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("cashflowId", id);
		List<TdCashflowInterest> list = tdCashflowInterestService.queryPrintList(map);
		
		if(list.size()>=1){
			String dealNo=list.get(0).getDealNo();
			map.put("dealNo", dealNo);
			
		}
		TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
		Integer pro=td.getPrdNo();
		if(pro!=null&&!"".equals(pro)){
			TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
			map.put("productName", p.getPrdName());
		}
		try{
			for(TdCashflowInterest li : list){
				map.put("partyBankName", li.getPartyBankName());
				map.put("interestAmt", li.getInterestAmt());
				map.put("amt", li.getAmt());
				map.put("partyAccCode", li.getPartyAccCode());
				map.put("theoryPaymentDate",li.getTheoryPaymentDate());
			}
			map.put("list",list);
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}

}
