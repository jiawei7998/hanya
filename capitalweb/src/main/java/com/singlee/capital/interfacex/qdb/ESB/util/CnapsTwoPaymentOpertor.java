package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdSettleMapper;
import com.singlee.capital.interfacex.qdb.pojo.TdTrdSettle;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.util.StringUtils;

/****
 * 
 * 二代支付 报文工具
 * @author lee
 *
 */
@Component
public class CnapsTwoPaymentOpertor {
	
	//ESB报文处理DAO
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	
	private static String sendPaymentPath_A100 ;//大额客户发起的汇兑_(CNAPS001_A100)_reqt.xml
	private static String sendPaymentPath_A200 ;//大额客户发起的汇兑_(CNAPS001_A200)_reqt.xml
	private static String queryStatusPath ;//查询前置交易状态_(CNAPS002_CNAPS002_())_V101_reqt.xml
	private static String querySystemStatusPath;//系统状态查询_(CNAPS007_CNAPS007_())_V101_reqt.xml
	private static String queryAcceptBankStatusPath;//接收行状态查询_(CNAPS008_CNAPS008_())_V101_reqt.xml
	private static String queryVerStatusPath;//头寸核实服务_签发
	
	private static String httpUrlSendPayment;//大额客户发起的汇兑服务Url
	private static String httpUrlQueryStatus;//查询前置交易状态
	private static String httpUrlQuerySystemStatus;//系统状态查询
	private static String httpUrlQueryAcceptBankStatus;//接收行状态查询
	private static String httpUrlQueryVerStatus;//头寸核实服务_签发
	
	private static int count = 0;

	private DecimalFormat decimalFormat = new DecimalFormat("############0.00");
	
	@Autowired
	private TdTrdSettleMapper tdTrdSettleMapper;
	
	public static Logger logger = LoggerFactory.getLogger("INTERFACEX");
	
	
	static
	{
		String esbXmlDir = PropertiesUtil.getProperties("ESB.EsbXmlDir");//ESB报文模板保存的路径
		sendPaymentPath_A100 = esbXmlDir + PropertiesUtil.getProperties("CNAPS.sendPaymentPath_A100");//1532100101_大额客户发起的汇兑_(CNAPS001_A100)_reqt.xml
		sendPaymentPath_A200 = esbXmlDir + PropertiesUtil.getProperties("CNAPS.sendPaymentPath_A200");//1532100101_大额客户发起的汇兑_(CNAPS001_A200)_reqt.xml
		queryStatusPath = esbXmlDir + PropertiesUtil.getProperties("CNAPS.queryStatusPath") ;//1532100102_查询前置交易状态_(CNAPS002_CNAPS002_())_V101_reqt.xml
		querySystemStatusPath = esbXmlDir + PropertiesUtil.getProperties("CNAPS.querySystemStatusPath");//1532100210_系统状态查询_(CNAPS007_CNAPS007_())_V101_reqt.xml
		queryAcceptBankStatusPath = esbXmlDir + PropertiesUtil.getProperties("CNAPS.queryAcceptBankStatusPath");//1532100211_接收行状态查询_(CNAPS008_CNAPS008_())_V101_reqt.xml
		queryVerStatusPath = esbXmlDir + PropertiesUtil.getProperties("CNAPS.queryVerStatusPath");//头寸签发核实服务
		
		String esbServiceUrl = PropertiesUtil.getProperties(PropertiesUtil.EsbServiceUrl);
		httpUrlSendPayment = esbServiceUrl + PropertiesUtil.getProperties("CNAPS.httpUrlSendPayment");//大额客户发起的汇兑服务Url
		httpUrlQueryStatus = esbServiceUrl + PropertiesUtil.getProperties("CNAPS.httpUrlQueryStatus");//查询前置交易状态
		httpUrlQuerySystemStatus = esbServiceUrl + PropertiesUtil.getProperties("CNAPS.httpUrlQuerySystemStatus");//系统状态查询
		httpUrlQueryAcceptBankStatus = esbServiceUrl + PropertiesUtil.getProperties("CNAPS.httpUrlQueryAcceptBankStatus");//接收行状态查询
		httpUrlQueryVerStatus = esbServiceUrl + PropertiesUtil.getProperties("CNAPS.httpUrlQueryVerStatus");//头寸签发核实服务
	}
	
	/****
	 * 将路径转换为流
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private InputStream transXmlToStream(String path)throws Exception
	{
		return CnapsTwoPaymentOpertor.class.getClassLoader().getResourceAsStream(path);
	}
	
	/**
	 * 176050_系统状态查询_(404_404_())_V101_reqt.xml
	 * 
	 * 系统状态查询CNAPS007：各渠道系统主动查询大额系统、小额系统状态。
	 * 
	 */
	public HashMap<String, String> cnapsQuerySystemStatus()throws Exception
	{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		try {
			/**
	         * 读取请求报文XML
	         */
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			Document xmlDoc = reader.read(transXmlToStream(querySystemStatusPath));
			xmlDoc.setXMLEncoding("UTF-8");
			/**
			 * 发送查询请求保存到本地服务器目录
			 */
			logger.info("发送查询报文:"+xmlDoc.asXML());
				
			/**
			 * HTTP 请求 ESB服务器 获得相应回执
			 */
	        HashMap<String, Object> xmlResultString = httpClientManagerDao.sendXmlToRequest(httpUrlQuerySystemStatus,xmlDoc.asXML());
	       
	        if(xmlResultString == null || xmlResultString.size() == 0){
	        	hashMap.put("status", "neterror");
	        	logger.info("网络异常！请检查网络！");
	        	return hashMap;
	        	
	        }else{
	        	logger.info("statusCode:"+xmlResultString.get("statusCode"));
		        if("200".equals(StringUtils.trimToEmpty(String.valueOf(xmlResultString.get("statusCode")))))
		        {
		        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				    HashMap<String, Object> campHdr = new HashMap<String, Object>();//campHdr
				    HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				    HashMap<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream((byte[]) xmlResultString.get("Bytes"));
				    Document xmlDocAccept = reader.read(byteArrayInputStream);
				    xmlDocAccept.setXMLEncoding("UTF-8");
				    /**
				     * 反馈请求结果持久化保存到本地服务器目录
				     */
				    logger.info("接收ESB系统状态反馈报文："+XmlFormat.format(xmlDocAccept.asXML()));
		        
				    PraseEsbXmlPackageDescription.praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
				    
				    if(appBody.size() > 0){
				    	 hashMap.put("sysSt", appBody.get(3).get("curStatus").toString().length()<=0?"":"_"+appBody.get(3).get("curStatus").toString());
						 hashMap.put("sysCd", appBody.get(2).get("sysCode").toString());
						 hashMap.put("prcSts", appBody.get(1).get("recordNm").toString());
				    }
				    
		        }
	        }
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return hashMap;
	}
	
	/*****
	 * 
	 * 176076_参与者运行状态查询_(430_430_())_V101_reqt.xml
	 * @param acceptBankId
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> cnapsQueryAcceptBankStatus(String acceptBankId) throws Exception{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		try {
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			Document xmlDoc = reader.read(transXmlToStream(queryAcceptBankStatusPath));
			xmlDoc.setXMLEncoding("UTF-8");
			xmlDoc.getRootElement().element("appBody").element("reqMsg").element("bankCode").setText(acceptBankId);
			/**
			 * 发送查询请求保存到本地服务器目录
			 */
//			FileUtils.messageXMLFileSaveLocal(xmlDoc.asXML(), fileSavePath,
//					"queryAcceptBankStatus"+"-"+ DateUtil.getCurrentCompactDateTimeAsString() + "-reqt.xml",DateUtil.getCurrentDateAsString("yyyyMMdd"));
//			
			/**
			 * HTTP 请求 ESB服务器 获得相应回执
			 */
	        HashMap<String, Object> xmlResultString = httpClientManagerDao.sendXmlToRequest(httpUrlQueryAcceptBankStatus,xmlDoc.asXML());
	       
	        if(xmlResultString == null || xmlResultString.size() == 0){
	        	hashMap.put("status", "neterror");
	        	logger.info("网络异常！请检查网络！");
	        	return hashMap;
	        	
	        } else {
	        	logger.info("CnapsQueryAcceptBankStatus--statusCode:"+xmlResultString.get("statusCode"));
		        if("200".equals(StringUtils.trimToEmpty(String.valueOf(xmlResultString.get("statusCode"))))){
		        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				    HashMap<String, Object> campHdr = new HashMap<String, Object>();//campHdr
				    HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				    HashMap<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream((byte[]) xmlResultString.get("Bytes"));
				    Document xmlDocAccept = reader.read(byteArrayInputStream);
				    xmlDocAccept.setXMLEncoding("UTF-8");
				    /**
				     * 反馈请求结果持久化保存到本地服务器目录
				     */
				    logger.info("接收ESB系统状态反馈报文："+XmlFormat.format(xmlDocAccept.asXML()));
//				    FileUtils.messageXMLFileSaveLocal(XmlFormat.format(xmlDocAccept.asXML()), fileSavePath,
//						"queryAcceptBankStatus"+"-"+DateUtil.getCurrentCompactDateTimeAsString()+"-resp.xml",DateUtil.getCurrentDateAsString("yyyyMMdd"));
//			        
				    PraseEsbXmlPackageDescription.praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
				    
				    if("26400000000000".equals(svcHdr.get("respCde"))){
				    	if (appBody.size() > 0) {
				            hashMap.put("respCde", svcHdr.get("respCde").toString());
				            hashMap.put("respMsg", svcHdr.get("respMsg").toString());
				            hashMap.put("instgPtySts", appBody.get(1).get("curStatus")==null?"":appBody.get(1).get("curStatus").toString());
				          }
				    	
				    }else {
						hashMap.put("respCde", svcHdr.get("respCde").toString());
						hashMap.put("respMsg", svcHdr.get("respMsg").toString());
					}
		        }
	        }
		} catch (Exception e) {
			throw e;
		}
		return hashMap;
	}
	
	
	/****
	 * 大额发起汇兑交易
	 * 176000_普通汇兑_(101_101_A200_())_V101_reqt.xml
	 * 176000_普通汇兑_(101_101_A100_())_V101_reqt.xml
	 * @param opicsPmtqCnaps
	 * @return
	 */
	public HashMap<String, String> cnapsSendPaymentDealOper(TdTrdSettle settle,String verifyFlag)throws Exception{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		try {
			/**
	         * 读取请求报文XML
	         */
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			Document xmlDoc = null;
			if("A200".equals(settle.getHvpType().trim())){
				xmlDoc = reader.read(transXmlToStream(sendPaymentPath_A200));
			}else{
				xmlDoc = reader.read(transXmlToStream(sendPaymentPath_A100));
			}
			xmlDoc.setXMLEncoding("UTF-8");
			
			xmlDoc.getRootElement().element("appBody").element("busMsg").element("pmtGrpId").setText(settle.getUserDealNo().replaceAll("-", ""));//汇兑组号
			xmlDoc.getRootElement().element("appBody").element("busMsg").element("seqNo").setText(settle.getBusDealNo());//交易流水号
			xmlDoc.getRootElement().element("appBody").element("payMsg").element("payAccount").setText(settle.getPayUserId());//付款人账号
			xmlDoc.getRootElement().element("appBody").element("payMsg").element("payNm").setText(settle.getPayUserName());//付款人名称
			xmlDoc.getRootElement().element("appBody").element("payMsg").element("payBank").setText(settle.getPayBankId());//付款行行号
			xmlDoc.getRootElement().element("appBody").element("payMsg").element("payOpenBank").setText(settle.getPayBankId());//付款人开户行行号
			
			xmlDoc.getRootElement().element("appBody").element("payeeMsg").element("payeeAccount").setText(settle.getRecUserId());//收款人账号
			xmlDoc.getRootElement().element("appBody").element("payeeMsg").element("payeeNm").setText(settle.getRecUserName());//收款人名称
			xmlDoc.getRootElement().element("appBody").element("payeeMsg").element("payeeBank").setText(settle.getRecBankId());//收款行行号
			xmlDoc.getRootElement().element("appBody").element("payeeMsg").element("payeeOpenBank").setText(settle.getRecBankId());//收款人开户行行号
			
			xmlDoc.getRootElement().element("appBody").element("cashMsg").element("amount").setText(decimalFormat.format(settle.getAmount() == null ? "0.00" : settle.getAmount().abs()));//汇款金额
			xmlDoc.getRootElement().element("appBody").element("cashMsg").element("totalSum").setText(decimalFormat.format(settle.getAmount() == null ? "0.00" : settle.getAmount().abs()));//总额
			
			xmlDoc.getRootElement().element("appBody").element("addMsg").element("addinfo").setText(settle.getRemarkFy() == null ? "" : settle.getRemarkFy());//附言
			xmlDoc.getRootElement().element("appBody").element("cerMsg").element("cerNo").setText(settle.getCno());//证件号
			xmlDoc.getRootElement().element("appBody").element("cerMsg").element("cerArea").setText(settle.getCidt() == null ? "CHN" : settle.getCidt());//证件发行国家
			xmlDoc.getRootElement().element("appBody").element("cerMsg").element("cifNbr").setText(settle.getCno());//核心CIF客户号
			if ("S".equals(verifyFlag)) {
				xmlDoc.getRootElement().element("appBody").element("cyberMsg").element("verifyFlag").setText("S");//企业网银监管户复核通过
			}
		    
		    logger.info("发送ESB汇兑报文："+XmlFormat.format(xmlDoc.asXML()));
		    /**
			 * 发送查询请求保存到本地服务器目录
			 */
//			FileUtils.messageXMLFileSaveLocal(xmlDoc.asXML(), fileSavePath,
//					"Cnaps2SendPayment"+"-" + settle.getUserDealNo().replace("-", "")+"-"+DateUtil.getCurrentCompactDateTimeAsString()+"-reqt.xml",DateUtil.getCurrentDateAsString("yyyyMMdd"));
//			
			/**
			 * HTTP 请求 ESB服务器 获得相应回执
			 */
	        HashMap<String, Object> xmlResultString = httpClientManagerDao.sendXmlToRequest(httpUrlSendPayment,xmlDoc.asXML());
	        if(xmlResultString == null || xmlResultString.size()==0){
	        	hashMap.put("status", "neterror");
	        	logger.info("网络异常！请检查网络！");
	        	return hashMap;
	        }else{
	        	logger.info("Cnaps2SendPayment--statusCode:"+xmlResultString.get("statusCode"));
		        if("200".equals(StringUtils.trimToEmpty(String.valueOf(xmlResultString.get("statusCode"))))){
	
		        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				    HashMap<String, Object> campHdr = new HashMap<String, Object>();//campHdr
				    HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				    HashMap<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream((byte[]) xmlResultString.get("Bytes"));
				    Document xmlDocAccept = reader.read(byteArrayInputStream);
				    xmlDocAccept.setXMLEncoding("UTF-8");
				    /**
				     * 反馈请求结果持久化保存到本地服务器目录
				     */
				    logger.info("接收ESB汇兑反馈报文："+XmlFormat.format(xmlDocAccept.asXML()));	        
	        
					PraseEsbXmlPackageDescription.praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
					hashMap.put("respCde", svcHdr.get("respCde").toString());
			        hashMap.put("respMsg", svcHdr.get("respMsg").toString());
			        
			        hashMap.put("preStat", StringUtils.trimToEmpty(((HashMap<String, Object>)appBody.get(Integer.valueOf(3))).get("preStatus").toString()));
			        hashMap.put("prcSts", StringUtils.trimToEmpty(((HashMap<String, Object>)appBody.get(Integer.valueOf(4))).get("prcSts").toString()));
						
		        } else {
		        	hashMap.put("respCde", "错误信息提示："+xmlResultString.get("statusCode"));
				}
	        }
		    
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return hashMap;
	}
	
	/***
	 * 生成汇兑组号
	 * @param replace
	 * @return
	 */
	public String createUserDealNo(TdTrdSettle settle) {
		String userDealNo = settle.getUserDealNo();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sStr","C");
		map.put("trdType","999");
		//C20170831339000707
		String tradeId = tdTrdSettleMapper.getTradeId(map);
		//80002-1709-252-000701
		settle.setUserDealNo(userDealNo.substring(0,15) + tradeId.substring(12, 18));
		
		//UPDATE TD_TRD_SETTLE S SET S.USER_DEAL_NO = #{userDealNo} WHERE S.DEAL_NO = #{dealNo}
		try {
			map.put("userDealNo", settle.getUserDealNo());
			map.put("dealNo", settle.getDealNo());
			tdTrdSettleMapper.updateTdTrdSettleUserDealNo(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return settle.getUserDealNo().replace("-", "");
	}
	
	public static void main(String[] args) {
		System.out.println("80002-1709-252-000701".substring(0,15) + "C20170831339000707".substring(12, 18));
	}

	/****
	 * 
	 * 查询大额清算状态信息
	 * @param slPmtq
	 * @return
	 */
	public HashMap<String, Object> cnapsQuerySettStatusOper(List<TdTrdSettle> settles)throws Exception{
		HashMap<String, Object> returnHashMap = new HashMap<String, Object>(); 
		try {
			/**
	         * 读取请求报文XML
	         */
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			Document xmlDoc = reader.read(transXmlToStream(queryStatusPath));
			xmlDoc.setXMLEncoding("UTF-8");
			/**
			 * 定义需要替换的域值 信息   节点 -- 值
			 */
			HashMap<String, HashMap<String, Object>> replaceHashMap = new HashMap<String, HashMap<String, Object>>();
			HashMap<String, Object> svcHdrReplace  = new HashMap<String, Object>();//svcHdr
		    HashMap<String, Object> appHdrReplace = new HashMap<String, Object>();//appHdr
		    HashMap<String, Object> appBodyReplace  = new HashMap<String, Object>();//appBody
		    if(settles.size() == 1){
		    	appBodyReplace.put("pmtGrpId", settles.get(0).getUserDealNo().replace("-", ""));
		    }

		    replaceHashMap.put("svcHdr", svcHdrReplace);
		    replaceHashMap.put("appHdr", appHdrReplace);
		    replaceHashMap.put("appBody", appBodyReplace);
		    
		    ReplaceElementContext.replaceElementCnaps2QueryStatus(xmlDoc.getRootElement(),replaceHashMap);
		    /**
			 * 发送查询请求保存到本地服务器目录
			 */
		   // System.out.println(slPmtq.size());
		    for(TdTrdSettle settle : settles){
		    	xmlDoc.getRootElement().element("appBody").addElement("cdtrAcctList").addElement("cdtrAcct").setText(settle.getRecUserId());
			    
		    }
		    logger.info("发送ESB大额往帐查询交易报文："+XmlFormat.format(xmlDoc.asXML()));
//			FileUtils.messageXMLFileSaveLocal(XmlFormat.format(xmlDoc.asXML()), fileSavePath,
//					"Cnaps2QueryStatus"+"-"+DateUtil.getCurrentCompactDateTimeAsString()+"-reqt.xml",DateUtil.getCurrentDateAsString("yyyyMMdd"));
//			
			/**
			 * HTTP 请求 ESB服务器 获得相应回执
			 */
	        HashMap<String, Object> xmlResultString = httpClientManagerDao.sendXmlToRequest(httpUrlQueryStatus,XmlFormat.format(xmlDoc.asXML()));
	        if(xmlResultString == null || xmlResultString.size()==0)
	        {
	        	count++;
	        	if(count <3 )
	        	{
	        		cnapsQuerySettStatusOper(settles);
	        	}
	        	returnHashMap.put("status", "neterror");
		        return returnHashMap;
				
	        }else{
		    	logger.info("CnapsQuerySettStatusOper--statusCode:"+xmlResultString.get("statusCode"));
		        if("200".equals(StringUtils.trimToEmpty(String.valueOf(xmlResultString.get("statusCode"))))){
		        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				    HashMap<String, Object> campHdr = new HashMap<String, Object>();//campHdr
				    HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				    HashMap<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream((byte[]) xmlResultString.get("Bytes"));
				    Document xmlDocAccept = reader.read(byteArrayInputStream);
				    xmlDocAccept.setXMLEncoding("UTF-8");
				    /**
				     * 反馈请求结果持久化保存到本地服务器目录
				     */
				    logger.info("接收ESB大额查询反馈报文："+XmlFormat.format(xmlDocAccept.asXML()));
//				    FileUtils.messageXMLFileSaveLocal(XmlFormat.format(xmlDocAccept.asXML()), fileSavePath,
//						"Cnaps2QueryStatus"+"-"+DateUtil.getCurrentCompactDateTimeAsString()+"-resp.xml",DateUtil.getCurrentDateAsString("yyyyMMdd"));

				    PraseEsbXmlPackageDescription.praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
					
				    returnHashMap.put("respCde", svcHdr.get("respCde").toString());
				    returnHashMap.put("respMsg", svcHdr.get("respMsg").toString());
				    
				    List<TdTrdSettle> list = new ArrayList<TdTrdSettle>();
				    TdTrdSettle settle = null;
				    if (appBody.size() > 0) 
				    {
				    	settle = new TdTrdSettle();
				        System.out.println("pmtGrpId:" + ((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("pmtGrpId"));
				        settle.setUserDealNo(((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("pmtGrpId").toString());
				        System.out.println("preStat:" + ((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("preStatus"));
				        settle.setSettFlag(((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("preStatus").toString());
				        System.out.println("proCode:" + ((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("proCode"));
				        System.out.println("proMsg:" + ((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("proMsg"));
				        settle.setRetCode(((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("proCode").toString());
				        settle.setRetMsg(((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("proMsg").toString());
				          
				        list.add(settle);
				          
				        returnHashMap.put("prcSts", ((HashMap<String, Object>)appBody.get(Integer.valueOf(2))).get("prcSts").toString().trim());
				    }
				    
				    returnHashMap.put("rec", list);

		        } else {
		        	returnHashMap.put("errCd", "错误信息提示："+xmlResultString.get("statusCode"));
				}
		       
	        }
		    
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			//还原i初始值
	        count=0;
		}
		return returnHashMap;
	}
	
	/***
	 * 发往头寸，做预报核实
	 * @param tdTrdSettle
	 * @return
	 */
	public HashMap<String, String> cnapsSendCash(TdTrdSettle tdTrdSettle)throws Exception{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		try{
			/**
	         * 读取请求报文XML
	         */
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			Document xmlDoc = reader.read(transXmlToStream(queryVerStatusPath));
			xmlDoc.setXMLEncoding("UTF-8");
			/**
			 * 定义需要替换的域值 信息   节点 -- 值
			 */
			xmlDoc.getRootElement().element("appHdr").element("reqDate").setText(DateUtil.getCurrentDateAsString("yyyyMMdd"));//交易日
		    xmlDoc.getRootElement().element("appHdr").element("reqTime").setText(DateUtil.getCurrentDateAsString("HHmmss"));//操作时点
		    
		    xmlDoc.getRootElement().element("appBody").element("pmtGrpId").setText(tdTrdSettle.getUserDealNo().replace("-", ""));//汇兑组号
		    xmlDoc.getRootElement().element("appBody").element("serNo").setText(tdTrdSettle.getBusDealNo());//交易流水号
		    xmlDoc.getRootElement().element("appBody").element("cifNo").setText(tdTrdSettle.getCno());//核心CIF客户号
		    xmlDoc.getRootElement().element("appBody").element("cifAcctNo").setText(tdTrdSettle.getPayUserId());//客户账号
		    xmlDoc.getRootElement().element("appBody").element("cifNm").setText(tdTrdSettle.getPayUserName());//客户名称
		    xmlDoc.getRootElement().element("appBody").element("ccyAmt").setText(decimalFormat.format(tdTrdSettle.getAmount() == null?"0.00" : tdTrdSettle.getAmount().abs()));//汇款金额
		    xmlDoc.getRootElement().element("appBody").element("ccyPosAmt").setText(decimalFormat.format(tdTrdSettle.getAmount() == null?"0.00" : tdTrdSettle.getAmount().abs()));//总额
		    xmlDoc.getRootElement().element("appBody").element("prodType").setText(tdTrdSettle.getProduct());//产品品种
		    xmlDoc.getRootElement().element("appBody").element("dealDt").setText(DateUtil.getCurrentDateAsString("yyyyMMdd"));//交易日
		    xmlDoc.getRootElement().element("appBody").element("operTm").setText(DateUtil.getCurrentDateAsString("HHmmss"));//操作时点
		    xmlDoc.getRootElement().element("appBody").element("targetAcctNo").setText(tdTrdSettle.getRecUserId());//客户账号
		    xmlDoc.getRootElement().element("appBody").element("tellerNm").setText(tdTrdSettle.getVoper());//柜员名称
		    xmlDoc.getRootElement().element("appBody").element("tellerNo").setText("QD96000868");//柜员号
		    if("A200".equals(tdTrdSettle.getHvpType().trim())){
		    	xmlDoc.getRootElement().element("appBody").element("remitType").setText("2");//汇款类型 
		    }else{
		    	xmlDoc.getRootElement().element("appBody").element("remitType").setText("1");//汇款类型 
		    }
			/**
			 * HTTP 请求 ESB服务器 获得相应回执
			 */
	        HashMap<String, Object> xmlResultString = httpClientManagerDao.sendXmlToRequest(httpUrlQueryVerStatus,xmlDoc.asXML());
	        if(xmlResultString.size()==0 || xmlResultString == null){
	        	hashMap.put("status", "neterror");
	        	logger.info("网络异常！请检查网络！");
	        	return hashMap;
	        }else{
	        	logger.info("CnapsSendCash--statusCode:"+xmlResultString.get("statusCode"));
		        if("200".equals(StringUtils.trimToEmpty(String.valueOf(xmlResultString.get("statusCode"))))){
	
		        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				    HashMap<String, Object> campHdr = new HashMap<String, Object>();//campHdr
				    HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				    HashMap<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream((byte[]) xmlResultString.get("Bytes"));
				    Document xmlDocAccept = reader.read(byteArrayInputStream);
				    xmlDocAccept.setXMLEncoding("UTF-8");
				    /**
				     * 反馈请求结果持久化保存到本地服务器目录
				     */
				    logger.info("接收ESB汇兑反馈报文："+XmlFormat.format(xmlDocAccept.asXML()));
	        
					PraseEsbXmlPackageDescription.praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
					hashMap.put("respCde", svcHdr.get("respCde").toString());
					hashMap.put("respMsg", svcHdr.get("respMsg").toString());
					hashMap.put("verStatus", StringUtils.trimToEmpty(appBody.get(1).get("verStatus").toString()));
					hashMap.put("verMsg", StringUtils.trimToEmpty(appBody.get(2).get("verMsg").toString()));
						
		        } else {
		        	hashMap.put("respCde", "999999");
					hashMap.put("respMsg", String.valueOf(xmlResultString.get("statusCode")));
					hashMap.put("verStatus", "999999");
					hashMap.put("verMsg", String.valueOf(xmlResultString.get("statusCode")));
				}
	        }
		    
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return hashMap;
	}

	/**
	 * @return the count
	 */
	public static int getCount() {
		return count;
	}


	/**
	 * @param count the count to set
	 */
	public static void setCount(int count) {
		CnapsTwoPaymentOpertor.count = count;
	}
	
}
