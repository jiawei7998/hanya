package com.singlee.capital.interfacex.quartzjob;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.service.SeasonTaskService;

public class GetWorkDayQuartzJob implements CronRunnable{
	@Autowired
	private SeasonTaskService seasonTaskService = SpringContextHolder.getBean("SeasonTaskServiceImpl");;
	
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		seasonTaskService.getSeasonTask();
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
