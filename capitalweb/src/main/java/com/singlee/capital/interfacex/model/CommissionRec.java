package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CommissionRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionRec", propOrder = { "commissionType",
		"commissionAmt" })
public class CommissionRec {

	@XmlElement(name = "CommissionType", required = true)
	protected String commissionType;
	@XmlElement(name = "CommissionAmt", required = true)
	protected String commissionAmt;

	/**
	 * Gets the value of the commissionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommissionType() {
		return commissionType;
	}

	/**
	 * Sets the value of the commissionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommissionType(String value) {
		this.commissionType = value;
	}

	/**
	 * Gets the value of the commissionAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommissionAmt() {
		return commissionAmt;
	}

	/**
	 * Sets the value of the commissionAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommissionAmt(String value) {
		this.commissionAmt = value;
	}

}
