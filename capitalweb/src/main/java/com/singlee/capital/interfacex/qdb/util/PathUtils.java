package com.singlee.capital.interfacex.qdb.util;

import org.springframework.web.context.ContextLoaderListener;

import com.singlee.capital.common.util.JY;


public class PathUtils extends ContextLoaderListener{
	private static String webRootPath = null;
	
	public static String getWebRootPath()
	{
		try {	
			if(webRootPath == null)
			{ 
				webRootPath = getCurrentWebApplicationContext().getServletContext().getRealPath("");
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("获取项目路径出错", e);
		}
		return webRootPath;
	}
	
	public static String getProjectDir(String dir)
	{
		return getCurrentWebApplicationContext().getServletContext().getRealPath("standard/"+dir);
	}
	
	
	public static String getAbsPath(String path)
	{
		return getWebRootPath() + "/" + path;
	}
	
	
}
