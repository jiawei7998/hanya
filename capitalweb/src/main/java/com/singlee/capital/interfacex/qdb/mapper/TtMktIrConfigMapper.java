package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktIrConfig;

public interface TtMktIrConfigMapper extends Mapper<TtMktIrConfig>{
	/**
	 * 查询基准利率数据来源配置表
	 * @param params
	 * @param bounds
	 * @return
	 */
	public Page<TtMktIrConfig> selectTtMktIrConfigPage(Map<String, Object> params,RowBounds bounds);
	
	/**
	 * 新增基准利率数据来源配置表
	 * @param ttMktIrConfig
	 */
	public void insertTtMktIrConfig(TtMktIrConfig ttMktIrConfig);
	
	/**
	 * 修改基准利率数据来源配置表
	 * @param map
	 */
	public void updateTtMktIrConfig(Map<String,Object> map);
	
	/**
	 * 删除基准利率数据来源配置表
	 * @param map
	 */
	public void daleteTtMktIrConfig(Map<String,Object> map);
	
	/**
	 * 查询配置地址
	 * @return
	 */
	public List<TtMktIrConfig> selectTtMktIrConfigList();
}
