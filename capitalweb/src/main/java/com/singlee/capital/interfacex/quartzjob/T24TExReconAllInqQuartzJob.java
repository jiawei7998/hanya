package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.TExReconAllInqRec;
import com.singlee.capital.interfacex.model.TExReconAllInqRq;
import com.singlee.capital.interfacex.model.TExReconAllInqRs;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class T24TExReconAllInqQuartzJob implements CronRunnable {
	/**
	 * 对账查询接口	
	 * IFBM取T24文件	TExReconAllInq	
	 * 请求后轮询	
        1、GTP配置IFBM与T24；
		2、文件命名格式：210-uuid.txt；通配符：210-*.txt
		3、同业目录定义为：/share_ifbm/gtp/T24/recv/
		4、分隔符号：“|”
		5、案例参见：199-GMT-00000000000-20170508R0010012.txt
		6、接收：/FileEXG/RECVIN/BFEII/BFE   发送/gtp/files/T24/BOSEXG/SND 
		7、sit：10.240.2.42/43   uat:10.240.2.15/16 
	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private SocketClientService socketClientService = SpringContextHolder.getBean("socketClientService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		
		try {
				int innerCount = 0;
				TExReconAllInqRq request = new TExReconAllInqRq();
				CommonRqHdr hdr = new CommonRqHdr();
				hdr.setRqUID(UUID.randomUUID().toString());
				hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
				hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
				request.setCommonRqHdr(hdr);
				request.setTradeDt(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				request.setChannelId(InterfaceCode.TI_CHANNELNO);
				request.setFBID(InterfaceCode.TI_FBID);
				request.setTxnType(InterfaceCode.TI_IFBM0001);
				request.setMode("F");
				
				TExReconAllInqRs response = socketClientService.t24TexReconAllInqRequest(request);
				if(response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
					//210-3e30b10e-8b99-40fc-a9fd-6fbefb719355.txt
					String filename = "";
					if(response.getTExReconAllInqRec().size()>0){
						filename = response.getTExReconAllInqRec().get(0).getFileName();
						for(innerCount = 0;innerCount<11;innerCount++){
							//将数据插入DB并返回记录
							List<TExReconAllInqRec>  reconAllList = gtpFileService.GtpIfbmT24TExReconAllInqRead(filename);
							if(reconAllList.size() > 0){
								break;
							}else{
								//文件未读到循环等待
								Thread.sleep(5*60*1000);
							}
						}
						if(innerCount==5){System.out.println("========未从GTP目录读取到文件==========");}
					}
				}else{
					System.out.println("========T24对账文件请求-->本次请求未成功!==========");
				}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		
		}
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
