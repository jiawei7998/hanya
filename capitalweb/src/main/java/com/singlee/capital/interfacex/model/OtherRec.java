package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OtherRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OtherRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OtherAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OtherCust" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherRec", propOrder = { "otherAcctNo", "otherCust" })
public class OtherRec {

	@XmlElement(name = "OtherAcctNo", required = true)
	protected String otherAcctNo;
	@XmlElement(name = "OtherCust", required = true)
	protected String otherCust;

	/**
	 * Gets the value of the otherAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOtherAcctNo() {
		return otherAcctNo;
	}

	/**
	 * Sets the value of the otherAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOtherAcctNo(String value) {
		this.otherAcctNo = value;
	}

	/**
	 * Gets the value of the otherCust property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOtherCust() {
		return otherCust;
	}

	/**
	 * Sets the value of the otherCust property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOtherCust(String value) {
		this.otherCust = value;
	}

}
