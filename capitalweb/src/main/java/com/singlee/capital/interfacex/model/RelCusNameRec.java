package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RelCusNameRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RelCusNameRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Relationship" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FrozenNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RelCusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelCusNameRec", propOrder = { "relationship", "frozenNo",
		"relCusName" })
public class RelCusNameRec {

	@XmlElement(name = "Relationship", required = true)
	protected String relationship;
	@XmlElement(name = "FrozenNo", required = true)
	protected String frozenNo;
	@XmlElement(name = "RelCusName", required = true)
	protected String relCusName;

	/**
	 * Gets the value of the relationship property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * Sets the value of the relationship property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelationship(String value) {
		this.relationship = value;
	}

	/**
	 * Gets the value of the frozenNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFrozenNo() {
		return frozenNo;
	}

	/**
	 * Sets the value of the frozenNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFrozenNo(String value) {
		this.frozenNo = value;
	}

	/**
	 * Gets the value of the relCusName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelCusName() {
		return relCusName;
	}

	/**
	 * Sets the value of the relCusName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelCusName(String value) {
		this.relCusName = value;
	}

}
