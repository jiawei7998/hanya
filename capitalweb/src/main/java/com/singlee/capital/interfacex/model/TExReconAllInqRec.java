package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExReconAllInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExReconAllInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FBID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TradeDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BranchNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExReconAllInqRec", propOrder = { "fbid", "mode", "fileName",
		"transId", "transRef", "channelId", "txnType", "amount", "tradeDt",
		"txnStatus", "branchNo", "txnResult", "amtCr", "amtDr", "feeBaseLogId" })
public class TExReconAllInqRec {

	@XmlElement(name = "FBID", required = true)
	protected String fbid;
	@XmlElement(name = "Mode", required = true)
	protected String mode;
	@XmlElement(name = "FileName", required = true)
	protected String fileName;
	@XmlElement(name = "TransId", required = true)
	protected String transId;
	@XmlElement(name = "TransRef", required = true)
	protected String transRef;
	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "Amount", required = true)
	protected String amount;
	@XmlElement(name = "TradeDt", required = true)
	protected String tradeDt;
	@XmlElement(name = "TxnStatus", required = true)
	protected String txnStatus;
	@XmlElement(name = "BranchNo", required = true)
	protected String branchNo;
	@XmlElement(name = "TxnResult", required = true)
	protected String txnResult;
	@XmlElement(name = "AmtCr", required = true)
	protected String amtCr;
	@XmlElement(name = "AmtDr", required = true)
	protected String amtDr;
	@XmlElement(name = "FeeBaseLogId", required = true)
	protected String feeBaseLogId;

	/**
	 * Gets the value of the fbid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFBID() {
		return fbid;
	}

	/**
	 * Sets the value of the fbid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFBID(String value) {
		this.fbid = value;
	}

	/**
	 * Gets the value of the mode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Sets the value of the mode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMode(String value) {
		this.mode = value;
	}

	/**
	 * Gets the value of the fileName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the value of the fileName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFileName(String value) {
		this.fileName = value;
	}

	/**
	 * Gets the value of the transId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransId() {
		return transId;
	}

	/**
	 * Sets the value of the transId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransId(String value) {
		this.transId = value;
	}

	/**
	 * Gets the value of the transRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransRef() {
		return transRef;
	}

	/**
	 * Sets the value of the transRef property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransRef(String value) {
		this.transRef = value;
	}

	/**
	 * Gets the value of the channelId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the value of the channelId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelId(String value) {
		this.channelId = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the tradeDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTradeDt() {
		return tradeDt;
	}

	/**
	 * Sets the value of the tradeDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTradeDt(String value) {
		this.tradeDt = value;
	}

	/**
	 * Gets the value of the txnStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnStatus() {
		return txnStatus;
	}

	/**
	 * Sets the value of the txnStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnStatus(String value) {
		this.txnStatus = value;
	}

	/**
	 * Gets the value of the branchNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBranchNo() {
		return branchNo;
	}

	/**
	 * Sets the value of the branchNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBranchNo(String value) {
		this.branchNo = value;
	}

	/**
	 * Gets the value of the txnResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnResult() {
		return txnResult;
	}

	/**
	 * Sets the value of the txnResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnResult(String value) {
		this.txnResult = value;
	}

	/**
	 * Gets the value of the amtCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtCr() {
		return amtCr;
	}

	/**
	 * Sets the value of the amtCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtCr(String value) {
		this.amtCr = value;
	}

	/**
	 * Gets the value of the amtDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtDr() {
		return amtDr;
	}

	/**
	 * Sets the value of the amtDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtDr(String value) {
		this.amtDr = value;
	}

	/**
	 * Gets the value of the feeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeBaseLogId() {
		return feeBaseLogId;
	}

	/**
	 * Sets the value of the feeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeBaseLogId(String value) {
		this.feeBaseLogId = value;
	}

}
