package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.JobExcuTerm;

public interface TiJobExcuTermMapper extends Mapper<JobExcuTerm>{
		
	/**
	 * 根据id查询时间窗口信息
	 * @param map
	 * @return
	 */
	public List<JobExcuTerm> selectJobExcuTerm(Map<String, Object> map);

	
	
	
}
