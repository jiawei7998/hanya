package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.ESB.util.DXPT01;
import com.singlee.capital.interfacex.qdb.mapper.MessageMapper;
import com.singlee.capital.interfacex.qdb.mapper.StockPriceMapper;
import com.singlee.capital.interfacex.qdb.model.Message;
import com.singlee.capital.interfacex.qdb.model.TtStockPrice;
import com.singlee.capital.interfacex.qdb.service.MessageService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TaUserParamMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TaUserParam;
import com.singlee.capital.trade.mapper.TdBaseAssetMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service("messageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MessageServiceImpl implements MessageService{

	public static int count1 = 1;
	static FTPClient ftp = new FTPClient();
	public static String time="";
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	@Resource
	private MessageMapper messageMapper;
	@Resource
	private TdBaseAssetMapper tdBaseAssetMapper;
	@Resource
	private TaUserParamMapper paramMapper;
	@Autowired
	private StockPriceMapper stockMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private TaUserMapper taUserMapper;
	
	private String msgEsbUrl = null;
	private String ftpPath = null;
	private String ftpAddr = null;
	private int ftpPort;
	private String ftpUserName = null;
	private String ftpPassWord = null;
	private String ftpTxtPath = null;
	private String charSet = null;
	public final static Logger logger = LoggerFactory.getLogger("FIBS");

	{
		msgEsbUrl   = PropertiesUtil.getProperties("MESSAGE.msgEsbUrl");
		ftpPath     = PropertiesUtil.getProperties("MESSAGE.ftpPath");
		ftpAddr     = PropertiesUtil.getProperties("MESSAGE.ftpAddr");
		ftpPort     = Integer.parseInt(PropertiesUtil.getProperties("MESSAGE.ftpPort"));
		ftpUserName = PropertiesUtil.getProperties("MESSAGE.ftpUserName");
		ftpPassWord = PropertiesUtil.getProperties("MESSAGE.ftpPassWord");
		ftpTxtPath  = PropertiesUtil.getProperties("MESSAGE.ftpTxtPath");
		charSet     = PropertiesUtil.getProperties("MESSAGE.charSet");
		if(charSet == null || "".equals(charSet)){
			charSet = "GBK";
		}
		
	}
	/****
	 * 
	 *短信平台单条
	  brNo--机构号，objAddr--手机号，msgCont--短信内容
	 */
	@Override
	public boolean sendDXPT01ReqtMsg(String brNo, String objAddr,String msgCont) {
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader reader = new SAXReader();
		reader.setEncoding("UTF-8");
		boolean res =false;
		try {
			String retpage = new DXPT01().packageDXPT01ReqtMsgSendToTis(brNo,objAddr, msgCont);
			HashMap<String, Object> hashMap = httpClientManagerDao.sendXmlToRequest(msgEsbUrl, retpage);
			if (hashMap.size() == 0 || hashMap == null) {
				logger.info("请求ESB时发生网络异常！请检查网络！");
			} else {
				if ("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))) 
				{
					byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
					org.dom4j.Document xmlDocAccept = reader.read(byteArrayInputStream);
					xmlDocAccept.setXMLEncoding("UTF-8");

					// logger.info("接收TIS返回报文："
					// + XmlFormat.format(xmlDocAccept.asXML()));
					HashMap<String, Object> svcHdr = new HashMap<String, Object>();// svcHdr
					HashMap<String, Object> appBody = new HashMap<String, Object>();// appBody
					HashMap<String, Object> appHdr = new HashMap<String, Object>();// appHdr
					res = getElementList((byte[]) hashMap.get("Bytes"),svcHdr, appHdr, appBody);
				}
			}
		} catch (Exception e) {
			logger.error("MessageServiceImpl--sendDXPT01ReqtMsg:"+e);
		}
		return res;
	}
	
	@SuppressWarnings("unchecked")
	public boolean getElementList(byte[] responseXml,HashMap<String, Object> svcHdr, HashMap<String, Object> appHdr,
			HashMap<String, Object> appBody) throws DocumentException {

		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader reader = new SAXReader();
		reader.setEncoding("UTF-8");
		byteArrayInputStream = new ByteArrayInputStream(responseXml);
		Document xmlDoc = reader.read(byteArrayInputStream);
		xmlDoc.setXMLEncoding("UTF-8");
		List<Element> elements = xmlDoc.getRootElement().elements();
		for (Element element : elements) 
		{
			if ("appHdr".equals(element.getName()))
			{
				String result = getElementList(element);
				if ("00000000".equals(result)) {
					return true;
				} else {
					return false;
				}
			}
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	private String getElementList(Element element) 
	{
		List<Element> elements = element.elements();
		for (int i = 0; i < elements.size(); i++) {
			if ("respCde".equals(StringUtils.trimToEmpty(elements.get(i).getName()))) {
				return elements.get(i).getName();
			}
		}
		return "";

	}
	
	/*****
	 * 
	 * 1.生成文件
	   2.上传
	      字段注释：cusList--客户信息(含客户号或者服务账号，手机号，短信内容)，
	   orgid--机构号，sTime--实时(0)，非实时(1),time--发送时间(yyyyMMdd HH:mm:ss)
	 */
	@Override
	public  HashMap<String, Object> FtpUtil(List<Message> cusList,String orgid, String sTime, String time) {
		HashMap<String, Object> result=null;
		HashMap<String, Object> resultList=null;
		InputStream local=null;
		InputStream locallist=null;
		try {			
			//count--统计发短信次数，tm--当日日期，hTime--时间
			String count = null;
			String tm = null;
			String aTime=null;
			String hTime = null;
			if ("1".equals(sTime)) {
				aTime=time;
				hTime=time.substring(time.length()-8,time.length());
			}else{
				aTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
				hTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
			}
			tm = aTime.substring(0,8);
			method1(tm);
			count = String.valueOf(count1);
			if(count1<10){
				count="0"+count;
			}else if (count1>=100){
				count="01";
			}
			//上传到ftp哪个路径下
			ftp.connect(ftpAddr,ftpPort);
			//登录
            ftp.login(ftpUserName, ftpPassWord);
          //設置為被動模式
            ftp.enterLocalPassiveMode();
            
          //检查上传路径是否存在 如果不存在返回false
	        boolean flagw = ftp.changeWorkingDirectory(ftpTxtPath);
	        if(!flagw){
	           //创建上传的路径  该方法只能创建一级目录，在这里如果/home/ftpuser存在则可创建image
	           ftp.makeDirectory(ftpTxtPath);
	        }
          //指定上传路径
            ftp.changeWorkingDirectory(ftpPath);
            //指定上传文件的类型  二进制文件
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
			//调用createFile方法生成.txt文件       并把文件名返回
			result = createFile(cusList, orgid,tm,hTime, sTime,count);
			//上传。txt文件
			String txtName = result.get("fileName").toString();
			String txtfileName=txtName+".txt";
			File dir = new File(PathUtils.getAbsPath("MessageTemp"));
			if(!dir.exists())
           {
        	   dir.mkdir();
           }
   		    //读取本地文件
            File file = new File(dir.getAbsolutePath() + "\\"+txtfileName);
            local = new FileInputStream(file);
            //第一个参数是文件名
            ftp.storeFile(txtfileName, local);
			//调用createListFile方法生成.list文件			
            resultList = createListFile(txtName);
            String listName = resultList.get("fileName").toString();
            String listfileName = listName+".list";
            File filelist = new File(dir.getAbsolutePath() + "\\"+listfileName);
            locallist = new FileInputStream(filelist);
            //第一个参数是文件名
            ftp.storeFile(listfileName, locallist);
          //关闭文件流
            local.close();
            locallist.close();
            //退出
            ftp.logout();
            //断开连接
            ftp.disconnect();
            deleFile(dir.getAbsolutePath() + "\\",txtfileName);
            deleFile(dir.getAbsolutePath() + "\\",listfileName);
//			//调用createFile方法生成.txt文件       并把文件名返回
//			result = createFile(cusList, orgid,tm,hTime, sTime,count);
//			//调用createListFile方法生成.list文件,并返回路径+文件名
//			String path1=createListFile(result.get("fileName").toString());
//			//上传到ftp哪个路径下
//			connect(ftpPath, ftpAddr, ftpPort, ftpUserName,ftpPassWord);
////			response.reset();
//			//上传文件
//			if(null != ftp){
//				File file = new File(path1);
//				File file1 = new File(result.get("txtPath").toString());
//				upload(file1);
//				upload(file);
//			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("短信平台批量上传文件失败：MessageServiceImpl--FtpUtil:"+e);
		}
		return result;
	}
	
	public static void method1(String tm){
		//同一天发送，则累计计数，
		if(!time.equals(tm)){
			count1 = 0;
		}
		count1++;
		time= tm;
	}
	
	/**
	 * 生成文件.txt文件
	 */
	@SuppressWarnings("deprecation")
	public HashMap<String, Object> createFile(List<Message> cusList, String orgid,String oTime,String hTime, String sTime,String count) {
		FileOutputStream fot = null;
		OutputStreamWriter writer  = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		String fileName = "";
		String fileName2 = "";
		String time1 = hTime.replace(":","").trim();
		File dir = new File(PathUtils.getAbsPath("MessageTemp"));
        if(!dir.exists())
        {
     	   dir.mkdir();
        }
		try {
			if(orgid==null||"".equals(orgid)){
				orgid="80002";
			}
			//文件名命名规则：sms 2位渠道号4位服务品种.6位机构号.8位预约日期.1位实时标志.6位预约时间.2位唯一标识.txt
			fileName = URLEncoder.encode("sms699999.0"+orgid+"."+ oTime + "." + sTime + "."	+ time1 +"."+ count);
			fileName2 = fileName + ".txt";
			
			fot = new FileOutputStream(dir.getAbsolutePath() +"\\"+ fileName2);
			writer = new OutputStreamWriter(fot, charSet);
			
			String texts = "";
			String texts2 = "";
			//客户号和服务账号必须填一个，若没有，可以用“0”代替。手机号和短信内容必填
			for(int i=0;i<cusList.size();i++){
				//手机号
				if(cusList.get(i).getPhone()!=null){
					//客户号为空，则用“0”代替
					if(cusList.get(i).getCifNo()==null||"".equals(cusList.get(i).getCifNo())){
						cusList.get(i).setCifNo("0");
					}
					//账号为空，则用“0”代替
					texts2 =cusList.get(i).getCifNo()+"|0|"+cusList.get(i).getPhone()+"|" + cusList.get(i).getMessage()+"|\r\n";
				}
				texts = texts + texts2;
			}			
			writer.write(new String(texts.getBytes()));
			writer.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("生成.txt文件失败：MessageServiceImpl--createFile:"+e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("生成.txt文件失败：MessageServiceImpl---createFile:"+e);
		} finally {
			if(fot != null){
				try {
					fot.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(writer != null){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		map.put("fileName", fileName);
		map.put("txtPath", ftpTxtPath + fileName2);
		return map;
	}

	
	/**
	 * 生成.list文件
	 */
	public static HashMap<String, Object> createListFile(String fileName) {		
		FileOutputStream fot;
		String fileName1 = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
//		String txtPath=PropertiesUtil.getPropertie("ftpUtil.txtPath");
//		String txtPath="E:\\";
		File dir = new File(PathUtils.getAbsPath("MessageTemp"));
        if(!dir.exists())
        {
     	   dir.mkdir();
        }
		try {
			//文件名
			fileName1 = fileName+".list";
			fot = new FileOutputStream(dir.getAbsolutePath() + "\\" + fileName1);
			String texts = "";
			texts = fileName+".txt";
			fot.write(texts.getBytes());
			fot.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
//			LogManager.getLogger(LogManager.LOGICALSYSTEM_LOG).error("短信平台生成.list文件失败：" + e);
		}
		map.put("fileName", fileName);
		map.put("txtPath", dir.getAbsolutePath() + "\\"+fileName1);
		return map;
	}
	/**
	 * 登陆ftp
	 * @param path
	 *            上传到ftp服务器哪个路径下
	 * @param addr
	 *            地址
	 * @param port
	 *            端口号
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return
	 * @throws Exception
	 */
//	public static boolean connect(String path, String addr, int port, String username,String password) throws Exception {
//		boolean result = false;
//		ftp = new FTPClient();
//		try {
//			ftp.connect(addr, port);
//			ftp.setControlEncoding("GBK");
//			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_NT);
//			conf.setServerLanguageCode("zh");
//			ftp.login(username, password);
//			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
//
//			// make directory
//			if (path != null && !"".equals(path.trim())) {
//				String[] pathes = path.split("/");
//				for (String onepath : pathes) {
//					if (onepath == null || "".equals(onepath.trim())) {
//						continue;
//					}
//					onepath = new String(onepath.getBytes("GBK"), "iso-8859-1");
//					if (!ftp.changeWorkingDirectory(onepath)) {
//						ftp.makeDirectory(onepath);
//						ftp.changeWorkingDirectory(onepath);
//					}
//				}
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("登陆ftp服务器失败：MessageServiceImpl--connect:"+e);
//		}
//
//		result = true;
//		return result;
//	}
//	/**
//	 * 上传文件
//	 * 
//	 * @param txtPath
//	 */
//	public static void upload(File file) {
//		try{
//			if (file.isDirectory()) {
//				ftp.makeDirectory(file.getName());
//				ftp.changeWorkingDirectory(file.getName());
//				String[] files = file.list();
//				for (int i = 0; i < files.length; i++) {
//					File file1 = new File(file.getPath() + "\\" + files[i]);
//					if (file1.isDirectory()) {
//						upload(file1);
//						ftp.changeToParentDirectory();
//					} else {
//						File file2 = new File(file.getPath() + "\\" + files[i]);
//						FileInputStream input = new FileInputStream(file2);
//						ftp.storeFile(new String(file2.getName().getBytes("gbk"),
//								"iso-8859-1"), input);
//						input.close();
//					}
//				}
//			} else {
//				File file2 = new File(file.getPath());
//				FileInputStream input = new FileInputStream(file2);
//				ftp.storeFile(new String(file2.getName().getBytes("gbk"),"iso-8859-1"), input);
//				input.close();
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//			logger.error("短信平台上传文件失败：MessageServiceImpl--upload:"+e);
//		}
//	}
	public static void deleFile(String url,String fileName) {
		//try {
		  //取得RWFileDir目录下的文件
		File file = new File(url+fileName);
        // 判断文件是否存在
        if (file.exists()) {
            // 文件删除
            file.delete();
            System.out.println("删除"+fileName+"文件成功");
        }
	}
	public static void main(String[] args){
		Message cust1 = new Message();
		cust1.setCifNo("000001");
		cust1.setPhone("18712345678");
		Message cust2 = new Message();
		cust2.setCifNo("000002");
		cust2.setPhone("18722345678");
		Message cust3 = new Message();
		cust3.setCifNo("000003");
		cust3.setPhone("18732345678");
		List<Message> cusList =  new ArrayList<Message>();
		cusList.add(cust1);
		cusList.add(cust2);
		cusList.add(cust3);
//		String orgid = "80201";
//		String rmdText = "明天是周二！";
//		String sTime = "0";
     // FtpUtil(cusList,orgid,rmdText,sTime,null);
	}

	@Override
	public List<Message> createMessageInfo() throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		Message message = null;
		//基础资产类型：13 股票
		params.put("assetType", "13");
		List<TdBaseAsset> tdBaseAssets = tdBaseAssetMapper.getAssetAndCust(params);
		TtStockPrice price = null;
		List<Message> messages=new ArrayList<Message>();
		String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
		for (TdBaseAsset tdBaseAsset : tdBaseAssets){
			params.put("importDate", nowdate);
			params.put("stockId", tdBaseAsset.getStockId());
			price = stockMapper.queryStockById(params);
			if(price != null && price.getPrice() != null){
				message = null;
				if(tdBaseAsset.getCoverLinePrice() >= price.getPrice()){
					message = new Message();
					getMessage(message,tdBaseAsset);
					message.setType("1");
					message.setMessage("你好，"+tdBaseAsset.getStockId()+"的价格已低于或等于补仓线(非标系统)");
						
				} else if(tdBaseAsset.getWarnLinePrice() >= price.getPrice()){
					message = new Message();
					getMessage(message,tdBaseAsset);
					message.setType("1");
					message.setMessage("你好，"+tdBaseAsset.getStockId()+"的价格已低于或等于警戒线(非标系统)");
					
				} else if(tdBaseAsset.getOpenLinePrice() <= price.getPrice()){
					message = new Message();
					getMessage(message,tdBaseAsset);
					message.setType("1");
					message.setMessage("你好，"+tdBaseAsset.getStockId()+"的价格已高于或等于平仓线(非标系统)");
				}
				if(message!=null){
					messages.add(message);
				}
			}		
		}
		//债券短信提醒
		Map<String, Object> paramsZhaiQuan = new HashMap<String, Object>();
		TaUserParam param = new TaUserParam();
		param.setP_name("bondDate");
		param = paramMapper.searchTaUserParam(param);
		if(param == null){
			paramsZhaiQuan.put("bondDate",10);
		}else{
			paramsZhaiQuan.put("bondDate",param.getValue());
		}
		/***
		 * 
		11	货币市场基金
		1	理财
		9	两融收益权
		8	股票质押式回购
		7	资产证券化
		6	资产转让
		5	贷款
		4	票据
		20	央行票据
		19	商业银行债
		18	地方政府债
		17	国债
		16	利率债券
		15	信用债券
		2	存款
		21	货币市场
		12	债券型基金
		13	股票
		14	其他
		10	收益凭证
		 */
		String[] assetTypes = {"19","18","17","16","15"};
		paramsZhaiQuan.put("assetTypes",assetTypes);
		paramsZhaiQuan.put("sysdate",DayendDateServiceProxy.getInstance().getSettlementDate());
		List<TdBaseAsset> tdBaseAssets2 = tdBaseAssetMapper.getAssetAndCust(paramsZhaiQuan);
		for (TdBaseAsset tdBaseAsset : tdBaseAssets2) 
		{
			message = null;
			if(tdBaseAsset.getSecNm() == null || "".equals(tdBaseAsset.getSecNm())){
				message = new Message();
				getMessage(message,tdBaseAsset);
				message.setType("2");
				message.setMessage("你好，您的债券即将到行权日期(非标系统)");
				
			}else{
				message = new Message();
				getMessage(message,tdBaseAsset);
				message.setType("2");
				message.setMessage("你好，"+tdBaseAsset.getSecNm()+"即将到行权日期(非标系统)");
				
			}
			if(message!=null){
				messages.add(message);
			}	
		}
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("postDate", nowdate);
		messageMapper.deleteAll(map);
		messageMapper.insertMessage(messages);
		FtpUtil(messages, "80002", "0", null);
		return messages;
	}
	
	public void getMessage(Message message, TdBaseAsset tdBaseAsset){
		if(tdBaseAsset!=null){
			String dealNo = tdBaseAsset.getDealNo();
			message.setDealNo(dealNo);
			String nowdate=DayendDateServiceProxy.getInstance().getSettlementDate();
			message.setPostDate(nowdate);
			
			TdProductApproveMain main = tdProductApproveMainMapper.getProductApproveMainActivated(dealNo);
			if(main != null){
				TaUser user = taUserMapper.selectUser(main.getSponsor());
				if(user!=null){
					message.setPhone(user.getUserCellphone());
				}	
			}
		}
		message.setCifNo(tdBaseAsset.getCnm());
		message.setCnName("");
		message.setFlg("0");
	}

	@Override
	public Page<Message> searchMessagePage(Map<String, Object> map) {
		return messageMapper.searchMessagePage(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<Message> createBondMessage() {
		Map<String, Object> params = new HashMap<String, Object>();
		Message message=null;
		TaUserParam param=new TaUserParam();
		param.setP_name("bondDate");
		param=paramMapper.searchTaUserParam(param);
		params.put("bondDate",param.getValue());
		List<TdBaseAsset> tdBaseAssets = tdBaseAssetMapper.getAssetAndCust(params);
		List<Message> messages=new ArrayList<Message>();
		for (TdBaseAsset tdBaseAsset : tdBaseAssets) 
		{
			message=new Message();
			getMessage(message,tdBaseAsset);
			message.setType("2");
			message.setMessage("你好，"+tdBaseAsset.getSecNm()+"即将到行权日期");
			if(message!=null){
				messages.add(message);
			}		
		}		
		//messageMapper.deleteAll("2");
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("type", message.getType());
		messageMapper.deleteAll(map);
		messageMapper.insertMessage(messages);
		FtpUtil(messages, "", "", "");
		return messages;
	}

	@Override
	public boolean updateMessageFlag(Map<String, Object> params) {
		return messageMapper.updateMessageFlag(params) > 0;
	}

}
