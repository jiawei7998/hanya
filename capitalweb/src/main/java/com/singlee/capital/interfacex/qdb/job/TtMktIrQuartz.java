package com.singlee.capital.interfacex.qdb.job;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.qdb.service.TtMktIrService;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

public class TtMktIrQuartz implements CronRunnable{
	private TtMktIrService ttMktIrService = SpringContextHolder.getBean("ttMktIrService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		String paths = PropertiesUtil.getProperties("marketData.ir.path");
		ttMktIrService.importIrByExcel(paths, null);
		return true;
	}

	@Override
	public void terminate() {
	}

}
