package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcActModRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcActModRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="SerialNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SettleMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Stdcollleftlamt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeCalType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntRateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TreasuryRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateFloat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CrMaxRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CrIntRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CMinPayAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcActModRs", propOrder = { "commonRsHdr", "serialNum",
		"settleMode", "stdcollleftlamt", "feeCalType", "intRateType",
		"treasuryRate", "rateFloat", "crMaxRate", "crIntRate", "cMinPayAmt" })
public class TDpAcAciModRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "SerialNum", required = true)
	protected String serialNum;
	@XmlElement(name = "SettleMode", required = true)
	protected String settleMode;
	@XmlElement(name = "Stdcollleftlamt", required = true)
	protected String stdcollleftlamt;
	@XmlElement(name = "FeeCalType", required = true)
	protected String feeCalType;
	@XmlElement(name = "IntRateType", required = true)
	protected String intRateType;
	@XmlElement(name = "TreasuryRate", required = true)
	protected String treasuryRate;
	@XmlElement(name = "RateFloat", required = true)
	protected String rateFloat;
	@XmlElement(name = "CrMaxRate", required = true)
	protected String crMaxRate;
	@XmlElement(name = "CrIntRate", required = true)
	protected String crIntRate;
	@XmlElement(name = "CMinPayAmt", required = true)
	protected String cMinPayAmt;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the serialNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSerialNum() {
		return serialNum;
	}

	/**
	 * Sets the value of the serialNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSerialNum(String value) {
		this.serialNum = value;
	}

	/**
	 * Gets the value of the settleMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettleMode() {
		return settleMode;
	}

	/**
	 * Sets the value of the settleMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettleMode(String value) {
		this.settleMode = value;
	}

	/**
	 * Gets the value of the stdcollleftlamt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStdcollleftlamt() {
		return stdcollleftlamt;
	}

	/**
	 * Sets the value of the stdcollleftlamt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStdcollleftlamt(String value) {
		this.stdcollleftlamt = value;
	}

	/**
	 * Gets the value of the feeCalType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeCalType() {
		return feeCalType;
	}

	/**
	 * Sets the value of the feeCalType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeCalType(String value) {
		this.feeCalType = value;
	}

	/**
	 * Gets the value of the intRateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntRateType() {
		return intRateType;
	}

	/**
	 * Sets the value of the intRateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntRateType(String value) {
		this.intRateType = value;
	}

	/**
	 * Gets the value of the treasuryRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTreasuryRate() {
		return treasuryRate;
	}

	/**
	 * Sets the value of the treasuryRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTreasuryRate(String value) {
		this.treasuryRate = value;
	}

	/**
	 * Gets the value of the rateFloat property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRateFloat() {
		return rateFloat;
	}

	/**
	 * Sets the value of the rateFloat property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRateFloat(String value) {
		this.rateFloat = value;
	}

	/**
	 * Gets the value of the crMaxRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCrMaxRate() {
		return crMaxRate;
	}

	/**
	 * Sets the value of the crMaxRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCrMaxRate(String value) {
		this.crMaxRate = value;
	}

	/**
	 * Gets the value of the crIntRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCrIntRate() {
		return crIntRate;
	}

	/**
	 * Sets the value of the crIntRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCrIntRate(String value) {
		this.crIntRate = value;
	}

	/**
	 * Gets the value of the cMinPayAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCMinPayAmt() {
		return cMinPayAmt;
	}

	/**
	 * Sets the value of the cMinPayAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCMinPayAmt(String value) {
		this.cMinPayAmt = value;
	}

}
