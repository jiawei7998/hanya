package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdRateChangeMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdRateChange;
@Service("durationRateChangeServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintDurationRateChangeServiceImpl implements AssemblyDataService{
	@Autowired
	private FlowController flowController; 
	@Autowired
	private TdRateChangeMapper tdRateChangeMapper;
	@Autowired
	private TtInstitutionMapper mapper;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Autowired
	private DayendDateService dayendDateService;
	
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			NumberFormat n = NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			TdRateChange ch=tdRateChangeMapper.getRateChangeById(id);
			String dealNo=ch.getRefNo();
			map.put("dealNo", dealNo);
			TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
			map.put("dealNo",td.getDealNo());
			map.put("contractRate", df.format(100*ch.getRate()));
			map.put("vDate", td.getvDate());
			map.put("mDate",td.getmDate());
			map.put("dealDate",td.getDealDate());
			String cno=td.getcNo();
			CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
			map.put("party_name", cp.getParty_name());
			Integer pro=td.getPrdNo();
			if(pro!=null&&!"".equals(pro)){
				TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
				map.put("productName", p.getPrdName());
			}
			//取原交易的经办人
			String Sponsor2=td.getSponsor();
			TaUser u=taUserMapper.selectUser(Sponsor2);
			map.put("sponsor",u.getUserName());
			
			
			//取存续期的经办人
			String Sponsor=ch.getSponsor();
			TaUser u2=taUserMapper.selectUser(Sponsor);
			map.put("sponsor2",u2.getUserName());
			String inst=u.getInstId();
			
			TtInstitution institution = new TtInstitution();
			if(inst!=null&&!"".equals(inst)){
				institution.setInstId(inst);
				institution = mapper.selectByPrimaryKey(institution);
				map.put("Inst",institution.getInstName());
			}	
			String rateType=td.getRateType();
			if("1".endsWith(rateType)){
				map.put("rateType", "固息");
			}else if("2".endsWith(rateType)){
				map.put("rateType", "浮息");
			}
			map.put("amt", n.format(td.getAmt()));
			map.put("vDate", td.getvDate());
			map.put("mDate", td.getmDate());
			map.put("SysDate", dayendDateService.getSettlementDate());
			map.put("changeRate",df.format(100*ch.getChangeRate()));
			String intType=td.getIntType();
			if("1".endsWith(intType)){
				map.put("intType", "先收息");
			}else if("2".endsWith(intType)){
				map.put("intType", "后收息");
			}
			map.put("effectDate", ch.getEffectDate());
			map.put("changeDate", ch.getChangeDate());
			map.put("changeReason", ch.getChangeReason());
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}

				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

}
