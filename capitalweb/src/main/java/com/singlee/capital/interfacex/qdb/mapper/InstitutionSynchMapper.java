package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.system.model.TtInstitution;

public interface InstitutionSynchMapper extends Mapper<TtInstitution>{
	
	public void updateInstPId(Map<String,String> map);
	/**
	 * 更新ta_user_inst_map 用户和机构的关系表
	 * @param map
	 */
	public void updateUserInstMap(Map<String,String> map);
	/**
	 * 新增ta_user_inst_map 用户和机构的关系表
	 * @param map
	 */
	public void insertUserInstMap(Map<String,String> map);
	
	public void insertESB(List<TtInstitution> list);
	
	public void deleteESB();
	
	
	
	

}
