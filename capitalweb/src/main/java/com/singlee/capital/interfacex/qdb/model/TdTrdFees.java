package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

@Entity
@Table(name = "TD_TRD_FEES")
public class TdTrdFees extends CashflowInterest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private double actualRamt;//实际收到利息
	@Transient
	private String actualDate;//本区间段已计提至日期
	private String cashIntfee;//收取频率
	private String cashtDate;//扣划日期
	private String cashBank;//托管行
	private String cfBase;//是否是底层资产
	private String feeType;//利率类型
	private Double feesAmt;//利率类型
	private String cfName;//底层资产名称
	
	public Double getFeesAmt() {
		return feesAmt;
	}

	public void setFeesAmt(Double feesAmt) {
		this.feesAmt = feesAmt;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getCashBank() {
		return cashBank;
	}

	public void setCashBank(String cashBank) {
		this.cashBank = cashBank;
	}


	public String getCashIntfee() {
		return cashIntfee;
	}

	public void setCashIntfee(String cashIntfee) {
		this.cashIntfee = cashIntfee;
	}

	public String getCashtDate() {
		return cashtDate;
	}

	public void setCashtDate(String cashtDate) {
		this.cashtDate = cashtDate;
	}

	public String getCfBase() {
		return cfBase;
	}

	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	
	public String getCfName() {
		return cfName;
	}

	public void setCfName(String cfName) {
		this.cfName = cfName;
	}

	public double getActualRamt() {
		return actualRamt;
	}
	public void setActualRamt(double actualRamt) {
		this.actualRamt = actualRamt;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
}
