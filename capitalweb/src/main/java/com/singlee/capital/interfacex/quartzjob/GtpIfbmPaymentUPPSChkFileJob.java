package com.singlee.capital.interfacex.quartzjob;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRq;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRs;
import com.singlee.capital.interfacex.model.UPPSChkFileEndDayBatch;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class GtpIfbmPaymentUPPSChkFileJob implements CronRunnable {

	
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private SocketClientService socketClientService = SpringContextHolder.getBean("socketClientService");
	
	private DictionaryGetService dictionaryGetService = SpringContextHolder.getBean("dictionaryGetService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		try {
				String filebak  = dictionaryGetService.getTaDictByCodeAndKey("ifbmPayGtpChkFile", "03").getDict_value();
				String prefix   = dictionaryGetService.getTaDictByCodeAndKey("ifbmPayGtpChkFile", "04").getDict_value();
				File bakFile = new File(filebak +"/"+ prefix +new SimpleDateFormat("yyyyMMdd").format(new Date()).toString()+".txt");
				if(bakFile.isFile()){
					System.out.println("已进行过对账");
				}else {
					int innerCount = 0;
					UPPSChkFileAplRq request = new UPPSChkFileAplRq();
					CommonRqHdr hdr = new CommonRqHdr();
					hdr.setRqUID(UUID.randomUUID().toString());
					hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
					
					//暂为当日00:00--18:00 请求二代支付对账文件为昨天的数据，18:00--23:59请求的为今天的数据
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date yesterday = sdf.parse(new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(new Date()));
					Date today = sdf.parse(new SimpleDateFormat("yyyy-MM-dd 18:00:00").format(new Date()));
					Date currentTime = sdf.parse(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					
				    if(currentTime.before(today)&&currentTime.after(yesterday)){
						hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(System.currentTimeMillis()-24*60*60*1000));
				    }else{
				    	hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				    }
				    hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
					request.setCommonRqHdr(hdr);
					request.setChannelId(InterfaceCode.TI_CHANNELNO);
					request.setClearDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
					request.setPayPathCode(InterfaceCode.TI_PAYPATHCODE1);
					request.setUserDefineTranCode(InterfaceCode.TI_UserDefineTranCode);
		
					
					UPPSChkFileAplRs response = socketClientService.T2ndPaymentUPPSChkFileAplRequest(request);
					if (response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)) {
						if (response.getDealCode().equals(InterfaceCode.TI_DEALCODE1)) {
							// S89_
							System.out.println("========受理成功==========");
							//String filename = "";
							if (!("".equals(response.getFileName())&& response.getFileName() == null)) {
								//filename = response.getFileName();
								
								for(innerCount = 0;innerCount<11;innerCount++){
									// 将数据插入DB并返回记录
									List<UPPSChkFileEndDayBatch> uPPSChkFileEndDayBatch = gtpFileService.GtpIfbmPaymentUPPSChkFileAplRead();
									if (uPPSChkFileEndDayBatch.size() > 0) {
										break;
									} else {
										// 文件未读到循环等待
										Thread.sleep(5 * 60 * 1000);
									}
							   }
							   if(innerCount==12){System.out.println("========未从GTP目录读取到文件==========");}
							} 
							
						}else{
							if(response.getDealCode().equals(InterfaceCode.TI_DEALCODE2)){
								System.out.println("========节假日不对账==========");
							}else if(response.getDealCode().equals(InterfaceCode.TI_DEALCODE3)){
								System.out.println("========人行未对账==========");
							}else{
								System.out.println("========人行正在对账中==========");
							}
						}
					}else{
						System.out.println("========二代支付对账文件-->本次请求未成功！==========");
					}
				}
		}catch(Exception e){
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
