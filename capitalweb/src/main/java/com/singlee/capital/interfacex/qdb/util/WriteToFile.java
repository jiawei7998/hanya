package com.singlee.capital.interfacex.qdb.util;

import java.io.File;
import java.io.FileOutputStream;

public class WriteToFile {

	/**
	 * 写文件
	 * @param data     写入文件数据
	 * @param path     路径
	 * @param filename 文件名称
	 * @param date     日期（120725）
	 * @throws Exception
	 */
	public synchronized static void writeToFile(String data,String path, String filename, String date) throws Exception{
		
		
		//新建文件路径 
		String newPath = path+date+"/";  
		File newFilePath = new File(newPath);
		//判断文件路径是否存在
		if(!newFilePath.exists()){
			newFilePath.mkdirs();
		}
		//判断文件是否存在
		File newFile = new File(newPath+filename);
		if(newFile.exists()){
			newFile.delete();
		}
		//开始写文件
		FileOutputStream writer = null ;
		try{
			writer = new FileOutputStream (newFile);
			writer.write(data.getBytes("UTF-8"));
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}finally{
			writer.close();
		}
		
	}
}
