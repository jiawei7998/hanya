package com.singlee.capital.interfacex.qdb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.Message;

/****
 * 
 * 短信平台接口
 * @author singlee
 *
 */
public interface MessageService {
	/**
	 * 短信平台单条
	 * @param brNo--机构号
	 * @param objAddr--手机号
	 * @param msgCont--短信内容
	 * @return
	 */
	public boolean sendDXPT01ReqtMsg(String brNo, String objAddr, String msgCont);
	/**
	 * 短信平台上传ftp
	 * @param cusList--客户信息(含客户号或者服务账号，手机号)
	 * @param orgid--机构号
	 * @param rmdText--短信内容
	 * @param sTime--实时(0)，非实时(1)
	 * @param time--发送时间(yyyyMMdd HH:mm:ss)
	 * @return
	 */
	public  HashMap<String, Object> FtpUtil(List<Message> cusList,String orgid,String sTime,String time);

	
	public List<Message> createMessageInfo()throws Exception;
	
	public List<Message> createBondMessage();
	
	public Page<Message> searchMessagePage(Map<String, Object> params);
	
	public boolean updateMessageFlag(Map<String, Object> params);
	
}
