package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.ordersorting.mapper.OrderSortingDetailMapper;
import com.singlee.capital.ordersorting.mapper.TdOrdersortingApproveMapper;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
@Service("orderSortingHBJHServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintOrderSortingHBJHServiceImpl implements AssemblyDataService{
	@Autowired
	private TdOrdersortingApproveMapper tdOrdersortingApproveMapper;
	@Autowired
	private FlowController flowController; 
	
	@Autowired
	private TtInstitutionMapper mapper;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Autowired
	private OrderSortingDetailMapper orderSortingDetailMapper;
	
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			NumberFormat n = NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			if(id!=null&&!"".equals(id)){
				Map<String, Object> params=new HashMap<String,Object>();
				params.put("dealNo", id);
				map.put("dealNoNew", id);
				
				List<OrderSortingDetail> list=orderSortingDetailMapper.getOrderSortingDetail(params);
				map.put("list",list);
				TdOrdersortingApprove order=tdOrdersortingApproveMapper.getTdOrdersortingApproveById(params); 
				if(order!=null){
					String refno=order.getRefNo();
					map.put("dealNoOld", refno);
					map.put("dealNo", refno);
					TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
					map.put("contractRate", df.format(100*td.getContractRate()));
					map.put("vDate", td.getvDate());
					map.put("tDate", td.gettDate());
					map.put("mDate",td.getmDate());
					map.put("meDate",td.getMeDate());
					map.put("Adate",order.getaDate());
					map.put("dealDate",td.getDealDate());
					map.put("term",td.getTerm());
					map.put("occupTerm",td.getOccupTerm());
					
					String amtFre=td.getAmtFre();
					if(amtFre!=null && !"".equals(amtFre)) {
						if("0".endsWith(amtFre)){
							map.put("amtFre", "到期一次性");
						}else if("1".endsWith(amtFre)){
							map.put("amtFre", "到期一次性");
						}else if("4".endsWith(amtFre)){
							map.put("amtFre", "季");
						}else if("12".endsWith(amtFre)){
							map.put("amtFre", "月");
						}else if("2".endsWith(amtFre)){
							map.put("amtFre", "半年");
						}else if("-1".endsWith(amtFre)){
							map.put("amtFre", "无");
						}
					}
					
					String intFre=td.getIntFre();
					if(intFre!=null && !"".equals(intFre)) {
						if("0".endsWith(intFre)){
							map.put("intFre", "到期一次性");
						}else if("1".endsWith(intFre)){
							map.put("intFre", "到期一次性");
						}else if("4".endsWith(intFre)){
							map.put("intFre", "季");
						}else if("12".endsWith(intFre)){
							map.put("intFre", "月");
						}else if("2".endsWith(intFre)){
							map.put("intFre", "半年");
						}else if("-1".endsWith(intFre)){
							map.put("intFre", "无");
						}
					}
					
					map.put("mInt", n.format(td.getmInt()));
					map.put("mAmt", n.format(td.getmAmt()));
					String cno=td.getcNo();
					CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
					map.put("party_name", cp.getParty_name());
					Integer pro=td.getPrdNo();
					if(pro!=null&&!"".equals(pro)){
						TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
						map.put("productName", p.getPrdName());
					}
					//取原交易的经办人
					String Sponsor2=td.getSponsor();
					TaUser u=taUserMapper.selectUser(Sponsor2);
					//map.put("sponsor",u.getUserName());
					
					
					
					//取存续期的经办人
					String Sponsor=order.getSponsor();
					TaUser u2=taUserMapper.selectUser(Sponsor);
					map.put("sponsor",u2.getUserName());
					map.put("sponsor2",u2.getUserName());
					String inst=u.getInstId();
					map.put("userCellphone",u.getUserCellphone());
					
					TtInstitution institution = new TtInstitution();
					if(inst!=null&&!"".equals(inst)){
						institution.setInstId(inst);
						institution = mapper.selectByPrimaryKey(institution);
						map.put("Inst",institution.getInstName());
					}	
					String rateType=td.getRateType();
					if("1".endsWith(rateType)){
						map.put("rateType", "固息");
					}else if("2".endsWith(rateType)){
						map.put("rateType", "浮息");
					}
					map.put("amt", n.format(td.getAmt()));
					map.put("basis", td.getBasis());
					map.put("mInt", n.format(td.getmInt()));
					map.put("mAmt", n.format(td.getmAmt()));
					map.put("SysDate", DayendDateServiceProxy.getInstance().getSettlementDate());
					
				}
			}
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}

				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

}
