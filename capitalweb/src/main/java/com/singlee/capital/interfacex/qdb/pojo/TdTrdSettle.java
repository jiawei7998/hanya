package com.singlee.capital.interfacex.qdb.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TD_TRD_SETTLE")
public class TdTrdSettle implements Serializable {

	private static final long serialVersionUID = 1L;
	private String dealNo;//流水号
	private String product;// 产品代码
	private String prodType;//产品类型
	private String refNo;//原交易流水号
	private String seq;//分序号
	private String server;//来源
	private String cno;//交易对手号
	private String vdate;//付款日期
	private String payrecnd;//收付方向
	private String ccy;// 币种
	private BigDecimal amount;//金额
	private String setmeans;//清算方式
	private String setacct;// 清算账户
	private String payBankId;//付款行行号
	private String payBankName;//付款行行名
	private String payUserId;//付款账号
	private String payUserName;//付款账号名称
	private String recBankId;//收款行行号
	private String recBankName;// 收款行行名
	private String recUserId;//收款账号
	private String recUserName;//收款账号名称
	private String institution;//营业机构号
	private String hvpType;//业务种类(10-现金汇款,11-普通汇兑,12-网银支付...)
	private String hvpType1;//汇兑类型(0-正常)
	private String pzType;//凭证种类
	private String hurryLevel;//加急等级
	private String remarkFy;//附言
	private String remarkYt;
	private String dealFlag;//处理状态(0,1,2,3,4,5,6,8,X)
	private String voidFlag;//撤销标志(0-申请撤销,1-撤销成功,2-撤销失败)
	private String settFlag;// 清算标志
	private String reason;//原因
	private String ioper;//经办人员
	private String itime;// 经办时间
	private String voper;//复核人员
	private String vtime;//复核时间
	private String backTime;//退回时间
	private String retCode;//返回码
	private String retMsg;//返回信息
	private String userDealNo;// 柜员流水号
	private String busDealNo;//业务编号
	private String inputTime;//接口数据插入时间
	private String note;//备注
	private String ssno; //证件号
	private String sscd; //证件类型
	private String cidt; //证件发行国家
	private String clas; //客户类型
	private String dealUser;//当前操作用户
	private String dealTime;//当前操作时间
	private String custName;//客户名称
	
	public String getBackTime() {
		return backTime;
	}
	public void setBackTime(String backTime) {
		this.backTime = backTime;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getDealUser() {
		return dealUser;
	}
	public void setDealUser(String dealUser) {
		this.dealUser = dealUser;
	}
	public String getDealTime() {
		return dealTime;
	}
	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}
	public String getSsno() {
		return ssno;
	}
	public void setSsno(String ssno) {
		this.ssno = ssno;
	}
	public String getSscd() {
		return sscd;
	}
	public void setSscd(String sscd) {
		this.sscd = sscd;
	}
	public String getCidt() {
		return cidt;
	}
	public void setCidt(String cidt) {
		this.cidt = cidt;
	}
	public String getClas() {
		return clas;
	}
	public void setClas(String clas) {
		this.clas = clas;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getPayrecnd() {
		return payrecnd;
	}
	public void setPayrecnd(String payrecnd) {
		this.payrecnd = payrecnd;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getSetmeans() {
		return setmeans;
	}
	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}
	public String getSetacct() {
		return setacct;
	}
	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}
	public String getPayBankId() {
		return payBankId;
	}
	public void setPayBankId(String payBankId) {
		this.payBankId = payBankId;
	}
	public String getPayBankName() {
		return payBankName;
	}
	public void setPayBankName(String payBankName) {
		this.payBankName = payBankName;
	}
	public String getPayUserId() {
		return payUserId;
	}
	public void setPayUserId(String payUserId) {
		this.payUserId = payUserId;
	}
	public String getPayUserName() {
		return payUserName;
	}
	public void setPayUserName(String payUserName) {
		this.payUserName = payUserName;
	}
	public String getRecBankId() {
		return recBankId;
	}
	public void setRecBankId(String recBankId) {
		this.recBankId = recBankId;
	}
	public String getRecBankName() {
		return recBankName;
	}
	public void setRecBankName(String recBankName) {
		this.recBankName = recBankName;
	}
	public String getRecUserId() {
		return recUserId;
	}
	public void setRecUserId(String recUserId) {
		this.recUserId = recUserId;
	}
	public String getRecUserName() {
		return recUserName;
	}
	public void setRecUserName(String recUserName) {
		this.recUserName = recUserName;
	}
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public String getHvpType() {
		return hvpType;
	}
	public void setHvpType(String hvpType) {
		this.hvpType = hvpType;
	}
	public String getHvpType1() {
		return hvpType1;
	}
	public void setHvpType1(String hvpType1) {
		this.hvpType1 = hvpType1;
	}
	public String getPzType() {
		return pzType;
	}
	public void setPzType(String pzType) {
		this.pzType = pzType;
	}
	public String getHurryLevel() {
		return hurryLevel;
	}
	public void setHurryLevel(String hurryLevel) {
		this.hurryLevel = hurryLevel;
	}
	public String getRemarkFy() {
		return remarkFy;
	}
	public void setRemarkFy(String remarkFy) {
		this.remarkFy = remarkFy;
	}
	public String getRemarkYt() {
		return remarkYt;
	}
	public void setRemarkYt(String remarkYt) {
		this.remarkYt = remarkYt;
	}
	public String getDealFlag() {
		return dealFlag;
	}
	public void setDealFlag(String dealFlag) {
		this.dealFlag = dealFlag;
	}
	public String getVoidFlag() {
		return voidFlag;
	}
	public void setVoidFlag(String voidFlag) {
		this.voidFlag = voidFlag;
	}
	public String getSettFlag() {
		return settFlag;
	}
	public void setSettFlag(String settFlag) {
		this.settFlag = settFlag;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getItime() {
		return itime;
	}
	public void setItime(String itime) {
		this.itime = itime;
	}
	public String getVoper() {
		return voper;
	}
	public void setVoper(String voper) {
		this.voper = voper;
	}
	public String getVtime() {
		return vtime;
	}
	public void setVtime(String vtime) {
		this.vtime = vtime;
	}
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getUserDealNo() {
		return userDealNo;
	}
	public void setUserDealNo(String userDealNo) {
		this.userDealNo = userDealNo;
	}
	public String getBusDealNo() {
		return busDealNo;
	}
	public void setBusDealNo(String busDealNo) {
		this.busDealNo = busDealNo;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}
