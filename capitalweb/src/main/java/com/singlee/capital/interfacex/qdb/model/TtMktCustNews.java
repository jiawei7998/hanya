package com.singlee.capital.interfacex.qdb.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_MKT_CUST_NEWS")
public class TtMktCustNews {
	//导入时间
	private String importDate;
	//客户号
	private String cno;
	//客户名称
	private String cname;
	//标题
	private String title;
	//详情
	private String news;
	//预留字段1
	private String remark1;
	//预留字段2
	private String remark2;
	
	public String getImportDate() {
		return importDate;
	}
	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNews() {
		return news;
	}
	public void setNews(String news) {
		this.news = news;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
}
