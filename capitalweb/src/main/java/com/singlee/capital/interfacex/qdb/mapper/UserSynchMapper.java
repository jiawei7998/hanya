package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.system.model.TaUser;

import tk.mybatis.mapper.common.Mapper;

public interface UserSynchMapper extends Mapper<TaUser>{
	
	/**
	 * 批量插入多个user
	 * @param users
	 */
	void insertUsers(List<TaUser> users);
	
	/**
	 * 批量插入多个UserIdinstID
	 * @param users
	 */
	void insertUserIdinstID(List<TaUser> users);
	/**
	 * 删除所以从核心同步来的柜员
	 */
	void deleteUserESB();

	Map<String, String> changeTemp(Map<String, String> map);

}
