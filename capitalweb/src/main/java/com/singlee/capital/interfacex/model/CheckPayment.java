package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class CheckPayment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String TotalAmt        ;
	private String SuccessTotalAmt      ;
	private String ErrorTotalAmt   ;
	private String TotalNumber  ;
	private String SuccessTotalNumber      ;
	private String ErrorTotalNumber      ;
	
	
	public String getTotalAmt() {
		return TotalAmt;
	}
	public void setTotalAmt(String totalAmt) {
		TotalAmt = totalAmt;
	}
	public String getSuccessTotalAmt() {
		return SuccessTotalAmt;
	}
	public void setSuccessTotalAmt(String successTotalAmt) {
		SuccessTotalAmt = successTotalAmt;
	}
	public String getErrorTotalAmt() {
		return ErrorTotalAmt;
	}
	public void setErrorTotalAmt(String errorTotalAmt) {
		ErrorTotalAmt = errorTotalAmt;
	}
	public String getTotalNumber() {
		return TotalNumber;
	}
	public void setTotalNumber(String totalNumber) {
		TotalNumber = totalNumber;
	}
	public String getSuccessTotalNumber() {
		return SuccessTotalNumber;
	}
	public void setSuccessTotalNumber(String successTotalNumber) {
		SuccessTotalNumber = successTotalNumber;
	}
	public String getErrorTotalNumber() {
		return ErrorTotalNumber;
	}
	public void setErrorTotalNumber(String errorTotalNumber) {
		ErrorTotalNumber = errorTotalNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
