package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TStFtNormRevRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TStFtNormRevRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="OrignTransId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DebitMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreditMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VersionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UserLoginId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthNo1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthNo2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStFtNormRevRq", propOrder = { "commonRqHdr", "orignTransId",
		"debitMedType", "acctNoDr", "creditMedType", "acctNoCr", "amt", "fbNo",
		"txnType", "versionName", "userLoginId", "authNo1", "authNo2" })
public class TStFtNormRevRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "OrignTransId", required = true)
	protected String orignTransId;
	@XmlElement(name = "DebitMedType", required = true)
	protected String debitMedType;
	@XmlElement(name = "AcctNoDr", required = true)
	protected String acctNoDr;
	@XmlElement(name = "CreditMedType", required = true)
	protected String creditMedType;
	@XmlElement(name = "AcctNoCr", required = true)
	protected String acctNoCr;
	@XmlElement(name = "Amt", required = true)
	protected String amt;
	@XmlElement(name = "FbNo", required = true)
	protected String fbNo;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "VersionName", required = true)
	protected String versionName;
	@XmlElement(name = "UserLoginId", required = true)
	protected String userLoginId;
	@XmlElement(name = "AuthNo1", required = true)
	protected String authNo1;
	@XmlElement(name = "AuthNo2", required = true)
	protected String authNo2;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the orignTransId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrignTransId() {
		return orignTransId;
	}

	/**
	 * Sets the value of the orignTransId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrignTransId(String value) {
		this.orignTransId = value;
	}

	/**
	 * Gets the value of the debitMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitMedType() {
		return debitMedType;
	}

	/**
	 * Sets the value of the debitMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitMedType(String value) {
		this.debitMedType = value;
	}

	/**
	 * Gets the value of the acctNoDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoDr() {
		return acctNoDr;
	}

	/**
	 * Sets the value of the acctNoDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoDr(String value) {
		this.acctNoDr = value;
	}

	/**
	 * Gets the value of the creditMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditMedType() {
		return creditMedType;
	}

	/**
	 * Sets the value of the creditMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditMedType(String value) {
		this.creditMedType = value;
	}

	/**
	 * Gets the value of the acctNoCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoCr() {
		return acctNoCr;
	}

	/**
	 * Sets the value of the acctNoCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoCr(String value) {
		this.acctNoCr = value;
	}

	/**
	 * Gets the value of the amt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmt() {
		return amt;
	}

	/**
	 * Sets the value of the amt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmt(String value) {
		this.amt = value;
	}

	/**
	 * Gets the value of the fbNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFbNo() {
		return fbNo;
	}

	/**
	 * Sets the value of the fbNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFbNo(String value) {
		this.fbNo = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the versionName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersionName() {
		return versionName;
	}

	/**
	 * Sets the value of the versionName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersionName(String value) {
		this.versionName = value;
	}

	/**
	 * Gets the value of the userLoginId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserLoginId() {
		return userLoginId;
	}

	/**
	 * Sets the value of the userLoginId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserLoginId(String value) {
		this.userLoginId = value;
	}

	/**
	 * Gets the value of the authNo1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthNo1() {
		return authNo1;
	}

	/**
	 * Sets the value of the authNo1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthNo1(String value) {
		this.authNo1 = value;
	}

	/**
	 * Gets the value of the authNo2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthNo2() {
		return authNo2;
	}

	/**
	 * Sets the value of the authNo2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthNo2(String value) {
		this.authNo2 = value;
	}

}
