package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExLdRpmSchInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExLdRpmSchInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DrawdownAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrinLiqAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProdCat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Maturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrentRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LdStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OdStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DispDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Principal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Int" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Commis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Fee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Chg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RunningBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExLdRpmSchInqRec", propOrder = { "number", "custNo",
		"drawdownAccount", "prinLiqAcct", "prodCat", "valueDate", "maturity",
		"currentRate", "ldStatus", "odStatus", "dispDate", "amt", "principal",
		"_int", "commis", "fee", "chg", "runningBal" })
public class TExLdRpmSchInqRec {

	@XmlElement(name = "Number", required = true)
	protected String number;
	@XmlElement(name = "CustNo", required = true)
	protected String custNo;
	@XmlElement(name = "DrawdownAccount", required = true)
	protected String drawdownAccount;
	@XmlElement(name = "PrinLiqAcct", required = true)
	protected String prinLiqAcct;
	@XmlElement(name = "ProdCat", required = true)
	protected String prodCat;
	@XmlElement(name = "ValueDate", required = true)
	protected String valueDate;
	@XmlElement(name = "Maturity", required = true)
	protected String maturity;
	@XmlElement(name = "CurrentRate", required = true)
	protected String currentRate;
	@XmlElement(name = "LdStatus", required = true)
	protected String ldStatus;
	@XmlElement(name = "OdStatus", required = true)
	protected String odStatus;
	@XmlElement(name = "DispDate", required = true)
	protected String dispDate;
	@XmlElement(name = "Amt", required = true)
	protected String amt;
	@XmlElement(name = "Principal", required = true)
	protected String principal;
	@XmlElement(name = "Int", required = true)
	protected String _int;
	@XmlElement(name = "Commis", required = true)
	protected String commis;
	@XmlElement(name = "Fee", required = true)
	protected String fee;
	@XmlElement(name = "Chg", required = true)
	protected String chg;
	@XmlElement(name = "RunningBal", required = true)
	protected String runningBal;

	/**
	 * Gets the value of the number property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the value of the number property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNumber(String value) {
		this.number = value;
	}

	/**
	 * Gets the value of the custNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustNo() {
		return custNo;
	}

	/**
	 * Sets the value of the custNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustNo(String value) {
		this.custNo = value;
	}

	/**
	 * Gets the value of the drawdownAccount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDrawdownAccount() {
		return drawdownAccount;
	}

	/**
	 * Sets the value of the drawdownAccount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDrawdownAccount(String value) {
		this.drawdownAccount = value;
	}

	/**
	 * Gets the value of the prinLiqAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrinLiqAcct() {
		return prinLiqAcct;
	}

	/**
	 * Sets the value of the prinLiqAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrinLiqAcct(String value) {
		this.prinLiqAcct = value;
	}

	/**
	 * Gets the value of the prodCat property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProdCat() {
		return prodCat;
	}

	/**
	 * Sets the value of the prodCat property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProdCat(String value) {
		this.prodCat = value;
	}

	/**
	 * Gets the value of the valueDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValueDate() {
		return valueDate;
	}

	/**
	 * Sets the value of the valueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValueDate(String value) {
		this.valueDate = value;
	}

	/**
	 * Gets the value of the maturity property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaturity() {
		return maturity;
	}

	/**
	 * Sets the value of the maturity property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaturity(String value) {
		this.maturity = value;
	}

	/**
	 * Gets the value of the currentRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrentRate() {
		return currentRate;
	}

	/**
	 * Sets the value of the currentRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrentRate(String value) {
		this.currentRate = value;
	}

	/**
	 * Gets the value of the ldStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLdStatus() {
		return ldStatus;
	}

	/**
	 * Sets the value of the ldStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLdStatus(String value) {
		this.ldStatus = value;
	}

	/**
	 * Gets the value of the odStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOdStatus() {
		return odStatus;
	}

	/**
	 * Sets the value of the odStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOdStatus(String value) {
		this.odStatus = value;
	}

	/**
	 * Gets the value of the dispDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDispDate() {
		return dispDate;
	}

	/**
	 * Sets the value of the dispDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDispDate(String value) {
		this.dispDate = value;
	}

	/**
	 * Gets the value of the amt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmt() {
		return amt;
	}

	/**
	 * Sets the value of the amt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmt(String value) {
		this.amt = value;
	}

	/**
	 * Gets the value of the principal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrincipal() {
		return principal;
	}

	/**
	 * Sets the value of the principal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrincipal(String value) {
		this.principal = value;
	}

	/**
	 * Gets the value of the int property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInt() {
		return _int;
	}

	/**
	 * Sets the value of the int property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInt(String value) {
		this._int = value;
	}

	/**
	 * Gets the value of the commis property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommis() {
		return commis;
	}

	/**
	 * Sets the value of the commis property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommis(String value) {
		this.commis = value;
	}

	/**
	 * Gets the value of the fee property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFee() {
		return fee;
	}

	/**
	 * Sets the value of the fee property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFee(String value) {
		this.fee = value;
	}

	/**
	 * Gets the value of the chg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChg() {
		return chg;
	}

	/**
	 * Sets the value of the chg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChg(String value) {
		this.chg = value;
	}

	/**
	 * Gets the value of the runningBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRunningBal() {
		return runningBal;
	}

	/**
	 * Sets the value of the runningBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRunningBal(String value) {
		this.runningBal = value;
	}

}
