package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jxls.transformer.XLSTransformer;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.POIReadExcelToHtml;
import com.singlee.capital.base.util.PoiUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.print.model.TcPrintTemplate;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.print.service.PrintTemplateService;
import com.singlee.capital.system.controller.CommonController;

/**
 * 打印模板控制层
 * 
 * @author XUHUI
 * 
 */
@Controller
@RequestMapping(value = "/PrintUtilController")
public class PrintUtilController extends CommonController {
	@Autowired
	private PrintTemplateService printTemplateService;

	/***
	 * 导出
	 * 
	 * @param request
	 * @param response
	 * @param typeid
	 * @param pkid
	 *            选中的所要导出记录的ID组合
	 * @param type
	 *            "print"打印 "excel" excel方式导出
	 * @throws Exception
	 */
	@RequestMapping(value = "/exportload/{type}/{tempid}/{pkid}")
	public void exportDownload(HttpServletRequest request,
			HttpServletResponse response,
			@PathVariable("tempid") String tempid,
			@PathVariable("pkid") String pkid, @PathVariable("type") String type)
			throws Exception {

		String[] spkids = pkid.split(",");
		String errorMsg = "";
		// 设置属性
		response.reset();
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "No-cache");
		response.setCharacterEncoding("UTF-8");
		response.setDateHeader("Expires", 0);
		// new
		OutputStream out = response.getOutputStream();
		// 获取模板
		TcPrintTemplate template = printTemplateService.getPrintTemplate(tempid);
		if (template == null) {
			errorMsg = "error:不存在编号" + tempid + "的模板!";
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			// response.sendError(response.SC_GATEWAY_TIMEOUT,errorMsg);
			out.write(errorMsg.getBytes());
			return;
		}
		if (template.getTempContent() == null) {
			errorMsg = "error:编号" + tempid + "的模板数据没有模板数据！";
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			// response.sendError(response.SC_GATEWAY_TIMEOUT,errorMsg);
			out.write(errorMsg.getBytes());
			return;
		}
		AssemblyDataService dataService = null;
		try {
			dataService = SpringContextHolder.getBean(template.getBeanName());
		} catch (Exception e) {
			errorMsg = "error:编号" + tempid + "的模板对应的数据处理bean类("
					+ template.getBeanName() + ")没有找到！";
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			// response.sendError(response.SC_GATEWAY_TIMEOUT,errorMsg);
			out.write(errorMsg.getBytes());
			return;
		}
		String ua = request.getHeader("User-Agent");
		// 是否是firefox浏览器
		boolean isFirefox = ua.contains("Firefox");
		InputStream fileInputStream = new ByteArrayInputStream(template.getTempContent());// 获取模板

		XLSTransformer transformer = new XLSTransformer();

		Workbook wb = WorkbookFactory.create(fileInputStream);
		Sheet sheet1 = wb.getSheetAt(0);
		List<String> sheetNameList = new ArrayList<String>();
		List<String> sheetTempletNameList = new ArrayList<String>();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

		for (String pkId : spkids) {
			String tempSheetName = template.getTemplateName() + pkId;
			if (dataService != null) {
				// 组装导出数据
				Map<String, Object> data = dataService.getData(pkId);

				sheetNameList.add(tempSheetName);
				Sheet sheet = wb.createSheet(tempSheetName + "templet");
				sheetTempletNameList.add(sheet.getSheetName());
				PoiUtil.copySheet(sheet, sheet1, wb, wb);
				sheet.protectSheet("singlee.com.cn");
				//sheet.protectSheet(SystemProperties.passwordExcel);
				// 初始化数据
				resultList.add(data);
			}
		}
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		wb.write(outputStream);
		InputStream ins2 = new ByteArrayInputStream(outputStream.toByteArray());
		Workbook wb2 = transformer.transformXLS(ins2, sheetTempletNameList,sheetNameList, resultList);
		if ("print".equals(type)) {
			response.setHeader("Content-Type", "text/html; charset=utf-8");
			String htmlExcel = "";
			if (wb2 instanceof XSSFWorkbook) {
				XSSFWorkbook xWb = (XSSFWorkbook) wb2;
				htmlExcel = POIReadExcelToHtml.getExcelInfo(xWb, true);
			} else if (wb2 instanceof HSSFWorkbook) {
				HSSFWorkbook hWb = (HSSFWorkbook) wb2;
				htmlExcel = POIReadExcelToHtml.getExcelInfo(hWb, true);
			}
			out.write(htmlExcel.getBytes("UTF-8"));
		} else {
			response.setHeader("Content-Type", "application/msexcel");
			// 设置头
			if (isFirefox) {
				response.setHeader("Content-Disposition",
						"attachment; filename=\""
								+ new String(template.getTemplateName()
										.getBytes("UTF-8"), "ISO-8859-1"));
			} else {
				response.setHeader(
						"Content-Disposition",
						"attachment; filename=\""
								+ URLEncoder.encode(template.getTemplateName(),
										"utf-8") + "\"");
			}
			wb2.write(out);
			out.flush();
		}
		response.setHeader("Connection", "close");
	}

	/**
	 * 上传打印文件
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadPrintTemplate", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadPrintTemplate(MultipartHttpServletRequest request,
			HttpServletResponse response) {
		StringBuffer sb = new StringBuffer();
		try {
			// 保存模板
			printTemplateService.savePrintTemplate(request);

		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return StringUtils.isEmpty(sb.toString()) ? "保存成功" : sb.toString();
	}

	/**
	 * 分页查询
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPrintTemplatePage")
	public RetMsg<PageInfo<TcPrintTemplate>> searchPrintTemplatePage(
			@RequestBody Map<String, Object> params) {
		Page<TcPrintTemplate> page = printTemplateService
				.searchPrintTemplatePage(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据模板编号获取信息
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getPrintTemplate")
	public RetMsg<TcPrintTemplate> getPrintTemplate(
			@RequestBody Map<String, Object> params) {
		String tempCode = ParameterUtil.getString(params, "tempCode", "");
		TcPrintTemplate temp = printTemplateService.getPrintTemplate(tempCode);
		return RetMsgHelper.ok(temp);
	}

	/**
	 * 下载模板
	 * 
	 * @param tempCode
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/downloadTemplate/{tempCode}", method = RequestMethod.GET)
	public void downloadTemplate(@PathVariable String tempCode,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		TcPrintTemplate temp = printTemplateService.getPrintTemplate(tempCode);
		JY.require(temp != null, "模板%s 没有找到！", tempCode);
		String encode_filename = URLEncoder.encode(temp.getTemplateName()
				+ ".xls", "utf-8");
		String ua = request.getHeader("User-Agent");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*="
					+ encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ encode_filename);
		}

		HttpUtil.flushHttpResponse(response, "application/msexcel",
				temp.getTempContent());
	}
}
