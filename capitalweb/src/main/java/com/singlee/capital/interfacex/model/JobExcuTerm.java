package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class JobExcuTerm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int JOB_ID;
	private String START_TIME;
	public String getSTART_TIME() {
		return START_TIME;
	}
	public void setSTART_TIME(String sTART_TIME) {
		START_TIME = sTART_TIME;
	}
	private String MAX_TIME;
	private String UINT;
	public int getJOB_ID() {
		return JOB_ID;
	}
	public void setJOB_ID(int jOB_ID) {
		JOB_ID = jOB_ID;
	}
	public String getMAX_TIME() {
		return MAX_TIME;
	}
	public void setMAX_TIME(String mAX_TIME) {
		MAX_TIME = mAX_TIME;
	}
	public String getUINT() {
		return UINT;
	}
	public void setUINT(String uINT) {
		UINT = uINT;
	}
	
}
