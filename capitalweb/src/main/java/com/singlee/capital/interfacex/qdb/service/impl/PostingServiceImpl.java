package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import org.apache.ibatis.session.RowBounds;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.mapper.PostingMapper;
import com.singlee.capital.interfacex.qdb.model.AcupXmlBean;
import com.singlee.capital.interfacex.qdb.model.Posting;
import com.singlee.capital.interfacex.qdb.service.PostingService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.util.ReadAcupXmlFactory;
import com.singlee.capital.interfacex.qdb.util.WriteToFile;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PostingServiceImpl implements PostingService{
	@Autowired
	private PostingMapper postingMapper;

	private String path = PropertiesUtil.getProperties("POSTING.path");
	private String addr = PropertiesUtil.getProperties("POSTING.addr");    
	private int port = Integer.parseInt(PropertiesUtil.getProperties("POSTING.port"));    
	private String userName = PropertiesUtil.getProperties("POSTING.userName");
	private String passWord = PropertiesUtil.getProperties("POSTING.passWord");
	private String txtPath = PropertiesUtil.getProperties("POSTING.txtPath");
	private String esbXmlDir = PropertiesUtil.getProperties("ESB.EsbXmlDir");//ESB报文模板保存的路径
	@Override
	public Page<Posting> pagePosting(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Posting> result = postingMapper.pagePosting(map, rowBounds);
		return result;
	}

	@Override
	public HashMap<Integer, AcupXmlBean> queryAcupXmlConfig(String XmlPath)throws Exception {
		SAXReader reader = new SAXReader();
		Document xmlDoc = reader.read(transXmlToStream(XmlPath));
		HashMap<Integer, AcupXmlBean> headerHashMap = new HashMap<Integer, AcupXmlBean>();
		ReadAcupXmlFactory.getElementList(xmlDoc.getRootElement(), headerHashMap);
		return headerHashMap;
	}
	
	/****
	 * 将路径转换为流
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private InputStream transXmlToStream(String path)throws Exception
	{
		return PostingService.class.getClassLoader().getResourceAsStream(path);
	}

	@Override
	public void messageXMLFileSaveLocal(String data, String path,
			String filename, String date) throws Exception {
		WriteToFile.writeToFile(data, path, filename, date);
	}

	@Override
	public List<Posting> listPosting(Map<String, Object> map) {
		return  postingMapper.listPosting(map);
	}

	@Override
	public List<Posting> zlistPosting(Map<String, Object> map) {
		return  postingMapper.zlistPosting(map);
	}

	@Override
	public Posting sumValuePosting(Map<String, Object> map) {
		return  postingMapper.sumValuePosting(map);
	}

	@Override
	public Posting countPosting(Map<String, Object> map) {
		return postingMapper.countPosting(map);
	}

	@Override
	public boolean createAcup(Map<String, Object> map) {
		boolean flag = true;
		//创建客户端对象
       FTPClient ftp = new FTPClient();
       InputStream inputStream = null;
		try {
			List<Posting> slAcupList =  listPosting(map);
			String postDate = map.get("postDate").toString().replace("-","");
			StringBuffer totalBuffer = new StringBuffer();
			StringBuffer acupHeaderBuffer = new StringBuffer();//存储头
			StringBuffer acupglmorcBuffer = new StringBuffer();//入账体
			StringBuffer acuptrailerBuffer = new StringBuffer();//尾
			if(slAcupList.size() > 0)
			{
				System.out.println(slAcupList.size());
				HashMap<Integer, AcupXmlBean> acupheader = queryAcupXmlConfig(esbXmlDir + "AFIB-HEADER.xml");//头
				acupheader.get(2).setValue("ISLOP001"+postDate);
				acupheader.get(5).setValue(postDate);
				for(int i=1; i<=acupheader.size(); i++){
					acupHeaderBuffer.append(acupheader.get(i).format());
				}
				
				HashMap<Integer, AcupXmlBean> acupglmorcRoot = queryAcupXmlConfig(esbXmlDir + "AFIB-GLMORC.xml");//入账体
				StringBuffer bodysonBuffer = null;
				for(Posting acupBean : slAcupList)
				{
					bodysonBuffer = new StringBuffer();
					//新建一个hashmap
					HashMap<Integer, AcupXmlBean> acupglmorc = new HashMap<Integer, AcupXmlBean>();
					/**
					 * 将读取配置文件的acupglmorcRoot 通过深拷贝给 acupglmorc；
					 * HashMap需要通过深拷贝才能替换掉
					 */
					acupglmorc = deepCopy(acupglmorcRoot);
					acupglmorc.get(2).setValue("ISLOP001"+postDate);
					acupglmorc.get(5).setValue("80002");
					acupglmorc.get(6).setValue(StringUtils.trimToEmpty(acupBean.getSubjCode())==null?"":StringUtils.trimToEmpty(acupBean.getSubjCode()));
//					acupglmorc.get(7).setValue(StringUtils.trimToEmpty(acupBean.getSUBBR())==null?"":StringUtils.trimToEmpty(acupBean.getSUBBR()));
					acupglmorc.get(7).setValue("999");
					acupglmorc.get(10).setValue("OPICS");
					acupglmorc.get(12).setValue(postDate);
					acupglmorc.get(13).setValue(StringUtils.trimToEmpty(acupBean.getCcy())==null?"":StringUtils.trimToEmpty(acupBean.getCcy()));
					if("D".equals(StringUtils.trimToEmpty(acupBean.getDebitCreditFlag())))
					{
						if(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).startsWith("-")){
							acupglmorc.get(14).setValue("-");
							acupglmorc.get(15).setValue(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).replace("-", "").replace(".", ""));
						}else {
							acupglmorc.get(14).setValue("+");
							System.out.println(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).replace(".", ""));
							acupglmorc.get(15).setValue(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).replace(".", ""));
						}
					}
					if("C".equals(StringUtils.trimToEmpty(acupBean.getDebitCreditFlag())))
					{
						if(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).startsWith("-")){
							acupglmorc.get(16).setValue("-");
							acupglmorc.get(17).setValue(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).replace("-", "").replace(".", ""));
						}else {
							acupglmorc.get(16).setValue("+");
							acupglmorc.get(17).setValue(StringUtils.trimToEmpty(formatBigDecimal(acupBean.getValue())).replace(".", ""));
						}
					}
					acupglmorc.get(19).setValue(postDate);
					acupglmorc.get(25).setValue("ISLOP001"+postDate);
//					acupglmorc.get(28).setValue(StringUtils.trim(acupBean.getSubjName())==null?"":StringUtils.trimToEmpty(acupBean.getSubjName().replace("-", "")));
					for(int i = 1; i <= acupglmorc.size(); i++)
					{
						bodysonBuffer.append(acupglmorc.get(i).format());
					}
					acupglmorcBuffer.append(bodysonBuffer).append("\r\n");
				}
				
				HashMap<Integer, AcupXmlBean> acuptrailer = queryAcupXmlConfig(esbXmlDir + "AFIB-TRAILER.xml");//尾
				acuptrailer.get(2).setValue("ISLOP001"+postDate);
				acuptrailer.get(3).setValue(StringUtils.trimToEmpty(slAcupList.get(1).getCount()));
				acuptrailer.get(4).setValue(StringUtils.trimToEmpty(formatBigDecimal(slAcupList.get(1).getTotalamount()).replace(".", "")));
				System.out.println("xxxxxxxxxxxxx"+formatBigDecimal(slAcupList.get(1).getTotalamount()).replace(".", ""));
				System.out.println(acuptrailer.get(4).format());
				for(int i = 1; i <= acuptrailer.size(); i++)
				{
					acuptrailerBuffer.append(acuptrailer.get(i).format());
				}
				//System.out.println(acuptrailerBuffer.toString());
				
				totalBuffer.append(acupHeaderBuffer).append("\r\n").append(acupglmorcBuffer).append(acuptrailerBuffer);
				//System.out.println(totalBuffer);
			}
			
			
			//连接ftp服务器
            ftp.connect(addr, port);
            //登录
            ftp.login(userName, passWord);
            //設置為被動模式
            ftp.enterLocalPassiveMode();
            //设置上传路径
			//检查上传路径是否存在 如果不存在返回false
	        boolean flagw = ftp.changeWorkingDirectory(txtPath);
	        if(!flagw){
	           //创建上传的路径  该方法只能创建一级目录，在这里如果/home/ftpuser存在则可创建image
	           ftp.makeDirectory(txtPath);
	        }
	      //指定上传路径
           ftp.changeWorkingDirectory(path);
           //指定上传文件的类型  二进制文件
           ftp.setFileType(FTP.BINARY_FILE_TYPE);
        
           String fileName = "AFIB" + postDate.substring(2, postDate.length());
           File dir = new File(PathUtils.getAbsPath("temp"));
           if(!dir.exists())
           {
        	   dir.mkdir();
           }
           FileOutputStream writer = new FileOutputStream(dir.getAbsolutePath() + "\\" + fileName);
   		   writer.write(totalBuffer.toString().getBytes("UTF-8"));
   		   writer.close();
           //读取本地文件
           inputStream = new FileInputStream(dir.getAbsolutePath() + "\\" + fileName);
           //第一个参数是文件名
           ftp.storeFile(fileName, inputStream);
           
           deleFile(dir.getAbsolutePath(),fileName);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		} finally 
		{
			try {
				if(inputStream != null)
				{
					//关闭文件流
					inputStream.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
			try {
				if(ftp != null){
					//退出
					ftp.logout();
					if(ftp.isConnected()) {ftp.disconnect();}
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public boolean createBcup(Map<String, Object> map) {
		boolean flag = true;

		FTPClient ftp = new FTPClient();
	    InputStream inputStream=null;
		StringBuffer totalBuffer = new StringBuffer();
		try{
			HashMap<Integer, AcupXmlBean> acupHeaderHashMap = queryAcupXmlConfig(esbXmlDir + "AFIB-HEADER.xml");//头
			HashMap<Integer, AcupXmlBean> acupbodyerHashMap = queryAcupXmlConfig(esbXmlDir + "AFIB-GLMORR.xml");
			HashMap<Integer, AcupXmlBean> acupTrailerHashMap = queryAcupXmlConfig(esbXmlDir + "AFIB-TRAILER.xml");
			String postDate = map.get("postDate").toString();
			String postdate = postDate.replace("-","");
			acupHeaderHashMap.get(2).setValue("ISLOP002"+postdate);
			acupHeaderHashMap.get(5).setValue(postdate);
			
			StringBuffer headerBuffer = new StringBuffer();
			for(int i = 1 ; i <= acupHeaderHashMap.size();i++)
			{
				//logger.info(BeanUtils.describe(acupHeaderHashMap.get(i)));
				headerBuffer.append(acupHeaderHashMap.get(i).format());
			}
			
			Posting posting  = countPosting(map);
			int count  = Integer.valueOf(posting.getCount());
			int totalPage = (count % 50 == 0? count / 50:count / 50+1);
			StringBuffer bodyBuffer = new StringBuffer();
			for(int i = 1 ;i <= totalPage;i++)
			{
				List<Posting> slAcupBLList = zlistPosting(map);
				for(Posting slAcupBL : slAcupBLList)
				{
					StringBuffer bodySonBuffer = new StringBuffer();
					acupbodyerHashMap.get(2).setValue(acupHeaderHashMap.get(2).getValue());
					acupbodyerHashMap.get(4).setValue("01");
					acupbodyerHashMap.get(5).setValue("80002");
					acupbodyerHashMap.get(6).setValue(StringUtils.trimToEmpty(slAcupBL.getSubjCode())==null?"":StringUtils.trimToEmpty(slAcupBL.getSubjCode()));
					acupbodyerHashMap.get(7).setValue("999");
					acupbodyerHashMap.get(11).setValue(postdate);
					acupbodyerHashMap.get(12).setValue(StringUtils.trimToEmpty(slAcupBL.getCcy())==null?"":StringUtils.trimToEmpty(slAcupBL.getCcy()));
					
					if(StringUtils.trimToEmpty(formatBigDecimal(slAcupBL.getValue())).startsWith("-")){
						acupbodyerHashMap.get(13).setValue("-");
						acupbodyerHashMap.get(14).setValue(StringUtils.trimToEmpty(formatBigDecimal(slAcupBL.getValue())).replace("-", "").replace(".", ""));
					}else {
						acupbodyerHashMap.get(13).setValue("+");
						System.out.println(StringUtils.trimToEmpty(formatBigDecimal(slAcupBL.getValue())).replace(".", ""));
						acupbodyerHashMap.get(14).setValue(StringUtils.trimToEmpty(formatBigDecimal(slAcupBL.getValue())).replace(".", ""));
					}
					
					for(int j= 1 ; j <= acupbodyerHashMap.size();j++)
					{
						//logger.info(BeanUtils.describe(acupbodyerHashMap.get(j)));
						bodySonBuffer.append(acupbodyerHashMap.get(j).format());
					}
					bodyBuffer.append(bodySonBuffer).append("\r\n");
				}
				
			}
			
			acupTrailerHashMap.get(2).setValue(acupHeaderHashMap.get(2).getValue());
			acupTrailerHashMap.get(3).setValue(String.valueOf(count));
			acupTrailerHashMap.get(4).setValue(sumValuePosting(map).getValue().toString().replace(".", ""));
		
			StringBuffer trailerBuffer = new StringBuffer();
			for(int j= 1 ; j <= acupTrailerHashMap.size();j++)
			{
				//logger.info(BeanUtils.describe(acupTrailerHashMap.get(j)));
				trailerBuffer.append(acupTrailerHashMap.get(j).format());
			}
			//logger.info("ACUP CHECK FILE SUCCESS");
			
			totalBuffer.append(headerBuffer).append("\r\n").append(bodyBuffer).append(trailerBuffer);
			System.out.println(totalBuffer);
			
			//连接ftp服务器
            ftp.connect(addr, port);
            //登录
            ftp.login(userName, passWord);
            //設置為被動模式
            ftp.enterLocalPassiveMode();
            //设置上传路径
			//检查上传路径是否存在 如果不存在返回false
	        boolean flagw = ftp.changeWorkingDirectory(txtPath);
	        if(!flagw){
	           //创建上传的路径  该方法只能创建一级目录，在这里如果/home/ftpuser存在则可创建image
	           ftp.makeDirectory(txtPath);
	        }
	      //指定上传路径
           ftp.changeWorkingDirectory(path);
           //指定上传文件的类型  二进制文件
           ftp.setFileType(FTP.BINARY_FILE_TYPE);
           
           String fileName = "BFIB" + postdate.substring(2, postdate.length());
           File dir = new File(PathUtils.getAbsPath("temp"));
           if(!dir.exists())
           {
        	   dir.mkdir();
           }
           
           FileOutputStream writer = new FileOutputStream (dir.getAbsolutePath() + "\\" + fileName);
   		   writer.write(totalBuffer.toString().getBytes("UTF-8"));
   		   writer.close();
           //读取本地文件
   		inputStream = new FileInputStream(dir.getAbsolutePath() + "\\" + fileName);
           //第一个参数是文件名
           ftp.storeFile(fileName, inputStream);
           
           deleFile(dir.getAbsolutePath(),fileName);
		}catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}finally {
            try {
                //关闭文件流
                if(inputStream != null) {inputStream.close();}
                if(ftp != null)
                {
                	//退出
                    ftp.logout();
                    //断开连接
                    if(ftp.isConnected()) {ftp.disconnect();}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		return flag;
	
	}

	@Override
	public boolean createFIBFile(Map<String, Object> map) {
		boolean flag = true;
		//创建客户端对象
       FTPClient ftp = new FTPClient();
       InputStream inputStream=null;
		try {
			String postDate = map.get("postDate").toString().replace("-","");
			//连接ftp服务器
            ftp.connect(addr, port);
            //登录
            ftp.login(userName, passWord);
            //设置上传路径
			//检查上传路径是否存在 如果不存在返回false
	        boolean flagw = ftp.changeWorkingDirectory(txtPath);
	        if(!flagw){
	           //创建上传的路径  该方法只能创建一级目录，在这里如果/home/ftpuser存在则可创建image
	           ftp.makeDirectory(txtPath);
	        }
	        //設置為被動模式
            ftp.enterLocalPassiveMode();
	      //指定上传路径
           ftp.changeWorkingDirectory(path);
           //指定上传文件的类型  二进制文件
           ftp.setFileType(FTP.BINARY_FILE_TYPE);
           
           String fileName = "FIB"+postDate.substring(2, postDate.length());
           File dir = new File(PathUtils.getAbsPath("temp"));
           if(!dir.exists())
           {
        	   dir.mkdir();
           }
           
           FileOutputStream writer = new FileOutputStream (dir.getAbsolutePath() + "\\" + fileName);
   			writer.write("".getBytes("UTF-8"));
   			writer.close();
           //读取本地文件
           inputStream = new FileInputStream(dir.getAbsolutePath() + "\\" + fileName);
           //第一个参数是文件名
           ftp.storeFile(fileName, inputStream);
	        
           deleFile(dir.getAbsolutePath(),fileName);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}finally {
			try {
                //关闭文件流
                if(inputStream != null) {inputStream.close();}
                if(ftp != null)
                {
                	//退出
                    ftp.logout();
                    //断开连接
                    if(ftp.isConnected()) {ftp.disconnect();}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		return flag;
		
	}
	public static void deleFile(String url,String fileName) {
		//try {
		  //取得RWFileDir目录下的文件
		File file = new File(url+fileName);
        // 判断文件是否存在
        if (file.exists()) {
            // 文件删除
            file.delete();
            System.out.println("删除"+fileName+"文件成功");
        }
	}
	@SuppressWarnings("unchecked")
	public HashMap<Integer, AcupXmlBean> deepCopy(HashMap<Integer, AcupXmlBean> acupHashMap) throws Exception
    {
		 HashMap<Integer, AcupXmlBean> hashMap = new HashMap<Integer, AcupXmlBean>();
		 ByteArrayOutputStream bos = new ByteArrayOutputStream();
		 ObjectOutputStream oos = new ObjectOutputStream(bos);
		 oos.writeObject(acupHashMap);
		 oos.close();
		 bos.close();
		 ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		 ObjectInputStream ois = new ObjectInputStream(bis);
		 hashMap = (HashMap<Integer, AcupXmlBean>)ois.readObject();
		 ois.close();
		 bis.close();
		 return hashMap;
     }
	public String formatBigDecimal(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("###############0.00");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}

	@Override
	public boolean createPostingFile(Map<String, Object> map) {
		map.put("postDate", DayendDateServiceProxy.getInstance().getSettlementDate());
		createAcup(map);
		createBcup(map);
		createFIBFile(map);
		return false;
	}
}
