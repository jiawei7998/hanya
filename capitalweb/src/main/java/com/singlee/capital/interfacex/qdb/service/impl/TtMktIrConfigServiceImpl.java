package com.singlee.capital.interfacex.qdb.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.mapper.TtMktIrConfigMapper;
import com.singlee.capital.interfacex.qdb.model.TtMktIrConfig;
import com.singlee.capital.interfacex.qdb.service.TtMktIrConfigService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TtMktIrConfigServiceImpl implements TtMktIrConfigService{
	@Autowired
	private TtMktIrConfigMapper configMapper;

	@Override
	public Page<TtMktIrConfig> selectTtMktIrConfigPage(Map<String, Object> params) {
		Page<TtMktIrConfig> page = configMapper.selectTtMktIrConfigPage(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public void insertTtMktIrConfig(TtMktIrConfig ttMktIrConfig) {
		String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
		ttMktIrConfig.setLstmntDate(nowdate);
		configMapper.insertTtMktIrConfig(ttMktIrConfig);
	}

	@Override
	public void updateTtMktIrConfig(Map<String, Object> map) {
		String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("lstmntDate", nowdate);
		configMapper.updateTtMktIrConfig(map);
	}

	@Override
	public void daleteTtMktIrConfig(Map<String, Object> map) {
		configMapper.daleteTtMktIrConfig(map);
	}

	@Override
	public List<TtMktIrConfig> selectTtMktIrConfigList() {
		List<TtMktIrConfig>  list = configMapper.selectTtMktIrConfigList();
		return list;
	}
	
}
