package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CloudProSynchTransRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CloudProSynchTransRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgTypId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloudProSynchTransRq", propOrder = { "commonRqHdr",
		"productId", "msgTypId" })
public class CloudProSynchTransRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "ProductId", required = true)
	protected String productId;
	@XmlElement(name = "MsgTypId", required = true)
	protected String msgTypId;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the productId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * Sets the value of the productId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductId(String value) {
		this.productId = value;
	}

	/**
	 * Gets the value of the msgTypId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgTypId() {
		return msgTypId;
	}

	/**
	 * Sets the value of the msgTypId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgTypId(String value) {
		this.msgTypId = value;
	}

}
