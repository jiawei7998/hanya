package com.singlee.capital.interfacex.qdb.pojo;

import java.io.Serializable;

public class FBZ002 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String source;//发起服务名
	private String destination;//目标服务名
	private String bizDate;//业务日期
	private String refNo;//业务参考号
	private String seqNo;//业务参考号
	private String respCde;//返回码
	private String respMsg;//返回信息
	private String tmStamp;//时间戳
	private String mfCustomerId;//核心客户号
	private String customerType;//客户类型
	private String certType;//证件类型
	private String certId;//证件号码
	private String startNum;
	private String showNum;
	private String isCyc;//是否循环
	private String currency;//额度币种
	private String limitSum;//额度金额
	private String limitBalance;//额度可用余额
	private String term;//期限
	private String termCde;//期限单位
	private String startDat;//起始日
	private String endDat;//到期日
	private String lmtID;//额度编号
	private String singField1;
	private String singField2;
	private String singField3;
	private String singField4;
	private String singField5;
	private String orderId;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getBizDate() {
		return bizDate;
	}
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public String getRespCde() {
		return respCde;
	}
	public void setRespCde(String respCde) {
		this.respCde = respCde;
	}
	public String getRespMsg() {
		return respMsg;
	}
	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}
	public String getTmStamp() {
		return tmStamp;
	}
	public void setTmStamp(String tmStamp) {
		this.tmStamp = tmStamp;
	}
	public String getMfCustomerId() {
		return mfCustomerId;
	}
	public void setMfCustomerId(String mfCustomerId) {
		this.mfCustomerId = mfCustomerId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getCertId() {
		return certId;
	}
	public void setCertId(String certId) {
		this.certId = certId;
	}
	public String getStartNum() {
		return startNum;
	}
	public void setStartNum(String startNum) {
		this.startNum = startNum;
	}
	public String getShowNum() {
		return showNum;
	}
	public void setShowNum(String showNum) {
		this.showNum = showNum;
	}
	public String getIsCyc() {
		return isCyc;
	}
	public void setIsCyc(String isCyc) {
		this.isCyc = isCyc;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLimitSum() {
		return limitSum;
	}
	public void setLimitSum(String limitSum) {
		this.limitSum = limitSum;
	}
	public String getLimitBalance() {
		return limitBalance;
	}
	public void setLimitBalance(String limitBalance) {
		this.limitBalance = limitBalance;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTermCde() {
		return termCde;
	}
	public void setTermCde(String termCde) {
		this.termCde = termCde;
	}
	public String getStartDat() {
		return startDat;
	}
	public void setStartDat(String startDat) {
		this.startDat = startDat;
	}
	public String getEndDat() {
		return endDat;
	}
	public void setEndDat(String endDat) {
		this.endDat = endDat;
	}
	public String getLmtID() {
		return lmtID;
	}
	public void setLmtID(String lmtID) {
		this.lmtID = lmtID;
	}
	public String getSingField1() {
		return singField1;
	}
	public void setSingField1(String singField1) {
		this.singField1 = singField1;
	}
	public String getSingField2() {
		return singField2;
	}
	public void setSingField2(String singField2) {
		this.singField2 = singField2;
	}
	public String getSingField3() {
		return singField3;
	}
	public void setSingField3(String singField3) {
		this.singField3 = singField3;
	}
	public String getSingField4() {
		return singField4;
	}
	public void setSingField4(String singField4) {
		this.singField4 = singField4;
	}
	public String getSingField5() {
		return singField5;
	}
	public void setSingField5(String singField5) {
		this.singField5 = singField5;
	}
}
