package com.singlee.capital.interfacex.socketservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.interfacex.model.EcifBatch;

public class FileReadWriteService {
	/**
	 * 根据前通配符去读取文件
	 * @param filepath  读文件地址
	 * @param filebak   备份文件地址
	 * @param indexof   文件名起始匹配符
	 * @param encoding  编码格式
	 * @return   数据返回后再进行按标志位拆分 例如：|， @！@
	 */
	public static List<String> readFile(String filepath,String filebak,String indexof,String encoding){
		BufferedReader br = null;
		List<String> retstrList = new ArrayList<String>();
		LogManager.getLogger(LogManager.MODEL_BATCH).info("**********************************************************************************************************************");
		try {
			File file = new File(filepath);
			file.setWritable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[readfile]-读文件的文件夹路径不存在："+filepath);
				return retstrList;
			}
			
			File[] files = file.listFiles();
			if(files.length<=0 || null == files){
				System.out.println("[readfile]-本次扫描路径下不存在文件："+filepath);
				return retstrList;
			}
			
			if(files.length>0){
				
				for(File rfile:files)
				{
					
					if(rfile.getName().indexOf(indexof) == 0){
						retstrList = new ArrayList<String>();
						br = new BufferedReader(new InputStreamReader(new FileInputStream(rfile),encoding));
						String data = null;
						while((data=br.readLine()) != null){
							retstrList.add(data);
						}
						
						if(br != null) {
                            br.close();
                        }
						/**
						 * 判断备份的文件路径是否存在
						 */
						if(filebak.length() > 0){
							File bakFile = new File(filebak +"/"+ rfile.getName());
							if(bakFile.isFile()){
								String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
								System.out.println("存在文件，进行重命名掉！"+bakFileName);
								bakFile.renameTo(new File(bakFileName));
							}
//							FileUtils.copyFile(rfile, bakFile);
							if(rfile.renameTo(bakFile)){
								System.out.println("文件备份成功!");
							}	
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[readfile]-读文件："+e.getMessage());
		} finally {
				try {
					if(br != null) {
                        br.close();
                    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
		}
		return retstrList;
	}
	
	/**
	 * 根据前后通配符去读取文件
	 * @param filepath  读文件地址
	 * @param filebak   备份文件地址
	 * @param indexof   文件名起始匹配符
	 * @param lastindexof 文件名结束通配符
	 * @param tlastindexof 文件名结束通配符2
	 * @return   数据返回后再进行按标志位拆分 例如：|， @！@
	 */
//	public static List<String> readFile(String filepath,String filebak,final String indexof,final String lastindexof,String tlastindexof, String encoding){
	public static Map<String, Object> readFile(String filepath,String filebak,final String indexof,final String lastindexof,String tlastindexof, String encoding){		
		BufferedReader br = null;
		BufferedReader br1 = null;
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> retstrList = new ArrayList<String>();
		List<String> retstrList2 = new ArrayList<String>();
		try {
			File file = new File(filepath);
			file.setWritable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]-读文件的文件夹路径不存在："+filepath);
				map.put("retstrList", retstrList);
				return map;
//				return retstrList;
			}
			LogManager.getLogger(LogManager.MODEL_BATCH).info("^"+indexof+"(.*)"+lastindexof+"$");
			File[] files = file.listFiles(new FileFilter() {
				Pattern pattern = Pattern.compile("^"+indexof+"(.*)"+lastindexof+"$");
				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					Matcher m = pattern.matcher(pathname.getName());
					return m.matches();
				}
			});
			if(files.length<=0 || null == files){
				LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]-本次扫描路径下不存在文件："+filepath);
				map.put("retstrList", retstrList);
				return map;
//				return retstrList;
			}
			
			if(files.length>0){
				for(File rfile:files)
				{
					retstrList2.clear();
					br1 = new BufferedReader(new InputStreamReader(new FileInputStream(rfile),encoding));
					String data1 = null;
					while((data1=br1.readLine()) != null){
						LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]S04_LD_SCH_DET_.flg:"+data1);
						retstrList2.add(data1);
					}
					
					if(br1 != null) {
                        br1.close();
                    }
					/**
					 * 判断备份的文件路径是否存在
					 */
					if(filebak.length() > 0){
						File bakFile = new File(filebak +"/"+ rfile.getName());
						if(bakFile.isFile()){
							String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
							LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]存在文件1，进行重命名掉！"+bakFileName);
							bakFile.renameTo(new File(bakFileName));
						}
//							FileUtils.copyFile(rfile, bakFile);
						if(rfile.renameTo(bakFile)){
							LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]文件1备份成功!");
						}	
					}
					String dataname = retstrList2.get(0).substring(0, 27);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]S04_LD_SCH_DET_.dat:"+dataname);
					if(dataname.indexOf(indexof) == 0 && dataname.lastIndexOf(tlastindexof)==(dataname.length()-tlastindexof.length())){
						file = new File(filepath+dataname);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]:"+filepath+dataname+"[encoding]:"+encoding);
						retstrList = new ArrayList<String>();
						br = new BufferedReader(new InputStreamReader(new FileInputStream(file),encoding));
						String data = null;
						while((data=br.readLine()) != null){
							LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]:"+data);
							retstrList.add(StringUtils.trimToEmpty(data));
							map.put("retstrList", retstrList);
							map.put("fileName", dataname);
						}
						if(br != null) {
                            br.close();
                        }
						/**
						 * 判断备份的文件路径是否存在
						 */
						if(filebak.length() > 0){
							File bakFile = new File(filebak +"/"+ dataname);
							if(bakFile.isFile()){
								String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
								LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]:存在文件2，进行重命名掉！"+bakFileName);
								bakFile.renameTo(new File(bakFileName));
							}
//								FileUtils.copyFile(rfile, bakFile);
							if(file.renameTo(bakFile)){
								LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]:文件2备份成功!");
							}	
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]-读文件："+e.getMessage());
		} finally {
				try {
					if(br != null) {
                        br.close();
                    }
					if(br1 != null) {
                        br1.close();
                    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
		}
		LogManager.getLogger(LogManager.MODEL_BATCH).info("[readfile]:retstrList.size():"+retstrList.size());
		LogManager.getLogger(LogManager.MODEL_BATCH).info("**********************************************************************************************************************");		
		return map;
//		return retstrList;
	}
	
	
	/**
	 * 批量读取ECIF文件
	 * 根据前后通配符去读取文件
	 * @param filepath  读文件地址
	 * @param filebak   备份文件地址
	 * @param indexof   文件名起始匹配符
	 * @param lastindexof 文件名结束通配符
	 * @return   数据返回后再进行按标志位拆分 例如：|， @！@
	 */
	public static List<EcifBatch> readEcifFile(String filepath,String filebak,String indexof,String lastindexof,String encoding){
		BufferedReader br = null;
		List<String> retstrList = new ArrayList<String>();
		List<EcifBatch> ecif = new ArrayList<EcifBatch>();
		EcifBatch eb = new EcifBatch();
		try {
			File file = new File(filepath);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[readfile]-读文件的文件夹路径不存在："+filepath);
//				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-读文件的文件夹路径不存在："+filepath);
//				return retstrList;
				return ecif;
			}
			
			File[] files = file.listFiles();
			if(files.length<=0 || null == files){
				System.out.println("[readfile]-本次扫描路径下不存在文件："+filepath);
//				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-本次扫描路径下不存在文件："+filepath);
//				return retstrList;
				return ecif;
			}
			
			if(files.length>0){
				
				for(File rfile:files)
				{
					
					if(rfile.getName().indexOf(indexof) == 0 && rfile.getName().lastIndexOf(lastindexof)==(rfile.getName().length()-lastindexof.length())){
						retstrList = new ArrayList<String>();
						br = new BufferedReader(new InputStreamReader(new FileInputStream(rfile),encoding));
						String data = null;
						while((data=br.readLine()) != null){
							retstrList.add(data);
						}
//						LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("retstrList  ：："+retstrList);
						if(retstrList.size()>0&&retstrList.size()!=1){
							 eb = new EcifBatch();
							 eb.setName(retstrList.get(0));
							 eb.setLenght(retstrList.get(1));
							 eb.setRow(retstrList.get(2));
							 ecif.add(eb);
						}
						if(retstrList.size()==1){
							String[] strList = retstrList.get(0).split(" ");
							eb = new EcifBatch();
							eb.setName(strList[0]);
							eb.setLenght(strList[1]);
							eb.setRow(strList[2]);
							ecif.add(eb);
						}
//						LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("ecif  ：："+ecif);
						if(br != null) {
                            br.close();
                        }
						/**
						 * 判断备份的文件路径是否存在
						 */
						if(filebak.length() > 0){
							File bakFile = new File(filebak +"/"+ rfile.getName());
							if(bakFile.isFile()){
								String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
								System.out.println("存在文件，进行重命名掉！"+bakFileName);
//								LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("存在文件，进行重命名掉！"+bakFileName);
								bakFile.renameTo(new File(bakFileName));
							}
//							FileUtils.copyFile(rfile, bakFile);
							if(rfile.renameTo(bakFile)){
								System.out.println("文件备份成功!");
//								LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("文件备份成功!");
							}	
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[readfile]-读文件："+e.getMessage());
//			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-读文件："+e.getMessage());
		} finally {
				try {
					if(br != null) {
                        br.close();
                    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
		}
//		return retstrList;
		return ecif;
	}
	
	/**
	 * 根据前后通配符去读取文件
	 * @param filepath  读文件地址
	 * @param filebak   备份文件地址
	 * @param indexof   文件名起始匹配符
	 * @param lastindexof 文件名结束通配符
	 * @return   数据返回后再进行按标志位拆分 例如：|， @！@
	 */
	public static List<String> readFile(String filepath,String filebak,String indexof,String lastindexof,String encoding){
		BufferedReader br = null;
		List<String> retstrList = new ArrayList<String>();
		try {
			File file = new File(filepath);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[readfile]-读文件的文件夹路径不存在："+filepath);
				return retstrList;
			}
			
			File[] files = file.listFiles();
			if(files.length<=0 || null == files){
				System.out.println("[readfile]-本次扫描路径下不存在文件："+filepath);
				return retstrList;
			}
			
			if(files.length>0){
				
				for(File rfile:files)
				{
					
					if(rfile.getName().indexOf(indexof) == 0 && rfile.getName().lastIndexOf(lastindexof)==(rfile.getName().length()-lastindexof.length())){
						retstrList = new ArrayList<String>();
						br = new BufferedReader(new InputStreamReader(new FileInputStream(rfile),encoding));
						String data = null;
						while((data=br.readLine()) != null){
							retstrList.add(data);
						}
						
						if(br != null) {
                            br.close();
                        }
						/**
						 * 判断备份的文件路径是否存在
						 */
						if(filebak.length() > 0){
							File bakFile = new File(filebak +"/"+ rfile.getName());
							if(bakFile.isFile()){
								String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
								System.out.println("存在文件，进行重命名掉！"+bakFileName);
								bakFile.renameTo(new File(bakFileName));
							}
//							FileUtils.copyFile(rfile, bakFile);
							if(rfile.renameTo(bakFile)){
								System.out.println("文件备份成功!");
							}	
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[readfile]-读文件："+e.getMessage());
		} finally {
				try {
					if(br != null) {
                        br.close();
                    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
		}
		return retstrList;
	}
	/**
	 * 直接根据文件名称读取目录下的文件
	 * @param filepath   读取路径
	 * @param filebak    备份路径
	 * @param filename   指定文件名
	 * @param encoding   编码格式
	 * @return
	 */
	public static List<String> readFile(String filepath,String filebak,String filename,int type,String encoding){
		BufferedReader br = null;
		List<String> retstrList = new ArrayList<String>();
		try {
			File file = new File(filepath);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[readfile]-读文件的文件夹路径不存在："+filepath);
//				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-读文件的文件夹路径不存在："+filepath);
				return retstrList;
			}
			
			File[] files = file.listFiles();
			if(files.length<=0 || null == files){
				System.out.println("[readfile]-本次扫描路径下不存在文件："+filepath);
//				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-本次扫描路径下不存在文件："+filepath);
				return retstrList;
			}
			
			if(files.length>0){
				
				for(File rfile:files)
				{
//					System.out.println(rfile.getName()+" *match* "+filename);
					if(rfile.getName().equals(filename)){
						retstrList = new ArrayList<String>();
						br = new BufferedReader(new InputStreamReader(new FileInputStream(rfile),encoding));
						String data = null;
						while((data=br.readLine()) != null){
							System.out.println("文件数据 : "+data);
							retstrList.add(data);
						}
//						LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("文件数据retstrList : "+retstrList);
						if(br != null) {
                            br.close();
                        }
						/**
						 * 判断备份的文件路径是否存在
						 */
						if(filebak.length() > 0){
							File bakFile = new File(filebak +"/"+ rfile.getName());
							if(bakFile.isFile()){
								String bakFileName = bakFile.getAbsoluteFile()+"."+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
								System.out.println("存在文件，进行重命名掉！"+bakFileName);
//								LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("存在文件，进行重命名掉！"+bakFileName);
								bakFile.renameTo(new File(bakFileName));
							}
							
//							FileUtils.copyFile(rfile, bakFile);
							if(rfile.renameTo(bakFile)){
								System.out.println("文件备份成功!");
//								LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("文件备份成功!");
							}	
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[readfile]-读文件："+e.getMessage());
//			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("[readfile]-读文件："+e.getMessage());
		} finally {
				try {
					if(br != null) {
                        br.close();
                    }
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
		}
		return retstrList;
	}
	
	/**
	 * 
	 * @param data       写文件的数据
	 * @param filename   文件名称-自己定义
	 * @param bakpath    备份路径-直接生成与此目录，生成完转移至GTP目录
	 * @param filepath   GTP上传目录
	 */
	public static Map<String, Object> writeFile2(String data,String filename,String bakpath,String filepath,String encoding) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			File file = new File(bakpath);
			file.setExecutable(true, false);//设置可执行权限  
			file.setReadable(true, false);//设置可读权限  
			file.setWritable(true, false);//设置可写权限 
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[writeFile]-写文件的文件夹路径不存在："+bakpath);
				map.put("retcode", "1001");
				map.put("retmsg", "写文件的文件夹路径不存在："+bakpath);
				return map;
			}
			File fileupdate = new File(filepath);
			fileupdate.setExecutable(true, false);//设置可执行权限  
			fileupdate.setReadable(true, false);//设置可读权限  
			fileupdate.setWritable(true, false);//设置可写权限 
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!fileupdate.exists()) {
				System.out.println("[writeFile]-上传文件的文件夹路径不存在："+filepath);
				map.put("retcode", "1002");
				map.put("retmsg", "上传文件的文件夹路径不存在："+filepath);
				return map;
			}
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			File file1 = new File(bakpath+format.format(new Date()));
			file1.setExecutable(true, false);//设置可执行权限  
			file1.setReadable(true, false);//设置可读权限  
			file1.setWritable(true, false);//设置可写权限 
			if(!file1.exists()){
				file1.mkdirs();
			}
			File file2 = new File(filepath+format.format(new Date()));
			file2.setExecutable(true, false);//设置可执行权限  
			file2.setReadable(true, false);//设置可读权限  
			file2.setWritable(true, false);//设置可写权限 
			if(!file2.exists()){
				file2.mkdirs();
			}
			/**
			 * 文件先生成至备份目录，生成完成后转移到GTP上传目录
			 */
			boolean flag = writeToFile(data,file1+"/"+filename,encoding);
			if(flag){
				File movefile = new File(file2 +"/"+ filename);
				movefile.setExecutable(true, false);//设置可执行权限  
				movefile.setReadable(true, false);//设置可读权限  
				movefile.setWritable(true, false);//设置可写权限 
				FileUtils.copyFile(new File(file1 +"/"+ filename), movefile);
				processShellScript(movefile.getPath());
//				if(new File(bakpath+filename).renameTo(movefile)){
//					System.out.println("文件转移成功!");
//				}
			}else{
				
			}
			map.put("retcode", "0000");
			map.put("retmsg", "写文件处理成功");
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("[writeFile]-写文件处理："+e.getMessage());
			map.put("retcode", "9999");
			map.put("retmsg", "写文件处理"+e.getMessage());
		}
		return map;
	}
	
	
	/**
	 * 
	 * @param data       写文件的数据
	 * @param filename   文件名称-自己定义
	 * @param bakpath    备份路径-直接生成与此目录，生成完转移至GTP目录
	 * @param filepath   GTP上传目录
	 */
	public static Map<String, Object> writeFile(String data,String filename,String bakpath,String filepath,String encoding) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			File file = new File(bakpath);
			file.setWritable(true, false);
			file.setExecutable(true, false);//设置可执行权限  
			file.setReadable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[writeFile]-写文件的文件夹路径不存在："+bakpath);
				map.put("retcode", "1001");
				map.put("retmsg", "写文件的文件夹路径不存在："+bakpath);
				return map;
			}
			File fileupdate = new File(filepath);
			fileupdate.setWritable(true, false);
			fileupdate.setExecutable(true, false);//设置可执行权限  
			fileupdate.setReadable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!fileupdate.exists()) {
				System.out.println("[writeFile]-上传文件的文件夹路径不存在："+filepath);
				map.put("retcode", "1002");
				map.put("retmsg", "上传文件的文件夹路径不存在："+filepath);
				return map;
			}
			/**
			 * 文件先生成至备份目录，生成完成后转移到GTP上传目录
			 */
			boolean flag1 = writeToFile(data,bakpath+filename,encoding);
			if(flag1){
				File movefile = new File(filepath +"/"+ filename);
				movefile.setWritable(true, false);
				movefile.setExecutable(true, false);//设置可执行权限  
				movefile.setReadable(true, false);
				FileUtils.copyFile(new File(bakpath+filename), movefile);
				processShellScript(movefile.getPath());
//				if(new File(bakpath+filename).renameTo(movefile)){
//					System.out.println("文件转移成功!");
//				}
			}
			map.put("retcode", "0000");
			map.put("retmsg", "写文件处理成功");
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("[writeFile]-写文件处理："+e.getMessage());
			map.put("retcode", "9999");
			map.put("retmsg", "写文件处理"+e.getMessage());
		}
		return map;
	}
	public static boolean writeToFile(String data,String filename,String encoding) throws Exception{
		boolean flag = true;
		File writefile = new File(filename);
		writefile.setWritable(true, false);
		writefile.setExecutable(true, false);//设置可执行权限  
		writefile.setReadable(true, false);
		FileOutputStream writer = null ;
		writefile.delete();
		try{
		
			writer = new FileOutputStream(writefile);
			writer.write(data.getBytes(encoding));
			
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("[writeFile]-写文件："+e.getMessage());
			flag = false;
		}finally{
			if(writer != null) {
                writer.close();
            }
			processShellScript(writefile.getPath());
			processShellScript(filename);
			System.out.println("*********************************************************************************");
			LogManager.getLogger("*********************************************************************************");
		}
		return flag;
	}
	
	/**
	 * 
	 * @param data       写文件的数据
	 * @param filename   文件名称-自己定义
	 * @param bakpath    备份路径-直接生成与此目录，生成完转移至GTP目录
	 * @param filepath   GTP上传目录
	 */
	public static boolean writeFileT24(String data,String filename,String bakpath,String filepath,String encoding) {
		boolean flag = true;
		try{
			File file = new File(bakpath);
			file.setWritable(true, false);
			file.setExecutable(true, false);//设置可执行权限  
			file.setReadable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[writeFile]-写文件的文件夹路径不存在："+bakpath);
				file.mkdirs();
			}
			File fileupdate = new File(filepath);
			fileupdate.setWritable(true, false);
			fileupdate.setExecutable(true, false);//设置可执行权限  
			fileupdate.setReadable(true, false);
			//判断文件夹是否存在,如果不存在则创建文件夹
			if (!fileupdate.exists()) {
				System.out.println("[writeFile]-上传文件的文件夹路径不存在："+filepath);
				fileupdate.mkdirs();
			}
			/**
			 * 文件先生成至备份目录，生成完成后转移到GTP上传目录
			 */
			flag = writeToFile(data,filepath+filename,encoding);
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("[writeFile]-写文件："+e.getMessage());
			flag = false;
		}
		return flag;
	}
	
	public static void processShellScript(String fileName){
		Process pro = null;
		 try {  
	            pro = Runtime.getRuntime().exec(new String[]{"sh",  
	            System.getProperty("web.root")+"/sh/chmod.sh",fileName});  
	            pro.waitFor();   
	            System.out.println("*********************************************************************************");
	            LogManager.getLogger("*********************************************************************************");
	        } catch (Exception e) {  
	        	//e.printStackTrace();
				throw new RException(e);
	        } finally{
	        	if(null != pro) {
                    pro.destroy();
                }
	        }
	}
}
