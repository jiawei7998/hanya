package com.singlee.capital.interfacex.qdb.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.interfacex.qdb.mapper.InstitutionSynchMapper;
import com.singlee.capital.interfacex.qdb.service.InstitutionSynchService;
import com.singlee.capital.system.model.TtInstitution;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class InstitutionSynchServiceImpl implements InstitutionSynchService{

	@Autowired
	private InstitutionSynchMapper mapper;
	
	@Override
	public void deleteESB() {
		mapper.deleteESB();
	}

	@Override
	public void insertESB(List<TtInstitution> insts) {
		mapper.insertESB(insts);
	}

	@Override
	public void updateInstPId(Map<String, String> rels) {
		mapper.updateInstPId(rels);
		
	}
	
	@Override
	public void updateUserInstMap(Map<String, String> userIDinstId) {
		mapper.updateUserInstMap(userIDinstId);
		
	}

	@Override
	public void insertUserInstMap(Map<String, String> userIDinstId) {
		
		mapper.insertUserInstMap(userIDinstId);
	}

}
