package com.singlee.capital.interfacex.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.ti2ndpayment.test package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _ErrorInfo_QNAME = new QName(
			"http://www.bankofshanghai.com/BOSFX/2010/08", "ErrorInfo");
	private final static QName _CommonRqHdr_QNAME = new QName(
			"http://www.bankofshanghai.com/BOSFX/2010/08", "CommonRqHdr");
	private final static QName _CommonRsHdr_QNAME = new QName(
			"http://www.bankofshanghai.com/BOSFX/2010/08", "CommonRsHdr");
	private final static QName _BOSFXII_QNAME = new QName(
			"http://www.bankofshanghai.com/BOSFX/2010/08", "BOSFXII");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.ti2ndpayment.test
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link RemitAddrRec }
	 * 
	 */
	public RemitAddrRec createRemitAddrRec() {
		return new RemitAddrRec();
	}

	/**
	 * Create an instance of {@link TDpCorpacctAllAaaRs }
	 * 
	 */
	public TDpCorpacctAllAaaRs createTDpCorpacctAllAaaRs() {
		return new TDpCorpacctAllAaaRs();
	}

	/**
	 * Create an instance of {@link PayDetailsRec }
	 * 
	 */
	public PayDetailsRec createPayDetailsRec() {
		return new PayDetailsRec();
	}

	/**
	 * Create an instance of {@link ErrorInfo }
	 * 
	 */
	public ErrorInfo createErrorInfo() {
		return new ErrorInfo();
	}

	/**
	 * Create an instance of {@link TDpAcctTypeInqRq }
	 * 
	 */
	public TDpAcctTypeInqRq createTDpAcctTypeInqRq() {
		return new TDpAcctTypeInqRq();
	}

	/**
	 * Create an instance of {@link OffBalRec }
	 * 
	 */
	public OffBalRec createOffBalRec() {
		return new OffBalRec();
	}

	/**
	 * Create an instance of {@link TExCurrencyAllInqRec }
	 * 
	 */
	public TExCurrencyAllInqRec createTExCurrencyAllInqRec() {
		return new TExCurrencyAllInqRec();
	}

	/**
	 * Create an instance of {@link FbNoRec }
	 * 
	 */
	public FbNoRec createFbNoRec() {
		return new FbNoRec();
	}

	/**
	 * Create an instance of {@link TStBeaRepayAaaRs }
	 * 
	 */
	public TStBeaRepayAaaRs createTStBeaRepayAaaRs() {
		return new TStBeaRepayAaaRs();
	}

	/**
	 * Create an instance of {@link TStFtRemitAaaRs }
	 * 
	 */
	public TStFtRemitAaaRs createTStFtRemitAaaRs() {
		return new TStFtRemitAaaRs();
	}

	/**
	 * Create an instance of {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr createCommonRsHdr() {
		return new CommonRsHdr();
	}

	/**
	 * Create an instance of {@link TDpAcctTypeInqRs }
	 * 
	 */
	public TDpAcctTypeInqRs createTDpAcctTypeInqRs() {
		return new TDpAcctTypeInqRs();
	}

	/**
	 * Create an instance of {@link TCoHolidayAllInqRs }
	 * 
	 */
	public TCoHolidayAllInqRs createTCoHolidayAllInqRs() {
		return new TCoHolidayAllInqRs();
	}

	/**
	 * Create an instance of {@link TCoSignonMultAaaRs }
	 * 
	 */
	public TCoSignonMultAaaRs createTCoSignonMultAaaRs() {
		return new TCoSignonMultAaaRs();
	}

	/**
	 * Create an instance of {@link BenCustRec }
	 * 
	 */
	public BenCustRec createBenCustRec() {
		return new BenCustRec();
	}

	/**
	 * Create an instance of {@link CreditCustRec }
	 * 
	 */
	public CreditCustRec createCreditCustRec() {
		return new CreditCustRec();
	}

	/**
	 * Create an instance of {@link DebitBeaRec }
	 * 
	 */
	public DebitBeaRec createDebitBeaRec() {
		return new DebitBeaRec();
	}

	/**
	 * Create an instance of {@link TDpCorpStmtInqRec }
	 * 
	 */
	public TDpAcStmtInfoRec createTDpCorpStmtInqRec() {
		return new TDpAcStmtInfoRec();
	}

	/**
	 * Create an instance of {@link TStFtNormRevRq }
	 * 
	 */
	public TStFtNormRevRq createTStFtNormRevRq() {
		return new TStFtNormRevRq();
	}

	/**
	 * Create an instance of {@link DebitCustRec }
	 * 
	 */
	public DebitCustRec createDebitCustRec() {
		return new DebitCustRec();
	}

	/**
	 * Create an instance of {@link TCoSignonMultAaaRq }
	 * 
	 */
	public TCoSignonMultAaaRq createTCoSignonMultAaaRq() {
		return new TCoSignonMultAaaRq();
	}

	/**
	 * Create an instance of {@link TExReconAllInqRs }
	 * 
	 */
	public TExReconAllInqRs createTExReconAllInqRs() {
		return new TExReconAllInqRs();
	}

	/**
	 * Create an instance of {@link BOSFXII }
	 * 
	 */
	public BOSFXII createBOSFXII() {
		return new BOSFXII();
	}

	/**
	 * Create an instance of {@link TDpAcStmtInqRq }
	 * 
	 */
	public TDpAcStmtInqRq createTDpAcStmtInqRq() {
		return new TDpAcStmtInqRq();
	}

	/**
	 * Create an instance of {@link OurRec }
	 * 
	 */
	public OurRec createOurRec() {
		return new OurRec();
	}

	/**
	 * Create an instance of {@link TExReconAllInqRq }
	 * 
	 */
	public TExReconAllInqRq createTExReconAllInqRq() {
		return new TExReconAllInqRq();
	}

	/**
	 * Create an instance of {@link TCoSignoffMultAaaRs }
	 * 
	 */
	public TCoSignoffMultAaaRs createTCoSignoffMultAaaRs() {
		return new TCoSignoffMultAaaRs();
	}

	/**
	 * Create an instance of {@link TDpAcStmtInqRs }
	 * 
	 */
	public TDpAcStmtInqRs createTDpAcStmtInqRs() {
		return new TDpAcStmtInqRs();
	}

	/**
	 * Create an instance of {@link TDpCorpacctAllAaaRq }
	 * 
	 */
	public TDpCorpacctAllAaaRq createTDpCorpacctAllAaaRq() {
		return new TDpCorpacctAllAaaRq();
	}

	/**
	 * Create an instance of {@link TStFtNormRevRs }
	 * 
	 */
	public TStFtNormRevRs createTStFtNormRevRs() {
		return new TStFtNormRevRs();
	}

	/**
	 * Create an instance of {@link IntermedBankRec }
	 * 
	 */
	public IntermedBankRec createIntermedBankRec() {
		return new IntermedBankRec();
	}

	/**
	 * Create an instance of {@link TExCurrencyAllInqRq }
	 * 
	 */
	public TExCurrencyAllInqRq createTExCurrencyAllInqRq() {
		return new TExCurrencyAllInqRq();
	}

	/**
	 * Create an instance of {@link TCoHolidayAllInqRq }
	 * 
	 */
	public TCoHolidayAllInqRq createTCoHolidayAllInqRq() {
		return new TCoHolidayAllInqRq();
	}

	/**
	 * Create an instance of {@link TExReconAllInqRec }
	 * 
	 */
	public TExReconAllInqRec createTExReconAllInqRec() {
		return new TExReconAllInqRec();
	}

	/**
	 * Create an instance of {@link TStFtRemitAaaRq }
	 * 
	 */
	public TStFtRemitAaaRq createTStFtRemitAaaRq() {
		return new TStFtRemitAaaRq();
	}

	/**
	 * Create an instance of {@link OrderingCustRec }
	 * 
	 */
	public OrderingCustRec createOrderingCustRec() {
		return new OrderingCustRec();
	}

	/**
	 * Create an instance of {@link CreditBeaRec }
	 * 
	 */
	public CreditBeaRec createCreditBeaRec() {
		return new CreditBeaRec();
	}

	/**
	 * Create an instance of {@link TStBeaRepayAaaRq }
	 * 
	 */
	public TStBeaRepayAaaRq createTStBeaRepayAaaRq() {
		return new TStBeaRepayAaaRq();
	}

	/**
	 * Create an instance of {@link AcctWithBankRec }
	 * 
	 */
	public AcctWithBankRec createAcctWithBankRec() {
		return new AcctWithBankRec();
	}

	/**
	 * Create an instance of {@link TCoSignoffMultAaaRq }
	 * 
	 */
	public TCoSignoffMultAaaRq createTCoSignoffMultAaaRq() {
		return new TCoSignoffMultAaaRq();
	}

	/**
	 * Create an instance of {@link TExCurrencyAllInqRs }
	 * 
	 */
	public TExCurrencyAllInqRs createTExCurrencyAllInqRs() {
		return new TExCurrencyAllInqRs();
	}

	/**
	 * Create an instance of {@link OtherRec }
	 * 
	 */
	public OtherRec createOtherRec() {
		return new OtherRec();
	}

	/**
	 * Create an instance of {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr createCommonRqHdr() {
		return new CommonRqHdr();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ErrorInfo }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.bankofshanghai.com/BOSFX/2010/08", name = "ErrorInfo")
	public JAXBElement<ErrorInfo> createErrorInfo(ErrorInfo value) {
		return new JAXBElement<ErrorInfo>(_ErrorInfo_QNAME, ErrorInfo.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link CommonRqHdr }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.bankofshanghai.com/BOSFX/2010/08", name = "CommonRqHdr")
	public JAXBElement<CommonRqHdr> createCommonRqHdr(CommonRqHdr value) {
		return new JAXBElement<CommonRqHdr>(_CommonRqHdr_QNAME,
				CommonRqHdr.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link CommonRsHdr }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.bankofshanghai.com/BOSFX/2010/08", name = "CommonRsHdr")
	public JAXBElement<CommonRsHdr> createCommonRsHdr(CommonRsHdr value) {
		return new JAXBElement<CommonRsHdr>(_CommonRsHdr_QNAME,
				CommonRsHdr.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link BOSFXII }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.bankofshanghai.com/BOSFX/2010/08", name = "BOSFXII")
	public JAXBElement<BOSFXII> createBOSFXII(BOSFXII value) {
		return new JAXBElement<BOSFXII>(_BOSFXII_QNAME, BOSFXII.class, null,
				value);
	}

}
