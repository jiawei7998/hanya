package com.singlee.capital.interfacex.qdb.model;

/***
 * 公允价值
 * @author lee
 *
 */
public class TdTrdMtm {
	private String  dealNo      ; 
	private double  valTd       ; 
	private double  valYstd     ; 
	private double  valDiff     ; 
	private double  valDiffYstd;  
	private String  valDate     ; 
	private double  val1         ;
	private double  val2         ;
	private String  remark1      ;
	private String  remark2      ;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public double getValTd() {
		return valTd;
	}
	public void setValTd(double valTd) {
		this.valTd = valTd;
	}
	public double getValYstd() {
		return valYstd;
	}
	public void setValYstd(double valYstd) {
		this.valYstd = valYstd;
	}
	public double getValDiff() {
		return valDiff;
	}
	public void setValDiff(double valDiff) {
		this.valDiff = valDiff;
	}
	public double getValDiffYstd() {
		return valDiffYstd;
	}
	public void setValDiffYstd(double valDiffYstd) {
		this.valDiffYstd = valDiffYstd;
	}
	public String getValDate() {
		return valDate;
	}
	public void setValDate(String valDate) {
		this.valDate = valDate;
	}
	public double getVal1() {
		return val1;
	}
	public void setVal1(double val1) {
		this.val1 = val1;
	}
	public double getVal2() {
		return val2;
	}
	public void setVal2(double val2) {
		this.val2 = val2;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

}
