package com.singlee.capital.interfacex.qdb.ESB.model;


public class SM5309ReqtBean extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pageKey;//页码
	private String searchKeys;//
	private String cifNo;
	private String cifNm;
	private String acctNbr; //账号
    private String acctType; //账户类型
    private String acctCurrCde; //账户币种
    private String prodType; //产品
    private String acctBal; //账户余额
    private String acctOpenDt; //开户日期
    private String judiFrzInd; //司法查冻扣标识
    private String brNbr; //支行号
    private String availBal; //可用余额
    private String forexType; //钞汇标识
    private String acctStat; //账户状态
    private String acctNature; //账户性质
    private String stmtPassbookInd; //账单/存折标识
    private String intRate; //利率
    private String hldCde1; //抑制代码1
    private String hldCde2; //抑制代码2
    private String hldCde3; //抑制代码3
    private String hldCde4; //抑制代码4
    private String hldCde5; //抑制代码5
    private String subHldCde0101; //子抑制代码1
    private String subHldCde0102; //子抑制代码2
    private String subHldCde0103; //子抑制代码3
    private String subHldCde0104; //子抑制代码4
    private String subHldCde0105; //子抑制代码5
    private String moreRecInd; //更多记录标识
    private String entrustedBr; //委托行
    private String acctStat0100; //账户状态
    private String hasLoss; //是否挂失
    private String cardNbr; //卡号
    private String cardType; //卡类型
    private String passbookSerNo; //存折凭证号
    private String frozenAmt0300; //冻结金额2
    private String acctName; //账户名称
    private String acctBalRMB; //RMB 账户余额
    private String availBalRMB; //RMB 可用余额
    private String netSetlAmt; //实付金额
    private String acctAddName1; //账户附加名称1
    private String nra; //NRA标识
    private String colUsed; //同城抵用
    private String frozenAmt; //冻结金额
    private String respCde;
    private String rtrnDataQName;
    private String rtrnDataQLib;
    private String transCde;
    private String sysRefNbr;
    private String compntRefNbr;
    private String srvrReconcNbr;
    private String nbrOfRecToRetrv;
    private String searchMeth;
    private String sqlInfo;
    private String applCde1;
    private String respErrCde1;
    private String respRsnForCde1;
    private String applCde2;
    private String respErrCde2;
    private String respRsnForCde2;
    private String applCde3;
    private String respErrCde3;
    private String respRsnForCde3;
    private String applCde4;
    private String respErrCde4;
    private String respRsnForCde4;
    private String applCde5;
    private String respErrCde5;
    private String respRsnForCde5;
    private String dtInFromClt;
    private String tmInFromClt;
    private String dtOut;
    private String transType;
    private String msgInd;
	public String getRespCde() {
		return respCde;
	}
	public void setRespCde(String respCde) {
		this.respCde = respCde;
	}
	public String getRtrnDataQName() {
		return rtrnDataQName;
	}
	public void setRtrnDataQName(String rtrnDataQName) {
		this.rtrnDataQName = rtrnDataQName;
	}
	public String getRtrnDataQLib() {
		return rtrnDataQLib;
	}
	public void setRtrnDataQLib(String rtrnDataQLib) {
		this.rtrnDataQLib = rtrnDataQLib;
	}
	public String getTransCde() {
		return transCde;
	}
	public void setTransCde(String transCde) {
		this.transCde = transCde;
	}
	public String getSysRefNbr() {
		return sysRefNbr;
	}
	public void setSysRefNbr(String sysRefNbr) {
		this.sysRefNbr = sysRefNbr;
	}
	public String getCompntRefNbr() {
		return compntRefNbr;
	}
	public void setCompntRefNbr(String compntRefNbr) {
		this.compntRefNbr = compntRefNbr;
	}
	public String getSrvrReconcNbr() {
		return srvrReconcNbr;
	}
	public void setSrvrReconcNbr(String srvrReconcNbr) {
		this.srvrReconcNbr = srvrReconcNbr;
	}
	public String getNbrOfRecToRetrv() {
		return nbrOfRecToRetrv;
	}
	public void setNbrOfRecToRetrv(String nbrOfRecToRetrv) {
		this.nbrOfRecToRetrv = nbrOfRecToRetrv;
	}
	public String getSearchMeth() {
		return searchMeth;
	}
	public void setSearchMeth(String searchMeth) {
		this.searchMeth = searchMeth;
	}
	public String getSqlInfo() {
		return sqlInfo;
	}
	public void setSqlInfo(String sqlInfo) {
		this.sqlInfo = sqlInfo;
	}
	public String getApplCde1() {
		return applCde1;
	}
	public void setApplCde1(String applCde1) {
		this.applCde1 = applCde1;
	}
	public String getRespErrCde1() {
		return respErrCde1;
	}
	public void setRespErrCde1(String respErrCde1) {
		this.respErrCde1 = respErrCde1;
	}
	public String getRespRsnForCde1() {
		return respRsnForCde1;
	}
	public void setRespRsnForCde1(String respRsnForCde1) {
		this.respRsnForCde1 = respRsnForCde1;
	}
	public String getApplCde2() {
		return applCde2;
	}
	public void setApplCde2(String applCde2) {
		this.applCde2 = applCde2;
	}
	public String getRespErrCde2() {
		return respErrCde2;
	}
	public void setRespErrCde2(String respErrCde2) {
		this.respErrCde2 = respErrCde2;
	}
	public String getRespRsnForCde2() {
		return respRsnForCde2;
	}
	public void setRespRsnForCde2(String respRsnForCde2) {
		this.respRsnForCde2 = respRsnForCde2;
	}
	public String getApplCde3() {
		return applCde3;
	}
	public void setApplCde3(String applCde3) {
		this.applCde3 = applCde3;
	}
	public String getRespErrCde3() {
		return respErrCde3;
	}
	public void setRespErrCde3(String respErrCde3) {
		this.respErrCde3 = respErrCde3;
	}
	public String getRespRsnForCde3() {
		return respRsnForCde3;
	}
	public void setRespRsnForCde3(String respRsnForCde3) {
		this.respRsnForCde3 = respRsnForCde3;
	}
	public String getApplCde4() {
		return applCde4;
	}
	public void setApplCde4(String applCde4) {
		this.applCde4 = applCde4;
	}
	public String getRespErrCde4() {
		return respErrCde4;
	}
	public void setRespErrCde4(String respErrCde4) {
		this.respErrCde4 = respErrCde4;
	}
	public String getRespRsnForCde4() {
		return respRsnForCde4;
	}
	public void setRespRsnForCde4(String respRsnForCde4) {
		this.respRsnForCde4 = respRsnForCde4;
	}
	public String getApplCde5() {
		return applCde5;
	}
	public void setApplCde5(String applCde5) {
		this.applCde5 = applCde5;
	}
	public String getRespErrCde5() {
		return respErrCde5;
	}
	public void setRespErrCde5(String respErrCde5) {
		this.respErrCde5 = respErrCde5;
	}
	public String getRespRsnForCde5() {
		return respRsnForCde5;
	}
	public void setRespRsnForCde5(String respRsnForCde5) {
		this.respRsnForCde5 = respRsnForCde5;
	}
	public String getDtInFromClt() {
		return dtInFromClt;
	}
	public void setDtInFromClt(String dtInFromClt) {
		this.dtInFromClt = dtInFromClt;
	}
	public String getTmInFromClt() {
		return tmInFromClt;
	}
	public void setTmInFromClt(String tmInFromClt) {
		this.tmInFromClt = tmInFromClt;
	}
	public String getDtOut() {
		return dtOut;
	}
	public void setDtOut(String dtOut) {
		this.dtOut = dtOut;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getMsgInd() {
		return msgInd;
	}
	public void setMsgInd(String msgInd) {
		this.msgInd = msgInd;
	}
	public String getCifNm() {
		return cifNm;
	}
	public void setCifNm(String cifNm) {
		this.cifNm = cifNm;
	}
	public String getCifNo() {
		return cifNo;
	}
	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}
	public String getPageKey() {
		return pageKey;
	}
	public void setPageKey(String pageKey) {
		this.pageKey = pageKey;
	}
	public String getSearchKeys() {
		return searchKeys;
	}
	public void setSearchKeys(String searchKeys) {
		this.searchKeys = searchKeys;
	}
	public String getAcctNbr() {
		return acctNbr;
	}
	public void setAcctNbr(String acctNbr) {
		this.acctNbr = acctNbr;
	}
	public String getAcctType() {
		return acctType;
	}
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	public String getAcctCurrCde() {
		return acctCurrCde;
	}
	public void setAcctCurrCde(String acctCurrCde) {
		this.acctCurrCde = acctCurrCde;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getAcctBal() {
		return acctBal;
	}
	public void setAcctBal(String acctBal) {
		this.acctBal = acctBal;
	}
	public String getAcctOpenDt() {
		return acctOpenDt;
	}
	public void setAcctOpenDt(String acctOpenDt) {
		this.acctOpenDt = acctOpenDt;
	}
	public String getJudiFrzInd() {
		return judiFrzInd;
	}
	public void setJudiFrzInd(String judiFrzInd) {
		this.judiFrzInd = judiFrzInd;
	}
	public String getBrNbr() {
		return brNbr;
	}
	public void setBrNbr(String brNbr) {
		this.brNbr = brNbr;
	}
	public String getAvailBal() {
		return availBal;
	}
	public void setAvailBal(String availBal) {
		this.availBal = availBal;
	}
	public String getForexType() {
		return forexType;
	}
	public void setForexType(String forexType) {
		this.forexType = forexType;
	}
	public String getAcctStat() {
		return acctStat;
	}
	public void setAcctStat(String acctStat) {
		this.acctStat = acctStat;
	}
	public String getAcctNature() {
		return acctNature;
	}
	public void setAcctNature(String acctNature) {
		this.acctNature = acctNature;
	}
	public String getStmtPassbookInd() {
		return stmtPassbookInd;
	}
	public void setStmtPassbookInd(String stmtPassbookInd) {
		this.stmtPassbookInd = stmtPassbookInd;
	}
	public String getIntRate() {
		return intRate;
	}
	public void setIntRate(String intRate) {
		this.intRate = intRate;
	}
	public String getHldCde1() {
		return hldCde1;
	}
	public void setHldCde1(String hldCde1) {
		this.hldCde1 = hldCde1;
	}
	public String getHldCde2() {
		return hldCde2;
	}
	public void setHldCde2(String hldCde2) {
		this.hldCde2 = hldCde2;
	}
	public String getHldCde3() {
		return hldCde3;
	}
	public void setHldCde3(String hldCde3) {
		this.hldCde3 = hldCde3;
	}
	public String getHldCde4() {
		return hldCde4;
	}
	public void setHldCde4(String hldCde4) {
		this.hldCde4 = hldCde4;
	}
	public String getHldCde5() {
		return hldCde5;
	}
	public void setHldCde5(String hldCde5) {
		this.hldCde5 = hldCde5;
	}
	public String getSubHldCde0101() {
		return subHldCde0101;
	}
	public void setSubHldCde0101(String subHldCde0101) {
		this.subHldCde0101 = subHldCde0101;
	}
	public String getSubHldCde0102() {
		return subHldCde0102;
	}
	public void setSubHldCde0102(String subHldCde0102) {
		this.subHldCde0102 = subHldCde0102;
	}
	public String getSubHldCde0103() {
		return subHldCde0103;
	}
	public void setSubHldCde0103(String subHldCde0103) {
		this.subHldCde0103 = subHldCde0103;
	}
	public String getSubHldCde0104() {
		return subHldCde0104;
	}
	public void setSubHldCde0104(String subHldCde0104) {
		this.subHldCde0104 = subHldCde0104;
	}
	public String getSubHldCde0105() {
		return subHldCde0105;
	}
	public void setSubHldCde0105(String subHldCde0105) {
		this.subHldCde0105 = subHldCde0105;
	}
	public String getMoreRecInd() {
		return moreRecInd;
	}
	public void setMoreRecInd(String moreRecInd) {
		this.moreRecInd = moreRecInd;
	}
	public String getEntrustedBr() {
		return entrustedBr;
	}
	public void setEntrustedBr(String entrustedBr) {
		this.entrustedBr = entrustedBr;
	}
	public String getAcctStat0100() {
		return acctStat0100;
	}
	public void setAcctStat0100(String acctStat0100) {
		this.acctStat0100 = acctStat0100;
	}
	public String getHasLoss() {
		return hasLoss;
	}
	public void setHasLoss(String hasLoss) {
		this.hasLoss = hasLoss;
	}
	public String getCardNbr() {
		return cardNbr;
	}
	public void setCardNbr(String cardNbr) {
		this.cardNbr = cardNbr;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getPassbookSerNo() {
		return passbookSerNo;
	}
	public void setPassbookSerNo(String passbookSerNo) {
		this.passbookSerNo = passbookSerNo;
	}
	public String getFrozenAmt0300() {
		return frozenAmt0300;
	}
	public void setFrozenAmt0300(String frozenAmt0300) {
		this.frozenAmt0300 = frozenAmt0300;
	}
	public String getAcctName() {
		return acctName;
	}
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	public String getAcctBalRMB() {
		return acctBalRMB;
	}
	public void setAcctBalRMB(String acctBalRMB) {
		this.acctBalRMB = acctBalRMB;
	}
	public String getAvailBalRMB() {
		return availBalRMB;
	}
	public void setAvailBalRMB(String availBalRMB) {
		this.availBalRMB = availBalRMB;
	}
	public String getNetSetlAmt() {
		return netSetlAmt;
	}
	public void setNetSetlAmt(String netSetlAmt) {
		this.netSetlAmt = netSetlAmt;
	}
	public String getAcctAddName1() {
		return acctAddName1;
	}
	public void setAcctAddName1(String acctAddName1) {
		this.acctAddName1 = acctAddName1;
	}
	public String getNra() {
		return nra;
	}
	public void setNra(String nra) {
		this.nra = nra;
	}
	public String getColUsed() {
		return colUsed;
	}
	public void setColUsed(String colUsed) {
		this.colUsed = colUsed;
	}
	public String getFrozenAmt() {
		return frozenAmt;
	}
	public void setFrozenAmt(String frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
    
	}
