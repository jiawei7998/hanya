package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CommonRsHdr complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CommonRsHdr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServerStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RqUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SPRsUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NumTranCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonRsHdr", propOrder = { "statusCode", "serverStatusCode",
		"rqUID", "spRsUID", "numTranCode", "companyCode" })
public class CommonRsHdr {

	@XmlElement(name = "StatusCode", required = true)
	protected String statusCode;
	@XmlElement(name = "ServerStatusCode", required = true)
	protected String serverStatusCode;
	@XmlElement(name = "RqUID", required = true)
	protected String rqUID;
	@XmlElement(name = "SPRsUID", required = true)
	protected String spRsUID;
	@XmlElement(name = "NumTranCode", required = true)
	protected String numTranCode;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;

	/**
	 * Gets the value of the statusCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the value of the statusCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatusCode(String value) {
		this.statusCode = value;
	}

	/**
	 * Gets the value of the serverStatusCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServerStatusCode() {
		return serverStatusCode;
	}

	/**
	 * Sets the value of the serverStatusCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServerStatusCode(String value) {
		this.serverStatusCode = value;
	}

	/**
	 * Gets the value of the rqUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRqUID() {
		return rqUID;
	}

	/**
	 * Sets the value of the rqUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRqUID(String value) {
		this.rqUID = value;
	}

	/**
	 * Gets the value of the spRsUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPRsUID() {
		return spRsUID;
	}

	/**
	 * Sets the value of the spRsUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPRsUID(String value) {
		this.spRsUID = value;
	}

	/**
	 * Gets the value of the numTranCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNumTranCode() {
		return numTranCode;
	}

	/**
	 * Sets the value of the numTranCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNumTranCode(String value) {
		this.numTranCode = value;
	}

	/**
	 * Gets the value of the companyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the value of the companyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}

}
