package com.singlee.capital.interfacex.qdb.ESB.model;


public class ESBSynch {
	
	private String busType;//操作类型，增删改次
	private String optType;//操作的类
	private String reqTime;//请求的时间
	private String reqDate;//
	private String noticeData;//
	
	private String userId;// 用户ID
	private String userName;// 用户姓名
	private String userPwd;// 密码
	private String isActive;// 状态 1-启用 0-停用 字典项 000031
	private String userCreatetime;// 用户创建时间
	private String userStoptime;// 用户停用时间
	private String userPwdchgtime;// 上次密码更改时间
	private String userMemo;// 备注
	private String userEmail;// 用户EMAIL
	private String userFixedphone;// 固定电话
	private String userCellphone;// 移动电话
	private String userFlag;// 标识 字典项 000003
	private String userLogontype;// 登陆方式 0000005
	private String isOnline;// 是否在线
	private String latestIp;// 最后一次ip地址
	private String latestLoginTime;// 最后一次登录时间
	private String latestTime;// 最后访问时间
	private String empId;//员工编号
	
	private String instId;// 机构编号
	private String instFullname;// 机构全称
	private String instName;// 机构简称
	private String pInstId;//父机构号
	private String instType; // 机构类型，字典
	private String instStatus; //机构状态，字典
	private String instLrInstCode; // 机构代码证
	private String linkMan; //联系人
	private String telephone; //电话
	private String mobile; //手机
	private String address; //地址
	private String onlineDate;// 上线日期
	private String bInstId;// 记账机构号
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getUserCreatetime() {
		return userCreatetime;
	}
	public void setUserCreatetime(String userCreatetime) {
		this.userCreatetime = userCreatetime;
	}
	public String getUserStoptime() {
		return userStoptime;
	}
	public void setUserStoptime(String userStoptime) {
		this.userStoptime = userStoptime;
	}
	public String getUserPwdchgtime() {
		return userPwdchgtime;
	}
	public void setUserPwdchgtime(String userPwdchgtime) {
		this.userPwdchgtime = userPwdchgtime;
	}
	public String getUserMemo() {
		return userMemo;
	}
	public void setUserMemo(String userMemo) {
		this.userMemo = userMemo;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserFixedphone() {
		return userFixedphone;
	}
	public void setUserFixedphone(String userFixedphone) {
		this.userFixedphone = userFixedphone;
	}
	public String getUserCellphone() {
		return userCellphone;
	}
	public void setUserCellphone(String userCellphone) {
		this.userCellphone = userCellphone;
	}
	public String getUserFlag() {
		return userFlag;
	}
	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}
	public String getUserLogontype() {
		return userLogontype;
	}
	public void setUserLogontype(String userLogontype) {
		this.userLogontype = userLogontype;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	public String getLatestIp() {
		return latestIp;
	}
	public void setLatestIp(String latestIp) {
		this.latestIp = latestIp;
	}
	public String getLatestLoginTime() {
		return latestLoginTime;
	}
	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}
	public String getLatestTime() {
		return latestTime;
	}
	public void setLatestTime(String latestTime) {
		this.latestTime = latestTime;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getInstFullname() {
		return instFullname;
	}
	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getpInstId() {
		return pInstId;
	}
	public void setpInstId(String pInstId) {
		this.pInstId = pInstId;
	}
	public String getInstType() {
		return instType;
	}
	public void setInstType(String instType) {
		this.instType = instType;
	}
	public String getInstStatus() {
		return instStatus;
	}
	public void setInstStatus(String instStatus) {
		this.instStatus = instStatus;
	}
	public String getInstLrInstCode() {
		return instLrInstCode;
	}
	public void setInstLrInstCode(String instLrInstCode) {
		this.instLrInstCode = instLrInstCode;
	}
	public String getLinkMan() {
		return linkMan;
	}
	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOnlineDate() {
		return onlineDate;
	}
	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}
	public String getbInstId() {
		return bInstId;
	}
	public void setbInstId(String bInstId) {
		this.bInstId = bInstId;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getOptType() {
		return optType;
	}
	public void setOptType(String optType) {
		this.optType = optType;
	}
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getNoticeData() {
		return noticeData;
	}
	public void setNoticeData(String noticeData) {
		this.noticeData = noticeData;
	}
}
