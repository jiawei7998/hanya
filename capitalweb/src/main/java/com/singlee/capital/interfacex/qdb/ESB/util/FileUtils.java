package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.File;
import java.io.FileOutputStream;


/****
 * 
 * 文件操作工具类
 * @author lee
 *
 */
public class FileUtils{

	public static void messageXMLFileSaveLocal(String data,String path, String filename, String date) throws Exception {
		writeToFile(data, path, filename, date);
	}

	public static void FileIsNotExist(String localPath,String filename,String date) throws Exception {
		File oldFile = new File(localPath+filename);
		String newPath = localPath+date+"/";
		File newFilePath = new File(newPath);
		if(!newFilePath.exists()){
			newFilePath.mkdirs();	
		}
		File newFile = new File(newPath+oldFile.getName());
		if(newFile.exists()){
			newFile.delete();
		}
		oldFile.renameTo(newFile);
		oldFile.delete();
	}
	
	/**
	 * 写文件
	 * @param data     写入文件数据
	 * @param path     路径
	 * @param filename 文件名称
	 * @param date     日期（120725）
	 * @throws Exception
	 */
	public synchronized static void writeToFile(String data,String path, String filename, String date) throws Exception{
		//新建文件路径 
		String newPath = path+date+"/";  
		File newFilePath = new File(newPath);
		
		//判断文件路径是否存在
		if(!newFilePath.exists()){
			newFilePath.mkdirs();
		}
		
		//判断文件是否存在
		File newFile = new File(newPath+filename);
		if(newFile.exists()){
			newFile.delete();
		}
		
		//开始写文件
		FileOutputStream writer = null ;
		try{
			writer = new FileOutputStream (newFile);
			writer.write(data.getBytes("UTF-8"));
		}catch (Exception e) {
			throw e;
		}finally{
			if(writer != null) {writer.close();}
		}
	}

}
