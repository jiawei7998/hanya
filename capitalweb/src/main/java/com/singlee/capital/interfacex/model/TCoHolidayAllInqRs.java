package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCoHolidayAllInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCoHolidayAllInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="Mth01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth05" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth09" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mth12" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCoHolidayAllInqRs", propOrder = { "commonRsHdr", "mth01",
		"mth02", "mth03", "mth04", "mth05", "mth06", "mth07", "mth08", "mth09",
		"mth10", "mth11", "mth12" })
public class TCoHolidayAllInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "Mth01", required = true)
	protected String mth01;
	@XmlElement(name = "Mth02", required = true)
	protected String mth02;
	@XmlElement(name = "Mth03", required = true)
	protected String mth03;
	@XmlElement(name = "Mth04", required = true)
	protected String mth04;
	@XmlElement(name = "Mth05", required = true)
	protected String mth05;
	@XmlElement(name = "Mth06", required = true)
	protected String mth06;
	@XmlElement(name = "Mth07", required = true)
	protected String mth07;
	@XmlElement(name = "Mth08", required = true)
	protected String mth08;
	@XmlElement(name = "Mth09", required = true)
	protected String mth09;
	@XmlElement(name = "Mth10", required = true)
	protected String mth10;
	@XmlElement(name = "Mth11", required = true)
	protected String mth11;
	@XmlElement(name = "Mth12", required = true)
	protected String mth12;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the mth01 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth01() {
		return mth01;
	}

	/**
	 * Sets the value of the mth01 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth01(String value) {
		this.mth01 = value;
	}

	/**
	 * Gets the value of the mth02 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth02() {
		return mth02;
	}

	/**
	 * Sets the value of the mth02 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth02(String value) {
		this.mth02 = value;
	}

	/**
	 * Gets the value of the mth03 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth03() {
		return mth03;
	}

	/**
	 * Sets the value of the mth03 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth03(String value) {
		this.mth03 = value;
	}

	/**
	 * Gets the value of the mth04 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth04() {
		return mth04;
	}

	/**
	 * Sets the value of the mth04 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth04(String value) {
		this.mth04 = value;
	}

	/**
	 * Gets the value of the mth05 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth05() {
		return mth05;
	}

	/**
	 * Sets the value of the mth05 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth05(String value) {
		this.mth05 = value;
	}

	/**
	 * Gets the value of the mth06 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth06() {
		return mth06;
	}

	/**
	 * Sets the value of the mth06 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth06(String value) {
		this.mth06 = value;
	}

	/**
	 * Gets the value of the mth07 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth07() {
		return mth07;
	}

	/**
	 * Sets the value of the mth07 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth07(String value) {
		this.mth07 = value;
	}

	/**
	 * Gets the value of the mth08 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth08() {
		return mth08;
	}

	/**
	 * Sets the value of the mth08 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth08(String value) {
		this.mth08 = value;
	}

	/**
	 * Gets the value of the mth09 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth09() {
		return mth09;
	}

	/**
	 * Sets the value of the mth09 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth09(String value) {
		this.mth09 = value;
	}

	/**
	 * Gets the value of the mth10 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth10() {
		return mth10;
	}

	/**
	 * Sets the value of the mth10 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth10(String value) {
		this.mth10 = value;
	}

	/**
	 * Gets the value of the mth11 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth11() {
		return mth11;
	}

	/**
	 * Sets the value of the mth11 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth11(String value) {
		this.mth11 = value;
	}

	/**
	 * Gets the value of the mth12 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMth12() {
		return mth12;
	}

	/**
	 * Sets the value of the mth12 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMth12(String value) {
		this.mth12 = value;
	}

}
