package com.singlee.capital.interfacex.qdb.ESB.util;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

public class DXPT01 {
	
	//组装报文
	public String packageDXPT01ReqtMsgSendToTis(String brNo,String objAddr,String msgCont){
		org.w3c.dom.Document doc = null;  
		Element root = null;
		StringWriter xmlOut = null;
		String xmlResp = null;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.newDocument();
			root = doc.createElement("reqt");
			doc.appendChild(root);
			Element svcHdr = doc.createElement("svcHdr");
			Element corrId = doc.createElement("corrId");
			corrId.setTextContent("");
			Element svcId = doc.createElement("svcId");
			svcId.setTextContent("104000");
			Element verNbr = doc.createElement("verNbr");
			verNbr.setTextContent("1.0");
			Element csmrId = doc.createElement("csmrId");
			csmrId.setTextContent("284000");
			Element csmrSerNbr = doc.createElement("csmrSerNbr");
			csmrSerNbr.setTextContent("100010001000");
			Element tmStamp = doc.createElement("tmStamp");
			tmStamp.setTextContent(getTime(new Date()));
			Element reqtIp = doc.createElement("reqtIp");
			reqtIp.setTextContent("10.1.80.185");
			svcHdr.appendChild(corrId);
			svcHdr.appendChild(svcId);
			svcHdr.appendChild(verNbr);
			svcHdr.appendChild(csmrId);
			svcHdr.appendChild(csmrSerNbr);
			svcHdr.appendChild(tmStamp);
			svcHdr.appendChild(reqtIp);
			/**
			 * appHdr
			 */
			Element appHdr = doc.createElement("appHdr");
			Element reqDt = doc.createElement("reqDt");
			reqDt.setTextContent("");
			Element reqTm = doc.createElement("reqTm");
			reqTm.setTextContent("");
			appHdr.appendChild(reqDt);
			appHdr.appendChild(reqTm);
			/**
			 * appBody
			 */
			Element appBody = doc.createElement("appBody");
			Element srvId1 = doc.createElement("srvId");
			srvId1.setTextContent("9999");
			Element chanNo1 = doc.createElement("chanNo");
			chanNo1.setTextContent("69");
			Element brNo1 = doc.createElement("brNo");
			brNo1.setTextContent(brNo);
			Element objAddr1 = doc.createElement("objAddr");
			objAddr1.setTextContent(objAddr);
			Element msgCont1 = doc.createElement("msgCont");
			msgCont1.setTextContent(msgCont);
			Element ordTm = doc.createElement("ordTm");
			ordTm.setTextContent("20140411101010");
			Element accNo = doc.createElement("accNo");
			accNo.setTextContent("");
			Element custNo = doc.createElement("custNo");
			custNo.setTextContent("");
			appBody.appendChild(srvId1);
			appBody.appendChild(brNo1);
			appBody.appendChild(objAddr1);
			appBody.appendChild(msgCont1);
			appBody.appendChild(ordTm);
			appBody.appendChild(accNo);
			appBody.appendChild(custNo);
			root.appendChild(svcHdr);
			root.appendChild(appHdr);
			root.appendChild(appBody);
			DOMSource source = new DOMSource(doc);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");// 设置文档的换行与缩进
			xmlOut = new StringWriter();
			StreamResult result = new StreamResult(xmlOut);
			transformer.transform(source, result);
			xmlResp = xmlOut.toString();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (null != xmlOut) {
				try {
					xmlOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return xmlResp;
	}
	
	
	
	public static String getTime(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss-SSS");
		return dateFormat.format(date);
	}
	


}
