package com.singlee.capital.interfacex.socketservice;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.xml.tools.XmlFormat;
import com.singlee.capital.interfacex.model.ApprovalChangeRq;
import com.singlee.capital.interfacex.model.ApprovalChangeRs;
import com.singlee.capital.interfacex.model.BOSFXII;
import com.singlee.capital.interfacex.model.LoanNoticeRq;
import com.singlee.capital.interfacex.model.LoanNoticeRs;
import com.singlee.capital.interfacex.model.QuotaSyncopateRq;
import com.singlee.capital.interfacex.model.QuotaSyncopateRs;
import com.singlee.capital.interfacex.model.RepayNoticeRq;
import com.singlee.capital.interfacex.model.RepayNoticeRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.service.impl.SocketClientServiceImpl;

/**
 * socket server
 * 
 * @author
 * 
 *         进入了
 */
public class SocketServerConnectionHandler implements Runnable {

	private boolean started = true;
	private int soTimeout;

	/** 阻塞队列 */
	private final BlockingQueue<Socket> socketQueue;
	/** */
	private final SocketClientService socketClientService;
	
	public SocketServerConnectionHandler(BlockingQueue<Socket> q, SocketClientService scs) {
		this.socketQueue = q;
		this.socketClientService = scs;
	}

	private void handleConnection(Socket connection) {
		/*
		 * System.out.println("*******ip*******");
		 * System.out.println(connection.getRemoteSocketAddress());
		 * System.out.println(Thread.currentThread().getName());
		 * System.out.println("*******ip*******");
		 */

		DataOutputStream out = null;

		DataInputStream in = null;

		String xmlMessage = null;
		String retcode = "000000";
		String retMsg = "SUCCESS";

		// //XCrmsLog.info("---------进入了SocketServerConnectionHandler--------");
		try {
			connection.setSoTimeout(60000);

			out = new DataOutputStream(connection.getOutputStream());

			in = new DataInputStream(connection.getInputStream());

			byte[] str = new byte[1];

			int i = in.read(str);

			List<Byte> list = new ArrayList<Byte>();

			list.add(str[0]);

			while (i != -1 && in.available() != 0) {
				i = in.read(str);

				list.add(str[0]);
			}
			byte[] total = new byte[list.size()];
			// System.out.println("list.size():" + list.size());
			// XCrmsLog.info("list.size():" + list.size());
			int count = 0;
			for (byte a : list) {
				total[count++] = a;
				// //System.out.println(a);
			}

			xmlMessage = new String(total, "UTF-8");
			// //System.out.println("报文内容xmlMessage:" + xmlMessage);
			// //XCrmsLog.info("报文内容xmlMessage:" + xmlMessage);
			/*
			 * System.out.println("********xmlMessage*******");
			 * System.out.println(xmlMessage);
			 * System.out.println(xmlMessage.length());
			 * System.out.println("********xmlMessage*****");
			 */
			/**
			 * 根据xmlMessage判断交易报文类型
			 */
			if (null != StringUtils.trimToNull(xmlMessage)
					&& xmlMessage.trim().length() > 0) {
				xmlMessage = xmlMessage
						.replace(
								" xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"",
								"");
				int tag_start = xmlMessage.indexOf("<BOSFXII>");
				int tag_end = xmlMessage.indexOf("<CommonRqHdr>");
				int uuid_start = xmlMessage.indexOf("<RqUID>");
				int uuid_end = xmlMessage.indexOf("</RqUID>");
				String intfaceName = xmlMessage.substring(tag_start + 10,
						tag_end - 1);
				xmlMessage.substring(uuid_start + 7, uuid_end);

				int pos = xmlMessage.indexOf("<?xml version");
				xmlMessage = xmlMessage.substring(pos);
				// LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("报文内容xmlMessage:"
				// + xmlMessage);
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
						"报文内容xmlMessage:" + (xmlMessage));


				if ("LoanNoticeRq".equals(intfaceName)) {
					// IFBM-放款通知（CRMS->同业）
					LoanNoticeRq requset = SocketClientServiceImpl
							.converyToJavaBean(xmlMessage, BOSFXII.class)
							.getLoanNoticeRq();
					LoanNoticeRs response = socketClientService
							.iFBMLoanNoticeResponse(requset);
					BOSFXII bosfxii = new BOSFXII();
					bosfxii.setLoanNoticeRs(response);
					retMsg = SocketClientServiceImpl.convertToXml(bosfxii);
					// LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("IFBM-放款通知retMsg:"
					// + retMsg);
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
							"IFBM-放款通知retMsg:" + XmlFormat.returnMsg(retMsg));

				} else if ("QuotaSyncopateRq".equals(intfaceName)) {
					// String s1 = "<DueDate>";
					// String s2 = "</DueDate>";
					// int counts = counts(xmlMessage,s1);
					// xmlMessage = replace(xmlMessage, counts);
					// 额度切分同步（CRMS->同业系统）
					QuotaSyncopateRq request = SocketClientServiceImpl
							.converyToJavaBean(xmlMessage, BOSFXII.class)
							.getQuotaSyncopateRq();
					QuotaSyncopateRs response = socketClientService
							.iFBMQuotaSyncopateResponse(request);
					BOSFXII bosfxii = new BOSFXII();
					bosfxii.setQuotaSyncopateRs(response);
					retMsg = SocketClientServiceImpl.convertToXml(bosfxii);
					// LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("切分同步retMsg:"
					// + retMsg);
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
							"切分同步retMsg:" + XmlFormat.returnMsg(retMsg));

				} else if ("ApprovalChangeRq".equals(intfaceName)) {
					// IFBM-批复信息变更（CRMS->同业）
					ApprovalChangeRq request = SocketClientServiceImpl
							.converyToJavaBean(xmlMessage, BOSFXII.class)
							.getApprovalChangeRq();
					ApprovalChangeRs response = socketClientService
							.iFBMApprovalChangeResponse(request);
					BOSFXII bosfxii = new BOSFXII();
					bosfxii.setApprovalChangeRs(response);
					retMsg = SocketClientServiceImpl.convertToXml(bosfxii);
					// LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("IFBM-批复信息变更retMsg:"
					// + retMsg);
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
							"IFBM-批复信息变更retMsg:" + XmlFormat.returnMsg(retMsg));

				} else if ("RepayNoticeRq".equals(intfaceName)) {
					// 提前还款通知（CRMS->同业系统）
					RepayNoticeRq request = SocketClientServiceImpl
							.converyToJavaBean(xmlMessage, BOSFXII.class)
							.getRepayNoticeRq();
					RepayNoticeRs response = socketClientService
							.iFBMRepayNoticeResponse(request);
					BOSFXII bosfxii = new BOSFXII();
					bosfxii.setRepayNoticeRs(response);
					retMsg = SocketClientServiceImpl.convertToXml(bosfxii);
					// LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("提前还款通知retMsg:"
					// + retMsg);
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
							"提前还款通知retMsg:" + XmlFormat.returnMsg(retMsg));

				} else {
					retcode = "000005";
					retMsg = "不存在此服务接口";
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
							"retcode:" + retcode + "  retMsg:  " + retMsg);
					retMsg = SocketMessageInterceptor.returnMessage(xmlMessage,
							retcode, retMsg);

				}

			}

			out.write((SocketClientServer.leftPadString(
					retMsg.getBytes().length, 9) + retMsg).getBytes("UTF-8"));
			out.flush();
			/*
			 * System.out.println("********retMsg******");
			 * System.out.println(retMsg); System.out.println(retMsg.length());
			 * System.out.println("********retMsg*****");
			 */
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e1) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭文件流出错：" + e1.getMessage());
			}
			try {
				if (null != connection) {
					connection.close();
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("A closed:" + connection.isClosed());
					connection = null;
				}
			} catch (IOException e) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭Socket通讯出错：" + e.getMessage());
			}
		} catch (Exception e) {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e1) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭文件流出错：" + e1.getMessage());
			}
			try {
				if (null != connection) {
					connection.close();
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("B closed:" + connection.isClosed());
					connection = null;
				}
			} catch (IOException e1) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭Socket通讯出错：" + e1.getMessage());
			}
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
					"连接Socket通讯：" + e);
			try {
				retcode = "999999";
				retMsg = "连接Socket通讯:" + e.getMessage();
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
						"retcode:" + retcode + "  retMsg:  " + retMsg);
				retMsg = SocketMessageInterceptor.returnMessage(xmlMessage,
						retcode, retMsg);
				out.write((SocketClientServer.leftPadString(
						retMsg.getBytes().length, 9) + retMsg)
						.getBytes("UTF-8"));// "UTF-8"
				out.flush();
			} catch (Exception ex) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"返回报文信息出错：" + ex.getMessage());
			}
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e1) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭文件流出错：" + e1.getMessage());
			}
			try {
				if (null != connection) {
					connection.close();
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("C closed:" + connection.isClosed());
					connection = null;
				}
			} catch (IOException e) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).error(
						"关闭Socket通讯出错：" + e.getMessage());
			}
		}
	}

	@Override
    public void run() {
		while (started) {
			Socket connection = null;
			try {
				connection = socketQueue.take();
			} catch (InterruptedException e) {
			}
			if (connection != null) {
				handleConnection(connection);
			}
		}
	}

	public void shutdown() {
		started = false;
	}


	public synchronized boolean IsNumber(String str) {
		try {
			Double.valueOf(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @return the soTimeout
	 */
	public int getSoTimeout() {
		return soTimeout;
	}

	/**
	 * @param soTimeout
	 *            the soTimeout to set
	 */
	public void setSoTimeout(int soTimeout) {
		this.soTimeout = soTimeout;
	}

}
