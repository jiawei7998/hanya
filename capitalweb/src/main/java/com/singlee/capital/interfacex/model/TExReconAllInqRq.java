package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExReconAllInqRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExReconAllInqRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="TradeDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FBID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TrnBranchNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExReconAllInqRq", propOrder = { "commonRqHdr", "tradeDt",
		"channelId", "fbid", "txnType", "transId", "trnBranchNo", "mode" })
public class TExReconAllInqRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "TradeDt", required = true)
	protected String tradeDt;
	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "FBID", required = true)
	protected String fbid;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "TransId", required = true)
	protected String transId;
	@XmlElement(name = "TrnBranchNo", required = true)
	protected String trnBranchNo;
	@XmlElement(name = "Mode", required = true)
	protected String mode;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the tradeDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTradeDt() {
		return tradeDt;
	}

	/**
	 * Sets the value of the tradeDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTradeDt(String value) {
		this.tradeDt = value;
	}

	/**
	 * Gets the value of the channelId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the value of the channelId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelId(String value) {
		this.channelId = value;
	}

	/**
	 * Gets the value of the fbid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFBID() {
		return fbid;
	}

	/**
	 * Sets the value of the fbid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFBID(String value) {
		this.fbid = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the transId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransId() {
		return transId;
	}

	/**
	 * Sets the value of the transId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransId(String value) {
		this.transId = value;
	}

	/**
	 * Gets the value of the trnBranchNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTrnBranchNo() {
		return trnBranchNo;
	}

	/**
	 * Sets the value of the trnBranchNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTrnBranchNo(String value) {
		this.trnBranchNo = value;
	}

	/**
	 * Gets the value of the mode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Sets the value of the mode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMode(String value) {
		this.mode = value;
	}

}
