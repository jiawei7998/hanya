package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CloudProSynchTransRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CloudProSynchTransRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgTypId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DepCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardCrDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnddDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BegDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DateEnd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Introducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MntnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DsCompanyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrgCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalEntTyp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalEntNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Relflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustSaleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Businoticeflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplOrg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClcRegionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RiskLev" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RelCusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Statuss" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkStateCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GrpRelCutCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DetailId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloudProSynchTransRs", propOrder = { "commonRsHdr",
		"productId", "msgTypId", "prdName", "busType", "sector", "depCode",
		"cardCrDt", "enddDate", "begDate", "dateEnd", "introducer",
		"applyName", "mntnType", "dsCompanyname", "orgCode", "legalEntTyp",
		"legalEntNo", "relflg", "custSaleCode", "relName", "businoticeflg",
		"applOrg", "clcRegionName", "riskLev", "relCusName", "billStatus",
		"statuss", "workStateCode", "grpRelCutCode", "detailId", "channelType" })
public class CloudProSynchTransRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "ProductId", required = true)
	protected String productId;
	@XmlElement(name = "MsgTypId", required = true)
	protected String msgTypId;
	@XmlElement(name = "PrdName", required = true)
	protected String prdName;
	@XmlElement(name = "BusType", required = true)
	protected String busType;
	@XmlElement(name = "Sector", required = true)
	protected String sector;
	@XmlElement(name = "DepCode", required = true)
	protected String depCode;
	@XmlElement(name = "CardCrDt", required = true)
	protected String cardCrDt;
	@XmlElement(name = "EnddDate", required = true)
	protected String enddDate;
	@XmlElement(name = "BegDate", required = true)
	protected String begDate;
	@XmlElement(name = "DateEnd", required = true)
	protected String dateEnd;
	@XmlElement(name = "Introducer", required = true)
	protected String introducer;
	@XmlElement(name = "ApplyName", required = true)
	protected String applyName;
	@XmlElement(name = "MntnType", required = true)
	protected String mntnType;
	@XmlElement(name = "DsCompanyname", required = true)
	protected String dsCompanyname;
	@XmlElement(name = "OrgCode", required = true)
	protected String orgCode;
	@XmlElement(name = "LegalEntTyp", required = true)
	protected String legalEntTyp;
	@XmlElement(name = "LegalEntNo", required = true)
	protected String legalEntNo;
	@XmlElement(name = "Relflg", required = true)
	protected String relflg;
	@XmlElement(name = "CustSaleCode", required = true)
	protected String custSaleCode;
	@XmlElement(name = "RelName", required = true)
	protected String relName;
	@XmlElement(name = "Businoticeflg", required = true)
	protected String businoticeflg;
	@XmlElement(name = "ApplOrg", required = true)
	protected String applOrg;
	@XmlElement(name = "ClcRegionName", required = true)
	protected String clcRegionName;
	@XmlElement(name = "RiskLev", required = true)
	protected String riskLev;
	@XmlElement(name = "RelCusName", required = true)
	protected String relCusName;
	@XmlElement(name = "BillStatus", required = true)
	protected String billStatus;
	@XmlElement(name = "Statuss", required = true)
	protected String statuss;
	@XmlElement(name = "WorkStateCode", required = true)
	protected String workStateCode;
	@XmlElement(name = "GrpRelCutCode", required = true)
	protected String grpRelCutCode;
	@XmlElement(name = "DetailId", required = true)
	protected String detailId;
	@XmlElement(name = "ChannelType",required = true)
	protected String channelType;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the productId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * Sets the value of the productId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductId(String value) {
		this.productId = value;
	}

	/**
	 * Gets the value of the msgTypId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgTypId() {
		return msgTypId;
	}

	/**
	 * Sets the value of the msgTypId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgTypId(String value) {
		this.msgTypId = value;
	}

	/**
	 * Gets the value of the prdName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrdName() {
		return prdName;
	}

	/**
	 * Sets the value of the prdName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrdName(String value) {
		this.prdName = value;
	}

	/**
	 * Gets the value of the busType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusType() {
		return busType;
	}

	/**
	 * Sets the value of the busType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusType(String value) {
		this.busType = value;
	}

	/**
	 * Gets the value of the sector property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * Sets the value of the sector property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSector(String value) {
		this.sector = value;
	}

	/**
	 * Gets the value of the depCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDepCode() {
		return depCode;
	}

	/**
	 * Sets the value of the depCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDepCode(String value) {
		this.depCode = value;
	}

	/**
	 * Gets the value of the cardCrDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardCrDt() {
		return cardCrDt;
	}

	/**
	 * Sets the value of the cardCrDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardCrDt(String value) {
		this.cardCrDt = value;
	}

	/**
	 * Gets the value of the enddDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnddDate() {
		return enddDate;
	}

	/**
	 * Sets the value of the enddDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnddDate(String value) {
		this.enddDate = value;
	}

	/**
	 * Gets the value of the begDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBegDate() {
		return begDate;
	}

	/**
	 * Sets the value of the begDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBegDate(String value) {
		this.begDate = value;
	}

	/**
	 * Gets the value of the dateEnd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateEnd() {
		return dateEnd;
	}

	/**
	 * Sets the value of the dateEnd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDateEnd(String value) {
		this.dateEnd = value;
	}

	/**
	 * Gets the value of the introducer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntroducer() {
		return introducer;
	}

	/**
	 * Sets the value of the introducer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntroducer(String value) {
		this.introducer = value;
	}

	/**
	 * Gets the value of the applyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getApplyName() {
		return applyName;
	}

	/**
	 * Sets the value of the applyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setApplyName(String value) {
		this.applyName = value;
	}

	/**
	 * Gets the value of the mntnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMntnType() {
		return mntnType;
	}

	/**
	 * Sets the value of the mntnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMntnType(String value) {
		this.mntnType = value;
	}

	/**
	 * Gets the value of the dsCompanyname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDsCompanyname() {
		return dsCompanyname;
	}

	/**
	 * Sets the value of the dsCompanyname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDsCompanyname(String value) {
		this.dsCompanyname = value;
	}

	/**
	 * Gets the value of the orgCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgCode() {
		return orgCode;
	}

	/**
	 * Sets the value of the orgCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgCode(String value) {
		this.orgCode = value;
	}

	/**
	 * Gets the value of the legalEntTyp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalEntTyp() {
		return legalEntTyp;
	}

	/**
	 * Sets the value of the legalEntTyp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalEntTyp(String value) {
		this.legalEntTyp = value;
	}

	/**
	 * Gets the value of the legalEntNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalEntNo() {
		return legalEntNo;
	}

	/**
	 * Sets the value of the legalEntNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalEntNo(String value) {
		this.legalEntNo = value;
	}

	/**
	 * Gets the value of the relflg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelflg() {
		return relflg;
	}

	/**
	 * Sets the value of the relflg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelflg(String value) {
		this.relflg = value;
	}

	/**
	 * Gets the value of the custSaleCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustSaleCode() {
		return custSaleCode;
	}

	/**
	 * Sets the value of the custSaleCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustSaleCode(String value) {
		this.custSaleCode = value;
	}

	/**
	 * Gets the value of the relName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelName() {
		return relName;
	}

	/**
	 * Sets the value of the relName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelName(String value) {
		this.relName = value;
	}

	/**
	 * Gets the value of the businoticeflg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusinoticeflg() {
		return businoticeflg;
	}

	/**
	 * Sets the value of the businoticeflg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusinoticeflg(String value) {
		this.businoticeflg = value;
	}

	/**
	 * Gets the value of the applOrg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getApplOrg() {
		return applOrg;
	}

	/**
	 * Sets the value of the applOrg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setApplOrg(String value) {
		this.applOrg = value;
	}

	/**
	 * Gets the value of the clcRegionName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClcRegionName() {
		return clcRegionName;
	}

	/**
	 * Sets the value of the clcRegionName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClcRegionName(String value) {
		this.clcRegionName = value;
	}

	/**
	 * Gets the value of the riskLev property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRiskLev() {
		return riskLev;
	}

	/**
	 * Sets the value of the riskLev property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRiskLev(String value) {
		this.riskLev = value;
	}

	/**
	 * Gets the value of the relCusName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelCusName() {
		return relCusName;
	}

	/**
	 * Sets the value of the relCusName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelCusName(String value) {
		this.relCusName = value;
	}

	/**
	 * Gets the value of the billStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillStatus() {
		return billStatus;
	}

	/**
	 * Sets the value of the billStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillStatus(String value) {
		this.billStatus = value;
	}

	/**
	 * Gets the value of the statuss property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatuss() {
		return statuss;
	}

	/**
	 * Sets the value of the statuss property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatuss(String value) {
		this.statuss = value;
	}

	/**
	 * Gets the value of the workStateCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkStateCode() {
		return workStateCode;
	}

	/**
	 * Sets the value of the workStateCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWorkStateCode(String value) {
		this.workStateCode = value;
	}

	/**
	 * Gets the value of the grpRelCutCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGrpRelCutCode() {
		return grpRelCutCode;
	}

	/**
	 * Sets the value of the grpRelCutCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGrpRelCutCode(String value) {
		this.grpRelCutCode = value;
	}

	/**
	 * Gets the value of the detailId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDetailId() {
		return detailId;
	}

	/**
	 * Sets the value of the detailId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDetailId(String value) {
		this.detailId = value;
	}

	/**
	 * Gets the value of the channelType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelType() {
		return channelType;
	}

	/**
	 * Sets the value of the channelType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelType(String value) {
		this.channelType = value;
	}

}
