package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class GtpGvatXxOutputQuartzJob implements CronRunnable {

	/**
	 *  1、GTP配置VAT；
		2、文件命名格式：IFBM_OUTPUT_DATA_YYYYMMDD_HHMMSS.csv
	                通配符：IFBM_OUTPUT_DATA_*.csv
	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("=======================开始处理========================");
		String classList= this.getClass().getName();
		System.out.println("classList:======"+classList);
		Map<String,String> map =new HashMap<String,String>();
		map.put("classList", classList);
		String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
		String jobName = jobExcuService.getJobByclassList(map).get(0).getJobName();

		JobExcuLog jobExcuLog = new JobExcuLog();
		jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
		jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
		jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		jobExcuService.insertJobExcuLog(jobExcuLog);
		LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");
		
		RetMsg<Object>  retmsg = gtpFileService.GtpGvatOutputDataWrite();
		System.out.println(retmsg.getCode());
		
		if("0000".equals(retmsg.getCode())){
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
			jobExcuService.updateJobExcuLog(jobExcuLog);
			System.out.println("=======================处理结束========================");
		}
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
