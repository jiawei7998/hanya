package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.mapper.FBZMapper;
import com.singlee.capital.interfacex.qdb.pojo.FBZ001;
import com.singlee.capital.interfacex.qdb.pojo.FBZ002;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.pojo.FBZ004;
import com.singlee.capital.interfacex.qdb.service.FBZService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FBZServiceImpl implements FBZService {
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	@Autowired
	private FBZMapper fbzMapper;
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	
	public FBZMapper getFbzMapper() {
		return fbzMapper;
	}

	public void setFbzMapper(FBZMapper fbzMapper) {
		this.fbzMapper = fbzMapper;
	}
	@Override
	public String parseXml(FBZ001 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		svc.put("bizDate",fbz.getBizDate()==null?"":fbz.getBizDate());
		svc.put("refNo",fbz.getRefNo()==null?"":fbz.getRefNo());
		svc.put("seqNo", fbz.getSeqNo()==null?"":fbz.getSeqNo());
		svc.put("tmStamp", getDate());
		hashmap.put("appHdr", svc);
		body.put("mfCustomerId", fbz.getMfCustomerId()==null?"":fbz.getMfCustomerId());
		body.put("customerType", fbz.getCustomerType()==null?"":fbz.getCustomerType());
		body.put("certType", fbz.getCertType()==null?"":fbz.getCertType());
		body.put("certId", fbz.getCertId()==null?"":fbz.getCertId());
		hashmap.put("appBody", body);
		String xml= XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0001.xml", hashmap);
		log.info(xml);
		return xml;
	}

	@Override
	public FBZ001 sendReq(FBZ001 fbz)throws Exception {
		String esbUrl=PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ001.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	System.out.println(xmlDocAccept.asXML());
		        	fbz=(FBZ001) XmlUtilFactory.getBeanByXml(FBZ001.class, xmlDocAccept.asXML());
	        		if(hashMap.get("Bytes")!=null){
			    	    return fbz;
			        }
	        	}
	        }			
		} catch (Exception e) {
			log.error("FBZ001接口："+e);
			e.printStackTrace();
			return null;
		}
		return null;
	}

	@Override
	public boolean insertFbz(FBZ003 fbz) {
		FBZ003 fbz003=fbzMapper.getFbzByOrder(fbz.getDealNo());
		int i=0;
		if(fbz003!=null){
			fbzMapper.deleteFbz(fbz);
			//i=fbzMapper.updateFbz(fbz);
		}
		i=fbzMapper.insertFbz(fbz);
		return i>0?true:false;
	}

	@Override
	public String parseXml(FBZ002 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		svc.put("bizDate",fbz!=null?fbz.getBizDate():"");
		svc.put("refNo",fbz!=null?fbz.getRefNo():"");
		svc.put("seqNo", fbz!=null?fbz.getSeqNo():"");
		svc.put("tmStamp", getDate());
		hashmap.put("appHdr", svc);
		body.put("mfCustomerId", fbz != null?fbz.getMfCustomerId():"");
		body.put("customerType", fbz != null?fbz.getCustomerType():"");
		body.put("certType", fbz!=null?fbz.getCertType():"");
		body.put("certId", fbz!=null?fbz.getCertId():"");
		hashmap.put("appBody", body);
		return XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0002.xml", hashmap);
	}

	@Override
	public String parseXml(FBZ003 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",fbz.getBizDate());
			svc.put("refNo",fbz.getRefNo());
			svc.put("seqNo", fbz.getSeqNo());
			svc.put("tmStamp", getDate());
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz==null?fbz.getMfCustomerId():"");
			body.put("customerType", fbz==null?fbz.getCustomerType():"");
			body.put("certType", fbz.getCertType());
			body.put("certId", fbz.getCertId());
			body.put("operTyp", fbz.getOperTyp());
			body.put("frzcurrency", fbz.getFrzcurrency());
			body.put("frzSum", fbz.getFrzSum());
			body.put("lmtID", fbz.getLmtID());
			body.put("maturity", fbz.getMaturity());
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0003.xml", hashmap);
	}

	@Override
	public String parseXml(FBZ004 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",fbz.getBizDate());
			svc.put("refNo",fbz.getRefNo());
			svc.put("seqNo", fbz.getSeqNo());
			svc.put("tmStamp", fbz.getTmStamp());
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz==null?fbz.getMfCustomerId():"");
			body.put("customerType", fbz==null?fbz.getCustomerType():"");
			body.put("certType", fbz.getCertType());
			body.put("certId", fbz.getCertId());
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0004.xml", hashmap);
	}


	@Override
	public boolean sendReq(FBZ002 fbz) throws Exception {
		String esbUrl=PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ002.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		//saxReader.setEncoding("UTF-8");
		String msg="";
		try {
			msg=parseXml(fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ002返回报文"+xmlDocAccept.asXML());
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return true;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return false;
		}
		return false;
	}

	@Override
	public FBZ003 sendReq(FBZ003 fbz){
		String esbUrl = "";
		FBZ003 f=null;
		try {
			esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ003.URL");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ003返回报文"+xmlDocAccept.asXML());
		        	f=(FBZ003) XmlUtilFactory.getBeanByXml(FBZ003.class, xmlDocAccept.asXML());
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return f;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz003接口："+e);
			return f;
		}
		return f;
	}

	@Override
	public List<Object> sendReq(FBZ004 fbz) throws Exception {
		String esbUrl=PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ004.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		List<Object> list=null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ004返回报文"+xmlDocAccept.asXML());
		        	list= XmlUtilFactory.getBeansByXml(FBZ002.class, xmlDocAccept.asXML(),"circularField");
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return list;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz004接口："+e);
			return list;
		}
		return list;
	}

	@Override
	public FBZ003 getFbzByOrder(TtTrdOrder order) {
		return fbzMapper.getFbzByOrder(order.getOrder_id());
	}

	@Override
	public boolean updateFbz(FBZ003 fbz) {
		int i= fbzMapper.updateFbz(fbz);
		return i>0?true:false;
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	@Override
	public FBZ003 getFbzByDealNo(String string) {
		return fbzMapper.getFbzByOrder(string);
	}

	@Override
	public void deleteFbz(FBZ003 f) {
		fbzMapper.deleteFbz(f);	
	}

	@Override
	public List<FBZ003> getFbzBySeriaNo(Map<String, Object> map) {
		return fbzMapper.getFbzBySeriaNo(map);
	}

	@Override
	public void updateFlag(FBZ003 fbz) {
		fbzMapper.updateFlag(fbz);
	}

	@Override
	public boolean checkCreditCust(Map<String, Object> map) {
		String cNo = ParameterUtil.getString(map, "cNo", "");
		String creditCust = ParameterUtil.getString(map, "creditCust", "");
		if(cNo.equals(creditCust)){
			return true;
			
		}else{
			String creditType = ParameterUtil.getString(map, "creditType", "");
			map.put("cNo", creditCust);
			if("company".equals(creditType)){//占用企业额度
				return fbzMapper.countBaseAsset(map) > 0;
				
			}else{//占用同业额度
				return fbzMapper.countPassWay(map) > 0;
				
			}
		}
		
	}
}
