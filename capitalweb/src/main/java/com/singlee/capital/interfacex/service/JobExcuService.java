package com.singlee.capital.interfacex.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.interfacex.model.DayendInfoRecord;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.model.JobExcuRecord;
import com.singlee.capital.interfacex.model.JobExcuTerm;
import com.singlee.capital.interfacex.model.NightJobStatus;

public interface JobExcuService {

	public void insertJobExcuLog(JobExcuLog jobExcuLog);
	
	public void updateJobExcuLog(JobExcuLog jobExcuLog);
	
	public int getJobExcuLog(JobExcuLog jobExcuLog);
	
	public List<TaJob> getJobByclassList(Map<String, String> params);
	
	public List<TaJob> getEnableJobList(Map<String, String> params);
	
	public List<JobExcuTerm> selectJobExcuTerm(Map<String, Object> map);

	public void insertJobExcuRecord(JobExcuRecord jobExcuRecord);
	
	public int getJobExcuRecord(Map<String, Object> map);
	
	public List<DayendInfoRecord> getDayendJobStatus(Map<String, String> params);
	
	public List<NightJobStatus> searchDayendJobStatus(Map<String, String> params);
	
}
