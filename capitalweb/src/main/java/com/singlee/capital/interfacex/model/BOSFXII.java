package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BOSFXII complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="BOSFXII">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TStBeaRepayAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStBeaRepayAaaRq"/>
 *         &lt;element name="TStBeaRepayAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStBeaRepayAaaRs"/>
 *         &lt;element name="TStFtNormRevRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStFtNormRevRq"/>
 *         &lt;element name="TStFtNormRevRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStFtNormRevRs"/>
 *         &lt;element name="TStFtRemitAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStFtRemitAaaRq"/>
 *         &lt;element name="TStFtRemitAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TStFtRemitAaaRs"/>
 *         &lt;element name="TDpAcStmtInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcStmtInqRq"/>
 *         &lt;element name="TDpAcStmtInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcStmtInqRs"/>
 *         &lt;element name="TExReconAllInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExReconAllInqRq"/>
 *         &lt;element name="TExReconAllInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExReconAllInqRs"/>
 *         &lt;element name="TDpAcctTypeInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcctTypeInqRq"/>
 *         &lt;element name="TDpAcctTypeInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcctTypeInqRs"/>
 *         &lt;element name="TDpCorpacctAllAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpCorpacctAllAaaRq"/>
 *         &lt;element name="TDpCorpacctAllAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpCorpacctAllAaaRs"/>
 *         &lt;element name="TCoSignonMultAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoSignonMultAaaRq"/>
 *         &lt;element name="TCoSignonMultAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoSignonMultAaaRs"/>
 *         &lt;element name="TCoSignoffMultAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoSignoffMultAaaRq"/>
 *         &lt;element name="TCoSignoffMultAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoSignoffMultAaaRs"/>
 *         &lt;element name="TCoHolidayAllInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoHolidayAllInqRq"/>
 *         &lt;element name="TCoHolidayAllInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoHolidayAllInqRs"/>
 *         &lt;element name="TExCurrencyAllInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExCurrencyAllInqRq"/>
 *         &lt;element name="TExCurrencyAllInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExCurrencyAllInqRs"/>
 *         &lt;element name="C201OrgCusInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C201OrgCusInqRq"/>
 *         &lt;element name="C201OrgCusInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C201OrgCusInqRs"/>
 *         &lt;element name="TDpAcctNewtdaAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcctNewtdaAaaRq"/>
 *         &lt;element name="TDpAcctNewtdaAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcctNewtdaAaaRs"/>
 *         &lt;element name="TDpAcActModRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcActModRq"/>
 *         &lt;element name="TDpAcActModRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpAcActModRs"/>
 *         &lt;element name="C167CoCorpCusInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C167CoCorpCusInqRq"/>
 *         &lt;element name="C167CoCorpCusInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C167CoCorpCusInqRs"/>
 *         &lt;element name="TCustDetInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCustDetInqRq"/>
 *         &lt;element name="TCustDetInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCustDetInqRs"/>
 *         &lt;element name="TDpIntBnkAaaRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpIntBnkAaaRq"/>
 *         &lt;element name="TDpIntBnkAaaRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpIntBnkAaaRs"/>
 *         &lt;element name="TCoActIntInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoActIntInqRq"/>
 *         &lt;element name="TCoActIntInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoActIntInqRs"/>
 *         &lt;element name="TCoLoansIntInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoLoansIntInqRq"/>
 *         &lt;element name="TCoLoansIntInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCoLoansIntInqRs"/>
*          &lt;element name="TExLoanAllInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExLoanAllInqRq"/>
 *         &lt;element name="TExLoanAllInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExLoanAllInqRs"/>
 *         &lt;element name="TExLdRpmSchInqRq" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExLdRpmSchInqRq"/>
 *         &lt;element name="TExLdRpmSchInqRs" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExLdRpmSchInqRs"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOSFXII", propOrder = { "tStBeaRepayAaaRq",
		"tStBeaRepayAaaRs", "tStFtNormRevRq", "tStFtNormRevRs",
		"tStFtRemitAaaRq", "tStFtRemitAaaRs", "tDpAcStmtInqRq",
		"tDpAcStmtInqRs", "tExReconAllInqRq", "tExReconAllInqRs",
		"tDpAcctTypeInqRq", "tDpAcctTypeInqRs", "tDpCorpacctAllAaaRq",
		"tDpCorpacctAllAaaRs", "tCoSignonMultAaaRq", "tCoSignonMultAaaRs",
		"tCoSignoffMultAaaRq", "tCoSignoffMultAaaRs", "tCoHolidayAllInqRq",
		"tCoHolidayAllInqRs", "tExCurrencyAllInqRq", "tExCurrencyAllInqRs",
		"c201OrgCusInqRq", "c201OrgCusInqRs","tDpAcctNewtdaAaaRq",
		"tDpAcctNewtdaAaaRs", "tDpAcAciModRq", "tDpAcAciModRs",
		"uppsCdtTrfRq","uppsCdtTrfRs","uppSigBusiQueryRq", "uppSigBusiQueryRs", "uppsChkFileAplRq",
		"uppsChkFileAplRs", "cloudProApplyTransRq", "cloudProApplyTransRs",
		"cloudProSynchTransRq", "cloudProSynchTransRs", "cloudProModTransRq",
		"cloudProModTransRs","c167CoCorpCusInqRq",
		"c167CoCorpCusInqRs", "tCustDetInqRq", "tCustDetInqRs", "enterpriseInqRq", "enterpriseInqRs",
		"enterpriseApproveRq", "enterpriseApproveRs", "finCreditInqRq",
		"finCreditInqRs", "finCreditApproveRq", "finCreditApproveRs",
		"loanNoticeRq", "loanNoticeRs", "repayNoticeRq", "repayNoticeRs",
		"approvalInqRq", "approvalInqRs", "approvalChangeRq",
		"approvalChangeRs", "quotaSyncopateRq", "quotaSyncopateRs","tDpIntBnkAaaRq", "tDpIntBnkAaaRs",
		"tCoActIntInqRq", "tCoActIntInqRs", "tCoLoansIntInqRq",
		"tCoLoansIntInqRs","tExLoanAllInqRq","tExLoanAllInqRs","tExLdRpmSchInqRq","tExLdRpmSchInqRs"})

@XmlRootElement(name = "BOSFXII")
public class BOSFXII {

	@XmlElement(name = "TStBeaRepayAaaRq", required = true)
	protected TStBeaRepayAaaRq tStBeaRepayAaaRq;
	@XmlElement(name = "TStBeaRepayAaaRs", required = true)
	protected TStBeaRepayAaaRs tStBeaRepayAaaRs;
	@XmlElement(name = "TStFtNormRevRq", required = true)
	protected TStFtNormRevRq tStFtNormRevRq;
	@XmlElement(name = "TStFtNormRevRs", required = true)
	protected TStFtNormRevRs tStFtNormRevRs;
	@XmlElement(name = "TStFtRemitAaaRq", required = true)
	protected TStFtRemitAaaRq tStFtRemitAaaRq;
	@XmlElement(name = "TStFtRemitAaaRs", required = true)
	protected TStFtRemitAaaRs tStFtRemitAaaRs;
	@XmlElement(name = "TDpAcStmtInqRq", required = true)
	protected TDpAcStmtInqRq tDpAcStmtInqRq;
	@XmlElement(name = "TDpAcStmtInqRs", required = true)
	protected TDpAcStmtInqRs tDpAcStmtInqRs;
	@XmlElement(name = "TExReconAllInqRq", required = true)
	protected TExReconAllInqRq tExReconAllInqRq;
	@XmlElement(name = "TExReconAllInqRs", required = true)
	protected TExReconAllInqRs tExReconAllInqRs;
	@XmlElement(name = "TDpAcctTypeInqRq", required = true)
	protected TDpAcctTypeInqRq tDpAcctTypeInqRq;
	@XmlElement(name = "TDpAcctTypeInqRs", required = true)
	protected TDpAcctTypeInqRs tDpAcctTypeInqRs;
	@XmlElement(name = "TDpCorpacctAllAaaRq", required = true)
	protected TDpCorpacctAllAaaRq tDpCorpacctAllAaaRq;
	@XmlElement(name = "TDpCorpacctAllAaaRs", required = true)
	protected TDpCorpacctAllAaaRs tDpCorpacctAllAaaRs;
	@XmlElement(name = "TCoSignonMultAaaRq", required = true)
	protected TCoSignonMultAaaRq tCoSignonMultAaaRq;
	@XmlElement(name = "TCoSignonMultAaaRs", required = true)
	protected TCoSignonMultAaaRs tCoSignonMultAaaRs;
	@XmlElement(name = "TCoSignoffMultAaaRq", required = true)
	protected TCoSignoffMultAaaRq tCoSignoffMultAaaRq;
	@XmlElement(name = "TCoSignoffMultAaaRs", required = true)
	protected TCoSignoffMultAaaRs tCoSignoffMultAaaRs;
	@XmlElement(name = "TCoHolidayAllInqRq", required = true)
	protected TCoHolidayAllInqRq tCoHolidayAllInqRq;
	@XmlElement(name = "TCoHolidayAllInqRs", required = true)
	protected TCoHolidayAllInqRs tCoHolidayAllInqRs;
	@XmlElement(name = "TExCurrencyAllInqRq", required = true)
	protected TExCurrencyAllInqRq tExCurrencyAllInqRq;
	@XmlElement(name = "TExCurrencyAllInqRs", required = true)
	protected TExCurrencyAllInqRs tExCurrencyAllInqRs;
	@XmlElement(name = "C201OrgCusInqRq", required = true)
	protected C201OrgCusInqRq c201OrgCusInqRq;
	@XmlElement(name = "C201OrgCusInqRs", required = true)
	protected C201OrgCusInqRs c201OrgCusInqRs;
	@XmlElement(name = "TDpAcctNewtdaAaaRq", required = true)
	protected TDpAcctNewtdaAaaRq tDpAcctNewtdaAaaRq;
	@XmlElement(name = "TDpAcctNewtdaAaaRs", required = true)
	protected TDpAcctNewtdaAaaRs tDpAcctNewtdaAaaRs;
	@XmlElement(name = "TDpAcAciModRq", required = true)
	protected TDpAcAciModRq tDpAcAciModRq;
	@XmlElement(name = "TDpAcAciModRs", required = true)
	protected TDpAcAciModRs tDpAcAciModRs;

	@XmlElement(name = "UPPSCdtTrfRq", required = true)
	protected UPPSCdtTrfRq uppsCdtTrfRq;
	@XmlElement(name = "UPPSCdtTrfRs", required = true)
	protected UPPSCdtTrfRs uppsCdtTrfRs;
	@XmlElement(name = "UPPSigBusiQueryRq", required = true)
	protected UPPSigBusiQueryRq uppSigBusiQueryRq;
	@XmlElement(name = "UPPSigBusiQueryRs", required = true)
	protected UPPSigBusiQueryRs uppSigBusiQueryRs;
	@XmlElement(name = "UPPSChkFileAplRq", required = true)
	protected UPPSChkFileAplRq uppsChkFileAplRq;
	@XmlElement(name = "UPPSChkFileAplRs", required = true)
	protected UPPSChkFileAplRs uppsChkFileAplRs;

	@XmlElement(name = "CloudProApplyTransRq", required = true)
	protected CloudProApplyTransRq cloudProApplyTransRq;
	@XmlElement(name = "CloudProApplyTransRs", required = true)
	protected CloudProApplyTransRs cloudProApplyTransRs;
	@XmlElement(name = "CloudProSynchTransRq", required = true)
	protected CloudProSynchTransRq cloudProSynchTransRq;
	@XmlElement(name = "CloudProSynchTransRs", required = true)
	protected CloudProSynchTransRs cloudProSynchTransRs;
	@XmlElement(name = "CloudProModTransRq", required = true)
	protected CloudProModTransRq cloudProModTransRq;
	@XmlElement(name = "CloudProModTransRs", required = true)
	protected CloudProModTransRs cloudProModTransRs;
	
	@XmlElement(name = "C167CoCorpCusInqRq", required = true)
	protected C167CoCorpCusInqRq c167CoCorpCusInqRq;
	@XmlElement(name = "C167CoCorpCusInqRs", required = true)
	protected C167CoCorpCusInqRs c167CoCorpCusInqRs;
	@XmlElement(name = "TCustDetInqRq", required = true)
	protected TCustDetInqRq tCustDetInqRq;
	@XmlElement(name = "TCustDetInqRs", required = true)
	protected TCustDetInqRs tCustDetInqRs;

	
	@XmlElement(name = "EnterpriseInqRq", required = true)
	protected EnterpriseInqRq enterpriseInqRq;
	@XmlElement(name = "EnterpriseInqRs", required = true)
	protected EnterpriseInqRs enterpriseInqRs;
	@XmlElement(name = "EnterpriseApproveRq", required = true)
	protected EnterpriseApproveRq enterpriseApproveRq;
	@XmlElement(name = "EnterpriseApproveRs", required = true)
	protected EnterpriseApproveRs enterpriseApproveRs;
	@XmlElement(name = "FinCreditInqRq", required = true)
	protected FinCreditInqRq finCreditInqRq;
	@XmlElement(name = "FinCreditInqRs", required = true)
	protected FinCreditInqRs finCreditInqRs;
	@XmlElement(name = "FinCreditApproveRq", required = true)
	protected FinCreditApproveRq finCreditApproveRq;
	@XmlElement(name = "FinCreditApproveRs", required = true)
	protected FinCreditApproveRs finCreditApproveRs;
	@XmlElement(name = "LoanNoticeRq", required = true)
	protected LoanNoticeRq loanNoticeRq;
	@XmlElement(name = "LoanNoticeRs", required = true)
	protected LoanNoticeRs loanNoticeRs;
	@XmlElement(name = "RepayNoticeRq", required = true)
	protected RepayNoticeRq repayNoticeRq;
	@XmlElement(name = "RepayNoticeRs", required = true)
	protected RepayNoticeRs repayNoticeRs;
	@XmlElement(name = "ApprovalInqRq", required = true)
	protected ApprovalInqRq approvalInqRq;
	@XmlElement(name = "ApprovalInqRs", required = true)
	protected ApprovalInqRs approvalInqRs;
	@XmlElement(name = "ApprovalChangeRq", required = true)
	protected ApprovalChangeRq approvalChangeRq;
	@XmlElement(name = "ApprovalChangeRs", required = true)
	protected ApprovalChangeRs approvalChangeRs;
	@XmlElement(name = "QuotaSyncopateRq", required = true)
	protected QuotaSyncopateRq quotaSyncopateRq;
	@XmlElement(name = "QuotaSyncopateRs", required = true)
	protected QuotaSyncopateRs quotaSyncopateRs;
	///////////////////////////ADD 20170616/////////////////////////////////////
	@XmlElement(name = "TDpIntBnkAaaRq", required = true)
	protected TDpIntBnkAaaRq tDpIntBnkAaaRq;
	@XmlElement(name = "TDpIntBnkAaaRs", required = true)
	protected TDpIntBnkAaaRs tDpIntBnkAaaRs;
	@XmlElement(name = "TCoActIntInqRq", required = true)
	protected TCoActIntInqRq tCoActIntInqRq;
	@XmlElement(name = "TCoActIntInqRs", required = true)
	protected TCoActIntInqRs tCoActIntInqRs;
	@XmlElement(name = "TCoLoansIntInqRq", required = true)
	protected TCoLoansIntInqRq tCoLoansIntInqRq;
	@XmlElement(name = "TCoLoansIntInqRs", required = true)
	protected TCoLoansIntInqRs tCoLoansIntInqRs;
    
    @XmlElement(name = "TExLoanAllInqRq", required = true)
	protected TExLoanAllInqRq tExLoanAllInqRq;
	@XmlElement(name = "TExLoanAllInqRs", required = true)
	protected TExLoanAllInqRs tExLoanAllInqRs;
	@XmlElement(name = "TExLdRpmSchInqRq", required = true)
	protected TExLdRpmSchInqRq tExLdRpmSchInqRq;
	@XmlElement(name = "TExLdRpmSchInqRs", required = true)
	protected TExLdRpmSchInqRs tExLdRpmSchInqRs;
    
    	
	/**
	 * Gets the value of the tExLoanAllInqRq property.
	 * 
	 * @return possible object is {@link TExLoanAllInqRq }
	 * 
	 */
	public TExLoanAllInqRq getTExLoanAllInqRq() {
		return tExLoanAllInqRq;
	}

	/**
	 * Sets the value of the tExLoanAllInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TExLoanAllInqRq }
	 * 
	 */
	public void setTExLoanAllInqRq(TExLoanAllInqRq value) {
		this.tExLoanAllInqRq = value;
	}

	/**
	 * Gets the value of the tExLoanAllInqRs property.
	 * 
	 * @return possible object is {@link TExLoanAllInqRs }
	 * 
	 */
	public TExLoanAllInqRs getTExLoanAllInqRs() {
		return tExLoanAllInqRs;
	}

	/**
	 * Sets the value of the tExLoanAllInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TExLoanAllInqRs }
	 * 
	 */
	public void setTExLoanAllInqRs(TExLoanAllInqRs value) {
		this.tExLoanAllInqRs = value;
	}

	/**
	 * Gets the value of the tExLdRpmSchInqRq property.
	 * 
	 * @return possible object is {@link TExLdRpmSchInqRq }
	 * 
	 */
	public TExLdRpmSchInqRq getTExLdRpmSchInqRq() {
		return tExLdRpmSchInqRq;
	}

	/**
	 * Sets the value of the tExLdRpmSchInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TExLdRpmSchInqRq }
	 * 
	 */
	public void setTExLdRpmSchInqRq(TExLdRpmSchInqRq value) {
		this.tExLdRpmSchInqRq = value;
	}

	/**
	 * Gets the value of the tExLdRpmSchInqRs property.
	 * 
	 * @return possible object is {@link TExLdRpmSchInqRs }
	 * 
	 */
	public TExLdRpmSchInqRs getTExLdRpmSchInqRs() {
		return tExLdRpmSchInqRs;
	}

	/**
	 * Sets the value of the tExLdRpmSchInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TExLdRpmSchInqRs }
	 * 
	 */
	public void setTExLdRpmSchInqRs(TExLdRpmSchInqRs value) {
		this.tExLdRpmSchInqRs = value;
	}
	/**
	 * Gets the value of the tDpIntBnkAaaRq property.
	 * 
	 * @return possible object is {@link TDpIntBnkAaaRq }
	 * 
	 */
	public TDpIntBnkAaaRq getTDpIntBnkAaaRq() {
		return tDpIntBnkAaaRq;
	}

	/**
	 * Sets the value of the tDpIntBnkAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpIntBnkAaaRq }
	 * 
	 */
	public void setTDpIntBnkAaaRq(TDpIntBnkAaaRq value) {
		this.tDpIntBnkAaaRq = value;
	}

	/**
	 * Gets the value of the tDpIntBnkAaaRs property.
	 * 
	 * @return possible object is {@link TDpIntBnkAaaRs }
	 * 
	 */
	public TDpIntBnkAaaRs getTDpIntBnkAaaRs() {
		return tDpIntBnkAaaRs;
	}

	/**
	 * Sets the value of the tDpIntBnkAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpIntBnkAaaRs }
	 * 
	 */
	public void setTDpIntBnkAaaRs(TDpIntBnkAaaRs value) {
		this.tDpIntBnkAaaRs = value;
	}

	/**
	 * Gets the value of the tCoActIntInqRq property.
	 * 
	 * @return possible object is {@link TCoActIntInqRq }
	 * 
	 */
	public TCoActIntInqRq getTCoActIntInqRq() {
		return tCoActIntInqRq;
	}

	/**
	 * Sets the value of the tCoActIntInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoActIntInqRq }
	 * 
	 */
	public void setTCoActIntInqRq(TCoActIntInqRq value) {
		this.tCoActIntInqRq = value;
	}

	/**
	 * Gets the value of the tCoActIntInqRs property.
	 * 
	 * @return possible object is {@link TCoActIntInqRs }
	 * 
	 */
	public TCoActIntInqRs getTCoActIntInqRs() {
		return tCoActIntInqRs;
	}

	/**
	 * Sets the value of the tCoActIntInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoActIntInqRs }
	 * 
	 */
	public void setTCoActIntInqRs(TCoActIntInqRs value) {
		this.tCoActIntInqRs = value;
	}

	/**
	 * Gets the value of the tCoLoansIntInqRq property.
	 * 
	 * @return possible object is {@link TCoLoansIntInqRq }
	 * 
	 */
	public TCoLoansIntInqRq getTCoLoansIntInqRq() {
		return tCoLoansIntInqRq;
	}

	/**
	 * Sets the value of the tCoLoansIntInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoLoansIntInqRq }
	 * 
	 */
	public void setTCoLoansIntInqRq(TCoLoansIntInqRq value) {
		this.tCoLoansIntInqRq = value;
	}

	/**
	 * Gets the value of the tCoLoansIntInqRs property.
	 * 
	 * @return possible object is {@link TCoLoansIntInqRs }
	 * 
	 */
	public TCoLoansIntInqRs getTCoLoansIntInqRs() {
		return tCoLoansIntInqRs;
	}

	/**
	 * Sets the value of the tCoLoansIntInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoLoansIntInqRs }
	 * 
	 */
	public void setTCoLoansIntInqRs(TCoLoansIntInqRs value) {
		this.tCoLoansIntInqRs = value;
	}
	///////////////////////////////////////////////////////
	/**
	 * Gets the value of the enterpriseInqRq property.
	 * 
	 * @return possible object is {@link EnterpriseInqRq }
	 * 
	 */
	public EnterpriseInqRq getEnterpriseInqRq() {
		return enterpriseInqRq;
	}

	/**
	 * Sets the value of the enterpriseInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link EnterpriseInqRq }
	 * 
	 */
	public void setEnterpriseInqRq(EnterpriseInqRq value) {
		this.enterpriseInqRq = value;
	}

	/**
	 * Gets the value of the enterpriseInqRs property.
	 * 
	 * @return possible object is {@link EnterpriseInqRs }
	 * 
	 */
	public EnterpriseInqRs getEnterpriseInqRs() {
		return enterpriseInqRs;
	}

	/**
	 * Sets the value of the enterpriseInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link EnterpriseInqRs }
	 * 
	 */
	public void setEnterpriseInqRs(EnterpriseInqRs value) {
		this.enterpriseInqRs = value;
	}

	/**
	 * Gets the value of the enterpriseApproveRq property.
	 * 
	 * @return possible object is {@link EnterpriseApproveRq }
	 * 
	 */
	public EnterpriseApproveRq getEnterpriseApproveRq() {
		return enterpriseApproveRq;
	}

	/**
	 * Sets the value of the enterpriseApproveRq property.
	 * 
	 * @param value
	 *            allowed object is {@link EnterpriseApproveRq }
	 * 
	 */
	public void setEnterpriseApproveRq(EnterpriseApproveRq value) {
		this.enterpriseApproveRq = value;
	}

	/**
	 * Gets the value of the enterpriseApproveRs property.
	 * 
	 * @return possible object is {@link EnterpriseApproveRs }
	 * 
	 */
	public EnterpriseApproveRs getEnterpriseApproveRs() {
		return enterpriseApproveRs;
	}

	/**
	 * Sets the value of the enterpriseApproveRs property.
	 * 
	 * @param value
	 *            allowed object is {@link EnterpriseApproveRs }
	 * 
	 */
	public void setEnterpriseApproveRs(EnterpriseApproveRs value) {
		this.enterpriseApproveRs = value;
	}

	/**
	 * Gets the value of the finCreditInqRq property.
	 * 
	 * @return possible object is {@link FinCreditInqRq }
	 * 
	 */
	public FinCreditInqRq getFinCreditInqRq() {
		return finCreditInqRq;
	}

	/**
	 * Sets the value of the finCreditInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link FinCreditInqRq }
	 * 
	 */
	public void setFinCreditInqRq(FinCreditInqRq value) {
		this.finCreditInqRq = value;
	}

	/**
	 * Gets the value of the finCreditInqRs property.
	 * 
	 * @return possible object is {@link FinCreditInqRs }
	 * 
	 */
	public FinCreditInqRs getFinCreditInqRs() {
		return finCreditInqRs;
	}

	/**
	 * Sets the value of the finCreditInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link FinCreditInqRs }
	 * 
	 */
	public void setFinCreditInqRs(FinCreditInqRs value) {
		this.finCreditInqRs = value;
	}

	/**
	 * Gets the value of the finCreditApproveRq property.
	 * 
	 * @return possible object is {@link FinCreditApproveRq }
	 * 
	 */
	public FinCreditApproveRq getFinCreditApproveRq() {
		return finCreditApproveRq;
	}

	/**
	 * Sets the value of the finCreditApproveRq property.
	 * 
	 * @param value
	 *            allowed object is {@link FinCreditApproveRq }
	 * 
	 */
	public void setFinCreditApproveRq(FinCreditApproveRq value) {
		this.finCreditApproveRq = value;
	}

	/**
	 * Gets the value of the finCreditApproveRs property.
	 * 
	 * @return possible object is {@link FinCreditApproveRs }
	 * 
	 */
	public FinCreditApproveRs getFinCreditApproveRs() {
		return finCreditApproveRs;
	}

	/**
	 * Sets the value of the finCreditApproveRs property.
	 * 
	 * @param value
	 *            allowed object is {@link FinCreditApproveRs }
	 * 
	 */
	public void setFinCreditApproveRs(FinCreditApproveRs value) {
		this.finCreditApproveRs = value;
	}

	/**
	 * Gets the value of the loanNoticeRq property.
	 * 
	 * @return possible object is {@link LoanNoticeRq }
	 * 
	 */
	public LoanNoticeRq getLoanNoticeRq() {
		return loanNoticeRq;
	}

	/**
	 * Sets the value of the loanNoticeRq property.
	 * 
	 * @param value
	 *            allowed object is {@link LoanNoticeRq }
	 * 
	 */
	public void setLoanNoticeRq(LoanNoticeRq value) {
		this.loanNoticeRq = value;
	}

	/**
	 * Gets the value of the loanNoticeRs property.
	 * 
	 * @return possible object is {@link LoanNoticeRs }
	 * 
	 */
	public LoanNoticeRs getLoanNoticeRs() {
		return loanNoticeRs;
	}

	/**
	 * Sets the value of the loanNoticeRs property.
	 * 
	 * @param value
	 *            allowed object is {@link LoanNoticeRs }
	 * 
	 */
	public void setLoanNoticeRs(LoanNoticeRs value) {
		this.loanNoticeRs = value;
	}

	/**
	 * Gets the value of the repayNoticeRq property.
	 * 
	 * @return possible object is {@link RepayNoticeRq }
	 * 
	 */
	public RepayNoticeRq getRepayNoticeRq() {
		return repayNoticeRq;
	}

	/**
	 * Sets the value of the repayNoticeRq property.
	 * 
	 * @param value
	 *            allowed object is {@link RepayNoticeRq }
	 * 
	 */
	public void setRepayNoticeRq(RepayNoticeRq value) {
		this.repayNoticeRq = value;
	}

	/**
	 * Gets the value of the repayNoticeRs property.
	 * 
	 * @return possible object is {@link RepayNoticeRs }
	 * 
	 */
	public RepayNoticeRs getRepayNoticeRs() {
		return repayNoticeRs;
	}

	/**
	 * Sets the value of the repayNoticeRs property.
	 * 
	 * @param value
	 *            allowed object is {@link RepayNoticeRs }
	 * 
	 */
	public void setRepayNoticeRs(RepayNoticeRs value) {
		this.repayNoticeRs = value;
	}

	/**
	 * Gets the value of the approvalInqRq property.
	 * 
	 * @return possible object is {@link ApprovalInqRq }
	 * 
	 */
	public ApprovalInqRq getApprovalInqRq() {
		return approvalInqRq;
	}

	/**
	 * Sets the value of the approvalInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link ApprovalInqRq }
	 * 
	 */
	public void setApprovalInqRq(ApprovalInqRq value) {
		this.approvalInqRq = value;
	}

	/**
	 * Gets the value of the approvalInqRs property.
	 * 
	 * @return possible object is {@link ApprovalInqRs }
	 * 
	 */
	public ApprovalInqRs getApprovalInqRs() {
		return approvalInqRs;
	}

	/**
	 * Sets the value of the approvalInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link ApprovalInqRs }
	 * 
	 */
	public void setApprovalInqRs(ApprovalInqRs value) {
		this.approvalInqRs = value;
	}

	/**
	 * Gets the value of the approvalChangeRq property.
	 * 
	 * @return possible object is {@link ApprovalChangeRq }
	 * 
	 */
	public ApprovalChangeRq getApprovalChangeRq() {
		return approvalChangeRq;
	}

	/**
	 * Sets the value of the approvalChangeRq property.
	 * 
	 * @param value
	 *            allowed object is {@link ApprovalChangeRq }
	 * 
	 */
	public void setApprovalChangeRq(ApprovalChangeRq value) {
		this.approvalChangeRq = value;
	}

	/**
	 * Gets the value of the approvalChangeRs property.
	 * 
	 * @return possible object is {@link ApprovalChangeRs }
	 * 
	 */
	public ApprovalChangeRs getApprovalChangeRs() {
		return approvalChangeRs;
	}

	/**
	 * Sets the value of the approvalChangeRs property.
	 * 
	 * @param value
	 *            allowed object is {@link ApprovalChangeRs }
	 * 
	 */
	public void setApprovalChangeRs(ApprovalChangeRs value) {
		this.approvalChangeRs = value;
	}

	/**
	 * Gets the value of the quotaSyncopateRq property.
	 * 
	 * @return possible object is {@link QuotaSyncopateRq }
	 * 
	 */
	public QuotaSyncopateRq getQuotaSyncopateRq() {
		return quotaSyncopateRq;
	}

	/**
	 * Sets the value of the quotaSyncopateRq property.
	 * 
	 * @param value
	 *            allowed object is {@link QuotaSyncopateRq }
	 * 
	 */
	public void setQuotaSyncopateRq(QuotaSyncopateRq value) {
		this.quotaSyncopateRq = value;
	}

	/**
	 * Gets the value of the quotaSyncopateRs property.
	 * 
	 * @return possible object is {@link QuotaSyncopateRs }
	 * 
	 */
	public QuotaSyncopateRs getQuotaSyncopateRs() {
		return quotaSyncopateRs;
	}

	/**
	 * Sets the value of the quotaSyncopateRs property.
	 * 
	 * @param value
	 *            allowed object is {@link QuotaSyncopateRs }
	 * 
	 */
	public void setQuotaSyncopateRs(QuotaSyncopateRs value) {
		this.quotaSyncopateRs = value;
	}

	/**
	 * Gets the value of the c167CoCorpCusInqRq property.
	 * 
	 * @return possible object is {@link C167CoCorpCusInqRq }
	 * 
	 */
	public C167CoCorpCusInqRq getC167CoCorpCusInqRq() {
		return c167CoCorpCusInqRq;
	}

	/**
	 * Sets the value of the c167CoCorpCusInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link C167CoCorpCusInqRq }
	 * 
	 */
	public void setC167CoCorpCusInqRq(C167CoCorpCusInqRq value) {
		this.c167CoCorpCusInqRq = value;
	}

	/**
	 * Gets the value of the c167CoCorpCusInqRs property.
	 * 
	 * @return possible object is {@link C167CoCorpCusInqRs }
	 * 
	 */
	public C167CoCorpCusInqRs getC167CoCorpCusInqRs() {
		return c167CoCorpCusInqRs;
	}

	/**
	 * Sets the value of the c167CoCorpCusInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link C167CoCorpCusInqRs }
	 * 
	 */
	public void setC167CoCorpCusInqRs(C167CoCorpCusInqRs value) {
		this.c167CoCorpCusInqRs = value;
	}

	/**
	 * Gets the value of the tCustDetInqRq property.
	 * 
	 * @return possible object is {@link TCustDetInqRq }
	 * 
	 */
	public TCustDetInqRq getTCustDetInqRq() {
		return tCustDetInqRq;
	}

	/**
	 * Sets the value of the tCustDetInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCustDetInqRq }
	 * 
	 */
	public void setTCustDetInqRq(TCustDetInqRq value) {
		this.tCustDetInqRq = value;
	}

	/**
	 * Gets the value of the tCustDetInqRs property.
	 * 
	 * @return possible object is {@link TCustDetInqRs }
	 * 
	 */
	public TCustDetInqRs getTCustDetInqRs() {
		return tCustDetInqRs;
	}

	/**
	 * Sets the value of the tCustDetInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCustDetInqRs }
	 * 
	 */
	public void setTCustDetInqRs(TCustDetInqRs value) {
		this.tCustDetInqRs = value;
	}


	/**
	 * Gets the value of the cloudProApplyTransRq property.
	 * 
	 * @return possible object is {@link CloudProApplyTransRq }
	 * 
	 */
	public CloudProApplyTransRq getCloudProApplyTransRq() {
		return cloudProApplyTransRq;
	}

	/**
	 * Sets the value of the cloudProApplyTransRq property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProApplyTransRq }
	 * 
	 */
	public void setCloudProApplyTransRq(CloudProApplyTransRq value) {
		this.cloudProApplyTransRq = value;
	}

	/**
	 * Gets the value of the cloudProApplyTransRs property.
	 * 
	 * @return possible object is {@link CloudProApplyTransRs }
	 * 
	 */
	public CloudProApplyTransRs getCloudProApplyTransRs() {
		return cloudProApplyTransRs;
	}

	/**
	 * Sets the value of the cloudProApplyTransRs property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProApplyTransRs }
	 * 
	 */
	public void setCloudProApplyTransRs(CloudProApplyTransRs value) {
		this.cloudProApplyTransRs = value;
	}

	/**
	 * Gets the value of the cloudProSynchTransRq property.
	 * 
	 * @return possible object is {@link CloudProSynchTransRq }
	 * 
	 */
	public CloudProSynchTransRq getCloudProSynchTransRq() {
		return cloudProSynchTransRq;
	}

	/**
	 * Sets the value of the cloudProSynchTransRq property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProSynchTransRq }
	 * 
	 */
	public void setCloudProSynchTransRq(CloudProSynchTransRq value) {
		this.cloudProSynchTransRq = value;
	}

	/**
	 * Gets the value of the cloudProSynchTransRs property.
	 * 
	 * @return possible object is {@link CloudProSynchTransRs }
	 * 
	 */
	public CloudProSynchTransRs getCloudProSynchTransRs() {
		return cloudProSynchTransRs;
	}

	/**
	 * Sets the value of the cloudProSynchTransRs property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProSynchTransRs }
	 * 
	 */
	public void setCloudProSynchTransRs(CloudProSynchTransRs value) {
		this.cloudProSynchTransRs = value;
	}

	/**
	 * Gets the value of the cloudProModTransRq property.
	 * 
	 * @return possible object is {@link CloudProModTransRq }
	 * 
	 */
	public CloudProModTransRq getCloudProModTransRq() {
		return cloudProModTransRq;
	}

	/**
	 * Sets the value of the cloudProModTransRq property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProModTransRq }
	 * 
	 */
	public void setCloudProModTransRq(CloudProModTransRq value) {
		this.cloudProModTransRq = value;
	}

	/**
	 * Gets the value of the cloudProModTransRs property.
	 * 
	 * @return possible object is {@link CloudProModTransRs }
	 * 
	 */
	public CloudProModTransRs getCloudProModTransRs() {
		return cloudProModTransRs;
	}

	/**
	 * Sets the value of the cloudProModTransRs property.
	 * 
	 * @param value
	 *            allowed object is {@link CloudProModTransRs }
	 * 
	 */
	public void setCloudProModTransRs(CloudProModTransRs value) {
		this.cloudProModTransRs = value;
	}
	/**
	 * Gets the value of the uppsCdtTrfRq property.
	 * 
	 * @return possible object is {@link UPPSCdtTrfRq }
	 * 
	 */
	public UPPSCdtTrfRq getUPPSCdtTrfRq() {
		return uppsCdtTrfRq;
	}

	/**
	 * Sets the value of the uppsCdtTrfRq property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSCdtTrfRq }
	 * 
	 */
	public void setUPPSCdtTrfRq(UPPSCdtTrfRq value) {
		this.uppsCdtTrfRq = value;
	}

	/**
	 * Gets the value of the uppsCdtTrfRs property.
	 * 
	 * @return possible object is {@link UPPSCdtTrfRs }
	 * 
	 */
	public UPPSCdtTrfRs getUPPSCdtTrfRs() {
		return uppsCdtTrfRs;
	}

	/**
	 * Sets the value of the uppsCdtTrfRs property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSCdtTrfRs }
	 * 
	 */
	public void setUPPSCdtTrfRs(UPPSCdtTrfRs value) {
		this.uppsCdtTrfRs = value;
	}

	/**
	 * Gets the value of the uppSigBusiQueryRq property.
	 * 
	 * @return possible object is {@link UPPSigBusiQueryRq }
	 * 
	 */
	public UPPSigBusiQueryRq getUPPSigBusiQueryRq() {
		return uppSigBusiQueryRq;
	}

	/**
	 * Sets the value of the uppSigBusiQueryRq property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSigBusiQueryRq }
	 * 
	 */
	public void setUPPSigBusiQueryRq(UPPSigBusiQueryRq value) {
		this.uppSigBusiQueryRq = value;
	}

	/**
	 * Gets the value of the uppSigBusiQueryRs property.
	 * 
	 * @return possible object is {@link UPPSigBusiQueryRs }
	 * 
	 */
	public UPPSigBusiQueryRs getUPPSigBusiQueryRs() {
		return uppSigBusiQueryRs;
	}

	/**
	 * Sets the value of the uppSigBusiQueryRs property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSigBusiQueryRs }
	 * 
	 */
	public void setUPPSigBusiQueryRs(UPPSigBusiQueryRs value) {
		this.uppSigBusiQueryRs = value;
	}

	/**
	 * Gets the value of the uppsChkFileAplRq property.
	 * 
	 * @return possible object is {@link UPPSChkFileAplRq }
	 * 
	 */
	public UPPSChkFileAplRq getUPPSChkFileAplRq() {
		return uppsChkFileAplRq;
	}

	/**
	 * Sets the value of the uppsChkFileAplRq property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSChkFileAplRq }
	 * 
	 */
	public void setUPPSChkFileAplRq(UPPSChkFileAplRq value) {
		this.uppsChkFileAplRq = value;
	}

	/**
	 * Gets the value of the uppsChkFileAplRs property.
	 * 
	 * @return possible object is {@link UPPSChkFileAplRs }
	 * 
	 */
	public UPPSChkFileAplRs getUPPSChkFileAplRs() {
		return uppsChkFileAplRs;
	}

	/**
	 * Sets the value of the uppsChkFileAplRs property.
	 * 
	 * @param value
	 *            allowed object is {@link UPPSChkFileAplRs }
	 * 
	 */
	public void setUPPSChkFileAplRs(UPPSChkFileAplRs value) {
		this.uppsChkFileAplRs = value;
	}

	/**
	 * Gets the value of the tStBeaRepayAaaRq property.
	 * 
	 * @return possible object is {@link TStBeaRepayAaaRq }
	 * 
	 */
	public TStBeaRepayAaaRq getTStBeaRepayAaaRq() {
		return tStBeaRepayAaaRq;
	}

	/**
	 * Sets the value of the tStBeaRepayAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TStBeaRepayAaaRq }
	 * 
	 */
	public void setTStBeaRepayAaaRq(TStBeaRepayAaaRq value) {
		this.tStBeaRepayAaaRq = value;
	}

	/**
	 * Gets the value of the tStBeaRepayAaaRs property.
	 * 
	 * @return possible object is {@link TStBeaRepayAaaRs }
	 * 
	 */
	public TStBeaRepayAaaRs getTStBeaRepayAaaRs() {
		return tStBeaRepayAaaRs;
	}

	/**
	 * Sets the value of the tStBeaRepayAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TStBeaRepayAaaRs }
	 * 
	 */
	public void setTStBeaRepayAaaRs(TStBeaRepayAaaRs value) {
		this.tStBeaRepayAaaRs = value;
	}

	/**
	 * Gets the value of the tStFtNormRevRq property.
	 * 
	 * @return possible object is {@link TStFtNormRevRq }
	 * 
	 */
	public TStFtNormRevRq getTStFtNormRevRq() {
		return tStFtNormRevRq;
	}

	/**
	 * Sets the value of the tStFtNormRevRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TStFtNormRevRq }
	 * 
	 */
	public void setTStFtNormRevRq(TStFtNormRevRq value) {
		this.tStFtNormRevRq = value;
	}

	/**
	 * Gets the value of the tStFtNormRevRs property.
	 * 
	 * @return possible object is {@link TStFtNormRevRs }
	 * 
	 */
	public TStFtNormRevRs getTStFtNormRevRs() {
		return tStFtNormRevRs;
	}

	/**
	 * Sets the value of the tStFtNormRevRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TStFtNormRevRs }
	 * 
	 */
	public void setTStFtNormRevRs(TStFtNormRevRs value) {
		this.tStFtNormRevRs = value;
	}

	/**
	 * Gets the value of the tStFtRemitAaaRq property.
	 * 
	 * @return possible object is {@link TStFtRemitAaaRq }
	 * 
	 */
	public TStFtRemitAaaRq getTStFtRemitAaaRq() {
		return tStFtRemitAaaRq;
	}

	/**
	 * Sets the value of the tStFtRemitAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TStFtRemitAaaRq }
	 * 
	 */
	public void setTStFtRemitAaaRq(TStFtRemitAaaRq value) {
		this.tStFtRemitAaaRq = value;
	}

	/**
	 * Gets the value of the tStFtRemitAaaRs property.
	 * 
	 * @return possible object is {@link TStFtRemitAaaRs }
	 * 
	 */
	public TStFtRemitAaaRs getTStFtRemitAaaRs() {
		return tStFtRemitAaaRs;
	}

	/**
	 * Sets the value of the tStFtRemitAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TStFtRemitAaaRs }
	 * 
	 */
	public void setTStFtRemitAaaRs(TStFtRemitAaaRs value) {
		this.tStFtRemitAaaRs = value;
	}

	/**
	 * Gets the value of the tDpAcStmtInqRq property.
	 * 
	 * @return possible object is {@link TDpAcStmtInqRq }
	 * 
	 */
	public TDpAcStmtInqRq getTDpAcStmtInqRq() {
		return tDpAcStmtInqRq;
	}

	/**
	 * Sets the value of the tDpAcStmtInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcStmtInqRq }
	 * 
	 */
	public void setTDpAcStmtInqRq(TDpAcStmtInqRq value) {
		this.tDpAcStmtInqRq = value;
	}

	/**
	 * Gets the value of the tDpAcStmtInqRs property.
	 * 
	 * @return possible object is {@link TDpAcStmtInqRs }
	 * 
	 */
	public TDpAcStmtInqRs getTDpAcStmtInqRs() {
		return tDpAcStmtInqRs;
	}

	/**
	 * Sets the value of the tDpAcStmtInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcStmtInqRs }
	 * 
	 */
	public void setTDpAcStmtInqRs(TDpAcStmtInqRs value) {
		this.tDpAcStmtInqRs = value;
	}

	/**
	 * Gets the value of the tExReconAllInqRq property.
	 * 
	 * @return possible object is {@link TExReconAllInqRq }
	 * 
	 */
	public TExReconAllInqRq getTExReconAllInqRq() {
		return tExReconAllInqRq;
	}

	/**
	 * Sets the value of the tExReconAllInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TExReconAllInqRq }
	 * 
	 */
	public void setTExReconAllInqRq(TExReconAllInqRq value) {
		this.tExReconAllInqRq = value;
	}

	/**
	 * Gets the value of the tExReconAllInqRs property.
	 * 
	 * @return possible object is {@link TExReconAllInqRs }
	 * 
	 */
	public TExReconAllInqRs getTExReconAllInqRs() {
		return tExReconAllInqRs;
	}

	/**
	 * Sets the value of the tExReconAllInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TExReconAllInqRs }
	 * 
	 */
	public void setTExReconAllInqRs(TExReconAllInqRs value) {
		this.tExReconAllInqRs = value;
	}

	/**
	 * Gets the value of the tDpAcctTypeInqRq property.
	 * 
	 * @return possible object is {@link TDpAcctTypeInqRq }
	 * 
	 */
	public TDpAcctTypeInqRq getTDpAcctTypeInqRq() {
		return tDpAcctTypeInqRq;
	}

	/**
	 * Sets the value of the tDpAcctTypeInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcctTypeInqRq }
	 * 
	 */
	public void setTDpAcctTypeInqRq(TDpAcctTypeInqRq value) {
		this.tDpAcctTypeInqRq = value;
	}

	/**
	 * Gets the value of the tDpAcctTypeInqRs property.
	 * 
	 * @return possible object is {@link TDpAcctTypeInqRs }
	 * 
	 */
	public TDpAcctTypeInqRs getTDpAcctTypeInqRs() {
		return tDpAcctTypeInqRs;
	}

	/**
	 * Sets the value of the tDpAcctTypeInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcctTypeInqRs }
	 * 
	 */
	public void setTDpAcctTypeInqRs(TDpAcctTypeInqRs value) {
		this.tDpAcctTypeInqRs = value;
	}

	/**
	 * Gets the value of the tDpCorpacctAllAaaRq property.
	 * 
	 * @return possible object is {@link TDpCorpacctAllAaaRq }
	 * 
	 */
	public TDpCorpacctAllAaaRq getTDpCorpacctAllAaaRq() {
		return tDpCorpacctAllAaaRq;
	}

	/**
	 * Sets the value of the tDpCorpacctAllAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpCorpacctAllAaaRq }
	 * 
	 */
	public void setTDpCorpacctAllAaaRq(TDpCorpacctAllAaaRq value) {
		this.tDpCorpacctAllAaaRq = value;
	}

	/**
	 * Gets the value of the tDpCorpacctAllAaaRs property.
	 * 
	 * @return possible object is {@link TDpCorpacctAllAaaRs }
	 * 
	 */
	public TDpCorpacctAllAaaRs getTDpCorpacctAllAaaRs() {
		return tDpCorpacctAllAaaRs;
	}

	/**
	 * Sets the value of the tDpCorpacctAllAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpCorpacctAllAaaRs }
	 * 
	 */
	public void setTDpCorpacctAllAaaRs(TDpCorpacctAllAaaRs value) {
		this.tDpCorpacctAllAaaRs = value;
	}

	/**
	 * Gets the value of the tCoSignonMultAaaRq property.
	 * 
	 * @return possible object is {@link TCoSignonMultAaaRq }
	 * 
	 */
	public TCoSignonMultAaaRq getTCoSignonMultAaaRq() {
		return tCoSignonMultAaaRq;
	}

	/**
	 * Sets the value of the tCoSignonMultAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoSignonMultAaaRq }
	 * 
	 */
	public void setTCoSignonMultAaaRq(TCoSignonMultAaaRq value) {
		this.tCoSignonMultAaaRq = value;
	}

	/**
	 * Gets the value of the tCoSignonMultAaaRs property.
	 * 
	 * @return possible object is {@link TCoSignonMultAaaRs }
	 * 
	 */
	public TCoSignonMultAaaRs getTCoSignonMultAaaRs() {
		return tCoSignonMultAaaRs;
	}

	/**
	 * Sets the value of the tCoSignonMultAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoSignonMultAaaRs }
	 * 
	 */
	public void setTCoSignonMultAaaRs(TCoSignonMultAaaRs value) {
		this.tCoSignonMultAaaRs = value;
	}

	/**
	 * Gets the value of the tCoSignoffMultAaaRq property.
	 * 
	 * @return possible object is {@link TCoSignoffMultAaaRq }
	 * 
	 */
	public TCoSignoffMultAaaRq getTCoSignoffMultAaaRq() {
		return tCoSignoffMultAaaRq;
	}

	/**
	 * Sets the value of the tCoSignoffMultAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoSignoffMultAaaRq }
	 * 
	 */
	public void setTCoSignoffMultAaaRq(TCoSignoffMultAaaRq value) {
		this.tCoSignoffMultAaaRq = value;
	}

	/**
	 * Gets the value of the tCoSignoffMultAaaRs property.
	 * 
	 * @return possible object is {@link TCoSignoffMultAaaRs }
	 * 
	 */
	public TCoSignoffMultAaaRs getTCoSignoffMultAaaRs() {
		return tCoSignoffMultAaaRs;
	}

	/**
	 * Sets the value of the tCoSignoffMultAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoSignoffMultAaaRs }
	 * 
	 */
	public void setTCoSignoffMultAaaRs(TCoSignoffMultAaaRs value) {
		this.tCoSignoffMultAaaRs = value;
	}

	/**
	 * Gets the value of the tCoHolidayAllInqRq property.
	 * 
	 * @return possible object is {@link TCoHolidayAllInqRq }
	 * 
	 */
	public TCoHolidayAllInqRq getTCoHolidayAllInqRq() {
		return tCoHolidayAllInqRq;
	}

	/**
	 * Sets the value of the tCoHolidayAllInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoHolidayAllInqRq }
	 * 
	 */
	public void setTCoHolidayAllInqRq(TCoHolidayAllInqRq value) {
		this.tCoHolidayAllInqRq = value;
	}

	/**
	 * Gets the value of the tCoHolidayAllInqRs property.
	 * 
	 * @return possible object is {@link TCoHolidayAllInqRs }
	 * 
	 */
	public TCoHolidayAllInqRs getTCoHolidayAllInqRs() {
		return tCoHolidayAllInqRs;
	}

	/**
	 * Sets the value of the tCoHolidayAllInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TCoHolidayAllInqRs }
	 * 
	 */
	public void setTCoHolidayAllInqRs(TCoHolidayAllInqRs value) {
		this.tCoHolidayAllInqRs = value;
	}

	/**
	 * Gets the value of the tExCurrencyAllInqRq property.
	 * 
	 * @return possible object is {@link TExCurrencyAllInqRq }
	 * 
	 */
	public TExCurrencyAllInqRq getTExCurrencyAllInqRq() {
		return tExCurrencyAllInqRq;
	}

	/**
	 * Sets the value of the tExCurrencyAllInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TExCurrencyAllInqRq }
	 * 
	 */
	public void setTExCurrencyAllInqRq(TExCurrencyAllInqRq value) {
		this.tExCurrencyAllInqRq = value;
	}

	/**
	 * Gets the value of the tExCurrencyAllInqRs property.
	 * 
	 * @return possible object is {@link TExCurrencyAllInqRs }
	 * 
	 */
	public TExCurrencyAllInqRs getTExCurrencyAllInqRs() {
		return tExCurrencyAllInqRs;
	}

	/**
	 * Sets the value of the tExCurrencyAllInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TExCurrencyAllInqRs }
	 * 
	 */
	public void setTExCurrencyAllInqRs(TExCurrencyAllInqRs value) {
		this.tExCurrencyAllInqRs = value;
	}

	/**
	 * Gets the value of the c201OrgCusInqRq property.
	 * 
	 * @return possible object is {@link C201OrgCusInqRq }
	 * 
	 */
	public C201OrgCusInqRq getC201OrgCusInqRq() {
		return c201OrgCusInqRq;
	}

	/**
	 * Sets the value of the c201OrgCusInqRq property.
	 * 
	 * @param value
	 *            allowed object is {@link C201OrgCusInqRq }
	 * 
	 */
	public void setC201OrgCusInqRq(C201OrgCusInqRq value) {
		this.c201OrgCusInqRq = value;
	}

	/**
	 * Gets the value of the c201OrgCusInqRs property.
	 * 
	 * @return possible object is {@link C201OrgCusInqRs }
	 * 
	 */
	public C201OrgCusInqRs getC201OrgCusInqRs() {
		return c201OrgCusInqRs;
	}

	/**
	 * Sets the value of the c201OrgCusInqRs property.
	 * 
	 * @param value
	 *            allowed object is {@link C201OrgCusInqRs }
	 * 
	 */
	public void setC201OrgCusInqRs(C201OrgCusInqRs value) {
		this.c201OrgCusInqRs = value;
	}
	
	/**
	 * Gets the value of the tDpAcctNewtdaAaaRq property.
	 * 
	 * @return possible object is {@link TDpAcctNewtdaAaaRq }
	 * 
	 */
	public TDpAcctNewtdaAaaRq getTDpAcctNewtdaAaaRq() {
		return tDpAcctNewtdaAaaRq;
	}

	/**
	 * Sets the value of the tDpAcctNewtdaAaaRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcctNewtdaAaaRq }
	 * 
	 */
	public void setTDpAcctNewtdaAaaRq(TDpAcctNewtdaAaaRq value) {
		this.tDpAcctNewtdaAaaRq = value;
	}

	/**
	 * Gets the value of the tDpAcctNewtdaAaaRs property.
	 * 
	 * @return possible object is {@link TDpAcctNewtdaAaaRs }
	 * 
	 */
	public TDpAcctNewtdaAaaRs getTDpAcctNewtdaAaaRs() {
		return tDpAcctNewtdaAaaRs;
	}

	/**
	 * Sets the value of the tDpAcctNewtdaAaaRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcctNewtdaAaaRs }
	 * 
	 */
	public void setTDpAcctNewtdaAaaRs(TDpAcctNewtdaAaaRs value) {
		this.tDpAcctNewtdaAaaRs = value;
	}

	/**
	 * Gets the value of the tDpAcActModRq property.
	 * 
	 * @return possible object is {@link TDpAcAciModRq }
	 * 
	 */
	public TDpAcAciModRq getTDpAcAciModRq() {
		return tDpAcAciModRq;
	}

	/**
	 * Sets the value of the tDpAcActModRq property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcAciModRq }
	 * 
	 */
	public void setTDpAcAciModRq(TDpAcAciModRq value) {
		this.tDpAcAciModRq = value;
	}

	/**
	 * Gets the value of the tDpAcActModRs property.
	 * 
	 * @return possible object is {@link TDpAcAciModRs }
	 * 
	 */
	public TDpAcAciModRs getTDpAcAciModRs() {
		return tDpAcAciModRs;
	}

	/**
	 * Sets the value of the tDpAcActModRs property.
	 * 
	 * @param value
	 *            allowed object is {@link TDpAcAciModRs }
	 * 
	 */
	public void setTDpAcAciModRs(TDpAcAciModRs value) {
		this.tDpAcAciModRs = value;
	}

}
