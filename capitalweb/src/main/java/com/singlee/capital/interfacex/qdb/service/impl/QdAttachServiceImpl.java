package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.FileTransferUtil;
import com.singlee.capital.interfacex.qdb.mapper.QdAttachMapper;
import com.singlee.capital.interfacex.qdb.service.QdAttachService;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

@Service("qdAttachService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class QdAttachServiceImpl implements QdAttachService{
	
	@Autowired
	private QdAttachMapper qdAttachMapper;
	
	//是否保存到远程服务器
	private static Boolean remoteSave = null;
	
	
	public static Logger log = Logger.getLogger("INTERFACEX");

	private static Boolean getRemoteSave(){
		if(remoteSave == null){
			remoteSave = Boolean.parseBoolean(PropertiesUtil.getProperties("Attach.remoteSave", "true"));
		}
		return remoteSave;
	}
	
	@Override
	public String attachUpload(String serino, String path) throws Exception {
		if(getRemoteSave() == false){
			log.info("配置设置不保存附件到远程服务器");
			return "FAIL";
		}
		String ret = null;
		try {
			ret = FileTransferUtil.fileUpload(serino, path);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("上传附件失败", e);
			ret = "ERROR";
		}
		return ret;
	}

	@Override
	public String attachDownload(String serino, String path) throws Exception {
		String ret = null;
		try {
			ret = FileTransferUtil.fileDownload(serino, path);
		} catch (Exception e) {
			log.error("上传附件失败", e);
			ret = "ERROR";
		}
		return ret;
	}

	@Override
	public String getAttachSerino() throws Exception{
		String day = new SimpleDateFormat("yyyyMMdd").format(new Date());
		return day +"_FIBS_"+  StringUtil.leftPad(qdAttachMapper.getAttachSerino(), 7, '0');
	}
	


}
