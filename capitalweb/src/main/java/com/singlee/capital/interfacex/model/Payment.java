package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class Payment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dealno        ;
	private String prdname       ;
	private String paypathcode   ;
	private String posentrymode  ;
	private String busitype      ;
	private String busikind      ;
	private String currency      ;
	private String amount        ;
	private String payeracc      ;
	private String payername     ;
	private String payeeaccbank  ;
	private String payeeacc      ;
	private String payeename     ;
	private String retcode       ;
	private String retmsg        ;
	private String workdate      ;
	private String srquid;
	private String bankid;
	private String remarkNote;
	private String handleCode;
	private String payresult;
	
	
	
	
	
	public String getBankid() {
		return bankid;
	}
	public void setBankid(String bankid) {
		this.bankid = bankid;
	}
	public String getPayresult() {
		return payresult;
	}
	public void setPayresult(String payresult) {
		this.payresult = payresult;
	}
	public String getRemarkNote() {
		return remarkNote;
	}
	public void setRemarkNote(String remarkNote) {
		this.remarkNote = remarkNote;
	}
	public String getHandleCode() {
		return handleCode;
	}
	public void setHandleCode(String handleCode) {
		this.handleCode = handleCode;
	}
	public String getSrquid() {
		return srquid;
	}
	public void setSrquid(String srquid) {
		this.srquid = srquid;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getPrdname() {
		return prdname;
	}
	public void setPrdname(String prdname) {
		this.prdname = prdname;
	}
	public String getPaypathcode() {
		return paypathcode;
	}
	public void setPaypathcode(String paypathcode) {
		this.paypathcode = paypathcode;
	}
	public String getPosentrymode() {
		return posentrymode;
	}
	public void setPosentrymode(String posentrymode) {
		this.posentrymode = posentrymode;
	}
	public String getBusitype() {
		return busitype;
	}
	public void setBusitype(String busitype) {
		this.busitype = busitype;
	}
	public String getBusikind() {
		return busikind;
	}
	public void setBusikind(String busikind) {
		this.busikind = busikind;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayeracc() {
		return payeracc;
	}
	public void setPayeracc(String payeracc) {
		this.payeracc = payeracc;
	}
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public String getPayeeaccbank() {
		return payeeaccbank;
	}
	public void setPayeeaccbank(String payeeaccbank) {
		this.payeeaccbank = payeeaccbank;
	}
	public String getPayeeacc() {
		return payeeacc;
	}
	public void setPayeeacc(String payeeacc) {
		this.payeeacc = payeeacc;
	}
	public String getPayeename() {
		return payeename;
	}
	public void setPayeename(String payeename) {
		this.payeename = payeename;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return retmsg;
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	public String getWorkdate() {
		return workdate;
	}
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}

}
