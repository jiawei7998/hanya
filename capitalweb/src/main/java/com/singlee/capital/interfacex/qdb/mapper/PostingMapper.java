package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.Posting;

import tk.mybatis.mapper.common.Mapper;

/****
 * 
 * 日终账务
 * @author singlee
 *
 */
public interface PostingMapper extends Mapper<Posting>{
	
	Page<Posting> pagePosting(Map<String, Object> map, RowBounds rb);
	
	List<Posting> listPosting(Map<String, Object> map);
	
	List<Posting> zlistPosting(Map<String, Object> map);
	
	Posting sumValuePosting(Map<String, Object> map);
	
	Posting countPosting(Map<String, Object> map);

}
