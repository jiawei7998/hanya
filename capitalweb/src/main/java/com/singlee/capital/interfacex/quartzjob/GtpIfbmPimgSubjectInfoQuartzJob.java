package com.singlee.capital.interfacex.quartzjob;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.service.GtpFileService;

public class GtpIfbmPimgSubjectInfoQuartzJob  implements CronRunnable {

	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		gtpFileService.GtpIfbmPimgSubjectInfoRead();
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
