package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCoSignonMultAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCoSignonMultAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumAccNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalEntTyp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbNoRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}FbNoRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FbNoStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCoSignonMultAaaRs", propOrder = { "commonRsHdr", "acctNo",
		"mediumType", "mediumAccNo", "customerId", "name", "legalEntTyp",
		"legalId", "fbNoRec", "fbNoStatus", "coCode" })
public class TCoSignonMultAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "MediumType", required = true)
	protected String mediumType;
	@XmlElement(name = "MediumAccNo", required = true)
	protected String mediumAccNo;
	@XmlElement(name = "CustomerId", required = true)
	protected String customerId;
	@XmlElement(name = "Name", required = true)
	protected String name;
	@XmlElement(name = "LegalEntTyp", required = true)
	protected String legalEntTyp;
	@XmlElement(name = "LegalId", required = true)
	protected String legalId;
	@XmlElement(name = "FbNoRec")
	protected List<FbNoRec> fbNoRec;
	@XmlElement(name = "FbNoStatus", required = true)
	protected String fbNoStatus;
	@XmlElement(name = "CoCode", required = true)
	protected String coCode;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the mediumType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumType() {
		return mediumType;
	}

	/**
	 * Sets the value of the mediumType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumType(String value) {
		this.mediumType = value;
	}

	/**
	 * Gets the value of the mediumAccNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumAccNo() {
		return mediumAccNo;
	}

	/**
	 * Sets the value of the mediumAccNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumAccNo(String value) {
		this.mediumAccNo = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerId(String value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the legalEntTyp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalEntTyp() {
		return legalEntTyp;
	}

	/**
	 * Sets the value of the legalEntTyp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalEntTyp(String value) {
		this.legalEntTyp = value;
	}

	/**
	 * Gets the value of the legalId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalId() {
		return legalId;
	}

	/**
	 * Sets the value of the legalId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalId(String value) {
		this.legalId = value;
	}

	/**
	 * Gets the value of the fbNoRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the fbNoRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getFbNoRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link FbNoRec }
	 * 
	 * 
	 */
	public List<FbNoRec> getFbNoRec() {
		if (fbNoRec == null) {
			fbNoRec = new ArrayList<FbNoRec>();
		}
		return this.fbNoRec;
	}

	/**
	 * Gets the value of the fbNoStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFbNoStatus() {
		return fbNoStatus;
	}

	/**
	 * Sets the value of the fbNoStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFbNoStatus(String value) {
		this.fbNoStatus = value;
	}

	/**
	 * Gets the value of the coCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoCode() {
		return coCode;
	}

	/**
	 * Sets the value of the coCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoCode(String value) {
		this.coCode = value;
	}

}
