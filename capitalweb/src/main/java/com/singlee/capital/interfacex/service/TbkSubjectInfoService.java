package com.singlee.capital.interfacex.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.SubjectInfo;

public interface TbkSubjectInfoService {
	
	/**
	 * 条件查询所有  （分页）
	 * @param map
	 * @return
	 */
	public Page<SubjectInfo> selectAllSubjectInfoPagedService(Map<String, Object> map);
}
