package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QuotaSyncopateRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QuotaSyncopateRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EcifNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SysId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TermType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntrStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuotaSyncopateRec", propOrder = { "ecifNum", "sysId",
		"sector", "customer", "productType", "currency", "loanAmt", "term",
		"termType", "dueDate", "cntrStatus" ,"dueDateLast","loanAmtAll"})
public class QuotaSyncopateRec {

	@XmlElement(name = "EcifNum", required = true)
	protected String ecifNum;
	@XmlElement(name = "SysId", required = true)
	protected String sysId;
	@XmlElement(name = "Sector", required = true)
	protected String sector;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "ProductType", required = true)
	protected String productType;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "LoanAmt", required = true)
	protected String loanAmt;
	@XmlElement(name = "Term", required = true)
	protected String term;
	@XmlElement(name = "TermType", required = true)
	protected String termType;
	@XmlElement(name = "DueDate", required = true)
	protected String dueDate;
	@XmlElement(name = "CntrStatus", required = true)
	protected String cntrStatus;
	@XmlElement(name = "DueDateLast", required = true)
	protected String dueDateLast;
	@XmlElement(name = "LoanAmtAll", required = true)
	protected String loanAmtAll;


	/**
	 * Gets the value of the ecifNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEcifNum() {
		return ecifNum;
	}

	/**
	 * Sets the value of the ecifNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEcifNum(String value) {
		this.ecifNum = value;
	}

	/**
	 * Gets the value of the sysId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSysId() {
		return sysId;
	}

	/**
	 * Sets the value of the sysId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSysId(String value) {
		this.sysId = value;
	}

	/**
	 * Gets the value of the sector property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * Sets the value of the sector property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSector(String value) {
		this.sector = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the productType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * Sets the value of the productType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductType(String value) {
		this.productType = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the loanAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAmt() {
		return loanAmt;
	}

	/**
	 * Sets the value of the loanAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAmt(String value) {
		this.loanAmt = value;
	}

	/**
	 * Gets the value of the term property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the value of the term property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTerm(String value) {
		this.term = value;
	}

	/**
	 * Gets the value of the termType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTermType() {
		return termType;
	}

	/**
	 * Sets the value of the termType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTermType(String value) {
		this.termType = value;
	}

	/**
	 * Gets the value of the dueDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * Sets the value of the dueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDueDate(String value) {
		this.dueDate = value;
	}

	public String getDueDateLast() {
		return dueDateLast;
	}

	public void setDueDateLast(String value) {
		this.dueDateLast = value;
	}
	/**
	 * Gets the value of the cntrStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntrStatus() {
		return cntrStatus;
	}

	/**
	 * Sets the value of the cntrStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntrStatus(String value) {
		this.cntrStatus = value;
	}

	public String getLoanAmtAll() {
		return loanAmtAll;
	}

	public void setLoanAmtAll(String loanAmtAll) {
		this.loanAmtAll = loanAmtAll;
	}

}
