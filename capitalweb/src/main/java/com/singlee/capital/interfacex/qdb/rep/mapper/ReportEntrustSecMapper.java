package com.singlee.capital.interfacex.qdb.rep.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustSecBean;

public interface ReportEntrustSecMapper extends Mapper<ReportEntrustSecBean> {
	

		/**
		 * 
		 * @param mapR
		 * @param row
		 * @return
		 */
		public Page<ReportEntrustSecBean> listReportEntrustSec(Map<String,Object> map, RowBounds row);
		/**
		 * 
		 * @param mapR
		 * @param row
		 * @return
		 */
		public List<ReportEntrustSecBean> listReportEntrustSec(Map<String,Object> map);

}
