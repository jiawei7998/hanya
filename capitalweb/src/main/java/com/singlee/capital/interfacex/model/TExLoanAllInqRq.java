package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExLoanAllInqRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExLoanAllInqRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanNoRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}LoanNoRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExLoanAllInqRq", propOrder = { "commonRqHdr", "custId",
		"loanNoRec" })
public class TExLoanAllInqRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "CustId", required = true)
	protected String custId;
	@XmlElement(name = "LoanNoRec")
	protected List<LoanNoRec> loanNoRec;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the custId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustId() {
		return custId;
	}

	/**
	 * Sets the value of the custId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustId(String value) {
		this.custId = value;
	}

	/**
	 * Gets the value of the loanNoRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the loanNoRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getLoanNoRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link LoanNoRec }
	 * 
	 * 
	 */
	public List<LoanNoRec> getLoanNoRec() {
		if (loanNoRec == null) {
			loanNoRec = new ArrayList<LoanNoRec>();
		}
		return this.loanNoRec;
	}

	public void setLoanNoRec(List<LoanNoRec> loanNoRec) {
		this.loanNoRec = loanNoRec;
	}
	
}
