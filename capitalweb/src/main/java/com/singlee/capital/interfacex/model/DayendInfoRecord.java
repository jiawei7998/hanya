package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class DayendInfoRecord implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String biz_date;
	public String step_id;
	public String step_name;
	public String status;
	public String step_sort;
	public String operate_time;
	public String getBiz_date() {
		return biz_date;
	}
	public void setBiz_date(String biz_date) {
		this.biz_date = biz_date;
	}
	public String getStep_id() {
		return step_id;
	}
	public void setStep_id(String step_id) {
		this.step_id = step_id;
	}
	public String getStep_name() {
		return step_name;
	}
	public void setStep_name(String step_name) {
		this.step_name = step_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStep_sort() {
		return step_sort;
	}
	public void setStep_sort(String step_sort) {
		this.step_sort = step_sort;
	}
	public String getOperate_time() {
		return operate_time;
	}
	public void setOperate_time(String operate_time) {
		this.operate_time = operate_time;
	}
	
}
