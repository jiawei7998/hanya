package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.CnapsType;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdSettleMapper;
import com.singlee.capital.interfacex.qdb.pojo.TdTrdSettle;
import com.singlee.capital.interfacex.qdb.service.CnapsService;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CnapsServiceImpl implements CnapsService{
	
	@Autowired
	private TdTrdSettleMapper tdTrdSettleMapper;
	
	@Autowired
	private TtInstitutionSettlsMapper institutionSettlsMapper;
	
	
	
	public static Logger log = Logger.getLogger("INTERFACEX");

	@Override
	public RetMsg<Serializable> validSettleState(TdTrdSettle td)throws Exception {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dealNo", td.getDealNo());
		param.put("is_locked", true);//加锁查询记录,查询时将交易记录上锁 悲观所for update
		TdTrdSettle trdSettle_1 = tdTrdSettleMapper.queryTdTrdSettleByDealNo(param);
		ret.setObj(trdSettle_1);
		if(StringUtils.isBlank(trdSettle_1.getVoper()) || StringUtils.isBlank(trdSettle_1.getVtime()))//复核人为空,该交易还没有被复核完成
		{
			if(StringUtils.isBlank(trdSettle_1.getDealUser())){//操作人为空,说明该交易还没有开始复核
				param = new HashMap<String, Object>();
				param.put("dealNo", td.getDealNo());
				param.put("dealFlag", td.getDealFlag());
				param.put("voidFlag", td.getVoidFlag());
				param.put("settFlag", td.getSettFlag());
				param.put("dealUser", td.getDealUser());
				param.put("dealTime", td.getDealTime());
				if(tdTrdSettleMapper.updateTdTrdSettleState(param) > 0){
					ret.setDesc("验证成功");
					
				} else {
					ret.setCode("999999");
					ret.setDesc("更新记录状态失败,请联系数据库管理员");
				}
				
			} else if (trdSettle_1.getDealUser().equals(td.getDealUser())){//操作人不为空,等于当前操作人,说明该交易之前处理出错,重复处理
				ret.setDesc("验证成功");
				
				
			} else {//操作人不为空,并且不等于当前操作人,说明该交易正在被复核,但是还没有复核完成
				ret.setCode("999999");
				ret.setDesc(
				"<br><br><br>您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
				+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
				+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已有用户: "
				+trdSettle_1.getDealUser()+"正在处理！<br>处理时间: "+trdSettle_1.getDealTime());
			}
			
		} else if(StringUtils.isNotBlank(trdSettle_1.getVoper()) 
				&& CnapsType.DealFlag.VERIFY.getType().equals(trdSettle_1.getDealFlag()))//复核人不为空,该交易已经被复核
		{
			ret.setCode("999999");
			ret.setDesc(
			"<br><br><br>您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
			+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
			+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已经被: "
			+trdSettle_1.getDealUser()+"复核处理！<br>签发时间: "+trdSettle_1.getDealTime());
			
		} 
		return ret;
	}

	@Override
	public int updateTdTrdSettleState(Map<String, Object> param)throws Exception {
		return tdTrdSettleMapper.updateTdTrdSettleState(param);
	}

	@Override
	public TdTrdSettle queryTdTrdSettleByDealNo(Map<String, Object> param)throws Exception {
		return tdTrdSettleMapper.queryTdTrdSettleByDealNo(param);
	}

	@Override
	public int updateTdTrdRetMsg(Map<String, Object> param) throws Exception {
		return tdTrdSettleMapper.updateTdTrdRetMsg(param);
	}

	@Override
	public Page<TdTrdSettle> getVerifySettleInstList(Map<String, Object> map) {
		return tdTrdSettleMapper.pageVerifySettles(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public RetMsg<Serializable> insertTdTrdSettleByProduct(TdProductApproveMain approveMain) throws Exception {
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("refNo", approveMain.getDealNo());
			if(tdTrdSettleMapper.queryTdTrdSettleByDealNo(param) != null)
			{
				return ret;
			}
			
			TdTrdSettle tdTrdSettle = new TdTrdSettle();
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("sStr","C");
			map.put("trdType",approveMain.getPrdNo());
			String tradeId = tdTrdSettleMapper.getTradeId(map);
			tdTrdSettle.setDealNo(tradeId);//流水号
			tdTrdSettle.setProduct(String.valueOf(approveMain.getPrdNo()));// 产品代码
			tdTrdSettle.setRefNo(approveMain.getDealNo());//原交易流水号
			tdTrdSettle.setSeq("0");//分序号
			tdTrdSettle.setServer("CNAPS");//来源
			tdTrdSettle.setCno(approveMain.getcNo());//交易对手号
			tdTrdSettle.setCustName(approveMain.getCounterParty() == null ? "" : approveMain.getCounterParty().getParty_name());
			tdTrdSettle.setVdate(approveMain.gettDate());//付款日期
			tdTrdSettle.setPayrecnd("P");//收付方向
			tdTrdSettle.setCcy(approveMain.getCcy());// 币种
			tdTrdSettle.setAmount(new BigDecimal(approveMain.getProcAmt()));//金额
			tdTrdSettle.setSetmeans("CNAPS");//清算方式
			tdTrdSettle.setSetacct("CNY-CNAPS-CTRBK");// 清算账户
			
			HashMap<String, Object> map_1 = new HashMap<String, Object>();
			map_1.put("instId", approveMain.getSponInst());
			List<TtInstitutionSettls> institutionSettls = institutionSettlsMapper.getInstSettlsList(map_1);
			if(institutionSettls == null || institutionSettls.size() == 0){
				map.put("instId", "99200");
				map.put("ccy", "CNY");
				institutionSettls = institutionSettlsMapper.getInstSettlsList(map);
			}
			if(institutionSettls != null && institutionSettls.size() > 0){
				tdTrdSettle.setPayBankId(institutionSettls.get(0).getInstAcctPbocNo());//付款行行号
				tdTrdSettle.setPayBankName(institutionSettls.get(0).getInstAcctBknm());//付款行行名
				tdTrdSettle.setPayUserId(institutionSettls.get(0).getInstAcctNo());//付款账号
				tdTrdSettle.setPayUserName(institutionSettls.get(0).getInstAcctNm());//付款账号名称
			}
			
			tdTrdSettle.setRecBankId(approveMain.getPartyBankCode());//收款行行号
			tdTrdSettle.setRecBankName(approveMain.getPartyBankName());// 收款行行名
			tdTrdSettle.setRecUserId(approveMain.getPartyAccCode());//收款账号
			tdTrdSettle.setRecUserName(approveMain.getPartyAccName());//收款账号名称
			tdTrdSettle.setInstitution("80002");//营业机构号
			tdTrdSettle.setHvpType("A100");//业务种类(10-现金汇款,11-普通汇兑,12-网银支付...)
			tdTrdSettle.setHvpType1("0");//汇兑类型(0-正常)
			tdTrdSettle.setDealFlag("0");//处理状态(0-待经办 1-已经办待复核 A-正在复核中 C-复核异常 F-已复核 B-退回 )
			tdTrdSettle.setVoidFlag("0");//撤销标志(0-申请撤销,1-撤销成功,2-撤销失败)
			tdTrdSettle.setSettFlag("0");// 清算标志
			String userDealNo = "80002-" + DateUtil.getCurrentDateAsString("yyMM") + "-252-" + tradeId.substring(tradeId.length() - 6, tradeId.length());
			tdTrdSettle.setUserDealNo(userDealNo);// 汇兑组号
			tdTrdSettle.setBusDealNo("000" + tradeId.substring(tradeId.length() - 6, tradeId.length()));
			tdTrdSettle.setInputTime(DateUtil.getCurrentDateTimeAsString());//接口数据插入时间
			tdTrdSettleMapper.insert(tdTrdSettle);
			ret.setObj(tdTrdSettle);
			return ret;
		} catch (Exception e) {
			log.error("生成结算指令出错", e);
			throw e;
		}
	}

	@Override
	public RetMsg<Serializable> submitSettle(Map<String, Object> map)throws Exception {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "经办成功");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dealNo", map.get("dealNo"));
		param.put("is_locked", true);//加锁查询记录,查询时将交易记录上锁 悲观所for update
		TdTrdSettle trdSettle_1 = tdTrdSettleMapper.queryTdTrdSettleByDealNo(param);
		trdSettle_1.setRecBankId(String.valueOf(map.get("recBankId")));  
		trdSettle_1.setRecBankName(String.valueOf(map.get("recBankName")));
		trdSettle_1.setRecUserId(String.valueOf(map.get("recUserId")));  
		trdSettle_1.setRecUserName(String.valueOf(map.get("recUserName")));
		
		ret.setObj(trdSettle_1);
		if(StringUtils.isEmpty(trdSettle_1.getRecBankId()) || StringUtils.isEmpty(trdSettle_1.getRecBankName()) 
				|| StringUtils.isEmpty(trdSettle_1.getRecUserId()) || StringUtils.isEmpty(trdSettle_1.getRecUserName()))
		{
			ret.setCode("999999");
			ret.setDesc("您所需要确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
					+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
					+"<br><html><font color=red>金额:"+StringUtils.formatDouble(trdSettle_1.getAmount().doubleValue())
					+"</font></html><br>清算信息空缺！");
			return ret;
		}
		
		if(!CnapsType.DealFlag.BACK.getType().equals(trdSettle_1.getDealFlag()) && StringUtils.isNotEmpty(trdSettle_1.getIoper()))
		{
			ret.setCode("999999");
			ret.setDesc(
			"您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
			+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
			+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已经被: "
			+trdSettle_1.getIoper()+"经办处理！<br>经办时间: "+trdSettle_1.getItime());
			return ret;
		}
		trdSettle_1.setIoper(SlSessionHelper.getUserId());
		trdSettle_1.setItime(DateUtil.getCurrentDateTimeAsString());
		trdSettle_1.setDealFlag("1");
		trdSettle_1.setRemarkFy(map.get("remarkFy").toString());
		trdSettle_1.setHvpType(map.get("hvpType").toString());
		
		param = new HashMap<String, Object>();
		param.put("dealNo", trdSettle_1.getDealNo());
		
		param.put("recBankId",   map.get("recBankId"));  
		param.put("recBankName", map.get("recBankName"));
		param.put("recUserId",   map.get("recUserId"));  
		param.put("recUserName", map.get("recUserName"));
		
		param.put("ioper", trdSettle_1.getIoper());
		param.put("itime", trdSettle_1.getItime());
		param.put("dealFlag", trdSettle_1.getDealFlag());
		param.put("remarkFy", trdSettle_1.getRemarkFy());
		param.put("hvpType", trdSettle_1.getHvpType());
		tdTrdSettleMapper.updateTdTrdSettleState(param);
		return ret;
	}

	@Override
	public RetMsg<Serializable> backSettle(Map<String, Object> params) throws Exception {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dealNo", params.get("dealNo"));
		param.put("is_locked", true);//加锁查询记录,查询时将交易记录上锁 悲观所for update
		TdTrdSettle trdSettle_1 = tdTrdSettleMapper.queryTdTrdSettleByDealNo(param);
		ret.setObj(trdSettle_1);
		if(StringUtils.isNotBlank(trdSettle_1.getDealUser()) && StringUtils.isBlank(trdSettle_1.getVoper()))//处理人不为空,该交易已经被人处理
		{
			ret.setCode("999999");
			ret.setDesc(
			"您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
			+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
			+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已有用户: "
			+trdSettle_1.getDealUser()+"正在处理！<br>处理时间: "+trdSettle_1.getDealTime());
			return ret;
		}
		if(StringUtils.isNotBlank(trdSettle_1.getVoper()) && CnapsType.DealFlag.BACK.getType().equals(trdSettle_1.getDealFlag()))//复核人不为空,该交易已经被退回
		{
			ret.setCode("999999");
			ret.setDesc(
			"您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
			+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
			+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已经被: "
			+trdSettle_1.getVoper()+"退回处理！<br>退回时间: "+trdSettle_1.getBackTime());
			return ret;
		}
		if(StringUtils.isNotBlank(trdSettle_1.getVoper()) && CnapsType.DealFlag.VERIFY.getType().equals(trdSettle_1.getDealFlag()))//复核人不为空,该交易已经被复核
		{
			ret.setCode("999999");
			ret.setDesc(
			"您所确认的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
			+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()
			+"<br><html><font color=red>金额: "+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())+"</font></html><br>已经被: "
			+trdSettle_1.getVoper()+"签发处理！<br>签发时间: "+trdSettle_1.getVtime());
			return ret;
		}
		trdSettle_1.setDealFlag("B");
		trdSettle_1.setVoper(SlSessionHelper.getUserId());
		trdSettle_1.setBackTime(DateUtil.getCurrentDateTimeAsString());
		
		param = new HashMap<String, Object>();
		param.put("dealNo", trdSettle_1.getDealNo());
		param.put("dealFlag", trdSettle_1.getDealFlag());
		param.put("voper", trdSettle_1.getVoper());
		param.put("backTime", trdSettle_1.getBackTime());
		
		if(tdTrdSettleMapper.updateTdTrdSettleStateForBack(param) > 0)
		{
			ret.setCode(RetMsgHelper.codeOk);
			ret.setDesc("您所确认签发的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
					+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()+"<br><html><font color=red>金额:"
					+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())
					+"</font></html><br>退回处理成功！");
			return ret;
			
		}else{
			ret.setCode("999999");
			ret.setDesc("您所确认签发的往账交易信息:<br>账号:"+trdSettle_1.getRecUserId()+"<br>户名:"+trdSettle_1.getRecUserName()
					+"<br>行号:"+trdSettle_1.getRecBankId()+"<br>行名:"+trdSettle_1.getRecBankName()+"<br><html><font color=red>金额:"
					+StringUtils.formatDouble(trdSettle_1.getAmount().abs().doubleValue())
					+"</font></html><br>退回失败,原因:更新数据库状态失败,请联系数据库管理员");
			return ret;
		}
	}

	@Override
	public Integer searchVerifySettlesCount(Map<String, Object> map)throws Exception {
		return tdTrdSettleMapper.searchVerifySettlesCount(map);
	}

}
