package com.singlee.capital.interfacex.qdb.service;

import java.io.Serializable;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.interfacex.qdb.pojo.TdTrdSettle;
import com.singlee.capital.trade.model.TdProductApproveMain;

/****
 * 
 * 大额支付接口
 * @author singlee
 *
 */
public interface CnapsService {
	
	public RetMsg<Serializable> validSettleState(TdTrdSettle tdTrdSettle)throws Exception;
	
	public int updateTdTrdSettleState(Map<String, Object> param)throws Exception;
	
	public int updateTdTrdRetMsg(Map<String, Object> param)throws Exception;
	
	public TdTrdSettle queryTdTrdSettleByDealNo(Map<String, Object> param)throws Exception;

	public Page<TdTrdSettle> getVerifySettleInstList(Map<String, Object> map);
	
	public RetMsg<Serializable> insertTdTrdSettleByProduct(TdProductApproveMain approveMain)throws Exception;

	public RetMsg<Serializable> submitSettle(Map<String, Object> map)throws Exception;

	public RetMsg<Serializable> backSettle(Map<String, Object> params)throws Exception;
	
	public Integer searchVerifySettlesCount(Map<String, Object> map)throws Exception;
}
