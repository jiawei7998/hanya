package com.singlee.capital.interfacex.signterminate.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRs;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRs;
import com.singlee.capital.interfacex.signterminate.model.SignAccount;
import com.singlee.capital.interfacex.signterminate.service.SignAndTerminateContactService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

/**
 * @projectName 同业业务管理系统
 * @className 同业签约解约功能
 * @description TODO
 * @author tangbin
 * @createDate 2017-5-13 下午16:16:28	
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/SignAndTerminateContractController")
public class SignAndTerminateContractController {
	@Autowired
	private SignAndTerminateContactService signAndTerminateContactService;
	

	/**
	 * 
	 * @param 签约
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/SignContract")
	public RetMsg<Serializable> signContract(@RequestBody Map<String, Object> map) throws Exception{
		TCoSignonMultAaaRs tCoSignonMultAaaRs=null;
		tCoSignonMultAaaRs=signAndTerminateContactService.signContract(map);
		if(tCoSignonMultAaaRs==null) {
            return RetMsgHelper.simple("与T24通信错误");
        }
		if(tCoSignonMultAaaRs.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
			if("1".equals(tCoSignonMultAaaRs.getFbNoStatus())) {
                return RetMsgHelper.ok();
            }
		}
		return RetMsgHelper.simple("签约失败");
	}
	
	/**
	 * 解约
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/TerminateContract")
	public RetMsg<Serializable> terminateContract(@RequestBody Map<String, Object> map)throws Exception {
		TCoSignoffMultAaaRs tCoSignoffMultAaaRs=signAndTerminateContactService.terminateContract(map);
		if(tCoSignoffMultAaaRs==null) {
            return RetMsgHelper.simple("与T24通信错误");
        }
		if(tCoSignoffMultAaaRs.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
			if("1".equals(tCoSignoffMultAaaRs.getFbNoStatus())) {
                return RetMsgHelper.ok();
            }
		}
		return RetMsgHelper.simple("解约失败");
	}
	
	
	/**
	 * 查询当前签约状态
	 * @param tcterm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/SignAndTerminateQueryList")
	public RetMsg<PageInfo<SignAccount>> creditQueryList(@RequestBody Map<String, Object> map) throws Exception{
		Page<SignAccount> page = signAndTerminateContactService.selectContract(map);
		return RetMsgHelper.ok(page);
	} 
	
}
