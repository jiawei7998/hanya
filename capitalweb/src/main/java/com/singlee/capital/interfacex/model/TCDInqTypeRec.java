package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCDInqType_Rec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCDInqType_Rec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddrNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustAdress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Continent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Province" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="District" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StrAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Contacter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCDInqType_Rec", propOrder = { "addrNum", "addrType",
		"custAdress", "continent", "addCountry", "province", "city",
		"district", "street", "strAddr", "contacter", "comPhone" })
public class TCDInqTypeRec {

	@XmlElement(name = "AddrNum", required = true)
	protected String addrNum;
	@XmlElement(name = "AddrType", required = true)
	protected String addrType;
	@XmlElement(name = "CustAdress", required = true)
	protected String custAdress;
	@XmlElement(name = "Continent", required = true)
	protected String continent;
	@XmlElement(name = "AddCountry", required = true)
	protected String addCountry;
	@XmlElement(name = "Province", required = true)
	protected String province;
	@XmlElement(name = "City", required = true)
	protected String city;
	@XmlElement(name = "District", required = true)
	protected String district;
	@XmlElement(name = "Street", required = true)
	protected String street;
	@XmlElement(name = "StrAddr", required = true)
	protected String strAddr;
	@XmlElement(name = "Contacter", required = true)
	protected String contacter;
	@XmlElement(name = "ComPhone", required = true)
	protected String comPhone;

	/**
	 * Gets the value of the addrNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrNum() {
		return addrNum;
	}

	/**
	 * Sets the value of the addrNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrNum(String value) {
		this.addrNum = value;
	}

	/**
	 * Gets the value of the addrType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrType() {
		return addrType;
	}

	/**
	 * Sets the value of the addrType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrType(String value) {
		this.addrType = value;
	}

	/**
	 * Gets the value of the custAdress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustAdress() {
		return custAdress;
	}

	/**
	 * Sets the value of the custAdress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustAdress(String value) {
		this.custAdress = value;
	}

	/**
	 * Gets the value of the continent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContinent() {
		return continent;
	}

	/**
	 * Sets the value of the continent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContinent(String value) {
		this.continent = value;
	}

	/**
	 * Gets the value of the addCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddCountry() {
		return addCountry;
	}

	/**
	 * Sets the value of the addCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddCountry(String value) {
		this.addCountry = value;
	}

	/**
	 * Gets the value of the province property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the value of the province property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProvince(String value) {
		this.province = value;
	}

	/**
	 * Gets the value of the city property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the value of the city property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCity(String value) {
		this.city = value;
	}

	/**
	 * Gets the value of the district property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * Sets the value of the district property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDistrict(String value) {
		this.district = value;
	}

	/**
	 * Gets the value of the street property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the value of the street property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStreet(String value) {
		this.street = value;
	}

	/**
	 * Gets the value of the strAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStrAddr() {
		return strAddr;
	}

	/**
	 * Sets the value of the strAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStrAddr(String value) {
		this.strAddr = value;
	}

	/**
	 * Gets the value of the contacter property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContacter() {
		return contacter;
	}

	/**
	 * Sets the value of the contacter property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContacter(String value) {
		this.contacter = value;
	}

	/**
	 * Gets the value of the comPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getComPhone() {
		return comPhone;
	}

	/**
	 * Sets the value of the comPhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setComPhone(String value) {
		this.comPhone = value;
	}

}
