package com.singlee.capital.interfacex.service;

import com.singlee.capital.interfacex.model.ApprovalChangeRq;
import com.singlee.capital.interfacex.model.ApprovalChangeRs;
import com.singlee.capital.interfacex.model.ApprovalInqRq;
import com.singlee.capital.interfacex.model.ApprovalInqRs;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRq;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRs;
import com.singlee.capital.interfacex.model.C201OrgCusInqRq;
import com.singlee.capital.interfacex.model.C201OrgCusInqRs;
import com.singlee.capital.interfacex.model.CloudProApplyTransRq;
import com.singlee.capital.interfacex.model.CloudProApplyTransRs;
import com.singlee.capital.interfacex.model.CloudProModTransRq;
import com.singlee.capital.interfacex.model.CloudProModTransRs;
import com.singlee.capital.interfacex.model.CloudProSynchTransRq;
import com.singlee.capital.interfacex.model.CloudProSynchTransRs;
import com.singlee.capital.interfacex.model.EnterpriseApproveRq;
import com.singlee.capital.interfacex.model.EnterpriseApproveRs;
import com.singlee.capital.interfacex.model.EnterpriseInqRq;
import com.singlee.capital.interfacex.model.EnterpriseInqRs;
import com.singlee.capital.interfacex.model.FinCreditApproveRq;
import com.singlee.capital.interfacex.model.FinCreditApproveRs;
import com.singlee.capital.interfacex.model.FinCreditInqRq;
import com.singlee.capital.interfacex.model.FinCreditInqRs;
import com.singlee.capital.interfacex.model.LoanNoticeRq;
import com.singlee.capital.interfacex.model.LoanNoticeRs;
import com.singlee.capital.interfacex.model.QuotaSyncopateRq;
import com.singlee.capital.interfacex.model.QuotaSyncopateRs;
import com.singlee.capital.interfacex.model.RepayNoticeRq;
import com.singlee.capital.interfacex.model.RepayNoticeRs;
import com.singlee.capital.interfacex.model.TCoActIntInqRq;
import com.singlee.capital.interfacex.model.TCoActIntInqRs;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRq;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRs;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRq;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRs;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRs;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRs;
import com.singlee.capital.interfacex.model.TCustDetInqRq;
import com.singlee.capital.interfacex.model.TCustDetInqRs;
import com.singlee.capital.interfacex.model.TDpAcAciModRq;
import com.singlee.capital.interfacex.model.TDpAcAciModRs;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRq;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRs;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRq;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRs;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRq;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRs;
import com.singlee.capital.interfacex.model.TDpCorpacctAllAaaRq;
import com.singlee.capital.interfacex.model.TDpCorpacctAllAaaRs;
import com.singlee.capital.interfacex.model.TDpIntBnkAaaRq;
import com.singlee.capital.interfacex.model.TDpIntBnkAaaRs;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRq;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRs;
import com.singlee.capital.interfacex.model.TExLdRpmSchInqRq;
import com.singlee.capital.interfacex.model.TExLdRpmSchInqRs;
import com.singlee.capital.interfacex.model.TExLoanAllInqRq;
import com.singlee.capital.interfacex.model.TExLoanAllInqRs;
import com.singlee.capital.interfacex.model.TExReconAllInqRq;
import com.singlee.capital.interfacex.model.TExReconAllInqRs;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.model.TStFtNormRevRq;
import com.singlee.capital.interfacex.model.TStFtNormRevRs;
import com.singlee.capital.interfacex.model.TStFtRemitAaaRq;
import com.singlee.capital.interfacex.model.TStFtRemitAaaRs;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRs;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRq;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRs;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;


public interface SocketClientService {

	/**
	 * 汇率信息查询（EX-CURRENCY-ALL-INQ）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TExCurrencyAllInqRs t24TexCurrencyAllInqRequest(TExCurrencyAllInqRq request) throws Exception;
	
	/**
	 * 多借多贷核心记账（ST-BEA-REPAY-AAA）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TStBeaRepayAaaRs t24TStBeaRepayAaaRequest(TStBeaRepayAaaRq request) throws Exception;
	/**
	 * 普通转账冲正（ST-FT-NORM-REV）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TStFtNormRevRs t24TstFtNormRevRequest(TStFtNormRevRq request) throws Exception;
	/**
	 * 国际业务汇出申请（ST-FT-REMIT-AAA）MT202接口、MT103接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TStFtRemitAaaRs t24TstFtRemitAaaRequest(TStFtRemitAaaRq request) throws Exception;
	/**
	 * 交易明细查询接口（DP-AC-STMT-INQ）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpAcStmtInqRs t24TdpAcStmtInqRequest(TDpAcStmtInqRq request) throws Exception;
	/**
	 * 对账查询接口（EX-RECON-ALL-INQ）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TExReconAllInqRs t24TexReconAllInqRequest(TExReconAllInqRq request) throws Exception;
	/**
	 * 账户介质类型查询（DP-ACCT-TYPE-INQ）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpAcctTypeInqRs t24TdpAcctTypeInqRequest(TDpAcctTypeInqRq request) throws Exception;
	/**
	 * 对公新开账户（DP-CORPACCT-ALL-AAA）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpCorpacctAllAaaRs t24TDpCorpacctAllAaaRequest(TDpCorpacctAllAaaRq request) throws Exception;
	/**
	 * 客户签约接口（CO-SIGNOFF-MULT-AAA）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCoSignonMultAaaRs t24TCoSignonMultAaaRequest(TCoSignonMultAaaRq request) throws Exception;
	/**
	 * 客户解约接口（CO-SIGNON-MULT-AAA）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCoSignoffMultAaaRs t24TCoSignoffMultAaaRequest(TCoSignoffMultAaaRq request) throws Exception;
	/**
	 * 节假日查询（CO-HOLIDAY-ALL-INQ）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCoHolidayAllInqRs t24TCoHolidayAllInqRequest(TCoHolidayAllInqRq request) throws Exception;
	/**
	 * 利率维护及新增 TDpAcActMod
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpAcAciModRs t24TDpAcActModRequest(TDpAcAciModRq request) throws Exception;
	/**
	 * 对公活期转定期 TDpAcctNewtdaAaaRs
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpAcctNewtdaAaaRs t24TDpAcctNewtdaAaa(TDpAcctNewtdaAaaRq request) throws Exception;
	
	
	/**
	 * ECIF查询接口 按组织机构代码证或客户号查询
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public C201OrgCusInqRs EcifC201OrgCustInqRequest(C201OrgCusInqRq request) throws Exception;
	/**
	 * 按证件号或客户号查询对公客户信息
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public C167CoCorpCusInqRs EcifC167CoCorpCusInqRequest(C167CoCorpCusInqRq request) throws Exception;
	/**
	 * 同业客户详细查询
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCustDetInqRs ecifTCustDetInqRequest(TCustDetInqRq request) throws Exception;
	
	
	/**
	 * 二代支付-普通贷记往账接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public UPPSCdtTrfRs T2ndPaymentUPPSCdtTrfRequest(UPPSCdtTrfRq request) throws Exception;

	/**
	 * 二代支付-自助渠道业务状态查询接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public UPPSigBusiQueryRs T2ndPaymentUPPSigBusiQueryRequest(UPPSigBusiQueryRq request) throws Exception;
	
	/**
	 * 二代支付-对账文件申请
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public UPPSChkFileAplRs T2ndPaymentUPPSChkFileAplRequest(UPPSChkFileAplRq request) throws Exception;
	
	
	/**
	 * 产品信息管理--产品组申请交易
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public CloudProApplyTransRs CloudProApplyTransRequest(CloudProApplyTransRq request) throws Exception;

	/**
	 * 产品信息管理-产品组同步交易
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public CloudProSynchTransRs CloudProSynchTransRequest(CloudProSynchTransRq request) throws Exception;

	/**
	 * 产品信息管理-产品组修改交易
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public CloudProModTransRs  CloudProModTransRequest(CloudProModTransRq request) throws Exception;
	
	/**
	 * CRMS-查询企信业务信息(同业->CRMS)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public EnterpriseInqRs CrmsEnterpriseInqRequest(EnterpriseInqRq request) throws Exception;
	
	/**
	 * CRMS-企信业务审批(同业->CRMS)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public EnterpriseApproveRs CrmsEnterpriseApproveRequest(EnterpriseApproveRq request) throws Exception;
	
	/**
	 * CRMS-查询融贷通业务信息(同业->CRMS)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public FinCreditInqRs CrmsFinCreditInqRequest(FinCreditInqRq request) throws Exception;
	
	/**
	 * CRMS-融贷通业务审批(同业->CRMS)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public FinCreditApproveRs CrmsFinCreditApproveRequest(FinCreditApproveRq request) throws Exception;
	
	/**
	 * CRMS-批复信息查询（同业->CRMS)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public ApprovalInqRs CrmsApprovalInqRequest(ApprovalInqRq request) throws Exception;
	
	/**
	 * IFBM-放款通知（CRMS->同业）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public LoanNoticeRs iFBMLoanNoticeResponse(LoanNoticeRq request) throws Exception;
	
	/**
	 * IFBM-提前还款通知（CRMS->系统）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public RepayNoticeRs iFBMRepayNoticeResponse(RepayNoticeRq request) throws Exception;
	
	/**
	 * IFBM-批复信息变更（CRMS->同业）
	 * @param requset
	 * @return
	 * @throws Exception
	 */
	public ApprovalChangeRs iFBMApprovalChangeResponse(ApprovalChangeRq requset) throws Exception;
	
	/**
	 * IFBM-额度切分同步（CRMS->同业系统）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public QuotaSyncopateRs iFBMQuotaSyncopateResponse(QuotaSyncopateRq request) throws Exception;
	/**
	 * 贷款信息查询
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TExLoanAllInqRs t24TExLoanAllInq(TExLoanAllInqRq request) throws Exception;
	/**
	 * T24还款计划查询
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TExLdRpmSchInqRs t24TExLdRpmSchInq(TExLdRpmSchInqRq request) throws Exception;
	
	/**
	 * 约定交易接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TDpIntBnkAaaRs t24TDpIntBnkAaaRequest(TDpIntBnkAaaRq request) throws Exception;
	/**
	 * 存款利率信息查询接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCoActIntInqRs t24TCoActIntInqRequest(TCoActIntInqRq request) throws Exception;
	
	/**
	 * 贷款利率信息查询接口
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public TCoLoansIntInqRs t24TCoLoansIntInqRequest(TCoLoansIntInqRq request) throws Exception;
	
}
