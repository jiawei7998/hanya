package com.singlee.capital.interfacex.qdb.job;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.qdb.ESB.controller.OrgUserSynchService;

public class AutoSynchOrgUser implements CronRunnable {
	
	private OrgUserSynchService orgUserService = SpringContextHolder.getBean("OrgUserSynchService");

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		orgUserService.snycMager();
		return false;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
}
