package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CommonRqHdr complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CommonRqHdr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SPName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RqUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NumTranCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClearDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TranDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TranTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DirectSendFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonRqHdr", propOrder = { "spName", "rqUID", "numTranCode",
		"clearDate", "tranDate", "tranTime", "directSendFlag", "channelId",
		"version", "cntId", "companyCode" })
public class CommonRqHdr {

	@XmlElement(name = "SPName", required = true)
	protected String spName;
	@XmlElement(name = "RqUID", required = true)
	protected String rqUID;
	@XmlElement(name = "NumTranCode", required = true)
	protected String numTranCode;
	@XmlElement(name = "ClearDate", required = true)
	protected String clearDate;
	@XmlElement(name = "TranDate", required = true)
	protected String tranDate;
	@XmlElement(name = "TranTime", required = true)
	protected String tranTime;
	@XmlElement(name = "DirectSendFlag", required = true)
	protected String directSendFlag;
	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "Version", required = true)
	protected String version;
	@XmlElement(name = "CntId", required = true)
	protected String cntId;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;

	/**
	 * Gets the value of the spName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPName() {
		return spName;
	}

	/**
	 * Sets the value of the spName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPName(String value) {
		this.spName = value;
	}

	/**
	 * Gets the value of the rqUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRqUID() {
		return rqUID;
	}

	/**
	 * Sets the value of the rqUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRqUID(String value) {
		this.rqUID = value;
	}

	/**
	 * Gets the value of the numTranCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNumTranCode() {
		return numTranCode;
	}

	/**
	 * Sets the value of the numTranCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNumTranCode(String value) {
		this.numTranCode = value;
	}

	/**
	 * Gets the value of the clearDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClearDate() {
		return clearDate;
	}

	/**
	 * Sets the value of the clearDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClearDate(String value) {
		this.clearDate = value;
	}

	/**
	 * Gets the value of the tranDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTranDate() {
		return tranDate;
	}

	/**
	 * Sets the value of the tranDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTranDate(String value) {
		this.tranDate = value;
	}

	/**
	 * Gets the value of the tranTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTranTime() {
		return tranTime;
	}

	/**
	 * Sets the value of the tranTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTranTime(String value) {
		this.tranTime = value;
	}

	/**
	 * Gets the value of the directSendFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDirectSendFlag() {
		return directSendFlag;
	}

	/**
	 * Sets the value of the directSendFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDirectSendFlag(String value) {
		this.directSendFlag = value;
	}

	/**
	 * Gets the value of the channelId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the value of the channelId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelId(String value) {
		this.channelId = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}

	/**
	 * Gets the value of the cntId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntId() {
		return cntId;
	}

	/**
	 * Sets the value of the cntId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntId(String value) {
		this.cntId = value;
	}

	/**
	 * Gets the value of the companyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the value of the companyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}

}
