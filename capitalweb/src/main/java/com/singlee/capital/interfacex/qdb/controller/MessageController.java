package com.singlee.capital.interfacex.qdb.controller;


import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.Message;
import com.singlee.capital.interfacex.qdb.service.MessageService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping("MessageController")
public class MessageController extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	@Resource
	private MessageService messageService;
	
	
	@RequestMapping("searchMessagePage")
	@ResponseBody
	public RetMsg<PageInfo<Message>> searchMessagePage(HttpServletRequest request,@RequestBody Map<String,Object> params) throws Exception{
		Page<Message> pages = messageService.searchMessagePage(params);
		return RetMsgHelper.ok(pages);
	}
	
	@RequestMapping("updateMessageFlag")
	@ResponseBody
	public RetMsg<Serializable> updateMessageFlag(HttpServletRequest request,@RequestBody Map<String,Object> params) throws Exception{
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		try {
			if(messageService.updateMessageFlag(params)){
				ret.setDesc("999");
				ret.setDetail("成功");
			}else{
				ret.setDesc("100");
				ret.setDetail("更新失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
