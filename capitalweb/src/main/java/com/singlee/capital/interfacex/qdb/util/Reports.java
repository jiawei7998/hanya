package com.singlee.capital.interfacex.qdb.util;

public enum Reports {
	
	ReportTrade("ReportTrade.xls","非标资产交易台账"),

	ReportInterBank("ReportInterBank.xls","存放同业、同业借款报表"),
	
	ReportSec("ReportSec.xls","债券台账明细"),
	
	ReportEntrustSec("ReportEntrustSec.xls","委外投资债券台账"),
	
	ReportEntrust("ReportEntrust.xls","委外业务统计表"),
	
	ReportDeValInterBank("ReportDeValInterBank.xls","存放同业(定期)、拆出资金、买入返售(债券)减值基础数据表"),
	
	ReportDeValTrade("ReportDeValTrade.xls","非债业务减值基础数据表"),
	
	ReportValTrade("ReportValTrade.xls","非债业务估值基础数据表"),
	
	FeesBroker("中介费详情表.xls","中介费详情表"),
	TradIvMain("tradIvMain.xls","委外业务模板"),
	CustNews("custNews.xls","舆情信息模板"),
	
	BassAsset("bassAsset.xls","基础资产模板");
	
	private String fileName;
	private String desc;
	
	private Reports(String fileName,String desc)
	{
		this.fileName = fileName;
		this.desc = desc;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static String getReportModel(Reports reports){
		return PathUtils.getAbsPath("standard/InterBank/mini_report/reportModel/") + reports.fileName;
	}

	public static Reports getReportsByFileName(String fileName){
		for(Reports report:Reports.class.getEnumConstants()){
			if(report.getFileName().equals(fileName)){
				return report;
			}
		}
		return null;
	}
}
