package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;
import java.math.BigDecimal;

/****
 * 
 * 日终账务
 * @author singlee
 *
 */
public class Posting implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String entryId;//分录id 
	private String taskId;//任务id 
	private String postDate;//分录日期  
	private String flowId;//流水号    
	private String subjCode;//科目码   
	private String subjName;//科目名称    
	private String innerAcctSn;//内部帐序号
	private String bkkpgOrgid;//记账机构
	private String debitCreditFlag;//借贷标识  
	private String partyCashAcctId;//对手方资金账号
	private BigDecimal value;//发生额 
	private String ccy;//币种 
	private String state;//状态 
	private String updateTime;//更新时间
	private String coreAcctCode;//核心账号   
	private String enforceDc;//
	private String dealNo;//交易编号
	private String sponInst;//交易机构 
	private String fundCode;//基金代码
	private String flowSeq;//分录序号
	private String eventId;//会计事件

	private String count;//条数
	private BigDecimal totalamount;//总金额
	public String getEntryId() {
		return entryId;
	}
	public String getTaskId() {
		return taskId;
	}
	public String getPostDate() {
		return postDate;
	}
	public String getFlowId() {
		return flowId;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public String getSubjName() {
		return subjName;
	}
	public String getInnerAcctSn() {
		return innerAcctSn;
	}
	public String getBkkpgOrgid() {
		return bkkpgOrgid;
	}
	public String getDebitCreditFlag() {
		return debitCreditFlag;
	}
	public String getPartyCashAcctId() {
		return partyCashAcctId;
	}
	public BigDecimal getValue() {
		return value;
	}
	public String getCcy() {
		return ccy;
	}
	public String getState() {
		return state;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public String getCoreAcctCode() {
		return coreAcctCode;
	}
	public String getEnforceDc() {
		return enforceDc;
	}
	public String getDealNo() {
		return dealNo;
	}
	public String getSponInst() {
		return sponInst;
	}
	public String getFundCode() {
		return fundCode;
	}
	public String getFlowSeq() {
		return flowSeq;
	}
	public String getEventId() {
		return eventId;
	}
	public String getCount() {
		return count;
	}
	public BigDecimal getTotalamount() {
		return totalamount;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public void setInnerAcctSn(String innerAcctSn) {
		this.innerAcctSn = innerAcctSn;
	}
	public void setBkkpgOrgid(String bkkpgOrgid) {
		this.bkkpgOrgid = bkkpgOrgid;
	}
	public void setDebitCreditFlag(String debitCreditFlag) {
		this.debitCreditFlag = debitCreditFlag;
	}
	public void setPartyCashAcctId(String partyCashAcctId) {
		this.partyCashAcctId = partyCashAcctId;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public void setCoreAcctCode(String coreAcctCode) {
		this.coreAcctCode = coreAcctCode;
	}
	public void setEnforceDc(String enforceDc) {
		this.enforceDc = enforceDc;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public void setFlowSeq(String flowSeq) {
		this.flowSeq = flowSeq;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public void setTotalamount(BigDecimal totalamount) {
		this.totalamount = totalamount;
	}
	
	
}
