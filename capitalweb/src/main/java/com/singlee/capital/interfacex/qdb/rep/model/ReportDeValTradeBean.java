package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

/***
 * 1.金市非债业务减值基础数据表
 * @author lee
 *
 */
public class ReportDeValTradeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String instName        ;  //分行/支行名称         
	private String prodBigType     ;  //产品大类              
	private String prodType        ;  //产品细类              
	private String prodNo          ;  //产品代码              
	private double rate            ;  //债项的利率            
	private String vDate           ;  //债项的起息日          
	private String mDate           ;  //债项的到期日          
	private double intAmt          ;  //债项的应计利息        
	private double amt             ;  //资产负债表日账面金额  
	private double settAmt         ;  //资产负债表日摊余成本  
	private String intType         ;  //该债项的付息方式      
	private String innerLevel      ;  //内部评级              
	private String shortProdNo     ;  //产品代码缩写          
	private String baseAssetMdate  ;  //底层债券到期日        
	
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getProdBigType() {
		return prodBigType;
	}
	public void setProdBigType(String prodBigType) {
		this.prodBigType = prodBigType;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getProdNo() {
		return prodNo;
	}
	public void setProdNo(String prodNo) {
		this.prodNo = prodNo;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public double getIntAmt() {
		return intAmt;
	}
	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getSettAmt() {
		return settAmt;
	}
	public void setSettAmt(double settAmt) {
		this.settAmt = settAmt;
	}
	public String getIntType() {
		return intType;
	}
	public void setIntType(String intType) {
		this.intType = intType;
	}
	public String getInnerLevel() {
		return innerLevel;
	}
	public void setInnerLevel(String innerLevel) {
		this.innerLevel = innerLevel;
	}
	public String getShortProdNo() {
		return shortProdNo;
	}
	public void setShortProdNo(String shortProdNo) {
		this.shortProdNo = shortProdNo;
	}
	public String getBaseAssetMdate() {
		return baseAssetMdate;
	}
	public void setBaseAssetMdate(String baseAssetMdate) {
		this.baseAssetMdate = baseAssetMdate;
	}

	
	
	
}
