 package com.singlee.capital.interfacex.qdb.ESB.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.qdb.ESB.model.BC5001ReqtBean;
import com.singlee.capital.interfacex.qdb.ESB.service.BC5001;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/*import cn.com.singlee.ESB.model.SIBS.SM5309ReqtBean;
import cn.com.singlee.ESB.servlet.SM5309;
import cn.com.singlee.util.DataCache;*/

/**
 * 核心接口查询客户号和客户名称
 * @author LI YS
 *
 */
@Controller
@RequestMapping(value = "/CIFController")
public class CIFController {
	@Autowired
	private BC5001 bc5001service;
	
	/**
	 * BC5001用户列表显示,通过客户名模糊查询
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/toCifList")
	@ResponseBody
	public RetMsg<PageInfo<BC5001ReqtBean>> findCifList(HttpServletRequest request, @RequestBody Map<String,String> map) {
		BC5001ReqtBean bc5001 = new BC5001ReqtBean();
		List<BC5001ReqtBean> list = new ArrayList<BC5001ReqtBean>();
		if((map.get("custName")!=null) && (!"".equals(map.get("custName")))){

			String partyName = map.get("custName");
			bc5001.setEmpName(partyName);
			bc5001.setSel("P");
			bc5001.setSelOpt("E");
			bc5001.setSearchKeys("");
			bc5001.setCifNo("");
			if("1".equals(map.get("pageNumber"))){
				bc5001.setMoreRecInd("N");	
			}else{
				bc5001.setMoreRecInd("Y");	
				bc5001.setNbrOfRecToRetrv(String.valueOf(10*(Integer.valueOf(map.get("pageNumber"))-1)));
			}

			SlSession __slSession = SessionStaticUtil.getSlSessionByHttp(request);
			// 当前session登陆用户
			TaUser user = SlSessionHelper.getUser(__slSession) == null ? new TaUser()
					: SlSessionHelper.getUser(__slSession);

			HashMap<String, Object> bc5001Map = bc5001service.sendTR1003ReqtMsgTOTis(bc5001, user,map.get("pageNumber"));
			list = (List<BC5001ReqtBean>) bc5001Map.get("List");	
			
			if("error.system.0002".equals(bc5001Map.get("code").toString().trim())) {
				RetMsg<PageInfo<BC5001ReqtBean>> retMsg = new RetMsg<PageInfo<BC5001ReqtBean>>(bc5001Map.get("code").toString(),bc5001Map.get("respMsg").toString(),null,null);
				return retMsg;
			}
			return RetMsgHelper.ok(getPage(list,map));
		}
		
		return RetMsgHelper.ok(getPage(list,map));
	}
	public Page<BC5001ReqtBean> getPage(List<BC5001ReqtBean> list, Map<String,String> map){
		Page<BC5001ReqtBean> pages=new Page<BC5001ReqtBean>();
		try {
			pages.addAll(list);
			if(list!=null&&list.size()>0){
				pages.setCount(true);
				pages.setPageNum(Integer.valueOf(map.get("pageNumber")));
				pages.setPageSize(Integer.valueOf(map.get("pageSize")));
				pages.setStartRow(0);
				pages.setEndRow(10);
				pages.setTotal(1000);
				pages.setPages(100);
				if(list.size()<10){
					pages.setPages(Integer.valueOf(map.get("pageNumber")));
					pages.setTotal(10*(Integer.valueOf(map.get("pageNumber"))-1)+list.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("核心未查询的客户");
		}
		return pages;
	}
	/**
	 * 通过客户号精准查询
	 * @param model
	 * @param request
	 * @param response
	 * @param bc5001
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/toCifNbr")
	public RetMsg<List<BC5001ReqtBean>> toCifNbr(HttpServletRequest request,
			@RequestBody Map<String, String> map) {

		List<BC5001ReqtBean> list = new ArrayList<BC5001ReqtBean>();
		if (map.get("cifNo") != null && !"".equals(map.get("cifNo"))) {
			BC5001ReqtBean bc5001 = new BC5001ReqtBean();
			bc5001.setCifNo(map.get("cifNo"));
			bc5001.setMoreRecInd("N");
			bc5001.setSel("E");
			bc5001.setSelOpt("A");

			SlSession __slSession = SessionStaticUtil.getSlSessionByHttp(request);
			// 当前session登陆用户
			TaUser user = SlSessionHelper.getUser(__slSession) == null ? new TaUser() : SlSessionHelper.getUser(__slSession);

			// 发送报文,得到核心系统返回报文经过解析的数据
			HashMap<String, Object> bc5001Map = bc5001service.sendTR1003ReqtMsgTOTis(bc5001, user,"1");

			list = (List<BC5001ReqtBean>) bc5001Map.get("List");
			if("error.system.0002".equals(bc5001Map.get("code").toString().trim())) {
				RetMsg<List<BC5001ReqtBean>> retMsg = new RetMsg<List<BC5001ReqtBean>>(bc5001Map.get("code").toString(),bc5001Map.get("respMsg").toString(),null,null);
				return retMsg;
			}
			return RetMsgHelper.ok(list);
		}
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 账号查询
	 * @param model
	 * @param request
	 * @param response
	 * @param sm5309
	 * @return
	 */
	/*@RequestMapping("/toAcctNbr")
	@ResponseBody
	public RetMsg<List<BC5001ReqtBean>> queryAcctNbr(HttpServletRequest request, @RequestBody Map<String,String> map) {
		// 前台出过来的区别客户号与名称查询还是精确查询
		SM5309ReqtBean sm5309 = new SM5309ReqtBean();
		
		SlSession __slSession = SessionStaticUtil.getSlSessionByHttp(request);
		//当前session登陆用户
		TaUser user = SlSessionHelper.getUser(__slSession)==null?new TaUser():SlSessionHelper.getUser(__slSession);
		
		String nbrCount = map.get("nbrCount");
		if (nbrCount.equals("searchNbr")) {
			String blurAcctNumber = request.getParameter("blurAcctNumber");
			SM5309ReqtBean bean = new SM5309ReqtBean();
			bean.setAcctNbr(blurAcctNumber);
			HashMap<String, Object> sm5309map = sm5309Service.sendTR1003ReqtMsgTOTis(bean,user);
			List<SM5309ReqtBean> list = (List<SM5309ReqtBean>) sm5309map.get("List");
			if (!list.get(0).getCifNo().equals("")) {
				BC5001ReqtBean bc5001 = new BC5001ReqtBean();
				bc5001.setCifNo(list.get(0).getCifNo());
				bc5001.setSel("E");
				bc5001.setSelOpt("A");
				HashMap<String, Object> bc5001Map = bc5001service.sendTR1003ReqtMsgTOTis(bc5001,user);
				List<BC5001ReqtBean> bc5001list = (List<BC5001ReqtBean>) bc5001Map.get("List");
				list.get(0).setCifNm(bc5001list.get(0).getCustName());
				return RetMsgHelper.ok(bc5001list);
			}
		} else if (nbrCount.equals("byCif")) {
			String cifNbr = request.getParameter("blurCifNbr");
			String pageKeys = request.getParameter("pageKey");
			String searchkeys = request.getParameter("searchKey");
			String key = request.getParameter("key");
			if(key==null){
				if(pageKeys==null){
					sm5309.setMoreRecInd("N");
					pageKeys = "1";
				}else{
					sm5309.setMoreRecInd("Y");
				}
				sm5309.setSearchKeys(searchkeys);
				sm5309.setCifNo(cifNbr);
				
				HashMap<String, Object> sm5309Map = sm5309Service
						.sendTR1003ReqtMsgTOTis(sm5309,user);
				List<SM5309ReqtBean> sm5309list = (List<SM5309ReqtBean>) sm5309Map.get("List");
				for (int i = 0; i < sm5309list.size(); i++) {
					sm5309list.get(i).setCifNo(cifNbr);
				}
				for (SM5309ReqtBean bean : sm5309list) {
					bean.setPageKey(pageKeys);
				}
				//return RetMsgHelper.ok(sm5309list);
			
			}
			
		}
		return null;
		
	}*/
}
