package com.singlee.capital.interfacex.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ApprovalInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ApprovalInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="PartyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrgNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UserNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApproveId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OriBatchNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountProperty" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanCreditAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lyed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanApplyed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tenor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExtenTermUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SameCustName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValidDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateReceiveType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FinalApprvResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SiteNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAssuKind" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApprovalInqRs", propOrder = { "commonRsHdr", "approvalInqRec" })
public class ApprovalInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "ApprovalInqRec", required = true)
	protected List<ApprovalInqRec> approvalInqRec; 
	
	
	public List<ApprovalInqRec> getApprovalInqRec() {
		return approvalInqRec;
	}
	public void setApprovalInqRec(List<ApprovalInqRec> approvalInqRec) {
		this.approvalInqRec = approvalInqRec;
	}

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}


}
