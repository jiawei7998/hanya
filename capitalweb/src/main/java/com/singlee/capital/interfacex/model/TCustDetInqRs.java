package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCustDetInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCustDetInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="CustmerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CNName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BaAccBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BaAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxerID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IDType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IDNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxerIden" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Stdcollflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BookingFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillPrd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SfCptyCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TyCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CnStreet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Gbstreet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Industry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LiveProvice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MgrId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TCDInqType_Rec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TCDInqType_Rec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCustDetInqRs", propOrder = { "commonRsHdr", "custmerCode",
		"cnName", "gbName", "custType", "baAccBank", "baAccount", "taxerID",
		"cfoName", "cfoId", "cfoIdType", "cfoPhoneNum", "idType", "idNumber",
		"taxerIden", "stdcollflag", "bookingFreq", "billPrd", "sfCptyCountry",
		"corpIdType", "tyCustId", "cnStreet", "gbstreet", "industry",
		"liveProvice", "mgrId", "percent", "tcdInqTypeRec" })
public class TCustDetInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "CustmerCode", required = true)
	protected String custmerCode;
	@XmlElement(name = "CNName", required = true)
	protected String cnName;
	@XmlElement(name = "GBName", required = true)
	protected String gbName;
	@XmlElement(name = "CustType", required = true)
	protected String custType;
	@XmlElement(name = "BaAccBank", required = true)
	protected String baAccBank;
	@XmlElement(name = "BaAccount", required = true)
	protected String baAccount;
	@XmlElement(name = "TaxerID", required = true)
	protected String taxerID;
	@XmlElement(name = "CFOName", required = true)
	protected String cfoName;
	@XmlElement(name = "CFOId", required = true)
	protected String cfoId;
	@XmlElement(name = "CFOIdType", required = true)
	protected String cfoIdType;
	@XmlElement(name = "CFOPhoneNum", required = true)
	protected String cfoPhoneNum;
	@XmlElement(name = "IDType", required = true)
	protected String idType;
	@XmlElement(name = "IDNumber", required = true)
	protected String idNumber;
	@XmlElement(name = "TaxerIden", required = true)
	protected String taxerIden;
	@XmlElement(name = "Stdcollflag", required = true)
	protected String stdcollflag;
	@XmlElement(name = "BookingFreq", required = true)
	protected String bookingFreq;
	@XmlElement(name = "BillPrd", required = true)
	protected String billPrd;
	@XmlElement(name = "SfCptyCountry", required = true)
	protected String sfCptyCountry;
	@XmlElement(name = "CorpIdType", required = true)
	protected String corpIdType;
	@XmlElement(name = "TyCustId", required = true)
	protected String tyCustId;
	@XmlElement(name = "CnStreet", required = true)
	protected String cnStreet;
	@XmlElement(name = "Gbstreet", required = true)
	protected String gbstreet;
	@XmlElement(name = "Industry", required = true)
	protected String industry;
	@XmlElement(name = "LiveProvice", required = true)
	protected String liveProvice;
	@XmlElement(name = "MgrId", required = true)
	protected String mgrId;
	@XmlElement(name = "Percent", required = true)
	protected String percent;
	@XmlElement(name = "TCDInqType_Rec")
	protected List<TCDInqTypeRec> tcdInqTypeRec;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the custmerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustmerCode() {
		return custmerCode;
	}

	/**
	 * Sets the value of the custmerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustmerCode(String value) {
		this.custmerCode = value;
	}

	/**
	 * Gets the value of the cnName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCNName() {
		return cnName;
	}

	/**
	 * Sets the value of the cnName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCNName(String value) {
		this.cnName = value;
	}

	/**
	 * Gets the value of the gbName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBName() {
		return gbName;
	}

	/**
	 * Sets the value of the gbName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBName(String value) {
		this.gbName = value;
	}

	/**
	 * Gets the value of the custType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * Sets the value of the custType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustType(String value) {
		this.custType = value;
	}

	/**
	 * Gets the value of the baAccBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBaAccBank() {
		return baAccBank;
	}

	/**
	 * Sets the value of the baAccBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBaAccBank(String value) {
		this.baAccBank = value;
	}

	/**
	 * Gets the value of the baAccount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBaAccount() {
		return baAccount;
	}

	/**
	 * Sets the value of the baAccount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBaAccount(String value) {
		this.baAccount = value;
	}

	/**
	 * Gets the value of the taxerID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxerID() {
		return taxerID;
	}

	/**
	 * Sets the value of the taxerID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxerID(String value) {
		this.taxerID = value;
	}

	/**
	 * Gets the value of the cfoName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOName() {
		return cfoName;
	}

	/**
	 * Sets the value of the cfoName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOName(String value) {
		this.cfoName = value;
	}

	/**
	 * Gets the value of the cfoId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOId() {
		return cfoId;
	}

	/**
	 * Sets the value of the cfoId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOId(String value) {
		this.cfoId = value;
	}

	/**
	 * Gets the value of the cfoIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOIdType() {
		return cfoIdType;
	}

	/**
	 * Sets the value of the cfoIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOIdType(String value) {
		this.cfoIdType = value;
	}

	/**
	 * Gets the value of the cfoPhoneNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOPhoneNum() {
		return cfoPhoneNum;
	}

	/**
	 * Sets the value of the cfoPhoneNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOPhoneNum(String value) {
		this.cfoPhoneNum = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDType(String value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDNumber() {
		return idNumber;
	}

	/**
	 * Sets the value of the idNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDNumber(String value) {
		this.idNumber = value;
	}

	/**
	 * Gets the value of the taxerIden property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxerIden() {
		return taxerIden;
	}

	/**
	 * Sets the value of the taxerIden property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxerIden(String value) {
		this.taxerIden = value;
	}

	/**
	 * Gets the value of the stdcollflag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStdcollflag() {
		return stdcollflag;
	}

	/**
	 * Sets the value of the stdcollflag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStdcollflag(String value) {
		this.stdcollflag = value;
	}

	/**
	 * Gets the value of the bookingFreq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBookingFreq() {
		return bookingFreq;
	}

	/**
	 * Sets the value of the bookingFreq property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBookingFreq(String value) {
		this.bookingFreq = value;
	}

	/**
	 * Gets the value of the billPrd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillPrd() {
		return billPrd;
	}

	/**
	 * Sets the value of the billPrd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillPrd(String value) {
		this.billPrd = value;
	}

	/**
	 * Gets the value of the sfCptyCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSfCptyCountry() {
		return sfCptyCountry;
	}

	/**
	 * Sets the value of the sfCptyCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSfCptyCountry(String value) {
		this.sfCptyCountry = value;
	}

	/**
	 * Gets the value of the corpIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpIdType() {
		return corpIdType;
	}

	/**
	 * Sets the value of the corpIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpIdType(String value) {
		this.corpIdType = value;
	}

	/**
	 * Gets the value of the tyCustId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTyCustId() {
		return tyCustId;
	}

	/**
	 * Sets the value of the tyCustId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTyCustId(String value) {
		this.tyCustId = value;
	}

	/**
	 * Gets the value of the cnStreet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCnStreet() {
		return cnStreet;
	}

	/**
	 * Sets the value of the cnStreet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCnStreet(String value) {
		this.cnStreet = value;
	}

	/**
	 * Gets the value of the gbstreet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGbstreet() {
		return gbstreet;
	}

	/**
	 * Sets the value of the gbstreet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGbstreet(String value) {
		this.gbstreet = value;
	}

	/**
	 * Gets the value of the industry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * Sets the value of the industry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndustry(String value) {
		this.industry = value;
	}

	/**
	 * Gets the value of the liveProvice property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLiveProvice() {
		return liveProvice;
	}

	/**
	 * Sets the value of the liveProvice property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLiveProvice(String value) {
		this.liveProvice = value;
	}

	/**
	 * Gets the value of the mgrId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMgrId() {
		return mgrId;
	}

	/**
	 * Sets the value of the mgrId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMgrId(String value) {
		this.mgrId = value;
	}

	/**
	 * Gets the value of the percent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPercent() {
		return percent;
	}

	/**
	 * Sets the value of the percent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPercent(String value) {
		this.percent = value;
	}

	/**
	 * Gets the value of the tcdInqTypeRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the tcdInqTypeRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTCDInqTypeRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TCDInqTypeRec }
	 * 
	 * 
	 */
	public List<TCDInqTypeRec> getTCDInqTypeRec() {
		if (tcdInqTypeRec == null) {
			tcdInqTypeRec = new ArrayList<TCDInqTypeRec>();
		}
		return this.tcdInqTypeRec;
	}

}
