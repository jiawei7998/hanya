package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;



public class XmlFormat {

	public static String format(String str) throws Exception {
		StringWriter out = null;
		XMLWriter writer = null;
		StringBuffer xmlBuffer = new StringBuffer();
		StringReader in = null;
		try{
			SAXReader reader = new SAXReader();
			
			in = new StringReader(str);
	
			Document doc = reader.read(in);
	
			OutputFormat formater = OutputFormat.createPrettyPrint();
	
			formater.setEncoding("utf-8");
			
			out = new StringWriter();
	
			writer = new XMLWriter(out, formater);
	
			writer.write(doc);
	
			writer.close();
			
			xmlBuffer.append(out.toString());
			
			out.close();
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}finally{
			if(writer != null)
			{
				writer.close();
				writer = null;
			}
			if(out != null)
			{
				out.close();
				out = null;
			}
			if(in != null)
			{
				in.close();
				in = null;
			}
		}
		
			
			return xmlBuffer.toString();

	}
	public static void getElementList(Element element,HashMap<Integer, FileXmlBean> resultHashMap)
	{
		@SuppressWarnings("unchecked")
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			FileXmlBean acupXml = new FileXmlBean();
			String no = StringUtils.trimToEmpty(element.attributeValue("No"));
			String fieldTag = StringUtils.trimToEmpty(element.attributeValue("FieldTag"));
			String position = StringUtils.trimToEmpty(element.attributeValue("Position"));
			String dataType = StringUtils.trimToEmpty(element.attributeValue("DataType"));
			String length = StringUtils.trimToEmpty(element.attributeValue("length"));
			String mod = StringUtils.trimToEmpty(element.attributeValue("M-O-D"));
			String leftOrRight = StringUtils.trimToEmpty(element.attributeValue("LeftOrRight"));
			String fillChar = element.attributeValue("FillChar");
			String value = StringUtils.trimToEmpty(element.attributeValue("value"));
			String description = StringUtils.trimToEmpty(element.attributeValue("Description"));
			String decimalsNo = StringUtils.trimToEmpty(element.attributeValue("DecimalsNo"));
			String num=StringUtils.trimToEmpty(element.attributeValue("num"));
			acupXml.setNo(Integer.parseInt(no));
			acupXml.setFieldTag(fieldTag);
			acupXml.setPosition(Integer.parseInt(position));
			acupXml.setDataType(dataType);
			acupXml.setLength(Integer.parseInt(length));
			acupXml.setMod(mod);
			acupXml.setLeftOrRight(leftOrRight);
			acupXml.setFillChar(fillChar);
			acupXml.setValue(value);
			acupXml.setDescription(description);
			acupXml.setDecimalsNo(Integer.parseInt(decimalsNo));
			acupXml.setNum(Integer.valueOf(num));
			resultHashMap.put(acupXml.getNum(), acupXml);
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				getElementList(element2,resultHashMap);
			}
		}
	}

}
