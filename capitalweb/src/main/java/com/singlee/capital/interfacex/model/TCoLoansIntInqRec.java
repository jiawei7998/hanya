package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCoLoansIntInqRec", propOrder = { "loanRate","currentRate","payPeriod","interestDate" })
public class TCoLoansIntInqRec {

	@XmlElement(name = "LoanRate", required = true)
	protected String loanRate;
	@XmlElement(name = "CurrentRate", required = true)
	protected String currentRate;
	@XmlElement(name = "PayPeriod", required = true)
	protected String payPeriod;
	@XmlElement(name = "InterestDate", required = true)
	protected String interestDate;


	/**
	 * Gets the value of the loanRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanRate() {
		return loanRate;
	}

	/**
	 * Sets the value of the loanRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanRate(String value) {
		this.loanRate = value;
	}

	/**
	 * Gets the value of the currentRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrentRate() {
		return currentRate;
	}

	/**
	 * Sets the value of the currentRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrentRate(String value) {
		this.currentRate = value;
	}

	/**
	 * Gets the value of the payPeriod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayPeriod() {
		return payPeriod;
	}

	/**
	 * Sets the value of the payPeriod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayPeriod(String value) {
		this.payPeriod = value;
	}

	/**
	 * Gets the value of the interestDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInterestDate() {
		return interestDate;
	}

	/**
	 * Sets the value of the interestDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInterestDate(String value) {
		this.interestDate = value;
	}
}
