package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExLoanAllInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExLoanAllInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="T24LoanAgreeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="T24DrawDownNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="T24DateDue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntRateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestSpread" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotRpyAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotInterestAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotPdIntAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrinLiqAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExLoanAllInqRec", propOrder = { "t24LoanAgreeNo",
		"t24DrawDownNo", "currency", "valueDate", "t24DateDue", "amt",
		"acctNo", "balance", "intRateType", "interestKey", "interestSpread",
		"rate", "amount", "coCode", "totRpyAmt", "totInterestAmt",
		"totPdIntAmt", "prinLiqAcct" })
public class TExLoanAllInqRec {

	@XmlElement(name = "T24LoanAgreeNo", required = true)
	protected String t24LoanAgreeNo;
	@XmlElement(name = "T24DrawDownNo", required = true)
	protected String t24DrawDownNo;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "ValueDate", required = true)
	protected String valueDate;
	@XmlElement(name = "T24DateDue", required = true)
	protected String t24DateDue;
	@XmlElement(name = "Amt", required = true)
	protected String amt;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "Balance", required = true)
	protected String balance;
	@XmlElement(name = "IntRateType", required = true)
	protected String intRateType;
	@XmlElement(name = "InterestKey", required = true)
	protected String interestKey;
	@XmlElement(name = "InterestSpread", required = true)
	protected String interestSpread;
	@XmlElement(name = "Rate", required = true)
	protected String rate;
	@XmlElement(name = "Amount", required = true)
	protected String amount;
	@XmlElement(name = "CoCode", required = true)
	protected String coCode;
	@XmlElement(name = "TotRpyAmt", required = true)
	protected String totRpyAmt;
	@XmlElement(name = "TotInterestAmt", required = true)
	protected String totInterestAmt;
	@XmlElement(name = "TotPdIntAmt", required = true)
	protected String totPdIntAmt;
	@XmlElement(name = "PrinLiqAcct", required = true)
	protected String prinLiqAcct;

	/**
	 * Gets the value of the t24LoanAgreeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getT24LoanAgreeNo() {
		return t24LoanAgreeNo;
	}

	/**
	 * Sets the value of the t24LoanAgreeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setT24LoanAgreeNo(String value) {
		this.t24LoanAgreeNo = value;
	}

	/**
	 * Gets the value of the t24DrawDownNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getT24DrawDownNo() {
		return t24DrawDownNo;
	}

	/**
	 * Sets the value of the t24DrawDownNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setT24DrawDownNo(String value) {
		this.t24DrawDownNo = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the valueDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValueDate() {
		return valueDate;
	}

	/**
	 * Sets the value of the valueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValueDate(String value) {
		this.valueDate = value;
	}

	/**
	 * Gets the value of the t24DateDue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getT24DateDue() {
		return t24DateDue;
	}

	/**
	 * Sets the value of the t24DateDue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setT24DateDue(String value) {
		this.t24DateDue = value;
	}

	/**
	 * Gets the value of the amt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmt() {
		return amt;
	}

	/**
	 * Sets the value of the amt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmt(String value) {
		this.amt = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the balance property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * Sets the value of the balance property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBalance(String value) {
		this.balance = value;
	}

	/**
	 * Gets the value of the intRateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntRateType() {
		return intRateType;
	}

	/**
	 * Sets the value of the intRateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntRateType(String value) {
		this.intRateType = value;
	}

	/**
	 * Gets the value of the interestKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInterestKey() {
		return interestKey;
	}

	/**
	 * Sets the value of the interestKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInterestKey(String value) {
		this.interestKey = value;
	}

	/**
	 * Gets the value of the interestSpread property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInterestSpread() {
		return interestSpread;
	}

	/**
	 * Sets the value of the interestSpread property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInterestSpread(String value) {
		this.interestSpread = value;
	}

	/**
	 * Gets the value of the rate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * Sets the value of the rate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRate(String value) {
		this.rate = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the coCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoCode() {
		return coCode;
	}

	/**
	 * Sets the value of the coCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoCode(String value) {
		this.coCode = value;
	}

	/**
	 * Gets the value of the totRpyAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotRpyAmt() {
		return totRpyAmt;
	}

	/**
	 * Sets the value of the totRpyAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotRpyAmt(String value) {
		this.totRpyAmt = value;
	}

	/**
	 * Gets the value of the totInterestAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotInterestAmt() {
		return totInterestAmt;
	}

	/**
	 * Sets the value of the totInterestAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotInterestAmt(String value) {
		this.totInterestAmt = value;
	}

	/**
	 * Gets the value of the totPdIntAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotPdIntAmt() {
		return totPdIntAmt;
	}

	/**
	 * Sets the value of the totPdIntAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotPdIntAmt(String value) {
		this.totPdIntAmt = value;
	}

	/**
	 * Gets the value of the prinLiqAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrinLiqAcct() {
		return prinLiqAcct;
	}

	/**
	 * Sets the value of the prinLiqAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrinLiqAcct(String value) {
		this.prinLiqAcct = value;
	}

}
