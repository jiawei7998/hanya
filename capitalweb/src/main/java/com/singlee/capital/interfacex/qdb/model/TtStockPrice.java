package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 股票价格
 * @author 旺旺
 *
 */
@Entity
@Table(name="TT_MKT_STOCK_PRICE")
public class TtStockPrice implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String importDate;
	
	private String stockId;
	
	private String stockName;
	
	private Double price;
	
	private Double price_1;
	
	private String lstModifyDate;
	
	private String remark1;
	
	private String remark2;

	public String getImportDate() {
		return importDate;
	}

	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPrice_1() {
		return price_1;
	}

	public void setPrice_1(Double price_1) {
		this.price_1 = price_1;
	}

	public String getLstModifyDate() {
		return lstModifyDate;
	}

	public void setLstModifyDate(String lstModifyDate) {
		this.lstModifyDate = lstModifyDate;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
	
	
}
