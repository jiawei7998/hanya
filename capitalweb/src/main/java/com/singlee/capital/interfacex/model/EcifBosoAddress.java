package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="BOS_O_ADDRESS")
public class EcifBosoAddress implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String address_id             	;
	private String ecif_id                  ;
	private String addr_usgae_tp_cd         ;
	private String effective_flag           ;
	private String preferred_flag           ;
	private String short_desc               ;
	private String continent_tp_cd          ;
	private String country_area_tp_cd       ;
	private String admin_area_tp_cd         ;
	private String city_tp_cd               ;
	private String county_admin_tp_cd       ;
	private String address                  ;
	private String province_tp_cd           ;
	private String county_area_tp_cd        ;
	private String town_tp_cd               ;
	private String street_name              ;
	private String postal_code              ;
	private String town_desc                ;
	private String bos_addr_tp_cd           ;
	private String bos_addr_channel_tp_cd   ;
	private String address_app_system       ;
	private String contact_person           ;
	private String source_sys_tp_cd         ;
	private String last_update_dt           ;
	private String last_update_tx_id        ;
	private String last_update_user         ;
	private String tel_area_code            ;
	private String oa_reserved_1            ;
	private String oa_reserved_2            ;
	private String oa_reserved_3            ;
	private String oa_reserved_4            ;
	private String oa_reserved_5            ;
	private String telephone                ;
	private String fax                      ;
	private String busi_sys_tp_cd           ;
	public String getAddress_id() {
		return address_id;
	}
	public void setAddress_id(String addressId) {
		address_id = addressId;
	}
	public String getEcif_id() {
		return ecif_id;
	}
	public void setEcif_id(String ecifId) {
		ecif_id = ecifId;
	}
	public String getAddr_usgae_tp_cd() {
		return addr_usgae_tp_cd;
	}
	public void setAddr_usgae_tp_cd(String addrUsgaeTpCd) {
		addr_usgae_tp_cd = addrUsgaeTpCd;
	}
	public String getEffective_flag() {
		return effective_flag;
	}
	public void setEffective_flag(String effectiveFlag) {
		effective_flag = effectiveFlag;
	}
	public String getPreferred_flag() {
		return preferred_flag;
	}
	public void setPreferred_flag(String preferredFlag) {
		preferred_flag = preferredFlag;
	}
	public String getShort_desc() {
		return short_desc;
	}
	public void setShort_desc(String shortDesc) {
		short_desc = shortDesc;
	}
	public String getContinent_tp_cd() {
		return continent_tp_cd;
	}
	public void setContinent_tp_cd(String continentTpCd) {
		continent_tp_cd = continentTpCd;
	}
	public String getCountry_area_tp_cd() {
		return country_area_tp_cd;
	}
	public void setCountry_area_tp_cd(String countryAreaTpCd) {
		country_area_tp_cd = countryAreaTpCd;
	}
	public String getAdmin_area_tp_cd() {
		return admin_area_tp_cd;
	}
	public void setAdmin_area_tp_cd(String adminAreaTpCd) {
		admin_area_tp_cd = adminAreaTpCd;
	}
	public String getCity_tp_cd() {
		return city_tp_cd;
	}
	public void setCity_tp_cd(String cityTpCd) {
		city_tp_cd = cityTpCd;
	}
	public String getCounty_admin_tp_cd() {
		return county_admin_tp_cd;
	}
	public void setCounty_admin_tp_cd(String countyAdminTpCd) {
		county_admin_tp_cd = countyAdminTpCd;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvince_tp_cd() {
		return province_tp_cd;
	}
	public void setProvince_tp_cd(String provinceTpCd) {
		province_tp_cd = provinceTpCd;
	}
	public String getCounty_area_tp_cd() {
		return county_area_tp_cd;
	}
	public void setCounty_area_tp_cd(String countyAreaTpCd) {
		county_area_tp_cd = countyAreaTpCd;
	}
	public String getTown_tp_cd() {
		return town_tp_cd;
	}
	public void setTown_tp_cd(String townTpCd) {
		town_tp_cd = townTpCd;
	}
	public String getStreet_name() {
		return street_name;
	}
	public void setStreet_name(String streetName) {
		street_name = streetName;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postalCode) {
		postal_code = postalCode;
	}
	public String getTown_desc() {
		return town_desc;
	}
	public void setTown_desc(String townDesc) {
		town_desc = townDesc;
	}
	public String getBos_addr_tp_cd() {
		return bos_addr_tp_cd;
	}
	public void setBos_addr_tp_cd(String bosAddrTpCd) {
		bos_addr_tp_cd = bosAddrTpCd;
	}
	public String getBos_addr_channel_tp_cd() {
		return bos_addr_channel_tp_cd;
	}
	public void setBos_addr_channel_tp_cd(String bosAddrChannelTpCd) {
		bos_addr_channel_tp_cd = bosAddrChannelTpCd;
	}
	public String getAddress_app_system() {
		return address_app_system;
	}
	public void setAddress_app_system(String addressAppSystem) {
		address_app_system = addressAppSystem;
	}
	public String getContact_person() {
		return contact_person;
	}
	public void setContact_person(String contactPerson) {
		contact_person = contactPerson;
	}
	public String getSource_sys_tp_cd() {
		return source_sys_tp_cd;
	}
	public void setSource_sys_tp_cd(String sourceSysTpCd) {
		source_sys_tp_cd = sourceSysTpCd;
	}
	public String getLast_update_dt() {
		return last_update_dt;
	}
	public void setLast_update_dt(String lastUpdateDt) {
		last_update_dt = lastUpdateDt;
	}
	public String getLast_update_tx_id() {
		return last_update_tx_id;
	}
	public void setLast_update_tx_id(String lastUpdateTxId) {
		last_update_tx_id = lastUpdateTxId;
	}
	public String getLast_update_user() {
		return last_update_user;
	}
	public void setLast_update_user(String lastUpdateUser) {
		last_update_user = lastUpdateUser;
	}
	public String getTel_area_code() {
		return tel_area_code;
	}
	public void setTel_area_code(String telAreaCode) {
		tel_area_code = telAreaCode;
	}
	public String getOa_reserved_1() {
		return oa_reserved_1;
	}
	public void setOa_reserved_1(String oaReserved_1) {
		oa_reserved_1 = oaReserved_1;
	}
	public String getOa_reserved_2() {
		return oa_reserved_2;
	}
	public void setOa_reserved_2(String oaReserved_2) {
		oa_reserved_2 = oaReserved_2;
	}
	public String getOa_reserved_3() {
		return oa_reserved_3;
	}
	public void setOa_reserved_3(String oaReserved_3) {
		oa_reserved_3 = oaReserved_3;
	}
	public String getOa_reserved_4() {
		return oa_reserved_4;
	}
	public void setOa_reserved_4(String oaReserved_4) {
		oa_reserved_4 = oaReserved_4;
	}
	public String getOa_reserved_5() {
		return oa_reserved_5;
	}
	public void setOa_reserved_5(String oaReserved_5) {
		oa_reserved_5 = oaReserved_5;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getBusi_sys_tp_cd() {
		return busi_sys_tp_cd;
	}
	public void setBusi_sys_tp_cd(String busiSysTpCd) {
		busi_sys_tp_cd = busiSysTpCd;
	}
	
}
