package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * 科目信息实体类
 * @author kf0741
 *
 */
@Entity
@Table(name ="tbk_subject_info")
public class SubjectInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 科目编号                                        	 subjCode
	 */
	@Column(name="SUB_CODE")
	private String SUB_CODE;
	/**
	 * 科目名称   					 subjName
	 */
	@Column(name="SUB_NAME")
	private String SUB_NAME;
	/**
	 * 科目级别（1级科目、2级科目、3级科目）
	 */
	@Column(name="SUB_LEVEL")
	private String SUB_LEVEL;
	/**
	 * 科目类型（就是科目首数字）			  subjType
	 */
	@Column(name="SUB_TYPE")
	private String SUB_TYPE;
	/**
	 * 借贷标识（没有用）                              	debitCredit
	 */
	@Column(name="CR_FLAG")
	private String CR_FLAG;
	/**
	 * 启用日期
	 */
	@Column(name="SDATE")
	private String SDATE;
	/**
	 * 停用日期
	 */
	@Column(name="EDATE")
	private String EDATE;
	/**
	 * 上级科目 						parentUbjectCode
	 */
	@Column(name="F_SUB_CODE")
	private String F_SUB_CODE;
	/**
	 * 英文名称
	 */
	@Column(name="EN_NAME")
	private String EN_NAME;
	/**
	 * 复核人
	 */
	@Column(name="REVIEW")
	private String REVIEW;
	/**
	 * 操作人
	 */
	@Column(name="OPERATOR")
	private String OPERATOR;
	/**
	 * 科目意义描述
	 */
	@Column(name="SUB_DESC")
	private String SUB_DESC;
	
	public String getSUB_CODE() {
		return SUB_CODE;
	}
	public void setSUB_CODE(String sUB_CODE) {
		SUB_CODE = sUB_CODE;
	}
	public String getSUB_NAME() {
		return SUB_NAME;
	}
	public void setSUB_NAME(String sUB_NAME) {
		SUB_NAME = sUB_NAME;
	}
	public String getSUB_LEVEL() {
		return SUB_LEVEL;
	}
	public void setSUB_LEVEL(String sUB_LEVEL) {
		SUB_LEVEL = sUB_LEVEL;
	}
	public String getSUB_TYPE() {
		return SUB_TYPE;
	}
	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	public String getCR_FLAG() {
		return CR_FLAG;
	}
	public void setCR_FLAG(String cR_FLAG) {
		CR_FLAG = cR_FLAG;
	}
	public String getSDATE() {
		return SDATE;
	}
	public void setSDATE(String sDATE) {
		SDATE = sDATE;
	}
	public String getEDATE() {
		return EDATE;
	}
	public void setEDATE(String eDATE) {
		EDATE = eDATE;
	}
	public String getF_SUB_CODE() {
		return F_SUB_CODE;
	}
	public void setF_SUB_CODE(String f_SUB_CODE) {
		F_SUB_CODE = f_SUB_CODE;
	}
	public String getEN_NAME() {
		return EN_NAME;
	}
	public void setEN_NAME(String eN_NAME) {
		EN_NAME = eN_NAME;
	}
	public String getREVIEW() {
		return REVIEW;
	}
	public void setREVIEW(String rEVIEW) {
		REVIEW = rEVIEW;
	}
	public String getOPERATOR() {
		return OPERATOR;
	}
	public void setOPERATOR(String oPERATOR) {
		OPERATOR = oPERATOR;
	}
	public String getSUB_DESC() {
		return SUB_DESC;
	}
	public void setSUB_DESC(String sUB_DESC) {
		SUB_DESC = sUB_DESC;
	}
	public String getIS_DISABLE() {
		return IS_DISABLE;
	}
	public void setIS_DISABLE(String iS_DISABLE) {
		IS_DISABLE = iS_DISABLE;
	}
	public String getAMT_FLAG() {
		return AMT_FLAG;
	}
	public void setAMT_FLAG(String aMT_FLAG) {
		AMT_FLAG = aMT_FLAG;
	}
	/**
	 * 是否有效（没有用，我这边默认接收到的科目都是有效科目）
	 */
	@Column(name="IS_DISABLE")
	private String IS_DISABLE;
	/**
	 * 表外科目借贷标识（0为借收贷付，1为借付贷收）
	 */
	@Column(name="AMT_FLAG")
	private String AMT_FLAG;
}
