
package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
@Entity
public class TsaAccountView  implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String acct;
	private String instId;
	private String payAcct;
    private String payBz;
    private String payNme;
    private String tranType;
    private String tranZy;
    private String tranAmt;
    private String tranDate;
    private String tranTime;
    private String resAcct;
    private String resBz;
    private String resNme;
    private String tRmk;
    private String tFlow;
    private String tNo;
    private String virAcct;
    
    private String status;

    public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

    public String getPayAcct() {
        return payAcct;
    }

    public void setPayAcct(String value) {
        this.payAcct = value;
    }

    public String getPayBz() {
        return payBz;
    }

    public void setPayBz(String value) {
        this.payBz = value;
    }

    public String getPayNme() {
        return payNme;
    }

    public void setPayNme(String value) {
        this.payNme = value;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String value) {
        this.tranType = value;
    }

    public String getTranZy() {
        return tranZy;
    }

    public void setTranZy(String value) {
        this.tranZy = value;
    }

    public String getTranAmt() {
        return tranAmt;
    }

    public void setTranAmt(String value) {
        this.tranAmt = value;
    }
    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String value) {
        this.tranDate = value;
    }

    public String getTranTime() {
        return tranTime;
    }

    public void setTranTime(String value) {
        this.tranTime = value;
    }

    public String getResAcct() {
        return resAcct;
    }

    public void setResAcct(String value) {
        this.resAcct = value;
    }

    public String getResBz() {
        return resBz;
    }

    public void setResBz(String value) {
        this.resBz = value;
    }

    public String getResNme() {
        return resNme;
    }

    public void setResNme(String value) {
        this.resNme = value;
    }

    public String getTRmk() {
        return tRmk;
    }

    public void setTRmk(String value) {
        this.tRmk = value;
    }

    public String getTFlow() {
        return tFlow;
    }

    public void setTFlow(String value) {
        this.tFlow = value;
    }

    public String getTNo() {
        return tNo;
    }

    public void setTNo(String value) {
        this.tNo = value;
    }

    public String getVirAcct() {
        return virAcct;
    }

    public void setVirAcct(String value) {
        this.virAcct = value;
    }

}
