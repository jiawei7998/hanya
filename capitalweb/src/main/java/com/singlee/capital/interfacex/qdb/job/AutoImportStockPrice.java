package com.singlee.capital.interfacex.qdb.job;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.qdb.service.StockService;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

public class AutoImportStockPrice implements CronRunnable {
	
	private StockService stockService = SpringContextHolder.getBean("StockService");

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		String paths = PropertiesUtil.getProperties("marketData.StockPrice.path");
		stockService.importStockPrice(paths);
		return true;
	}

	@Override
	public void terminate() {
	}

}
