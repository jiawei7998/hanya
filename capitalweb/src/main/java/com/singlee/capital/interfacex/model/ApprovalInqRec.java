package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApprovalInqRec", propOrder = { "partyName",
		"clientNo", "orgNum", "userNum", "approveId", "oriBatchNo",
		"productType", "accountProperty", "loanAmt", "loanCreditAmt", "lyed",
		"loanApplyed", "currency", "tenor", "extenTermUnit", "sameCustName",
		"validDate", "rateType", "rateReceiveType", "rate", "finalApprvResult",
		"siteNo", "limitType", "loanAssuKind" })
public class ApprovalInqRec {

	@XmlElement(name = "PartyName", required = true)
	protected String partyName;
	@XmlElement(name = "ClientNo", required = true)
	protected String clientNo;
	@XmlElement(name = "OrgNum", required = true)
	protected String orgNum;
	@XmlElement(name = "UserNum", required = true)
	protected String userNum;
	@XmlElement(name = "ApproveId", required = true)
	protected String approveId;
	@XmlElement(name = "OriBatchNo", required = true)
	protected String oriBatchNo;
	@XmlElement(name = "ProductType", required = true)
	protected String productType;
	@XmlElement(name = "AccountProperty", required = true)
	protected String accountProperty;
	@XmlElement(name = "LoanAmt", required = true)
	protected String loanAmt;
	@XmlElement(name = "LoanCreditAmt", required = true)
	protected String loanCreditAmt;
	@XmlElement(required = true)
	protected String lyed;
	@XmlElement(name = "LoanApplyed", required = true)
	protected String loanApplyed;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "Tenor", required = true)
	protected String tenor;
	@XmlElement(name = "ExtenTermUnit", required = true)
	protected String extenTermUnit;
	@XmlElement(name = "SameCustName", required = true)
	protected String sameCustName;
	@XmlElement(name = "ValidDate", required = true)
	protected String validDate;
	@XmlElement(name = "RateType", required = true)
	protected String rateType;
	@XmlElement(name = "RateReceiveType", required = true)
	protected String rateReceiveType;
	@XmlElement(name = "Rate", required = true)
	protected String rate;
	@XmlElement(name = "FinalApprvResult", required = true)
	protected String finalApprvResult;
	@XmlElement(name = "SiteNo", required = true)
	protected String siteNo;
	@XmlElement(name = "LimitType", required = true)
	protected String limitType;
	@XmlElement(name = "LoanAssuKind", required = true)
	protected String loanAssuKind;

	/**
	 * Gets the value of the partyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPartyName() {
		return partyName;
	}

	/**
	 * Sets the value of the partyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPartyName(String value) {
		this.partyName = value;
	}

	/**
	 * Gets the value of the clientNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClientNo() {
		return clientNo;
	}

	/**
	 * Sets the value of the clientNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClientNo(String value) {
		this.clientNo = value;
	}

	/**
	 * Gets the value of the orgNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgNum() {
		return orgNum;
	}

	/**
	 * Sets the value of the orgNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgNum(String value) {
		this.orgNum = value;
	}

	/**
	 * Gets the value of the userNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserNum() {
		return userNum;
	}

	/**
	 * Sets the value of the userNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserNum(String value) {
		this.userNum = value;
	}

	/**
	 * Gets the value of the approveId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getApproveId() {
		return approveId;
	}

	/**
	 * Sets the value of the approveId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setApproveId(String value) {
		this.approveId = value;
	}

	/**
	 * Gets the value of the oriBatchNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOriBatchNo() {
		return oriBatchNo;
	}

	/**
	 * Sets the value of the oriBatchNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOriBatchNo(String value) {
		this.oriBatchNo = value;
	}

	/**
	 * Gets the value of the productType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * Sets the value of the productType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductType(String value) {
		this.productType = value;
	}

	/**
	 * Gets the value of the accountProperty property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountProperty() {
		return accountProperty;
	}

	/**
	 * Sets the value of the accountProperty property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountProperty(String value) {
		this.accountProperty = value;
	}

	/**
	 * Gets the value of the loanAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAmt() {
		return loanAmt;
	}

	/**
	 * Sets the value of the loanAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAmt(String value) {
		this.loanAmt = value;
	}

	/**
	 * Gets the value of the loanCreditAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanCreditAmt() {
		return loanCreditAmt;
	}

	/**
	 * Sets the value of the loanCreditAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanCreditAmt(String value) {
		this.loanCreditAmt = value;
	}

	/**
	 * Gets the value of the lyed property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLyed() {
		return lyed;
	}

	/**
	 * Sets the value of the lyed property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLyed(String value) {
		this.lyed = value;
	}

	/**
	 * Gets the value of the loanApplyed property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanApplyed() {
		return loanApplyed;
	}

	/**
	 * Sets the value of the loanApplyed property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanApplyed(String value) {
		this.loanApplyed = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the tenor property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTenor() {
		return tenor;
	}

	/**
	 * Sets the value of the tenor property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTenor(String value) {
		this.tenor = value;
	}

	/**
	 * Gets the value of the extenTermUnit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExtenTermUnit() {
		return extenTermUnit;
	}

	/**
	 * Sets the value of the extenTermUnit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExtenTermUnit(String value) {
		this.extenTermUnit = value;
	}

	/**
	 * Gets the value of the sameCustName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSameCustName() {
		return sameCustName;
	}

	/**
	 * Sets the value of the sameCustName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSameCustName(String value) {
		this.sameCustName = value;
	}

	/**
	 * Gets the value of the validDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValidDate() {
		return validDate;
	}

	/**
	 * Sets the value of the validDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValidDate(String value) {
		this.validDate = value;
	}

	/**
	 * Gets the value of the rateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRateType() {
		return rateType;
	}

	/**
	 * Sets the value of the rateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRateType(String value) {
		this.rateType = value;
	}

	/**
	 * Gets the value of the rateReceiveType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRateReceiveType() {
		return rateReceiveType;
	}

	/**
	 * Sets the value of the rateReceiveType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRateReceiveType(String value) {
		this.rateReceiveType = value;
	}

	/**
	 * Gets the value of the rate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * Sets the value of the rate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRate(String value) {
		this.rate = value;
	}

	/**
	 * Gets the value of the finalApprvResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFinalApprvResult() {
		return finalApprvResult;
	}

	/**
	 * Sets the value of the finalApprvResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFinalApprvResult(String value) {
		this.finalApprvResult = value;
	}

	/**
	 * Gets the value of the siteNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSiteNo() {
		return siteNo;
	}

	/**
	 * Sets the value of the siteNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSiteNo(String value) {
		this.siteNo = value;
	}

	/**
	 * Gets the value of the limitType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitType() {
		return limitType;
	}

	/**
	 * Sets the value of the limitType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitType(String value) {
		this.limitType = value;
	}

	/**
	 * Gets the value of the loanAssuKind property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAssuKind() {
		return loanAssuKind;
	}

	/**
	 * Sets the value of the loanAssuKind property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAssuKind(String value) {
		this.loanAssuKind = value;
	}
}
