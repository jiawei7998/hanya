package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for NetAddrRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="NetAddrRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpTypeAddn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetAddrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetAddrRec", propOrder = { "opTypeAddn", "netType",
		"netAddrNo", "netChannelType", "netAddr" })
public class NetAddrRec {

	@XmlElement(name = "OpTypeAddn", required = true)
	protected String opTypeAddn;
	@XmlElement(name = "NetType", required = true)
	protected String netType;
	@XmlElement(name = "NetAddrNo", required = true)
	protected String netAddrNo;
	@XmlElement(name = "NetChannelType", required = true)
	protected String netChannelType;
	@XmlElement(name = "NetAddr", required = true)
	protected String netAddr;

	/**
	 * Gets the value of the opTypeAddn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpTypeAddn() {
		return opTypeAddn;
	}

	/**
	 * Sets the value of the opTypeAddn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpTypeAddn(String value) {
		this.opTypeAddn = value;
	}

	/**
	 * Gets the value of the netType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetType() {
		return netType;
	}

	/**
	 * Sets the value of the netType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetType(String value) {
		this.netType = value;
	}

	/**
	 * Gets the value of the netAddrNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetAddrNo() {
		return netAddrNo;
	}

	/**
	 * Sets the value of the netAddrNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetAddrNo(String value) {
		this.netAddrNo = value;
	}

	/**
	 * Gets the value of the netChannelType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetChannelType() {
		return netChannelType;
	}

	/**
	 * Sets the value of the netChannelType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetChannelType(String value) {
		this.netChannelType = value;
	}

	/**
	 * Gets the value of the netAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetAddr() {
		return netAddr;
	}

	/**
	 * Sets the value of the netAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetAddr(String value) {
		this.netAddr = value;
	}

}
