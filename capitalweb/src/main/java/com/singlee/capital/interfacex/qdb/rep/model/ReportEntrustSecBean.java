package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportEntrustSecBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String zjlx       ;//资金类型           
	private String partyName  ;//交易对手名称       
	private String secId      ;//债券代号           
	private String secNm      ;//债券名称           
	private double faceAmt    ;//面值               
	private double unitCost   ;//单位成本           
	private double costAmt    ;//成本总价    
	private String cost		  ;//成本中心
	private String port		  ;//投资组合
	
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getZjlx() {
		return zjlx;
	}
	public void setZjlx(String zjlx) {
		this.zjlx = zjlx;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getSecNm() {
		return secNm;
	}
	public void setSecNm(String secNm) {
		this.secNm = secNm;
	}
	public double getFaceAmt() {
		return faceAmt;
	}
	public void setFaceAmt(double faceAmt) {
		this.faceAmt = faceAmt;
	}
	public double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}
	public double getCostAmt() {
		return costAmt;
	}
	public void setCostAmt(double costAmt) {
		this.costAmt = costAmt;
	}
	
}
