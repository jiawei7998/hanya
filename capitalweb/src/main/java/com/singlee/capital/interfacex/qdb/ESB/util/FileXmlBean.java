package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.IOException;
import java.io.Serializable;

public class FileXmlBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int num;//序号
	private int no;//序列号

	private String fieldTag;//域名

	private int position;//起始位置

	private String dataType;//数据类型

	private int length;//长度

	private String mod;//是否关键字

	private String leftOrRight;//左填充还是右填充

	private String fillChar;//填充字符

	private String value;//值ֵ

	private String description;//描述

	private int decimalsNo;//小数位数

	public FileXmlBean() {
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public FileXmlBean(int num,int no, String fieldTag, int position, String dataType,
			int length, String mod, String leftOrRight, String fillChar,
			String value, String description, int decimalsNo) {
		super();
		this.no = no;
		this.num=num;
		this.fieldTag = fieldTag;
		this.position = position;
		this.dataType = dataType;
		this.length = length;
		this.mod = mod;
		this.leftOrRight = leftOrRight;
		this.fillChar = fillChar;
		this.value = value;
		this.description = description;
		this.decimalsNo = decimalsNo;
	}

	/**
	 * @return the no
	 */
	public int getNo() {
		return no;
	}

	/**
	 * @param no
	 *            the no to set
	 */
	public void setNo(int no) {
		this.no = no;
	}

	/**
	 * @return the fieldTag
	 */
	public String getFieldTag() {
		return fieldTag;
	}

	/**
	 * @param fieldTag
	 *            the fieldTag to set
	 */
	public void setFieldTag(String fieldTag) {
		this.fieldTag = fieldTag;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType
	 *            the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the mod
	 */
	public String getMod() {
		return mod;
	}

	/**
	 * @param mod
	 *            the mod to set
	 */
	public void setMod(String mod) {
		this.mod = mod;
	}

	/**
	 * @return the leftOrRight
	 */
	public String getLeftOrRight() {
		return leftOrRight;
	}

	/**
	 * @param leftOrRight
	 *            the leftOrRight to set
	 */
	public void setLeftOrRight(String leftOrRight) {
		this.leftOrRight = leftOrRight;
	}

	/**
	 * @return the fillChar
	 */
	public String getFillChar() {
		return fillChar;
	}

	/**
	 * @param fillChar
	 *            the fillChar to set
	 */
	public void setFillChar(String fillChar) {
		this.fillChar = fillChar;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @param inParam1
	 *           
	 * @param len
	 *           
	 * @param align
	 *           
	 * @param flag
	 *           
	 * @return
	 * @throws IOException 
	 */
	public String getFormatStr(String inParam1, int len, String align,String flag) throws Exception {
		if (inParam1 == null) {inParam1 = " ";
		}
		String result = "";
		String inParam = inParam1;
		if (inParam.length() < len) 
		{
			if ("R".equalsIgnoreCase(align)) {
				for(int i=0;i<len-inParam.length();i++){
					result += flag;
				}
				result = inParam+result;
			} else {
				for(int i=0;i<len-inParam.length();i++){
					result += flag;
				}
				result = result+inParam;
			}
		}else {
			result = inParam;
		}
		return result;
	}
	
	public String format() throws Exception {
		if ("A".equalsIgnoreCase(this.dataType)) {
			if(null == this.value || "".equals(this.value))
			{
				this.value = " ";
			}
			return getFormatStr(this.value, this.length, this.leftOrRight,
					this.fillChar);
		}else if("C".equalsIgnoreCase(this.dataType)){
			if(null == this.value || "".equals(this.value))
			{
				this.value = " ";
			}
			return getFormatStr(" "+this.value, this.length-this.value.length(), this.leftOrRight,
					this.fillChar);
		} else if ("S".equalsIgnoreCase(this.dataType)) {
			if(this.value == null || "".equals(this.value)) {this.value = "0";}
			String left = "";
			String right = "";
			if(this.value.indexOf(".") != -1)
			{
			 left = this.value.substring(0, this.value.indexOf("."));
			 right = this.value.substring(this.value.indexOf(".") + 1,
					this.value.length());
			}else {
				left = this.value;
			}
			return getFormatStr(left, this.length - this.decimalsNo,
					this.leftOrRight, this.fillChar)
					+ getFormatStr(right, this.decimalsNo, "L"
							.equalsIgnoreCase(this.leftOrRight) ? "R" : "L", this.fillChar);
		}
		return "";
	}

	/**
	 * ��ȡ�ַ��е������ַ���
	 * 
	 * @param o
	 * @return
	 */
	public int getChineseCharLen(Object o) {

		String str = (String) o;

		int count = 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) < 0 || str.charAt(i) > 255) {
				count += 1;
			}
		}

		return count;
	}


	/**
	 * @return the decimalsNo
	 */
	public int getDecimalsNo() {
		return decimalsNo;
	}

	/**
	 * @param decimalsNo  the decimalsNo to set
	 */
	public void setDecimalsNo(int decimalsNo) {
		this.decimalsNo = decimalsNo;
	}
	
}
