package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorpCusInfoRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorpCusInfoRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OurReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RelCust" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpCusInfoRec", propOrder = { "relationCode", "ourReference",
		"relCust" })
public class CorpCusInfoRec {

	@XmlElement(name = "RelationCode", required = true)
	protected String relationCode;
	@XmlElement(name = "OurReference", required = true)
	protected String ourReference;
	@XmlElement(name = "RelCust", required = true)
	protected String relCust;

	/**
	 * Gets the value of the relationCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelationCode() {
		return relationCode;
	}

	/**
	 * Sets the value of the relationCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelationCode(String value) {
		this.relationCode = value;
	}

	/**
	 * Gets the value of the ourReference property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOurReference() {
		return ourReference;
	}

	/**
	 * Sets the value of the ourReference property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOurReference(String value) {
		this.ourReference = value;
	}

	/**
	 * Gets the value of the relCust property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelCust() {
		return relCust;
	}

	/**
	 * Sets the value of the relCust property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelCust(String value) {
		this.relCust = value;
	}

}
