package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class PraseEsbXmlPackageDescription {
	//返回报文解析

	/*********************解析ESB XML RESPONSE PACKAGE*************************/
	/**
	 * 迭代递归出节点下面的所以子节点  不重复
	 * @param element
	 * @param resultHashMap
	 */
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,HashMap<String, Object> resultHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			/** 
			 * ConvertToServe.readEsbFile() 读取ESB配置文件
			 * name    element名称
			 * type    String or Number
			 * length  域长度
			 */
			HashMap<String,EsbBean> hashMap = ConvertToServe.readEsbFile(); 
			boolean flag = false;
			Iterator<String> name = hashMap.keySet().iterator();
			while(name.hasNext()){
				String str = name.next();
				if(element.getName().equalsIgnoreCase(str)&& "Number".equalsIgnoreCase(hashMap.get(str).getType())){
					flag = true;
				}
			}
			if(flag){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element.getName()))+""+StringUtils.trimToEmpty(ConvertUtil.getRemoveZero(element.getText())));
				resultHashMap.put(StringUtils.trimToEmpty(element.getName()), StringUtils.trimToEmpty(ConvertUtil.getRemoveZero(element.getText())));
			}
			else{
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element.getName()))+""+StringUtils.trimToEmpty(element.getText()));
				resultHashMap.put(StringUtils.trimToEmpty(element.getName()), StringUtils.trimToEmpty(element.getText()));
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				getElementList(element2,resultHashMap);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void praseEsbXmlPackageDescription(byte[] responseXml,HashMap<String, Object> svcHdr,HashMap<String, Object> campHdr,HashMap<String,Object> appHdr,HashMap<Integer, HashMap<String, Object>> appBody) throws Exception
	{
		ByteArrayInputStream byteArrayInputStream = null;
		try{
			SAXReader reader = new SAXReader();
			byteArrayInputStream = new ByteArrayInputStream(responseXml);
			Document xmlDoc = reader.read(byteArrayInputStream);
			System.out.println(XmlFormat.format(xmlDoc.asXML()));
			List<Element> elements = xmlDoc.getRootElement().elements();
			for(Element element:elements)
			{
				if("svcHdr".equals(element.getName()))
				{
					System.out.println("*************************B svcHdr**************************");
					getElementList(element,svcHdr);
					System.out.println("*************************E svcHdr**************************");
				}else 
				if("campHdr".equals(element.getName()))
				{
					System.out.println("*************************B campHdr*************************");
					getElementList(element,campHdr);
					System.out.println("*************************E campHdr*************************");
				}else 
				if("appHdr".equals(element.getName()))
				{
					System.out.println("*************************B appHdr**************************");
					getElementList(element,appHdr);
					System.out.println("*************************E appHdr**************************");
				}else 
				if("appBody".equals(element.getName()))
				{
					System.out.println("*************************B appBody*************************");
					List<Element> childElements = element.elements();
					HashMap<String, Object> childHashMap = null;
					int count = 1;
					for(Element childelement:childElements)
					{
					System.out.println("********************NUM:"+count+"**************************");
						childHashMap = new HashMap<String, Object>();
						getElementList(childelement, childHashMap);
						appBody.put(count, childHashMap);
						count++;
					}
					System.out.println("*************************B appBody*************************");
				}
			}
		}catch (Exception e) {
			throw e;
		}finally{
			if(null != byteArrayInputStream)
			{
				byteArrayInputStream.close();
			}
		}
	}
}
