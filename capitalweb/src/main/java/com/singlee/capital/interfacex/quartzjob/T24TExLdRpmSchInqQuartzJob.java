package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.model.TExLdRpmSchInq;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class T24TExLdRpmSchInqQuartzJob implements CronRunnable {
	/**
	 * 还款计划数据字典文件接口	
	 * 请求后轮询	
        1、GTP配置IFBM与T24；
		2、文件命名格式：210-uuid.txt；通配符：S04_LD_SCH_DET_*.flg
		3、同业目录定义为：/share_ifbm/gtp/T24/recv/
		4、分隔符号：“@!@”
		5、案例参见：199-GMT-00000000000-20170508R0010012.txt
		6、接收：/FileEXG/RECVIN/BFEII/BFE   发送/gtp/files/T24/BOSEXG/SND 
		7、sit：10.240.2.42/43   uat:10.240.2.15/16 
	 */
	
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		try {
			
			System.out.println("=======================开始处理========================");
			String classList= this.getClass().getName();
			System.out.println("classList:======"+classList);
			Map<String,String> map =new HashMap<String,String>();
			map.put("classList", classList);
			String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
			String jobName=jobExcuService.getJobByclassList(map).get(0).getJobName();
			
			JobExcuLog jobExcuLog = new JobExcuLog();
			jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
			jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
			jobExcuService.insertJobExcuLog(jobExcuLog);
			
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");

			List<TExLdRpmSchInq> TExLdRpmSchInqs=gtpFileService.GtpIfbmT24TExLdRpmSchInqRead();
			 
			if(TExLdRpmSchInqs.size()>0){
				 jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
				 jobExcuService.updateJobExcuLog(jobExcuLog);
				 System.out.println("=======================处理结束========================");
			}else{
				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:文件读取或处理失败--ERROR");
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
	
}


