package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCustDetInqRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCustDetInqRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="CustmerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CNName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCustDetInqRq", propOrder = { "commonRqHdr", "custmerCode",
		"cnName", "idType", "idNum" })
public class TCustDetInqRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "CustmerCode", required = true)
	protected String custmerCode;
	@XmlElement(name = "CNName", required = true)
	protected String cnName;
	@XmlElement(name = "IdType", required = true)
	protected String idType;
	@XmlElement(name = "IdNum", required = true)
	protected String idNum;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the custmerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustmerCode() {
		return custmerCode;
	}

	/**
	 * Sets the value of the custmerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustmerCode(String value) {
		this.custmerCode = value;
	}

	/**
	 * Gets the value of the cnName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCNName() {
		return cnName;
	}

	/**
	 * Sets the value of the cnName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCNName(String value) {
		this.cnName = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdType(String value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdNum() {
		return idNum;
	}

	/**
	 * Sets the value of the idNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdNum(String value) {
		this.idNum = value;
	}

}
