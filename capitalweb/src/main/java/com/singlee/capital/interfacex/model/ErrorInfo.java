package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ErrorInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ErrorInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorMessageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorMessageText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorInfo", propOrder = { "errorMessageType", "errorCode",
		"errorMessageText", "errorState" })
public class ErrorInfo {

	@XmlElement(required = true)
	protected String errorMessageType;
	@XmlElement(required = true)
	protected String errorCode;
	@XmlElement(required = true)
	protected String errorMessageText;
	@XmlElement(required = true)
	protected String errorState;

	/**
	 * Gets the value of the errorMessageType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorMessageType() {
		return errorMessageType;
	}

	/**
	 * Sets the value of the errorMessageType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorMessageType(String value) {
		this.errorMessageType = value;
	}

	/**
	 * Gets the value of the errorCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the value of the errorCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorCode(String value) {
		this.errorCode = value;
	}

	/**
	 * Gets the value of the errorMessageText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorMessageText() {
		return errorMessageText;
	}

	/**
	 * Sets the value of the errorMessageText property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorMessageText(String value) {
		this.errorMessageText = value;
	}

	/**
	 * Gets the value of the errorState property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorState() {
		return errorState;
	}

	/**
	 * Sets the value of the errorState property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorState(String value) {
		this.errorState = value;
	}

}
