package com.singlee.capital.interfacex.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TtSettlDateMapper;
import com.singlee.capital.base.model.TtSettlDate;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.service.SeasonTaskService;
@Service("SeasonTaskServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SeasonTaskServiceImpl implements SeasonTaskService {
	
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private TtSettlDateMapper ttSettlDateMapper;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	
	@Override
	public void getSeasonTask() {
		String sysdate = dayendDateService.getDayendDate();
		String year = sysdate.split("-")[0];
		String month = dictionaryGetService.getTaDictByCodeAndKey("SETTLE_DATE", "01").getDict_value();
		String day = dictionaryGetService.getTaDictByCodeAndKey("SETTLE_DATE", "02").getDict_value();
		if("1".equals(month)){
			ttSettlDateMapper.deleteAll();
			for(int i=3;i<13;){
				String date = year+"-"+i+"-"+day;
				TtSettlDate tsd = new TtSettlDate();
				tsd.setSettlDate(date);
				tsd.setSettlFlag("1");
				ttSettlDateMapper.insert(tsd);
				i=i+3;
			}
		}else if("2".equals(month)){
			ttSettlDateMapper.deleteAll();
			for(int i=1;i<13;){
				String date = year+"-"+i+"-"+day;
				TtSettlDate tsd = new TtSettlDate();
				tsd.setSettlDate(date);
				tsd.setSettlFlag("1");
				ttSettlDateMapper.insert(tsd);
			}
		}
		
		
	}
	

	
}
