package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 委外业务底层资产明细表
 * @author Administrator
 *
 */
@Entity
@Table(name="TD_TRADE_INV_DETAIL")
public class TdTradeInvDetail implements Serializable{

	private static final long serialVersionUID = 1L;
	//交易号
	private String tradeId;
	//资产类型
	private String aType;
	//债券ID
	private String secId;
	//债券名称
	private String secNm;
	//面额
	private String faceamt;
	//单位成本
	private String unitCost;
	//成本总价
	private String costAmt;
	//版本号
	private String version;
	//备注
	private String remark;
	//导入日期
	private String iDate;
	
	public String getiDate() {
		return iDate;
	}
	public void setiDate(String iDate) {
		this.iDate = iDate;
	}
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getSecNm() {
		return secNm;
	}
	public void setSecNm(String secNm) {
		this.secNm = secNm;
	}
	public String getFaceamt() {
		return faceamt;
	}
	public void setFaceamt(String faceamt) {
		this.faceamt = faceamt;
	}
	public String getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}
	public String getCostAmt() {
		return costAmt;
	}
	public void setCostAmt(String costAmt) {
		this.costAmt = costAmt;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
