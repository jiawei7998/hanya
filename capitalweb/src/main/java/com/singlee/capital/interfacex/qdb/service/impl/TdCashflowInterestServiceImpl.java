package com.singlee.capital.interfacex.qdb.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.service.TdCashflowInterestService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCashflowInterestServiceImpl implements TdCashflowInterestService{
	@Autowired
	private TdCashflowInterestMapper tdCashflowInterestMapper;
	
	@Autowired
	private TdCashflowCapitalMapper  cashflowCapitalMapper;
	
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;

	@Override
	public Page<TdCashflowInterest> pageCfComfirm(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdCashflowInterest> result = tdCashflowInterestMapper.pageCfComfirm(params, rowBounds);
		return result;
	}

	@Override
	public boolean updateConfirmFlag(List<TdCashflowInterest> tdCashflowInterest) {
		return tdCashflowInterestMapper.updateConfirmFlag(tdCashflowInterest);
	}

	@Override
	public List<TdCashflowInterest> queryPrintList(Map<String, Object> params) {
		return tdCashflowInterestMapper.queryPrintList(params);
	}
	
	@Override
	public List<TdCashflowCapital> getCapitalListByDate(Map<String, Object> params) {
		return cashflowCapitalMapper.getCapitalListByDate(params);
	}
	
	@Override
	public List<CashflowDailyInterest> listDailyInterestList(Map<String, Object> params) {
		return cashflowDailyInterestMapper.pageDailyInterestList(params);
	}

	
}
