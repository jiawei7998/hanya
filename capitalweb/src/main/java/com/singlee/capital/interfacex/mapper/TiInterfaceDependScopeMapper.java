package com.singlee.capital.interfacex.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.TiInterfaceDependScope;

public interface TiInterfaceDependScopeMapper extends Mapper<TiInterfaceDependScope>{

	public TiInterfaceDependScope queryDependScopeByScopes(Map<String, Object> map) throws Exception;
	
	public Page<TiInterfaceDependScope> queryDependScopePage(Map<String, Object> map) throws Exception;
	
	public void insertDependScope(TiInterfaceDependScope tiInterfaceDependScope) throws Exception;
	
}
