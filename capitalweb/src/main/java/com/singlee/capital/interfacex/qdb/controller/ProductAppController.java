package com.singlee.capital.interfacex.qdb.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;
/**
 * 委外业务
 * @author mqp
 *
 */
@Controller
@RequestMapping(value = "/ProductAppController")
public class ProductAppController  extends CommonController{
	@Autowired
	private ProductApproveService productApproveService;
	
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/selectPageProduct")
	public RetMsg<PageInfo<TdProductApproveMain>> selectPageProduct(@RequestBody Map<String,Object> params){
		Page<TdProductApproveMain> page = productApproveService.getProductAppPage(params);
		return RetMsgHelper.ok(page);
	}

}
