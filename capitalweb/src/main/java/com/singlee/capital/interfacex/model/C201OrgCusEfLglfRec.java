package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for C201OrgCusEfLglfRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="C201OrgCusEfLglfRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorpIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalEntTyp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalIdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginEffDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndEffDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C201OrgCusEfLglfRec", propOrder = { "corpIdType",
		"legalEntTyp", "legalIdNo", "beginEffDt", "endEffDt" })
public class C201OrgCusEfLglfRec {

	@XmlElement(name = "CorpIdType", required = true)
	protected String corpIdType;
	@XmlElement(name = "LegalEntTyp", required = true)
	protected String legalEntTyp;
	@XmlElement(name = "LegalIdNo", required = true)
	protected String legalIdNo;
	@XmlElement(name = "BeginEffDt", required = true)
	protected String beginEffDt;
	@XmlElement(name = "EndEffDt", required = true)
	protected String endEffDt;

	/**
	 * Gets the value of the corpIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpIdType() {
		return corpIdType;
	}

	/**
	 * Sets the value of the corpIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpIdType(String value) {
		this.corpIdType = value;
	}

	/**
	 * Gets the value of the legalEntTyp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalEntTyp() {
		return legalEntTyp;
	}

	/**
	 * Sets the value of the legalEntTyp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalEntTyp(String value) {
		this.legalEntTyp = value;
	}

	/**
	 * Gets the value of the legalIdNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalIdNo() {
		return legalIdNo;
	}

	/**
	 * Sets the value of the legalIdNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalIdNo(String value) {
		this.legalIdNo = value;
	}

	/**
	 * Gets the value of the beginEffDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginEffDt() {
		return beginEffDt;
	}

	/**
	 * Sets the value of the beginEffDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginEffDt(String value) {
		this.beginEffDt = value;
	}

	/**
	 * Gets the value of the endEffDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndEffDt() {
		return endEffDt;
	}

	/**
	 * Sets the value of the endEffDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndEffDt(String value) {
		this.endEffDt = value;
	}

}
