package com.singlee.capital.interfacex.qdb.ESB.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.singlee.capital.interfacex.qdb.ESB.util.FileTransferUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.FileXmlBean;
import com.singlee.capital.interfacex.qdb.ESB.util.XmlFormat;
import com.singlee.capital.interfacex.qdb.service.InstitutionSynchService;
import com.singlee.capital.interfacex.qdb.service.UserSynchService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

/**
 * 用户管理控制器
 * @author lihb
 * @date   2016-07-06
 * @company 杭州新利科技有限公司
 */

@Service("OrgUserSynchService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OrgUserSynchService {
	/* 用户service */
	@Autowired
	private UserSynchService userService;
	
	@Autowired
	private InstitutionSynchService instService;
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	/**
	 * 日终同步柜员信息和机构信息
	 */
	
	public void snycMager() {
		try {
			//存放柜员机构信息的流水号是 ******_CSS_99999999
			// System.out.println("OrgController snycMager BEGIN");
			String base= PathUtils.getWebRootPath() +"/WEB-INF/classes/schema/";
			String path = base+"ICDWNU.txt";
 			log.info("------------------------------开始同步机构柜员");
			log.info("机构柜员文件路径"+path);
			
			String f = FileTransferUtil.fileDownload(FileTransferUtil.getBusiSerialToday("CSS_99999999"), base);
			if (!"SUCCESS".equals(f)) {
				// 如果今天没数据，则拿到昨天的数据
				f = FileTransferUtil.fileDownload(FileTransferUtil.getBusiSerialYesterday("CSS_99999999"), base);
				log.info("未获取到今天流水号信息，请确认文件是否已经上传!"+FileTransferUtil.getBusiSerialToday("CSS_99999999"));
				if (!"SUCCESS".equals(f)) {
					log.info("未获取到昨天流水号信息，请确认文件是否已经上传!"+FileTransferUtil.getBusiSerialYesterday("CSS_99999999"));
					return;
				}
			}

			SAXReader sax = new SAXReader();
			InputStreamReader in = null;
			BufferedReader br = null;
			TaUser user = null;
			try {
				in = new InputStreamReader(new FileInputStream(new File(path)), "GBK");
				br = new BufferedReader(in);
				Document docusrU = sax.read(new File(base + "user.xml"));
				Document docusrI = sax.read(new File(base + "organ.xml"));
				Document docusrR = sax.read(new File(base + "organization.xml"));

				HashMap<Integer, FileXmlBean> userMap = new HashMap<Integer, FileXmlBean>();
				HashMap<Integer, FileXmlBean> instMap = new HashMap<Integer, FileXmlBean>();
				HashMap<Integer, FileXmlBean> relMap = new HashMap<Integer, FileXmlBean>();
				// 解析user.xml文件 放到map里
				XmlFormat.getElementList(docusrU.getRootElement(), userMap);
				XmlFormat.getElementList(docusrI.getRootElement(), instMap);
				XmlFormat.getElementList(docusrR.getRootElement(), relMap);
				String str = null;
				String key = null;
				// 每条数据的所有字段存放
				String[] content = null;
				List<TaUser> users = new ArrayList<TaUser>();
				List<TtInstitution> insts = new ArrayList<TtInstitution>();
				Map<String,String> rels = new HashMap<String,String>();
				Map<String,String> userIDinstId = new HashMap<String,String>();
				
				try {
					
					boolean flag=sync("IN");
					if(!flag){
						return;
					}
			
					while ((str = br.readLine()) != null) {
						key = str.substring(0, str.indexOf(" "));					
						if ("USRBASE".equals(key)) {
							user = new TaUser();
							content = str.substring(userMap.get(0).getPosition() - 1, str.lastIndexOf("EOR")).split(";");
							for (int i = 0; i < userMap.size(); i++) {
								getValueFromUserModel(user, StringUtils.trimToEmpty(userMap.get(i).getDescription()), StringUtils.trimToEmpty(content[userMap.get(i).getNo()]));
							}
							//默认密码
							user.setUserPwd("E10ADC3949BA59ABBE56E057F20F883E");
							user.setEmpId(user.getUserId());
							user.setIsActive("1");
							user.setIsAccess("1");
							user.setBranchId("QCCBCNBQ");
							users.add(user);
							
							if (users.size() >= 1000) {
								userService.insertUsers(users);
								userService.insertUserIdinstID(users);
								users.clear();
							}
						} else if ("BRNBASE".equals(key)) {
							TtInstitution inst = new TtInstitution();
							content = str.substring(instMap.get(0).getPosition() - 1, str.lastIndexOf("EOR")).split(";");
							for (int i = 0; i < instMap.size(); i++) {
								// 对应的user.xml中的字段 //字段在ICDWNU.txt中对应的值
								getValueFromInstModel(inst,StringUtils.trimToEmpty(instMap.get(i).getDescription()),content[instMap.get(i).getNo()]);
							}
							inst.setPInstId("-1");
							inst.setBranchId("QCCBCNBQ");
							
							//把机构类型解析成 对应本系统的参数
							parserInstType(inst);
						
							insts.add(inst);
							if (insts.size() >= 200) {
								instService.insertESB(insts);
								insts.clear();
							}

						} else if ("BRNREL".equals(key)) {
							// 机构关系信息
							if(insts.size() != 0) {
								instService.insertESB(insts);
								insts.clear();
							}
							String type = str.substring(16, 18);
							if("01".equals(type)) {
								content = str.substring(relMap.get(0).getPosition() - 1, str.lastIndexOf("EOR")).split(";");
								String instId = content[0];
								String instPId = content[4];
								rels.put("instId", instId);
								rels.put("instPId", instPId);
								instService.updateInstPId(rels);
								
							}
						}
					}
					
					if(users.size() != 0) {
						userService.insertUsers(users);
						userService.insertUserIdinstID(users);
						users.clear();
					}
					if(insts.size()!= 0){
						instService.insertESB(insts);
						insts.clear();
					}
					log.info("------------------------------结束同步机构柜员");
					
				} catch (Exception e) {
					boolean flag=sync("out");
					log.error("同步失败已回滚"+e.getMessage());
					log.error("回滚:" + flag);
					e.printStackTrace();
					throw e;
				}
				
			} catch (Exception e) {
				log.error("同步柜员机构失败："+e.getMessage());
				e.printStackTrace();
			} finally {
				try {
					br.close();
					in.close();
				} catch (Exception e) {
					
				}
			}

		} catch (Exception e) {
			log.error("同步机构"+e.getMessage());
			e.printStackTrace();
		}
		return;
	}


	/**
	 * 把每行数据定义为一个TaUser，并且赋值
	 * @param user 接收赋值的TaUser
	 * @param fieldName 需要赋值的字段
	 * @param fieldText 字段对应的值
	 * @return 字段对应的值
	 */
	//@SuppressWarnings("deprecation")
	public static void getValueFromUserModel(TaUser user ,String fieldName,String fieldText) {
//		String result = "";
		if(org.springframework.util.StringUtils.hasText(fieldName)){
			try {
				String firstLetter = fieldName.substring(0, 1).toUpperCase();
	            //获得对应的setXxxxx方法的名子。。。
	            String setMethodName = "set" + firstLetter + fieldName.substring(1);
	            //拿到字段对应的值
	            Field field = user.getClass().getDeclaredField(fieldName);
	            Method setMethod = user.getClass().getMethod(setMethodName, new Class[]{field.getType()});
		        if(field.getType().equals(String.class)){
		        	setMethod.invoke(user, new Object[]{getRemoveZero(fieldText)});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Integer.class)){
		        	setMethod.invoke(user, new Object[]{Integer.parseInt(getRemoveZero(fieldText))});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Date.class)){
		        	setMethod.invoke(user, new Object[]{new Date(fieldText)});//调用原有对像的setXxxx方法...
		        }
			} catch (SecurityException e) {
				
			} catch (NoSuchMethodException e) {
				
			} catch (IllegalArgumentException e) {
				
			} catch (IllegalAccessException e) {
				
			} catch (Exception e) {
				
			}
		}		
//		result = org.springframework.util.StringUtils.hasText(result)?result:"";
		return;
	}
	
	@SuppressWarnings("deprecation")
	public static void getValueFromInstModel(TtInstitution inst ,String fieldName,String fieldText) {
		String result = "";
		if(org.springframework.util.StringUtils.hasText(fieldName)){
			try {
				String firstLetter = fieldName.substring(0, 1).toUpperCase();
	            //获得对应的getXxxxx方法的名子。。。
	            String setMethodName = "set" + firstLetter + fieldName.substring(1);
	            Field field = inst.getClass().getDeclaredField(fieldName);
	            Method setMethod = inst.getClass().getMethod(setMethodName, new Class[]{field.getType()});
		        if(field.getType().equals(String.class)){
		        	setMethod.invoke(inst, new Object[]{fieldText});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Integer.class)){
		        	setMethod.invoke(inst, new Object[]{Integer.parseInt(getRemoveZero(fieldText))});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Date.class)){
		        	setMethod.invoke(inst, new Object[]{new Date(fieldText)});//调用原有对像的setXxxx方法...
		        }
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				//e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}		
		result = org.springframework.util.StringUtils.hasText(result)?result:"";
		return;
	}
	public boolean sync(String type){
		Map<String,String> map=new HashMap<String,String>();
		map.put("RESCODE", "");
		map.put("RESTYPE", type);
		map=userService.changeTemp(map);
		//if(map.get("RESTYPE").equals("0")){
			return true;
		//}
		//return false;
	}
	
	/**
	 * 格式化字符，去掉前面零
	 * @param str
	 * @return
	 */
	public static String getRemoveZero(String str) {
		int num = 0;
		for (int i = 0; i < str.length(); i++) {
			if ('0' != str.charAt(i) || i == (str.length() - 1)) {
				num = i;
				break;
			}
		}
		return str.substring(num, str.length());
	}
	
	/**
	 * 解析机构类型
	 * @param str
	 * @return
	 */
	public static void parserInstType(TtInstitution inst) {
		if(inst.getInstType() == null) {return;}
		if("1".equals(inst.getInstType())) {
			inst.setInstType("0");
		} else if("3".equals(inst.getInstType())) {
			inst.setInstType("1");
		} else if("5".equals(inst.getInstType()) || "7".equals(inst.getInstType())) {
			inst.setInstType("2");
		}
	}

}
