package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name ="TI_INTERFACE_DEPENDSLIMIT")
public class TiInterfaceDependScope implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT TI_INTERFACE_INFO_S.nextval FROM DUAL")
	private String seq;
	private String tenor      ;
	private String beginScope ;
	private String endScope   ;
	private String inputTime  ;
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getBeginScope() {
		return beginScope;
	}
	public void setBeginScope(String beginScope) {
		this.beginScope = beginScope;
	}
	public String getEndScope() {
		return endScope;
	}
	public void setEndScope(String endScope) {
		this.endScope = endScope;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
}
