package com.singlee.capital.interfacex.model;

public class Company {
	private String ID;     //支行ID                            
	private String COMPANY_NAME1;     //支行名称                          
	private String COMPANY_NAME2;     //支行名称2                         
	private String NAME_ADDRESS1;     //地址                              
	private String NAME_ADDRESS2;     //地址2                             
	private String NAME_ADDRESS3;     //地址3                             
	private String MNEMONIC;     //简名                              
	private String LANGUAGE_CODE;     //所用语言编号                      
	private String STAT_REP_AREA;     //报表送达区域                      
	private String STAT_REP_NAME;     //报表送达人                        
	private String STAT_REP_DELIV;     //报表送达位置                      
	private String MGMT_REP_AREA;     //管理类报表送达区域                
	private String MGMT_REP_NAME;     //管理类报表送达人                  
	private String MGMT_REP_DELIV;     //管理类报表送达位置                
	private String CONSOLIDATION_MARK;     //支行记录合并标示                  
	private String DEFAULT_NO_OF_AUTH;     //授权人数                          
	private String LOCAL_COUNTRY;     //支行所在国家                      
	private String LOCAL_REGION;     //支行所在地区                      
	private String LOCAL_CURRENCY;     //支行基础币别                      
	private String RATE_SYSTEM;     //默认汇率类型                      
	private String CUSTOMER_COMPANY;     //维护客户文件的支行                
	private String CUSTOMER_MNEMONIC;     //维护客户文件的支行助记名          
	private String DEFAULT_CUST_COM;     //维护此支行客户表文件的支行        
	private String DEFAULT_CUST_MNE;     //维护此支行客户表文件的支行助记码  
	private String DEFAULT_FINAN_COM;     //维护财务信息静态表文件的支行      
	private String DEFAULT_FINAN_MNE;     //维护财务信息静态表文件的支行助记码
	private String CURRENCY_COMPANY;     //维护币别文件的支行                
	private String CURRENCY_MNEMONIC;     //维护币别文件的支行助记码          
	private String FINAN_FINAN_COM;     //维护财务信息表文件的支行          
	private String FINAN_FINAN_MNE;     //维护财务信息表文件的支行助记码    
	private String NOSTRO_COMPANY;     //维护往来账户的支行                
	private String NOSTRO_MNEMONIC;     //维护往来账户的支行助记码          
	private String NOSTRO_SUB_DIV;     //往来支行的下层代码                
	private String INTER_COM_CATEGORY;     //会计分录的分类代码                
	private String BR_ABB_CODE;     //拼音缩写名                        
	private String PAY_DECLR_CODE;     //国际交易支付号                    
	private String DECLARE_BRN;     //安全的支付行                      
	private String CLEARING_CODE;     //清算号                            
	private String CPS_CODE;     //支付系统行号                      
	private String BUSINESS_UNIT;     //业务单元                          
	private String HIGH_LV_CLR_BR;     //上级清算行                        
	private String BRANCH_LEVEL;     //支行等级                          
	private String BRANCH_FUNCTION;     //支行功能                          
	private String HIGHER_LEVEL_BR;     //上级主办行                        
	private String CONTACT_PERSON;     //联系人                            
	private String CONTACT_PHONE;     //联系电话                          
	private String MON_START_TM;     //上午开始营业时间                  
	private String MON_END_TIME;     //上午营业结束时间                  
	private String AFT_START_TM;     //下午开始营业时间                  
	private String AFT_END_TM;     //下午营业结束时间                  
	private String CITY;     //所在城市                          
	private String CLC_REGION;     //交换行号                          
	private String DISTRICT_CODE;     //                                  
	private String BRANCH_STATUS;     //                                  
	private String B_FIN_INST_CODE;     //                                  
	private String PARENT_COMPANY;     //                                  
	private String B_INT_MARKET_RG;     //                                  
	private String COMP_ID;     //                                  
	private String LOCAL_PROCESS_NAME;     //本地央行处理名称                  
	private String DEFAULT_RATE_ALLOW;     //汇率允许的最大差异                
	private String FCY_CONV_TOLER_MIN;     //金额折合时允许的差异              
	private String FCY_CONV_TOLER_MAX;     //金额折合时不允许越权的差异        
	private String CHARGE_CALCULATION;     //计算费用的周期                    
	private String FINANCIAL_YEAR_END;     //年终日                            
	private String INACTIVITY_MONTHS;     //标记为不活动账户的日期            
	private String SUB_DIVISION_CODE;     //支行的下层代码                    
	private String SWIFT_ADD_COM_REF;     //SWIFT地址的6位字符                
	private String LAST_YEAR_END;     //上一个会计年度年终日              
	private String DC_TOLERANCE_RATE;     //自动调整时的可允许偏差率          
	private String DC_MAX_ROUND_AMT;     //调整DATA CAPTURE最大值            
	private String ACCT_CHECKDIG_TYPE;     //支行的校验位计算方法              
	private String ACCOUNT_MASK;     //客户帐户屏蔽位                    
	private String BOOK;     //帐簿号                            
	private String CONS_KEY_CO;     //合并报表的支行代码                
	private String COMPANY_GROUP;     //支行类别                          
	private String OFFICIAL_HOLIDAY;     //官方节假日表                      
	private String BRANCH_HOLIDAY;     //支行的节假日表                    
	private String BATCH_HOLIDAY;     //批量节假日表                      
	private String FINANCIAL_COM;     //所属的财务归宿行                  
	private String FINANCIAL_MNE;     //所属的财务归宿行助记名            
	private String RECORD_STATUS;     //该记录状态                        
	private String CURR_NO;     //记录数                            
	private String AUTHORISER;     //复核者                            
	private String CO_CODE;     //支行号                            
	private String DEPT_CODE;     //部门号                            
	private String AUDITOR_CODE;     //稽核号                            
	private String AUDIT_DATE_TIME;     //稽核时间                          
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getCOMPANY_NAME1() {
		return COMPANY_NAME1;
	}
	public void setCOMPANY_NAME1(String cOMPANY_NAME1) {
		COMPANY_NAME1 = cOMPANY_NAME1;
	}
	public String getCOMPANY_NAME2() {
		return COMPANY_NAME2;
	}
	public void setCOMPANY_NAME2(String cOMPANY_NAME2) {
		COMPANY_NAME2 = cOMPANY_NAME2;
	}
	public String getNAME_ADDRESS1() {
		return NAME_ADDRESS1;
	}
	public void setNAME_ADDRESS1(String nAME_ADDRESS1) {
		NAME_ADDRESS1 = nAME_ADDRESS1;
	}
	public String getNAME_ADDRESS2() {
		return NAME_ADDRESS2;
	}
	public void setNAME_ADDRESS2(String nAME_ADDRESS2) {
		NAME_ADDRESS2 = nAME_ADDRESS2;
	}
	public String getNAME_ADDRESS3() {
		return NAME_ADDRESS3;
	}
	public void setNAME_ADDRESS3(String nAME_ADDRESS3) {
		NAME_ADDRESS3 = nAME_ADDRESS3;
	}
	public String getMNEMONIC() {
		return MNEMONIC;
	}
	public void setMNEMONIC(String mNEMONIC) {
		MNEMONIC = mNEMONIC;
	}
	public String getLANGUAGE_CODE() {
		return LANGUAGE_CODE;
	}
	public void setLANGUAGE_CODE(String lANGUAGE_CODE) {
		LANGUAGE_CODE = lANGUAGE_CODE;
	}
	public String getSTAT_REP_AREA() {
		return STAT_REP_AREA;
	}
	public void setSTAT_REP_AREA(String sTAT_REP_AREA) {
		STAT_REP_AREA = sTAT_REP_AREA;
	}
	public String getSTAT_REP_NAME() {
		return STAT_REP_NAME;
	}
	public void setSTAT_REP_NAME(String sTAT_REP_NAME) {
		STAT_REP_NAME = sTAT_REP_NAME;
	}
	public String getSTAT_REP_DELIV() {
		return STAT_REP_DELIV;
	}
	public void setSTAT_REP_DELIV(String sTAT_REP_DELIV) {
		STAT_REP_DELIV = sTAT_REP_DELIV;
	}
	public String getMGMT_REP_AREA() {
		return MGMT_REP_AREA;
	}
	public void setMGMT_REP_AREA(String mGMT_REP_AREA) {
		MGMT_REP_AREA = mGMT_REP_AREA;
	}
	public String getMGMT_REP_NAME() {
		return MGMT_REP_NAME;
	}
	public void setMGMT_REP_NAME(String mGMT_REP_NAME) {
		MGMT_REP_NAME = mGMT_REP_NAME;
	}
	public String getMGMT_REP_DELIV() {
		return MGMT_REP_DELIV;
	}
	public void setMGMT_REP_DELIV(String mGMT_REP_DELIV) {
		MGMT_REP_DELIV = mGMT_REP_DELIV;
	}
	public String getCONSOLIDATION_MARK() {
		return CONSOLIDATION_MARK;
	}
	public void setCONSOLIDATION_MARK(String cONSOLIDATION_MARK) {
		CONSOLIDATION_MARK = cONSOLIDATION_MARK;
	}
	public String getDEFAULT_NO_OF_AUTH() {
		return DEFAULT_NO_OF_AUTH;
	}
	public void setDEFAULT_NO_OF_AUTH(String dEFAULT_NO_OF_AUTH) {
		DEFAULT_NO_OF_AUTH = dEFAULT_NO_OF_AUTH;
	}
	public String getLOCAL_COUNTRY() {
		return LOCAL_COUNTRY;
	}
	public void setLOCAL_COUNTRY(String lOCAL_COUNTRY) {
		LOCAL_COUNTRY = lOCAL_COUNTRY;
	}
	public String getLOCAL_REGION() {
		return LOCAL_REGION;
	}
	public void setLOCAL_REGION(String lOCAL_REGION) {
		LOCAL_REGION = lOCAL_REGION;
	}
	public String getLOCAL_CURRENCY() {
		return LOCAL_CURRENCY;
	}
	public void setLOCAL_CURRENCY(String lOCAL_CURRENCY) {
		LOCAL_CURRENCY = lOCAL_CURRENCY;
	}
	public String getRATE_SYSTEM() {
		return RATE_SYSTEM;
	}
	public void setRATE_SYSTEM(String rATE_SYSTEM) {
		RATE_SYSTEM = rATE_SYSTEM;
	}
	public String getCUSTOMER_COMPANY() {
		return CUSTOMER_COMPANY;
	}
	public void setCUSTOMER_COMPANY(String cUSTOMER_COMPANY) {
		CUSTOMER_COMPANY = cUSTOMER_COMPANY;
	}
	public String getCUSTOMER_MNEMONIC() {
		return CUSTOMER_MNEMONIC;
	}
	public void setCUSTOMER_MNEMONIC(String cUSTOMER_MNEMONIC) {
		CUSTOMER_MNEMONIC = cUSTOMER_MNEMONIC;
	}
	public String getDEFAULT_CUST_COM() {
		return DEFAULT_CUST_COM;
	}
	public void setDEFAULT_CUST_COM(String dEFAULT_CUST_COM) {
		DEFAULT_CUST_COM = dEFAULT_CUST_COM;
	}
	public String getDEFAULT_CUST_MNE() {
		return DEFAULT_CUST_MNE;
	}
	public void setDEFAULT_CUST_MNE(String dEFAULT_CUST_MNE) {
		DEFAULT_CUST_MNE = dEFAULT_CUST_MNE;
	}
	public String getDEFAULT_FINAN_COM() {
		return DEFAULT_FINAN_COM;
	}
	public void setDEFAULT_FINAN_COM(String dEFAULT_FINAN_COM) {
		DEFAULT_FINAN_COM = dEFAULT_FINAN_COM;
	}
	public String getDEFAULT_FINAN_MNE() {
		return DEFAULT_FINAN_MNE;
	}
	public void setDEFAULT_FINAN_MNE(String dEFAULT_FINAN_MNE) {
		DEFAULT_FINAN_MNE = dEFAULT_FINAN_MNE;
	}
	public String getCURRENCY_COMPANY() {
		return CURRENCY_COMPANY;
	}
	public void setCURRENCY_COMPANY(String cURRENCY_COMPANY) {
		CURRENCY_COMPANY = cURRENCY_COMPANY;
	}
	public String getCURRENCY_MNEMONIC() {
		return CURRENCY_MNEMONIC;
	}
	public void setCURRENCY_MNEMONIC(String cURRENCY_MNEMONIC) {
		CURRENCY_MNEMONIC = cURRENCY_MNEMONIC;
	}
	public String getFINAN_FINAN_COM() {
		return FINAN_FINAN_COM;
	}
	public void setFINAN_FINAN_COM(String fINAN_FINAN_COM) {
		FINAN_FINAN_COM = fINAN_FINAN_COM;
	}
	public String getFINAN_FINAN_MNE() {
		return FINAN_FINAN_MNE;
	}
	public void setFINAN_FINAN_MNE(String fINAN_FINAN_MNE) {
		FINAN_FINAN_MNE = fINAN_FINAN_MNE;
	}
	public String getNOSTRO_COMPANY() {
		return NOSTRO_COMPANY;
	}
	public void setNOSTRO_COMPANY(String nOSTRO_COMPANY) {
		NOSTRO_COMPANY = nOSTRO_COMPANY;
	}
	public String getNOSTRO_MNEMONIC() {
		return NOSTRO_MNEMONIC;
	}
	public void setNOSTRO_MNEMONIC(String nOSTRO_MNEMONIC) {
		NOSTRO_MNEMONIC = nOSTRO_MNEMONIC;
	}
	public String getNOSTRO_SUB_DIV() {
		return NOSTRO_SUB_DIV;
	}
	public void setNOSTRO_SUB_DIV(String nOSTRO_SUB_DIV) {
		NOSTRO_SUB_DIV = nOSTRO_SUB_DIV;
	}
	public String getINTER_COM_CATEGORY() {
		return INTER_COM_CATEGORY;
	}
	public void setINTER_COM_CATEGORY(String iNTER_COM_CATEGORY) {
		INTER_COM_CATEGORY = iNTER_COM_CATEGORY;
	}
	public String getBR_ABB_CODE() {
		return BR_ABB_CODE;
	}
	public void setBR_ABB_CODE(String bR_ABB_CODE) {
		BR_ABB_CODE = bR_ABB_CODE;
	}
	public String getPAY_DECLR_CODE() {
		return PAY_DECLR_CODE;
	}
	public void setPAY_DECLR_CODE(String pAY_DECLR_CODE) {
		PAY_DECLR_CODE = pAY_DECLR_CODE;
	}
	public String getDECLARE_BRN() {
		return DECLARE_BRN;
	}
	public void setDECLARE_BRN(String dECLARE_BRN) {
		DECLARE_BRN = dECLARE_BRN;
	}
	public String getCLEARING_CODE() {
		return CLEARING_CODE;
	}
	public void setCLEARING_CODE(String cLEARING_CODE) {
		CLEARING_CODE = cLEARING_CODE;
	}
	public String getCPS_CODE() {
		return CPS_CODE;
	}
	public void setCPS_CODE(String cPS_CODE) {
		CPS_CODE = cPS_CODE;
	}
	public String getBUSINESS_UNIT() {
		return BUSINESS_UNIT;
	}
	public void setBUSINESS_UNIT(String bUSINESS_UNIT) {
		BUSINESS_UNIT = bUSINESS_UNIT;
	}
	public String getHIGH_LV_CLR_BR() {
		return HIGH_LV_CLR_BR;
	}
	public void setHIGH_LV_CLR_BR(String hIGH_LV_CLR_BR) {
		HIGH_LV_CLR_BR = hIGH_LV_CLR_BR;
	}
	public String getBRANCH_LEVEL() {
		return BRANCH_LEVEL;
	}
	public void setBRANCH_LEVEL(String bRANCH_LEVEL) {
		BRANCH_LEVEL = bRANCH_LEVEL;
	}
	public String getBRANCH_FUNCTION() {
		return BRANCH_FUNCTION;
	}
	public void setBRANCH_FUNCTION(String bRANCH_FUNCTION) {
		BRANCH_FUNCTION = bRANCH_FUNCTION;
	}
	public String getHIGHER_LEVEL_BR() {
		return HIGHER_LEVEL_BR;
	}
	public void setHIGHER_LEVEL_BR(String hIGHER_LEVEL_BR) {
		HIGHER_LEVEL_BR = hIGHER_LEVEL_BR;
	}
	public String getCONTACT_PERSON() {
		return CONTACT_PERSON;
	}
	public void setCONTACT_PERSON(String cONTACT_PERSON) {
		CONTACT_PERSON = cONTACT_PERSON;
	}
	public String getCONTACT_PHONE() {
		return CONTACT_PHONE;
	}
	public void setCONTACT_PHONE(String cONTACT_PHONE) {
		CONTACT_PHONE = cONTACT_PHONE;
	}
	public String getMON_START_TM() {
		return MON_START_TM;
	}
	public void setMON_START_TM(String mON_START_TM) {
		MON_START_TM = mON_START_TM;
	}
	public String getMON_END_TIME() {
		return MON_END_TIME;
	}
	public void setMON_END_TIME(String mON_END_TIME) {
		MON_END_TIME = mON_END_TIME;
	}
	public String getAFT_START_TM() {
		return AFT_START_TM;
	}
	public void setAFT_START_TM(String aFT_START_TM) {
		AFT_START_TM = aFT_START_TM;
	}
	public String getAFT_END_TM() {
		return AFT_END_TM;
	}
	public void setAFT_END_TM(String aFT_END_TM) {
		AFT_END_TM = aFT_END_TM;
	}
	public String getCITY() {
		return CITY;
	}
	public void setCITY(String cITY) {
		CITY = cITY;
	}
	public String getCLC_REGION() {
		return CLC_REGION;
	}
	public void setCLC_REGION(String cLC_REGION) {
		CLC_REGION = cLC_REGION;
	}
	public String getDISTRICT_CODE() {
		return DISTRICT_CODE;
	}
	public void setDISTRICT_CODE(String dISTRICT_CODE) {
		DISTRICT_CODE = dISTRICT_CODE;
	}
	public String getBRANCH_STATUS() {
		return BRANCH_STATUS;
	}
	public void setBRANCH_STATUS(String bRANCH_STATUS) {
		BRANCH_STATUS = bRANCH_STATUS;
	}
	public String getB_FIN_INST_CODE() {
		return B_FIN_INST_CODE;
	}
	public void setB_FIN_INST_CODE(String b_FIN_INST_CODE) {
		B_FIN_INST_CODE = b_FIN_INST_CODE;
	}
	public String getPARENT_COMPANY() {
		return PARENT_COMPANY;
	}
	public void setPARENT_COMPANY(String pARENT_COMPANY) {
		PARENT_COMPANY = pARENT_COMPANY;
	}
	public String getB_INT_MARKET_RG() {
		return B_INT_MARKET_RG;
	}
	public void setB_INT_MARKET_RG(String b_INT_MARKET_RG) {
		B_INT_MARKET_RG = b_INT_MARKET_RG;
	}
	public String getCOMP_ID() {
		return COMP_ID;
	}
	public void setCOMP_ID(String cOMP_ID) {
		COMP_ID = cOMP_ID;
	}
	public String getLOCAL_PROCESS_NAME() {
		return LOCAL_PROCESS_NAME;
	}
	public void setLOCAL_PROCESS_NAME(String lOCAL_PROCESS_NAME) {
		LOCAL_PROCESS_NAME = lOCAL_PROCESS_NAME;
	}
	public String getDEFAULT_RATE_ALLOW() {
		return DEFAULT_RATE_ALLOW;
	}
	public void setDEFAULT_RATE_ALLOW(String dEFAULT_RATE_ALLOW) {
		DEFAULT_RATE_ALLOW = dEFAULT_RATE_ALLOW;
	}
	public String getFCY_CONV_TOLER_MIN() {
		return FCY_CONV_TOLER_MIN;
	}
	public void setFCY_CONV_TOLER_MIN(String fCY_CONV_TOLER_MIN) {
		FCY_CONV_TOLER_MIN = fCY_CONV_TOLER_MIN;
	}
	public String getFCY_CONV_TOLER_MAX() {
		return FCY_CONV_TOLER_MAX;
	}
	public void setFCY_CONV_TOLER_MAX(String fCY_CONV_TOLER_MAX) {
		FCY_CONV_TOLER_MAX = fCY_CONV_TOLER_MAX;
	}
	public String getCHARGE_CALCULATION() {
		return CHARGE_CALCULATION;
	}
	public void setCHARGE_CALCULATION(String cHARGE_CALCULATION) {
		CHARGE_CALCULATION = cHARGE_CALCULATION;
	}
	public String getFINANCIAL_YEAR_END() {
		return FINANCIAL_YEAR_END;
	}
	public void setFINANCIAL_YEAR_END(String fINANCIAL_YEAR_END) {
		FINANCIAL_YEAR_END = fINANCIAL_YEAR_END;
	}
	public String getINACTIVITY_MONTHS() {
		return INACTIVITY_MONTHS;
	}
	public void setINACTIVITY_MONTHS(String iNACTIVITY_MONTHS) {
		INACTIVITY_MONTHS = iNACTIVITY_MONTHS;
	}
	public String getSUB_DIVISION_CODE() {
		return SUB_DIVISION_CODE;
	}
	public void setSUB_DIVISION_CODE(String sUB_DIVISION_CODE) {
		SUB_DIVISION_CODE = sUB_DIVISION_CODE;
	}
	public String getSWIFT_ADD_COM_REF() {
		return SWIFT_ADD_COM_REF;
	}
	public void setSWIFT_ADD_COM_REF(String sWIFT_ADD_COM_REF) {
		SWIFT_ADD_COM_REF = sWIFT_ADD_COM_REF;
	}
	public String getLAST_YEAR_END() {
		return LAST_YEAR_END;
	}
	public void setLAST_YEAR_END(String lAST_YEAR_END) {
		LAST_YEAR_END = lAST_YEAR_END;
	}
	public String getDC_TOLERANCE_RATE() {
		return DC_TOLERANCE_RATE;
	}
	public void setDC_TOLERANCE_RATE(String dC_TOLERANCE_RATE) {
		DC_TOLERANCE_RATE = dC_TOLERANCE_RATE;
	}
	public String getDC_MAX_ROUND_AMT() {
		return DC_MAX_ROUND_AMT;
	}
	public void setDC_MAX_ROUND_AMT(String dC_MAX_ROUND_AMT) {
		DC_MAX_ROUND_AMT = dC_MAX_ROUND_AMT;
	}
	public String getACCT_CHECKDIG_TYPE() {
		return ACCT_CHECKDIG_TYPE;
	}
	public void setACCT_CHECKDIG_TYPE(String aCCT_CHECKDIG_TYPE) {
		ACCT_CHECKDIG_TYPE = aCCT_CHECKDIG_TYPE;
	}
	public String getACCOUNT_MASK() {
		return ACCOUNT_MASK;
	}
	public void setACCOUNT_MASK(String aCCOUNT_MASK) {
		ACCOUNT_MASK = aCCOUNT_MASK;
	}
	public String getBOOK() {
		return BOOK;
	}
	public void setBOOK(String bOOK) {
		BOOK = bOOK;
	}
	public String getCONS_KEY_CO() {
		return CONS_KEY_CO;
	}
	public void setCONS_KEY_CO(String cONS_KEY_CO) {
		CONS_KEY_CO = cONS_KEY_CO;
	}
	public String getCOMPANY_GROUP() {
		return COMPANY_GROUP;
	}
	public void setCOMPANY_GROUP(String cOMPANY_GROUP) {
		COMPANY_GROUP = cOMPANY_GROUP;
	}
	public String getOFFICIAL_HOLIDAY() {
		return OFFICIAL_HOLIDAY;
	}
	public void setOFFICIAL_HOLIDAY(String oFFICIAL_HOLIDAY) {
		OFFICIAL_HOLIDAY = oFFICIAL_HOLIDAY;
	}
	public String getBRANCH_HOLIDAY() {
		return BRANCH_HOLIDAY;
	}
	public void setBRANCH_HOLIDAY(String bRANCH_HOLIDAY) {
		BRANCH_HOLIDAY = bRANCH_HOLIDAY;
	}
	public String getBATCH_HOLIDAY() {
		return BATCH_HOLIDAY;
	}
	public void setBATCH_HOLIDAY(String bATCH_HOLIDAY) {
		BATCH_HOLIDAY = bATCH_HOLIDAY;
	}
	public String getFINANCIAL_COM() {
		return FINANCIAL_COM;
	}
	public void setFINANCIAL_COM(String fINANCIAL_COM) {
		FINANCIAL_COM = fINANCIAL_COM;
	}
	public String getFINANCIAL_MNE() {
		return FINANCIAL_MNE;
	}
	public void setFINANCIAL_MNE(String fINANCIAL_MNE) {
		FINANCIAL_MNE = fINANCIAL_MNE;
	}
	public String getRECORD_STATUS() {
		return RECORD_STATUS;
	}
	public void setRECORD_STATUS(String rECORD_STATUS) {
		RECORD_STATUS = rECORD_STATUS;
	}
	public String getCURR_NO() {
		return CURR_NO;
	}
	public void setCURR_NO(String cURR_NO) {
		CURR_NO = cURR_NO;
	}
	public String getAUTHORISER() {
		return AUTHORISER;
	}
	public void setAUTHORISER(String aUTHORISER) {
		AUTHORISER = aUTHORISER;
	}
	public String getCO_CODE() {
		return CO_CODE;
	}
	public void setCO_CODE(String cO_CODE) {
		CO_CODE = cO_CODE;
	}
	public String getDEPT_CODE() {
		return DEPT_CODE;
	}
	public void setDEPT_CODE(String dEPT_CODE) {
		DEPT_CODE = dEPT_CODE;
	}
	public String getAUDITOR_CODE() {
		return AUDITOR_CODE;
	}
	public void setAUDITOR_CODE(String aUDITOR_CODE) {
		AUDITOR_CODE = aUDITOR_CODE;
	}
	public String getAUDIT_DATE_TIME() {
		return AUDIT_DATE_TIME;
	}
	public void setAUDIT_DATE_TIME(String aUDIT_DATE_TIME) {
		AUDIT_DATE_TIME = aUDIT_DATE_TIME;
	}

	
	
}
