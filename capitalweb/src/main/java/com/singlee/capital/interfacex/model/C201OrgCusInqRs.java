package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for C201OrgCusInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="C201OrgCusInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TranTotalAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AppTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InitAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FinaResources" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BlklstRson" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelType_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBCountryCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ListedCmp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusiTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ImpExpFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpAreaOwn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpPassPeatFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgentName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PurposeScope" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProdtRemark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PerfAcctFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBStreet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TranTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnterpFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddressType_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBTown" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResidenceCountryCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EcgncCtgCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mertype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IndexNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MobilePhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpCloseFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpCreditFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CM_sEdDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegtAssRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenCurrecy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FtaCsmFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InputDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotalCurry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EstateFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnterpriseSize" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GovernMFinance" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBAddressUsageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IndustrialTypeCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InputAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpBnkImptFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpSpclFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MainDept" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegOrg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreditPolicy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CountryFinFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SPName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AnnualRst" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpDespSizeFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpImportFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DrawBackAcctFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FurrEnterpSize" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GrpRelCutFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SwiftBicCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBPostCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EcgncSctCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CapReg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpLmtSizeFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpVipFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReMark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FurzEnterpSize" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InspectionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TechCompFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IfClient" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnterOrganForm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanCardNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EstabDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClrAcctFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BlackLstFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GrpRelCutCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FinanceInstType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AreaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MarketPartType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="C201OrgCusEfLglfRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C201OrgCusEfLglfRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NetAddrRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}NetAddrRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AddrAppRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}AddrAppRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C201OrgCusInqRs", propOrder = { "commonRsHdr", "customer",
		"number", "tranTotalAmt", "companyUrl", "appTime", "initAmt",
		"finaResources", "blklstRson", "commiStatus", "remark1",
		"channelType2", "gbCountryCd", "listedCmp", "busiTypeName", "email",
		"impExpFlg", "corpAreaOwn", "corpPassPeatFlg", "agentName",
		"purposeScope", "prodtRemark", "perfAcctFlg", "gbStreet", "applyType",
		"tranTime", "usage", "openAcctNo", "enterpFlg", "regType", "reason",
		"addressType2", "gbTown", "residenceCountryCd", "ecgncCtgCode",
		"mertype", "indexNo", "mobilePhone", "corpCloseFlg", "corpCreditFlg",
		"cmsEdDay", "regtAssRate", "openCurrecy", "ftaCsmFlg", "inputDt",
		"totalCurry", "estateFlag", "enterpriseSize", "governMFinance",
		"gbAddressUsageType", "industrialTypeCd", "inputAmt", "corpBnkImptFlg",
		"corpSpclFlg", "mainDept", "regOrg", "creditPolicy", "countryFinFlg",
		"spName", "gbShortName", "currency", "annualRst", "corpDespSizeFlg",
		"corpImportFlg", "drawBackAcctFlg", "furrEnterpSize", "bankType",
		"grpRelCutFlg", "swiftBicCode", "gbPostCd", "nationality",
		"ecgncSctCode", "capReg", "beginDt", "corpArea", "corpLmtSizeFlg",
		"corpVipFlg", "reMark", "furzEnterpSize", "inspectionDate",
		"techCompFlag", "ifClient", "enterOrganForm", "loanCardNo",
		"estabDate", "corpFlg", "companyName", "clrAcctFlg", "blackLstFlg",
		"grpRelCutCode", "financeInstType", "areaType", "marketPartType",
		"c201OrgCusEfLglfRec", "netAddrRec", "addrAppRec" })
public class C201OrgCusInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "Number", required = true)
	protected String number;
	@XmlElement(name = "TranTotalAmt", required = true)
	protected String tranTotalAmt;
	@XmlElement(name = "CompanyUrl", required = true)
	protected String companyUrl;
	@XmlElement(name = "AppTime", required = true)
	protected String appTime;
	@XmlElement(name = "InitAmt", required = true)
	protected String initAmt;
	@XmlElement(name = "FinaResources", required = true)
	protected String finaResources;
	@XmlElement(name = "BlklstRson", required = true)
	protected String blklstRson;
	@XmlElement(name = "CommiStatus", required = true)
	protected String commiStatus;
	@XmlElement(name = "Remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "ChannelType_2", required = true)
	protected String channelType2;
	@XmlElement(name = "GBCountryCd", required = true)
	protected String gbCountryCd;
	@XmlElement(name = "ListedCmp", required = true)
	protected String listedCmp;
	@XmlElement(name = "BusiTypeName", required = true)
	protected String busiTypeName;
	@XmlElement(name = "Email", required = true)
	protected String email;
	@XmlElement(name = "ImpExpFlg", required = true)
	protected String impExpFlg;
	@XmlElement(name = "CorpAreaOwn", required = true)
	protected String corpAreaOwn;
	@XmlElement(name = "CorpPassPeatFlg", required = true)
	protected String corpPassPeatFlg;
	@XmlElement(name = "AgentName", required = true)
	protected String agentName;
	@XmlElement(name = "PurposeScope", required = true)
	protected String purposeScope;
	@XmlElement(name = "ProdtRemark", required = true)
	protected String prodtRemark;
	@XmlElement(name = "PerfAcctFlg", required = true)
	protected String perfAcctFlg;
	@XmlElement(name = "GBStreet", required = true)
	protected String gbStreet;
	@XmlElement(name = "ApplyType", required = true)
	protected String applyType;
	@XmlElement(name = "TranTime", required = true)
	protected String tranTime;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "OpenAcctNo", required = true)
	protected String openAcctNo;
	@XmlElement(name = "EnterpFlg", required = true)
	protected String enterpFlg;
	@XmlElement(name = "RegType", required = true)
	protected String regType;
	@XmlElement(name = "Reason", required = true)
	protected String reason;
	@XmlElement(name = "AddressType_2", required = true)
	protected String addressType2;
	@XmlElement(name = "GBTown", required = true)
	protected String gbTown;
	@XmlElement(name = "ResidenceCountryCd", required = true)
	protected String residenceCountryCd;
	@XmlElement(name = "EcgncCtgCode", required = true)
	protected String ecgncCtgCode;
	@XmlElement(name = "Mertype", required = true)
	protected String mertype;
	@XmlElement(name = "IndexNo", required = true)
	protected String indexNo;
	@XmlElement(name = "MobilePhone", required = true)
	protected String mobilePhone;
	@XmlElement(name = "CorpCloseFlg", required = true)
	protected String corpCloseFlg;
	@XmlElement(name = "CorpCreditFlg", required = true)
	protected String corpCreditFlg;
	@XmlElement(name = "CM_sEdDay", required = true)
	protected String cmsEdDay;
	@XmlElement(name = "RegtAssRate", required = true)
	protected String regtAssRate;
	@XmlElement(name = "OpenCurrecy", required = true)
	protected String openCurrecy;
	@XmlElement(name = "FtaCsmFlg", required = true)
	protected String ftaCsmFlg;
	@XmlElement(name = "InputDt", required = true)
	protected String inputDt;
	@XmlElement(name = "TotalCurry", required = true)
	protected String totalCurry;
	@XmlElement(name = "EstateFlag", required = true)
	protected String estateFlag;
	@XmlElement(name = "EnterpriseSize", required = true)
	protected String enterpriseSize;
	@XmlElement(name = "GovernMFinance", required = true)
	protected String governMFinance;
	@XmlElement(name = "GBAddressUsageType", required = true)
	protected String gbAddressUsageType;
	@XmlElement(name = "IndustrialTypeCd", required = true)
	protected String industrialTypeCd;
	@XmlElement(name = "InputAmt", required = true)
	protected String inputAmt;
	@XmlElement(name = "CorpBnkImptFlg", required = true)
	protected String corpBnkImptFlg;
	@XmlElement(name = "CorpSpclFlg", required = true)
	protected String corpSpclFlg;
	@XmlElement(name = "MainDept", required = true)
	protected String mainDept;
	@XmlElement(name = "RegOrg", required = true)
	protected String regOrg;
	@XmlElement(name = "CreditPolicy", required = true)
	protected String creditPolicy;
	@XmlElement(name = "CountryFinFlg", required = true)
	protected String countryFinFlg;
	@XmlElement(name = "SPName", required = true)
	protected String spName;
	@XmlElement(name = "GBShortName", required = true)
	protected String gbShortName;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "AnnualRst", required = true)
	protected String annualRst;
	@XmlElement(name = "CorpDespSizeFlg", required = true)
	protected String corpDespSizeFlg;
	@XmlElement(name = "CorpImportFlg", required = true)
	protected String corpImportFlg;
	@XmlElement(name = "DrawBackAcctFlg", required = true)
	protected String drawBackAcctFlg;
	@XmlElement(name = "FurrEnterpSize", required = true)
	protected String furrEnterpSize;
	@XmlElement(name = "BankType", required = true)
	protected String bankType;
	@XmlElement(name = "GrpRelCutFlg", required = true)
	protected String grpRelCutFlg;
	@XmlElement(name = "SwiftBicCode", required = true)
	protected String swiftBicCode;
	@XmlElement(name = "GBPostCd", required = true)
	protected String gbPostCd;
	@XmlElement(name = "Nationality", required = true)
	protected String nationality;
	@XmlElement(name = "EcgncSctCode", required = true)
	protected String ecgncSctCode;
	@XmlElement(name = "CapReg", required = true)
	protected String capReg;
	@XmlElement(name = "BeginDt", required = true)
	protected String beginDt;
	@XmlElement(name = "CorpArea", required = true)
	protected String corpArea;
	@XmlElement(name = "CorpLmtSizeFlg", required = true)
	protected String corpLmtSizeFlg;
	@XmlElement(name = "CorpVipFlg", required = true)
	protected String corpVipFlg;
	@XmlElement(name = "ReMark", required = true)
	protected String reMark;
	@XmlElement(name = "FurzEnterpSize", required = true)
	protected String furzEnterpSize;
	@XmlElement(name = "InspectionDate", required = true)
	protected String inspectionDate;
	@XmlElement(name = "TechCompFlag", required = true)
	protected String techCompFlag;
	@XmlElement(name = "IfClient", required = true)
	protected String ifClient;
	@XmlElement(name = "EnterOrganForm", required = true)
	protected String enterOrganForm;
	@XmlElement(name = "LoanCardNo", required = true)
	protected String loanCardNo;
	@XmlElement(name = "EstabDate", required = true)
	protected String estabDate;
	@XmlElement(name = "CorpFlg", required = true)
	protected String corpFlg;
	@XmlElement(name = "CompanyName", required = true)
	protected String companyName;
	@XmlElement(name = "ClrAcctFlg", required = true)
	protected String clrAcctFlg;
	@XmlElement(name = "BlackLstFlg", required = true)
	protected String blackLstFlg;
	@XmlElement(name = "GrpRelCutCode", required = true)
	protected String grpRelCutCode;
	@XmlElement(name = "FinanceInstType", required = true)
	protected String financeInstType;
	@XmlElement(name = "AreaType", required = true)
	protected String areaType;
	@XmlElement(name = "MarketPartType", required = true)
	protected String marketPartType;
	@XmlElement(name = "C201OrgCusEfLglfRec")
	protected List<C201OrgCusEfLglfRec> c201OrgCusEfLglfRec;
	@XmlElement(name = "NetAddrRec")
	protected List<NetAddrRec> netAddrRec;
	@XmlElement(name = "AddrAppRec")
	protected List<AddrAppRec> addrAppRec;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the number property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the value of the number property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNumber(String value) {
		this.number = value;
	}

	/**
	 * Gets the value of the tranTotalAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTranTotalAmt() {
		return tranTotalAmt;
	}

	/**
	 * Sets the value of the tranTotalAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTranTotalAmt(String value) {
		this.tranTotalAmt = value;
	}

	/**
	 * Gets the value of the companyUrl property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyUrl() {
		return companyUrl;
	}

	/**
	 * Sets the value of the companyUrl property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyUrl(String value) {
		this.companyUrl = value;
	}

	/**
	 * Gets the value of the appTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAppTime() {
		return appTime;
	}

	/**
	 * Sets the value of the appTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAppTime(String value) {
		this.appTime = value;
	}

	/**
	 * Gets the value of the initAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInitAmt() {
		return initAmt;
	}

	/**
	 * Sets the value of the initAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInitAmt(String value) {
		this.initAmt = value;
	}

	/**
	 * Gets the value of the finaResources property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFinaResources() {
		return finaResources;
	}

	/**
	 * Sets the value of the finaResources property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFinaResources(String value) {
		this.finaResources = value;
	}

	/**
	 * Gets the value of the blklstRson property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBlklstRson() {
		return blklstRson;
	}

	/**
	 * Sets the value of the blklstRson property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBlklstRson(String value) {
		this.blklstRson = value;
	}

	/**
	 * Gets the value of the commiStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommiStatus() {
		return commiStatus;
	}

	/**
	 * Sets the value of the commiStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommiStatus(String value) {
		this.commiStatus = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the channelType2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelType2() {
		return channelType2;
	}

	/**
	 * Sets the value of the channelType2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelType2(String value) {
		this.channelType2 = value;
	}

	/**
	 * Gets the value of the gbCountryCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBCountryCd() {
		return gbCountryCd;
	}

	/**
	 * Sets the value of the gbCountryCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBCountryCd(String value) {
		this.gbCountryCd = value;
	}

	/**
	 * Gets the value of the listedCmp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getListedCmp() {
		return listedCmp;
	}

	/**
	 * Sets the value of the listedCmp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setListedCmp(String value) {
		this.listedCmp = value;
	}

	/**
	 * Gets the value of the busiTypeName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusiTypeName() {
		return busiTypeName;
	}

	/**
	 * Sets the value of the busiTypeName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusiTypeName(String value) {
		this.busiTypeName = value;
	}

	/**
	 * Gets the value of the email property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the value of the email property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEmail(String value) {
		this.email = value;
	}

	/**
	 * Gets the value of the impExpFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImpExpFlg() {
		return impExpFlg;
	}

	/**
	 * Sets the value of the impExpFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImpExpFlg(String value) {
		this.impExpFlg = value;
	}

	/**
	 * Gets the value of the corpAreaOwn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpAreaOwn() {
		return corpAreaOwn;
	}

	/**
	 * Sets the value of the corpAreaOwn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpAreaOwn(String value) {
		this.corpAreaOwn = value;
	}

	/**
	 * Gets the value of the corpPassPeatFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpPassPeatFlg() {
		return corpPassPeatFlg;
	}

	/**
	 * Sets the value of the corpPassPeatFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpPassPeatFlg(String value) {
		this.corpPassPeatFlg = value;
	}

	/**
	 * Gets the value of the agentName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the value of the agentName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgentName(String value) {
		this.agentName = value;
	}

	/**
	 * Gets the value of the purposeScope property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPurposeScope() {
		return purposeScope;
	}

	/**
	 * Sets the value of the purposeScope property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPurposeScope(String value) {
		this.purposeScope = value;
	}

	/**
	 * Gets the value of the prodtRemark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProdtRemark() {
		return prodtRemark;
	}

	/**
	 * Sets the value of the prodtRemark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProdtRemark(String value) {
		this.prodtRemark = value;
	}

	/**
	 * Gets the value of the perfAcctFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPerfAcctFlg() {
		return perfAcctFlg;
	}

	/**
	 * Sets the value of the perfAcctFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPerfAcctFlg(String value) {
		this.perfAcctFlg = value;
	}

	/**
	 * Gets the value of the gbStreet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBStreet() {
		return gbStreet;
	}

	/**
	 * Sets the value of the gbStreet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBStreet(String value) {
		this.gbStreet = value;
	}

	/**
	 * Gets the value of the applyType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getApplyType() {
		return applyType;
	}

	/**
	 * Sets the value of the applyType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setApplyType(String value) {
		this.applyType = value;
	}

	/**
	 * Gets the value of the tranTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTranTime() {
		return tranTime;
	}

	/**
	 * Sets the value of the tranTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTranTime(String value) {
		this.tranTime = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the openAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenAcctNo() {
		return openAcctNo;
	}

	/**
	 * Sets the value of the openAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenAcctNo(String value) {
		this.openAcctNo = value;
	}

	/**
	 * Gets the value of the enterpFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnterpFlg() {
		return enterpFlg;
	}

	/**
	 * Sets the value of the enterpFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnterpFlg(String value) {
		this.enterpFlg = value;
	}

	/**
	 * Gets the value of the regType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegType() {
		return regType;
	}

	/**
	 * Sets the value of the regType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegType(String value) {
		this.regType = value;
	}

	/**
	 * Gets the value of the reason property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Sets the value of the reason property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReason(String value) {
		this.reason = value;
	}

	/**
	 * Gets the value of the addressType2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressType2() {
		return addressType2;
	}

	/**
	 * Sets the value of the addressType2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressType2(String value) {
		this.addressType2 = value;
	}

	/**
	 * Gets the value of the gbTown property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBTown() {
		return gbTown;
	}

	/**
	 * Sets the value of the gbTown property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBTown(String value) {
		this.gbTown = value;
	}

	/**
	 * Gets the value of the residenceCountryCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResidenceCountryCd() {
		return residenceCountryCd;
	}

	/**
	 * Sets the value of the residenceCountryCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResidenceCountryCd(String value) {
		this.residenceCountryCd = value;
	}

	/**
	 * Gets the value of the ecgncCtgCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEcgncCtgCode() {
		return ecgncCtgCode;
	}

	/**
	 * Sets the value of the ecgncCtgCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEcgncCtgCode(String value) {
		this.ecgncCtgCode = value;
	}

	/**
	 * Gets the value of the mertype property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMertype() {
		return mertype;
	}

	/**
	 * Sets the value of the mertype property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMertype(String value) {
		this.mertype = value;
	}

	/**
	 * Gets the value of the indexNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndexNo() {
		return indexNo;
	}

	/**
	 * Sets the value of the indexNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndexNo(String value) {
		this.indexNo = value;
	}

	/**
	 * Gets the value of the mobilePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * Sets the value of the mobilePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMobilePhone(String value) {
		this.mobilePhone = value;
	}

	/**
	 * Gets the value of the corpCloseFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpCloseFlg() {
		return corpCloseFlg;
	}

	/**
	 * Sets the value of the corpCloseFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpCloseFlg(String value) {
		this.corpCloseFlg = value;
	}

	/**
	 * Gets the value of the corpCreditFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpCreditFlg() {
		return corpCreditFlg;
	}

	/**
	 * Sets the value of the corpCreditFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpCreditFlg(String value) {
		this.corpCreditFlg = value;
	}

	/**
	 * Gets the value of the cmsEdDay property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCMSEdDay() {
		return cmsEdDay;
	}

	/**
	 * Sets the value of the cmsEdDay property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCMSEdDay(String value) {
		this.cmsEdDay = value;
	}

	/**
	 * Gets the value of the regtAssRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegtAssRate() {
		return regtAssRate;
	}

	/**
	 * Sets the value of the regtAssRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegtAssRate(String value) {
		this.regtAssRate = value;
	}

	/**
	 * Gets the value of the openCurrecy property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenCurrecy() {
		return openCurrecy;
	}

	/**
	 * Sets the value of the openCurrecy property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenCurrecy(String value) {
		this.openCurrecy = value;
	}

	/**
	 * Gets the value of the ftaCsmFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFtaCsmFlg() {
		return ftaCsmFlg;
	}

	/**
	 * Sets the value of the ftaCsmFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFtaCsmFlg(String value) {
		this.ftaCsmFlg = value;
	}

	/**
	 * Gets the value of the inputDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInputDt() {
		return inputDt;
	}

	/**
	 * Sets the value of the inputDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInputDt(String value) {
		this.inputDt = value;
	}

	/**
	 * Gets the value of the totalCurry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotalCurry() {
		return totalCurry;
	}

	/**
	 * Sets the value of the totalCurry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotalCurry(String value) {
		this.totalCurry = value;
	}

	/**
	 * Gets the value of the estateFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEstateFlag() {
		return estateFlag;
	}

	/**
	 * Sets the value of the estateFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEstateFlag(String value) {
		this.estateFlag = value;
	}

	/**
	 * Gets the value of the enterpriseSize property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnterpriseSize() {
		return enterpriseSize;
	}

	/**
	 * Sets the value of the enterpriseSize property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnterpriseSize(String value) {
		this.enterpriseSize = value;
	}

	/**
	 * Gets the value of the governMFinance property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGovernMFinance() {
		return governMFinance;
	}

	/**
	 * Sets the value of the governMFinance property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGovernMFinance(String value) {
		this.governMFinance = value;
	}

	/**
	 * Gets the value of the gbAddressUsageType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBAddressUsageType() {
		return gbAddressUsageType;
	}

	/**
	 * Sets the value of the gbAddressUsageType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBAddressUsageType(String value) {
		this.gbAddressUsageType = value;
	}

	/**
	 * Gets the value of the industrialTypeCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndustrialTypeCd() {
		return industrialTypeCd;
	}

	/**
	 * Sets the value of the industrialTypeCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndustrialTypeCd(String value) {
		this.industrialTypeCd = value;
	}

	/**
	 * Gets the value of the inputAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInputAmt() {
		return inputAmt;
	}

	/**
	 * Sets the value of the inputAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInputAmt(String value) {
		this.inputAmt = value;
	}

	/**
	 * Gets the value of the corpBnkImptFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpBnkImptFlg() {
		return corpBnkImptFlg;
	}

	/**
	 * Sets the value of the corpBnkImptFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpBnkImptFlg(String value) {
		this.corpBnkImptFlg = value;
	}

	/**
	 * Gets the value of the corpSpclFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpSpclFlg() {
		return corpSpclFlg;
	}

	/**
	 * Sets the value of the corpSpclFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpSpclFlg(String value) {
		this.corpSpclFlg = value;
	}

	/**
	 * Gets the value of the mainDept property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMainDept() {
		return mainDept;
	}

	/**
	 * Sets the value of the mainDept property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMainDept(String value) {
		this.mainDept = value;
	}

	/**
	 * Gets the value of the regOrg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegOrg() {
		return regOrg;
	}

	/**
	 * Sets the value of the regOrg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegOrg(String value) {
		this.regOrg = value;
	}

	/**
	 * Gets the value of the creditPolicy property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditPolicy() {
		return creditPolicy;
	}

	/**
	 * Sets the value of the creditPolicy property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditPolicy(String value) {
		this.creditPolicy = value;
	}

	/**
	 * Gets the value of the countryFinFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCountryFinFlg() {
		return countryFinFlg;
	}

	/**
	 * Sets the value of the countryFinFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCountryFinFlg(String value) {
		this.countryFinFlg = value;
	}

	/**
	 * Gets the value of the spName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPName() {
		return spName;
	}

	/**
	 * Sets the value of the spName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPName(String value) {
		this.spName = value;
	}

	/**
	 * Gets the value of the gbShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBShortName() {
		return gbShortName;
	}

	/**
	 * Sets the value of the gbShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBShortName(String value) {
		this.gbShortName = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the annualRst property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAnnualRst() {
		return annualRst;
	}

	/**
	 * Sets the value of the annualRst property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAnnualRst(String value) {
		this.annualRst = value;
	}

	/**
	 * Gets the value of the corpDespSizeFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpDespSizeFlg() {
		return corpDespSizeFlg;
	}

	/**
	 * Sets the value of the corpDespSizeFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpDespSizeFlg(String value) {
		this.corpDespSizeFlg = value;
	}

	/**
	 * Gets the value of the corpImportFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpImportFlg() {
		return corpImportFlg;
	}

	/**
	 * Sets the value of the corpImportFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpImportFlg(String value) {
		this.corpImportFlg = value;
	}

	/**
	 * Gets the value of the drawBackAcctFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDrawBackAcctFlg() {
		return drawBackAcctFlg;
	}

	/**
	 * Sets the value of the drawBackAcctFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDrawBackAcctFlg(String value) {
		this.drawBackAcctFlg = value;
	}

	/**
	 * Gets the value of the furrEnterpSize property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFurrEnterpSize() {
		return furrEnterpSize;
	}

	/**
	 * Sets the value of the furrEnterpSize property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFurrEnterpSize(String value) {
		this.furrEnterpSize = value;
	}

	/**
	 * Gets the value of the bankType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankType() {
		return bankType;
	}

	/**
	 * Sets the value of the bankType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankType(String value) {
		this.bankType = value;
	}

	/**
	 * Gets the value of the grpRelCutFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGrpRelCutFlg() {
		return grpRelCutFlg;
	}

	/**
	 * Sets the value of the grpRelCutFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGrpRelCutFlg(String value) {
		this.grpRelCutFlg = value;
	}

	/**
	 * Gets the value of the swiftBicCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSwiftBicCode() {
		return swiftBicCode;
	}

	/**
	 * Sets the value of the swiftBicCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSwiftBicCode(String value) {
		this.swiftBicCode = value;
	}

	/**
	 * Gets the value of the gbPostCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBPostCd() {
		return gbPostCd;
	}

	/**
	 * Sets the value of the gbPostCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBPostCd(String value) {
		this.gbPostCd = value;
	}

	/**
	 * Gets the value of the nationality property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * Sets the value of the nationality property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNationality(String value) {
		this.nationality = value;
	}

	/**
	 * Gets the value of the ecgncSctCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEcgncSctCode() {
		return ecgncSctCode;
	}

	/**
	 * Sets the value of the ecgncSctCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEcgncSctCode(String value) {
		this.ecgncSctCode = value;
	}

	/**
	 * Gets the value of the capReg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCapReg() {
		return capReg;
	}

	/**
	 * Sets the value of the capReg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCapReg(String value) {
		this.capReg = value;
	}

	/**
	 * Gets the value of the beginDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDt() {
		return beginDt;
	}

	/**
	 * Sets the value of the beginDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDt(String value) {
		this.beginDt = value;
	}

	/**
	 * Gets the value of the corpArea property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpArea() {
		return corpArea;
	}

	/**
	 * Sets the value of the corpArea property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpArea(String value) {
		this.corpArea = value;
	}

	/**
	 * Gets the value of the corpLmtSizeFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpLmtSizeFlg() {
		return corpLmtSizeFlg;
	}

	/**
	 * Sets the value of the corpLmtSizeFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpLmtSizeFlg(String value) {
		this.corpLmtSizeFlg = value;
	}

	/**
	 * Gets the value of the corpVipFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpVipFlg() {
		return corpVipFlg;
	}

	/**
	 * Sets the value of the corpVipFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpVipFlg(String value) {
		this.corpVipFlg = value;
	}

	/**
	 * Gets the value of the reMark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReMark() {
		return reMark;
	}

	/**
	 * Sets the value of the reMark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReMark(String value) {
		this.reMark = value;
	}

	/**
	 * Gets the value of the furzEnterpSize property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFurzEnterpSize() {
		return furzEnterpSize;
	}

	/**
	 * Sets the value of the furzEnterpSize property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFurzEnterpSize(String value) {
		this.furzEnterpSize = value;
	}

	/**
	 * Gets the value of the inspectionDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * Sets the value of the inspectionDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInspectionDate(String value) {
		this.inspectionDate = value;
	}

	/**
	 * Gets the value of the techCompFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTechCompFlag() {
		return techCompFlag;
	}

	/**
	 * Sets the value of the techCompFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTechCompFlag(String value) {
		this.techCompFlag = value;
	}

	/**
	 * Gets the value of the ifClient property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIfClient() {
		return ifClient;
	}

	/**
	 * Sets the value of the ifClient property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIfClient(String value) {
		this.ifClient = value;
	}

	/**
	 * Gets the value of the enterOrganForm property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnterOrganForm() {
		return enterOrganForm;
	}

	/**
	 * Sets the value of the enterOrganForm property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnterOrganForm(String value) {
		this.enterOrganForm = value;
	}

	/**
	 * Gets the value of the loanCardNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanCardNo() {
		return loanCardNo;
	}

	/**
	 * Sets the value of the loanCardNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanCardNo(String value) {
		this.loanCardNo = value;
	}

	/**
	 * Gets the value of the estabDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEstabDate() {
		return estabDate;
	}

	/**
	 * Sets the value of the estabDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEstabDate(String value) {
		this.estabDate = value;
	}

	/**
	 * Gets the value of the corpFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpFlg() {
		return corpFlg;
	}

	/**
	 * Sets the value of the corpFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpFlg(String value) {
		this.corpFlg = value;
	}

	/**
	 * Gets the value of the companyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the value of the companyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyName(String value) {
		this.companyName = value;
	}

	/**
	 * Gets the value of the clrAcctFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClrAcctFlg() {
		return clrAcctFlg;
	}

	/**
	 * Sets the value of the clrAcctFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClrAcctFlg(String value) {
		this.clrAcctFlg = value;
	}

	/**
	 * Gets the value of the blackLstFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBlackLstFlg() {
		return blackLstFlg;
	}

	/**
	 * Sets the value of the blackLstFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBlackLstFlg(String value) {
		this.blackLstFlg = value;
	}

	/**
	 * Gets the value of the grpRelCutCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGrpRelCutCode() {
		return grpRelCutCode;
	}

	/**
	 * Sets the value of the grpRelCutCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGrpRelCutCode(String value) {
		this.grpRelCutCode = value;
	}

	/**
	 * Gets the value of the financeInstType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFinanceInstType() {
		return financeInstType;
	}

	/**
	 * Sets the value of the financeInstType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFinanceInstType(String value) {
		this.financeInstType = value;
	}

	/**
	 * Gets the value of the areaType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAreaType() {
		return areaType;
	}

	/**
	 * Sets the value of the areaType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAreaType(String value) {
		this.areaType = value;
	}

	/**
	 * Gets the value of the marketPartType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMarketPartType() {
		return marketPartType;
	}

	/**
	 * Sets the value of the marketPartType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMarketPartType(String value) {
		this.marketPartType = value;
	}

	/**
	 * Gets the value of the c201OrgCusEfLglfRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the c201OrgCusEfLglfRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getC201OrgCusEfLglfRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link C201OrgCusEfLglfRec }
	 * 
	 * 
	 */
	public List<C201OrgCusEfLglfRec> getC201OrgCusEfLglfRec() {
		if (c201OrgCusEfLglfRec == null) {
			c201OrgCusEfLglfRec = new ArrayList<C201OrgCusEfLglfRec>();
		}
		return this.c201OrgCusEfLglfRec;
	}

	/**
	 * Gets the value of the netAddrRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the netAddrRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getNetAddrRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link NetAddrRec }
	 * 
	 * 
	 */
	public List<NetAddrRec> getNetAddrRec() {
		if (netAddrRec == null) {
			netAddrRec = new ArrayList<NetAddrRec>();
		}
		return this.netAddrRec;
	}

	/**
	 * Gets the value of the addrAppRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the addrAppRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAddrAppRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AddrAppRec }
	 * 
	 * 
	 */
	public List<AddrAppRec> getAddrAppRec() {
		if (addrAppRec == null) {
			addrAppRec = new ArrayList<AddrAppRec>();
		}
		return this.addrAppRec;
	}

}
