package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ShrhldRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ShrhldRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShrhldName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RefNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShrhldCnty" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PerShrOwn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShrhldRec", propOrder = { "shrhldName", "refNo", "shrhldCnty",
		"custId", "perShrOwn" })
public class ShrhldRec {

	@XmlElement(name = "ShrhldName", required = true)
	protected String shrhldName;
	@XmlElement(name = "RefNo", required = true)
	protected String refNo;
	@XmlElement(name = "ShrhldCnty", required = true)
	protected String shrhldCnty;
	@XmlElement(name = "CustId", required = true)
	protected String custId;
	@XmlElement(name = "PerShrOwn", required = true)
	protected String perShrOwn;

	/**
	 * Gets the value of the shrhldName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShrhldName() {
		return shrhldName;
	}

	/**
	 * Sets the value of the shrhldName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShrhldName(String value) {
		this.shrhldName = value;
	}

	/**
	 * Gets the value of the refNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * Sets the value of the refNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRefNo(String value) {
		this.refNo = value;
	}

	/**
	 * Gets the value of the shrhldCnty property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShrhldCnty() {
		return shrhldCnty;
	}

	/**
	 * Sets the value of the shrhldCnty property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShrhldCnty(String value) {
		this.shrhldCnty = value;
	}

	/**
	 * Gets the value of the custId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustId() {
		return custId;
	}

	/**
	 * Sets the value of the custId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustId(String value) {
		this.custId = value;
	}

	/**
	 * Gets the value of the perShrOwn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPerShrOwn() {
		return perShrOwn;
	}

	/**
	 * Sets the value of the perShrOwn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPerShrOwn(String value) {
		this.perShrOwn = value;
	}

}
