package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BenCustRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="BenCustRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BenCustomer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BenCustRec", propOrder = { "benCustomer" })
public class BenCustRec {

	@XmlElement(name = "BenCustomer", required = true)
	protected String benCustomer;

	/**
	 * Gets the value of the benCustomer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBenCustomer() {
		return benCustomer;
	}

	/**
	 * Sets the value of the benCustomer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBenCustomer(String value) {
		this.benCustomer = value;
	}

}
