package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcctTypeInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcctTypeInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="MediumType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CateGory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctOfficerCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PostingRestrict" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CashTxnFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SealPwd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpeningDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DepValDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaturityDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcCharacter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CharDiff" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExchangeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PassbookNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DEPProd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StockNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RtFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InactivMarker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenClearedBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OnlineActualBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OnLineClearedBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkingBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LockedAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AltAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MasterAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubCdFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardProduct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BlklstType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExpiryMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenComp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LastBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrsnLegalType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndDt_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectDt_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CloseRestrict" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PBOCStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcEffDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IndentityClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NAcCharacter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Flag_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcySpeActype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PwdStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LglExpDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbisAcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OtherBankNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OtherCrcdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EacctMobile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BlockFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EacctStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrgPwdFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SettelBankName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WaiveLedgerFee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChequesAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SafeDepEnd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrvAcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccPro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RolloverFlage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubPrd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClcRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClcRegionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubOcbTot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NraFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SsfaFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IccdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubWorkbalTot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardOpenDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CdOpenComname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VirtAccr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VirtAcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctPbook" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SignCardFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctLevelFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustName1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TrdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BenpAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VirtualFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WithDrawLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WithDrowCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcctTypeInqRs", propOrder = { "commonRsHdr", "mediumType",
		"acctNo", "custId", "cateGory", "acctName", "shortName", "currency",
		"acctOfficerCd", "postingRestrict", "acctType", "cashTxnFlg",
		"sealPwd", "openingDate", "depValDate", "term", "maturityDate",
		"acCharacter", "charDiff", "exchangeNo", "passbookNo", "cardNo",
		"mediumStatus", "fcyType", "depProd", "stockNumber", "rtFlag",
		"idType", "idNo", "inactivMarker", "openClearedBal", "onlineActualBal",
		"onLineClearedBal", "workingBal", "lockedAmt", "altAcctNo",
		"masterAcctNo", "companyCode", "cardSector", "subCdFlag",
		"cardProduct", "blklstType", "expiryMonth", "number", "openComp",
		"lastBal", "loanAcctFlag", "prsnLegalType", "flag", "endDt",
		"effectDt", "endDt1", "effectDt1", "acctType1", "closeRestrict",
		"pbocStatus", "acEffDt", "indentityClass", "nationality", "custLevel",
		"nAcCharacter", "flag1", "fcySpeActype", "pwdStatus", "lglExpDate",
		"fbisAcctType", "otherBankNo", "otherCrcdNo", "eacctMobile", "msgFlg",
		"blockFlag", "mediumFlag", "eacctStatus", "orgPwdFlg", "agtName",
		"agtIdType", "agtId", "settelBankName", "payAcctNo", "waiveLedgerFee",
		"chequesAllowed", "safeDepEnd", "prvAcctType", "accPro",
		"rolloverFlage", "intRate", "subPrd", "clcRegion", "clcRegionName",
		"subOcbTot", "nraFlag", "coName", "ssfaFlag", "accountingType",
		"iccdType", "subWorkbalTot", "cardOpenDt", "cdOpenComname", "virtAccr",
		"virtAcctName", "acctPbook", "acctCard", "signCardFlg", "acctLevelFlg",
		"custName1", "trdCode", "benpAcct", "virtualFlag", "withDrawLimit",
		"withDrowCnt" })
public class TDpAcctTypeInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "MediumType", required = true)
	protected String mediumType;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "CustId", required = true)
	protected String custId;
	@XmlElement(name = "CateGory", required = true)
	protected String cateGory;
	@XmlElement(name = "AcctName", required = true)
	protected String acctName;
	@XmlElement(name = "ShortName", required = true)
	protected String shortName;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "AcctOfficerCd", required = true)
	protected String acctOfficerCd;
	@XmlElement(name = "PostingRestrict", required = true)
	protected String postingRestrict;
	@XmlElement(name = "AcctType", required = true)
	protected String acctType;
	@XmlElement(name = "CashTxnFlg", required = true)
	protected String cashTxnFlg;
	@XmlElement(name = "SealPwd", required = true)
	protected String sealPwd;
	@XmlElement(name = "OpeningDate", required = true)
	protected String openingDate;
	@XmlElement(name = "DepValDate", required = true)
	protected String depValDate;
	@XmlElement(name = "Term", required = true)
	protected String term;
	@XmlElement(name = "MaturityDate", required = true)
	protected String maturityDate;
	@XmlElement(name = "AcCharacter", required = true)
	protected String acCharacter;
	@XmlElement(name = "CharDiff", required = true)
	protected String charDiff;
	@XmlElement(name = "ExchangeNo", required = true)
	protected String exchangeNo;
	@XmlElement(name = "PassbookNo", required = true)
	protected String passbookNo;
	@XmlElement(name = "CardNo", required = true)
	protected String cardNo;
	@XmlElement(name = "MediumStatus", required = true)
	protected String mediumStatus;
	@XmlElement(name = "FcyType", required = true)
	protected String fcyType;
	@XmlElement(name = "DEPProd", required = true)
	protected String depProd;
	@XmlElement(name = "StockNumber", required = true)
	protected String stockNumber;
	@XmlElement(name = "RtFlag", required = true)
	protected String rtFlag;
	@XmlElement(name = "IdType", required = true)
	protected String idType;
	@XmlElement(name = "IdNo", required = true)
	protected String idNo;
	@XmlElement(name = "InactivMarker", required = true)
	protected String inactivMarker;
	@XmlElement(name = "OpenClearedBal", required = true)
	protected String openClearedBal;
	@XmlElement(name = "OnlineActualBal", required = true)
	protected String onlineActualBal;
	@XmlElement(name = "OnLineClearedBal", required = true)
	protected String onLineClearedBal;
	@XmlElement(name = "WorkingBal", required = true)
	protected String workingBal;
	@XmlElement(name = "LockedAmt", required = true)
	protected String lockedAmt;
	@XmlElement(name = "AltAcctNo", required = true)
	protected String altAcctNo;
	@XmlElement(name = "MasterAcctNo", required = true)
	protected String masterAcctNo;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;
	@XmlElement(name = "CardSector", required = true)
	protected String cardSector;
	@XmlElement(name = "SubCdFlag", required = true)
	protected String subCdFlag;
	@XmlElement(name = "CardProduct", required = true)
	protected String cardProduct;
	@XmlElement(name = "BlklstType", required = true)
	protected String blklstType;
	@XmlElement(name = "ExpiryMonth", required = true)
	protected String expiryMonth;
	@XmlElement(name = "Number", required = true)
	protected String number;
	@XmlElement(name = "OpenComp", required = true)
	protected String openComp;
	@XmlElement(name = "LastBal", required = true)
	protected String lastBal;
	@XmlElement(name = "LoanAcctFlag", required = true)
	protected String loanAcctFlag;
	@XmlElement(name = "PrsnLegalType", required = true)
	protected String prsnLegalType;
	@XmlElement(name = "Flag", required = true)
	protected String flag;
	@XmlElement(name = "EndDt", required = true)
	protected String endDt;
	@XmlElement(name = "EffectDt", required = true)
	protected String effectDt;
	@XmlElement(name = "EndDt_1", required = true)
	protected String endDt1;
	@XmlElement(name = "EffectDt_1", required = true)
	protected String effectDt1;
	@XmlElement(name = "AcctType_1", required = true)
	protected String acctType1;
	@XmlElement(name = "CloseRestrict", required = true)
	protected String closeRestrict;
	@XmlElement(name = "PBOCStatus", required = true)
	protected String pbocStatus;
	@XmlElement(name = "AcEffDt", required = true)
	protected String acEffDt;
	@XmlElement(name = "IndentityClass", required = true)
	protected String indentityClass;
	@XmlElement(name = "Nationality", required = true)
	protected String nationality;
	@XmlElement(name = "CustLevel", required = true)
	protected String custLevel;
	@XmlElement(name = "NAcCharacter", required = true)
	protected String nAcCharacter;
	@XmlElement(name = "Flag_1", required = true)
	protected String flag1;
	@XmlElement(name = "FcySpeActype", required = true)
	protected String fcySpeActype;
	@XmlElement(name = "PwdStatus", required = true)
	protected String pwdStatus;
	@XmlElement(name = "LglExpDate", required = true)
	protected String lglExpDate;
	@XmlElement(name = "FbisAcctType", required = true)
	protected String fbisAcctType;
	@XmlElement(name = "OtherBankNo", required = true)
	protected String otherBankNo;
	@XmlElement(name = "OtherCrcdNo", required = true)
	protected String otherCrcdNo;
	@XmlElement(name = "EacctMobile", required = true)
	protected String eacctMobile;
	@XmlElement(name = "MsgFlg", required = true)
	protected String msgFlg;
	@XmlElement(name = "BlockFlag", required = true)
	protected String blockFlag;
	@XmlElement(name = "MediumFlag", required = true)
	protected String mediumFlag;
	@XmlElement(name = "EacctStatus", required = true)
	protected String eacctStatus;
	@XmlElement(name = "OrgPwdFlg", required = true)
	protected String orgPwdFlg;
	@XmlElement(name = "AgtName", required = true)
	protected String agtName;
	@XmlElement(name = "AgtIdType", required = true)
	protected String agtIdType;
	@XmlElement(name = "AgtId", required = true)
	protected String agtId;
	@XmlElement(name = "SettelBankName", required = true)
	protected String settelBankName;
	@XmlElement(name = "PayAcctNo", required = true)
	protected String payAcctNo;
	@XmlElement(name = "WaiveLedgerFee", required = true)
	protected String waiveLedgerFee;
	@XmlElement(name = "ChequesAllowed", required = true)
	protected String chequesAllowed;
	@XmlElement(name = "SafeDepEnd", required = true)
	protected String safeDepEnd;
	@XmlElement(name = "PrvAcctType", required = true)
	protected String prvAcctType;
	@XmlElement(name = "AccPro", required = true)
	protected String accPro;
	@XmlElement(name = "RolloverFlage", required = true)
	protected String rolloverFlage;
	@XmlElement(name = "IntRate", required = true)
	protected String intRate;
	@XmlElement(name = "SubPrd", required = true)
	protected String subPrd;
	@XmlElement(name = "ClcRegion", required = true)
	protected String clcRegion;
	@XmlElement(name = "ClcRegionName", required = true)
	protected String clcRegionName;
	@XmlElement(name = "SubOcbTot", required = true)
	protected String subOcbTot;
	@XmlElement(name = "NraFlag", required = true)
	protected String nraFlag;
	@XmlElement(name = "CoName", required = true)
	protected String coName;
	@XmlElement(name = "SsfaFlag", required = true)
	protected String ssfaFlag;
	@XmlElement(name = "AccountingType", required = true)
	protected String accountingType;
	@XmlElement(name = "IccdType", required = true)
	protected String iccdType;
	@XmlElement(name = "SubWorkbalTot", required = true)
	protected String subWorkbalTot;
	@XmlElement(name = "CardOpenDt", required = true)
	protected String cardOpenDt;
	@XmlElement(name = "CdOpenComname", required = true)
	protected String cdOpenComname;
	@XmlElement(name = "VirtAccr", required = true)
	protected String virtAccr;
	@XmlElement(name = "VirtAcctName", required = true)
	protected String virtAcctName;
	@XmlElement(name = "AcctPbook", required = true)
	protected String acctPbook;
	@XmlElement(name = "AcctCard", required = true)
	protected String acctCard;
	@XmlElement(name = "SignCardFlg", required = true)
	protected String signCardFlg;
	@XmlElement(name = "AcctLevelFlg", required = true)
	protected String acctLevelFlg;
	@XmlElement(name = "CustName1", required = true)
	protected String custName1;
	@XmlElement(name = "TrdCode", required = true)
	protected String trdCode;
	@XmlElement(name = "BenpAcct", required = true)
	protected String benpAcct;
	@XmlElement(name = "VirtualFlag", required = true)
	protected String virtualFlag;
	@XmlElement(name = "WithDrawLimit", required = true)
	protected String withDrawLimit;
	@XmlElement(name = "WithDrowCnt", required = true)
	protected String withDrowCnt;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the mediumType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumType() {
		return mediumType;
	}

	/**
	 * Sets the value of the mediumType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumType(String value) {
		this.mediumType = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the custId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustId() {
		return custId;
	}

	/**
	 * Sets the value of the custId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustId(String value) {
		this.custId = value;
	}

	/**
	 * Gets the value of the cateGory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCateGory() {
		return cateGory;
	}

	/**
	 * Sets the value of the cateGory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCateGory(String value) {
		this.cateGory = value;
	}

	/**
	 * Gets the value of the acctName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * Sets the value of the acctName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctName(String value) {
		this.acctName = value;
	}

	/**
	 * Gets the value of the shortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the value of the shortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShortName(String value) {
		this.shortName = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the acctOfficerCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctOfficerCd() {
		return acctOfficerCd;
	}

	/**
	 * Sets the value of the acctOfficerCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctOfficerCd(String value) {
		this.acctOfficerCd = value;
	}

	/**
	 * Gets the value of the postingRestrict property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostingRestrict() {
		return postingRestrict;
	}

	/**
	 * Sets the value of the postingRestrict property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostingRestrict(String value) {
		this.postingRestrict = value;
	}

	/**
	 * Gets the value of the acctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType() {
		return acctType;
	}

	/**
	 * Sets the value of the acctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType(String value) {
		this.acctType = value;
	}

	/**
	 * Gets the value of the cashTxnFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCashTxnFlg() {
		return cashTxnFlg;
	}

	/**
	 * Sets the value of the cashTxnFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCashTxnFlg(String value) {
		this.cashTxnFlg = value;
	}

	/**
	 * Gets the value of the sealPwd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSealPwd() {
		return sealPwd;
	}

	/**
	 * Sets the value of the sealPwd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSealPwd(String value) {
		this.sealPwd = value;
	}

	/**
	 * Gets the value of the openingDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpeningDate() {
		return openingDate;
	}

	/**
	 * Sets the value of the openingDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpeningDate(String value) {
		this.openingDate = value;
	}

	/**
	 * Gets the value of the depValDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDepValDate() {
		return depValDate;
	}

	/**
	 * Sets the value of the depValDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDepValDate(String value) {
		this.depValDate = value;
	}

	/**
	 * Gets the value of the term property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the value of the term property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTerm(String value) {
		this.term = value;
	}

	/**
	 * Gets the value of the maturityDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaturityDate() {
		return maturityDate;
	}

	/**
	 * Sets the value of the maturityDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaturityDate(String value) {
		this.maturityDate = value;
	}

	/**
	 * Gets the value of the acCharacter property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcCharacter() {
		return acCharacter;
	}

	/**
	 * Sets the value of the acCharacter property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcCharacter(String value) {
		this.acCharacter = value;
	}

	/**
	 * Gets the value of the charDiff property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCharDiff() {
		return charDiff;
	}

	/**
	 * Sets the value of the charDiff property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCharDiff(String value) {
		this.charDiff = value;
	}

	/**
	 * Gets the value of the exchangeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExchangeNo() {
		return exchangeNo;
	}

	/**
	 * Sets the value of the exchangeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExchangeNo(String value) {
		this.exchangeNo = value;
	}

	/**
	 * Gets the value of the passbookNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPassbookNo() {
		return passbookNo;
	}

	/**
	 * Sets the value of the passbookNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPassbookNo(String value) {
		this.passbookNo = value;
	}

	/**
	 * Gets the value of the cardNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * Sets the value of the cardNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardNo(String value) {
		this.cardNo = value;
	}

	/**
	 * Gets the value of the mediumStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumStatus() {
		return mediumStatus;
	}

	/**
	 * Sets the value of the mediumStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumStatus(String value) {
		this.mediumStatus = value;
	}

	/**
	 * Gets the value of the fcyType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcyType() {
		return fcyType;
	}

	/**
	 * Sets the value of the fcyType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcyType(String value) {
		this.fcyType = value;
	}

	/**
	 * Gets the value of the depProd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDEPProd() {
		return depProd;
	}

	/**
	 * Sets the value of the depProd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDEPProd(String value) {
		this.depProd = value;
	}

	/**
	 * Gets the value of the stockNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStockNumber() {
		return stockNumber;
	}

	/**
	 * Sets the value of the stockNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStockNumber(String value) {
		this.stockNumber = value;
	}

	/**
	 * Gets the value of the rtFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRtFlag() {
		return rtFlag;
	}

	/**
	 * Sets the value of the rtFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRtFlag(String value) {
		this.rtFlag = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdType(String value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdNo() {
		return idNo;
	}

	/**
	 * Sets the value of the idNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdNo(String value) {
		this.idNo = value;
	}

	/**
	 * Gets the value of the inactivMarker property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInactivMarker() {
		return inactivMarker;
	}

	/**
	 * Sets the value of the inactivMarker property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInactivMarker(String value) {
		this.inactivMarker = value;
	}

	/**
	 * Gets the value of the openClearedBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenClearedBal() {
		return openClearedBal;
	}

	/**
	 * Sets the value of the openClearedBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenClearedBal(String value) {
		this.openClearedBal = value;
	}

	/**
	 * Gets the value of the onlineActualBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOnlineActualBal() {
		return onlineActualBal;
	}

	/**
	 * Sets the value of the onlineActualBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOnlineActualBal(String value) {
		this.onlineActualBal = value;
	}

	/**
	 * Gets the value of the onLineClearedBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOnLineClearedBal() {
		return onLineClearedBal;
	}

	/**
	 * Sets the value of the onLineClearedBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOnLineClearedBal(String value) {
		this.onLineClearedBal = value;
	}

	/**
	 * Gets the value of the workingBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkingBal() {
		return workingBal;
	}

	/**
	 * Sets the value of the workingBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWorkingBal(String value) {
		this.workingBal = value;
	}

	/**
	 * Gets the value of the lockedAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLockedAmt() {
		return lockedAmt;
	}

	/**
	 * Sets the value of the lockedAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLockedAmt(String value) {
		this.lockedAmt = value;
	}

	/**
	 * Gets the value of the altAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAltAcctNo() {
		return altAcctNo;
	}

	/**
	 * Sets the value of the altAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAltAcctNo(String value) {
		this.altAcctNo = value;
	}

	/**
	 * Gets the value of the masterAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMasterAcctNo() {
		return masterAcctNo;
	}

	/**
	 * Sets the value of the masterAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMasterAcctNo(String value) {
		this.masterAcctNo = value;
	}

	/**
	 * Gets the value of the companyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the value of the companyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}

	/**
	 * Gets the value of the cardSector property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardSector() {
		return cardSector;
	}

	/**
	 * Sets the value of the cardSector property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardSector(String value) {
		this.cardSector = value;
	}

	/**
	 * Gets the value of the subCdFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubCdFlag() {
		return subCdFlag;
	}

	/**
	 * Sets the value of the subCdFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubCdFlag(String value) {
		this.subCdFlag = value;
	}

	/**
	 * Gets the value of the cardProduct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardProduct() {
		return cardProduct;
	}

	/**
	 * Sets the value of the cardProduct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardProduct(String value) {
		this.cardProduct = value;
	}

	/**
	 * Gets the value of the blklstType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBlklstType() {
		return blklstType;
	}

	/**
	 * Sets the value of the blklstType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBlklstType(String value) {
		this.blklstType = value;
	}

	/**
	 * Gets the value of the expiryMonth property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpiryMonth() {
		return expiryMonth;
	}

	/**
	 * Sets the value of the expiryMonth property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpiryMonth(String value) {
		this.expiryMonth = value;
	}

	/**
	 * Gets the value of the number property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the value of the number property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNumber(String value) {
		this.number = value;
	}

	/**
	 * Gets the value of the openComp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenComp() {
		return openComp;
	}

	/**
	 * Sets the value of the openComp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenComp(String value) {
		this.openComp = value;
	}

	/**
	 * Gets the value of the lastBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastBal() {
		return lastBal;
	}

	/**
	 * Sets the value of the lastBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLastBal(String value) {
		this.lastBal = value;
	}

	/**
	 * Gets the value of the loanAcctFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAcctFlag() {
		return loanAcctFlag;
	}

	/**
	 * Sets the value of the loanAcctFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAcctFlag(String value) {
		this.loanAcctFlag = value;
	}

	/**
	 * Gets the value of the prsnLegalType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrsnLegalType() {
		return prsnLegalType;
	}

	/**
	 * Sets the value of the prsnLegalType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrsnLegalType(String value) {
		this.prsnLegalType = value;
	}

	/**
	 * Gets the value of the flag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * Sets the value of the flag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFlag(String value) {
		this.flag = value;
	}

	/**
	 * Gets the value of the endDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDt() {
		return endDt;
	}

	/**
	 * Sets the value of the endDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDt(String value) {
		this.endDt = value;
	}

	/**
	 * Gets the value of the effectDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectDt() {
		return effectDt;
	}

	/**
	 * Sets the value of the effectDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectDt(String value) {
		this.effectDt = value;
	}

	/**
	 * Gets the value of the endDt1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDt1() {
		return endDt1;
	}

	/**
	 * Sets the value of the endDt1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDt1(String value) {
		this.endDt1 = value;
	}

	/**
	 * Gets the value of the effectDt1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectDt1() {
		return effectDt1;
	}

	/**
	 * Sets the value of the effectDt1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectDt1(String value) {
		this.effectDt1 = value;
	}

	/**
	 * Gets the value of the acctType1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType1() {
		return acctType1;
	}

	/**
	 * Sets the value of the acctType1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType1(String value) {
		this.acctType1 = value;
	}

	/**
	 * Gets the value of the closeRestrict property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCloseRestrict() {
		return closeRestrict;
	}

	/**
	 * Sets the value of the closeRestrict property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCloseRestrict(String value) {
		this.closeRestrict = value;
	}

	/**
	 * Gets the value of the pbocStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPBOCStatus() {
		return pbocStatus;
	}

	/**
	 * Sets the value of the pbocStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPBOCStatus(String value) {
		this.pbocStatus = value;
	}

	/**
	 * Gets the value of the acEffDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcEffDt() {
		return acEffDt;
	}

	/**
	 * Sets the value of the acEffDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcEffDt(String value) {
		this.acEffDt = value;
	}

	/**
	 * Gets the value of the indentityClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndentityClass() {
		return indentityClass;
	}

	/**
	 * Sets the value of the indentityClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndentityClass(String value) {
		this.indentityClass = value;
	}

	/**
	 * Gets the value of the nationality property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * Sets the value of the nationality property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNationality(String value) {
		this.nationality = value;
	}

	/**
	 * Gets the value of the custLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustLevel() {
		return custLevel;
	}

	/**
	 * Sets the value of the custLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustLevel(String value) {
		this.custLevel = value;
	}

	/**
	 * Gets the value of the nAcCharacter property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNAcCharacter() {
		return nAcCharacter;
	}

	/**
	 * Sets the value of the nAcCharacter property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNAcCharacter(String value) {
		this.nAcCharacter = value;
	}

	/**
	 * Gets the value of the flag1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFlag1() {
		return flag1;
	}

	/**
	 * Sets the value of the flag1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFlag1(String value) {
		this.flag1 = value;
	}

	/**
	 * Gets the value of the fcySpeActype property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcySpeActype() {
		return fcySpeActype;
	}

	/**
	 * Sets the value of the fcySpeActype property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcySpeActype(String value) {
		this.fcySpeActype = value;
	}

	/**
	 * Gets the value of the pwdStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPwdStatus() {
		return pwdStatus;
	}

	/**
	 * Sets the value of the pwdStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPwdStatus(String value) {
		this.pwdStatus = value;
	}

	/**
	 * Gets the value of the lglExpDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLglExpDate() {
		return lglExpDate;
	}

	/**
	 * Sets the value of the lglExpDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLglExpDate(String value) {
		this.lglExpDate = value;
	}

	/**
	 * Gets the value of the fbisAcctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFbisAcctType() {
		return fbisAcctType;
	}

	/**
	 * Sets the value of the fbisAcctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFbisAcctType(String value) {
		this.fbisAcctType = value;
	}

	/**
	 * Gets the value of the otherBankNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOtherBankNo() {
		return otherBankNo;
	}

	/**
	 * Sets the value of the otherBankNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOtherBankNo(String value) {
		this.otherBankNo = value;
	}

	/**
	 * Gets the value of the otherCrcdNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOtherCrcdNo() {
		return otherCrcdNo;
	}

	/**
	 * Sets the value of the otherCrcdNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOtherCrcdNo(String value) {
		this.otherCrcdNo = value;
	}

	/**
	 * Gets the value of the eacctMobile property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEacctMobile() {
		return eacctMobile;
	}

	/**
	 * Sets the value of the eacctMobile property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEacctMobile(String value) {
		this.eacctMobile = value;
	}

	/**
	 * Gets the value of the msgFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgFlg() {
		return msgFlg;
	}

	/**
	 * Sets the value of the msgFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgFlg(String value) {
		this.msgFlg = value;
	}

	/**
	 * Gets the value of the blockFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBlockFlag() {
		return blockFlag;
	}

	/**
	 * Sets the value of the blockFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBlockFlag(String value) {
		this.blockFlag = value;
	}

	/**
	 * Gets the value of the mediumFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumFlag() {
		return mediumFlag;
	}

	/**
	 * Sets the value of the mediumFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumFlag(String value) {
		this.mediumFlag = value;
	}

	/**
	 * Gets the value of the eacctStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEacctStatus() {
		return eacctStatus;
	}

	/**
	 * Sets the value of the eacctStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEacctStatus(String value) {
		this.eacctStatus = value;
	}

	/**
	 * Gets the value of the orgPwdFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgPwdFlg() {
		return orgPwdFlg;
	}

	/**
	 * Sets the value of the orgPwdFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgPwdFlg(String value) {
		this.orgPwdFlg = value;
	}

	/**
	 * Gets the value of the agtName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtName() {
		return agtName;
	}

	/**
	 * Sets the value of the agtName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtName(String value) {
		this.agtName = value;
	}

	/**
	 * Gets the value of the agtIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtIdType() {
		return agtIdType;
	}

	/**
	 * Sets the value of the agtIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtIdType(String value) {
		this.agtIdType = value;
	}

	/**
	 * Gets the value of the agtId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtId() {
		return agtId;
	}

	/**
	 * Sets the value of the agtId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtId(String value) {
		this.agtId = value;
	}

	/**
	 * Gets the value of the settelBankName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettelBankName() {
		return settelBankName;
	}

	/**
	 * Sets the value of the settelBankName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettelBankName(String value) {
		this.settelBankName = value;
	}

	/**
	 * Gets the value of the payAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayAcctNo() {
		return payAcctNo;
	}

	/**
	 * Sets the value of the payAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayAcctNo(String value) {
		this.payAcctNo = value;
	}

	/**
	 * Gets the value of the waiveLedgerFee property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWaiveLedgerFee() {
		return waiveLedgerFee;
	}

	/**
	 * Sets the value of the waiveLedgerFee property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWaiveLedgerFee(String value) {
		this.waiveLedgerFee = value;
	}

	/**
	 * Gets the value of the chequesAllowed property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChequesAllowed() {
		return chequesAllowed;
	}

	/**
	 * Sets the value of the chequesAllowed property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChequesAllowed(String value) {
		this.chequesAllowed = value;
	}

	/**
	 * Gets the value of the safeDepEnd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSafeDepEnd() {
		return safeDepEnd;
	}

	/**
	 * Sets the value of the safeDepEnd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSafeDepEnd(String value) {
		this.safeDepEnd = value;
	}

	/**
	 * Gets the value of the prvAcctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrvAcctType() {
		return prvAcctType;
	}

	/**
	 * Sets the value of the prvAcctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrvAcctType(String value) {
		this.prvAcctType = value;
	}

	/**
	 * Gets the value of the accPro property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccPro() {
		return accPro;
	}

	/**
	 * Sets the value of the accPro property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccPro(String value) {
		this.accPro = value;
	}

	/**
	 * Gets the value of the rolloverFlage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRolloverFlage() {
		return rolloverFlage;
	}

	/**
	 * Sets the value of the rolloverFlage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRolloverFlage(String value) {
		this.rolloverFlage = value;
	}

	/**
	 * Gets the value of the intRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntRate() {
		return intRate;
	}

	/**
	 * Sets the value of the intRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntRate(String value) {
		this.intRate = value;
	}

	/**
	 * Gets the value of the subPrd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubPrd() {
		return subPrd;
	}

	/**
	 * Sets the value of the subPrd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubPrd(String value) {
		this.subPrd = value;
	}

	/**
	 * Gets the value of the clcRegion property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClcRegion() {
		return clcRegion;
	}

	/**
	 * Sets the value of the clcRegion property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClcRegion(String value) {
		this.clcRegion = value;
	}

	/**
	 * Gets the value of the clcRegionName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClcRegionName() {
		return clcRegionName;
	}

	/**
	 * Sets the value of the clcRegionName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClcRegionName(String value) {
		this.clcRegionName = value;
	}

	/**
	 * Gets the value of the subOcbTot property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubOcbTot() {
		return subOcbTot;
	}

	/**
	 * Sets the value of the subOcbTot property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubOcbTot(String value) {
		this.subOcbTot = value;
	}

	/**
	 * Gets the value of the nraFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNraFlag() {
		return nraFlag;
	}

	/**
	 * Sets the value of the nraFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNraFlag(String value) {
		this.nraFlag = value;
	}

	/**
	 * Gets the value of the coName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoName() {
		return coName;
	}

	/**
	 * Sets the value of the coName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoName(String value) {
		this.coName = value;
	}

	/**
	 * Gets the value of the ssfaFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSsfaFlag() {
		return ssfaFlag;
	}

	/**
	 * Sets the value of the ssfaFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSsfaFlag(String value) {
		this.ssfaFlag = value;
	}

	/**
	 * Gets the value of the accountingType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountingType() {
		return accountingType;
	}

	/**
	 * Sets the value of the accountingType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountingType(String value) {
		this.accountingType = value;
	}

	/**
	 * Gets the value of the iccdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIccdType() {
		return iccdType;
	}

	/**
	 * Sets the value of the iccdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIccdType(String value) {
		this.iccdType = value;
	}

	/**
	 * Gets the value of the subWorkbalTot property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubWorkbalTot() {
		return subWorkbalTot;
	}

	/**
	 * Sets the value of the subWorkbalTot property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubWorkbalTot(String value) {
		this.subWorkbalTot = value;
	}

	/**
	 * Gets the value of the cardOpenDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardOpenDt() {
		return cardOpenDt;
	}

	/**
	 * Sets the value of the cardOpenDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardOpenDt(String value) {
		this.cardOpenDt = value;
	}

	/**
	 * Gets the value of the cdOpenComname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCdOpenComname() {
		return cdOpenComname;
	}

	/**
	 * Sets the value of the cdOpenComname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCdOpenComname(String value) {
		this.cdOpenComname = value;
	}

	/**
	 * Gets the value of the virtAccr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVirtAccr() {
		return virtAccr;
	}

	/**
	 * Sets the value of the virtAccr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVirtAccr(String value) {
		this.virtAccr = value;
	}

	/**
	 * Gets the value of the virtAcctName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVirtAcctName() {
		return virtAcctName;
	}

	/**
	 * Sets the value of the virtAcctName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVirtAcctName(String value) {
		this.virtAcctName = value;
	}

	/**
	 * Gets the value of the acctPbook property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctPbook() {
		return acctPbook;
	}

	/**
	 * Sets the value of the acctPbook property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctPbook(String value) {
		this.acctPbook = value;
	}

	/**
	 * Gets the value of the acctCard property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctCard() {
		return acctCard;
	}

	/**
	 * Sets the value of the acctCard property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctCard(String value) {
		this.acctCard = value;
	}

	/**
	 * Gets the value of the signCardFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSignCardFlg() {
		return signCardFlg;
	}

	/**
	 * Sets the value of the signCardFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSignCardFlg(String value) {
		this.signCardFlg = value;
	}

	/**
	 * Gets the value of the acctLevelFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctLevelFlg() {
		return acctLevelFlg;
	}

	/**
	 * Sets the value of the acctLevelFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctLevelFlg(String value) {
		this.acctLevelFlg = value;
	}

	/**
	 * Gets the value of the custName1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustName1() {
		return custName1;
	}

	/**
	 * Sets the value of the custName1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustName1(String value) {
		this.custName1 = value;
	}

	/**
	 * Gets the value of the trdCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTrdCode() {
		return trdCode;
	}

	/**
	 * Sets the value of the trdCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTrdCode(String value) {
		this.trdCode = value;
	}

	/**
	 * Gets the value of the benpAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBenpAcct() {
		return benpAcct;
	}

	/**
	 * Sets the value of the benpAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBenpAcct(String value) {
		this.benpAcct = value;
	}

	/**
	 * Gets the value of the virtualFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVirtualFlag() {
		return virtualFlag;
	}

	/**
	 * Sets the value of the virtualFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVirtualFlag(String value) {
		this.virtualFlag = value;
	}

	/**
	 * Gets the value of the withDrawLimit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWithDrawLimit() {
		return withDrawLimit;
	}

	/**
	 * Sets the value of the withDrawLimit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWithDrawLimit(String value) {
		this.withDrawLimit = value;
	}

	/**
	 * Gets the value of the withDrowCnt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWithDrowCnt() {
		return withDrowCnt;
	}

	/**
	 * Sets the value of the withDrowCnt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWithDrowCnt(String value) {
		this.withDrowCnt = value;
	}

}
