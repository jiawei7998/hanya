package com.singlee.capital.interfacex.qdb.ESB.util;

import java.util.HashMap;

import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;


/**
 * @author Fang
 * 
 */
public class ConvertToServe {
	
	private static String atxclientHeader = "atx-client-esb.xml";
	//CIF客户名称查询报文
	private static String atxlientQueryEname="atx-client-queryEname-esb.xml";
	//CIF别名维护报文
	private static String atxclientEName="atx-client-ename-esb.xml";
	//CIF创建报文
	private static String atxclientCreate = "atx-client-create-esb.xml";
	//CIF明细查询报文
	private static String atxclientQueryDetailed = "atx-client-querydetailed-esb.xml";
	//CNAPS2 发起汇兑报文
	private static String atxCnaps2SendPayment = "atx-cnaps2-sendpayment-esb.xml";
	//CNAPS2 查询付款交易
	private static String atxCnaps2QueryStatus = "atx-cnaps2-querystatus-esb.xml";
	//来账待处理列表查询_RM2049
	private static String atxclientPend1 = "atx-client-2049-esb.xml";
	//来账待处理明细查询_RM2051
	private static String atxclientPend2 = "atx-client-2051-esb.xml";
	//大额来账待处理列表查询_RM2049
	private static String atxclientCnaps4 = "atx-client-2054-esb.xml";
	//大额来账待处理明细查询_RM2051
	private static String atxclientCnaps3 = "atx-client-2043-esb.xml";
	
	static
	{
		String esbXmlDir = PropertiesUtil.getProperties("ESB.EsbXmlDir");//ESB报文模板保存的路径
		atxclientHeader        = esbXmlDir + atxclientHeader        ;  
		atxlientQueryEname     = esbXmlDir + atxlientQueryEname     ;  
		atxclientEName         = esbXmlDir + atxclientEName         ;  
		atxclientCreate        = esbXmlDir + atxclientCreate        ;  
		atxclientQueryDetailed = esbXmlDir + atxclientQueryDetailed ;  
		atxCnaps2SendPayment   = esbXmlDir + atxCnaps2SendPayment   ;  
		atxCnaps2QueryStatus   = esbXmlDir + atxCnaps2QueryStatus   ;  
		atxclientPend1         = esbXmlDir + atxclientPend1         ;  
		atxclientPend2         = esbXmlDir + atxclientPend2         ;  
		atxclientCnaps4        = esbXmlDir + atxclientCnaps4        ;  
		atxclientCnaps3        = esbXmlDir + atxclientCnaps3        ;  
	}
	
	public static HashMap<String, EsbBean> CnapsPend4()throws Exception{
		return ConvertUtil.explainMessageByClassPath(atxclientCnaps4);
	}

	public static HashMap<String, EsbBean> CnapsPend3()throws Exception{
		return ConvertUtil.explainMessageByClassPath(atxclientCnaps3);
	}
	
	public static HashMap<String, EsbBean> CnapsPend1()throws Exception{
		return ConvertUtil.explainMessageByClassPath(atxclientPend1);
	}

	public static HashMap<String, EsbBean> CnapsPend2()throws Exception{
		return ConvertUtil.explainMessageByClassPath(atxclientPend2);
	}
	
	public static HashMap<String, EsbBean> Cnaps2SendPayment()throws Exception{
		
		return ConvertUtil.explainMessageByClassPath(atxCnaps2SendPayment);
	}
	
	public static HashMap<String, EsbBean> Cnaps2QueryStatus()throws Exception{
		
		return ConvertUtil.explainMessageByClassPath(atxCnaps2QueryStatus);
	}
	
	public static HashMap<String,EsbBean> readEsbFile()throws Exception 
	{
		return ConvertUtil.explainMessageByClassPath(atxclientHeader);
	}
	
	public static HashMap<String,EsbBean> readENameFile() throws Exception
	{
		return ConvertUtil.explainMessageByClassPath(atxclientEName);
	}
	
	public static HashMap<String,EsbBean> readQueryENameFile() throws Exception
	{
		return ConvertUtil.explainMessageByClassPath(atxlientQueryEname);
	}
	
	
	public static HashMap<String,EsbBean> readEsbCreateFile() throws Exception
	{
		return ConvertUtil.explainMessageByClassPath(atxclientCreate);
	}
	
	
	public static HashMap<String,EsbBean> readEsbQueryDetailedFile() throws Exception
	{
		return ConvertUtil.explainMessageByClassPath(atxclientQueryDetailed);
	}
}
