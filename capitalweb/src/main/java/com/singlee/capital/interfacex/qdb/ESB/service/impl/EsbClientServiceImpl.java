package com.singlee.capital.interfacex.qdb.ESB.service.impl;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;



@Service
public class EsbClientServiceImpl implements EsbClientService {

	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	

	@Override
	public HashMap<String, Object> sendXmlToRequest(String url, String xmlString)throws Exception {
		return this.httpClientManagerDao.sendXmlToRequest(url, xmlString);
	}

}
