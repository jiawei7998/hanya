package com.singlee.capital.interfacex.quartzjob;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.service.GtpFileService;

public class GtpGlAcupFileCreateQuartzJob implements CronRunnable {

	/**
	 *  1、GTP配置IFBM与总账和VAT；
		2、文件命名格式：IFBMledgerYYYY_ppp.dat
	      (YYYY代表年；ppp该年度内每天数据文件对应的会计期间1月1日->001)；
	                通配符：IFBMledger*.dat
	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		RetMsg<Object>  retmsg = gtpFileService.GtpGLOutputDataWrite();
		System.out.println(retmsg.getCode());
		if(null == retmsg || !"error.common.0000".equalsIgnoreCase(retmsg.getCode())){
			throw new RException("总账文件异常："+retmsg.toJsonString());
		}
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
