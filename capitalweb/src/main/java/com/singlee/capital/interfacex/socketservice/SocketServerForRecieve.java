package com.singlee.capital.interfacex.socketservice;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.service.SocketClientService;

public class SocketServerForRecieve {

	@Autowired
	private SocketClientService socketClientService;
	
	
	private int maxClientConnecttion;

	private int listenPort;

	private ServerSocket serverSocket;

	private boolean started = true;
	
	/**
	 * cz add it 使用阻塞队列存放socket 
	 */
	private BlockingQueue<Socket> toHandleSocketQueue;
	
	private SocketServerConnectionHandler[] quotePooledConnectionHandlers;
	
	public void shutdownMethod(){
		started = false;
		try {
			serverSocket.close();
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("---------------------Socket Server 关闭 ---------------------");
		} catch (IOException e) {
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).error("shutdown server socket error!\r\n", e);
		}
		for(SocketServerConnectionHandler wt : quotePooledConnectionHandlers){
			wt.shutdown();
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("---------------------Socket Server Connection Handler 关闭 ---------------------");
		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("---------------------SocketServerForRecieve关闭完成 ---------------------");
	}
	
	
	public void initMethod() {
		// 设置阻塞队列
		toHandleSocketQueue = new LinkedBlockingQueue<Socket>();
		// 设置 sockethandler 线程
		setupHandlers();
		// 打开监端口
		listenPort();
		// 新开线程进行接收
		Thread myThread = new Thread(new Runnable() {
			@Override
            public void run() {
				acceptConnections();
			}
		}, "SocketServerAcceptThread");
		myThread.start();
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("---------------------SocketServerForRecieve启动完成 ---------------------");
	}

	/**
	 * @return the maxClientConnecttion
	 */
	public int getMaxClientConnecttion() {
		return maxClientConnecttion;
	}

	/**
	 * @param maxClientConnecttion
	 *            the maxClientConnecttion to set
	 */
	public void setMaxClientConnecttion(int maxClientConnecttion) {
		this.maxClientConnecttion = maxClientConnecttion;
	}

	/**
	 * @return the listenPort
	 */
	public int getListenPort() {
		return listenPort;
	}

	/**
	 * @param listenPort
	 *            the listenPort to set
	 */
	public void setListenPort(int listenPort) {
		this.listenPort = listenPort;
	}


	

	/**
	 * 接收 连接线程
	 */
	private void acceptConnections() {
		try {
			while (started) {
				try {
					Socket incomingConnection = serverSocket.accept();
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(String.format("--------------------- %d端口有一个连接建立 ---------------------", listenPort));
					handleConnection(incomingConnection);
				} catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).error("server socket accept error!\r\n", e);
				}
			}
		}catch (Exception e) {
			JY.raiseRException(String.format("-----------打开端口 %d 异常！-----------", listenPort), e);
		}
	}

	/**
	 * 对 socket 实例进行队列处理
	 * @param connectionToHandle
	 */
	private void handleConnection(Socket connectionToHandle){
		try {
			// 将socket实例扔到 阻塞队列
			toHandleSocketQueue.put(connectionToHandle);
		} catch (InterruptedException e) {
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).warn(e);
		}

	}

	/**
	 * 设置处理线程
	 */
	private void setupHandlers() {
		quotePooledConnectionHandlers = new SocketServerConnectionHandler[maxClientConnecttion];
		for (int i = 0; i < maxClientConnecttion; i++) {
			quotePooledConnectionHandlers[i] = new SocketServerConnectionHandler(toHandleSocketQueue,socketClientService);
			new Thread(quotePooledConnectionHandlers[i], "SocketServerHandler " + i).start();
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(String.format("SocketServerConnectionHandler %d loaded ", i));
		}
	}

	/**
	 * 监听 
	 */
	private void listenPort(){
		try {
			serverSocket = new ServerSocket(listenPort, 10);
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(String.format("---------------------系统打开 %d 端口进行监听 ---------------------", listenPort));
		}catch (Exception e) {
			JY.raiseRException(String.format("-----------打开端口 %d 异常！-----------", listenPort), e);
		}
	}
}
