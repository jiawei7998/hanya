package com.singlee.capital.interfacex.qdb.model;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 委外业务底层资产主表
 * @author Administrator
 *
 */
@Entity
@Table(name="TD_TRADE_INV_MAIN")
public class TdTradInvMain implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * 交易编号
	 */
	private String tradeId;
	/**
	 * 资产类合计
	 */
	private String asset;
	//负债类合计
	private String debt;
	//资产净值（按成本）
	private String cpriceAssetCost;
	//资产净值（市值）
	private String cpriceAssetMarket;
	//基金单位净值（按成本）
	private String cpriceAssetUnitCost;
	//基金单位净值（按市值）
	private String cpriceAssetUnitMarket;
	//资产净值（按成本）
	private String cpriceCost;
	//资产净值（按市值）
	private String cpriceMarket;
	//证券投资合计（成本）
	private String totalSecInv;
	//资产名称
	private String aName;
	//版本号
	private String version;
	//备注
	private String remark;
	//导入日期
	private String iDate;
	//委外业务底层资产明细
	@Transient
	private String secNm;
	@Transient
	private String unitCost;
	@Transient
	private String costAmt;
	
	
	
	public String getaName() {
		return aName;
	}
	public void setaName(String aName) {
		this.aName = aName;
	}
	public String getiDate() {
		return iDate;
	}
	public void setiDate(String iDate) {
		this.iDate = iDate;
	}
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public String getAsset() {
		return asset;
	}
	public void setAsset(String asset) {
		this.asset = asset;
	}
	public String getDebt() {
		return debt;
	}
	public void setDebt(String debt) {
		this.debt = debt;
	}
	public String getCpriceCost() {
		return cpriceCost;
	}
	public void setCpriceCost(String cpriceCost) {
		this.cpriceCost = cpriceCost;
	}
	public String getCpriceMarket() {
		return cpriceMarket;
	}
	public void setCpriceMarket(String cpriceMarket) {
		this.cpriceMarket = cpriceMarket;
	}
	public String getAName() {
		return aName;
	}
	public void setAName(String aName) {
		this.aName = aName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSecNm() {
		return secNm;
	}
	public void setSecNm(String secNm) {
		this.secNm = secNm;
	}
	public String getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}
	public String getCostAmt() {
		return costAmt;
	}
	public void setCostAmt(String costAmt) {
		this.costAmt = costAmt;
	}
	public String getTotalSecInv() {
		return totalSecInv;
	}
	public void setTotalSecInv(String totalSecInv) {
		this.totalSecInv = totalSecInv;
	}
	public String getCpriceAssetCost() {
		return cpriceAssetCost;
	}
	public void setCpriceAssetCost(String cpriceAssetCost) {
		this.cpriceAssetCost = cpriceAssetCost;
	}
	public String getCpriceAssetUnitCost() {
		return cpriceAssetUnitCost;
	}
	public void setCpriceAssetUnitCost(String cpriceAssetUnitCost) {
		this.cpriceAssetUnitCost = cpriceAssetUnitCost;
	}
	public String getCpriceAssetUnitMarket() {
		return cpriceAssetUnitMarket;
	}
	public void setCpriceAssetUnitMarket(String cpriceAssetUnitMarket) {
		this.cpriceAssetUnitMarket = cpriceAssetUnitMarket;
	}
	public String getCpriceAssetMarket() {
		return cpriceAssetMarket;
	}
	public void setCpriceAssetMarket(String cpriceAssetMarket) {
		this.cpriceAssetMarket = cpriceAssetMarket;
	}

	
}
