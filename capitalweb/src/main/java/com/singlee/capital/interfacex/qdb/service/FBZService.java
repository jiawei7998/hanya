package com.singlee.capital.interfacex.qdb.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.interfacex.qdb.pojo.FBZ001;
import com.singlee.capital.interfacex.qdb.pojo.FBZ002;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.pojo.FBZ004;

public interface FBZService {
	public String parseXml(FBZ001 fbz);
	public String parseXml(FBZ002 fbz);
	public String parseXml(FBZ003 fbz);
	public String parseXml(FBZ004 fbz);
	public FBZ001 sendReq(FBZ001 fbz) throws Exception;
	public boolean sendReq(FBZ002 fbz) throws Exception;
	public FBZ003 sendReq(FBZ003 fbz);
	public List<Object> sendReq(FBZ004 fbz) throws Exception;
	public boolean insertFbz(FBZ003 fbz);
	public FBZ003 getFbzByOrder(TtTrdOrder order);
	public boolean updateFbz(FBZ003 fbz);
	public FBZ003 getFbzByDealNo(String string);
	public void deleteFbz(FBZ003 f);
	public List<FBZ003> getFbzBySeriaNo(Map<String, Object> map);
	public void updateFlag(FBZ003 fbz);
	
	public boolean checkCreditCust(Map<String, Object> map);
	
}
