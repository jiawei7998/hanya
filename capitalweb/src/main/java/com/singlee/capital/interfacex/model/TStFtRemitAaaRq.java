package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TStFtRemitAaaRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TStFtRemitAaaRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="DebitMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValueDateDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TrnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrderingCustRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}OrderingCustRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RemitAddrRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}RemitAddrRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BenAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BenCustRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}BenCustRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PayDetailsRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}PayDetailsRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AcctWithBankRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}AcctWithBankRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IntermedBankRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}IntermedBankRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrginstCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BenOurCharges" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReMark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeebaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RemitMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SfSettleMetho" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JwjnFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OtherAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SfTxnCodePmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Scnap_9" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Scnap_8" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SfCptyCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStFtRemitAaaRq", propOrder = { "commonRqHdr", "debitMedType",
		"acctNoDr", "currencyDr", "valueDateDr", "currencyCr", "amt", "fbNo",
		"txnType", "trnType", "orderingCustRec", "remitAddrRec", "benAcctNo",
		"benCustRec", "payDetailsRec", "acctWithBankRec", "intermedBankRec",
		"orginstCode", "benOurCharges", "usage", "reMark", "feebaseLogId",
		"remitMethod", "sfSettleMetho", "jwjnFlg", "otherAcct", "sfTxnCodePmt",
		"scnap9", "scnap8", "sfCptyCountry", "contractNo", "invoiceNo" })
public class TStFtRemitAaaRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "DebitMedType", required = true)
	protected String debitMedType;
	@XmlElement(name = "AcctNoDr", required = true)
	protected String acctNoDr;
	@XmlElement(name = "CurrencyDr", required = true)
	protected String currencyDr;
	@XmlElement(name = "ValueDateDr", required = true)
	protected String valueDateDr;
	@XmlElement(name = "CurrencyCr", required = true)
	protected String currencyCr;
	@XmlElement(name = "Amt", required = true)
	protected String amt;
	@XmlElement(name = "FbNo", required = true)
	protected String fbNo;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "TrnType", required = true)
	protected String trnType;
	@XmlElement(name = "OrderingCustRec")
	protected List<OrderingCustRec> orderingCustRec;
	@XmlElement(name = "RemitAddrRec")
	protected List<RemitAddrRec> remitAddrRec;
	@XmlElement(name = "BenAcctNo", required = true)
	protected String benAcctNo;
	@XmlElement(name = "BenCustRec")
	protected List<BenCustRec> benCustRec;
	@XmlElement(name = "PayDetailsRec")
	protected List<PayDetailsRec> payDetailsRec;
	@XmlElement(name = "AcctWithBankRec")
	protected List<AcctWithBankRec> acctWithBankRec;
	@XmlElement(name = "IntermedBankRec")
	protected List<IntermedBankRec> intermedBankRec;
	@XmlElement(name = "OrginstCode", required = true)
	protected String orginstCode;
	@XmlElement(name = "BenOurCharges", required = true)
	protected String benOurCharges;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "ReMark", required = true)
	protected String reMark;
	@XmlElement(name = "FeebaseLogId", required = true)
	protected String feebaseLogId;
	@XmlElement(name = "RemitMethod", required = true)
	protected String remitMethod;
	@XmlElement(name = "SfSettleMetho", required = true)
	protected String sfSettleMetho;
	@XmlElement(name = "JwjnFlg", required = true)
	protected String jwjnFlg;
	@XmlElement(name = "OtherAcct", required = true)
	protected String otherAcct;
	@XmlElement(name = "SfTxnCodePmt", required = true)
	protected String sfTxnCodePmt;
	@XmlElement(name = "Scnap_9", required = true)
	protected String scnap9;
	@XmlElement(name = "Scnap_8", required = true)
	protected String scnap8;
	@XmlElement(name = "SfCptyCountry", required = true)
	protected String sfCptyCountry;
	@XmlElement(name = "ContractNo", required = true)
	protected String contractNo;
	@XmlElement(name = "InvoiceNo", required = true)
	protected String invoiceNo;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the debitMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitMedType() {
		return debitMedType;
	}

	/**
	 * Sets the value of the debitMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitMedType(String value) {
		this.debitMedType = value;
	}

	/**
	 * Gets the value of the acctNoDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoDr() {
		return acctNoDr;
	}

	/**
	 * Sets the value of the acctNoDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoDr(String value) {
		this.acctNoDr = value;
	}

	/**
	 * Gets the value of the currencyDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyDr() {
		return currencyDr;
	}

	/**
	 * Sets the value of the currencyDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyDr(String value) {
		this.currencyDr = value;
	}

	/**
	 * Gets the value of the valueDateDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValueDateDr() {
		return valueDateDr;
	}

	/**
	 * Sets the value of the valueDateDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValueDateDr(String value) {
		this.valueDateDr = value;
	}

	/**
	 * Gets the value of the currencyCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyCr() {
		return currencyCr;
	}

	/**
	 * Sets the value of the currencyCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyCr(String value) {
		this.currencyCr = value;
	}

	/**
	 * Gets the value of the amt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmt() {
		return amt;
	}

	/**
	 * Sets the value of the amt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmt(String value) {
		this.amt = value;
	}

	/**
	 * Gets the value of the fbNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFbNo() {
		return fbNo;
	}

	/**
	 * Sets the value of the fbNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFbNo(String value) {
		this.fbNo = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the trnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTrnType() {
		return trnType;
	}

	/**
	 * Sets the value of the trnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTrnType(String value) {
		this.trnType = value;
	}

	/**
	 * Gets the value of the orderingCustRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the orderingCustRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOrderingCustRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link OrderingCustRec }
	 * 
	 * 
	 */
	public List<OrderingCustRec> getOrderingCustRec() {
		if (orderingCustRec == null) {
			orderingCustRec = new ArrayList<OrderingCustRec>();
		}
		return this.orderingCustRec;
	}

	/**
	 * Gets the value of the remitAddrRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the remitAddrRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getRemitAddrRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link RemitAddrRec }
	 * 
	 * 
	 */
	public List<RemitAddrRec> getRemitAddrRec() {
		if (remitAddrRec == null) {
			remitAddrRec = new ArrayList<RemitAddrRec>();
		}
		return this.remitAddrRec;
	}

	/**
	 * Gets the value of the benAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBenAcctNo() {
		return benAcctNo;
	}

	/**
	 * Sets the value of the benAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBenAcctNo(String value) {
		this.benAcctNo = value;
	}

	/**
	 * Gets the value of the benCustRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the benCustRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getBenCustRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link BenCustRec }
	 * 
	 * 
	 */
	public List<BenCustRec> getBenCustRec() {
		if (benCustRec == null) {
			benCustRec = new ArrayList<BenCustRec>();
		}
		return this.benCustRec;
	}

	/**
	 * Gets the value of the payDetailsRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the payDetailsRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPayDetailsRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link PayDetailsRec }
	 * 
	 * 
	 */
	public List<PayDetailsRec> getPayDetailsRec() {
		if (payDetailsRec == null) {
			payDetailsRec = new ArrayList<PayDetailsRec>();
		}
		return this.payDetailsRec;
	}

	/**
	 * Gets the value of the acctWithBankRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the acctWithBankRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAcctWithBankRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctWithBankRec }
	 * 
	 * 
	 */
	public List<AcctWithBankRec> getAcctWithBankRec() {
		if (acctWithBankRec == null) {
			acctWithBankRec = new ArrayList<AcctWithBankRec>();
		}
		return this.acctWithBankRec;
	}

	/**
	 * Gets the value of the intermedBankRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the intermedBankRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getIntermedBankRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link IntermedBankRec }
	 * 
	 * 
	 */
	public List<IntermedBankRec> getIntermedBankRec() {
		if (intermedBankRec == null) {
			intermedBankRec = new ArrayList<IntermedBankRec>();
		}
		return this.intermedBankRec;
	}

	/**
	 * Gets the value of the orginstCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrginstCode() {
		return orginstCode;
	}

	/**
	 * Sets the value of the orginstCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrginstCode(String value) {
		this.orginstCode = value;
	}

	/**
	 * Gets the value of the benOurCharges property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBenOurCharges() {
		return benOurCharges;
	}

	/**
	 * Sets the value of the benOurCharges property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBenOurCharges(String value) {
		this.benOurCharges = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the reMark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReMark() {
		return reMark;
	}

	/**
	 * Sets the value of the reMark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReMark(String value) {
		this.reMark = value;
	}

	/**
	 * Gets the value of the feebaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeebaseLogId() {
		return feebaseLogId;
	}

	/**
	 * Sets the value of the feebaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeebaseLogId(String value) {
		this.feebaseLogId = value;
	}

	/**
	 * Gets the value of the remitMethod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemitMethod() {
		return remitMethod;
	}

	/**
	 * Sets the value of the remitMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemitMethod(String value) {
		this.remitMethod = value;
	}

	/**
	 * Gets the value of the sfSettleMetho property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSfSettleMetho() {
		return sfSettleMetho;
	}

	/**
	 * Sets the value of the sfSettleMetho property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSfSettleMetho(String value) {
		this.sfSettleMetho = value;
	}

	/**
	 * Gets the value of the jwjnFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getJwjnFlg() {
		return jwjnFlg;
	}

	/**
	 * Sets the value of the jwjnFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setJwjnFlg(String value) {
		this.jwjnFlg = value;
	}

	/**
	 * Gets the value of the otherAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOtherAcct() {
		return otherAcct;
	}

	/**
	 * Sets the value of the otherAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOtherAcct(String value) {
		this.otherAcct = value;
	}

	/**
	 * Gets the value of the sfTxnCodePmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSfTxnCodePmt() {
		return sfTxnCodePmt;
	}

	/**
	 * Sets the value of the sfTxnCodePmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSfTxnCodePmt(String value) {
		this.sfTxnCodePmt = value;
	}

	/**
	 * Gets the value of the scnap9 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScnap9() {
		return scnap9;
	}

	/**
	 * Sets the value of the scnap9 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScnap9(String value) {
		this.scnap9 = value;
	}

	/**
	 * Gets the value of the scnap8 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScnap8() {
		return scnap8;
	}

	/**
	 * Sets the value of the scnap8 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScnap8(String value) {
		this.scnap8 = value;
	}

	/**
	 * Gets the value of the sfCptyCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSfCptyCountry() {
		return sfCptyCountry;
	}

	/**
	 * Sets the value of the sfCptyCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSfCptyCountry(String value) {
		this.sfCptyCountry = value;
	}

	/**
	 * Gets the value of the contractNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContractNo() {
		return contractNo;
	}

	/**
	 * Sets the value of the contractNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContractNo(String value) {
		this.contractNo = value;
	}

	/**
	 * Gets the value of the invoiceNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * Sets the value of the invoiceNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInvoiceNo(String value) {
		this.invoiceNo = value;
	}

}
