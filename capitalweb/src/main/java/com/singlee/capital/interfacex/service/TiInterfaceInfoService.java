package com.singlee.capital.interfacex.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.interfacex.model.CheckPayment;
import com.singlee.capital.interfacex.model.Payment;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.interfacex.model.SelfPayment;
import com.singlee.capital.interfacex.model.TiInterfaceDependScope;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.trade.model.TdProductApproveMain;

public interface TiInterfaceInfoService {
	public void deleteInterfaceInfo(Map<String, Object> map) throws Exception;
	public void updateInterfaceInfo(TiInterfaceInfo interfaceInfo) throws Exception;
	public void addInterfaceInfo(TiInterfaceInfo interfaceInfo) throws Exception;
	public Page<TiInterfaceInfo> selectInterfaceInfoByName(Map<String,Object> map) throws Exception;
	
	public void deleteDependScope(Map<String, Object> map) throws Exception;
	public Page<TiInterfaceDependScope> queryDependScopePage(Map<String, Object> map) throws Exception;
	public TiInterfaceDependScope queryDependScopeByScopes(Map<String, Object> map) throws Exception;
	public void insertDependScope(TiInterfaceDependScope tiInterfaceDependScope) throws Exception;
	
	public void insertQuotaSyncopate(List<QuotaSyncopateRec> quotaSyncopateRec) throws Exception;
	
	public Page<Payment> query2ndPaymentPage(Map<String, Object> map, RowBounds rb) throws Exception;
	
	public boolean sendQueryPaymentTradeStatus(Map<String, Object> map) throws Exception;
	
	public List<UPPSCdtTrfRq> queryT2ndPaymentConfirmTrade(Map<String, Object> map) throws Exception;
	
	public Page<CheckPayment> Check2ndPayment(Map<String, Object> map, RowBounds rb)throws Exception;
	
	public Page<SelfPayment> SelfPayment(Map<String, Object> map, RowBounds rb)throws Exception;
	
	public List<TdProductApproveMain> queryTdProductApproveMainByDealno(Map<String, Object> map);
	
	public List<ProductApproveFund> queryTdProductFundByDealno(Map<String, Object> map);
	
	public List<TaLog> queryTaLogMessage (Map<String, Object> map);
	
	public int insertHandleCodeRemarkNote(Map<String, Object> map) throws Exception;
	
	public void exceptExcel(Map<String, Object> map,HSSFWorkbook wb) throws Exception;
}
