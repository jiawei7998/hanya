package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.Message;

public interface MessageMapper extends Mapper<Message>{
	public int insertMessage(List<Message> list);

	public Page<Message> searchMessagePage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteAll(Map<String, Object> map);
	
	public int updateMessageFlag(Map<String, Object> map);
}
	