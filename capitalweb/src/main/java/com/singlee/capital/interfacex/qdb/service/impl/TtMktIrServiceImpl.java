package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.mapper.TtDayendDateMapper;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.mapper.TtMktIrConfigMapper;
import com.singlee.capital.interfacex.qdb.mapper.TtMktIrMapper;
import com.singlee.capital.interfacex.qdb.mapper.TtMktSerriessMapper;
import com.singlee.capital.interfacex.qdb.model.TtMktIr;
import com.singlee.capital.interfacex.qdb.model.TtMktIrConfig;
import com.singlee.capital.interfacex.qdb.model.TtMktSeriess;
import com.singlee.capital.interfacex.qdb.service.TtMktIrService;

@Service("ttMktIrService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Component
public class TtMktIrServiceImpl implements TtMktIrService{
	@Autowired
	private TtMktIrMapper ttMktIrMapper;
	@Autowired
	private TtMktSerriessMapper ttMktSerriessMapper;
	@Autowired
	private TtMktIrConfigMapper ttMktIrConfigMapper;

	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	@Autowired
	private TtDayendDateMapper dayendDateMapper;//系统日期表
	

	@Override
	public Page<TtMktIr> selectTtMktIrPage(Map<String, Object> map) {
		Page<TtMktIr> page = ttMktIrMapper.selectTtMktIrPage(map, ParameterUtil.getRowBounds(map));
		return page;
	}

	@Override
	public String insertTtMktIr(TtMktIr ttMktIr) {
		int t = ttMktIrMapper.queryTtMktIrById(ttMktIr);
		if(t == 0){
			ttMktIrMapper.insertTtMktIr(ttMktIr);
			return "000";
		}else{
			log.info("基准利率代码("+ttMktIr.getI_code()+")已经存在");
			return "基准利率代码("+ttMktIr.getI_code()+")已经存在";
		}
	}

	@Override
	public void updateTtMktIr(TtMktIr ttMktIr) {
		ttMktIrMapper.updateTtMktIr(ttMktIr);
	}

	@Override
	public void deleteTtMktIrById(String[] i_code) {
		for (int i = 0; i < i_code.length; i++) {
			ttMktIrMapper.deleteTtMktIrById(i_code[i]);
		}
	}
	

	@Override
	public void deleteTtMktIrById(String Sid) {
			ttMktIrMapper.deleteTtMktIrBySid(Sid);
	}
	
	@Override
	public Page<TtMktSeriess> selectTtMktSeriessPage(Map<String, Object> map) {
		Page<TtMktSeriess> page = ttMktIrMapper.selectTtMktSeriessPage(map, ParameterUtil.getRowBounds(map));
		return page;
	}

	@Override
	public void insertSeries(Map<String, Object> map) {
		String type = ParameterUtil.getString(map, "type", null);
		TtMktSeriess ttMktIr = this.selectSeries(map);
		//当前系统时间
		String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
		if("add".equals(type)){
			if (ttMktIr != null) {
				log.info(MsgUtils.getMessage("error.system.0004"));
				throw new RuntimeException("当天的基准利率已经存在");
			}
			//以前导入日期为空，现在把导入日期改为当前系统时间
			map.put("imp_date", nowdate);
			ttMktIrMapper.insertSeries(map);
		}else if ("edit".equals(type)){
			if (ttMktIr == null) {
				log.info(MsgUtils.getMessage("error.system.0003"));
				throw new RuntimeException("查询基准利率出错");
			}
			ttMktIrMapper.updateSeries(map);
		}
	}

	@Override
	public void deleteSeries(Map<String, Object> map) {
		ttMktIrMapper.deleteSeries(map);
	}
	
	@Override
	public void deleteSeries(TtMktSeriess ttMktSeriess) {
		ttMktSerriessMapper.deleteByPrimaryKey(ttMktSeriess.getS_id());
		
	}

	@Override
	public TtMktSeriess selectSeries(Map<String, Object> map) {
		return ttMktIrMapper.selectSeries(map);
	}

	@Override
	public String getSeriesSeq() {
		return ttMktIrMapper.getSeriesSeq();
	}

	
	@Override
	public String importIrByExcel(String type, InputStream is) throws Exception{
		StringBuffer sb = new StringBuffer();
		List<TtMktIrConfig> listTtMktIrConfig = null;
		Sheet sheet = null;
		Row row = null;
		String iCode = null;
		String aType = null;
		String mType = null;
		Double sClose = null;
		String DataSource = null;
		String str3 = null;
		String str4 = null;
		ExcelUtil excelUtil = null;
		try {
			int c = 0;
			List<TtMktSeriess> list = new ArrayList<TtMktSeriess>();
			//拿到所有基准利率配置的数据
			listTtMktIrConfig = ttMktIrConfigMapper.selectTtMktIrConfigList();
			//删除所有
			List<TtDayendDate> ls = dayendDateMapper.selecteTtDayendDate();
			String currDate = ls.get(0).getCurDate();
			
			//查询有多少条行情数据
			int count = ttMktIrMapper.selectTtMktSeriessCount();
			//拿到当前系统日期
			String strs = DateUtil.getCurrentCompactDateTimeAsString();
			//截取字符串，例如：“20170513000000”==>"20170513"
			String strs1 = strs.substring(0,strs.length()-6);
			//判断输入流是否为空
			if(is==null){
				//开始解析Excel
				excelUtil = new ExcelUtil(type);
			}else{
				//开始解析Excel
				excelUtil = new ExcelUtil(type,is);
			}
			for(TtMktIrConfig ttMktIrConfig : listTtMktIrConfig){
				TtMktSeriess mktSeriess = new TtMktSeriess();
				iCode = ttMktIrConfig.getI_code();
				aType = ttMktIrConfig.getRateType();
				mType = ttMktIrConfig.getM_type();
				//拿到数据来源中的数据
				DataSource = ttMktIrConfig.getDataSource();
				if(DataSource!=null){
					//拿到进入Excel想要的页数，例如："sheet$A$5"==> sheet 5 0
					String[] dataSourceArr =DataSource.split("\\$");
					if(dataSourceArr.length>3) {
						throw new RuntimeException(iCode+" :的数据源配置有问题(格式为：Sheet3$A$5): "+DataSource);
					}
					//获取sheet页
					String sheetValue =dataSourceArr[0].toString();
					sheet = excelUtil.getWb().getSheet(sheetValue);
					if(sheet==null) {
						throw new RuntimeException(iCode+" :的数据源Sheet页配置有问题 (格式为：Sheet？)："+DataSource);
					}
					//获取第几行
					String rowValue =dataSourceArr[2].toString();
					row = sheet.getRow(Integer.parseInt(rowValue)-1);
					if(row ==null) {
						throw new RuntimeException(iCode +" 的数据源行号有问题(格式为：数字)："+ DataSource);
					}
					//获取第几列
					String cellValue =dataSourceArr[1].toString();
					if(cellValue==null) {
						throw new RuntimeException(iCode+" :的数据源列有问题(格式为：字母)："+ DataSource);
					}
					//获取Excel文件中的收盘价
					try {
						sClose  = row.getCell(excelUtil.transColumn(cellValue)).getNumericCellValue();
					} catch (Exception e) {
						throw new RuntimeException(iCode +" :的数据源内容必须是数字："+ row.getCell(excelUtil.transColumn(cellValue)));
					}
					if(sClose.compareTo(Double.valueOf("9999")) > 0)
					{
						throw new RuntimeException("第" + rowValue + "行,第" + cellValue + "列的值超过允许的最大值9999");
					}
					c++;
					//String 补0
					String strs2 = String.format("%05d%n", count+c);
					String strs3 = strs1+strs2;
					mktSeriess.setS_id(strs3);
				}
				mktSeriess.setI_code(iCode);
				mktSeriess.setA_type(aType);
				mktSeriess.setM_type(mType);
				if(sClose!=null){
					mktSeriess.setS_close(sClose);
					mktSeriess.setS_high(sClose);
				}else{
					throw new RuntimeException("需要导出的收盘价为空");
				}
				mktSeriess.setS_low(0);
				mktSeriess.setS_sell(0);
				mktSeriess.setS_buy(0);
				mktSeriess.setS_mid(0);
				mktSeriess.setS_bank("MARKET");
				mktSeriess.setStatus("3");
				mktSeriess.setBeg_date(currDate);
				mktSeriess.setEnd_date(currDate);
				mktSeriess.setImp_date(currDate);
				list.add(mktSeriess);
			}
			if(list.size() > 0)
			{
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("beg_date", currDate);
				ttMktIrMapper.deleteSeriesByDate(map);
				ttMktIrMapper.insertTtMktSeriessExcel(list);
			}
		} catch (Exception e) {
			log.error("导入行情数据出错",e);
			if(str3=="" || str4==""){
				sb.append("数据来源配置("+DataSource+")不正确");
				log.error("数据来源配置("+DataSource+")不正确,TtMktIrServiceImpl--setStockPriceBysExcel"+e);
			}else if(sheet==null){
				sb.append(e.getMessage());
				log.error("需要导入的("+type+")为空,TtMktIrServiceImpl--setStockPriceBysExcel"+e);
			}else if(row==null){
				sb.append("未找到("+type+")指定的行和列");
				log.error("未找到("+type+")指定的行和列,TtMktIrServiceImpl--setStockPriceBysExcel"+e);
			}else{
				sb.append("EXCEL格式不正确:"+e.getMessage()+",请确认!");
			}
		}
		return sb.toString();
	}

	@Override
	public void updateTtMktSeriess(TtMktSeriess ttMktSeriess) {

		ttMktSerriessMapper.updateByPrimaryKey(ttMktSeriess);
	}

	@Override
	public List<TtMktSeriess> selectTtMktSeriessBySize(
			Map<String, Object> params) {
		return ttMktIrMapper.selectTtMktSeriessBySize(params);
	}

	@Override
	public TtMktSeriess selectSerieByDate(Map<String, Object> params) {
		return ttMktIrMapper.selectSerieByDate(params);
	}
	@Override
	public TtMktSeriess selectSerieByDate2(Map<String, Object> params) {
		return ttMktIrMapper.selectSerieByDate2(params);
	}
	
	@Override
	public TtMktSeriess selectSerieBySid(String Sid) {
		TtMktSeriess ttMktSeriess=ttMktIrMapper.selectSerieBySid(Sid);
		return ttMktSeriess;
	}



}
