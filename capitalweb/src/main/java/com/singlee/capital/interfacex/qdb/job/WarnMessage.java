package com.singlee.capital.interfacex.qdb.job;

import java.util.List;
import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.qdb.model.Message;
import com.singlee.capital.interfacex.qdb.service.MessageService;
public class WarnMessage implements CronRunnable{

	private MessageService messageService = SpringContextHolder.getBean("messageService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		List<Message> messaes = messageService.createMessageInfo();
		System.out.println(messaes.size());
		return true;
	}

	@Override
	public void terminate() {
		
	}

}
