package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCustRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCustRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctNoCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustIdCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCustRec", propOrder = { "acctNoCr", "custIdCr", "amtCr" })
public class CreditCustRec {

	@XmlElement(name = "AcctNoCr", required = true)
	protected String acctNoCr;
	@XmlElement(name = "CustIdCr", required = true)
	protected String custIdCr;
	@XmlElement(name = "AmtCr", required = true)
	protected String amtCr;

	/**
	 * Gets the value of the acctNoCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoCr() {
		return acctNoCr;
	}

	/**
	 * Sets the value of the acctNoCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoCr(String value) {
		this.acctNoCr = value;
	}

	/**
	 * Gets the value of the custIdCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustIdCr() {
		return custIdCr;
	}

	/**
	 * Sets the value of the custIdCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustIdCr(String value) {
		this.custIdCr = value;
	}

	/**
	 * Gets the value of the amtCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtCr() {
		return amtCr;
	}

	/**
	 * Sets the value of the amtCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtCr(String value) {
		this.amtCr = value;
	}

}
