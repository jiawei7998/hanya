package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class GvatXx implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*交易ID*/
	private String tx_id;
	/*数据日期*/
	private String data_date;
	/*机构号*/
	private String brno;
	/*业务流水号*/
	private String seq_no;
	/*交易日期  可选*/
	private String tx_date;
	/*记账日期*/
	private String value_date;
	/*来源系统 IFBM*/
	private String sys_code;
	/*交易代码 可选*/
	private String tx_code;
	/*记账科目代码*/
	private String entry_no;
	/*收入账号 可选*/
	private String act_no;
	/*借贷标识 1：借 2：贷*/
	private String dc_flag;
	/*客户机构号  不提供*/
	private String cust_brno;
	/*客户编号*/
	private String cust_no;
	/*客户账号 可选*/
	private String cust_act_no;
	/*产品类型 可选*/
	private String prod_type;
	/*产品代码 可选*/
	private String prod_code;
	/*币种*/
	private String ccy;
	/*含税收入金额（原币种） 可选*/
	private String tax_inclusive_amt;
	/* 收入金额（原币种）*/
	private String income_amt;
	/*税额(原币种) 可选*/
	private String tax_amt;
	/*税率 可选*/
	private String tax_rate;
	/*折算汇率 可选*/
	private String fx_rate;
	/*含税收入人民币金额 可选*/
	private String cny_tax_inclusive_amt;
	/*收入人民币金额 可选*/
	private String cny_amt;
	/*人民币税额 可选*/
	private String cny_tax_amt;
	/*含税标志 可选*/
	private String inc_tax;
	/*税额币种 可选*/
	private String tax_ccy;
	/*数量 可选*/
	private String num;
	/*单价 可选*/
	private String price_amt;
	/*收款标志 可选*/
	private String pay_flag;
	/*备注信息 可选*/
	private String remarks;
	/*冲正标志   (IFBM没有冲正交易)*/
	private String reverse_flag;
	/*原业务ID 可选**/
	private String ori_tx_id;
	/*业务流水号 可选**/
	private String ori_seq_no;
	private String ori_tx_date;
	private String ref_no;
	private String op_user;
	private String op_ssn;
	private String dept_no;
	private String ext1;
	private String ext2;
	private String ext3;
	private String ext4;
	private String ext5;
	
	
	public String getTx_id() {
		return tx_id;
	}
	public void setTx_id(String tx_id) {
		this.tx_id = tx_id;
	}
	public String getData_date() {
		return data_date;
	}
	public void setData_date(String data_date) {
		this.data_date = data_date;
	}
	public String getBrno() {
		return brno;
	}
	public void setBrno(String brno) {
		this.brno = brno;
	}
	public String getSeq_no() {
		return seq_no;
	}
	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}
	public String getTx_date() {
		return tx_date;
	}
	public void setTx_date(String tx_date) {
		this.tx_date = tx_date;
	}
	public String getValue_date() {
		return value_date;
	}
	public void setValue_date(String value_date) {
		this.value_date = value_date;
	}
	public String getSys_code() {
		return sys_code;
	}
	public void setSys_code(String sys_code) {
		this.sys_code = sys_code;
	}
	public String getTx_code() {
		return tx_code;
	}
	public void setTx_code(String tx_code) {
		this.tx_code = tx_code;
	}
	public String getEntry_no() {
		return entry_no;
	}
	public void setEntry_no(String entry_no) {
		this.entry_no = entry_no;
	}
	public String getAct_no() {
		return act_no;
	}
	public void setAct_no(String act_no) {
		this.act_no = act_no;
	}
	public String getDc_flag() {
		return dc_flag;
	}
	public void setDc_flag(String dc_flag) {
		this.dc_flag = dc_flag;
	}
	public String getCust_brno() {
		return cust_brno;
	}
	public void setCust_brno(String cust_brno) {
		this.cust_brno = cust_brno;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getCust_act_no() {
		return cust_act_no;
	}
	public void setCust_act_no(String cust_act_no) {
		this.cust_act_no = cust_act_no;
	}
	public String getProd_type() {
		return prod_type;
	}
	public void setProd_type(String prod_type) {
		this.prod_type = prod_type;
	}
	public String getProd_code() {
		return prod_code;
	}
	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getTax_inclusive_amt() {
		return tax_inclusive_amt;
	}
	public void setTax_inclusive_amt(String tax_inclusive_amt) {
		this.tax_inclusive_amt = tax_inclusive_amt;
	}
	public String getIncome_amt() {
		return income_amt;
	}
	public void setIncome_amt(String income_amt) {
		this.income_amt = income_amt;
	}
	public String getTax_amt() {
		return tax_amt;
	}
	public void setTax_amt(String tax_amt) {
		this.tax_amt = tax_amt;
	}
	public String getTax_rate() {
		return tax_rate;
	}
	public void setTax_rate(String tax_rate) {
		this.tax_rate = tax_rate;
	}
	public String getFx_rate() {
		return fx_rate;
	}
	public void setFx_rate(String fx_rate) {
		this.fx_rate = fx_rate;
	}
	public String getCny_tax_inclusive_amt() {
		return cny_tax_inclusive_amt;
	}
	public void setCny_tax_inclusive_amt(String cny_tax_inclusive_amt) {
		this.cny_tax_inclusive_amt = cny_tax_inclusive_amt;
	}
	public String getCny_amt() {
		return cny_amt;
	}
	public void setCny_amt(String cny_amt) {
		this.cny_amt = cny_amt;
	}
	public String getCny_tax_amt() {
		return cny_tax_amt;
	}
	public void setCny_tax_amt(String cny_tax_amt) {
		this.cny_tax_amt = cny_tax_amt;
	}
	public String getInc_tax() {
		return inc_tax;
	}
	public void setInc_tax(String inc_tax) {
		this.inc_tax = inc_tax;
	}
	public String getTax_ccy() {
		return tax_ccy;
	}
	public void setTax_ccy(String tax_ccy) {
		this.tax_ccy = tax_ccy;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getPrice_amt() {
		return price_amt;
	}
	public void setPrice_amt(String price_amt) {
		this.price_amt = price_amt;
	}
	public String getPay_flag() {
		return pay_flag;
	}
	public void setPay_flag(String pay_flag) {
		this.pay_flag = pay_flag;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getReverse_flag() {
		return reverse_flag;
	}
	public void setReverse_flag(String reverse_flag) {
		this.reverse_flag = reverse_flag;
	}
	public String getOri_tx_id() {
		return ori_tx_id;
	}
	public void setOri_tx_id(String ori_tx_id) {
		this.ori_tx_id = ori_tx_id;
	}
	public String getOri_seq_no() {
		return ori_seq_no;
	}
	public void setOri_seq_no(String ori_seq_no) {
		this.ori_seq_no = ori_seq_no;
	}
	public String getOri_tx_date() {
		return ori_tx_date;
	}
	public void setOri_tx_date(String ori_tx_date) {
		this.ori_tx_date = ori_tx_date;
	}
	public String getRef_no() {
		return ref_no;
	}
	public void setRef_no(String ref_no) {
		this.ref_no = ref_no;
	}
	public String getOp_user() {
		return op_user;
	}
	public void setOp_user(String op_user) {
		this.op_user = op_user;
	}
	public String getOp_ssn() {
		return op_ssn;
	}
	public void setOp_ssn(String op_ssn) {
		this.op_ssn = op_ssn;
	}
	public String getDept_no() {
		return dept_no;
	}
	public void setDept_no(String dept_no) {
		this.dept_no = dept_no;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public String getExt4() {
		return ext4;
	}
	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}
	public String getExt5() {
		return ext5;
	}
	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}

}
