package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportInterBankBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountType;//账户类型
	
	private String productCode;//交易代码
	
	private String prdName;//业务类型

	private String partyName;//客户名称
	
	private String custType;//客户类型
	
	private String custActType;//会计类型

	private double amt;//金额

	private String rateType;//利率类型
	
	private double contractRate;//利率
	
	private String basis;//计息方式
	
	private String vDate;//起息日
	
	private String mDate;//到期日
	
	private String term;//期限
	
	private String intFre;//付息频率
	
	private double interestAmt;//下次付息金额
	
	private double mAmt;//本息合计
	
	private String sponInst;//发起机构
	
	private double feesAmt;//中介费用
	
	private String instName;//机构名称
	
	private String baseProductRemark;//备注
	private String cost            ;
	private String port            ;
	private String organization;//中介机构
	
	
	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getCustActType() {
		return custActType;
	}

	public void setCustActType(String custActType) {
		this.custActType = custActType;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public double getContractRate() {
		return contractRate;
	}

	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getIntFre() {
		return intFre;
	}

	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}

	public double getInterestAmt() {
		return interestAmt;
	}

	public void setInterestAmt(double interestAmt) {
		this.interestAmt = interestAmt;
	}

	public double getmAmt() {
		return mAmt;
	}

	public void setmAmt(double mAmt) {
		this.mAmt = mAmt;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public double getFeesAmt() {
		return feesAmt;
	}

	public void setFeesAmt(double feesAmt) {
		this.feesAmt = feesAmt;
	}

	public String getBaseProductRemark() {
		return baseProductRemark;
	}

	public void setBaseProductRemark(String baseProductRemark) {
		this.baseProductRemark = baseProductRemark;
	}
	
}
