package com.singlee.capital.interfacex.qdb.ESB.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import com.bqd.cms.Client;
//import com.bqd.util.ServerMessage;

public class FileTransferUtil {
	
	//private static Client client = new Client();
	/**
	 * 文件上传至影像平台指定流水号
	 * @param busiSerialNo 流水号为空则默认 ******_FIBS_99999999
	 * @param path 上传的文件路径，或上传的文件夹
	 * @return
	 * 上传文件至同一流水号，会自动覆盖前面的文件。如果想上传多个文件可以上传文件夹。
	 * 每次的上传影像平台都会保存，只是下载时会下载最新时间段的上传内容
	 */
	public static String fileUpload(String busiSerialNo, String path) throws Exception{
//		Client client = new Client();
		if(busiSerialNo == null) {
			busiSerialNo = getCurrentTimeNo();
		}
		String ret = "FAIL";
		try {
//			ret = client.uploadByPathAndSerialNo(path, busiSerialNo, "OL_PT");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}
	
	/**
	 * 从影像平台下载指定流水号的文件
	 * @param busiSerialNo 流水号为空则默认 ******_FIBS_99999999
	 * @param path 下载文件本地存放的地址。（文件夹）
	 * @return
	 * 如果有多次上传，影像平台会返回最新的上传的内容。
	 * 每个流水号对应一个储存空间。
	 */
	public static String fileDownload(String busiSerialNo, String path) throws Exception{
//		Client client = new Client();
		if(busiSerialNo == null) {
			busiSerialNo = getToday()+"_FIBS_99999999";
		}
		String ret = "FAIL";
		try {
//			ret = client.downloadByBusiSerialNo(path, busiSerialNo, "OL_PT", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;

	}
	
	
	/**
	 * 修改已经上传至影像平台的文件信息,这个接口有问题尽量别用
	 * @param busiSerialNo 文件流水号
	 * @param option 操作：add（追加）、repalce（替换）、delete（删除）
	 * @param fileName 文件标识
	 * @param path 文件路径
	 * @return
	 */
	public static String fileUpdate(String busiSerialNo, String option, String fileName, String path) throws Exception{
//		Client client = new Client(true);
//		ServerMessage.setUserName("onLine1");
		try {
//			client.login();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if(busiSerialNo == null) {
			busiSerialNo = getCurrentTimeNo();
		}
		String ret = "FAIL";
		try {
//			ret = client.updateByBusiSerial(busiSerialNo, "OL_PT", option, fileName, path);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}
	
	/**
	 * 删除影像平台的文件
	 * @param busiSerialNo 文件流水号
	 * @return
	 * 删除功能用不了的时候，可以上传新内容覆盖需要删除的文件，实现软删除
	 */
	public static String fileDelete(String busiSerialNo) throws Exception{
//		Client client = new Client();
		if(busiSerialNo == null) {
			busiSerialNo = getCurrentTimeNo();
		}
		String ret = "FAIL";
		try {
//			ret = client.deleteByBusiSerialNo(busiSerialNo, "OL_PT");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}
	
	/**
	 * 获取今天日期
	 * 例子：20170505
	 */
	public static String getToday() {
		String day = new SimpleDateFormat("yyyyMMdd").format(new Date());
		//业务发生日期_系统标示_业务系统交易流水号
		return day;
	}
	
	/**
	 * 获取昨天日期
	 * 例子：20170504
	 */
	public static String getYesterday() {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, -1);
		String yesterday = new SimpleDateFormat("yyyyMMdd").format(now.getTime());
		//业务发生日期_系统标示_业务系统交易流水号
		return yesterday;
	}
	
	/**
	 * 自定义系统标识和交易号，返回对应今天的流水号
	 * @param systemFlagAndCode 系统标识和交易号
	 */
	public static String getBusiSerialToday(String systemFlagAndCode) {
		String today = getToday() +"_"+systemFlagAndCode;
		return today;
	}
	
	/**
	 * 自定义系统标识和交易号，返回对应的昨天的流水号
	 * @param systemFlagAndCode 系统标识和交易号
	 */
	public static String getBusiSerialYesterday(String systemFlagAndCode) {
		String yesterday = getYesterday() +"_"+systemFlagAndCode;
		return yesterday;
	}
	
	/**
	 * 自定义交易号，返回本系统对应的今天的流水号
	 * @param code 交易号
	 */
	public static String getBusiSerialTodayByCode(String code) {
		String today = getToday() +"_FIBS_"+ code;
		return today;
	}
	
	/**
	 * 自定义交易号，返回本系统对应的昨天的流水号
	 * @param code 交易号
	 */
	public static String getBusiSerialYesterdayByCode(String code) {
		String yesterday = getYesterday() +"_FIBS_"+ code;
		return yesterday;
	}
	
	/**
	 * 得到当前时间的唯一标识交易号
	 */
	public static String getCurrentTimeCode() {
		String code = new SimpleDateFormat("HHmmssSS").format(new Date());
		int c = (int) (Math.random()*100);
		return code+c;
	}
	
	/**
	 * 得到当前唯一流水号
	 */
	public static String getCurrentTimeNo() {
		String code = new SimpleDateFormat("HHmmssSS").format(new Date());
		int c = (int) (Math.random()*100);
		return getBusiSerialTodayByCode(code+c);
	}
	
}
