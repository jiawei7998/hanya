package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class UPPSChkFileEndDayBatch implements Serializable {

	/**
	 * 对账文件申请
	 * 支付日终后，请求后轮询
	 * 1、GTP配置IFBM与支付；
	 */
	private static final long serialVersionUID = 1L;
	
	private String ChannelCode     ;
	private String ChannelSerNo    ;
	private String PayPathCode     ;
	private String ModeFlag        ;
	private String WorkDate        ;
	private String WorkTime        ;
	private String AgentSerialNo   ;
	private String BankDate        ;
	private String BankId          ;
	private String FeeBaseLogId    ;
	private String EntrustDate     ;
	private String MsgId           ;
	private String SettlementDate  ;
	private String ComeFromFlag    ;
	private String Currency        ;
	private String Amount          ;
	private String PayerAcc        ;
	private String PayerName       ;
	private String RealPayerAcc    ;
	private String RealPayerName   ;
	private String PayeeAcc        ;
	private String PayeeName       ;
	private String RealPayeeAcc    ;
	private String RealPayeeName   ;
	private String PayResult       ;
	public String getChannelCode() {
		return ChannelCode;
	}
	public void setChannelCode(String channelCode) {
		ChannelCode = channelCode;
	}
	public String getChannelSerNo() {
		return ChannelSerNo;
	}
	public void setChannelSerNo(String channelSerNo) {
		ChannelSerNo = channelSerNo;
	}
	public String getPayPathCode() {
		return PayPathCode;
	}
	public void setPayPathCode(String payPathCode) {
		PayPathCode = payPathCode;
	}
	public String getModeFlag() {
		return ModeFlag;
	}
	public void setModeFlag(String modeFlag) {
		ModeFlag = modeFlag;
	}
	public String getWorkDate() {
		return WorkDate;
	}
	public void setWorkDate(String workDate) {
		WorkDate = workDate;
	}
	public String getWorkTime() {
		return WorkTime;
	}
	public void setWorkTime(String workTime) {
		WorkTime = workTime;
	}
	public String getAgentSerialNo() {
		return AgentSerialNo;
	}
	public void setAgentSerialNo(String agentSerialNo) {
		AgentSerialNo = agentSerialNo;
	}
	public String getBankDate() {
		return BankDate;
	}
	public void setBankDate(String bankDate) {
		BankDate = bankDate;
	}
	public String getBankId() {
		return BankId;
	}
	public void setBankId(String bankId) {
		BankId = bankId;
	}
	public String getFeeBaseLogId() {
		return FeeBaseLogId;
	}
	public void setFeeBaseLogId(String feeBaseLogId) {
		FeeBaseLogId = feeBaseLogId;
	}
	public String getEntrustDate() {
		return EntrustDate;
	}
	public void setEntrustDate(String entrustDate) {
		EntrustDate = entrustDate;
	}
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	public String getSettlementDate() {
		return SettlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		SettlementDate = settlementDate;
	}
	public String getComeFromFlag() {
		return ComeFromFlag;
	}
	public void setComeFromFlag(String comeFromFlag) {
		ComeFromFlag = comeFromFlag;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getPayerAcc() {
		return PayerAcc;
	}
	public void setPayerAcc(String payerAcc) {
		PayerAcc = payerAcc;
	}
	public String getPayerName() {
		return PayerName;
	}
	public void setPayerName(String payerName) {
		PayerName = payerName;
	}
	public String getRealPayerAcc() {
		return RealPayerAcc;
	}
	public void setRealPayerAcc(String realPayerAcc) {
		RealPayerAcc = realPayerAcc;
	}
	public String getRealPayerName() {
		return RealPayerName;
	}
	public void setRealPayerName(String realPayerName) {
		RealPayerName = realPayerName;
	}
	public String getPayeeAcc() {
		return PayeeAcc;
	}
	public void setPayeeAcc(String payeeAcc) {
		PayeeAcc = payeeAcc;
	}
	public String getPayeeName() {
		return PayeeName;
	}
	public void setPayeeName(String payeeName) {
		PayeeName = payeeName;
	}
	public String getRealPayeeAcc() {
		return RealPayeeAcc;
	}
	public void setRealPayeeAcc(String realPayeeAcc) {
		RealPayeeAcc = realPayeeAcc;
	}
	public String getRealPayeeName() {
		return RealPayeeName;
	}
	public void setRealPayeeName(String realPayeeName) {
		RealPayeeName = realPayeeName;
	}
	public String getPayResult() {
		return PayResult;
	}
	public void setPayResult(String payResult) {
		PayResult = payResult;
	}

}
