package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TT_MKT_IR 市场利率行情定义
 * 
 * @author Libo Create Time 2012-12-19 11:48:52
 */

@Entity
@Table(name = "TT_MKT_IR")
public class TtMktIr implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**    */
	@Id
	private String i_code;

	/**    */
	private String a_type;

	/**    */
	private String m_type;

	/** 币种 */
	private String currency;

	/** 报价方式 */
	private String q_type;

	/** 计息基准 */
	private String ir_daycount;

	/** 名称 */
	private String ir_name;

	/** 期限 */
	private String ir_term;

	/** 导入时间 */
	private String imp_date;

	/** 来源 */
	private String data_source;

	/** ------- Generate Getter And Setter -------- **/

	public String getI_code() {
		return this.i_code;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtMktIr [i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", q_type=");
		builder.append(q_type);
		builder.append(", ir_daycount=");
		builder.append(ir_daycount);
		builder.append(", ir_name=");
		builder.append(ir_name);
		builder.append(", ir_term=");
		builder.append(ir_term);
		builder.append(", imp_date=");
		builder.append(imp_date);
		builder.append(", data_source=");
		builder.append(data_source);
		builder.append("]");
		return builder.toString();
	}

	public void setI_code(String i_code) {
		this.i_code = i_code;
	}

	public String getA_type() {
		return this.a_type;
	}

	public void setA_type(String a_type) {
		this.a_type = a_type;
	}

	public String getM_type() {
		return this.m_type;
	}

	public void setM_type(String m_type) {
		this.m_type = m_type;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getQ_type() {
		return this.q_type;
	}

	public void setQ_type(String q_type) {
		this.q_type = q_type;
	}

	public String getIr_daycount() {
		return this.ir_daycount;
	}

	public void setIr_daycount(String ir_daycount) {
		this.ir_daycount = ir_daycount;
	}

	public String getIr_name() {
		return this.ir_name;
	}

	public void setIr_name(String ir_name) {
		this.ir_name = ir_name;
	}

	public String getIr_term() {
		return this.ir_term;
	}

	public void setIr_term(String ir_term) {
		this.ir_term = ir_term;
	}

	public String getImp_date() {
		return this.imp_date;
	}

	public void setImp_date(String imp_date) {
		this.imp_date = imp_date;
	}

	public String getData_source() {
		return this.data_source;
	}

	public void setData_source(String data_source) {
		this.data_source = data_source;
	}

	
	public void setInstrumentId(String s){
	}
	public String getInstrumentId(){
		return String.format("%s,%s,%s",i_code,a_type,m_type);
	}
}