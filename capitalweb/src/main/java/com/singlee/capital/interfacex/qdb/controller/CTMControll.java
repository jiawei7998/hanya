package com.singlee.capital.interfacex.qdb.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.dayend.service.impl.InitDayendFlowHandler;
import com.singlee.capital.interfacex.qdb.ESB.controller.OrgUserSynchService;
import com.singlee.capital.interfacex.qdb.service.CleanLogService;
import com.singlee.capital.interfacex.qdb.service.PostingService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.acc.service.AccForTradeAccessService;
@Controller
@RequestMapping("CTMControll")
public class CTMControll extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("CRON");

	@Autowired
	private PostingService postingService;
	@Autowired
	private OrgUserSynchService orgUserService ;
	@Autowired
	private CleanLogService cleanLogService ;
	@Autowired
	private AccForTradeAccessService accessService;
	@Autowired
	private InitDayendFlowHandler initDayendFlowHandler;
	
	@RequestMapping("batch")
	@ResponseBody
	public int ctm(HttpServletRequest request,HttpServletResponse response){
		String flag="";
		SlSession session = null;
		
		try {
			flag=request.getParameter("flag");
			
			log.info("跑批准备执行第"+flag+"步");
			if("1".equals(flag)){
				initDayendFlowHandler.execute(session);
				log.info("切换日期批量步骤执行完成");
				return 1;
				
			}else if("2".equals(flag)){//日终每日计提核算
				accessService.getNeedForAccDrawingDatasStepOne(null);
				log.info("日终每日计提核算执行完成");
				return 1;	
				
			}else if("3".equals(flag)){
				accessService.getNeedForCurrentAccDrawingDatasStepOne(null);//日终每日计提核算(活期) 
				log.info("日终每日计提核算(活期)");
				
				accessService.getNeedForSettleDatasStepOne(null); //日终结息日核算(活期)
				log.info("日终结息日核算(活期)");
				return 1;
				
			}else if("4".equals(flag)){//摊销计提
				accessService.getNeedForAccDrawingDatesForAmorStepThree(null);
				accessService.getNeedForMtmDatasStepOne(null);
				log.info("摊销计提执行完成");
				return 1;
				
			}else if("5".equals(flag)){
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("postDate", DayendDateServiceProxy.getInstance().getSettlementDate());			
				postingService.createAcup(map);
				log.info("createAcup执行完成");
				postingService.createBcup(map);
				log.info("createBcup执行完成");
				postingService.createFIBFile(map);
				log.info("createFIBFile执行完成");
				return 1;
				
			}else if("6".equals(flag)){
				initDayendFlowHandler.postprocess(session,null);
				log.info("切换日期批量步骤执行完成");
				return 1;
				
			}else if("7".equals(flag)){
				orgUserService.snycMager();
				cleanLogService.zipAndCleanning();
				log.info("同步柜员信息和机构信息完成");
				return 1;
				
			}
			return -1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("执行批量任务报错："+flag+"-----"+e.getMessage(),e);
			return -1;
		}
			
	}
}
