package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.CheckPayment;
import com.singlee.capital.interfacex.model.Payment;
import com.singlee.capital.interfacex.model.SelfPayment;
import com.singlee.capital.interfacex.model.TExReconT24lz;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;

public interface Ti2ndPaymentMapper  {

	public int insertTi2ndPayment(UPPSCdtTrfRq uppsCdtTrfRq) throws Exception;
	
	public int queryTi2ndPaymentCount(String feeBaseLogid) throws Exception;
	
	public UPPSCdtTrfRq queryTi2ndPayment(Map<String,Object> map) throws Exception;
	
	public int update2ndPaymentRetMsg(Map<String,Object> map)  throws Exception;
	
	public Page<Payment> query2ndPaymentPage(Map<String, Object> map, RowBounds rb);
	
	public List<UPPSCdtTrfRq> queryT2ndPaymentConfirmTrade(Map<String, Object> map) throws Exception;
	
	public List<UPPSCdtTrfRq> payment2ndUPPSigBusiQueryQuartzJob(Map<String, Object> map) throws Exception;
	
	public Page<CheckPayment> Check2ndPaymentPage(Map<String, Object> map, RowBounds rb);
	
	public Page<SelfPayment> SelfPaymentPage(Map<String, Object> map, RowBounds rb);
	
	void deleteTi2ndPayment(String feeBaseLogid) throws Exception;
	public int queryTi2ndPaymentCount2(String feeBaseLogid) throws Exception;
	public UPPSCdtTrfRq queryTi2ndPayment2 (Map<String,Object> map) throws Exception;
	public Page<Payment> query2ndPaymentPage2(Map<String, Object> map, RowBounds rb);
	public List<Payment> query2ndPaymentPage(Map<String, Object> map);
	public List<Payment> query2ndPaymentPage2(Map<String, Object> map);
	public int updateHandleCodeRemarkNote(Map<String,Object> map)  throws Exception;
	public int querySrquidCount(String srquid) throws Exception;

	public TExReconT24lz searchTExReconT24lz(Map<String,Object> map) throws Exception;
}
