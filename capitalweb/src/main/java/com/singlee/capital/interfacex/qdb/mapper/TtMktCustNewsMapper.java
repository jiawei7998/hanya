package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktCustNews;

import tk.mybatis.mapper.common.Mapper;

public interface TtMktCustNewsMapper extends Mapper<TtMktCustNews>{
	/**
	 * 查询舆情信息管理，分页
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktCustNews> sreachTtMktCustNews(Map<String, Object> params,RowBounds rowBounds);
	
	/**
	 * 批量插入数据
	 * @param list
	 */
	public void insertTtMktCustNewsExcel(List<TtMktCustNews> list);
}
