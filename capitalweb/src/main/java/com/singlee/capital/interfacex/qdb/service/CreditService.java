package com.singlee.capital.interfacex.qdb.service;

import java.io.Serializable;
import java.util.Map;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.trade.model.TtTrdTrade;

/****
 * 
 * 授信接口
 * @author singlee
 *
 */
public interface CreditService {
	
	public void creditOperation(TtTrdTrade ttTrdTrade,Object o,String approveStatus)throws Exception;
	
	
	public void creditRelease(String dealNo,String approveStatus,String releaseAmt)throws Exception;

	
	public boolean reback(String dealNo);
	
	public RetMsg<Serializable> checkUserCreditLimit(Map<String, Object> param);
	
	
}
