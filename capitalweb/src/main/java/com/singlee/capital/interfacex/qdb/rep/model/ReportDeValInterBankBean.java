package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportDeValInterBankBean implements Serializable{

	/**
	 * 2.金市存放同业定期、拆出资金、买入返售债券业务减值基础数据表
	 */
	private static final long serialVersionUID = 1L;
	private String dealNo      ;//合同编号                       
	private String acctInst    ;//记账单位名称                   
	private String acctNo      ;//账号                           
	private String custName    ;//同业/金融性公司名称            
	private String custTypeName;//交易对手类别                   
	private String custLocate  ;//地区分析                       
	private String vDate       ;//起存日/买入结算日期（起息日）  
	private String mDate       ;//止存日/返售结算日期（止息日）  
	private double amt         ;//期末余额（折人民币）           
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	private double rate        ;//年利率                         
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getAcctInst() {
		return acctInst;
	}
	public void setAcctInst(String acctInst) {
		this.acctInst = acctInst;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustTypeName() {
		return custTypeName;
	}
	public void setCustTypeName(String custTypeName) {
		this.custTypeName = custTypeName;
	}
	public String getCustLocate() {
		return custLocate;
	}
	public void setCustLocate(String custLocate) {
		this.custLocate = custLocate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	

	
	
	
	
}
