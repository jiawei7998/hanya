package com.singlee.capital.interfacex.thread;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.service.MessageQueueService;

@Component
public class MqAccountiingThreadHandler implements Runnable {
	
	private MessageQueueService messageQueueService;
	
	public MqAccountiingThreadHandler(){
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		messageQueueService = SpringContextHolder.getBean("messageQueueService");
		while(true){
			 try {
				 LogManager.getLogger(LogManager.MODEL_MQ).info("MQ-1*******************************************************");
				 TimeUnit.MILLISECONDS.sleep(60000);
				 messageQueueService.TsaMessageQueueService();
				 LogManager.getLogger(LogManager.MODEL_MQ).info("*******************************************************MQ-1");
			 } catch (Exception e) {
		            // TODO 记日志
				 LogManager.getLogger(LogManager.MODEL_MQ).debug(e);
		     }
		}
	}

}
