package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditBeaRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CreditBeaRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcyTypeCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TheirRefCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditBeaRec", propOrder = { "creditMedType", "acctNoCr",
		"fcyTypeCr", "theirRefCr", "amtCr" })
public class CreditBeaRec {

	@XmlElement(name = "CreditMedType", required = true)
	protected String creditMedType;
	@XmlElement(name = "AcctNoCr", required = true)
	protected String acctNoCr;
	@XmlElement(name = "FcyTypeCr", required = true)
	protected String fcyTypeCr;
	@XmlElement(name = "TheirRefCr", required = true)
	protected String theirRefCr;
	@XmlElement(name = "AmtCr", required = true)
	protected String amtCr;

	/**
	 * Gets the value of the creditMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditMedType() {
		return creditMedType;
	}

	/**
	 * Sets the value of the creditMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditMedType(String value) {
		this.creditMedType = value;
	}

	/**
	 * Gets the value of the acctNoCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoCr() {
		return acctNoCr;
	}

	/**
	 * Sets the value of the acctNoCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoCr(String value) {
		this.acctNoCr = value;
	}

	/**
	 * Gets the value of the fcyTypeCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcyTypeCr() {
		return fcyTypeCr;
	}

	/**
	 * Sets the value of the fcyTypeCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcyTypeCr(String value) {
		this.fcyTypeCr = value;
	}

	/**
	 * Gets the value of the theirRefCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTheirRefCr() {
		return theirRefCr;
	}

	/**
	 * Sets the value of the theirRefCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTheirRefCr(String value) {
		this.theirRefCr = value;
	}

	/**
	 * Gets the value of the amtCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtCr() {
		return amtCr;
	}

	/**
	 * Sets the value of the amtCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtCr(String value) {
		this.amtCr = value;
	}

}
