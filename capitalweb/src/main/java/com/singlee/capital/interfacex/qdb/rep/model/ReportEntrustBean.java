package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportEntrustBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prdName               ;  //产品名称                 
	private double amt                   ;  //初始委托投资额（亿元）   
	private String port                  ;
	private double cpriceCost            ;  //净值(按成本)             
	private double cpriceMarket          ;  //净值(按市值)             
	private double rate                  ;  //产品计划收益率           
	private double contractRate          ;  //过去一年的预计年化收益率 
	private String vDate                 ;  //产品起息日               
	private String mDate                 ;  //产品到期日               
	private String intFre                ;  //付息频率                 
	private String theoryPaymentDate     ;  //上期付息时间             
	private double interestAmt           ;  //上期付息金额   
	private String cost                  ;  //成本中心      
	private double cpriceAssetCost       ;  //净值(按成本)             
	private double cpriceAssetMarket     ;  //净值(按市值)    
	private double asset                 ;  //资产类合计             
	private double debt                  ;  //负债类合计   
	private double totalSecInv           ;  //证券投资合计
	private double settlAmt              ;  //期末资产总额
	

	
	public double getTotalSecInv() {
		return totalSecInv;
	}
	public void setTotalSecInv(double totalSecInv) {
		this.totalSecInv = totalSecInv;
	}
	public double getAsset() {
		return asset;
	}
	public void setAsset(double asset) {
		this.asset = asset;
	}
	public double getDebt() {
		return debt;
	}
	public void setDebt(double debt) {
		this.debt = debt;
	}
	public double getCpriceAssetCost() {
		return cpriceAssetCost;
	}
	public void setCpriceAssetCost(double cpriceAssetCost) {
		this.cpriceAssetCost = cpriceAssetCost;
	}
	public double getCpriceAssetMarket() {
		return cpriceAssetMarket;
	}
	public void setCpriceAssetMarket(double cpriceAssetMarket) {
		this.cpriceAssetMarket = cpriceAssetMarket;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getCpriceCost() {
		return cpriceCost;
	}
	public void setCpriceCost(double cpriceCost) {
		this.cpriceCost = cpriceCost;
	}
	public double getCpriceMarket() {
		return cpriceMarket;
	}
	public void setCpriceMarket(double cpriceMarket) {
		this.cpriceMarket = cpriceMarket;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getContractRate() {
		return contractRate;
	}
	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getIntFre() {
		return intFre;
	}
	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}
	public String getTheoryPaymentDate() {
		return theoryPaymentDate;
	}
	public void setTheoryPaymentDate(String theoryPaymentDate) {
		this.theoryPaymentDate = theoryPaymentDate;
	}
	public double getInterestAmt() {
		return interestAmt;
	}
	public void setInterestAmt(double interestAmt) {
		this.interestAmt = interestAmt;
	}
	public double getSettlAmt() {
		return settlAmt;
	}
	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}
	

}
