package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 基准利率数据来源配置表
 * @author Administrator
 *
 */

@Table(name = "TT_MKT_IR_CONFIG")
@Entity
public class TtMktIrConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**  基准利率代码  */
	private String i_code;
	
	/**  市场类型  */
	private String m_type;
	
	public String getI_code() {
		return i_code;
	}
	public void setI_code(String i_code) {
		this.i_code = i_code;
	}
	public String getM_type() {
		return m_type;
	}
	public void setM_type(String m_type) {
		this.m_type = m_type;
	}
	public String getQ_type() {
		return q_type;
	}
	public void setQ_type(String q_type) {
		this.q_type = q_type;
	}
	/** 报价方式 */
	private String q_type;
	//利率类型
	private String rateType;
	//期限
	private String term;
	//币种
	private String ccy;
	//数据来源系统
	private String sourceSys;
	//数据来源配置
	private String dataSource;
	//最后修改时间
	private String lstmntDate;
	//备注1
	private String note1;
	//备注2
	private String note2;
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getSourceSys() {
		return sourceSys;
	}
	public void setSourceSys(String sourceSys) {
		this.sourceSys = sourceSys;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	public String getLstmntDate() {
		return lstmntDate;
	}
	public void setLstmntDate(String lstmntDate) {
		this.lstmntDate = lstmntDate;
	}
	public String getNote1() {
		return note1;
	}
	public void setNote1(String note1) {
		this.note1 = note1;
	}
	public String getNote2() {
		return note2;
	}
	public void setNote2(String note2) {
		this.note2 = note2;
	}
	
}
