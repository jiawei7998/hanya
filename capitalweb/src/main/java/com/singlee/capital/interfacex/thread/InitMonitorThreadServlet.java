package com.singlee.capital.interfacex.thread;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

public class InitMonitorThreadServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	@Autowired
	private ThreadMonitor threadMonitor;  
    @Autowired
    MqAccountiingThreadHandler mqAccountiingThreadHandler;
    public InitMonitorThreadServlet(){  
    }  
    //private List<Thread> monitoredThread;//需要被监控的线程
    
    @Override
    public void init(){
//        String str = null;  
//        monitoredThread = new ArrayList<Thread>();//初始化
//        Thread ownerThread = new Thread(mqAccountiingThreadHandler,"NUPD");
//        ownerThread.start();
//        monitoredThread.add(ownerThread);
//        if (str == null && threadMonitor != null) {
//        	threadMonitor.setMonitoredThread(monitoredThread);//加入监控队列
//        	threadMonitor.start();  
//        }  
    }  
  
    @Override
    public void doGet(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
        throws ServletException, IOException{  
    }  
  
    public void destory(){  
        if (threadMonitor != null && threadMonitor.isInterrupted()) {  
            threadMonitor.interrupt();  
        }  
    }

}
