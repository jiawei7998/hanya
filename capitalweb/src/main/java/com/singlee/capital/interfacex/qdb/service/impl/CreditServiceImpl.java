package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.mapper.FBZMapper;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.service.CreditService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaUserParamMapper;
import com.singlee.capital.system.model.TaUserParam;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.listener.CreditListener;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditServiceImpl implements CreditService,CreditListener{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	@Resource
	private FBZMapper fbzMapper;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private TaUserParamMapper userParamMapper;
	
	@Override
	public void commitLimit(String dealNo, Object o,String approveStatus){
		FBZ003 fbz = fbzMapper.getFbzByOrder(dealNo);
		FBZ003 f=null;
		if(fbz == null)
		{
			log.info("冻结额度：系统中未查询到相关额度记录");
			return;
		}
		
		if("2".equals(fbz.getContentType())&&!"03".equals(fbz.getOperTyp())){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(dealNo);
			String mdate = main.getmDate() == null ? PlaningTools.addOrSubMonth_Day(main.getvDate(), Frequency.YEAR, 5) : main.getmDate();
			fbz.setMaturity(mdate.replaceAll("-", "/"));
			
			fbz.setFrzSum(String.valueOf(main.getAmt()));
			fbz.setFrzcurrency(fbz.getCurrency());
			fbz.setOperTyp("03");
			fbz.setSerialNo("");
			f = sendReqFbz003Flag(fbz);
		}
		if(f!=null&&"15700000000000".equals(f.getRespCde())){
			f.setDealNo(fbz.getDealNo());
			f.setLimitBalance(f.getFrzSum());
			updateFbz(f);
			log.info("CreditServiceImpl额度：提交放款审批 冻结额度成功");
			JY.info("------------额度冻结完成！---------");	
		}else{
			log.info("CreditServiceImpl额度：提交放款审批 冻结额度失败");
			JY.info("------------额度冻结有误！---------");
		}
	}
	
	@Override
	public void creditOperation(TtTrdTrade ttTrdTrade, Object o,String approveStatus) throws Exception {
		FBZ003 ff =null;
		if(DictConstants.ApproveStatus.New.equals(approveStatus)){//提交放款审批 冻结额度
			TtTrdTrade trade = (TtTrdTrade)o;			
			FBZ003 fbz = fbzMapper.getFbzByOrder(trade.getTrade_id());
			if(fbz == null)
			{
				log.info("冻结额度：系统中未查询到相关额度记录");
				return;
			}
				
			if("1".equals(fbz.getContentType()))//企业客户不冻结
			{
				return;
			}
			TdProductApproveMain main = productApproveService.getProductApproveActivated(ttTrdTrade.getTrade_id());
			String mdate = main.getmDate() == null ? PlaningTools.addOrSubMonth_Day(main.getvDate(), Frequency.YEAR, 5) : main.getmDate();
			fbz.setMaturity(mdate.replaceAll("-", "/"));
			
			fbz.setFrzSum(String.valueOf(trade.getSettleamount()));
			fbz.setOperTyp("03");
			boolean f = sendReqFbz003(fbz);
			if(f){
				insertFbz(fbz);
				log.info("CreditServiceImpl额度：提交放款审批 冻结额度成功");
				JY.info("------------额度冻结完成！---------");	
			}else{
				log.info("CreditServiceImpl额度：提交放款审批 冻结额度失败");
				JY.info("------------额度冻结有误！---------");
			}		
		}else if(DictConstants.ApproveStatus.ApprovedNoPass.equals(approveStatus)){//审批未通过解冻额度 
			TtTrdTrade trade = (TtTrdTrade)o;
			FBZ003 fbz = fbzMapper.getFbzByOrder(trade.getTrade_id());
			if(fbz != null){
				if("1".equals(fbz.getContentType()))//企业客户
				{
					return;
				}
				fbz.setFrzSum(String.valueOf(trade.getSettleamount()));
				fbz.setFrzcurrency(fbz.getCurrency());
				
				fbz.setOperTyp("04");
				boolean f = sendReqFbz003(fbz);
				if(f){
					updateFbz(fbz);
					log.info("CreditServiceImpl额度：审批未通过释放额度 成功");
					JY.info("------------额度释放完成！---------");	
				}else{
					log.info("CreditServiceImpl额度：审批未通过释放额度 失败");
					JY.info("------------额度释放有误！---------");
				}			
			}			
		}else if(DictConstants.ApproveStatus.ApprovedPass.equals(approveStatus)){//审批通过
			if (DictConstants.TrdType.Custom.equals(ttTrdTrade.getTrdtype())){//交易审批通过 占用额度
				TdProductApproveMain main = (TdProductApproveMain)o;
				FBZ003 fbz = fbzMapper.getFbzByOrder(main.getDealNo());
				if(fbz != null)
				{
					fbz.setFrzSum(String.valueOf(ttTrdTrade.getSettleamount()));
					
					boolean f = false;
					if("1".equals(fbz.getContentType())){// 1 企业  通知放款成功
						f = sendReqFbz006(fbz);
						
					}else{ // 2 同业 同业直接占用额度
						fbz.setOperTyp("01");// 01 占用 
						String mdate = main.getmDate() == null ? PlaningTools.addOrSubMonth_Day(main.getvDate(), Frequency.YEAR, 5) : main.getmDate();
						fbz.setMaturity(mdate.replaceAll("-", "/"));
						fbz.setFrzcurrency(fbz.getCurrency());
						
						ff = sendReqFbz003Flag(fbz);
						f=ff!=null?true:false;
						fbz.setSerialNo(ff.getSerialNo());
					}
					
					if(f){
						updateFbz(fbz);
						log.info("CreditServiceImpl额度：交易审批通过 占用额度成功");
						JY.info("------------额度占用完成！---------");	
					}else{
						log.info("CreditServiceImpl额度：交易审批通过 占用额度失败");
						JY.info("------------额度占用有误！---------");
					}
				}					
				
			}else if (DictConstants.TrdType.CustomOutRightSale.equals(ttTrdTrade.getTrdtype())) {//资产卖断 释放额度
//				TdOutrightSale outrightSale = (TdOutrightSale)o;
//				FBZ003 fbz = fbzMapper.getFbzByOrder(outrightSale.getDealNo());
//				if(fbz!=null){
//					fbz.setOperTyp("02");
//					boolean f=sendReq(fbz,"2");
//					if(f){
//						updateFbz(fbz);
//						log.info("CreditServiceImpl额度：资产卖断 释放额度成功");
//						JY.info("------------额度释放完成！---------");	
//					}else{
//						log.info("CreditServiceImpl额度：资产卖断 释放额度释放");
//						JY.info("------------额度释放有误！---------");
//					}
//				}					
				
			}else if(DictConstants.TrdType.AccountTradeOffset.equals(ttTrdTrade.getTrdtype())){//审批撤销 释放额度
				FBZ003 fbz = fbzMapper.getFbzByOrder(ttTrdTrade.getRef_tradeid());
				TdProductApproveMain main = productApproveService.getProductApproveActivated(ttTrdTrade.getRef_tradeid());
				if(fbz != null){
					if("1".equals(fbz.getContentType()))//企业客户
					{
						boolean f = sendReqFbz006(fbz);
						if(f){
							fbzMapper.deleteFbz(fbz);
							log.info("CreditServiceImpl额度：审批撤销 释放额度成功");
							JY.info("------------额度释放完成！---------");	
						}else{
							log.info("CreditServiceImpl额度：审批撤销 释放额度失败");
							JY.info("------------额度释放有误！---------");
						}
					}else{
						String mdate = main.getmDate() == null ? PlaningTools.addOrSubMonth_Day(main.getvDate(), Frequency.YEAR, 5) : main.getmDate();
						fbz.setMaturity(mdate.replaceAll("-", "/"));
						fbz.setFrzSum(String.valueOf(ttTrdTrade.getSettleamount()));
						fbz.setOperTyp("02");
						fbz.setFrzcurrency(fbz.getCurrency());
						boolean f = sendReqFbz003(fbz);
						if(f){
							updateFbz(fbz);
							log.info("CreditServiceImpl额度：审批撤销 释放额度成功");
							JY.info("------------额度释放完成！---------");	
						}else{
							log.info("CreditServiceImpl额度：审批撤销 释放额度失败");
							JY.info("------------额度释放有误！---------");
						}
					}
				}					
				
			}
			
		}
		
	}
	
	/****
	 * 
	 * 01：占用；02：释放；03：冻结；04：解冻
	 * @param fbz
	 * @return
	 */
	public FBZ003 sendReqFbz003Flag(FBZ003 fbz){
		FBZ003 f=null;
		String esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ003.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg = parseXml(fbz);
			log.info("FBZ03请求报文："+msg);	
			
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("返回报文："+xmlDocAccept.asXML());		        	
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  f=(FBZ003) XmlUtilFactory.getBeanByXml(FBZ003.class, xmlDocAccept.asXML());
			    	  return f;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz003接口发生错误："+e,e);
			return null;
		}
		return null;
	}
	public boolean sendReqFbz003(FBZ003 fbz){
		String esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ003.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg = parseXml(fbz);
			log.info("FBZ03请求报文："+msg);	
			
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("返回报文："+xmlDocAccept.asXML());
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return true;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz003接口发生错误："+e,e);
			return false;
		}
		return false;
	}
	
	/****
	 * 发送信管系统 放款成功确认报文
	 * @param fbz
	 * @param type
	 * @return
	 */
	public boolean sendReqFbz006(FBZ003 fbz){
		String esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("FBZ006.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parse06Xml(fbz);
			log.info("FBZ06请求报文："+msg);
			
			HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("返回报文："+xmlDocAccept.asXML());
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return true;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz006接口发生错误："+e,e);
			return false;
		}
		return false;
	}
	
	private String parse06Xml(FBZ003 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",fbz.getBizDate()!=null?fbz.getBizDate():"");
			svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
			svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
			svc.put("tmStamp", getDate());
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
			body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
			body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
			body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
			body.put("serialNo", fbz.getSerialNo()!=null?fbz.getSerialNo():"");
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0006.xml", hashmap);
	}
	public String parseXml(FBZ003 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",fbz.getBizDate()!=null?fbz.getBizDate():"");
			svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
			svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
			svc.put("tmStamp", getDate());
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
			body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
			body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
			body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
			body.put("operTyp", fbz.getOperTyp()!=null?fbz.getOperTyp():"");
			body.put("frzcurrency", fbz.getFrzcurrency()!=null?fbz.getFrzcurrency():"");
			body.put("frzSum", fbz.getFrzSum() != null ? fbz.getFrzSum() : "");
			body.put("lmtID", fbz.getLmtID()!=null?fbz.getLmtID():"");
			//body.put("maturity", fbz.getMaturity()!=null?fbz.getMaturity():"");
			//01：占用；02：释放；03：冻结；04：解冻
			body.put("maturity", fbz.getMaturity()!=null?fbz.getMaturity():"");
			body.put("serialNo", fbz.getSerialNo()!=null?fbz.getSerialNo():"");
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(PathUtils.getWebRootPath()+"/WEB-INF/classes/schema/FBZC0003.xml", hashmap);
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	public boolean insertFbz(FBZ003 fbz) {
		int i=fbzMapper.insertFbz(fbz);
		fbzMapper.saveLog(fbz);
		return i>0?true:false;
	}
	public boolean updateFbz(FBZ003 fbz) {
		int i= fbzMapper.updateFbz(fbz);
		fbzMapper.saveLog(fbz);
		return i>0?true:false;
	}
	public FBZ003 getFbz(TtTrdTrade order, String type){
		FBZ003 fbz = fbzMapper.getFbzByOrder(order.getOrder_id());
		fbz.setOperTyp(type);
		return fbz;
	}
	public void saveLog(FBZ003 fbz){
		fbzMapper.saveLog(fbz);
	}
	@Override
	public void creditRelease(String dealNo,String approveStatus,String releaseAmt) throws Exception {
		FBZ003 fbz = fbzMapper.getFbzByOrder(dealNo);
		if(fbz != null){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(dealNo);
			double cash=Double.valueOf(fbz.getLimitBalance());
			if(Double.valueOf(releaseAmt)>cash){
				fbz.setFrzSum(String.valueOf(cash));
			}else{
				fbz.setFrzSum(releaseAmt);	
			}
			
			fbz.setOperTyp("02");
			fbz.setFrzcurrency(fbz.getCurrency());
			String mdate = main.getmDate() == null ? PlaningTools.addOrSubMonth_Day(main.getvDate(), Frequency.YEAR, 5) : main.getmDate();
			fbz.setMaturity(mdate.replaceAll("-", "/"));
			boolean f = sendReqFbz003(fbz);
			if(f){
				fbz.setLimitBalance(String.valueOf(new BigDecimal(fbz.getLimitBalance() == null ? "0" : fbz.getLimitBalance()).subtract(new BigDecimal(releaseAmt))));
				updateFbz(fbz);
				log.info("CreditServiceImpl额度：审批通过释放额度 成功");
				JY.info("------------额度释放完成！---------");	
			}else{
				log.info("CreditServiceImpl额度：审批通过释放额度 失败");
				JY.info("------------额度释放有误！---------");
			}			
		}	
	}
	@Override
	public boolean reback(String dealNo) {
		FBZ003 f=null;
		boolean flag=false;
		try {
			f=fbzMapper.getFbzByOrder(dealNo);
			if(f!=null&&"2".equals(f.getContentType())){
				TdProductApproveMain main = productApproveService.getProductApproveActivated(dealNo);
				f.setOperTyp("04");
				f.setFrzSum(String.valueOf(main.getAmt()));
				f.setFrzcurrency(f.getCurrency());
				flag = sendReqFbz003(f);
				if(flag){
					//fbzMapper.deleteFbz(f);
					f.setOperTyp("00");
					updateFbz(f);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public RetMsg<Serializable> checkUserCreditLimit(Map<String, Object> param) {
		RetMsg<Serializable> ret = RetMsgHelper.ok("999", "");
		//用户参数 p_type 1	 分组设置 p_sub_type 10
		param.put("p_type", "1");
		param.put("p_sub_type", "10");
		param.put("p_name", param.get("userId"));
		String group = null;
		//获取权限分组
		List<TaUserParam> params = userParamMapper.searchTaUserParam(param);
		if(params != null && params.size() > 0){
			group = params.get(0).getP_value();
		}else{
			group = "A";//默认A类分组
		}
		//获取分组对应的限额
		//用户参数 p_type 1	 分组设置 p_sub_type 9
		param.put("p_type", "1");
		param.put("p_sub_type", "9");
		param.put("p_name", group);
		//获取权限金额
		params = userParamMapper.searchTaUserParam(param);
		BigDecimal limitAmt = null;
		if(params != null && params.size() > 0){
			try {
				limitAmt = new BigDecimal(params.get(0).getP_value());
			} catch (Exception e) {
				limitAmt = new BigDecimal(0);
			}
			
		}else{
			limitAmt = new BigDecimal(0);
			
		}
		//审批权限校验
		BigDecimal amt = new BigDecimal(param.get("amt").toString());
		if(amt.compareTo(limitAmt) <= 0){
			ret.setDesc("999");
			ret.setDetail("成功");
		}else{
			ret.setDesc("100");
			ret.setDetail("校验失败");
		}
		return ret;
	}
}
