package com.singlee.capital.interfacex.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.SubjectInfo;

public interface TbkSubjectInfoMapper extends Mapper<SubjectInfo>{
		
	/**
	 * 条件查询所有  （分页）
	 * @param map
	 * @return
	 */
		public Page<SubjectInfo> selectAllSubjectInfoPaged(Map<String, Object> map,RowBounds rb);
}
