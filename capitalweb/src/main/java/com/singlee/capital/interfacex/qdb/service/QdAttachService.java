package com.singlee.capital.interfacex.qdb.service;

public interface QdAttachService {
	
	public String getAttachSerino()throws Exception;
	
	public String attachUpload(String serino, String path)throws Exception;
	
	public String attachDownload(String serino, String path)throws Exception;
	

}
