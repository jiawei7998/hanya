package com.singlee.capital.interfacex.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;


public class ReadAcupXmlFactory {
	
	
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,HashMap<Integer, AcupXmlBean> resultHashMap)
	{
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			AcupXmlBean acupXml = new AcupXmlBean();
			String no = StringUtils.trimToEmpty(element.attributeValue("No"));
			String fieldTag = StringUtils.trimToEmpty(element.attributeValue("FieldTag"));
			String position = StringUtils.trimToEmpty(element.attributeValue("Position"));
			String dataType = StringUtils.trimToEmpty(element.attributeValue("DataType"));
			String length = StringUtils.trimToEmpty(element.attributeValue("length"));
			String mod = StringUtils.trimToEmpty(element.attributeValue("M-O-D"));
			String leftOrRight = StringUtils.trimToEmpty(element.attributeValue("LeftOrRight"));
			String fillChar = element.attributeValue("FillChar");
			String value = StringUtils.trimToEmpty(element.attributeValue("value"));
			String description = StringUtils.trimToEmpty(element.attributeValue("Description"));
			String decimalsNo = StringUtils.trimToEmpty(element.attributeValue("DecimalsNo"));
			acupXml.setNo(Integer.parseInt(no));
			acupXml.setFieldTag(fieldTag);
			acupXml.setPosition(Integer.parseInt(position));
			acupXml.setDataType(dataType);
			acupXml.setLength(Integer.parseInt(length));
			acupXml.setMod(mod);
			acupXml.setLeftOrRight(leftOrRight);
			acupXml.setFillChar(fillChar);
			acupXml.setValue(value);
			acupXml.setDescription(description);
			acupXml.setDecimalsNo(Integer.parseInt(decimalsNo));
			resultHashMap.put(acupXml.getNo(), acupXml);
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				getElementList(element2,resultHashMap);
			}
		}
	}
}
