package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.TtMktCustNews;
import com.singlee.capital.interfacex.qdb.service.TtMktCustNewsService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TtMktCustNewsController")
public class TtMktCustNewsController extends CommonController{
	@Autowired
	private TtMktCustNewsService ttMktCustNewsService;
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	@ResponseBody
	@RequestMapping(value = "/getTtMktCustNewsList")
	public RetMsg<PageInfo<TtMktCustNews>> getTtMktCustNewsList(@RequestBody Map<String, Object> params) throws RException{
		Page<TtMktCustNews> param = ttMktCustNewsService.sreachTtMktCustNews(params);
		return RetMsgHelper.ok(param);
	}
	
	//上传Excel
	@ResponseBody
	@RequestMapping(value = "/uploadTtMktCustNewsExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadTtMktCustNewsExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		StringBuffer sb = new StringBuffer();
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		try {
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			for (UploadedFile uploadedFile : uploadedFileList) {
				String name = uploadedFile.getFullname();
				InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
				sb.append(ttMktCustNewsService.setStockPriceByExcel(name, is));
			}
		} catch (IOException e) {
			log.error("TtMktCustNewsController--uploadTtMktCustNewsExcel"+e);
			sb.append("舆情信息上传失败");
		}
		return StringUtils.isEmpty(sb.toString())?"舆情信息上传成功":sb.toString();
	}
}
