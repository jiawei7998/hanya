package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for LoanNoticeRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="LoanNoticeRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="LoanSummaryId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustBrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanNoticeRq", propOrder = { "commonRqHdr", "loanSummaryId",
		"custBrNo", "transDesc", "loanAmt", "currency", "loanType" ,"crmsLoanId","infoFlag"})
public class LoanNoticeRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "LoanSummaryId", required = true)
	protected String loanSummaryId;
	@XmlElement(name = "CustBrNo", required = true)
	protected String custBrNo;
	@XmlElement(name = "TransDesc", required = true)
	protected String transDesc;
	@XmlElement(name = "LoanAmt", required = true)
	protected String loanAmt;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "LoanType", required = true)
	protected String loanType;
	@XmlElement(name = "CrmsLoanId", required = true)
	protected String crmsLoanId;
	@XmlElement(name = "InfoFlag", required = true)
	protected String infoFlag;
	
	



	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	public String getCrmsLoanId() {
		return crmsLoanId;
	}

	public void setCrmsLoanId(String crmsLoanId) {
		this.crmsLoanId = crmsLoanId;
	}

	public String getInfoFlag() {
		return infoFlag;
	}

	public void setInfoFlag(String infoFlag) {
		this.infoFlag = infoFlag;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the loanSummaryId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanSummaryId() {
		return loanSummaryId;
	}

	/**
	 * Sets the value of the loanSummaryId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanSummaryId(String value) {
		this.loanSummaryId = value;
	}

	/**
	 * Gets the value of the custBrNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustBrNo() {
		return custBrNo;
	}

	/**
	 * Sets the value of the custBrNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustBrNo(String value) {
		this.custBrNo = value;
	}

	/**
	 * Gets the value of the transDesc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransDesc() {
		return transDesc;
	}

	/**
	 * Sets the value of the transDesc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransDesc(String value) {
		this.transDesc = value;
	}

	/**
	 * Gets the value of the loanAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAmt() {
		return loanAmt;
	}

	/**
	 * Sets the value of the loanAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAmt(String value) {
		this.loanAmt = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the loanType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanType() {
		return loanType;
	}

	/**
	 * Sets the value of the loanType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanType(String value) {
		this.loanType = value;
	}

}
