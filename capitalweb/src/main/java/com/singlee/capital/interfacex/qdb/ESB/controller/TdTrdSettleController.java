package com.singlee.capital.interfacex.qdb.ESB.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.CnapsTwoPaymentOpertor;
import com.singlee.capital.interfacex.qdb.ESB.util.CnapsType;
import com.singlee.capital.interfacex.qdb.pojo.TdTrdSettle;
import com.singlee.capital.interfacex.qdb.service.CnapsService;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/TdTrdSettleController", method = RequestMethod.POST)
public class TdTrdSettleController {

	@Resource
	private CnapsService cnapsService;
	
	@Resource
	private CnapsTwoPaymentOpertor cnapsTwoPaymentOpertor;
	
	@Autowired
	private ProductService productService;
	
	public static Logger log = LoggerFactory.getLogger("INTERFACEX");
	
	

	/**
	 * 查询自定义产品信息
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadCustomProduct")
	@ResponseBody
	public RetMsg<List<TcProduct>> loadCustomProduct(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		paramMap.put("is_active", DictConstants.YesNo.YES);
		List<TcProduct> retList = productService.getProductList(paramMap);
		RetMsg<List<TcProduct>> ret = RetMsgHelper.ok(retList);
	    return ret;
	}
	
	@RequestMapping(value = "/queryTdTrdSettleByDealNo")
	@ResponseBody
	public RetMsg<TdTrdSettle> queryTdTrdSettleByDealNo(@RequestBody HashMap<String, Object> paramMap) throws Exception 
	{
		TdTrdSettle tdTrdSettle = cnapsService.queryTdTrdSettleByDealNo(paramMap);
		return RetMsgHelper.ok(tdTrdSettle);
	}
	
	/**
	 * 查询待经办结算主指令信息
	 * 
	 * map内参数：
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchSubmitSettleInstList")
	@ResponseBody
	public RetMsg<PageInfo<TdTrdSettle>> searchSubmitSettleInstList(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String dealNo_s = ParameterUtil.getString(paramMap, "dealNo_s", "");
		String custNo_s = ParameterUtil.getString(paramMap, "custNo_s", "");
		String custName_s = ParameterUtil.getString(paramMap, "custName_s", "");
		String startVdate = ParameterUtil.getString(paramMap, "startVdate", "");
		String endVdate = ParameterUtil.getString(paramMap, "endVdate", "");
		String hvpType = ParameterUtil.getString(paramMap, "busType", "");
		
		int searchType = ParameterUtil.getInt(paramMap, "searchType", -1);
		
		String pageSize = ParameterUtil.getString(paramMap, "pageSize", null);
		String pageNumber = ParameterUtil.getString(paramMap, "pageNumber", null);
		
		String dealFlags = ParameterUtil.getString(paramMap, "dealFlags", "");
		if(StringUtil.isNullOrEmpty(dealFlags)){
			List<String> dealFlagList = new ArrayList<String>();
			dealFlagList.add(CnapsType.DealFlag.INIT.getType());
			dealFlagList.add(CnapsType.DealFlag.BACK.getType());
			map.put("dealFlags", dealFlagList);
			
		}else{
			map.put("dealFlags", Arrays.asList(dealFlags.split(",")));
			
		}
		
		String product_s = ParameterUtil.getString(paramMap, "product_s", "");
		if(!StringUtil.isNullOrEmpty(product_s) && !"[]".equals(product_s)){
			product_s = product_s.replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "");
			map.put("product_s", Arrays.asList(product_s.split(",")));
		}
		
		String voidFlags = ParameterUtil.getString(paramMap, "voidFlags", "");
		if(!StringUtil.isNullOrEmpty(voidFlags)){
			map.put("voidFlags", Arrays.asList(voidFlags.split(",")));
		}
		
		String settFlags = ParameterUtil.getString(paramMap, "settFlags", "");
		if(!StringUtil.isNullOrEmpty(settFlags)){
			map.put("settFlags", Arrays.asList(settFlags.split(",")));
		}
		
		map.put("dealNo", dealNo_s);
		map.put("startVdate", startVdate);
		map.put("endVdate", endVdate);
		map.put("custNo", custNo_s);
		map.put("custName", custName_s);
		map.put("hvpType", hvpType);
		map.put("searchType", searchType);
		map.put("pageSize", pageSize);
		map.put("pageNumber", pageNumber);
	    Page<TdTrdSettle> retList = cnapsService.getVerifySettleInstList(map);
	    return RetMsgHelper.ok(retList);
	}
	
	
	/**
	 * 查询待复核结算主指令信息
	 * 
	 * map内参数：
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchVerifySettleInstList")
	@ResponseBody
	public RetMsg<PageInfo<TdTrdSettle>> searchVerifySettleInstList(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String dealNo_s = ParameterUtil.getString(paramMap, "dealNo_s", "");
		String custNo_s = ParameterUtil.getString(paramMap, "custNo_s", "");
		String custName_s = ParameterUtil.getString(paramMap, "custName_s", "");
		String startVdate = ParameterUtil.getString(paramMap, "startVdate", "");
		String endVdate = ParameterUtil.getString(paramMap, "endVdate", "");
		String hvpType = ParameterUtil.getString(paramMap, "busType", "");
		
		int searchType = ParameterUtil.getInt(paramMap, "searchType", -1);
		String pageSize = ParameterUtil.getString(paramMap, "pageSize", null);
		String pageNumber = ParameterUtil.getString(paramMap, "pageNumber", null);
		
		String dealFlags = ParameterUtil.getString(paramMap, "dealFlags", "");
		if(StringUtil.isNullOrEmpty(dealFlags)){
			List<String> dealFlagList = new ArrayList<String>();
			//0-待经办 1-已经办待复核 A-正在复核中 C-复核异常 F-已复核 B-退回 
			if(searchType == 0) {
				dealFlagList.add("1");
				dealFlagList.add("C");
			} else if (searchType == 1) {
				dealFlagList.add("F");
			} else {
				dealFlagList.add(CnapsType.DealFlag.IOPER.getType());
				dealFlagList.add(CnapsType.DealFlag.VERIFY_APPEND.getType());
			}
			map.put("dealFlags", dealFlagList);
			
			
		}else{
			map.put("dealFlags", Arrays.asList(dealFlags.split(",")));
			
		}
		
		String product_s = ParameterUtil.getString(paramMap, "product_s", "");
		if(!StringUtil.isNullOrEmpty(product_s) && !"[]".equals(product_s)){
			product_s = product_s.replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "");
			map.put("product_s", Arrays.asList(product_s.split(",")));
		}
		
		String voidFlags = ParameterUtil.getString(paramMap, "voidFlags", "");
		if(!StringUtil.isNullOrEmpty(voidFlags)){
			map.put("voidFlags", Arrays.asList(voidFlags.split(",")));
		}
		
		String settFlags = ParameterUtil.getString(paramMap, "settFlags", "");
		if(!StringUtil.isNullOrEmpty(settFlags)){
			map.put("settFlags", Arrays.asList(settFlags.split(",")));
		}
		
		map.put("dealNo", dealNo_s);
		map.put("custNo", custNo_s);
		map.put("custName", custName_s);
		map.put("hvpType", hvpType);
		map.put("startVdate", startVdate);
		map.put("endVdate", endVdate);
		
		map.put("searchType", searchType);
		map.put("pageSize", pageSize);
		map.put("pageNumber", pageNumber);
	    Page<TdTrdSettle> retList = cnapsService.getVerifySettleInstList(map);
	    return RetMsgHelper.ok(retList);
	}
	
	@RequestMapping(value = "/submitSettle")
	@ResponseBody
	public RetMsg<Serializable> submitSettle(@RequestBody Map<String,Object> params) 
	{
		try {
			return cnapsService.submitSettle(params);
		} catch (Exception e) {
			log.error("验证大额支付信复核状态出错", e);
			return RetMsgHelper.simple("999999", "经办出错:"+e.getMessage());
		}
	}
	
	@RequestMapping(value = "/backSettle")
	@ResponseBody
	public RetMsg<Serializable> backSettle(@RequestBody Map<String,Object> params) 
	{
		try {
			return cnapsService.backSettle(params);
		} catch (Exception e) {
			log.error("验证大额支付信复核状态出错", e);
			return RetMsgHelper.simple("999999", "经办出错:"+e.getMessage());
		}
	}
	@RequestMapping(value = "/selectStatus")
	@ResponseBody
	public RetMsg<Serializable> selectStatus(@RequestBody Map<String,Object> params) {
		TdTrdSettle tdTrdSettle = null;
		HashMap<String, Object> checkTradeMap = null;
		try {
			tdTrdSettle = cnapsService.queryTdTrdSettleByDealNo(params);
			tdTrdSettle.setDealFlag(null);
			tdTrdSettle.setVoper(null);
			tdTrdSettle.setVtime(null);
			
			List<TdTrdSettle> tdTrdSettles = new ArrayList<TdTrdSettle>();
			TdTrdSettle tdSettle_2 = new TdTrdSettle();
			tdSettle_2.setUserDealNo(tdTrdSettle.getUserDealNo());
			tdSettle_2.setRecUserId(tdTrdSettle.getRecUserId());
			tdTrdSettles.add(tdSettle_2);
			
			checkTradeMap = cnapsTwoPaymentOpertor.cnapsQuerySettStatusOper(tdTrdSettles);
			//16700090004010  respCde ESB连接服务提供方失败
			if(checkTradeMap!=null){
				if("16700090004010".equals(checkTradeMap.get("respCde"))){
					tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
					tdTrdSettle.setRetMsg("提示代码："+checkTradeMap.get("respCde")+"<br>提示信息:连接ESB服务失败！");
					return updateTrdSettleRetMsg(tdTrdSettle);
					
				}else  if(checkTradeMap.containsKey("prcSts") == false)
				{
					tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
					tdTrdSettle.setRetMsg("提示代码："+checkTradeMap.get("respCde")+"<br>提示信息:查询结算状态返回报文有误,报文缺少prcSts值！");
					return updateTrdSettleRetMsg(tdTrdSettle);
					
				}else{
					tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
					tdTrdSettle.setRetMsg("提示代码："+checkTradeMap.get("respCde")+"<br>提示信息:查询结算状态成功");
					tdTrdSettle.setSettFlag(((TdTrdSettle)((List<?>)checkTradeMap.get("rec")).get(0)).getSettFlag());
					return updateTrdSettleRetMsg(tdTrdSettle);
				}
			}else{
				tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
				tdTrdSettle.setRetMsg("提示信息:大额支付状态查询出错,无法获取返回信息");
				return updateTrdSettleRetMsg(tdTrdSettle); 
			}
			
		} catch (Exception e) {
			log.error("查询大额支付信息状态错误", e);
			tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
			tdTrdSettle.setRetMsg("提示信息:大额支付状态查询出错," + e.getMessage());
			return updateTrdSettleRetMsg(tdTrdSettle); 
		}
		
	}
	@RequestMapping(value = "/verifySettle")
	@ResponseBody
	public RetMsg<Serializable> verifySettle(@RequestBody Map<String,Object> params) 
	{
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "复核成功");
		log.info("---------------------------------------------------------------大额支付开始");
		try {
			TdTrdSettle trdSettle = cnapsService.queryTdTrdSettleByDealNo(params);
			if(StringUtils.isBlank(trdSettle.getPayUserName()) || StringUtils.isBlank(trdSettle.getPayUserId()) ||
					StringUtils.isBlank(trdSettle.getPayBankName()) || StringUtils.isBlank(trdSettle.getPayBankId()) ||
					StringUtils.isBlank(trdSettle.getRecUserName()) || StringUtils.isBlank(trdSettle.getRecUserId()) ||
					StringUtils.isBlank(trdSettle.getRecBankName()) || StringUtils.isBlank(trdSettle.getRecBankId()))
			{
				ret.setCode("999999");
				ret.setDesc("您所需要确认的往账交易信息:<br>账号:"+trdSettle.getRecUserId()+"<br>户名:"+trdSettle.getRecUserName()
						+"<br>行号:"+trdSettle.getRecBankId()+"<br>行名:"+trdSettle.getRecBankName()
						+"<br><html><font color=red>金额:"+StringUtils.formatDouble(trdSettle.getAmount().doubleValue())
						+"</font></html><br>清算信息空缺！");
				return ret;
			}
		} catch (Exception e) {
			log.error("验证大额支付信复核状态出错", e);
			return RetMsgHelper.simple("000010", "复核出错:"+e.getMessage());
		}
		
		TdTrdSettle tdTrdSettle = null;
		HashMap<String, Object> param = null;
		try {
			tdTrdSettle = new TdTrdSettle(); 
			tdTrdSettle.setDealNo(params.get("dealNo").toString());
			tdTrdSettle.setDealFlag("A");
			tdTrdSettle.setVoidFlag("0");
			tdTrdSettle.setSettFlag("0");
			tdTrdSettle.setDealUser(SlSessionHelper.getUserId());
			tdTrdSettle.setDealTime(DateUtil.getCurrentDateTimeAsString());
			RetMsg<Serializable> retmsg = cnapsService.validSettleState(tdTrdSettle);
			if(RetMsgHelper.isOk(retmsg) == false)//验证失败
			{
				return retmsg;
			}
			
			tdTrdSettle = (TdTrdSettle)retmsg.getObj();
			tdTrdSettle.setVoper(SlSessionHelper.getUserId());
			tdTrdSettle.setVtime(DateUtil.getCurrentDateTimeAsString());
		} catch (Exception e) {
			log.error("验证大额支付复核状态出错", e);
			tdTrdSettle.setRetCode("000020");
			tdTrdSettle.setRetMsg("复核出错:"+e.getMessage());
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		
		try {
			/**
			 * 176050_系统状态查询_(404_404_())_V101_reqt.xml
			 * 
			 * 系统状态查询CNAPS007：各渠道系统主动查询大额系统、小额系统状态。
			 */
			HashMap<String, String> sysStatusHashMap = cnapsTwoPaymentOpertor.cnapsQuerySystemStatus();
			if(sysStatusHashMap.containsKey("status") && "neterror".equals(sysStatusHashMap.get("status")))
			{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("您所发送的往账交易信息:<br>账号:"+tdTrdSettle.getRecUserId()+"<br>户名:"+tdTrdSettle.getRecUserName()
						+"<br>行号:"+tdTrdSettle.getRecBankId()+"<br>行名:"+tdTrdSettle.getRecBankName()+"<br><html><font color=red>金额: "
						+StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())+"</font></html><br>由于网络原因未发送成功！");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
			
			if(!"_10".equals(sysStatusHashMap.get("sysSt")))
			{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("<html>大额清算中心处于：<font color=red>["+CnapsType.SysStatus.getName(sysStatusHashMap.get("sysSt"))+"]</font>不允许汇款！</html>");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
		} catch (Exception e) {
			log.error("查询大额系统状态错误", e);
			tdTrdSettle.setRetCode("000020");
			tdTrdSettle.setRetMsg("复核出错:"+e.getMessage());
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		
		try {
			/***
			 * 176076_参与者运行状态查询_(430_430_())_V101_reqt
			 */
			HashMap<String, String> acceptBankHashMap = cnapsTwoPaymentOpertor.cnapsQueryAcceptBankStatus(tdTrdSettle.getRecBankId());
			if((!"".equals(acceptBankHashMap.get("instgPtySts"))) && (!"ST02".equals(acceptBankHashMap.get("instgPtySts")))){
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("<html>大额接收行状态处于<font color=red>[非正常状态]</font>！</html>");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
		} catch (Exception e) {
			log.error("查询接收行状态错误", e);
			tdTrdSettle.setRetCode("000040");
			tdTrdSettle.setRetMsg("查询接收行状态错误:"+e.getMessage());
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		
		HashMap<String, Object> checkTradeMap = null;
		try {
			List<TdTrdSettle> tdTrdSettles = new ArrayList<TdTrdSettle>();
			TdTrdSettle tdSettle_2 = new TdTrdSettle();
			tdSettle_2.setUserDealNo(tdTrdSettle.getUserDealNo());
			tdSettle_2.setRecUserId(tdTrdSettle.getRecUserId());
			tdTrdSettles.add(tdSettle_2);
			
			checkTradeMap = cnapsTwoPaymentOpertor.cnapsQuerySettStatusOper(tdTrdSettles);
			//16700090004010  respCde ESB连接服务提供方失败
			if("16700090004010".equals(checkTradeMap.get("respCde"))){
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("提示代码："+checkTradeMap.get("respCde")+"<br>提示信息:连接ESB服务失败！");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
			
			if(checkTradeMap.containsKey("prcSts") == false)
			{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("提示代码："+checkTradeMap.get("respCde")+"<br>提示信息:查询结算状态返回报文有误,报文缺少prcSts值！");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
			
				
		} catch (Exception e) {
			log.error("查询大额支付信息状态错误", e);
			
			tdTrdSettle.setRetCode("000050");
			tdTrdSettle.setRetMsg("查询大额支付信息状态错误:"+e.getMessage());
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
				
		try{
			/* 空    无记录
			 * 01 失败   prcSts
			 * 00 成功 存在记录 prcSts
			 * 02处理中
			 * 16700090004010  respCde ESB连接服务提供方失败
			 */
			if("".equals(checkTradeMap.get("prcSts"))|| "01".equals(checkTradeMap.get("prcSts")))
			{
				String quoteStartFlag = params.get("quota_flag").toString();
				String verifyFlag = "";
				if("0".equals(quoteStartFlag))//发头寸
				{
					//发头寸，做预报核实300202
					HashMap<String, String> cashHashMap = cnapsTwoPaymentOpertor.cnapsSendCash(tdTrdSettle);
					if("16700090004010".equals(cashHashMap.get("respCde")))
					{
						tdTrdSettle.setRetCode("999999");
						tdTrdSettle.setRetMsg("提示代码："+cashHashMap.get("respCde")+"\r\n提示信息："+"连接ESB服务失败！如果想继续签发，请您关闭头寸开关，重新发送！");
						tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
						return updateTrdSettleRetMsg(tdTrdSettle);
					}
					if(cashHashMap.containsKey("status"))
					{	
						if("neterror".equals(cashHashMap.get("status")))
						{
							tdTrdSettle.setRetCode("999999");
							tdTrdSettle.setRetMsg("您所发送的往账交易信息由于网络原因未发送成功！");
							tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());						
							return updateTrdSettleRetMsg(tdTrdSettle);
						}
					}
					if("Y".equals(cashHashMap.get("verStatus")) || "S".equals(cashHashMap.get("verStatus"))){
						//头寸异常或通讯超时，提示用户关闭头寸开关进行重发，此时就不发头寸了
						//先判断开关是否开启
						//1、头寸返回：通过
						//2、头寸返回：通过待授权
						//异常处理机制：
						verifyFlag = cashHashMap.get("verStatus");
						
					} else {//头寸验证失败,
						tdTrdSettle.setRetCode("999999");
						tdTrdSettle.setRetMsg("提示代码："+cashHashMap.get("verStatus")+"<br>提示信息:"+cashHashMap.get("verMsg")+",如果想继续签发,请您先增加头寸预报,重新发送！");
						tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());		
						
						return updateTrdSettleRetMsg(tdTrdSettle);
						
					}
				}
				return cnapsSendPaymentDealOper(tdTrdSettle,verifyFlag);
			
			}else if("00".equals(checkTradeMap.get("prcSts")) || "02".equals(checkTradeMap.get("prcSts")))//00 成功 存在记录 prcSts 02处理中
			{	
				//解析报文获取相关域值
				tdTrdSettle.setDealFlag("F");
				tdTrdSettle.setVoidFlag("0");
				tdTrdSettle.setSettFlag("1");
				
				param = new HashMap<String, Object>();
				param.put("dealNo", tdTrdSettle.getDealNo());
				param.put("dealFlag", tdTrdSettle.getDealFlag());
				param.put("voidFlag", tdTrdSettle.getVoidFlag());
				param.put("settFlag", tdTrdSettle.getSettFlag());
				param.put("voper", tdTrdSettle.getVoper());
				param.put("vtime", tdTrdSettle.getVtime());
				
				if(cnapsService.updateTdTrdSettleState(param) > 0)
				{
					tdTrdSettle.setRetCode(RetMsgHelper.codeOk);
					tdTrdSettle.setRetMsg("您所确认签发的往账交易信息:<br>账号:"+tdTrdSettle.getRecUserId()+"<br>户名:"+tdTrdSettle.getRecUserName()
							+"<br>行号:"+tdTrdSettle.getRecBankId()+"<br>行名:"+tdTrdSettle.getRecBankName()+"<br><html><font color=red>金额:"
							+StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())
							+"</font></html><br>签发处理成功，请稍后查询业务状态！");
					tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY.getType());
					
					return updateTrdSettleRetMsg(tdTrdSettle);
				}
			}else{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("前置返回状态异常,请与管理员确认！");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());						
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
			
		} catch (Exception e) {
			log.error("发送大额支付报文错误", e);
			tdTrdSettle.setRetCode("000070");
			tdTrdSettle.setRetMsg("发送大额支付报文错误:"+e.getMessage());
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		return ret;
	}
	
	private RetMsg<Serializable> cnapsSendPaymentDealOper(TdTrdSettle tdTrdSettle, String verifyFlag) throws Exception{
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "复核成功");
		HashMap<String, String> retHashMap = cnapsTwoPaymentOpertor.cnapsSendPaymentDealOper(tdTrdSettle,verifyFlag);
		if("16700090004010".equals(retHashMap.get("respCde")))
		{
			tdTrdSettle.setRetCode("999999");
			tdTrdSettle.setRetMsg("提示代码："+retHashMap.get("respCde")+"<br>提示信息:连接ESB服务失败！");
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		/*网络问题发送失败！*/
		if(retHashMap.containsKey("status"))
		{
			if("neterror".equals(retHashMap.get("status")))
			{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("您所发送的往账交易信息:<br>账号:" + tdTrdSettle.getRecUserId()+"<br>户名:" + tdTrdSettle.getRecUserName()
						+ "<br>行号:" + tdTrdSettle.getRecBankId() + "<br>行名:"+tdTrdSettle.getRecBankName() + "<br><html><font color=red>金额: "
						+ StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())+"</font></html><br>由于网络原因未发送成功！");
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());

				return updateTrdSettleRetMsg(tdTrdSettle);
			}
		}
		/*判断二代前置返回汇兑报文中包含记录已存在情况（RM0002），则将交易状态置为已处理*/
        if(retHashMap.get("respMsg").indexOf("RM0002")!=-1){
        	tdTrdSettle.setDealFlag("F");
			tdTrdSettle.setVoidFlag("0");
			tdTrdSettle.setSettFlag("1");
			return updateTrdSettleRetMsg(tdTrdSettle);
        }
		/*SEND CNAPS2 SUCCESS*/
		if("00".equals(retHashMap.get("prcSts")) || "02".equals(retHashMap.get("prcSts")))  //00 成功 存在记录 prcSts 02处理中
		{
			if("26400000000000".equals(retHashMap.get("respCde")))
			{
				tdTrdSettle.setDealFlag("F");
				tdTrdSettle.setVoidFlag("0");
				tdTrdSettle.setSettFlag("1");
				tdTrdSettle.setVoper(SlSessionHelper.getUserId());
				tdTrdSettle.setVtime(DateUtil.getCurrentDateTimeAsString());

				HashMap<String, Object> param = new HashMap<String, Object>();
				param.put("dealNo", tdTrdSettle.getDealNo());
				param.put("dealFlag", tdTrdSettle.getDealFlag());
				param.put("voidFlag", tdTrdSettle.getVoidFlag());
				param.put("settFlag", tdTrdSettle.getSettFlag());
				param.put("voper", tdTrdSettle.getVoper());
				param.put("vtime", tdTrdSettle.getVtime());
				
				if(cnapsService.updateTdTrdSettleState(param) > 0)
				{
					tdTrdSettle.setRetCode(RetMsgHelper.codeOk);//error.common.0000
					tdTrdSettle.setRetMsg("您所确认签发的往账交易信息:<br>账号:"+tdTrdSettle.getRecUserId()+"<br>户名:"+tdTrdSettle.getRecUserName()
							+"<br>行号:"+tdTrdSettle.getRecBankId()+"<br>行名:"+tdTrdSettle.getRecBankName()+"<br><html><font color=red>金额:"
							+StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())
							+"</font></html><br>签发处理成功，请稍后查询业务状态！");
					tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY.getType());
					return updateTrdSettleRetMsg(tdTrdSettle);
					
				}
				
			}else{
				tdTrdSettle.setRetCode("999999");
				tdTrdSettle.setRetMsg("您所确认签发的往账交易信息:<br>账号:"+tdTrdSettle.getRecUserId()+"<br>户名:"+tdTrdSettle.getRecUserName()
						+"<br>行号:"+tdTrdSettle.getRecBankId()+"<br>行名:"+tdTrdSettle.getRecBankName()+"<br><html><font color=red>金额:"
						+StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())+"</font></html><br>大额签发报文信息:" 
						+"<br>错误代码:"+retHashMap.get("errCd")+"<br>错误信息:"+retHashMap.get("errDesc")
						+"<br>提示代码:"+retHashMap.get("respCde")+"<br>提示信息:"+retHashMap.get("respMsg"));
				tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
				
				return updateTrdSettleRetMsg(tdTrdSettle);
			}
			
		} else { // 发送失败
			tdTrdSettle.setRetCode("999999");
			tdTrdSettle.setRetMsg("您所确认签发的往账交易信息:<br>账号:"+tdTrdSettle.getRecUserId()+"<br>户名:"+tdTrdSettle.getRecUserName()
					+"<br>行号:"+tdTrdSettle.getRecBankId()+"<br>行名:"+tdTrdSettle.getRecBankName()+"<br><html><font color=red>金额:"
					+StringUtils.formatDouble(tdTrdSettle.getAmount().abs().doubleValue())+"</font></html><br>大额签发报文信息:" 
					+"<br>错误代码:"+retHashMap.get("errCd")+"<br>错误信息:"+retHashMap.get("errDesc")
					+"<br>提示代码:"+retHashMap.get("respCde")+"<br>提示信息:"+retHashMap.get("respMsg"));
			tdTrdSettle.setDealFlag(CnapsType.DealFlag.VERIFY_ERR.getType());
			
			return updateTrdSettleRetMsg(tdTrdSettle);
		}
		return ret;
	}

	private RetMsg<Serializable> updateTrdSettleRetMsg(TdTrdSettle tdTrdSettle)
	{
		RetMsg<Serializable> ret = new RetMsg<Serializable>(tdTrdSettle.getRetCode(), tdTrdSettle.getRetMsg());
		try {
			HashMap<String, Object> param = new HashMap<String, Object>();
			param.put("dealNo", tdTrdSettle.getDealNo());
			param.put("retCode", tdTrdSettle.getRetCode());
			param.put("retMsg", tdTrdSettle.getRetMsg());
			param.put("dealFlag", tdTrdSettle.getDealFlag());
			param.put("voper", tdTrdSettle.getVoper());
			param.put("settFlag", tdTrdSettle.getSettFlag());
			param.put("vtime", tdTrdSettle.getVtime());
			cnapsService.updateTdTrdRetMsg(param);
		} catch (Exception e) {
			log.error("更新交易号为:"+tdTrdSettle.getDealNo()+"的大额支付信息出错:", e);
			ret.setCode("999999");
			ret.setDesc("更新交易号为:"+tdTrdSettle.getDealNo()+"的大额支付信息出错:"+e.getMessage());
		}
		return ret;
	}
	
	
	/**
	 * 查询待复核数量
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchVerifySettlesCount")
	@ResponseBody
	public RetMsg<Integer> searchVerifySettlesCount(@RequestBody HashMap<String, Object> paramMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> dealFlagList = new ArrayList<String>();
		//0-待经办 1-已经办待复核 A-正在复核中 C-复核异常 F-已复核 B-退回 
		dealFlagList.add("1");
		dealFlagList.add("C");
		map.put("dealFlags", dealFlagList);
	    return RetMsgHelper.ok(cnapsService.searchVerifySettlesCount(map));
	}
	
}
