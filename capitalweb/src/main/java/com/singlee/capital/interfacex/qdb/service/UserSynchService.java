package com.singlee.capital.interfacex.qdb.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.system.model.TaUser;

public interface UserSynchService {

	void deleteUserESB();

	void insertUsers(List<TaUser> users);
	void insertUserIdinstID(List<TaUser> users);

	Map<String, String> changeTemp(Map<String, String> map);

}
