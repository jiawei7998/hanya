package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="BOS_O_CUSTOMERMANAGER")
public class EcifBosoCustomerManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customer_manager_id  ;
	private String ecif_id              ;
	private String cust_manager         ;
	private String cust_manager_name    ;
	private String manager_perf_ratio   ;
	private String source_sys_tp_cd     ;
	private String last_update_dt       ;
	private String last_update_tx_id    ;
	private String last_update_user     ;
	private String cm_reserved_1        ;
	private String cm_reserved_2        ;
	private String cm_reserved_3        ;
	private String cm_reserved_4        ;
	private String cm_reserved_5        ;
	private String effective_flag       ;
	public String getCustomer_manager_id() {
		return customer_manager_id;
	}
	public void setCustomer_manager_id(String customerManagerId) {
		customer_manager_id = customerManagerId;
	}
	public String getEcif_id() {
		return ecif_id;
	}
	public void setEcif_id(String ecifId) {
		ecif_id = ecifId;
	}
	public String getCust_manager() {
		return cust_manager;
	}
	public void setCust_manager(String custManager) {
		cust_manager = custManager;
	}
	public String getCust_manager_name() {
		return cust_manager_name;
	}
	public void setCust_manager_name(String custManagerName) {
		cust_manager_name = custManagerName;
	}
	public String getManager_perf_ratio() {
		return manager_perf_ratio;
	}
	public void setManager_perf_ratio(String managerPerfRatio) {
		manager_perf_ratio = managerPerfRatio;
	}
	public String getSource_sys_tp_cd() {
		return source_sys_tp_cd;
	}
	public void setSource_sys_tp_cd(String sourceSysTpCd) {
		source_sys_tp_cd = sourceSysTpCd;
	}
	public String getLast_update_dt() {
		return last_update_dt;
	}
	public void setLast_update_dt(String lastUpdateDt) {
		last_update_dt = lastUpdateDt;
	}
	public String getLast_update_tx_id() {
		return last_update_tx_id;
	}
	public void setLast_update_tx_id(String lastUpdateTxId) {
		last_update_tx_id = lastUpdateTxId;
	}
	public String getLast_update_user() {
		return last_update_user;
	}
	public void setLast_update_user(String lastUpdateUser) {
		last_update_user = lastUpdateUser;
	}
	public String getCm_reserved_1() {
		return cm_reserved_1;
	}
	public void setCm_reserved_1(String cmReserved_1) {
		cm_reserved_1 = cmReserved_1;
	}
	public String getCm_reserved_2() {
		return cm_reserved_2;
	}
	public void setCm_reserved_2(String cmReserved_2) {
		cm_reserved_2 = cmReserved_2;
	}
	public String getCm_reserved_3() {
		return cm_reserved_3;
	}
	public void setCm_reserved_3(String cmReserved_3) {
		cm_reserved_3 = cmReserved_3;
	}
	public String getCm_reserved_4() {
		return cm_reserved_4;
	}
	public void setCm_reserved_4(String cmReserved_4) {
		cm_reserved_4 = cmReserved_4;
	}
	public String getCm_reserved_5() {
		return cm_reserved_5;
	}
	public void setCm_reserved_5(String cmReserved_5) {
		cm_reserved_5 = cmReserved_5;
	}
	public String getEffective_flag() {
		return effective_flag;
	}
	public void setEffective_flag(String effectiveFlag) {
		effective_flag = effectiveFlag;
	}

}
