package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DebitCustRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DebitCustRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctNoDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustIdDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCustRec", propOrder = { "acctNoDr", "custIdDr", "amtDr" })
public class DebitCustRec {

	@XmlElement(name = "AcctNoDr", required = true)
	protected String acctNoDr;
	@XmlElement(name = "CustIdDr", required = true)
	protected String custIdDr;
	@XmlElement(name = "AmtDr", required = true)
	protected String amtDr;

	/**
	 * Gets the value of the acctNoDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoDr() {
		return acctNoDr;
	}

	/**
	 * Sets the value of the acctNoDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoDr(String value) {
		this.acctNoDr = value;
	}

	/**
	 * Gets the value of the custIdDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustIdDr() {
		return custIdDr;
	}

	/**
	 * Sets the value of the custIdDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustIdDr(String value) {
		this.custIdDr = value;
	}

	/**
	 * Gets the value of the amtDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtDr() {
		return amtDr;
	}

	/**
	 * Sets the value of the amtDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtDr(String value) {
		this.amtDr = value;
	}

}
