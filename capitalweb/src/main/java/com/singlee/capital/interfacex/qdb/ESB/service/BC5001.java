package com.singlee.capital.interfacex.qdb.ESB.service;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.ESB.model.BC5001ReqtBean;
import com.singlee.capital.interfacex.qdb.ESB.util.BC5001ReplaceElementContext;
import com.singlee.capital.interfacex.qdb.ESB.util.ConvertUtil;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.system.model.TaUser;



@Component
public class BC5001{
	
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	private String esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("BC5001");
	
	//存放每页访问id
	HashMap<String, Object> bc5001Map  = new HashMap<String, Object>();
	
	public HashMap<String, Object> sendTR1003ReqtMsgTOTis(BC5001ReqtBean dldtReqtBean,TaUser user,String pageNum) {
		List<BC5001ReqtBean> applays = new ArrayList<BC5001ReqtBean>();
		try {
				//打包请求数据成报文格式
				if("Y".equals(dldtReqtBean.getMoreRecInd()) && pageNum != null){
					dldtReqtBean.setSearchKeys(String.valueOf(bc5001Map.get("searchKeys" + pageNum)));
				}
				String reqtMsg = packageTr1003ReqtMsgSendToTis(dldtReqtBean,user);
				log.info("发送客户查询(BC5001)请求报文.........\n" + reqtMsg +"\n");
				System.out.print("发送客户查询(BC5001)请求报文.........\n" + reqtMsg +"\n");
				//发送报文，返回访问状态和报文数据
				HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, reqtMsg);
				if(hashMap.size()==0 || hashMap == null){
					log.info("请求报文时发生网络异常！请检查网络！");
		        }else{
		        	//判断访问是否正常
			        if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
			        	
			        	log.info("接收客户查询(bc5001)返回报文.");
			        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				        HashMap<String, Object> appBody = new HashMap<String, Object>();//appBody
				        HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				        //把报文信息封装到Map<svcHdr>,Map<appBody>,Map<AppHdr>中
				        			//把Map<apopBody>中的数据解析成BC5001ReqtBean类，存放到List中
				        applays = BC5001ReplaceElementContext.praseEsbXmlPackageDescription((byte[]) hashMap.get("Bytes"), svcHdr, appHdr, appBody);
				        
				        //查询异常时
				        if(!"交易处理成功".equals(svcHdr.get("respMsg").toString().trim())) {
				        	bc5001Map.put("code", "error.system.0002");
			        		bc5001Map.put("respMsg", svcHdr.get("respMsg"));
			        		bc5001Map.put("List", new ArrayList<BC5001ReqtBean>());
			        		return bc5001Map;
				        }
				        
				        for(BC5001ReqtBean bc5001Reqt:applays){
				        	//给给个BC5001ReqtBean类设置“searchKeys”值
				        	bc5001Reqt.setSearchKeys((String)appHdr.get("searchKeys"));
				        }
				        //判断若无记录，去除第一条数据（核心返回无数据会存一条数据）
			        	if("AB".equals(appHdr.get("respCde"))){
			        		bc5001Map.put("code", "error");
			        		bc5001Map.put("respCde", appHdr.get("respCde"));
			        		bc5001Map.put("respMsg", svcHdr.get("respMsg"));
			        		bc5001Map.put("List", new ArrayList<BC5001ReqtBean>());
			        	}else{
//			        		bc5001Map.put("moreRecInd", appHdr.get("moreRecInd"));
					        bc5001Map.put("List", applays);
					        //存放下一页的访问id
					        bc5001Map.put("searchKeys" + (Integer.parseInt(pageNum) + 1), appHdr.get("searchKeys"));
					        bc5001Map.put("code", "success");
			        	}
			        }
		        }
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return bc5001Map;
	}
	
	/**
	 * 生成查询的报文
	 * @param dldtReqtBean 查询的过滤条件
	 * @param user 执行查询的柜员
	 * @return
	 */
	public String packageTr1003ReqtMsgSendToTis(BC5001ReqtBean dldtReqtBean,TaUser user){
		org.w3c.dom.Document doc = null;  
		Element root = null;
		StringWriter xmlOut = null;
		String xmlResp = null;
		try{
			//得到DOM解析器的工厂实例
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
			//然后从DOM工厂获得DOM解析器
			DocumentBuilder builder = factory.newDocumentBuilder();  
			doc = builder.newDocument();
			root = doc.createElement("reqt");  
			doc.appendChild(root);
			
			Element svcHdr = doc.createElement("svcHdr");
			Element corrId = doc.createElement("corrId");
			corrId.setTextContent("");
			Element svcId = doc.createElement("svcId");
			svcId.setTextContent("151232");
			Element verNbr = doc.createElement("verNbr");
			verNbr.setTextContent("105");
			Element csmrId = doc.createElement("csmrId");
			csmrId.setTextContent("284000");
			Element csmrSerNbr = doc.createElement("csmrSerNbr");
			csmrSerNbr.setTextContent("100010001000");
			Element tmStamp = doc.createElement("tmStamp");
			tmStamp.setTextContent(ConvertUtil.getTime(new Date()));
			Element reqtIp = doc.createElement("reqtIp");
			reqtIp.setTextContent("10.1.80.185");
			svcHdr.appendChild(corrId);
			svcHdr.appendChild(svcId);
			svcHdr.appendChild(verNbr);
			svcHdr.appendChild(csmrId);
			svcHdr.appendChild(csmrSerNbr);
			svcHdr.appendChild(tmStamp);
			svcHdr.appendChild(reqtIp);
			
			Element appHdr = doc.createElement("appHdr");  
			Element transSrc = doc.createElement("transSrc");
			transSrc.setTextContent("FIB");
			Element destId = doc.createElement("destId");
			destId.setTextContent("BWC");
			Element termId = doc.createElement("termId");
			termId.setTextContent("WIN-4424AG");
			Element bnkNbr = doc.createElement("bnkNbr");
			bnkNbr.setTextContent("1");
			Element brNbr = doc.createElement("brNbr");
			brNbr.setTextContent("80201");
			Element userId = doc.createElement("userId");
			userId.setTextContent("BR80201143");
			Element locRevuSupvId = doc.createElement("locRevuSupvId");
			locRevuSupvId.setTextContent("");
			Element locTransSupvId = doc.createElement("locTransSupvId");
			locTransSupvId.setTextContent("");
			Element hostSupvId = doc.createElement("hostSupvId");
			hostSupvId.setTextContent("");
			Element transCde = doc.createElement("transCde");
			transCde.setTextContent("1000");
			Element actCde = doc.createElement("actCde");
			actCde.setTextContent("I");
			Element transMode = doc.createElement("transMode");
			transMode.setTextContent("R");
			Element compntRefNbr = doc.createElement("compntRefNbr");
			compntRefNbr.setTextContent("745138407");
			Element srvrReconcNbr = doc.createElement("srvrReconcNbr");
			srvrReconcNbr.setTextContent("");
			Element nbrOfRecToRetrv = doc.createElement("nbrOfRecToRetrv");
			nbrOfRecToRetrv.setTextContent("10");
			Element moreRecInd = doc.createElement("moreRecInd");
			moreRecInd.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getMoreRecInd()));
			Element searchMeth = doc.createElement("searchMeth");
			searchMeth.setTextContent("F");
			Element searchKeys = doc.createElement("searchKeys");
			searchKeys.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getSearchKeys()));
			appHdr.appendChild(searchKeys);
			Element sqlInfo = doc.createElement("sqlInfo");
			sqlInfo.setTextContent("");
			Element dtInFromClt = doc.createElement("dtInFromClt");
			dtInFromClt.setTextContent("20140116");
			Element tmInFromClt = doc.createElement("tmInFromClt");
			tmInFromClt.setTextContent("174700");
			Element transType = doc.createElement("transType");
			transType.setTextContent("");
			Element upsSysCde = doc.createElement("upsSysCde");
			upsSysCde.setTextContent("");
			Element recvAtdSrcId = doc.createElement("recvAtdSrcId");
			recvAtdSrcId.setTextContent("");
			Element recvAtdUserId = doc.createElement("recvAtdUserId");
			recvAtdUserId.setTextContent("");
			Element recvAtdJrnlSeqNbr = doc.createElement("recvAtdJrnlSeqNbr");
			recvAtdJrnlSeqNbr.setTextContent("");
			Element procSrcId = doc.createElement("procSrcId");
			procSrcId.setTextContent("");
			Element procUserId = doc.createElement("procUserId");
			procUserId.setTextContent("");
			Element procJrnlSeqNbr = doc.createElement("procJrnlSeqNbr");
			procJrnlSeqNbr.setTextContent("");
			Element pkgCde = doc.createElement("pkgCde");
			pkgCde.setTextContent("");
			Element promoCde = doc.createElement("promoCde");
			promoCde.setTextContent("");
			Element varFmt = doc.createElement("varFmt");
			varFmt.setTextContent("");
			appHdr.appendChild(transSrc);
			appHdr.appendChild(destId);
			appHdr.appendChild(termId);
			appHdr.appendChild(bnkNbr);
			appHdr.appendChild(brNbr);
			appHdr.appendChild(userId);
			appHdr.appendChild(locRevuSupvId);
			appHdr.appendChild(locTransSupvId);
			appHdr.appendChild(hostSupvId);
			appHdr.appendChild(transCde);
			appHdr.appendChild(actCde);
			appHdr.appendChild(transMode);
			appHdr.appendChild(compntRefNbr);
			appHdr.appendChild(srvrReconcNbr);
			appHdr.appendChild(nbrOfRecToRetrv);
			appHdr.appendChild(moreRecInd);
			appHdr.appendChild(searchMeth);
			appHdr.appendChild(searchKeys);
			appHdr.appendChild(sqlInfo);
			appHdr.appendChild(dtInFromClt);
			appHdr.appendChild(tmInFromClt);
			appHdr.appendChild(transType);
			appHdr.appendChild(upsSysCde);
			appHdr.appendChild(recvAtdSrcId);
			appHdr.appendChild(recvAtdUserId);
			appHdr.appendChild(recvAtdJrnlSeqNbr);
			appHdr.appendChild(procSrcId);
			appHdr.appendChild(procUserId);
			appHdr.appendChild(procJrnlSeqNbr);
			appHdr.appendChild(pkgCde);
			appHdr.appendChild(promoCde);
			appHdr.appendChild(varFmt);
			
			Element appBody = doc.createElement("appBody");
			Element cifNbr = doc.createElement("cifNo");
			cifNbr.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getCifNo()));
			appBody.appendChild(cifNbr);
			Element idNbr = doc.createElement("idNbr");
			idNbr.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(idNbr);
			Element idTypeCde = doc.createElement("idType");
			idTypeCde.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getIdTypeCde()));
			appBody.appendChild(idTypeCde);
			Element idIssCntryCde = doc.createElement("idOwnerCntry");
			idIssCntryCde.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getIdIssCntryCde()));
			appBody.appendChild(idIssCntryCde);
			Element empNbr = doc.createElement("bnkInterCde");
			empNbr.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(empNbr);
			Element empName = doc.createElement("custName");
			empName.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getEmpName()));
			appBody.appendChild(empName);
			Element sel = doc.createElement("searchMeth");
			sel.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getSel()));
			appBody.appendChild(sel);
			Element selOpt = doc.createElement("searchOpt");
			selOpt.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getSelOpt()));
			appBody.appendChild(selOpt);
			Element taxationId = doc.createElement("taxId");
			taxationId.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getTaxId()));
			appBody.appendChild(taxationId);
			Element idPrfx = doc.createElement("idPrfx");
			idPrfx.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getIdPrfx()));
			appBody.appendChild(idPrfx);
			
			root.appendChild(svcHdr);
			root.appendChild(appHdr);
			root.appendChild(appBody);
			
			DOMSource source = new DOMSource(doc);  
			TransformerFactory tf = TransformerFactory.newInstance();  
			Transformer transformer = tf.newTransformer();  
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");//设置文档的换行与缩进  
			 
            xmlOut=new StringWriter();
            StreamResult result=new StreamResult(xmlOut);
            transformer.transform(source,result);
            xmlResp = xmlOut.toString(); 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			if(null != xmlOut){
				try {
					xmlOut.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return xmlResp;
		
	}
	
	public String formatBigDecimal4(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.0000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	public String formatBigDecimal8(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.00000000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	

	@SuppressWarnings("unchecked")
	public static void getElementAppBody(org.dom4j.Element element,BC5001ReqtBean dldt) throws Exception
	{
//		logger.info("TR1001RepoReqtBean = "+BeanUtils.describe(dldt));
		List<org.dom4j.Element> elements = element.elements();
		if(elements.size() ==0)
		{
			String key = StringUtils.trimToEmpty(element.getName()).substring(0, 1)+StringUtils.trimToEmpty(element.getName()).substring(1);
			System.out.println(key);
			Method[] methods = dldt.getClass().getMethods();
			for(Method method : methods){
 				String methodName = method.getName();
 				if(methodName.startsWith("get")){
 					String popKey = StringUtils.trimToEmpty(methodName).substring(3, methodName.length());
 					String pop = popKey.substring(0, 1).toLowerCase()+popKey.substring(1);
 					if(key.equalsIgnoreCase(popKey)){
 						//判断popKey属性第2个字母是否大写
 						if(Character.isUpperCase(popKey.charAt(1))){
 							element.setText(BeanUtils.getProperty(dldt, popKey));
 						}else {
 							element.setText(BeanUtils.getProperty(dldt, pop));
 						}
 					}
 				}
 			}
		}
		else {
			for(Iterator<org.dom4j.Element> iterator = elements.iterator();iterator.hasNext();)
			{
				org.dom4j.Element element2 = (org.dom4j.Element)iterator.next();
				getElementAppBody(element2,dldt);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void getElementAppHdr(org.dom4j.Element element) throws Exception
	{
		List<org.dom4j.Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if("reqDt".equalsIgnoreCase(element.getName())){
				element.setText(ConvertUtil.getDataString(new Date()));
			}
			if("reqTm".equalsIgnoreCase(element.getName())){
				element.setText(ConvertUtil.getTimeString(new Date()));
			}
		}
		else {
			for(Iterator<org.dom4j.Element> iterator = elements.iterator();iterator.hasNext();)
			{
				org.dom4j.Element element2 = (org.dom4j.Element)iterator.next();
				getElementAppHdr(element2);
			}
		}
	}
	

	/**
	 * @return the esbUrl
	 */
	public String getEsbUrl() {
		return esbUrl;
	}

	/**
	 * @param esbUrl the esbUrl to set
	 */
	public void setEsbUrl(String esbUrl) {
		this.esbUrl = esbUrl;
	}
	
}
