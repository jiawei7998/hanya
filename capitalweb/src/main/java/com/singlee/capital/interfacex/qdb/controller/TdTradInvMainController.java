package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.model.TdTradInvMain;
import com.singlee.capital.interfacex.qdb.model.TdTradeInvDetail;
import com.singlee.capital.interfacex.qdb.service.TdTradInvMainService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TdTradInvMainController")
public class TdTradInvMainController extends CommonController{
	@Autowired
	private TdTradInvMainService invMainService;
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	@ResponseBody
	@RequestMapping(value = "/selectTdTradInvMainHisPage")
	public RetMsg<PageInfo<TdTradInvMain>> selectTdTradInvMainPage(@RequestBody Map<String, Object> params) throws RException{
		String iDate = ParameterUtil.getString(params, "iDate", null);
		params.put("iDate", iDate);
		Page<TdTradInvMain> param = invMainService.selectTdTradInvMainPage(params);
		return RetMsgHelper.ok(param);
	}
	
	@ResponseBody
	@RequestMapping(value = "/selectTdTradInvDetailHisPage")
	public RetMsg<PageInfo<TdTradeInvDetail>> selectTdTradInvDetailPage(@RequestBody Map<String, Object> params) throws RException{
		String iDate = ParameterUtil.getString(params, "iDate", null);
		params.put("iDate", iDate);
		Page<TdTradeInvDetail> param = invMainService.selectTdTradInvDetailPage(params);
		return RetMsgHelper.ok(param);
	}
	//委外业务上传Excel
	@ResponseBody
	@RequestMapping(value = "/setTdTradInvMainByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String setTdTradInvMainByExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		StringBuffer sb = new StringBuffer();
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		try {
			String tradeid = HttpUtil.getParameterString(request, "dealNo", null);
			
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			for (UploadedFile uploadedFile : uploadedFileList) {
				String name = uploadedFile.getFullname();
				InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
				sb.append(invMainService.setTdTradInvMainByExcel(name, is,tradeid));
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error("TdTradInvMainController--setTdTradInvMainByExcel"+e);
			sb.append("委外业务信息上传失败");
		}
		return StringUtils.isEmpty(sb.toString())?"委外业务信息上传成功":sb.toString();
	}
	
	
	/**
	 * 按照时间删除委外业务表（包括主表以及明细表）
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTdtradeInv")
	public RetMsg<Serializable> deleteTdtradeInv(@RequestBody Map<String, Object> params) {
		invMainService.deleteTdtradeInv(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/downloadTdTradInvMainByExcel")
	public void downloadTdTradInvMainByExcel(HttpServletRequest request, HttpServletResponse response){

		String ua = request.getHeader("User-Agent");
		String filename = "委外业务基础资产.xlsx";
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		OutputStream ouputStream=null;
		try {
			String tradeid=request.getParameter("tradeid");
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("tradeid", tradeid);
			ExcelUtil e = invMainService.downloadExcel(map);
			
			ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(ouputStream!=null){
				try {
					ouputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
