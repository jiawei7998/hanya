package com.singlee.capital.interfacex.qdb.ESB.dao;

import java.util.HashMap;


public interface HttpClientManagerDao {
    
    public HashMap<String, Object> sendXmlToRequest(String url,String xmlString) throws Exception;
    
    /*public boolean addFx5002(List<Writtengrarantee> listw)throws Exception;
    public List<Writtengrarantee> queryWrittengrarantee()throws Exception;
    
    public List<Writtengrarantee> queryWrittengraranteeBytime(Page page)throws Exception;*/

}
