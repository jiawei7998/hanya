package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.qdb.model.TtMktIr;
import com.singlee.capital.interfacex.qdb.model.TtMktSeriess;
import com.singlee.capital.interfacex.qdb.service.TtMktIrService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * 基本利率
 * 
 * @author xx
 * 
 */
@Component
@Controller
@RequestMapping(value = "/QdTtMktIrController")
public class TtMktIrController extends CommonController {

	/** 基准利率服务层 */
	@Autowired
	private TtMktIrService ttMktIrService;
	
//	public static ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1,new BasicThreadFactory.Builder().namingPattern("example-schedule-pool-%d").daemon(true).build());   

	/**
	 * 基准利率分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtMktIrPage")
	public RetMsg<PageInfo<TtMktIr>> selectTtMktIrPage(@RequestBody Map<String,Object> params) {
		Page<TtMktIr> page = ttMktIrService.selectTtMktIrPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 添加基准利率信息
	 * @param ttMktIr
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtMktIr")
	public RetMsg<Serializable> insertTtMktIr(@RequestBody TtMktIr ttMktIr) {
		ttMktIr.setI_code(ttMktIr.getIr_name()+"_"+ttMktIr.getIr_term());
		ttMktIr.setImp_date("");
		String msg = ttMktIrService.insertTtMktIr(ttMktIr);
		if( "000" .equalsIgnoreCase(msg))
		{
			return RetMsgHelper.ok();
		}else{
			return RetMsgHelper.simple("001", msg);
		}
	}
	
	/**
	 * 修改基准利率信息
	 * @param ttMktIr
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTtMktIr")
	public RetMsg<Serializable> updateTtMktIr(@RequestBody TtMktIr ttMktIr) {
		ttMktIrService.updateTtMktIr(ttMktIr);
		return RetMsgHelper.ok();
	}
	/**
	 * 基准利率提交复核
	 * @param ttMktIr
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/TtMktSeriessVerify")
	public RetMsg<Serializable> TtMktSeriessVerify(@RequestBody TtMktSeriess ttMktSeriess) {
		ttMktSeriess.setStatus("2");
		ttMktSeriess.setHanduser(SlSessionHelper.getUserId());
		ttMktIrService.updateTtMktSeriess(ttMktSeriess);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 修改基准利率信息
	 * @param ttMktIr
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTtMktIrById")
	public RetMsg<Serializable> deleteTtMktIrById(@RequestBody String[] i_code) {
		ttMktIrService.deleteTtMktIrById(i_code);
		return RetMsgHelper.ok();
	}
	
	/*
	 * 基准利率行情历史分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtMktSeriessPage")
	public RetMsg<PageInfo<TtMktSeriess>> selectTtMktSeriessPage(@RequestBody Map<String,Object> params) {
		Page<TtMktSeriess> page = ttMktIrService.selectTtMktSeriessPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/*
	 * 基准利率行情指定个数
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtMktSeriessBySize")
	public RetMsg<List<TtMktSeriess>> selectTtMktSeriessBySize(@RequestBody Map<String,Object> params) {
		List<TtMktSeriess> page = ttMktIrService.selectTtMktSeriessBySize(params);
		return RetMsgHelper.ok(page);
	}
	
	/*
	 * 第一个基准利率行情
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectSerieByDate")
	public RetMsg<TtMktSeriess> selectSerieByDate(@RequestBody Map<String,Object> params) {
		TtMktSeriess page = ttMktIrService.selectSerieByDate(params);
		return RetMsgHelper.ok(page);
	}
	/*
	 * 基准利率行情经办分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIrSeriesHandle")
	public RetMsg<PageInfo<TtMktSeriess>> searchIrSeriesHandle(@RequestBody Map<String,Object> params) {
		params.put("status", "1");
		Page<TtMktSeriess> page = ttMktIrService.selectTtMktSeriessPage(params);
		return RetMsgHelper.ok(page);
	}
	/*
	 * 基准利率行情复核分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIrSeriesVerify")
	public RetMsg<PageInfo<TtMktSeriess>> searchIrSeriesVerify(@RequestBody Map<String,Object> params) {
		params.put("status", "2");
		Page<TtMktSeriess> page = ttMktIrService.selectTtMktSeriessPage(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 基准利率Series经办保存
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIrSeriesHandle")
	public RetMsg<Serializable> saveIrSeriesHandle(@RequestBody Map<String,Object> params) {
		params.put("status", "1");
		ttMktIrService.insertSeries(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率Series复核保存
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/saveIrSeriesVerify")
	public RetMsg<Serializable> saveIrSeriesVerify(@RequestBody Map<String,Object> params) {
		params.put("status", "2");
		params.put("handuser", SlSessionHelper.getUserId());
		ttMktIrService.insertSeries(params);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 基准利率Series删除
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteIrSeries")
	public RetMsg<Serializable> deleteIrSeries(@RequestBody Map<String,Object> params) {
		ttMktIrService.deleteSeries(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率Series详情
	 * 
	 * @param map
	 * @return 用户对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectIrSeries")
	public RetMsg<TtMktSeriess> selectIrSeries(@RequestBody Map<String,Object> params) {
		TtMktSeriess mktSeriess= ttMktIrService.selectSeries(params);
		return RetMsgHelper.ok(mktSeriess);
	}
	
//	/**
//	 * 基准利率Series建立经办
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/createSeries")
//	public RetMsg<Serializable> createSeries(@RequestBody Map<String,Object> params)  {
//		TtMktSeriess mktSeriess = new TtMktSeriess();
//		ParentChildUtil.HashMapToClass(params, mktSeriess);
//		String refId = mktSeriess.getS_id();
//		String type = ParameterUtil.getString(params, "type", "");
//		ttMktIrServiceFlow.create(refId, type, mktSeriess, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId());
//		return RetMsgHelper.ok();
//		
//	}
	
//	/**
//	 * 基准利率Series提交复核
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/operateSeries")
//	public RetMsg<Serializable> operateSeries(@RequestBody Map<String,Object> params) {
//		TtFlowDoubleApprove ttFlowDoubleApprove = new TtFlowDoubleApprove();
//		ParentChildUtil.HashMapToClass(params, ttFlowDoubleApprove);
//		ttMktIrServiceFlow.operate(ttFlowDoubleApprove);
//		return RetMsgHelper.ok();
//		
//	}
	
//	/**
//	 * 基准利率Series建立经办并提交复核
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/createAndOperateSeries")
//	public RetMsg<Serializable> createAndOperateSeries(@RequestBody Map<String,Object> params) {
//		TtMktSeriess mktSeriess = new TtMktSeriess();
//		ParentChildUtil.HashMapToClass(params, mktSeriess);
//		String refId = mktSeriess.getS_id();
//		String type = ParameterUtil.getString(params, "type", "");
//		ttMktIrServiceFlow.createAndOperate(refId, type, mktSeriess, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId());
//		return RetMsgHelper.ok();
//	}
	
	/**
	 * 基准利率Series复核通过
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/TtMktSeriessPass")
	public RetMsg<Serializable> TtMktSeriessPass( @RequestBody TtMktSeriess ttMktSeriess) {
		String handuser= ttMktSeriess.getHanduser();
		if(handuser!=null&&!"".equals(handuser)){
			if(handuser.equals(SlSessionHelper.getUserId())){
				throw new RuntimeException("经办人不能复核");
			}
		}else {throw new RuntimeException("未查询到经办人信息");}
		/*
		 * 先按照日期查询，若当天的日期存在基准利率，则，修改当天的基准利率，否则，修改一下最新的基准利率状态。
		 */
			//查询已通过的基准利率并修改为新数据。
			Map<String, Object> params=new HashMap<String, Object>();
			params.put("status", 3);
			params.put("beg_date", ttMktSeriess.getBeg_date());
			TtMktSeriess ttMktSeriess2=ttMktIrService.selectSerieByDate2(params);
			if(ttMktSeriess2!=null){
				//查询到当天数据重新修改基准利率
				ttMktSeriess2.setVerifyuser(SlSessionHelper.getUserId());
				ttMktSeriess2.setI_code(ttMktSeriess.getI_code());
				ttMktSeriess2.setIr_name(ttMktSeriess.getIr_name());
				ttMktSeriess2.setBeg_date(ttMktSeriess.getBeg_date());
				ttMktSeriess2.setS_close(ttMktSeriess.getS_close());
				ttMktSeriess2.setS_high(ttMktSeriess.getS_high());
				ttMktIrService.updateTtMktSeriess(ttMktSeriess2);
				//然后删除最新的基准利率
				ttMktIrService.deleteSeries(ttMktSeriess);
			}else{
				ttMktSeriess.setStatus("3");
				ttMktIrService.updateTtMktSeriess(ttMktSeriess);
			}

		return RetMsgHelper.ok();
	}
	
	/**
	 * 基准利率Series复核拒绝
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/TtMktSeriessRefuse")
	public RetMsg<Serializable> TtMktSeriessRefuse( @RequestBody TtMktSeriess ttMktSeriess) {
		String handuser= ttMktSeriess.getHanduser();
		if(handuser!=null&&!"".equals(handuser)){
			if(handuser.equals(SlSessionHelper.getUserId())){
				throw new RuntimeException("经办人不能复核");
			}
		}else {throw new RuntimeException("未查询到经办人信息");}
		ttMktSeriess.setStatus("0");
		ttMktIrService.updateTtMktSeriess(ttMktSeriess);
		return RetMsgHelper.ok();
	}
	
//	/**
//	 * 基准利率Series复核
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/verifySeries")
//	public RetMsg<Serializable> verifySeries(@RequestBody List<Map<String,Object>> params) {
//		for(Map<String, Object> map : params){
//			TtFlowDoubleApprove ttFlowDoubleApprove = new TtFlowDoubleApprove();
//			ParentChildUtil.HashMapToClass(map, ttFlowDoubleApprove);
//			boolean isOk = ParameterUtil.getBoolean(map, "isOk", true);
//			ttMktIrServiceFlow.verify(ttFlowDoubleApprove, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId(), isOk);
//		}
//		return RetMsgHelper.ok();
//	}
	
//	/**
//	 * 基准利率Series经办
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchIrSeriesWaitingOperate")
//	public RetMsg<List<TtFlowDoubleApprove>> searchIrSeriesWaitingOperate(@RequestBody Map<String,Object> params){
//		TtMktSeriess mktSeriess = new TtMktSeriess();
//		String status = DictConstants.DoubleApproveStatus.WaitOperate;
//		String refId = ParameterUtil.getString(params, "refId", null);
//		List<TtFlowDoubleApprove> list = ttMktIrServiceFlow.searchDoubleApprove(refId, mktSeriess.getClass().getName(), status, 
//				SlSessionHelper.getUserId(), null);
//		return RetMsgHelper.ok(list);
//	}
	
//	/**
//	 * 基准利率Series/复核记录查询
//	 * 
//	 * @param map
//	 * @return 
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/searchIrSeriesWaitingApprove")
//	public RetMsg<List<TtFlowDoubleApprove>> searchIrSeriesWaitingApprove(@RequestBody Map<String,Object> params)  {
//		TtMktSeriess mktSeriess = new TtMktSeriess();
//		String status = DictConstants.DoubleApproveStatus.WaitApprove;
//		String refId = ParameterUtil.getString(params, "refId", null);
//		List<TtFlowDoubleApprove> list = ttMktIrServiceFlow.searchDoubleApprove(refId, mktSeriess.getClass().getName(), status, 
//				null, null);
//		return RetMsgHelper.ok(list);
//	}
	
	@ResponseBody
	@RequestMapping(value="setStockIrExcel")
	public void setStockIrExcel() throws Exception{
		//StringBuffer sb = new StringBuffer();
		//sb.append(ttMktIrService.setStockPriceByExcel());
		//return StringUtils.isEmpty(sb.toString())?"自动上传成功":sb.toString();
		/*Runnable runnable = new Runnable() {
			@Override
			public void run() {
				//System.out.println("executorService:"+sdf.format(new Date()));
				try {
					ttMktIrService.setStockPriceByExcel();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		executorService.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);*/
	}
	
	
	
	//上传Excel
	@ResponseBody
	@RequestMapping(value = "/importIrByExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String importIrByExcel(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception{
		StringBuffer sb = new StringBuffer();
		try {
			// 1.文件获取
			List<UploadedFile> uploadedFileList = null;
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			for (UploadedFile uploadedFile : uploadedFileList) {
				String name = uploadedFile.getFullname();
				InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
				sb.append(ttMktIrService.importIrByExcel(name,is));
			}
		} catch (IOException e) {
			JY.error("手动上传失败");
			sb.append("手动上传失败");
		}
		return StringUtils.isEmpty(sb.toString())?"手动上传成功":sb.toString();
	}
}
