package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportValTradeBean implements Serializable{

	/**
	 * 3.金市非债业务估值基础数据表
	 */
	private static final long serialVersionUID = 1L;
	private String seq         ;//序号                  
	private String dealNo      ;//投资代号              
	private String custName    ;//客户名称              
	private double amt         ;//面值                  
	private double rate        ;//利率                  
	private String vDate       ;//起息日                
	private String mDate       ;//到期日                
	private String ccy         ;//币种                  
	private double intAmt      ;//应计利息              
	private double balance     ;//账面余额              
	private String intPayCycle ;//付息频率(INTPAYCYCLE) 
	private String intType     ;//计息方式              
	private String basis       ;//计息基准(BASIS)       
	private String intCalcRule ;//计息规则(INTCALCRULE) 
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getIntAmt() {
		return intAmt;
	}
	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getIntPayCycle() {
		return intPayCycle;
	}
	public void setIntPayCycle(String intPayCycle) {
		this.intPayCycle = intPayCycle;
	}
	public String getIntType() {
		return intType;
	}
	public void setIntType(String intType) {
		this.intType = intType;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getIntCalcRule() {
		return intCalcRule;
	}
	public void setIntCalcRule(String intCalcRule) {
		this.intCalcRule = intCalcRule;
	}

	
	
	
	
	
}
