package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpIntBnkAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpIntBnkAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="RefNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustmerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CloseDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpIntBnkAaaRs", propOrder = { "commonRsHdr", "refNo",
		"coCode", "acctName", "custmerCode", "cusName", "term", "openDate",
		"closeDate", "closeDetail", "intAmt", "status" })
public class TDpIntBnkAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "RefNo", required = true)
	protected String refNo;
	@XmlElement(name = "CoCode", required = true)
	protected String coCode;
	@XmlElement(name = "AcctName", required = true)
	protected String acctName;
	@XmlElement(name = "CustmerCode", required = true)
	protected String custmerCode;
	@XmlElement(required = true)
	protected String cusName;
	@XmlElement(name = "Term", required = true)
	protected String term;
	@XmlElement(name = "OpenDate", required = true)
	protected String openDate;
	@XmlElement(name = "CloseDate", required = true)
	protected String closeDate;
	@XmlElement(name = "CloseDetail", required = true)
	protected String closeDetail;
	@XmlElement(name = "IntAmt", required = true)
	protected String intAmt;
	@XmlElement(name = "Status", required = true)
	protected String status;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the refNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * Sets the value of the refNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRefNo(String value) {
		this.refNo = value;
	}

	/**
	 * Gets the value of the coCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoCode() {
		return coCode;
	}

	/**
	 * Sets the value of the coCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoCode(String value) {
		this.coCode = value;
	}

	/**
	 * Gets the value of the acctName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * Sets the value of the acctName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctName(String value) {
		this.acctName = value;
	}

	/**
	 * Gets the value of the custmerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustmerCode() {
		return custmerCode;
	}

	/**
	 * Sets the value of the custmerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustmerCode(String value) {
		this.custmerCode = value;
	}

	/**
	 * Gets the value of the cusName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCusName() {
		return cusName;
	}

	/**
	 * Sets the value of the cusName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCusName(String value) {
		this.cusName = value;
	}

	/**
	 * Gets the value of the term property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the value of the term property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTerm(String value) {
		this.term = value;
	}

	/**
	 * Gets the value of the openDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenDate() {
		return openDate;
	}

	/**
	 * Sets the value of the openDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenDate(String value) {
		this.openDate = value;
	}

	/**
	 * Gets the value of the closeDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCloseDate() {
		return closeDate;
	}

	/**
	 * Sets the value of the closeDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCloseDate(String value) {
		this.closeDate = value;
	}

	/**
	 * Gets the value of the closeDetail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCloseDetail() {
		return closeDetail;
	}

	/**
	 * Sets the value of the closeDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCloseDetail(String value) {
		this.closeDetail = value;
	}

	/**
	 * Gets the value of the intAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntAmt() {
		return intAmt;
	}

	/**
	 * Sets the value of the intAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntAmt(String value) {
		this.intAmt = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

}
