package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OffBalRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OffBalRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OffBalAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OffBalAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OffBalRec", propOrder = { "offBalAcct", "offBalAmt" })
public class OffBalRec {

	@XmlElement(name = "OffBalAcct", required = true)
	protected String offBalAcct;
	@XmlElement(name = "OffBalAmt", required = true)
	protected String offBalAmt;

	/**
	 * Gets the value of the offBalAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOffBalAcct() {
		return offBalAcct;
	}

	/**
	 * Sets the value of the offBalAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOffBalAcct(String value) {
		this.offBalAcct = value;
	}

	/**
	 * Gets the value of the offBalAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOffBalAmt() {
		return offBalAmt;
	}

	/**
	 * Sets the value of the offBalAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOffBalAmt(String value) {
		this.offBalAmt = value;
	}

}
