package com.singlee.capital.interfacex.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fund.mapper.ProductApproveFundMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceDependScopeMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper;
import com.singlee.capital.interfacex.model.CheckPayment;
import com.singlee.capital.interfacex.model.Payment;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.interfacex.model.SelfPayment;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRq;
import com.singlee.capital.interfacex.model.TiInterfaceDependScope;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRec;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.service.TiInterfaceInfoService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.mapper.TaLogMapper;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TiInterfaceInfoServiceImpl implements TiInterfaceInfoService{

	@Autowired
	private TiInterfaceInfoMapper tiInterfaceInfoMapper;
	
	@Autowired
	private TiInterfaceDependScopeMapper tiInterfaceDependScopeMapper;
	
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	
	@Autowired
	private BatchDao batchDao;
	
	@Autowired
	private SocketClientService socketClientService;
	
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	
	@Autowired
	private TaLogMapper logMapper;
	
	@Autowired
	private ProductApproveFundMapper productApproveFundMapper;
	
	public void deleteDependScope(Map<String, Object> map) throws Exception {
		TiInterfaceDependScope dependscope = new TiInterfaceDependScope();
		dependscope.setSeq(map.get("seq").toString());
		tiInterfaceDependScopeMapper.delete(dependscope);
	}
	public Page<TiInterfaceDependScope> queryDependScopePage(Map<String, Object> map) throws Exception{
		return tiInterfaceDependScopeMapper.queryDependScopePage(map);
	}
	public TiInterfaceDependScope queryDependScopeByScopes(Map<String, Object> map) throws Exception{
		return tiInterfaceDependScopeMapper.queryDependScopeByScopes(map);
	}
	public void insertDependScope(TiInterfaceDependScope tiInterfaceDependScope) throws Exception{
		tiInterfaceDependScopeMapper.insertDependScope(tiInterfaceDependScope);
	}
	
	@Override
	public void deleteInterfaceInfo(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		
		tiInterfaceInfoMapper.deleteTiInterfaceInfoById(map);
	}

	@Override
	public void updateInterfaceInfo(TiInterfaceInfo interfaceInfo)
			throws Exception {
		// TODO Auto-generated method stub
		tiInterfaceInfoMapper.updateTiInterfaceInfo(interfaceInfo);
	}

	@Override
	public void addInterfaceInfo(TiInterfaceInfo interfaceInfo) throws Exception {
		// TODO Auto-generated method stub
		tiInterfaceInfoMapper.insertTiInterfaceInfo(interfaceInfo);
	}

	@Override
	public Page<TiInterfaceInfo> selectInterfaceInfoByName(Map<String, Object> map)
			throws Exception {
		// TODO Auto-generated method stub
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return tiInterfaceInfoMapper.selectTiInterfaceInfoByName(map);
	}
	@Override
	public void insertQuotaSyncopate(List<QuotaSyncopateRec> quotaSyncopateRec)
			throws Exception {
		// TODO Auto-generated method stub
		batchDao.batch("com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper.insertQuotaSyncopate", quotaSyncopateRec);
	}
	@Override
	public Page<Payment> query2ndPaymentPage(Map<String, Object> map, RowBounds rb)
			throws Exception {
		// TODO Auto-generated method stub  
		//formal 1 正式   0 预申请
		if(map.get("formal").equals("1")){
			return ti2ndPaymentMapper.query2ndPaymentPage(map, rb);
		}else{
			return ti2ndPaymentMapper.query2ndPaymentPage2(map, rb);
		}
	}
	@Override
	public boolean sendQueryPaymentTradeStatus(Map<String, Object> map)
			throws Exception {
		// TODO Auto-generated method stub
		boolean flag = true;
		try {
			if(map.get("userdefinetrancode").equals("UPPSCdtTrf")){
				UPPSigBusiQueryRq request = new UPPSigBusiQueryRq();
				request.setUserDefineTranCode(InterfaceCode.TI2ND_QueryUserDefineTranCode);
				request.setOrigChannelCode(InterfaceCode.TI_CHANNELNO);
				request.setOrigChannelDate(map.get("workdate").toString().replace("-", ""));
				request.setOrigFeeBaseLogId(map.get("feeid").toString());
				UPPSigBusiQueryRs response = socketClientService.T2ndPaymentUPPSigBusiQueryRequest(request);
				for(UPPSigBusiQueryRec rec : response.getUPPSigBusiQueryRec()){
					if(rec.getFeeBaseLogId().trim().endsWith(map.get("feeid").toString().trim())){
						Map<String, Object> map1 = new HashMap<String, Object>();
						map1.put("retcode", rec.getCenterDealCode());
						map1.put("retmsg", rec.getCenterDealMsg());
						map1.put("rquid",rec.getChannelSerNo());
						map1.put("agentserialno", rec.getAgentSerialNo());
						map1.put("centerdealcode", rec.getCenterDealCode());
						map1.put("centerdealmsg", rec.getCenterDealMsg());
						map1.put("payresult", rec.getPayResult());
						map1.put("workdate", rec.getWorkDate());
						map1.put("bankdate", rec.getBankDate());
						map1.put("bankid", rec.getBankId());
						map1.put("feeBaseLogid", map.get("feeid").toString().trim());
						ti2ndPaymentMapper.update2ndPaymentRetMsg(map1);
					}
				}
				
			}else if(map.get("userdefinetrancode").equals("TStBeaRepayAaaRs")){
				
				TDpAcStmtInqRq request = new TDpAcStmtInqRq();
				request.setMediumType("A");
				request.setAcctNo(map.get("accno").toString());
				request.setBeginDt(map.get("workdate").toString().replace("-", ""));
				request.setEndDt(map.get("workdate").toString().replace("-", ""));
				request.setMode("I");
				socketClientService.t24TdpAcStmtInqRequest(request);
			}
		} catch (Exception e) {
			// TODO: handle exception
			flag = false;
			//e.printStackTrace();
			throw new RException(e);
		}
		
		return flag;
	}
	@Override
	public List<UPPSCdtTrfRq> queryT2ndPaymentConfirmTrade(
			Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return ti2ndPaymentMapper.queryT2ndPaymentConfirmTrade(map);
	}


	@Override
	public Page<CheckPayment> Check2ndPayment(Map<String, Object> map, RowBounds rb)
			throws Exception {
		// TODO Auto-generated method stub
		return ti2ndPaymentMapper.Check2ndPaymentPage(map, rb);
	}
	
	
	@Override
	public Page<SelfPayment> SelfPayment(Map<String, Object> map, RowBounds rb)
			throws Exception {
		// TODO Auto-generated method stub
		return ti2ndPaymentMapper.SelfPaymentPage(map, rb);
	}
	@Override
	public List<TdProductApproveMain> queryTdProductApproveMainByDealno( Map<String, Object> map) {
		List<TdProductApproveMain> list=new ArrayList<TdProductApproveMain>();
		TdProductApproveMain productApproveMain = productApproveMainMapper.getProductApproveMainActivated(map.get("dealno").toString().trim());
		
		map.put("feeBaseLogid", StringUtils.trimToEmpty(map.get("dealno").toString()));
		try {
			UPPSCdtTrfRq uppsCdtTrfRq = ti2ndPaymentMapper.queryTi2ndPayment(map);
			if(null != uppsCdtTrfRq && null !=productApproveMain)
			{
				productApproveMain.setRetcode(uppsCdtTrfRq.getRetCode());
				productApproveMain.setRetmsg(uppsCdtTrfRq.getRetMsg());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new RException(e);
		}
		list.add(productApproveMain);
		return list;
	}
	@Override
	public List<TaLog> queryTaLogMessage(Map<String, Object> map) {
		return logMapper.queryTaLogMessage(map);
	}
	@Override
	public int insertHandleCodeRemarkNote(Map<String, Object> map) throws Exception {
		return ti2ndPaymentMapper.updateHandleCodeRemarkNote(map);
	}
	@Override
	public List<ProductApproveFund> queryTdProductFundByDealno(
			Map<String, Object> map) {
		List<ProductApproveFund> list=new ArrayList<ProductApproveFund>();
		map.put("dealNo", StringUtils.trimToEmpty(map.get("dealno").toString()));
		ProductApproveFund productApproveFund=productApproveFundMapper.getProductApproveFund(map);
		map.put("feeBaseLogid", StringUtils.trimToEmpty(map.get("dealno").toString()));
		try {
			UPPSCdtTrfRq uppsCdtTrfRq = ti2ndPaymentMapper.queryTi2ndPayment(map);
			if(null != uppsCdtTrfRq && productApproveFund !=null)
			{
				productApproveFund.setContact(uppsCdtTrfRq.getRetCode());
				productApproveFund.setContactAddr(uppsCdtTrfRq.getRetMsg());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new RException(e);
		}
		list.add(productApproveFund);
		return list;
	}
	@Override
	public void exceptExcel(Map<String, Object> map,HSSFWorkbook wb)
			throws Exception {
		String clear = map.get("userdefinetrancode").toString().equals("UPPSCdtTrf")?"行外清算":"行内清算";
		String appro = map.get("formal").toString().equals("0")?"基金":"正式审批";
		String fileName =clear+appro;  
		// 第一步，创建一个webbook，对应一个Excel文件
//		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(fileName);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		//第五步，写入实体数据
		List<Payment> lists = null;
		if(map.get("formal").equals("1")){
			lists = ti2ndPaymentMapper.query2ndPaymentPage(map);
		}else{
			lists = ti2ndPaymentMapper.query2ndPaymentPage2(map);
		}
		if(clear.equals("行外清算")){
			HSSFCell cell = row.createCell((short) 0);
			cell.setCellValue("交易单编号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 1);
			cell.setCellValue("产品名称");
			cell.setCellStyle(style);
			cell = row.createCell((short) 2);
			cell.setCellValue("划款路径");
			cell.setCellStyle(style);
			cell = row.createCell((short) 3);
			cell.setCellValue("汇款模式");
			cell.setCellStyle(style);
			cell = row.createCell((short) 4);
			cell.setCellValue("业务类型编码");
			cell.setCellStyle(style);
			cell = row.createCell((short) 5);
			cell.setCellValue("业务种类");
			cell.setCellStyle(style);
			cell = row.createCell((short) 6);
			cell.setCellValue("币种");
			cell.setCellStyle(style);
			cell = row.createCell((short) 7);
			cell.setCellValue("交易金额");
			cell.setCellStyle(style);
			cell = row.createCell((short) 8);
			cell.setCellValue("付款人账号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 9);
			cell.setCellValue("付款人名称");
			cell.setCellStyle(style);
			cell = row.createCell((short) 10);
			cell.setCellValue("收款人行号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 11);
			cell.setCellValue("收款人账号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 12);
			cell.setCellValue("收款人名称");
			cell.setCellStyle(style);
			cell = row.createCell((short) 13);
			cell.setCellValue("处理代码");
			cell.setCellStyle(style);
			cell = row.createCell((short) 14);
			cell.setCellValue("处理说明");
			cell.setCellStyle(style);
			cell = row.createCell((short) 15);
			cell.setCellValue("处理日期");
			cell.setCellStyle(style);
			cell = row.createCell((short) 16);
			cell.setCellValue("处理状态");
			cell.setCellStyle(style);
//			cell = row.createCell((short) 17);
//			cell.setCellValue("外部流水");
//			cell.setCellStyle(style);
			cell = row.createCell((short) 17);
			cell.setCellValue("帐务主机流水");
			cell.setCellStyle(style);
			cell = row.createCell((short) 18);
			cell.setCellValue("手工处理");
			cell.setCellStyle(style);
			cell = row.createCell((short) 19);
			cell.setCellValue("手工处理说明");
			cell.setCellStyle(style);
			for(int i = 0; i < lists.size(); i++){
				row = sheet.createRow((int)i+1);
				row.createCell((short) 0).setCellValue(lists.get(i).getDealno());
				row.createCell((short) 1).setCellValue(lists.get(i).getPrdname());
				row.createCell((short) 2).setCellValue(lists.get(i).getPaypathcode());
				row.createCell((short) 3).setCellValue(lists.get(i).getPosentrymode());
				row.createCell((short) 4).setCellValue(lists.get(i).getBusitype());
				row.createCell((short) 5).setCellValue(lists.get(i).getBusikind());
				row.createCell((short) 6).setCellValue(lists.get(i).getCurrency());
				row.createCell((short) 7).setCellValue(lists.get(i).getAmount());
				row.createCell((short) 8).setCellValue(lists.get(i).getPayeracc());
				row.createCell((short) 9).setCellValue(lists.get(i).getPayername());
				row.createCell((short) 10).setCellValue(lists.get(i).getPayeeaccbank());
				row.createCell((short) 11).setCellValue(lists.get(i).getPayeeacc());
				row.createCell((short) 12).setCellValue(lists.get(i).getPayeename());
				row.createCell((short) 13).setCellValue(lists.get(i).getRetcode());
				row.createCell((short) 14).setCellValue(lists.get(i).getRetmsg());
				row.createCell((short) 15).setCellValue(lists.get(i).getWorkdate());
				row.createCell((short) 16).setCellValue(lists.get(i).getPayresult());
				row.createCell((short) 17).setCellValue(lists.get(i).getBankid());
				row.createCell((short) 18).setCellValue(lists.get(i).getHandleCode());
				row.createCell((short) 19).setCellValue(lists.get(i).getRemarkNote());
			}
		}else{
			HSSFCell cell = row.createCell((short) 0);
			cell.setCellValue("交易单编号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 1);
			cell.setCellValue("产品名称");
			cell.setCellStyle(style);
//			cell = row.createCell((short) 2);
//			cell.setCellValue("划款路径");
//			cell.setCellStyle(style);
//			cell = row.createCell((short) 3);
//			cell.setCellValue("汇款模式");
//			cell.setCellStyle(style);
			cell = row.createCell((short) 2);
			cell.setCellValue("业务类型编码");
			cell.setCellStyle(style);
			cell = row.createCell((short) 3);
			cell.setCellValue("业务种类");
			cell.setCellStyle(style);
			cell = row.createCell((short) 4);
			cell.setCellValue("币种");
			cell.setCellStyle(style);
			cell = row.createCell((short) 5);
			cell.setCellValue("交易金额");
			cell.setCellStyle(style);
			cell = row.createCell((short) 6);
			cell.setCellValue("付款人账号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 7);
			cell.setCellValue("付款人名称");
			cell.setCellStyle(style);
			cell = row.createCell((short) 8);
			cell.setCellValue("收款人行号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 9);
			cell.setCellValue("收款人账号");
			cell.setCellStyle(style);
			cell = row.createCell((short) 10);
			cell.setCellValue("收款人名称");
			cell.setCellStyle(style);
			cell = row.createCell((short) 11);
			cell.setCellValue("处理代码");
			cell.setCellStyle(style);
			cell = row.createCell((short) 12);
			cell.setCellValue("处理说明");
			cell.setCellStyle(style);
			cell = row.createCell((short) 13);
			cell.setCellValue("处理日期");
			cell.setCellStyle(style);
//			cell = row.createCell((short) 16);
//			cell.setCellValue("处理状态");
//			cell.setCellStyle(style);
			cell = row.createCell((short) 14);
			cell.setCellValue("外部流水");
			cell.setCellStyle(style);
//			cell = row.createCell((short) 15);
//			cell.setCellValue("帐务主机流水");
//			cell.setCellStyle(style);
			cell = row.createCell((short) 15);
			cell.setCellValue("手工处理");
			cell.setCellStyle(style);
			cell = row.createCell((short) 16);
			cell.setCellValue("手工处理说明");
			cell.setCellStyle(style);
			for(int i = 0; i < lists.size(); i++){
				row = sheet.createRow((int)i+1);
				row.createCell((short) 0).setCellValue(lists.get(i).getDealno());
				row.createCell((short) 1).setCellValue(lists.get(i).getPrdname());
//				row.createCell((short) 2).setCellValue(lists.get(i).getPaypathcode());
//				row.createCell((short) 3).setCellValue(lists.get(i).getPosentrymode());
				row.createCell((short) 2).setCellValue(lists.get(i).getBusitype());
				row.createCell((short) 3).setCellValue(lists.get(i).getBusikind());
				row.createCell((short) 4).setCellValue(lists.get(i).getCurrency());
				row.createCell((short) 5).setCellValue(lists.get(i).getAmount());
				row.createCell((short) 6).setCellValue(lists.get(i).getPayeracc());
				row.createCell((short) 7).setCellValue(lists.get(i).getPayername());
				row.createCell((short) 8).setCellValue(lists.get(i).getPayeeaccbank());
				row.createCell((short) 9).setCellValue(lists.get(i).getPayeeacc());
				row.createCell((short) 10).setCellValue(lists.get(i).getPayeename());
				row.createCell((short) 11).setCellValue(lists.get(i).getRetcode());
				row.createCell((short) 12).setCellValue(lists.get(i).getRetmsg());
				row.createCell((short) 13).setCellValue(lists.get(i).getWorkdate());
//				row.createCell((short) 15).setCellValue(lists.get(i).getPayresult());
				row.createCell((short) 14).setCellValue(lists.get(i).getSrquid());
				row.createCell((short) 15).setCellValue(lists.get(i).getHandleCode());
				row.createCell((short) 16).setCellValue(lists.get(i).getRemarkNote());
			}
		}		
		
	}
}
