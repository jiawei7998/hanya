package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class SelfPayment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String feebaselogid        ;//交易单编号
	private String prd_name;//产品名称
	private String paypathcode;//划款路径
	private String posentrymode;//汇款模式
	private String busitype;//业务类型编码
	private String busikind;//业务种类
	private String currency;//币种
	private String amount;//交易金额
	private String CONTRACT_RATE;//投资利率
	private String V_DATE;//起息日
	private String M_DATE;//到期日
	private String INT_FRE;//收息频率
	private String AMT_FRE;//还本频率
	private String INT_TYPE;//收息方式
	public String getFeebaselogid() {
		return feebaselogid;
	}
	public void setFeebaselogid(String feebaselogid) {
		this.feebaselogid = feebaselogid;
	}
	public String getPrd_name() {
		return prd_name;
	}
	public void setPrd_name(String prdName) {
		prd_name = prdName;
	}
	public String getPaypathcode() {
		return paypathcode;
	}
	public void setPaypathcode(String paypathcode) {
		this.paypathcode = paypathcode;
	}
	public String getPosentrymode() {
		return posentrymode;
	}
	public void setPosentrymode(String posentrymode) {
		this.posentrymode = posentrymode;
	}
	public String getBusitype() {
		return busitype;
	}
	public void setBusitype(String busitype) {
		this.busitype = busitype;
	}
	public String getBusikind() {
		return busikind;
	}
	public void setBusikind(String busikind) {
		this.busikind = busikind;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCONTRACT_RATE() {
		return CONTRACT_RATE;
	}
	public void setCONTRACT_RATE(String cONTRACTRATE) {
		CONTRACT_RATE = cONTRACTRATE;
	}
	public String getV_DATE() {
		return V_DATE;
	}
	public void setV_DATE(String vDATE) {
		V_DATE = vDATE;
	}
	public String getM_DATE() {
		return M_DATE;
	}
	public void setM_DATE(String mDATE) {
		M_DATE = mDATE;
	}
	public String getINT_FRE() {
		return INT_FRE;
	}
	public void setINT_FRE(String iNTFRE) {
		INT_FRE = iNTFRE;
	}
	public String getAMT_FRE() {
		return AMT_FRE;
	}
	public void setAMT_FRE(String aMTFRE) {
		AMT_FRE = aMTFRE;
	}
	public String getINT_TYPE() {
		return INT_TYPE;
	}
	public void setINT_TYPE(String iNTTYPE) {
		INT_TYPE = iNTTYPE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
