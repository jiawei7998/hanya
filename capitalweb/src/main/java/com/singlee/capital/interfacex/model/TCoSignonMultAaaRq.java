package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCoSignonMultAaaRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCoSignonMultAaaRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="FBID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FtTxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumAccNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pwd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalEntTyp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbNoRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}FbNoRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCoSignonMultAaaRq", propOrder = { "commonRqHdr", "fbid",
		"ftTxnType", "mediumType", "mediumAccNo", "pwd", "name", "legalEntTyp",
		"legalId", "fbNoRec" })
public class TCoSignonMultAaaRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "FBID", required = true)
	protected String fbid;
	@XmlElement(name = "FtTxnType", required = true)
	protected String ftTxnType;
	@XmlElement(name = "MediumType", required = true)
	protected String mediumType;
	@XmlElement(name = "MediumAccNo", required = true)
	protected String mediumAccNo;
	@XmlElement(name = "Pwd", required = true)
	protected String pwd;
	@XmlElement(name = "Name", required = true)
	protected String name;
	@XmlElement(name = "LegalEntTyp", required = true)
	protected String legalEntTyp;
	@XmlElement(name = "LegalId", required = true)
	protected String legalId;
	@XmlElement(name = "FbNoRec")
	protected List<FbNoRec> fbNoRec;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the fbid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFBID() {
		return fbid;
	}

	/**
	 * Sets the value of the fbid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFBID(String value) {
		this.fbid = value;
	}

	/**
	 * Gets the value of the ftTxnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFtTxnType() {
		return ftTxnType;
	}

	/**
	 * Sets the value of the ftTxnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFtTxnType(String value) {
		this.ftTxnType = value;
	}

	/**
	 * Gets the value of the mediumType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumType() {
		return mediumType;
	}

	/**
	 * Sets the value of the mediumType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumType(String value) {
		this.mediumType = value;
	}

	/**
	 * Gets the value of the mediumAccNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumAccNo() {
		return mediumAccNo;
	}

	/**
	 * Sets the value of the mediumAccNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumAccNo(String value) {
		this.mediumAccNo = value;
	}

	/**
	 * Gets the value of the pwd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * Sets the value of the pwd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPwd(String value) {
		this.pwd = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the legalEntTyp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalEntTyp() {
		return legalEntTyp;
	}

	/**
	 * Sets the value of the legalEntTyp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalEntTyp(String value) {
		this.legalEntTyp = value;
	}

	/**
	 * Gets the value of the legalId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalId() {
		return legalId;
	}

	/**
	 * Sets the value of the legalId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalId(String value) {
		this.legalId = value;
	}

	/**
	 * Gets the value of the fbNoRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the fbNoRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getFbNoRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link FbNoRec }
	 * 
	 * 
	 */
	public List<FbNoRec> getFbNoRec() {
		if (fbNoRec == null) {
			fbNoRec = new ArrayList<FbNoRec>();
		}
		return this.fbNoRec;
	}

}
