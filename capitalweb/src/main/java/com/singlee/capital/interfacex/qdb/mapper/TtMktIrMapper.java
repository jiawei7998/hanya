package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktIr;
import com.singlee.capital.interfacex.qdb.model.TtMktSeriess;

public interface TtMktIrMapper extends Mapper<TtMktIr>{
	/**
	 * 查询基准利率信息，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktIr> selectTtMktIrPage(Map<String, Object> map,RowBounds rowBounds);
	
	/**
	 * 根据基准利率代码删除信息
	 * @param i_code
	 */
	public void deleteTtMktIrById(@Param("i_code")String i_code);
	
	/**
	 * 根据基准利率代码删除信息
	 * @param s_id
	 */
	public void deleteTtMktIrBySid(@Param("s_id")String s_id);
	
	/**
	 * 修改基准利率利率信息
	 * @param ttMktIr
	 */
	public void updateTtMktIr(TtMktIr ttMktIr);
	
	/**
	 * 添加基准利率利率信息
	 * @param ttMktIr
	 */
	public void insertTtMktIr(TtMktIr ttMktIr);
	
	/**
	 * 根据id查询存在多少条数据
	 * @param ttMktIr
	 * @return
	 */
	public int queryTtMktIrById(TtMktIr ttMktIr);
	
	/**
	 * 查询市场利率行情历史，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktSeriess> selectTtMktSeriessPage(Map<String, Object> map,RowBounds rowBounds);
	
	/**
	 * 添加Series
	 * @param map
	 * @return
	 */
	public void insertSeries(Map<String,Object> map);
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	public void deleteSeries(Map<String,Object> map);
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */
	public void updateSeries(Map<String,Object> map);
	
	/**
	 * 获得指定的利率行情对象
	 * @param map
	 * 	s_id
	 * @return
	 */
	public TtMktSeriess selectSeries(Map<String,Object> map);
	
	/**
	 * 获得指定的利率行情对象
	 * @param map
	 * 	s_id
	 * @return
	 */
	public TtMktSeriess selectSerieByDate(Map<String,Object> map);
	
	
	/**
	 * 获得当天的利率行情对象
	 * @param map
	 * 	s_id
	 * @return
	 */
	public TtMktSeriess selectSerieByDate2(Map<String,Object> map);
	/**
	 * 按照sid获得指定的利率行情对象
	 * @param map
	 * 	s_id
	 * @return
	 */
	public TtMktSeriess selectSerieBySid(String Sid);
	
	
	
	
	/**
	 * 获得行情序列号
	 * @param 
	 * @return String
	 */
	public String getSeriesSeq();
	
	/**
	 * 批量插入行情数据
	 * @param list
	 */
	public void insertTtMktSeriessExcel(List<TtMktSeriess> list);
	
	/**
	 * 查询有多少条数据
	 * @return
	 */
	public int selectTtMktSeriessCount();
	
	/**
	 * 删除所有
	 */
	public void deleteSeriesAll();
	
	
	/**
	 * 删除所有
	 */
	public void deleteSeriesByDate(Map<String,Object> map);
	
	/*
	 * 修改
	 */
	public void updateSeries(TtMktSeriess ttMktSeriess);

	public List<TtMktSeriess> selectTtMktSeriessBySize(Map<String, Object> params);
}
