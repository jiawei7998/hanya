package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcStmtInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcStmtInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="TDpCorpStmtInqRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TDpCorpStmtInqRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcStmtInqRs", propOrder = { "commonRsHdr",
		"tDpAcStmtInfoRec" })
public class TDpAcStmtInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "TDpAcStmtInfoRec")
	protected List<TDpAcStmtInfoRec> tDpAcStmtInfoRec;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the tDpCorpStmtInqRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the tDpCorpStmtInqRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTDpCorpStmtInqRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TDpCorpStmtInqRec }
	 * 
	 * 
	 */
	public List<TDpAcStmtInfoRec> getTDpAcStmtInfoRec() {
		if (tDpAcStmtInfoRec == null) {
			tDpAcStmtInfoRec = new ArrayList<TDpAcStmtInfoRec>();
		}
		return this.tDpAcStmtInfoRec;
	}

}
