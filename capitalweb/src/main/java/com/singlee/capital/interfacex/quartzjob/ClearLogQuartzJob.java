package com.singlee.capital.interfacex.quartzjob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.exception.RException;

public class ClearLogQuartzJob implements CronRunnable{
	private static int MAX_STR_LEN = 1024;
	/**
	 * 启动日志备份脚本	
	 */
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = null;
		try{
			String shpath = System.getenv().get("HOME")+"/user_projects/domains/ifbm/sh/logFileClear.sh";
			LogManager.getLogger(LogManager.MODEL_BATCH).info("shpath:::"+shpath);
			Process ps = Runtime.getRuntime().exec(shpath);
			ps.waitFor();
			br = new BufferedReader(new InputStreamReader(ps.getInputStream(),"UTF-8"));
			LogManager.getLogger(LogManager.MODEL_BATCH).info("br获取正常");
			StringBuffer sb = new StringBuffer();
			int intC;
			while ((intC = br.read()) != -1) {
				char c = (char) intC;
				if(sb.length() >= MAX_STR_LEN) {
					throw new RException("input too long");
				}
				sb.append(c);
			}
			LogManager.getLogger(LogManager.MODEL_BATCH).info("br获取全部正常");
			String result = sb.toString();
			LogManager.getLogger(LogManager.MODEL_BATCH).info("卸数程序调用日志："+result);
			if(br != null) {
                br.close();
            }
		}finally{
			try {
				if(br != null) {
                    br.close();
                }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new RException(e);
			}
		}
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
