package com.singlee.capital.interfacex.signterminate.service.impl;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.FbNoRec;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRs;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.signterminate.mapper.TiSignTerminateContractMapper;
import com.singlee.capital.interfacex.signterminate.model.SignAccount;
import com.singlee.capital.interfacex.signterminate.service.SignAndTerminateContactService;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Service
public class SignAndTerminateContactServiceImpl implements SignAndTerminateContactService{
	@Autowired
	TiSignTerminateContractMapper tiSignTerminateContractMapper;
	
	@Autowired
	private SocketClientService socketClientService;
	
	private final String fBIDString="210";
	
	private final String ftTxnType = "IFBM0004";
	
	private final String signIn = "1";//签约
	
	private final String signOff = "2";//解约
	
	private final String t24Success = "1";
	//核心签约
	@Override
	public TCoSignonMultAaaRs signContract(Map<String, Object> map) throws Exception{
		System.out.println((String)map.get("accountNumber"));
		SignAccount signAccount = tiSignTerminateContractMapper.selectContractByAccount((String)map.get("accountNumber"));
		if(signAccount!=null && signAccount.getSingType()==signIn){
			throw new RuntimeException("该账户已经签约，请勿重复签约。");
		}
		//发送签约信息
		TCoSignonMultAaaRq tCoSignonMultAaaRq = new TCoSignonMultAaaRq();
		tCoSignonMultAaaRq.setCommonRqHdr(generateCommonRqHdr());
		tCoSignonMultAaaRq.setFBID(fBIDString);
		tCoSignonMultAaaRq.setFtTxnType(ftTxnType);
		tCoSignonMultAaaRq.setMediumType((String)map.get("accountType"));
		tCoSignonMultAaaRq.setMediumAccNo((String)map.get("accountNumber"));
		FbNoRec fbNoRec = new FbNoRec();
		fbNoRec.setFbNo("210");
		tCoSignonMultAaaRq.getFbNoRec().add(fbNoRec);
		
		TCoSignonMultAaaRs responseAaaRs = socketClientService.t24TCoSignonMultAaaRequest(tCoSignonMultAaaRq);
		//数据库处理
		if(responseAaaRs.getFbNoStatus()!=null && responseAaaRs.getFbNoStatus().equals(t24Success)){
			if(signAccount!=null){
				signAccount.setSingType(signIn);
				signAccount.settOperationTime("");
				signAccount.settSponsor("");
				signAccount.settSponInstitution("");
				signAccount.setOpTime(DateTimeUtil.getLocalDateTime());
				signAccount.setSponsor(SlSessionHelper.getUserId());
				signAccount.setSponsorInstitution(SlSessionHelper.getInstitutionType());
				signAccount.setAccountID((String)map.get("accountID"));
				signAccount.setAccountType((String)map.get("accountType"));
				signAccount.setAccountNumber((String)map.get("accountNumber"));
				tiSignTerminateContractMapper.updateContract(signAccount);
			}
			else{
				signAccount = new SignAccount();
				signAccount.setAccountNumber((String)map.get("accountNumber"));
				signAccount.setAccountType((String)map.get("accountType"));
				signAccount.setSponsor(SlSessionHelper.getUserId());
				signAccount.setSponsorInstitution(SlSessionHelper.getInstitutionType());
				signAccount.setOpTime(DateTimeUtil.getLocalDateTime());
				signAccount.setSingType(signIn);
				tiSignTerminateContractMapper.insertContractInfo(signAccount);
			}
			return responseAaaRs;
			
		}else {
            return responseAaaRs==null? null:responseAaaRs;
        }
		
	}

	@Override
	public TCoSignoffMultAaaRs terminateContract(Map<String, Object> map)  throws Exception{
		// TODO Auto-generated method stub
		if(map.get("accountNumber")==null){
			JY.raiseRException("解约账号为空!");
			return null;
		}
		//发送解约信息
		TCoSignoffMultAaaRq tCoSignoffMultAaaRq = new TCoSignoffMultAaaRq();
		tCoSignoffMultAaaRq.setCommonRqHdr(generateCommonRqHdr());
		tCoSignoffMultAaaRq.setFBID(fBIDString);
		tCoSignoffMultAaaRq.setFtTxnType(ftTxnType);
		tCoSignoffMultAaaRq.setMediumType((String)map.get("accountType"));
		tCoSignoffMultAaaRq.setMediumAccNo((String)map.get("accountNumber"));
		FbNoRec fbNoRec = new FbNoRec();
		fbNoRec.setFbNo("210");
		tCoSignoffMultAaaRq.getFbNoRec().add(fbNoRec);
		
		TCoSignoffMultAaaRs responseAaaRs = socketClientService.t24TCoSignoffMultAaaRequest(tCoSignoffMultAaaRq);
		if(responseAaaRs!=null && responseAaaRs.getFbNoStatus().equals(t24Success)){
			SignAccount signAccount = new SignAccount();
			signAccount.setAccountNumber(responseAaaRs.getAcctNo());
			signAccount.setAccountType(responseAaaRs.getMediumType());
			signAccount.settOperationTime(DateTimeUtil.getLocalDateTime());
			signAccount.settSponInstitution(SlSessionHelper.getInstitutionType());
			signAccount.settSponsor(SlSessionHelper.getUserId());
			signAccount.setSingType(signOff);
			signAccount.setOpTime((String)map.get("opTime"));
			signAccount.setSponsor((String)map.get("sponsor"));
			signAccount.setSponsorInstitution((String)map.get("sponsorInstitution"));
			signAccount.setAccountID((String)map.get("accountID"));
//			signAccount.setAccountNumber((String)map.get("accountNumber"));
//			signAccount.setAccountType((String)map.get("accountType"));
			
			tiSignTerminateContractMapper.updateContract(signAccount);
			return responseAaaRs;
		}
		
		return null;
				
		
		
		
	}

	@Override
	public Page<SignAccount> selectContract(Map<String, Object> map) throws Exception{
		return tiSignTerminateContractMapper.selectContractInfo(map, ParameterUtil.getRowBounds(map));
	}
	
	private CommonRqHdr generateCommonRqHdr(){
		CommonRqHdr commonRqHdr = new CommonRqHdr();
		commonRqHdr.setChannelId("S89");
		commonRqHdr.setRqUID(UUID.randomUUID().toString());
		String s1=DateUtil.getCurrentDateAsString().replace("-", "");
		String s2=DateTimeUtil.getLocalTime().replace(":", "");
		commonRqHdr.setTranDate(s1);
		commonRqHdr.setTranTime(s2);
		return commonRqHdr;
	}
	
	
}
