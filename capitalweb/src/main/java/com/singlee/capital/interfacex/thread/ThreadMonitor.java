package com.singlee.capital.interfacex.thread;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class ThreadMonitor extends Thread {
	
	@Autowired
	MqAccountiingThreadHandler mqAccountiingThreadHandler;
	
	private List<Thread> monitoredThread;//需要被监控的线程
	
	public List<Thread> getMonitoredThread() {
		return monitoredThread;
	}

	public void setMonitoredThread(List<Thread> monitoredThread) {
		this.monitoredThread = monitoredThread;
	}

	public ThreadMonitor()
	{
		
	}
	
	public ThreadMonitor(List<Thread> monitoredThread) {
        this.monitoredThread = monitoredThread;
    }

    @Override
    public void run() {
        while (true) {
        	monitor();
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                // TODO 记日志
                e.printStackTrace();
            }
        }

    }
    
    /**
     * 监控主要逻辑
     */
    private void monitor() {
        for (int i = 0; i < monitoredThread.size(); i++) {
        	try{
	            Thread.State state = monitoredThread.get(i).getState(); // 获得指定线程状态
	            /**
	             * 根据线程ID 获取数据库DB配置的线程类及启用状态
	             * 判定当前的现场状态是否是TERMINATED 还是其他状态
	             * 根据启用状态动态改变线程执行
	             * 如若是启用状态，但线程是 TERMINATED 那么重启线程
	             * 如若是非启用状态，线程活动中，则需要monitoredThread.get(i).interrupt();
	             */
	            if (Thread.State.TERMINATED.equals(state)) {
	            	String threadNameString = monitoredThread.get(i).getName();
	                System.out.println(threadNameString+ "监控线程已经终止,现在开始重启监控线程!");
	                monitoredThread.remove(i);
	                Thread ownerThread = new Thread(mqAccountiingThreadHandler,threadNameString.substring(0,4)+ new SimpleDateFormat("yyyyMMdd24HHmmss").format(new Date()));
	                ownerThread.start();
	                monitoredThread.add(ownerThread);
	            }
        	}catch (Exception e) {
				// TODO: handle exception
        		e.printStackTrace();
			}
        }
    }
    private Calendar calendar = new java.util.GregorianCalendar();
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("kk:mm");
    /**
	 * ]判断是否是工作时间
	 * @return
	 * @throws Exception 
	 */
	public  boolean isWorkTime(String dayBeginTime,String dayEndTime) throws Exception
	{
		if (calendar.getTime().after(simpleDateFormat.parse(dayBeginTime)) && simpleDateFormat.parse(dayEndTime).before(calendar.getTime())) {
		
			return true;
		}else
		{
			return false;
		}
		
	}
	
}
