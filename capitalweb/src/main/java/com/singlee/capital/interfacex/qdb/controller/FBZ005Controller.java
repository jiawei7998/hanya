package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.pojo.FBZ005;
import com.singlee.capital.interfacex.qdb.service.FBZService;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.controller.CommonController;
//查询客户额度信息
@Controller
@RequestMapping("fbz005")
public class FBZ005Controller extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private EsbClientService esbClientService;
	@Resource
	private FBZService fbzService;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping("getFbz005")
	@ResponseBody
	public RetMsg<Map<String, Object>> getResp(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ005 fbz) throws Exception{
		fbz.setMfCustomerId((String)map.get("cifNo"));
		fbz.setShowNum(String.valueOf(map.get("showNum") == null ? "10" : map.get("showNum")));
		fbz.setStartNum(String.valueOf(1+((Integer)map.get("startNum")-1) * Integer.valueOf(fbz.getShowNum())));
		 Map<String, Object> map_1 = sendReqByPage(request,fbz);
		FBZ005 temp = null;
		if(map_1.get("rows") != null)
		{
			PropertiesConfiguration configuration = PropertiesUtil.parseFile("currency.properties");
			List<FBZ005> fbzs = (List<FBZ005>)map_1.get("rows");
			if(fbzs.size() == 1)
			{
				temp = (FBZ005)fbzs.get(0);
				if(!"15700000000000".equals(temp.getRespCde()))
				{
					fbzs.remove(0);
					return RetMsgHelper.ok(map_1);
				}
			}
    		for (int i=0;i<fbzs.size();i++) 
    		{
    			temp = (FBZ005)fbzs.get(i);
    			temp.setCurrency(configuration.getString(((FBZ005)fbzs.get(i)).getCurrency()));
    			temp.setPutoutSum(temp.getPutoutSum() != null ? temp.getPutoutSum().replaceAll(",", "") : "0");
			}
    	}
		return RetMsgHelper.ok(map_1);
	}
	
	@RequestMapping("queryLimitNo")
	@ResponseBody
	public RetMsg<List<Object>> queryLimitNo(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ005 fbz) throws Exception{
		fbz.setMfCustomerId((String)map.get("cifNo"));
		fbz.setShowNum(String.valueOf(map.get("showNum") == null ? "30" : map.get("showNum")));
		fbz.setStartNum(String.valueOf(1+((Integer)map.get("start")-1)*10));
		List<Object> fbzs = sendReq(request,fbz);
		FBZ005 temp = null;
		if(fbzs!=null)
		{
			if(fbzs.size() == 1)
			{
				temp = (FBZ005)fbzs.get(0);
				if(!"15700000000000".equals(temp.getRespCde()))
				{
					fbzs.remove(0);
					return RetMsgHelper.ok(fbzs);
				}
			}
			
			FBZ005 bean = null;
			List<Object> list = new ArrayList<Object>();
			Map<String, Object> map_1 = new HashMap<String, Object>();
    		for (int i=0;i < fbzs.size();i++) 
    		{
    			temp = (FBZ005)fbzs.get(i);
    			if(map_1.get(temp.getLmtID()) == null){
    				bean = new FBZ005();
        			bean.setId(temp.getLmtID());
        			bean.setText(temp.getLmtID());
        			map_1.put(bean.getId(), bean);
    			}
			}
    		list.addAll(map_1.values());
    		return RetMsgHelper.ok(list);
    		
    	}
		return RetMsgHelper.ok(fbzs);
	}
	
	@RequestMapping("ifExitDeal")
	@ResponseBody
	public boolean ifExitDeal(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ003 fbz) throws Exception{
		map.put("flag", "0");
		List<FBZ003> fbzs=fbzService.getFbzBySeriaNo(map);
		if(fbzs!=null&&fbzs.size()>0){
			if(fbzs.get(0).getDealNo().equals((String)map.get("dealNo"))){
				return false;
			}
			return true;
		}
		return false;
	}
	public List<Object> sendReq(HttpServletRequest request,FBZ005 fbz) throws Exception{
		List<Object> fbzs=new ArrayList<Object>();
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ005.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ005返回报文"+xmlDocAccept.asXML());
		        	fbzs = XmlUtilFactory.getBeansByXml(FBZ005.class, xmlDocAccept.asXML(),"circularField");		        	
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return fbzs;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return null;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> sendReqByPage(HttpServletRequest request,FBZ005 fbz) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> fbzs=new ArrayList<Object>();
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ005.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ005返回报文"+xmlDocAccept.asXML());
		        	fbzs = XmlUtilFactory.getBeansByXml(FBZ005.class, xmlDocAccept.asXML(),"circularField");	
		        	
		        	String totalNum = null;
		        	Element element = xmlDocAccept.getRootElement().element("appBody");
					List<Element> eles = element.elements("totalNum");
					if(eles != null){
						totalNum = eles.get(0).getText();
					}
					
					map.put("total", totalNum);
					map.put("rows", fbzs);
		        	
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return map;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return null;
		}
		return null;
	}

	public String parseXml(HttpServletRequest request,FBZ005 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		svc.put("bizDate",getDate());
		svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
		svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
		svc.put("tmStamp", getDateTime());
		hashmap.put("appHdr", svc);
		body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
		body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
		body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
		body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
		body.put("startNum", fbz.getStartNum()!=null?fbz.getStartNum():"");
		body.put("showNum",fbz.getShowNum());
		hashmap.put("appBody", body);
		return XmlUtilFactory.getXml(request.getSession().getServletContext().getRealPath("/")+ "/WEB-INF/classes/schema/FBZC0005.xml", hashmap);
	}
	public String getDateTime(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
}
