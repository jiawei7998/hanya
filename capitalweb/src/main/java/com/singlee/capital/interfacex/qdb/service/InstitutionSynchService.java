package com.singlee.capital.interfacex.qdb.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.system.model.TtInstitution;

public interface InstitutionSynchService {

	void deleteESB();

	void insertESB(List<TtInstitution> insts);

	void updateInstPId(Map<String, String> rels);
	
	void updateUserInstMap(Map<String, String> rels);
	void insertUserInstMap(Map<String, String> rels);

}
