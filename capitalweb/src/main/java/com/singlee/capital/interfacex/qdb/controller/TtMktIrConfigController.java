package com.singlee.capital.interfacex.qdb.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.TtMktIrConfig;
import com.singlee.capital.interfacex.qdb.service.TtMktIrConfigService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value ="TtMktIrConfigController")
public class TtMktIrConfigController extends CommonController{
	@Autowired
	private TtMktIrConfigService configService;
	
	/**
	 * 基准利率分页
	 * 
	 * @param map
	 * @return 对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtMktIrConfigPage")
	public RetMsg<PageInfo<TtMktIrConfig>> selectTtMktIrConfigPage(@RequestBody Map<String,Object> params) {
		Page<TtMktIrConfig> page = configService.selectTtMktIrConfigPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 添加基准利率信息
	 * @param ttMktIrConfig
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtMktIrConfig")
	public RetMsg<Serializable> insertTtMktIrConfig(@RequestBody TtMktIrConfig ttMktIrConfig) {
		configService.insertTtMktIrConfig(ttMktIrConfig);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改基准利率信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTtMktIrConfig")
	public RetMsg<Serializable> updateTtMktIrConfig(@RequestBody Map<String, Object> map) {
		configService.updateTtMktIrConfig(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除基准利率信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/daleteTtMktIrConfig")
	public RetMsg<Serializable> daleteTtMktIrConfig(@RequestBody Map<String, Object> map) {
		configService.daleteTtMktIrConfig(map);
		return RetMsgHelper.ok();
	}
}
