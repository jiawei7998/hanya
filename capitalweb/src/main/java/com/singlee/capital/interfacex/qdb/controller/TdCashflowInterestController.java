package com.singlee.capital.interfacex.qdb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.TdCashflowFeeMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdFeesMapper;
import com.singlee.capital.interfacex.qdb.service.TdCashflowInterestService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TdCashflowInterestController")
public class TdCashflowInterestController extends CommonController{
	
	@Autowired
	private TdCashflowInterestService tdCashflowInterestService;
	@Autowired
	private TdCashflowFeeMapper cashflowFeeMapper;
	@Autowired
	private TdTrdFeesMapper tdTrdFeesMapper;
	/**
	 * 查询收息计划确认信息
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCfComfirm")
	public RetMsg<PageInfo<TdCashflowInterest>> searchPageCfComfirm(@RequestBody Map<String,Object> params) throws RException {
		Page<TdCashflowInterest> page = tdCashflowInterestService.pageCfComfirm(params);
		return RetMsgHelper.ok(page);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/saveCashFlowConfirm")
	@ResponseBody
	public RetMsg saveCashFlowConfirm(@RequestBody Map<String,Object> params){
		boolean f=false;
		try{
			// 资金账户类型
			String ids = ParameterUtil.getString(params, "cashflowId", "");
			@SuppressWarnings("unchecked")
			List<TdCashflowInterest> list = new ArrayList();
			String[] strArr = ids.split(","); 
			for(String cashflowId : strArr){
				TdCashflowInterest tcf = new TdCashflowInterest();
				tcf.setCashflowId(cashflowId);
				list.add(tcf);
			}
			f=tdCashflowInterestService.updateConfirmFlag(list);
		}catch(Exception e){
			e.printStackTrace();
		}
		return RetMsgHelper.ok(f);
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageFee")
	public RetMsg<List<CashflowInterest>> searchPageFee(@RequestBody Map<String,Object> params) throws RException {
		List<CashflowInterest> page = cashflowFeeMapper.getInterestList(params);		
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTdTrdFees")
	public RetMsg<List<CashflowInterest>> searchPageTdTrdFees(@RequestBody Map<String,Object> params) throws RException {
		List<CashflowInterest> page = tdTrdFeesMapper.getInterestList(params);		
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchFeeByPage")
	public RetMsg<PageInfo<CashflowInterest>> searchFeeByPage(@RequestBody Map<String,Object> params) throws RException {
		Page<CashflowInterest> page = cashflowFeeMapper.getInterestList(params,ParameterUtil.getRowBounds(params));		
		return RetMsgHelper.ok(page);
	}
}
