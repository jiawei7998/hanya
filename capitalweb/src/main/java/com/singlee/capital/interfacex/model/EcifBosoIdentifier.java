package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="BOS_O_IDENTIFIER")
public class EcifBosoIdentifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String identifier_id    				;
	private String ecif_id                  ;
	private String id_tp_cd                 ;
	private String open_acct_flag           ;
	private String id_num                   ;
	private String id_issued_dt             ;
	private String id_expiry_dt             ;
	private String id_issued_depart         ;
	private String id_verified_dt           ;
	private String identifier_description   ;
	private String t24idc_tp_cd             ;
	private String source_sys_tp_cd         ;
	private String effective_flag           ;
	private String last_update_dt           ;
	private String last_update_tx_id        ;
	private String last_update_user         ;
	private String oi_reserved_1            ;
	private String oi_reserved_2            ;
	private String oi_reserved_3            ;
	private String oi_reserved_4            ;
	private String oi_reserved_5            ;
	public String getIdentifier_id() {
		return identifier_id;
	}
	public void setIdentifier_id(String identifierId) {
		identifier_id = identifierId;
	}
	public String getEcif_id() {
		return ecif_id;
	}
	public void setEcif_id(String ecifId) {
		ecif_id = ecifId;
	}
	public String getId_tp_cd() {
		return id_tp_cd;
	}
	public void setId_tp_cd(String idTpCd) {
		id_tp_cd = idTpCd;
	}
	public String getOpen_acct_flag() {
		return open_acct_flag;
	}
	public void setOpen_acct_flag(String openAcctFlag) {
		open_acct_flag = openAcctFlag;
	}
	public String getId_num() {
		return id_num;
	}
	public void setId_num(String idNum) {
		id_num = idNum;
	}
	public String getId_issued_dt() {
		return id_issued_dt;
	}
	public void setId_issued_dt(String idIssuedDt) {
		id_issued_dt = idIssuedDt;
	}
	public String getId_expiry_dt() {
		return id_expiry_dt;
	}
	public void setId_expiry_dt(String idExpiryDt) {
		id_expiry_dt = idExpiryDt;
	}
	public String getId_issued_depart() {
		return id_issued_depart;
	}
	public void setId_issued_depart(String idIssuedDepart) {
		id_issued_depart = idIssuedDepart;
	}
	public String getId_verified_dt() {
		return id_verified_dt;
	}
	public void setId_verified_dt(String idVerifiedDt) {
		id_verified_dt = idVerifiedDt;
	}
	public String getIdentifier_description() {
		return identifier_description;
	}
	public void setIdentifier_description(String identifierDescription) {
		identifier_description = identifierDescription;
	}
	public String getT24idc_tp_cd() {
		return t24idc_tp_cd;
	}
	public void setT24idc_tp_cd(String t24idcTpCd) {
		t24idc_tp_cd = t24idcTpCd;
	}
	public String getSource_sys_tp_cd() {
		return source_sys_tp_cd;
	}
	public void setSource_sys_tp_cd(String sourceSysTpCd) {
		source_sys_tp_cd = sourceSysTpCd;
	}
	public String getEffective_flag() {
		return effective_flag;
	}
	public void setEffective_flag(String effectiveFlag) {
		effective_flag = effectiveFlag;
	}
	public String getLast_update_dt() {
		return last_update_dt;
	}
	public void setLast_update_dt(String lastUpdateDt) {
		last_update_dt = lastUpdateDt;
	}
	public String getLast_update_tx_id() {
		return last_update_tx_id;
	}
	public void setLast_update_tx_id(String lastUpdateTxId) {
		last_update_tx_id = lastUpdateTxId;
	}
	public String getLast_update_user() {
		return last_update_user;
	}
	public void setLast_update_user(String lastUpdateUser) {
		last_update_user = lastUpdateUser;
	}
	public String getOi_reserved_1() {
		return oi_reserved_1;
	}
	public void setOi_reserved_1(String oiReserved_1) {
		oi_reserved_1 = oiReserved_1;
	}
	public String getOi_reserved_2() {
		return oi_reserved_2;
	}
	public void setOi_reserved_2(String oiReserved_2) {
		oi_reserved_2 = oiReserved_2;
	}
	public String getOi_reserved_3() {
		return oi_reserved_3;
	}
	public void setOi_reserved_3(String oiReserved_3) {
		oi_reserved_3 = oiReserved_3;
	}
	public String getOi_reserved_4() {
		return oi_reserved_4;
	}
	public void setOi_reserved_4(String oiReserved_4) {
		oi_reserved_4 = oiReserved_4;
	}
	public String getOi_reserved_5() {
		return oi_reserved_5;
	}
	public void setOi_reserved_5(String oiReserved_5) {
		oi_reserved_5 = oiReserved_5;
	}
	
}
