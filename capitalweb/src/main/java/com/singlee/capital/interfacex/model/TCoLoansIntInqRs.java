package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TCoLoansIntInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TCoLoansIntInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="LoanRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrentRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCoLoansIntInqRs", propOrder = { "commonRsHdr", "tCoLoansIntInqRec" })
public class TCoLoansIntInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "TCoLoansIntInqRec")
	protected List<TCoLoansIntInqRec> tCoLoansIntInqRec;
	
	public List<TCoLoansIntInqRec> getTCoLoansIntInqRec() {
		if (tCoLoansIntInqRec == null) {
			tCoLoansIntInqRec = new ArrayList<TCoLoansIntInqRec>();
		}
		return this.tCoLoansIntInqRec;
	}

	public void setTCoLoansIntInqRec(List<TCoLoansIntInqRec> tCoLoansIntInqRec) {
		this.tCoLoansIntInqRec = tCoLoansIntInqRec;
	}

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

}
