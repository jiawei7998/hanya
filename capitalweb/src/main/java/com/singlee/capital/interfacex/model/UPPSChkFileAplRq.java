package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSChkFileAplRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSChkFileAplRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="UserDefineTranCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayPathCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClearDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSChkFileAplRq", propOrder = { "commonRqHdr",
		"userDefineTranCode", "channelId", "payPathCode", "clearDate",
		"remark1", "remark2", "remark3", "remark4", "remark5" })
public class UPPSChkFileAplRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "UserDefineTranCode", required = true)
	protected String userDefineTranCode;
	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "PayPathCode", required = true)
	protected String payPathCode;
	@XmlElement(name = "ClearDate", required = true)
	protected String clearDate;
	@XmlElement(name = "remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "remark_3", required = true)
	protected String remark3;
	@XmlElement(name = "remark_4", required = true)
	protected String remark4;
	@XmlElement(name = "remark_5", required = true)
	protected String remark5;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the userDefineTranCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserDefineTranCode() {
		return userDefineTranCode;
	}

	/**
	 * Sets the value of the userDefineTranCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserDefineTranCode(String value) {
		this.userDefineTranCode = value;
	}

	/**
	 * Gets the value of the channelId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the value of the channelId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelId(String value) {
		this.channelId = value;
	}

	/**
	 * Gets the value of the payPathCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayPathCode() {
		return payPathCode;
	}

	/**
	 * Sets the value of the payPathCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayPathCode(String value) {
		this.payPathCode = value;
	}

	/**
	 * Gets the value of the clearDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClearDate() {
		return clearDate;
	}

	/**
	 * Sets the value of the clearDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClearDate(String value) {
		this.clearDate = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the remark3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark3() {
		return remark3;
	}

	/**
	 * Sets the value of the remark3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark3(String value) {
		this.remark3 = value;
	}

	/**
	 * Gets the value of the remark4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark4() {
		return remark4;
	}

	/**
	 * Sets the value of the remark4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark4(String value) {
		this.remark4 = value;
	}

	/**
	 * Gets the value of the remark5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark5() {
		return remark5;
	}

	/**
	 * Sets the value of the remark5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark5(String value) {
		this.remark5 = value;
	}

}
