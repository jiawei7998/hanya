package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtStockPrice;


public interface StockPriceMapper extends Mapper<TtStockPrice> {

	/**
	 * 查询股票价格
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<TtStockPrice> pageStock(Map<String,Object> map, RowBounds row);
	
	
	/**
	 * 批量插入股票价格
	 * @param list
	 */
	public void insertStockExcel(List<TtStockPrice> list);
	
	/**
	 * 删除所以已存在的股票数据
	 */
	public void deleteStockAll();
	
	/**
	 * 更新股票价格信息
	 * @param tsp
	 */
	public void updateStock(TtStockPrice tsp);
	
	/**
	 * 增加股票价格信息
	 * @param tsp
	 */
	public void addStock(TtStockPrice tsp);
	
	/**
	 * 删除股票价格信息
	 * @param tsp
	 */
	public void deleteStock(TtStockPrice tsp);
	
	
	public TtStockPrice queryStockById(Map<String,Object> map);
	
}
