package com.singlee.capital.interfacex.qdb.ESB.util;

public class CnapsType {
	public static enum SysStatus
	{
		ZERO("0","等待签发"),
		PEND("PEND","中间状态"),
		STLL("STLL","已清算"),
		OPEN("OPEN","主机还在处理"),
		VOID("VOID","已作废"),
		SEND("SEND","核心前置已接收"),
		PR96("PR96","待发送"),
		PR97("PR97","已发送"),
		PR84("PR84","前置待授权"),
		PR12("PR12","清算排队"),
		PR55("PR55","已暂存"),
		PR09("PR09","已拒绝"),
		PR24("PR24","未受理"),
		PR04("PR04","已清算"),
		PR08("PR08","已撤销"),
		PR99("PR99","故障"),
		PR01("PR01","本地拒绝"),
		_01("_01","启运"),
		_02("_02","停运"),
		_03("_03","维护"),
		_00("_00","营业准备"),
		_10("_10","日间"),
		_20("_20","业务截止"),
		_30("_30","清算窗口"),
		_40("_40","日终处理"),
		_15("_15","日切");
		private String type;
		
		private String name;

		
		private SysStatus(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {
	        for (SysStatus c : SysStatus.values()) 
	        {   
	            if (c.getType().equalsIgnoreCase(type)) 
	            {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return this.type+":"+this.name;
		}
		
	}
	
	public static enum DealFlag
	{
		//0-待经办 1-已经办待复核 A-正在复核中 C-复核异常 F-已复核 B-退回 
		INIT("0","初始状态"),
		IOPER("1","已经办待复核"),
		VERIFY_APPEND("A","复核中"),
		VERIFY_ERR("C","复核异常"),
		VERIFY("F","已复核"),
		BACK("B","已退回");
		
		private String type;
		
		private String name;

		
		private DealFlag(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {
	        for (SysStatus c : SysStatus.values()) 
	        {   
	            if (c.getType().equalsIgnoreCase(type)) 
	            {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return this.type+":"+this.name;
		}
		
	}
}
