package com.singlee.capital.interfacex.qdb.rep.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValTradeBean;

public interface ReportDeValTradeMapper extends Mapper<ReportDeValTradeBean> {
	

		/**
		 * 
		 * @param mapR
		 * @param row
		 * @return
		 */
		public Page<ReportDeValTradeBean> listReportDeValTradeBean(Map<String,Object> map, RowBounds row);
		
		/**
		 * 
		 * @param mapR
		 * @param row
		 * @return
		 */
		public List<ReportDeValTradeBean> listReportDeValTradeBean(Map<String,Object> map);


}
