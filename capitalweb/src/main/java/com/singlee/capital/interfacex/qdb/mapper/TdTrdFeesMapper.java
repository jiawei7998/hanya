package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.interfacex.qdb.model.TdTrdFees;

public interface TdTrdFeesMapper extends Mapper<TdTrdFees>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowInterest> getInterestList(Map<String, Object> map);
	
	
	void deleteTdCashflowFeeList(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowFeeCopy(Map<String, Object> map);
	
	/**
	 * 查询出需要重新计算的收息区间 （还本计划调整审批）
	 * @param map
	 * @return
	 */
	List<TdCashflowInterest> getTdCashflowFeeForApprove(Map<String, Object> map);
	/**
	 * 删除未收息确认的计划数据（计划变更调整审批）
	 * @param map
	 */
	void deleteForApproveNotActual(Map<String, Object> map);
	
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
	void inserTFees(TdTrdFees fees);


	List<TdTrdFees> selectByDealNo(Map<String, Object> params);


}