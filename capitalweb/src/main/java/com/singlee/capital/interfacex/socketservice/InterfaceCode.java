package com.singlee.capital.interfacex.socketservice;

public class InterfaceCode {

	public static String TI_TExCurrencyAllInqRs = "TExCurrencyAllInqRs";//汇率信息查询
	public static String TI_TStBeaRepayAaaRs = "TStBeaRepayAaaRs"; //多借多贷核心记账
	public static String TI_TStFtNormRevRs = "TStFtNormRevRs";   //普通转账冲正
	public static String TI_TStFtRemitAaaRs = "TStFtRemitAaaRs";  //国际业务汇出申请
	public static String TI_TDpAcStmtInqRs = "TDpAcStmtInqRs";   //交易明细查询接口
	public static String TI_TExReconAllInqRs = "TExReconAllInqRs"; //对账查询接口
	public static String TI_TDpAcctTypeInqRs = "TDpAcctTypeInqRs"; //账户介质类型查询
	public static String TI_TDpCorpacctAllAaaRs = "TDpCorpacctAllAaaRs"; //对公新开账户
	public static String TI_TCoSignonMultAaaRs = "TCoSignonMultAaaRs";  //客户解约接口
	public static String TI_TCoSignoffMultAaaRs = "TCoSignoffMultAaaRs"; //客户签约接口
	public static String TI_TCoHolidayAllInqRs = "TCoHolidayAllInqRs";//节假日查询
	public static String TI_UPPSCdtTrfRs = "UPPSCdtTrfRs";       //二代支付-普通贷记往账接口
	public static String TI_UPPSigBusiQueryRs = "UPPSigBusiQueryRs";  //二代支付-自助渠道业务状态查询接口
	public static String TI_UPPSChkFileAplRs = "UPPSChkFileAplRs";   //二代支付-对账文件申请
	public static String TI_CloudProApplyTransRs = "CloudProApplyTransRs";  //产品信息管理--产品组申请交易
	public static String TI_CloudProSynchTransRs = "CloudProSynchTransRs";  //产品信息管理-产品组同步交易
	public static String TI_CloudProModTransRs = "CloudProModTransRs";   //产品信息管理-产品组修改交易
	public static String TI_C167CoCorpCusInqRs = "C167CoCorpCusInqRs";   // 按证件号或客户号查询对公客户信息
	public static String TI_TCustDetInqRs = "TCustDetInqRs";     //同业客户详细查询
	public static String TI_TDpAcActModRs = "TDpAcActModRs";  //利率维护及新增 TDpAcActMod
	public static String TI_TDpAcctNewtdaAaaRs = "TDpAcctNewtdaAaaRs";  //对公活期转定期 TDpAcctNewtdaAaaRs
	public static String TI_ApprovalInqRs = "ApprovalInqRs";   // CRMS-批复信息查询（同业->CRMS)
	public static String TI_ApprovalChangeRs = "ApprovalChangeRs";   //IFBM-批复信息变更（CRMS->同业）
	public static String TI_LoanNoticeRs = "LoanNoticeRs"; //IFBM-放款通知（CRMS->同业）
	public static String TI_QuotaSyncopateRs = "QuotaSyncopateRs";  //IFBM-额度切分同步（CRMS->同业系统）
	public static String TI_RepayNoticeRs = "RepayNoticeRs";  //IFBM-提前还款通知（CRMS->系统）
	public static String TI_FinCreditInqRs = "FinCreditInqRs"; //CRMS-查询融贷通业务信息(同业->CRMS)
	public static String TI_FinCreditApproveRs = "FinCreditApproveRs"; //CRMS-融贷通业务审批(同业->CRMS)
	public static String TI_EnterpriseInqRs = "EnterpriseInqRs"; //CRMS-查询企信业务信息(同业->CRMS)
	public static String TI_EnterpriseApproveRs = "EnterpriseApproveRs"; //CRMS-企信业务审批(同业->CRMS)
	public static String TI_TExLoanAllInqRs = "TExLoanAllInqRs"; //T24贷款信息查询
	public static String TI_TExLdRpmSchInqRs = "TExLdRpmSchInqRs"; //T24还款计划查询
	public static String TI_TDpIntBnkAaaRs = "TDpIntBnkAaaRs"; //T24约定交易接口
	public static String TI_TCoActIntInqRs = "TCoActIntInqRs"; //T24存款利率信息查询接口
	public static String TI_TCoLoansIntInqRs = "TCoLoansIntInqRs"; //T24贷款利率信息查询接口
	
	
	public static String TI_CHANNELNO = "S89";
	public static String TI2ND_QueryUserDefineTranCode = "upbs.ipw.c050.01";
	public static String TI2ND_UPPSCdtTrf = "UPPSCdtTrf";
	public static String TI2ND_UPPSChkFileApl = "UPPSChkFileApl";
	public static String TI2ND_PayPathCode1001 = "1001";//大额支付
	public static String TI2ND_PayPathCode1002 = "1002";//小额支付
	public static String TI2ND_PayPathCode1003 = "1003";//超级网银
	public static String TI2ND_PayPathCode1004 = "1004";//上海同城
	public static String TI2ND_PayPathCode1005 = "1005";//同城转汇
	public static String TI2ND_PayPathCode1012 = "1012";//城商行
	public static String TI2ND_PosEntryMode0 = "0";  //普通汇款
	public static String TI2ND_PosEntryMode1 = "1";  //实时汇款
	public static String TI2ND_BusiTypeA100 = "A100";   //普通汇兑
	public static String TI2ND_BusiTypeA108 = "A108";	//现金汇款
	public static String TI2ND_BusiKind02101 = "02101"; //现金汇款
	public static String TI2ND_BusiKind02102 = "02102"; //普通汇兑
	
	
	
	public static String TI_FBID = "210";
	public static String TI_IFBM0001 = "IFBM0001";
	public static String TI_IFBM0002 = "IFBM0002";
	public static String TI_IFBM0003 = "IFBM0003";
	public static String TI_IFBM0004 = "IFBM0004";
	public static String TI_IFBM0005 = "IFBM0005";
	public static String TI_IFBM0006 = "IFBM0006";
	public static String TI_IFBM0007 = "IFBM0007";
	public static String TI_IFBM0008 = "IFBM0008";
	public static String TI_IFBM0009 = "IFBM0009";
	public static String TI_IFBM0010 = "IFBM0010";
	
	/**
	 *  000001-额度已到期
		000002-额度不足
		000003-额度信息未维护
		000004-额度重算中
		999999-处理异常，看返回信息
	 */
	public static String TI_SUCCESS = "000000";
	public static String TI_SUCMSG  = "SUCCESS";
	public static String TI_FAILURE = "999999";
	public static String TI_STATUS1 = "000001";
	public static String TI_STATUS2 = "000002";
	public static String TI_STATUS3 = "000003";
	public static String TI_STATUS4 = "000004";
	/**
	 * 100000-此接口已关闭
	 */
	public static String TI_STATUS11 = "100000";
	public static String TI_MESSAGE11 = "此接口已关闭";
	public static String TI_STATUS21 = "200001";
	public static String TI_MESSAGE21 = "标识文件与实际文件行数不相等！";
	
	public static String TI_PAYPATHCODE1 = "1001";  //对账文件申请汇路编码  1001-大额支付
	public static String TI_PAYPATHCODE2 = "1002";  //对账文件申请汇路编码  1002-小额支付
	public static String TI_PAYPATHCODE3 = "1003";  //对账文件申请汇路编码  1003-超级网银
	public static String TI_PAYPATHCODE4 = "1004";  //对账文件申请汇路编码  1004-上海同城（不含转汇）
	public static String TI_PAYPATHCODE5 = "1005";  //对账文件申请汇路编码  1005-上海同城（含转汇）
	public static String TI_PAYPATHCODE6 = "1012";  //对账文件申请汇路编码  1012-城商行
	
	
	public static String TI_DEALCODE1 = "00";   //处理状态  00-已受理
	public static String TI_DEALCODE2 = "01";   //处理状态  01-节假日不对账
	public static String TI_DEALCODE3 = "02";   //处理状态  02-人行不对账
	public static String TI_DEALCODE4 = "03";   //处理状态  03-人行正在对账中
	
	public static String TI_UserDefineTranCode = "UPPSChkFileApl";
	
	
	public static String TI_JOB_STATUS_START="0";  //批量执行状态
	public static String TI_JOB_STATUS_END="1";
	public static String TI_JOB_RECORD_SUCCESS="1";  //记录状态
	public static String TI_JOB_RECORD_ERROR="0";
	public static String TI_JOB_TERM_TYPE1="m";//批量执行时间单位
	
}
