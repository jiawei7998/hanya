package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSigBusiQueryRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSigBusiQueryRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NowPageNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotalPageNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PageRowNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UPPSigBusiQueryRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}UPPSigBusiQueryRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSigBusiQueryRs", propOrder = { "commonRsHdr",
		"totalRowNum", "nowPageNum", "totalPageNum", "pageRowNum",
		"uppSigBusiQueryRec" })
public class UPPSigBusiQueryRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "TotalRowNum", required = true)
	protected String totalRowNum;
	@XmlElement(name = "NowPageNum", required = true)
	protected String nowPageNum;
	@XmlElement(name = "TotalPageNum", required = true)
	protected String totalPageNum;
	@XmlElement(name = "PageRowNum", required = true)
	protected String pageRowNum;
	@XmlElement(name = "UPPSigBusiQueryRec")
	protected List<UPPSigBusiQueryRec> uppSigBusiQueryRec;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the totalRowNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotalRowNum() {
		return totalRowNum;
	}

	/**
	 * Sets the value of the totalRowNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotalRowNum(String value) {
		this.totalRowNum = value;
	}

	/**
	 * Gets the value of the nowPageNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNowPageNum() {
		return nowPageNum;
	}

	/**
	 * Sets the value of the nowPageNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNowPageNum(String value) {
		this.nowPageNum = value;
	}

	/**
	 * Gets the value of the totalPageNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotalPageNum() {
		return totalPageNum;
	}

	/**
	 * Sets the value of the totalPageNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotalPageNum(String value) {
		this.totalPageNum = value;
	}

	/**
	 * Gets the value of the pageRowNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPageRowNum() {
		return pageRowNum;
	}

	/**
	 * Sets the value of the pageRowNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPageRowNum(String value) {
		this.pageRowNum = value;
	}

	/**
	 * Gets the value of the uppSigBusiQueryRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the uppSigBusiQueryRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getUPPSigBusiQueryRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link UPPSigBusiQueryRec }
	 * 
	 * 
	 */
	public List<UPPSigBusiQueryRec> getUPPSigBusiQueryRec() {
		if (uppSigBusiQueryRec == null) {
			uppSigBusiQueryRec = new ArrayList<UPPSigBusiQueryRec>();
		}
		return this.uppSigBusiQueryRec;
	}

}
