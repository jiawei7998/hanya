package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.EcifBosOrg;
import com.singlee.capital.interfacex.model.EcifBosoAddress;
import com.singlee.capital.interfacex.model.EcifBosoCustomerManager;
import com.singlee.capital.interfacex.model.EcifBosoIdentifier;
import com.singlee.capital.users.model.TdCustEcif;

public interface TiEcifBosOrgMapper extends Mapper<EcifBosOrg> {

	void insertEcifBosoIdentifier(EcifBosoIdentifier ecifBosoIdentifier) throws Exception;
	
	void insertEcifBosoAddress(EcifBosoAddress ecifBosoAddress) throws Exception;
	
	void insertEcifBosoCustomerManager(EcifBosoCustomerManager ecifBosoCustomerManager) throws Exception;
	
	public List<TdCustEcif> queryEcifCustomerInfo(Map<String, Object> map) throws Exception;
	
	void deleteEcifBosoAddress(Map<String, Object> map) throws Exception;
	void deleteEcifBosoCustomerManager(Map<String, Object> map) throws Exception;
	void deleteEcifBosoIdentifier(Map<String, Object> map) throws Exception;
	void deleteEcifBosOrg(Map<String, Object> map) throws Exception;
	
	public int queryEcifBosoAddress(Map<String, Object> map) throws Exception;
	public int queryEcifBosoCustomerManage(Map<String, Object> map) throws Exception;
	public int queryEcifBosoIdentifier(Map<String, Object> map) throws Exception;
	public int queryEcifBosOrg(Map<String, Object> map) throws Exception;
	
	public int updateEcifBosoAddress(EcifBosoAddress ecifBosoAddress);
	public int updateEcifBosoCustomerManage(EcifBosoCustomerManager ecifBosoCustomerManager);
	public int updateEcifBosoIdentifier(EcifBosoIdentifier ecifBosoIdentifier);
	public int updateEcifBosOrg(EcifBosOrg ecifBosOrg);
}
