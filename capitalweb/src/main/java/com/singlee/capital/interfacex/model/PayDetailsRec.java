package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PayDetailsRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PayDetailsRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentDetails" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayDetailsRec", propOrder = { "paymentDetails" })
public class PayDetailsRec {

	@XmlElement(name = "PaymentDetails", required = true)
	protected String paymentDetails;

	/**
	 * Gets the value of the paymentDetails property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentDetails() {
		return paymentDetails;
	}

	/**
	 * Sets the value of the paymentDetails property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentDetails(String value) {
		this.paymentDetails = value;
	}

}
