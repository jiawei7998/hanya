package com.singlee.capital.interfacex.qdb.ESB.service;

import java.util.HashMap;
public interface EsbClientService {
	
	public HashMap<String, Object> sendXmlToRequest(String url,String xmlString) throws Exception;
}
