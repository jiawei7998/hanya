package com.singlee.capital.interfacex.qdb.rep.model;

import java.io.Serializable;

public class ReportSecBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode          ; //债券代号                     
    
	private String productName          ; //债券名称                     
	                                                  
	private String cNo                  ; //CIF号                        
	                                                  
	private String custAccty            ; //授信客户属性(代码)           
	                                               
	private String custAcctyName        ; //客户授信属性中文名           
	                             
	private String port                 ; //投资组合                     
	                             
	private String aDate               ; //日期                         
	                            
	private String prdName              ; //业务种类                     
	                             
	private String type1                ; //业务种类1                    
	                            
	private String accountType          ; //账户类别                     
	                              
	private String accThreeSubject              ; //资产分类                     
	                            
	private String rsrAmt               ; //风险缓释                     
	                           
	private double amt                  ; //面值                         
	                             
	private double gyamt                ; //公允价值                     
	                             
	private double tyamt                ; //摊余成本                     
	                              
	private double mInt                 ; //应计利息                     
	                           
	private String rateType             ; //计息方式                     
	                            
	private double contractRate         ; //利率                         
	                             
	private String vDate                ; //起息日                       
	                             
	private String mDate                ; //到期日                       
	                            
	private String rateCode             ; //利率浮动代码                 
	                             
	private String irTerm               ; //利率浮动频率                 
	                            
	private String custType             ; //客户类型                     
	                            
	private String partyName            ; //客户名称中文                 
	                            
	private String partyFinanceLicense  ; //客户证件编号                 
	                            
	private String partyId              ; //授信客户                     
	                            
	private String partyOrganGrade      ; //债券评级 
	
	private String cost                 ; //成本中心
	
	private String last_update                 ; //更新日期
	
	private String acc_three_subject    ; //会计三分类（这个字段不用）
	
	private String acctSettlType; //结算类型
	
	private String invType; //新会计三分类
	


	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getAcctSettlType() {
		return acctSettlType;
	}

	public void setAcctSettlType(String acctSettlType) {
		this.acctSettlType = acctSettlType;
	}

	public String getAcc_three_subject() {
		return acc_three_subject;
	}

	public void setAcc_three_subject(String acc_three_subject) {
		this.acc_three_subject = acc_three_subject;
	}

	public String getLast_update() {
		return last_update;
	}

	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public String getCustAccty() {
		return custAccty;
	}

	public void setCustAccty(String custAccty) {
		this.custAccty = custAccty;
	}

	public String getCustAcctyName() {
		return custAcctyName;
	}

	public void setCustAcctyName(String custAcctyName) {
		this.custAcctyName = custAcctyName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}


	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String getAccThreeSubject() {
		return accThreeSubject;
	}

	public void setAccThreeSubject(String accThreeSubject) {
		this.accThreeSubject = accThreeSubject;
	}

	public String getRsrAmt() {
		return rsrAmt;
	}

	public void setRsrAmt(String rsrAmt) {
		this.rsrAmt = rsrAmt;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public double getGyamt() {
		return gyamt;
	}

	public void setGyamt(double gyamt) {
		this.gyamt = gyamt;
	}

	public double getTyamt() {
		return tyamt;
	}

	public void setTyamt(double tyamt) {
		this.tyamt = tyamt;
	}

	public double getmInt() {
		return mInt;
	}

	public void setmInt(double mInt) {
		this.mInt = mInt;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public double getContractRate() {
		return contractRate;
	}

	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getIrTerm() {
		return irTerm;
	}

	public void setIrTerm(String irTerm) {
		this.irTerm = irTerm;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getPartyFinanceLicense() {
		return partyFinanceLicense;
	}

	public void setPartyFinanceLicense(String partyFinanceLicense) {
		this.partyFinanceLicense = partyFinanceLicense;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPartyOrganGrade() {
		return partyOrganGrade;
	}

	public void setPartyOrganGrade(String partyOrganGrade) {
		this.partyOrganGrade = partyOrganGrade;
	}

}
