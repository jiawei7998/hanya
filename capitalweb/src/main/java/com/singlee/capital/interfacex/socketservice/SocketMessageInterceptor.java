package com.singlee.capital.interfacex.socketservice;


public class SocketMessageInterceptor {
	
	public static String returnMessage(String sendmsg,String statuscode,String statusmsg) throws Exception {
		String returnMsg = null ;
		int tag_start = sendmsg.toString().indexOf("<BOSFXII>");
		int tag_end = sendmsg.toString().indexOf("<CommonRqHdr>");
		int uuid_start =  sendmsg.toString().indexOf("<RqUID>");
		int uuid_end = sendmsg.toString().indexOf("</RqUID>");
		String intfaceName = sendmsg.toString().substring(tag_start+10, tag_end-2);
		intfaceName = intfaceName+"s";
		String uuid = sendmsg.toString().substring(uuid_start+7,uuid_end);
		
		String statusCode = statuscode.length()<=0?InterfaceCode.TI_STATUS11:statuscode;
		String serverStatusCode = statusmsg.length()<=0?InterfaceCode.TI_MESSAGE11:statusmsg;
		//String numTranCode = "123456";
		//String cntId = "7890";
		returnMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\">"+ 
			"<"+intfaceName+">"+ 
			"<CommonRsHdr>"+ 
			"<StatusCode>"+statusCode+"</StatusCode>"+ 
			"<ServerStatusCode>"+serverStatusCode+"</ServerStatusCode>"+ 
			"<RqUID>"+uuid+"</RqUID>"+ 
			//"<NumTranCode>"+numTranCode+"</NumTranCode>"+ 
			//"<CntId>"+cntId+"</CntId>"+ 
			"</CommonRsHdr>"+ 
			"</"+intfaceName+">"+ 
			"</BOSFXII>";
//		System.out.println("RETURN MSG:"+returnMsg+"----------------------------------------------------");
		return returnMsg;
	}
	
}
