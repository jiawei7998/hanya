package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExLdRpmSchInqRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExLdRpmSchInqRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="CustNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShowPdInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExLdRpmSchInqRq", propOrder = { "commonRqHdr", "custNo",
		"contractId", "endDt", "beginDt", "category", "showPdInfo" })
public class TExLdRpmSchInqRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "CustNo", required = true)
	protected String custNo;
	@XmlElement(name = "ContractId", required = true)
	protected String contractId;
	@XmlElement(name = "EndDt", required = true)
	protected String endDt;
	@XmlElement(name = "BeginDt", required = true)
	protected String beginDt;
	@XmlElement(name = "Category", required = true)
	protected String category;
	@XmlElement(name = "ShowPdInfo", required = true)
	protected String showPdInfo;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the custNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustNo() {
		return custNo;
	}

	/**
	 * Sets the value of the custNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustNo(String value) {
		this.custNo = value;
	}

	/**
	 * Gets the value of the contractId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContractId() {
		return contractId;
	}

	/**
	 * Sets the value of the contractId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContractId(String value) {
		this.contractId = value;
	}

	/**
	 * Gets the value of the endDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDt() {
		return endDt;
	}

	/**
	 * Sets the value of the endDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDt(String value) {
		this.endDt = value;
	}

	/**
	 * Gets the value of the beginDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDt() {
		return beginDt;
	}

	/**
	 * Sets the value of the beginDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDt(String value) {
		this.beginDt = value;
	}

	/**
	 * Gets the value of the category property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the value of the category property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCategory(String value) {
		this.category = value;
	}

	/**
	 * Gets the value of the showPdInfo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShowPdInfo() {
		return showPdInfo;
	}

	/**
	 * Sets the value of the showPdInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShowPdInfo(String value) {
		this.showPdInfo = value;
	}

}
