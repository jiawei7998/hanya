package com.singlee.capital.interfacex.socketservice;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

//import com.bank.loadBalancingLib.LoadBalancingLib;
import com.singlee.capital.base.cal.LogManager;

/**
 * SOCKET
 * 
 * @author litengfei
 * 
 */
@Component("socketClientServer")
public class SocketClientServer implements Runnable {

	/**
	 * IP
	 */
	private String socketClientIp;
	/**
	 * port
	 */
	private int socketClientPort;
	/**
	 * time
	 */
	private int socketClientTimeOut;

	private Object object;

	private String retMsg;

	private boolean sendflag = true;

	public boolean isSendflag() {
		return sendflag;
	}

	public void setSendflag(boolean sendflag) {
		this.sendflag = sendflag;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getSocketClientIp() {
		return socketClientIp;
	}

	public void setSocketClientIp(String socketClientIp) {
		this.socketClientIp = socketClientIp;
	}

	public int getSocketClientPort() {
		return socketClientPort;
	}

	public void setSocketClientPort(int socketClientPort) {
		this.socketClientPort = socketClientPort;
	}

	public int getSocketClientTimeOut() {
		return socketClientTimeOut;
	}

	public void setSocketClientTimeOut(int socketClientTimeOut) {
		this.socketClientTimeOut = socketClientTimeOut;
	}

	@Override
	public void run() {

		DataOutputStream out = null;

		DataInputStream in = null;

		Socket socket = null;

		try {
			if (null != getObject() && sendflag) {

//				Map<String, String> map = LoadBalancingLib.GetDtnInfo(System
//						.getenv().get("HOME") + "/bosswlbfv/cfg/bosswlb.cfg",
//						"IFBM_SOCKET");
				Map<String, String> map =null;
				if ("0".equals(map.get("CODE"))) {

					socketClientIp = map.get("IP");
					socketClientPort = Integer.parseInt(map.get("PORT"));
					LogManager.getLogger(LogManager.MODEL_BATCH).info("log:!!!=====Ip=" + socketClientIp + "    Port="+ socketClientPort);

					socket = new Socket(socketClientIp, socketClientPort);

					socket.setSoTimeout(socketClientTimeOut);

					out = new DataOutputStream(socket.getOutputStream());

					in = new DataInputStream(socket.getInputStream());

					out.write((leftPadString(
							((String) getObject()).getBytes().length, 9) + (String) getObject())
							.getBytes());

					out.flush();

					byte[] str = new byte[1];

					int i = in.read(str);

					List<Byte> list = new ArrayList<Byte>();

					list.add(str[0]);

					while (i != -1 && in.available() != 0) {
						i = in.read(str);

						list.add(str[0]);
					}
					byte[] total = new byte[list.size()];
					int count = 0;
					for (byte a : list) {
						total[count++] = a;
					}
					retMsg = new String(total, "UTF-8");
					System.out.println("RETURN MSG:"+ retMsg+ "----------------------------------------------------");
				} else {
					String CODE = map.get("CODE");
					String ERRORMSG = map.get("ERRORMSG");
					retMsg = "连接错误！错误代码：" + CODE + "，错误信息：" + ERRORMSG;
					System.out.println("RETURN MSG:"+ retMsg+ "----------------------------------------------------");
				}

			} else {

				String sendmsg = (String) getObject();
				retMsg = SocketMessageInterceptor.returnMessage(sendmsg,
						InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			String sendmsg = (String) getObject();
			try {
				retMsg = SocketMessageInterceptor.returnMessage(sendmsg,
						InterfaceCode.TI_FAILURE, e.getMessage());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			try {
				if (null != out) {
                    out.close();
                }
				if (null != in) {
                    in.close();
                }
				if (null != socket) {
                    socket.close();
                }
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}

	}

	public String start() {
		// TODO Auto-generated method stub
		retMsg = "";
		DataOutputStream out = null;

		DataInputStream in = null;

		Socket socket = null;

		try {
			if (null != getObject() && sendflag) {

//				Map<String, String> map = LoadBalancingLib.GetDtnInfo(System.getenv().get("HOME") + "/bosswlbfv/cfg/bosswlb.cfg","IFBM_SOCKET");
				Map<String, String> map =null;
				if ("0".equals(map.get("CODE"))) {

					socketClientIp = map.get("IP");
					socketClientPort = Integer.parseInt(map.get("PORT"));
					LogManager.getLogger(LogManager.MODEL_BATCH).info("log:!!!=====Ip=" + socketClientIp + "    Port="+ socketClientPort);

					socket = new Socket(socketClientIp, socketClientPort);

					socket.setSoTimeout(socketClientTimeOut);

					out = new DataOutputStream(socket.getOutputStream());

					in = new DataInputStream(socket.getInputStream());

					out.write((leftPadString(
							((String) getObject()).getBytes().length, 9) + (String) getObject())
							.getBytes());

					out.flush();

					byte[] str = new byte[1];

					int i = in.read(str);

					List<Byte> list = new ArrayList<Byte>();

					list.add(str[0]);

					while (i != -1 && in.available() != 0) {
						i = in.read(str);

						list.add(str[0]);
					}
					byte[] total = new byte[list.size()];
					int count = 0;
					for (byte a : list) {
						total[count++] = a;
					}
					retMsg = new String(total, "UTF-8");
					System.out.println("RETURN MSG:"+ retMsg + "----------------------------------------------------");
				} else {
					String CODE = map.get("CODE");
					String ERRORMSG = map.get("ERRORMSG");
					retMsg = "连接错误！错误代码：" + CODE + "，错误信息：" + ERRORMSG;
					System.out.println("RETURN MSG:"+ retMsg+ "----------------------------------------------------");
				}

			} else {

				String sendmsg = (String) getObject();
				retMsg = SocketMessageInterceptor.returnMessage(sendmsg,
						InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			String sendmsg = (String) getObject();
			try {
				retMsg = SocketMessageInterceptor.returnMessage(sendmsg,
						InterfaceCode.TI_FAILURE, e.getMessage());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			try {
				if (null != out) {
                    out.close();
                }
				if (null != in) {
                    in.close();
                }
				if (null != socket) {
                    socket.close();
                }
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}
		return retMsg;
	}

	public static String leftPadString(int lengthStr, int len) {
		return String.format("%0" + len + "d", lengthStr);
	}

	public static void main(String[] args) throws UnknownHostException,
			IOException, InterruptedException {
		for(int j=0;j<10000000;j++){
		DataOutputStream out = null;
		DataInputStream in = null;
		Socket socket = null;
		Thread.sleep(1000);
		String str =
		// 额度切分同步（CRMS-同业系统） 11
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><QuotaSyncopateRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>1707171629327f000001580808000001</RqUID><NumTranCode>093070</NumTranCode><ClearDate>20170717</ClearDate><TranDate>20170717</TranDate><TranTime>162932</TranTime><ChannelId>S64</ChannelId></CommonRqHdr><QuotaSyncopateRec><EcifNum>ECIF201708210001</EcifNum><Sector>0401</Sector><Customer>20130510000024</Customer><ProductType>110010</ProductType><Currency>CNY</Currency><LoanAmt>100000.00000000</LoanAmt><Term>12</Term><TermType>04</TermType><DueDate>2017-05-20T0:00:00.0Z</DueDate><CntrStatus>02</CntrStatus></QuotaSyncopateRec><QuotaSyncopateRec><EcifNum>ECIF201708210001</EcifNum><Sector>0401</Sector><Customer>20130510000024</Customer><ProductType>110001</ProductType><Currency>CNY</Currency><LoanAmt>100000.00000000</LoanAmt><Term>12</Term><TermType>04</TermType><DueDate>2017-05-20T0:00:00.0Z</DueDate><CntrStatus>02</CntrStatus></QuotaSyncopateRec><QuotaSyncopateRec><EcifNum>ECIF201708210001</EcifNum><Sector>0401</Sector><Customer>20130510000024</Customer><ProductType>110013</ProductType><Currency>CNY</Currency><LoanAmt>100000.00000000</LoanAmt><Term>12</Term><TermType>04</TermType><DueDate>2017-05-20T0:00:00.0Z</DueDate><CntrStatus>02</CntrStatus></QuotaSyncopateRec></QuotaSyncopateRq></BOSFXII>";
		// 批复信息变更（CRMS-同业）
		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><ApprovalChangeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>8A8A8A8A8A8A8A8A8A</RqUID><NumTranCode>093070></NumTranCode><ClearDate>20170720</ClearDate><TranDate>20170720</TranDate><TranTime>162436</TranTime><ChanneId>S64</ChanneId></CommonRqHdr><ClientNo>20140303000032</ClientNo></ApprovalChangeRq></BOSFXII>";
		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><ApprovalChangeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>8A8A8A8A8A8A8A8A8A</RqUID><NumTranCode>093070></NumTranCode><ClearDate>20170720</ClearDate><TranDate>20170720</TranDate><TranTime>162436</TranTime><ChanneId>S64</ChanneId></CommonRqHdr><ApprovalChangeRec><ClientNo>20140303000032</ClientNo></ApprovalChangeRec></ApprovalChangeRq></BOSFXII>";
		// 放款通知 11
		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><LoanNoticeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>8a7ba5d85dc0adff015dc0d4108700fa</RqUID><NumTranCode>093070</NumTranCode><ClearDate>20170808</ClearDate><TranDate>20170808</TranDate><TranTime>155149</TranTime><ChannelId>S64</ChannelId></CommonRqHdr><LoanSummaryId>LD1224202575</LoanSummaryId><TransDesc>LD1224202575WT</TransDesc><LoanAmt>190000000.00</LoanAmt><Currency>CNY</Currency><InfoFlag>01</InfoFlag><LoanType>01</LoanType></LoanNoticeRq></BOSFXII>";
		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><LoanNoticeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>1707111128187f000001580808000003</RqUID><TranTime>112818</TranTime><ChannelId>S64</ChannelId></CommonRqHdr><LoanSummaryId>LD0807000043</LoanSummaryId><CustBrNo>LD0807000043</CustBrNo><TransDesc>SH6191200803100003</TransDesc><LoanAmt>111500000.00</LoanAmt><Currency>CNY</Currency><LoanType>01</LoanType></LoanNoticeRq></BOSFXII>";

		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><LoanNoticeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>8a7ba5d85dc0adff015dc0d4108700fa</RqUID><NumTranCode>093070</NumTranCode><ClearDate>20170810</ClearDate><TranDate>20170810</TranDate><TranTime>182936</TranTime><ChannelId>S64</ChannelId></CommonRqHdr><CrmsLoanId>LOAN17081016921</CrmsLoanId><LoanAmt>0.00</LoanAmt><InfoFlag>01</InfoFlag></LoanNoticeRq></BOSFXII>";
		// 提前还款通知（CRMS-系统）
		// "<?xml version=\"1.0\" encoding=\"UTF-8\"?><BOSFXII xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"><RepayNoticeRq><CommonRqHdr><SPName>IFBM</SPName><RqUID>1707111132137f000001580808000004</RqUID><NumTranCode>093070</NumTranCode><ClearDate>20170711</ClearDate><TranDate>20170711</TranDate><TranTime>113213</TranTime><ChannelId>S64</ChannelId></CommonRqHdr><LoanSummaryId>LD1410676947</LoanSummaryId><RetAmt>0.00</RetAmt><Currency>CNY</Currency></RepayNoticeRq></BOSFXII>";
		socket = new Socket("10.240.88.208", 9999);// 10.240.88.203 localhost

		socket.setSoTimeout(600000);

		out = new DataOutputStream(socket.getOutputStream());

		in = new DataInputStream(socket.getInputStream());

		out.write((leftPadString(str.getBytes().length, 9) + str).getBytes());

		out.flush();

		byte[] str1 = new byte[1];

		int i = in.read(str1);

		List<Byte> list = new ArrayList<Byte>();

		list.add(str1[0]);

		while (i != -1 && in.available() != 0) {
			i = in.read(str1);

			list.add(str1[0]);
		}
		byte[] total = new byte[list.size()];
		int count = 0;
		for (byte a : list) {
			total[count++] = a;
		}
		String retMsg = new String(total, "UTF-8");
		System.out.println("RETURN MSG:" + retMsg
				+ "----------------------------------------------------");
		socket.close();
		}
	}
}
