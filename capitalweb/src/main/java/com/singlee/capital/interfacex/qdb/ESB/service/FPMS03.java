package com.singlee.capital.interfacex.qdb.ESB.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import com.singlee.capital.interfacex.qdb.ESB.model.PMS300202Model;
import com.singlee.capital.interfacex.qdb.ESB.service.impl.EsbClientServiceImpl;
import com.singlee.capital.interfacex.qdb.ESB.util.PMS300202ReplaceElementContext;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

@Component
public class FPMS03{


//	private static Logger logger = LogManager.getLogger(LogManager.ESB_LOG);
	
	private EsbClientService esbClientService;
	
	private String esbUrl = PropertiesUtil.getProperties(PropertiesUtil.EsbServiceUrl) + "300202";	
	
	HashMap<String, Object> pmsMap  = new HashMap<String, Object>();
	
	@SuppressWarnings("unused")
	public HashMap<String, Object> sendPMS300202Reqt(PMS300202Model reqtBean) {
		esbClientService = new EsbClientServiceImpl();
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader reader = new SAXReader();
//		reader.setEncoding("UTF-8");
		try {
				String reqtMsg = packagePMS300202Reqt(reqtBean);
				
//				logger.info("发送客户查询(BC5001)请求报文.");
				
				HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, reqtMsg);
				
				if(hashMap.size()==0 || hashMap == null){
//					logger.info("请求报文时发生网络异常！请检查网络！");
		        }else{
//			        logger.info(hashMap.get("statusCode"));
			        if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
			        	byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
			        	org.dom4j.Document xmlDocAccept = reader.read(byteArrayInputStream);
//			        	xmlDocAccept.setXMLEncoding("UTF-8");
			        	
//			        	logger.info("接收客户查询(bc5001)返回报文.");
			        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				        HashMap<String, Object> appBody = new HashMap<String, Object>();//appBody
				        HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				        
				        PMS300202ReplaceElementContext.praseEsbXmlPackageDescription((byte[]) hashMap.get("Bytes"), svcHdr, appHdr, appBody);
//				        for(PMS300202Model bc5001Reqt:applays){
//				        	bc5001Reqt.setSearchKeys((String)appHdr.get("searchKeys"));
//				        }
//				        //判断若无记录，去除第一条数据（核心返回无数据会存一条数据）
//			        	if(appHdr.get("respCde").equals("AB")){
//			        		bc5001Map.put("code", "error");
//			        		bc5001Map.put("respCde", appHdr.get("respCde"));
//			        		bc5001Map.put("respMsg", svcHdr.get("respMsg"));
//			        	}else{
////			        		bc5001Map.put("moreRecInd", appHdr.get("moreRecInd"));
//					        bc5001Map.put("List", applays);
//					        bc5001Map.put("searchKeys", appHdr.get("searchKeys"));
//					        bc5001Map.put("code", "success");
//			        	}
			        }
		        }
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return pmsMap;
	}
	
	public String packagePMS300202Reqt(PMS300202Model reqtBean){
		org.w3c.dom.Document doc = null;  
		Element root = null;
		StringWriter xmlOut = null;
		String xmlResp = null;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
			DocumentBuilder builder = factory.newDocumentBuilder();  
			doc = builder.newDocument();  
			root = doc.createElement("reqt");  
			doc.appendChild(root);
			
			Element svcHdr = doc.createElement("svcHdr");
			Element corrId = doc.createElement("corrId");
			corrId.setTextContent("");
			Element svcId = doc.createElement("svcId");
			svcId.setTextContent("151232");
			Element verNbr = doc.createElement("verNbr");
			verNbr.setTextContent("105");
			Element csmrId = doc.createElement("csmrId");
			csmrId.setTextContent("265000");
			Element csmrSerNbr = doc.createElement("csmrSerNbr");
			csmrSerNbr.setTextContent("100010001000");
			Element tmStamp = doc.createElement("tmStamp");
			tmStamp.setTextContent("");
//			tmStamp.setTextContent(ConvertUtil.getTime(new Date()));
			Element reqtIp = doc.createElement("reqtIp");
			reqtIp.setTextContent("10.1.80.185");
			svcHdr.appendChild(corrId);
			svcHdr.appendChild(svcId);
			svcHdr.appendChild(verNbr);
			svcHdr.appendChild(csmrId);
			svcHdr.appendChild(csmrSerNbr);
			svcHdr.appendChild(tmStamp);
			svcHdr.appendChild(reqtIp);
			
			Element appHdr = doc.createElement("appHdr");  
			
			Element reqDate = doc.createElement("reqDate");
			reqDate.setTextContent("");
			Element reqTime = doc.createElement("reqTime");
			reqTime.setTextContent("");
			appHdr.appendChild(reqDate);
			appHdr.appendChild(reqTime);
			
			Element appBody = doc.createElement("appBody");
			
			Element chanNo = doc.createElement("chanNo");
			chanNo.setTextContent(StringUtils.trimToEmpty("284000"));
			appBody.appendChild(chanNo);
			Element serNo = doc.createElement("serNo");
			serNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getSerNo()));
			appBody.appendChild(serNo);
			Element dealDt = doc.createElement("dealDt");
			dealDt.setTextContent(StringUtils.trimToEmpty(reqtBean.getDealDt()));
			appBody.appendChild(dealDt);
			Element cifNo = doc.createElement("cifNo");
			cifNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getCifNo()));
			appBody.appendChild(cifNo);
			Element cifNm = doc.createElement("cifNm");
			cifNm.setTextContent(StringUtils.trimToEmpty(reqtBean.getCifNm()));
			appBody.appendChild(cifNm);
			Element cifAcctNo = doc.createElement("cifAcctNo");
			cifAcctNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getCifAcctNo()));
			appBody.appendChild(cifAcctNo);
			Element ccy = doc.createElement("ccy");
			ccy.setTextContent(StringUtils.trimToEmpty(reqtBean.getCcy()));
			appBody.appendChild(ccy);
			Element ccyAmt = doc.createElement("ccyAmt");
			ccyAmt.setTextContent(StringUtils.trimToEmpty(String.valueOf(reqtBean.getCcyAmt())));
			appBody.appendChild(ccyAmt);
			Element ccyPosAmt = doc.createElement("ccyPosAmt");
			ccyPosAmt.setTextContent(StringUtils.trimToEmpty(String.valueOf(reqtBean.getCcyPosAmt())));
			appBody.appendChild(ccyPosAmt);
			Element targetCifNo = doc.createElement("targetCifNo");
			targetCifNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getTargetCifNo()));
			appBody.appendChild(targetCifNo);
			Element targetCifNm = doc.createElement("targetCifNm");
			targetCifNm.setTextContent(StringUtils.trimToEmpty(reqtBean.getTargetCifNm()));
			appBody.appendChild(targetCifNm);
			Element targetAcctNo = doc.createElement("targetAcctNo");
			targetAcctNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getTargetAcctNo()));
			appBody.appendChild(targetAcctNo);
			Element operFlg = doc.createElement("operFlg");
			operFlg.setTextContent(StringUtils.trimToEmpty(reqtBean.getOperFlg()));
			appBody.appendChild(operFlg);
			Element tellerNo = doc.createElement("tellerNo");
			tellerNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getTellerNo()));
			appBody.appendChild(tellerNo);
			Element tellerNm = doc.createElement("tellerNm");
			tellerNm.setTextContent(StringUtils.trimToEmpty(reqtBean.getTellerNm()));
			appBody.appendChild(tellerNm);
			Element operTm = doc.createElement("operTm");
			operTm.setTextContent(StringUtils.trimToEmpty(reqtBean.getOperTm()));
			appBody.appendChild(operTm);
			Element note = doc.createElement("note");
			note.setTextContent(StringUtils.trimToEmpty(reqtBean.getNote()));
			appBody.appendChild(note);
			Element pmtGrpId = doc.createElement("pmtGrpId");
			pmtGrpId.setTextContent(StringUtils.trimToEmpty(reqtBean.getPmtGrpId()));
			appBody.appendChild(pmtGrpId);
			Element orgNo = doc.createElement("orgNo");
			orgNo.setTextContent(StringUtils.trimToEmpty(reqtBean.getOrgNo()));
			appBody.appendChild(orgNo);
			Element orgNm = doc.createElement("orgNm");
			orgNm.setTextContent(StringUtils.trimToEmpty(reqtBean.getOrgNm()));
			appBody.appendChild(orgNm);
			Element remitType = doc.createElement("remitType");
			remitType.setTextContent(StringUtils.trimToEmpty(reqtBean.getRemitType()));
			appBody.appendChild(remitType);
			Element sysType = doc.createElement("sysType");
			sysType.setTextContent(StringUtils.trimToEmpty(reqtBean.getSysType()));
			appBody.appendChild(sysType);
			Element oldPmtGrpId = doc.createElement("oldPmtGrpId");
			oldPmtGrpId.setTextContent(StringUtils.trimToEmpty(reqtBean.getOldPmtGrpId()));
			appBody.appendChild(oldPmtGrpId);
			
			root.appendChild(svcHdr);
			root.appendChild(appHdr);
			root.appendChild(appBody);
			
			DOMSource source = new DOMSource(doc);  
			TransformerFactory tf = TransformerFactory.newInstance();  
			Transformer transformer = tf.newTransformer();  
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");//设置文档的换行与缩进  
			 
            xmlOut=new StringWriter();
            StreamResult result=new StreamResult(xmlOut);
            transformer.transform(source,result);
            xmlResp = xmlOut.toString(); 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			if(null != xmlOut){
				try {
					xmlOut.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return xmlResp;
		
	}
	
	public String formatBigDecimal4(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.0000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	public String formatBigDecimal8(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.00000000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	/**
	 * @return the esbUrl
	 */
	public String getEsbUrl() {
		return esbUrl;
	}

	/**
	 * @param esbUrl the esbUrl to set
	 */
	public void setEsbUrl(String esbUrl) {
		this.esbUrl = esbUrl;
	}

	public EsbClientService getEsbClientService() {
		return esbClientService;
	}

	public void setEsbClientService(EsbClientService esbClientService) {
		this.esbClientService = esbClientService;
	}

}
