package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class TExReconT24lz implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String srquid;
	private String feebaselogid;
	private String RETCODE;
	public String getSrquid() {
		return srquid;
	}
	public void setSrquid(String srquid) {
		this.srquid = srquid;
	}
	public String getFeebaselogid() {
		return feebaselogid;
	}
	public void setFeebaselogid(String feebaselogid) {
		this.feebaselogid = feebaselogid;
	}
	public String getRETCODE() {
		return RETCODE;
	}
	public void setRETCODE(String rETCODE) {
		RETCODE = rETCODE;
	}
	
	
}
