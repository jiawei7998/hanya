package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class JobExcuLog implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String LOG_ID;
	public String getLOG_ID() {
		return LOG_ID;
	}
	public void setLOG_ID(String lOG_ID) {
		LOG_ID = lOG_ID;
	}
	private int JOB_ID;
	private String EXCU_TIME;
	private String JOB_STATUS;
	private String INPUTTIME;
	public String getINPUTTIME() {
		return INPUTTIME;
	}
	public void setINPUTTIME(String iNPUTTIME) {
		INPUTTIME = iNPUTTIME;
	}
	public int getJOB_ID() {
		return JOB_ID;
	}
	public void setJOB_ID(int jOB_ID) {
		JOB_ID = jOB_ID;
	}
	public String getEXCU_TIME() {
		return EXCU_TIME;
	}
	public void setEXCU_TIME(String eXCU_TIME) {
		EXCU_TIME = eXCU_TIME;
	}
	public String getJOB_STATUS() {
		return JOB_STATUS;
	}
	public void setJOB_STATUS(String jOB_STATUS) {
		JOB_STATUS = jOB_STATUS;
	}
	
}
