package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class tiInterfaceLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  rquid      ;
	private String  spname     ;
	private String  numtrancode;
	private String  cleardate  ;
	private String  trandate   ;
	private String  trantime   ;
	private String  channelid  ;
	private String  companycode;
	private String  inputtime  ;
	private String  retcode    ;
	private String  retmsg     ;
	public String getRquid() {
		return rquid;
	}
	public void setRquid(String rquid) {
		this.rquid = rquid;
	}
	public String getSpname() {
		return spname;
	}
	public void setSpname(String spname) {
		this.spname = spname;
	}
	public String getNumtrancode() {
		return numtrancode;
	}
	public void setNumtrancode(String numtrancode) {
		this.numtrancode = numtrancode;
	}
	public String getCleardate() {
		return cleardate;
	}
	public void setCleardate(String cleardate) {
		this.cleardate = cleardate;
	}
	public String getTrandate() {
		return trandate;
	}
	public void setTrandate(String trandate) {
		this.trandate = trandate;
	}
	public String getTrantime() {
		return trantime;
	}
	public void setTrantime(String trantime) {
		this.trantime = trantime;
	}
	public String getChannelid() {
		return channelid;
	}
	public void setChannelid(String channelid) {
		this.channelid = channelid;
	}
	public String getCompanycode() {
		return companycode;
	}
	public void setCompanycode(String companycode) {
		this.companycode = companycode;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return retmsg;
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

}
