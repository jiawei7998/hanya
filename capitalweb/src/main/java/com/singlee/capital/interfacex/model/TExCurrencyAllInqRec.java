package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExCurrencyAllInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExCurrencyAllInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GbCcyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CnCcyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BoardBuyRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BoardSellRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BoardMidRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanknoteBuyRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanknoteSellRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanknoteMidRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SafeBuyRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SafeSellRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SafeMidRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NoOfDecimals" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UpdateDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UpdateTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CcyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExCurrencyAllInqRec", propOrder = { "gbCcyName", "cnCcyName",
		"boardBuyRate", "boardSellRate", "boardMidRate", "banknoteBuyRate",
		"banknoteSellRate", "banknoteMidRate", "safeBuyRate", "safeSellRate",
		"safeMidRate", "code", "noOfDecimals", "currencyCode", "updateDate",
		"updateTime", "ccyId" })
public class TExCurrencyAllInqRec {

	@XmlElement(name = "GbCcyName", required = true)
	protected String gbCcyName;
	@XmlElement(name = "CnCcyName", required = true)
	protected String cnCcyName;
	@XmlElement(name = "BoardBuyRate", required = true)
	protected String boardBuyRate;
	@XmlElement(name = "BoardSellRate", required = true)
	protected String boardSellRate;
	@XmlElement(name = "BoardMidRate", required = true)
	protected String boardMidRate;
	@XmlElement(name = "BanknoteBuyRate", required = true)
	protected String banknoteBuyRate;
	@XmlElement(name = "BanknoteSellRate", required = true)
	protected String banknoteSellRate;
	@XmlElement(name = "BanknoteMidRate", required = true)
	protected String banknoteMidRate;
	@XmlElement(name = "SafeBuyRate", required = true)
	protected String safeBuyRate;
	@XmlElement(name = "SafeSellRate", required = true)
	protected String safeSellRate;
	@XmlElement(name = "SafeMidRate", required = true)
	protected String safeMidRate;
	@XmlElement(name = "Code", required = true)
	protected String code;
	@XmlElement(name = "NoOfDecimals", required = true)
	protected String noOfDecimals;
	@XmlElement(name = "CurrencyCode", required = true)
	protected String currencyCode;
	@XmlElement(name = "UpdateDate", required = true)
	protected String updateDate;
	@XmlElement(name = "UpdateTime", required = true)
	protected String updateTime;
	@XmlElement(name = "CcyId", required = true)
	protected String ccyId;

	/**
	 * Gets the value of the gbCcyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGbCcyName() {
		return gbCcyName;
	}

	/**
	 * Sets the value of the gbCcyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGbCcyName(String value) {
		this.gbCcyName = value;
	}

	/**
	 * Gets the value of the cnCcyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCnCcyName() {
		return cnCcyName;
	}

	/**
	 * Sets the value of the cnCcyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCnCcyName(String value) {
		this.cnCcyName = value;
	}

	/**
	 * Gets the value of the boardBuyRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBoardBuyRate() {
		return boardBuyRate;
	}

	/**
	 * Sets the value of the boardBuyRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBoardBuyRate(String value) {
		this.boardBuyRate = value;
	}

	/**
	 * Gets the value of the boardSellRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBoardSellRate() {
		return boardSellRate;
	}

	/**
	 * Sets the value of the boardSellRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBoardSellRate(String value) {
		this.boardSellRate = value;
	}

	/**
	 * Gets the value of the boardMidRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBoardMidRate() {
		return boardMidRate;
	}

	/**
	 * Sets the value of the boardMidRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBoardMidRate(String value) {
		this.boardMidRate = value;
	}

	/**
	 * Gets the value of the banknoteBuyRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBanknoteBuyRate() {
		return banknoteBuyRate;
	}

	/**
	 * Sets the value of the banknoteBuyRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBanknoteBuyRate(String value) {
		this.banknoteBuyRate = value;
	}

	/**
	 * Gets the value of the banknoteSellRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBanknoteSellRate() {
		return banknoteSellRate;
	}

	/**
	 * Sets the value of the banknoteSellRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBanknoteSellRate(String value) {
		this.banknoteSellRate = value;
	}

	/**
	 * Gets the value of the banknoteMidRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBanknoteMidRate() {
		return banknoteMidRate;
	}

	/**
	 * Sets the value of the banknoteMidRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBanknoteMidRate(String value) {
		this.banknoteMidRate = value;
	}

	/**
	 * Gets the value of the safeBuyRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSafeBuyRate() {
		return safeBuyRate;
	}

	/**
	 * Sets the value of the safeBuyRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSafeBuyRate(String value) {
		this.safeBuyRate = value;
	}

	/**
	 * Gets the value of the safeSellRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSafeSellRate() {
		return safeSellRate;
	}

	/**
	 * Sets the value of the safeSellRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSafeSellRate(String value) {
		this.safeSellRate = value;
	}

	/**
	 * Gets the value of the safeMidRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSafeMidRate() {
		return safeMidRate;
	}

	/**
	 * Sets the value of the safeMidRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSafeMidRate(String value) {
		this.safeMidRate = value;
	}

	/**
	 * Gets the value of the code property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the value of the code property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCode(String value) {
		this.code = value;
	}

	/**
	 * Gets the value of the noOfDecimals property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNoOfDecimals() {
		return noOfDecimals;
	}

	/**
	 * Sets the value of the noOfDecimals property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNoOfDecimals(String value) {
		this.noOfDecimals = value;
	}

	/**
	 * Gets the value of the currencyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * Sets the value of the currencyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyCode(String value) {
		this.currencyCode = value;
	}

	/**
	 * Gets the value of the updateDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the value of the updateDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUpdateDate(String value) {
		this.updateDate = value;
	}

	/**
	 * Gets the value of the updateTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUpdateTime() {
		return updateTime;
	}

	/**
	 * Sets the value of the updateTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUpdateTime(String value) {
		this.updateTime = value;
	}

	/**
	 * Gets the value of the ccyId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCcyId() {
		return ccyId;
	}

	/**
	 * Sets the value of the ccyId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCcyId(String value) {
		this.ccyId = value;
	}

}
