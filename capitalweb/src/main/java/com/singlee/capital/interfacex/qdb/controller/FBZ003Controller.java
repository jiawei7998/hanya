package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.service.FBZService;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;
//客户额度占用交易
@Controller
@RequestMapping("fbz003")
public class FBZ003Controller extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private EsbClientService esbClientService;
	@Resource 
	private FBZService fbzService;
	@Autowired
	private ProductApproveService productApproveService;
	
	@RequestMapping("getFbz003")
	@ResponseBody
	public FBZ003 getResp(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ003 fbz){
		FBZ003 f=null;
		try {
			fbz.setMfCustomerId(String.valueOf(map.get("mfCustomerId")));
			fbz.setCustomerType(String.valueOf(map.get("customerType")));
			fbz.setLmtID(String.valueOf(map.get("lmtID")));
			fbz.setDealNo(String.valueOf(map.get("dealNo")));
			fbz.setFrzcurrency(PropertiesUtil.parseFile("currency.properties").getString(String.valueOf(map.get("frzcurrency"))));
			fbz.setOperTyp(String.valueOf(map.get("operTyp")));
			fbz.setFrzSum(String.valueOf(map.get("frzSum")));
			fbz.setMaturity(String.valueOf(map.get("maturity")));
			f = sendReq(request,fbz);
			if(f!=null){
				fbzService.insertFbz(fbz);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return f;
	}
	@RequestMapping("reback")
	@ResponseBody
	public FBZ003 reback(HttpServletRequest request,@RequestBody Map<String, Object> map){
		FBZ003 f=null;
		FBZ003 fbz=null;
		try {
			f=fbzService.getFbzByDealNo(String.valueOf(map.get("dealNo")));
			if(f!=null){
				TdProductApproveMain main = productApproveService.getProductApproveActivated(String.valueOf(map.get("dealNo")));
				f.setOperTyp("04");
				f.setFrzSum(String.valueOf(main.getAmt()));
				fbz = sendReq(request,f);
				if(fbz!=null){
					//fbzService.deleteFbz(f);
					f.setOperTyp("00");
					fbzService.updateFbz(f);
				}
			}else{
				fbz=new FBZ003();
				fbz.setRespCde("15700000000000");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fbz;
	}
	@RequestMapping("saveSercurate")
	@ResponseBody
	public boolean saveSercurate(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ003 fbz) throws Exception{		
		fbz=new FBZ003();
		fbz.setMfCustomerId(String.valueOf(map.get("mfCustomerId")));
		fbz.setLmtID(String.valueOf(map.get("lmtID")));
		fbz.setDealNo(String.valueOf(map.get("dealNo")));
		fbz.setCurrency(PropertiesUtil.parseFile("currency.properties").getString(String.valueOf(map.get("frzcurrency"))));
		fbz.setOperTyp(String.valueOf(map.get("operTyp")));
		fbz.setLimitSum(String.valueOf(map.get("frzSum")));
		fbz.setLimitBalance(String.valueOf(map.get("frzSum")));
		fbz.setMaturity(String.valueOf(map.get("maturity")));
		fbz.setContentType(String.valueOf(map.get("contentType")));
		fbz.setSerialNo(String.valueOf(map.get("serialNo")));
		return fbzService.insertFbz(fbz);	
	}
	@RequestMapping("saveLimit")
	@ResponseBody
	public boolean saveLimit(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ003 fbz){
		boolean f=false;
		try {
			/***
			 * 保存额度记录到后台
			 * mfCustomerId 授信客户号
			 * dealNo 交易流水号
			 * frzcurrency 币种
			 * frzSum 占用金额
			 * lmtID 授信编号
			 * maturity 到期日期
			 * operTyp 00 01
			 * contentType 企业 1 同业 2
			 * customerType 客户类型
			 * serialNo 出账流水号
			 * ***/
			fbz.setMfCustomerId(String.valueOf(map.get("mfCustomerId")));
			fbz.setDealNo(String.valueOf(map.get("dealNo")));
			fbz.setCurrency(PropertiesUtil.parseFile("currency.properties").getString(String.valueOf(map.get("frzcurrency"))));
			fbz.setLimitSum(String.valueOf(map.get("frzSum")));
			fbz.setLimitBalance(String.valueOf(map.get("frzSum")));
			fbz.setLmtID(String.valueOf(map.get("lmtID")));
			fbz.setMaturity(String.valueOf(map.get("maturity")));
			fbz.setOperTyp(String.valueOf(map.get("operTyp")));
			fbz.setContentType(String.valueOf(map.get("contentType")));
			fbz.setCustomerType(String.valueOf(map.get("customerType")));
			fbz.setSerialNo(String.valueOf(map.get("serialNo")));
			f = fbzService.insertFbz(fbz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return f;
	}
	public FBZ003 sendReq(HttpServletRequest request,FBZ003 fbz) throws Exception{
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ003.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		FBZ003 f=null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ002返回报文"+xmlDocAccept.asXML());
		        	f=(FBZ003) XmlUtilFactory.getBeanByXml(FBZ003.class, xmlDocAccept.asXML());
		        	byteArrayInputStream.close();
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return null;
		}
		return f;
	}

	public String parseXml(HttpServletRequest request,FBZ003 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",getDate());
			svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
			svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
			svc.put("tmStamp", getDateTime());
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");			
			body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
			//body.put("customerType", "0110");
			body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
			body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
			body.put("operTyp", fbz.getOperTyp()!=null?fbz.getOperTyp():"");
			body.put("frzcurrency", fbz.getCurrency()!=null?fbz.getCurrency():"");
			body.put("frzSum", fbz.getFrzSum()!=null?fbz.getFrzSum():"");
			body.put("lmtID", fbz.getLmtID()!=null?fbz.getLmtID():"");
			body.put("maturity", fbz.getMaturity()!=null?fbz.getMaturity():"");
			body.put("serialNo", fbz.getSerialNo()!=null?fbz.getSerialNo():"");
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(request.getSession().getServletContext().getRealPath("/")+ "/WEB-INF/classes/schema/FBZC0003.xml", hashmap);
	}
	public String getDateTime(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	
	@RequestMapping("checkCreditCust")
	@ResponseBody
	public boolean checkCreditCust(@RequestBody Map<String, Object> map) throws Exception{		
		return fbzService.checkCreditCust(map);
	}
}
