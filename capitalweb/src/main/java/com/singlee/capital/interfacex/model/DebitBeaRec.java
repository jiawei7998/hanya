package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DebitBeaRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DebitBeaRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DebitMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcyTypeDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TheirRefDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmtDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitBeaRec", propOrder = { "debitMedType", "acctNoDr",
		"fcyTypeDr", "theirRefDr", "amtDr" })
public class DebitBeaRec {

	@XmlElement(name = "DebitMedType", required = true)
	protected String debitMedType;
	@XmlElement(name = "AcctNoDr", required = true)
	protected String acctNoDr;
	@XmlElement(name = "FcyTypeDr", required = true)
	protected String fcyTypeDr;
	@XmlElement(name = "TheirRefDr", required = true)
	protected String theirRefDr;
	@XmlElement(name = "AmtDr", required = true)
	protected String amtDr;

	/**
	 * Gets the value of the debitMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitMedType() {
		return debitMedType;
	}

	/**
	 * Sets the value of the debitMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitMedType(String value) {
		this.debitMedType = value;
	}

	/**
	 * Gets the value of the acctNoDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoDr() {
		return acctNoDr;
	}

	/**
	 * Sets the value of the acctNoDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoDr(String value) {
		this.acctNoDr = value;
	}

	/**
	 * Gets the value of the fcyTypeDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcyTypeDr() {
		return fcyTypeDr;
	}

	/**
	 * Sets the value of the fcyTypeDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcyTypeDr(String value) {
		this.fcyTypeDr = value;
	}

	/**
	 * Gets the value of the theirRefDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTheirRefDr() {
		return theirRefDr;
	}

	/**
	 * Sets the value of the theirRefDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTheirRefDr(String value) {
		this.theirRefDr = value;
	}

	/**
	 * Gets the value of the amtDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmtDr() {
		return amtDr;
	}

	/**
	 * Sets the value of the amtDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmtDr(String value) {
		this.amtDr = value;
	}

}
