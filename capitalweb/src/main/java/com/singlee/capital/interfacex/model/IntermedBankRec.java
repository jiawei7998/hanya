package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for IntermedBankRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="IntermedBankRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntermedBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntermedBankRec", propOrder = { "intermedBank" })
public class IntermedBankRec {

	@XmlElement(name = "IntermedBank", required = true)
	protected String intermedBank;

	/**
	 * Gets the value of the intermedBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntermedBank() {
		return intermedBank;
	}

	/**
	 * Sets the value of the intermedBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntermedBank(String value) {
		this.intermedBank = value;
	}

}
