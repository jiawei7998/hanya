package com.singlee.capital.interfacex.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="BOS_ORG")
public class EcifBosOrg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String ecif_id                    					;
	private String base_ctg_tp_cd                       ;
	private String depositor_tp_cd                      ;
	private String sector_tp_cd                         ;
	private String t24_industry_tp_cd                   ;
	private String trader_tp_cd                         ;
	private String source_sys_tp_cd                     ;
	private String full_cn_name                         ;
	private String short_cn_name                        ;
	private String full_en_name                         ;
	private String short_en_name                        ;
	private String memo_name                            ;
	private String created_dt                           ;
	private String maintain_tp_cd                       ;
	private String cust_status_tp_cd                    ;
	private String status_modified_dt                   ;
	private String cust_manager_id                      ;
	private String first_created_teller_id              ;
	private String referral_teller_id                   ;
	private String first_created_org_id                 ;
	private String manager_org_id                       ;
	private String cust_source_tp_cd                    ;
	private String cust_source_introducer               ;
	private String taxation_official_area_tp_cd         ;
	private String inter_start_dt                       ;
	private String inter_stop_dt                        ;
	private String inter_official_level                 ;
	private String cust_level                           ;
	private String cust_level_standard_tp_cd            ;
	private String cust_level_start_dt                  ;
	private String cust_level_end_dt                    ;
	private String cust_bank_work_rela_tp_cd            ;
	private String cust_bank_trade_rela_tp_cd           ;
	private String first_default_dt                     ;
	private String latest_default_dt                    ;
	private String black_list_rsn_tp_cd                 ;
	private String ent_economic_tp_cd                   ;
	private String employee_number                      ;
	private String loc_gov_finc_tp_cd                   ;
	private String loc_gov_finc_admit_org_tp_cd         ;
	private String loc_gov_finc_bank_name               ;
	private String public_ent_tp_cd                     ;
	private String industry_status_tp_cd                ;
	private String credential_dt                        ;
	private String credential_tp_cd                     ;
	private String credential_verify_dt                 ;
	private String base_account_name                    ;
	private String base_account_number                  ;
	private String base_account_bank                    ;
	private String settlement_account_number            ;
	private String settlement_account_name              ;
	private String settlement_account_org               ;
	private String credit_rela_establish_dt             ;
	private String finance_org_tp_cd                    ;
	private String business_oper_area                   ;
	private String business_oper_owner_ratio            ;
	private String supply_industry_tp_cd                ;
	private String cust_start_operation_dt              ;
	private String industry_tp_cd                       ;
	private String agriculture_ent_level                ;
	private String other_ent_org_tp_cd                  ;
	private String ent_scope_basel_tp_cd                ;
	private String ent_scope_govern_tp_cd               ;
	private String ent_scope_govern_auto_tp_cd          ;
	private String ent_scope_shbank_tp_cd               ;
	private String establish_dt                         ;
	private String ent_holding_tp_cd                    ;
	private String ent_industry_tp_cd                   ;
	private String cn_employee_num                      ;
	private String mcc_tp_cd                            ;
	private String commerical_owner_num                 ;
	private String swift_num                            ;
	private String org_credit_code                      ;
	private String loan_card_num                        ;
	private String loan_card_apply_dt                   ;
	private String finance_org_num                      ;
	private String fina_org_legal_person_id             ;
	private String foreign_invst_number                 ;
	private String economy_area_name                    ;
	private String group_member_tp_cd                   ;
	private String group_member_order                   ;
	private String manager_expand_ratio                 ;
	private String bank_cust_acct_rela_tp_cd            ;
	private String evaluated_dt                         ;
	private String legal_cust_markt_level_tp_cd         ;
	private String shareholder_order                    ;
	private String cust_sponsor_tp_cd                   ;
	private String national_tax_registry_num            ;
	private String state_tax_registry_num               ;
	private String company_cust_business_scope          ;
	private String check_book_flag                      ;
	private String trans_acct_restrict_tp_cd            ;
	private String old_cust_id                          ;
	private String t24_comments                         ;
	private String main_manag_perf_ratio                ;
	private String nationality_tp_cd                    ;
	private String residence_tp_cd                      ;
	private String belong_city_tp_cd                    ;
	private String belong_province_tp_cd                ;
	private String area_code                            ;
	private String business_cooperation_tp_cd           ;
	private String agent_name                           ;
	private String business_income                      ;
	private String asset_summary                        ;
	private String steel_market_name                    ;
	private String t24_insert_success_flag              ;
	private String t24_update_success_flag              ;
	private String org_flag_id                          ;
	private String org_registry_id                      ;
	private String mark_target_tp_cd                    ;
	private String t24_cust_status_tp_cd                ;
	private String review_frequency                     ;
	private String customer_liability                   ;
	private String shb_language_tp_cd                   ;
	private String interest_tax_country_tp_cd           ;
	private String version_name                         ;
	private String other_cust_manager_id                ;
	private String cust_manager_name                    ;
	private String blacklist_src                        ;
	private String blacklist_tp_cd                      ;
	private String no_blacklist_time                    ;
	private String blacklist_comment                    ;
	private String contract_type                        ;
	private String agency_rela                          ;
	private String t24_enterprise_scope                 ;
	private String money_launder_rst_tp_cd              ;
	private String annual_inspection_index              ;
	private String annual_inspection_date               ;
	private String operating_area_ownership             ;
	private String financial_institution                ;
	private String swift_bic_num                        ;
	private String subject_type                         ;
	private String assets_total_amt_ccy                 ;
	private String area_type                            ;
	private String gold_customer_id                     ;
	private String remark_one                           ;
	private String remark_two                           ;
	private String remark_three                         ;
	private String operate_auth                         ;
	private String correlation_id                       ;
	private String transaction_id                       ;
	private String transaction_type                     ;
	private String orgn_num                             ;
	private String last_update_dt                       ;
	private String last_update_tx_id                    ;
	private String last_update_user                     ;
	private String t24_customer_flag                    ;
	private String first_created_channel_id             ;
	private String last_modify_channel_id               ;
	private String o_reserved_1                         ;
	private String o_reserved_2                         ;
	private String o_reserved_3                         ;
	private String o_reserved_4                         ;
	private String o_reserved_5                         ;
	private String o_reserved_6                         ;
	private String o_reserved_7                         ;
	private String o_reserved_8                         ;
	private String o_reserved_9                         ;
	private String o_reserved_10                        ;
	private String o_reserved_11                        ;
	private String o_reserved_12                        ;
	private String o_reserved_13                        ;
	private String o_reserved_14                        ;
	private String o_reserved_15                        ;
	private String o_reserved_16                        ;
	private String o_reserved_17                        ;
	private String o_reserved_18                        ;
	private String o_reserved_19                        ;
	private String o_reserved_20                        ;
	private String taxpayer_type                        ;
	private String sys_attach_flg                       ;
	public String getEcif_id() {
		return ecif_id;
	}
	public void setEcif_id(String ecifId) {
		ecif_id = ecifId;
	}
	public String getBase_ctg_tp_cd() {
		return base_ctg_tp_cd;
	}
	public void setBase_ctg_tp_cd(String baseCtgTpCd) {
		base_ctg_tp_cd = baseCtgTpCd;
	}
	public String getDepositor_tp_cd() {
		return depositor_tp_cd;
	}
	public void setDepositor_tp_cd(String depositorTpCd) {
		depositor_tp_cd = depositorTpCd;
	}
	public String getSector_tp_cd() {
		return sector_tp_cd;
	}
	public void setSector_tp_cd(String sectorTpCd) {
		sector_tp_cd = sectorTpCd;
	}
	public String getT24_industry_tp_cd() {
		return t24_industry_tp_cd;
	}
	public void setT24_industry_tp_cd(String t24IndustryTpCd) {
		t24_industry_tp_cd = t24IndustryTpCd;
	}
	public String getTrader_tp_cd() {
		return trader_tp_cd;
	}
	public void setTrader_tp_cd(String traderTpCd) {
		trader_tp_cd = traderTpCd;
	}
	public String getSource_sys_tp_cd() {
		return source_sys_tp_cd;
	}
	public void setSource_sys_tp_cd(String sourceSysTpCd) {
		source_sys_tp_cd = sourceSysTpCd;
	}
	public String getFull_cn_name() {
		return full_cn_name;
	}
	public void setFull_cn_name(String fullCnName) {
		full_cn_name = fullCnName;
	}
	public String getShort_cn_name() {
		return short_cn_name;
	}
	public void setShort_cn_name(String shortCnName) {
		short_cn_name = shortCnName;
	}
	public String getFull_en_name() {
		return full_en_name;
	}
	public void setFull_en_name(String fullEnName) {
		full_en_name = fullEnName;
	}
	public String getShort_en_name() {
		return short_en_name;
	}
	public void setShort_en_name(String shortEnName) {
		short_en_name = shortEnName;
	}
	public String getMemo_name() {
		return memo_name;
	}
	public void setMemo_name(String memoName) {
		memo_name = memoName;
	}
	public String getCreated_dt() {
		return created_dt;
	}
	public void setCreated_dt(String createdDt) {
		created_dt = createdDt;
	}
	public String getMaintain_tp_cd() {
		return maintain_tp_cd;
	}
	public void setMaintain_tp_cd(String maintainTpCd) {
		maintain_tp_cd = maintainTpCd;
	}
	public String getCust_status_tp_cd() {
		return cust_status_tp_cd;
	}
	public void setCust_status_tp_cd(String custStatusTpCd) {
		cust_status_tp_cd = custStatusTpCd;
	}
	public String getStatus_modified_dt() {
		return status_modified_dt;
	}
	public void setStatus_modified_dt(String statusModifiedDt) {
		status_modified_dt = statusModifiedDt;
	}
	public String getCust_manager_id() {
		return cust_manager_id;
	}
	public void setCust_manager_id(String custManagerId) {
		cust_manager_id = custManagerId;
	}
	public String getFirst_created_teller_id() {
		return first_created_teller_id;
	}
	public void setFirst_created_teller_id(String firstCreatedTellerId) {
		first_created_teller_id = firstCreatedTellerId;
	}
	public String getReferral_teller_id() {
		return referral_teller_id;
	}
	public void setReferral_teller_id(String referralTellerId) {
		referral_teller_id = referralTellerId;
	}
	public String getFirst_created_org_id() {
		return first_created_org_id;
	}
	public void setFirst_created_org_id(String firstCreatedOrgId) {
		first_created_org_id = firstCreatedOrgId;
	}
	public String getManager_org_id() {
		return manager_org_id;
	}
	public void setManager_org_id(String managerOrgId) {
		manager_org_id = managerOrgId;
	}
	public String getCust_source_tp_cd() {
		return cust_source_tp_cd;
	}
	public void setCust_source_tp_cd(String custSourceTpCd) {
		cust_source_tp_cd = custSourceTpCd;
	}
	public String getCust_source_introducer() {
		return cust_source_introducer;
	}
	public void setCust_source_introducer(String custSourceIntroducer) {
		cust_source_introducer = custSourceIntroducer;
	}
	public String getTaxation_official_area_tp_cd() {
		return taxation_official_area_tp_cd;
	}
	public void setTaxation_official_area_tp_cd(String taxationOfficialAreaTpCd) {
		taxation_official_area_tp_cd = taxationOfficialAreaTpCd;
	}
	public String getInter_start_dt() {
		return inter_start_dt;
	}
	public void setInter_start_dt(String interStartDt) {
		inter_start_dt = interStartDt;
	}
	public String getInter_stop_dt() {
		return inter_stop_dt;
	}
	public void setInter_stop_dt(String interStopDt) {
		inter_stop_dt = interStopDt;
	}
	public String getInter_official_level() {
		return inter_official_level;
	}
	public void setInter_official_level(String interOfficialLevel) {
		inter_official_level = interOfficialLevel;
	}
	public String getCust_level() {
		return cust_level;
	}
	public void setCust_level(String custLevel) {
		cust_level = custLevel;
	}
	public String getCust_level_standard_tp_cd() {
		return cust_level_standard_tp_cd;
	}
	public void setCust_level_standard_tp_cd(String custLevelStandardTpCd) {
		cust_level_standard_tp_cd = custLevelStandardTpCd;
	}
	public String getCust_level_start_dt() {
		return cust_level_start_dt;
	}
	public void setCust_level_start_dt(String custLevelStartDt) {
		cust_level_start_dt = custLevelStartDt;
	}
	public String getCust_level_end_dt() {
		return cust_level_end_dt;
	}
	public void setCust_level_end_dt(String custLevelEndDt) {
		cust_level_end_dt = custLevelEndDt;
	}
	public String getCust_bank_work_rela_tp_cd() {
		return cust_bank_work_rela_tp_cd;
	}
	public void setCust_bank_work_rela_tp_cd(String custBankWorkRelaTpCd) {
		cust_bank_work_rela_tp_cd = custBankWorkRelaTpCd;
	}
	public String getCust_bank_trade_rela_tp_cd() {
		return cust_bank_trade_rela_tp_cd;
	}
	public void setCust_bank_trade_rela_tp_cd(String custBankTradeRelaTpCd) {
		cust_bank_trade_rela_tp_cd = custBankTradeRelaTpCd;
	}
	public String getFirst_default_dt() {
		return first_default_dt;
	}
	public void setFirst_default_dt(String firstDefaultDt) {
		first_default_dt = firstDefaultDt;
	}
	public String getLatest_default_dt() {
		return latest_default_dt;
	}
	public void setLatest_default_dt(String latestDefaultDt) {
		latest_default_dt = latestDefaultDt;
	}
	public String getBlack_list_rsn_tp_cd() {
		return black_list_rsn_tp_cd;
	}
	public void setBlack_list_rsn_tp_cd(String blackListRsnTpCd) {
		black_list_rsn_tp_cd = blackListRsnTpCd;
	}
	public String getEnt_economic_tp_cd() {
		return ent_economic_tp_cd;
	}
	public void setEnt_economic_tp_cd(String entEconomicTpCd) {
		ent_economic_tp_cd = entEconomicTpCd;
	}
	public String getEmployee_number() {
		return employee_number;
	}
	public void setEmployee_number(String employeeNumber) {
		employee_number = employeeNumber;
	}
	public String getLoc_gov_finc_tp_cd() {
		return loc_gov_finc_tp_cd;
	}
	public void setLoc_gov_finc_tp_cd(String locGovFincTpCd) {
		loc_gov_finc_tp_cd = locGovFincTpCd;
	}
	public String getLoc_gov_finc_admit_org_tp_cd() {
		return loc_gov_finc_admit_org_tp_cd;
	}
	public void setLoc_gov_finc_admit_org_tp_cd(String locGovFincAdmitOrgTpCd) {
		loc_gov_finc_admit_org_tp_cd = locGovFincAdmitOrgTpCd;
	}
	public String getLoc_gov_finc_bank_name() {
		return loc_gov_finc_bank_name;
	}
	public void setLoc_gov_finc_bank_name(String locGovFincBankName) {
		loc_gov_finc_bank_name = locGovFincBankName;
	}
	public String getPublic_ent_tp_cd() {
		return public_ent_tp_cd;
	}
	public void setPublic_ent_tp_cd(String publicEntTpCd) {
		public_ent_tp_cd = publicEntTpCd;
	}
	public String getIndustry_status_tp_cd() {
		return industry_status_tp_cd;
	}
	public void setIndustry_status_tp_cd(String industryStatusTpCd) {
		industry_status_tp_cd = industryStatusTpCd;
	}
	public String getCredential_dt() {
		return credential_dt;
	}
	public void setCredential_dt(String credentialDt) {
		credential_dt = credentialDt;
	}
	public String getCredential_tp_cd() {
		return credential_tp_cd;
	}
	public void setCredential_tp_cd(String credentialTpCd) {
		credential_tp_cd = credentialTpCd;
	}
	public String getCredential_verify_dt() {
		return credential_verify_dt;
	}
	public void setCredential_verify_dt(String credentialVerifyDt) {
		credential_verify_dt = credentialVerifyDt;
	}
	public String getBase_account_name() {
		return base_account_name;
	}
	public void setBase_account_name(String baseAccountName) {
		base_account_name = baseAccountName;
	}
	public String getBase_account_number() {
		return base_account_number;
	}
	public void setBase_account_number(String baseAccountNumber) {
		base_account_number = baseAccountNumber;
	}
	public String getBase_account_bank() {
		return base_account_bank;
	}
	public void setBase_account_bank(String baseAccountBank) {
		base_account_bank = baseAccountBank;
	}
	public String getSettlement_account_number() {
		return settlement_account_number;
	}
	public void setSettlement_account_number(String settlementAccountNumber) {
		settlement_account_number = settlementAccountNumber;
	}
	public String getSettlement_account_name() {
		return settlement_account_name;
	}
	public void setSettlement_account_name(String settlementAccountName) {
		settlement_account_name = settlementAccountName;
	}
	public String getSettlement_account_org() {
		return settlement_account_org;
	}
	public void setSettlement_account_org(String settlementAccountOrg) {
		settlement_account_org = settlementAccountOrg;
	}
	public String getCredit_rela_establish_dt() {
		return credit_rela_establish_dt;
	}
	public void setCredit_rela_establish_dt(String creditRelaEstablishDt) {
		credit_rela_establish_dt = creditRelaEstablishDt;
	}
	public String getFinance_org_tp_cd() {
		return finance_org_tp_cd;
	}
	public void setFinance_org_tp_cd(String financeOrgTpCd) {
		finance_org_tp_cd = financeOrgTpCd;
	}
	public String getBusiness_oper_area() {
		return business_oper_area;
	}
	public void setBusiness_oper_area(String businessOperArea) {
		business_oper_area = businessOperArea;
	}
	public String getBusiness_oper_owner_ratio() {
		return business_oper_owner_ratio;
	}
	public void setBusiness_oper_owner_ratio(String businessOperOwnerRatio) {
		business_oper_owner_ratio = businessOperOwnerRatio;
	}
	public String getSupply_industry_tp_cd() {
		return supply_industry_tp_cd;
	}
	public void setSupply_industry_tp_cd(String supplyIndustryTpCd) {
		supply_industry_tp_cd = supplyIndustryTpCd;
	}
	public String getCust_start_operation_dt() {
		return cust_start_operation_dt;
	}
	public void setCust_start_operation_dt(String custStartOperationDt) {
		cust_start_operation_dt = custStartOperationDt;
	}
	public String getIndustry_tp_cd() {
		return industry_tp_cd;
	}
	public void setIndustry_tp_cd(String industryTpCd) {
		industry_tp_cd = industryTpCd;
	}
	public String getAgriculture_ent_level() {
		return agriculture_ent_level;
	}
	public void setAgriculture_ent_level(String agricultureEntLevel) {
		agriculture_ent_level = agricultureEntLevel;
	}
	public String getOther_ent_org_tp_cd() {
		return other_ent_org_tp_cd;
	}
	public void setOther_ent_org_tp_cd(String otherEntOrgTpCd) {
		other_ent_org_tp_cd = otherEntOrgTpCd;
	}
	public String getEnt_scope_basel_tp_cd() {
		return ent_scope_basel_tp_cd;
	}
	public void setEnt_scope_basel_tp_cd(String entScopeBaselTpCd) {
		ent_scope_basel_tp_cd = entScopeBaselTpCd;
	}
	public String getEnt_scope_govern_tp_cd() {
		return ent_scope_govern_tp_cd;
	}
	public void setEnt_scope_govern_tp_cd(String entScopeGovernTpCd) {
		ent_scope_govern_tp_cd = entScopeGovernTpCd;
	}
	public String getEnt_scope_govern_auto_tp_cd() {
		return ent_scope_govern_auto_tp_cd;
	}
	public void setEnt_scope_govern_auto_tp_cd(String entScopeGovernAutoTpCd) {
		ent_scope_govern_auto_tp_cd = entScopeGovernAutoTpCd;
	}
	public String getEnt_scope_shbank_tp_cd() {
		return ent_scope_shbank_tp_cd;
	}
	public void setEnt_scope_shbank_tp_cd(String entScopeShbankTpCd) {
		ent_scope_shbank_tp_cd = entScopeShbankTpCd;
	}
	public String getEstablish_dt() {
		return establish_dt;
	}
	public void setEstablish_dt(String establishDt) {
		establish_dt = establishDt;
	}
	public String getEnt_holding_tp_cd() {
		return ent_holding_tp_cd;
	}
	public void setEnt_holding_tp_cd(String entHoldingTpCd) {
		ent_holding_tp_cd = entHoldingTpCd;
	}
	public String getEnt_industry_tp_cd() {
		return ent_industry_tp_cd;
	}
	public void setEnt_industry_tp_cd(String entIndustryTpCd) {
		ent_industry_tp_cd = entIndustryTpCd;
	}
	public String getCn_employee_num() {
		return cn_employee_num;
	}
	public void setCn_employee_num(String cnEmployeeNum) {
		cn_employee_num = cnEmployeeNum;
	}
	public String getMcc_tp_cd() {
		return mcc_tp_cd;
	}
	public void setMcc_tp_cd(String mccTpCd) {
		mcc_tp_cd = mccTpCd;
	}
	public String getCommerical_owner_num() {
		return commerical_owner_num;
	}
	public void setCommerical_owner_num(String commericalOwnerNum) {
		commerical_owner_num = commericalOwnerNum;
	}
	public String getSwift_num() {
		return swift_num;
	}
	public void setSwift_num(String swiftNum) {
		swift_num = swiftNum;
	}
	public String getOrg_credit_code() {
		return org_credit_code;
	}
	public void setOrg_credit_code(String orgCreditCode) {
		org_credit_code = orgCreditCode;
	}
	public String getLoan_card_num() {
		return loan_card_num;
	}
	public void setLoan_card_num(String loanCardNum) {
		loan_card_num = loanCardNum;
	}
	public String getLoan_card_apply_dt() {
		return loan_card_apply_dt;
	}
	public void setLoan_card_apply_dt(String loanCardApplyDt) {
		loan_card_apply_dt = loanCardApplyDt;
	}
	public String getFinance_org_num() {
		return finance_org_num;
	}
	public void setFinance_org_num(String financeOrgNum) {
		finance_org_num = financeOrgNum;
	}
	public String getFina_org_legal_person_id() {
		return fina_org_legal_person_id;
	}
	public void setFina_org_legal_person_id(String finaOrgLegalPersonId) {
		fina_org_legal_person_id = finaOrgLegalPersonId;
	}
	public String getForeign_invst_number() {
		return foreign_invst_number;
	}
	public void setForeign_invst_number(String foreignInvstNumber) {
		foreign_invst_number = foreignInvstNumber;
	}
	public String getEconomy_area_name() {
		return economy_area_name;
	}
	public void setEconomy_area_name(String economyAreaName) {
		economy_area_name = economyAreaName;
	}
	public String getGroup_member_tp_cd() {
		return group_member_tp_cd;
	}
	public void setGroup_member_tp_cd(String groupMemberTpCd) {
		group_member_tp_cd = groupMemberTpCd;
	}
	public String getGroup_member_order() {
		return group_member_order;
	}
	public void setGroup_member_order(String groupMemberOrder) {
		group_member_order = groupMemberOrder;
	}
	public String getManager_expand_ratio() {
		return manager_expand_ratio;
	}
	public void setManager_expand_ratio(String managerExpandRatio) {
		manager_expand_ratio = managerExpandRatio;
	}
	public String getBank_cust_acct_rela_tp_cd() {
		return bank_cust_acct_rela_tp_cd;
	}
	public void setBank_cust_acct_rela_tp_cd(String bankCustAcctRelaTpCd) {
		bank_cust_acct_rela_tp_cd = bankCustAcctRelaTpCd;
	}
	public String getEvaluated_dt() {
		return evaluated_dt;
	}
	public void setEvaluated_dt(String evaluatedDt) {
		evaluated_dt = evaluatedDt;
	}
	public String getLegal_cust_markt_level_tp_cd() {
		return legal_cust_markt_level_tp_cd;
	}
	public void setLegal_cust_markt_level_tp_cd(String legalCustMarktLevelTpCd) {
		legal_cust_markt_level_tp_cd = legalCustMarktLevelTpCd;
	}
	public String getShareholder_order() {
		return shareholder_order;
	}
	public void setShareholder_order(String shareholderOrder) {
		shareholder_order = shareholderOrder;
	}
	public String getCust_sponsor_tp_cd() {
		return cust_sponsor_tp_cd;
	}
	public void setCust_sponsor_tp_cd(String custSponsorTpCd) {
		cust_sponsor_tp_cd = custSponsorTpCd;
	}
	public String getNational_tax_registry_num() {
		return national_tax_registry_num;
	}
	public void setNational_tax_registry_num(String nationalTaxRegistryNum) {
		national_tax_registry_num = nationalTaxRegistryNum;
	}
	public String getState_tax_registry_num() {
		return state_tax_registry_num;
	}
	public void setState_tax_registry_num(String stateTaxRegistryNum) {
		state_tax_registry_num = stateTaxRegistryNum;
	}
	public String getCompany_cust_business_scope() {
		return company_cust_business_scope;
	}
	public void setCompany_cust_business_scope(String companyCustBusinessScope) {
		company_cust_business_scope = companyCustBusinessScope;
	}
	public String getCheck_book_flag() {
		return check_book_flag;
	}
	public void setCheck_book_flag(String checkBookFlag) {
		check_book_flag = checkBookFlag;
	}
	public String getTrans_acct_restrict_tp_cd() {
		return trans_acct_restrict_tp_cd;
	}
	public void setTrans_acct_restrict_tp_cd(String transAcctRestrictTpCd) {
		trans_acct_restrict_tp_cd = transAcctRestrictTpCd;
	}
	public String getOld_cust_id() {
		return old_cust_id;
	}
	public void setOld_cust_id(String oldCustId) {
		old_cust_id = oldCustId;
	}
	public String getT24_comments() {
		return t24_comments;
	}
	public void setT24_comments(String t24Comments) {
		t24_comments = t24Comments;
	}
	public String getMain_manag_perf_ratio() {
		return main_manag_perf_ratio;
	}
	public void setMain_manag_perf_ratio(String mainManagPerfRatio) {
		main_manag_perf_ratio = mainManagPerfRatio;
	}
	public String getNationality_tp_cd() {
		return nationality_tp_cd;
	}
	public void setNationality_tp_cd(String nationalityTpCd) {
		nationality_tp_cd = nationalityTpCd;
	}
	public String getResidence_tp_cd() {
		return residence_tp_cd;
	}
	public void setResidence_tp_cd(String residenceTpCd) {
		residence_tp_cd = residenceTpCd;
	}
	public String getBelong_city_tp_cd() {
		return belong_city_tp_cd;
	}
	public void setBelong_city_tp_cd(String belongCityTpCd) {
		belong_city_tp_cd = belongCityTpCd;
	}
	public String getBelong_province_tp_cd() {
		return belong_province_tp_cd;
	}
	public void setBelong_province_tp_cd(String belongProvinceTpCd) {
		belong_province_tp_cd = belongProvinceTpCd;
	}
	public String getArea_code() {
		return area_code;
	}
	public void setArea_code(String areaCode) {
		area_code = areaCode;
	}
	public String getBusiness_cooperation_tp_cd() {
		return business_cooperation_tp_cd;
	}
	public void setBusiness_cooperation_tp_cd(String businessCooperationTpCd) {
		business_cooperation_tp_cd = businessCooperationTpCd;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agentName) {
		agent_name = agentName;
	}
	public String getBusiness_income() {
		return business_income;
	}
	public void setBusiness_income(String businessIncome) {
		business_income = businessIncome;
	}
	public String getAsset_summary() {
		return asset_summary;
	}
	public void setAsset_summary(String assetSummary) {
		asset_summary = assetSummary;
	}
	public String getSteel_market_name() {
		return steel_market_name;
	}
	public void setSteel_market_name(String steelMarketName) {
		steel_market_name = steelMarketName;
	}
	public String getT24_insert_success_flag() {
		return t24_insert_success_flag;
	}
	public void setT24_insert_success_flag(String t24InsertSuccessFlag) {
		t24_insert_success_flag = t24InsertSuccessFlag;
	}
	public String getT24_update_success_flag() {
		return t24_update_success_flag;
	}
	public void setT24_update_success_flag(String t24UpdateSuccessFlag) {
		t24_update_success_flag = t24UpdateSuccessFlag;
	}
	public String getOrg_flag_id() {
		return org_flag_id;
	}
	public void setOrg_flag_id(String orgFlagId) {
		org_flag_id = orgFlagId;
	}
	public String getOrg_registry_id() {
		return org_registry_id;
	}
	public void setOrg_registry_id(String orgRegistryId) {
		org_registry_id = orgRegistryId;
	}
	public String getMark_target_tp_cd() {
		return mark_target_tp_cd;
	}
	public void setMark_target_tp_cd(String markTargetTpCd) {
		mark_target_tp_cd = markTargetTpCd;
	}
	public String getT24_cust_status_tp_cd() {
		return t24_cust_status_tp_cd;
	}
	public void setT24_cust_status_tp_cd(String t24CustStatusTpCd) {
		t24_cust_status_tp_cd = t24CustStatusTpCd;
	}
	public String getReview_frequency() {
		return review_frequency;
	}
	public void setReview_frequency(String reviewFrequency) {
		review_frequency = reviewFrequency;
	}
	public String getCustomer_liability() {
		return customer_liability;
	}
	public void setCustomer_liability(String customerLiability) {
		customer_liability = customerLiability;
	}
	public String getShb_language_tp_cd() {
		return shb_language_tp_cd;
	}
	public void setShb_language_tp_cd(String shbLanguageTpCd) {
		shb_language_tp_cd = shbLanguageTpCd;
	}
	public String getInterest_tax_country_tp_cd() {
		return interest_tax_country_tp_cd;
	}
	public void setInterest_tax_country_tp_cd(String interestTaxCountryTpCd) {
		interest_tax_country_tp_cd = interestTaxCountryTpCd;
	}
	public String getVersion_name() {
		return version_name;
	}
	public void setVersion_name(String versionName) {
		version_name = versionName;
	}
	public String getOther_cust_manager_id() {
		return other_cust_manager_id;
	}
	public void setOther_cust_manager_id(String otherCustManagerId) {
		other_cust_manager_id = otherCustManagerId;
	}
	public String getCust_manager_name() {
		return cust_manager_name;
	}
	public void setCust_manager_name(String custManagerName) {
		cust_manager_name = custManagerName;
	}
	public String getBlacklist_src() {
		return blacklist_src;
	}
	public void setBlacklist_src(String blacklistSrc) {
		blacklist_src = blacklistSrc;
	}
	public String getBlacklist_tp_cd() {
		return blacklist_tp_cd;
	}
	public void setBlacklist_tp_cd(String blacklistTpCd) {
		blacklist_tp_cd = blacklistTpCd;
	}
	public String getNo_blacklist_time() {
		return no_blacklist_time;
	}
	public void setNo_blacklist_time(String noBlacklistTime) {
		no_blacklist_time = noBlacklistTime;
	}
	public String getBlacklist_comment() {
		return blacklist_comment;
	}
	public void setBlacklist_comment(String blacklistComment) {
		blacklist_comment = blacklistComment;
	}
	public String getContract_type() {
		return contract_type;
	}
	public void setContract_type(String contractType) {
		contract_type = contractType;
	}
	public String getAgency_rela() {
		return agency_rela;
	}
	public void setAgency_rela(String agencyRela) {
		agency_rela = agencyRela;
	}
	public String getT24_enterprise_scope() {
		return t24_enterprise_scope;
	}
	public void setT24_enterprise_scope(String t24EnterpriseScope) {
		t24_enterprise_scope = t24EnterpriseScope;
	}
	public String getMoney_launder_rst_tp_cd() {
		return money_launder_rst_tp_cd;
	}
	public void setMoney_launder_rst_tp_cd(String moneyLaunderRstTpCd) {
		money_launder_rst_tp_cd = moneyLaunderRstTpCd;
	}
	public String getAnnual_inspection_index() {
		return annual_inspection_index;
	}
	public void setAnnual_inspection_index(String annualInspectionIndex) {
		annual_inspection_index = annualInspectionIndex;
	}
	public String getAnnual_inspection_date() {
		return annual_inspection_date;
	}
	public void setAnnual_inspection_date(String annualInspectionDate) {
		annual_inspection_date = annualInspectionDate;
	}
	public String getOperating_area_ownership() {
		return operating_area_ownership;
	}
	public void setOperating_area_ownership(String operatingAreaOwnership) {
		operating_area_ownership = operatingAreaOwnership;
	}
	public String getFinancial_institution() {
		return financial_institution;
	}
	public void setFinancial_institution(String financialInstitution) {
		financial_institution = financialInstitution;
	}
	public String getSwift_bic_num() {
		return swift_bic_num;
	}
	public void setSwift_bic_num(String swiftBicNum) {
		swift_bic_num = swiftBicNum;
	}
	public String getSubject_type() {
		return subject_type;
	}
	public void setSubject_type(String subjectType) {
		subject_type = subjectType;
	}
	public String getAssets_total_amt_ccy() {
		return assets_total_amt_ccy;
	}
	public void setAssets_total_amt_ccy(String assetsTotalAmtCcy) {
		assets_total_amt_ccy = assetsTotalAmtCcy;
	}
	public String getArea_type() {
		return area_type;
	}
	public void setArea_type(String areaType) {
		area_type = areaType;
	}
	public String getGold_customer_id() {
		return gold_customer_id;
	}
	public void setGold_customer_id(String goldCustomerId) {
		gold_customer_id = goldCustomerId;
	}
	public String getRemark_one() {
		return remark_one;
	}
	public void setRemark_one(String remarkOne) {
		remark_one = remarkOne;
	}
	public String getRemark_two() {
		return remark_two;
	}
	public void setRemark_two(String remarkTwo) {
		remark_two = remarkTwo;
	}
	public String getRemark_three() {
		return remark_three;
	}
	public void setRemark_three(String remarkThree) {
		remark_three = remarkThree;
	}
	public String getOperate_auth() {
		return operate_auth;
	}
	public void setOperate_auth(String operateAuth) {
		operate_auth = operateAuth;
	}
	public String getCorrelation_id() {
		return correlation_id;
	}
	public void setCorrelation_id(String correlationId) {
		correlation_id = correlationId;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transactionId) {
		transaction_id = transactionId;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transactionType) {
		transaction_type = transactionType;
	}
	public String getOrgn_num() {
		return orgn_num;
	}
	public void setOrgn_num(String orgnNum) {
		orgn_num = orgnNum;
	}
	public String getLast_update_dt() {
		return last_update_dt;
	}
	public void setLast_update_dt(String lastUpdateDt) {
		last_update_dt = lastUpdateDt;
	}
	public String getLast_update_tx_id() {
		return last_update_tx_id;
	}
	public void setLast_update_tx_id(String lastUpdateTxId) {
		last_update_tx_id = lastUpdateTxId;
	}
	public String getLast_update_user() {
		return last_update_user;
	}
	public void setLast_update_user(String lastUpdateUser) {
		last_update_user = lastUpdateUser;
	}
	public String getT24_customer_flag() {
		return t24_customer_flag;
	}
	public void setT24_customer_flag(String t24CustomerFlag) {
		t24_customer_flag = t24CustomerFlag;
	}
	public String getFirst_created_channel_id() {
		return first_created_channel_id;
	}
	public void setFirst_created_channel_id(String firstCreatedChannelId) {
		first_created_channel_id = firstCreatedChannelId;
	}
	public String getLast_modify_channel_id() {
		return last_modify_channel_id;
	}
	public void setLast_modify_channel_id(String lastModifyChannelId) {
		last_modify_channel_id = lastModifyChannelId;
	}
	public String getO_reserved_1() {
		return o_reserved_1;
	}
	public void setO_reserved_1(String oReserved_1) {
		o_reserved_1 = oReserved_1;
	}
	public String getO_reserved_2() {
		return o_reserved_2;
	}
	public void setO_reserved_2(String oReserved_2) {
		o_reserved_2 = oReserved_2;
	}
	public String getO_reserved_3() {
		return o_reserved_3;
	}
	public void setO_reserved_3(String oReserved_3) {
		o_reserved_3 = oReserved_3;
	}
	public String getO_reserved_4() {
		return o_reserved_4;
	}
	public void setO_reserved_4(String oReserved_4) {
		o_reserved_4 = oReserved_4;
	}
	public String getO_reserved_5() {
		return o_reserved_5;
	}
	public void setO_reserved_5(String oReserved_5) {
		o_reserved_5 = oReserved_5;
	}
	public String getO_reserved_6() {
		return o_reserved_6;
	}
	public void setO_reserved_6(String oReserved_6) {
		o_reserved_6 = oReserved_6;
	}
	public String getO_reserved_7() {
		return o_reserved_7;
	}
	public void setO_reserved_7(String oReserved_7) {
		o_reserved_7 = oReserved_7;
	}
	public String getO_reserved_8() {
		return o_reserved_8;
	}
	public void setO_reserved_8(String oReserved_8) {
		o_reserved_8 = oReserved_8;
	}
	public String getO_reserved_9() {
		return o_reserved_9;
	}
	public void setO_reserved_9(String oReserved_9) {
		o_reserved_9 = oReserved_9;
	}
	public String getO_reserved_10() {
		return o_reserved_10;
	}
	public void setO_reserved_10(String oReserved_10) {
		o_reserved_10 = oReserved_10;
	}
	public String getO_reserved_11() {
		return o_reserved_11;
	}
	public void setO_reserved_11(String oReserved_11) {
		o_reserved_11 = oReserved_11;
	}
	public String getO_reserved_12() {
		return o_reserved_12;
	}
	public void setO_reserved_12(String oReserved_12) {
		o_reserved_12 = oReserved_12;
	}
	public String getO_reserved_13() {
		return o_reserved_13;
	}
	public void setO_reserved_13(String oReserved_13) {
		o_reserved_13 = oReserved_13;
	}
	public String getO_reserved_14() {
		return o_reserved_14;
	}
	public void setO_reserved_14(String oReserved_14) {
		o_reserved_14 = oReserved_14;
	}
	public String getO_reserved_15() {
		return o_reserved_15;
	}
	public void setO_reserved_15(String oReserved_15) {
		o_reserved_15 = oReserved_15;
	}
	public String getO_reserved_16() {
		return o_reserved_16;
	}
	public void setO_reserved_16(String oReserved_16) {
		o_reserved_16 = oReserved_16;
	}
	public String getO_reserved_17() {
		return o_reserved_17;
	}
	public void setO_reserved_17(String oReserved_17) {
		o_reserved_17 = oReserved_17;
	}
	public String getO_reserved_18() {
		return o_reserved_18;
	}
	public void setO_reserved_18(String oReserved_18) {
		o_reserved_18 = oReserved_18;
	}
	public String getO_reserved_19() {
		return o_reserved_19;
	}
	public void setO_reserved_19(String oReserved_19) {
		o_reserved_19 = oReserved_19;
	}
	public String getO_reserved_20() {
		return o_reserved_20;
	}
	public void setO_reserved_20(String oReserved_20) {
		o_reserved_20 = oReserved_20;
	}
	public String getTaxpayer_type() {
		return taxpayer_type;
	}
	public void setTaxpayer_type(String taxpayerType) {
		taxpayer_type = taxpayerType;
	}
	public String getSys_attach_flg() {
		return sys_attach_flg;
	}
	public void setSys_attach_flg(String sysAttachFlg) {
		sys_attach_flg = sysAttachFlg;
	}
}
