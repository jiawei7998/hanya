package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service("prodAppPrintServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProdAppPrintServiceImpl implements AssemblyDataService{
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private FlowController flowController; 
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		
		try{
			NumberFormat n = NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			map.put("dealNo", id);
			TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
			map.put("tdate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			map.put("dealNo",td.getDealNo());
			map.put("contractRate", df.format(100*td.getContractRate()));
			map.put("vDate", td.getvDate());
			map.put("mDate",td.getmDate());
			String cno=td.getcNo();
			CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
			map.put("party_name", cp.getParty_name());
			//map.put("conTitle", td.getConTitle());
			//map.put("chargeFeeamt", td.getChargeFeeamt());
			map.put("amt", n.format(td.getAmt()));
			map.put("term", td.getTerm());
			map.put("remarkNote", td.getRemarkNote());
			String Sponsor=td.getSponsor();
			TaUser u=taUserMapper.selectUser(Sponsor);
			map.put("sponsor",u.getUserName());
			
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}

}
