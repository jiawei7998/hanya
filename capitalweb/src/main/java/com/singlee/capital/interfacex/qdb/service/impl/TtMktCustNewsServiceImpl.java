package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.mapper.TtMktCustNewsMapper;
import com.singlee.capital.interfacex.qdb.model.TtMktCustNews;
import com.singlee.capital.interfacex.qdb.service.TtMktCustNewsService;

@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service
public class TtMktCustNewsServiceImpl implements TtMktCustNewsService{
	@Autowired
	private TtMktCustNewsMapper ttMktCustNewsMapper;
	@Resource
	private DayendDateService dateService;
	private static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	
	@Override
	public Page<TtMktCustNews> sreachTtMktCustNews(Map<String, Object> param) {
		Page<TtMktCustNews> pages = ttMktCustNewsMapper.sreachTtMktCustNews(param, ParameterUtil.getRowBounds(param));
		return pages;
	}

	@Override
	public boolean setTdBaseAssetPriceByExcel(String file) {
		try {
			ExcelUtil excelUtil = new ExcelUtil(file);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
			List<TtMktCustNews> list  = new ArrayList<TtMktCustNews>();
			for (int i = 1; i <= rowNum; i++) {
				TtMktCustNews tdBaseAsset = new TtMktCustNews();
				row = sheet.getRow(i);
				tdBaseAsset.setImportDate(row.getCell(0).getStringCellValue());
				tdBaseAsset.setCno(row.getCell(1).getStringCellValue());
				tdBaseAsset.setCname(row.getCell(2).getStringCellValue());
				tdBaseAsset.setTitle(row.getCell(3).getStringCellValue());
				tdBaseAsset.setNews(row.getCell(4).getStringCellValue());
				list.add(tdBaseAsset);
				if (i%1000 == 0) {
					ttMktCustNewsMapper.insertTtMktCustNewsExcel(list);
					list = new ArrayList<TtMktCustNews>();
				}
			}
			ttMktCustNewsMapper.insertTtMktCustNewsExcel(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 手工导入
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String setStockPriceByExcel(String type, InputStream is) {
		StringBuffer sb = new StringBuffer();
		Sheet sheet = null;
		try {
			ExcelUtil excelUtil = new ExcelUtil(type,is);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			sheet = excelUtil.getSheetAt(0);
			Row row;
			List<TtMktCustNews> list  = new ArrayList<TtMktCustNews>();
			TtMktCustNews tdBaseAsset =null;
			String date=dateService.getDayendDate();
			Cell cell=null;
			if(rowNum >0) {
			for (int i = 1; i <= rowNum; i++) {
				tdBaseAsset = new TtMktCustNews();
				row = sheet.getRow(i);
				cell=row.getCell(0);
				String t = cell.toString().trim();
				//正则表达式，判断是否有数字
				Pattern pattern = Pattern.compile("[0-9]*");
				Matcher isNum = pattern.matcher(t);
				//判断值中是否为数字，并且不含小数点
				cell.setCellType(Cell.CELL_TYPE_STRING);
				t=cell.toString().trim();
				if(!isNum.matches() && t.indexOf(String.valueOf('.'))==-1){
					tdBaseAsset.setCno(t);
				}else{
					String str = t.substring(0,t.length()-2);
					tdBaseAsset.setCno(str.intern());
				}
				tdBaseAsset.setCname(row.getCell(1).toString());
				tdBaseAsset.setTitle(row.getCell(2).toString());
				tdBaseAsset.setNews(row.getCell(3).toString());
				tdBaseAsset.setImportDate(date);
				list.add(tdBaseAsset);
				if (i%1000 == 0) {
					ttMktCustNewsMapper.insertTtMktCustNewsExcel(list);
					list = new ArrayList<TtMktCustNews>();
				}
			}
			ttMktCustNewsMapper.insertTtMktCustNewsExcel(list);
		}else {
			throw new RuntimeException("模板不能为空");
		}
			
		} catch (Exception e) {
			if(sheet!=null){
				log.error("TtMktCustNewsServiceImpl--setStockPriceByExcel"+e);
				sb.append("舆情信息上传失败 : "+e.getMessage());
			}else{
				log.error("TtMktCustNewsServiceImpl--setStockPriceByExcel"+e);
				sb.append("无法识别Excel后缀名");
			}
			
		}
		return sb.toString();
	}
}
