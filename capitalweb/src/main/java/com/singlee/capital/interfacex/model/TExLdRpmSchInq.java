package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class TExLdRpmSchInq implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ID;						//放款编号
	private String CUST;					//客户号
	private String DRAWDOWN_ACCOUNT;		//放款帐号
	private String PRIN_LIQ_ACCT;			//本金还款帐号
	private String PROD_CAT;				//门类代码
	private String VALUE_DATE;				//放款日
	private String MATURITY;				//放款到期日
	private String CURRENT_RATE;			//当前利率
	private String LD_STATUS;				//状态
	private String OD_STATUS;				//逾期状态
	private String DISP_DATE;				//还款日期
	private Double AMT_DUE;					//本期总还款额
	private Double PRINCIPAL;				//本金
	private Double INT;						//利息
	private Double COMMISSION;				//佣金
	private Double FEE;						//费用
	private Double CHG;						//手续费
	private Double RUNNING_BAL;				//所欠款项
	private String ID_NO;					//放款计划序号
	private String ID_NO_TIME;				//放款批次文件时间
	
	
	
	public String getID_NO_TIME() {
		return ID_NO_TIME;
	}
	public void setID_NO_TIME(String iD_NO_TIME) {
		ID_NO_TIME = iD_NO_TIME;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getCUST() {
		return CUST;
	}
	public void setCUST(String cUST) {
		CUST = cUST;
	}
	public String getDRAWDOWN_ACCOUNT() {
		return DRAWDOWN_ACCOUNT;
	}
	public void setDRAWDOWN_ACCOUNT(String dRAWDOWNACCOUNT) {
		DRAWDOWN_ACCOUNT = dRAWDOWNACCOUNT;
	}
	public String getPRIN_LIQ_ACCT() {
		return PRIN_LIQ_ACCT;
	}
	public void setPRIN_LIQ_ACCT(String pRINLIQACCT) {
		PRIN_LIQ_ACCT = pRINLIQACCT;
	}
	public String getPROD_CAT() {
		return PROD_CAT;
	}
	public void setPROD_CAT(String pRODCAT) {
		PROD_CAT = pRODCAT;
	}
	public String getVALUE_DATE() {
		return VALUE_DATE;
	}
	public void setVALUE_DATE(String vALUEDATE) {
		VALUE_DATE = vALUEDATE;
	}
	public String getMATURITY() {
		return MATURITY;
	}
	public void setMATURITY(String mATURITY) {
		MATURITY = mATURITY;
	}
	public String getCURRENT_RATE() {
		return CURRENT_RATE;
	}
	public void setCURRENT_RATE(String cURRENTRATE) {
		CURRENT_RATE = cURRENTRATE;
	}
	public String getLD_STATUS() {
		return LD_STATUS;
	}
	public void setLD_STATUS(String lDSTATUS) {
		LD_STATUS = lDSTATUS;
	}
	public String getOD_STATUS() {
		return OD_STATUS;
	}
	public void setOD_STATUS(String oDSTATUS) {
		OD_STATUS = oDSTATUS;
	}
	public String getDISP_DATE() {
		return DISP_DATE;
	}
	public void setDISP_DATE(String dISPDATE) {
		DISP_DATE = dISPDATE;
	}
	public Double getAMT_DUE() {
		return AMT_DUE;
	}
	public void setAMT_DUE(Double aMTDUE) {
		AMT_DUE = aMTDUE;
	}
	public Double getPRINCIPAL() {
		return PRINCIPAL;
	}
	public void setPRINCIPAL(Double pRINCIPAL) {
		PRINCIPAL = pRINCIPAL;
	}
	public Double getINT() {
		return INT;
	}
	public void setINT(Double iNT) {
		INT = iNT;
	}
	public Double getCOMMISSION() {
		return COMMISSION;
	}
	public void setCOMMISSION(Double cOMMISSION) {
		COMMISSION = cOMMISSION;
	}
	public Double getFEE() {
		return FEE;
	}
	public void setFEE(Double fEE) {
		FEE = fEE;
	}
	public Double getCHG() {
		return CHG;
	}
	public void setCHG(Double cHG) {
		CHG = cHG;
	}
	public Double getRUNNING_BAL() {
		return RUNNING_BAL;
	}
	public void setRUNNING_BAL(Double rUNNINGBAL) {
		RUNNING_BAL = rUNNINGBAL;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iDNO) {
		ID_NO = iDNO;
	}

	
}
