package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcctNewtdaAaaRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcctNewtdaAaaRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="DebitMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcyTypeDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValueDateDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TheirRefDr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TheirRefCr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RolloverFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CateGory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FbNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VoucherFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcctNewtdaAaaRq", propOrder = { "commonRqHdr",
		"debitMedType", "acctNoDr", "fcyTypeDr", "currencyDr", "valueDateDr",
		"theirRefDr", "theirRefCr", "amt", "term", "rolloverFlag", "acctNo",
		"cateGory", "fbNo", "txnType", "usage", "remark", "voucherFlag",
		"feeBaseLogId" })
public class TDpAcctNewtdaAaaRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "DebitMedType", required = true)
	protected String debitMedType;
	@XmlElement(name = "AcctNoDr", required = true)
	protected String acctNoDr;
	@XmlElement(name = "FcyTypeDr", required = true)
	protected String fcyTypeDr;
	@XmlElement(name = "CurrencyDr", required = true)
	protected String currencyDr;
	@XmlElement(name = "ValueDateDr", required = true)
	protected String valueDateDr;
	@XmlElement(name = "TheirRefDr", required = true)
	protected String theirRefDr;
	@XmlElement(name = "TheirRefCr", required = true)
	protected String theirRefCr;
	@XmlElement(name = "Amt", required = true)
	protected String amt;
	@XmlElement(name = "Term", required = true)
	protected String term;
	@XmlElement(name = "RolloverFlag", required = true)
	protected String rolloverFlag;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "CateGory", required = true)
	protected String cateGory;
	@XmlElement(name = "FbNo", required = true)
	protected String fbNo;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "Remark", required = true)
	protected String remark;
	@XmlElement(name = "VoucherFlag", required = true)
	protected String voucherFlag;
	@XmlElement(name = "FeeBaseLogId", required = true)
	protected String feeBaseLogId;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the debitMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitMedType() {
		return debitMedType;
	}

	/**
	 * Sets the value of the debitMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitMedType(String value) {
		this.debitMedType = value;
	}

	/**
	 * Gets the value of the acctNoDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoDr() {
		return acctNoDr;
	}

	/**
	 * Sets the value of the acctNoDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoDr(String value) {
		this.acctNoDr = value;
	}

	/**
	 * Gets the value of the fcyTypeDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcyTypeDr() {
		return fcyTypeDr;
	}

	/**
	 * Sets the value of the fcyTypeDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcyTypeDr(String value) {
		this.fcyTypeDr = value;
	}

	/**
	 * Gets the value of the currencyDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyDr() {
		return currencyDr;
	}

	/**
	 * Sets the value of the currencyDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyDr(String value) {
		this.currencyDr = value;
	}

	/**
	 * Gets the value of the valueDateDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValueDateDr() {
		return valueDateDr;
	}

	/**
	 * Sets the value of the valueDateDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValueDateDr(String value) {
		this.valueDateDr = value;
	}

	/**
	 * Gets the value of the theirRefDr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTheirRefDr() {
		return theirRefDr;
	}

	/**
	 * Sets the value of the theirRefDr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTheirRefDr(String value) {
		this.theirRefDr = value;
	}

	/**
	 * Gets the value of the theirRefCr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTheirRefCr() {
		return theirRefCr;
	}

	/**
	 * Sets the value of the theirRefCr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTheirRefCr(String value) {
		this.theirRefCr = value;
	}

	/**
	 * Gets the value of the amt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmt() {
		return amt;
	}

	/**
	 * Sets the value of the amt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmt(String value) {
		this.amt = value;
	}

	/**
	 * Gets the value of the term property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the value of the term property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTerm(String value) {
		this.term = value;
	}

	/**
	 * Gets the value of the rolloverFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRolloverFlag() {
		return rolloverFlag;
	}

	/**
	 * Sets the value of the rolloverFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRolloverFlag(String value) {
		this.rolloverFlag = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the cateGory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCateGory() {
		return cateGory;
	}

	/**
	 * Sets the value of the cateGory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCateGory(String value) {
		this.cateGory = value;
	}

	/**
	 * Gets the value of the fbNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFbNo() {
		return fbNo;
	}

	/**
	 * Sets the value of the fbNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFbNo(String value) {
		this.fbNo = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the voucherFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVoucherFlag() {
		return voucherFlag;
	}

	/**
	 * Sets the value of the voucherFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVoucherFlag(String value) {
		this.voucherFlag = value;
	}

	/**
	 * Gets the value of the feeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeBaseLogId() {
		return feeBaseLogId;
	}

	/**
	 * Sets the value of the feeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeBaseLogId(String value) {
		this.feeBaseLogId = value;
	}

}
