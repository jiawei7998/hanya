package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdValueAssessmentMapper;
import com.singlee.capital.interfacex.qdb.model.TdTrdValueAssessment;
import com.singlee.capital.interfacex.qdb.service.TdTrdValueAssessmentService;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
@Service
public class TdTrdValueAssessmentServiceImpl  implements TdTrdValueAssessmentService{

	
	@Autowired
	private TdTrdValueAssessmentMapper mapper;
	

	@Override
	public String setTdTrdValueAssessmentByExcel(String type, InputStream is) {
		try {
			ExcelUtil excelUtil = new ExcelUtil(type, is);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
			TdTrdValueAssessment tdTrdValueAssessment=null;
			Integer a = 0;
			Integer b = 1;
		
			//初始化资产代码 和 上传的系统时间
			String prodCode=null;
			//当前系统时间
			String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
			Map<String, Object> map = new HashMap<String, Object>();
			for (int i = 1; i <= rowNum; i++) 
			{
				tdTrdValueAssessment = new TdTrdValueAssessment();
				row = sheet.getRow(i);	
				if(row.getCell(0) != null)
				{
					tdTrdValueAssessment.setProdCode(row.getCell(a).getStringCellValue());
				}
				tdTrdValueAssessment.setVal(row.getCell(b).getNumericCellValue());
				tdTrdValueAssessment.setValDate(nowdate);//导入时间
				prodCode=StringUtils.trimToEmpty(row.getCell(a) == null ? "" : row.getCell(0).getStringCellValue());
				
				//根据prod_code 获取dealNo
				List<String> dealNos = mapper.searchList(prodCode);
				if(dealNos.size() > 0)
				{
					tdTrdValueAssessment.setDealNo(dealNos.get(0));
					//导入之前，根据prod_code 和 val_date 查询原有的表格数据，如果有这相同的属性，则先删除，再insert
					List<String> pv= mapper.searchProCodeValDate(prodCode, nowdate);
					//删除存在的，在插入数据
					if(pv!=null && pv.size() > 0){
						map.put("prodCode", prodCode);
						map.put("valDate", nowdate);
						mapper.deleteVal(map);
					}
					mapper.insert(tdTrdValueAssessment);
				}
			}// end for
		} catch (Exception e) {
			e.printStackTrace();
			return "估值数据同步失败,请查看估值数据文件" ;
		}
		return "导入成功";
	}
	/**
	 * 查询
	*/
	@Override
	public Page<TdTrdValueAssessment> pageList(Map<String, Object> pageData) {
		Page<TdTrdValueAssessment> page = mapper.pageList(pageData,ParameterUtil.getRowBounds(pageData));
		return page;
	}


}
