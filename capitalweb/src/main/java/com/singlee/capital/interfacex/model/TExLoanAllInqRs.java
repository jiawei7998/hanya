package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TExLoanAllInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TExLoanAllInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="TExLoanAllInqRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}TExLoanAllInqRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OnlyOneLdRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}OnlyOneLdRec" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TExLoanAllInqRs", propOrder = { "commonRsHdr",
		"tExLoanAllInqRec", "onlyOneLdRec" })
public class TExLoanAllInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "TExLoanAllInqRec")
	protected List<TExLoanAllInqRec> tExLoanAllInqRec;
	@XmlElement(name = "OnlyOneLdRec")
	protected List<OnlyOneLdRec> onlyOneLdRec;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the tExLoanAllInqRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the tExLoanAllInqRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTExLoanAllInqRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TExLoanAllInqRec }
	 * 
	 * 
	 */
	public List<TExLoanAllInqRec> getTExLoanAllInqRec() {
		if (tExLoanAllInqRec == null) {
			tExLoanAllInqRec = new ArrayList<TExLoanAllInqRec>();
		}
		return this.tExLoanAllInqRec;
	}

	/**
	 * Gets the value of the onlyOneLdRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the onlyOneLdRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOnlyOneLdRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link OnlyOneLdRec }
	 * 
	 * 
	 */
	public List<OnlyOneLdRec> getOnlyOneLdRec() {
		if (onlyOneLdRec == null) {
			onlyOneLdRec = new ArrayList<OnlyOneLdRec>();
		}
		return this.onlyOneLdRec;
	}

}
