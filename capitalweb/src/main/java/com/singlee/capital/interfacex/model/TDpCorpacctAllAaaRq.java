package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpCorpacctAllAaaRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpCorpacctAllAaaRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="DebitMedType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CateGory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctTitle1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctTitle2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShortTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctOfficer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MainOfficerPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Passtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExprDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaturityDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcLegMatDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SpecAcctNat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CloseRestrict" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Freeflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChequesAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FBID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Useage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SaccStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CredentialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LimitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LimitCurrency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNoType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrvAcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MastType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpCorpacctAllAaaRq", propOrder = { "commonRqHdr",
		"debitMedType", "customer", "cateGory", "acctTitle1", "acctTitle2",
		"shortTitle", "currency", "acctOfficer", "mainOfficerPercent",
		"acctType", "passtype", "endDt", "exprDt", "maturityDt",
		"acLegMatDate", "acctType1", "agtName", "agtIdType", "agtId",
		"specAcctNat", "loanAcctFlag", "closeRestrict", "openBank", "acctNo",
		"freeflag", "chequesAllowed", "beginDt", "beginDate", "endDate",
		"fbid", "txnType", "feeBaseLogId", "useage", "saccStatus", "startDt",
		"credentialNo", "limitID", "limitCurrency", "timeLimit", "acctNoType",
		"accountNo", "prvAcctType", "mastType" })
public class TDpCorpacctAllAaaRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "DebitMedType", required = true)
	protected String debitMedType;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "CateGory", required = true)
	protected String cateGory;
	@XmlElement(name = "AcctTitle1", required = true)
	protected String acctTitle1;
	@XmlElement(name = "AcctTitle2", required = true)
	protected String acctTitle2;
	@XmlElement(name = "ShortTitle", required = true)
	protected String shortTitle;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "AcctOfficer", required = true)
	protected String acctOfficer;
	@XmlElement(name = "MainOfficerPercent", required = true)
	protected String mainOfficerPercent;
	@XmlElement(name = "AcctType", required = true)
	protected String acctType;
	@XmlElement(name = "Passtype", required = true)
	protected String passtype;
	@XmlElement(name = "EndDt", required = true)
	protected String endDt;
	@XmlElement(name = "ExprDt", required = true)
	protected String exprDt;
	@XmlElement(name = "MaturityDt", required = true)
	protected String maturityDt;
	@XmlElement(name = "AcLegMatDate", required = true)
	protected String acLegMatDate;
	@XmlElement(name = "AcctType_1", required = true)
	protected String acctType1;
	@XmlElement(name = "AgtName", required = true)
	protected String agtName;
	@XmlElement(name = "AgtIdType", required = true)
	protected String agtIdType;
	@XmlElement(name = "AgtId", required = true)
	protected String agtId;
	@XmlElement(name = "SpecAcctNat", required = true)
	protected String specAcctNat;
	@XmlElement(name = "LoanAcctFlag", required = true)
	protected String loanAcctFlag;
	@XmlElement(name = "CloseRestrict", required = true)
	protected String closeRestrict;
	@XmlElement(name = "OpenBank", required = true)
	protected String openBank;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "Freeflag", required = true)
	protected String freeflag;
	@XmlElement(name = "ChequesAllowed", required = true)
	protected String chequesAllowed;
	@XmlElement(name = "BeginDt", required = true)
	protected String beginDt;
	@XmlElement(name = "BeginDate", required = true)
	protected String beginDate;
	@XmlElement(name = "EndDate", required = true)
	protected String endDate;
	@XmlElement(name = "FBID", required = true)
	protected String fbid;
	@XmlElement(name = "TxnType", required = true)
	protected String txnType;
	@XmlElement(name = "FeeBaseLogId", required = true)
	protected String feeBaseLogId;
	@XmlElement(name = "Useage", required = true)
	protected String useage;
	@XmlElement(name = "SaccStatus", required = true)
	protected String saccStatus;
	@XmlElement(name = "StartDt", required = true)
	protected String startDt;
	@XmlElement(name = "CredentialNo", required = true)
	protected String credentialNo;
	@XmlElement(name = "LimitID", required = true)
	protected String limitID;
	@XmlElement(name = "LimitCurrency", required = true)
	protected String limitCurrency;
	@XmlElement(name = "TimeLimit", required = true)
	protected String timeLimit;
	@XmlElement(name = "AcctNoType", required = true)
	protected String acctNoType;
	@XmlElement(name = "AccountNo", required = true)
	protected String accountNo;
	@XmlElement(name = "PrvAcctType", required = true)
	protected String prvAcctType;
	@XmlElement(name = "MastType", required = true)
	protected String mastType;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the debitMedType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitMedType() {
		return debitMedType;
	}

	/**
	 * Sets the value of the debitMedType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitMedType(String value) {
		this.debitMedType = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the cateGory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCateGory() {
		return cateGory;
	}

	/**
	 * Sets the value of the cateGory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCateGory(String value) {
		this.cateGory = value;
	}

	/**
	 * Gets the value of the acctTitle1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctTitle1() {
		return acctTitle1;
	}

	/**
	 * Sets the value of the acctTitle1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctTitle1(String value) {
		this.acctTitle1 = value;
	}

	/**
	 * Gets the value of the acctTitle2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctTitle2() {
		return acctTitle2;
	}

	/**
	 * Sets the value of the acctTitle2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctTitle2(String value) {
		this.acctTitle2 = value;
	}

	/**
	 * Gets the value of the shortTitle property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShortTitle() {
		return shortTitle;
	}

	/**
	 * Sets the value of the shortTitle property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShortTitle(String value) {
		this.shortTitle = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the acctOfficer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctOfficer() {
		return acctOfficer;
	}

	/**
	 * Sets the value of the acctOfficer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctOfficer(String value) {
		this.acctOfficer = value;
	}

	/**
	 * Gets the value of the mainOfficerPercent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMainOfficerPercent() {
		return mainOfficerPercent;
	}

	/**
	 * Sets the value of the mainOfficerPercent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMainOfficerPercent(String value) {
		this.mainOfficerPercent = value;
	}

	/**
	 * Gets the value of the acctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType() {
		return acctType;
	}

	/**
	 * Sets the value of the acctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType(String value) {
		this.acctType = value;
	}

	/**
	 * Gets the value of the passtype property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPasstype() {
		return passtype;
	}

	/**
	 * Sets the value of the passtype property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPasstype(String value) {
		this.passtype = value;
	}

	/**
	 * Gets the value of the endDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDt() {
		return endDt;
	}

	/**
	 * Sets the value of the endDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDt(String value) {
		this.endDt = value;
	}

	/**
	 * Gets the value of the exprDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExprDt() {
		return exprDt;
	}

	/**
	 * Sets the value of the exprDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExprDt(String value) {
		this.exprDt = value;
	}

	/**
	 * Gets the value of the maturityDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaturityDt() {
		return maturityDt;
	}

	/**
	 * Sets the value of the maturityDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaturityDt(String value) {
		this.maturityDt = value;
	}

	/**
	 * Gets the value of the acLegMatDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcLegMatDate() {
		return acLegMatDate;
	}

	/**
	 * Sets the value of the acLegMatDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcLegMatDate(String value) {
		this.acLegMatDate = value;
	}

	/**
	 * Gets the value of the acctType1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType1() {
		return acctType1;
	}

	/**
	 * Sets the value of the acctType1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType1(String value) {
		this.acctType1 = value;
	}

	/**
	 * Gets the value of the agtName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtName() {
		return agtName;
	}

	/**
	 * Sets the value of the agtName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtName(String value) {
		this.agtName = value;
	}

	/**
	 * Gets the value of the agtIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtIdType() {
		return agtIdType;
	}

	/**
	 * Sets the value of the agtIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtIdType(String value) {
		this.agtIdType = value;
	}

	/**
	 * Gets the value of the agtId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtId() {
		return agtId;
	}

	/**
	 * Sets the value of the agtId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtId(String value) {
		this.agtId = value;
	}

	/**
	 * Gets the value of the specAcctNat property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSpecAcctNat() {
		return specAcctNat;
	}

	/**
	 * Sets the value of the specAcctNat property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSpecAcctNat(String value) {
		this.specAcctNat = value;
	}

	/**
	 * Gets the value of the loanAcctFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanAcctFlag() {
		return loanAcctFlag;
	}

	/**
	 * Sets the value of the loanAcctFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanAcctFlag(String value) {
		this.loanAcctFlag = value;
	}

	/**
	 * Gets the value of the closeRestrict property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCloseRestrict() {
		return closeRestrict;
	}

	/**
	 * Sets the value of the closeRestrict property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCloseRestrict(String value) {
		this.closeRestrict = value;
	}

	/**
	 * Gets the value of the openBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenBank() {
		return openBank;
	}

	/**
	 * Sets the value of the openBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenBank(String value) {
		this.openBank = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the freeflag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFreeflag() {
		return freeflag;
	}

	/**
	 * Sets the value of the freeflag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFreeflag(String value) {
		this.freeflag = value;
	}

	/**
	 * Gets the value of the chequesAllowed property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChequesAllowed() {
		return chequesAllowed;
	}

	/**
	 * Sets the value of the chequesAllowed property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChequesAllowed(String value) {
		this.chequesAllowed = value;
	}

	/**
	 * Gets the value of the beginDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDt() {
		return beginDt;
	}

	/**
	 * Sets the value of the beginDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDt(String value) {
		this.beginDt = value;
	}

	/**
	 * Gets the value of the beginDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDate() {
		return beginDate;
	}

	/**
	 * Sets the value of the beginDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDate(String value) {
		this.beginDate = value;
	}

	/**
	 * Gets the value of the endDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Sets the value of the endDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDate(String value) {
		this.endDate = value;
	}

	/**
	 * Gets the value of the fbid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFBID() {
		return fbid;
	}

	/**
	 * Sets the value of the fbid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFBID(String value) {
		this.fbid = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the feeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeBaseLogId() {
		return feeBaseLogId;
	}

	/**
	 * Sets the value of the feeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeBaseLogId(String value) {
		this.feeBaseLogId = value;
	}

	/**
	 * Gets the value of the useage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUseage() {
		return useage;
	}

	/**
	 * Sets the value of the useage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUseage(String value) {
		this.useage = value;
	}

	/**
	 * Gets the value of the saccStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSaccStatus() {
		return saccStatus;
	}

	/**
	 * Sets the value of the saccStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSaccStatus(String value) {
		this.saccStatus = value;
	}

	/**
	 * Gets the value of the startDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStartDt() {
		return startDt;
	}

	/**
	 * Sets the value of the startDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStartDt(String value) {
		this.startDt = value;
	}

	/**
	 * Gets the value of the credentialNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCredentialNo() {
		return credentialNo;
	}

	/**
	 * Sets the value of the credentialNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCredentialNo(String value) {
		this.credentialNo = value;
	}

	/**
	 * Gets the value of the limitID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitID() {
		return limitID;
	}

	/**
	 * Sets the value of the limitID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitID(String value) {
		this.limitID = value;
	}

	/**
	 * Gets the value of the limitCurrency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitCurrency() {
		return limitCurrency;
	}

	/**
	 * Sets the value of the limitCurrency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitCurrency(String value) {
		this.limitCurrency = value;
	}

	/**
	 * Gets the value of the timeLimit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTimeLimit() {
		return timeLimit;
	}

	/**
	 * Sets the value of the timeLimit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTimeLimit(String value) {
		this.timeLimit = value;
	}

	/**
	 * Gets the value of the acctNoType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNoType() {
		return acctNoType;
	}

	/**
	 * Sets the value of the acctNoType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNoType(String value) {
		this.acctNoType = value;
	}

	/**
	 * Gets the value of the accountNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * Sets the value of the accountNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountNo(String value) {
		this.accountNo = value;
	}

	/**
	 * Gets the value of the prvAcctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrvAcctType() {
		return prvAcctType;
	}

	/**
	 * Sets the value of the prvAcctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrvAcctType(String value) {
		this.prvAcctType = value;
	}

	/**
	 * Gets the value of the mastType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMastType() {
		return mastType;
	}

	/**
	 * Sets the value of the mastType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMastType(String value) {
		this.mastType = value;
	}

}
