package com.singlee.capital.interfacex.qdb.service;

import java.io.InputStream;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktCustNews;

public interface TtMktCustNewsService {
	/**
	 * 查询舆情信息管理，分页
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktCustNews> sreachTtMktCustNews(Map<String, Object> param);
	
	public boolean setTdBaseAssetPriceByExcel(String file);
	
	public String setStockPriceByExcel(String type, InputStream is);
}
