package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class JobExcuRecord implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String LOG_ID;
	private int JOB_ID;
	private String EXCU_TIME;
	private String RECORD_STATUS;
	private String INPUTTIME;
	
	public String getLOG_ID() {
		return LOG_ID;
	}
	public void setLOG_ID(String lOG_ID) {
		LOG_ID = lOG_ID;
	}
	public String getRECORD_STATUS() {
		return RECORD_STATUS;
	}
	public void setRECORD_STATUS(String rECORD_STATUS) {
		RECORD_STATUS = rECORD_STATUS;
	}
	public String getINPUTTIME() {
		return INPUTTIME;
	}
	public void setINPUTTIME(String iNPUTTIME) {
		INPUTTIME = iNPUTTIME;
	}
	public int getJOB_ID() {
		return JOB_ID;
	}
	public void setJOB_ID(int jOB_ID) {
		JOB_ID = jOB_ID;
	}
	public String getEXCU_TIME() {
		return EXCU_TIME;
	}
	public void setEXCU_TIME(String eXCU_TIME) {
		EXCU_TIME = eXCU_TIME;
	}
	
}
