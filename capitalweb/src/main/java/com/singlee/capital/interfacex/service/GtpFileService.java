package com.singlee.capital.interfacex.service;

import java.util.List;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.credit.model.TcCredit;
import com.singlee.capital.interfacex.model.CreditBatch;
import com.singlee.capital.interfacex.model.SubjectInfo;
import com.singlee.capital.interfacex.model.TExLdRpmSchInq;
import com.singlee.capital.interfacex.model.TExReconAllInqRec;
import com.singlee.capital.interfacex.model.UPPSChkFileEndDayBatch;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.users.model.TdCustEcif;

public interface GtpFileService {

	/**
	 * 额度信息同步接口 ---CRMS取IFBM文件,IFBM生成文件
	 * 分隔符号：“|”
	 * @return
	 * @throws Exception
	 */
	public RetMsg<Object> GtpCrmsQuotaEndDayBatchWrite(List<CreditBatch> creditBatchs) throws Exception;
	
	/**
	 * 销项交易数据明细--GVAT系统取IFBM文件
	 * 生成CSV文件
	 * @param gvatXxs
	 * @return
	 * @throws Exception
	 */
	public RetMsg<Object> GtpGvatOutputDataWrite() throws Exception;
	/**
	 * 对账查询接口 ----IFBM取T24文件，IFBM读文件
	 * 分隔符号：“|”
	 * @return
	 * @throws Exception
	 */
	public List<TExReconAllInqRec> GtpIfbmT24TExReconAllInqRead(String filename) throws Exception;
	
	/**
	 * 还款计划查询接口 ----IFBM取T24文件，IFBM读文件
	 * 分隔符号：“@!@”
	 * @return
	 * @throws Exception
	 */
	public List<TExLdRpmSchInq> GtpIfbmT24TExLdRpmSchInqRead() throws Exception;
	
	/**
	 * 对账文件申请  -----IFBM取支付的文件，IFBM读文件
	 * 支付日终后，请求后轮询
	 * 分割符号：文件内容以“ @!@”分隔
	 * @return
	 * @throws Exception
	 */
	public List<UPPSChkFileEndDayBatch>  GtpIfbmPaymentUPPSChkFileAplRead() throws Exception;
	
	/**
	 *  产品同步接口--IFBM取CRMS文件,IFBM读文件
	 *  分隔符号：“|”
	 * @return
	 * @throws Exception
	 */
	public List<TcCredit> GtpIfbmCrmsProductRead() throws Exception;
	
	/**
	 * 读取ECIF数据仓库批量客户信息
	 * @return
	 * @throws Exception
	 */
	public RetMsg<Object> GtpEcifBosoIdentifierRead() throws Exception;
	public RetMsg<Object> GtpEcifBosoAddressRead() throws Exception;
	public RetMsg<Object> GtpEcifBosoCustomerManagerRead() throws Exception;
	public RetMsg<Object> GtpEcifBosOrgRead() throws Exception;
	
	/**
	 *  科目信息同步--IFBM取Ifbm文件,IFBM读文件
	 *  分隔符号：“@!@”
	 * @return
	 * @throws Exception
	 */
	public List<SubjectInfo> GtpIfbmPimgSubjectInfoRead() throws Exception;
	/**
	 *  总账记账接口  ifbm生成文件给总账
	 *  分隔符号：
	 * @return
	 * @throws Exception
	 */

	public RetMsg<Object> GtpGLOutputDataWrite() throws Exception;
	/**
	 *  网点机构信息   ifbm取数仓文件
	 *  分隔符号：
	 * @return
	 * @throws Exception
	 */
	public List<TtInstitution> GtpDWCompanyRead() throws Exception;
	
	/**
	 * 	融贷通、企信的借据信息
	 */
	public RetMsg<Object> GtpDWBussinessDueBillRead() throws Exception;
	
	public void EcifCustImport(TdCustEcif ecif,String isdelete) throws Exception;
	
	/**
	 * 数据移型当晚总账记账方法
	 * @return
	 * @throws Exception
	 */
	public RetMsg<Object> GtpT24TransforGLOutputDataWrite() throws Exception;

	public void insertCustEcifFromBos();
	
}
