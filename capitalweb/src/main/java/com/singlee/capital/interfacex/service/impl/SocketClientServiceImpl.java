package com.singlee.capital.interfacex.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.xml.tools.XmlFormat;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceLogMapper;
import com.singlee.capital.interfacex.model.ApprovalChangeRec;
import com.singlee.capital.interfacex.model.ApprovalChangeRq;
import com.singlee.capital.interfacex.model.ApprovalChangeRs;
import com.singlee.capital.interfacex.model.ApprovalInqRq;
import com.singlee.capital.interfacex.model.ApprovalInqRs;
import com.singlee.capital.interfacex.model.BOSFXII;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRq;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRs;
import com.singlee.capital.interfacex.model.C201OrgCusInqRq;
import com.singlee.capital.interfacex.model.C201OrgCusInqRs;
import com.singlee.capital.interfacex.model.CloudProApplyTransRq;
import com.singlee.capital.interfacex.model.CloudProApplyTransRs;
import com.singlee.capital.interfacex.model.CloudProModTransRq;
import com.singlee.capital.interfacex.model.CloudProModTransRs;
import com.singlee.capital.interfacex.model.CloudProSynchTransRq;
import com.singlee.capital.interfacex.model.CloudProSynchTransRs;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.CommonRsHdr;
import com.singlee.capital.interfacex.model.EnterpriseApproveRq;
import com.singlee.capital.interfacex.model.EnterpriseApproveRs;
import com.singlee.capital.interfacex.model.EnterpriseInqRq;
import com.singlee.capital.interfacex.model.EnterpriseInqRs;
import com.singlee.capital.interfacex.model.FinCreditApproveRq;
import com.singlee.capital.interfacex.model.FinCreditApproveRs;
import com.singlee.capital.interfacex.model.FinCreditInqRq;
import com.singlee.capital.interfacex.model.FinCreditInqRs;
import com.singlee.capital.interfacex.model.LoanNoticeRq;
import com.singlee.capital.interfacex.model.LoanNoticeRs;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.interfacex.model.QuotaSyncopateRq;
import com.singlee.capital.interfacex.model.QuotaSyncopateRs;
import com.singlee.capital.interfacex.model.RepayNoticeRq;
import com.singlee.capital.interfacex.model.RepayNoticeRs;
import com.singlee.capital.interfacex.model.TCoActIntInqRq;
import com.singlee.capital.interfacex.model.TCoActIntInqRs;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRq;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRs;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRq;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRs;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRs;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRq;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRs;
import com.singlee.capital.interfacex.model.TCustDetInqRq;
import com.singlee.capital.interfacex.model.TCustDetInqRs;
import com.singlee.capital.interfacex.model.TDpAcAciModRq;
import com.singlee.capital.interfacex.model.TDpAcAciModRs;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRq;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRs;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRq;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRs;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRq;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRs;
import com.singlee.capital.interfacex.model.TDpCorpacctAllAaaRq;
import com.singlee.capital.interfacex.model.TDpCorpacctAllAaaRs;
import com.singlee.capital.interfacex.model.TDpIntBnkAaaRq;
import com.singlee.capital.interfacex.model.TDpIntBnkAaaRs;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRq;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRs;
import com.singlee.capital.interfacex.model.TExLdRpmSchInqRq;
import com.singlee.capital.interfacex.model.TExLdRpmSchInqRs;
import com.singlee.capital.interfacex.model.TExLoanAllInqRq;
import com.singlee.capital.interfacex.model.TExLoanAllInqRs;
import com.singlee.capital.interfacex.model.TExReconAllInqRq;
import com.singlee.capital.interfacex.model.TExReconAllInqRs;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.model.TStFtNormRevRq;
import com.singlee.capital.interfacex.model.TStFtNormRevRs;
import com.singlee.capital.interfacex.model.TStFtRemitAaaRq;
import com.singlee.capital.interfacex.model.TStFtRemitAaaRs;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRs;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRq;
import com.singlee.capital.interfacex.model.UPPSChkFileAplRs;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRec;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.service.TiInterfaceInfoService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.interfacex.socketservice.SocketClientServer;
import com.singlee.capital.interfacex.socketservice.SocketMessageInterceptor;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.ProductCustCreditService;

@Service("socketClientService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SocketClientServiceImpl implements SocketClientService{

	private static final Logger log = LoggerFactory.getLogger("INTERFACEX");
	@Autowired
	private TiInterfaceLogMapper tiInterfacleLogMapper;
	
	@Autowired
	private SocketClientServer socketClientServer;
	
	@Autowired
	private TiInterfaceInfoMapper tiInterfaceInfoMapper;
	
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	
	@Autowired
	private CreditManageService creditManageService;
	
	@Autowired
	private EdCustManangeService edCustManangeService;
	@Autowired
	private TiInterfaceInfoService tiInterfaceInfoService;
	
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private ProductCustCreditService productCustCreditService;
	@Autowired
	private TdEdCustBatchStatusMapper edCustBatchStatusMapper;
	
	@SuppressWarnings("unchecked")
	public static <T> T converyToJavaBean(String xml, Class<T> c) {  
        T t = null;  
        try {  
        	int pos=xml.indexOf("<?xml version");
        	xml=xml.substring(pos);
        	xml=xml.replace(" xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"","");

        	JAXBContext context = JAXBContext.newInstance(c);  
            Unmarshaller unmarshaller = context.createUnmarshaller();  
            t = (T) unmarshaller.unmarshal(new StringReader(xml));  
        } catch (Exception e) {  
        	//e.printStackTrace();
			throw new RException(e);
        }  
  
        return t;  
    }
	
	public static String convertToXml(Object obj) {  
		
       return null;
  
    }  

	@Override
	//@AutoLogMethod(value="TExCurrencyAllInqRs")
	public TExCurrencyAllInqRs t24TexCurrencyAllInqRequest(
			TExCurrencyAllInqRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTExCurrencyAllInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TExCurrencyAllInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("TExCurrencyAllInqRs请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("TExCurrencyAllInqRs请求发送报文："+(xml));
		System.out.println("TExCurrencyAllInqRs反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("TExCurrencyAllInqRs反馈报文："+XmlFormat.returnMsg(reponsexml));

		TExCurrencyAllInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTExCurrencyAllInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TStBeaRepayAaaRs")
	public synchronized TStBeaRepayAaaRs t24TStBeaRepayAaaRequest(TStBeaRepayAaaRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		UPPSCdtTrfRq payment = new UPPSCdtTrfRq();
		payment.setUserDefineTranCode(InterfaceCode.TI_TStBeaRepayAaaRs);
		payment.setPayPathCode("");
		payment.setBusiType(request.getFbNo());
		payment.setBusiKind(request.getTxnType());
		payment.setPayerAcc(request.getDebitBeaRec().get(0).getAcctNoDr());
		payment.setPayeeAcc(request.getCreditBeaRec().get(0).getAcctNoCr());
		payment.setCurrency(request.getCurrencyDr());
		payment.setAmount(request.getDebitBeaRec().get(0).getAmtDr());
		payment.setFeeBaseLogId(request.getFeeBaseLogId());
		payment.setWorkDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		//先判断在清算表中是否存在交易，如存在，则删掉。
		if(ti2ndPaymentMapper.queryTi2ndPaymentCount(request.getFeeBaseLogId())>0){
//			ti2ndPaymentMapper.deleteTi2ndPayment(request.getFeeBaseLogId());
		}else{
			ti2ndPaymentMapper.insertTi2ndPayment(payment);
		}
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTStBeaRepayAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TStBeaRepayAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("多借多贷核心记账TStBeaRepayAaaRs请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("多借多贷核心记账TStBeaRepayAaaRs请求发送报文："+(xml));
		System.out.println("多借多贷核心记账TStBeaRepayAaaRs反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("多借多贷核心记账TStBeaRepayAaaRs反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TStBeaRepayAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTStBeaRepayAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode()+"|"+request.getFeeBaseLogId());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("retcode", response.getCommonRsHdr().getStatusCode());
		map1.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map1.put("srquid",response.getSPRsUID());
		map1.put("agentserialno", "");
		map1.put("centerdealcode", "");
		map1.put("centerdealmsg", "");
		map1.put("payresult", "");
		map1.put("workdate", response.getProcessDt()==null|| "".equals(response.getProcessDt())?new SimpleDateFormat("yyyyMMdd").format(new Date()):response.getProcessDt());
		map1.put("bankdate", response.getExposureDt());
		map1.put("bankid", response.getCompanyCode());
		map1.put("feeBaseLogid", request.getFeeBaseLogId());

		ti2ndPaymentMapper.update2ndPaymentRetMsg(map1);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TStFtNormRevRs")
	public TStFtNormRevRs t24TstFtNormRevRequest(TStFtNormRevRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTStFtNormRevRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TStFtNormRevRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("普通转账冲正TStFtNormRevRq请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("普通转账冲正TStFtNormRevRq请求发送报文："+(xml));
		System.out.println("普通转账冲正TStFtNormRevRq反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("普通转账冲正TStFtNormRevRq反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TStFtNormRevRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTStFtNormRevRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TStFtRemitAaaRs")
	public TStFtRemitAaaRs t24TstFtRemitAaaRequest(TStFtRemitAaaRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTStFtRemitAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TStFtRemitAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("国际业务汇出申请（ST-FT-REMIT-AAA）MT202接口、MT103接口-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("国际业务汇出申请（ST-FT-REMIT-AAA）MT202接口、MT103接口-请求发送报文："+(xml));
		System.out.println("国际业务汇出申请（ST-FT-REMIT-AAA）MT202接口、MT103接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("国际业务汇出申请（ST-FT-REMIT-AAA）MT202接口、MT103接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TStFtRemitAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTStFtRemitAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpAcStmtInqRs")
	public TDpAcStmtInqRs t24TdpAcStmtInqRequest(TDpAcStmtInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpAcStmtInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpAcStmtInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("交易明细查询接口（DP-AC-STMT-INQ）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("交易明细查询接口（DP-AC-STMT-INQ）-请求发送报文："+(xml));
		System.out.println("交易明细查询接口（DP-AC-STMT-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("交易明细查询接口（DP-AC-STMT-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpAcStmtInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpAcStmtInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TExReconAllInqRs")
	public TExReconAllInqRs t24TexReconAllInqRequest(TExReconAllInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTExReconAllInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TExReconAllInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("对账查询接口（EX-RECON-ALL-INQ）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对账查询接口（EX-RECON-ALL-INQ）-请求发送报文："+(xml));
		System.out.println("对账查询接口（EX-RECON-ALL-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对账查询接口（EX-RECON-ALL-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TExReconAllInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTExReconAllInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpAcctTypeInqRs")
	public TDpAcctTypeInqRs t24TdpAcctTypeInqRequest(TDpAcctTypeInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpAcctTypeInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpAcctTypeInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("账户介质类型查询（DP-ACCT-TYPE-INQ）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("账户介质类型查询（DP-ACCT-TYPE-INQ）-请求发送报文："+(xml));
		System.out.println("账户介质类型查询（DP-ACCT-TYPE-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("账户介质类型查询（DP-ACCT-TYPE-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpAcctTypeInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpAcctTypeInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpCorpacctAllAaaRs")
	public TDpCorpacctAllAaaRs t24TDpCorpacctAllAaaRequest(
			TDpCorpacctAllAaaRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpCorpacctAllAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpCorpacctAllAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("对公新开账户(DP-CORPACCT-ALL-AAA)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对公新开账户(DP-CORPACCT-ALL-AAA)-请求发送报文："+(xml));
		System.out.println("对公新开账户(DP-CORPACCT-ALL-AAA)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对公新开账户(DP-CORPACCT-ALL-AAA)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpCorpacctAllAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpCorpacctAllAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCoSignonMultAaaRs")
	public TCoSignonMultAaaRs t24TCoSignonMultAaaRequest(
			TCoSignonMultAaaRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCoSignonMultAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCoSignonMultAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("客户签约接口（CO-SIGNOFF-MULT-AAA）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("客户签约接口（CO-SIGNOFF-MULT-AAA）-请求发送报文："+(xml));
		System.out.println("客户签约接口（CO-SIGNOFF-MULT-AAA）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("客户签约接口（CO-SIGNOFF-MULT-AAA）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCoSignonMultAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCoSignonMultAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCoSignoffMultAaaRs")
	public TCoSignoffMultAaaRs t24TCoSignoffMultAaaRequest(
			TCoSignoffMultAaaRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCoSignoffMultAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCoSignoffMultAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("客户解约接口(CO-SIGNON-MULT-AAA)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("客户解约接口(CO-SIGNON-MULT-AAA)-请求发送报文："+(xml));
		System.out.println("客户解约接口(CO-SIGNON-MULT-AAA)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("客户解约接口(CO-SIGNON-MULT-AAA)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCoSignoffMultAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCoSignoffMultAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCoHolidayAllInqRs")
	public TCoHolidayAllInqRs t24TCoHolidayAllInqRequest(
			TCoHolidayAllInqRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCoHolidayAllInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCoHolidayAllInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("节假日查询（CO-HOLIDAY-ALL-INQ）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("节假日查询（CO-HOLIDAY-ALL-INQ）-请求发送报文："+(xml));
		System.out.println("节假日查询（CO-HOLIDAY-ALL-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("节假日查询（CO-HOLIDAY-ALL-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCoHolidayAllInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCoHolidayAllInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="C201OrgCusInqRs")
	public C201OrgCusInqRs EcifC201OrgCustInqRequest(C201OrgCusInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setC201OrgCusInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		socketClientServer.setSendflag(false);
		String reponsexml = socketClientServer.start();
		 
		System.out.println("TExCurrencyAllInqRs请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("TExCurrencyAllInqRs请求发送报文："+(xml));
		System.out.println("TExCurrencyAllInqRs反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("TExCurrencyAllInqRs反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		C201OrgCusInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getC201OrgCusInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="UPPSCdtTrfRs")
	public UPPSCdtTrfRs T2ndPaymentUPPSCdtTrfRequest(UPPSCdtTrfRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		/**
		 * 二代支付往账报文发送接口
		 * 1、根据第三方流水号FeeBaseLogId交易是否重复
		 * 2、记录请求发送报文TI_2NDPAYMENT
		 * 3、发送往账报文
		 * 4、解析返回结果
		 */
//		if(ti2ndPaymentMapper.queryTi2ndPaymentCount(request.getFeeBaseLogId())>0){
		if(ti2ndPaymentMapper.queryTi2ndPaymentCount2(request.getFeeBaseLogId())>0){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("feeBaseLogid", request.getFeeBaseLogId());
			map.put("is_locked", true);
			UPPSCdtTrfRq response = ti2ndPaymentMapper.queryTi2ndPayment(map);
			UPPSCdtTrfRs rs = new UPPSCdtTrfRs();
			CommonRsHdr rsHdr = new CommonRsHdr();
			/**
			 * 处理失败的交易不允许流水号相同
			 */
			if(response.getPayResult() == null|| "".equals(response.getPayResult())){
				
				UPPSigBusiQueryRq requestQuery = new UPPSigBusiQueryRq();
				CommonRqHdr hdr = new CommonRqHdr();
				hdr.setRqUID(UUID.randomUUID().toString());
				requestQuery.setCommonRqHdr(hdr);
				requestQuery.setUserDefineTranCode(InterfaceCode.TI2ND_QueryUserDefineTranCode);
				requestQuery.setOrigFeeBaseLogId(response.getFeeBaseLogId());
				requestQuery.setOrigChannelCode(InterfaceCode.TI_CHANNELNO);
				requestQuery.setOrigChannelDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				
				UPPSigBusiQueryRs responseQuery = T2ndPaymentUPPSigBusiQueryRequest(requestQuery);
				List<UPPSigBusiQueryRec> recList = responseQuery.getUPPSigBusiQueryRec();
				
				if(responseQuery.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
					
					if(recList != null && recList.size() == 1){
						
						for (int i = 0; i < recList.size(); i++) {
							if(recList.get(i).getFeeBaseLogId().equals(response.getFeeBaseLogId())){
								
								UPPSigBusiQueryRec uppSigBusiQueryRec = ((UPPSigBusiQueryRec)recList.get(i));
								
								Map<String, Object> maprec = new HashMap<String, Object>();
								maprec.put("retcode", responseQuery.getCommonRsHdr().getStatusCode());
								maprec.put("retmsg", responseQuery.getCommonRsHdr().getServerStatusCode());
								maprec.put("srquid",uppSigBusiQueryRec.getAgentSerialNo());
								maprec.put("agentserialno", uppSigBusiQueryRec.getAgentSerialNo());
								maprec.put("centerdealcode", uppSigBusiQueryRec.getCenterDealCode());
								maprec.put("centerdealmsg", uppSigBusiQueryRec.getCenterDealMsg());
								maprec.put("payresult", uppSigBusiQueryRec.getPayResult());
								maprec.put("workdate", uppSigBusiQueryRec.getWorkDate());
								maprec.put("bankdate", uppSigBusiQueryRec.getBankDate());
								maprec.put("bankid", uppSigBusiQueryRec.getBankId());
								maprec.put("feeBaseLogid", request.getFeeBaseLogId());

								ti2ndPaymentMapper.update2ndPaymentRetMsg(maprec);
								
								
								rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
								rsHdr.setStatusCode(responseQuery.getCommonRsHdr().getStatusCode());
								rsHdr.setServerStatusCode(responseQuery.getCommonRsHdr().getServerStatusCode());
								rs.setCommonRsHdr(rsHdr);
								rs.setPayResult(uppSigBusiQueryRec.getPayResult());
								return rs;
							}
						}
					}
					
				} else {
					
					rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
					rsHdr.setStatusCode(InterfaceCode.TI_FAILURE);
					rsHdr.setServerStatusCode("交易查询出错！");
					rs.setCommonRsHdr(rsHdr);
					return rs;
				}
			}else if("2".equals(response.getPayResult())){
				rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
				rsHdr.setStatusCode(InterfaceCode.TI_SUCCESS);
				rsHdr.setServerStatusCode("交易已处理成功！");
				rs.setCommonRsHdr(rsHdr);
				rs.setPayResult(response.getPayResult());
				return rs;
			}else if("1".equals(response.getPayResult())){
				rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
				rsHdr.setStatusCode(InterfaceCode.TI_SUCCESS);
				rsHdr.setServerStatusCode("交易处理中！");
				rs.setCommonRsHdr(rsHdr);
				rs.setPayResult(response.getPayResult());
				return rs;
			}else {
				rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
				rsHdr.setStatusCode(InterfaceCode.TI_STATUS1);
				rsHdr.setServerStatusCode("该大额往账交易流水重复！");
				rs.setCommonRsHdr(rsHdr);
				return rs;
			}
			
		}
		String srquid = null;
		if(ti2ndPaymentMapper.queryTi2ndPaymentCount(request.getFeeBaseLogId())>0){			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("feeBaseLogid", request.getFeeBaseLogId());
			srquid = ti2ndPaymentMapper.queryTi2ndPayment2(map).getSrquid();
			ti2ndPaymentMapper.deleteTi2ndPayment(request.getFeeBaseLogId());
		}
		
		try {
			tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
			request.setWorkDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			ti2ndPaymentMapper.insertTi2ndPayment(request);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("feeBaseLogid", request.getFeeBaseLogId());
			map.put("is_locked", true);
			ti2ndPaymentMapper.queryTi2ndPayment(map);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("T2ndPaymentUPPSCdtTrfRequest:"+e.getMessage());
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T2ndPaymentUPPSCdtTrfRequest:"+e.getMessage());
			UPPSCdtTrfRs rs = new UPPSCdtTrfRs();
			CommonRsHdr rsHdr = new CommonRsHdr();
			rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
			rsHdr.setStatusCode(InterfaceCode.TI_STATUS2);
			rsHdr.setServerStatusCode("交易正在处理中！");
			rs.setCommonRsHdr(rsHdr);
			return rs;
		}
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setUPPSCdtTrfRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_UPPSCdtTrfRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		log.info("二代支付-普通贷记往账接口-请求发送报文："+(xml));
		System.out.println("二代支付-普通贷记往账接口-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-普通贷记往账接口-请求发送报文："+(xml));
		log.info("二代支付-普通贷记往账接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		System.out.println("二代支付-普通贷记往账接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-普通贷记往账接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		UPPSCdtTrfRs response=converyToJavaBean(reponsexml,BOSFXII.class).getUPPSCdtTrfRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		map.put("agentserialno", response.getAgentSerialNo());
		map.put("centerdealcode", response.getCenterDealCode());
		map.put("centerdealmsg", response.getCenterDealMsg());
		map.put("payresult", response.getPayResult());
		map.put("workdate", response.getWorkDate()==null|| "".equals(response.getWorkDate())?new SimpleDateFormat("yyyyMMdd").format(new Date()):response.getWorkDate());
		map.put("bankdate", response.getBankDate());
		map.put("bankid", response.getBankId());
		map.put("srquid", srquid);
		map.put("feeBaseLogid", request.getFeeBaseLogId());

		ti2ndPaymentMapper.update2ndPaymentRetMsg(map);
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="UPPSigBusiQueryRs")
	public UPPSigBusiQueryRs T2ndPaymentUPPSigBusiQueryRequest(
			UPPSigBusiQueryRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setUPPSigBusiQueryRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_UPPSigBusiQueryRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("二代支付-自助渠道业务状态查询接口-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-自助渠道业务状态查询接口-请求发送报文："+(xml));
		System.out.println("二代支付-自助渠道业务状态查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-自助渠道业务状态查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		UPPSigBusiQueryRs response=converyToJavaBean(reponsexml,BOSFXII.class).getUPPSigBusiQueryRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="UPPSChkFileAplRs")
	public UPPSChkFileAplRs T2ndPaymentUPPSChkFileAplRequest(
			UPPSChkFileAplRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setUPPSChkFileAplRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_UPPSChkFileAplRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("二代支付-对账文件申请-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-对账文件申请-请求发送报文："+(xml));
		System.out.println("二代支付-对账文件申请-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("二代支付-对账文件申请-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		UPPSChkFileAplRs response=converyToJavaBean(reponsexml,BOSFXII.class).getUPPSChkFileAplRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="CloudProApplyTransRs")
	public CloudProApplyTransRs CloudProApplyTransRequest(
			CloudProApplyTransRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setCloudProApplyTransRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_CloudProApplyTransRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("产品信息管理--产品组申请交易-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理--产品组申请交易-请求发送报文："+(xml));
		System.out.println("产品信息管理--产品组申请交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理--产品组申请交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		CloudProApplyTransRs response=converyToJavaBean(reponsexml,BOSFXII.class).getCloudProApplyTransRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="CloudProSynchTransRs")
	public CloudProSynchTransRs CloudProSynchTransRequest(
			CloudProSynchTransRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setCloudProSynchTransRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_CloudProSynchTransRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("产品信息管理-产品组同步交易-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理-产品组同步交易-请求发送报文："+(xml));
		System.out.println("产品信息管理-产品组同步交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理-产品组同步交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		CloudProSynchTransRs response=converyToJavaBean(reponsexml,BOSFXII.class).getCloudProSynchTransRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="CloudProModTransRs")
	public CloudProModTransRs CloudProModTransRequest(CloudProModTransRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setCloudProModTransRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_CloudProModTransRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("产品信息管理-产品组修改交易-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理-产品组修改交易-请求发送报文："+(xml));
		System.out.println("产品信息管理-产品组修改交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("产品信息管理-产品组修改交易-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		CloudProModTransRs response=converyToJavaBean(reponsexml,BOSFXII.class).getCloudProModTransRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="C167CoCorpCusInqRs")
	public C167CoCorpCusInqRs EcifC167CoCorpCusInqRequest(
			C167CoCorpCusInqRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setC167CoCorpCusInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_C167CoCorpCusInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("按证件号或客户号查询对公客户信息-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("按证件号或客户号查询对公客户信息-请求发送报文："+(xml));
		System.out.println("按证件号或客户号查询对公客户信息-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("按证件号或客户号查询对公客户信息-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		C167CoCorpCusInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getC167CoCorpCusInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCustDetInqRs")
	public TCustDetInqRs ecifTCustDetInqRequest(TCustDetInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCustDetInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCustDetInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("同业客户详细查询-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("同业客户详细查询-请求发送报文："+(xml));
		System.out.println("同业客户详细查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("同业客户详细查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCustDetInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCustDetInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpAcAciModRs")
	public TDpAcAciModRs t24TDpAcActModRequest(TDpAcAciModRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpAcAciModRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpAcActModRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("利率维护及新增-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("利率维护及新增-请求发送报文："+(xml));
		System.out.println("利率维护及新增-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("利率维护及新增-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpAcAciModRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpAcAciModRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpAcctNewtdaAaaRs")
	public TDpAcctNewtdaAaaRs t24TDpAcctNewtdaAaa(TDpAcctNewtdaAaaRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpAcctNewtdaAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpAcctNewtdaAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("对公活期转定期-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对公活期转定期-请求发送报文："+(xml));
		System.out.println("对公活期转定期-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("对公活期转定期-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpAcctNewtdaAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpAcctNewtdaAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="ApprovalInqRs")
	public ApprovalInqRs CrmsApprovalInqRequest(ApprovalInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setApprovalInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_ApprovalInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("批复信息查询（同业->CRMS)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("批复信息查询（同业->CRMS)-请求发送报文："+(xml));
		System.out.println("批复信息查询（同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("批复信息查询（同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		ApprovalInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getApprovalInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="EnterpriseApproveRs")
	public EnterpriseApproveRs CrmsEnterpriseApproveRequest(
			EnterpriseApproveRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setEnterpriseApproveRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_EnterpriseApproveRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("CRMS-企信业务审批(同业->CRMS)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-企信业务审批(同业->CRMS)-请求发送报文："+(xml));
		System.out.println("CRMS-企信业务审批(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-企信业务审批(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		EnterpriseApproveRs response=converyToJavaBean(reponsexml,BOSFXII.class).getEnterpriseApproveRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="EnterpriseInqRs")
	public EnterpriseInqRs CrmsEnterpriseInqRequest(EnterpriseInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setEnterpriseInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_EnterpriseInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("CRMS-查询企信业务信息(同业->CRMS)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-查询企信业务信息(同业->CRMS)-请求发送报文："+(xml));
		System.out.println("CRMS-查询企信业务信息(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-查询企信业务信息(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		EnterpriseInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getEnterpriseInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="FinCreditApproveRs")
	public FinCreditApproveRs CrmsFinCreditApproveRequest(
			FinCreditApproveRq request) throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setFinCreditApproveRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_FinCreditApproveRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("CRMS-融贷通业务审批(同业->CRMS)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-融贷通业务审批(同业->CRMS)-请求发送报文："+(xml));
		System.out.println("CRMS-融贷通业务审批(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-融贷通业务审批(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		FinCreditApproveRs response=converyToJavaBean(reponsexml,BOSFXII.class).getFinCreditApproveRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="FinCreditInqRs")
	public FinCreditInqRs CrmsFinCreditInqRequest(FinCreditInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setFinCreditInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_FinCreditInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("CRMS-查询融贷通业务信息(同业->CRMS)-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-查询融贷通业务信息(同业->CRMS)-请求发送报文："+(xml));
		System.out.println("CRMS-查询融贷通业务信息(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("CRMS-查询融贷通业务信息(同业->CRMS)-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		FinCreditInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getFinCreditInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="ApprovalChangeRs")
	public ApprovalChangeRs iFBMApprovalChangeResponse(ApprovalChangeRq requset)
			throws Exception {
		// TODO Auto-generated method stub
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_ApprovalChangeRs);
		if(info==null?true:Integer.valueOf(info.getInfaceStatus())==0){
			BOSFXII bosfxii=new BOSFXII();
			bosfxii.setApprovalChangeRq(requset);
			String xml = convertToXml(bosfxii);
			String responsexml = SocketMessageInterceptor.returnMessage(xml, InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			ApprovalChangeRs response=converyToJavaBean(responsexml,BOSFXII.class).getApprovalChangeRs();
			return response;
		}
		ApprovalChangeRs response = new ApprovalChangeRs();
		CommonRsHdr rsHdr = new CommonRsHdr();
		List<ApprovalChangeRec> recList = new ArrayList<ApprovalChangeRec>(); 
		for(ApprovalChangeRec rec : requset.getApprovalChangeRec()){
			recList.add(rec);
		}
		RetMsg<Object> ret = creditManageService.creditApprovalChange(recList);
		if(ret.getCode().equals(RetMsgHelper.codeOk)){
			ret.setCode("000000");
		}
		rsHdr.setStatusCode(ret.getCode());
		rsHdr.setServerStatusCode(ret.getDesc());
		response.setRetCode(ret.getCode());
		response.setRetInfo(ret.getDesc());
		return response;
	}

	@Override
	//@AutoLogMethod(value="LoanNoticeRs")
	public LoanNoticeRs iFBMLoanNoticeResponse(LoanNoticeRq request)
			throws Exception {
		// TODO Auto-generated method stub
		LoanNoticeRs response = new LoanNoticeRs();
		CommonRsHdr rsHdr = new CommonRsHdr();
		
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_LoanNoticeRs);
		if(info==null?true:Integer.valueOf(info.getInfaceStatus())==0){
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info ("QuotaSyncopateRs接口状态:"+info.getInfaceStatus());
			BOSFXII bosfxii=new BOSFXII();
			bosfxii.setLoanNoticeRq(request);
			String xml = convertToXml(bosfxii);
			String responsexml = SocketMessageInterceptor.returnMessage(xml, InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			response=converyToJavaBean(responsexml,BOSFXII.class).getLoanNoticeRs();
			return response;
		}
		rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
		response.setCommonRsHdr(rsHdr);
		/****
		 *  LoanSummaryId    借据号（LD号）
			CustBrNo		       业务流水号（同业系统）
			TransDesc		       合同号
			LoanAmt		              放款金额
			Currency		       币种
			LoanType		       放款类型  正常放款（01），修改（02），冲正（03）
			CrmsLoanId		放款号
			InfoFlag		通知类型
		 */
		String retcode = null;
		String retmsg  = null;
		if(StringUtils.isEmpty(request.getCustBrNo())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "同业系统业务流水号为空";
		}if(StringUtils.isEmpty(request.getTransDesc())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "合同号为空";
		}if(StringUtils.isEmpty(request.getLoanAmt())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "放款金额为空";
		}if(StringUtils.isEmpty(request.getCurrency())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "币种为空";
		}if(StringUtils.isEmpty(request.getLoanType())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "放款类型为空";
		}if(StringUtils.isEmpty(request.getCrmsLoanId())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "放款号为空";
		}if(StringUtils.isEmpty(request.getInfoFlag())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "通知类型为空";
		}
		if(retcode != null && retmsg != null){
			rsHdr.setStatusCode(retcode);
			rsHdr.setServerStatusCode(retmsg);
			response.setRetCode(retcode);
			response.setRetInfo(retmsg);
			return response;
		}
		RetMsg<Object> ret = new RetMsg<Object>(RetMsgHelper.codeOk, "处理成功");
		String dealNo = request.getCustBrNo();
		String crmsLoanId = request.getCrmsLoanId();
		String loanSummaryId = request.getLoanSummaryId();
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("[CustBrNo同业系统业务流水号]:"+dealNo
				+"[CrmsLoanId放款号]:"+crmsLoanId
				+"[LoanSummaryId借据号（LD号）]:"+loanSummaryId
				);
		TdProductApproveMain main = productApproveService.getProductApproveActivated(dealNo);
		if(main == null)
		{
			ret.setCode("999999");
			ret.setDesc("原交易"+dealNo+"不存在");
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("原交易"+dealNo+"不存在");
		}else{
			main.setCrmsLoadId(crmsLoanId);
			if(null == StringUtils.trimToNull(loanSummaryId)){
				main.setLdNo("");
			}else{
				main.setLdNo(StringUtils.trimToNull(loanSummaryId));
			}
			productApproveService.updateMainRefNo(main);//更新LD编号，如果存在则更新;
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(
					 "[LoanType放款类型  正常放款（01），修改（02），冲正（03）]:"+request.getLoanType()
					+"[InfoFlag通知类型01-交互，02-交互失败，03-复核通过，04-复核拒绝]:"+request.getInfoFlag()
					);
			//正常放款,额度占用,交互完成  NORMAL_PAY + INTER_ACTIVE_SUCCESS
			if(DictConstants.LoanType.NORMAL_PAY.equals(request.getLoanType()) && 
					DictConstants.InfoFlag.INTER_ACTIVE_SUCCESS.equals(request.getInfoFlag())){
				/**
				 * 只做第一次的预先占用转正式占用
				 */
				//判断 TD_ED_CUST_BATCH_STATUS 是否已有 dealType=2的数据，有则过滤
				List<EdInParams> edInParams = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(BeanUtil.beanToMap(main));
				if(null != edInParams && edInParams.size()>0 && edInParams.get(0).getDealType().equalsIgnoreCase(com.singlee.capital.system.dict.DictConstants.DealType.Verify))
				{
					
				}else{
					//查询所有需要进行额度占用的记录
					List<EdInParams> creditList = productCustCreditService.getEdInParamsForProductCustCredit(BeanUtil.beanToMap(main));
					for(int i = 0;i<creditList.size();i++){
						creditList.get(i).setDealType(com.singlee.capital.system.dict.DictConstants.DealType.Verify);
					}
					//代入额度操作服务
					/**
					 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
					 * 3、保留最后操作的日志
					 * 这样处理的好处：
					 */
					List<String> dealNos =  new ArrayList<String>();
					dealNos.add(main.getDealNo());
					if(main.getPrdNo()==299)//融贷通项下流动资金接力贷款
                    {
                        try{
                            edCustManangeService.eduOccpFlowService(creditList,dealNos);
                        }catch (Exception e) {
                            // TODO: handle exception
                            ret.setCode("999999");
                            ret.setDesc(e.getMessage());
                        }
                    }
				}
			}else if(DictConstants.LoanType.ADJUST_PAY.equals(request.getLoanType()) && 
					DictConstants.InfoFlag.INTER_ACTIVE_SUCCESS.equals(request.getInfoFlag())){ 
				//放款修改 无论交互是否成功 都不做处理；除了需要判定额度修改值是否与第一次修改值有差 
			
			//正常放款  复核通过，说明CRMS联动T24成功，额度从预占用转为占用
			}else if(DictConstants.LoanType.NORMAL_PAY.equals(request.getLoanType()) && 
					DictConstants.InfoFlag.VERIFY_SUCCESS.equals(request.getInfoFlag())){
				
			//冲正,释放交易     冲正复核通过  说明交易正式结束，释放额度
			} else if(DictConstants.LoanType.REVERSE_PAY.equals(request.getLoanType()) && 
					DictConstants.InfoFlag.VERIFY_SUCCESS.equals(request.getInfoFlag())){ 
				
			}
			
			if(ret.getCode().equals(RetMsgHelper.codeOk)){
				ret.setCode("000000");
			}
		}
		rsHdr.setStatusCode(ret.getCode());
		rsHdr.setServerStatusCode(ret.getDesc());
		response.setRetCode(ret.getCode());
		response.setRetInfo(ret.getDesc());
		
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	//@AutoLogMethod(value="QuotaSyncopateRs")
	public QuotaSyncopateRs iFBMQuotaSyncopateResponse(QuotaSyncopateRq request)
			throws Exception {
		// TODO Auto-generated method stub
		QuotaSyncopateRs response = new QuotaSyncopateRs();
		CommonRsHdr rsHdr = new CommonRsHdr();
		rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
		response.setCommonRsHdr(rsHdr);
		
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_QuotaSyncopateRs);
		if(info==null?true:Integer.valueOf(info.getInfaceStatus())==0){
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info ("QuotaSyncopateRs接口状态:"+info.getInfaceStatus());
			BOSFXII bosfxii=new BOSFXII();
			bosfxii.setQuotaSyncopateRq(request);
			String xml = convertToXml(bosfxii);
			String responsexml = SocketMessageInterceptor.returnMessage(xml, InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info ("responsexml接口回执:"+responsexml);
			response=converyToJavaBean(responsexml,BOSFXII.class).getQuotaSyncopateRs();
			return response;
		}
		
		String retcode = null;
		String retmsg  = null;
		
		//为了处理DUEDATE日期
		List<QuotaSyncopateRec> recList = new ArrayList<QuotaSyncopateRec>(); 
		for(QuotaSyncopateRec rec : request.getQuotaSyncopateRec()){
//			rec.setDueDate(rec.getDueDate().length()>10?rec.getDueDate().substring(0,10).replace("-", ""):rec.getDueDate().replace("-", ""));
			rec.setDueDate(rec.getDueDate().length()>10?rec.getDueDate().substring(0,10):rec.getDueDate());
			recList.add(rec);
			
			if(StringUtils.isEmpty(rec.getEcifNum())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "ECIF号为空";
			}if(StringUtils.isEmpty(rec.getProductType())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "额度品种为空";
			}if(StringUtils.isEmpty(rec.getLoanAmt())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "授信额度为空";
			}if(StringUtils.isEmpty(rec.getTerm())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "期限类型为空";
			}if(StringUtils.isEmpty(rec.getTermType())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "期限单位为空";
			}if(StringUtils.isEmpty(rec.getDueDate())){
				retcode = InterfaceCode.TI_FAILURE;
				retmsg  = "到期日期为空";
			}
		}
		if(retcode != null && retmsg != null){
			rsHdr.setStatusCode(retcode);
			rsHdr.setServerStatusCode(retmsg);
			response.setRetCode(retcode);
			response.setRetInfo(retmsg);
			return response;
		}
		
		tiInterfaceInfoService.insertQuotaSyncopate(recList);
		RetMsg<Object>  ret = new RetMsg<Object>(RetMsgHelper.codeOk, "额度切分成功", "");
		try{
			ret = edCustManangeService.asegService(recList);
			if(ret.getCode().equals(RetMsgHelper.codeOk)){
				ret.setCode("000000");
			}
		}catch (Exception e) {
			// TODO: handle exception
			ret = FastJsonUtil.parseObject(e.getMessage(), RetMsg.class);
		}
		rsHdr.setStatusCode(ret.getCode());
		rsHdr.setServerStatusCode(ret.getDesc());
		response.setRetCode(ret.getCode());
		response.setRetInfo(ret.getDesc());
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="RepayNoticeRs")
	public RepayNoticeRs iFBMRepayNoticeResponse(RepayNoticeRq request)
			throws Exception {
		// TODO Auto-generated method stub
		RepayNoticeRs response = new RepayNoticeRs();
		CommonRsHdr rsHdr = new CommonRsHdr();
		response.setCommonRsHdr(rsHdr);
		
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_RepayNoticeRs);
		if(info==null?true:Integer.valueOf(info.getInfaceStatus())==0){
			BOSFXII bosfxii=new BOSFXII();
			bosfxii.setRepayNoticeRq(request);
			String xml = convertToXml(bosfxii);
			String responsexml = SocketMessageInterceptor.returnMessage(xml, InterfaceCode.TI_STATUS11, InterfaceCode.TI_MESSAGE11);
			response=converyToJavaBean(responsexml,BOSFXII.class).getRepayNoticeRs();
			return response;
		}
		
		String retcode = null;
		String retmsg  = null;
		if(StringUtils.isEmpty(request.getLoanSummaryId())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "放款号为空";
		}if(StringUtils.isEmpty(request.getRetAmt())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "还款金额为空";
		}if(StringUtils.isEmpty(request.getCurrency())){
			retcode = InterfaceCode.TI_FAILURE;
			retmsg  = "币种为空";
		}
		if(retcode != null && retmsg != null){
			rsHdr.setStatusCode(retcode);
			rsHdr.setServerStatusCode(retmsg);
			return response;
		}
		RetMsg<Object> ret = new RetMsg<Object>("000000", "交易成功");
		/**
		 * 只做第一次的预先占用转正式占用
		 */
		//判断 TD_ED_CUST_BATCH_STATUS 是否已有 dealType=2的数据，有则过滤
		/*****
		 * LoanSummaryId		借据号	A	    40
		   RetAmt		              还款金额	DECIMAL	26,8
		   Currency		               币种	A	    20
		 */
		Map<String, Object> param1 = new HashMap<String, Object>();
		param1.put("isActive", com.singlee.capital.system.dict.DictConstants.YesNo.YES);
		param1.put("ldNo", String.valueOf(request.getLoanSummaryId()));
		TdProductApproveMain main = productApproveService.getProductApproveByCondition(param1);
		if(main == null)
		{
			ret.setCode("000001");
			ret.setDesc("借据号为" + String.valueOf(request.getLoanSummaryId()) + "的交易不存在");
		}else{
			List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(BeanUtil.beanToMap(main));
			if(null != creditList && creditList.size()>0 && creditList.get(0).getDealType().equalsIgnoreCase(com.singlee.capital.system.dict.DictConstants.DealType.Verify))
			{
				//查询所有需要进行额度占用的记录
				//List<EdInParams> creditList = productCustCreditService.getEdInParamsForProductCustCredit(BeanUtil.beanToMap(main));
				for(int i = 0; i<creditList.size();i++){
					creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), new BigDecimal(request.getRetAmt()).doubleValue()));
					if(creditList.get(i).getAmt()<0){
						creditList.get(i).setAmt(0);
					}
				}
				//代入额度操作服务
				/**
				 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
				 * 3、保留最后操作的日志
				 * 这样处理的好处：
				 */
				List<String> dealNos =  new ArrayList<String>();
				dealNos.add(main.getDealNo());
				if(main.getPrdNo()==299)//融贷通项下流动资金接力贷款
                {
                    try{
                        edCustManangeService.eduOccpFlowService(creditList,dealNos);
                    }catch (Exception e) {
                        // TODO: handle exception
                        ret.setCode("999999");
                        ret.setDesc(e.getMessage());
                    }
                }
			}else{
				
			}
		}
		if(ret.getCode().equals(RetMsgHelper.codeOk)){
			ret.setCode("000000");
		}
		rsHdr.setRqUID(request.getCommonRqHdr().getRqUID());
		rsHdr.setStatusCode(ret.getCode());
		rsHdr.setServerStatusCode(ret.getDesc());

		return response;
	}

	@Override
	//@AutoLogMethod(value="TExLoanAllInqRs")
	public TExLoanAllInqRs t24TExLoanAllInq(TExLoanAllInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTExLoanAllInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TExLoanAllInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("贷款信息查询（EX-MATCH-STA-INQ）-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("贷款信息查询（EX-MATCH-STA-INQ）-请求发送报文："+(xml));
		System.out.println("贷款信息查询（EX-MATCH-STA-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("贷款信息查询（EX-MATCH-STA-INQ）-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TExLoanAllInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTExLoanAllInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TExLdRpmSchInqRs")
	public TExLdRpmSchInqRs t24TExLdRpmSchInq(TExLdRpmSchInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTExLdRpmSchInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TExLdRpmSchInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("T24还款计划查询-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24还款计划查询-请求发送报文："+(xml));
		System.out.println("T24还款计划查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24还款计划查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TExLdRpmSchInqRs response = converyToJavaBean(reponsexml,BOSFXII.class).getTExLdRpmSchInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
//		TExLdRpmSchInqRs response = null;
//		if(null == StringUtils.trimToNull(reponsexml))
//			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24还款计划查询-反馈报文：空 ");
//		else{
//			System.out.println("T24还款计划查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
//			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24还款计划查询-反馈报文："+XmlFormat.returnMsg(reponsexml));
//			
//			response=converyToJavaBean(reponsexml,BOSFXII.class).getTExLdRpmSchInqRs();
//	
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("retcode", response.getCommonRsHdr().getStatusCode());
//			map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
//			map.put("rquid",response.getCommonRsHdr().getRqUID());
//			
//			tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
//		}
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCoActIntInqRs")
	public TCoActIntInqRs t24TCoActIntInqRequest(TCoActIntInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCoActIntInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCoActIntInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("T24存款利率信息查询接口-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24存款利率信息查询接口-请求发送报文："+(xml));
		System.out.println("T24存款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24存款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCoActIntInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCoActIntInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TCoLoansIntInqRs")
	public TCoLoansIntInqRs t24TCoLoansIntInqRequest(TCoLoansIntInqRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTCoLoansIntInqRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TCoLoansIntInqRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("T24存款利率信息查询接口-请求发送报文："+(xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24存款利率信息查询接口-请求发送报文："+(xml));
		System.out.println("T24存款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24存款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TCoLoansIntInqRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTCoLoansIntInqRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}

	@Override
	//@AutoLogMethod(value="TDpIntBnkAaaRs")
	public TDpIntBnkAaaRs t24TDpIntBnkAaaRequest(TDpIntBnkAaaRq request)
			throws Exception {
		// TODO Auto-generated method stub
		if(request.getCommonRqHdr() == null){
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
		}
		
		tiInterfacleLogMapper.insertTiInterfaceLog(request.getCommonRqHdr());
		
		BOSFXII bosfxii=new BOSFXII();
		bosfxii.setTDpIntBnkAaaRq(request);
		String xml = convertToXml(bosfxii);
		
//		socketClientServer =new SocketClientServer();
		socketClientServer.setObject(xml);
		socketClientServer.setSocketClientIp(SystemProperties.SOCKET_GTPIP);
		socketClientServer.setSocketClientPort(SystemProperties.SOCKET_GTPPORT);
		socketClientServer.setSocketClientTimeOut(SystemProperties.SOCKET_GTPTIMEOUT);
		TiInterfaceInfo info = tiInterfaceInfoMapper.queryByInterfaceName(InterfaceCode.TI_TDpIntBnkAaaRs);
		socketClientServer.setSendflag(info==null?false:Integer.valueOf(info.getInfaceStatus())>0);
		String reponsexml = socketClientServer.start();
		
		System.out.println("T24贷款利率信息查询接口-请求发送报文："+ (xml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24贷款利率信息查询接口-请求发送报文："+ (xml));
		System.out.println("T24贷款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("T24贷款利率信息查询接口-反馈报文："+XmlFormat.returnMsg(reponsexml));
		
		TDpIntBnkAaaRs response=converyToJavaBean(reponsexml,BOSFXII.class).getTDpIntBnkAaaRs();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retcode", response.getCommonRsHdr().getStatusCode());
		map.put("retmsg", response.getCommonRsHdr().getServerStatusCode());
		map.put("rquid",response.getCommonRsHdr().getRqUID());
		
		tiInterfacleLogMapper.updateTiInterfaceRetcode(map);
		
		return response;
	}  
	 
}
