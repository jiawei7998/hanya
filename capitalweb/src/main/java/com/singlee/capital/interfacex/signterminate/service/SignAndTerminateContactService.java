package com.singlee.capital.interfacex.signterminate.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.TCoSignoffMultAaaRs;
import com.singlee.capital.interfacex.model.TCoSignonMultAaaRs;
import com.singlee.capital.interfacex.signterminate.model.SignAccount;

public interface SignAndTerminateContactService {	
	TCoSignonMultAaaRs signContract(Map<String, Object> map) throws Exception;
	
	TCoSignoffMultAaaRs terminateContract(Map<String, Object> map) throws Exception;
	
    Page<SignAccount> selectContract(Map<String, Object> map) throws Exception;
    
}
