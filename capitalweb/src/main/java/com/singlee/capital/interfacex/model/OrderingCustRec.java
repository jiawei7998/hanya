package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OrderingCustRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OrderingCustRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderingCust" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderingCustRec", propOrder = { "orderingCust" })
public class OrderingCustRec {

	@XmlElement(name = "OrderingCust", required = true)
	protected String orderingCust;

	/**
	 * Gets the value of the orderingCust property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderingCust() {
		return orderingCust;
	}

	/**
	 * Sets the value of the orderingCust property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderingCust(String value) {
		this.orderingCust = value;
	}

}
