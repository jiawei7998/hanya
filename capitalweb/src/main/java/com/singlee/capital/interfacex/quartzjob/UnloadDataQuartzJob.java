package com.singlee.capital.interfacex.quartzjob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class UnloadDataQuartzJob implements CronRunnable{
	
	private static int MAX_STR_LEN = 1024;
	/**
	 * 启动卸数shell脚本	
	 */
	
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
//		BufferedReader br = null;
		try{
			System.out.println("=======================开始处理========================");
			String classList= this.getClass().getName();
			System.out.println("classList:======"+classList);
			Map<String,String> map =new HashMap<String,String>();
			map.put("classList", classList);
			String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
			String jobName = jobExcuService.getJobByclassList(map).get(0).getJobName();
			
			JobExcuLog jobExcuLog = new JobExcuLog();
			jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
			jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
			jobExcuService.insertJobExcuLog(jobExcuLog);
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");

			
			String shpath = System.getenv().get("HOME")+"/data_source/DataDrawOut/ifbm_jp1.sh";
			LogManager.getLogger(LogManager.MODEL_BATCH).info("shpath:::"+shpath);
			Process ps = Runtime.getRuntime().exec(shpath);
			LogManager.getLogger(LogManager.MODEL_BATCH).info("ps执行开始");
			ps.waitFor();
			LogManager.getLogger(LogManager.MODEL_BATCH).info("ps执行正常");
			/*br = new BufferedReader(new InputStreamReader(ps.getInputStream(),"UTF-8"));
			LogManager.getLogger(LogManager.MODEL_BATCH).info("br获取正常");
			StringBuffer sb = new StringBuffer();
			int intC;
			while ((intC = br.read()) != -1) {
				char c = (char) intC;
				if(sb.length() >= MAX_STR_LEN) {
					throw new RException("input too long");
				}
				sb.append(c);
			}
			LogManager.getLogger(LogManager.MODEL_BATCH).info("br获取全部正常");
			String result = sb.toString();
			LogManager.getLogger(LogManager.MODEL_BATCH).info("卸数程序调用日志："+result);
			if(br != null)
				br.close();*/
			
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
			jobExcuService.updateJobExcuLog(jobExcuLog);
			System.out.println("=======================处理结束========================");
			
			
		}catch(Exception e){
			//e.printStackTrace();
			LogManager.getLogger(LogManager.MODEL_BATCH).info("卸数程序调用出现问题");
		}finally{
			/*try {
				if(br != null)
					br.close();
				LogManager.getLogger(LogManager.MODEL_BATCH).info("br关闭");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				LogManager.getLogger(LogManager.MODEL_BATCH).info("br关闭异常");
			}*/
		}
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
