package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for C201OrgCusInqRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="C201OrgCusInqRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrsnLegalType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SwiftBusId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C201OrgCusInqRq", propOrder = { "commonRqHdr", "customer",
		"prsnLegalType", "idCode", "swiftBusId" })
public class C201OrgCusInqRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "PrsnLegalType", required = true)
	protected String prsnLegalType;
	@XmlElement(name = "IdCode", required = true)
	protected String idCode;
	@XmlElement(name = "SwiftBusId", required = true)
	protected String swiftBusId;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the prsnLegalType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrsnLegalType() {
		return prsnLegalType;
	}

	/**
	 * Sets the value of the prsnLegalType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrsnLegalType(String value) {
		this.prsnLegalType = value;
	}

	/**
	 * Gets the value of the idCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdCode() {
		return idCode;
	}

	/**
	 * Sets the value of the idCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdCode(String value) {
		this.idCode = value;
	}

	/**
	 * Gets the value of the swiftBusId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSwiftBusId() {
		return swiftBusId;
	}

	/**
	 * Sets the value of the swiftBusId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSwiftBusId(String value) {
		this.swiftBusId = value;
	}

}
