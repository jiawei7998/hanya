package com.singlee.capital.interfacex.qdb.mapper;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.pojo.TdTrdSettle;

public interface TdTrdSettleMapper extends Mapper<TdTrdSettle>{
	
	public int updateTdTrdSettleState(Map<String, Object> param)throws Exception;
	
	public int updateTdTrdRetMsg(Map<String, Object> param)throws Exception;
	
	public TdTrdSettle queryTdTrdSettleByDealNo(Map<String, Object> param)throws Exception;

	public Page<TdTrdSettle> pageVerifySettles(Map<String, Object> map,RowBounds rowBounds);
	
	/**
	 * 获取审批单编号
	 * @return
	 */
	String getTradeId(HashMap<String,Object> map);
	
	
	public int updateTdTrdSettleStateForBack(Map<String, Object> param)throws Exception;
	
	public int updateTdTrdSettleUserDealNo(Map<String, Object> param)throws Exception;
	
	public Integer searchVerifySettlesCount(Map<String, Object> map);
	
}
