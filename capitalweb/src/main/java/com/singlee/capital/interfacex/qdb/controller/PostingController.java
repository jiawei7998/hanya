package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.AcupXmlBean;
import com.singlee.capital.interfacex.qdb.model.Posting;
import com.singlee.capital.interfacex.qdb.service.PostingService;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value = "/PostingController")
public class PostingController extends CommonController {
	@Autowired
	private PostingService postingService;
	
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	
	@ResponseBody
	@RequestMapping(value = "/searchPagePosting")
	public RetMsg<PageInfo<Posting>> searchPagePostingl(@RequestBody Map<String,Object> params) throws RException {
		Page<Posting> page = postingService.pagePosting(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 生成入账文件 ACUP
	 * @param postDate
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/createAcupFile")
	public boolean createAcupFile(HttpServletRequest request,@RequestBody Map<String,Object> params) throws Exception{
		return postingService.createAcup(params);
	}
	
	/**
	 * 生成入账文件 BCUP
	 * @param postDate
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/createBcupFile")
	public boolean createBcupFile(HttpServletRequest request,@RequestBody Map<String,Object> params) throws Exception {
		return postingService.createBcup(params);
	}
	
	
	/**
	 * 生成标识文件FIB
	 * @param postDate
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/createFIBFile")
	public boolean createFIBFile(HttpServletRequest request,@RequestBody Map<String,Object> params) throws Exception{
		return postingService.createFIBFile(params);
	}
	
	
	
	/**
	 * 格式化
	 * @param bigDecimal
	 * @return
	 */
	public String formatBigDecimal(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("###############0.00");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	/**
	 * hashMap深拷贝方法
	 * @param acupHashMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<Integer, AcupXmlBean> deepCopy(HashMap<Integer, AcupXmlBean> acupHashMap) throws Exception
    {
		 HashMap<Integer, AcupXmlBean> hashMap = new HashMap<Integer, AcupXmlBean>();
		 ByteArrayOutputStream bos = new ByteArrayOutputStream();
		 ObjectOutputStream oos = new ObjectOutputStream(bos);
		 oos.writeObject(acupHashMap);
		 oos.close();
		 bos.close();
		 ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		 ObjectInputStream ois = new ObjectInputStream(bis);
		 hashMap = (HashMap<Integer, AcupXmlBean>)ois.readObject();
		 ois.close();
		 bis.close();
		 return hashMap;
     }
	
	public static void deleFile(String url,String fileName) {
		//try {
		  //取得RWFileDir目录下的文件
		File file = new File(url+fileName);
        // 判断文件是否存在
        if (file.exists()) {
            // 文件删除
            file.delete();
            System.out.println("删除"+fileName+"文件成功");
        }
	}

}
