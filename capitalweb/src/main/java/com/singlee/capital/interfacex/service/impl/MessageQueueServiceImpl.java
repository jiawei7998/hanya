package com.singlee.capital.interfacex.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper;
import com.singlee.capital.interfacex.model.MessageQueueBean;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.service.MessageQueueService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.interfacex.socketservice.MessageQueueReceiverOfCopy;

@Service("messageQueueService")
public class MessageQueueServiceImpl implements MessageQueueService {

	@Autowired
	private  DictionaryGetService dictionaryGetService;
	@Autowired
	private  TiInterfaceInfoMapper tiInterfaceInfoMapper;
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	
	@Override
	public RetMsg<Object> TsaMessageQueueService() throws Exception {
		// TODO Auto-generated method stub
		RetMsg<Object> retMsg = null;
		int deeplen = 0;
		try {
				String hostname = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "hostname3").getDict_value();
				String port = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "port3").getDict_value();
				String qManager = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "qManager3").getDict_value();
				String channel = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "channel3").getDict_value();
				String ccsid    = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "ccsid3").getDict_value();
				String character = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "character3").getDict_value();
				String waitInterval  = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "waitInterval3").getDict_value();
				String messageID  = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "messageID3").getDict_value();
				String servermessageID  = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "servermessageID3").getDict_value();
				String receiveqname  = dictionaryGetService.getTaDictByCodeAndKey("tsaMqInterface", "receiveqname3").getDict_value();
				
				LogManager.getLogger(LogManager.MODEL_MQ).info("============hostname="+hostname+"   port="+port+"    qManager="+qManager+"      channel="+channel+"    receiveqname="+receiveqname);
				
				try{
				String[] Iplist = StringUtils.split(hostname, ",");
				String[] qManagerlist = StringUtils.split(qManager,",");
				
					for(int k=0;k<Iplist.length;k++){
						LogManager.getLogger(LogManager.MODEL_MQ).info("come in");
						MessageQueueBean mqbean = new MessageQueueBean();
						mqbean.setCcsId(ccsid);
						mqbean.setCharacter(character);
						mqbean.setWaitInterval(waitInterval);
						mqbean.setMessageID(messageID);
						mqbean.setServerMessageID(servermessageID);
						mqbean.setHostName(Iplist[k]);
						mqbean.setPort(port);
						mqbean.setChannel(channel);
						mqbean.setqManager(qManagerlist[k]);
						mqbean.setReceiveQname(receiveqname);
						
						LogManager.getLogger(LogManager.MODEL_MQ).info(mqbean.toString());
						
						MessageQueueReceiverOfCopy receiver = new MessageQueueReceiverOfCopy();
//						List<Object> lists = receiver.receive(mqbean,deeplen);	
						List<Object> lists = null;
						deeplen = (Integer) lists.get(0);
						LogManager.getLogger(LogManager.MODEL_MQ).info("hostname="+Iplist[k]+"   port="+port+"    qManager="+qManagerlist[k]+"      channel="+channel+"    receiveqname="+receiveqname+"Queue Depth=====deeplen=" + deeplen);
						String returnStr = null;
						for(int i=1; i<=deeplen; i++){
							returnStr = (String) lists.get(i);
							//解析msgString
							if(returnStr==null|| "".equals(returnStr)){
								System.out.println("none");
							}else{
								LogManager.getLogger(LogManager.MODEL_MQ).info("动账通知报文"+returnStr);
//								Root root1 = MessageQueueReceiverOfCopy.converyToJavaBean(returnStr,Root.class);
								Root root1 = null;
								Map<String,Object> tmap = new HashMap<String, Object>();
								String tflowStr = root1.getTFlow();
								tmap.put("tFlow", tflowStr.split(","));
								//先查看是否数据库中已存在BEA号码，如果存在则不进行插入
								List<Root> rootList = tiInterfaceInfoMapper.searchTiTsaAccountByTno(tmap);
								if(rootList == null || rootList.size()<=0){
									//判断是否为贷方
									if(root1.getTranType() !=null && "2".equals(root1.getTranType())){
										//根据root1.getTFlow()的值去判断清算表中是否有相同的值										
										if(ti2ndPaymentMapper.querySrquidCount(root1.getTFlow())<=0){
										if(root1.getTFlow().contains("FT")){
//											Map<String, Object> mapt = new HashMap<String, Object>();
//											List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
//											mapt.put("ccy", root1.getPayBz());
//											if(acct != null && acct.size() >0){
//												for (int j = 0; j < acct.size(); j++) {
//													
//												}
//											}
											//取值即可
											tiInterfaceInfoMapper.insertTiTsaAccount(root1);
										}
										}
									}
								}
							}
						}
					}
				}catch(Exception e){
					//e.printStackTrace();
					LogManager.getLogger(LogManager.MODEL_MQ).info(e.getMessage());
				}
				
				
					
				
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			retMsg = new RetMsg<Object>(InterfaceCode.TI_FAILURE, e.getMessage(), "", null);
			return retMsg;
		}
		
		retMsg = new RetMsg<Object>(InterfaceCode.TI_SUCCESS, InterfaceCode.TI_SUCMSG, "", null);
		return retMsg;
	}
	


}

