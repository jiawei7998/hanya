package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TStFtRemitAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TStFtRemitAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="SPRsUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DebitDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStFtRemitAaaRs", propOrder = { "commonRsHdr", "spRsUID",
		"debitDate", "cntId", "companyNo" })
public class TStFtRemitAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "SPRsUID", required = true)
	protected String spRsUID;
	@XmlElement(name = "DebitDate", required = true)
	protected String debitDate;
	@XmlElement(name = "CntId", required = true)
	protected String cntId;
	@XmlElement(name = "CompanyNo", required = true)
	protected String companyNo;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the spRsUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPRsUID() {
		return spRsUID;
	}

	/**
	 * Sets the value of the spRsUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPRsUID(String value) {
		this.spRsUID = value;
	}

	/**
	 * Gets the value of the debitDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDebitDate() {
		return debitDate;
	}

	/**
	 * Sets the value of the debitDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDebitDate(String value) {
		this.debitDate = value;
	}

	/**
	 * Gets the value of the cntId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntId() {
		return cntId;
	}

	/**
	 * Sets the value of the cntId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntId(String value) {
		this.cntId = value;
	}

	/**
	 * Gets the value of the companyNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyNo() {
		return companyNo;
	}

	/**
	 * Sets the value of the companyNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyNo(String value) {
		this.companyNo = value;
	}

}
