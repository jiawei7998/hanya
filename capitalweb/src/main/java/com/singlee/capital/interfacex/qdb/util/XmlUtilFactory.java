package com.singlee.capital.interfacex.qdb.util;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


@SuppressWarnings("unchecked")
public class XmlUtilFactory {

	public static String getXml(String xmlSchema,Map<String,Map<String,String>> map){
		SAXReader reader = new SAXReader();
		Document xmlDocAccept;
		String xml="";
		try {
			xmlDocAccept = reader.read(xmlSchema);
			List<Element> eles=xmlDocAccept.getRootElement().elements();
			for (int i=0;i< eles.size();i++) {
				if(map.containsKey(eles.get(i).getName())){
					replaceValue(eles.get(i), map.get(eles.get(i).getName()));
				}
			}
			xml=xmlDocAccept.asXML();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		System.out.println(xml);
		return xml;
	}
	public static Map<Integer, HashMap<String, Object>> parseXml(Map<String,Object> xmlResultString) throws Exception{
		/*reader.setEncoding("UTF-8");*/
		Map<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
		Map<String, Object> campHdr = new HashMap<String, Object>();//campHdr
        Map<String, Object> appHdr = new HashMap<String, Object>();
        Map<Integer, HashMap<String, Object>> appBody = new HashMap<Integer, HashMap<String,Object>>();//appBody
		try {
			praseEsbXmlPackageDescription((byte[]) xmlResultString.get("Bytes"),svcHdr,campHdr,appHdr,appBody);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return appBody;
	}
	public static void getElementList(Element element,Map<String, Object> svcHdr)
	{
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{			
			svcHdr.put(StringUtils.trimToEmpty(element.getName()), StringUtils.trimToEmpty(element.getText()));
		}else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				getElementList(element2,svcHdr);
			}
		}
	}
	public static void replaceValue(Element ele,Map<String,String> map){
		List<Element> eles=ele.elements();
		if(eles.size()>0){
			for (int i=0;i<eles.size();i++) {
				if(map.containsKey(eles.get(i).getName())){
					eles.get(i).setText(map.get(eles.get(i).getName()));
				}else{
					for (Element element : eles) {
						replaceValue(element, map);
					}
				}
			}
		}
	}
	public static void praseEsbXmlPackageDescription(byte[] responseXml,Map<String, Object> svcHdr,Map<String, Object> campHdr,Map<String,Object> appHdr,Map<Integer, HashMap<String, Object>> appBody) throws Exception
	{
		ByteArrayInputStream byteArrayInputStream = null;
		try{
			SAXReader reader = new SAXReader();
			byteArrayInputStream = new ByteArrayInputStream(responseXml);
			Document xmlDoc = reader.read(byteArrayInputStream);
			
			List<Element> elements = xmlDoc.getRootElement().elements();
			for(Element element:elements)
			{
				if("svcHdr".equals(element.getName()))
				{					
					getElementList(element,svcHdr);					
				}else 
				if("campHdr".equals(element.getName()))
				{					
					getElementList(element,campHdr);				
				}else 
				if("appHdr".equals(element.getName()))
				{					
					getElementList(element,appHdr);				
				}else 
				if("appBody".equals(element.getName()))
				{					
					List<Element> childElements = element.elements();
					HashMap<String, Object> childHashMap = null;
					int count = 1;
					for(Element childelement:childElements)
					{					
						childHashMap = new HashMap<String, Object>();
						getElementList(childelement, childHashMap);
						appBody.put(count, childHashMap);
						count++;
					}
				}
			}
		}catch (Exception e) {		
			e.printStackTrace();
		}finally{
			if(null != byteArrayInputStream)
			{
				byteArrayInputStream.close();
			}
		}
	}
	public static Object getBeanByXml(Class<?> clazz,String xml){
		Object obj=null;
		try {
			obj=clazz.newInstance();
			Document doc = DocumentHelper.parseText(xml);
			Element element=doc.getRootElement().element("appBody");
			Element respE=doc.getRootElement().element("appHdr");
			Field[] fields=clazz.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				if(element.elementTextTrim(f.getName())!=null&&!"".equals(element.elementTextTrim(f.getName()))){
					f.set(obj, element.elementTextTrim(f.getName()));	
				}
				if(respE.elementTextTrim(f.getName())!=null&&!"".equals(respE.elementTextTrim(f.getName()))){
					f.set(obj, respE.elementTextTrim(f.getName()));	
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	public static List<Object> getBeansByXml(Class<?> clazz,String xml,String area){
		List<Object> objs=new ArrayList<Object>();
		Object obj=null;
		try {
			Document doc = DocumentHelper.parseText(xml);
			Element element=doc.getRootElement().element("appBody");
			List<Element> eles=element.elements(area);
			Element respE=doc.getRootElement().element("appHdr");
			Field[] fields=clazz.getDeclaredFields();
			for (Element ele : eles) {
				obj=clazz.newInstance();
				for (Field f : fields) {
					f.setAccessible(true);
					if(ele.elementTextTrim(f.getName())!=null&&!"".equals(ele.elementTextTrim(f.getName()))){
						f.set(obj, ele.elementTextTrim(f.getName()));	
					}
					if(respE.elementTextTrim(f.getName())!=null&&!"".equals(respE.elementTextTrim(f.getName()))){
						f.set(obj, respE.elementTextTrim(f.getName()));	
					}
				}
				objs.add(obj);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objs;
	}
}
