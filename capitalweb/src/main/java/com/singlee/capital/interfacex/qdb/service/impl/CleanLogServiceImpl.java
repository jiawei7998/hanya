package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.interfacex.qdb.service.CleanLogService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
@Service("CleanLogServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CleanLogServiceImpl implements CleanLogService {

	@Override
	public void zipAndCleanning() {
		try {
			//获取需要清理日志的路径path
			String path= PathUtils.getAbsPath("log");
			Calendar cal=Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			//打包zip文件
			generateZip(path, path+"//remove//","-", true,new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param path  选取的log日志的路径
	 * @param returnPath 生成的.zip的路径
	 * @param date "-",删除带有“-”的文件
	 * @param isDrop true，删除
	 * @param time 生成文件的日期，例如remove2017-11-01.zip中的2017-11-01
	 * @return
	 * @throws Exception
	 */
	public static String generateZip(String path, String returnPath,String date,
			Boolean isDrop, String time) throws Exception {
		List<File> files = new ArrayList<File>();
		File file = new File(path);
		//所有的file文件都要先判断路径存不存在
		if (!file.exists()) {
			throw new Exception("原因文件：[ " + file.getPath() + " ]不存在，打包失败！");
		}
		if (file.exists() && file.isFile()) {
			files.add(file);
		} else if (file.exists() && file.isDirectory()) {
			File[] fil = new File(path).listFiles();
			for (int i = 0; i < fil.length; i++) {
				if(fil[i].getName().contains(date)){
					files.add(fil[i]);
				}
			}
		}
		File outFile = new File(returnPath);
		File fileMidir = new File(outFile.getParent());
		if (!fileMidir.exists()) {
			boolean isMkDirs = fileMidir.mkdirs();
			if (!isMkDirs) {
				throw new Exception("存放压缩文件目录：[ " + outFile.getName()
						+ " ]，创建目录失败！");
			}
		}
		if (!outFile.exists() || !outFile.isFile()) {
			//目录不存在，则创建路径
			if (!outFile.isFile()) {
				outFile.mkdir();
				outFile = new File(outFile.getPath() + "/" + outFile.getName()+time
						+ ".zip");
			}
			//创建目录是否成功。
			boolean isMkDirs = outFile.createNewFile();
			if (!isMkDirs) {
				throw new Exception("压缩文件目录：[ " + outFile.getName()
						+ " ]，创建压缩文件失败！");
			}
		}
		// 创建文件输出流
		FileOutputStream fous = new FileOutputStream(outFile);
		/**
		 * 打包的方法我们会用到ZipOutputStream这样一个输出流, 所以这里我们把输出流转换一下
		 */
		ZipOutputStream zipOut = new ZipOutputStream(fous);
		try {
			/**
			 * 这个方法接受的就是一个所要打包文件的集合， 还有一个ZipOutputStream
			 */
			if (files != null && files.size() > 0) {
				zipFile(files, zipOut);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			zipOut.close();
			fous.close();
			if (isDrop) {
				clean(new File(path),date);
			}
		}
		return outFile.getPath();
	}
	private static void zipFile(List<File> files, ZipOutputStream outputStream) {
		int size = files.size();
		for (int i = 0; i < size; i++) {
			File file = (File) files.get(i);
			zipFile(file, outputStream);
		}
	}
	private static void zipFile(File inputFile, ZipOutputStream ouputStream) {
		try {
			if (inputFile.exists()) {
				/**
				 * 如果是目录的话这里是不采取操作的， 至于目录的打包正在研究中
				 */
				if (inputFile.isFile()) {
					FileInputStream IN = new FileInputStream(inputFile);
					BufferedInputStream bins = new BufferedInputStream(IN, 512);
					ZipEntry entry = new ZipEntry(inputFile.getName());
					ouputStream.putNextEntry(entry);
					// 向压缩文件中输出数据
					int nNumber;
					byte[] buffer = new byte[1024];
					while ((nNumber = bins.read(buffer)) != -1) {
						ouputStream.write(buffer, 0, nNumber);
					}
					// 关闭创建的流对象
					bins.close();
					IN.close();
				} else {
					try {
						File[] files = inputFile.listFiles();
						for (int i = 0; i < files.length; i++) {
							zipFile(files[i], ouputStream);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 清空文件和文件目录
	 * 
	 * @param f
	 */
	private static void clean(File f,String date) throws Exception {
		String[] cs = f.list();
		if (cs == null || cs.length <= 0) {
			f.delete();
		} else {
			for (int i = 0; i < cs.length; i++) {
				String cn = cs[i];
				String cp = f.getPath() + File.separator + cn;
				File f2 = new File(cp);
				if(f2.getName().contains(date)){
					if (f2.exists() && f2.isFile()) {
						f2.delete();
					} else if (f2.exists() && f2.isDirectory()) {
						clean(f2,date);
					}
				}
			}
			f.delete();
		}
	}
}
