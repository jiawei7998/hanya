package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for FinCreditInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FinCreditInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubContractNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IndusType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BaseRateKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CInteType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ForcatsProfitRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OverdueRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IncomeDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransferDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RecvDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Basis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SyInteRefre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestFrequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntPymtMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinCreditInqRec", propOrder = { "partyName", "subContractNum",
		"indusType", "region", "currency", "contractAmt", "rateType",
		"baseRateKey", "cInteType", "forcatsProfitRate", "penSpread",
		"incomeDate", "transferDate", "dueDate", "recvDate", "basis",
		"syInteRefre", "interestFrequency", "intPymtMethod","ecifNum","loanUse" })
public class FinCreditInqRec {

	@XmlElement(name = "PartyName", required = true)
	protected String partyName;
	@XmlElement(name = "SubContractNum", required = true)
	protected String subContractNum;
	@XmlElement(name = "IndusType", required = true)
	protected String indusType;
	@XmlElement(name = "Region", required = true)
	protected String region;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "ContractAmt", required = true)
	protected String contractAmt;
	@XmlElement(name = "RateType", required = true)
	protected String rateType;
	@XmlElement(name = "BaseRateKey", required = true)
	protected String baseRateKey;
	@XmlElement(name = "CInteType", required = true)
	protected String cInteType;
	@XmlElement(name = "ForcatsProfitRate", required = true)
	protected String forcatsProfitRate;
	@XmlElement(name = "PenSpread", required = true)
	protected String penSpread;
	@XmlElement(name = "IncomeDate", required = true)
	protected String incomeDate;
	@XmlElement(name = "TransferDate", required = true)
	protected String transferDate;
	@XmlElement(name = "DueDate", required = true)
	protected String dueDate;
	@XmlElement(name = "RecvDate", required = true)
	protected String recvDate;
	@XmlElement(name = "Basis", required = true)
	protected String basis;
	@XmlElement(name = "SyInteRefre", required = true)
	protected String syInteRefre;
	@XmlElement(name = "InterestFrequency", required = true)
	protected String interestFrequency;
	@XmlElement(name = "IntPymtMethod", required = true)
	protected String intPymtMethod;
	@XmlElement(name = "LoanUse", required = true)
	protected String loanUse;

	public String getLoanUse() {
		return loanUse;
	}
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}

	protected String ecifNum;
	public String getEcifNum() {
		return ecifNum;
	}
	public void setEcifNum(String ecifNum) {
		this.ecifNum = ecifNum;
	}

	/**
	 * Gets the value of the partyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPartyName() {
		return partyName;
	}

	/**
	 * Sets the value of the partyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPartyName(String value) {
		this.partyName = value;
	}

	/**
	 * Gets the value of the subContractNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubContractNum() {
		return subContractNum;
	}

	/**
	 * Sets the value of the subContractNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubContractNum(String value) {
		this.subContractNum = value;
	}

	/**
	 * Gets the value of the indusType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndusType() {
		return indusType;
	}

	/**
	 * Sets the value of the indusType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndusType(String value) {
		this.indusType = value;
	}

	/**
	 * Gets the value of the region property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Sets the value of the region property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegion(String value) {
		this.region = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the contractAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContractAmt() {
		return contractAmt;
	}

	/**
	 * Sets the value of the contractAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContractAmt(String value) {
		this.contractAmt = value;
	}

	/**
	 * Gets the value of the rateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRateType() {
		return rateType;
	}

	/**
	 * Sets the value of the rateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRateType(String value) {
		this.rateType = value;
	}

	/**
	 * Gets the value of the baseRateKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBaseRateKey() {
		return baseRateKey;
	}

	/**
	 * Sets the value of the baseRateKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBaseRateKey(String value) {
		this.baseRateKey = value;
	}

	/**
	 * Gets the value of the cInteType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCInteType() {
		return cInteType;
	}

	/**
	 * Sets the value of the cInteType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCInteType(String value) {
		this.cInteType = value;
	}

	/**
	 * Gets the value of the forcatsProfitRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getForcatsProfitRate() {
		return forcatsProfitRate;
	}

	/**
	 * Sets the value of the forcatsProfitRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setForcatsProfitRate(String value) {
		this.forcatsProfitRate = value;
	}

	/**
	 * Gets the value of the penSpread property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPenSpread() {
		return penSpread;
	}

	/**
	 * Sets the value of the penSpread property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPenSpread(String value) {
		this.penSpread = value;
	}

	/**
	 * Gets the value of the incomeDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIncomeDate() {
		return incomeDate;
	}

	/**
	 * Sets the value of the incomeDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIncomeDate(String value) {
		this.incomeDate = value;
	}

	/**
	 * Gets the value of the transferDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransferDate() {
		return transferDate;
	}

	/**
	 * Sets the value of the transferDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransferDate(String value) {
		this.transferDate = value;
	}

	/**
	 * Gets the value of the dueDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * Sets the value of the dueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDueDate(String value) {
		this.dueDate = value;
	}

	/**
	 * Gets the value of the recvDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecvDate() {
		return recvDate;
	}

	/**
	 * Sets the value of the recvDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRecvDate(String value) {
		this.recvDate = value;
	}

	/**
	 * Gets the value of the basis property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBasis() {
		return basis;
	}

	/**
	 * Sets the value of the basis property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBasis(String value) {
		this.basis = value;
	}

	/**
	 * Gets the value of the syInteRefre property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSyInteRefre() {
		return syInteRefre;
	}

	/**
	 * Sets the value of the syInteRefre property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSyInteRefre(String value) {
		this.syInteRefre = value;
	}

	/**
	 * Gets the value of the interestFrequency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInterestFrequency() {
		return interestFrequency;
	}

	/**
	 * Sets the value of the interestFrequency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInterestFrequency(String value) {
		this.interestFrequency = value;
	}

	/**
	 * Gets the value of the intPymtMethod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntPymtMethod() {
		return intPymtMethod;
	}

	/**
	 * Sets the value of the intPymtMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntPymtMethod(String value) {
		this.intPymtMethod = value;
	}

}
