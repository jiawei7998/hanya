package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AcctWithBankRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AcctWithBankRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctWithBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctWithBankRec", propOrder = { "acctWithBank" })
public class AcctWithBankRec {

	@XmlElement(name = "AcctWithBank", required = true)
	protected String acctWithBank;

	/**
	 * Gets the value of the acctWithBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctWithBank() {
		return acctWithBank;
	}

	/**
	 * Sets the value of the acctWithBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctWithBank(String value) {
		this.acctWithBank = value;
	}

}
