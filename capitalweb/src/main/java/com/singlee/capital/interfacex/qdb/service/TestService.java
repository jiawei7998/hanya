package com.singlee.capital.interfacex.qdb.service;

import java.util.List;

import com.singlee.capital.trade.model.TtTrdTrade;

public interface TestService<T> {
	public List<TtTrdTrade> getAll();
}
