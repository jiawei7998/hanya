package com.singlee.capital.interfacex.socketservice;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import com.singlee.capital.common.util.JY;


//@Service
public class ServerSocketShutdownListener implements ApplicationListener<ContextClosedEvent> {

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		JY.info("..............................................");
		JY.info(sysname()+"系统shutdown工作开始");
		JY.info("..............................................");
		shutdown(event.getApplicationContext());
		
		JY.info("....ConetextClosed Source :" +event.getSource().getClass().getName());
		JY.info("....ConetextClosed Source :" +event.getSource().toString());
		
		JY.info("..............................................");
		JY.info(sysname()+"系统shutdown工作结束");
		JY.info("..............................................");
	}

	//@Autowired
	private SocketServerForRecieve ssfr;
	protected String sysname() {
		return "socket server X ";
	}

	protected void shutdown(ApplicationContext ac) {
		ssfr.shutdownMethod();
	}
	
}