package com.singlee.capital.interfacex.qdb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.AcupXmlBean;
import com.singlee.capital.interfacex.qdb.model.Posting;


/****
 * 
 * 日终账务接口
 * @author singlee
 *
 */
public interface PostingService {
	
	public Page<Posting> pagePosting(Map<String, Object> map);
	
	public List<Posting> listPosting(Map<String, Object> map);
	
	public HashMap<Integer, AcupXmlBean> queryAcupXmlConfig(String XmlPath) throws Exception;
	
	public void messageXMLFileSaveLocal(String data,String path, String filename, String date) throws Exception;
	
    public List<Posting> zlistPosting(Map<String, Object> map);
	
	public Posting sumValuePosting(Map<String, Object> map);
	
	public Posting countPosting(Map<String, Object> map);
	
	public boolean createAcup(Map<String, Object> map);
	public boolean createBcup(Map<String, Object> map);
	public boolean createFIBFile(Map<String, Object> map);
	
	public boolean createPostingFile(Map<String, Object> map);

}
