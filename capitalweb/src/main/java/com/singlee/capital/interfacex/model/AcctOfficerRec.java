package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AcctOfficerRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AcctOfficerRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctOfficer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DeviceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CsmgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MainAeAlPer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctOfficerRec", propOrder = { "acctOfficer", "deviceNo",
		"csmgName", "mainAeAlPer" })
public class AcctOfficerRec {

	@XmlElement(name = "AcctOfficer", required = true)
	protected String acctOfficer;
	@XmlElement(name = "DeviceNo", required = true)
	protected String deviceNo;
	@XmlElement(name = "CsmgName", required = true)
	protected String csmgName;
	@XmlElement(name = "MainAeAlPer", required = true)
	protected String mainAeAlPer;

	/**
	 * Gets the value of the acctOfficer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctOfficer() {
		return acctOfficer;
	}

	/**
	 * Sets the value of the acctOfficer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctOfficer(String value) {
		this.acctOfficer = value;
	}

	/**
	 * Gets the value of the deviceNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeviceNo() {
		return deviceNo;
	}

	/**
	 * Sets the value of the deviceNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDeviceNo(String value) {
		this.deviceNo = value;
	}

	/**
	 * Gets the value of the csmgName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCsmgName() {
		return csmgName;
	}

	/**
	 * Sets the value of the csmgName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCsmgName(String value) {
		this.csmgName = value;
	}

	/**
	 * Gets the value of the mainAeAlPer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMainAeAlPer() {
		return mainAeAlPer;
	}

	/**
	 * Sets the value of the mainAeAlPer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMainAeAlPer(String value) {
		this.mainAeAlPer = value;
	}

}
