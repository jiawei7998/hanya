package com.singlee.capital.interfacex.mapper;

import java.util.Map;

import com.singlee.capital.interfacex.model.CommonRqHdr;

public interface TiInterfaceLogMapper {
	
	public void insertTiInterfaceLog(CommonRqHdr commonRqHdr);

	public void updateTiInterfaceRetcode(Map<String, Object> map);
}
