package com.singlee.capital.interfacex.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.cron.mapper.TaJobMapper;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.interfacex.mapper.NightJobStatusMapper;
import com.singlee.capital.interfacex.mapper.TiJobExcuLogMapper;
import com.singlee.capital.interfacex.mapper.TiJobExcuRecordMapper;
import com.singlee.capital.interfacex.mapper.TiJobExcuTermMapper;
import com.singlee.capital.interfacex.model.DayendInfoRecord;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.model.JobExcuRecord;
import com.singlee.capital.interfacex.model.JobExcuTerm;
import com.singlee.capital.interfacex.model.NightJobStatus;
import com.singlee.capital.interfacex.service.JobExcuService;



@Service("jobExcuService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class JobExcuServiceImpl implements JobExcuService{
	@Autowired
	private TiJobExcuLogMapper tiJobExcuLogMapper;
	@Autowired
	private TaJobMapper taJobMapper;
	@Autowired
	private TiJobExcuTermMapper tiJobExcuTermMapper;
	
	@Autowired
	private TiJobExcuRecordMapper tiJobExcuRecordMapper;
	@Autowired
	private NightJobStatusMapper jobStatusMapper;
	

	@Override
	public void insertJobExcuLog(JobExcuLog jobExcuLog) {
		// TODO Auto-generated method stub
		tiJobExcuLogMapper.insertJobExcuLog(jobExcuLog);
	}


	@Override
	public void updateJobExcuLog(JobExcuLog jobExcuLog) {
		// TODO Auto-generated method stub
		tiJobExcuLogMapper.updateJobExcuLog(jobExcuLog);
	}


	@Override
	public int getJobExcuLog(JobExcuLog jobExcuLog) {
		// TODO Auto-generated method stub
		return tiJobExcuLogMapper.getJobExcuLog(jobExcuLog);
	}


	@Override
	public List<TaJob> getJobByclassList(Map<String, String> params) {
		// TODO Auto-generated method stub
		return taJobMapper.getJobByclassList(params);
	}


	@Override
	public List<TaJob> getEnableJobList(Map<String, String> params) {
		// TODO Auto-generated method stub
		return taJobMapper.getEnableJobList(params);
	}


	@Override
	public List<JobExcuTerm> selectJobExcuTerm(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return tiJobExcuTermMapper.selectJobExcuTerm(map);
	}


	@Override
	public void insertJobExcuRecord(JobExcuRecord jobExcuRecord) {
		// TODO Auto-generated method stub
		tiJobExcuRecordMapper.insertJobExcuRecord(jobExcuRecord);
	}



	@Override
	public int getJobExcuRecord(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return tiJobExcuRecordMapper.getJobExcuRecord(map);
	}


	@Override
	public List<DayendInfoRecord> getDayendJobStatus(Map<String, String> params) {
		// TODO Auto-generated method stub
		return tiJobExcuLogMapper.getDayendJobStatus(params);
	}


	@Override
	public List<NightJobStatus> searchDayendJobStatus(Map<String, String> params) {
		// TODO Auto-generated method stub
		return jobStatusMapper.searchDayendJobStatus(params);
	}
	

}