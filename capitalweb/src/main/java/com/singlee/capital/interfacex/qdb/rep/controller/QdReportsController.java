package com.singlee.capital.interfacex.qdb.rep.controller;

import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportValTradeBean;
import com.singlee.capital.interfacex.qdb.rep.service.ReportService;
import com.singlee.capital.interfacex.qdb.util.Reports;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.service.TdFeesBrokerService;

@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/QdReportsController")
public class QdReportsController extends CommonController{

	
	@Resource
	private ReportService reportService;
	
	@Resource(name = "tdFeesBrokerServiceImpl")
	private TdFeesBrokerService tdFeesBrokerServiceImpl;

	
	/****
	 * 委外业务统计表
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReportEntrustPage")
	public RetMsg<PageInfo<ReportEntrustBean>> getReportEntrustPage(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportEntrustBean> page = reportService.pageReportEntrust(params);
		return RetMsgHelper.ok(page);
	}
	
	/****
	 * 委外投资债券台账
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReportEntrustSecPage")
	public RetMsg<PageInfo<ReportEntrustSecBean>> getReportEntrustSecPage(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportEntrustSecBean> page = reportService.pageReportEntrustSec(params);
		return RetMsgHelper.ok(page);
	}
	/****
	 * 存放同业、同业借款报表
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReportInterBankPage")
	public RetMsg<PageInfo<ReportInterBankBean>> getReportInterBankPage(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportInterBankBean> page = reportService.pageReportInterBank(params);
		return RetMsgHelper.ok(page);
	}
	/****
	 * 债券台账明细
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReportSecPage")
	public RetMsg<PageInfo<ReportSecBean>> getReportSecPage(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportSecBean> page = reportService.pageReportSecBean(params);
		return RetMsgHelper.ok(page);
	}
	
	/***
	 * 非标资产交易台账
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReportTradePage")
	public RetMsg<PageInfo<ReportTradeBean>> getReportTradePage(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportTradeBean> param = reportService.pageReportTradeBean(params);
		return RetMsgHelper.ok(param);
	}
	
	/***
	 * 存放同业（定期）、拆出资金、买入返售（债券）减值基础数据表
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/pageReportDeValInterBankBean")
	public RetMsg<PageInfo<ReportDeValInterBankBean>> pageReportDeValInterBankBean(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportDeValInterBankBean> param = reportService.pageReportDeValInterBankBean(params);
		return RetMsgHelper.ok(param);
	}
	
	/***
	 * 非债业务减值基础数据表
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/pageReportDeValTradeBean")
	public RetMsg<PageInfo<ReportDeValTradeBean>> pageReportDeValTradeBean(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportDeValTradeBean> param = reportService.pageReportDeValTradeBean(params);
		return RetMsgHelper.ok(param);
	}
	
	/***
	 * 非债业务估值基础数据表
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/pageReportValTradeBean")
	public RetMsg<PageInfo<ReportValTradeBean>> pageReportValTradeBean(@RequestBody Map<String, Object> params) throws RException{
		Page<ReportValTradeBean> param = reportService.pageReportValTradeBean(params);
		return RetMsgHelper.ok(param);
	}
	
	
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportEntrustRep")
	public void exportEntrustRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportEntrustRep(map);
			downLoadFile(request,response, Reports.ReportEntrust.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/exportBassAsset")
	public void exportBassAsset(HttpServletRequest request, HttpServletResponse response){
		try {
			
			String	reportDate = DateUtil.getCurrentDateAsString();
			byte[] retMsg = reportService.exportBassAsset();
			downLoadFile(request,response, Reports.BassAsset.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	@ResponseBody
	@RequestMapping(value="/exportTradInvMain")
	public void exportTradInvMain(HttpServletRequest request,HttpServletResponse response){
		try{
		String	reportDate = DateUtil.getCurrentDateAsString();
		byte[] retMsg = reportService.exportTradInvMain();
		downLoadFile(request,response, Reports.TradIvMain.getDesc() + "_" + reportDate + ".xls", retMsg);
		} catch (Exception e) {
		//e.printStackTrace();
	}
	}
	
	@ResponseBody
	@RequestMapping(value="/exportCustNews")
	public void exportCustNews(HttpServletRequest request,HttpServletResponse response){
		try{
		String	reportDate = DateUtil.getCurrentDateAsString();
		byte[] retMsg = reportService.exportCustNews();
		downLoadFile(request,response, Reports.CustNews.getDesc() + "_" + reportDate + ".xls", retMsg);
		} catch (Exception e) {
		//e.printStackTrace();
	}
	}
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportEntrustSecRep")
	public void exportEntrustSecRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportEntrustSecRep(map);
			downLoadFile(request,response, Reports.ReportEntrustSec.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportInterBankRep")
	public void exportInterBankRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportInterBankRep(map);
			downLoadFile(request,response, Reports.ReportInterBank.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportSecRep")
	public void exportSecRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportSecRep(map);
			downLoadFile(request,response, Reports.ReportSec.getDesc() + "_" + reportDate + ".xls", retMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportTradeRep")
	public void exportTradeRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportTradeRep(map);
			downLoadFile(request,response, Reports.ReportTrade.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportDeValInterBankRep")
	public void exportDeValInterBankRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportDeValInterBankRep(map);
			downLoadFile(request,response, Reports.ReportDeValInterBank.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportDeValTrade")
	public void exportDeValTrade(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportDeValTrade(map);
			downLoadFile(request,response, Reports.ReportDeValTrade.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exportValTradeRep")
	public void exportValTradeRep(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportService.exportValTradeRep(map);
			downLoadFile(request,response, Reports.ReportValTrade.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expFeesBroker")
	public void expFeesBroker(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String dealDate = map.get("dealDate") == null ? "" : String.valueOf(map.get("dealDate"));
			map.put("dealDate", dealDate);
			byte[] retMsg = tdFeesBrokerServiceImpl.exportRep(map);
			downLoadFile(request,response, Reports.FeesBroker.getDesc() + "_" + dealDate + ".xls", retMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void downLoadFile(HttpServletRequest request,HttpServletResponse response,String fileName,byte[] retMsg)throws Exception
	{
		String ua = request.getHeader("User-Agent");
		if (ua != null && ua.indexOf("Firefox") >= 0 || ua.indexOf("Chrome") >= 0 || ua.indexOf("Safari") >= 0) {  
		   fileName = new String((fileName).getBytes(), "GBK");  
		} else {  
		   fileName = URLEncoder.encode(fileName,"UTF8"); //其他浏览器  
		}
		
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + new String(fileName.getBytes(), "ISO8859-1"));
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes("GBK"), "ISO8859-1"));
		}
		response.setHeader("Connection", "close");
		if(null != retMsg) {
			response.getOutputStream().write(retMsg);}
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
	
	/**
	 * 读取request中的参数,转换为map对象
	 * 
	 * @param request
	 * @return
	 */
	private static HashMap<String,Object> requestParamToMap(HttpServletRequest request) 
	{
		HashMap<String,Object> parameters = new HashMap<String,Object>();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) 
		{
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (StringUtils.isNotEmpty(paramValue)) 
			{
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
}
