package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.NightJobStatus;

public interface NightJobStatusMapper extends Mapper<NightJobStatus>{
		

	public List<NightJobStatus> searchDayendJobStatus(Map<String, String> params);
	
}
