package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ChargeRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChargeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeRec", propOrder = { "chargeType", "chargeAmt" })
public class ChargeRec {

	@XmlElement(name = "ChargeType", required = true)
	protected String chargeType;
	@XmlElement(name = "ChargeAmt", required = true)
	protected String chargeAmt;

	/**
	 * Gets the value of the chargeType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * Sets the value of the chargeType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeType(String value) {
		this.chargeType = value;
	}

	/**
	 * Gets the value of the chargeAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeAmt() {
		return chargeAmt;
	}

	/**
	 * Sets the value of the chargeAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeAmt(String value) {
		this.chargeAmt = value;
	}

}
