package com.singlee.capital.interfacex.signterminate.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TI_SIGN_ACCT")
public class SignAccount implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_PLAN.NEXTVAL FROM DUAL")
	private String accountID;
	private String accountNumber;
	private String accountType;
	private String sponsor;
	private String sponsorInstitution;
	private String opTime;
	private String singType;
	private String tSponsor;
	private String tSponInstitution;
	private String tOperationTime;
	
	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public void setAccountType(String accounType) {
		this.accountType = accounType;
	}
	
	public String getSponsor() {
		return sponsor;
	}
	
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	
	public String getSponsorInstitution() {
		return sponsorInstitution;
	}
	
	public void setSponsorInstitution(String sponsorInstitution) {
		this.sponsorInstitution = sponsorInstitution;
	}
	
	public String getOpTime() {
		return opTime;
	}
	
	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}
	
	public String getSingType() {
		return singType;
	}
	
	public void setSingType(String singType) {
		this.singType = singType;
	}
	
	public String gettSponsor() {
		return tSponsor;
	}
	
	public void settSponsor(String tSponsor) {
		this.tSponsor = tSponsor;
	}
	
	public String gettSponInstitution() {
		return tSponInstitution;
	}
	
	public void settSponInstitution(String tSponInstitution) {
		this.tSponInstitution = tSponInstitution;
	}
	
	public String gettOperationTime() {
		return tOperationTime;
	}
	
	public void settOperationTime(String tOperationTime) {
		this.tOperationTime = tOperationTime;
	}
}
