package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSCdtTrfRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSCdtTrfRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="UserDefineTranCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayPathCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PosEntryMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustGtLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusiType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusiKind" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayerAccType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayerAcc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayerAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayerAccBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RealPayerAcc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RealPayerAccType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RealPayerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayeeAccBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayeeAcc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayeeAccType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayeeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayeeAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommissionRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ChargeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChargeRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}ChargeRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Useage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PostScript" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SeperateFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VoucherFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CusVoucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CusVouchNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CusVouchDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UseAttr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSCdtTrfRq", propOrder = { "commonRqHdr",
		"userDefineTranCode", "payPathCode", "posEntryMode", "custGtLevel",
		"busiType", "busiKind", "payerAccType", "payerAcc", "payerName",
		"payerAddr", "payerAccBank", "realPayerAcc", "realPayerAccType",
		"realPayerName", "payeeAccBank", "payeeAcc", "payeeAccType",
		"payeeName", "payeeAddr", "currency", "amount", "priority",
		"commissionNo", "commissionRec", "chargeNo", "chargeRec", "useage",
		"postScript", "remark1", "remark2", "remark3", "remark4", "remark5",
		"remark6", "remark7", "seperateFlg", "voucherFlag", "feeBaseLogId","workDate","srquid",
		"password", "cusVoucherType", "cusVouchNo", "cusVouchDate", "useAttr","payResult","retCode","retMsg" })
public class UPPSCdtTrfRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "UserDefineTranCode", required = true)
	protected String userDefineTranCode;
	@XmlElement(name = "PayPathCode", required = true)
	protected String payPathCode;
	@XmlElement(name = "PosEntryMode", required = true)
	protected String posEntryMode;
	@XmlElement(name = "CustGtLevel", required = true)
	protected String custGtLevel;
	@XmlElement(name = "BusiType", required = true)
	protected String busiType;
	@XmlElement(name = "BusiKind", required = true)
	protected String busiKind;
	@XmlElement(name = "PayerAccType", required = true)
	protected String payerAccType;
	@XmlElement(name = "PayerAcc", required = true)
	protected String payerAcc;
	@XmlElement(name = "PayerName", required = true)
	protected String payerName;
	@XmlElement(name = "PayerAddr", required = true)
	protected String payerAddr;
	@XmlElement(name = "PayerAccBank", required = true)
	protected String payerAccBank;
	@XmlElement(name = "RealPayerAcc", required = true)
	protected String realPayerAcc;
	@XmlElement(name = "RealPayerAccType", required = true)
	protected String realPayerAccType;
	@XmlElement(name = "RealPayerName", required = true)
	protected String realPayerName;
	@XmlElement(name = "PayeeAccBank", required = true)
	protected String payeeAccBank;
	@XmlElement(name = "PayeeAcc", required = true)
	protected String payeeAcc;
	@XmlElement(name = "PayeeAccType", required = true)
	protected String payeeAccType;
	@XmlElement(name = "PayeeName", required = true)
	protected String payeeName;
	@XmlElement(name = "PayeeAddr", required = true)
	protected String payeeAddr;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "Amount", required = true)
	protected String amount;
	@XmlElement(name = "Priority", required = true)
	protected String priority;
	@XmlElement(name = "CommissionNo", required = true)
	protected String commissionNo;
	@XmlElement(name = "CommissionRec")
	protected List<CommissionRec> commissionRec;
	@XmlElement(name = "ChargeNo", required = true)
	protected String chargeNo;
	@XmlElement(name = "ChargeRec")
	protected List<ChargeRec> chargeRec;
	@XmlElement(name = "Useage", required = true)
	protected String useage;
	@XmlElement(name = "PostScript", required = true)
	protected String postScript;
	@XmlElement(name = "Remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "Remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "Remark_3", required = true)
	protected String remark3;
	@XmlElement(name = "Remark_4", required = true)
	protected String remark4;
	@XmlElement(name = "Remark_5", required = true)
	protected String remark5;
	@XmlElement(name = "Remark_6", required = true)
	protected String remark6;
	@XmlElement(name = "Remark_7", required = true)
	protected String remark7;
	@XmlElement(name = "SeperateFlg", required = true)
	protected String seperateFlg;
	@XmlElement(name = "VoucherFlag", required = true)
	protected String voucherFlag;
	@XmlElement(name = "FeeBaseLogId", required = true)
	protected String feeBaseLogId;
	@XmlElement(name = "Password", required = true)
	protected String password;
	@XmlElement(name = "CusVoucherType", required = true)
	protected String cusVoucherType;
	@XmlElement(name = "CusVouchNo", required = true)
	protected String cusVouchNo;
	@XmlElement(name = "CusVouchDate", required = true)
	protected String cusVouchDate;
	@XmlElement(name = "UseAttr", required = true)
	protected String useAttr;

	protected String payResult;
	protected String retCode;
	protected String retMsg;
	protected String workDate;
	protected String srquid;
	
	
	
	
	public String getSrquid() {
		return srquid;
	}

	public void setSrquid(String srquid) {
		this.srquid = srquid;
	}

	public String getWorkDate() {
		return workDate;
	}

	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}

	public String getPayResult() {
		return payResult;
	}

	public void setPayResult(String payResult) {
		this.payResult = payResult;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the userDefineTranCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserDefineTranCode() {
		return userDefineTranCode;
	}

	/**
	 * Sets the value of the userDefineTranCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserDefineTranCode(String value) {
		this.userDefineTranCode = value;
	}

	/**
	 * Gets the value of the payPathCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayPathCode() {
		return payPathCode;
	}

	/**
	 * Sets the value of the payPathCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayPathCode(String value) {
		this.payPathCode = value;
	}

	/**
	 * Gets the value of the posEntryMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPosEntryMode() {
		return posEntryMode;
	}

	/**
	 * Sets the value of the posEntryMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPosEntryMode(String value) {
		this.posEntryMode = value;
	}

	/**
	 * Gets the value of the custGtLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustGtLevel() {
		return custGtLevel;
	}

	/**
	 * Sets the value of the custGtLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustGtLevel(String value) {
		this.custGtLevel = value;
	}

	/**
	 * Gets the value of the busiType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusiType() {
		return busiType;
	}

	/**
	 * Sets the value of the busiType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusiType(String value) {
		this.busiType = value;
	}

	/**
	 * Gets the value of the busiKind property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusiKind() {
		return busiKind;
	}

	/**
	 * Sets the value of the busiKind property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusiKind(String value) {
		this.busiKind = value;
	}

	/**
	 * Gets the value of the payerAccType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayerAccType() {
		return payerAccType;
	}

	/**
	 * Sets the value of the payerAccType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayerAccType(String value) {
		this.payerAccType = value;
	}

	/**
	 * Gets the value of the payerAcc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayerAcc() {
		return payerAcc;
	}

	/**
	 * Sets the value of the payerAcc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayerAcc(String value) {
		this.payerAcc = value;
	}

	/**
	 * Gets the value of the payerName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayerName() {
		return payerName;
	}

	/**
	 * Sets the value of the payerName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayerName(String value) {
		this.payerName = value;
	}

	/**
	 * Gets the value of the payerAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayerAddr() {
		return payerAddr;
	}

	/**
	 * Sets the value of the payerAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayerAddr(String value) {
		this.payerAddr = value;
	}

	/**
	 * Gets the value of the payerAccBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayerAccBank() {
		return payerAccBank;
	}

	/**
	 * Sets the value of the payerAccBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayerAccBank(String value) {
		this.payerAccBank = value;
	}

	/**
	 * Gets the value of the realPayerAcc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRealPayerAcc() {
		return realPayerAcc;
	}

	/**
	 * Sets the value of the realPayerAcc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRealPayerAcc(String value) {
		this.realPayerAcc = value;
	}

	/**
	 * Gets the value of the realPayerAccType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRealPayerAccType() {
		return realPayerAccType;
	}

	/**
	 * Sets the value of the realPayerAccType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRealPayerAccType(String value) {
		this.realPayerAccType = value;
	}

	/**
	 * Gets the value of the realPayerName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRealPayerName() {
		return realPayerName;
	}

	/**
	 * Sets the value of the realPayerName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRealPayerName(String value) {
		this.realPayerName = value;
	}

	/**
	 * Gets the value of the payeeAccBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayeeAccBank() {
		return payeeAccBank;
	}

	/**
	 * Sets the value of the payeeAccBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayeeAccBank(String value) {
		this.payeeAccBank = value;
	}

	/**
	 * Gets the value of the payeeAcc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayeeAcc() {
		return payeeAcc;
	}

	/**
	 * Sets the value of the payeeAcc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayeeAcc(String value) {
		this.payeeAcc = value;
	}

	/**
	 * Gets the value of the payeeAccType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayeeAccType() {
		return payeeAccType;
	}

	/**
	 * Sets the value of the payeeAccType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayeeAccType(String value) {
		this.payeeAccType = value;
	}

	/**
	 * Gets the value of the payeeName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayeeName() {
		return payeeName;
	}

	/**
	 * Sets the value of the payeeName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayeeName(String value) {
		this.payeeName = value;
	}

	/**
	 * Gets the value of the payeeAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayeeAddr() {
		return payeeAddr;
	}

	/**
	 * Sets the value of the payeeAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayeeAddr(String value) {
		this.payeeAddr = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the priority property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Sets the value of the priority property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPriority(String value) {
		this.priority = value;
	}

	/**
	 * Gets the value of the commissionNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommissionNo() {
		return commissionNo;
	}

	/**
	 * Sets the value of the commissionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommissionNo(String value) {
		this.commissionNo = value;
	}

	/**
	 * Gets the value of the commissionRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the commissionRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCommissionRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CommissionRec }
	 * 
	 * 
	 */
	public List<CommissionRec> getCommissionRec() {
		if (commissionRec == null) {
			commissionRec = new ArrayList<CommissionRec>();
		}
		return this.commissionRec;
	}

	/**
	 * Gets the value of the chargeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeNo() {
		return chargeNo;
	}

	/**
	 * Sets the value of the chargeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeNo(String value) {
		this.chargeNo = value;
	}

	/**
	 * Gets the value of the chargeRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the chargeRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getChargeRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ChargeRec }
	 * 
	 * 
	 */
	public List<ChargeRec> getChargeRec() {
		if (chargeRec == null) {
			chargeRec = new ArrayList<ChargeRec>();
		}
		return this.chargeRec;
	}

	/**
	 * Gets the value of the useage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUseage() {
		return useage;
	}

	/**
	 * Sets the value of the useage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUseage(String value) {
		this.useage = value;
	}

	/**
	 * Gets the value of the postScript property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostScript() {
		return postScript;
	}

	/**
	 * Sets the value of the postScript property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostScript(String value) {
		this.postScript = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the remark3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark3() {
		return remark3;
	}

	/**
	 * Sets the value of the remark3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark3(String value) {
		this.remark3 = value;
	}

	/**
	 * Gets the value of the remark4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark4() {
		return remark4;
	}

	/**
	 * Sets the value of the remark4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark4(String value) {
		this.remark4 = value;
	}

	/**
	 * Gets the value of the remark5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark5() {
		return remark5;
	}

	/**
	 * Sets the value of the remark5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark5(String value) {
		this.remark5 = value;
	}

	/**
	 * Gets the value of the remark6 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark6() {
		return remark6;
	}

	/**
	 * Sets the value of the remark6 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark6(String value) {
		this.remark6 = value;
	}

	/**
	 * Gets the value of the remark7 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark7() {
		return remark7;
	}

	/**
	 * Sets the value of the remark7 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark7(String value) {
		this.remark7 = value;
	}

	/**
	 * Gets the value of the seperateFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSeperateFlg() {
		return seperateFlg;
	}

	/**
	 * Sets the value of the seperateFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSeperateFlg(String value) {
		this.seperateFlg = value;
	}

	/**
	 * Gets the value of the voucherFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVoucherFlag() {
		return voucherFlag;
	}

	/**
	 * Sets the value of the voucherFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVoucherFlag(String value) {
		this.voucherFlag = value;
	}

	/**
	 * Gets the value of the feeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeBaseLogId() {
		return feeBaseLogId;
	}

	/**
	 * Sets the value of the feeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeBaseLogId(String value) {
		this.feeBaseLogId = value;
	}

	/**
	 * Gets the value of the password property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the value of the password property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPassword(String value) {
		this.password = value;
	}

	/**
	 * Gets the value of the cusVoucherType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCusVoucherType() {
		return cusVoucherType;
	}

	/**
	 * Sets the value of the cusVoucherType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCusVoucherType(String value) {
		this.cusVoucherType = value;
	}

	/**
	 * Gets the value of the cusVouchNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCusVouchNo() {
		return cusVouchNo;
	}

	/**
	 * Sets the value of the cusVouchNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCusVouchNo(String value) {
		this.cusVouchNo = value;
	}

	/**
	 * Gets the value of the cusVouchDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCusVouchDate() {
		return cusVouchDate;
	}

	/**
	 * Sets the value of the cusVouchDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCusVouchDate(String value) {
		this.cusVouchDate = value;
	}

	/**
	 * Gets the value of the useAttr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUseAttr() {
		return useAttr;
	}

	/**
	 * Sets the value of the useAttr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUseAttr(String value) {
		this.useAttr = value;
	}

}
