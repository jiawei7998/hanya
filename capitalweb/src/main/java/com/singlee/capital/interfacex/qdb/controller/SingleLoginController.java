package com.singlee.capital.interfacex.qdb.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.session.LoginListener;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.mapper.TaBrccMapper;
import com.singlee.capital.system.mapper.TaRoleMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaBrcc;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.RoleService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
//import com.siro.efs2.server.TokenHttpClient;

@Controller
@RequestMapping("/singleLogin")
public class SingleLoginController{
	@Resource
	private UserService userService;
	@Resource
	private InstitutionService is;
	@Autowired
	private LoginListener loginListener;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private TaBrccMapper brccMapper;

	
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public final static Logger log = LoggerFactory.getLogger("com");
	
	public static String ips=null;
	public static int port;
	public static String txtPath=null;
	public static String sysno=null;
	
	static{
		try {
			ips = PropertiesUtil.getProperties("singleLogin.ip");
			port = Integer.valueOf(PropertiesUtil.getProperties("singleLogin.port"));
			txtPath = PropertiesUtil.getProperties("singleLogin.txtPath");
			sysno = PropertiesUtil.getProperties("singleLogin.sysno");	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("checkLogin")
	public void checkLogin(HttpServletRequest request,HttpServletResponse response) throws IOException{
		log.error("开始单点登录验证");
		try{
			String ip  =request.getHeader("X-Forwarded-For");
			ip  =request.getHeader("X-Real-IP");
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("Proxy-Client-IP");
			}else{
				String[] ipArr = ip.split(",");
				if(ipArr.length > 1){
					ip = ipArr[0];
				}
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getRemoteAddr();
			}
			String token = request.getParameter("token");  
			String tellerno = request.getParameter("tellerno"); 
			try {
				log.info("开始单点登进入TokenHttpClient client = new TokenHttpClient(); ");
//				TokenHttpClient client = new TokenHttpClient();  
				String url = "http://"+ips+":"+port+txtPath+"?tellerno="+tellerno+"&sysno="+sysno+"&token="+token;
//	 			boolean tokenCheck = client.tokenCheck(url);
				boolean tokenCheck =false;
				log.info("url:" + url); 			 
				log.info("*****tokenCheck:" + tokenCheck);
				if(tokenCheck) {    
					try {    
						log.info("*****TaUser user = userService.getUserById(tellerno.trim());");
						TaUser user = userService.getUserById(tellerno.trim());
						//TaUser user = userService.getUserById("QD13001099");
						
						if(!"1".equals(user.getIsActive())){
							log.info("*****用户未启用******");	
							response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+"standard/login.jsp");
							return;
							
						}
						log.info("*****TaUser user = String branchId ="+ user.getUserId());
						// branchId和用户,用户放置在session中
						String branchId ="QCCBCNBQ";
						log.info("*****TaUser user Map<String, Object> map = null; =");
						//查询机构号
						Map<String, Object> map=new HashMap<String, Object>();
						map.put("passwd", "");
						map.put("branchId", branchId);
						map.put("instId", "");
						
						map.put("userId", user.getUserId());
						map.put("roleId", "");
						log.info("*****TaUser user Map<String, Object> "+ map.get("userId"));
						
						
						log.info("*****TaUser user Map<String, Object> "+ map.get("branchId"));
						//根据用户id和branchId查询机构号
						log.info("*****开始获取机构号");	
						List<TtInstitution> ttInstitutions = institutionService.getInstByUser(map);
						//取第一个
						log.info("*****开始获取第一个机构号");
						TtInstitution ttInstitution=ttInstitutions.get(0);
						log.info("*****第一个机构号获取成功");
						String instId=ttInstitution.getInstId();
						log.info("*****第一个机构号中的机构获取成功"+instId);
						//把机构号插入map中
						map.put("instId", instId);
						log.info("*****机构插入map成功");
						//根据用户id和机构号查询用户角色
						log.info("*****开始获取角色号");
						List<TaRole> taRoles = roleService.getRoleByUserIdAndInstId(map);
						//取第一个
						log.info("*****开始获取第一个角色号");
						TaRole taRole=taRoles.get(0);
						log.info("*****开始获取第一个角色号成功");
						String roleId=taRole.getRoleId();
					   // 进行数据更新，并获取需要存放到 slsession 中的数据 TODO
						log.info("*****开始保存数据");
						TtInstitution in = is.getInstitutionById(instId);

						log.info("*****数据保存成功");
						user.setInstName(in.getInstFullname());
						user.setInstId(in.getInstId());// 当前用户登入机构
						user.setRoles(roleId);// 当前用户登入角色
						user.setBranchId(branchId);// 当前用户输入那个行的
						// 查询系统配置信息表
						log.info("*****开始查询系统配置信息表");
						TaBrcc brcc = brccMapper.selectByBranchId(branchId);

						user.setLatestIp(ip);	
						SimpleDateFormat dtfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						user.setLatestLoginTime(dtfm.format(new Date()));
					
						
						SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
						// 是否匿名
						SlSessionHelper.setAnonymity(session,false);
						// 用户信息
						SlSessionHelper.setUser(session, user);
						// 机构信息
						SlSessionHelper.setInstitution(session, in);
						// BRCC信息
						SlSessionHelper.setBrcc(session, brcc);

						SlSessionHelper.setUser(session, user);
						
						in = is.getInstitutionById(user.getInstId());
						if(in != null){
							SlSessionHelper.setInstitution(session, in);
						}
						SessionStaticUtil.holdSlSessionThreadLocal(session);
						log.info("*****loginListener.loginOk(session);");
						loginListener.loginOk(session);
						
						SessionStaticUtil.setSessionByHttp(request, 
								SessionStaticUtil.getSlSessionThreadLocal());						
						log.info(user.getUserName()+"::单点登录验证成功::"+tellerno);
						//return "index";
						response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+"standard/index.jsp");
						return;
			 		} catch (Exception e) {
			 			log.error("*****单点登录验证失败(验证为false)");	
			 			response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+"standard/login.jsp");
						return;
					} 
				} else {    
					log.error("*****单点登录验证失败(验证为false)");	
					response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+"standard/login.jsp");
					return;
				}
			} catch (Exception e) {  
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
		}	
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+"standard/login.jsp");
	}
	public LoginListener getLoginListener() {
		return loginListener;
	}
	public void setLoginListener(LoginListener loginListener) {
		this.loginListener = loginListener;
	}
	@RequestMapping("index")
	public String index(HttpServletRequest request,HttpServletResponse response){
		System.out.println("..........");
		return "standard/index";
	}
}
