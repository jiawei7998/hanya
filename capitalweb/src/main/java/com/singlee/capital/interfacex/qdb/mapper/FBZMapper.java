package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.interfacex.qdb.pojo.FBZ003;

public interface FBZMapper {
	public int insertFbz(FBZ003 fbz);
	
	public int updateFbz(FBZ003 fbz);
	
	public FBZ003 getFbzByOrder(String orderId);

	public void deleteFbz(FBZ003 f);

	public void saveLog(FBZ003 fbz);

	public List<FBZ003> getFbzBySeriaNo(Map<String, Object> map);

	public void updateFlag(FBZ003 f);
	
	public int countBaseAsset(Map<String, Object> map);
	
	public int countPassWay(Map<String, Object> map);
}
