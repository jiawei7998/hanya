package com.singlee.capital.interfacex.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.BussinessDueBill;

public interface TiBussinessDueBillMapper extends Mapper<BussinessDueBill>{

}
