package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.users.mapper.TdCustAccMapper;
import com.singlee.capital.users.model.TdCustAcc;
@Service("inComePrintServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
              
public class InComePrintServiceImpl implements AssemblyDataService{
	
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	
	@Autowired
	private TdCustAccMapper TcaMapper;
	
	@Override
	public Map<String, Object> getData(String id) {
		Map<String, Object> params =new  HashMap<String, Object>();
		params.put("dealNo", id);
		TdProductApproveMain entry=productApproveMainMapper.getProductApproveMainActivated(id);
		if(entry!=null){
			Map<String,Object> entrymap=ParentChildUtil.ClassToHashMap(entry);
			if(entry!=null){
				String cNo= entry.getcNo();
				TdCustAcc custAcc=TcaMapper.selectByPrimaryKey(cNo);
				if(custAcc!=null){
					entrymap.put("bankaccname",custAcc.getBankaccname());	
					entrymap.put("bankaccid",custAcc.getBankaccid());	
					entrymap.put("openBankLargeAccno",custAcc.getOpenBankLargeAccno());	
				}
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd");
				entrymap.put("sysdate",sdf.format(new Date()) );	
			
				
			}
			entrymap.put("amt",entry.getAmt() );
			entrymap.put("productName",entry.getProductName());
			return entrymap;
		}else{
			Map<String, Object> param =new  HashMap<String, Object>();
			param.put("", "");
			return param;
		}
		
	}

}
