package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.trade.mapper.TdInterestSettleMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdInterestSettle;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service(value="durationInterestSettleConfirImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintInterestSettleConfirImpl  implements AssemblyDataService{

	@Autowired
	private FlowController flowController; 

	@Autowired
	private TtInstitutionMapper mapper;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private TdInterestSettleMapper tdInterestSettleMapper;
	@Override
	public Map<String, Object> getData(String id) {
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			NumberFormat nf=NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			if(id != null && !"".equals(id)){
				TdInterestSettle tdInt=tdInterestSettleMapper.getInterestSettleById(id);
				if(tdInt != null){
					String refNo=tdInt.getRefNo();
					map.put("dealNo", refNo); //原交易单号
					
					TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
					map.put("deal_No", td.getDealNo());
					map.put("contractRate", df.format(100*td.getContractRate()));//投资利率
					map.put("vDate", td.getvDate());//起息日期
					map.put("mDate", td.getmDate());//到期日期
					map.put("deal_date", tdInt.getDealDate()); //交易日期
					String  settle_flag=tdInt.getSettleFlag(); //是否结清
					if("1".endsWith(settle_flag)){
						map.put("settleFlag", "是");
					}
					if("0".endsWith(settle_flag)){
						map.put("settleFlag", "否");
					}
					map.put("interestSettleAmt", tdInt.getInterestSettlAmt());//当前结息金额
					map.put("settlInt", tdInt.getSettlInt());//实际结息金额
					map.put("remark", tdInt.getAmReason()); //结息备注
					
					String cno=td.getcNo();
					CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
					map.put("party_name", cp.getParty_name()); //交易对手名称
					Integer pro=td.getPrdNo();
					if(pro!=null&&!"".equals(pro)){
						TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
						map.put("productName", p.getPrdName()); // 产品名称
					}
					
					//取原交易的经办人
					String Sponsor2=td.getSponsor();
					TaUser u=taUserMapper.selectUser(Sponsor2);
					map.put("sponsor",u.getUserName());
					
					
					//取存续期的经办人
					String Sponsor=tdInt.getSponsor();
					TaUser u2=taUserMapper.selectUser(Sponsor);
					map.put("sponsor2",u2.getUserName());
					String inst=u.getInstId();
					
					TtInstitution institution = new TtInstitution();
					if(inst!=null&&!"".equals(inst)){
						institution.setInstId(inst);
						institution = mapper.selectByPrimaryKey(institution);
						map.put("Inst",institution.getInstName());  //合同发起机构
					}
					map.put("amt", nf.format(td.getAmt()));  //合同本金
					map.put("basis", td.getBasis());
					map.put("mInt", nf.format(td.getmInt()));
					map.put("mAmt", nf.format(td.getmAmt()));
					map.put("SysDate", dayendDateService.getSettlementDate());
				}
			}
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}

				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
