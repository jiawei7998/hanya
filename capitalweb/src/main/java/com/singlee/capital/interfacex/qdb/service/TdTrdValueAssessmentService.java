package com.singlee.capital.interfacex.qdb.service;

import java.io.InputStream;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TdTrdValueAssessment;

public interface TdTrdValueAssessmentService {

	
	public String setTdTrdValueAssessmentByExcel(String type, InputStream is);
	
	
	Page<TdTrdValueAssessment> pageList(Map<String,Object> map);
	

}
