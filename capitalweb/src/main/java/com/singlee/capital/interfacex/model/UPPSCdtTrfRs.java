package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSCdtTrfRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSCdtTrfRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="WorkDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgentSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayPathCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccbrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SendBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntrustDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommissionRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ChargeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChargeRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}ChargeRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CenterDealCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CenterDealMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoreResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSCdtTrfRs", propOrder = { "commonRsHdr", "workDate",
		"agentSerialNo", "payPathCode", "accbrNo", "bankDate", "bankId",
		"sendBank", "entrustDate", "msgId", "commissionNo", "commissionRec",
		"chargeNo", "chargeRec", "centerDealCode", "centerDealMsg",
		"payResult", "coreResult", "remark1", "remark2", "remark3", "remark4",
		"remark5", "remark6", "remark7" })
public class UPPSCdtTrfRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "WorkDate", required = true)
	protected String workDate;
	@XmlElement(name = "AgentSerialNo", required = true)
	protected String agentSerialNo;
	@XmlElement(name = "PayPathCode", required = true)
	protected String payPathCode;
	@XmlElement(name = "AccbrNo", required = true)
	protected String accbrNo;
	@XmlElement(name = "BankDate", required = true)
	protected String bankDate;
	@XmlElement(name = "BankId", required = true)
	protected String bankId;
	@XmlElement(name = "SendBank", required = true)
	protected String sendBank;
	@XmlElement(name = "EntrustDate", required = true)
	protected String entrustDate;
	@XmlElement(name = "MsgId", required = true)
	protected String msgId;
	@XmlElement(name = "CommissionNo", required = true)
	protected String commissionNo;
	@XmlElement(name = "CommissionRec")
	protected List<CommissionRec> commissionRec;
	@XmlElement(name = "ChargeNo", required = true)
	protected String chargeNo;
	@XmlElement(name = "ChargeRec")
	protected List<ChargeRec> chargeRec;
	@XmlElement(name = "CenterDealCode", required = true)
	protected String centerDealCode;
	@XmlElement(name = "CenterDealMsg", required = true)
	protected String centerDealMsg;
	@XmlElement(name = "PayResult", required = true)
	protected String payResult;
	@XmlElement(name = "CoreResult", required = true)
	protected String coreResult;
	@XmlElement(name = "Remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "Remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "Remark_3", required = true)
	protected String remark3;
	@XmlElement(name = "Remark_4", required = true)
	protected String remark4;
	@XmlElement(name = "Remark_5", required = true)
	protected String remark5;
	@XmlElement(name = "Remark_6", required = true)
	protected String remark6;
	@XmlElement(name = "Remark_7", required = true)
	protected String remark7;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the workDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkDate() {
		return workDate;
	}

	/**
	 * Sets the value of the workDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWorkDate(String value) {
		this.workDate = value;
	}

	/**
	 * Gets the value of the agentSerialNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgentSerialNo() {
		return agentSerialNo;
	}

	/**
	 * Sets the value of the agentSerialNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgentSerialNo(String value) {
		this.agentSerialNo = value;
	}

	/**
	 * Gets the value of the payPathCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayPathCode() {
		return payPathCode;
	}

	/**
	 * Sets the value of the payPathCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayPathCode(String value) {
		this.payPathCode = value;
	}

	/**
	 * Gets the value of the accbrNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccbrNo() {
		return accbrNo;
	}

	/**
	 * Sets the value of the accbrNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccbrNo(String value) {
		this.accbrNo = value;
	}

	/**
	 * Gets the value of the bankDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankDate() {
		return bankDate;
	}

	/**
	 * Sets the value of the bankDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankDate(String value) {
		this.bankDate = value;
	}

	/**
	 * Gets the value of the bankId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankId() {
		return bankId;
	}

	/**
	 * Sets the value of the bankId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankId(String value) {
		this.bankId = value;
	}

	/**
	 * Gets the value of the sendBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSendBank() {
		return sendBank;
	}

	/**
	 * Sets the value of the sendBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSendBank(String value) {
		this.sendBank = value;
	}

	/**
	 * Gets the value of the entrustDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntrustDate() {
		return entrustDate;
	}

	/**
	 * Sets the value of the entrustDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntrustDate(String value) {
		this.entrustDate = value;
	}

	/**
	 * Gets the value of the msgId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * Sets the value of the msgId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgId(String value) {
		this.msgId = value;
	}

	/**
	 * Gets the value of the commissionNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommissionNo() {
		return commissionNo;
	}

	/**
	 * Sets the value of the commissionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommissionNo(String value) {
		this.commissionNo = value;
	}

	/**
	 * Gets the value of the commissionRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the commissionRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCommissionRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CommissionRec }
	 * 
	 * 
	 */
	public List<CommissionRec> getCommissionRec() {
		if (commissionRec == null) {
			commissionRec = new ArrayList<CommissionRec>();
		}
		return this.commissionRec;
	}

	/**
	 * Gets the value of the chargeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeNo() {
		return chargeNo;
	}

	/**
	 * Sets the value of the chargeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeNo(String value) {
		this.chargeNo = value;
	}

	/**
	 * Gets the value of the chargeRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the chargeRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getChargeRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ChargeRec }
	 * 
	 * 
	 */
	public List<ChargeRec> getChargeRec() {
		if (chargeRec == null) {
			chargeRec = new ArrayList<ChargeRec>();
		}
		return this.chargeRec;
	}

	/**
	 * Gets the value of the centerDealCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCenterDealCode() {
		return centerDealCode;
	}

	/**
	 * Sets the value of the centerDealCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCenterDealCode(String value) {
		this.centerDealCode = value;
	}

	/**
	 * Gets the value of the centerDealMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCenterDealMsg() {
		return centerDealMsg;
	}

	/**
	 * Sets the value of the centerDealMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCenterDealMsg(String value) {
		this.centerDealMsg = value;
	}

	/**
	 * Gets the value of the payResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayResult() {
		return payResult;
	}

	/**
	 * Sets the value of the payResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayResult(String value) {
		this.payResult = value;
	}

	/**
	 * Gets the value of the coreResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoreResult() {
		return coreResult;
	}

	/**
	 * Sets the value of the coreResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoreResult(String value) {
		this.coreResult = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the remark3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark3() {
		return remark3;
	}

	/**
	 * Sets the value of the remark3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark3(String value) {
		this.remark3 = value;
	}

	/**
	 * Gets the value of the remark4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark4() {
		return remark4;
	}

	/**
	 * Sets the value of the remark4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark4(String value) {
		this.remark4 = value;
	}

	/**
	 * Gets the value of the remark5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark5() {
		return remark5;
	}

	/**
	 * Sets the value of the remark5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark5(String value) {
		this.remark5 = value;
	}

	/**
	 * Gets the value of the remark6 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark6() {
		return remark6;
	}

	/**
	 * Sets the value of the remark6 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark6(String value) {
		this.remark6 = value;
	}

	/**
	 * Gets the value of the remark7 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark7() {
		return remark7;
	}

	/**
	 * Sets the value of the remark7 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark7(String value) {
		this.remark7 = value;
	}

}
