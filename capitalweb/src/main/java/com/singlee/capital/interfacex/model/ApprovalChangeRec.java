package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApprovalChangeRec", propOrder = { "partyName",
		"clientNo", "orgNum", "userNum", "approveId", "oriBatchNo",
		"productType", "accountProperty", "loanAmt", "loanCreditAmt", "lyed",
		"loanApplyed", "currency", "tenor", "extenTermUnit", "sameCustName",
		"validDate", "rateType", "rateReceiveType", "rate", "finalApprvResult",
		"siteNo", "limitType", "loanAssuKind" })
public class ApprovalChangeRec {
	@XmlElement(name = "PartyName", required = true)
	protected String partyName;
	@XmlElement(name = "ClientNo", required = true)
	protected String clientNo;
	@XmlElement(name = "OrgNum", required = true)
	protected String orgNum;
	@XmlElement(name = "UserNum", required = true)
	protected String userNum;
	@XmlElement(name = "ApproveId", required = true)
	protected String approveId;
	@XmlElement(name = "OriBatchNo", required = true)
	protected String oriBatchNo;
	@XmlElement(name = "ProductType", required = true)
	protected String productType;
	@XmlElement(name = "AccountProperty", required = true)
	protected String accountProperty;
	@XmlElement(name = "LoanAmt", required = true)
	protected String loanAmt;
	@XmlElement(name = "LoanCreditAmt", required = true)
	protected String loanCreditAmt;
	@XmlElement(name = "Lyed", required = true)
	protected String lyed;
	@XmlElement(name = "LoanApplyed", required = true)
	protected String loanApplyed;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "Tenor", required = true)
	protected String tenor;
	@XmlElement(name = "ExtenTermUnit", required = true)
	protected String extenTermUnit;
	@XmlElement(name = "SameCustName", required = true)
	protected String sameCustName;
	@XmlElement(name = "ValidDate", required = true)
	protected String validDate;
	@XmlElement(name = "RateType", required = true)
	protected String rateType;
	@XmlElement(name = "RateReceiveType", required = true)
	protected String rateReceiveType;
	@XmlElement(name = "Rate", required = true)
	protected String rate;
	@XmlElement(name = "FinalApprvResult", required = true)
	protected String finalApprvResult;
	@XmlElement(name = "SiteNo", required = true)
	protected String siteNo;
	@XmlElement(name = "LimitType", required = true)
	protected String limitType;
	@XmlElement(name = "LoanAssuKind", required = true)
	protected String loanAssuKind;
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getClientNo() {
		return clientNo;
	}
	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}
	public String getOrgNum() {
		return orgNum;
	}
	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}
	public String getUserNum() {
		return userNum;
	}
	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}
	public String getApproveId() {
		return approveId;
	}
	public void setApproveId(String approveId) {
		this.approveId = approveId;
	}
	public String getOriBatchNo() {
		return oriBatchNo;
	}
	public void setOriBatchNo(String oriBatchNo) {
		this.oriBatchNo = oriBatchNo;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getAccountProperty() {
		return accountProperty;
	}
	public void setAccountProperty(String accountProperty) {
		this.accountProperty = accountProperty;
	}
	public String getLoanAmt() {
		return loanAmt;
	}
	public void setLoanAmt(String loanAmt) {
		this.loanAmt = loanAmt;
	}
	public String getLoanCreditAmt() {
		return loanCreditAmt;
	}
	public void setLoanCreditAmt(String loanCreditAmt) {
		this.loanCreditAmt = loanCreditAmt;
	}
	public String getLyed() {
		return lyed;
	}
	public void setLyed(String lyed) {
		this.lyed = lyed;
	}
	public String getLoanApplyed() {
		return loanApplyed;
	}
	public void setLoanApplyed(String loanApplyed) {
		this.loanApplyed = loanApplyed;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getExtenTermUnit() {
		return extenTermUnit;
	}
	public void setExtenTermUnit(String extenTermUnit) {
		this.extenTermUnit = extenTermUnit;
	}
	public String getSameCustName() {
		return sameCustName;
	}
	public void setSameCustName(String sameCustName) {
		this.sameCustName = sameCustName;
	}
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getRateReceiveType() {
		return rateReceiveType;
	}
	public void setRateReceiveType(String rateReceiveType) {
		this.rateReceiveType = rateReceiveType;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getFinalApprvResult() {
		return finalApprvResult;
	}
	public void setFinalApprvResult(String finalApprvResult) {
		this.finalApprvResult = finalApprvResult;
	}
	public String getSiteNo() {
		return siteNo;
	}
	public void setSiteNo(String siteNo) {
		this.siteNo = siteNo;
	}
	public String getLimitType() {
		return limitType;
	}
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	public String getLoanAssuKind() {
		return loanAssuKind;
	}
	public void setLoanAssuKind(String loanAssuKind) {
		this.loanAssuKind = loanAssuKind;
	}
	
	

}
