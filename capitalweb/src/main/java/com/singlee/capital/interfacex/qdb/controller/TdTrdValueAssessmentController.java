package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.TdTrdValueAssessment;
import com.singlee.capital.interfacex.qdb.service.TdTrdValueAssessmentService;
import com.singlee.capital.trade.acc.service.AccForTradeAccessService;

@Controller
@RequestMapping(value="TdTrdValueAssessmentController")
public class TdTrdValueAssessmentController {
	
	@Autowired
	private TdTrdValueAssessmentService  service;
	
	@Autowired
	private AccForTradeAccessService accessService;
	
	@ResponseBody
	@RequestMapping(value="pageList")
	public RetMsg<PageInfo<TdTrdValueAssessment>> pageStock(@RequestBody Map<String, Object> map){
		Page<TdTrdValueAssessment> list = service.pageList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 手动上传估值数据列表
	*/
	@ResponseBody
	@RequestMapping(value = "/uploadExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadTdTrdValueAssessmentExcel(MultipartHttpServletRequest request,HttpServletResponse response){
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String ret = "导入失败";
		try {
			uploadedFileList = FileManage.requestExtractor(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 2.交验并处理成excel
		for (UploadedFile uploadedFile : uploadedFileList) {
			String name = uploadedFile.getFullname();
			InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
			ret = service.setTdTrdValueAssessmentByExcel(name, is);
		}
		return ret;
	}
	
	@ResponseBody
	@RequestMapping(value="createVal")
	public RetMsg<Serializable> createVal(@RequestBody Map<String, Object> map){
		accessService.getNeedForMtmDatasStepOne(String.valueOf(map.get("dealNo")));
		return RetMsgHelper.ok();
	}

}
