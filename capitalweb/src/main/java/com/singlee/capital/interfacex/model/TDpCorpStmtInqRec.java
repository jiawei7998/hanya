package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpCorpStmtInqRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpCorpStmtInqRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Mode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotDrCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotDrAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotCrCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotCrAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Stmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnUuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnBsnId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BookingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnDrcr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HisBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Desc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VouType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VouNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OppAc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OppacName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OppbrName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransCardNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Marker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VirtualAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnFbDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FBID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnShowDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TxnOppno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OperUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MediumNflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpCorpStmtInqRec", propOrder = { "mode", "fileName",
		"totCnt", "totDrCnt", "totDrAmt", "totCrCnt", "totCrAmt", "stmt",
		"txnRef", "txnUuid", "txnBsnId", "bookingDate", "txnDate", "txnTime",
		"txnDrcr", "txnAmt", "hisBal", "desc", "vouType", "vouNo", "oppAc",
		"oppacName", "oppbrName", "remark", "usage", "compName", "transCardNo",
		"status", "marker", "virtualAcct", "txnFbDesc", "memo", "fbid",
		"txnShowDate", "txnOppno", "operUser", "mediumNflg" })
public class TDpCorpStmtInqRec {

	@XmlElement(name = "Mode", required = true)
	protected String mode;
	@XmlElement(name = "FileName", required = true)
	protected String fileName;
	@XmlElement(name = "TotCnt", required = true)
	protected String totCnt;
	@XmlElement(name = "TotDrCnt", required = true)
	protected String totDrCnt;
	@XmlElement(name = "TotDrAmt", required = true)
	protected String totDrAmt;
	@XmlElement(name = "TotCrCnt", required = true)
	protected String totCrCnt;
	@XmlElement(name = "TotCrAmt", required = true)
	protected String totCrAmt;
	@XmlElement(name = "Stmt", required = true)
	protected String stmt;
	@XmlElement(name = "TxnRef", required = true)
	protected String txnRef;
	@XmlElement(name = "TxnUuid", required = true)
	protected String txnUuid;
	@XmlElement(name = "TxnBsnId", required = true)
	protected String txnBsnId;
	@XmlElement(name = "BookingDate", required = true)
	protected String bookingDate;
	@XmlElement(name = "TxnDate", required = true)
	protected String txnDate;
	@XmlElement(name = "TxnTime", required = true)
	protected String txnTime;
	@XmlElement(name = "TxnDrcr", required = true)
	protected String txnDrcr;
	@XmlElement(name = "TxnAmt", required = true)
	protected String txnAmt;
	@XmlElement(name = "HisBal", required = true)
	protected String hisBal;
	@XmlElement(name = "Desc", required = true)
	protected String desc;
	@XmlElement(name = "VouType", required = true)
	protected String vouType;
	@XmlElement(name = "VouNo", required = true)
	protected String vouNo;
	@XmlElement(name = "OppAc", required = true)
	protected String oppAc;
	@XmlElement(name = "OppacName", required = true)
	protected String oppacName;
	@XmlElement(name = "OppbrName", required = true)
	protected String oppbrName;
	@XmlElement(name = "Remark", required = true)
	protected String remark;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "CompName", required = true)
	protected String compName;
	@XmlElement(name = "TransCardNo", required = true)
	protected String transCardNo;
	@XmlElement(name = "Status", required = true)
	protected String status;
	@XmlElement(name = "Marker", required = true)
	protected String marker;
	@XmlElement(name = "VirtualAcct", required = true)
	protected String virtualAcct;
	@XmlElement(name = "TxnFbDesc", required = true)
	protected String txnFbDesc;
	@XmlElement(name = "Memo", required = true)
	protected String memo;
	@XmlElement(name = "FBID", required = true)
	protected String fbid;
	@XmlElement(name = "TxnShowDate", required = true)
	protected String txnShowDate;
	@XmlElement(name = "TxnOppno", required = true)
	protected String txnOppno;
	@XmlElement(name = "OperUser", required = true)
	protected String operUser;
	@XmlElement(name = "MediumNflg", required = true)
	protected String mediumNflg;

	/**
	 * Gets the value of the mode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Sets the value of the mode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMode(String value) {
		this.mode = value;
	}

	/**
	 * Gets the value of the fileName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the value of the fileName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFileName(String value) {
		this.fileName = value;
	}

	/**
	 * Gets the value of the totCnt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotCnt() {
		return totCnt;
	}

	/**
	 * Sets the value of the totCnt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotCnt(String value) {
		this.totCnt = value;
	}

	/**
	 * Gets the value of the totDrCnt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotDrCnt() {
		return totDrCnt;
	}

	/**
	 * Sets the value of the totDrCnt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotDrCnt(String value) {
		this.totDrCnt = value;
	}

	/**
	 * Gets the value of the totDrAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotDrAmt() {
		return totDrAmt;
	}

	/**
	 * Sets the value of the totDrAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotDrAmt(String value) {
		this.totDrAmt = value;
	}

	/**
	 * Gets the value of the totCrCnt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotCrCnt() {
		return totCrCnt;
	}

	/**
	 * Sets the value of the totCrCnt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotCrCnt(String value) {
		this.totCrCnt = value;
	}

	/**
	 * Gets the value of the totCrAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotCrAmt() {
		return totCrAmt;
	}

	/**
	 * Sets the value of the totCrAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotCrAmt(String value) {
		this.totCrAmt = value;
	}

	/**
	 * Gets the value of the stmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStmt() {
		return stmt;
	}

	/**
	 * Sets the value of the stmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStmt(String value) {
		this.stmt = value;
	}

	/**
	 * Gets the value of the txnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnRef() {
		return txnRef;
	}

	/**
	 * Sets the value of the txnRef property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnRef(String value) {
		this.txnRef = value;
	}

	/**
	 * Gets the value of the txnUuid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnUuid() {
		return txnUuid;
	}

	/**
	 * Sets the value of the txnUuid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnUuid(String value) {
		this.txnUuid = value;
	}

	/**
	 * Gets the value of the txnBsnId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnBsnId() {
		return txnBsnId;
	}

	/**
	 * Sets the value of the txnBsnId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnBsnId(String value) {
		this.txnBsnId = value;
	}

	/**
	 * Gets the value of the bookingDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBookingDate() {
		return bookingDate;
	}

	/**
	 * Sets the value of the bookingDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBookingDate(String value) {
		this.bookingDate = value;
	}

	/**
	 * Gets the value of the txnDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnDate() {
		return txnDate;
	}

	/**
	 * Sets the value of the txnDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnDate(String value) {
		this.txnDate = value;
	}

	/**
	 * Gets the value of the txnTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnTime() {
		return txnTime;
	}

	/**
	 * Sets the value of the txnTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnTime(String value) {
		this.txnTime = value;
	}

	/**
	 * Gets the value of the txnDrcr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnDrcr() {
		return txnDrcr;
	}

	/**
	 * Sets the value of the txnDrcr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnDrcr(String value) {
		this.txnDrcr = value;
	}

	/**
	 * Gets the value of the txnAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnAmt() {
		return txnAmt;
	}

	/**
	 * Sets the value of the txnAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnAmt(String value) {
		this.txnAmt = value;
	}

	/**
	 * Gets the value of the hisBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHisBal() {
		return hisBal;
	}

	/**
	 * Sets the value of the hisBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setHisBal(String value) {
		this.hisBal = value;
	}

	/**
	 * Gets the value of the desc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Sets the value of the desc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDesc(String value) {
		this.desc = value;
	}

	/**
	 * Gets the value of the vouType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVouType() {
		return vouType;
	}

	/**
	 * Sets the value of the vouType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVouType(String value) {
		this.vouType = value;
	}

	/**
	 * Gets the value of the vouNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVouNo() {
		return vouNo;
	}

	/**
	 * Sets the value of the vouNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVouNo(String value) {
		this.vouNo = value;
	}

	/**
	 * Gets the value of the oppAc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOppAc() {
		return oppAc;
	}

	/**
	 * Sets the value of the oppAc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOppAc(String value) {
		this.oppAc = value;
	}

	/**
	 * Gets the value of the oppacName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOppacName() {
		return oppacName;
	}

	/**
	 * Sets the value of the oppacName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOppacName(String value) {
		this.oppacName = value;
	}

	/**
	 * Gets the value of the oppbrName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOppbrName() {
		return oppbrName;
	}

	/**
	 * Sets the value of the oppbrName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOppbrName(String value) {
		this.oppbrName = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the compName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompName() {
		return compName;
	}

	/**
	 * Sets the value of the compName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompName(String value) {
		this.compName = value;
	}

	/**
	 * Gets the value of the transCardNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransCardNo() {
		return transCardNo;
	}

	/**
	 * Sets the value of the transCardNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransCardNo(String value) {
		this.transCardNo = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the marker property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMarker() {
		return marker;
	}

	/**
	 * Sets the value of the marker property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMarker(String value) {
		this.marker = value;
	}

	/**
	 * Gets the value of the virtualAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVirtualAcct() {
		return virtualAcct;
	}

	/**
	 * Sets the value of the virtualAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVirtualAcct(String value) {
		this.virtualAcct = value;
	}

	/**
	 * Gets the value of the txnFbDesc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnFbDesc() {
		return txnFbDesc;
	}

	/**
	 * Sets the value of the txnFbDesc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnFbDesc(String value) {
		this.txnFbDesc = value;
	}

	/**
	 * Gets the value of the memo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * Sets the value of the memo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemo(String value) {
		this.memo = value;
	}

	/**
	 * Gets the value of the fbid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFBID() {
		return fbid;
	}

	/**
	 * Sets the value of the fbid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFBID(String value) {
		this.fbid = value;
	}

	/**
	 * Gets the value of the txnShowDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnShowDate() {
		return txnShowDate;
	}

	/**
	 * Sets the value of the txnShowDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnShowDate(String value) {
		this.txnShowDate = value;
	}

	/**
	 * Gets the value of the txnOppno property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnOppno() {
		return txnOppno;
	}

	/**
	 * Sets the value of the txnOppno property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTxnOppno(String value) {
		this.txnOppno = value;
	}

	/**
	 * Gets the value of the operUser property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOperUser() {
		return operUser;
	}

	/**
	 * Sets the value of the operUser property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOperUser(String value) {
		this.operUser = value;
	}

	/**
	 * Gets the value of the mediumNflg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediumNflg() {
		return mediumNflg;
	}

	/**
	 * Sets the value of the mediumNflg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediumNflg(String value) {
		this.mediumNflg = value;
	}

}
