package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for C167CoCorpCusInqRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="C167CoCorpCusInqRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TCustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mnemonic" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CNShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBName1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CNName1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OfficePhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SecTraderCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Industry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlCnapsCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlEisCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlAcName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlAcBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlAcNoCash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StlAcNoBond" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpCusInfoRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CorpCusInfoRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AcctOfficerRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}AcctOfficerRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Introducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerLby" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustDepType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IssueCheques" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntyTagRegNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CityTaxRegNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanCodeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PerCodePrAc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OldCusId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ScopeOfBus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RgBegDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LimitDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegPerIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegPerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtPossCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoAcPerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoLegalType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoLegalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoLegalPersonNm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoLegalPerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtNation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgtId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CapCurr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CapReg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CapRecPer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CfoName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CfoIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOIdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOTelNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFOPostCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShrhldRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}ShrhldRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RelCusNameRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}RelCusNameRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InstCredtCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CusContriType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatementSndFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BirthIncorpDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Addr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResidenceCountryCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalPerAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Target" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AvailableWith" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MedSmEntFlg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VoucherNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyBook" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="C167CorpCfoInfoRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}C167CorpCfoInfoRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AddrAppRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}AddrAppRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Vaddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddrAppSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Continent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Province" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Dist" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Strent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StrentDoor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Contacter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TePhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FaxPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddrUsageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetAddrRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}NetAddrRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NetAddrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetAddrAppSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NetAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddressType_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelType_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBCountryCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBTown" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBStreet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBPostCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GBAddressUsageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BlacklstSrc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgencyRel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Customer_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustLevel_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NewCustLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxRegisNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxPayerFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BenpAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Stdcollflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BookingFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillPrd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubCdFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C167CoCorpCusInqRs", propOrder = { "commonRsHdr", "customer",
		"tCustFlag", "mnemonic", "gbShortName", "cnShortName", "gbName1",
		"cnName1", "language", "officePhone", "taxCountry", "idType", "idNo",
		"clientDt", "custType", "secTraderCode", "industry", "stlCnapsCode",
		"stlEisCode", "stlAcName", "stlAcBank", "stlAcNoCash", "stlAcNoBond",
		"corpCusInfoRec", "acctOfficerRec", "beginDate", "introducer",
		"customerLby", "custDepType", "issueCheques", "cntyTagRegNo",
		"cityTaxRegNo", "loanCodeNo", "perCodePrAc", "oldCusId", "scopeOfBus",
		"regType", "regId", "rgBegDate", "limitDate", "corpName",
		"legPerIdType", "legPerId", "agtPossCode", "legalPhone",
		"pcoAcPerCode", "pcoLegalType", "pcoLegalId", "pcoLegalPersonNm",
		"type", "pcoLegalPerId", "agtNation", "agtName", "agtIdType", "agtId",
		"capCurr", "capReg", "capRecPer", "cfoName", "cfoIdType", "cfoIdNo",
		"cfoTelNo", "cfoAddr", "cfoPostCode", "shrhldRec", "relCusNameRec",
		"instCredtCode", "cusContriType", "statementSndFreq", "birthIncorpDay",
		"addr", "compName", "residenceCountryCd", "legalPerAddr", "target",
		"customerStatus", "availableWith", "medSmEntFlg", "corpCode",
		"voucherNo", "reason", "companyBook", "c167CorpCfoInfoRec",
		"addrAppRec", "vaddr", "channelType", "addrAppSystem", "usage",
		"continent", "addCountry", "province", "city", "dist", "strent",
		"strentDoor", "code", "contacter", "tePhone", "faxPhone",
		"addrUsageType", "netAddrRec", "netAddrNo", "netChannelType",
		"netAddrAppSystem", "netAddr", "addressType2", "channelType2",
		"gbCountryCd", "gbTown", "gbStreet", "gbPostCd", "gbAddressUsageType",
		"remark", "blacklstSrc", "agencyRel", "customer1", "custLevel1",
		"newCustLevel", "taxRegisNo", "taxPayerFlag", "openBank", "benpAcct",
		"stdcollflag", "bookingFreq", "billPrd", "subCdFlag" })
public class C167CoCorpCusInqRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "TCustFlag", required = true)
	protected String tCustFlag;
	@XmlElement(name = "Mnemonic", required = true)
	protected String mnemonic;
	@XmlElement(name = "GBShortName", required = true)
	protected String gbShortName;
	@XmlElement(name = "CNShortName", required = true)
	protected String cnShortName;
	@XmlElement(name = "GBName1", required = true)
	protected String gbName1;
	@XmlElement(name = "CNName1", required = true)
	protected String cnName1;
	@XmlElement(name = "Language", required = true)
	protected String language;
	@XmlElement(name = "OfficePhone", required = true)
	protected String officePhone;
	@XmlElement(name = "TaxCountry", required = true)
	protected String taxCountry;
	@XmlElement(name = "IdType", required = true)
	protected String idType;
	@XmlElement(name = "IdNo", required = true)
	protected String idNo;
	@XmlElement(name = "ClientDt", required = true)
	protected String clientDt;
	@XmlElement(name = "CustType", required = true)
	protected String custType;
	@XmlElement(name = "SecTraderCode", required = true)
	protected String secTraderCode;
	@XmlElement(name = "Industry", required = true)
	protected String industry;
	@XmlElement(name = "StlCnapsCode", required = true)
	protected String stlCnapsCode;
	@XmlElement(name = "StlEisCode", required = true)
	protected String stlEisCode;
	@XmlElement(name = "StlAcName", required = true)
	protected String stlAcName;
	@XmlElement(name = "StlAcBank", required = true)
	protected String stlAcBank;
	@XmlElement(name = "StlAcNoCash", required = true)
	protected String stlAcNoCash;
	@XmlElement(name = "StlAcNoBond", required = true)
	protected String stlAcNoBond;
	@XmlElement(name = "CorpCusInfoRec")
	protected List<CorpCusInfoRec> corpCusInfoRec;
	@XmlElement(name = "AcctOfficerRec")
	protected List<AcctOfficerRec> acctOfficerRec;
	@XmlElement(name = "BeginDate", required = true)
	protected String beginDate;
	@XmlElement(name = "Introducer", required = true)
	protected String introducer;
	@XmlElement(name = "CustomerLby", required = true)
	protected String customerLby;
	@XmlElement(name = "CustDepType", required = true)
	protected String custDepType;
	@XmlElement(name = "IssueCheques", required = true)
	protected String issueCheques;
	@XmlElement(name = "CntyTagRegNo", required = true)
	protected String cntyTagRegNo;
	@XmlElement(name = "CityTaxRegNo", required = true)
	protected String cityTaxRegNo;
	@XmlElement(name = "LoanCodeNo", required = true)
	protected String loanCodeNo;
	@XmlElement(name = "PerCodePrAc", required = true)
	protected String perCodePrAc;
	@XmlElement(name = "OldCusId", required = true)
	protected String oldCusId;
	@XmlElement(name = "ScopeOfBus", required = true)
	protected String scopeOfBus;
	@XmlElement(name = "RegType", required = true)
	protected String regType;
	@XmlElement(name = "RegId", required = true)
	protected String regId;
	@XmlElement(name = "RgBegDate", required = true)
	protected String rgBegDate;
	@XmlElement(name = "LimitDate", required = true)
	protected String limitDate;
	@XmlElement(name = "CorpName", required = true)
	protected String corpName;
	@XmlElement(name = "LegPerIdType", required = true)
	protected String legPerIdType;
	@XmlElement(name = "LegPerId", required = true)
	protected String legPerId;
	@XmlElement(name = "AgtPossCode", required = true)
	protected String agtPossCode;
	@XmlElement(name = "LegalPhone", required = true)
	protected String legalPhone;
	@XmlElement(name = "PcoAcPerCode", required = true)
	protected String pcoAcPerCode;
	@XmlElement(name = "PcoLegalType", required = true)
	protected String pcoLegalType;
	@XmlElement(name = "PcoLegalId", required = true)
	protected String pcoLegalId;
	@XmlElement(name = "PcoLegalPersonNm", required = true)
	protected String pcoLegalPersonNm;
	@XmlElement(name = "Type", required = true)
	protected String type;
	@XmlElement(name = "PcoLegalPerId", required = true)
	protected String pcoLegalPerId;
	@XmlElement(name = "AgtNation", required = true)
	protected String agtNation;
	@XmlElement(name = "AgtName", required = true)
	protected String agtName;
	@XmlElement(name = "AgtIdType", required = true)
	protected String agtIdType;
	@XmlElement(name = "AgtId", required = true)
	protected String agtId;
	@XmlElement(name = "CapCurr", required = true)
	protected String capCurr;
	@XmlElement(name = "CapReg", required = true)
	protected String capReg;
	@XmlElement(name = "CapRecPer", required = true)
	protected String capRecPer;
	@XmlElement(name = "CfoName", required = true)
	protected String cfoName;
	@XmlElement(name = "CfoIdType", required = true)
	protected String cfoIdType;
	@XmlElement(name = "CFOIdNo", required = true)
	protected String cfoIdNo;
	@XmlElement(name = "CFOTelNo", required = true)
	protected String cfoTelNo;
	@XmlElement(name = "CFOAddr", required = true)
	protected String cfoAddr;
	@XmlElement(name = "CFOPostCode", required = true)
	protected String cfoPostCode;
	@XmlElement(name = "ShrhldRec")
	protected List<ShrhldRec> shrhldRec;
	@XmlElement(name = "RelCusNameRec")
	protected List<RelCusNameRec> relCusNameRec;
	@XmlElement(name = "InstCredtCode", required = true)
	protected String instCredtCode;
	@XmlElement(name = "CusContriType", required = true)
	protected String cusContriType;
	@XmlElement(name = "StatementSndFreq", required = true)
	protected String statementSndFreq;
	@XmlElement(name = "BirthIncorpDay", required = true)
	protected String birthIncorpDay;
	@XmlElement(name = "Addr", required = true)
	protected String addr;
	@XmlElement(name = "CompName", required = true)
	protected String compName;
	@XmlElement(name = "ResidenceCountryCd", required = true)
	protected String residenceCountryCd;
	@XmlElement(name = "LegalPerAddr", required = true)
	protected String legalPerAddr;
	@XmlElement(name = "Target", required = true)
	protected String target;
	@XmlElement(name = "CustomerStatus", required = true)
	protected String customerStatus;
	@XmlElement(name = "AvailableWith", required = true)
	protected String availableWith;
	@XmlElement(name = "MedSmEntFlg", required = true)
	protected String medSmEntFlg;
	@XmlElement(name = "CorpCode", required = true)
	protected String corpCode;
	@XmlElement(name = "VoucherNo", required = true)
	protected String voucherNo;
	@XmlElement(name = "Reason", required = true)
	protected String reason;
	@XmlElement(name = "CompanyBook", required = true)
	protected String companyBook;
	@XmlElement(name = "C167CorpCfoInfoRec")
	protected List<C167CorpCfoInfoRec> c167CorpCfoInfoRec;
	@XmlElement(name = "AddrAppRec")
	protected List<AddrAppRec> addrAppRec;
	@XmlElement(name = "Vaddr", required = true)
	protected String vaddr;
	@XmlElement(name = "ChannelType", required = true)
	protected String channelType;
	@XmlElement(name = "AddrAppSystem", required = true)
	protected String addrAppSystem;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "Continent", required = true)
	protected String continent;
	@XmlElement(name = "AddCountry", required = true)
	protected String addCountry;
	@XmlElement(name = "Province", required = true)
	protected String province;
	@XmlElement(name = "City", required = true)
	protected String city;
	@XmlElement(name = "Dist", required = true)
	protected String dist;
	@XmlElement(name = "Strent", required = true)
	protected String strent;
	@XmlElement(name = "StrentDoor", required = true)
	protected String strentDoor;
	@XmlElement(name = "Code", required = true)
	protected String code;
	@XmlElement(name = "Contacter", required = true)
	protected String contacter;
	@XmlElement(name = "TePhone", required = true)
	protected String tePhone;
	@XmlElement(name = "FaxPhone", required = true)
	protected String faxPhone;
	@XmlElement(name = "AddrUsageType", required = true)
	protected String addrUsageType;
	@XmlElement(name = "NetAddrRec")
	protected List<NetAddrRec> netAddrRec;
	@XmlElement(name = "NetAddrNo", required = true)
	protected String netAddrNo;
	@XmlElement(name = "NetChannelType", required = true)
	protected String netChannelType;
	@XmlElement(name = "NetAddrAppSystem", required = true)
	protected String netAddrAppSystem;
	@XmlElement(name = "NetAddr", required = true)
	protected String netAddr;
	@XmlElement(name = "AddressType_2", required = true)
	protected String addressType2;
	@XmlElement(name = "ChannelType_2", required = true)
	protected String channelType2;
	@XmlElement(name = "GBCountryCd", required = true)
	protected String gbCountryCd;
	@XmlElement(name = "GBTown", required = true)
	protected String gbTown;
	@XmlElement(name = "GBStreet", required = true)
	protected String gbStreet;
	@XmlElement(name = "GBPostCd", required = true)
	protected String gbPostCd;
	@XmlElement(name = "GBAddressUsageType", required = true)
	protected String gbAddressUsageType;
	@XmlElement(name = "Remark", required = true)
	protected String remark;
	@XmlElement(name = "BlacklstSrc", required = true)
	protected String blacklstSrc;
	@XmlElement(name = "AgencyRel", required = true)
	protected String agencyRel;
	@XmlElement(name = "Customer_1", required = true)
	protected String customer1;
	@XmlElement(name = "CustLevel_1", required = true)
	protected String custLevel1;
	@XmlElement(name = "NewCustLevel", required = true)
	protected String newCustLevel;
	@XmlElement(name = "TaxRegisNo", required = true)
	protected String taxRegisNo;
	@XmlElement(name = "TaxPayerFlag", required = true)
	protected String taxPayerFlag;
	@XmlElement(name = "OpenBank", required = true)
	protected String openBank;
	@XmlElement(name = "BenpAcct", required = true)
	protected String benpAcct;
	@XmlElement(name = "Stdcollflag", required = true)
	protected String stdcollflag;
	@XmlElement(name = "BookingFreq", required = true)
	protected String bookingFreq;
	@XmlElement(name = "BillPrd", required = true)
	protected String billPrd;
	@XmlElement(name = "SubCdFlag", required = true)
	protected String subCdFlag;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the tCustFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTCustFlag() {
		return tCustFlag;
	}

	/**
	 * Sets the value of the tCustFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTCustFlag(String value) {
		this.tCustFlag = value;
	}

	/**
	 * Gets the value of the mnemonic property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the value of the mnemonic property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMnemonic(String value) {
		this.mnemonic = value;
	}

	/**
	 * Gets the value of the gbShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBShortName() {
		return gbShortName;
	}

	/**
	 * Sets the value of the gbShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBShortName(String value) {
		this.gbShortName = value;
	}

	/**
	 * Gets the value of the cnShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCNShortName() {
		return cnShortName;
	}

	/**
	 * Sets the value of the cnShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCNShortName(String value) {
		this.cnShortName = value;
	}

	/**
	 * Gets the value of the gbName1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBName1() {
		return gbName1;
	}

	/**
	 * Sets the value of the gbName1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBName1(String value) {
		this.gbName1 = value;
	}

	/**
	 * Gets the value of the cnName1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCNName1() {
		return cnName1;
	}

	/**
	 * Sets the value of the cnName1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCNName1(String value) {
		this.cnName1 = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

	/**
	 * Gets the value of the officePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfficePhone() {
		return officePhone;
	}

	/**
	 * Sets the value of the officePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfficePhone(String value) {
		this.officePhone = value;
	}

	/**
	 * Gets the value of the taxCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxCountry() {
		return taxCountry;
	}

	/**
	 * Sets the value of the taxCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxCountry(String value) {
		this.taxCountry = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdType(String value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdNo() {
		return idNo;
	}

	/**
	 * Sets the value of the idNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdNo(String value) {
		this.idNo = value;
	}

	/**
	 * Gets the value of the clientDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClientDt() {
		return clientDt;
	}

	/**
	 * Sets the value of the clientDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClientDt(String value) {
		this.clientDt = value;
	}

	/**
	 * Gets the value of the custType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * Sets the value of the custType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustType(String value) {
		this.custType = value;
	}

	/**
	 * Gets the value of the secTraderCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSecTraderCode() {
		return secTraderCode;
	}

	/**
	 * Sets the value of the secTraderCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSecTraderCode(String value) {
		this.secTraderCode = value;
	}

	/**
	 * Gets the value of the industry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * Sets the value of the industry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndustry(String value) {
		this.industry = value;
	}

	/**
	 * Gets the value of the stlCnapsCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlCnapsCode() {
		return stlCnapsCode;
	}

	/**
	 * Sets the value of the stlCnapsCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlCnapsCode(String value) {
		this.stlCnapsCode = value;
	}

	/**
	 * Gets the value of the stlEisCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlEisCode() {
		return stlEisCode;
	}

	/**
	 * Sets the value of the stlEisCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlEisCode(String value) {
		this.stlEisCode = value;
	}

	/**
	 * Gets the value of the stlAcName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlAcName() {
		return stlAcName;
	}

	/**
	 * Sets the value of the stlAcName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlAcName(String value) {
		this.stlAcName = value;
	}

	/**
	 * Gets the value of the stlAcBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlAcBank() {
		return stlAcBank;
	}

	/**
	 * Sets the value of the stlAcBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlAcBank(String value) {
		this.stlAcBank = value;
	}

	/**
	 * Gets the value of the stlAcNoCash property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlAcNoCash() {
		return stlAcNoCash;
	}

	/**
	 * Sets the value of the stlAcNoCash property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlAcNoCash(String value) {
		this.stlAcNoCash = value;
	}

	/**
	 * Gets the value of the stlAcNoBond property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStlAcNoBond() {
		return stlAcNoBond;
	}

	/**
	 * Sets the value of the stlAcNoBond property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStlAcNoBond(String value) {
		this.stlAcNoBond = value;
	}

	/**
	 * Gets the value of the corpCusInfoRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the corpCusInfoRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorpCusInfoRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CorpCusInfoRec }
	 * 
	 * 
	 */
	public List<CorpCusInfoRec> getCorpCusInfoRec() {
		if (corpCusInfoRec == null) {
			corpCusInfoRec = new ArrayList<CorpCusInfoRec>();
		}
		return this.corpCusInfoRec;
	}

	/**
	 * Gets the value of the acctOfficerRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the acctOfficerRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAcctOfficerRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AcctOfficerRec }
	 * 
	 * 
	 */
	public List<AcctOfficerRec> getAcctOfficerRec() {
		if (acctOfficerRec == null) {
			acctOfficerRec = new ArrayList<AcctOfficerRec>();
		}
		return this.acctOfficerRec;
	}

	/**
	 * Gets the value of the beginDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDate() {
		return beginDate;
	}

	/**
	 * Sets the value of the beginDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDate(String value) {
		this.beginDate = value;
	}

	/**
	 * Gets the value of the introducer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntroducer() {
		return introducer;
	}

	/**
	 * Sets the value of the introducer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntroducer(String value) {
		this.introducer = value;
	}

	/**
	 * Gets the value of the customerLby property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerLby() {
		return customerLby;
	}

	/**
	 * Sets the value of the customerLby property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerLby(String value) {
		this.customerLby = value;
	}

	/**
	 * Gets the value of the custDepType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustDepType() {
		return custDepType;
	}

	/**
	 * Sets the value of the custDepType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustDepType(String value) {
		this.custDepType = value;
	}

	/**
	 * Gets the value of the issueCheques property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIssueCheques() {
		return issueCheques;
	}

	/**
	 * Sets the value of the issueCheques property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIssueCheques(String value) {
		this.issueCheques = value;
	}

	/**
	 * Gets the value of the cntyTagRegNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntyTagRegNo() {
		return cntyTagRegNo;
	}

	/**
	 * Sets the value of the cntyTagRegNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntyTagRegNo(String value) {
		this.cntyTagRegNo = value;
	}

	/**
	 * Gets the value of the cityTaxRegNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCityTaxRegNo() {
		return cityTaxRegNo;
	}

	/**
	 * Sets the value of the cityTaxRegNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCityTaxRegNo(String value) {
		this.cityTaxRegNo = value;
	}

	/**
	 * Gets the value of the loanCodeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoanCodeNo() {
		return loanCodeNo;
	}

	/**
	 * Sets the value of the loanCodeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoanCodeNo(String value) {
		this.loanCodeNo = value;
	}

	/**
	 * Gets the value of the perCodePrAc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPerCodePrAc() {
		return perCodePrAc;
	}

	/**
	 * Sets the value of the perCodePrAc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPerCodePrAc(String value) {
		this.perCodePrAc = value;
	}

	/**
	 * Gets the value of the oldCusId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOldCusId() {
		return oldCusId;
	}

	/**
	 * Sets the value of the oldCusId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOldCusId(String value) {
		this.oldCusId = value;
	}

	/**
	 * Gets the value of the scopeOfBus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScopeOfBus() {
		return scopeOfBus;
	}

	/**
	 * Sets the value of the scopeOfBus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScopeOfBus(String value) {
		this.scopeOfBus = value;
	}

	/**
	 * Gets the value of the regType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegType() {
		return regType;
	}

	/**
	 * Sets the value of the regType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegType(String value) {
		this.regType = value;
	}

	/**
	 * Gets the value of the regId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegId() {
		return regId;
	}

	/**
	 * Sets the value of the regId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegId(String value) {
		this.regId = value;
	}

	/**
	 * Gets the value of the rgBegDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRgBegDate() {
		return rgBegDate;
	}

	/**
	 * Sets the value of the rgBegDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRgBegDate(String value) {
		this.rgBegDate = value;
	}

	/**
	 * Gets the value of the limitDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitDate() {
		return limitDate;
	}

	/**
	 * Sets the value of the limitDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitDate(String value) {
		this.limitDate = value;
	}

	/**
	 * Gets the value of the corpName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpName() {
		return corpName;
	}

	/**
	 * Sets the value of the corpName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpName(String value) {
		this.corpName = value;
	}

	/**
	 * Gets the value of the legPerIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegPerIdType() {
		return legPerIdType;
	}

	/**
	 * Sets the value of the legPerIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegPerIdType(String value) {
		this.legPerIdType = value;
	}

	/**
	 * Gets the value of the legPerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegPerId() {
		return legPerId;
	}

	/**
	 * Sets the value of the legPerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegPerId(String value) {
		this.legPerId = value;
	}

	/**
	 * Gets the value of the agtPossCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtPossCode() {
		return agtPossCode;
	}

	/**
	 * Sets the value of the agtPossCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtPossCode(String value) {
		this.agtPossCode = value;
	}

	/**
	 * Gets the value of the legalPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalPhone() {
		return legalPhone;
	}

	/**
	 * Sets the value of the legalPhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalPhone(String value) {
		this.legalPhone = value;
	}

	/**
	 * Gets the value of the pcoAcPerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoAcPerCode() {
		return pcoAcPerCode;
	}

	/**
	 * Sets the value of the pcoAcPerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoAcPerCode(String value) {
		this.pcoAcPerCode = value;
	}

	/**
	 * Gets the value of the pcoLegalType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoLegalType() {
		return pcoLegalType;
	}

	/**
	 * Sets the value of the pcoLegalType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoLegalType(String value) {
		this.pcoLegalType = value;
	}

	/**
	 * Gets the value of the pcoLegalId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoLegalId() {
		return pcoLegalId;
	}

	/**
	 * Sets the value of the pcoLegalId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoLegalId(String value) {
		this.pcoLegalId = value;
	}

	/**
	 * Gets the value of the pcoLegalPersonNm property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoLegalPersonNm() {
		return pcoLegalPersonNm;
	}

	/**
	 * Sets the value of the pcoLegalPersonNm property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoLegalPersonNm(String value) {
		this.pcoLegalPersonNm = value;
	}

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setType(String value) {
		this.type = value;
	}

	/**
	 * Gets the value of the pcoLegalPerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoLegalPerId() {
		return pcoLegalPerId;
	}

	/**
	 * Sets the value of the pcoLegalPerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoLegalPerId(String value) {
		this.pcoLegalPerId = value;
	}

	/**
	 * Gets the value of the agtNation property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtNation() {
		return agtNation;
	}

	/**
	 * Sets the value of the agtNation property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtNation(String value) {
		this.agtNation = value;
	}

	/**
	 * Gets the value of the agtName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtName() {
		return agtName;
	}

	/**
	 * Sets the value of the agtName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtName(String value) {
		this.agtName = value;
	}

	/**
	 * Gets the value of the agtIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtIdType() {
		return agtIdType;
	}

	/**
	 * Sets the value of the agtIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtIdType(String value) {
		this.agtIdType = value;
	}

	/**
	 * Gets the value of the agtId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgtId() {
		return agtId;
	}

	/**
	 * Sets the value of the agtId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgtId(String value) {
		this.agtId = value;
	}

	/**
	 * Gets the value of the capCurr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCapCurr() {
		return capCurr;
	}

	/**
	 * Sets the value of the capCurr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCapCurr(String value) {
		this.capCurr = value;
	}

	/**
	 * Gets the value of the capReg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCapReg() {
		return capReg;
	}

	/**
	 * Sets the value of the capReg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCapReg(String value) {
		this.capReg = value;
	}

	/**
	 * Gets the value of the capRecPer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCapRecPer() {
		return capRecPer;
	}

	/**
	 * Sets the value of the capRecPer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCapRecPer(String value) {
		this.capRecPer = value;
	}

	/**
	 * Gets the value of the cfoName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCfoName() {
		return cfoName;
	}

	/**
	 * Sets the value of the cfoName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCfoName(String value) {
		this.cfoName = value;
	}

	/**
	 * Gets the value of the cfoIdType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCfoIdType() {
		return cfoIdType;
	}

	/**
	 * Sets the value of the cfoIdType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCfoIdType(String value) {
		this.cfoIdType = value;
	}

	/**
	 * Gets the value of the cfoIdNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOIdNo() {
		return cfoIdNo;
	}

	/**
	 * Sets the value of the cfoIdNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOIdNo(String value) {
		this.cfoIdNo = value;
	}

	/**
	 * Gets the value of the cfoTelNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOTelNo() {
		return cfoTelNo;
	}

	/**
	 * Sets the value of the cfoTelNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOTelNo(String value) {
		this.cfoTelNo = value;
	}

	/**
	 * Gets the value of the cfoAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOAddr() {
		return cfoAddr;
	}

	/**
	 * Sets the value of the cfoAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOAddr(String value) {
		this.cfoAddr = value;
	}

	/**
	 * Gets the value of the cfoPostCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCFOPostCode() {
		return cfoPostCode;
	}

	/**
	 * Sets the value of the cfoPostCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCFOPostCode(String value) {
		this.cfoPostCode = value;
	}

	/**
	 * Gets the value of the shrhldRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the shrhldRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getShrhldRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ShrhldRec }
	 * 
	 * 
	 */
	public List<ShrhldRec> getShrhldRec() {
		if (shrhldRec == null) {
			shrhldRec = new ArrayList<ShrhldRec>();
		}
		return this.shrhldRec;
	}

	/**
	 * Gets the value of the relCusNameRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the relCusNameRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getRelCusNameRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link RelCusNameRec }
	 * 
	 * 
	 */
	public List<RelCusNameRec> getRelCusNameRec() {
		if (relCusNameRec == null) {
			relCusNameRec = new ArrayList<RelCusNameRec>();
		}
		return this.relCusNameRec;
	}

	/**
	 * Gets the value of the instCredtCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInstCredtCode() {
		return instCredtCode;
	}

	/**
	 * Sets the value of the instCredtCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInstCredtCode(String value) {
		this.instCredtCode = value;
	}

	/**
	 * Gets the value of the cusContriType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCusContriType() {
		return cusContriType;
	}

	/**
	 * Sets the value of the cusContriType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCusContriType(String value) {
		this.cusContriType = value;
	}

	/**
	 * Gets the value of the statementSndFreq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatementSndFreq() {
		return statementSndFreq;
	}

	/**
	 * Sets the value of the statementSndFreq property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatementSndFreq(String value) {
		this.statementSndFreq = value;
	}

	/**
	 * Gets the value of the birthIncorpDay property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBirthIncorpDay() {
		return birthIncorpDay;
	}

	/**
	 * Sets the value of the birthIncorpDay property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBirthIncorpDay(String value) {
		this.birthIncorpDay = value;
	}

	/**
	 * Gets the value of the addr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddr() {
		return addr;
	}

	/**
	 * Sets the value of the addr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddr(String value) {
		this.addr = value;
	}

	/**
	 * Gets the value of the compName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompName() {
		return compName;
	}

	/**
	 * Sets the value of the compName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompName(String value) {
		this.compName = value;
	}

	/**
	 * Gets the value of the residenceCountryCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResidenceCountryCd() {
		return residenceCountryCd;
	}

	/**
	 * Sets the value of the residenceCountryCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResidenceCountryCd(String value) {
		this.residenceCountryCd = value;
	}

	/**
	 * Gets the value of the legalPerAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalPerAddr() {
		return legalPerAddr;
	}

	/**
	 * Sets the value of the legalPerAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalPerAddr(String value) {
		this.legalPerAddr = value;
	}

	/**
	 * Gets the value of the target property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Sets the value of the target property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTarget(String value) {
		this.target = value;
	}

	/**
	 * Gets the value of the customerStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerStatus() {
		return customerStatus;
	}

	/**
	 * Sets the value of the customerStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerStatus(String value) {
		this.customerStatus = value;
	}

	/**
	 * Gets the value of the availableWith property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAvailableWith() {
		return availableWith;
	}

	/**
	 * Sets the value of the availableWith property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAvailableWith(String value) {
		this.availableWith = value;
	}

	/**
	 * Gets the value of the medSmEntFlg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMedSmEntFlg() {
		return medSmEntFlg;
	}

	/**
	 * Sets the value of the medSmEntFlg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMedSmEntFlg(String value) {
		this.medSmEntFlg = value;
	}

	/**
	 * Gets the value of the corpCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpCode() {
		return corpCode;
	}

	/**
	 * Sets the value of the corpCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpCode(String value) {
		this.corpCode = value;
	}

	/**
	 * Gets the value of the voucherNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVoucherNo() {
		return voucherNo;
	}

	/**
	 * Sets the value of the voucherNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVoucherNo(String value) {
		this.voucherNo = value;
	}

	/**
	 * Gets the value of the reason property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Sets the value of the reason property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReason(String value) {
		this.reason = value;
	}

	/**
	 * Gets the value of the companyBook property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyBook() {
		return companyBook;
	}

	/**
	 * Sets the value of the companyBook property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyBook(String value) {
		this.companyBook = value;
	}

	/**
	 * Gets the value of the c167CorpCfoInfoRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the c167CorpCfoInfoRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getC167CorpCfoInfoRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link C167CorpCfoInfoRec }
	 * 
	 * 
	 */
	public List<C167CorpCfoInfoRec> getC167CorpCfoInfoRec() {
		if (c167CorpCfoInfoRec == null) {
			c167CorpCfoInfoRec = new ArrayList<C167CorpCfoInfoRec>();
		}
		return this.c167CorpCfoInfoRec;
	}

	/**
	 * Gets the value of the addrAppRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the addrAppRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAddrAppRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AddrAppRec }
	 * 
	 * 
	 */
	public List<AddrAppRec> getAddrAppRec() {
		if (addrAppRec == null) {
			addrAppRec = new ArrayList<AddrAppRec>();
		}
		return this.addrAppRec;
	}

	/**
	 * Gets the value of the vaddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVaddr() {
		return vaddr;
	}

	/**
	 * Sets the value of the vaddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVaddr(String value) {
		this.vaddr = value;
	}

	/**
	 * Gets the value of the channelType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelType() {
		return channelType;
	}

	/**
	 * Sets the value of the channelType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelType(String value) {
		this.channelType = value;
	}

	/**
	 * Gets the value of the addrAppSystem property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrAppSystem() {
		return addrAppSystem;
	}

	/**
	 * Sets the value of the addrAppSystem property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrAppSystem(String value) {
		this.addrAppSystem = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the continent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContinent() {
		return continent;
	}

	/**
	 * Sets the value of the continent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContinent(String value) {
		this.continent = value;
	}

	/**
	 * Gets the value of the addCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddCountry() {
		return addCountry;
	}

	/**
	 * Sets the value of the addCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddCountry(String value) {
		this.addCountry = value;
	}

	/**
	 * Gets the value of the province property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the value of the province property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProvince(String value) {
		this.province = value;
	}

	/**
	 * Gets the value of the city property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the value of the city property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCity(String value) {
		this.city = value;
	}

	/**
	 * Gets the value of the dist property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDist() {
		return dist;
	}

	/**
	 * Sets the value of the dist property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDist(String value) {
		this.dist = value;
	}

	/**
	 * Gets the value of the strent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStrent() {
		return strent;
	}

	/**
	 * Sets the value of the strent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStrent(String value) {
		this.strent = value;
	}

	/**
	 * Gets the value of the strentDoor property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStrentDoor() {
		return strentDoor;
	}

	/**
	 * Sets the value of the strentDoor property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStrentDoor(String value) {
		this.strentDoor = value;
	}

	/**
	 * Gets the value of the code property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the value of the code property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCode(String value) {
		this.code = value;
	}

	/**
	 * Gets the value of the contacter property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContacter() {
		return contacter;
	}

	/**
	 * Sets the value of the contacter property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContacter(String value) {
		this.contacter = value;
	}

	/**
	 * Gets the value of the tePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTePhone() {
		return tePhone;
	}

	/**
	 * Sets the value of the tePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTePhone(String value) {
		this.tePhone = value;
	}

	/**
	 * Gets the value of the faxPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFaxPhone() {
		return faxPhone;
	}

	/**
	 * Sets the value of the faxPhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFaxPhone(String value) {
		this.faxPhone = value;
	}

	/**
	 * Gets the value of the addrUsageType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrUsageType() {
		return addrUsageType;
	}

	/**
	 * Sets the value of the addrUsageType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrUsageType(String value) {
		this.addrUsageType = value;
	}

	/**
	 * Gets the value of the netAddrRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the netAddrRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getNetAddrRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link NetAddrRec }
	 * 
	 * 
	 */
	public List<NetAddrRec> getNetAddrRec() {
		if (netAddrRec == null) {
			netAddrRec = new ArrayList<NetAddrRec>();
		}
		return this.netAddrRec;
	}

	/**
	 * Gets the value of the netAddrNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetAddrNo() {
		return netAddrNo;
	}

	/**
	 * Sets the value of the netAddrNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetAddrNo(String value) {
		this.netAddrNo = value;
	}

	/**
	 * Gets the value of the netChannelType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetChannelType() {
		return netChannelType;
	}

	/**
	 * Sets the value of the netChannelType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetChannelType(String value) {
		this.netChannelType = value;
	}

	/**
	 * Gets the value of the netAddrAppSystem property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetAddrAppSystem() {
		return netAddrAppSystem;
	}

	/**
	 * Sets the value of the netAddrAppSystem property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetAddrAppSystem(String value) {
		this.netAddrAppSystem = value;
	}

	/**
	 * Gets the value of the netAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetAddr() {
		return netAddr;
	}

	/**
	 * Sets the value of the netAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetAddr(String value) {
		this.netAddr = value;
	}

	/**
	 * Gets the value of the addressType2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressType2() {
		return addressType2;
	}

	/**
	 * Sets the value of the addressType2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressType2(String value) {
		this.addressType2 = value;
	}

	/**
	 * Gets the value of the channelType2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelType2() {
		return channelType2;
	}

	/**
	 * Sets the value of the channelType2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelType2(String value) {
		this.channelType2 = value;
	}

	/**
	 * Gets the value of the gbCountryCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBCountryCd() {
		return gbCountryCd;
	}

	/**
	 * Sets the value of the gbCountryCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBCountryCd(String value) {
		this.gbCountryCd = value;
	}

	/**
	 * Gets the value of the gbTown property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBTown() {
		return gbTown;
	}

	/**
	 * Sets the value of the gbTown property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBTown(String value) {
		this.gbTown = value;
	}

	/**
	 * Gets the value of the gbStreet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBStreet() {
		return gbStreet;
	}

	/**
	 * Sets the value of the gbStreet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBStreet(String value) {
		this.gbStreet = value;
	}

	/**
	 * Gets the value of the gbPostCd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBPostCd() {
		return gbPostCd;
	}

	/**
	 * Sets the value of the gbPostCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBPostCd(String value) {
		this.gbPostCd = value;
	}

	/**
	 * Gets the value of the gbAddressUsageType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGBAddressUsageType() {
		return gbAddressUsageType;
	}

	/**
	 * Sets the value of the gbAddressUsageType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGBAddressUsageType(String value) {
		this.gbAddressUsageType = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the blacklstSrc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBlacklstSrc() {
		return blacklstSrc;
	}

	/**
	 * Sets the value of the blacklstSrc property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBlacklstSrc(String value) {
		this.blacklstSrc = value;
	}

	/**
	 * Gets the value of the agencyRel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgencyRel() {
		return agencyRel;
	}

	/**
	 * Sets the value of the agencyRel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgencyRel(String value) {
		this.agencyRel = value;
	}

	/**
	 * Gets the value of the customer1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer1() {
		return customer1;
	}

	/**
	 * Sets the value of the customer1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer1(String value) {
		this.customer1 = value;
	}

	/**
	 * Gets the value of the custLevel1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustLevel1() {
		return custLevel1;
	}

	/**
	 * Sets the value of the custLevel1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustLevel1(String value) {
		this.custLevel1 = value;
	}

	/**
	 * Gets the value of the newCustLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNewCustLevel() {
		return newCustLevel;
	}

	/**
	 * Sets the value of the newCustLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNewCustLevel(String value) {
		this.newCustLevel = value;
	}

	/**
	 * Gets the value of the taxRegisNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxRegisNo() {
		return taxRegisNo;
	}

	/**
	 * Sets the value of the taxRegisNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxRegisNo(String value) {
		this.taxRegisNo = value;
	}

	/**
	 * Gets the value of the taxPayerFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxPayerFlag() {
		return taxPayerFlag;
	}

	/**
	 * Sets the value of the taxPayerFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTaxPayerFlag(String value) {
		this.taxPayerFlag = value;
	}

	/**
	 * Gets the value of the openBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenBank() {
		return openBank;
	}

	/**
	 * Sets the value of the openBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenBank(String value) {
		this.openBank = value;
	}

	/**
	 * Gets the value of the benpAcct property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBenpAcct() {
		return benpAcct;
	}

	/**
	 * Sets the value of the benpAcct property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBenpAcct(String value) {
		this.benpAcct = value;
	}

	/**
	 * Gets the value of the stdcollflag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStdcollflag() {
		return stdcollflag;
	}

	/**
	 * Sets the value of the stdcollflag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStdcollflag(String value) {
		this.stdcollflag = value;
	}

	/**
	 * Gets the value of the bookingFreq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBookingFreq() {
		return bookingFreq;
	}

	/**
	 * Sets the value of the bookingFreq property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBookingFreq(String value) {
		this.bookingFreq = value;
	}

	/**
	 * Gets the value of the billPrd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillPrd() {
		return billPrd;
	}

	/**
	 * Sets the value of the billPrd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillPrd(String value) {
		this.billPrd = value;
	}

	/**
	 * Gets the value of the subCdFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubCdFlag() {
		return subCdFlag;
	}

	/**
	 * Sets the value of the subCdFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubCdFlag(String value) {
		this.subCdFlag = value;
	}

}
