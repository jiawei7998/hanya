package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AddrAppRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AddrAppRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpTypeAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddressType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Vaddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddrAppSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Usage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Continent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Province" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Dist" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Strent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StrentDoor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TePhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FaxPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AddrUsageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddrAppRec", propOrder = { "opTypeAddr", "addressType",
		"vaddr", "channelType", "addrAppSystem", "usage", "continent",
		"addCountry", "province", "city", "dist", "strent", "strentDoor",
		"tePhone", "faxPhone", "addrUsageType" })
public class AddrAppRec {

	@XmlElement(name = "OpTypeAddr", required = true)
	protected String opTypeAddr;
	@XmlElement(name = "AddressType", required = true)
	protected String addressType;
	@XmlElement(name = "Vaddr", required = true)
	protected String vaddr;
	@XmlElement(name = "ChannelType", required = true)
	protected String channelType;
	@XmlElement(name = "AddrAppSystem", required = true)
	protected String addrAppSystem;
	@XmlElement(name = "Usage", required = true)
	protected String usage;
	@XmlElement(name = "Continent", required = true)
	protected String continent;
	@XmlElement(name = "AddCountry", required = true)
	protected String addCountry;
	@XmlElement(name = "Province", required = true)
	protected String province;
	@XmlElement(name = "City", required = true)
	protected String city;
	@XmlElement(name = "Dist", required = true)
	protected String dist;
	@XmlElement(name = "Strent", required = true)
	protected String strent;
	@XmlElement(name = "StrentDoor", required = true)
	protected String strentDoor;
	@XmlElement(name = "TePhone", required = true)
	protected String tePhone;
	@XmlElement(name = "FaxPhone", required = true)
	protected String faxPhone;
	@XmlElement(name = "AddrUsageType", required = true)
	protected String addrUsageType;

	/**
	 * Gets the value of the opTypeAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpTypeAddr() {
		return opTypeAddr;
	}

	/**
	 * Sets the value of the opTypeAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpTypeAddr(String value) {
		this.opTypeAddr = value;
	}

	/**
	 * Gets the value of the addressType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressType() {
		return addressType;
	}

	/**
	 * Sets the value of the addressType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressType(String value) {
		this.addressType = value;
	}

	/**
	 * Gets the value of the vaddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVaddr() {
		return vaddr;
	}

	/**
	 * Sets the value of the vaddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVaddr(String value) {
		this.vaddr = value;
	}

	/**
	 * Gets the value of the channelType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelType() {
		return channelType;
	}

	/**
	 * Sets the value of the channelType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelType(String value) {
		this.channelType = value;
	}

	/**
	 * Gets the value of the addrAppSystem property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrAppSystem() {
		return addrAppSystem;
	}

	/**
	 * Sets the value of the addrAppSystem property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrAppSystem(String value) {
		this.addrAppSystem = value;
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsage(String value) {
		this.usage = value;
	}

	/**
	 * Gets the value of the continent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContinent() {
		return continent;
	}

	/**
	 * Sets the value of the continent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContinent(String value) {
		this.continent = value;
	}

	/**
	 * Gets the value of the addCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddCountry() {
		return addCountry;
	}

	/**
	 * Sets the value of the addCountry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddCountry(String value) {
		this.addCountry = value;
	}

	/**
	 * Gets the value of the province property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the value of the province property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProvince(String value) {
		this.province = value;
	}

	/**
	 * Gets the value of the city property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the value of the city property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCity(String value) {
		this.city = value;
	}

	/**
	 * Gets the value of the dist property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDist() {
		return dist;
	}

	/**
	 * Sets the value of the dist property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDist(String value) {
		this.dist = value;
	}

	/**
	 * Gets the value of the strent property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStrent() {
		return strent;
	}

	/**
	 * Sets the value of the strent property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStrent(String value) {
		this.strent = value;
	}

	/**
	 * Gets the value of the strentDoor property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStrentDoor() {
		return strentDoor;
	}

	/**
	 * Sets the value of the strentDoor property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStrentDoor(String value) {
		this.strentDoor = value;
	}

	/**
	 * Gets the value of the tePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTePhone() {
		return tePhone;
	}

	/**
	 * Sets the value of the tePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTePhone(String value) {
		this.tePhone = value;
	}

	/**
	 * Gets the value of the faxPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFaxPhone() {
		return faxPhone;
	}

	/**
	 * Sets the value of the faxPhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFaxPhone(String value) {
		this.faxPhone = value;
	}

	/**
	 * Gets the value of the addrUsageType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddrUsageType() {
		return addrUsageType;
	}

	/**
	 * Sets the value of the addrUsageType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddrUsageType(String value) {
		this.addrUsageType = value;
	}

}
