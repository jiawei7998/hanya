package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TdTrdValueAssessment;
import tk.mybatis.mapper.common.Mapper;

public interface TdTrdValueAssessmentMapper extends Mapper<TdTrdValueAssessment>{

	/**
	 * 
	 * 估值数据查询
	*/
	public Page<TdTrdValueAssessment> pageList(Map<String,Object> map, RowBounds row);
	
	/**
	 * 
	 * 导入估值数据(批量掺入)
	*/
	public void  insertListExcel(List<TdTrdValueAssessment> list);
	/**
	 * 根据proCode回去dealNo
	*/
	public List<String> searchList(String prodCode);
	/**
	 * 导入之前，根据prod_code 和 val_date 查询原有的表格数据，如果有这相同的属性，则先删除，再insert
	 * 
	*/
	public List<String> searchProCodeValDate(@Param("prodCode")String prodcode,@Param("valDate")String valDate);
	/**导入
	 * 
	 *相同日期下 同一prod_code只能存在一个值 每次导入时根据 日期 和prod_code查询 ,如果存在则先删除 在insert
	 *
	 *根据searchList查询列表，如果查到了数据，删除。
	*/
	public  void deleteVal(Map<String,Object> map);
	
	public int createValueAssessmentByDate(Map<String, Object> map);
	
}
