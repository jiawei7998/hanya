package com.singlee.capital.interfacex.model;

public class NightJobStatus {
	private String jobId;
	private String jobName;
	private String excuTime;
	private String recordStatus;
	private String inputTime;
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getExcuTime() {
		return excuTime;
	}
	public void setExcuTime(String excuTime) {
		this.excuTime = excuTime;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
}
