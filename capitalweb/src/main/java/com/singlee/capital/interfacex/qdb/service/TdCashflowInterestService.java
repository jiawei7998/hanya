package com.singlee.capital.interfacex.qdb.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

public interface TdCashflowInterestService {
	/**
	 * 查询收息计划确认信息
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2017-5-18
	 */
	public Page<TdCashflowInterest> pageCfComfirm(Map<String,Object> params);
	
	public boolean updateConfirmFlag(List<TdCashflowInterest> tdCashflowInterest);
	
	public List<TdCashflowInterest> queryPrintList(Map<String,Object> params);
	
	public List<TdCashflowCapital> getCapitalListByDate(Map<String,Object> params);

	public List<CashflowDailyInterest> listDailyInterestList(
			Map<String, Object> params);


}
