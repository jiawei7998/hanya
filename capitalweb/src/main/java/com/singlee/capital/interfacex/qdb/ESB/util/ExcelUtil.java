package com.singlee.capital.interfacex.qdb.ESB.util;

import com.singlee.capital.common.exception.RException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

@SuppressWarnings("deprecation")
public class ExcelUtil {

	private Workbook wb = null;

	private boolean isExcel2003 = false;

	/***
	 * 四位小数
	 */
	public static final String NUMBER_FMT_4 = "#,##0.0000";

	/***
	 * 两位小数
	 */
	public static final String NUMBER_FMT_2 = "#,##0.00";

	/***
	 * 1位小数
	 */
	public static final String NUMBER_FMT_1 = "#,##0.0";

	/***
	 * 不保留小数
	 */
	public static final String NUMBER_FMT_0 = "#,##0";

	/***
	 * yyyy/MM/dd
	 */
	public static final String DATE_FMT_1 = "yyyy/MM/dd";

	/***
	 * yyyy-MM-dd
	 */
	public static final String DATE_FMT_2 = "yyyy-MM-dd";

	/***
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final String DATE_FMT_3 = "yyyy-MM-dd HH:mm:ss";

	/***
	 * 2小数%
	 */
	public static final String PERCENT_2 = "0.00%";

	private Map<Integer, CellStyle> cellStyles = new HashMap<Integer, CellStyle>();

	private CellStyle defaultLeftStrCellStyle = null;
	private CellStyle defaultRightStrCellStyle = null;
	private CellStyle defaultCenterStrCellStyle = null;
	private CellStyle defaultAllCenterStrCellStyle = null;
	private CellStyle defaultNoBorderCellStyle = null;
	private CellStyle defaultNoBorderRightStrCellStyle = null;
	private CellStyle defaultNoBorderLeftStrCellStyle = null;
	private CellStyle centerStrWrapCellStyle = null;

	private CellStyle defaultLeftBorderCellStyle = null;

	private CellStyle defaultDate1CellStyle = null;
	private CellStyle defaultDate2CellStyle = null;
	private CellStyle defaultDate3CellStyle = null;

	private CellStyle defaultDouble0CellStyle = null;
	private CellStyle defaultDouble1CellStyle = null;
	private CellStyle defaultDouble2CellStyle = null;
	private CellStyle defaultDouble4CellStyle = null;

	private CellStyle titleCellStyle = null;

	private CellStyle defaultPercentCellStype_2 = null;

	private Font defaultFont = null;

	private Font defaultTitleFont = null;

	public final static int TYPE_2003 = 2003;

	public final static int TYPE_2007 = 2007;

	public final static int TYPE_2007_BIG_DATA = 20071;

	public ExcelUtil(String file) throws Exception {
		if (file.toUpperCase().endsWith(".XLS")) {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
			wb = new HSSFWorkbook(fs);
			setExcel2003(true);
		} else if (file.toUpperCase().endsWith(".XLSX")) {
			wb = new XSSFWorkbook(new FileInputStream(file));
			setExcel2003(false);
		} else {
			throw new Exception("无法识别Excel后缀名");
		}
	}

	public ExcelUtil(int type, int rowAccessWindowSize) throws Exception {
		if (TYPE_2003 == type) {
			wb = new HSSFWorkbook();
			setExcel2003(true);

		} else if (TYPE_2007 == type) {
			wb = new XSSFWorkbook();
			setExcel2003(false);

		} else if (TYPE_2007_BIG_DATA == type) {
			wb = new SXSSFWorkbook(rowAccessWindowSize);
			setExcel2003(false);

		} else {
			throw new Exception("无法识别Excel类型");
		}
	}

	/**
	 * 2017-7-27汪泽峰
	 * 
	 * @param type
	 * @param inputStream
	 * @throws Exception
	 */
	public ExcelUtil(String type, InputStream inputStream) throws Exception {
		if (type.toUpperCase().endsWith(".XLS")) {
			wb = new HSSFWorkbook(inputStream);
			setExcel2003(true);
		} else if (type.toUpperCase().endsWith(".XLSX")) {
			wb = new XSSFWorkbook(inputStream);
			setExcel2003(false);
		} else {
			throw new Exception("无法识别Excel后缀名");
		}
	}

	public void writeBoolean(int sheetIdx, int rowIdx, String columnIdx, CellStyle style, Boolean b) throws Exception {
		getCell(sheetIdx, rowIdx, transColumn(columnIdx), style).setCellValue(b);
	}

	public void writeDouble(int sheetIdx, int rowIdx, String columnIdx, CellStyle style, double d) throws Exception {
		getCell(sheetIdx, rowIdx, transColumn(columnIdx), style).setCellValue(d);
	}

	public void writeStr(int sheetIdx, int rowIdx, String columnIdx, CellStyle style, String str) throws Exception {
		getCell(sheetIdx, rowIdx, transColumn(columnIdx), style).setCellValue(str);
	}

	public void writeDate(int sheetIdx, int rowIdx, String columnIdx, CellStyle style, Date date) throws Exception {
		setValue(getCell(sheetIdx, rowIdx, transColumn(columnIdx), style), date);
	}

	public void writeBoolean(int sheetIdx, int rowIdx, int columnIdx, CellStyle style, Boolean b) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, style).setCellValue(b);
	}

	public void writeDouble(int sheetIdx, int rowIdx, int columnIdx, CellStyle style, double d) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, style).setCellValue(d);
	}

	public void writeStr(int sheetIdx, int rowIdx, int columnIdx, CellStyle style, String str) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, style).setCellValue(str);
	}

	public void writeDate(int sheetIdx, int rowIdx, int columnIdx, CellStyle style, Date date) throws Exception {
		setValue(getCell(sheetIdx, rowIdx, columnIdx, style), date);
	}

	/*
	 * -----------------------------------------------------------------------------
	 * --
	 * 
	 **/
	public void writeBoolean(int sheetIdx, int rowIdx, int columnIdx, Boolean b) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, getDefaultLeftStrCellStyle()).setCellValue(b);
	}

	public void writeDouble(int sheetIdx, int rowIdx, int columnIdx, double d) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, getDefaultDouble2CellStyle()).setCellValue(d);
	}

	public void writeStr(int sheetIdx, int rowIdx, int columnIdx, String str) throws Exception {
		getCell(sheetIdx, rowIdx, columnIdx, getDefaultLeftStrCellStyle()).setCellValue(str);
	}

	public void writeDate(int sheetIdx, int rowIdx, int columnIdx, Date date) throws Exception {
		setValue(getCell(sheetIdx, rowIdx, columnIdx, getDefaultDate2CellStyle()), date);
	}

	public void writeBoolean(int rowIdx, int columnIdx, Boolean b) throws Exception {
		getCell(0, rowIdx, columnIdx, getDefaultLeftStrCellStyle()).setCellValue(b);
	}

	public void writeDouble(int rowIdx, int columnIdx, double d) throws Exception {
		getCell(0, rowIdx, columnIdx, getDefaultDouble2CellStyle()).setCellValue(d);
	}

	public void writeStr(int rowIdx, int columnIdx, String str) throws Exception {
		getCell(0, rowIdx, columnIdx, getDefaultLeftStrCellStyle()).setCellValue(str);
	}

	public void writeDate(int rowIdx, int columnIdx, Date date) throws Exception {
		setValue(getCell(0, rowIdx, columnIdx, getDefaultDate2CellStyle()), date);
	}

	public void writeBoolean(int rowIdx, String columnIdx, Boolean b) throws Exception {
		getCell(0, rowIdx, transColumn(columnIdx), getDefaultLeftStrCellStyle()).setCellValue(b);
	}

	public void writeDouble(int rowIdx, String columnIdx, double d) throws Exception {
		getCell(0, rowIdx, transColumn(columnIdx), getDefaultDouble2CellStyle()).setCellValue(d);
	}

	public void writeStr(int rowIdx, String columnIdx, String str) throws Exception {
		getCell(0, rowIdx, transColumn(columnIdx), getDefaultLeftStrCellStyle()).setCellValue(str);
	}

	public void writeDate(int rowIdx, String columnIdx, Date date) throws Exception {
		setValue(getCell(0, rowIdx, transColumn(columnIdx), getDefaultDate2CellStyle()), date);
	}

	private void setValue(Cell cell, Object value) {
		if (value == null) {
			cell.setCellValue("");
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof String) {
			cell.setCellValue(String.valueOf(value));
		} else if (value instanceof Double) {
			cell.setCellValue(Double.valueOf(value.toString()));
		} else if (value instanceof Boolean) {
			cell.setCellValue(Boolean.valueOf(value.toString()));
		}

	}

	public Cell getCell(int sheetIdx, int rowIdx, int columnIdx, CellStyle style) {
		Sheet sheet = getSheetAt(sheetIdx);

		Row row = sheet.getRow(rowIdx);
		if (row == null) {
			row = sheet.createRow(rowIdx);
		}
		Cell cell = row.getCell(columnIdx);
		if (cell == null) {
			cell = row.createCell(columnIdx);
		}
		if (style != null) {
			cell.setCellStyle(style);
		} else {
			cell.setCellStyle(getDefaultLeftStrCellStyle());
		}
		return cell;
	}

	/**
	 * 将EXCEL的列转化为所在的列数 EXCEL记数从0开始 'A'-0 'AC'-28
	 * 
	 * @param cols 'A','AC','BC'
	 * @return
	 */
	public int transColumn(String str) throws Exception {
		// 此处判断是否是由A-Z字母组成的字符串，略（正在表达式片段）
		char[] letter = str.toUpperCase().toCharArray(); // 拆分字符串
		int reNum = 0;
		int power = 1; // 用于次方算值
		int times = 1; // 最高位需要加1
		int num = letter.length;// 得到字符串个数
		// 得到最后一个字母的尾数值
		reNum += charToNum(letter[num - 1]);
		// 得到除最后一个字母的所以值,多于两位才执行这个函数
		if (num >= 2) {
			for (int i = num - 1; i > 0; i--) {
				power = 1;// 致1，用于下一次循环使用次方计算
				for (int j = 0; j < i; j++) // 幂，j次方，应该有函数
				{
					power *= 26;
				}
				reNum += (power * (charToNum(letter[num - i - 1]) + times)); // 最高位需要加1，中间位数不需要加一
				times = 0;
			}
		}
		return reNum;
	}

	/**
	 * @param ch
	 * @return <summary> 输入字符得到相应的数字，这是最笨的方法，还可用ASIICK编码； </summary>
	 *         <param name="ch"></param> <returns></returns>
	 */
	// 例如 ：s :115 A: 65 方法调用返回数字50
	private int charToNum(char ch) {
		return ch - 'A';
	}

	/**
	 * EXCEL记数从0开始
	 * 
	 * @param rows '12'
	 * @return
	 */
	public int convertCharToIntegerRows(String rows) {
		return Integer.valueOf(rows) - 1;
	}

	public CellStyle getDateStyle(String dateFmt) {
		if (ExcelUtil.DATE_FMT_1.equals(dateFmt)) {
			return getDefaultDate1CellStyle();
		} else if (ExcelUtil.DATE_FMT_2.equals(dateFmt)) {
			return getDefaultDate2CellStyle();
		} else if (ExcelUtil.DATE_FMT_3.equals(dateFmt)) {
			return getDefaultDate3CellStyle();
		} else {
			return getDefaultDate2CellStyle();
		}
	}

	public CellStyle getDoubleStyle(String fmt) {
		if (ExcelUtil.NUMBER_FMT_0.equals(fmt)) {
			return getDefaultDouble0CellStyle();
		} else if (ExcelUtil.NUMBER_FMT_1.equals(fmt)) {
			return getDefaultDouble1CellStyle();
		} else if (ExcelUtil.NUMBER_FMT_2.equals(fmt)) {
			return getDefaultDouble2CellStyle();
		} else if (ExcelUtil.NUMBER_FMT_4.equals(fmt)) {
			return getDefaultDouble4CellStyle();
		} else {
			return getDefaultDouble2CellStyle();
		}
	}

	/**
	 * 
	 * 显示count位小数 *
	 */
	public CellStyle getDoubleStyleByCount(int count) {
		CellStyle cellStyle = cellStyles.get(count);
		if (cellStyle == null) {
			cellStyle = wb.createCellStyle();
			cellStyle.setAlignment(HorizontalAlignment.RIGHT);
			cellStyle.setBorderLeft(BorderStyle.THIN);
			cellStyle.setBorderRight(BorderStyle.THIN);
			cellStyle.setBorderTop(BorderStyle.THIN);
			cellStyle.setBorderBottom(BorderStyle.THIN);
			StringBuffer fmt = new StringBuffer("#,##0.");
			while (count > 0) {
				fmt.append("0");
				count--;
			}
			short df = wb.createDataFormat().getFormat(fmt.toString());
			cellStyle.setDataFormat(df);
			cellStyle.setFont(getDefaultFonts());
			cellStyles.put(count, cellStyle);
		}
		return cellStyle;
	}

	public CellStyle getStringStyle() {
		return getDefaultLeftStrCellStyle();
	}

	public CellStyle getStringStyle(HorizontalAlignment align) {
		switch (align) {
		case CENTER:
			return getDefaultCenterStrCellStyle();
		case LEFT:
			return getDefaultLeftStrCellStyle();
		case RIGHT:
			return getDefaultRightStrCellStyle();

		default:
			return getDefaultLeftStrCellStyle();
		}
	}

	/*
	 * ------------------------------------------------ double style
	 **/
	public CellStyle getDefaultLeftStrCellStyle() {
		if (defaultLeftStrCellStyle == null) {
			defaultLeftStrCellStyle = wb.createCellStyle();
			defaultLeftStrCellStyle.setAlignment(HorizontalAlignment.LEFT);
			defaultLeftStrCellStyle.setBorderLeft(BorderStyle.THIN);
			defaultLeftStrCellStyle.setBorderRight(BorderStyle.THIN);
			defaultLeftStrCellStyle.setBorderTop(BorderStyle.THIN);
			defaultLeftStrCellStyle.setBorderBottom(BorderStyle.THIN);
			defaultLeftStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultLeftStrCellStyle;
	}

	public CellStyle getDefaultRightStrCellStyle() {
		if (defaultRightStrCellStyle == null) {
			defaultRightStrCellStyle = wb.createCellStyle();
			defaultRightStrCellStyle.setAlignment(HorizontalAlignment.RIGHT);
			defaultRightStrCellStyle.setBorderLeft(BorderStyle.THIN);
			defaultRightStrCellStyle.setBorderRight(BorderStyle.THIN);
			defaultRightStrCellStyle.setBorderTop(BorderStyle.THIN);
			defaultRightStrCellStyle.setBorderBottom(BorderStyle.THIN);
			defaultRightStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultRightStrCellStyle;
	}

	public CellStyle getDefaultCenterStrCellStyle() {
		if (defaultCenterStrCellStyle == null) {
			defaultCenterStrCellStyle = wb.createCellStyle();
			defaultCenterStrCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultCenterStrCellStyle.setBorderLeft(BorderStyle.THIN);
			defaultCenterStrCellStyle.setBorderRight(BorderStyle.THIN);
			defaultCenterStrCellStyle.setBorderTop(BorderStyle.THIN);
			defaultCenterStrCellStyle.setBorderBottom(BorderStyle.THIN);
			defaultCenterStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultCenterStrCellStyle;
	}

	public CellStyle getDefaultAllCenterStrCellStyle() {
		if (defaultAllCenterStrCellStyle == null) {
			defaultAllCenterStrCellStyle = wb.createCellStyle();
			defaultAllCenterStrCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultAllCenterStrCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			defaultAllCenterStrCellStyle.setBorderLeft(BorderStyle.THIN);
			defaultAllCenterStrCellStyle.setBorderRight(BorderStyle.THIN);
			defaultAllCenterStrCellStyle.setBorderTop(BorderStyle.THIN);
			defaultAllCenterStrCellStyle.setBorderBottom(BorderStyle.THIN);
			defaultAllCenterStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultAllCenterStrCellStyle;
	}

	public CellStyle getCenterStrWrapCellStyle() {
		if (centerStrWrapCellStyle == null) {
			centerStrWrapCellStyle = wb.createCellStyle();
			centerStrWrapCellStyle.setAlignment(HorizontalAlignment.CENTER);
			centerStrWrapCellStyle.setBorderLeft(BorderStyle.THIN);
			centerStrWrapCellStyle.setBorderRight(BorderStyle.THIN);
			centerStrWrapCellStyle.setBorderTop(BorderStyle.THIN);
			centerStrWrapCellStyle.setBorderBottom(BorderStyle.THIN);
			centerStrWrapCellStyle.setWrapText(true);
			centerStrWrapCellStyle.setFont(getDefaultFonts());
		}
		return centerStrWrapCellStyle;
	}

	public CellStyle getTitleCellStyle() {
		if (titleCellStyle == null) {
			titleCellStyle = wb.createCellStyle();
			titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
			titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			titleCellStyle.setBorderLeft(BorderStyle.THIN);
			titleCellStyle.setBorderRight(BorderStyle.THIN);
			titleCellStyle.setBorderTop(BorderStyle.THIN);
			titleCellStyle.setBorderBottom(BorderStyle.THIN);
			titleCellStyle.setFont(getDefaultTitleFontFonts());
		}
		return titleCellStyle;
	}

	public CellStyle getDefaultNoBorderCellStyle() {
		if (defaultNoBorderCellStyle == null) {
			defaultNoBorderCellStyle = wb.createCellStyle();
			defaultNoBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultNoBorderCellStyle.setFont(getDefaultFonts());
		}
		return defaultNoBorderCellStyle;
	}

	public CellStyle getDefaultNoBorderRightStrCellStyle() {
		if (defaultNoBorderRightStrCellStyle == null) {
			defaultNoBorderRightStrCellStyle = wb.createCellStyle();
			defaultNoBorderRightStrCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultNoBorderRightStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultNoBorderRightStrCellStyle;
	}

	public CellStyle getDefaultNoBorderLeftStrCellStyle() {
		if (defaultNoBorderLeftStrCellStyle == null) {
			defaultNoBorderLeftStrCellStyle = wb.createCellStyle();
			defaultNoBorderLeftStrCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultNoBorderLeftStrCellStyle.setFont(getDefaultFonts());
		}
		return defaultNoBorderLeftStrCellStyle;
	}

	public CellStyle getDefaultLeftBorderCellStyle() {
		if (defaultLeftBorderCellStyle == null) {
			defaultLeftBorderCellStyle = wb.createCellStyle();
			defaultLeftBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultLeftBorderCellStyle.setBorderLeft(BorderStyle.THIN);
			defaultLeftBorderCellStyle.setFont(getDefaultFonts());
		}
		return defaultLeftBorderCellStyle;
	}

	/*
	 * ------------------------------------------------ double style
	 **/

	/*
	 * ------------------------------------------------ double style
	 **/

	public CellStyle getDefaultDouble0CellStyle() {
		if (defaultDouble0CellStyle == null) {
			defaultDouble0CellStyle = wb.createCellStyle();
			defaultDouble0CellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultDouble0CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDouble0CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDouble0CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDouble0CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.NUMBER_FMT_0);
			defaultDouble0CellStyle.setDataFormat(df);
			defaultDouble0CellStyle.setFont(getDefaultFonts());
		}
		return defaultDouble0CellStyle;
	}

	public CellStyle getDefaultDouble1CellStyle() {
		if (defaultDouble1CellStyle == null) {
			defaultDouble1CellStyle = wb.createCellStyle();
			defaultDouble1CellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultDouble1CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDouble1CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDouble1CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDouble1CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.NUMBER_FMT_1);
			defaultDouble1CellStyle.setDataFormat(df);
			defaultDouble1CellStyle.setFont(getDefaultFonts());
		}
		return defaultDouble1CellStyle;
	}

	public CellStyle getDefaultDouble2CellStyle() {
		if (defaultDouble2CellStyle == null) {
			defaultDouble2CellStyle = wb.createCellStyle();
			defaultDouble2CellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultDouble2CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDouble2CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDouble2CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDouble2CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.NUMBER_FMT_2);
			defaultDouble2CellStyle.setDataFormat(df);
			defaultDouble2CellStyle.setFont(getDefaultFonts());
		}
		return defaultDouble2CellStyle;
	}

	public CellStyle getDefaultDouble4CellStyle() {
		if (defaultDouble4CellStyle == null) {
			defaultDouble4CellStyle = wb.createCellStyle();
			defaultDouble4CellStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultDouble4CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDouble4CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDouble4CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDouble4CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.NUMBER_FMT_4);
			defaultDouble4CellStyle.setDataFormat(df);
			defaultDouble4CellStyle.setFont(getDefaultFonts());
		}
		return defaultDouble4CellStyle;
	}

	/*
	 * ------------------------------------------------ double style
	 **/

	/*
	 * ------------------------------------------------ date style
	 **/

	public CellStyle getDefaultDate1CellStyle() {
		if (defaultDate1CellStyle == null) {
			defaultDate1CellStyle = wb.createCellStyle();
			defaultDate1CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDate1CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDate1CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDate1CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.DATE_FMT_1);
			defaultDate1CellStyle.setDataFormat(df);
			defaultDate1CellStyle.setFont(getDefaultFonts());
		}
		return defaultDate1CellStyle;
	}

	public CellStyle getDefaultDate2CellStyle() {
		if (defaultDate2CellStyle == null) {
			defaultDate2CellStyle = wb.createCellStyle();
			defaultDate2CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDate2CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDate2CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDate2CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.DATE_FMT_2);
			defaultDate2CellStyle.setDataFormat(df);
			defaultDate2CellStyle.setFont(getDefaultFonts());
		}
		return defaultDate2CellStyle;
	}

	public CellStyle getDefaultDate3CellStyle() {
		if (defaultDate3CellStyle == null) {
			defaultDate3CellStyle = wb.createCellStyle();
			defaultDate3CellStyle.setBorderLeft(BorderStyle.THIN);
			defaultDate3CellStyle.setBorderRight(BorderStyle.THIN);
			defaultDate3CellStyle.setBorderTop(BorderStyle.THIN);
			defaultDate3CellStyle.setBorderBottom(BorderStyle.THIN);
			short df = wb.createDataFormat().getFormat(ExcelUtil.DATE_FMT_3);
			defaultDate3CellStyle.setDataFormat(df);
			defaultDate3CellStyle.setFont(getDefaultFonts());
		}
		return defaultDate3CellStyle;
	}

	/*
	 * ------------------------------------------------ date style
	 **/

	public CellStyle getDefaultPercentCellStype_2() {
		if (defaultPercentCellStype_2 == null) {
			defaultPercentCellStype_2 = wb.createCellStyle();
			defaultPercentCellStype_2.setBorderLeft(BorderStyle.THIN);
			defaultPercentCellStype_2.setBorderRight(BorderStyle.THIN);
			defaultPercentCellStype_2.setBorderTop(BorderStyle.THIN);
			defaultPercentCellStype_2.setBorderBottom(BorderStyle.THIN);
			defaultPercentCellStype_2.setDataFormat(HSSFDataFormat.getBuiltinFormat(PERCENT_2));
			defaultPercentCellStype_2.setFont(getDefaultFonts());
		}
		return defaultPercentCellStype_2;
	}

	public void writeExcel(String file) throws Exception {
		// 输出文件
		FileOutputStream fileOut = null;
		try {
			File f1 = new File(file);
			if (f1.exists() && !f1.renameTo(f1)) {
				JOptionPane.showMessageDialog(null, "文件" + f1.getName() + "已被占用,\n请先将该文件关闭,再执行导出操作", "消息提示",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			setForceRecalculation();

			fileOut = new FileOutputStream(file);
			wb.write(fileOut);
		} catch (Exception e) {
			throw e;
		} finally {
			if (fileOut != null) {
				fileOut.close();
			}
		}
	}

	/***
	 * 计算公式单元格
	 */
	public void setForceRecalculation() {
		if (isExcel2003()) {
			HSSFWorkbook wb2003 = (HSSFWorkbook) wb;
			for (int i = 0; i < wb2003.getNumberOfSheets(); i++) {
				wb2003.getSheetAt(i).setForceFormulaRecalculation(true);
			}
		}
	}

	/**
	 * 设置字体
	 * 
	 * @param wb
	 * @return
	 */
	public Font createFonts(String name, Short height, Short weight, Short color, Boolean italic) {
		// 创建Font对象
		Font font = wb.createFont();
		// 设置字体
		if (name != null) {
			font.setFontName(name);
		}
		// 字体大小
		if (height != null) {
			font.setFontHeight(height);
		}
		if (weight != null) {
			font.setFontHeight(weight);
		}
		// 着色
		if (color != null) {
			font.setColor(color);
		}
		// 斜体
		if (italic != null) {
			font.setItalic(italic);
		}
		return font;
	}

	/**
	 * 设置字体
	 * 
	 * @param wb
	 * @return
	 */
	public Font createFonts(Short height, Short weight) {
		// 创建Font对象
		Font font = wb.createFont();
		// 设置字体
		font.setFontName("宋体");
		// 字体大小
		if (height != null) {
			font.setFontHeight(height);
		}
		if (weight != null) {
			font.setFontHeight(weight);
		}
		return font;
	}

	/**
	 * 设置字体
	 * 
	 * @param wb
	 * @return
	 */
	public Font getDefaultFonts() {
		if (defaultFont == null) {
			// 创建Font对象
			defaultFont = wb.createFont();
			// 设置字体
			defaultFont.setFontName("Arial");
			// 字体大小
			defaultFont.setFontHeight((short) 200);
		}
		return defaultFont;
	}

	/**
	 * 设置字体
	 * 
	 * @param wb
	 * @return
	 */
	public Font getDefaultTitleFontFonts() {
		if (defaultTitleFont == null) {
			// 创建Font对象
			defaultTitleFont = wb.createFont();
			// 设置字体
			defaultTitleFont.setFontName("宋体");
			// 字体大小
			defaultTitleFont.setFontHeight((short) 200);
		}
		return defaultTitleFont;
	}

	public Sheet getSheetAt(int idx) {
		int i = 1;
		while (wb.getNumberOfSheets() < (idx + 1)) {
			wb.createSheet("AutoCreate_" + i++);
		}
		return wb.getSheetAt(idx);
	}

	public Workbook getWb() {
		return wb;
	}

	public void setWb(Workbook wb) {
		this.wb = wb;
	}

	public boolean isExcel2003() {
		return isExcel2003;
	}

	public void setExcel2003(boolean isExcel2003) {
		this.isExcel2003 = isExcel2003;
	}

	/***************************************************
	 * copy sheet Start
	 *****************************************************/

	/****
	 * 用于存放HSSFCellStyle样式,用于复制sheet
	 */
	private Map<String, HSSFCellStyle> hssfCellStyleMap = new HashMap<String, HSSFCellStyle>();

	/***
	 * 从其他workBook中复制一个文件到目标workBook
	 * 
	 * @param sourceSheet
	 * @param sourceWork
	 * @param targetSheetName
	 * @return
	 * @throws Exception
	 */
	public int copySheetToTagetSheetBy2003(String targetSheetName, HSSFSheet sourceSheet, HSSFWorkbook sourceWork)
			throws Exception {
		if (!isExcel2003()) {
			throw new Exception("EXCEL 模板版本必须是EXCEL2003版本");
		}
		Sheet targetSheet = this.wb.getSheet(targetSheetName);
		if (targetSheet == null) {
			targetSheet = this.wb.createSheet(targetSheetName);
		}
		copySheet((HSSFSheet) targetSheet, sourceSheet, (HSSFWorkbook) this.wb, sourceWork, true);

		int index = this.wb.getNumberOfSheets() - 1;
		for (int i = index; i >= 0; i--) {
			if (targetSheetName.equals(this.wb.getSheetAt(i).getSheetName())) {
				return i;
			}
		}
		return index;
	}

	/**
	 * 只支持 EXCEL XLS格式 功能：拷贝sheet 实际调用 copySheet(targetSheet, sourceSheet,
	 * targetWork, sourceWork, true)
	 * 
	 * @param targetSheet
	 * @param sourceSheet
	 * @param targetWork
	 * @param sourceWork
	 */
	public void copySheet(HSSFSheet targetSheet, HSSFSheet sourceSheet, HSSFWorkbook targetWork,
			HSSFWorkbook sourceWork) throws Exception {
		if (targetSheet == null || sourceSheet == null || targetWork == null || sourceWork == null) {
			throw new IllegalArgumentException(
					"调用copySheet()方法时，targetSheet、sourceSheet、targetWork、sourceWork都不能为空，故抛出该异常！");
		}
		copySheet(targetSheet, sourceSheet, targetWork, sourceWork, true);
	}

	/**
	 * 只支持 EXCEL XLS格式 功能：拷贝sheet
	 * 
	 * @param targetSheet
	 * @param sourceSheet
	 * @param targetWork
	 * @param sourceWork
	 * @param copyStyle   boolean 是否拷贝样式
	 */
	public void copySheet(HSSFSheet targetSheet, HSSFSheet sourceSheet, HSSFWorkbook targetWork,
			HSSFWorkbook sourceWork, boolean copyStyle) throws Exception {
		if (targetSheet == null || sourceSheet == null || targetWork == null || sourceWork == null) {
			throw new IllegalArgumentException(
					"调用copySheet()方法时，targetSheet、sourceSheet、targetWork、sourceWork都不能为空，故抛出该异常！");
		}

		// 复制源表中的行
		int maxColumnNum = 0;

		HSSFPatriarch patriarch = targetSheet.createDrawingPatriarch(); // 用于复制注释
		for (int i = sourceSheet.getFirstRowNum(); i <= sourceSheet.getLastRowNum(); i++) {
			HSSFRow sourceRow = sourceSheet.getRow(i);
			HSSFRow targetRow = targetSheet.createRow(i);

			if (sourceRow != null) {
				copyRow(targetRow, sourceRow, targetWork, sourceWork, patriarch, copyStyle);
				if (sourceRow.getLastCellNum() > maxColumnNum) {
					maxColumnNum = sourceRow.getLastCellNum();
				}
			}
		}

		// 复制源表中的合并单元格
		mergerRegion(targetSheet, sourceSheet);

		// 设置目标sheet的列宽
		for (int i = 0; i <= maxColumnNum; i++) {
			targetSheet.setColumnWidth(i, sourceSheet.getColumnWidth(i));
		}
	}

	/**
	 * 功能：拷贝row
	 * 
	 * @param targetRow
	 * @param sourceRow
	 * @param targetWork
	 * @param sourceWork
	 * @param targetPatriarch
	 */
	public void copyRow(HSSFRow targetRow, HSSFRow sourceRow, HSSFWorkbook targetWork, HSSFWorkbook sourceWork,
			HSSFPatriarch targetPatriarch, boolean copyStyle) throws Exception {

		if (targetRow == null || sourceRow == null || targetWork == null || sourceWork == null
				|| targetPatriarch == null) {
			throw new IllegalArgumentException(
					"调用copyRow()方法时，targetRow、sourceRow、targetWork、sourceWork、targetPatriarch都不能为空，故抛出该异常！");
		}

		// 设置行高
		targetRow.setHeight(sourceRow.getHeight());

		for (int i = sourceRow.getFirstCellNum(); i <= sourceRow.getLastCellNum(); i++) {
			HSSFCell sourceCell = sourceRow.getCell(i);
			HSSFCell targetCell = targetRow.getCell(i);

			if (sourceCell != null) {
				if (targetCell == null) {
					targetCell = targetRow.createCell(i);
				}

				// 拷贝单元格，包括内容和样式
				copyCell(targetCell, sourceCell, targetWork, sourceWork, copyStyle);

				// 拷贝单元格注释
				copyComment(targetCell, sourceCell, targetPatriarch);
			}
		}
	}

	/**
	 * 功能：拷贝cell，依据styleMap是否为空判断是否拷贝单元格样式
	 * 
	 * @param targetCell 不能为空
	 * @param sourceCell 不能为空
	 * @param targetWork 不能为空
	 * @param sourceWork 不能为空
	 * @param copyStyle  不能为空
	 */
	public void copyCell(HSSFCell targetCell, HSSFCell sourceCell, HSSFWorkbook targetWork, HSSFWorkbook sourceWork,
			boolean copyStyle) {
		if (targetCell == null || sourceCell == null || targetWork == null || sourceWork == null) {
			throw new IllegalArgumentException(
					"调用copyCell()方法时，targetCell、sourceCell、targetWork、sourceWork都不能为空，故抛出该异常！");
		}

		// 处理单元格样式
		if (copyStyle) {
			if (targetWork == sourceWork) {
				targetCell.setCellStyle(sourceCell.getCellStyle());
			} else {
				String styleCode = getCellStyleCode(sourceCell.getCellStyle(), sourceWork);
				HSSFCellStyle targetCellStyle = hssfCellStyleMap.get(styleCode);
				if (targetCellStyle == null) {
					targetCellStyle = targetWork.createCellStyle();
					targetCellStyle.setBorderBottom(sourceCell.getCellStyle().getBorderBottomEnum());
					targetCellStyle.setBorderTop(sourceCell.getCellStyle().getBorderTopEnum());
					targetCellStyle.setBorderLeft(sourceCell.getCellStyle().getBorderLeftEnum());
					targetCellStyle.setBorderRight(sourceCell.getCellStyle().getBorderRightEnum());
					targetCellStyle.setAlignment(sourceCell.getCellStyle().getAlignmentEnum());
					targetCellStyle.setVerticalAlignment(sourceCell.getCellStyle().getVerticalAlignmentEnum());
					targetCellStyle.setWrapText(sourceCell.getCellStyle().getWrapText());
					targetCellStyle.setFillBackgroundColor(sourceCell.getCellStyle().getFillBackgroundColor());
					targetCellStyle.setFillForegroundColor(sourceCell.getCellStyle().getFillForegroundColor());

					HSSFFont sourceFont = sourceCell.getCellStyle().getFont(sourceWork);

					HSSFFont targetFront = targetWork.createFont();
					targetFront.setFontHeightInPoints(sourceFont.getFontHeightInPoints());
					targetFront.setColor(sourceFont.getColor());
					targetFront.setFontName(sourceFont.getFontName());
					targetFront.setFontHeight(sourceFont.getFontHeight());

					targetCellStyle.setFont(targetFront);
					hssfCellStyleMap.put(styleCode, targetCellStyle);
				}
				targetCell.setCellStyle(targetCellStyle);
			}
		}

		// 处理单元格内容
		switch (sourceCell.getCellType()) {
		case HSSFCell.CELL_TYPE_STRING:
			targetCell.setCellValue(sourceCell.getRichStringCellValue());
			break;
		case HSSFCell.CELL_TYPE_NUMERIC:
			targetCell.setCellValue(sourceCell.getNumericCellValue());
			break;
		case HSSFCell.CELL_TYPE_BLANK:
			targetCell.setCellType(HSSFCell.CELL_TYPE_BLANK);
			break;
		case HSSFCell.CELL_TYPE_BOOLEAN:
			targetCell.setCellValue(sourceCell.getBooleanCellValue());
			break;
		case HSSFCell.CELL_TYPE_ERROR:
			targetCell.setCellErrorValue(sourceCell.getErrorCellValue());
			break;
		case HSSFCell.CELL_TYPE_FORMULA:
			targetCell.setCellFormula(sourceCell.getCellFormula());
			break;
		default:
			break;
		}
	}

	/**
	 * 功能：拷贝comment
	 * 
	 * @param targetCell
	 * @param sourceCell
	 * @param targetPatriarch
	 */
	public static void copyComment(HSSFCell targetCell, HSSFCell sourceCell, HSSFPatriarch targetPatriarch)
			throws Exception {
		if (targetCell == null || sourceCell == null || targetPatriarch == null) {
			throw new IllegalArgumentException(
					"调用copyCommentr()方法时，targetCell、sourceCell、targetPatriarch都不能为空，故抛出该异常！");
		}

		// 处理单元格注释
		HSSFComment comment = sourceCell.getCellComment();
		if (comment != null) {
			HSSFComment newComment = targetPatriarch.createComment(new HSSFClientAnchor());
			newComment.setAuthor(comment.getAuthor());
			newComment.setColumn(comment.getColumn());
			newComment.setFillColor(comment.getFillColor());
			newComment.setHorizontalAlignment(comment.getHorizontalAlignment());
			newComment.setLineStyle(comment.getLineStyle());
			newComment.setLineStyleColor(comment.getLineStyleColor());
			newComment.setLineWidth(comment.getLineWidth());
			newComment.setMarginBottom(comment.getMarginBottom());
			newComment.setMarginLeft(comment.getMarginLeft());
			newComment.setMarginTop(comment.getMarginTop());
			newComment.setMarginRight(comment.getMarginRight());
			newComment.setNoFill(comment.isNoFill());
			newComment.setRow(comment.getRow());
			newComment.setShapeType(comment.getShapeType());
			newComment.setString(comment.getString());
			newComment.setVerticalAlignment(comment.getVerticalAlignment());
			newComment.setVisible(comment.isVisible());
			targetCell.setCellComment(newComment);
		}
	}

	/**
	 * 功能：复制原有sheet的合并单元格到新创建的sheet
	 * 
	 * @param sheetCreat
	 * @param sourceSheet
	 */
	public static void mergerRegion(HSSFSheet targetSheet, HSSFSheet sourceSheet) throws Exception {
		if (targetSheet == null || sourceSheet == null) {
			throw new IllegalArgumentException("调用mergerRegion()方法时，targetSheet或者sourceSheet不能为空，故抛出该异常！");
		}
		for (int i = 0; i < sourceSheet.getNumMergedRegions(); i++) {
			CellRangeAddress oldRange = sourceSheet.getMergedRegion(i);
			CellRangeAddress newRange = new CellRangeAddress(oldRange.getFirstRow(), oldRange.getLastRow(),
					oldRange.getFirstColumn(), oldRange.getLastColumn());
			targetSheet.addMergedRegion(newRange);
		}
	}

	/**
	 * 功能：重新定义HSSFColor.YELLOW的色值
	 * 
	 * @param workbook
	 * @return
	 */
	public static HSSFColor setMForeColor(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();
		HSSFColor hssfColor = null;
		byte[] rgb = { (byte) 221, (byte) 241, (byte) 255 };
		try {
			hssfColor = palette.findColor(rgb[0], rgb[1], rgb[2]);
			if (hssfColor == null) {
				palette.setColorAtIndex(HSSFColor.YELLOW.index, rgb[0], rgb[1], rgb[2]);
				hssfColor = palette.getColor(HSSFColor.YELLOW.index);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new RException(e);
		}
		return hssfColor;
	}

	/**
	 * 功能：重新定义HSSFColor.PINK的色值
	 * 
	 * @param workbook
	 * @return
	 */
	public static HSSFColor setMBorderColor(HSSFWorkbook workbook) {
		HSSFPalette palette = workbook.getCustomPalette();
		HSSFColor hssfColor = null;
		byte[] rgb = { (byte) 0, (byte) 128, (byte) 192 };
		try {
			hssfColor = palette.findColor(rgb[0], rgb[1], rgb[2]);
			if (hssfColor == null) {
				palette.setColorAtIndex(HSSFColor.PINK.index, rgb[0], rgb[1], rgb[2]);
				hssfColor = palette.getColor(HSSFColor.PINK.index);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new RException(e);
		}
		return hssfColor;
	}

	public static String getCellStyleCode(HSSFCellStyle c, HSSFWorkbook sourceWork) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(c.getAlignment());
		sbf.append(c.getBorderLeft());
		sbf.append(c.getBorderRight());
		sbf.append(c.getBorderTop());
		sbf.append(c.getBorderBottom());
		sbf.append(c.getDataFormat());
		sbf.append(c.getDataFormatString());
		sbf.append(c.getFillBackgroundColor());
		sbf.append(c.getFillForegroundColor());
		sbf.append(c.getFillPattern());
		sbf.append(c.getWrapText());
		sbf.append(c.getVerticalAlignment());
		Font font = c.getFont(sourceWork);
		if (font != null) {
			sbf.append(font.getFontName());
			sbf.append(font.getFontHeightInPoints());
			sbf.append(font.getColor());
			sbf.append(font.getFontHeight());
		}
		return sbf.toString();
	}

	/***************************************************
	 * copy sheet end
	 *****************************************************/

	/******
	 * 合并单元格
	 * 
	 * @param firstRow
	 * @param lastRow
	 * @param firstCol
	 * @param lastCol
	 * @param s
	 * @param wb
	 */
	public static void cellRange(int firstRow, int lastRow, int firstCol, int lastCol, Sheet s, Workbook wb) {
		CellRangeAddress address = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		s.addMergedRegion(address);
		setBorder(address, s, wb);
	}

	private static void setBorder(CellRangeAddress address, Sheet s, Workbook wb) {
//		RegionUtil.setBorderBottom(BorderStyle.THIN, address, s, wb);
//		RegionUtil.setBorderLeft(BorderStyle.THIN, address, s, wb);
//		RegionUtil.setBorderRight(BorderStyle.THIN, address, s, wb);
//		RegionUtil.setBorderTop(BorderStyle.THIN, address, s, wb);
	}

	/**
	 * 用于将Excel表格中列号字母转成列索引，从1对应A开始
	 * 
	 * @param column 列号
	 * @return 列索引
	 */
	public static int columnToIndex(String column) throws Exception {
		if (!column.matches("[A-Z]+")) {
			throw new Exception("Invalid parameter");
		}
		int index = 0;
		char[] chars = column.toUpperCase().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			index += ((int) chars[i] - (int) 'A' + 1) * (int) Math.pow(26, chars.length - i - 1);
		}
		return index;
	}

	/**
	 * 用于将excel表格中列索引转成列号字母，从A对应1开始
	 * 
	 * @param index 列索引
	 * @return 列号
	 */
	public static String indexToColumn(int index) throws Exception {
		if (index <= 0) {
			throw new Exception("Invalid parameter");
		}
		index--;
		String column = "";
		do {
			if (column.length() > 0) {
				index--;
			}
			column = ((char) (index % 26 + (int) 'A')) + column;
			index = (int) ((index - index % 26) / 26);
		} while (index > 0);
		return column;
	}

	/**
	 * 解析字符串 |分割
	 * 
	 * @param str
	 * @return
	 */
	public List<String> splitStr(String str) {
		List<String> ls = new ArrayList<String>();
		String[] bt = str.split("");
		StringBuffer sb = new StringBuffer();
		for (int j = 0; j < bt.length; j++) {
			if ("|".equals(bt[j]) || j == (bt.length - 1)) {
				if (j == (bt.length - 1)) {
					sb.append("|".equals(bt[j]) ? "" : bt[j]);
				}
				ls.add(sb.toString() == null ? "NULL" : sb.toString().length() == 0 ? "NULL" : sb.toString());
				sb = new StringBuffer();
			} else {
				sb.append(bt[j]);
			}
		}
		return ls;
	}

}
