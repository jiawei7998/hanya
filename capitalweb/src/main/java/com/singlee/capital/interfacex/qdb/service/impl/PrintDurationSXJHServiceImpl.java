package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.tpos.mapper.DurationForTposProductMainMapper;
import com.singlee.capital.tpos.vo.DurationForTposProductMain;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdTransforScheduleIntMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforScheduleInt;
@Service("durationSXJHServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintDurationSXJHServiceImpl implements AssemblyDataService{
	@Autowired
	private TdTransforScheduleIntMapper tdTransforScheduleIntMapper;
	@Autowired
	private FlowController flowController; 

	@Autowired
	private TtInstitutionMapper mapper;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	DurationForTposProductMainMapper durationForTposProductMainMapper;
	
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			NumberFormat n = NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			if(id!=null&&!"".equals(id)){
				TdTransforScheduleInt sint=tdTransforScheduleIntMapper.getTransforScheduleIntById(id); 
				if(sint!=null){
					String refno=sint.getRefNo();
					map.put("dealNo", refno);
					TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
					//实时查询持仓本金
//					Map<String,Object> mp = new HashMap<String, Object>();
//					mp.put("i_code", refno);
//					List<DurationForTposProductMain> list =durationForTposProductMainMapper.getDurationForTposProductMain(mp);
					map.put("dealNo",td.getDealNo());
					map.put("contractRate", df.format(100*td.getContractRate()));
					map.put("vDate", td.getvDate());
					map.put("mDate",td.getmDate());
					String amtFre=td.getAmtFre();
					if("0".endsWith(amtFre)){
						map.put("amtFre", "到期一次性");
					}else if("1".endsWith(amtFre)){
						map.put("amtFre", "到期一次性");
					}else if("4".endsWith(amtFre)){
						map.put("amtFre", "季");
					}else if("12".endsWith(amtFre)){
						map.put("amtFre", "月");
					}else if("2".endsWith(amtFre)){
						map.put("amtFre", "半年");
					}else if("-1".endsWith(amtFre)){
						map.put("amtFre", "无");
					}
//					if(list!=null && list.size()>0) {
//						
//					}
					map.put("remainAmt",sint.getRemainAmt());
					map.put("effectDate",sint.getEffectDate());
					map.put("changeReason",sint.getChangeReason());
					String cno=td.getcNo();
					CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
					map.put("party_name", cp.getParty_name());
					Integer pro=td.getPrdNo();
					if(pro!=null&&!"".equals(pro)){
						TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
						map.put("productName", p.getPrdName());
					}
					//取原交易的经办人
					String Sponsor2=td.getSponsor();
					TaUser u=taUserMapper.selectUser(Sponsor2);
					map.put("sponsor",u.getUserName());
					
					
					//取存续期的经办人
					String Sponsor=sint.getSponsor();
					TaUser u2=taUserMapper.selectUser(Sponsor);
					map.put("sponsor2",u2.getUserName());
					String inst=u.getInstId();
					
					TtInstitution institution = new TtInstitution();
					if(inst!=null&&!"".equals(inst)){
						institution.setInstId(inst);
						institution = mapper.selectByPrimaryKey(institution);
						map.put("Inst",institution.getInstName());
					}	
					String rateType=td.getRateType();
					if("1".endsWith(rateType)){
						map.put("rateType", "固息");
					}else if("2".endsWith(rateType)){
						map.put("rateType", "浮息");
					}
					map.put("amt", n.format(td.getAmt()));
					map.put("basis", td.getBasis());
					map.put("mInt", n.format(td.getmInt()));
					map.put("mAmt", n.format(td.getmAmt()));
					map.put("SysDate", dayendDateService.getSettlementDate());
				}
			}
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}

				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

}
