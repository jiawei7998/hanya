package com.singlee.capital.interfacex.qdb.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.interfacex.qdb.mapper.UserSynchMapper;
import com.singlee.capital.interfacex.qdb.service.UserSynchService;
import com.singlee.capital.system.model.TaUser;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserSynchServiceImpl implements UserSynchService{

	@Autowired
	private UserSynchMapper userMapper;
	
	@Override
	public void deleteUserESB() {
		userMapper.deleteUserESB();
		
	}

	@Override
	public void insertUsers(List<TaUser> users) {
		userMapper.insertUsers(users);
		
	}

	@Override
	public Map<String, String> changeTemp(Map<String, String> map) {
		return userMapper.changeTemp(map);
	}

	@Override
	public void insertUserIdinstID(List<TaUser> users) {
		
		userMapper.insertUserIdinstID(users);
	}

}
