package com.singlee.capital.interfacex.qdb.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.model.TdTradInvMain;
import com.singlee.capital.interfacex.qdb.model.TdTradeInvDetail;

/**
 * 
 * @author singlee
 *
 */
public interface TdTradInvMainService {
	/**
	 * 查询委外业务，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdTradInvMain> selectTdTradInvMainPage(Map<String, Object> map);
	/**
	 * 查询委外业务明细，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdTradeInvDetail> selectTdTradInvDetailPage(Map<String, Object> map);

	
	/**
	 * 手工导入数据
	 * @param type
	 * @param is
	 * @return
	 */
	public String setTdTradInvMainByExcel(String name, InputStream is,String tradeid);
	
	public void deleteTdtradeInv(Map<String, Object> map);
	/**
	 * 委外业务基础资产下载
	 * @param map
	 * @return
	 */
	ExcelUtil downloadExcel(HashMap<String, Object> map);
}
