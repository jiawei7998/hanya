package com.singlee.capital.interfacex.qdb.pojo;

import java.io.Serializable;

public class FBZ001 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String source;//发起服务名
	private String destination;//目标服务名
	private String bizDate;//业务日期
	private String refNo;//业务参考号
	private String seqNo;//业务参考号
	private String respCde;//返回码
	private String respMsg;//返回信息
	private String tmStamp;//时间戳
	private String mfCustomerId;//核心客户号
	private String customerType;//客户类型
	private String certType;//证件类型
	private String certId;//证件号码
	private String startNum;
	private String showNum;
	private String customerName;//客户名称
	private String alikeType;//同业类型
	private String alikeTypeDown;//同业子类型
	private String entHoldingType;//企业控股类型
	private String fiCilityCredit;//社会信用代码证号
	private String area;//客户所属地区
	private String sCope;//企业规模
	private String regCapital;//注册资本
	private String rcCurrency;//注册资本币种
	private String inGrade;//行内评级
	private String outGrade;//公开评级
	private String industryType;//国民经济行业分类
	private String singField1;
	private String singField2;
	private String singField3;
	private String singField4;
	private String singField5;
	
	private String indusCatBig; //行业大类
	private String indusCatMid; //行业中类
	private String indusCatSmall; //行业小类
	
	public String getIndusCatBig() {
		return indusCatBig;
	}
	public void setIndusCatBig(String indusCatBig) {
		this.indusCatBig = indusCatBig;
	}
	public String getIndusCatMid() {
		return indusCatMid;
	}
	public void setIndusCatMid(String indusCatMid) {
		this.indusCatMid = indusCatMid;
	}
	public String getIndusCatSmall() {
		return indusCatSmall;
	}
	public void setIndusCatSmall(String indusCatSmall) {
		this.indusCatSmall = indusCatSmall;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getBizDate() {
		return bizDate;
	}
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public String getRespCde() {
		return respCde;
	}
	public void setRespCde(String respCde) {
		this.respCde = respCde;
	}
	public String getRespMsg() {
		return respMsg;
	}
	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}
	public String getTmStamp() {
		return tmStamp;
	}
	public void setTmStamp(String tmStamp) {
		this.tmStamp = tmStamp;
	}
	public String getMfCustomerId() {
		return mfCustomerId;
	}
	public void setMfCustomerId(String mfCustomerId) {
		this.mfCustomerId = mfCustomerId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getCertId() {
		return certId;
	}
	public void setCertId(String certId) {
		this.certId = certId;
	}
	public String getStartNum() {
		return startNum;
	}
	public void setStartNum(String startNum) {
		this.startNum = startNum;
	}
	public String getShowNum() {
		return showNum;
	}
	public void setShowNum(String showNum) {
		this.showNum = showNum;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAlikeType() {
		return alikeType;
	}
	public void setAlikeType(String alikeType) {
		this.alikeType = alikeType;
	}
	public String getAlikeTypeDown() {
		return alikeTypeDown;
	}
	public void setAlikeTypeDown(String alikeTypeDown) {
		this.alikeTypeDown = alikeTypeDown;
	}
	public String getEntHoldingType() {
		return entHoldingType;
	}
	public void setEntHoldingType(String entHoldingType) {
		this.entHoldingType = entHoldingType;
	}
	public String getFiCilityCredit() {
		return fiCilityCredit;
	}
	public void setFiCilityCredit(String fiCilityCredit) {
		this.fiCilityCredit = fiCilityCredit;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getsCope() {
		return sCope;
	}
	public void setsCope(String sCope) {
		this.sCope = sCope;
	}
	public String getRegCapital() {
		return regCapital;
	}
	public void setRegCapital(String regCapital) {
		this.regCapital = regCapital;
	}
	public String getRcCurrency() {
		return rcCurrency;
	}
	public void setRcCurrency(String rcCurrency) {
		this.rcCurrency = rcCurrency;
	}
	public String getInGrade() {
		return inGrade;
	}
	public void setInGrade(String inGrade) {
		this.inGrade = inGrade;
	}
	public String getOutGrade() {
		return outGrade;
	}
	public void setOutGrade(String outGrade) {
		this.outGrade = outGrade;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getSingField1() {
		return singField1;
	}
	public void setSingField1(String singField1) {
		this.singField1 = singField1;
	}
	public String getSingField2() {
		return singField2;
	}
	public void setSingField2(String singField2) {
		this.singField2 = singField2;
	}
	public String getSingField3() {
		return singField3;
	}
	public void setSingField3(String singField3) {
		this.singField3 = singField3;
	}
	public String getSingField4() {
		return singField4;
	}
	public void setSingField4(String singField4) {
		this.singField4 = singField4;
	}
	public String getSingField5() {
		return singField5;
	}
	public void setSingField5(String singField5) {
		this.singField5 = singField5;
	}
	
}
