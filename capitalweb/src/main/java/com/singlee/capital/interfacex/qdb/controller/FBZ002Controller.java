package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;
import com.singlee.capital.interfacex.qdb.pojo.FBZ002;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.controller.CommonController;
//查询客户额度信息
@Controller
@RequestMapping("fbz002")
public class FBZ002Controller extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private EsbClientService esbClientService;
	
	@RequestMapping("getFbz002")
	@ResponseBody
	public RetMsg<FBZ002> getResp(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ002 fbz) throws Exception{
		RetMsg<FBZ002> ret = new RetMsg<FBZ002>(RetMsgHelper.codeOk,"成功");
		ret.setObj(fbz);
		
		fbz.setMfCustomerId(String.valueOf(map.get("mfCustomerId")));
		String ccy = String.valueOf(map.get("ccy"));
		BigDecimal amt = new BigDecimal(String.valueOf(map.get("amt")));
		List<Object> fbzs = sendReq(request,fbz);
		FBZ002 temp = null;
		String limitId = null;
		BigDecimal tempAmt = null;
		if(fbzs!=null)
		{
			if(fbzs.size() == 0)
			{
				ret.setObj(null);
				ret.setDesc("信管系统未查询到客户["+fbz.getMfCustomerId()+"]的额度信息!");
				return ret;
			}
			if(fbzs.size() == 1)
			{
				temp = (FBZ002)fbzs.get(0);
				if(!"15700000000000".equals(temp.getRespCde()))
				{
					ret.setObj(null);
					ret.setDesc("信管系统未查询到客户["+fbz.getMfCustomerId()+"]的额度信息!");
					return ret;
				}
			}
    		for (int i = 0;i<fbzs.size();i++) 
    		{
    			temp = (FBZ002)fbzs.get(i);
    			temp.setCurrency(PropertiesUtil.parseFile("currency.properties").getString(((FBZ002)fbzs.get(i)).getCurrency()));
    			
    			if(ccy != null && ccy.equals(temp.getCurrency()))
    			{
    				//起息日 到期日判断 2014/11/11 2014/11/11
    				String vdate = String.valueOf(map.get("vdate"));
    				if(vdate != null){
    					vdate = vdate.replaceAll("-", "/");
    					if(vdate.compareTo(temp.getStartDat()) < 0){
    						ret.setObj(null);
        					ret.setDesc("交易起息日不能小于额度起始日,该客户额度起始日为["+temp.getStartDat()+"]!");
        					return ret;
    					}
    				}
    				
    				String mdate = String.valueOf(map.get("mdate"));
    				if(mdate != null){
    					mdate = mdate.replaceAll("-", "/");
    					if(mdate.compareTo(temp.getEndDat()) > 0){
    						ret.setObj(null);
        					ret.setDesc("交易到息日不能大于额度到期日,该客户额度到期日为["+temp.getEndDat()+"]!");
        					return ret;
    					}
    				}
    				
    				tempAmt = temp.getLimitBalance() == null ? new BigDecimal(0) : new BigDecimal(temp.getLimitBalance().replaceAll(",", ""));
    				//如果交易金额比额度可用金额大,则无法占用
    				if(amt.compareTo(tempAmt) > 0)
    				{
    					ret.setObj(null);
    					ret.setDesc("交易金额超过信管系统授信余额["+temp.getLimitBalance()+"]!");
    					return ret;
    				}else{
    					limitId = temp.getLmtID();
    					break;
    				}
    				
    			}
			}
    		if(limitId != null){
    			fbz.setLmtID(limitId);
    			return ret;
    		}else{
    			ret.setObj(null);
    			ret.setDesc("信管系统未查询到客户["+fbz.getMfCustomerId()+"]的额度相关信息");
    			return ret;
    		}
    	}else{
    		ret.setObj(null);
    		ret.setDesc("信管系统未查询到客户["+fbz.getMfCustomerId()+"]的额度相关信息");
			return ret;
    	}
	}
	
	public List<Object> sendReq(HttpServletRequest request,FBZ002 fbz) throws Exception{
		List<Object> fbzs=new ArrayList<Object>();
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ002.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg = parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ002返回报文"+xmlDocAccept.asXML());
		        	fbzs = XmlUtilFactory.getBeansByXml(FBZ002.class, xmlDocAccept.asXML(),"circularField");		        	
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return fbzs;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return null;
		}
		return null;
	}

	public String parseXml(HttpServletRequest request,FBZ002 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		svc.put("bizDate",getDate());
		svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
		svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
		svc.put("tmStamp", getDateTime());
		hashmap.put("appHdr", svc);
		body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
		body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
		body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
		body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
		hashmap.put("appBody", body);
		return XmlUtilFactory.getXml(request.getSession().getServletContext().getRealPath("/")+ "/WEB-INF/classes/schema/FBZC0002.xml", hashmap);
	}
	public String getDateTime(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
}
