package com.singlee.capital.interfacex.quartzjob;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.credit.model.TcCredit;
import com.singlee.capital.credit.service.TcCreditService;
import com.singlee.capital.interfacex.service.GtpFileService;

public class CrmsProductSyncQuartzJob implements CronRunnable{

	/**
	 * 产品同步接口（企信-融贷通）	
	 * IFBM取CRMS文件	
	 * 产品同步接口	日终轮询获取	
		1、GTP配置IFBM与CRMS；
		2、文件命名格式：crms_product_yyyyMMdd.txt；通配符：crms_*.txt
		3、同业目录定义为：/share_ifbm/gtp/crms/recv
		4、分隔符号：“|”"
	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	@Autowired
	private TcCreditService tcCreditService  = SpringContextHolder.getBean("tcCreditService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("=======================华丽丽的分割线========================");
		List<TcCredit> tcCredits = gtpFileService.GtpIfbmCrmsProductRead();
		if(tcCredits.size() > 0){
			
			tcCreditService.batchImportTcCredit(tcCredits);
		}
		System.out.println("=======================华丽丽的分割线========================");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
