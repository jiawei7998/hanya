package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.capital.interfacex.qdb.ESB.model.BaseModel;
import com.singlee.capital.interfacex.qdb.ESB.model.SM5309ReqtBean;



public class SM5309ReplaceElementContext {

	
	@SuppressWarnings("unchecked")
	public static List<SM5309ReqtBean> praseEsbXmlPackageDescription(byte[] responseXml,HashMap<String, Object> svcHdr,HashMap<String,Object> appHdr,HashMap<String, Object> appBody) throws Exception
	{
		ByteArrayInputStream byteArrayInputStream = null;
		List<SM5309ReqtBean> appBodys = null;
		try{
			SAXReader reader = new SAXReader();
			//reader.setEncoding("UTF-8");
			byteArrayInputStream = new ByteArrayInputStream(responseXml);
			Document xmlDoc = reader.read(byteArrayInputStream);
			//xmlDoc.setXMLEncoding("UTF-8");
			List<Element> elements = xmlDoc.getRootElement().elements();
			appBodys = new ArrayList<SM5309ReqtBean>();
			SM5309ReqtBean bean = new SM5309ReqtBean();
			for(Element element:elements)
			{
				if("svcHdr".equals(element.getName()))
				{
					System.out.println("*************************B svcHdr**************************");
					getElementList(element,svcHdr);
					System.out.println("*************************E svcHdr**************************");
				}else
				if("appHdr".equals(element.getName()))
				{
					System.out.println("*************************B appHdr**************************");
					getElementList(element,appHdr);
					System.out.println("*************************E appHdr**************************");
				}else
				if("appBody".equals(element.getName()))
				{
					System.out.println("*************************B appBody*************************");
					appBodys = getElementList(bean,element,appBody);
					System.out.println("*************************B appBody*************************");
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			if(null != byteArrayInputStream)
			{
				byteArrayInputStream.close();
			}
		}
		return appBodys;
	}
	@SuppressWarnings("unchecked")
	public static List<SM5309ReqtBean> getElementList(SM5309ReqtBean bean,Element element,HashMap<String, Object> resultHashMap)
	{
		List<SM5309ReqtBean> custFields = new ArrayList<SM5309ReqtBean>();
		String cifNo = null;
		try {
			List<Element> elements = element.elements();//custField层
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				
				Element element3 = (Element)iterator.next();
				bean = new SM5309ReqtBean();
				if("cifNo".equals(element3.getQName().getQualifiedName())){
					cifNo = StringUtils.trimToEmpty(ConvertUtil.getRemoveZero(element3.getText()));
				}
				List<Element> elementLasts = element3.elements();
				if(elementLasts.size() != 0)
				{
					for(int i = 0;i<elementLasts.size();i++){
						System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(elementLasts.get(i).getName()))+""+StringUtils.trimToEmpty(ConvertUtil.getRemoveZero(elementLasts.get(i).getText())));
						resultHashMap.put(StringUtils.trimToEmpty(elementLasts.get(i).getName()), StringUtils.trimToEmpty(elementLasts.get(i).getText()));
						getValueFromExternalModel(bean,StringUtils.trimToEmpty(elementLasts.get(i).getName()),StringUtils.trimToEmpty(elementLasts.get(i).getText()));
					}
					bean.setCifNo(cifNo);
					custFields.add(bean);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return custFields;
	}
	
	@SuppressWarnings("deprecation")
	public static String getValueFromExternalModel(BaseModel model ,String fieldName,String fieldText) {
		String result = "";
		if(org.springframework.util.StringUtils.hasText(fieldName)){
			try {
//				fieldName = fieldName.toLowerCase();
				String firstLetter = fieldName.substring(0, 1).toUpperCase();
	            //获得对应的getXxxxx方法的名子。。。
	            String setMethodName = "set" + firstLetter + fieldName.substring(1);
	            Field field = model.getClass().getDeclaredField(fieldName);
	            Method setMethod = model.getClass().getMethod(setMethodName, new Class[]{field.getType()});
		        if(field.getType().equals(String.class)){
		        	setMethod.invoke(model, new Object[]{ConvertUtil.getRemoveZero(fieldText)});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Integer.class)){
		        	setMethod.invoke(model, new Object[]{Integer.parseInt(ConvertUtil.getRemoveZero(fieldText))});//调用原有对像的setXxxx方法...
		        }
		        if(field.getType().equals(Date.class)){
		        	setMethod.invoke(model, new Object[]{new Date(fieldText)});//调用原有对像的setXxxx方法...
		        }
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		result = org.springframework.util.StringUtils.hasText(result)?result:"";
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,HashMap<String, Object> resultHashMap)
	{
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element.getName()))+""+StringUtils.trimToEmpty(element.getText()));
			resultHashMap.put(StringUtils.trimToEmpty(element.getName()), StringUtils.trimToEmpty(element.getText()));
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				getElementList(element2,resultHashMap);
			}
		}
	}
}
