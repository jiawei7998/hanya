package com.singlee.capital.interfacex.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TStBeaRepayAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TStBeaRepayAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="SPRsUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DebitCustRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}DebitCustRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CreditCustRec" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CreditCustRec" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProcessDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExposureDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStBeaRepayAaaRs", propOrder = { "commonRsHdr", "spRsUID",
		"debitCustRec", "creditCustRec", "processDt", "exposureDt",
		"companyCode", "cntId" })
public class TStBeaRepayAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "SPRsUID", required = true)
	protected String spRsUID;
	@XmlElement(name = "DebitCustRec")
	protected List<DebitCustRec> debitCustRec;
	@XmlElement(name = "CreditCustRec")
	protected List<CreditCustRec> creditCustRec;
	@XmlElement(name = "ProcessDt", required = true)
	protected String processDt;
	@XmlElement(name = "ExposureDt", required = true)
	protected String exposureDt;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;
	@XmlElement(name = "CntId", required = true)
	protected String cntId;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the spRsUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPRsUID() {
		return spRsUID;
	}

	/**
	 * Sets the value of the spRsUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPRsUID(String value) {
		this.spRsUID = value;
	}

	/**
	 * Gets the value of the debitCustRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the debitCustRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getDebitCustRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link DebitCustRec }
	 * 
	 * 
	 */
	public List<DebitCustRec> getDebitCustRec() {
		if (debitCustRec == null) {
			debitCustRec = new ArrayList<DebitCustRec>();
		}
		return this.debitCustRec;
	}

	/**
	 * Gets the value of the creditCustRec property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the creditCustRec property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCreditCustRec().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CreditCustRec }
	 * 
	 * 
	 */
	public List<CreditCustRec> getCreditCustRec() {
		if (creditCustRec == null) {
			creditCustRec = new ArrayList<CreditCustRec>();
		}
		return this.creditCustRec;
	}

	/**
	 * Gets the value of the processDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProcessDt() {
		return processDt;
	}

	/**
	 * Sets the value of the processDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProcessDt(String value) {
		this.processDt = value;
	}

	/**
	 * Gets the value of the exposureDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExposureDt() {
		return exposureDt;
	}

	/**
	 * Sets the value of the exposureDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExposureDt(String value) {
		this.exposureDt = value;
	}

	/**
	 * Gets the value of the companyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the value of the companyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}

	/**
	 * Gets the value of the cntId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntId() {
		return cntId;
	}

	/**
	 * Sets the value of the cntId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntId(String value) {
		this.cntId = value;
	}

}
