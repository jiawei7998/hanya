package com.singlee.capital.interfacex.qdb.ESB.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.ESB.model.SM5309ReqtBean;
import com.singlee.capital.interfacex.qdb.ESB.util.ConvertUtil;
import com.singlee.capital.interfacex.qdb.ESB.util.SM5309ReplaceElementContext;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;
import com.singlee.capital.system.model.TaUser;


@Component
public class SM5309{
	@Resource
	private HttpClientManagerDao httpClientManagerDao;
	
	
	private String esbUrl = PropertiesUtil.getProperties("ESB.ServiceUrl")+PropertiesUtil.getProperties("SM5309");
	HashMap<String, Object> sm5309Map  = new HashMap<String, Object>();
	
	@SuppressWarnings("unused")
	public HashMap<String, Object> sendTR1003ReqtMsgTOTis(SM5309ReqtBean dldtReqtBean,TaUser user) {
		ByteArrayInputStream byteArrayInputStream = null;
		List<SM5309ReqtBean> applays = new ArrayList<SM5309ReqtBean>();
		SAXReader reader = new SAXReader();
		//reader.setEncoding("UTF-8");
		try {
				String reqtMsg = packageTr1003ReqtMsgSendToTis(dldtReqtBean,user);
				HashMap<String, Object> hashMap = this.httpClientManagerDao.sendXmlToRequest(esbUrl, reqtMsg);
				
				if(hashMap.size()==0 || hashMap == null){
		        }else{
			        if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
			        	byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
			        	org.dom4j.Document xmlDocAccept = reader.read(byteArrayInputStream);
			        	//xmlDocAccept.setXMLEncoding("UTF-8");
			        	
			        	HashMap<String, Object> svcHdr  = new HashMap<String, Object>();//svcHdr
				        HashMap<String, Object> appBody = new HashMap<String, Object>();//appBody
				        HashMap<String, Object> appHdr  = new HashMap<String, Object>();//appHdr
				        
				        applays = SM5309ReplaceElementContext.praseEsbXmlPackageDescription((byte[]) hashMap.get("Bytes"), svcHdr, appHdr, appBody);
				        sm5309Map.put("respCde", appHdr.get("respCde"));
				        if("AB".equals(appHdr.get("respCde"))){
				        	sm5309Map.put("code", "error");
				        	sm5309Map.put("respCde", appHdr.get("respCde"));
				        	sm5309Map.put("respMsg", svcHdr.get("respMsg"));
			        	}else{
					        for(SM5309ReqtBean sm5309Reqt:applays){
					        	sm5309Reqt.setSearchKeys((String)appHdr.get("searchKeys"));
					        }
					        sm5309Map.put("List", applays);
					        sm5309Map.put("searchKeys", appHdr.get("searchKeys"));
			        	}
			        }
		        }
		} catch (Exception e) {
//			logger.error("核心账户查询出错"+e);
		}
		return sm5309Map;
	}
	
	public String packageTr1003ReqtMsgSendToTis(SM5309ReqtBean dldtReqtBean,TaUser user){
		
		org.w3c.dom.Document doc = null;  
		Element root = null;
		StringWriter xmlOut = null;
		String xmlResp = null;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
			DocumentBuilder builder = factory.newDocumentBuilder();  
			doc = builder.newDocument();  
			root = doc.createElement("reqt");  
			doc.appendChild(root);
			
			Element svcHdr = doc.createElement("svcHdr");
			Element corrId = doc.createElement("corrId");
			corrId.setTextContent("");
			Element svcId = doc.createElement("svcId");
			svcId.setTextContent("160091");
			Element verNbr = doc.createElement("verNbr");
			verNbr.setTextContent("105");
			Element csmrId = doc.createElement("csmrId");
			csmrId.setTextContent("284000");
			Element csmrSerNbr = doc.createElement("csmrSerNbr");
			csmrSerNbr.setTextContent("100010001000");
			Element tmStamp = doc.createElement("tmStamp");
			tmStamp.setTextContent(ConvertUtil.getTime(new Date()));
			Element reqtIp = doc.createElement("reqtIp");
			reqtIp.setTextContent("10.1.80.185");
			svcHdr.appendChild(corrId);
			svcHdr.appendChild(svcId);
			svcHdr.appendChild(verNbr);
			svcHdr.appendChild(csmrId);
			svcHdr.appendChild(csmrSerNbr);
			svcHdr.appendChild(tmStamp);
			svcHdr.appendChild(reqtIp);
			
			Element campHdr = doc.createElement("campHdr");
			Element custNbr = doc.createElement("custNbr");
			custNbr.setTextContent("");
			Element drivTrd = doc.createElement("drivTrd");
			drivTrd.setTextContent("");
			Element srcChlNbr = doc.createElement("srcChlNbr");
			srcChlNbr.setTextContent("");
			Element maxAmt = doc.createElement("maxAmt");
			maxAmt.setTextContent("");
			Element custLoc = doc.createElement("custLoc");
			custLoc.setTextContent("");
			Element stepFlag = doc.createElement("stepFlag");
			stepFlag.setTextContent("E");
			Element custType = doc.createElement("custType");
			custType.setTextContent("");
			Element custUniqInfo = doc.createElement("custUniqInfo");
			custUniqInfo.setTextContent("");
			Element attr1 = doc.createElement("attr1");
			attr1.setTextContent("");
			Element attr2 = doc.createElement("attr2");
			attr2.setTextContent("");
			Element attr3 = doc.createElement("attr3");
			attr3.setTextContent("");
			Element attr4 = doc.createElement("attr4");
			attr4.setTextContent("");
			Element attr5 = doc.createElement("attr5");
			attr5.setTextContent("");
			campHdr.appendChild(custNbr);
			campHdr.appendChild(drivTrd);
			campHdr.appendChild(srcChlNbr);
			campHdr.appendChild(maxAmt);
			campHdr.appendChild(custLoc);
			campHdr.appendChild(stepFlag);
			campHdr.appendChild(custType);
			campHdr.appendChild(custUniqInfo);
			campHdr.appendChild(attr1);
			campHdr.appendChild(attr2);
			campHdr.appendChild(attr3);
			campHdr.appendChild(attr4);
			campHdr.appendChild(attr5);
			
			Element appHdr = doc.createElement("appHdr");  
			Element transSrc = doc.createElement("transSrc");
			transSrc.setTextContent("FBI");
			appHdr.appendChild(transSrc);
			Element destId = doc.createElement("destId");
			destId.setTextContent("");
			appHdr.appendChild(destId);
			Element termId = doc.createElement("termId");
			termId.setTextContent("WIN-4424AG");
			appHdr.appendChild(termId);
			Element bnkNbr = doc.createElement("bnkNbr");
			bnkNbr.setTextContent("1");
			appHdr.appendChild(bnkNbr);
			Element brNbr = doc.createElement("brNbr");
			brNbr.setTextContent("80201");
			appHdr.appendChild(brNbr);
			Element userId = doc.createElement("userId");
			userId.setTextContent("BR80201143");
			appHdr.appendChild(userId);
			Element locRevuSupvId = doc.createElement("locRevuSupvId");
			locRevuSupvId.setTextContent("");
			appHdr.appendChild(locRevuSupvId);
			Element locTransSupvId = doc.createElement("locTransSupvId");
			locTransSupvId.setTextContent("");
			appHdr.appendChild(locTransSupvId);
			Element hostSupvId = doc.createElement("hostSupvId");
			hostSupvId.setTextContent("");
			appHdr.appendChild(hostSupvId);
			Element transCde = doc.createElement("transCde");
			transCde.setTextContent("5309");
			appHdr.appendChild(transCde);
			Element actCde = doc.createElement("actCde");
			actCde.setTextContent("I");
			appHdr.appendChild(actCde);
			Element transMode = doc.createElement("transMode");
			transMode.setTextContent("R");
			appHdr.appendChild(transMode);
			Element compntRefNbr = doc.createElement("compntRefNbr");
			compntRefNbr.setTextContent("745138407");
			appHdr.appendChild(compntRefNbr);
			Element srvrReconcNbr = doc.createElement("srvrReconcNbr");
			srvrReconcNbr.setTextContent("");
			appHdr.appendChild(srvrReconcNbr);
			Element nbrOfRecToRetrv = doc.createElement("nbrOfRecToRetrv");
			nbrOfRecToRetrv.setTextContent("10");
			appHdr.appendChild(nbrOfRecToRetrv);
			Element moreRecInd = doc.createElement("moreRecInd");
			moreRecInd.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getMoreRecInd()));
			appHdr.appendChild(moreRecInd);
			Element searchMeth = doc.createElement("searchMeth");
			searchMeth.setTextContent("F");
			appHdr.appendChild(searchMeth);
			Element searchKeys = doc.createElement("searchKeys");
			searchKeys.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getSearchKeys()));
			appHdr.appendChild(searchKeys);
			Element sqlInfo = doc.createElement("sqlInfo");
			sqlInfo.setTextContent("");
			appHdr.appendChild(sqlInfo);
			Element dtInFromClt = doc.createElement("dtInFromClt");
			dtInFromClt.setTextContent("20140116");
			appHdr.appendChild(dtInFromClt);
			Element tmInFromClt = doc.createElement("tmInFromClt");
			tmInFromClt.setTextContent("174700");
			appHdr.appendChild(tmInFromClt);
			Element transType = doc.createElement("transType");
			transType.setTextContent("");
			appHdr.appendChild(transType);
			Element upsSysCde = doc.createElement("upsSysCde");
			upsSysCde.setTextContent("");
			appHdr.appendChild(upsSysCde);
			Element recvAtdSrcId = doc.createElement("recvAtdSrcId");
			recvAtdSrcId.setTextContent("");
			appHdr.appendChild(recvAtdSrcId);
			Element recvAtdUserId = doc.createElement("recvAtdUserId");
			recvAtdUserId.setTextContent("");
			appHdr.appendChild(recvAtdUserId);
			Element recvAtdJrnlSeqNbr = doc.createElement("recvAtdJrnlSeqNbr");
			recvAtdJrnlSeqNbr.setTextContent("");
			appHdr.appendChild(recvAtdJrnlSeqNbr);
			Element procSrcId = doc.createElement("procSrcId");
			procSrcId.setTextContent("");
			appHdr.appendChild(procSrcId);
			Element procUserId = doc.createElement("procUserId");
			procUserId.setTextContent("");
			appHdr.appendChild(procUserId);
			Element procJrnlSeqNbr = doc.createElement("procJrnlSeqNbr");
			procJrnlSeqNbr.setTextContent("");
			appHdr.appendChild(procJrnlSeqNbr);
			Element pkgCde = doc.createElement("pkgCde");
			pkgCde.setTextContent("");
			appHdr.appendChild(pkgCde);
			Element promoCde = doc.createElement("promoCde");
			promoCde.setTextContent("");
			appHdr.appendChild(promoCde);
			Element varFmt = doc.createElement("varFmt");
			varFmt.setTextContent("");
			appHdr.appendChild(varFmt);
			
			
			Element appBody = doc.createElement("appBody");
			Element cifNo = doc.createElement("cifNo");
			cifNo.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getCifNo()));
			appBody.appendChild(cifNo);
			Element idNbr = doc.createElement("idNbr");
			idNbr.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(idNbr);
			Element idType = doc.createElement("idType");
			idType.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(idType);
			Element idOwnerCntry  = doc.createElement("idOwnerCntry");
			idOwnerCntry.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(idOwnerCntry);
			Element cardNbr  = doc.createElement("cardNbr");
			cardNbr.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(cardNbr);
			Element acctNbr = doc.createElement("acctNbr");
			acctNbr.setTextContent(StringUtils.trimToEmpty(dldtReqtBean.getAcctNbr()));
			appBody.appendChild(acctNbr);
			Element entrustedDepstInd = doc.createElement("entrustedDepstInd");
			entrustedDepstInd.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(entrustedDepstInd);
			Element hldCdeInd = doc.createElement("hldCdeInd");
			hldCdeInd.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(hldCdeInd);
			Element hasLoss = doc.createElement("hasLoss");
			hasLoss.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(hasLoss);
			Element judiFrzInd = doc.createElement("judiFrzInd");
			judiFrzInd.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(judiFrzInd);
			Element idPrfx = doc.createElement("idPrfx");
			idPrfx.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(idPrfx);
			Element acctType = doc.createElement("acctType");
			acctType.setTextContent(StringUtils.trimToEmpty(""));
			appBody.appendChild(acctType);
			
			
			root.appendChild(svcHdr);
			root.appendChild(campHdr);
			root.appendChild(appHdr);
			root.appendChild(appBody);
			
			DOMSource source = new DOMSource(doc);  
			TransformerFactory tf = TransformerFactory.newInstance();  
			Transformer transformer = tf.newTransformer();  
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");//设置文档的换行与缩进  
			 
            xmlOut=new StringWriter();
            StreamResult result=new StreamResult(xmlOut);
            transformer.transform(source,result);
            xmlResp = xmlOut.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(null != xmlOut){
				try {
					xmlOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return xmlResp;
		
	}
	
	public String formatBigDecimal4(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.0000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	public String formatBigDecimal8(BigDecimal bigDecimal)
	{
		DecimalFormat   f =new   DecimalFormat("##############0.00000000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	
	@SuppressWarnings("unchecked")
	public static void getElementAppHdr(org.dom4j.Element element) throws Exception
	{
		List<org.dom4j.Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if("reqDt".equalsIgnoreCase(element.getName())){
				element.setText(ConvertUtil.getDataString(new Date()));
			}
			if("reqTm".equalsIgnoreCase(element.getName())){
				element.setText(ConvertUtil.getTimeString(new Date()));
			}
		}
		else {
			for(Iterator<org.dom4j.Element> iterator = elements.iterator();iterator.hasNext();)
			{
				org.dom4j.Element element2 = (org.dom4j.Element)iterator.next();
				getElementAppHdr(element2);
			}
		}
	}

	/**
	 * @return the esbUrl
	 */
	public String getEsbUrl() {
		return esbUrl;
	}

	/**
	 * @param esbUrl the esbUrl to set
	 */
	public void setEsbUrl(String esbUrl) {
		this.esbUrl = esbUrl;
	}
	
}
