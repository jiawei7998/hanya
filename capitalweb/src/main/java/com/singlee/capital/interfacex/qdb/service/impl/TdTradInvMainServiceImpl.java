package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.mapper.TdTradInvMainMapper;
import com.singlee.capital.interfacex.qdb.mapper.TdTradeInvDetailMapper;
import com.singlee.capital.interfacex.qdb.model.TdTradInvMain;
import com.singlee.capital.interfacex.qdb.model.TdTradeInvDetail;
import com.singlee.capital.interfacex.qdb.service.TdTradInvMainService;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdTradInvMainServiceImpl implements TdTradInvMainService{
	@Autowired
	private TdTradeInvDetailMapper tradeInvDetailMapper;
	@Autowired
	private TdTradInvMainMapper tdTradInvMainMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private DayendDateService dayendDateService;
	private static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");
	
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	@Override
	public Page<TdTradInvMain> selectTdTradInvMainPage(Map<String, Object> map) {
		Page<TdTradInvMain> page = tdTradInvMainMapper.selectTdTradInvMainPage(map, ParameterUtil.getRowBounds(map));
		return page;
	}
	@Override
	public Page<TdTradeInvDetail> selectTdTradInvDetailPage(Map<String, Object> map) {
		Page<TdTradeInvDetail> page = tradeInvDetailMapper.selectTdTradInvDetailPage(map, ParameterUtil.getRowBounds(map));
		return page;
	}
	@Override
	public String setTdTradInvMainByExcel(String type, InputStream is,String tradeid) {
		StringBuffer sb = new StringBuffer();
		Sheet sheet = null;
		TdTradInvMain tdTradInvMain  = null;
		String str1 = null;
		String str2 = null;
		String str3 = null;
		String str4 = null;
		try {
			log.info("TdTradInvMainServiceImpl--setTdTradInvMainByExcel     "+"开始--解析Excel文档中数据!!!");
			//开始解析Excel
			ExcelUtil excelUtil = new ExcelUtil(type,is);
			//Excel第一页里面的具体数据
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			sheet = excelUtil.getSheetAt(0);
			Row row;
			//创建委外业务基础资产信息集合
			List<TdTradInvMain> invMainList = new ArrayList<TdTradInvMain>();
			//创建委外业务基础资产信息明细集合
			List<TdTradeInvDetail> invDetailList  = new ArrayList<TdTradeInvDetail>();
			//创建委外业务基础资产信息对象
			tdTradInvMain = new TdTradInvMain();
			TdTradeInvDetail tdTradeInvDetail = null;
			Pattern pattern = Pattern.compile("[0-9]*");
			String value = null;
			String importDate = dayendDateService.getSettlementDate();
			String version = new SimpleDateFormat("HHmmss").format(new Date());
			if(rowNum > 0) {
				
			
			for (int i = 2; i <= rowNum; i++) 
			{
				tdTradeInvDetail = new TdTradeInvDetail();
				row = sheet.getRow(i);
				value = getCellData(row.getCell(0), 500,null);
				if(value == null){
					value = "";
				}
				if(value.contains("证券投资合计")||value.contains("债券投资")){
					if(pandun(getCellData(row.getCell(4), 22,null))||pandun(getCellData(row.getCell(5), 22,null))){
						//加入逻辑，A816青岛银行定向第e列取不到值，需要取第f列的数据
						String CellE=getCellData(row.getCell(4), 22,null);
						if(CellE!=null&&!"".equals(CellE)){
							if(tdTradInvMain.getTotalSecInv()==null || "".equals(tdTradInvMain.getTotalSecInv())){
								tdTradInvMain.setTotalSecInv(CellE);
							}
						}else{
							if(pandun(getCellData(row.getCell(5), 22,null))){
								if(tdTradInvMain.getTotalSecInv()==null || "".equals(tdTradInvMain.getTotalSecInv())){
									tdTradInvMain.setTotalSecInv(getCellData(row.getCell(5), 22,null));
								}
								
							}
						}
					}else{
						throw new RuntimeException("证券投资合计不符合数字规范");
					}
				}
				else if(value.contains("资产类合计") || value.contains("资产合计")){
					//e取不到取f的数据
					String asset="";
					for(int cellVal=1;cellVal<10;cellVal++){
						if(getCellData(row.getCell(cellVal), 22,null)!=null && !"".equals(getCellData(row.getCell(cellVal), 22,null))){
							asset =getCellData(row.getCell(cellVal), 22,null);
							break;
						}
					}
					if(pandun(asset)){
						//加入逻辑，华泰证券第e列取不到值，需要取第f列的数据
//						String CellE=getCellData(row.getCell(4), 22,null);
//						if(CellE!=null&&!"".equals(CellE)){
//							tdTradInvMain.setAsset(CellE);
//						}else{
//							if(pandun(getCellData(row.getCell(5), 22,null))){
//								tdTradInvMain.setAsset(getCellData(row.getCell(5), 22,null));
//							}
//						}
						if(!"".equals(asset)){
							tdTradInvMain.setAsset(asset);
						}
					}else{
						throw new RuntimeException("资产类合计不符合数字规范");
					}
				}else if(value.contains("负债类合计") || value.contains("负债合计")){
					String debt="";
					for(int debtVal=1;debtVal<10;debtVal++){
						if(getCellData(row.getCell(debtVal), 22,null)!=null && !"".equals(getCellData(row.getCell(debtVal), 22,null))){
							debt =getCellData(row.getCell(debtVal), 22,null);
							break;
						}
					}
					if(pandun(debt)){
						//加入逻辑，华泰证券第e列取不到值，需要取第f列的数据
//						String CellE=getCellData(row.getCell(4), 22,null);
//						if(CellE!=null&&!"".equals(CellE)){
//							tdTradInvMain.setDebt(CellE);
//						}else{
//							if(pandun(getCellData(row.getCell(5), 22,null))){
//								tdTradInvMain.setAsset(getCellData(row.getCell(5), 22,null));
//							}
//						}
						if(!"".equals(debt)){
							tdTradInvMain.setDebt(debt);
						}
					}else{
						throw new RuntimeException("负债类合计不符合数字规范");
					}
				}else if(value.contains("基金资产净值") || value.contains("资产资产净值") || value.contains("资产净值"))
				{	
					String cpriceAssetCost="";//资产净值按成本
					
					for(int cpriceAssetCostVal =1;cpriceAssetCostVal<10;cpriceAssetCostVal++){
						if(getCellData(row.getCell(cpriceAssetCostVal), 22,null)!=null && !"".equals(getCellData(row.getCell(cpriceAssetCostVal), 22,null))){
							cpriceAssetCost = getCellData(row.getCell(cpriceAssetCostVal), 22,null);
							break;
						}
					}
					if(pandun(cpriceAssetCost)){
						//加入逻辑，华泰证券第e列取不到值，需要取第f列的数据
//						String CellE=getCellData(row.getCell(4), 22,null);
//						if(CellE!=null&&!"".equals(CellE)){
//							tdTradInvMain.setCpriceAssetCost(CellE);
//						}else{
//							if(pandun(getCellData(row.getCell(5), 22,null))){
//								tdTradInvMain.setCpriceAssetCost(getCellData(row.getCell(5), 22,null));
//							}
//						}
						if(!"".equals(cpriceAssetCost)){
							tdTradInvMain.setCpriceAssetCost(cpriceAssetCost);
						}
					}else{
						throw new RuntimeException("资产净值（按成本）不符合数字规范");
					}
					if(pandun(getCellData(row.getCell(7), 22,null)) || pandun(getCellData(row.getCell(8), 22,null))){
						//加入逻辑，华泰证券第e列取不到值，需要取第f列的数据
						String CellH=getCellData(row.getCell(7), 22,null);
						if(CellH!=null&&!"".equals(CellH)){
							tdTradInvMain.setCpriceAssetMarket(CellH);
						}else{
							if(pandun(getCellData(row.getCell(8), 22,null))){
								tdTradInvMain.setCpriceAssetMarket(getCellData(row.getCell(8), 22,null));
							}
						}
						
					}else{
						throw new RuntimeException("资产净值（按市值）不符合数字规范");
					}
				}else{
					//创建委外业务基础资产信息明细对象
				    str1 = getCellData(row.getCell(1), 500,null);
					str2 = getCellData(row.getCell(2), 500,null);
					str3 = getCellData(row.getCell(3), 500,null);
					str4 = getCellData(row.getCell(4), 500,null);
					//1.增加逻辑，只插入证券数量不为0，并且证券名称存在的数据到明细表。2.增加逻辑当科目名称为面额时，不能继续将面额插入明细表str2.indexOf(String.valueOf('.'))== -1||判断小数点存在不存在
					if(str1==null||"".equals(str1)||str2==null||"".equals(str2)||str3==null||"".equals(str3)||str4==null||"".equals(str4)||str1.contains("面额")){
						continue;
					}
					
					
					//正则表达式，判断是否有数字
					Matcher isNum = pattern.matcher(str2);
					//判断值中是否为数字，并且是否含小数点
					if(!isNum.matches() && str2.indexOf(String.valueOf('.'))!= -1){
						if(str1!= "" && str1 != null&&str2!= "" && str2!= null){
							//审批单编号
							tdTradeInvDetail.setTradeId(tradeid);
							//资产类型
							tdTradeInvDetail.setaType("自营");
							//债券ID
							tdTradeInvDetail.setSecId(getCellData(row.getCell(0), 35,null));
							//债券名称
							tdTradeInvDetail.setSecNm(getCellData(row.getCell(1), 60,null));
							//债券面值
							tdTradeInvDetail.setFaceamt(getCellData(row.getCell(2), 18,null));
							//单位成本
							tdTradeInvDetail.setUnitCost(getCellData(row.getCell(3), 18,null));
							//成本总价
							tdTradeInvDetail.setCostAmt(getCellData(row.getCell(4), 18,null));
							//版本号
							tdTradeInvDetail.setVersion(version);
							//导入日期
							tdTradeInvDetail.setiDate(importDate);
						}
					}
					if(str1!= "" && str1 != null&&str2 != "" && str2 != null&&str3 != "" && str3 != null&&str4 != "" && str4!= null){
						invDetailList.add(tdTradeInvDetail);
					}
				}
			}// end for
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("dealNo", tradeid);
			TdProductApproveMain tdmain = tdProductApproveMainMapper.getProductApproveMain(map);
			tdTradInvMain.setAName(tdmain.getProductName());
			tdTradInvMain.setTradeId(tradeid);
			//版本号
			tdTradInvMain.setVersion(version);
			//导入日期
			tdTradInvMain.setiDate(importDate);
			invMainList.add(tdTradInvMain);
			
			//删除主表上版本记录
			tdTradInvMainMapper.deleteTdTradInvMainById(tradeid);
			//保存主表记录
			tdTradInvMainMapper.insertTdTradInvMain(invMainList);
			tdTradInvMainMapper.insertTdTradeInvMainHis2(invMainList);
			
			//删除明细表上版本记录
			tradeInvDetailMapper.deleteTdTradeInvDetailById(tradeid);
			//保存明细记录
			tradeInvDetailMapper.insertTdTradeInvDetail(invDetailList);
			tradeInvDetailMapper.insertTdTradeInvDetailHis2(invDetailList);
			}else {
				throw new RuntimeException("模板不能为空");
			}
			log.info("TdTradInvMainServiceImpl--setTdTradInvMainByExcel   "+"结束--解析Excel文档中数据!!!");
		} catch (Exception e) {
			if(sheet!=null){
				log.error("TdTradInvMainServiceImpl--setTdTradInvMainByExcel"+e);
				sb.append("委外业务信息上传失败:"+e.getMessage());
			}else{
				log.error("TdTradInvMainServiceImpl--setTdTradInvMainByExcel"+e);
				sb.append("无法识别Excel后缀名:"+e.getMessage());
			}
			
		}
		return sb.toString();
	}
	
	
	

	public  static final int CELL_TYPE_DATE = 999;
	@SuppressWarnings({ "deprecation" })
	private String getCellData(Cell cell,int lenth,Integer type)throws Exception{
		if(cell == null) {return null;}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String value = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			value = null;
			break;
			
		case Cell.CELL_TYPE_STRING:
			if(type!=null&&Cell.CELL_TYPE_STRING != type){
				throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是字符类型");
			}
			value = cell.getRichStringCellValue().getString();
			break;
			
		case Cell.CELL_TYPE_NUMERIC:
			if(type != null)
			{
				if(CELL_TYPE_DATE == type){
					if(DateUtil.isCellDateFormatted(cell)){
						value = sdf.format(cell.getDateCellValue());
					}else{
						throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是日期类型");
					}
				}else if(Cell.CELL_TYPE_NUMERIC == type || Cell.CELL_TYPE_STRING == type){
					value = new BigDecimal(String.valueOf(cell.getNumericCellValue())).toPlainString();
				}else{
					throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是数字类型");
					
				}
			}else{
				if(DateUtil.isCellDateFormatted(cell)){
					value = sdf.format(cell.getDateCellValue());
				}else{
					value = new BigDecimal(String.valueOf(cell.getNumericCellValue())).toPlainString();
				}
			}
			break;
			
		case Cell.CELL_TYPE_BOOLEAN:
			value = String.valueOf(cell.getBooleanCellValue());
			break;
			
		case Cell.CELL_TYPE_FORMULA:
			try {
				value = String.valueOf(cell.getNumericCellValue());
			} catch (Exception e) {
				try {
					value = cell.getRichStringCellValue().toString();
				} catch (Exception e2) {
					value = "";
				}
			}
			break;

		default:
			break;
		}
		if(value != null && lenth > 0 && value.length() > lenth){
			throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容超过最大长度："+lenth);
		}
		return value;
	}
	
	private boolean pandun(String str){
	
        boolean ret = true;
        try{
        	if(str==null||"".equals(str)){
        		ret=false;
        		return ret;
    		}else{
    			Double.parseDouble(str);
    			ret = true;
    		}
        }catch(Exception ex){
            ret = false;
        }
        return ret;
    }
	@Override
	public void deleteTdtradeInv(Map<String, Object> map) {
		tdTradInvMainMapper.deleteTdTradInvMainById(String.valueOf(map.get("tradeId")));
		tdTradInvMainMapper.deleteTdTradeInvMainByDate(map);
		
		tradeInvDetailMapper.deleteTdTradeInvDetailById(String.valueOf(map.get("tradeId")));
		tradeInvDetailMapper.deleteTdTradeInvDetailByDate(map);
	}

	
	/**
	 * 生成委外业务基础资产
	 */
	@Override
	public ExcelUtil downloadExcel(HashMap<String, Object> map) {
		List<TdTradeInvDetail> exitDetailList =null;
		List<TdTradInvMain> exitMainList =null;
		ExcelUtil e = null;
		try {
			String tradeid=(String)map.get("tradeid");
			//查出所有的委外业务主表信息与明细表数据，并且将主（明细表的数组做成主表的一个属性，）一个版本号对应一个委外业务主表，多个委外业务明细表
			map.put("tradeid", tradeid);
			exitDetailList = tradeInvDetailMapper.selectAllTdTradInvDetail(map);
			exitMainList = tdTradInvMainMapper.selectAllTdTradInvMain(map);
			
			Map<String, List<TdTradeInvDetail>> map1 = new HashMap<String, List<TdTradeInvDetail>>();
			
			List<TdTradeInvDetail> temp = null;
			for (TdTradeInvDetail de : exitDetailList) {
				if(de!=null){
					temp = map1.get(de.getVersion());
					if(temp == null){
						temp = new ArrayList<TdTradeInvDetail>();
						map1.put(de.getVersion(), temp);
					}
					temp.add(de);
				}
				
			}
		
			//表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			CellStyle left = e.getDefaultLeftStrCellStyle();
			int row = 0;
			e.getWb().createSheet("表");
			e.writeStr(0,row, "A",center, "交易编号"); 
			e.writeStr(0,row, "B",center, "资产版本"); 
			e.writeStr(0,row, "C",center, "科目代码"); 
			e.writeStr(0,row, "D",center, "科目名称");       
			e.writeStr(0,row, "E",center, "数    量");   
			e.writeStr(0,row, "F", center,"单位成本");   
			e.writeStr(0,row, "G",center, "成    本");   
			
			//设置列宽
			e.getSheetAt(0).setColumnWidth(0, 20 * 256);
			e.getSheetAt(0).setColumnWidth(1, 20 * 256);
			e.getSheetAt(0).setColumnWidth(2, 20 * 256);
			e.getSheetAt(0).setColumnWidth(3, 20 * 256);
			e.getSheetAt(0).setColumnWidth(4, 20 * 256);
			e.getSheetAt(0).setColumnWidth(5, 20 * 256);
			e.getSheetAt(0).setColumnWidth(6, 20 * 256);
			e.getSheetAt(0).setColumnWidth(7, 20 * 256);
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			TdTradeInvDetail bean;
			
			for (TdTradInvMain m : exitMainList) 
			{	
				exitDetailList = map1.get(m.getVersion());
				if(exitDetailList!=null){
					for (int j = 0; j < exitDetailList.size(); j++) {
						row++;
						bean = exitDetailList.get(j);
						if(bean!=null){
							e.writeStr(0, row, "A", center, tradeid);
							e.writeStr(0, row, "B", center, bean.getVersion());
							e.writeStr(0, row, "C", left, bean.getSecId());
							e.writeStr(0, row, "D", left, bean.getSecNm());
							//e.writeDouble(0, row, "C", e.getDefaultDouble2CellStyle(),bean.getCreditAmt().doubleValue());
							//e.writeDouble(0,row,"D",e.getDefaultDouble2CellStyle(),bean.getQuotaSegAmt().doubleValue());
							Double faceamt = Double.valueOf(bean.getFaceamt());
							e.writeDouble(0, row, "E", e.getDefaultDouble2CellStyle(),faceamt.doubleValue());
							Double UnitCost = Double.valueOf(bean.getUnitCost());
							e.writeDouble(0,row,"F",e.getDefaultDouble2CellStyle(),UnitCost.doubleValue());
							Double CostAmt = Double.valueOf(bean.getCostAmt());
							e.writeDouble(0, row, "G", e.getDefaultDouble2CellStyle(),CostAmt.doubleValue());
						}
						
					}
					e.writeStr(0,++row, "C",left, "证券投资合计"); 
					e.writeStr(0,row, "A",left, tradeid); 
					e.writeStr(0,row, "B",center, m.getVersion()); 
					Double totalSecInv = Double.valueOf(m.getTotalSecInv());
					e.writeDouble(0,row, "H",e.getDefaultDouble2CellStyle(), totalSecInv.doubleValue());
					e.writeStr(0,++row, "C",left, "资产类合计");
					Double asset = Double.valueOf(m.getAsset());
					e.writeDouble(0,row, "H",e.getDefaultDouble2CellStyle(), asset.doubleValue());
					e.writeStr(0,row, "A",left, tradeid); 
					e.writeStr(0,row, "B",center, m.getVersion()); 
					e.writeStr(0,++row, "C",left, "负债类合计");   
					Double debt = Double.valueOf(m.getDebt());
					e.writeDouble(0,row, "H",e.getDefaultDouble2CellStyle(), debt.doubleValue());
					e.writeStr(0,row, "A",left, tradeid); 
					e.writeStr(0,row, "B",center, m.getVersion()); 
					e.writeStr(0,++row, "C",left, "资产资产净值(按成本)");  
					Double cpriceAssetCost = Double.valueOf(m.getCpriceAssetCost());
					e.writeDouble(0,row, "H",e.getDefaultDouble2CellStyle(), cpriceAssetCost.doubleValue());
					e.writeStr(0,row, "A",left, tradeid); 
					e.writeStr(0,row, "B",center, m.getVersion()); 
					e.writeStr(0,++row, "C",left, "资产资产净值(按市值)");  
					e.writeStr(0,row, "A",left, tradeid); 
					e.writeStr(0,row, "B",center, m.getVersion()); 
					Double cpriceAssetMarket = Double.valueOf(m.getCpriceAssetMarket());
					e.writeDouble(0,row, "H",e.getDefaultDouble2CellStyle(), cpriceAssetMarket.doubleValue());
				}	
			}
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			return e;
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return e;
	}
}
