package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpCorpacctAllAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpCorpacctAllAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Customer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CateGory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctTitle1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctTitle2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShortTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctOfficer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PostingRestrict" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CashTxnFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Passtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DepValDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaturityDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcCharacter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CharDiff" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExchangeNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FcyAprvlCrtf" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InactivMarker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClearBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OnlineActualBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClearBal_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkingBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AltAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MasterAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OpenBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpCorpacctAllAaaRs", propOrder = { "commonRsHdr",
		"accountNo", "customer", "cateGory", "acctTitle1", "acctTitle2",
		"shortTitle", "currency", "acctOfficer", "postingRestrict", "acctType",
		"cashTxnFlag", "passtype", "beginDt", "depValDt", "term", "maturityDt",
		"acCharacter", "charDiff", "exchangeNo", "fcyAprvlCrtf", "idType",
		"idNo", "inactivMarker", "clearBal", "onlineActualBal", "clearBal2",
		"workingBal", "altAcctNo", "masterAcctNo", "openBank" })
public class TDpCorpacctAllAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "AccountNo", required = true)
	protected String accountNo;
	@XmlElement(name = "Customer", required = true)
	protected String customer;
	@XmlElement(name = "CateGory", required = true)
	protected String cateGory;
	@XmlElement(name = "AcctTitle1", required = true)
	protected String acctTitle1;
	@XmlElement(name = "AcctTitle2", required = true)
	protected String acctTitle2;
	@XmlElement(name = "ShortTitle", required = true)
	protected String shortTitle;
	@XmlElement(name = "Currency", required = true)
	protected String currency;
	@XmlElement(name = "AcctOfficer", required = true)
	protected String acctOfficer;
	@XmlElement(name = "PostingRestrict", required = true)
	protected String postingRestrict;
	@XmlElement(name = "AcctType", required = true)
	protected String acctType;
	@XmlElement(name = "CashTxnFlag", required = true)
	protected String cashTxnFlag;
	@XmlElement(name = "Passtype", required = true)
	protected String passtype;
	@XmlElement(name = "BeginDt", required = true)
	protected String beginDt;
	@XmlElement(name = "DepValDt", required = true)
	protected String depValDt;
	@XmlElement(name = "Term", required = true)
	protected String term;
	@XmlElement(name = "MaturityDt", required = true)
	protected String maturityDt;
	@XmlElement(name = "AcCharacter", required = true)
	protected String acCharacter;
	@XmlElement(name = "CharDiff", required = true)
	protected String charDiff;
	@XmlElement(name = "ExchangeNo", required = true)
	protected String exchangeNo;
	@XmlElement(name = "FcyAprvlCrtf", required = true)
	protected String fcyAprvlCrtf;
	@XmlElement(name = "IdType", required = true)
	protected String idType;
	@XmlElement(name = "IdNo", required = true)
	protected String idNo;
	@XmlElement(name = "InactivMarker", required = true)
	protected String inactivMarker;
	@XmlElement(name = "ClearBal", required = true)
	protected String clearBal;
	@XmlElement(name = "OnlineActualBal", required = true)
	protected String onlineActualBal;
	@XmlElement(name = "ClearBal_2", required = true)
	protected String clearBal2;
	@XmlElement(name = "WorkingBal", required = true)
	protected String workingBal;
	@XmlElement(name = "AltAcctNo", required = true)
	protected String altAcctNo;
	@XmlElement(name = "MasterAcctNo", required = true)
	protected String masterAcctNo;
	@XmlElement(name = "OpenBank", required = true)
	protected String openBank;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the accountNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * Sets the value of the accountNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountNo(String value) {
		this.accountNo = value;
	}

	/**
	 * Gets the value of the customer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomer() {
		return customer;
	}

	/**
	 * Sets the value of the customer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomer(String value) {
		this.customer = value;
	}

	/**
	 * Gets the value of the cateGory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCateGory() {
		return cateGory;
	}

	/**
	 * Sets the value of the cateGory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCateGory(String value) {
		this.cateGory = value;
	}

	/**
	 * Gets the value of the acctTitle1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctTitle1() {
		return acctTitle1;
	}

	/**
	 * Sets the value of the acctTitle1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctTitle1(String value) {
		this.acctTitle1 = value;
	}

	/**
	 * Gets the value of the acctTitle2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctTitle2() {
		return acctTitle2;
	}

	/**
	 * Sets the value of the acctTitle2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctTitle2(String value) {
		this.acctTitle2 = value;
	}

	/**
	 * Gets the value of the shortTitle property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShortTitle() {
		return shortTitle;
	}

	/**
	 * Sets the value of the shortTitle property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShortTitle(String value) {
		this.shortTitle = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the acctOfficer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctOfficer() {
		return acctOfficer;
	}

	/**
	 * Sets the value of the acctOfficer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctOfficer(String value) {
		this.acctOfficer = value;
	}

	/**
	 * Gets the value of the postingRestrict property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostingRestrict() {
		return postingRestrict;
	}

	/**
	 * Sets the value of the postingRestrict property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostingRestrict(String value) {
		this.postingRestrict = value;
	}

	/**
	 * Gets the value of the acctType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctType() {
		return acctType;
	}

	/**
	 * Sets the value of the acctType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctType(String value) {
		this.acctType = value;
	}

	/**
	 * Gets the value of the cashTxnFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCashTxnFlag() {
		return cashTxnFlag;
	}

	/**
	 * Sets the value of the cashTxnFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCashTxnFlag(String value) {
		this.cashTxnFlag = value;
	}

	/**
	 * Gets the value of the passtype property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPasstype() {
		return passtype;
	}

	/**
	 * Sets the value of the passtype property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPasstype(String value) {
		this.passtype = value;
	}

	/**
	 * Gets the value of the beginDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeginDt() {
		return beginDt;
	}

	/**
	 * Sets the value of the beginDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeginDt(String value) {
		this.beginDt = value;
	}

	/**
	 * Gets the value of the depValDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDepValDt() {
		return depValDt;
	}

	/**
	 * Sets the value of the depValDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDepValDt(String value) {
		this.depValDt = value;
	}

	/**
	 * Gets the value of the term property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the value of the term property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTerm(String value) {
		this.term = value;
	}

	/**
	 * Gets the value of the maturityDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaturityDt() {
		return maturityDt;
	}

	/**
	 * Sets the value of the maturityDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaturityDt(String value) {
		this.maturityDt = value;
	}

	/**
	 * Gets the value of the acCharacter property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcCharacter() {
		return acCharacter;
	}

	/**
	 * Sets the value of the acCharacter property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcCharacter(String value) {
		this.acCharacter = value;
	}

	/**
	 * Gets the value of the charDiff property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCharDiff() {
		return charDiff;
	}

	/**
	 * Sets the value of the charDiff property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCharDiff(String value) {
		this.charDiff = value;
	}

	/**
	 * Gets the value of the exchangeNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExchangeNo() {
		return exchangeNo;
	}

	/**
	 * Sets the value of the exchangeNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExchangeNo(String value) {
		this.exchangeNo = value;
	}

	/**
	 * Gets the value of the fcyAprvlCrtf property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFcyAprvlCrtf() {
		return fcyAprvlCrtf;
	}

	/**
	 * Sets the value of the fcyAprvlCrtf property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFcyAprvlCrtf(String value) {
		this.fcyAprvlCrtf = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdType(String value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdNo() {
		return idNo;
	}

	/**
	 * Sets the value of the idNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIdNo(String value) {
		this.idNo = value;
	}

	/**
	 * Gets the value of the inactivMarker property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInactivMarker() {
		return inactivMarker;
	}

	/**
	 * Sets the value of the inactivMarker property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInactivMarker(String value) {
		this.inactivMarker = value;
	}

	/**
	 * Gets the value of the clearBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClearBal() {
		return clearBal;
	}

	/**
	 * Sets the value of the clearBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClearBal(String value) {
		this.clearBal = value;
	}

	/**
	 * Gets the value of the onlineActualBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOnlineActualBal() {
		return onlineActualBal;
	}

	/**
	 * Sets the value of the onlineActualBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOnlineActualBal(String value) {
		this.onlineActualBal = value;
	}

	/**
	 * Gets the value of the clearBal2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClearBal2() {
		return clearBal2;
	}

	/**
	 * Sets the value of the clearBal2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClearBal2(String value) {
		this.clearBal2 = value;
	}

	/**
	 * Gets the value of the workingBal property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkingBal() {
		return workingBal;
	}

	/**
	 * Sets the value of the workingBal property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWorkingBal(String value) {
		this.workingBal = value;
	}

	/**
	 * Gets the value of the altAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAltAcctNo() {
		return altAcctNo;
	}

	/**
	 * Sets the value of the altAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAltAcctNo(String value) {
		this.altAcctNo = value;
	}

	/**
	 * Gets the value of the masterAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMasterAcctNo() {
		return masterAcctNo;
	}

	/**
	 * Sets the value of the masterAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMasterAcctNo(String value) {
		this.masterAcctNo = value;
	}

	/**
	 * Gets the value of the openBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOpenBank() {
		return openBank;
	}

	/**
	 * Sets the value of the openBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOpenBank(String value) {
		this.openBank = value;
	}

}
