package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class GLBatch implements Serializable{

	/**
	 * 总账数据文件 总账取IFBM文件
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 总帐系统设定的业务单位
	 */
	private String businessUnit;  
	/**
	 * 总帐系统中按照币种分别设定的账本
	 */
	private String ledger; 
	/**
	 * 交易数据发生的会计科目
	 */
	private String account; 
	/**
	 * 本次不用
	 */
	private String alternateAccount; 
	/**
	 * 总帐系统中设定的业务部门
	 */
	private String department; 
	/**
	 * 上海银行所辖的交换行号，交换行号和网点有对应关系
	 */
	private String operatingUnit; 
	/**
	 * 本次不用
	 */
	private String product; 
	/**
	 * 各个业务分系统名称代码
	 */
	private String fundCode;  
	/**
	 * 本次不用
	 */
	private String classField;                         
	private String programCode;                         
	private String budgetReference;                         
	private String affiliate;                         
	private String fundAffiliate;                         
	private String operatingUnitAffiliate;                         
	private String chartField1;                        
	private String chartField2;                        
	private String chartField3;                        
	private String projectID;                         
	private String bookCode;                         
	private String adjustmentType; 
	/**
	 * 该行交易数据的交易货币种类
	 */
	private String transactionCurrencyCode;  
	/**
	 * 本次不用
	 */
	private String statisticsCode;
	/**
	 * 当前的会计年度
	 */
	private String fiscalYear; 
	/**
	 * 总帐系统可以识别的会计期间,比如2月28日，期间表示为59（即一年的第59个会计期间）
	 */
	private String accountingPeriod; 
	
	/**
	 * 在本次总帐系统实施中，对应每个币种帐的报告币种＝交易币种＝本位币
	 * Posted Total Amount = Base Currency Amount = Posted Transaction Amount
	 */
	
	/**
	 * 按照报告币种计算的借贷方轧差数
	 */
	private String postedTotalAmount; 
	/**
	 * 本位币计算的借贷方轧差数
	 */
	private String postedBaseCurrencyAmount;
	/**
	 * 按照交易币种计算的借贷方轧差数
	 */
	private String postedTransactionAmount;   
	/**
	 * 按报告币种计算的当天借方发生汇总
	 */
	private String postedTotalDebitAmount;  
	/**
	 * 按报告币种计算的当天贷方发生汇总
	 */
	private String postedTotalCreditAmount; 
	/**
	 * 按照交易币种计算的当天借方发生汇总
	 */
	private String postedTransactionDebitAmount;  
	/**
	 * 按照交易币种计算的当天贷方发生汇总
	 */
	private String postedTransactionCreditAmount;   
	/**
	 * 交易数据发生的日期
	 */
	private String transactionDate;
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getLedger() {
		return ledger;
	}
	public void setLedger(String ledger) {
		this.ledger = ledger;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getAlternateAccount() {
		return alternateAccount;
	}
	public void setAlternateAccount(String alternateAccount) {
		this.alternateAccount = alternateAccount;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getOperatingUnit() {
		return operatingUnit;
	}
	public void setOperatingUnit(String operatingUnit) {
		this.operatingUnit = operatingUnit;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getClassField() {
		return classField;
	}
	public void setClassField(String classField) {
		this.classField = classField;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getBudgetReference() {
		return budgetReference;
	}
	public void setBudgetReference(String budgetReference) {
		this.budgetReference = budgetReference;
	}
	public String getAffiliate() {
		return affiliate;
	}
	public void setAffiliate(String affiliate) {
		this.affiliate = affiliate;
	}
	public String getFundAffiliate() {
		return fundAffiliate;
	}
	public void setFundAffiliate(String fundAffiliate) {
		this.fundAffiliate = fundAffiliate;
	}
	public String getOperatingUnitAffiliate() {
		return operatingUnitAffiliate;
	}
	public void setOperatingUnitAffiliate(String operatingUnitAffiliate) {
		this.operatingUnitAffiliate = operatingUnitAffiliate;
	}
	public String getChartField1() {
		return chartField1;
	}
	public void setChartField1(String chartField1) {
		this.chartField1 = chartField1;
	}
	public String getChartField2() {
		return chartField2;
	}
	public void setChartField2(String chartField2) {
		this.chartField2 = chartField2;
	}
	public String getChartField3() {
		return chartField3;
	}
	public void setChartField3(String chartField3) {
		this.chartField3 = chartField3;
	}
	public String getProjectID() {
		return projectID;
	}
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	public String getBookCode() {
		return bookCode;
	}
	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}
	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
	}
	public String getStatisticsCode() {
		return statisticsCode;
	}
	public void setStatisticsCode(String statisticsCode) {
		this.statisticsCode = statisticsCode;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getAccountingPeriod() {
		return accountingPeriod;
	}
	public void setAccountingPeriod(String accountingPeriod) {
		this.accountingPeriod = accountingPeriod;
	}
	public String getPostedTotalAmount() {
		return postedTotalAmount;
	}
	public void setPostedTotalAmount(String postedTotalAmount) {
		this.postedTotalAmount = postedTotalAmount;
	}
	public String getPostedBaseCurrencyAmount() {
		return postedBaseCurrencyAmount;
	}
	public void setPostedBaseCurrencyAmount(String postedBaseCurrencyAmount) {
		this.postedBaseCurrencyAmount = postedBaseCurrencyAmount;
	}
	public String getPostedTransactionAmount() {
		return postedTransactionAmount;
	}
	public void setPostedTransactionAmount(String postedTransactionAmount) {
		this.postedTransactionAmount = postedTransactionAmount;
	}
	public String getPostedTotalDebitAmount() {
		return postedTotalDebitAmount;
	}
	public void setPostedTotalDebitAmount(String postedTotalDebitAmount) {
		this.postedTotalDebitAmount = postedTotalDebitAmount;
	}
	public String getPostedTotalCreditAmount() {
		return postedTotalCreditAmount;
	}
	public void setPostedTotalCreditAmount(String postedTotalCreditAmount) {
		this.postedTotalCreditAmount = postedTotalCreditAmount;
	}
	public String getPostedTransactionDebitAmount() {
		return postedTransactionDebitAmount;
	}
	public void setPostedTransactionDebitAmount(String postedTransactionDebitAmount) {
		this.postedTransactionDebitAmount = postedTransactionDebitAmount;
	}
	public String getPostedTransactionCreditAmount() {
		return postedTransactionCreditAmount;
	}
	public void setPostedTransactionCreditAmount(
			String postedTransactionCreditAmount) {
		this.postedTransactionCreditAmount = postedTransactionCreditAmount;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}   
	
	
	
	
}
