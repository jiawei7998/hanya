package com.singlee.capital.interfacex.qdb.controller;

import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.interfacex.qdb.service.CreditService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping("qdCreditController")
public class QdCreditController extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	
	@Autowired
	private CreditService creditService;
	
	@RequestMapping("checkUserCreditLimit")
	@ResponseBody
	public RetMsg<Serializable> checkUserCreditLimit(HttpServletRequest request,@RequestBody Map<String, Object> map) throws Exception{
		RetMsg<Serializable> ret = null;
		try {
			SlSession __slSession = SessionStaticUtil.getSlSessionByHttp(request);
			TaUser __sessionUser = SlSessionHelper.getUser(__slSession)==null?new TaUser():SlSessionHelper.getUser(__slSession);
			map.put("userId", __sessionUser.getUserId());
			ret = creditService.checkUserCreditLimit(map);
			
		} catch (Exception e) {
			ret = RetMsgHelper.ok("100", "额度校验异常:"+e.getMessage());
			log.error("额度校验失败:",e);
		}
		return ret;
	}

}
