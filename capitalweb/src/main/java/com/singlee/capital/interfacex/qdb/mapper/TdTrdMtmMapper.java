package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.qdb.model.TdTrdMtm;

public interface TdTrdMtmMapper extends Mapper<TdTrdMtm>{
	
	public int createMtmByDate(Map<String, Object> map);
	
	public List<TdTrdMtm> selectTdTradMtm(Map<String, Object> map);
	
	public List<TdTrdMtm> selectTdTradRevMtm(Map<String, Object> map);
	
	void deleteMtmByDate(Map<String, Object> map);

}
	