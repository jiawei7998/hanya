package com.singlee.capital.interfacex.qdb.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.mapper.StockPriceMapper;
import com.singlee.capital.interfacex.qdb.model.TtStockPrice;
import com.singlee.capital.interfacex.qdb.service.StockService;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

@Service("StockService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class StockServiceImpl implements StockService{

	@Autowired
	private StockPriceMapper stockMapper;
	
	@Override
	public String setStockPriceByExcel(String type, InputStream is){
		try {
			ExcelUtil excelUtil = new ExcelUtil(type, is);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			System.out.println(rowNum);
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
			List<TtStockPrice> stockList = new ArrayList<TtStockPrice>();
			TtStockPrice stockPrice =null;
			Integer a=0;//Integer.valueOf(PropertiesUtil.getProperties("one"));
			Integer b=Integer.valueOf(PropertiesUtil.getProperties("two"));
			Integer c=Integer.valueOf(PropertiesUtil.getProperties("three"));
			stockMapper.deleteStockAll();
			//上海运行的  青岛非标的不是这个
			String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
			
			for (int i = 1; i <= rowNum; i++) {
				stockPrice = new TtStockPrice();
				row = sheet.getRow(i);				
				stockPrice.setStockId(row.getCell(a).getStringCellValue());
				stockPrice.setStockName(row.getCell(b).getStringCellValue());
				stockPrice.setPrice(row.getCell(c).getNumericCellValue());
				stockPrice.setImportDate(nowdate);
				stockList.add(stockPrice);
				if (i%1000 == 0) {
					stockMapper.insertStockExcel(stockList);
					stockList.clear();
				}
			}
			if(stockList.size() > 0) {
				stockMapper.insertStockExcel(stockList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "同步股票价格失败,请检查股票文件或询问技术人员";
		}
		return "导入成功";
	}
	/**
	 * 青岛版本股票导入
	 * 
	 * */
	@Override
	public String insertStockPriceByExcel(String type, InputStream is){
		try {
			ExcelUtil excelUtil = new ExcelUtil(type, is);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			System.out.println(rowNum);
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
//			List<TtStockPrice> stockList = new ArrayList<TtStockPrice>();
			TtStockPrice stockPrice =null;
			Integer a=0;//Integer.valueOf(PropertiesUtil.getProperties("one"));
			Integer b=Integer.valueOf(PropertiesUtil.getProperties("two"));
			Integer c=Integer.valueOf(PropertiesUtil.getProperties("three"));
			String nowdate = DayendDateServiceProxy.getInstance().getSettlementDate();
			
			for (int i = 1; i <= rowNum; i++) {
				stockPrice = new TtStockPrice();
				row = sheet.getRow(i);				
				stockPrice.setStockId(row.getCell(a).getStringCellValue());
				stockPrice.setStockName(row.getCell(b).getStringCellValue());
				stockPrice.setPrice(row.getCell(c).getNumericCellValue());
				stockPrice.setImportDate(nowdate);
				Map<String ,Object> map=new HashMap<String, Object>();
				map.put("stockId", stockPrice.getStockId());
				TtStockPrice newStockPrice=stockMapper.queryStockById(map);
				if(newStockPrice!=null && !"".equals(newStockPrice)){
					stockMapper.updateStock(stockPrice);
				}else{
					stockMapper.addStock(stockPrice);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "同步股票价格失败,请检查股票文件或询问技术人员";
		}
		return "导入成功";
	}
	
	
	
	@Override
	public Page<TtStockPrice> pageStock(Map<String, Object> pageData) {
		Page<TtStockPrice> page = stockMapper.pageStock(pageData,ParameterUtil.getRowBounds(pageData));
		return page;
	}
	
	@Override
	public void updateStock(TtStockPrice tsp) {
		tsp.setImportDate(DayendDateServiceProxy.getInstance().getSettlementDate());
		stockMapper.updateStock(tsp);
	}

	@Override
	public void addStock(TtStockPrice tsp) {
		//新增时先根据股票代码查询，如果有，股票，则不允许新增。
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("stockId", tsp.getStockId());
		TtStockPrice s=stockMapper.queryStockById(map);
		if(s!=null){
			throw new RuntimeException("该股票已经存在");
		}
		tsp.setImportDate(DayendDateServiceProxy.getInstance().getSettlementDate());
		stockMapper.addStock(tsp);
	}

	@Override
	public void deleteStock(TtStockPrice tsp) {
		stockMapper.deleteStock(tsp);
	}

	@Override
	public boolean importStockPrice(String file){
		try {
			ExcelUtil excelUtil = new ExcelUtil(file);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
			List<TtStockPrice> stockList = new ArrayList<TtStockPrice>();
			String nowdate=DayendDateServiceProxy.getInstance().getSettlementDate();
			for (int i = 1; i <= rowNum; i++) 
			{
				TtStockPrice stockPrice = new TtStockPrice();
				row = sheet.getRow(i);
				stockPrice.setStockId(row.getCell(0).getStringCellValue());
				stockPrice.setStockName(row.getCell(1).getStringCellValue());
				stockPrice.setPrice(row.getCell(2).getNumericCellValue());
				stockPrice.setImportDate(nowdate);
				stockList.add(stockPrice);
				if (i%1000 == 0) {
					stockMapper.insertStockExcel(stockList);
					stockList = new ArrayList<TtStockPrice>();
				}
			}
			if(stockList.size() > 0)
			{
				stockMapper.deleteStockAll();
				stockMapper.insertStockExcel(stockList);
			}
		} catch (Exception e) {
			System.out.println("同步股票价格失败");
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public TtStockPrice queryStockById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return stockMapper.queryStockById(map);
	}

}
