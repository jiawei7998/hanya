package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QuotaSyncopateRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QuotaSyncopateRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="RetCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RetInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuotaSyncopateRs", propOrder = { "commonRsHdr", "retCode",
		"retInfo" })
@XmlRootElement(name = "QuotaSyncopateRs")
public class QuotaSyncopateRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "RetCode", required = true)
	protected String retCode;
	@XmlElement(name = "RetInfo", required = true)
	protected String retInfo;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the retCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRetCode() {
		return retCode;
	}

	/**
	 * Sets the value of the retCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRetCode(String value) {
		this.retCode = value;
	}

	/**
	 * Gets the value of the retInfo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRetInfo() {
		return retInfo;
	}

	/**
	 * Sets the value of the retInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRetInfo(String value) {
		this.retInfo = value;
	}

}
