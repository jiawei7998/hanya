package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcctNewtdaAaaRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcctNewtdaAaaRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="SPRsUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExposureDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CntId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcctNewtdaAaaRs", propOrder = { "commonRsHdr", "spRsUID",
		"acctNo", "rate", "processDt", "exposureDt", "companyCode", "cntId" })
public class TDpAcctNewtdaAaaRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "SPRsUID", required = true)
	protected String spRsUID;
	@XmlElement(name = "AcctNo", required = true)
	protected String acctNo;
	@XmlElement(name = "Rate", required = true)
	protected String rate;
	@XmlElement(name = "ProcessDt", required = true)
	protected String processDt;
	@XmlElement(name = "ExposureDt", required = true)
	protected String exposureDt;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;
	@XmlElement(name = "CntId", required = true)
	protected String cntId;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the spRsUID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSPRsUID() {
		return spRsUID;
	}

	/**
	 * Sets the value of the spRsUID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSPRsUID(String value) {
		this.spRsUID = value;
	}

	/**
	 * Gets the value of the acctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * Sets the value of the acctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctNo(String value) {
		this.acctNo = value;
	}

	/**
	 * Gets the value of the rate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * Sets the value of the rate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRate(String value) {
		this.rate = value;
	}

	/**
	 * Gets the value of the processDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProcessDt() {
		return processDt;
	}

	/**
	 * Sets the value of the processDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProcessDt(String value) {
		this.processDt = value;
	}

	/**
	 * Gets the value of the exposureDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExposureDt() {
		return exposureDt;
	}

	/**
	 * Sets the value of the exposureDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExposureDt(String value) {
		this.exposureDt = value;
	}

	/**
	 * Gets the value of the companyCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the value of the companyCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}

	/**
	 * Gets the value of the cntId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCntId() {
		return cntId;
	}

	/**
	 * Sets the value of the cntId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCntId(String value) {
		this.cntId = value;
	}

}
