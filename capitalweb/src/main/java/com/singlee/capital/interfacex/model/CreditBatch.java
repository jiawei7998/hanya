package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class CreditBatch implements Serializable {

	/**
	 *（额度信息同步接口）IFBM->CRMS
	 * 日终批量接口
	 */
	private static final long serialVersionUID = 1L;
	private String ecifNum;
	private String syncopateSys;
	private String userDepartment;
	private String partyNum;
	private String productType;
	private String currencyCd;
	private String creditAmt;
	private String term;
	private String termType;
	private String dueDate;
	private String occupiedAmt;
	private String availableAmt;
	private String crdStatus;
	public String getEcifNum() {
		return ecifNum;
	}
	public void setEcifNum(String ecifNum) {
		this.ecifNum = ecifNum;
	}
	public String getSyncopateSys() {
		return syncopateSys;
	}
	public void setSyncopateSys(String syncopateSys) {
		this.syncopateSys = syncopateSys;
	}
	public String getUserDepartment() {
		return userDepartment;
	}
	public void setUserDepartment(String userDepartment) {
		this.userDepartment = userDepartment;
	}
	public String getPartyNum() {
		return partyNum;
	}
	public void setPartyNum(String partyNum) {
		this.partyNum = partyNum;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(String creditAmt) {
		this.creditAmt = creditAmt;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getOccupiedAmt() {
		return occupiedAmt;
	}
	public void setOccupiedAmt(String occupiedAmt) {
		this.occupiedAmt = occupiedAmt;
	}
	public String getAvailableAmt() {
		return availableAmt;
	}
	public void setAvailableAmt(String availableAmt) {
		this.availableAmt = availableAmt;
	}
	public String getCrdStatus() {
		return crdStatus;
	}
	public void setCrdStatus(String crdStatus) {
		this.crdStatus = crdStatus;
	}
	
}
