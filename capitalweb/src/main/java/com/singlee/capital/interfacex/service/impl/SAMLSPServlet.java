package com.singlee.capital.interfacex.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;



public class SAMLSPServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private String targetURL = "";
	//========================= AxMx session Start =========================
	
	private String AxMxSessionToken="";
	
	//========================= AxMx session End   =========================

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		config.getInitParameter("idpId");
		config.getInitParameter("authnContext");
		config.getInitParameter("samlPropertiesPath");
		this.targetURL = config.getInitParameter("targetURL");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response)
			throws ServletException, IOException {
		String samlResponseParam = req.getParameter("SAMLResponse");
		String samlRelayState = req.getParameter("RelayState");

		if (samlResponseParam != null) {
            acs(req, response, samlResponseParam, samlRelayState);
        } else {
            System.out.println("请求报文为空！");
        }
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	
	private void acs(HttpServletRequest req, HttpServletResponse response,String samlResponseParam, String samlRelayState)
			throws ServletException, IOException {

			try {
			String comingAxMxSessionToken=req.getParameter("AxMxSessionToken");
			if(comingAxMxSessionToken == null){
				comingAxMxSessionToken = (String) req.getSession().getAttribute("AxMxSessionToken");
			}
 		    Map<String,String> attrMap = null;
 		    for(String key:attrMap.keySet()){
 		    	System.out.println("Key+"+key +"-------"+attrMap.get(key).toString());
 		    	if("content".equals(key)){
					/**
					 * 返回字段属性主体事例
					 * EMPLOYEE -> 员工信息
					 * USER -> 用户信息
					 * PRIVILEGE -> 用户权限信息
					 * e.g.
					 * {"EMPLOYEE":{
						  "organId": "BS0000",
						  "createTime": "2016-08-19 14:35:17",
						  "passwordType": "M_FINGER@指纹,M_PASSWORD@密码",
						  "updateTime": "2016-08-19 14:35:17",
						  "status": "1@正常",
						  "userType": "1@本行员工",
						  "id": "000022",
						  "certificatesType": "A@身份证",
						  "certificatesNumber": "789456199605247896",
						  "name": "史传英",
						  "uuid": "772a7455c8b140ac8b9f66c5b095cf03",
						  "organName": "BS0000-上海银行"
						},"USER":{
						  "organId": "YF5301",
						  "appId": "iamgate",
						  "createdTime": "2016-08-22 10:05:49",
						  "updateTime": "2016-08-22 10:05:49",
						  "startTime": "20160822",
						  "name": "Orchid",
						  "userId": "888822",
						  "userStatus": "0@正常",
						  "subUserId": "888822",
						  "uuid": "98764536122c4d65b03c74b01d85c3f3",
						  "organName": "YF5301-南京分行",
						  "createUserId": "000010"
						},"PRIVILEGE":[]}
					 */
					String infoJson = attrMap.get("content");
					System.out.println(infoJson);
 		    }
 		   }
 		   req.setAttribute("userIdentity",attrMap.get("userId").toString());
 		   req.setAttribute("userOrganId",attrMap.get("username").toString());
 		   /**
 		    * 
 		    */
 		   // 3 密码、用户状态校验 （包括登录错误次数、等等）
 			Map<String, Object> map = new HashMap<String, Object>();
 			map.put("empId", StringUtils.trimToEmpty(attrMap.get("userId").toString()));
 			TaUserMapper userMapper = SpringContextHolder.getBean("taUserMapper");
 			List<TaUser> tuList = userMapper.findByEmpId(map);
 			TaUser tu = null;
 			if(!tuList.isEmpty()) {
 				tu = tuList.get(0);
 			}
 			if(null == tu) {
                throw new RException("用戶不存在！");
            }
 		   SlSession session =SessionStaticUtil.createSlSession();
 		   SlSessionHelper.setAnonymity(session,false);
		   SlSessionHelper.setUser(session, tu);
		   SessionStaticUtil.setSessionByHttpSession(req.getSession(), session);
 		   SessionStaticUtil.setSessionByHttp(req,session);
 		   SessionStaticUtil.holdSlSessionThreadLocal(session);
 		  
		   //SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
		   //SlSession session =SessionStaticUtil.getSlSessionByHttp(req);
		  
// 		   req.setAttribute("userPwd",attrMap.get("organId").toString());
// 		   req.setAttribute("userAttributes",attrMap.toString());
// 		   req.setAttribute("userIdentity","sh_admin");
// 		   req.setAttribute("userOrganId","总行管理员");
// 		   req.setAttribute("userPwd","123456");
// 		   req.setAttribute("userAttributes","");
 		   req.setAttribute("AxMxSessionToken", comingAxMxSessionToken);
		   req.getSession().setAttribute("AxMxSessionToken", AxMxSessionToken);
//			//进入各个应用系统登录登陆后成功的页面
			samlRelayState = targetURL;//index_approve.jsp  ssologin_shb.jsp
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(samlRelayState);
			dispatcher.forward(req, response);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
	
	}
 
}