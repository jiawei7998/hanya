package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.DayendInfoRecord;
import com.singlee.capital.interfacex.model.JobExcuLog;

public interface TiJobExcuLogMapper extends Mapper<JobExcuLog>{
		
	/**
	 * 新增任务
	 * @param map
	 * @return
	 */
	public void insertJobExcuLog(JobExcuLog jobExcuLog);
	
	public void updateJobExcuLog(JobExcuLog jobExcuLog);
	
	public int getJobExcuLog(JobExcuLog jobExcuLog);
	
	public List<DayendInfoRecord> getDayendJobStatus(Map<String, String> params);
	
}
