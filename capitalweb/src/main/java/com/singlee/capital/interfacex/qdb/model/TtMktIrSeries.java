package com.singlee.capital.interfacex.qdb.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * TT_MKT_IR_SERIES 市场利率行情历史详细
 * ·
 * @author Libo Create Time 2012-12-19 11:48:52
 */

@Entity
@Table(name = "TT_MKT_IR_SERIES")
public class TtMktIrSeries implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** pkid */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_IR_SERIES.NEXTVAL FROM DUAL")
	private String s_id;

	/**    */
	private String i_code;

	/**    */
	private String a_type;

	/**    */
	private String m_type;

	/** 收盘价 */
	private double s_close;

	/** 最高 */
	private double s_high;

	/** 最低 */
	private double s_low;

	/**    */
	private double s_sell;

	/**    */
	private double s_buy;

	/**    */
	private double s_mid;

	/** 开始日期 */
	private String beg_date;

	/** 结束日期 */
	private String end_date;

	/** 导入日期 */
	private String imp_date;

	/** 来源 */
	private String data_source;

	/**    */
	private String s_bank;
	
	/** 导入时间 */
	private String imp_time;

	/** 经办人 */
	private String handuser;
	
	/** 复核人*/
	private String verifyuser;
	/** 经办备注 */
	private String handmemo;
	/** 复核备注 */
	private String verifymemo;
	
	/** 名称 */
	@Transient
	private String ir_name;
	@Transient
	private String ir_term;

	/** ------- Generate Getter And Setter -------- **/

	public String getS_id() {
		return this.s_id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtMktIrSeries [s_id=");
		builder.append(s_id);
		builder.append(", i_code=");
		builder.append(i_code);
		builder.append(", a_type=");
		builder.append(a_type);
		builder.append(", m_type=");
		builder.append(m_type);
		builder.append(", s_close=");
		builder.append(s_close);
		builder.append(", s_high=");
		builder.append(s_high);
		builder.append(", s_low=");
		builder.append(s_low);
		builder.append(", s_sell=");
		builder.append(s_sell);
		builder.append(", s_buy=");
		builder.append(s_buy);
		builder.append(", s_mid=");
		builder.append(s_mid);
		builder.append(", beg_date=");
		builder.append(beg_date);
		builder.append(", end_date=");
		builder.append(end_date);
		builder.append(", imp_date=");
		builder.append(imp_date);
		builder.append(", data_source=");
		builder.append(data_source);
		builder.append(", s_bank=");
		builder.append(s_bank);
		builder.append(", imp_time=");
		builder.append(imp_time);
		builder.append("]");
		return builder.toString();
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getI_code() {
		return this.i_code;
	}

	public void setI_code(String i_code) {
		this.i_code = i_code;
	}

	public String getA_type() {
		return this.a_type;
	}

	public void setA_type(String a_type) {
		this.a_type = a_type;
	}

	public String getM_type() {
		return this.m_type;
	}

	public void setM_type(String m_type) {
		this.m_type = m_type;
	}

	public double getS_close() {
		return this.s_close;
	}

	public void setS_close(double s_close) {
		this.s_close = s_close;
	}

	public double getS_high() {
		return this.s_high;
	}

	public void setS_high(double s_high) {
		this.s_high = s_high;
	}

	public double getS_low() {
		return this.s_low;
	}

	public void setS_low(double s_low) {
		this.s_low = s_low;
	}

	public double getS_sell() {
		return s_sell;
	}

	public void setS_sell(double s_sell) {
		this.s_sell = s_sell;
	}

	public double getS_buy() {
		return s_buy;
	}

	public void setS_buy(double s_buy) {
		this.s_buy = s_buy;
	}

	public double getS_mid() {
		return this.s_mid;
	}

	public void setS_mid(double s_mid) {
		this.s_mid = s_mid;
	}

	public String getBeg_date() {
		return this.beg_date;
	}

	public void setBeg_date(String beg_date) {
		this.beg_date = beg_date;
	}

	public String getEnd_date() {
		return this.end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getImp_date() {
		return this.imp_date;
	}

	public void setImp_date(String imp_date) {
		this.imp_date = imp_date;
	}

	public String getData_source() {
		return this.data_source;
	}

	public void setData_source(String data_source) {
		this.data_source = data_source;
	}

	public String getS_bank() {
		return this.s_bank;
	}

	public void setS_bank(String s_bank) {
		this.s_bank = s_bank;
	}

	public String getImp_time() {
		return this.imp_time;
	}

	public void setImp_time(String imp_time) {
		this.imp_time = imp_time;
	}

	public String getIr_name() {
		return ir_name;
	}

	public void setIr_name(String ir_name) {
		this.ir_name = ir_name;
	}

	public String getIr_term() {
		return ir_term;
	}

	public void setIr_term(String ir_term) {
		this.ir_term = ir_term;
	}

	public String getHanduser() {
		return handuser;
	}

	public void setHanduser(String handuser) {
		this.handuser = handuser;
	}

	public String getVerifyuser() {
		return verifyuser;
	}

	public void setVerifyuser(String verifyuser) {
		this.verifyuser = verifyuser;
	}

	public String getHandmemo() {
		return handmemo;
	}

	public void setHandmemo(String handmemo) {
		this.handmemo = handmemo;
	}

	public String getVerifymemo() {
		return verifymemo;
	}

	public void setVerifymemo(String verifymemo) {
		this.verifymemo = verifymemo;
	}
}