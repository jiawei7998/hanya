package com.singlee.capital.interfacex.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.CreditBatch;
import com.singlee.capital.interfacex.model.GLBatch;
import com.singlee.capital.interfacex.model.GvatXx;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.model.TExLdRpmSchInq;
import com.singlee.capital.interfacex.model.TExReconAllInqRec;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.interfacex.model.TsaAccountView;
import com.singlee.capital.interfacex.model.UPPSChkFileEndDayBatch;
import com.singlee.capital.system.model.TtInstitution;


public interface TiInterfaceInfoMapper {
	
	public void insertTiInterfaceInfo(TiInterfaceInfo interfaceInfo);

	public void updateTiInterfaceInfo(TiInterfaceInfo interfaceInfo);
	
	public void deleteTiInterfaceInfoById(Map<String, Object> map);
	
	public Page<TiInterfaceInfo> selectTiInterfaceInfoByName(Map<String, Object> map);
	
	public TiInterfaceInfo queryByInterfaceName(String inface_name);
	
	public void insertTExReconAllInq(TExReconAllInqRec texReconAllInqRec);
	
	public Page<Root> selectTiTsaAccountByTranDate(Map<String, Object> map, RowBounds rb);
	public Page<TsaAccountView> selectTsaAccountViewByTran(Map<String, Object> map, RowBounds rb);
	void insertTiTsaAccount(Root root);
	void updateTiTsaAccountByStatus(Map<String, Object> map);
	public List<GLBatch> queryGLBatchAcupList(Map<String, Object> map) throws Exception;
	public List<Root> searchTiTsaAccountByTno(Map<String, Object> map);
	void updateTiTsaAccountStatusByTno (Map<String, Object> map);
	
	
	public void insertQuotaSyncopate(QuotaSyncopateRec quotaSyncopateRec) throws Exception;
	
	
	public TtInstitution queryTtinst(Map<String, Object> map) throws Exception;
	
	public List<CreditBatch> queryCreditBatch() throws Exception;
	
	public void insertUPPSChkFile(UPPSChkFileEndDayBatch uppsChkFileEndDayBatch) throws Exception;
	
	public void insertLdRpm(TExLdRpmSchInq tExLdRpmSchInq) throws Exception;
	
	public List<TExLdRpmSchInq> queryLdRpmIdno(String ID);
	
	public void deleteLdRpmById(Map<String, Object> map);
	
	public TExLdRpmSchInq queryLdRpmByIdAndIdNo(Map<String, Object> map);
	
	void updateLdRpmByIdAndIdNo(TExLdRpmSchInq tExLdRpmSchInq);

	public List<GLBatch> queryT24TransforSubjBalList(Map<String, Object> map) throws Exception;
	
	public List<GvatXx> queryGvatXxList(Map<String, Object> map) throws Exception;
	/**
	 * 根据LDNO 查询
	 * @param map
	 * @return
	 */
	public List<TExLdRpmSchInq> queryLdRpmByLdNos(String id);
	
	public List<Root> searchTiTsaAccountByTflow(Map<String, Object> map);
}
