package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;
import com.singlee.capital.interfacex.qdb.pojo.FBZ004;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.controller.CommonController;
//客户额度释放交易
@Controller
@RequestMapping("fbz004")
public class FBZ004Controller extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private EsbClientService esbClientService;
	@RequestMapping("getFbz004")
	@ResponseBody
	public RetMsg<List<Object>> getResp(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ004 fbz) throws Exception{
		fbz.setMfCustomerId((String)map.get("cif"));
		fbz.setLmtID((String)map.get("lmtNo"));
		List<Object> list = sendReq(request,fbz);
		
		return RetMsgHelper.ok(list);
	}
	
	public List<Object> sendReq(HttpServletRequest request,FBZ004 fbz) throws Exception{
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ004.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		List<Object> list=null;
		String msg="";
		try {
			msg=parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ004返回报文"+xmlDocAccept.asXML());
		        	list= XmlUtilFactory.getBeansByXml(FBZ004.class, xmlDocAccept.asXML(),"circularField");
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return list;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz004接口："+e);
			return list;
		}
		return list;
	}

	public String parseXml(HttpServletRequest request,FBZ004 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		if(fbz!=null){
			svc.put("bizDate",fbz.getBizDate()!=null?fbz.getBizDate():"");
			svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
			svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
			svc.put("tmStamp", fbz.getTmStamp()!=null?fbz.getTmStamp():"");
			hashmap.put("appHdr", svc);
			body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
			body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
			body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
			body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
			body.put("lmtID", fbz.getLmtID()!=null?fbz.getLmtID():"");
			hashmap.put("appBody", body);
		}
		return XmlUtilFactory.getXml(request.getSession().getServletContext().getRealPath("/")+ "/WEB-INF/classes/schema/FBZC0004.xml", hashmap);
	}
}
