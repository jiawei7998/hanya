package com.singlee.capital.interfacex.qdb.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.impl.ProductServiceImpl;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.service.impl.CounterPartyServiceImpl;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.flow.controller.FlowController;
import com.singlee.capital.print.service.AssemblyDataService;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdTradeExpireMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTradeExpire;
@Service("durationTradeExpServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintDurationExpireServiceImpl implements AssemblyDataService{
	@Autowired
	private FlowController flowController; 
	@Autowired
	private TdTradeExpireMapper TdTradeExpireMapper;
	@Autowired
	private TtInstitutionMapper mapper;
	@Autowired
	private TaUserMapper taUserMapper;
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	private CounterPartyServiceImpl counterPartyServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	
	@Autowired
	private DayendDateService dayendDateService;
	
	@Override
	public Map<String, Object> getData(String id) {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			NumberFormat n = NumberFormat.getInstance();
			DecimalFormat df=new DecimalFormat("0.0000");
			NumberFormat nf =NumberFormat.getPercentInstance();
			nf.setMinimumFractionDigits(4);
			TdTradeExpire expire=TdTradeExpireMapper.getTradeExpireById(id);
			String dealNo=expire.getRefNo();
			map.put("expire.refNo", expire.getRefNo());
			map.put("dealNo", dealNo);
			TdProductApproveMain td = tdProductApproveMainMapper.getProductApproveMain(map);
			map.put("dealNo",td.getDealNo());
			map.put("contractRate", df.format(100*td.getContractRate()));
			map.put("vDate", td.getvDate());
			map.put("mDate",td.getmDate());
			map.put("occupTerm",td.getOccupTerm());
			String cno=td.getcNo();
			CounterPartyVo cp = counterPartyServiceImpl.selectCP(cno, null);
			map.put("party_name", cp.getParty_name());
			Integer pro=td.getPrdNo();
			if(pro!=null&&!"".equals(pro)){
				TcProduct p=productServiceImpl.getProductById(String.valueOf(pro));
				map.put("productName", p.getPrdName());
			}
			//取原交易的经办人
			String Sponsor2=td.getSponsor();
			TaUser u=taUserMapper.selectUser(Sponsor2);
			map.put("sponsor",u.getUserName());
			
			
			//取存续期的经办人
			String Sponsor=expire.getSponsor();
			TaUser u2=taUserMapper.selectUser(Sponsor);
			map.put("sponsor2",u2.getUserName());
			String inst=u2.getInstId();
			
			TtInstitution institution = new TtInstitution();
			if(inst!=null&&!"".equals(inst)){
				institution.setInstId(inst);
				institution = mapper.selectByPrimaryKey(institution);
				map.put("Inst",institution.getInstName());
			}	
			String rateType=td.getRateType();
			if("1".endsWith(rateType)){
				map.put("rateType", "固息");
			}else if("2".endsWith(rateType)){
				map.put("rateType", "浮息");
			}else if("3".endsWith(rateType)){
				map.put("rateType", "净值");
			}
			map.put("amt", n.format(td.getAmt()));
			map.put("basis", td.getBasis());
			map.put("mInt", n.format(td.getmInt()));
			map.put("mAmt", n.format(td.getmAmt()));
			map.put("SysDate", dayendDateService.getSettlementDate());
			
			map.put("remainAmt", n.format(expire.getRemainAmt()));
			map.put("expMDate", expire.getExpMDate());
			map.put("tDate", td.gettDate());
			map.put("expMeDate", expire.getExpMeDate());
			map.put("expTerm", expire.getExpTerm());
			map.put("expOccupTerm", expire.getExpOccupTerm());
			map.put("expMInt", expire.getExpMInt());
			map.put("expMAmt", expire.getExpMAmt());
			
			map.put("expRate", df.format(100*expire.getExpRate()));
			map.put("aDate", expire.getaDate());
			map.put("reason", expire.getReason());
			
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("serial_no", id);
			RetMsg<List<Map<String, Object>>> ret = flowController.approveLog(map1);
			if(ret != null){
				List<Map<String, Object>> approveList = ret.getObj();
				if(approveList!= null && approveList.size() >= 1){
					Map<String, Object> approveMap1 = approveList.get(1);
					//oper1 = approveMap1.getActivityName();
					String oper1 =(String) approveMap1.get("Assignee");
					TaUser user2=taUserMapper.selectUser(oper1);
					map.put("oper2", user2.getUserName());
				}

				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

}
