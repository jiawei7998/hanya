package com.singlee.capital.interfacex.qdb.ESB.dao.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.singlee.capital.interfacex.qdb.ESB.dao.HttpClientManagerDao;
import com.singlee.capital.interfacex.qdb.util.PropertiesUtil;

import org.springframework.stereotype.Component;

@Component
public class HttpClientManagerDaoImpl implements HttpClientManagerDao {
	
	//连接URL超时时间
	private static int connectionTimeout = 10000;
	//连接URL后等待返回超时时间
	private static int soTimeout = 60000;
	
	static
	{
		connectionTimeout = Integer.parseInt(PropertiesUtil.getProperties("ESB.ConnectionTimeout", "10000"));
		soTimeout = Integer.parseInt(PropertiesUtil.getProperties("ESB.SoTimeout", "60000"));
	}
	
	private  HttpClient httpclient;   
    public void setHttpclient(HttpClient httpclient) {   
        this.httpclient = httpclient;   
    }  
	/**
	 * @return the httpclient
	 */
	public HttpClient getHttpclient() {
		return httpclient;
	}
	/**  
     * 发送Xml报文,并拿到核心的返回报文
     * @return  
     */  
	@Override
	public HashMap<String, Object> sendXmlToRequest(String url, String xmlString)throws Exception {
		HttpClient httpclient = new HttpClient();
		PostMethod postMethod = null;
		InputStream inputStream = null;
		HashMap<String, Object> resultHashMap = new HashMap<String, Object>();
		try {
			//设置连接参数
			httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(connectionTimeout);
			httpclient.getHttpConnectionManager().getParams().setSoTimeout(soTimeout);
			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(new StringRequestEntity(xmlString, "text/xml","UTF-8"));
			
			//postMethod.setRequestBody(new NameValuePair[]{name}); 
			// 设置成了默认的恢复策略，在发生异常时候将自动重试3次，在这里你也可以设置成自定义的恢复策略 
			postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());
			// 执行executeMethod 
			int statusCode = httpclient.executeMethod(postMethod);
			resultHashMap.put("statusCode", statusCode);
			//拿到核心返回的数据通道
			inputStream = postMethod.getResponseBodyAsStream();
			byte[] str = new byte[1];
			int  i = inputStream.read(str);
			List<Byte> list = new ArrayList<Byte>();
			list.add(str[0]);
			//拿到通道里的数据
			while(i!=-1 && inputStream.available()!=0){
				i = inputStream.read(str);
				list.add(str[0]);
			}
			//把数据封装到byte[]里
			byte[] total = new byte[list.size()];
			int count = 0;
			for(byte a : list){
				total[count++] = a;
			}
			resultHashMap.put("Bytes", total); //中文环境
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
			if(null != inputStream)
			{
				inputStream.close();
			}
		}
		//返回访问状态和报文参数
		return resultHashMap;
	}
	
	/**
	 * 将返回报文插入到数据库
	 * @param List<Writtengrarantee> listw
	 * @return Boolean
	 */
	/*
	@Override
	public boolean addFx5002(final List<Writtengrarantee> listw) throws Exception {		 
		super.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			int res= 0; 
			@Override
			public Object doInSqlMapClient(SqlMapExecutor executor)throws SQLException { 
	 			 executor.startBatch();
				 for (Writtengrarantee quotecur:listw) { 
					res = executor.update("Writtengrarantee.addwrittengrarantee", quotecur); 
				}
				 executor.executeBatch(); 
				return res>0?true:false; 
			}
		});  
		return false;
	}
	
	@Override
	public List<Writtengrarantee> queryWrittengrarantee() throws Exception {
		return super.getSqlMapClientTemplate().queryForList("Writtengrarantee.querywrigroup");  
	}
	@Override
	public List<Writtengrarantee> queryWrittengraranteeBytime(Page page)throws Exception {
		return  super.getSqlMapClientTemplate().queryForList("Writtengrarantee.querywrigroupbytime",page);
	}
	
	 */
	 
} 
