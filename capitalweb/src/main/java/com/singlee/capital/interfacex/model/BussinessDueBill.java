package com.singlee.capital.interfacex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="TI_BUSINESSDUEBILL")
public class BussinessDueBill {
	@Id
	@Column(name="SerialNo")
	private String SerialNo;
	@Column(name="RelativeSerialNo1")
	private String RelativeSerialNo1;
	@Column(name="BusinessCurrency")
	private String BusinessCurrency;
	@Column(name="SubjectNo")
	private String SubjectNo;
	@Column(name="SubjectNo_130_01")
	private String SubjectNo_130_01;
	@Column(name="SubjectNo_130_02")
	private String SubjectNo_130_02;
	@Column(name="SubjectNo_130_03")
	private String SubjectNo_130_03;
	@Column(name="SubjectNo_130_04")
	private String SubjectNo_130_04;
	@Column(name="BusinessSum")
	private String BusinessSum;
	@Column(name="DiscountSum")
	private String DiscountSum;
	@Column(name="BusinessRate")
	private String BusinessRate;
	@Column(name="Balance")
	private String Balance;
	@Column(name="NormalBalance")
	private String NormalBalance;
	@Column(name="OverDueBalance")
	private String OverDueBalance;
	@Column(name="OverDueBalance3")
	private String OverDueBalance3;
	@Column(name="SlowDueBalance")
	private String SlowDueBalance;
	@Column(name="BadDueBalance")
	private String BadDueBalance;
	@Column(name="InterestBalance1")
	private String InterestBalance1;
	@Column(name="InterestBalance2")
	private String InterestBalance2;
	@Column(name="Not_Amortised_Int_Amt")
	private String Not_Amortised_Int_Amt;
	@Column(name="FinishDate")
	private String FinishDate;
	@Column(name="AdvanceFlag")
	private String AdvanceFlag;
	@Column(name="MainOrgID")
	private String MainOrgID;
	@Column(name="PaymentType")
	private String PaymentType;
	@Column(name="Op_Type_Ind")
	private String Op_Type_Ind;
	@Column(name="Consol_Key")
	private String Consol_Key;
	@Column(name="Product_Code")
	private String Product_Code;
	@Column(name="Rediscount_Status")
	private String Rediscount_Status;
	@Column(name="DP_BAL")
	private String DP_BAL;
	@Column(name="PS_RATE")
	private String PS_RATE;
	@Column(name="PE_RATE")
	private String PE_RATE;
	@Column(name="Prin_Payment_Days")
	private String Prin_Payment_Days;
	@Column(name="Intr_Payment_Days")
	private String Intr_Payment_Days;
	@Column(name="Ar_Intr")
	private String Ar_Intr;
	@Column(name="Loan_Recov_Type")
	private String Loan_Recov_Type;
	@Column(name="RelativeSerialNo2")
	private String RelativeSerialNo2;
	@Column(name="MainCustomerID")
	private String MainCustomerID;
	@Column(name="MainCorpID")
	private String MainCorpID;
	@Column(name="CustomerName")
	private String CustomerName;
	@Column(name="ActualPutoutDate")
	private String ActualPutoutDate;
	@Column(name="ActualMaturity")
	private String ActualMaturity;
	 
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getRelativeSerialNo1() {
		return RelativeSerialNo1;
	}


	public void setRelativeSerialNo1(String relativeSerialNo1) {
		RelativeSerialNo1 = relativeSerialNo1;
	}


	public String getBusinessCurrency() {
		return BusinessCurrency;
	}


	public void setBusinessCurrency(String businessCurrency) {
		BusinessCurrency = businessCurrency;
	}


	public String getSubjectNo() {
		return SubjectNo;
	}


	public void setSubjectNo(String subjectNo) {
		SubjectNo = subjectNo;
	}


	public String getSubjectNo_130_01() {
		return SubjectNo_130_01;
	}


	public void setSubjectNo_130_01(String subjectNo_130_01) {
		SubjectNo_130_01 = subjectNo_130_01;
	}


	public String getSubjectNo_130_02() {
		return SubjectNo_130_02;
	}


	public void setSubjectNo_130_02(String subjectNo_130_02) {
		SubjectNo_130_02 = subjectNo_130_02;
	}


	public String getSubjectNo_130_03() {
		return SubjectNo_130_03;
	}


	public void setSubjectNo_130_03(String subjectNo_130_03) {
		SubjectNo_130_03 = subjectNo_130_03;
	}


	public String getSubjectNo_130_04() {
		return SubjectNo_130_04;
	}


	public void setSubjectNo_130_04(String subjectNo_130_04) {
		SubjectNo_130_04 = subjectNo_130_04;
	}


	public String getBusinessSum() {
		return BusinessSum;
	}


	public void setBusinessSum(String businessSum) {
		BusinessSum = businessSum;
	}


	public String getDiscountSum() {
		return DiscountSum;
	}


	public void setDiscountSum(String discountSum) {
		DiscountSum = discountSum;
	}


	public String getBusinessRate() {
		return BusinessRate;
	}


	public void setBusinessRate(String businessRate) {
		BusinessRate = businessRate;
	}


	public String getBalance() {
		return Balance;
	}


	public void setBalance(String balance) {
		Balance = balance;
	}


	public String getNormalBalance() {
		return NormalBalance;
	}


	public void setNormalBalance(String normalBalance) {
		NormalBalance = normalBalance;
	}


	public String getOverDueBalance() {
		return OverDueBalance;
	}


	public void setOverDueBalance(String overDueBalance) {
		OverDueBalance = overDueBalance;
	}


	public String getOverDueBalance3() {
		return OverDueBalance3;
	}


	public void setOverDueBalance3(String overDueBalance3) {
		OverDueBalance3 = overDueBalance3;
	}


	public String getSlowDueBalance() {
		return SlowDueBalance;
	}


	public void setSlowDueBalance(String slowDueBalance) {
		SlowDueBalance = slowDueBalance;
	}


	public String getBadDueBalance() {
		return BadDueBalance;
	}


	public void setBadDueBalance(String badDueBalance) {
		BadDueBalance = badDueBalance;
	}


	public String getInterestBalance1() {
		return InterestBalance1;
	}


	public void setInterestBalance1(String interestBalance1) {
		InterestBalance1 = interestBalance1;
	}


	public String getInterestBalance2() {
		return InterestBalance2;
	}


	public void setInterestBalance2(String interestBalance2) {
		InterestBalance2 = interestBalance2;
	}


	public String getNot_Amortised_Int_Amt() {
		return Not_Amortised_Int_Amt;
	}


	public void setNot_Amortised_Int_Amt(String notAmortisedIntAmt) {
		Not_Amortised_Int_Amt = notAmortisedIntAmt;
	}


	public String getFinishDate() {
		return FinishDate;
	}


	public void setFinishDate(String finishDate) {
		FinishDate = finishDate;
	}


	public String getAdvanceFlag() {
		return AdvanceFlag;
	}


	public void setAdvanceFlag(String advanceFlag) {
		AdvanceFlag = advanceFlag;
	}


	public String getMainOrgID() {
		return MainOrgID;
	}


	public void setMainOrgID(String mainOrgID) {
		MainOrgID = mainOrgID;
	}


	public String getPaymentType() {
		return PaymentType;
	}


	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}


	public String getOp_Type_Ind() {
		return Op_Type_Ind;
	}


	public void setOp_Type_Ind(String opTypeInd) {
		Op_Type_Ind = opTypeInd;
	}


	public String getConsol_Key() {
		return Consol_Key;
	}


	public void setConsol_Key(String consolKey) {
		Consol_Key = consolKey;
	}


	public String getProduct_Code() {
		return Product_Code;
	}


	public void setProduct_Code(String productCode) {
		Product_Code = productCode;
	}


	public String getRediscount_Status() {
		return Rediscount_Status;
	}


	public void setRediscount_Status(String rediscountStatus) {
		Rediscount_Status = rediscountStatus;
	}


	public String getDP_BAL() {
		return DP_BAL;
	}


	public void setDP_BAL(String dPBAL) {
		DP_BAL = dPBAL;
	}


	public String getPS_RATE() {
		return PS_RATE;
	}


	public void setPS_RATE(String pSRATE) {
		PS_RATE = pSRATE;
	}


	public String getPE_RATE() {
		return PE_RATE;
	}


	public void setPE_RATE(String pERATE) {
		PE_RATE = pERATE;
	}


	public String getPrin_Payment_Days() {
		return Prin_Payment_Days;
	}


	public void setPrin_Payment_Days(String prinPaymentDays) {
		Prin_Payment_Days = prinPaymentDays;
	}


	public String getIntr_Payment_Days() {
		return Intr_Payment_Days;
	}


	public void setIntr_Payment_Days(String intrPaymentDays) {
		Intr_Payment_Days = intrPaymentDays;
	}


	public String getAr_Intr() {
		return Ar_Intr;
	}


	public void setAr_Intr(String arIntr) {
		Ar_Intr = arIntr;
	}


	public String getLoan_Recov_Type() {
		return Loan_Recov_Type;
	}


	public void setLoan_Recov_Type(String loanRecovType) {
		Loan_Recov_Type = loanRecovType;
	}


	public String getRelativeSerialNo2() {
		return RelativeSerialNo2;
	}


	public void setRelativeSerialNo2(String relativeSerialNo2) {
		RelativeSerialNo2 = relativeSerialNo2;
	}


	public String getMainCustomerID() {
		return MainCustomerID;
	}


	public void setMainCustomerID(String mainCustomerID) {
		MainCustomerID = mainCustomerID;
	}


	public String getMainCorpID() {
		return MainCorpID;
	}


	public void setMainCorpID(String mainCorpID) {
		MainCorpID = mainCorpID;
	}


	public String getCustomerName() {
		return CustomerName;
	}


	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}


	public String getActualPutoutDate() {
		return ActualPutoutDate;
	}


	public void setActualPutoutDate(String actualPutoutDate) {
		ActualPutoutDate = actualPutoutDate;
	}


	public String getActualMaturity() {
		return ActualMaturity;
	}


	public void setActualMaturity(String actualMaturity) {
		ActualMaturity = actualMaturity;
	}


	@Override
	public String toString() {
		return "BussinessDueBill [SerialNo=" + SerialNo
				+ ", RelativeSerialNo1=" + RelativeSerialNo1
				+ ", BusinessCurrency=" + BusinessCurrency + ", SubjectNo="
				+ SubjectNo + ", SubjectNo_130_01=" + SubjectNo_130_01
				+ ", SubjectNo_130_02=" + SubjectNo_130_02
				+ ", SubjectNo_130_03=" + SubjectNo_130_03
				+ ", SubjectNo_130_04=" + SubjectNo_130_04 + ", BusinessSum="
				+ BusinessSum + ", DiscountSum=" + DiscountSum
				+ ", BusinessRate=" + BusinessRate + ", Balance=" + Balance
				+ ", NormalBalance=" + NormalBalance + ", OverDueBalance="
				+ OverDueBalance + ", OverDueBalance3=" + OverDueBalance3
				+ ", SlowDueBalance=" + SlowDueBalance + ", BadDueBalance="
				+ BadDueBalance + ", InterestBalance1=" + InterestBalance1
				+ ", InterestBalance2=" + InterestBalance2
				+ ", Not_Amortised_Int_Amt=" + Not_Amortised_Int_Amt
				+ ", FinishDate=" + FinishDate + ", AdvanceFlag=" + AdvanceFlag
				+ ", MainOrgID=" + MainOrgID + ", PaymentType=" + PaymentType
				+ ", Op_Type_Ind=" + Op_Type_Ind + ", Consol_Key=" + Consol_Key
				+ ", Product_Code=" + Product_Code + ", Rediscount_Status="
				+ Rediscount_Status + ", DP_BAL=" + DP_BAL + ", PS_RATE="
				+ PS_RATE + ", PE_RATE=" + PE_RATE + ", Prin_Payment_Days="
				+ Prin_Payment_Days + ", Intr_Payment_Days="
				+ Intr_Payment_Days + ", Ar_Intr=" + Ar_Intr
				+ ", Loan_Recov_Type=" + Loan_Recov_Type
				+ ", RelativeSerialNo2=" + RelativeSerialNo2
				+ ", MainCustomerID=" + MainCustomerID + ", MainCorpID="
				+ MainCorpID + ", CustomerName=" + CustomerName
				+ ", ActualPutoutDate=" + ActualPutoutDate
				+ ", ActualMaturity=" + ActualMaturity + "]";
	}
	
	
	
}
