package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRec;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class Payment2ndUPPSigBusiQueryQuartzJob implements CronRunnable {

	/**
	 * 大额二代支付轮训查询更新状态JOB
	 * 筛选处理结果为payresult为1,3的交易（1-处理中 3-受理成功） retcode为000000的交易 因为其他错误码为其他的交易都是进行线下划款
	 * 根据反馈进行数据更新
	 */
	private SocketClientService socketClientService = SpringContextHolder.getBean("socketClientService");

	private Ti2ndPaymentMapper ti2ndPaymentMapper = SpringContextHolder.getBean("ti2ndPaymentMapper");
	
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("workdate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		map.put("userdefinetrancode", InterfaceCode.TI2ND_UPPSCdtTrf);
		List<UPPSCdtTrfRq> list = ti2ndPaymentMapper.payment2ndUPPSigBusiQueryQuartzJob(map);

		for(UPPSCdtTrfRq rq : list){
			UPPSigBusiQueryRq request = new UPPSigBusiQueryRq();
			request.setUserDefineTranCode(InterfaceCode.TI2ND_QueryUserDefineTranCode);
			request.setOrigChannelCode(InterfaceCode.TI_CHANNELNO);
			request.setOrigChannelDate(map.get("workdate").toString().replace("-", ""));
			request.setOrigFeeBaseLogId(rq.getFeeBaseLogId());
			UPPSigBusiQueryRs response = socketClientService.T2ndPaymentUPPSigBusiQueryRequest(request);
			for(UPPSigBusiQueryRec rec : response.getUPPSigBusiQueryRec()){
				if(rec.getFeeBaseLogId().trim().endsWith(rq.getFeeBaseLogId())){
					Map<String, Object> map1 = new HashMap<String, Object>();
					map1.put("retcode", rec.getCenterDealCode());
					map1.put("retmsg", rec.getCenterDealMsg());
					map1.put("rquid",rec.getChannelSerNo());
					map1.put("agentserialno", rec.getAgentSerialNo());
					map1.put("centerdealcode", rec.getCenterDealCode());
					map1.put("centerdealmsg", rec.getCenterDealMsg());
					map1.put("payresult", rec.getPayResult());
					map1.put("workdate", rec.getWorkDate());
					map1.put("bankdate", rec.getBankDate());
					map1.put("bankid", rec.getBankId());
					map1.put("feeBaseLogid", rq.getFeeBaseLogId());
					String srquid = ti2ndPaymentMapper.queryTi2ndPayment2(map1).getSrquid();
					map1.put("srquid", srquid);
					ti2ndPaymentMapper.update2ndPaymentRetMsg(map1);				
				}
			}
//			Thread.sleep(1000);
		}
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
