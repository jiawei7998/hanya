package com.singlee.capital.interfacex.qdb.model;

import java.io.IOException;
import java.io.Serializable;

public class AcupXmlBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int no;//���к�

	private String fieldTag;//����

	private int position;//��ʼλ��

	private String dataType;//�������

	private int length;//����

	private String mod;//�Ƿ�ؼ���

	private String leftOrRight;//����仹�������

	private String fillChar;//����ַ�

	private String value;//ֵ

	private String description;//����

	private int decimalsNo;//С��λ��

	public AcupXmlBean() {
	}

	public AcupXmlBean(int no, String fieldTag, int position, String dataType,
			int length, String mod, String leftOrRight, String fillChar,
			String value, String description, int decimalsNo) {
		super();
		this.no = no;
		this.fieldTag = fieldTag;
		this.position = position;
		this.dataType = dataType;
		this.length = length;
		this.mod = mod;
		this.leftOrRight = leftOrRight;
		this.fillChar = fillChar;
		this.value = value;
		this.description = description;
		this.decimalsNo = decimalsNo;
	}

	/**
	 * @return the no
	 */
	public int getNo() {
		return no;
	}

	/**
	 * @param no
	 *            the no to set
	 */
	public void setNo(int no) {
		this.no = no;
	}

	/**
	 * @return the fieldTag
	 */
	public String getFieldTag() {
		return fieldTag;
	}

	/**
	 * @param fieldTag
	 *            the fieldTag to set
	 */
	public void setFieldTag(String fieldTag) {
		this.fieldTag = fieldTag;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType
	 *            the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the mod
	 */
	public String getMod() {
		return mod;
	}

	/**
	 * @param mod
	 *            the mod to set
	 */
	public void setMod(String mod) {
		this.mod = mod;
	}

	/**
	 * @return the leftOrRight
	 */
	public String getLeftOrRight() {
		return leftOrRight;
	}

	/**
	 * @param leftOrRight
	 *            the leftOrRight to set
	 */
	public void setLeftOrRight(String leftOrRight) {
		this.leftOrRight = leftOrRight;
	}

	/**
	 * @return the fillChar
	 */
	public String getFillChar() {
		return fillChar;
	}

	/**
	 * @param fillChar
	 *            the fillChar to set
	 */
	public void setFillChar(String fillChar) {
		this.fillChar = fillChar;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @param inParam1
	 *            ��Ҫ������ַ�
	 * @param len
	 *            ���뵽�ĳ���
	 * @param align
	 *            ���뷽ʽ
	 * @param flag
	 *            �����ַ�
	 * @return
	 * @throws IOException 
	 */
	public String getFormatStr(String inParam1, int len, String align,
			String flag) throws IOException {
		String result = "";
		String inParam = "";
		try {
			if (inParam1 == null) {
				inParam1 = " ";}
			//������Խ����ַ��ת�� ʹ���ַ�ͳһ
			inParam = inParam1;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (inParam.length() < len) {
			//System.out.println(inParam.length());
			if ("R".equalsIgnoreCase(align)) {
				for(int i=0;i<len-inParam.length();i++){
					result += flag;
				}
				result = inParam+result;
			} else {
				for(int i=0;i<len-inParam.length();i++){
					result += flag;
				}
				result = result+inParam;
			}
		} else {
			result = inParam;
		}
		return result;
	}

	public String format() throws Exception {
		if ("A".equalsIgnoreCase(this.dataType)) {
			if(null == this.value || "".equals(this.value))
			{
				this.value = " ";
			}
			return getFormatStr(this.value, this.length, this.leftOrRight,
					this.fillChar);
		} else if ("S".equalsIgnoreCase(this.dataType)) {
			if(this.value == null || "".equals(this.value)) { this.value = "0";}
			String left = "";
			String right = "";
			if(this.value.indexOf(".") != -1)
			{
			 left = this.value.substring(0, this.value.indexOf("."));
			 right = this.value.substring(this.value.indexOf(".") + 1,
					this.value.length());
			}else {
				left = this.value;
			}
			return getFormatStr(left, this.length - this.decimalsNo,
					this.leftOrRight, this.fillChar)
					+ getFormatStr(right, this.decimalsNo, "L"
							.equalsIgnoreCase(this.leftOrRight) ? "R" : "L", this.fillChar);
		}
		return "";
	}

	/**
	 * ��ȡ�ַ��е������ַ���
	 * 
	 * @param o
	 * @return
	 */
	public int getChineseCharLen(Object o) {

		String str = (String) o;

		int count = 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) < 0 || str.charAt(i) > 255) {
				count += 1;
			}
		}

		return count;
	}

	public static void main(String[] args) throws Exception {

		System.out.println(new AcupXmlBean(1, "GLRECTYP", 1, "A", 19, "M", "L", "0",
				"298755892528", "abx", 0).format());

	}

	/**
	 * @return the decimalsNo
	 */
	public int getDecimalsNo() {
		return decimalsNo;
	}

	/**
	 * @param decimalsNo  the decimalsNo to set
	 */
	public void setDecimalsNo(int decimalsNo) {
		this.decimalsNo = decimalsNo;
	}
	
}
