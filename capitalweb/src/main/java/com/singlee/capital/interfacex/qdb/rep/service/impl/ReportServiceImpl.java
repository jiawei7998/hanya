package com.singlee.capital.interfacex.qdb.rep.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.counterparty.mapper.CounterPartyDao;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.interfacex.qdb.ESB.util.POICascadeUtil;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportDeValInterBankMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportDeValTradeMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportEntrustMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportEntrustSecMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportInterBankMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportSecMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportTradeMapper;
import com.singlee.capital.interfacex.qdb.rep.mapper.ReportValTradeMapper;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportValTradeBean;
import com.singlee.capital.interfacex.qdb.rep.service.ReportService;
import com.singlee.capital.interfacex.qdb.util.Reports;
import com.singlee.capital.reports.util.ExcelUtil;

@Service
public class ReportServiceImpl implements ReportService{
	
	@Resource
	private ReportEntrustMapper reportEntrustMapper;
	
	@Resource
	private ReportEntrustSecMapper reportEntrustSecMapper;
	
	@Resource
	private ReportInterBankMapper reportInterBankMapper;
	
	@Resource
	private ReportSecMapper reportSecMapper;
	
	@Resource
	private ReportTradeMapper reportTradeMapper;
	
	@Resource
	private ReportDeValInterBankMapper reportDeValInterBankMapper;
	
	@Resource
	private ReportDeValTradeMapper reportDeValTradeMapper;
	
	@Resource
	private ReportValTradeMapper reportValTradeMapper;
	
	@Resource
	private TaDictVoMapper taDictVoMapper;
	
	@Resource
	private CounterPartyDao counterPartyDao;
	

	@Override
	public Page<ReportEntrustBean> pageReportEntrust(Map<String, Object> map) {
		return reportEntrustMapper.listReportEntrust(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportEntrustSecBean> pageReportEntrustSec(Map<String, Object> map) {
		return reportEntrustSecMapper.listReportEntrustSec(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportInterBankBean> pageReportInterBank(Map<String, Object> map) {
		return reportInterBankMapper.listReportInterBank(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportSecBean> pageReportSecBean(Map<String, Object> map) {
		return reportSecMapper.listReportSecBean(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportTradeBean> pageReportTradeBean(Map<String, Object> map) {
		return reportTradeMapper.listReportTradeBean(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public byte[] exportEntrustRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportEntrust));
			List<ReportEntrustBean> list = reportEntrustMapper.listReportEntrust(map);
			int rowIdx = 1;
			for (ReportEntrustBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getPrdName        ());  //产品名称      
				eu.writeStr(rowIdx, "B",    bean.getCost        ());     //成本中心
				eu.writeStr(rowIdx, "C",    bean.getPort        ());     //投资组合
				eu.writeDouble(0, rowIdx, "D", eu.getDefaultDouble2CellStyle(), bean.getAmt());   //初始委托投资额（亿元）             
				eu.writeDouble(0, rowIdx, "E", eu.getDefaultDouble2CellStyle(), bean.getCpriceCost());//净值(按成本)              
				eu.writeDouble(0, rowIdx, "F", eu.getDefaultDouble2CellStyle(), bean.getCpriceMarket());//净值(按市值)
				eu.writeDouble(0, rowIdx, "G", eu.getDefaultDouble2CellStyle(), bean.getCpriceAssetCost());//本期末委托投资份额净值(成本）
				eu.writeDouble(0, rowIdx, "H", eu.getDefaultDouble2CellStyle(), bean.getCpriceAssetMarket());//本期末委托投资份额净值(市值）
				eu.writeDouble(0, rowIdx, "I", eu.getDefaultDouble2CellStyle(), bean.getAsset());//资产类合计（成本）
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultDouble2CellStyle(), bean.getDebt());//负债类合计（成本）
				eu.writeDouble(0, rowIdx, "K", eu.getDefaultDouble2CellStyle(), bean.getTotalSecInv());//证券投资合计
				eu.writeStr(rowIdx, "L",    "");        //杠杆率
				eu.writeDouble(0, rowIdx, "M", eu.getDefaultPercentCellStype_2(), bean.getRate());  //产品计划收益率            
				eu.writeDouble(0, rowIdx, "N", eu.getDefaultPercentCellStype_2(), bean.getContractRate());  //过去一年的预计年化收益率
				eu.writeStr(rowIdx, "O", 	bean.getvDate()          );  //产品起息日          
				eu.writeStr(rowIdx, "P", 	bean.getmDate()          );  //产品到期日          
				eu.writeStr(rowIdx, "Q",    bean.getIntFre        ());  //付息频率  
				eu.writeStr(rowIdx, "R", 	bean.getTheoryPaymentDate()          );  //上期付息时间 
				eu.writeDouble(0, rowIdx, "S", eu.getDefaultDouble2CellStyle(), bean.getInterestAmt());//上期付息金额
				eu.writeDouble(0, rowIdx, "T", eu.getDefaultDouble2CellStyle(), bean.getSettlAmt()); //期末资产总额
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportEntrustSecRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportEntrustSec));
			List<ReportEntrustSecBean> list = reportEntrustSecMapper.listReportEntrustSec(map);
			int rowIdx = 1;
			for (ReportEntrustSecBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getZjlx        ());  //资金类型        
				eu.writeStr(rowIdx, "B",    bean.getCost        ());  //成本中心
				eu.writeStr(rowIdx, "C",    bean.getPort        ());  //投资组合
				eu.writeStr(rowIdx, "D",    bean.getPartyName        ());  //交易对手名称
				eu.writeStr(rowIdx, "E", bean.getSecId());//债券代码
				eu.writeDouble(0, rowIdx, "F", eu.getDefaultDouble2CellStyle(), bean.getFaceAmt());   //面值   
				eu.writeDouble(0, rowIdx, "G", eu.getDefaultDouble2CellStyle(), bean.getUnitCost());//单位成本             
				eu.writeDouble(0, rowIdx, "H", eu.getDefaultDouble2CellStyle(), bean.getCostAmt());//成本总价               
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportInterBankRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportInterBank));
			List<ReportInterBankBean> list = reportInterBankMapper.listReportInterBank(map);
			int rowIdx = 1;
			for (ReportInterBankBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getOrganization());  //       中介公司
				eu.writeStr(rowIdx, "B",    bean.getAccountType        ());  //账户类型       
				eu.writeStr(rowIdx, "C",    bean.getProductCode        ());  //交易代码
				eu.writeStr(rowIdx, "D",    bean.getPrdName        ());  //业务类型
				eu.writeStr(rowIdx, "E",    bean.getPartyName        ());  //客户名称
				eu.writeStr(rowIdx, "F",    bean.getCustType        ());  //客户类型
				eu.writeStr(rowIdx, "G",    bean.getCost       ()); //成本中心
				eu.writeStr(rowIdx, "H",    bean.getPort    ());//投资组合
				eu.writeStr(rowIdx, "I",    bean.getCustActType        ());  //会计类型
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultDouble2CellStyle(), bean.getAmt());   //金额       
				eu.writeStr(rowIdx, "K",    bean.getRateType        ());  //利率类型
				eu.writeDouble(0, rowIdx, "L", eu.getDefaultPercentCellStype_2(), bean.getContractRate());  //利率   
				eu.writeStr(rowIdx, "M",    bean.getBasis        ());  //计息方式
				eu.writeStr(rowIdx, "N", 	bean.getvDate()          );  //起息日          
				eu.writeStr(rowIdx, "O", 	bean.getmDate()          );  //到期日   
				eu.writeStr(rowIdx, "P",    bean.getTerm        ());  //期限
				eu.writeStr(rowIdx, "Q",    bean.getIntFre        ());  //付息频率
				eu.writeDouble(0, rowIdx, "R", eu.getDefaultDouble2CellStyle(), bean.getInterestAmt());//下次付息金额              
				eu.writeDouble(0, rowIdx, "S", eu.getDefaultDouble2CellStyle(), bean.getmAmt());//本息合计               
				eu.writeStr(rowIdx, "T",    bean.getSponInst        ());  //发起机构
				eu.writeDouble(0, rowIdx, "U", eu.getDefaultDouble2CellStyle(), bean.getFeesAmt());//中介费用
				eu.writeStr(rowIdx, "V",    bean.getBaseProductRemark        ());  //备注
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportSecRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportSec));
			List<ReportSecBean> list = reportSecMapper.listReportSecBean(map);
			int rowIdx = 1;
			for (ReportSecBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getProductCode        ());  //债券代号       
				eu.writeStr(rowIdx, "B",    bean.getProductName        ());  //债券名称
				eu.writeStr(rowIdx, "C",    bean.getcNo        ());  //CIF号
                eu.writeStr(rowIdx, "D",    bean.getCustAcctyName        ());  //客户授信属性中文名                                                        // bean.getCustAccty        ());  //授信客户属性(代码)
				eu.writeStr(rowIdx, "E",    bean.getPort        ());  //投资组合
				eu.writeStr(rowIdx, "F",    bean.getCost());  //成本中心

				//eu.writeStr(rowIdx, "G",    bean.getLast_update()          );  //日期
				eu.writeStr(0, rowIdx, "G", eu.getDefaultDate2CellStyle(), bean.getLast_update());//日期
				eu.writeStr(rowIdx, "H", 	bean.getPrdName        ());  //业务种类
				eu.writeStr(rowIdx, "I",    bean.getType1        ());  //业务种类1
				eu.writeStr(rowIdx, "J",    bean.getAccountType        ());  //账户类别 
				eu.writeStr(rowIdx, "K",    bean.getInvType        ());  //资产分类
				eu.writeStr(rowIdx, "L",    bean.getRsrAmt        ());  //风险缓释
				     
				eu.writeDouble(0, rowIdx, "M", eu.getDefaultDouble2CellStyle(), bean.getAmt());//面值
				eu.writeDouble(0, rowIdx, "N", eu.getDefaultDouble2CellStyle(), bean.getGyamt());   //公允价值        
				eu.writeDouble(0, rowIdx, "O", eu.getDefaultDouble2CellStyle(), bean.getGyamt());//摊余成本
				eu.writeDouble(0, rowIdx, "P", eu.getDefaultDouble2CellStyle(), bean.getmInt());//应计利息
				eu.writeStr(rowIdx, "Q",    bean.getRateType        ());  //计息方式
				eu.writeDouble(0, rowIdx, "R", eu.getDefaultPercentCellStype_2(), bean.getContractRate());  //利率   
				eu.writeStr(rowIdx, "S", 	bean.getvDate()          );  //起息日          
				eu.writeStr(rowIdx, "T", 	bean.getmDate()          );  //到期日   
				eu.writeStr(rowIdx, "U",    bean.getRateCode        ());  //利率浮动代码
				eu.writeStr(rowIdx, "V",    bean.getIrTerm        ());  //利率浮动频率
				eu.writeStr(rowIdx, "W",    bean.getCustType        ());  //客户类型
				eu.writeStr(rowIdx, "X",    bean.getPartyName        ());  //客户名称中文
				eu.writeStr(rowIdx, "Y",    bean.getPartyFinanceLicense        ());  //客户证件编号
				eu.writeStr(rowIdx, "Z",    bean.getPartyId        ());  //授信客户
				eu.writeStr(rowIdx, "AA",    bean.getPartyOrganGrade        ());  //债券评级
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportTradeRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportTrade));
//			Map<String,Object> params = new HashMap<String, Object>();
//			Map<String,Object> saleType = new HashMap<String, Object>();
//			params.put("dict_id", "saleType");
//			List<TaDictVo> dict = taDictVoMapper.getTaDictByDictId(params);
//			if(dict.size()>0){
//				for(TaDictVo vo:dict){
//					saleType.put(vo.getDict_key(), vo.getDict_value());
//				}
//			}
			List<ReportTradeBean> list = reportTradeMapper.listReportTradeBean(map);
			int rowIdx = 1;
			for (ReportTradeBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getZhfl        ());  //账户分类       
				eu.writeStr(rowIdx, "B",    bean.getPrdName());  //资产类别
				eu.writeStr(rowIdx, "C",    bean.getProductName        ());  //债券名称
				eu.writeStr(rowIdx, "D",    bean.getProductCode        ());  //债券代号
				eu.writeStr(rowIdx, "E",    bean.getZcqd        ());  //资产清单
				eu.writeStr(rowIdx, "F",    bean.getCost        ()); //成本中心
				eu.writeStr(rowIdx, "G",    bean.getPort        ()); //投资组合
				eu.writeStr(rowIdx, "H",    bean.getBaseProductRemark        ());  //资产说明
				eu.writeStr(rowIdx, "I",    bean.getInvType        ());  //资产分类
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultDouble2CellStyle(), bean.getAmt());//面值
				eu.writeDouble(0, rowIdx, "K", eu.getDefaultDouble2CellStyle(), bean.getGyamt());//公允价值
				eu.writeDouble(0, rowIdx, "L", eu.getDefaultDouble2CellStyle(), bean.getTyamt());//摊余成本
				eu.writeStr(rowIdx, "M",    bean.getRateType        ());  //计息方式
				eu.writeDouble(0, rowIdx, "N", eu.getDefaultPercentCellStype_2(), bean.getContractRate());  //实际利率   
				eu.writeStr(rowIdx, "O", 	bean.getvDate()          );  //起息日          
				eu.writeStr(rowIdx, "P", 	bean.getmDate()          );  //到期日   
				eu.writeStr(rowIdx, "Q", 	bean.getsDate()          );  //本期付息起息日          
				eu.writeStr(rowIdx, "R", 	bean.getLastbdate()          );  //最后一个买入日期   
				eu.writeStr(rowIdx, "S", 	bean.getLastsdate()          );  //最后一个卖出日期         
				eu.writeDouble(0, rowIdx, "T", eu.getDefaultDouble2CellStyle(), bean.getmInt());//一次还本利息
				eu.writeDouble(0, rowIdx, "U", eu.getDefaultDouble2CellStyle(), bean.getActualTint());//应收利息
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
	

	@Override
	public Page<ReportDeValInterBankBean> pageReportDeValInterBankBean(Map<String, Object> map) {
		return reportDeValInterBankMapper.listReportDeValInterBankBean(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportDeValTradeBean> pageReportDeValTradeBean(Map<String, Object> map) {
		return reportDeValTradeMapper.listReportDeValTradeBean(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ReportValTradeBean> pageReportValTradeBean(Map<String, Object> map) {
		return reportValTradeMapper.listReportValTradeBean(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public byte[] exportDeValInterBankRep(Map<String, Object> map)throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportDeValInterBank));
			List<ReportDeValInterBankBean> list = reportDeValInterBankMapper.listReportDeValInterBankBean(map);
			int rowIdx = 1;
			for (ReportDeValInterBankBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getDealNo        ());  //合同编号         
				eu.writeStr(rowIdx, "B", 	bean.getAcctInst     ());  //记账单位名称              
				eu.writeStr(rowIdx, "C", 	bean.getDealNo        ());  //账号              
				eu.writeStr(rowIdx, "D", 	bean.getCustName          ());  //同业/金融性公司名称              
				eu.writeStr(rowIdx, "E", 	bean.getCustTypeName            ());  //交易对手类别            
				eu.writeStr(rowIdx, "F", 	bean.getCustLocate            ());  //地区分析
				eu.writeStr(rowIdx, "G", 	bean.getvDate()          );  //起存日/买入结算日期（起息日）          
				eu.writeStr(rowIdx, "H", 	bean.getmDate()          );  //止存日/返售结算日期（止息日）          
				eu.writeDouble(0, rowIdx, "I", eu.getDefaultDouble2CellStyle(), bean.getAmt());  //期末余额（折人民币）  
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultPercentCellStype_2(), bean.getRate());  //年利率  
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportDeValTrade(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportDeValTrade));
			List<ReportDeValTradeBean> list = reportDeValTradeMapper.listReportDeValTradeBean(map);
			//机构或类似字段	产品大类	产品细类	产品代码	利率	起息日	到期日	应计利息	账面余额	摊余成本	付息方式	内部评级（分类为同业信用和企业信用）	产品代码缩写	底层债券到期日
			int rowIdx = 1;
			for (ReportDeValTradeBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    bean.getInstName        ());  //分行/支行名称         
				eu.writeStr(rowIdx, "B", 	bean.getProdBigType     ());  //产品大类              
				eu.writeStr(rowIdx, "C", 	bean.getProdType        ());  //产品细类              
				eu.writeStr(rowIdx, "D", 	bean.getProdNo          ());  //产品代码              
				eu.writeDouble(0, rowIdx, "E", eu.getDefaultPercentCellStype_2(), bean.getRate());  //债项的利率            
				eu.writeStr(rowIdx, "F", 	bean.getvDate()          );  //债项的起息日          
				eu.writeStr(rowIdx, "G", 	bean.getmDate()          );  //债项的到期日          
				eu.writeDouble(0, rowIdx, "H", eu.getDefaultDouble2CellStyle(), bean.getIntAmt());  //债项的应计利息        
				eu.writeDouble(0, rowIdx, "I", eu.getDefaultDouble2CellStyle(), bean.getAmt());  //资产负债表日账面金额  
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultDouble2CellStyle(), bean.getSettAmt());  //资产负债表日摊余成本  
				eu.writeStr(rowIdx, "K", 	bean.getIntType         ());  //该债项的付息方式      
				eu.writeStr(rowIdx, "L", 	bean.getInnerLevel      ());  //内部评级              
				eu.writeStr(rowIdx, "M", 	bean.getShortProdNo     ());  //产品代码缩写          
				eu.writeStr(rowIdx, "N", 	bean.getBaseAssetMdate  ());  //底层债券到期日   
				
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportValTradeRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.ReportValTrade));
			List<ReportValTradeBean> list = reportValTradeMapper.listReportValTradeBean(map);
			int rowIdx = 1;
			int i=1;
			for (ReportValTradeBean bean : list) 
			{
				eu.writeStr(rowIdx, "A",    String.valueOf(i++));  //序号        
				eu.writeStr(rowIdx, "B", 	bean.getDealNo     ());  //投资代号              
				eu.writeStr(rowIdx, "C", 	bean.getCustName        ());  //客户名称              
				eu.writeDouble(0, rowIdx, "D", eu.getDefaultDouble2CellStyle(), bean.getAmt());  //面值  
				eu.writeDouble(0, rowIdx, "E", eu.getDefaultPercentCellStype_2(), bean.getRate());  //利率  
				eu.writeStr(rowIdx, "F", 	bean.getvDate()          );  //起息日          
				eu.writeStr(rowIdx, "G", 	bean.getmDate()          );  //止息日   
				eu.writeStr(rowIdx, "H", 	bean.getCcy            ());  //币种  
				eu.writeDouble(0, rowIdx, "I", eu.getDefaultDouble2CellStyle(), bean.getIntAmt());  //应计利息 
				eu.writeDouble(0, rowIdx, "J", eu.getDefaultDouble2CellStyle(), bean.getBalance());  //账面余额 
				eu.writeStr(rowIdx, "K", 	bean.getIntPayCycle            ());  //付息频率(INTPAYCYCLE)            
				eu.writeStr(rowIdx, "L", 	bean.getIntType            ());  //计息方式
				eu.writeStr(rowIdx, "M", 	bean.getBasis            ());  //计息基准(BASIS)             
				eu.writeStr(rowIdx, "N", 	bean.getIntCalcRule            ());  //计息规则(INTCALCRULE)
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultPercentCellStype_2(), d); 利率格式
				//eu.writeDouble(0, rowIdx, columnIdx, eu.getDefaultDouble2CellStyle(), d);  金额格式
				rowIdx++;
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportBassAsset() throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.BassAsset));
			Workbook wb = eu.getWb();
            //获取行业分类
			Map<String, List<String>> map =getAllCounterPartyKind();
			//获取基础资产类别
			List<String> baseAssetType = taDictVoMapper.getTextByDictId("baseAssetType");
			map.put("baseAssetType", baseAssetType);
			//是否是本行客户
			List<String> isCust = taDictVoMapper.getTextByDictId("YesNo");
			map.put("isCust", isCust);
			//质押物类型
			List<String> pledgeType = taDictVoMapper.getTextByDictId("pledgeType");
			map.put("pledgeType", pledgeType);
			//基础资产增信方式
			List<String> mortgageType = taDictVoMapper.getTextByDictId("icWay");
			map.put("mortgageType", mortgageType);
			//股东性质
			List<String> shareType = taDictVoMapper.getTextByDictId("shoreholderType");
			map.put("shareType", shareType);
			//保证人所属行业
			List<String> guarantorIndus = taDictVoMapper.getTextByDictId("GBT4754_2011");
			map.put("guarantorIndus", guarantorIndus);
			//货币市场类型
			List<String> moneyMarketType = taDictVoMapper.getTextByDictId("moneyMarketType");
			map.put("moneyMarketType", moneyMarketType);
	       	POICascadeUtil pu = new POICascadeUtil(wb, map);
	       	
	        //创建隐藏的参数页
	       	pu.createParamDataPage(false);
	       	pu.setColumnValidation(0, 2,"H", 300,"BigKind");//第一级
	       	pu.setColumnIndirectValidation(0, 2, "I", 300, "H");//第二级
	       	pu.setColumnIndirectValidation(0, 2, "J", 300, "I");//第三极
	       	pu.setColumnValidation(0, 2, "D", 300, "baseAssetType");//设置基础资产类别
	       	pu.setColumnValidation(0, 2, "F", 300, "isCust");//是否是本行客户
	       	pu.setColumnValidation(0, 2, "M", 300, "pledgeType");//质押物类型
	    	pu.setColumnValidation(0, 2, "W", 300, "guarantorIndus");//保证人所属行业
	    	pu.setColumnValidation(0, 2, "U", 300, "shareType");//股东性质
	    	pu.setColumnValidation(0, 2, "S", 300, "mortgageType");//基础资产增信方式
	    	pu.setColumnValidation(0, 2, "G", 300, "moneyMarketType");//货币市场类型
	       	pu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	private LinkedHashMap<String, List<String>> getAllCounterPartyKind() {
		Map<String,Object> map =new HashMap<String, Object>();
		map.put("kind_grade", "0");
		List<CounterPartyKindVo> fistKind = counterPartyDao.getAllPartyKind(map);
		
		map.put("kind_grade", "1");
		List<CounterPartyKindVo> secondKind = counterPartyDao.getAllPartyKind(map);
		
		map.put("kind_grade", "2");
		List<CounterPartyKindVo> thirdKind = counterPartyDao.getAllPartyKind(map);
		
		LinkedHashMap<String, List<String>> kindMap =new LinkedHashMap<String, List<String>>();
		CounterPartyKindVo tempkind = null;
		List<String> fistKind_1 = new ArrayList<String>();
		kindMap.put("BigKind", fistKind_1);//行业大类
		
		for (CounterPartyKindVo kind : fistKind) 
		{
			tempkind = new CounterPartyKindVo();
			tempkind.setId(kind.getId());
			tempkind.setText(kind.getText());
			fistKind_1.add(tempkind.getText().replace("、", "_"));
			
			for (CounterPartyKindVo skind : secondKind) 
			{
				if(kind.getKind_code().substring(0, 1).equals(skind.getKind_code().substring(0, 1))){
					kind.addChildrenText(skind.getText().replace("、", "_"));
				}
			}
			kindMap.put(kind.getText().replace("、", "_"), kind.getChildrenText());//行业中类
		}
		
		for (CounterPartyKindVo kind_1 : secondKind) 
		{
			tempkind = new CounterPartyKindVo();
			tempkind.setId(kind_1.getId());
			tempkind.setText(kind_1.getText());
			tempkind.setKind_code(kind_1.getKind_code());
			for (CounterPartyKindVo tkind : thirdKind) 
			{
				if(tempkind.getKind_code().substring(0, 3).equals(tkind.getKind_code().substring(0, 3))){
					tempkind.addChildrenText(tkind.getText().replace("、", "_"));
				}
			}
			kindMap.put(tempkind.getText().replace("、", "_"), tempkind.getChildrenText());//行业小类
		}
		
		return kindMap;
	}

	@Override
	public byte[] exportTradInvMain()  throws Exception{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.TradIvMain));
			
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}

	@Override
	public byte[] exportCustNews() throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel(Reports.CustNews));
			
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
	

}
