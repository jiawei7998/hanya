package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TDpAcActModRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TDpAcActModRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="PlanMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SerialNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SettleMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Stdcollleftlamt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeCalType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntRateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TreasuryRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateFloat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CrMaxRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CrIntRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CMinPayAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDpAcAciModRq", propOrder = { "commonRqHdr", "planMode","approveType","ftTxnType",
		"serialNum", "settleMode", "stdcollleftlamt", "feeCalType",
		"intRateType", "treasuryRate", "rateFloat", "crMaxRate", "crIntRate",
		"cMinPayAmt","companyCode" })
public class TDpAcAciModRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "PlanMode", required = true)
	protected String planMode;
	@XmlElement(name = "ApproveType", required = true)
	protected String approveType;
	@XmlElement(name = "FtTxnType", required = true)
	protected String ftTxnType;
	@XmlElement(name = "SerialNum", required = true)
	protected String serialNum;
	@XmlElement(name = "SettleMode", required = true)
	protected String settleMode;
	@XmlElement(name = "Stdcollleftlamt", required = true)
	protected String stdcollleftlamt;
	@XmlElement(name = "FeeCalType", required = true)
	protected String feeCalType;
	@XmlElement(name = "IntRateType", required = true)
	protected String intRateType;
	@XmlElement(name = "TreasuryRate", required = true)
	protected String treasuryRate;
	@XmlElement(name = "RateFloat", required = true)
	protected String rateFloat;
	@XmlElement(name = "CrMaxRate", required = true)
	protected String crMaxRate;
	@XmlElement(name = "CrIntRate", required = true)
	protected String crIntRate;
	@XmlElement(name = "CMinPayAmt", required = true)
	protected String cMinPayAmt;
	@XmlElement(name = "CompanyCode", required = true)
	protected String companyCode;
	

	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String value) {
		this.companyCode = value;
	}
	public String getApproveType() {
		return approveType;
	}
	public void setApproveType(String value) {
		this.approveType = value;
	}
	public String getFtTxnType() {
		return ftTxnType;
	}
	public void setFtTxnType(String value) {
		this.ftTxnType = value;
	}
	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the planMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPlanMode() {
		return planMode;
	}

	/**
	 * Sets the value of the planMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPlanMode(String value) {
		this.planMode = value;
	}

	/**
	 * Gets the value of the serialNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSerialNum() {
		return serialNum;
	}

	/**
	 * Sets the value of the serialNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSerialNum(String value) {
		this.serialNum = value;
	}

	/**
	 * Gets the value of the settleMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettleMode() {
		return settleMode;
	}

	/**
	 * Sets the value of the settleMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettleMode(String value) {
		this.settleMode = value;
	}

	/**
	 * Gets the value of the stdcollleftlamt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStdcollleftlamt() {
		return stdcollleftlamt;
	}

	/**
	 * Sets the value of the stdcollleftlamt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStdcollleftlamt(String value) {
		this.stdcollleftlamt = value;
	}

	/**
	 * Gets the value of the feeCalType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeCalType() {
		return feeCalType;
	}

	/**
	 * Sets the value of the feeCalType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeCalType(String value) {
		this.feeCalType = value;
	}

	/**
	 * Gets the value of the intRateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIntRateType() {
		return intRateType;
	}

	/**
	 * Sets the value of the intRateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIntRateType(String value) {
		this.intRateType = value;
	}

	/**
	 * Gets the value of the treasuryRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTreasuryRate() {
		return treasuryRate;
	}

	/**
	 * Sets the value of the treasuryRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTreasuryRate(String value) {
		this.treasuryRate = value;
	}

	/**
	 * Gets the value of the rateFloat property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRateFloat() {
		return rateFloat;
	}

	/**
	 * Sets the value of the rateFloat property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRateFloat(String value) {
		this.rateFloat = value;
	}

	/**
	 * Gets the value of the crMaxRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCrMaxRate() {
		return crMaxRate;
	}

	/**
	 * Sets the value of the crMaxRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCrMaxRate(String value) {
		this.crMaxRate = value;
	}

	/**
	 * Gets the value of the crIntRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCrIntRate() {
		return crIntRate;
	}

	/**
	 * Sets the value of the crIntRate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCrIntRate(String value) {
		this.crIntRate = value;
	}

	/**
	 * Gets the value of the cMinPayAmt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCMinPayAmt() {
		return cMinPayAmt;
	}

	/**
	 * Sets the value of the cMinPayAmt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCMinPayAmt(String value) {
		this.cMinPayAmt = value;
	}

}
