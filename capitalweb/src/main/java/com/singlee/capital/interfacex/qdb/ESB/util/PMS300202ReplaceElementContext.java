package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.capital.interfacex.qdb.ESB.model.BaseModel;
import com.singlee.capital.interfacex.qdb.ESB.model.PMS300202Model;

public class PMS300202ReplaceElementContext {

	public static List<PMS300202Model> praseEsbXmlPackageDescription(byte[] responseXml,HashMap<String, Object> svcHdr,HashMap<String,Object> appHdr,HashMap<String, Object> appBody) throws Exception
	{
		ByteArrayInputStream byteArrayInputStream = null;
		List<PMS300202Model> appBodys = null;
		try{
			SAXReader reader = new SAXReader();
			reader.setEncoding("UTF-8");
			byteArrayInputStream = new ByteArrayInputStream(responseXml);
			Document xmlDoc = reader.read(byteArrayInputStream);
			xmlDoc.setXMLEncoding("UTF-8");
			System.out.println(XmlFormat.format(xmlDoc.asXML()));
			@SuppressWarnings("unchecked")
			List<Element> elements = xmlDoc.getRootElement().elements();
			appBodys = new ArrayList<PMS300202Model>();
			PMS300202Model bean = new PMS300202Model();
			for(Element element:elements) 
			{
				if("svcHdr".equals(element.getName()))
				{
					System.out.println("*************************B svcHdr**************************");
					getElementList(element,svcHdr);
					System.out.println("*************************E svcHdr**************************");
				}else
				if("appHdr".equals(element.getName()))
				{
					System.out.println("*************************B appHdr**************************");
					getElementList(element,appHdr);
					System.out.println("*************************E appHdr**************************");
				}else
				if("appBody".equals(element.getName()))
				{
					System.out.println("*************************B appBody*************************");
					
					appBodys = getElementList(bean,element,appBody);
					System.out.println("*************************B appBody*************************");
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			if(null != byteArrayInputStream)
			{
				byteArrayInputStream.close();
			}
		}
		return appBodys;
	}
	@SuppressWarnings("unchecked")
	public static List<PMS300202Model> getElementList(PMS300202Model bean,Element element,HashMap<String, Object> resultHashMap){
		List<PMS300202Model> custFields = new ArrayList<PMS300202Model>();
		try {
			List<Element> elements = element.elements();//custField层
			bean = new PMS300202Model();
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element3 = (Element)iterator.next();
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element3.getName()))+""+StringUtils.trimToEmpty(element3.getText()));
				resultHashMap.put(StringUtils.trimToEmpty(element3.getName()), StringUtils.trimToEmpty(element3.getText()));
				getValueFromExternalModel(bean,StringUtils.trimToEmpty(element3.getName()),StringUtils.trimToEmpty(element3.getText()));		 	
			}
			custFields.add(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return custFields;
	}
	
	@SuppressWarnings("deprecation")
	public static void getValueFromExternalModel(BaseModel model ,String fieldName,String fieldText) {
		if(org.springframework.util.StringUtils.hasText(fieldName)){
			try {
//				fieldName = fieldName.toLowerCase();
				String firstLetter = fieldName.substring(0, 1).toUpperCase();
	            //获得对应的getXxxxx方法的名子。。。
	            String setMethodName = "set" + firstLetter + fieldName.substring(1);
	            if(checkField(model.getClass().getDeclaredFields(),fieldName)){
		            Field field = model.getClass().getDeclaredField(fieldName);
		            Method setMethod = model.getClass().getMethod(setMethodName, new Class[]{field.getType()});
			        if(fieldText!=null&&!"".equals(fieldText)){
			        	if(field.getType().equals(String.class)){
				        	setMethod.invoke(model, new Object[]{ConvertUtil.getRemoveZero(fieldText)});//调用原有对像的setXxxx方法...
				        }
				        if(field.getType().equals(Integer.class)){
				        	setMethod.invoke(model, new Object[]{Integer.parseInt(ConvertUtil.getRemoveZero(fieldText))});//调用原有对像的setXxxx方法...
				        }
				        if(field.getType().equals(Date.class)){
				        	setMethod.invoke(model, new Object[]{new Date(fieldText)});//调用原有对像的setXxxx方法...
				        }
				        if(field.getType().equals(BigDecimal.class)){
				        	setMethod.invoke(model, new Object[]{new BigDecimal(ConvertUtil.getRemoveZero(fieldText))});//调用原有对像的setXxxx方法...
				        }
			        }
	            }
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	/**
	 * 判断是否存在该属性
	 * @param fields
	 * @param fieldName
	 * @return
	 */
	public static boolean checkField(Field[] fields,String fieldName){
		boolean bool = false;
		for (int i = 0; i < fields.length; i++) {
			if(fieldName.equals(fields[i].getName())){
				bool = true;
			}
		}
		return bool;
	}
	
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,HashMap<String, Object> resultHashMap)
	{
		List<Element> elements = element.elements();
		if(elements.size() ==0){
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element.getName()))+""+StringUtils.trimToEmpty(element.getText()));
			resultHashMap.put(StringUtils.trimToEmpty(element.getName()), StringUtils.trimToEmpty(element.getText()));
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();){
				Element element2 = (Element)iterator.next();
				getElementList(element2,resultHashMap);
			}
		}
	}
}
