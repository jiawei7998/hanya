package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.interfacex.model.DayendInfoRecord;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.model.JobExcuRecord;
import com.singlee.capital.interfacex.model.JobExcuTerm;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class ScanJobStatusQuartzJob implements CronRunnable{

	/**
	 * 
	 */
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
	
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("=======================开始处理========================");
		
		List<TaJob> jobList = jobExcuService.getEnableJobList(null);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(int i = 0;i<jobList.size();i++){
				Map<String, Object> map = new HashMap<String,Object>();
				map.put("JOB_ID", jobList.get(i).getJobId());
				List<JobExcuTerm> Jobinfo = jobExcuService.selectJobExcuTerm(map);
				
				Map<String, Object> map2 = new HashMap<String,Object>();
				map2.put("JOB_ID", jobList.get(i).getJobId());
				map2.put("EXCU_TIME",new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
				if(jobExcuService.getJobExcuRecord(map2)<1){
					if(Jobinfo!=null){
						if(InterfaceCode.TI_JOB_TERM_TYPE1.equals(Jobinfo.get(0).getUINT())){
							

							String startime = new SimpleDateFormat(Jobinfo.get(0).getSTART_TIME()).format(Calendar.getInstance().getTime());
							String currtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
							String endtime = sdf.format(sdf.parse(jobList.get(i).getLastRunDate()).getTime()+Integer.parseInt(Jobinfo.get(0).getMAX_TIME())*60*1000);
				
					
							if(sdf.parse(jobList.get(i).getLastRunDate()).after(sdf.parse(startime))){
								if((sdf.parse(currtime).after(sdf.parse(endtime)))){
									
									//检查日终批量是否执行完成
									
									if(Jobinfo.get(0).getJOB_ID()==17){
										Map<String, String> params = new HashMap<String, String>();
										params.put("BIZ_DATE", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
										
										List<DayendInfoRecord> list = jobExcuService.getDayendJobStatus(params);
										if(list.size()>0){
											for(DayendInfoRecord d:list){
												LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ StartDayendJob --> "+d.getStep_name()+" ]:处理失败--ERROR");
											}
										}else{
											JobExcuLog jobExcuLog2 = new JobExcuLog();
											jobExcuLog2.setJOB_ID(Jobinfo.get(0).getJOB_ID());//需查询
											jobExcuLog2.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
											jobExcuLog2.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
											jobExcuService.updateJobExcuLog(jobExcuLog2);
										
										}
									}
									
									
									JobExcuLog jobExcuLog = new JobExcuLog();
									jobExcuLog.setJOB_ID(Integer.parseInt(jobList.get(i).getJobId()));
									jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
									jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
									
									JobExcuRecord jobExcuRecord = new JobExcuRecord();
									jobExcuRecord.setJOB_ID(Integer.parseInt(jobList.get(i).getJobId()));
									jobExcuRecord.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
									
									if(jobExcuService.getJobExcuLog(jobExcuLog)>0){
										LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobList.get(i).getJobName()+" ]:执行完成--SUCCESS");
										jobExcuRecord.setRECORD_STATUS(InterfaceCode.TI_JOB_RECORD_SUCCESS);
										jobExcuService.insertJobExcuRecord(jobExcuRecord);
									}else{
										LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobList.get(i).getJobName()+" ]:执行失败--ERROR");
										jobExcuRecord.setRECORD_STATUS(InterfaceCode.TI_JOB_RECORD_ERROR);
										jobExcuService.insertJobExcuRecord(jobExcuRecord);
									}
									
								}
							}
						}
					}
				}
		}
		
		System.out.println("=======================处理完成========================");
		return false;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
