package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class GtpCrmsQuotaEndDayQuartzJob implements CronRunnable {

	/**
	 *  额度信息同步接口
	 *  1、GTP配置IFBM与CRMS；
		2、文件命名格式：crms_quota_yyyyMMdd.txt；通配符：crms_*.txt
		3、同业目录定义为：/share_ifbm/gtp/crms/backup，再cp至/share_ifbm/gtp/crms/send
		4、分隔符号：“|”
		5.、sit的地址是10.240.81.112；uat的地址是10.240.88.46
		6、/crmsshare/CRMS/gtp/files/REC/IFBM  接收目录；/crmsshare/CRMS/gtp/files/SND/IFBM  发送目录

	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");

	

	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("=======================开始处理========================");
		String classList= this.getClass().getName();
		System.out.println("classList:======"+classList);
		Map<String,String> map =new HashMap<String,String>();
		map.put("classList", classList);
		String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
		String jobName = jobExcuService.getJobByclassList(map).get(0).getJobName();

		JobExcuLog jobExcuLog = new JobExcuLog();
		jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
		jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
		jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		jobExcuService.insertJobExcuLog(jobExcuLog);
		LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");
		RetMsg<Object> retMsg = gtpFileService.GtpCrmsQuotaEndDayBatchWrite(null);
		
		if("0000".equals(retMsg.getCode())){
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
			jobExcuService.updateJobExcuLog(jobExcuLog);
			System.out.println("=======================处理结束========================");
		}
		
		return false;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
