package com.singlee.capital.interfacex.qdb.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktIrConfig;

public interface TtMktIrConfigService {
	/**
	 * 查询基准利率数据来源配置表
	 * @param params
	 * @param bounds
	 * @return
	 */
	public Page<TtMktIrConfig> selectTtMktIrConfigPage(Map<String, Object> params);
	
	/**
	 * 新增基准利率数据来源配置表
	 * @param ttMktIrConfig
	 */
	public void insertTtMktIrConfig(TtMktIrConfig ttMktIrConfig);
	
	/**
	 * 修改基准利率数据来源配置表
	 * @param ttMktIrConfig
	 */
	public void updateTtMktIrConfig(Map<String, Object> map);
	
	/**
	 * 删除基准利率数据来源配置表
	 * @param map
	 */
	public void daleteTtMktIrConfig(Map<String, Object> map);
	
	/**
	 * 查询配置地址
	 * @return
	 */
	public List<TtMktIrConfig> selectTtMktIrConfigList();
}
