package com.singlee.capital.interfacex.socketservice;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

//import com.ibm.mq.MQC;
//import com.ibm.mq.MQEnvironment;
//import com.ibm.mq.MQException;
//import com.ibm.mq.MQGetMessageOptions;
//import com.ibm.mq.MQMessage;
//import com.ibm.mq.MQQueue;
//import com.ibm.mq.MQQueueManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.interfacex.model.MessageQueueBean;

public class MessageQueueReceiver {
	
//	@SuppressWarnings({ "unchecked", "deprecation" })
//	public MessageQueueBean receive(MessageQueueBean messageQueueBean) throws Exception{
//		
//		MQEnvironment.hostname = messageQueueBean.getHostName();
//		MQEnvironment.channel = messageQueueBean.getChannel();
//		MQEnvironment.CCSID = Integer.valueOf(messageQueueBean.getCcsId());
//		MQEnvironment.port = Integer.valueOf(messageQueueBean.getPort());
//		MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES);
//		//Connection To the Queue Manager(连接到队列管理器)
//		MQQueueManager qMgr = new MQQueueManager(messageQueueBean.getqManager());
//		/*
//		 * Set up the open options to open the queue for out put and additionally we have set the option to fail if the queue manager is quiescing.
//		 * 建立打开选项以便打开用于输出的队列，进一步而言，如果队列管理器是停顿的话，我们也已设置了选项去应对不成功情况。
//		 */
//		int openOptions = MQC.MQOO_INPUT_SHARED | MQC.MQOO_FAIL_IF_QUIESCING;
//		// Open the queue(打开队列)
//		MQQueue queue = qMgr.accessQueue(messageQueueBean.getReceiveQname(), openOptions, null, null, null);
//		try {
//			// Set the put message options.(设置放置消息选项)
//			MQGetMessageOptions gmo = new MQGetMessageOptions();
//			gmo.options = gmo.options + MQC.MQGMO_SYNCPOINT; // Get messages
//			// control(在同步点控制下获取消息);
//			gmo.options = gmo.options + MQC.MQGMO_WAIT; // Wait if no messages
//			// Queue(如果在队列上没有消息则等待);
//			gmo.options = gmo.options + MQC.MQGMO_FAIL_IF_QUIESCING; // Fail
//			// QeueManager :Sets the time limit for the
//			// Quiescing(如果队列管理器停顿则失败);
//			gmo.waitInterval = Integer.valueOf(messageQueueBean.getWaitInterval()); 
//			// wait.(设置等待的时间限制)
//			/*
//			 * Next we Build a message The MQMessage class encapsulates the data
//			 * buffer
//			 * that contains the actual message data, together with all the MQMD
//			 * parameters
//			 * that describe the message.(下一步我们建立消息，MQMessage
//			 * 类压缩了包含实际消息数据的数据缓冲区，
//			 * 和描述消息的所有MQMD 参数)
//			 *
//			 */
//			MQMessage inMsg = new MQMessage(); // Create the message
//			// buffer(创建消息缓冲区)
//			//    inMsg.characterSet=819;
//			inMsg.format = MQC.MQFMT_STRING;
//			inMsg.correlationId = messageQueueBean.getMessageID().getBytes();
//			// Get the message from the queue on to the message
//			// buffer.(从队列到消息缓冲区获取消息)
//			queue.get(inMsg, gmo);
//			// Read the User data from the message.(从消息读取用户数据)
//			// String msgString = inMsg.readString(inMsg.getMessageLength());
//			String msgString = inMsg.readStringOfByteLength(inMsg.getMessageLength());
//			//String msgString =inMsg.readUTF();
//			System.out.println("接收消息的长度--" + msgString.length());
//			messageQueueBean.setRetContext(msgString);
//			System.out.println(" The Message from the Queue is : " + msgString);
//			// Commit the transaction.(提交事务处理)
//			qMgr.commit();
//			// Close the the Queue and Queue manager objects.(关闭队列和队列管理器对象)
//			queue.close();
//			qMgr.disconnect();
//		
//		} catch (MQException ex) {
//			messageQueueBean.setRetContext(null);
//			System.out.println("An MQ Error Occurred: Completion Code is :" + ex.completionCode + "\n\n The Reason Code is :\t" + ex.reasonCode);
//			//ex.printStackTrace();
//		} catch (Exception e) {
//			messageQueueBean.setRetContext(null);
//			//e.printStackTrace();
//		}finally{
//			queue.close();
//			qMgr.disconnect();
//		}
//		return messageQueueBean;
//		
//	}
//	
//	@SuppressWarnings("unchecked")
//	public static <T> T converyToJavaBean(String xml, Class<T> c) {  
//        T t = null;  
//        try {  
//        	int pos=xml.indexOf("<?xml version");
//        	xml=xml.substring(pos);
//
//        	JAXBContext context = JAXBContext.newInstance(c);  
//            Unmarshaller unmarshaller = context.createUnmarshaller();  
//            t = (T) unmarshaller.unmarshal(new StringReader(xml));  
//        } catch (Exception e) {  
//        	//e.printStackTrace();
//			throw new RException(e);
//        }  
//  
//        return t;  
//    }

}
