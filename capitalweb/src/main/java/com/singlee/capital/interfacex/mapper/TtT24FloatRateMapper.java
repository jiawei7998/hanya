package com.singlee.capital.interfacex.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.interfacex.model.TtT24FloatRate;

public interface TtT24FloatRateMapper extends Mapper<TtT24FloatRate> {
	
	public void updateFloatrateByRateCode(TtT24FloatRate t24FloatRate) ;
	
	public TtT24FloatRate getFloatRateByRateCode(TtT24FloatRate t24FloatRate) ;
	
	public TtT24FloatRate getMaxFloatRateByRateCode(TtT24FloatRate t24FloatRate) ;
	
}
