
package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Root complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Root">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pay_Acct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pay_Bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pay_Nme" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tran_Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tran_Zy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tran_Amt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tran_Date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tran_Time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Res_Acct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Res_Bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Res_Nme" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="T_Rmk" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="T_Flow" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="T_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Vir_Acct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Root", propOrder = {
    "payAcct",
    "payBz",
    "payNme",
    "tranType",
    "tranZy",
    "tranAmt",
    "tranDate",
    "tranTime",
    "resAcct",
    "resBz",
    "resNme",
    "tRmk",
    "tFlow",
    "tNo",
    "virAcct",
    "status"
})

@XmlRootElement(name = "Root")
public class Root {

    @XmlElement(name = "Pay_Acct", required = true)
    protected String payAcct;
    @XmlElement(name = "Pay_Bz", required = true)
    protected String payBz;
    @XmlElement(name = "Pay_Nme", required = true)
    protected String payNme;
    @XmlElement(name = "Tran_Type", required = true)
    protected String tranType;
    @XmlElement(name = "Tran_Zy", required = true)
    protected String tranZy;
    @XmlElement(name = "Tran_Amt", required = true)
    protected String tranAmt;
    @XmlElement(name = "Tran_Date", required = true)
    protected String tranDate;
    @XmlElement(name = "Tran_Time", required = true)
    protected String tranTime;
    @XmlElement(name = "Res_Acct", required = true)
    protected String resAcct;
    @XmlElement(name = "Res_Bz", required = true)
    protected String resBz;
    @XmlElement(name = "Res_Nme", required = true)
    protected String resNme;
    @XmlElement(name = "T_Rmk", required = true)
    protected String tRmk;
    @XmlElement(name = "T_Flow", required = true)
    protected String tFlow;
    @XmlElement(name = "T_No", required = true)
    protected String tNo;
    @XmlElement(name = "Vir_Acct", required = true)
    protected String virAcct;
    
    protected String status;

    public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	/**
     * Gets the value of the payAcct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayAcct() {
        return payAcct;
    }

    /**
     * Sets the value of the payAcct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayAcct(String value) {
        this.payAcct = value;
    }

    /**
     * Gets the value of the payBz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayBz() {
        return payBz;
    }

    /**
     * Sets the value of the payBz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayBz(String value) {
        this.payBz = value;
    }

    /**
     * Gets the value of the payNme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayNme() {
        return payNme;
    }

    /**
     * Sets the value of the payNme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayNme(String value) {
        this.payNme = value;
    }

    /**
     * Gets the value of the tranType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranType() {
        return tranType;
    }

    /**
     * Sets the value of the tranType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranType(String value) {
        this.tranType = value;
    }

    /**
     * Gets the value of the tranZy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranZy() {
        return tranZy;
    }

    /**
     * Sets the value of the tranZy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranZy(String value) {
        this.tranZy = value;
    }

    /**
     * Gets the value of the tranAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranAmt() {
        return tranAmt;
    }

    /**
     * Sets the value of the tranAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranAmt(String value) {
        this.tranAmt = value;
    }

    /**
     * Gets the value of the tranDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranDate() {
        return tranDate;
    }

    /**
     * Sets the value of the tranDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranDate(String value) {
        this.tranDate = value;
    }

    /**
     * Gets the value of the tranTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranTime() {
        return tranTime;
    }

    /**
     * Sets the value of the tranTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranTime(String value) {
        this.tranTime = value;
    }

    /**
     * Gets the value of the resAcct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResAcct() {
        return resAcct;
    }

    /**
     * Sets the value of the resAcct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResAcct(String value) {
        this.resAcct = value;
    }

    /**
     * Gets the value of the resBz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResBz() {
        return resBz;
    }

    /**
     * Sets the value of the resBz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResBz(String value) {
        this.resBz = value;
    }

    /**
     * Gets the value of the resNme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResNme() {
        return resNme;
    }

    /**
     * Sets the value of the resNme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResNme(String value) {
        this.resNme = value;
    }

    /**
     * Gets the value of the tRmk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRmk() {
        return tRmk;
    }

    /**
     * Sets the value of the tRmk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRmk(String value) {
        this.tRmk = value;
    }

    /**
     * Gets the value of the tFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTFlow() {
        return tFlow;
    }

    /**
     * Sets the value of the tFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTFlow(String value) {
        this.tFlow = value;
    }

    /**
     * Gets the value of the tNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTNo() {
        return tNo;
    }

    /**
     * Sets the value of the tNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTNo(String value) {
        this.tNo = value;
    }

    /**
     * Gets the value of the virAcct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirAcct() {
        return virAcct;
    }

    /**
     * Sets the value of the virAcct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirAcct(String value) {
        this.virAcct = value;
    }

}
