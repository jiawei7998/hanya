package com.singlee.capital.interfacex.qdb.ESB.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;


public class ReplaceElementContext {

	/**
	 * 请求报文处理
	 * 
	 * 替换请求报文中对应域值
	 */
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void  replaceElementContext(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElement(element2,hashMap);
			}
		}

	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElement(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.readEsbFile();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				System.out.println("type is :"+esbBean.getType());
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElement(element2,hashMap);
			}
		}
	}
	/**
	 * 请求报文处理
	 * 
	 * 替换请求报文中对应域值
	 */
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	//CIF别名查询
	public static void  replaceElementContextQueryEName(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementQueryEName(element2,hashMap);
			}
		}

	}
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void  replaceElementContextEName(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementEName(element2,hashMap);
			}
		}

	}
	@SuppressWarnings("unchecked")
	public static void replaceElementQueryEName(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.readQueryENameFile();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementQueryEName(element2,hashMap);
			}
		}
	}
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	//cif别名维护
	public static void replaceElementEName(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.readENameFile();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementEName(element2,hashMap);
			}
		}
	}
	
	/**
	 * 请求报文处理
	 * 
	 * 替换请求报文中对应域值
	 */
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void  replaceElementContextCreate(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCreate(element2,hashMap);
			}
		}

	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCreate(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.readEsbCreateFile();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCreate(element2,hashMap);
			}
		}
	}
	
	
	/**
	 * 请求报文处理
	 * 
	 * 替换请求报文中对应域值
	 */
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void  replaceElementContextQueryDetail(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception
	{
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementQueryDetail(element2,hashMap);
			}
		}

	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementQueryDetail(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.readEsbQueryDetailedFile();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementQueryDetail(element2,hashMap);
			}
		}
	}
	
	
	
	//CNAPS2 二代支付 发起交易汇兑
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps2Analyze(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2Analyze(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.Cnaps2SendPayment();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("String".equals(esbBean.getType())){
					if(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))).length()>esbBean.getLength()){
						JOptionPane.showMessageDialog(null, "报文域："+StringUtils.trimToEmpty(element.getName())+"\r\n域名称："+esbBean.getDesc()+"\r\n报文值："+String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName())))+
								"\r\n域值长度超过报文要求长度！");
					}else {
						element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					}
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps2Analyze(element2,hashMap);
			}
		}
	}
	
	
	
	//CNAPS2 二代支付 交易状态查询
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2QueryStatus(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps2AnalyzeQueryStatus(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2AnalyzeQueryStatus(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.Cnaps2QueryStatus();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("String".equals(esbBean.getType())){
					if(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))).length()>esbBean.getLength()){
						JOptionPane.showMessageDialog(null, "报文域："+StringUtils.trimToEmpty(element.getName())+"\r\n域名称："+esbBean.getDesc()+"\r\n报文值："+String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName())))+
								"\r\n域值长度超过报文要求长度！");
					}else {
						element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					}
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps2AnalyzeQueryStatus(element2,hashMap);
			}
		}
	}
	
	//大额来帐待处理明细查询_2053
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps3PendMX(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps3Pend1(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps3Pend1(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.CnapsPend3();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps3Pend1(element2,hashMap);
			}
		}
	}
	
	//大额来帐待处理列表查询_2054
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps4PendLB(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps4Pend1(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps4Pend1(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.CnapsPend4();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps4Pend1(element2,hashMap);
			}
		}
	}
	
	//来帐待处理列表查询_2049
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2PendLB(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps2Pend1(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2Pend1(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.CnapsPend1();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps2Pend1(element2,hashMap);
			}
		}
	}
	
	//来帐待处理明细查询_2051
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2PendMX(Element element,HashMap<String, HashMap<String, Object>> replaceHashMap)throws Exception{
		
		List<Element> elements = element.elements();
		for(Element element2:elements){
			if(replaceHashMap.containsKey(StringUtils.trimToEmpty(element2.getName())))
			{
				HashMap<String, Object> hashMap = replaceHashMap.get(StringUtils.trimToEmpty(element2.getName()));
				replaceElementCnaps2Pend2(element2,hashMap);
			}
		}
	}
	
	/*********************替换请求报文REQUEST PACKAGE域值************************/
	@SuppressWarnings("unchecked")
	public static void replaceElementCnaps2Pend2(Element element, HashMap<String, Object> hashMap)throws Exception{
	
		List<Element> elements = element.elements();
		if(elements.size() ==0)
		{
			if(hashMap.containsKey(StringUtils.trimToEmpty(element.getName())))
			{
				/** 
				 * ConvertToServe.readEsbFile() 读取ESB配置文件
				 * name    element名称
				 * type    String or Number
				 * length  域长度
				 */
				HashMap<String,EsbBean> esbMap = ConvertToServe.CnapsPend2();
				EsbBean esbBean = esbMap.get(StringUtils.trimToEmpty(element.getName()));
				if("Date".equals(esbBean.getType())){
					//8位长度的日期类型 yyyyMMdd
					element.setText(ConvertUtil.getDataString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}else if("Time".equals(esbBean.getType())){
					//6位长度的时间类型 HHmmss
					element.setText(ConvertUtil.getTimeString((Date)hashMap.get(StringUtils.trimToEmpty(element.getName()))));
					
				}else if("Number".equals(esbBean.getType())){
					//数字类型是 长度不够 前补0
					element.setText(ConvertUtil.getFormat(Long.valueOf((String) hashMap.get(StringUtils.trimToEmpty(element.getName()))), esbBean.getLength()));
					
				}else {
					element.setText(String.valueOf(hashMap.get(StringUtils.trimToEmpty(element.getName()))));
				}
			}
			
		}
		else {
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				replaceElementCnaps2Pend2(element2,hashMap);
			}
		}
	}
}
