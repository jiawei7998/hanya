package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


public class ConvertUtil {

	//number类型常量
	public static String NUMBER_TYPE = "Number";
	/**
	 * 固定字符长度，不足补零
	 * 
	 * @param cifNo
	 * @return
	 */
	public static String addZeroForNum(int cifNo) {
		DecimalFormat df = new DecimalFormat("0000000000000000000");
		return df.format(cifNo);
	}

	/**
	 * 动态字符长度，不足补零
	 * 
	 * @param value
	 * @param length
	 * @return
	 */
	public static String getFormat(long value, int length) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			sb.append("0");
		}
		DecimalFormat df = new DecimalFormat(sb.toString());
		return df.format(value);
	}

	/**
	 * 日期转换
	 */
	public static String getDataString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String str = dateFormat.format(date);
		return str;
	}

	/**
	 * 时间转换
	 */
	public static String getTimeString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmss");
		String str = dateFormat.format(date);
		return str;
	}

	/**
	 * 格式化字符，去掉前面零
	 * 
	 * @param str
	 * @return
	 */
	public static String getRemoveZero(String str) {
		int num = 0;
		for (int i = 0; i < str.length(); i++) {
			if ('0' != str.charAt(i) || i == (str.length() - 1)) {
				num = i;
				break;
			}
		}
		return str.substring(num, str.length());
	}

	/**
	 * 日期转换为特定格式
	 * 
	 * @param args
	 */
	public static String getTimeClient(Date date) {
		// 2012.07.20-13.41.38.033
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy.MM.dd-HH:mm:ss.SSS");
		return dateFormat.format(date);
	}
	
	/**
	 * 得到配置XML文件的根
	 * @param fileName XML配置文件所在的路径
	 * @return
	 */
	public static Element getXmlRootByClassPath(String fileName)throws Exception
	{
		Element root=null;
		SAXReader saxReader = new SAXReader();
		Document document;
		try {
			document = saxReader.read(ConvertUtil.class.getClassLoader().getResourceAsStream(fileName));
			root = document.getRootElement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return root;
	}
	
	/**
	 * 将报解析成相应的字段
	 * @param fileName 报文格式文件URL
	 * @param messageList 报文
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static  HashMap<String,EsbBean> explainMessageByClassPath(String fileName) throws Exception
	{
		Element detailElement = getXmlRootByClassPath(fileName);
		HashMap<String,EsbBean> hashMap = new HashMap<String,EsbBean>();
		hashMap = getEsbField(detailElement);
		return hashMap;
	}
	
	/**
	 * 得到配置XML文件的根
	 * @param fileName XML配置文件所在的路径
	 * @return
	 */
	public static Element getXmlRoot(String fileName)throws Exception
	{
		Element root=null;
		File inputXml = new File(fileName);
		SAXReader saxReader = new SAXReader();
		Document document;
		try {
			document = saxReader.read(inputXml);
			root = document.getRootElement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return root;
	}
	
	/**
	 * 将报解析成相应的字段
	 * @param fileName 报文格式文件URL
	 * @param messageList 报文
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static  HashMap<String,EsbBean> explainMessage(String fileName) throws Exception
	{
		Element detailElement = getXmlRoot(fileName);
		HashMap<String,EsbBean> hashMap = new HashMap<String,EsbBean>();
		hashMap = getEsbField(detailElement);
		return hashMap;
	}
	
	/**
	 * 
	 * @param element
	 * @param 
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static HashMap<String,EsbBean> getEsbField(Element element)throws Exception
	{
		HashMap<String,EsbBean> hashMapEsb = new HashMap<String,EsbBean>();
		EsbBean esbBean = null;
		for(Iterator<?> fieldIte = element.elementIterator(); fieldIte.hasNext();)
		{
			Element field = (Element) fieldIte.next();
			//字段名称
			String name=field.attributeValue("name");
			//字段类型 
			String type=field.attributeValue("type");
		    int length=Integer.parseInt(field.attributeValue("length")); //字段长度
		    String desc = field.attributeValue("desc");
			//添加到hashmap中
		    esbBean = new EsbBean();
		    esbBean.setLength(length);
		    esbBean.setName(name);
		    esbBean.setType(type);
		    esbBean.setDesc(desc);
		    
		    hashMapEsb.put(name, esbBean);
			
		}
		return hashMapEsb;
	}

	public static String getTime(Date date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss-SSS");
		return dateFormat.format(date);
	}
	
	public static void main(String[] args) {
		System.out.println(getFormat(Integer.valueOf("0"),19));
		System.out.println(getTimeClient(new Date()));
	}
}
