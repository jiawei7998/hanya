package com.singlee.capital.interfacex.qdb.rep.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportDeValTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportEntrustSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportInterBankBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportSecBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportTradeBean;
import com.singlee.capital.interfacex.qdb.rep.model.ReportValTradeBean;

public interface ReportService {

	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportEntrustBean> pageReportEntrust(Map<String,Object> map);

	
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportEntrustSecBean> pageReportEntrustSec(Map<String,Object> map);
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportInterBankBean> pageReportInterBank(Map<String,Object> map);
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportSecBean> pageReportSecBean(Map<String,Object> map);
	
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportTradeBean> pageReportTradeBean(Map<String,Object> map);
	
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportDeValInterBankBean> pageReportDeValInterBankBean(Map<String,Object> map);
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportDeValTradeBean> pageReportDeValTradeBean(Map<String,Object> map);
	
	/**
	 * 
	 * @param mapR
	 * @param row
	 * @return
	 */
	public Page<ReportValTradeBean> pageReportValTradeBean(Map<String,Object> map);
	
	

	/******************************/
	public byte[] exportEntrustRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportEntrustSecRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportInterBankRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportSecRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportTradeRep(Map<String, Object> map) throws Exception;

	public byte[] exportBassAsset() throws Exception;
	public byte[] exportDeValInterBankRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportDeValTrade(Map<String, Object> map) throws Exception;
	
	public byte[] exportValTradeRep(Map<String, Object> map) throws Exception;



	public byte[] exportTradInvMain() throws Exception;



	public byte[] exportCustNews() throws Exception;
	
}
