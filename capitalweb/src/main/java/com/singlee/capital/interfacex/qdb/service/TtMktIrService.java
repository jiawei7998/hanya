package com.singlee.capital.interfacex.qdb.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;


import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtMktIr;
import com.singlee.capital.interfacex.qdb.model.TtMktSeriess;

public interface TtMktIrService {
	/**
	 * 查询基准利率信息，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktIr> selectTtMktIrPage(Map<String, Object> map);
	
	/**
	 * 添加基准利率利率信息
	 * @param ttMktIr
	 */
	public String insertTtMktIr(TtMktIr ttMktIr);
	
	/**
	 * 修改基准利率利率信息
	 * @param ttMktIr
	 */
	public void updateTtMktIr(TtMktIr ttMktIr);
	
	/**
	 * 根据基准利率代码删除信息
	 * @param i_code
	 */
	public void deleteTtMktIrById(String[] i_code);
	
	/**
	 * 根据基准利率代码sid删除
	 * @param i_code
	 */
	public void deleteTtMktIrById(String Sid);
	
	/**
	 * 查询市场利率行情历史，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TtMktSeriess> selectTtMktSeriessPage(Map<String, Object> map);
	
	/**
	 * 添加Series
	 * @param map
	 * @return
	 */
	public void insertSeries(Map<String,Object> map);
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	public void deleteSeries(Map<String,Object> map);
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	public void deleteSeries(TtMktSeriess ttMktSeriess);
	
	/**
	 * 获得指定的利率行情对象
	 * @param map
	 * 	s_id
	 * @return
	 */
	public TtMktSeriess selectSeries(Map<String,Object> map);
	
	/**
	 * 获得行情序列号
	 * @param 
	 * @return String
	 */
	public String getSeriesSeq();
	
	/**
	 * 手动导入Excel
	 * @param type
	 * @param is
	 * @return
	 */
	public String importIrByExcel(String type, InputStream is) throws Exception;

	public void updateTtMktSeriess(TtMktSeriess ttMktSeriess);

	public List<TtMktSeriess> selectTtMktSeriessBySize(
			Map<String, Object> params);

	public TtMktSeriess selectSerieByDate(Map<String, Object> params);
	
	/**
	 * 按照日期查询当天的基准利率
	 */
	public TtMktSeriess selectSerieByDate2(Map<String, Object> params);
	/**
	 * 复核时按照sid查询基准利率，若已存在，则修改之前的基准利率，否则，。
	 */
	public TtMktSeriess selectSerieBySid(String Sid);
	
}
