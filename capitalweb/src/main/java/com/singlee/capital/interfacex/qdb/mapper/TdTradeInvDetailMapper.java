package com.singlee.capital.interfacex.qdb.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TdTradeInvDetail;

import tk.mybatis.mapper.common.Mapper;

public interface TdTradeInvDetailMapper extends Mapper<TdTradeInvDetail>{
	/**
	 * 添加委外业务底层资产明细
	 * @param tdTradeInvDetail
	 */
	public void insertTdTradeInvDetail(List<TdTradeInvDetail> list);
	
	/**
	 * 删除委外业务主表信息
	 */
	public void deleteTdTradeInvDetailById(String tradeid);
	
	/**
	 * 查询委外业务明细表信息
	 */
	public List<TdTradeInvDetail> selectTdTradeInvDetailById(String tradeid);
	

	/**
	 * 插入历史表
	 */
	public void insertTdTradeInvDetailHis(TdTradeInvDetail tdTradeInvDetail);
	
	/**
	 * 插入历史表（参数为集合）
	 */
	public void insertTdTradeInvDetailHis2(List<TdTradeInvDetail> list);
	
	/**
	 * 按时间删除委外业务主表信息
	 */
	public void deleteTdTradeInvDetailByDate(Map<String, Object> map );
	
	/**
	 * 查询委外业务，实现分页
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdTradeInvDetail> selectTdTradInvDetailPage(Map<String, Object> map,RowBounds rowBounds);
	
	
	/**
	 * 查询所有的委外业务历史表明细
	 */
	public List<TdTradeInvDetail> selectAllTdTradInvDetail(Map<String, Object> map);
}
