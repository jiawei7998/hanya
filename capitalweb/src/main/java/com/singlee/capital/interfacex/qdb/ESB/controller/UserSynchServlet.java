package com.singlee.capital.interfacex.qdb.ESB.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List; 
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.singlee.capital.interfacex.qdb.ESB.model.ESBSynch;
import com.singlee.capital.interfacex.qdb.ESB.util.XmlFormat;
import com.singlee.capital.interfacex.qdb.service.InstitutionSynchService;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.UserService;

//import cn.com.singlee.ESB.service.FPMS11Service;

@SuppressWarnings("serial")
@Component("UserSynchServlet")
public class UserSynchServlet extends HttpServlet {
	
/*	@Autowired
	private FPMS11Service fpms11Service;*/
	@Autowired
	private UserService userService;
	@Autowired
	private InstitutionSynchService instSynService;
	@Autowired
	private InstitutionService instService;
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TaUser user;
		TtInstitution inst;
		Map<String,String> rels = new HashMap<String,String>();
		Map<String,String> userIDinstId = new HashMap<String,String>();
		log.info("核心发送报文同步客户和机构");		
		//解析发送来的文件为 FPMS11REQ类，所有数据存放在fpms里
		List<ESBSynch> esbSynch=init(request, response);
		try{
			if(esbSynch!=null&&esbSynch.size()>0){
				for(ESBSynch esbs : esbSynch){
					if("".equals(esbs.getOptType())){
						log.info("操作类型不能为空");
						return;
					}else if(!"0".equals(esbs.getOptType()) && !"1".equals(esbs.getOptType()) && !"2".equals(esbs.getOptType())){
						log.info("操作类型不在填写范围内");
						return;
					}else if(!"".equals(esbs.getOptType())){
						if("1".equals(esbs.getBusType())){
							if(esbs.getUserId()!=null&&!"".equals(esbs.getUserId())){
								//用户信息
								if("0".equals(esbs.getOptType())){
									//新增
									user = new TaUser();
									//给user赋值
									setUser(user,esbs);
									//判断用户是否存在 true：存在，false：不存在
									String judge = judgeUser(esbs);
									if("false".equals(judge)){
										user.setIsAccess("1");
										user.setBranchId("QCCBCNBQ");
										userService.createUser(user);
//										userIDinstId.put("instId", user.getInstId());
//										userIDinstId.put("userId", user.getUserId());
//										instSynService.insertUserInstMap(userIDinstId);
										log.info("成功增加柜员！");
									}else{
										log.info("该柜员号重复，请重新输入");
										return;
									}
								}else if("1".equals(esbs.getOptType())){
									//修改
									user = new TaUser();
									inst = new TtInstitution();
									setUser(user,esbs);
									String judge = judgeUser(esbs);
									if("true".equals(judge)){
										user.setIsAccess("1");
										user.setBranchId("QCCBCNBQ");
										userIDinstId.put("instId", user.getInstId());
										userIDinstId.put("userId", user.getUserId());
										userService.updateUserByEsb(user);
										instSynService.updateUserInstMap(userIDinstId);
										log.info("成功更新柜员！");
									}else{
										log.info("该柜员不存在，无法修改，请先新增该柜员");
										return;
									}
								}else if("2".equals(esbs.getOptType())){
									//删除
									String[] userIdList = new String[1];
									userIdList[0] = esbs.getUserId();
									userService.deleteUser(userIdList);
									log.info("成功删除柜员！");
								}
							}else{
								log.info("用户信息中用户号不能为空");
								return;
							}
						}else if("3".equals(esbs.getBusType())){
							if(esbs.getInstId()!=null&&!"".equals(esbs.getInstId())){
								//机构
								if("0".equals(esbs.getOptType())){
									//新增
									inst = new TtInstitution();
									setInst(esbs,inst);
									String judge = judgeOrg(esbs);
									if("false".equals(judge)){
										inst.setBranchId("QCCBCNBQ");
										instService.addInstitution(inst);
										log.info("成功添加机构！");
									}else{
										log.info("该机构重复，请重新输入");
										return;
									}
								}else if("1".equals(esbs.getOptType())){
									//修改
									inst = new TtInstitution();
									setInst(esbs,inst);
									String judge = judgeOrg(esbs);
									if("true".equals(judge)){
										inst.setBranchId("QCCBCNBQ");
										instService.updateInstitution(inst);
										log.info("成功更新机构信息！");
									}else{
										log.info("该机构不存在，无法修改，请先新增该机构");
										return;
									}
								}else if("2".equals(esbs.getOptType())){
									//删除
									instService.deleteInstitution(esbs.getInstId());
									log.info("成功删除机构！");
								}
							}else{
								log.info("机构信息中机构号不能为空");
								return;
							}
						}else if("5".equals(esbs.getBusType())){
							if(esbs.getInstId()!=null&&!"".equals(esbs.getInstId())){
								//机构关系
								if("0".equals(esbs.getOptType()) || "1".equals(esbs.getOptType())){
									//新增
									inst = new TtInstitution();
									String instId = esbs.getInstId();
									String instPId = esbs.getpInstId();
									rels.put("instId", instId);
									rels.put("instPId", instPId); 
									instSynService.updateInstPId(rels);
									
								}
							}else{
								return;
							}
						}
					}
				}
/*同步信息
boolean f=fpms11Service.insertDateChange(esbSynch);
				if(f){
System.out.println("接收成功", response);
System.out.println("同步用户机构接受成功");
				}else{
System.out.println("接受失败", response);
System.out.println("同步用户机构接受失败");
					return;
				}
				*/
			}
		}catch(Exception e){
			log.info("操作同步数据时"+e);
			return;
		}
	}

	/**
	 * 把请求的数据封装成类
	 */
	@SuppressWarnings("unchecked")
	private List<ESBSynch> init(HttpServletRequest request,HttpServletResponse response){
		//解析发送来的文件
		String dataXml =recieveData(request);
		ESBSynch message=new ESBSynch();
		List<ESBSynch> messages=new ArrayList<ESBSynch>();
		log.info("接收到的报文是："+dataXml);
		
		if(StringUtils.isNotEmpty(dataXml) && null != dataXml){
			try {
				dataXml = XmlFormat.format(dataXml);
				//建立schema工厂
				SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
				//建立验证文档文件对象，利用此文件对象所封装的文件进行schema验证
				File schemaFile = new File(request.getSession().getServletContext().getRealPath("/")+"/WEB-INF/classes/schema/FPMS11.xsd");
				//利用schema工厂，接收验证文档文件对象生成Schema对象
				Schema schema = schemaFactory.newSchema(schemaFile);
				//通过Schema产生针对于此Schema的验证器，利用schenaFile进行验证
				Validator validator = schema.newValidator();
				//得到验证的数据源
				Source source = new StreamSource(new ByteArrayInputStream(dataXml.getBytes("UTF-8")));
				validator.validate(source);
					try {
						Document doc=DocumentHelper.parseText(dataXml);
						Element element=doc.getRootElement();
						List<Element> eles=element.elements("appHdr");
						List<Element> es=element.elements("appBody");
						for (Element ele : es) {
							if("1".equals(ele.elementTextTrim("busType"))){
								List<Element> ess=ele.elements("userInfo");
								for(Element eele : ess){
									List<Element> userId = eele.elements("userId");
									List<Element> userName = eele.elements("cnName");
//									List<Element> enNames = eele.elements("enName");
//									List<Element> idTypes = eele.elements("idType");
//									List<Element> idCards = eele.elements("idCard");
//									List<Element> positions = eele.elements("position");
//									List<Element> sexs = eele.elements("sex");
//									List<Element> userClasss = eele.elements("userClass");
									List<Element> userEmail = eele.elements("email");
									List<Element> userFixedphone = eele.elements("phone");
									List<Element> userCellphone = eele.elements("mobile");
//									List<Element> faxs = eele.elements("fax");
//									List<Element> addrs = eele.elements("addr");
//									List<Element> attTypes = eele.elements("attType");
									List<Element> instId = eele.elements("orgId");
//									List<Element> beSubOrgIds = eele.elements("beSubOrgId");
//									List<Element> userTypes = eele.elements("userType");
									List<Element> isActive = eele.elements("userStat");
									List<Element> userMemo = eele.elements("userRmk");
									if(userId!=null&&!userId.isEmpty()){
										for(int i=0;i<userId.size();i++){
											message=new ESBSynch();
											makeObj(message, ele, eles);
											message.setUserId(userId.get(i).getTextTrim());
											message.setUserName(userName.get(i).getTextTrim());
											message.setUserPwd("E10ADC3949BA59ABBE56E057F20F883E");
											message.setIsActive(isActive.get(i).getTextTrim());
//											message.setUserCreatetime(DateUtil.getCurrentDate());
											message.setUserStoptime(null);
											message.setUserPwdchgtime(null);
											message.setUserMemo(userMemo.get(i).getTextTrim());
											message.setUserEmail(userEmail.get(i).getTextTrim());
											message.setUserFixedphone(userFixedphone.get(i).getTextTrim());
											message.setUserCellphone(userCellphone.get(i).getTextTrim());
											//message.setUserFlag(userFlag.get(i).getTextTrim());
											//message.setUserLogontype(userLogontype.get(i).getTextTrim());
											//message.setIsOnline(isOnline.get(i).getTextTrim());
											//message.setLatestIp(latestIp.get(i).getTextTrim());
											//message.setLatestLoginTime(latestLoginTime.get(i).getTextTrim());
											//message.setLatestTime(latestTime.get(i).getTextTrim());
											//message.setEmpId(empId.get(i).getTextTrim());
											message.setInstId(instId.get(i).getTextTrim());
											messages.add(message);
										}
									}
								}
							}else if("3".equals(ele.elementTextTrim("busType"))){
								List<Element> esssss=ele.elements("branchInfo");
								for(Element eelesss : esssss){
									List<Element> instId = eelesss.elements("brNo");
									//List<Element> subBankNos = eelesss.elements("subBankNo");
									List<Element> instName = eelesss.elements("brSimName");
									List<Element> instFullname = eelesss.elements("brFulName");
									//List<Element> brEnNames = eelesss.elements("brEnName");
									//List<Element> brTypes = eelesss.elements("brType");
									List<Element> instType = eelesss.elements("brLevel");
									//List<Element> attTypes = eelesss.elements("attType");
									//List<Element> swiftNos = eelesss.elements("swiftNo");
									List<Element> instLrInstCode = eelesss.elements("brMark");
									//List<Element> goldTradeMarks = eelesss.elements("goldTradeMark");
									//List<Element> proNos = eelesss.elements("proNo");
									//List<Element> proNames = eelesss.elements("proName");
									//List<Element> cityNos = eelesss.elements("cityNo");
									//List<Element> cityNames = eelesss.elements("cityName");
									//List<Element> distNos = eelesss.elements("distNo");
									//List<Element> distNames = eelesss.elements("distName");
									List<Element> linkMan = eelesss.elements("username");
									List<Element> telephone = eelesss.elements("conNo");
									List<Element> address = eelesss.elements("contAddr");
									//List<Element> zipCods = eelesss.elements("zipCod");
									//List<Element> mailAddrs = eelesss.elements("mailAddr");
									//List<Element> brNotes = eelesss.elements("brNote");
									//List<Element> salSeniors = eelesss.elements("salSenior");
									//List<Element> autoBalans = eelesss.elements("autoBalan");
									//List<Element> subFlgs = eelesss.elements("subFlg");
									List<Element> instStatus = eelesss.elements("stat");
									//List<Element> devContNos = eelesss.elements("devContNo");
									//List<Element> cityExchgNos = eelesss.elements("cityExchgNo");
									//List<Element> payBanNos = eelesss.elements("payBanNo");
									//List<Element> forCurInfos = eelesss.elements("forCurInfo");
									//List<Element> upayOrgCodes = eelesss.elements("upayOrgCode");
									//List<Element> promisAreas = eelesss.elements("promisArea");
									//List<Element> bocTcktNos = eelesss.elements("bocTcktNo");
									//List<Element> emergBrFlgs = eelesss.elements("emergBrFlg");
									//List<Element> faxNos = eelesss.elements("faxNo");
									//List<Element> engAddrs = eelesss.elements("engAddr");
									//List<Element> orgCodes = eelesss.elements("orgCode");
									//List<Element> accBnkNos = eelesss.elements("accBnkNo");
									
									if(instId!=null&&!instId.isEmpty()){
										for(int i=0;i<instId.size();i++){
											message=new ESBSynch();
											makeObj(message, ele, eles);
											message.setInstId(instId.get(i).getTextTrim());
											message.setInstFullname(instFullname.get(i).getTextTrim());
											message.setInstName(instName.get(i).getTextTrim());
											message.setpInstId("-9");
											message.setInstType(instType.get(i).getTextTrim());
											parserInstType(message);
											message.setInstStatus(instStatus.get(i).getTextTrim());
											message.setInstLrInstCode(instLrInstCode.get(i).getTextTrim());
											message.setLinkMan(linkMan.get(i).getTextTrim());
											message.setTelephone(telephone.get(i).getTextTrim());
											message.setMobile(telephone.get(i).getTextTrim());
											message.setAddress(address.get(i).getTextTrim());
											//message.setOnlineDate(onlineDate.get(i).getTextTrim());
											//message.setbInstId(bInstId.get(i).getTextTrim());
											messages.add(message);
										}
									}
								}
							}else if("5".equals(ele.elementTextTrim("busType"))){
								List<Element> essssss=ele.elements("branchRel");
								for(Element eelessss : essssss){
									List<Element> instId = eelessss.elements("brNo");
									//List<Element> brNames = eelessss.elements("brName");
									//List<Element> branInfoGaths = eelessss.elements("branInfoGath");
									//List<Element> relTypes = eelessss.elements("relType");
									List<Element> pInstId = eelessss.elements("subOrgId");
									//List<Element> subOrgNames = eelessss.elements("subOrgName");
									//List<Element> rmks = eelessss.elements("rmk");
									if(instId!=null&&!instId.isEmpty()){
										for(int i=0;i<instId.size();i++){
											message=new ESBSynch();
											makeObj(message, ele, eles);
											message.setInstId(instId.get(i).getTextTrim());
											message.setpInstId(pInstId.get(i).getTextTrim());
											messages.add(message);
										}
									}
								}
							}else if("".equals(ele.elementTextTrim("busType"))){
								log.info("业务类型不能为空");
								return messages;
							}else if(!"0".equals(ele.elementTextTrim("busType"))||!"1".equals(ele.elementTextTrim("busType"))){
								log.info("业务类型不在填写范围内");
								return messages;
							}
						}
					} catch (Exception e) {
						log.info("组装报文有误"+e);
						return null;
					}
			} catch (Exception e) {
				log.info("报文格式有误"+e);
				return null;
			}
		}else{
			log.info("报文为空");
			return null;
		}
		return messages;
	}
	
	
/*	private String handler(String code,String msg,HttpServletResponse response){
		Document doc = null;  
		Element root = null;
		PrintWriter out = null;
		try {
			doc = DocumentHelper.createDocument(); 
//doc.setXMLEncoding("UTF-8");
			root = doc.addElement("resp");
			doc.setRootElement(root);
			Element appHdr=root.addElement("appHdr");
			Element respCde=appHdr.addElement("respCde");
			Element respMsg=appHdr.addElement("respMsg");
			respCde.setText(code);
			respMsg.setText(msg);
			@SuppressWarnings("unused")
			Element appBody=root.addElement("appBody");
			response.setCharacterEncoding("utf-8");
		    response.setContentType("text/html; charset=utf-8");
			out=response.getWriter();
			out.println(doc.asXML());
//System.out.println("FPMS11接口：返回报文-"+doc.asXML());
		} catch (Exception e) {
//System.out.println(e.fillInStackTrace());
		} finally {
			if (out != null) {
				out.close();
				out = null;
			}
		}
		return doc.asXML();
	}
	*/
	
	/**
	 * 赋值请求的的数据类型，操作类型，请求的时间
	 */
	private boolean makeObj(ESBSynch message,Element ele,List<Element> eles) throws Exception{
		for (Element headEl : eles) {
			message.setReqDate(headEl.elementTextTrim("reqDate"));
			message.setReqTime(headEl.elementTextTrim("reqTime"));
		}
		message.setBusType((ele.elementTextTrim("busType")));
		message.setOptType((ele.elementTextTrim("optType")));
		message.setNoticeData((ele.elementTextTrim("noticeData")));
		return true;
	}
	
	/**
	 * //把发送过来的文件解析成String类型返回
	 * @param request
	 */
	public static String recieveData(HttpServletRequest request){
        String inputLine = null;
        StringBuffer recieveData = new StringBuffer();
        BufferedReader in = null;
        try{
        	//拿到发送过来的文件，解析为StringBuffer
            in = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null){
                recieveData.append(inputLine);
            }
        }catch (IOException e){
        	
        }finally{            
            try{
                if (null != in){
                    in.close();
                }
            }
            catch (IOException e){
            	
            }            
        }
        return recieveData.toString();
    }

	/**
	 * 给柜员初始化赋值
	 */
	public void setUser(TaUser user,ESBSynch esbSynch){
		try {
			user.setUserId(esbSynch.getUserId());
			user.setUserName(esbSynch.getUserName());
			user.setUserPwd(esbSynch.getUserPwd());
			user.setIsActive(esbSynch.getIsActive());
			user.setUserCreatetime(esbSynch.getUserCreatetime());
			user.setUserStoptime(esbSynch.getUserStoptime());
			user.setUserPwdchgtime(esbSynch.getUserPwdchgtime());
			user.setUserMemo(esbSynch.getUserMemo());
			user.setUserEmail(esbSynch.getUserEmail());
			user.setUserFixedphone(esbSynch.getUserFixedphone());
			user.setUserCellphone(esbSynch.getUserCellphone());
			user.setUserFlag(esbSynch.getUserFlag());
			user.setUserLogontype(esbSynch.getUserLogontype());
			user.setIsOnline(esbSynch.getIsOnline());
			user.setLatestIp(esbSynch.getLatestIp());
			user.setLatestLoginTime(esbSynch.getLatestLoginTime());
			user.setLatestTime(esbSynch.getLatestTime());
			user.setInstId(esbSynch.getInstId());
			user.setEmpId(esbSynch.getUserId());
		} catch (Exception e) {
//System.out.println("set用户，机构查询机构号时"+e);
		}
	}
	
	/**
	 * 给机构初始化赋值
	 */
	public void setInst(ESBSynch fpms11req,TtInstitution inst){
		inst.setInstId(fpms11req.getInstId());
		inst.setInstFullname(fpms11req.getInstFullname());
		inst.setInstName(fpms11req.getInstName());
		inst.setpInstId(fpms11req.getpInstId());
		inst.setInstType(fpms11req.getInstType());
		inst.setInstStatus(fpms11req.getInstStatus());
		inst.setInstLrInstCode(fpms11req.getInstLrInstCode());
		inst.setLinkMan(fpms11req.getLinkMan());
		inst.setTelephone(fpms11req.getTelephone());
		inst.setMobile(fpms11req.getMobile());
		inst.setAddress(fpms11req.getAddress());
		inst.setOnlineDate(fpms11req.getOnlineDate());
		inst.setbInstId(fpms11req.getbInstId());
	}
	

	/**
	 * 判断柜员是否存在 true：存在，false：不存在
	 */
	public String judgeUser(ESBSynch esbSynch){
		String result="false";
		try {
			TaUser user = userService.getUserById(esbSynch.getUserId());
			if(user!=null){
				result="true";
				return result;
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			log.info("判断用户是否重复时"+e);
		}
		return null;
	}
	
	/**
	 * 判断机构是否存在 true：存在，false：不存在
	 */
	public String judgeOrg(ESBSynch esbSynch){
		String result="false";
		try {
			TtInstitution inst = instService.getInstitutionById(esbSynch.getInstId());
			if(inst!=null){
				result="true";
				return result;
			}
			return result;
		} catch (Exception e) {
			log.info("判断机构是否重复时"+e);
		}
		return null;
	}
	
	/**
	 * 解析机构类型
	 * @param str
	 * @return
	 */
	public static void parserInstType(ESBSynch inst) {
		if("1".equals(inst.getInstType())) {
			inst.setInstType("0");
		} else if("3".equals(inst.getInstType())) {
			inst.setInstType("1");
		} else if("5".equals(inst.getInstType()) || "7".equals(inst.getInstType())) {
			inst.setInstType("2");
		}
	}
	
}
