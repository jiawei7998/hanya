package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSChkFileAplRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSChkFileAplRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="DealCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DealMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClearDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="remark_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSChkFileAplRs", propOrder = { "commonRsHdr", "dealCode",
		"dealMsg", "clearDate", "fileName", "remark1", "remark2", "remark3",
		"remark4", "remark5" })
public class UPPSChkFileAplRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "DealCode", required = true)
	protected String dealCode;
	@XmlElement(name = "DealMsg", required = true)
	protected String dealMsg;
	@XmlElement(name = "ClearDate", required = true)
	protected String clearDate;
	@XmlElement(name = "FileName", required = true)
	protected String fileName;
	@XmlElement(name = "remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "remark_3", required = true)
	protected String remark3;
	@XmlElement(name = "remark_4", required = true)
	protected String remark4;
	@XmlElement(name = "remark_5", required = true)
	protected String remark5;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the dealCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealCode() {
		return dealCode;
	}

	/**
	 * Sets the value of the dealCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealCode(String value) {
		this.dealCode = value;
	}

	/**
	 * Gets the value of the dealMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealMsg() {
		return dealMsg;
	}

	/**
	 * Sets the value of the dealMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealMsg(String value) {
		this.dealMsg = value;
	}

	/**
	 * Gets the value of the clearDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClearDate() {
		return clearDate;
	}

	/**
	 * Sets the value of the clearDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClearDate(String value) {
		this.clearDate = value;
	}

	/**
	 * Gets the value of the fileName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the value of the fileName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFileName(String value) {
		this.fileName = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the remark3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark3() {
		return remark3;
	}

	/**
	 * Sets the value of the remark3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark3(String value) {
		this.remark3 = value;
	}

	/**
	 * Gets the value of the remark4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark4() {
		return remark4;
	}

	/**
	 * Sets the value of the remark4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark4(String value) {
		this.remark4 = value;
	}

	/**
	 * Gets the value of the remark5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark5() {
		return remark5;
	}

	/**
	 * Sets the value of the remark5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark5(String value) {
		this.remark5 = value;
	}

}
