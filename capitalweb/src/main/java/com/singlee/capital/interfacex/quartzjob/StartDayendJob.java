package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.impl.DayendDateServiceImpl;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.session.impl.SlSessionHelper;

public class StartDayendJob implements CronRunnable {

	private DayendDateService dayendDateServiceImpl = SpringContextHolder.getBean(DayendDateServiceImpl.class);

	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
		
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		JY.info("==========定时跑批开始==========");
		String classList= this.getClass().getName();
		System.out.println("classList:======"+classList);
		Map<String,String> map =new HashMap<String,String>();
		map.put("classList", classList);
		String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
		String jobName=jobExcuService.getJobByclassList(map).get(0).getJobName();

		JobExcuLog jobExcuLog = new JobExcuLog();
		jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
		jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
		jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		jobExcuService.insertJobExcuLog(jobExcuLog);
		LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");
		dayendDateServiceImpl.startDayend(SlSessionHelper.getSlSession());
//		System.err.println("before:"+DayendTask.countDownLatch.getCount());
//		DayendTask.countDownLatch.await();
//		System.out.println("after2:"+DayendTask.countDownLatch.getCount());
//		
		
		/*Map<String, String> params = new HashMap<String, String>();
		params.put("BIZ_DATE", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		
		List<DayendInfoRecord> list = jobExcuService.getDayendJobStatus(params);
		if(list.size()>0){
			for(DayendInfoRecord d:list){
				LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ StartDayendJob --> "+d.getStep_name()+" ]:处理失败--ERROR");
			}
		}else{
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
			jobExcuService.updateJobExcuLog(jobExcuLog);
			
			JY.info("==========定时跑批结束==========");
		}*/
		
		return true;
	}

	@Override
	public void terminate() {
		
	}

}
