package com.singlee.capital.interfacex.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.mapper.TbkSubjectInfoMapper;
import com.singlee.capital.interfacex.model.SubjectInfo;
import com.singlee.capital.interfacex.service.TbkSubjectInfoService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TbkSubjectInfoServiceImpl implements TbkSubjectInfoService {
	@Autowired
	private TbkSubjectInfoMapper tbkSubjectInfoMapper;

	
	@Override
	public Page<SubjectInfo> selectAllSubjectInfoPagedService(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return tbkSubjectInfoMapper.selectAllSubjectInfoPaged(map, rb);
	}
	
	

}
