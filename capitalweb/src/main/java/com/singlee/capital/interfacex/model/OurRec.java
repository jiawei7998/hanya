package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OurRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OurRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OurAcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OurCust" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OurRec", propOrder = { "ourAcctNo", "ourCust" })
public class OurRec {

	@XmlElement(name = "OurAcctNo", required = true)
	protected String ourAcctNo;
	@XmlElement(name = "OurCust", required = true)
	protected String ourCust;

	/**
	 * Gets the value of the ourAcctNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOurAcctNo() {
		return ourAcctNo;
	}

	/**
	 * Sets the value of the ourAcctNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOurAcctNo(String value) {
		this.ourAcctNo = value;
	}

	/**
	 * Gets the value of the ourCust property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOurCust() {
		return ourCust;
	}

	/**
	 * Sets the value of the ourCust property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOurCust(String value) {
		this.ourCust = value;
	}

}
