package com.singlee.capital.interfacex.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.interfacex.model.SubjectInfo;
import com.singlee.capital.interfacex.service.TbkSubjectInfoService;

@Controller
@RequestMapping(value = "/TbkSubjectInfoController")
public class TbkSubjectInfoController {
	@Autowired
	private TbkSubjectInfoService tbkSubjectInfoService;
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	
	@ResponseBody
	@RequestMapping(value = "/selectTbkSubjectInfo")
	public RetMsg<PageInfo<SubjectInfo>> selectTbkSubjectInfo(@RequestBody Map<String, Object> map) throws Exception{
		Page<SubjectInfo> page = tbkSubjectInfoService.selectAllSubjectInfoPagedService(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveTbkSubjectInfo")
	public RetMsg<Serializable> saveTbkSubjectInfo(@RequestBody Map<String, Object> map) throws Exception{
		List<SubjectInfo> list = FastJsonUtil.parseArrays(FastJsonUtil.toJSONString(map.get("entryRows")), SubjectInfo.class);
		for (SubjectInfo subjectInfo : list) {
			Map<String, Object> maps = new HashMap<String, Object>();
			maps.put("subjCode", subjectInfo.getSUB_CODE());
			//如果存在相同  则跳过
			if(tbkSubjectDefService.getTbkSubjectDefSameService(maps).size()>0){
				continue;
			}
			TbkSubjectDef tsd = new TbkSubjectDef();
			tsd.setSubjCode(subjectInfo.getSUB_CODE());
			tsd.setSubjName(subjectInfo.getSUB_NAME());
			tsd.setSubjType(subjectInfo.getSUB_TYPE());
			tsd.setDebitCredit(subjectInfo.getCR_FLAG());
			tsd.setParentSubjCode(subjectInfo.getF_SUB_CODE());
			tbkSubjectDefService.creatTbkSubjectDef(tsd);
		}
		return RetMsgHelper.ok();
	}
	
}
