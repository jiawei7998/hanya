package com.singlee.capital.interfacex.qdb.service;

import java.io.InputStream;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.qdb.model.TtStockPrice;


public interface StockService {

	public boolean importStockPrice(String file);
	
	public String setStockPriceByExcel(String type, InputStream is);
	
	public String insertStockPriceByExcel(String type,InputStream is);
	
	Page<TtStockPrice> pageStock(Map<String, Object> map);
	
	/**
	 * 更新股票价格信息
	 * @param tsp
	 */
	public void updateStock(TtStockPrice tsp);
	
	/**
	 * 增加股票价格信息
	 * @param tsp
	 */
	public void addStock(TtStockPrice tsp);
	public TtStockPrice queryStockById(Map<String, Object> map);
	/**
	 * 删除股票价格信息
	 * @param tsp
	 */
	public void deleteStock(TtStockPrice tsp);
	
	
}
