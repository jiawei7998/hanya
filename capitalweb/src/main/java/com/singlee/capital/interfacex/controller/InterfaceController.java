package com.singlee.capital.interfacex.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.model.*;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.service.TiInterfaceInfoService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.trade.model.TdProductApproveMain;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.*;


@Controller
@RequestMapping(value = "/InterfaceController")
public class InterfaceController {
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private TiInterfaceInfoService tiInterfaceInfoService;
	@Autowired
	private SocketClientService socketClientService;
	
	@Autowired
	private JobExcuService jobExcuService;
	
	@ResponseBody
	@RequestMapping(value = "/queryDependScopePage")
	public RetMsg<PageInfo<TiInterfaceDependScope>> queryDependScopePage(@RequestBody Map<String, Object> map) throws Exception{
		Page<TiInterfaceDependScope> page = tiInterfaceInfoService.queryDependScopePage(map);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/queryInterfaceInfo")
	public RetMsg<List<TiInterfaceInfo>> queryInterfaceInfo(@RequestBody Map<String, Object> map) throws Exception{
		map.put("pageNumber", 1);
		map.put("pageSize", 9999);
		List<TiInterfaceInfo> tiInterfaceInfos = tiInterfaceInfoService.selectInterfaceInfoByName(map).getResult();
		return RetMsgHelper.ok(tiInterfaceInfos);
	}



	@ResponseBody
	@RequestMapping(value = "/deleteDependScope")
	public RetMsg<Serializable>  deleteDependScope(@RequestBody Map<String, Object> map) throws Exception{
		tiInterfaceInfoService.deleteDependScope(map);
		return RetMsgHelper.ok();
	}
	 
	@ResponseBody
	@RequestMapping(value = "/addDependScope")
	public RetMsg<Serializable>  addDependScope(@RequestBody TiInterfaceDependScope tiInterfaceDependScope) throws Exception{
		tiInterfaceInfoService.insertDependScope(tiInterfaceDependScope);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 
	 * @param 删除接口信息
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteInterInfo")
	public RetMsg<Serializable> deleteInteInfo(@RequestBody Map<String, Object> map) throws Exception {
		tiInterfaceInfoService.deleteInterfaceInfo(map);
		return RetMsgHelper.ok();
	}
	

	/**
	 * 
	 * @param 更新接口信息
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/updateInterfaceInfo")
	public RetMsg<Serializable> updateInterfaceInfo(@RequestBody TiInterfaceInfo interfaceInfo) throws Exception {
		tiInterfaceInfoService.updateInterfaceInfo(interfaceInfo);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 
	 * @param 增加接口信息
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/addInterfaceInfo")
	public RetMsg<Serializable> addInterfaceInfo(@RequestBody TiInterfaceInfo interfaceInfo) throws Exception {
		tiInterfaceInfoService.addInterfaceInfo(interfaceInfo);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询产品信息
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/InterfaceInfoList")
	public RetMsg<PageInfo<TiInterfaceInfo>> interfaceInfoPageQuery(@RequestBody Map<String, Object> map) throws Exception {
		Page<TiInterfaceInfo> result = tiInterfaceInfoService.selectInterfaceInfoByName(map);
		return RetMsgHelper.ok(result);
	}
	
	@ResponseBody
	@RequestMapping(value = "/EnterpriseInqQueryList")
	public RetMsg<PageInfo<EnterpriseInqRec>> EnterpriseInqPageQuery(@RequestBody Map<String, Object> map) throws Exception{
		RetMsg<PageInfo<EnterpriseInqRec>> retmsg = new RetMsg<PageInfo<EnterpriseInqRec>>(RetMsgHelper.codeOk,"SUCCESS");
		PageInfo<EnterpriseInqRec> result = new PageInfo<EnterpriseInqRec>();
		EnterpriseInqRq request = new EnterpriseInqRq();
		try {
			request.setEcifNum(map.get("ecifNum").toString());
			List<EnterpriseInqRec> list = new ArrayList<EnterpriseInqRec>();
			System.out.println(request.getEcifNum());
			EnterpriseInqRs response = socketClientService.CrmsEnterpriseInqRequest(request);
			if(response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
				for(EnterpriseInqRec rec : response.getEnterpriseInqRec()){
					rec.setEcifNum(map.get("ecifNum").toString());
					list.add(rec);
				}
				result.setList(list);
			}
			retmsg.setCode("000000".equals(response.getCommonRsHdr().getStatusCode())?"error.common.0000":response.getCommonRsHdr().getStatusCode());
			retmsg.setDesc(response.getCommonRsHdr().getServerStatusCode());
			System.out.println(response.getCommonRsHdr().getStatusCode()+"||"+response.getCommonRsHdr().getServerStatusCode());
			retmsg.setObj(result);
		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());
			
		}
		
		return retmsg;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/FinCreditInqQueryList")
	public RetMsg<PageInfo<FinCreditInqRec>> FinCreditInqPageQuery(@RequestBody Map<String, Object> map) throws Exception{
		RetMsg<PageInfo<FinCreditInqRec>> retmsg = new RetMsg<PageInfo<FinCreditInqRec>>(RetMsgHelper.codeOk,"SUCCESS");
		PageInfo<FinCreditInqRec> result = new PageInfo<FinCreditInqRec>();
		FinCreditInqRq request = new FinCreditInqRq();
		request.setEcifNum(map.get("ecifNum").toString());
		System.out.println(request.getEcifNum());
		List<FinCreditInqRec> list = new ArrayList<FinCreditInqRec>();
		try {
			FinCreditInqRs response = socketClientService.CrmsFinCreditInqRequest(request);
			if(response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
				for(FinCreditInqRec rec : response.getFinCreditInqRec()){
					rec.setEcifNum(map.get("ecifNum").toString());
					list.add(rec);
				}
				result.setList(list);
			}
			retmsg.setCode("000000".equals(response.getCommonRsHdr().getStatusCode())?"error.common.0000":response.getCommonRsHdr().getStatusCode());
			retmsg.setDesc(response.getCommonRsHdr().getServerStatusCode());
			System.out.println(response.getCommonRsHdr().getStatusCode()+"||"+response.getCommonRsHdr().getServerStatusCode());
			retmsg.setObj(result);

		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());
			
		}
		
		

		return retmsg;
	}
	
	

	@ResponseBody
	@RequestMapping(value = "/TDpAcctTypeInq")
	public RetMsg<TDpAcctTypeInqRs> TDpAcctTypeInqRequest(@RequestBody Map<String, Object> map) throws Exception{
		 
		TDpAcctTypeInqRq request = new TDpAcctTypeInqRq();
		request.setMedAcctNo(map.get("acctNo").toString());
		request.setMediumType("A");
		System.out.println(map.get("acctNo").toString());
		TDpAcctTypeInqRs  response = socketClientService.t24TdpAcctTypeInqRequest(request);
		
		return RetMsgHelper.ok(response);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/query2ndPayment")
	public RetMsg<PageInfo<Payment>> query2ndPaymentPage(@RequestBody Map<String, Object> map) throws Exception{
		Page<Payment> result = new Page<Payment>();
		RowBounds rb = ParameterUtil.getRowBounds(map);
		result = tiInterfaceInfoService.query2ndPaymentPage(map, rb);
		return RetMsgHelper.ok(result);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/query2ndPaymentAndSendMsg")
	public RetMsg<PageInfo<Payment>> query2ndPaymentAndSendMsg(@RequestBody Map<String, Object> map) throws Exception{
		
		tiInterfaceInfoService.sendQueryPaymentTradeStatus(map);
		Page<Payment> result = new Page<Payment>();
		RowBounds rb = ParameterUtil.getRowBounds(map);
		result = tiInterfaceInfoService.query2ndPaymentPage(map, rb);
		return RetMsgHelper.ok(result);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/Check2ndPayment")
	public RetMsg<PageInfo<CheckPayment>> Check2ndPayment(@RequestBody Map<String, Object> map) throws Exception{
		Page<CheckPayment> result = new Page<CheckPayment>();
		RowBounds rb = ParameterUtil.getRowBounds(map);
		result = tiInterfaceInfoService.Check2ndPayment(map, rb);
		return RetMsgHelper.ok(result);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/SelfPayment")
	public RetMsg<PageInfo<SelfPayment>> SelfPayment(@RequestBody Map<String, Object> map) throws Exception{
		Page<SelfPayment> result = new Page<SelfPayment>();
		RowBounds rb = ParameterUtil.getRowBounds(map);
		result = tiInterfaceInfoService.SelfPayment(map, rb);
		return RetMsgHelper.ok(result);
	}
	@ResponseBody
	@RequestMapping(value = "/queryTdProductApproveMainByDealno")
	public RetMsg<List<TdProductApproveMain>> queryTdProductApproveMainByDealno(@RequestBody Map<String, Object> map) throws Exception{
		List<TdProductApproveMain> result = tiInterfaceInfoService.queryTdProductApproveMainByDealno(map);
		return RetMsgHelper.ok(result);
	}
	@ResponseBody
	@RequestMapping(value = "/queryTdProductFundByDealno")
	public RetMsg<List<ProductApproveFund>> queryTdProductFundByDealno(@RequestBody Map<String, Object> map) throws Exception{
		List<ProductApproveFund> result = tiInterfaceInfoService.queryTdProductFundByDealno(map);
		return RetMsgHelper.ok(result);
	}
	
	@ResponseBody
	@RequestMapping(value = "/queryTaLogMessage")
	public RetMsg<List<TaLog>> queryTaLogMessage(@RequestBody Map<String, Object> map) throws Exception{
		List<TaLog> result = tiInterfaceInfoService.queryTaLogMessage(map);
		return RetMsgHelper.ok(result);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveHandleCodeRemarkNote")
	public RetMsg<Serializable> saveHandleCodeRemarkNote(@RequestBody Map<String, Object> map) throws Exception {
		tiInterfaceInfoService.insertHandleCodeRemarkNote(map);
		return RetMsgHelper.ok();
	}
	
	/*
	 * 清算导出excel
	 * formal		1 正式  0 基金
	 * dealno  		交易号
	 * workdate		日期
	 * userdefinetrancode	行内还是行外
	 * retcode		0 成功  1失败
	 */
	@SuppressWarnings({ "rawtypes","unchecked"})
	@RequestMapping(value = "/exceptExcel")
	public void exceptExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HashMap map = requestParamToMap(request);
		
		String clear = "UPPSCdtTrf".equals(map.get("userdefinetrancode").toString())?"行外清算":"行内清算";
		String appro = "0".equals(map.get("formal").toString())?"基金":"正式审批";
		String fileName =clear+appro;  
		String encode_filename = URLEncoder.encode(fileName, "utf-8");
        response.setContentType("application/vnd.ms-excel;charset=utf-8");  
        response.setHeader("Content-Disposition", "attachment;filename="+ encode_filename + ".xls");   
        
        HSSFWorkbook wb = new HSSFWorkbook();
        try {
			tiInterfaceInfoService.exceptExcel(map,wb);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			OutputStream ouputStream = response.getOutputStream();
			wb.write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RException(e.getMessage());
		}
	};
	
	/**
	 * 读取request中的参数,转换为map对象
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private HashMap requestParamToMap(HttpServletRequest request) {
		HashMap parameters = new HashMap();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) {
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (!StringUtils.isEmpty(paramValue)) {
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchDayendJobStatus")
	public RetMsg<List<NightJobStatus>> searchDayendJobStatus(@RequestBody Map<String, String> params) throws Exception{
		List<NightJobStatus> result = jobExcuService.searchDayendJobStatus(params);
		return RetMsgHelper.ok(result);
	}
}
