package com.singlee.capital.interfacex.quartzjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.GtpFileService;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;

public class EcifBatchFileImportQuartzJob implements CronRunnable{

	/**
	 * 企业客户管理信息ECIF
	 * 来源：汇聚
	 * 4个文件：BOS_ORG、BOS_O_IDENTIFIER、BOS_O_ADDRESS、BOS_O_CUSTOMERMANAGER
	 */
	private GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
	
	private JobExcuService jobExcuService = SpringContextHolder.getBean("jobExcuService");
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("=======================开始处理========================");
		boolean flag1=true;boolean flag2=true;boolean flag3=true;boolean flag4=true;
		String classList= this.getClass().getName();
		Map<String,String> map =new HashMap<String,String>();
		map.put("classList", classList);
		String jobId=jobExcuService.getJobByclassList(map).get(0).getJobId();
		String jobName = jobExcuService.getJobByclassList(map).get(0).getJobName();
		
		JobExcuLog jobExcuLog = new JobExcuLog();
		jobExcuLog.setJOB_ID(Integer.parseInt(jobId));//需查询
		jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_START);
		jobExcuLog.setEXCU_TIME(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		jobExcuService.insertJobExcuLog(jobExcuLog);
		LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" ]:开始执行-- START");
		
		RetMsg<Object> retMsg1 = gtpFileService.GtpEcifBosoAddressRead();
		RetMsg<Object> retMsg2 = gtpFileService.GtpEcifBosoCustomerManagerRead();
		RetMsg<Object> retMsg3 = gtpFileService.GtpEcifBosoIdentifierRead();
		RetMsg<Object> retMsg4 = gtpFileService.GtpEcifBosOrgRead();
		gtpFileService.insertCustEcifFromBos();
//		List<String> retMsg1List = (List<String>) retMsg1.getObj();
//		List<String> retMsg2List = (List<String>) retMsg2.getObj();
//		List<String> retMsg3List = (List<String>) retMsg3.getObj();
//		List<String> retMsg4List = (List<String>) retMsg4.getObj();
//		
//		List<String> retmsg = new ArrayList<String>();
//		retmsg.addAll(retMsg4List);
//		retmsg.addAll(retMsg3List);
//		retmsg.addAll(retMsg2List);
//		retmsg.addAll(retMsg1List);
//		HashSet hset = new HashSet(retmsg);
//		retmsg.clear();
//		retmsg.addAll(hset);
//		
//		for(String code : retmsg){
//			gtpFileService.EcifCustImport(code);
//		}
		
		if(InterfaceCode.TI_SUCCESS.equals(retMsg1.getCode())){
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoAddress ]:文件读取且处理成功--SUCCESS");
		}else{
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoAddress ]:文件读取或处理失败"+retMsg1.getDesc()+"--ERROR");
			flag1=false;
		}
		
		if(InterfaceCode.TI_SUCCESS.equals(retMsg2.getCode())){
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoCustomerManager ]:文件读取且处理成功--SUCCESS");
		}else{
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoCustomerManager ]:文件读取或处理失败"+retMsg2.getDesc()+"--ERROR");
			flag2=false;
		}
		
		if(InterfaceCode.TI_SUCCESS.equals(retMsg3.getCode())){
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoIdentifier ]:文件读取且处理成功--SUCCESS");
		}else{
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosoIdentifier ]:文件读取或处理失败"+retMsg3.getDesc()+"--ERROR");
			flag3=false;
		}
		
		if(InterfaceCode.TI_SUCCESS.equals(retMsg4.getCode())){
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosOrg ]:文件读取且处理成功--SUCCESS");
		}else{
			LogManager.getLogger(LogManager.MODEL_BATCHSCAN).info("自动批量[ "+jobName+" --> EcifBosOrg ]:文件读取或处理失败"+retMsg4.getDesc()+"--ERROR");
			flag4=false;
		}
			
		if(flag1 && flag2 && flag3 && flag4){
			jobExcuLog.setJOB_STATUS(InterfaceCode.TI_JOB_STATUS_END);
			jobExcuService.updateJobExcuLog(jobExcuLog);
			System.out.println("=======================处理结束========================");
		}
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
