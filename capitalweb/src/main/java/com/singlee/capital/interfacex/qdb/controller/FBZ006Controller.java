package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.interfacex.qdb.ESB.service.EsbClientService;
import com.singlee.capital.interfacex.qdb.pojo.FBZ006;
import com.singlee.capital.interfacex.qdb.util.XmlUtilFactory;
import com.singlee.capital.system.controller.CommonController;
//出账通知
@Controller
@RequestMapping("fbz006")
public class FBZ006Controller extends CommonController{
	public final static Logger log = LoggerFactory.getLogger("ESB");
	@Resource
	private EsbClientService esbClientService;
	@RequestMapping("getFbz006")
	@ResponseBody
	public RetMsg<List<Object>> getResp(HttpServletRequest request,@RequestBody Map<String, Object> map,FBZ006 fbz) throws Exception{
		fbz.setMfCustomerId((String)map.get("mfCustomerId"));
		List<Object> fbzs=sendReq(request,fbz);		
		return RetMsgHelper.ok(fbzs);
	}
	
	public List<Object> sendReq(HttpServletRequest request,FBZ006 fbz) throws Exception{
		List<Object> fbzs=new ArrayList<Object>();
		String esbUrl=PropertiesUtil.parseFile("common.properties").getString("ESB.ServiceUrl")+PropertiesUtil.parseFile("common.properties").getString("FBZ006.URL");
		ByteArrayInputStream byteArrayInputStream = null;
		SAXReader saxReader = new SAXReader();
		String msg="";
		try {
			msg=parseXml(request,fbz);
			log.info(msg);
			HashMap<String, Object> hashMap = this.esbClientService.sendXmlToRequest(esbUrl, msg);
			if(hashMap == null){
				log.info("请求报文时发生网络异常！请检查网络！");
	        }else{
	        	if("200".equals(StringUtils.trimToEmpty(String.valueOf(hashMap.get("statusCode"))))){
	        		byteArrayInputStream = new ByteArrayInputStream((byte[]) hashMap.get("Bytes"));
		        	org.dom4j.Document xmlDocAccept = saxReader.read(byteArrayInputStream);
		        	log.info("FBZ006返回报文"+xmlDocAccept.asXML());
		        	fbzs = XmlUtilFactory.getBeansByXml(FBZ006.class, xmlDocAccept.asXML(),"circularField");		        	
		        	byteArrayInputStream.close();
			      if(hashMap.get("Bytes")!=null){
			    	  return fbzs;
			      }
	        	}
	        }			
		} catch (Exception e) {
			log.error("fbz001接口："+e);
			return null;
		}
		return null;
	}

	public String parseXml(HttpServletRequest request,FBZ006 fbz) {
		Map<String,Map<String,String>> hashmap=new HashMap<String, Map<String,String>>(2);
		Map<String,String> svc=new HashMap<String,String>();
		Map<String,String> body=new HashMap<String,String>();
		svc.put("bizDate",getDate());
		svc.put("refNo",fbz.getRefNo()!=null?fbz.getRefNo():"");
		svc.put("seqNo", fbz.getSeqNo()!=null?fbz.getSeqNo():"");
		svc.put("tmStamp", getDateTime());
		hashmap.put("appHdr", svc);
		body.put("mfCustomerId", fbz.getMfCustomerId()!=null?fbz.getMfCustomerId():"");
		body.put("customerType", fbz.getCustomerType()!=null?fbz.getCustomerType():"");
		body.put("certType", fbz.getCertType()!=null?fbz.getCertType():"");
		body.put("certId", fbz.getCertId()!=null?fbz.getCertId():"");
		body.put("serialNo", fbz.getSerialNo()!=null?fbz.getSerialNo():"");
		hashmap.put("appBody", body);
		return XmlUtilFactory.getXml(request.getSession().getServletContext().getRealPath("/")+ "/WEB-INF/classes/schema/FBZC0006.xml", hashmap);
	}
	public String getDateTime(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	public String getDate(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
}
