package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSigBusiQueryRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSigBusiQueryRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChannelDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChannelSerNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AgentSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayPathCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SendBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntrustDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SettlementDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DealCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DealMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CoreResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankDealCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankDealMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CenterDealCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CenterDealMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSigBusiQueryRec", propOrder = { "channelDate",
		"channelSerNo", "workDate", "agentSerialNo", "payPathCode", "sendBank",
		"entrustDate", "msgId", "settlementDate", "payResult", "dealCode",
		"dealMsg", "coreResult", "bankDealCode", "bankDealMsg",
		"centerDealCode", "centerDealMsg", "bankDate", "bankId",
		"feeBaseLogId", "remark1", "remark2", "remark3", "remark4", "remark5",
		"remark6", "remark7" })
public class UPPSigBusiQueryRec {

	@XmlElement(name = "ChannelDate", required = true)
	protected String channelDate;
	@XmlElement(name = "ChannelSerNo", required = true)
	protected String channelSerNo;
	@XmlElement(name = "WorkDate", required = true)
	protected String workDate;
	@XmlElement(name = "AgentSerialNo", required = true)
	protected String agentSerialNo;
	@XmlElement(name = "PayPathCode", required = true)
	protected String payPathCode;
	@XmlElement(name = "SendBank", required = true)
	protected String sendBank;
	@XmlElement(name = "EntrustDate", required = true)
	protected String entrustDate;
	@XmlElement(name = "MsgId", required = true)
	protected String msgId;
	@XmlElement(name = "SettlementDate", required = true)
	protected String settlementDate;
	@XmlElement(name = "PayResult", required = true)
	protected String payResult;
	@XmlElement(name = "DealCode", required = true)
	protected String dealCode;
	@XmlElement(name = "DealMsg", required = true)
	protected String dealMsg;
	@XmlElement(name = "CoreResult", required = true)
	protected String coreResult;
	@XmlElement(name = "BankDealCode", required = true)
	protected String bankDealCode;
	@XmlElement(name = "BankDealMsg", required = true)
	protected String bankDealMsg;
	@XmlElement(name = "CenterDealCode", required = true)
	protected String centerDealCode;
	@XmlElement(name = "CenterDealMsg", required = true)
	protected String centerDealMsg;
	@XmlElement(name = "BankDate", required = true)
	protected String bankDate;
	@XmlElement(name = "BankId", required = true)
	protected String bankId;
	@XmlElement(name = "FeeBaseLogId", required = true)
	protected String feeBaseLogId;
	@XmlElement(name = "Remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "Remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "Remark_3", required = true)
	protected String remark3;
	@XmlElement(name = "Remark_4", required = true)
	protected String remark4;
	@XmlElement(name = "Remark_5", required = true)
	protected String remark5;
	@XmlElement(name = "Remark_6", required = true)
	protected String remark6;
	@XmlElement(name = "Remark_7", required = true)
	protected String remark7;

	/**
	 * Gets the value of the channelDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelDate() {
		return channelDate;
	}

	/**
	 * Sets the value of the channelDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelDate(String value) {
		this.channelDate = value;
	}

	/**
	 * Gets the value of the channelSerNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelSerNo() {
		return channelSerNo;
	}

	/**
	 * Sets the value of the channelSerNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelSerNo(String value) {
		this.channelSerNo = value;
	}

	/**
	 * Gets the value of the workDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkDate() {
		return workDate;
	}

	/**
	 * Sets the value of the workDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWorkDate(String value) {
		this.workDate = value;
	}

	/**
	 * Gets the value of the agentSerialNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgentSerialNo() {
		return agentSerialNo;
	}

	/**
	 * Sets the value of the agentSerialNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgentSerialNo(String value) {
		this.agentSerialNo = value;
	}

	/**
	 * Gets the value of the payPathCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayPathCode() {
		return payPathCode;
	}

	/**
	 * Sets the value of the payPathCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayPathCode(String value) {
		this.payPathCode = value;
	}

	/**
	 * Gets the value of the sendBank property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSendBank() {
		return sendBank;
	}

	/**
	 * Sets the value of the sendBank property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSendBank(String value) {
		this.sendBank = value;
	}

	/**
	 * Gets the value of the entrustDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntrustDate() {
		return entrustDate;
	}

	/**
	 * Sets the value of the entrustDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntrustDate(String value) {
		this.entrustDate = value;
	}

	/**
	 * Gets the value of the msgId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * Sets the value of the msgId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgId(String value) {
		this.msgId = value;
	}

	/**
	 * Gets the value of the settlementDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the value of the settlementDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettlementDate(String value) {
		this.settlementDate = value;
	}

	/**
	 * Gets the value of the payResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayResult() {
		return payResult;
	}

	/**
	 * Sets the value of the payResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayResult(String value) {
		this.payResult = value;
	}

	/**
	 * Gets the value of the dealCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealCode() {
		return dealCode;
	}

	/**
	 * Sets the value of the dealCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealCode(String value) {
		this.dealCode = value;
	}

	/**
	 * Gets the value of the dealMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealMsg() {
		return dealMsg;
	}

	/**
	 * Sets the value of the dealMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealMsg(String value) {
		this.dealMsg = value;
	}

	/**
	 * Gets the value of the coreResult property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoreResult() {
		return coreResult;
	}

	/**
	 * Sets the value of the coreResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCoreResult(String value) {
		this.coreResult = value;
	}

	/**
	 * Gets the value of the bankDealCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankDealCode() {
		return bankDealCode;
	}

	/**
	 * Sets the value of the bankDealCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankDealCode(String value) {
		this.bankDealCode = value;
	}

	/**
	 * Gets the value of the bankDealMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankDealMsg() {
		return bankDealMsg;
	}

	/**
	 * Sets the value of the bankDealMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankDealMsg(String value) {
		this.bankDealMsg = value;
	}

	/**
	 * Gets the value of the centerDealCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCenterDealCode() {
		return centerDealCode;
	}

	/**
	 * Sets the value of the centerDealCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCenterDealCode(String value) {
		this.centerDealCode = value;
	}

	/**
	 * Gets the value of the centerDealMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCenterDealMsg() {
		return centerDealMsg;
	}

	/**
	 * Sets the value of the centerDealMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCenterDealMsg(String value) {
		this.centerDealMsg = value;
	}

	/**
	 * Gets the value of the bankDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankDate() {
		return bankDate;
	}

	/**
	 * Sets the value of the bankDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankDate(String value) {
		this.bankDate = value;
	}

	/**
	 * Gets the value of the bankId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankId() {
		return bankId;
	}

	/**
	 * Sets the value of the bankId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankId(String value) {
		this.bankId = value;
	}

	/**
	 * Gets the value of the feeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFeeBaseLogId() {
		return feeBaseLogId;
	}

	/**
	 * Sets the value of the feeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFeeBaseLogId(String value) {
		this.feeBaseLogId = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the remark3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark3() {
		return remark3;
	}

	/**
	 * Sets the value of the remark3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark3(String value) {
		this.remark3 = value;
	}

	/**
	 * Gets the value of the remark4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark4() {
		return remark4;
	}

	/**
	 * Sets the value of the remark4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark4(String value) {
		this.remark4 = value;
	}

	/**
	 * Gets the value of the remark5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark5() {
		return remark5;
	}

	/**
	 * Sets the value of the remark5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark5(String value) {
		this.remark5 = value;
	}

	/**
	 * Gets the value of the remark6 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark6() {
		return remark6;
	}

	/**
	 * Sets the value of the remark6 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark6(String value) {
		this.remark6 = value;
	}

	/**
	 * Gets the value of the remark7 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark7() {
		return remark7;
	}

	/**
	 * Sets the value of the remark7 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark7(String value) {
		this.remark7 = value;
	}

}
