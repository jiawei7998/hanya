package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RemitAddrRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RemitAddrRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemitAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemitAddrRec", propOrder = { "remitAddr" })
public class RemitAddrRec {

	@XmlElement(name = "RemitAddr", required = true)
	protected String remitAddr;

	/**
	 * Gets the value of the remitAddr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemitAddr() {
		return remitAddr;
	}

	/**
	 * Sets the value of the remitAddr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemitAddr(String value) {
		this.remitAddr = value;
	}

}
