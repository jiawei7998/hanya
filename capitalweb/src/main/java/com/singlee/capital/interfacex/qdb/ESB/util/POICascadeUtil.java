package com.singlee.capital.interfacex.qdb.ESB.util;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;

/****
 * 
 * 创建excel下拉框 、联动下拉框
 * @author lee
 *
 */
public class POICascadeUtil {
	
	private static String EXCEL_HIDE_SHEET_NAME = "PARAMDATA";   

    private Map<String, List<String>> dataListMap = null;
    private Workbook wb = null;
    
    public POICascadeUtil(Workbook wb,Map<String, List<String>> dataListMap){
    	this.dataListMap = dataListMap;
    	this.wb = wb;
    }
    
    /**   
     * 设置模板文件的横向表头单元格的样式   
     * @param wb   
     * @return   
     */   
    public void createParamDataPage(boolean hidden)throws Exception{ 
        Sheet hideInfoSheet = wb.createSheet(EXCEL_HIDE_SHEET_NAME);//隐藏一些信息     
        //在隐藏页设置选择信息
        int rowIndex = 0;
        Row row = null;
        for (Entry<String, List<String>> entry : dataListMap.entrySet()) 
        {
        	row = hideInfoSheet.createRow(rowIndex);    
        	creatNameList(row,entry.getKey() ,entry.getValue()); 
        	
        	rowIndex++;
		}
        //设置隐藏页标志     
        if(hidden){
        	wb.setSheetHidden(wb.getSheetIndex(EXCEL_HIDE_SHEET_NAME),true);  
        }  
    }
    
    /**   
	 * 创建一列数据   
	 * @param currentRow   
	 * @param textList   
	 */    
	private void creatNameList(Row currentRow,String name,List<String> textList)throws Exception{     
	    if(textList != null && textList.size() > 0)
	    {    
	        int i = 0;     
	        for(String cellValue : textList){     
	            Cell userNameLableCell = currentRow.createCell(i++);     
	            userNameLableCell.setCellValue(cellValue);     
	        }
	        creatExcelNameList(wb, name, currentRow.getRowNum() + 1, textList.size(), false); 
	    }
	}
	
	/****
	 *  
	 * 
	 * @param sheetIndex sheet页索引 从0开始
	 * @param rowStart 从rowStart行开始设置格式 从1开始
	 * @param column 要设置格式的列 A B C
	 * @param rowCount 设置数据验证的行数
	 * @param name  引用的命名
	 */
	public void setColumnValidation(int sheetIndex,int rowStart,String column,int rowCount,String name)throws Exception{
		int columnCount = ExcelUtil.columnToIndex(column);
		Sheet sheet = wb.getSheetAt(sheetIndex);     
        if(!EXCEL_HIDE_SHEET_NAME.equals(sheet.getSheetName()))
        { 
            DataValidation data_validation_list = null;  
            //省份选项添加验证数据      
            for(int row = rowStart; row < rowCount;row++){  
                data_validation_list = getDataValidationByFormula(name,row,columnCount);     
                sheet.addValidationData(data_validation_list);    
            }
        }
	}
    
	/***
	 *    
	 * @param sheetIndex sheet页索引 从0开始
	 * @param rowStart 从rowStart行开始设置格式 从1开始
	 * @param column 要设置格式的列  A B C
	 * @param rowCount 设置数据验证的行数
	 * @param indirectColumn 被引用的列 A B C
	 */
	public void setColumnIndirectValidation(int sheetIndex,int rowStart,String column,int rowCount,String indirectColumn)throws Exception{ 
		int columnCount = ExcelUtil.columnToIndex(column);
		Sheet sheet = wb.getSheetAt(sheetIndex);     
        if(!EXCEL_HIDE_SHEET_NAME.equals(sheet.getSheetName()))
        {
            DataValidation data_validation_list = null;  
            for(int row = rowStart; row < rowCount;row++){  
                data_validation_list = getDataValidationByFormula("INDIRECT($"+ indirectColumn +(row)+")",row,columnCount);     
                sheet.addValidationData(data_validation_list);     
            }  
        } 
	}
    
    public Map<String, List<String>> getDataListMap() {
		return dataListMap;
	}

	public void setDataListMap(Map<String, List<String>> dataListMap) {
		this.dataListMap = dataListMap;
	}

	public Workbook getWb() {
		return wb;
	}

	public void setWb(Workbook wb) {
		this.wb = wb;
	}

	public static void main(String[] args) {
    	try {
    		ExcelUtil eu = new ExcelUtil("f://1.xls");
        	Workbook wb = eu.getWb();
            
        	 //设置下拉列表的内容     
        	List<String> provinceList = new ArrayList<String>();
        	provinceList.add("浙江");
        	provinceList.add("山东");
        	provinceList.add("江西");
        	provinceList.add("四川");
        	
        	List<String> zjProvinceList = new ArrayList<String>();
        	zjProvinceList.add("杭州");
        	zjProvinceList.add("宁波");
        	zjProvinceList.add("温州");
       
    
        	List<String> sdProvinceList = new ArrayList<String>();
        	sdProvinceList.add("济南");
        	sdProvinceList.add("青岛");
        	sdProvinceList.add("烟台");
        	
        	List<String> qdList = new ArrayList<String>();
        	qdList.add("市南区");
        	qdList.add("市北区");
        	qdList.add("城阳区");
              

        	Map<String, List<String>> dataListMap = new LinkedHashMap<String, List<String>>();
        	dataListMap.put("省份_a", provinceList);
        	dataListMap.put("浙江", zjProvinceList);
        	dataListMap.put("山东", sdProvinceList);
        	dataListMap.put("青岛", qdList);
        	
        	POICascadeUtil pu = new POICascadeUtil(wb, dataListMap);
        	
            //创建隐藏的参数页
        	pu.createParamDataPage(false);
        	pu.setColumnValidation(0, 2,"A", 300,"省份_a");//第一级
        	pu.setColumnIndirectValidation(0, 2, "B", 300, "A");//第二级
        	pu.setColumnIndirectValidation(0, 2, "C", 300, "B");//第三极
            
            FileOutputStream fileOut;  
            try {
                fileOut = new FileOutputStream("f://2.xls");  
                pu.getWb().write(fileOut);     
                fileOut.close();  
            } catch (Exception e) {
                e.printStackTrace();  
            }     
		} catch (Exception e) {
			e.printStackTrace();
		}
    }     
         
    /**   
     * 创建一个名称   
     * @param workbook   
     */    
    private static void creatExcelNameList(Workbook workbook,String nameCode,int order,int size,boolean cascadeFlag){     
        Name name;     
        name = workbook.createName();     
        name.setNameName(nameCode);
        name.setRefersToFormula(EXCEL_HIDE_SHEET_NAME+"!"+creatExcelNameList(order,size,cascadeFlag));     
    }     
	      
	/**   
	 * 名称数据行列计算表达式   
	 * @param workbook   
	 */    
	private static String creatExcelNameList(int order,int size,boolean cascadeFlag){     
		 char start = 'A';     
	     if(cascadeFlag){     
	         start = 'B';     
	         if(size<=25){     
	             char end = (char)(start+size-1);     
	             return "$"+start+"$"+order+":$"+end+"$"+order;     
	         }else{     
	             char endPrefix = 'A';     
	             char endSuffix = 'A';     
	             if((size-25)/26==0||size==51){//26-51之间，包括边界（仅两次字母表计算）     
	                 if((size-25)%26==0){//边界值     
	                     endSuffix = (char)('A'+25);     
	                 }else{     
	                     endSuffix = (char)('A'+(size-25)%26-1);     
	                 }     
	             }else{//51以上     
	                 if((size-25)%26==0){     
	                     endSuffix = (char)('A'+25);     
	                     endPrefix = (char)(endPrefix + (size-25)/26 - 1);     
	                 }else{     
	                     endSuffix = (char)('A'+(size-25)%26-1);     
	                     endPrefix = (char)(endPrefix + (size-25)/26);     
	                 }     
	             }     
	             return "$"+start+"$"+order+":$"+endPrefix+endSuffix+"$"+order;     
	         }     
	     }else{     
	         if(size<=26){     
	             char end = (char)(start+size-1);     
	             return "$"+start+"$"+order+":$"+end+"$"+order;     
	         }else{     
	             char endPrefix = 'A';     
	             char endSuffix = 'A';     
	             if(size%26==0){     
	                 endSuffix = (char)('A'+25);     
	                 if(size>52&&size/26>0){     
	                     endPrefix = (char)(endPrefix + size/26-2);     
	                 }     
	             }else{     
	                 endSuffix = (char)('A'+size%26-1);     
	                 if(size>52&&size/26>0){     
	                     endPrefix = (char)(endPrefix + size/26-1);     
	                 }     
	             }     
	             return "$"+start+"$"+order+":$"+endPrefix+endSuffix+"$"+order;     
	         }     
	     }     
	}   
	      
	/**   
	 * 使用已定义的数据源方式设置一个数据验证   
	 * @param formulaString   
	 * @param naturalRowIndex   
	 * @param naturalColumnIndex   
	 * @return   
	 */    
	private static DataValidation getDataValidationByFormula(String formulaString,int naturalRowIndex,int naturalColumnIndex){     
	    //加载下拉列表内容       
	    DVConstraint constraint = DVConstraint.createFormulaListConstraint(formulaString);      
	    //设置数据有效性加载在哪个单元格上。       
	    //四个参数分别是：起始行、终止行、起始列、终止列       
	    int firstRow = naturalRowIndex-1;     
	    int lastRow = naturalRowIndex-1;     
	    int firstCol = naturalColumnIndex-1;     
	    int lastCol = naturalColumnIndex-1;     
	    CellRangeAddressList regions=new CellRangeAddressList(firstRow,lastRow,firstCol,lastCol);       
	    //数据有效性对象      
	    DataValidation data_validation_list = new HSSFDataValidation(regions,constraint);     
	    //设置输入信息提示信息     
	    data_validation_list.createPromptBox("下拉选择提示","请使用下拉方式选择合适的值！");     
	    //设置输入错误提示信息     
	    data_validation_list.createErrorBox("选择错误提示","你输入的值未在备选列表中，请下拉选择合适的值！");     
	    return data_validation_list;     
	}     
	  
	public static DataValidation getDataValidationByDate(int naturalRowIndex,int naturalColumnIndex){     
	    //加载下拉列表内容       
	    DVConstraint constraint = DVConstraint.createDateConstraint(DVConstraint.OperatorType.BETWEEN,"1900-01-01", "5000-01-01", "yyyy-mm-dd");      
	    //设置数据有效性加载在哪个单元格上。       
	    //四个参数分别是：起始行、终止行、起始列、终止列       
	    int firstRow = naturalRowIndex-1;     
	    int lastRow = naturalRowIndex-1;     
	    int firstCol = naturalColumnIndex-1;     
	    int lastCol = naturalColumnIndex-1;     
	    CellRangeAddressList regions=new CellRangeAddressList(firstRow,lastRow,firstCol,lastCol);       
	    //数据有效性对象      
	    DataValidation data_validation_list = new HSSFDataValidation(regions,constraint);     
	    //设置输入信息提示信息     
	    data_validation_list.createPromptBox("日期格式提示","请按照'yyyy-mm-dd'格式输入日期值！");     
	    //设置输入错误提示信息     
	    data_validation_list.createErrorBox("日期格式错误提示","你输入的日期格式不符合'yyyy-mm-dd'格式规范，请重新输入！");     
	    return data_validation_list;     
	}      
}
