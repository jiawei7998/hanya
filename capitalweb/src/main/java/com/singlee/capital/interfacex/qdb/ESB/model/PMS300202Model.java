package com.singlee.capital.interfacex.qdb.ESB.model;


public class PMS300202Model extends BaseModel{
	/**
	 * 头寸系统FPMS03（本币汇款核实服务）
	 */
	private static final long serialVersionUID = 1L;
	
	private String chanNo;  //渠道号 必输项 元素类型:string 长度:10
    private String serNo;  //流水号 必输项 元素类型:string 长度:30
    private String dealDt;  //交易日 必输项 元素类型:string 长度:8
    private String cifNo;  //CIF客户号 必输项 元素类型:string 长度:30
    private String cifNm;  //CIF客户名称 可选项 元素类型:string 长度:50
    private String cifAcctNo;  //客户账号 必输项 元素类型:string 长度:30
    private String prodType;  //产品品种 必输项 元素类型:string 长度:4
    private String ccy;  //货币 必输项 元素类型:string 长度:3
    private double ccyAmt;  //货币金额 必输项 元素类型:double 长度:19
    private double ccyPosAmt;  //货币头寸金额 必输项 元素类型:double
    private String targetCifNo;  //收款方CIF客户号 可选项 元素类型:string 长度:30
    private String targetCifNm;  //收款方CIF客户名 可选项 元素类型:string 长度:50
    private String targetAcctNo;  //收款方账号 必输项 元素类型:string 长度:30
    private String operFlg;  //操作标识 必输项 元素类型:string 长度:2
    private String tellerNo;  //柜员号 必输项 元素类型:string 长度:30
    private String tellerNm;  //柜员名称 必输项 元素类型:string 长度:50
    private String operTm;  //操作时点 必输项 元素类型:string 长度:30
    private String note;  //备注信息 可选项 元素类型:string 长度:255
    private String pmtGrpId;  //支付组号 可选项 元素类型:string 长度:20
    private String orgNo;  //机构号 可选项 元素类型:string 长度:30
    private String orgNm;  //机构名称 可选项 元素类型:string 长度:60
    private String remitType;  //汇款类型 必输项 元素类型:string 长度:2备注:1、普通客户 2、金融机构-->
    private String sysType;  //系统类型 必输项 元素类型:string 长度:4
    private String oldPmtGrpId;  //旧支付组号 可选项 元素类型:string 长度:20
	public String getChanNo() {
		return chanNo;
	}
	public void setChanNo(String chanNo) {
		this.chanNo = chanNo;
	}
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public String getDealDt() {
		return dealDt;
	}
	public void setDealDt(String dealDt) {
		this.dealDt = dealDt;
	}
	public String getCifNo() {
		return cifNo;
	}
	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}
	public String getCifNm() {
		return cifNm;
	}
	public void setCifNm(String cifNm) {
		this.cifNm = cifNm;
	}
	public String getCifAcctNo() {
		return cifAcctNo;
	}
	public void setCifAcctNo(String cifAcctNo) {
		this.cifAcctNo = cifAcctNo;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getCcyAmt() {
		return ccyAmt;
	}
	public void setCcyAmt(double ccyAmt) {
		this.ccyAmt = ccyAmt;
	}
	public double getCcyPosAmt() {
		return ccyPosAmt;
	}
	public void setCcyPosAmt(double ccyPosAmt) {
		this.ccyPosAmt = ccyPosAmt;
	}
	public String getTargetCifNo() {
		return targetCifNo;
	}
	public void setTargetCifNo(String targetCifNo) {
		this.targetCifNo = targetCifNo;
	}
	public String getTargetCifNm() {
		return targetCifNm;
	}
	public void setTargetCifNm(String targetCifNm) {
		this.targetCifNm = targetCifNm;
	}
	public String getTargetAcctNo() {
		return targetAcctNo;
	}
	public void setTargetAcctNo(String targetAcctNo) {
		this.targetAcctNo = targetAcctNo;
	}
	public String getOperFlg() {
		return operFlg;
	}
	public void setOperFlg(String operFlg) {
		this.operFlg = operFlg;
	}
	public String getTellerNo() {
		return tellerNo;
	}
	public void setTellerNo(String tellerNo) {
		this.tellerNo = tellerNo;
	}
	public String getTellerNm() {
		return tellerNm;
	}
	public void setTellerNm(String tellerNm) {
		this.tellerNm = tellerNm;
	}
	public String getOperTm() {
		return operTm;
	}
	public void setOperTm(String operTm) {
		this.operTm = operTm;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getPmtGrpId() {
		return pmtGrpId;
	}
	public void setPmtGrpId(String pmtGrpId) {
		this.pmtGrpId = pmtGrpId;
	}
	public String getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	public String getOrgNm() {
		return orgNm;
	}
	public void setOrgNm(String orgNm) {
		this.orgNm = orgNm;
	}
	public String getRemitType() {
		return remitType;
	}
	public void setRemitType(String remitType) {
		this.remitType = remitType;
	}
	public String getSysType() {
		return sysType;
	}
	public void setSysType(String sysType) {
		this.sysType = sysType;
	}
	public String getOldPmtGrpId() {
		return oldPmtGrpId;
	}
	public void setOldPmtGrpId(String oldPmtGrpId) {
		this.oldPmtGrpId = oldPmtGrpId;
	}
}