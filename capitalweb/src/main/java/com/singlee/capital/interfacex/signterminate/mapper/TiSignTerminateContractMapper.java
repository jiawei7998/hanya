package com.singlee.capital.interfacex.signterminate.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.signterminate.model.SignAccount;

public interface TiSignTerminateContractMapper extends Mapper<SignAccount>{
	public  Page<SignAccount> selectContractInfo(Map<String, Object> map, RowBounds rowBounds)  throws Exception;
	
	public int insertContractInfo(SignAccount signAccount)  throws Exception;
	
//	public int updateContractInfo(Map<String,Object> map);
	
    public SignAccount selectContractByAccount(@Param(value="accountNumber") String accountNumber) throws Exception;
    
    public void updateContractByAccount(String accountNo, String signStatus)  throws Exception;
    
    public void updateContract(SignAccount signAccount);
    
    

}

