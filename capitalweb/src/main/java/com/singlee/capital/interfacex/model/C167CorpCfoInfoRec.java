package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for C167CorpCfoInfoRec complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="C167CorpCfoInfoRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChoiceTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Linkman" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PcoLegalPerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalIdNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CfoAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CfoPostCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CfoTelNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C167CorpCfoInfoRec", propOrder = { "choiceTag", "linkman",
		"name", "pcoLegalPerType", "legalIdNo", "cfoAddress", "cfoPostCode",
		"cfoTelNo" })
public class C167CorpCfoInfoRec {

	@XmlElement(name = "ChoiceTag", required = true)
	protected String choiceTag;
	@XmlElement(name = "Linkman", required = true)
	protected String linkman;
	@XmlElement(name = "Name", required = true)
	protected String name;
	@XmlElement(name = "PcoLegalPerType", required = true)
	protected String pcoLegalPerType;
	@XmlElement(name = "LegalIdNo", required = true)
	protected String legalIdNo;
	@XmlElement(name = "CfoAddress", required = true)
	protected String cfoAddress;
	@XmlElement(name = "CfoPostCode", required = true)
	protected String cfoPostCode;
	@XmlElement(name = "CfoTelNo", required = true)
	protected String cfoTelNo;

	/**
	 * Gets the value of the choiceTag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChoiceTag() {
		return choiceTag;
	}

	/**
	 * Sets the value of the choiceTag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChoiceTag(String value) {
		this.choiceTag = value;
	}

	/**
	 * Gets the value of the linkman property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLinkman() {
		return linkman;
	}

	/**
	 * Sets the value of the linkman property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLinkman(String value) {
		this.linkman = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the pcoLegalPerType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPcoLegalPerType() {
		return pcoLegalPerType;
	}

	/**
	 * Sets the value of the pcoLegalPerType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPcoLegalPerType(String value) {
		this.pcoLegalPerType = value;
	}

	/**
	 * Gets the value of the legalIdNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLegalIdNo() {
		return legalIdNo;
	}

	/**
	 * Sets the value of the legalIdNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLegalIdNo(String value) {
		this.legalIdNo = value;
	}

	/**
	 * Gets the value of the cfoAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCfoAddress() {
		return cfoAddress;
	}

	/**
	 * Sets the value of the cfoAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCfoAddress(String value) {
		this.cfoAddress = value;
	}

	/**
	 * Gets the value of the cfoPostCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCfoPostCode() {
		return cfoPostCode;
	}

	/**
	 * Sets the value of the cfoPostCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCfoPostCode(String value) {
		this.cfoPostCode = value;
	}

	/**
	 * Gets the value of the cfoTelNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCfoTelNo() {
		return cfoTelNo;
	}

	/**
	 * Sets the value of the cfoTelNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCfoTelNo(String value) {
		this.cfoTelNo = value;
	}

}
