package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UPPSigBusiQueryRq complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UPPSigBusiQueryRq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRqHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRqHdr"/>
 *         &lt;element name="UserDefineTranCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrigChannelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrigChannelDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrigChannelSerNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrigFeeBaseLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remark_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IndexNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PageNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UPPSigBusiQueryRq", propOrder = { "commonRqHdr",
		"userDefineTranCode", "origChannelCode", "origChannelDate",
		"origChannelSerNo", "origFeeBaseLogId", "remark1", "remark2",
		"indexNo", "pageNum" })
public class UPPSigBusiQueryRq {

	@XmlElement(name = "CommonRqHdr", required = true)
	protected CommonRqHdr commonRqHdr;
	@XmlElement(name = "UserDefineTranCode", required = true)
	protected String userDefineTranCode;
	@XmlElement(name = "OrigChannelCode", required = true)
	protected String origChannelCode;
	@XmlElement(name = "OrigChannelDate", required = true)
	protected String origChannelDate;
	@XmlElement(name = "OrigChannelSerNo", required = true)
	protected String origChannelSerNo;
	@XmlElement(name = "OrigFeeBaseLogId", required = true)
	protected String origFeeBaseLogId;
	@XmlElement(name = "Remark_1", required = true)
	protected String remark1;
	@XmlElement(name = "Remark_2", required = true)
	protected String remark2;
	@XmlElement(name = "IndexNo", required = true)
	protected String indexNo;
	@XmlElement(name = "PageNum", required = true)
	protected String pageNum;

	/**
	 * Gets the value of the commonRqHdr property.
	 * 
	 * @return possible object is {@link CommonRqHdr }
	 * 
	 */
	public CommonRqHdr getCommonRqHdr() {
		return commonRqHdr;
	}

	/**
	 * Sets the value of the commonRqHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRqHdr }
	 * 
	 */
	public void setCommonRqHdr(CommonRqHdr value) {
		this.commonRqHdr = value;
	}

	/**
	 * Gets the value of the userDefineTranCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserDefineTranCode() {
		return userDefineTranCode;
	}

	/**
	 * Sets the value of the userDefineTranCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserDefineTranCode(String value) {
		this.userDefineTranCode = value;
	}

	/**
	 * Gets the value of the origChannelCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrigChannelCode() {
		return origChannelCode;
	}

	/**
	 * Sets the value of the origChannelCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrigChannelCode(String value) {
		this.origChannelCode = value;
	}

	/**
	 * Gets the value of the origChannelDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrigChannelDate() {
		return origChannelDate;
	}

	/**
	 * Sets the value of the origChannelDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrigChannelDate(String value) {
		this.origChannelDate = value;
	}

	/**
	 * Gets the value of the origChannelSerNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrigChannelSerNo() {
		return origChannelSerNo;
	}

	/**
	 * Sets the value of the origChannelSerNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrigChannelSerNo(String value) {
		this.origChannelSerNo = value;
	}

	/**
	 * Gets the value of the origFeeBaseLogId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrigFeeBaseLogId() {
		return origFeeBaseLogId;
	}

	/**
	 * Sets the value of the origFeeBaseLogId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrigFeeBaseLogId(String value) {
		this.origFeeBaseLogId = value;
	}

	/**
	 * Gets the value of the remark1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark1() {
		return remark1;
	}

	/**
	 * Sets the value of the remark1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark1(String value) {
		this.remark1 = value;
	}

	/**
	 * Gets the value of the remark2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark2() {
		return remark2;
	}

	/**
	 * Sets the value of the remark2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark2(String value) {
		this.remark2 = value;
	}

	/**
	 * Gets the value of the indexNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndexNo() {
		return indexNo;
	}

	/**
	 * Sets the value of the indexNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndexNo(String value) {
		this.indexNo = value;
	}

	/**
	 * Gets the value of the pageNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPageNum() {
		return pageNum;
	}

	/**
	 * Sets the value of the pageNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPageNum(String value) {
		this.pageNum = value;
	}

}
