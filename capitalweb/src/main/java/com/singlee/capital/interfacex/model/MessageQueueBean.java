package com.singlee.capital.interfacex.model;

import java.io.Serializable;

public class MessageQueueBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ccsId;
	private String character;
	private String waitInterval;
	private String messageID;
	private String serverMessageID;
	private String hostName;
	private String port;
	private String channel;
	private String qManager;
	private String receiveQname;
	private String retContext;
	public String getCcsId() {
		return ccsId;
	}
	public void setCcsId(String ccsId) {
		this.ccsId = ccsId;
	}
	public String getCharacter() {
		return character;
	}
	public void setCharacter(String character) {
		this.character = character;
	}
	public String getWaitInterval() {
		return waitInterval;
	}
	public void setWaitInterval(String waitInterval) {
		this.waitInterval = waitInterval;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public String getServerMessageID() {
		return serverMessageID;
	}
	public void setServerMessageID(String serverMessageID) {
		this.serverMessageID = serverMessageID;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getqManager() {
		return qManager;
	}
	public void setqManager(String qManager) {
		this.qManager = qManager;
	}
	public String getReceiveQname() {
		return receiveQname;
	}
	public void setReceiveQname(String receiveQname) {
		this.receiveQname = receiveQname;
	}
	public String getRetContext() {
		return retContext;
	}
	public void setRetContext(String retContext) {
		this.retContext = retContext;
	}

}
