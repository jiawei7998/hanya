package com.singlee.capital.interfacex.qdb.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.qdb.model.TtStockPrice;
import com.singlee.capital.interfacex.qdb.service.StockService;

@Controller
@RequestMapping(value="StockController")
public class StockController{
	public final static Logger log = LoggerFactory.getLogger("CRON");
	@Autowired
	private StockService stockService;	

	
	@ResponseBody
	@RequestMapping(value = "/pageStock")
	public RetMsg<PageInfo<TtStockPrice>> pageStock(@RequestBody Map<String, Object> map){
		Page<TtStockPrice> list = stockService.pageStock(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 手动上传股票价格Excel
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadsStockExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadStockExcel(MultipartHttpServletRequest request,HttpServletResponse response) {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = null;
		String ret = "导入失败";
		try {
			uploadedFileList = FileManage.requestExtractor(request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 2.交验并处理成excel
		for (UploadedFile uploadedFile : uploadedFileList) {
			String name = uploadedFile.getFullname();
			InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
			ret = stockService.insertStockPriceByExcel(name, is);
		}
		return ret;
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateStock")
	public RetMsg<Serializable> updateStock(@RequestBody TtStockPrice tsp) {
		RetMsg<Serializable> msg = RetMsgHelper.ok("");
		try {
			stockService.updateStock(tsp);
			msg.setDesc("保存成功");
			return msg;
		} catch (Exception e) {
			msg.setDesc(""+e.getMessage());
			return msg;
		}

	}

	@ResponseBody
	@RequestMapping(value = "/addStock")
	public  RetMsg<Serializable>  addStock(@RequestBody TtStockPrice tsp) {
		RetMsg<Serializable> msg = RetMsgHelper.ok("");
		try {
			stockService.addStock(tsp);
			msg.setDesc("保存成功");
			return msg;
		} catch (Exception e) {
			msg.setDesc(""+e.getMessage());
			return msg;
		}
		
	}
	@ResponseBody
	@RequestMapping(value = "/queryStockById")
	public  RetMsg<TtStockPrice>  queryStockById(@RequestBody Map<String, Object> map) {
		TtStockPrice tp =stockService.queryStockById(map);
		return RetMsgHelper.ok(tp);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteStock")
	public int deleteStock(@RequestBody TtStockPrice tsp) {
		stockService.deleteStock(tsp);
		return 0;
	}

}
