package com.singlee.capital.interfacex.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CloudProApplyTransRs complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CloudProApplyTransRs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommonRsHdr" type="{http://www.bankofshanghai.com/BOSFX/2010/08}CommonRsHdr"/>
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MsgTypId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DepCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CardCrDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EnddDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BegDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloudProApplyTransRs", propOrder = { "commonRsHdr",
		"productId", "msgTypId", "prdName", "busType", "sector", "depCode",
		"cardCrDt", "enddDate", "begDate" })
public class CloudProApplyTransRs {

	@XmlElement(name = "CommonRsHdr", required = true)
	protected CommonRsHdr commonRsHdr;
	@XmlElement(name = "ProductId", required = true)
	protected String productId;
	@XmlElement(name = "MsgTypId", required = true)
	protected String msgTypId;
	@XmlElement(name = "PrdName", required = true)
	protected String prdName;
	@XmlElement(name = "BusType", required = true)
	protected String busType;
	@XmlElement(name = "Sector", required = true)
	protected String sector;
	@XmlElement(name = "DepCode", required = true)
	protected String depCode;
	@XmlElement(name = "CardCrDt", required = true)
	protected String cardCrDt;
	@XmlElement(name = "EnddDate", required = true)
	protected String enddDate;
	@XmlElement(name = "BegDate", required = true)
	protected String begDate;

	/**
	 * Gets the value of the commonRsHdr property.
	 * 
	 * @return possible object is {@link CommonRsHdr }
	 * 
	 */
	public CommonRsHdr getCommonRsHdr() {
		return commonRsHdr;
	}

	/**
	 * Sets the value of the commonRsHdr property.
	 * 
	 * @param value
	 *            allowed object is {@link CommonRsHdr }
	 * 
	 */
	public void setCommonRsHdr(CommonRsHdr value) {
		this.commonRsHdr = value;
	}

	/**
	 * Gets the value of the productId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * Sets the value of the productId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductId(String value) {
		this.productId = value;
	}

	/**
	 * Gets the value of the msgTypId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMsgTypId() {
		return msgTypId;
	}

	/**
	 * Sets the value of the msgTypId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMsgTypId(String value) {
		this.msgTypId = value;
	}

	/**
	 * Gets the value of the prdName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrdName() {
		return prdName;
	}

	/**
	 * Sets the value of the prdName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrdName(String value) {
		this.prdName = value;
	}

	/**
	 * Gets the value of the busType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusType() {
		return busType;
	}

	/**
	 * Sets the value of the busType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusType(String value) {
		this.busType = value;
	}

	/**
	 * Gets the value of the sector property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * Sets the value of the sector property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSector(String value) {
		this.sector = value;
	}

	/**
	 * Gets the value of the depCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDepCode() {
		return depCode;
	}

	/**
	 * Sets the value of the depCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDepCode(String value) {
		this.depCode = value;
	}

	/**
	 * Gets the value of the cardCrDt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardCrDt() {
		return cardCrDt;
	}

	/**
	 * Sets the value of the cardCrDt property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardCrDt(String value) {
		this.cardCrDt = value;
	}

	/**
	 * Gets the value of the enddDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnddDate() {
		return enddDate;
	}

	/**
	 * Sets the value of the enddDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnddDate(String value) {
		this.enddDate = value;
	}

	/**
	 * Gets the value of the begDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBegDate() {
		return begDate;
	}

	/**
	 * Sets the value of the begDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBegDate(String value) {
		this.begDate = value;
	}

}
