package com.singlee.capital.tpos.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.tpos.model.TdTrdTpos;

import tk.mybatis.mapper.common.Mapper;
/**
 * 操作头寸的表
 * @author SINGLEE
 *
 */
public interface TrdTposMapper extends Mapper<TdTrdTpos>{
	/**
	 * 获取唯一的头寸记录
	 * @param map
	 * @return
	 */
	public TdTrdTpos getTdTrdTposByKey(Map<String,Object> map);
	
	public Page<TdTrdTpos> getTdTrdTposPage(Map<String,Object> map,RowBounds rb);
	
	public void updateClear(String dealNo);
}
