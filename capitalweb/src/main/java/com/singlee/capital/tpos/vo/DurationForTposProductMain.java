package com.singlee.capital.tpos.vo;

import com.singlee.capital.trade.model.TdProductApproveMain;

public class DurationForTposProductMain extends TdProductApproveMain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double settlAmt;//清算金额
	
	private double conctAmt;//合同金额
	
	private double retrnAmt;//归还本金累计
	
	private double interestSettleAmt; //本期结算金额

	public double getSettlAmt() {
		return settlAmt;
	}

	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}

	public double getConctAmt() {
		return conctAmt;
	}

	public void setConctAmt(double conctAmt) {
		this.conctAmt = conctAmt;
	}

	public double getRetrnAmt() {
		return retrnAmt;
	}

	public void setRetrnAmt(double retrnAmt) {
		this.retrnAmt = retrnAmt;
	}

	public double getInterestSettleAmt() {
		return interestSettleAmt;
	}

	public void setInterestSettlAmt(double interestSettleAmt) {
		this.interestSettleAmt = interestSettleAmt;
	}

}
