package com.singlee.capital.tpos.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.tpos.mapper.DurationForTposProductMainMapper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.tpos.vo.DurationForTposProductMain;
import com.singlee.capital.trade.model.TdProductApproveMain;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrdTposServiceImpl implements TrdTposService {
	@Autowired
	TrdTposMapper tposMapper;
	@Autowired
	DayendDateService dateService;
	@Autowired
	DurationForTposProductMainMapper durationForTposProductMainMapper;
	@Override
	public void insertTposByTrade(Map<String, Object> params) {
		
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		try{
			BeanUtil.populate(productApproveMain, params);
			TdTrdTpos tdTrdTpos = new TdTrdTpos();
			tdTrdTpos.setDealNo(productApproveMain.getDealNo());
			tdTrdTpos.setVersion(productApproveMain.getVersion());
			tdTrdTpos.setSettlAmt(productApproveMain.getAmt());//清算本金
			tdTrdTpos.setConctAmt(productApproveMain.getAmt());//合同本金
			tdTrdTpos.setPostdate(dateService.getDayendDate());
			tdTrdTpos.setProcAmt(productApproveMain.getProcAmt());//清算金额
			tdTrdTpos.setDisAmt(productApproveMain.getDisAmt());//折溢价
			tdTrdTpos.setUnamorInt(productApproveMain.getDisAmt());//未摊销部分
			tposMapper.insert(tdTrdTpos);
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
	}
	
	@Override
	public Page<DurationForTposProductMain> pageDurationForTposProductMainCustoms(
			Map<String, Object> map) {
		
		return durationForTposProductMainMapper.getDurationForTposProductMain(map, ParameterUtil.getRowBounds(map));
	}
	@Override
	public Page<DurationForTposProductMain> searchDurationCurrentCustomVoPage(
			Map<String, Object> map) {
		
		return durationForTposProductMainMapper.searchDurationCurrentCustomVoPage(map, ParameterUtil.getRowBounds(map));
	}
	

	@Override
	public TdTrdTpos getTdTrdTposByKey(Map<String, Object> map) {
		return tposMapper.getTdTrdTposByKey(map);
	}

	@Override
	public Page<TdTrdTpos> getTdTrdTposPage(Map<String, Object> params,RowBounds rb) {
		return tposMapper.getTdTrdTposPage(params,rb);
	}

	@Override
	public Page<DurationForTposProductMain> searchProductMainLdNoUpdate(Map<String, Object> map) {
		if(map.containsKey("cnName")&&map.get("cnName")!=null){
			String cnName=map.get("cnName").toString().trim();
			if(cnName.indexOf(" ")>0){
				String[] cnNameArr=cnName.split(" ");
				String cnNameStr="";
				int j=0;
				for(int i=0;i<cnNameArr.length;i++){
					if(cnNameArr[i].length()>0){
						map.put("cnName"+j, cnNameArr[i]);
						cnNameStr +=cnNameArr[i];
						j++;
					}
				}
				if (j<=5){
					map.remove("cnName");
					map.put("partyName"+j,cnNameStr);
				}else{
					map.remove("cnName");
					map.put("cnName",cnNameStr);
				}
			}
		}
		return durationForTposProductMainMapper.searchProductMainLdNoUpdate(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public void updateClear(String dealNo) {
		tposMapper.updateClear(dealNo);
	}

	@Override
	public Page<DurationForTposProductMain> pageDurationForTposProductMainCustomsTDJH(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationForTposProductMainTDJH(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期交易展期的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationTradeExtend(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationTradeExtend(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期资产到期的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationTradeExpire(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationTradeExpire(map, ParameterUtil.getRowBounds(map));
	}
	
	
	/**
	 * 存续期交易冲正的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationTradeOffset(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationTradeOffset(map, ParameterUtil.getRowBounds(map));
	}
	
	
	/**
	 * 存续期提前还款的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationAdvanceMaturity(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationAdvanceMaturity(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期还本计划的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationCapitalSchedule(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationCapitalSchedule(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期利率变更的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationRateChange(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationRateChange(map, ParameterUtil.getRowBounds(map));
	}

	/**
	 * 存续期收息计划确认的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationInterestSchedule(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationInterestSchedule(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期资产卖断的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationOutRightSale(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationOutRightSale(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期资产转让的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationOutRightSaleZCZR(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationOutRightSaleZCZR(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期通道计划的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationPassageWay(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationPassageWay(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 存续期利率重订的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageDurationRateReset(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getDurationRateReset(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 清算交易结清的查询
	 * @param map
	 * @return
	 */
	@Override
    public Page<DurationForTposProductMain> pageBalance(
			Map<String, Object> map) {
		return durationForTposProductMainMapper.getBalance(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<DurationForTposProductMain> searchDurationForTposProductMain(
			Map<String, Object> params) {
		return durationForTposProductMainMapper.getDurationForTposProductMain(params);
	}

	@Override
	public Page<DurationForTposProductMain> getBalance(Map<String, Object> map) {
		return durationForTposProductMainMapper.getBalance(map, ParameterUtil.getRowBounds(map));
	}

}
