package com.singlee.capital.tpos.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.vo.DurationForTposProductMain;

/**
 * 操作总持仓的服务类
 * @author SINGLEE
 *
 */
public interface TrdTposService {
	/**
	 * 插入持仓头寸数据  params=TdProductApproveMain
	 * @param params
	 */
	public void insertTposByTrade(Map<String, Object> params);
	
	/**
	 * 通过交易号和版本查询
	 * @param map
	 * @return
	 */
	public TdTrdTpos getTdTrdTposByKey(Map<String,Object> map);
	/**
	 * 分页查询存续期自定义产品余额信息
	 * @param map 参数：
	 * 				  customPrdTypeList:产品类型
	 * 				  customPrdName：产品名称，模糊查询
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationForTposProductMainCustoms(Map<String, Object> map);
	
	/**
	 * LD编号补录
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> searchProductMainLdNoUpdate(Map<String, Object> map);
	
	Page<DurationForTposProductMain> getBalance(Map<String, Object> map);
	
	/**
	 * 分页查询活期需结息确认交易
	 * @return
	 */
	Page<DurationForTposProductMain> searchDurationCurrentCustomVoPage(Map<String, Object> map);
	
	public Page<TdTrdTpos> getTdTrdTposPage(Map<String, Object> params,RowBounds rb);
	

	/*************************************/
	
	/**
	 * 存续期交易展期的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationTradeExtend(Map<String, Object> map);
	
	/**
	 * 存续期交易冲正的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationTradeOffset(Map<String, Object> map);
	
	/**
	 * 存续期提前还款的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationAdvanceMaturity(Map<String, Object> map);
	
	/**
	 * 存续期还本计划调整的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationCapitalSchedule(Map<String, Object> map);
	
	/**
	 * 存续期利率变更的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationRateChange(Map<String, Object> map);
	
	/**
	 * 存续期收息计划的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationOutRightSale(Map<String, Object> map);
	
	
	/**
	 * 存续期资产转让的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationOutRightSaleZCZR(Map<String, Object> map);
	/**
	 * 存续期资产卖断的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationInterestSchedule(Map<String, Object> map);
	
	
	/**
	 * 存续期通道计划的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationPassageWay(Map<String, Object> map);
	
	/**
	 * 存续期利率重订的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationRateReset(Map<String, Object> map);
	
	
	/**
	 * 清算交易结清的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> pageBalance(Map<String, Object> map);
	
	public void updateClear(String dealNo);
	
	/**
	 * 分页查询存续期自定义产品余额信息--通道计划
	 * @param map 参数：
	 * 				  customPrdTypeList:产品类型
	 * 				  customPrdName：产品名称，模糊查询
	 * @author yushi
	 * @return
	 */
	Page<DurationForTposProductMain> pageDurationForTposProductMainCustomsTDJH(Map<String, Object> map);

	/**
	 * 存续期资产到期的查询
	 * @param map
	 * @return
	 */
	public Page<DurationForTposProductMain> pageDurationTradeExpire(Map<String, Object> params);
	public List<DurationForTposProductMain> searchDurationForTposProductMain(Map<String, Object> params);
}
