package com.singlee.capital.tpos.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.tpos.vo.DurationForTposProductMain;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TrdEventService;
@Controller
@RequestMapping(value = "/trdTposController", method = RequestMethod.POST)
public class TrdTposController extends CommonController {

	@Autowired
	private ProductService productService;
	@Autowired
	private TrdTposService tposService;
	@Autowired
	private TrdEventService trdEventService;
	@Autowired
	ProductApproveService productApproveService;
	
	/**
	 * 查询自定义产品信息
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadCustomProduct")
	@ResponseBody
	public RetMsg<List<TcProduct>> loadCustomProduct(@RequestBody Map<String, Object> paramMap) throws IOException {
		paramMap.put("is_active", DictConstants.YesNo.YES);
		List<TcProduct> retList = productService.getProductList(paramMap);
		RetMsg<List<TcProduct>> ret = RetMsgHelper.ok(retList);
		if(RetMsgHelper.codeOk.equals(ret.getCode())){
		}
	    return ret;
	}
	
	/**
	 * 存续期原交易选择列表（有余额） 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDurationCustomVoPage")
	public RetMsg<PageInfo<DurationForTposProductMain>> searchDurationCustomVoPage(@RequestBody Map<String,Object> params){
        String tradeId = ParameterUtil.getString(params, "trade_id", "");
        params.put("i_code", tradeId);
        /*boolean isHeaderInst =ParameterUtil.getBoolean(params, "isHeaderInst", false);
        if(!isHeaderInst){
        	params.put("spon_inst", SlSessionHelper.getInstitutionId());
        }*/
		Page<DurationForTposProductMain> page = tposService.pageDurationForTposProductMainCustoms(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBalance")
	public RetMsg<PageInfo<DurationForTposProductMain>> getBalance(@RequestBody Map<String,Object> params){
		Page<DurationForTposProductMain> page = tposService.getBalance(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 活期存续期原交易选择列表（有余额）
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDurationCurrentCustomVoPage")
	public RetMsg<PageInfo<DurationForTposProductMain>> searchDurationCurrentCustomVoPage(@RequestBody Map<String,Object> params){
        String tradeId = ParameterUtil.getString(params, "trade_id", "");
        params.put("i_code", tradeId);
		Page<DurationForTposProductMain> page = tposService.searchDurationCurrentCustomVoPage(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 通过交易号和版本查询交易tops
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdTrdTposByKey")
	public RetMsg<List<TdTrdTpos>> searchTdTrdTposByKey(@RequestBody Map<String,Object> params){
		TdTrdTpos page = tposService.getTdTrdTposByKey(params);
		List<TdTrdTpos> list = new ArrayList<TdTrdTpos>();
		list.add(page);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 通过交易号查询交易事件
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTrdEventByKey")
	public RetMsg<List<TdTrdEvent>> searchTrdEventByKey(@RequestBody Map<String,Object> params){
		List<TdTrdEvent> page = trdEventService.searchTrdEventByKey(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTdTrdTposPage")
	public RetMsg<PageInfo<TdTrdTpos>> getTdTrdTposPage(@RequestBody Map<String,Object> params){
		Page<TdTrdTpos> page = tposService.getTdTrdTposPage(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	
	
	
	/**
	 * 获取自己处理的业务 ，包含业务归属
	 */
	@ResponseBody
	@RequestMapping(value = "/getProductApprovePageByInst")
	public RetMsg<PageInfo<TdProductApproveMain>> getProductApprovePageByInst(@RequestBody Map<String,Object> params){
       
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePageByInst(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * LD编号补录
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductMainLdNoUpdate")
	public RetMsg<PageInfo<DurationForTposProductMain>> searchProductMainLdNoUpdate(@RequestBody Map<String,Object> params){
        /*boolean isHeaderInst =ParameterUtil.getBoolean(params, "isHeaderInst", false);
        if(!isHeaderInst){
        	params.put("spon_inst", SlSessionHelper.getInstitutionId());
        }*/
		
		Page<DurationForTposProductMain> page = tposService.searchProductMainLdNoUpdate(params);
		return RetMsgHelper.ok(page);
	}

	
	/**
	 * 存续期原交易选择列表（有余额）
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDurationCustomVoPageByType")
	public RetMsg<PageInfo<DurationForTposProductMain>> searchDurationCustomVoPageByType(@RequestBody Map<String,Object> params){
		 String tradeId = ParameterUtil.getString(params, "trade_id", "");
	     String type=ParameterUtil.getString(params, "type", "");
	     params.put("i_code", tradeId);
	     params.put("type", type);
		 String businessType  =ParameterUtil.getString(params, "businessType", "");
		 if("TradeExtend".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationTradeExtend(params);
			 return RetMsgHelper.ok(page);
		 }else if("TradeExpire".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationTradeExpire(params);
			 return RetMsgHelper.ok(page);
		 }else if("TradeOffset".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationTradeOffset(params);
			 return RetMsgHelper.ok(page);
		 }else if("AdvanceMaturity".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationAdvanceMaturity(params);
			 return RetMsgHelper.ok(page);
		 }else if("TrasForProductCapitalScheduleManage".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationCapitalSchedule(params);
			 return RetMsgHelper.ok(page);
		 }else if("RateChangeManage".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationRateChange(params);
			 return RetMsgHelper.ok(page);
		 }else if("TrasForProductInterestScheduleManage".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationInterestSchedule(params);
			 return RetMsgHelper.ok(page);
		 }else if("TradeOutRightSale".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationOutRightSale(params);
			 return RetMsgHelper.ok(page);
		 }else if("TradeOutRightSaleZCZR".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationOutRightSaleZCZR(params);
			 return RetMsgHelper.ok(page);
		 }else if("PassageWayFeesManage".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationPassageWay(params);
			 return RetMsgHelper.ok(page);
		 }else if("RateResetManage".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageDurationRateReset(params);
			 return RetMsgHelper.ok(page);
		 }else if("Balance".equals(businessType)){
			 Page<DurationForTposProductMain> page = tposService.pageBalance(params);
			 return RetMsgHelper.ok(page);
		 }
		 else{
		    Page<DurationForTposProductMain> page = tposService.pageDurationForTposProductMainCustoms(params);
			return RetMsgHelper.ok(page);
		}

	}
	/**
	 * 根据交易号获取持仓本金
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSettlAmt")
	public RetMsg<List<DurationForTposProductMain>> searchDurationForTposProductMain(@RequestBody Map<String,Object> params){
		List<DurationForTposProductMain> list =tposService.searchDurationForTposProductMain(params);
		return RetMsgHelper.ok(list);
	}
}
