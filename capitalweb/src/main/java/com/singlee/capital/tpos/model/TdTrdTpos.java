package com.singlee.capital.tpos.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="TD_TRD_TPOS")
public class TdTrdTpos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//业务编号
	@Id
	private int version;//版本号
	
	private double settlAmt;//清算金额
	
	private double conctAmt;//合同金额
	
	private double retrnAmt;//归还本金累计
	
	private double accruedTint;//应计利息累计致当前日180
	private double actualTint;//利息收入514累计致当日
	private double overdueAccruedTint;//逾期应计利息累计致当日 180
	private double overdueActualTint;//逾期利息累计致当日 514
	private int overdueDays;//已逾期天数
	private double amorTint;//已摊销累计致当日
	private double procAmt;//实际清算金额
	private double disAmt;//折溢价金额 
	private double unamorInt;//未摊销金额
	private double penaltyAmt;//违约，罚息金额
	private double interestSettleAmt;// 当前结息金额
	public double getPenaltyAmt() {
		return penaltyAmt;
	}

	public void setPenaltyAmt(double penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}

	public double getOverdueAccruedTint() {
		return overdueAccruedTint;
	}

	public void setOverdueAccruedTint(double overdueAccruedTint) {
		this.overdueAccruedTint = overdueAccruedTint;
	}

	public double getOverdueActualTint() {
		return overdueActualTint;
	}

	public void setOverdueActualTint(double overdueActualTint) {
		this.overdueActualTint = overdueActualTint;
	}

	public double getUnamorInt() {
		return unamorInt;
	}

	public void setUnamorInt(double unamorInt) {
		this.unamorInt = unamorInt;
	}

	private String postdate;

	public double getProcAmt() {
		return procAmt;
	}

	public void setProcAmt(double procAmt) {
		this.procAmt = procAmt;
	}

	public double getDisAmt() {
		return disAmt;
	}

	public void setDisAmt(double disAmt) {
		this.disAmt = disAmt;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public double getSettlAmt() {
		return settlAmt;
	}

	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}

	public double getConctAmt() {
		return conctAmt;
	}

	public void setConctAmt(double conctAmt) {
		this.conctAmt = conctAmt;
	}

	public double getRetrnAmt() {
		return retrnAmt;
	}

	public void setRetrnAmt(double retrnAmt) {
		this.retrnAmt = retrnAmt;
	}
	public double getAccruedTint() {
		return accruedTint;
	}

	public void setAccruedTint(double accruedTint) {
		this.accruedTint = accruedTint;
	}
	public double getActualTint() {
		return actualTint;
	}

	public void setActualTint(double actualTint) {
		this.actualTint = actualTint;
	}
	public int getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(int overdueDays) {
		this.overdueDays = overdueDays;
	}
	public double getAmorTint() {
		return amorTint;
	}

	public void setAmorTint(double amorTint) {
		this.amorTint = amorTint;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public double getInterestSettleAmt() {
		return interestSettleAmt;
	}

	public void setInterestSettleAmt(double interestSettleAmt) {
		this.interestSettleAmt = interestSettleAmt;
	}

}
