package com.singlee.capital.tpos.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.tpos.vo.DurationForTposProductMain;

public interface DurationForTposProductMainMapper extends Mapper<DurationForTposProductMain> {
	/**
	 * 存续期有余额的交易
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationForTposProductMain(Map<String, Object> map, RowBounds rb);
	
	/**
	 * LD编号补录
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<DurationForTposProductMain> searchProductMainLdNoUpdate(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期有余额的交易
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<DurationForTposProductMain> searchDurationCurrentCustomVoPage(Map<String, Object> map, RowBounds rb);
	

	/**********************************************************/
	/**
	 * 存续期有余额的交易--通道计划
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationForTposProductMainTDJH(Map<String, Object> map, RowBounds rb);
	
	
	/**
	 * 存续期交易展期的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationTradeExtend(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期资产到期的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationTradeExpire(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期交易冲正的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationTradeOffset(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期提前还款的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationAdvanceMaturity(Map<String, Object> map, RowBounds rb);
	

	/**
	 * 存续期还本计划的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationCapitalSchedule(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期利率变更的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationRateChange(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期收息计划的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationInterestSchedule(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期资产卖断的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationOutRightSale(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期资产转让的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationOutRightSaleZCZR(Map<String, Object> map, RowBounds rb);

	/**
	 * 存续期通道计划的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationPassageWay(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期利率重订的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getDurationRateReset(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期交易结清的查询
	 * @param map
	 * @return
	 */
	Page<DurationForTposProductMain> getBalance(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 存续期有余额的交易
	 * @param map
	 * @param rb
	 * @return
	 */
	List<DurationForTposProductMain> getDurationForTposProductMain(Map<String, Object> map);
	
}
