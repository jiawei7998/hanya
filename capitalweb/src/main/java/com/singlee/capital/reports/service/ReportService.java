package com.singlee.capital.reports.service;

import java.io.Serializable;
import java.util.Map;

import com.singlee.capital.common.pojo.RetMsg;

public interface ReportService {
	
	public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception;
	
	public Object queryRep(Map<String, Object> map) throws Exception;
	
	public byte[] exportRep(Map<String, Object> map) throws Exception;
	
	
}
