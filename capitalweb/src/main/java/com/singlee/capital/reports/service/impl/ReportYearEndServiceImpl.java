package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.accounting.mapper.TbkYearBalanceMapper;
import com.singlee.cap.accounting.model.TbkYearBalance;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;
@Service("reportYearEndServiceImpl")
public class ReportYearEndServiceImpl implements ReportService {
	@Autowired
	private TbkYearBalanceMapper tbkYearBalanceMapper;
	
	
	
	@Override
	public RetMsg<Serializable> transRepToHtml(Map<String, Object> map)
			throws Exception {
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil e = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.YearEnd.getFileName()));
		
		transportExcel(e, map);
		
		ret.setObj(ExcelShower.transExcelToHtml(e.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}
	
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		map.put("postDate", ParameterUtil.getString(map, "reportDate", ""));
		CellStyle center = e.getDefaultCenterStrCellStyle();
		int row = 1;
		
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		e.getSheetAt(0).setColumnWidth(1, 20 * 256);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		e.getSheetAt(0).setColumnWidth(3, 20 * 256);
		e.getSheetAt(0).setColumnWidth(4, 20 * 256);
		e.getSheetAt(0).setColumnWidth(5, 20 * 256);
		e.getSheetAt(0).setColumnWidth(6, 20 * 256);
		e.getSheetAt(0).setColumnWidth(7, 20 * 256);
		e.getSheetAt(0).setColumnWidth(8, 20 * 256);
		e.getSheetAt(0).setColumnWidth(9, 20 * 256);
		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		List<TbkYearBalance> list = tbkYearBalanceMapper.selectTbkYearBalanceReport(map);
		
//		int num = 0;
		for (int i=0;i<list.size();i++) {

			e.writeStr(0, row+2+i,"A", center,list.get(i).getSponInst());
			e.writeStr(0, row+2+i,"B", center,list.get(i).getFromSubjCode());
			e.writeStr(0, row+2+i,"C", center,list.get(i).getFromSubjName());
			e.writeStr(0, row+2+i,"D", center,list.get(i).getFromDebitCredit());
			e.writeStr(0, row+2+i,"E", center,list.get(i).getFromCcy());
			e.writeDouble(0, row+2+i,"F", e.getDefaultDouble2CellStyle(),list.get(i).getAmount());
			e.writeStr(0, row+2+i,"G", center,list.get(i).getEndSubjCode());
			e.writeStr(0, row+2+i,"H", center,list.get(i).getEndSubjName());
			e.writeStr(0, row+2+i,"I", center,list.get(i).getEndDebitCredit());
			e.writeStr(0, row+2+i,"J", center,list.get(i).getBeaNo());
//			if (i>0){
//			if((!(list.get(i).getYearSer().equals(list.get(i-1).getYearSer()))) ){
//				Double db = 0.0;
//				for(int x=num;x<i;x++){
//					db =PlaningTools.add(db, list.get(x).getAmount());
//				}
////				e.cellRange(0, 0, row+2+num, row+2+i-1, e.getSheetAt(0),e.getWb());
////				e.cellRange(10, 10, row+2+num, row+2+i-1, e.getSheetAt(0),e.getWb());
//				e.writeStr(0, row+2+i-1,"J", center,list.get(i-1).getSponInst());
//				e.writeDouble(0, row+2+i-1,"K", center,db);
//				num = i;
//			}
//			if( list.size()==i+1){
//				Double db = 0.0;
//				for(int x=num;x<i;x++){
//					db = db + list.get(x).getAmount();
//				}
//				e.writeStr(0, row+2+i,"J", center,list.get(i-1).getSponInst());
//				e.writeDouble(0, row+2+i,"K", center,db);
//				num = i;
//			}
			
//			}
		}
		
	}
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.YearEnd.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
	
	
	/**
	 * 根据属性 来分类List中的对象
	 * -汪泽峰
	 * @param list
	 * @param strings
	 * @return
	 */
	 public static <T> Map<String, List<T>> listToMap(List<T> list, String... strings) {
	        Map<String, List<T>> returnMap = new HashMap<String, List<T>>();
	        try {
	            for (T t : list) {
	                StringBuffer stringBuffer = new StringBuffer();
	                for (String s : strings) {
	                    Field name1 = t.getClass().getDeclaredField(s);//通过反射获得私有属性,这里捕获获取不到属性异常
	                    name1.setAccessible(true);//获得访问和修改私有属性的权限
	                    String key = name1.get(t).toString();//获得key值
	                    stringBuffer.append(key+"_");
	                }
	                String KeyName = stringBuffer.toString();

	                List<T> tempList = returnMap.get(KeyName);
	                if (tempList == null) {
	                    tempList = new ArrayList<T>();
	                    tempList.add(t);
	                    returnMap.put(KeyName, tempList);
	                } else {
	                    tempList.add(t);
	                }
	            }
	        } catch (NoSuchFieldException e) {
	            throw new RException(e);
	        } catch (IllegalAccessException e) {
	            throw new RException(e);
	        }
	        return returnMap;
	    }

}