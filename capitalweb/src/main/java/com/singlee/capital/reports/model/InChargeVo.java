package com.singlee.capital.reports.model;

import java.io.Serializable;

/**
 * 经营单位中收明细表
 * @author kf0742
 *
 */
public class InChargeVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String instName;  //机构名称
	
	private double sameMonSumgwf;   //当月顾问费
	private double totalSumgwf;		//累计顾问费
	private double sameMonSumtgf;	//当月托管费
	private double totalSumtgf;		//累计托管费
	private double sameMonSumfwf;	//当月服务费
	private double totalSumfwf;		//累计服务费
	private double sameMonSumhj;	//当月合计
	private double totalSumhj;		//累计合计
	
	
	
	
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public double getSameMonSumgwf() {
		return sameMonSumgwf;
	}
	public void setSameMonSumgwf(double sameMonSumgwf) {
		this.sameMonSumgwf = sameMonSumgwf;
	}
	public double getTotalSumgwf() {
		return totalSumgwf;
	}
	public void setTotalSumgwf(double totalSumgwf) {
		this.totalSumgwf = totalSumgwf;
	}
	public double getSameMonSumtgf() {
		return sameMonSumtgf;
	}
	public void setSameMonSumtgf(double sameMonSumtgf) {
		this.sameMonSumtgf = sameMonSumtgf;
	}
	public double getTotalSumtgf() {
		return totalSumtgf;
	}
	public void setTotalSumtgf(double totalSumtgf) {
		this.totalSumtgf = totalSumtgf;
	}
	public double getSameMonSumfwf() {
		return sameMonSumfwf;
	}
	public void setSameMonSumfwf(double sameMonSumfwf) {
		this.sameMonSumfwf = sameMonSumfwf;
	}
	public double getTotalSumfwf() {
		return totalSumfwf;
	}
	public void setTotalSumfwf(double totalSumfwf) {
		this.totalSumfwf = totalSumfwf;
	}
	public double getSameMonSumhj() {
		return sameMonSumhj;
	}
	public void setSameMonSumhj(double sameMonSumhj) {
		this.sameMonSumhj = sameMonSumhj;
	}
	public double getTotalSumhj() {
		return totalSumhj;
	}
	public void setTotalSumhj(double totalSumhj) {
		this.totalSumhj = totalSumhj;
	}
	
	
}
