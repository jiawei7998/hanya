package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;

@Service("reportG31ServiceImpl")
public class ReportG31ServiceImpl implements ReportService{
	
	@Override
    public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.G31.getFileName()));
		ret.setObj(ExcelShower.transExcelToHtml(eu.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}

	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.G31.getFileName()));
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
}
