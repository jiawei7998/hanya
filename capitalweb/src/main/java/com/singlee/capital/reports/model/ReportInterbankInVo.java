package com.singlee.capital.reports.model;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * 同业投资情况表实体类
 * @author lxy
 *
 */
@Entity
public class ReportInterbankInVo implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	/** 业务品种 */
	private String prdName; 
	/** 人民币总和   */
	private double sumAmtCcy;
	/** 美元总和    */
	private double sumAmtUsd;
	/*** 合计  */
	private double sumAmt;
	/** 较上月差额  */
	private double differAmt;
	
	
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public double getSumAmtCcy() {
		return sumAmtCcy;
	}
	public void setSumAmtCcy(double sumAmtCcy) {
		this.sumAmtCcy = sumAmtCcy;
	}
	public double getSumAmtUsd() {
		return sumAmtUsd;
	}
	public void setSumAmtUsd(double sumAmtUsd) {
		this.sumAmtUsd = sumAmtUsd;
	}
	public double getSumAmt() {
		return sumAmt;
	}
	public void setSumAmt(double sumAmt) {
		this.sumAmt = sumAmt;
	}
	public double getDifferAmt() {
		return differAmt;
	}
	public void setDifferAmt(double differAmt) {
		this.differAmt = differAmt;
	}
	
	
}
