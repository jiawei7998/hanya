package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.reports.mapper.ReportCommMapper;
import com.singlee.capital.reports.model.RepAssetLiabilityRatioVo;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;

/****
 * 信用资产负债率指标
 * @author lee
 *
 */
@Service("reportAssetLiabilityRatioServiceImpl")
public class ReportAssetLiabilityRatioServiceImpl implements ReportService{
	
	@Autowired
	private ReportCommMapper reportCommMapper;
	
	@Override
    public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.AssetLiabilityRatio.getFileName()));
		
		transportExcel(eu, map);
		ret.setObj(ExcelShower.transExcelToHtml(eu.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}

	@SuppressWarnings("static-access")
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		// 获取当月第一天和最后一天
		Calendar cale = null ;
		CellStyle center = e.getDefaultCenterStrCellStyle();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		
		
		int row = 1;
		Date reportDate = format.parse(String.valueOf(map.get("reportDate")));
		map.put("reDate", "%"+sdf.format(reportDate)+"%");
		//年初
		map.put("starYearDate", year.format(reportDate)+"-01-01");
		// 获取当月的最后一天
		cale = Calendar.getInstance();
		cale.setTime(reportDate);
		cale.add(Calendar.MONTH, 1);
		cale.set(Calendar.DAY_OF_MONTH, 0);
		map.put("lastDay", format.format(cale.getTime()));
		
		// 获取当月的天数
		cale = Calendar.getInstance();
		cale.setTime(reportDate);
		cale.set(Calendar.DATE, 1);
		cale.roll(Calendar.DATE, -1);
		int maxDate = cale.get(Calendar.DATE);
		map.put("maxDate", maxDate);
		
		//获取上一年12月末
		cale = Calendar.getInstance();
		cale.setTime(reportDate);
		cale.add(Calendar.YEAR, -1);
		
		String upDate = year.format(cale.getTime());
		map.put("upDate", upDate+"-12-30");
		
		
		
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		e.getSheetAt(0).setColumnWidth(1, 20 * 256);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		e.getSheetAt(0).setColumnWidth(3, 20 * 256);
		e.getSheetAt(0).setColumnWidth(4, 20 * 256);
		e.getSheetAt(0).setColumnWidth(5, 20 * 256);		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		List<RepAssetLiabilityRatioVo> lists = reportCommMapper.getRepAssetLiabilityRatioAccount(map);
		//合并单元格
		if(lists.size() > 1){
			e.cellRange(2, lists.size() + 1, 0, 0, e.getSheetAt(0),e.getWb());
		}
		//字体加粗
		e.writeStr(0, 1,"E", center,upDate+"年12月末");
		e.writeStr(0, 2,"A", center,"分子");

		for (int i=0;i<lists.size();i++) {
			e.writeStr(0, row+1+i,"B", center,lists.get(i).getPrdName());
			e.writeDouble(0, row+1+i,"C", center,lists.get(i).getAvgAmt());
			e.writeDouble(0, row+1+i,"D", center,lists.get(i).getSumMonthEndAmt());
			e.writeDouble(0, row+1+i,"E", center,lists.get(i).getSumYearEndAmt());
			e.writeDouble(0, row+1+i,"F", center,lists.get(i).getDefferAmt());
		}
		
	}
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.AssetLiabilityRatio.getFileName()));
			//to_do
			transportExcel(eu, map);
			
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					//e.printStackTrace();
					throw new RException(e);
				}
			}
		}
	}
}
