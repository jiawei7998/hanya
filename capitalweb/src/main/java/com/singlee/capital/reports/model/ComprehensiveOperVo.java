package com.singlee.capital.reports.model;

import java.io.Serializable;

/**
 * 综合经营计划完成情况
 * @author kf0742
 *
 */
public class ComprehensiveOperVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String instName;  //机构名称
	
	private double sumgsyty; //高收益同业资产
	
	private double sumtyhq; //同业活期存款
	
	private double sumzjyw; //金融市场业务中间业务收入
	
	private double sumpj; //票据业务考核利润

	
	
	
	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public double getSumgsyty() {
		return sumgsyty;
	}

	public void setSumgsyty(double sumgsyty) {
		this.sumgsyty = sumgsyty;
	}

	public double getSumtyhq() {
		return sumtyhq;
	}

	public void setSumtyhq(double sumtyhq) {
		this.sumtyhq = sumtyhq;
	}

	public double getSumzjyw() {
		return sumzjyw;
	}

	public void setSumzjyw(double sumzjyw) {
		this.sumzjyw = sumzjyw;
	}

	public double getSumpj() {
		return sumpj;
	}

	public void setSumpj(double sumpj) {
		this.sumpj = sumpj;
	}
	
	
}
