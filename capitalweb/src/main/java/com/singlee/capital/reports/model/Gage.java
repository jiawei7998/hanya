package com.singlee.capital.reports.model;
/**
 * 交易台账
 * @author fll
 *
 */
public class Gage {
	//机构号 
	private String  br;   
	//交易号 
	private String  dealno;
	//交易对手                 
	private String  cno;       
	//客户英文简称          
	private String  cmne; 
	//成本中心
	private String  cost;
	//交易日期
	private String  retcode;
	//开始日期
	private String  startdate;
	//到期日
	private String  matdate;
	//投资组合
	private String  port;
	//交易员
	private String  trad;
	//固息计息基数代码			
	private String  basis;
	//固息付款/收款标志
	private String  payrecind;
	//固息付息周期代码
	private String  intpaycycle;
	//固息名义货币金额
	private String  notccyamt;
	//固息利率代码
	private String  ratecode;
	//固息利率
	private String  intrate_8;
	//固息净现值
	private String  npvamt;
	//浮息计息基数代码		
	private String  floatbasis;
	//浮息名义货币金额
	private String  floatnotccyamt;
	//浮息利率代码
	private String  floatratecode;
	//浮息利率
	private String  floatintrate_8;
	//浮息净现值
	private String  floatnpvamt;
	//收付息总金额
	private String  mtm;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getMatdate() {
		return matdate;
	}
	public void setMatdate(String matdate) {
		this.matdate = matdate;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getPayrecind() {
		return payrecind;
	}
	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}
	public String getIntpaycycle() {
		return intpaycycle;
	}
	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}
	public String getNotccyamt() {
		return notccyamt;
	}
	public void setNotccyamt(String notccyamt) {
		this.notccyamt = notccyamt;
	}
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	public String getIntrate_8() {
		return intrate_8;
	}
	public void setIntrate_8(String intrate_8) {
		this.intrate_8 = intrate_8;
	}
	public String getNpvamt() {
		return npvamt;
	}
	public void setNpvamt(String npvamt) {
		this.npvamt = npvamt;
	}
	public String getFloatbasis() {
		return floatbasis;
	}
	public void setFloatbasis(String floatbasis) {
		this.floatbasis = floatbasis;
	}
	public String getFloatnotccyamt() {
		return floatnotccyamt;
	}
	public void setFloatnotccyamt(String floatnotccyamt) {
		this.floatnotccyamt = floatnotccyamt;
	}
	public String getFloatratecode() {
		return floatratecode;
	}
	public void setFloatratecode(String floatratecode) {
		this.floatratecode = floatratecode;
	}
	public String getFloatintrate_8() {
		return floatintrate_8;
	}
	public void setFloatintrate_8(String floatintrate_8) {
		this.floatintrate_8 = floatintrate_8;
	}
	public String getFloatnpvamt() {
		return floatnpvamt;
	}
	public void setFloatnpvamt(String floatnpvamt) {
		this.floatnpvamt = floatnpvamt;
	}
	public String getMtm() {
		return mtm;
	}
	public void setMtm(String mtm) {
		this.mtm = mtm;
	}
	@Override
	public String toString() {
		return "Gage [br=" + br + ", dealno=" + dealno + ", cno=" + cno
				+ ", cmne=" + cmne + ", cost=" + cost + ", retcode=" + retcode
				+ ", startdate=" + startdate + ", matdate=" + matdate
				+ ", port=" + port + ", trad=" + trad + ", basis=" + basis
				+ ", payrecind=" + payrecind + ", intpaycycle=" + intpaycycle
				+ ", notccyamt=" + notccyamt + ", ratecode=" + ratecode
				+ ", intrate_8=" + intrate_8 + ", npvamt=" + npvamt
				+ ", floatbasis=" + floatbasis + ", floatnotccyamt="
				+ floatnotccyamt + ", floatratecode=" + floatratecode
				+ ", floatintrate_8=" + floatintrate_8 + ", floatnpvamt="
				+ floatnpvamt + ", mtm=" + mtm + "]";
	}

}
