package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.base.model.TbkScenceSubject;
import com.singlee.cap.base.service.TbkScenceSubjectService;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;
@Service("reportTbkEntryServiceImpl")
public class ReportTbkEntryServiceImpl implements ReportService {
	@Autowired
	private TbkEntryService tbkEntryService;
	@Autowired
	private TbkScenceSubjectService tbkScenceSubjectService;
	
	
	@Override
	public RetMsg<Serializable> transRepToHtml(Map<String, Object> map)
			throws Exception {
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil e = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.TbkEntryRep.getFileName()));
		
		transportExcel(e, map);
		
		ret.setObj(ExcelShower.transExcelToHtml(e.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}
	
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		CellStyle center = e.getDefaultCenterStrCellStyle();
		int row = 1;
		if(map.get("time1")!=null && map.get("time2")!= null){
		e.writeStr(0, row, "C",center,map.get("time1")+"到"+map.get("time2")+"的税收");
		}else{
			e.writeStr(0, row, "C",center,"所有的税收");
		}
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		e.getSheetAt(0).setColumnWidth(1, 20 * 256);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		
		//获取已配置的产品科目
		map.put("pageSize", 9999999); map.put("pageNumber", 1); 
		List<TbkScenceSubject> list1 = tbkScenceSubjectService.showAllTbkScenceSubject(map);
		Map<String, Object> map2 = new HashMap<String, Object>();
		//map2.put("postDate1", "2017-08-30");
		//map2.put("postDate2", "2017-09-05");
		//获取所有的tbkEntry
		List<TbkEntry> listt = tbkEntryService.selectByTimeService(map2);
//		List<TbkEntry> list = new ArrayList<TbkEntry>();
		//过滤出已配置产品科目的tbkEntry放入list
//		for (TbkEntry tbkEntry : listt) {
//			for (TbkScenceSubject tss : list1) {
//				if(tbkEntry.getPrdNo().equals(tss.getPrdNo())&&tbkEntry.getSubjCode().equals(tss.getSubjCode()))
//				{
//					list.add(tbkEntry);
//				}
//			}
//		}
	
		for (int i = 0;i<list1.size();i++) {
			if(i>0){
				if(list1.get(i).getPrdNo().equals(list1.get(i-1).getPrdNo())&&(list1.get(i).getSubjCode().equals(list1.get(i-1).getSubjCode()))){
					list1.remove(i);
				}
			}
		}
		for (int i = 0;i<list1.size();i++) {
			double total = 0;
			String Name = null;
			String Cny = null;
			
			for (TbkEntry tbkEntry : listt) {
				
				if(list1.get(i).getPrdNo().equals(tbkEntry.getPrdNo())&&list1.get(i).getSubjCode().equals(tbkEntry.getSubjCode()))
				{
					if("C".equals(tbkEntry.getDebitCreditFlag())){
						total = total - tbkEntry.getValue();
					}
					else{
						total += tbkEntry.getValue();
					}
					
					Name = tbkEntry.getPrdName();
					Cny = tbkEntry.getCcy();
				}
			}
			if(Name != null && Cny != null){
			e.writeStr(0, row+2+i,"A", center,Name);
			e.writeStr(0, row+2+i,"B", center,Cny);
			e.writeStr(0, row+2+i,"C", center,MathUtil.round(total, 2)+"");
			}
		}
	}
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.TbkEntryRep.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
	
	/**
	 * 根据属性 来分类List中的对象
	 * -汪泽峰
	 * @param list
	 * @param strings
	 * @return
	 */
//	 public static <T> Map<String, List<T>> listToMap(List<T> list, String... strings) {
//	        Map<String, List<T>> returnMap = new HashMap<String, List<T>>();
//	        try {
//	            for (T t : list) {
//	                StringBuffer stringBuffer = new StringBuffer();
//	                for (String s : strings) {
//	                    Field name1 = t.getClass().getDeclaredField(s);//通过反射获得私有属性,这里捕获获取不到属性异常
//	                    name1.setAccessible(true);//获得访问和修改私有属性的权限
//	                    String key = name1.get(t).toString();//获得key值
//	                    stringBuffer.append(key+"_");
//	                }
//	                String KeyName = stringBuffer.toString();
//
//	                List<T> tempList = returnMap.get(KeyName);
//	                if (tempList == null) {
//	                    tempList = new ArrayList<T>();
//	                    tempList.add(t);
//	                    returnMap.put(KeyName, tempList);
//	                } else {
//	                    tempList.add(t);
//	                }
//	            }
//	        } catch (NoSuchFieldException e) {
//	            throw new RException(e);
//	        } catch (IllegalAccessException e) {
//	            throw new RException(e);
//	        }
//	        return returnMap;
//	    }

}
