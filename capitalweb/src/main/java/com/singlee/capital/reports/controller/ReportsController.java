package com.singlee.capital.reports.controller;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.cap.bookkeeping.service.TbkEntryInterfaceService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;
import com.singlee.capital.system.controller.CommonController;

@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/ReportsController")
public class ReportsController extends CommonController{

	@Resource(name = "reportDailyAvgBalanceServiceImpl")
	private ReportService reportDailyAvgBalanceServiceImpl;
	
	@Resource(name = "reportAssetLiabilityRatioServiceImpl")
	private ReportService reportAssetLiabilityRatioServiceImpl;
	
	@Resource(name = "reportBalanceSummaryServiceImpl")
	private ReportService reportBalanceSummaryServiceImpl;
	
	@Resource(name = "reportBusinessStatusServiceImpl")
	private ReportService reportBusinessStatusServiceImpl;
	
	@Resource(name = "reportCnyBalanceSheetServiceImpl")
	private ReportService reportCnyBalanceSheetServiceImpl;
	
	@Resource(name = "reportComprehensiveOperServiceImpl")
	private ReportService reportComprehensiveOperServiceImpl;
	
	@Resource(name = "reportG31ServiceImpl")
	private ReportService reportG31ServiceImpl;
	
	@Resource(name = "reportTbkEntryServiceImpl")
	private ReportService reportTbkEntryServiceImpl;
	
	@Resource(name = "reportTradingAccountServiceImpl")
	private ReportService reportTradingAccountServiceImpl;
	
	@Resource(name = "reportYearEndServiceImpl")
	private ReportService reportYearEndServiceImpl;
	
	@Resource(name = "reportInChargeServiceImpl")
	private ReportService reportInChargeServiceImpl;
	
	@Resource(name = "reportInChargeSumServiceImpl")
	private ReportService reportInChargeSumServiceImpl;
	
	@Resource(name = "reportIncomeSummaryServiceImpl")
	private ReportService reportIncomeSummaryServiceImpl;
	
	@Resource(name = "reportInterbankAssetServiceImpl")
	private ReportService reportInterbankAssetServiceImpl;
	
	@Resource(name = "reportInterbankInvServiceImpl")
	private ReportService reportInterbankInvServiceImpl;
	
	@Autowired
	private TbkEntryInterfaceService tbkEntryInterfaceService;
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryDailyAvgBalance")
	public RetMsg<Serializable> queryDailyAvgBalance(@RequestBody Map<String, Object> map){
		try {
			return reportDailyAvgBalanceServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryAssetLiabilityRatio")
	public RetMsg<Serializable> queryAssetLiabilityRatio(@RequestBody Map<String, Object> map) {
		try {
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			return reportAssetLiabilityRatioServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryBalanceSummary")
	public RetMsg<Serializable> queryBalanceSummary(@RequestBody Map<String, Object> map) {
		try {
			return reportBalanceSummaryServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryBusinessStatus")
	public RetMsg<Serializable> queryBusinessStatus(@RequestBody Map<String, Object> map) {
		try {
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			String ccy = ParameterUtil.getString(map, "ccy", "");
			String instId = ParameterUtil.getString(map, "instId", "");
			map.put("ccy", ccy);
			map.put("instId", instId);
			return reportBusinessStatusServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCnyBalanceSheet")
	public RetMsg<Serializable> queryCnyBalanceSheet(@RequestBody Map<String, Object> map) {
		try {
			return reportCnyBalanceSheetServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryComprehensiveOper")
	public RetMsg<Serializable> queryComprehensiveOper(@RequestBody Map<String, Object> map) {
		try {
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			return reportComprehensiveOperServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryG31")
	public RetMsg<Serializable> queryG31(@RequestBody Map<String, Object> map) {
		try {
			return reportG31ServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryTbkEntry")
	public RetMsg<Serializable> queryTbkEntry(@RequestBody Map<String, Object> map) {
		try {
			return reportTbkEntryServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/queryTradingAccount")
	public RetMsg<Serializable> queryTradingAccount(@RequestBody Map<String, Object> map) {
		try {
			return reportTradingAccountServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/queryYearEnd")
	public RetMsg<Serializable> queryYearEnd(@RequestBody Map<String, Object> map) {
		try {
			return reportYearEndServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryInCharge")
	public RetMsg<Serializable> queryInCharge(@RequestBody Map<String, Object> map) {
		try {
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			return reportInChargeServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryInChargeSum")
	public RetMsg<Serializable> queryInChargeSum(@RequestBody Map<String, Object> map) {
		try {
			return reportInChargeSumServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryIncomeSummary")
	public RetMsg<Serializable> queryIncomeSummary(@RequestBody Map<String, Object> map) {
		try {
			return reportIncomeSummaryServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryInterbankAsset")
	public RetMsg<Serializable> queryInterbankAsset(@RequestBody Map<String, Object> map) {
		try {
			return reportInterbankAssetServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}

	/**
	 * 查询
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/queryInterbankInv")
	public RetMsg<Serializable> queryInterbankInv(@RequestBody Map<String, Object> map) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
			}
			map.put("endDate", reportDate);
			c.setTime(sdf.parse(reportDate));
			c.add(Calendar.MONTH, -2);
			Date stratDate = c.getTime();
			map.put("stratDate", sdf.format(stratDate));
			
			return reportInterbankInvServiceImpl.transRepToHtml(map);
		} catch (Exception e) {
			return RetMsgHelper.simple("999999", "查询报表出错:"+e.getMessage());
		}
	}
	@ResponseBody
	@RequestMapping(value = "/expLcModel")
	public void expLcModel(HttpServletRequest request, HttpServletResponse response){
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			try {
				POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(ReportUtils.getReportModelFilePath("信用证批量信息模板.xls")));
				HSSFWorkbook  workbook = new HSSFWorkbook(fs);
	            workbook.write(os);
				byte[] content = os.toByteArray();
				downLoadFile(request,response, "信用证批量信息模板.xls",content);
			} catch (Exception e) {
				throw e;
			} finally {
				if(os != null){
					try {
						os.close();
					} catch (IOException e) {
						//e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/expBillModel")
	public void expBillModel(HttpServletRequest request, HttpServletResponse response){
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			try {
				POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(ReportUtils.getReportModelFilePath("票据信息批量模板.xls")));
				HSSFWorkbook  workbook = new HSSFWorkbook(fs);
	            //创建一个sheet
	            HSSFSheet sheet = workbook.getSheetAt(0);
	            //准备下拉列表数据
	            String[] strs = new String[]{"1","2"};
	            //设置第一列的1-300行为下拉列表
	            CellRangeAddressList regions = new CellRangeAddressList(1, 300, 2, 2);
	            //创建下拉列表数据
	            DVConstraint constraint = DVConstraint.createExplicitListConstraint(strs);
	            //绑定
	            HSSFDataValidation dataValidation = new HSSFDataValidation(regions, constraint);
	            sheet.addValidationData(dataValidation);
	            
	            CellRangeAddressList regions2 = new CellRangeAddressList(1, 300, 3, 3);
	            DVConstraint constraint2 = DVConstraint.createExplicitListConstraint(strs);
	            HSSFDataValidation dataValidation2 = new HSSFDataValidation(regions2, constraint2);
	            sheet.addValidationData(dataValidation2);
	            
	            
	            String[] strs12 = new String[]{"01","02","03","99"};
	            CellRangeAddressList regions12 = new CellRangeAddressList(1, 300, 12, 12);
	            DVConstraint constraint12 = DVConstraint.createExplicitListConstraint(strs12);
	            HSSFDataValidation dataValidation12 = new HSSFDataValidation(regions12, constraint12);
	            sheet.addValidationData(dataValidation12);
	            
	            String[] strs13 = new String[]{"01","02","03","04","05","06","07","08","09","99"};
	            CellRangeAddressList regions13 = new CellRangeAddressList(1, 300, 13, 13);
	            DVConstraint constraint13 = DVConstraint.createExplicitListConstraint(strs13);
	            HSSFDataValidation dataValidation13 = new HSSFDataValidation(regions13, constraint13);
	            sheet.addValidationData(dataValidation13);
	            
	            String[] strs14 = new String[]{"A0000","B0000","C0000","D0000","E0000","F0000","G0000","H0000","I0000","J0000","K0000","L0000","M0000","N0000","O0000","P0000","Q0000","R0000","S0000","T0000"};

	            CellRangeAddressList regions14 = new CellRangeAddressList(1, 300, 14, 14);
	            DVConstraint constraint14 = DVConstraint.createExplicitListConstraint(strs14);
	            HSSFDataValidation dataValidation14 = new HSSFDataValidation(regions14, constraint14);
	            sheet.addValidationData(dataValidation14);
	            
	            workbook.write(os);
				byte[] content = os.toByteArray();
				downLoadFile(request,response, "票据信息批量模板.xls",content);
			} catch (Exception e) {
				throw e;
			} finally {
				if(os != null){
					try {
						os.close();
					} catch (IOException e) {
						//e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expDailyAvgBalance")
	public void expDailyAvgBalance(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportDailyAvgBalanceServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);
			} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expAssetLiabilityRatio")
	public void expAssetLiabilityRatio(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportAssetLiabilityRatioServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
		
	}
		

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expBalanceSummary")
	public void expBalanceSummary(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportBalanceSummaryServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expBusinessStatus")
	public void expBusinessStatus(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			String ccy = ParameterUtil.getString(map, "ccy", "");
			String instId = ParameterUtil.getString(map, "instId", "");
			map.put("ccy", ccy);
			map.put("instId", instId);
			map.put("instName", ParameterUtil.getString(map, "instName", ""));
			byte[] retMsg = reportBusinessStatusServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.BusinessStatus.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expCnyBalanceSheet")
	public void expCnyBalanceSheet(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportCnyBalanceSheetServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expComprehensiveOper")
	public void expComprehensiveOper(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportComprehensiveOperServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.ComprehensiveOper.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expG31")
	public void expG31(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportG31ServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expTbkEntry")
	public void expTbkEntry(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			tbkEntryInterfaceService.GtpTbkEntryOutputDataWrite(map);
			 	String reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			
			byte[] retMsg = reportTbkEntryServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.TbkEntryRep.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/exptradingAccount")
	public void exptradingAccount(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			 	String reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			
			byte[] retMsg = reportTradingAccountServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.TradingAccount.getDesc() + "_" + reportDate + ".xls", retMsg);
		
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expYearEnd")
	public void expYearEnd(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportYearEndServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.YearEnd.getDesc() + "_" + reportDate + ".xls", retMsg);
		
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expInCharge")
	public void expInCharge(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportInChargeServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.InCharge.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expInChargeSum")
	public void expInChargeSum(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportInChargeSumServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expIncomeSummary")
	public void expIncomeSummary(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportIncomeSummaryServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expInterbankAsset")
	public void expInterbankAsset(HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
				map.put("reportDate", reportDate);
			}
			byte[] retMsg = reportInterbankAssetServiceImpl.exportRep(null);
			downLoadFile(request,response, ReportType.DailyAvgBalance.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	/**
	 * 导出
	 * @param 
	 * @return
	 * @
	 */
	@ResponseBody
	@RequestMapping(value = "/expInterbankInv")
	public void expInterbankInv(HttpServletRequest request, HttpServletResponse response){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			Map<String, Object> map = requestParamToMap(request);
			String reportDate = map.get("reportDate") == null ? "" : String.valueOf(map.get("reportDate"));
			if(reportDate == null || "".equals(reportDate))
			{
				reportDate = DateUtil.getCurrentDateAsString();
			}
			map.put("endDate", reportDate);
			c.setTime(sdf.parse(reportDate));
			c.add(Calendar.MONTH, -2);
			Date stratDate = c.getTime();
			map.put("stratDate", sdf.format(stratDate));
			
			byte[] retMsg = reportInterbankInvServiceImpl.exportRep(map);
			downLoadFile(request,response, ReportType.InterbankInv.getDesc() + "_" + reportDate + ".xls", retMsg);

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	private void downLoadFile(HttpServletRequest request,HttpServletResponse response,String fileName,byte[] retMsg)throws Exception
	{
		String ua = request.getHeader("User-Agent");
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + new String(fileName.getBytes(), "ISO8859-1"));
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes("GBK"), "ISO8859-1"));
		}
		response.setHeader("Connection", "close");
		if(null != retMsg) {
            response.getOutputStream().write(retMsg);
        }
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
	
	/**
	 * 读取request中的参数,转换为map对象
	 * 
	 * @param request
	 * @return
	 */
	private static HashMap<String,Object> requestParamToMap(HttpServletRequest request) 
	{
		HashMap<String,Object> parameters = new HashMap<String,Object>();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) 
		{
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (StringUtils.isNotEmpty(paramValue)) 
			{
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
}
