package com.singlee.capital.reports.model;

import java.io.Serializable;


/**
 * 信用资产负债率指标
 * @author kf0742
 *
 */
public class RepAssetLiabilityRatioVo implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String prdName;  //业务品种
	
	private double avgAmt;    //日均
	private double sumMonthEndAmt;   //月末
	private double sumYearEndAmt;    //去年年末
	private double sumYearStarAmt;   //年初
	private double defferAmt;        //较年初
	
	
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public double getAvgAmt() {
		return avgAmt;
	}
	public void setAvgAmt(double avgAmt) {
		this.avgAmt = avgAmt;
	}
	public double getSumMonthEndAmt() {
		return sumMonthEndAmt;
	}
	public void setSumMonthEndAmt(double sumMonthEndAmt) {
		this.sumMonthEndAmt = sumMonthEndAmt;
	}
	public double getSumYearEndAmt() {
		return sumYearEndAmt;
	}
	public void setSumYearEndAmt(double sumYearEndAmt) {
		this.sumYearEndAmt = sumYearEndAmt;
	}
	public double getSumYearStarAmt() {
		return sumYearStarAmt;
	}
	public void setSumYearStarAmt(double sumYearStarAmt) {
		this.sumYearStarAmt = sumYearStarAmt;
	}
	public double getDefferAmt() {
		return defferAmt;
	}
	public void setDefferAmt(double defferAmt) {
		this.defferAmt = defferAmt;
	}
	
	
}
