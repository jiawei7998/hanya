package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.reports.mapper.ReportCommMapper;
import com.singlee.capital.reports.model.ReportInterbankInVo;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;

/****
 * 
 * 同业投资情况表
 * @author lee
 *
 */
@Service("reportInterbankInvServiceImpl")
public class ReportInterbankInvServiceImpl implements ReportService{
	
	@Autowired
	private ReportCommMapper reportCommMapper;
	
	@Override
    public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.InterbankInv.getFileName()));
		//重构Excel
		transportExcel(eu, map);
		
		ret.setObj(ExcelShower.transExcelToHtml(eu.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}
	
	
	@SuppressWarnings("static-access")
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		CellStyle center = e.getDefaultCenterStrCellStyle();
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		int row = 1;
		
		java.util.Calendar c = java.util.Calendar.getInstance();
		Date beginDate = sdf.parse(String.valueOf(map.get("endDate")));
		Date endDate = sdf.parse(String.valueOf(map.get("stratDate")));
		Date date =  beginDate;
		
		int l = 0;
		int p  = 1;
		//拼接人民币、美元数据集单元格
		while(date.getTime() >= endDate.getTime()){
			String str = sdf.format(date);
			map.put("deanDate", "%"+str+"%");

			//设置列宽
			e.getSheetAt(0).setColumnWidth(l+1, 20 * 256);
			e.getSheetAt(0).setColumnWidth(l+2, 20 * 256);
			
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			List<ReportInterbankInVo> lists = reportCommMapper.getReportInterbankAccount(map);
			//合并单元格
			e.cellRange(1, 1, l+1, l+2, e.getSheetAt(0),e.getWb());
			e.cellRange(2, 2, l+1, l+2, e.getSheetAt(0),e.getWb());
			e.writeStr(0, row+0,l+1, center,str);
			e.writeStr(0, row+1,l+1, center,"金融市场部");
			
			e.writeStr(0, row+2,l+1, center,"人民币");
			e.writeStr(0, row+2,l+2, center,"美元");
			
			for (int i=0;i<lists.size();i++) {
				if(l==0){
					e.writeStr(0, row+3+i,l+0, center,lists.get(i).getPrdName());
				}
				e.writeDouble(0, row+3+i,l+1, center,lists.get(i).getSumAmtCcy());
				e.writeDouble(0, row+3+i,l+2, center,lists.get(i).getSumAmtUsd());
			}
			
			l = l + 2;
			p = p + 1;
			c.setTime(date);
			c.add(java.util.Calendar.MONTH, -1);
			date = c.getTime();
		}
		
		
		Date hjdate =  beginDate;
		//拼接本外合计数据集单元格
		e.cellRange(2, 2, l+1, l+p-1, e.getSheetAt(0),e.getWb());
		e.writeStr(0, row+1,l+1, center,"汇总");
		while(hjdate.getTime() >= endDate.getTime()){
			String str = sdf.format(hjdate);
			map.put("deanDate", "%"+str+"%");
			e.writeStr(0, row+0,l+1, center,str);
			e.writeStr(0, row+2,l+1, center,"本外合计");
			
			List<ReportInterbankInVo> hjlists = reportCommMapper.getReportInterbankhjAccount(map);
			for (int i=0;i<hjlists.size();i++) {
				e.writeDouble(0, row+3+i,l+1, center,hjlists.get(i).getSumAmt());
			}
			l = l + 1;
			
			c.setTime(hjdate);
			c.add(java.util.Calendar.MONTH, -1);
			hjdate = c.getTime();
		}
		
		c.setTime(beginDate);
		c.add(java.util.Calendar.MONTH, -1);
		Date update  = c.getTime();
		map.put("beginDate", "%"+sdf.format(beginDate)+"%");
		map.put("update", "%"+sdf.format(update)+"%");
		e.writeStr(0, row+0,l+1, center," ");
		e.writeStr(0, row+1,l+1, center,"较上月");
		e.writeStr(0, row+2,l+1, center,"本外合计");
		//查询较上月差额
		List<ReportInterbankInVo> differLists = reportCommMapper.getReportInterbankdifferAccount(map);
		for (int i=0;i<differLists.size();i++) {
			e.writeDouble(0, row+3+i,l+1, center,differLists.get(i).getDifferAmt());
		}
		//合并标题栏
		e.cellRange(0, 0, 0, 3+p*2+p, e.getSheetAt(0),e.getWb());
		
	}
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.InterbankInv.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
}
