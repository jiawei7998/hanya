package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.reports.mapper.ReportCommMapper;
import com.singlee.capital.reports.model.ComprehensiveOperVo;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;

/****
 * 
 * 综合经营指标-指标数表
 * @author lee
 *
 */
@Service("reportComprehensiveOperServiceImpl")
public class ReportComprehensiveOperServiceImpl implements ReportService{
	
	
	@Autowired
	private ReportCommMapper reportCommMapper;
	
	
	@Override
    public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.ComprehensiveOper.getFileName()));
		
		transportExcel(eu, map);
		
		ret.setObj(ExcelShower.transExcelToHtml(eu.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}

	
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		Date reportDate = sdf.parse(String.valueOf(map.get("reportDate")));
		String str = sdf.format(reportDate);
		map.put("starDate", str+"-01-01");
		
		CellStyle center = e.getDefaultCenterStrCellStyle();
		int row = 1;
		
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		e.getSheetAt(0).setColumnWidth(1, 20 * 256);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		e.getSheetAt(0).setColumnWidth(3, 20 * 256);
		e.getSheetAt(0).setColumnWidth(4, 20 * 256);
		e.getSheetAt(0).setColumnWidth(5, 20 * 256);
		e.getSheetAt(0).setColumnWidth(6, 20 * 256);
		e.getSheetAt(0).setColumnWidth(7, 20 * 256);
		e.getSheetAt(0).setColumnWidth(8, 20 * 256);
		e.getSheetAt(0).setColumnWidth(9, 20 * 256);
		e.getSheetAt(0).setColumnWidth(10, 20 * 256);
		e.getSheetAt(0).setColumnWidth(11, 20 * 256);
		e.getSheetAt(0).setColumnWidth(12, 20 * 256);
		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		List<ComprehensiveOperVo> lists = reportCommMapper.queryComprehensiveOper(map);
		e.writeStr(0, 0,0, center,str+"年综合经营计划完成情况");
		e.writeStr(0, 1,0, center,"日期："+String.valueOf(map.get("reportDate")));
		for (int i=0;i<lists.size();i++) {
			e.writeStr(0, row+3+i,"A", center,lists.get(i).getInstName());
			e.writeDouble(0, row+3+i,"B", center,lists.get(i).getSumgsyty());
			e.writeStr(0, row+3+i,"C", center,"");
			e.writeStr(0, row+3+i,"D", center,"");
			e.writeDouble(0, row+3+i,"E", center,lists.get(i).getSumtyhq());
			e.writeStr(0, row+3+i,"F", center,"");
			e.writeStr(0, row+3+i,"G", center,"");
			e.writeDouble(0, row+3+i,"H", center,lists.get(i).getSumzjyw());
			e.writeStr(0, row+3+i,"I", center,"");
			e.writeStr(0, row+3+i,"J", center,"");
			e.writeDouble(0, row+3+i,"K", center,lists.get(i).getSumpj());
			e.writeStr(0, row+3+i,"L", center,"");
			e.writeStr(0, row+3+i,"M", center,"");
		}
		
	}
	
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.ComprehensiveOper.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
}
