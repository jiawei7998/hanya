package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.base.mapper.TbkSubjectDefMapper;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.bookkeeping.mapper.TbkSubjectBalanceSumMapper;
import com.singlee.cap.bookkeeping.model.TbkSubjectBalanceSum;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;

/***
 * 业务状况
 * @author lee
 *
 */
@Service("reportBusinessStatusServiceImpl")
public class ReportBusinessStatusServiceImpl implements ReportService{
	@Autowired
	private TbkSubjectBalanceSumMapper tbkSubjectBalanceSumMapper;
	@Autowired
	private TbkSubjectDefMapper ttBkSubjectDefMapper;
	@Autowired
	DayendDateService dateService;
	
	@Override
    public RetMsg<Serializable> transRepToHtml(Map<String, Object> map) throws Exception
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.BusinessStatus.getFileName()));
		transportExcel(eu, map);
		ret.setObj(ExcelShower.transExcelToHtml(eu.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}

	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		map.put("postDate", ParameterUtil.getString(map, "reportDate", ""));
		CellStyle center = e.getDefaultCenterStrCellStyle();
		int row = 3;
		
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 156);
		e.getSheetAt(0).setColumnWidth(1, 20 * 206);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		e.getSheetAt(0).setColumnWidth(3, 20 * 216);
		e.getSheetAt(0).setColumnWidth(4, 20 * 216);
		e.getSheetAt(0).setColumnWidth(5, 20 * 216);
		e.getSheetAt(0).setColumnWidth(6, 20 * 216);
		e.getSheetAt(0).setColumnWidth(7, 20 * 216);
		e.getSheetAt(0).setColumnWidth(8, 20 * 216);
	
		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		map.put("isActive", "1");
		String postDate = dateService.getDayendDate().replaceAll("-", "");
		String dateParam= ParameterUtil.getString(map, "reportDate", "").replaceAll("-", "");
		List<TbkSubjectBalanceSum> list=null;
		if(postDate.equals(dateParam)){
			list = tbkSubjectBalanceSumMapper.getAllDiffListBySameDay(map);
		}else{
			list = tbkSubjectBalanceSumMapper.getAllDiffList(map);
		}
		if(list.size() > 0){
			map.put("isActive", "2");
			if(postDate.equals(dateParam)){
				list=tbkSubjectBalanceSumMapper.getAllDiffListBySameDay(map);
			}else{
				list = tbkSubjectBalanceSumMapper.getAllDiffList(map);
			}
			list.addAll(postDate.equals(dateParam)?tbkSubjectBalanceSumMapper.getSubjectBalSumTotalBySameDay(map):tbkSubjectBalanceSumMapper.getSubjectBalSumTotal(map));
			List<TbkSubjectDef> defList = ttBkSubjectDefMapper.getTbkSubjectDefDistinctFirst(null);
			TbkSubjectBalanceSum sum = null;
			for (int i = 0; i < defList.size(); i++) {
				map.put("codeFirst", defList.get(i).getSubjCode());
				List<TbkSubjectBalanceSum> codeTotalList = null;
				if(postDate.equals(dateParam)){
					codeTotalList=tbkSubjectBalanceSumMapper.getSubjectBalCodeTotalBySameDay(map);
				}else{
					codeTotalList =tbkSubjectBalanceSumMapper.getSubjectBalCodeTotal(map);
				}
				if(codeTotalList.size() > 0){
					list.addAll(codeTotalList);
				}else{
					sum = new TbkSubjectBalanceSum();
					sum.setBkkpgOrgId(ParameterUtil.getString(map, "instId", ""));
					sum.setCcy(ParameterUtil.getString(map, "ccy", ""));
					sum.setSubjCode((i+1)+"小计");
					sum.setbCreditValue(0);
					sum.setbDebitValue(0);
					sum.setCreditChg(0);
					sum.setDebitChg(0);
					sum.setCreditValue(0);
					sum.setDebitValue(0);
					list.add(sum);
				}
			}
			
			CollectionUtils.sort(list, false, "subjCode");
		}

		e.writeStr(0, 2,0, center,"单位名称:"+ParameterUtil.getString(map, "instName", ""));
		e.writeStr(0, 2,1, center,"币种:"+ParameterUtil.getString(map, "ccy", ""));
		e.writeStr(0, 2,2, center,"日期:"+ParameterUtil.getString(map, "reportDate", ""));
		e.writeStr(0, 2,3, center,"报表频度:日报");
		for (int i=0;i<list.size();i++) {
			
			e.writeStr(0, row+2+i,"A", center,i+1+"");
			e.writeStr(0, row+2+i,"B", e.getDefaultLeftStrCellStyle(),list.get(i).getSubjCode());
			e.writeStr(0, row+2+i,"C", e.getDefaultLeftStrCellStyle(),list.get(i).getSubjName());
			e.writeDouble(0, row+2+i,"D", e.getDefaultDouble2CellStyle(),list.get(i).getbDebitValue());
			e.writeDouble(0, row+2+i,"E", e.getDefaultDouble2CellStyle(),list.get(i).getbCreditValue());
			e.writeDouble(0, row+2+i,"F", e.getDefaultDouble2CellStyle(),list.get(i).getDebitChg());
			e.writeDouble(0, row+2+i,"G", e.getDefaultDouble2CellStyle(),list.get(i).getCreditChg());
			e.writeDouble(0, row+2+i,"H", e.getDefaultDouble2CellStyle(),list.get(i).getDebitValue());
			e.writeDouble(0, row+2+i,"I", e.getDefaultDouble2CellStyle(),list.get(i).getCreditValue());
			
		}
		
	}
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.BusinessStatus.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
}
