package com.singlee.capital.reports.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.base.service.TradingAccountService;
import com.singlee.cap.base.vo.TradingAccountVo;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.reports.service.ReportService;
import com.singlee.capital.reports.util.ExcelShower;
import com.singlee.capital.reports.util.ExcelUtil;
import com.singlee.capital.reports.util.ReportType;
import com.singlee.capital.reports.util.ReportUtils;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
@Service("reportTradingAccountServiceImpl")
public class ReportTradingAccountServiceImpl implements ReportService {
	@Autowired
	private TradingAccountService tradingAccountService;
	@Autowired
	private TrdTposService trdTposService;
	
	
	
	@Override
	public RetMsg<Serializable> transRepToHtml(Map<String, Object> map)
			throws Exception {
		RetMsg<Serializable> ret = RetMsgHelper.ok("处理成功");
		ExcelUtil e = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.TradingAccount.getFileName()));
		
		transportExcel(e, map);
		
		ret.setObj(ExcelShower.transExcelToHtml(e.getWb(),0));
		return  ret;
	}

	@Override
	public Object queryRep(Map<String, Object> map) throws Exception {
		return null;
	}
	
	public void transportExcel (ExcelUtil e,Map<String, Object> map) throws Exception{
		CellStyle center = e.getDefaultCenterStrCellStyle();
		int row = 1;
		
		//设置列宽
		e.getSheetAt(0).setColumnWidth(0, 20 * 256);
		e.getSheetAt(0).setColumnWidth(1, 20 * 256);
		e.getSheetAt(0).setColumnWidth(2, 20 * 256);
		e.getSheetAt(0).setColumnWidth(3, 20 * 256);
		e.getSheetAt(0).setColumnWidth(4, 20 * 256);
		
		//设置默认行高
		e.getSheetAt(0).setDefaultRowHeightInPoints(20);
		List<TradingAccountVo> lists = new ArrayList<TradingAccountVo>();
		List<TradingAccountVo> listav = tradingAccountService.selectAllTradingAccountService();
		for (TradingAccountVo tradingAccountVo : listav) {
			
			Map<String, Object> mapt = new HashMap<String, Object>();
			mapt.put("dealNo", tradingAccountVo.getDealNo());
			TdTrdTpos ttt = trdTposService.getTdTrdTposByKey(mapt);
			if(ttt == null){
				continue;
			}
			if(PlaningTools.sub(ttt.getSettlAmt(), PlaningTools.sub(tradingAccountVo.getDebitValue(), tradingAccountVo.getCreditValue()))==0){
				//如果差额为0
				continue;
			}
			//头寸
			tradingAccountVo.setSettlAmt(ttt.getSettlAmt());
			//科目余额
			tradingAccountVo.setSubjBalance(PlaningTools.sub(tradingAccountVo.getDebitValue(), tradingAccountVo.getCreditValue()));
			//差额
			tradingAccountVo.setDifference(PlaningTools.sub(ttt.getSettlAmt(), PlaningTools.sub(tradingAccountVo.getDebitValue(), tradingAccountVo.getCreditValue())));
			lists.add(tradingAccountVo);
		}
		
		for (int i=0;i<lists.size();i++) {
			e.writeStr(0, row+2+i,"A", center,lists.get(i).getDealNo());
			e.writeDouble(0, row+2+i,"B", center,lists.get(i).getSettlAmt());
			e.writeStr(0, row+2+i,"C", center,lists.get(i).getSubjCode());
			e.writeDouble(0, row+2+i,"D", center,lists.get(i).getSubjBalance());
			e.writeDouble(0, row+2+i,"E", center,lists.get(i).getDifference());
		}
		
	}
	
	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ExcelUtil eu = new ExcelUtil(ReportUtils.getReportModelFilePath(ReportType.TradingAccount.getFileName()));
			//to_do
			transportExcel(eu, map);
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					throw new RException(e);
				}
			}
		}
	}
	
	/**
	 * 根据属性 来分类List中的对象
	 * -汪泽峰
	 * @param list
	 * @param strings
	 * @return
	 */
//	 public static <T> Map<String, List<T>> listToMap(List<T> list, String... strings) {
//	        Map<String, List<T>> returnMap = new HashMap<String, List<T>>();
//	        try {
//	            for (T t : list) {
//	                StringBuffer stringBuffer = new StringBuffer();
//	                for (String s : strings) {
//	                    Field name1 = t.getClass().getDeclaredField(s);//通过反射获得私有属性,这里捕获获取不到属性异常
//	                    name1.setAccessible(true);//获得访问和修改私有属性的权限
//	                    String key = name1.get(t).toString();//获得key值
//	                    stringBuffer.append(key+"_");
//	                }
//	                String KeyName = stringBuffer.toString();
//
//	                List<T> tempList = returnMap.get(KeyName);
//	                if (tempList == null) {
//	                    tempList = new ArrayList<T>();
//	                    tempList.add(t);
//	                    returnMap.put(KeyName, tempList);
//	                } else {
//	                    tempList.add(t);
//	                }
//	            }
//	        } catch (NoSuchFieldException e) {
//	            throw new RException(e);
//	        } catch (IllegalAccessException e) {
//	            throw new RException(e);
//	        }
//	        return returnMap;
//	    }

}
