package com.singlee.capital.reports.util;

import org.springframework.web.context.ContextLoaderListener;

import com.singlee.capital.common.util.JY;

import java.math.RoundingMode;
import java.text.DecimalFormat;


public class ReportUtils extends ContextLoaderListener{
	private static String model_path = null;
	private static final String ST_FORMAT = "###,###,###,###,###,###,##0.00";
	private static final DecimalFormat SC_FORMATOR = new DecimalFormat(ST_FORMAT);

	public static String getReportModelDir()
	{
		try {	
			if(model_path == null)
			{ 
				model_path = getCurrentWebApplicationContext().getServletContext().getRealPath("standard/reports/reportModel");
			}
		} catch (Exception e) {
			//e.printStackTrace();
			JY.error("获取报表模板路径出错", e);
		}
		return model_path;
	}
	
	public static String getReportModelFilePath(String reportName)
	{
		return getReportModelDir() + "/" + reportName;
	}

	public static String ObjtoStr(Object obj){
		if(obj==null||obj.equals("")){
			obj=0.0000;
		}
		//double douNum=Double.parseDouble(obj.toString());
		return  string2Account(obj.toString());
	}

	private static String string2Account(String value) {
		if(value != null && !value.equals("") && Double.parseDouble(value)!=0){
			SC_FORMATOR.setRoundingMode(RoundingMode.HALF_UP);
			return SC_FORMATOR.format(Double.parseDouble(value));
		}
		return "0";
	}
}
