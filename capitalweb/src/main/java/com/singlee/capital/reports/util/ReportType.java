package com.singlee.capital.reports.util;

public enum ReportType {
	DailyAvgBalance("DailyAvgBalance.xls","日均余额汇总表"), 
	
	AssetLiabilityRatio("信用资产负债率指标.xls","信用资产负债率指标表"),
	
	BalanceSummary("余额汇总表.xls","余额汇总表") , 
	
	BusinessStatus("业务状况表.xls","业务状况表"),  
	
	CnyBalanceSheet("JSB28人民币业务资产负债表.xls","JSB28人民币业务资产负债表"),  
	
	ComprehensiveOper("综合经营指标-指标数表.xls","综合经营指标-指标数表"),  
	
	G31("G31投资业务情况表.xls","G31投资业务情况表") ,  
	
	InCharge("经营单位中收明细表.xls","经营单位中收明细表"), 
	
	InChargeSum("中间业务收入汇总表.xls","中间业务收入汇总表") ,
	
	IncomeSummary("收益汇总表.xls","收益汇总表"),   
	
	InterbankAsset("JSB10月末同业资产表.xls","月末同业资产表"),   
	
	InterbankInv("同业投资情况表.xls","同业投资情况表"), 
	
	TradingAccount("交易头寸对账.xls","交易头寸对账"),
	
	TbkEntryRep("增值税补缴表.xls","增值税补缴表"),
	
	YearEnd("年终结算报表.xls","年终结算报表"),
	
	ReportTrade("ReportTrade.xls","非标资产交易台账"),

	ReportInterBank("ReportInterBank.xls","存放同业、同业借款报表"),
	
	ReportSec("ReportSec.xls","债券台账明细"),
	
	ReportEntrustSec("ReportEntrustSec.xls","委外投资债券台账"),
	
	ReportEntrust("ReportEntrust.xls","委外业务统计表"),

	FeesBroker("中介费详情表.xls","中介费详情表"),

	EntriesQuery("会计分录查询.xls","会计分录查询");

	private String fileName;
	private String desc;
	
	private ReportType(String fileName,String desc)
	{
		this.fileName = fileName;
		this.desc = desc;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
