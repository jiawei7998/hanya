package com.singlee.capital.reports.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
@SuppressWarnings("deprecation")
public class ExcelShower {
	
	private static DecimalFormat dfm = new DecimalFormat("#,##0.00");

	/**
	 * 读取 Excel 显示页面.
	 * 
	 * @throws Exception
	 */
	public static StringBuffer transExcelToHtml(InputStream io,int sheetIndex) throws Exception {
		StringBuffer html = new StringBuffer();
		try {
			HSSFWorkbook workbook = new HSSFWorkbook(io); // 获整个Excel
			if(sheetIndex >= 0 && sheetIndex < workbook.getNumberOfSheets() && workbook.getSheetAt(sheetIndex) != null){
				html.append(transExcelToTable(workbook, workbook.getSheetAt(sheetIndex)));
				
			}else{
				for (sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) 
				{
					if (workbook.getSheetAt(sheetIndex) != null)
					{
						html.append(transExcelToTable(workbook, workbook.getSheetAt(sheetIndex)));
					}
				}
			}	
		} catch (Exception e) {
			throw new Exception("读取EXCEL文件出错",e);
		}
		return html;
	}
	
	/**
	 * 读取 Excel 显示页面.
	 * 
	 * @throws Exception
	 */
	public static StringBuffer transExcelToHtml(Workbook workbook1,int sheetIndex) throws Exception {
		StringBuffer html = new StringBuffer();
		HSSFWorkbook workbook = null;
		try {
			if(workbook1 instanceof HSSFWorkbook){
				workbook = (HSSFWorkbook)workbook1;
			}else{
				throw new Exception("只支持HSSFWorkbook");
			}
			if(sheetIndex >= 0 && sheetIndex < workbook.getNumberOfSheets() && workbook.getSheetAt(sheetIndex) != null){
				html.append(transExcelToTable(workbook, workbook.getSheetAt(sheetIndex)));
				
			}else{
				for (sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) 
				{
					if (workbook.getSheetAt(sheetIndex) != null)
					{
						html.append(transExcelToTable(workbook, workbook.getSheetAt(sheetIndex)));
					}
				}
			}	
		} catch (Exception e) {
			throw new Exception("读取EXCEL文件出错",e);
		}
		return html;
	}
	
	
	public static StringBuffer transExcelToTable(HSSFWorkbook workbook,HSSFSheet sheet) throws Exception{
		StringBuffer table = new StringBuffer();
		String mergeIndex = null;
		String locate = null;
		String[] index = null;
		HashMap<String, String> mergerCellMap = getMergerCellMap(sheet);
		HashMap<String, String> mergerFirstCellMap = getMergerFirstCellMap(sheet);
		int firstRowNum = sheet.getFirstRowNum(); // 第一行
		int lastRowNum = sheet.getLastRowNum(); // 最后一行
		// 构造Table
		//lsb.append("<table width=\"100%\" style=\"border:1px solid #000;border-width:1px 0 0 1px;margin:2px 0 2px 0;border-collapse:collapse;\">");
		table.append("<table class = 'form_table' style=\"border:1px solid #000;border-width:1px 0 0 1px;margin:2px 0 2px 0;border-collapse:collapse;\">");
		for (int rowNum = firstRowNum; rowNum <= lastRowNum; rowNum++) 
		{
			if (sheet.getRow(rowNum) != null) 
			{// 如果行不为空，
				HSSFRow row = sheet.getRow(rowNum);
				short firstCellNum = row.getFirstCellNum(); // 该行的第一个单元格
				short lastCellNum = row.getLastCellNum(); // 该行的最后一个单元格
				int height = (int) (row.getHeight() / 10.625); // 行的高度
				table.append("<tr height=\"" + height + "\" style=\"border:1px solid #000;border-width:0 1px 1px 0;margin:2px 0 2px 0;\">");
				for (short cellNum = firstCellNum; cellNum <= lastCellNum; cellNum++) { // 循环该行的每一个单元格
					HSSFCell cell = row.getCell((int)cellNum);
					if (cell != null) {
						locate = rowNum  + "_" + cellNum;
						//如果是合并的单元格并且不是第一个单元格,则直接跳过
						if (cell.getCellType() == HSSFCell.CELL_TYPE_BLANK && mergerCellMap.get(locate) != null && mergerFirstCellMap.get(locate) == null) {
							continue;
						} else {
							StringBuffer tdStyle = new StringBuffer("<td style=\"border:1px solid #000; border-width:0 1px 1px 0;margin:2px 0 2px 0; ");
							HSSFCellStyle cellStyle = cell.getCellStyle();
							HSSFPalette palette = workbook.getCustomPalette(); // 类HSSFPalette用于求颜色的国际标准形式
							HSSFColor hColor = palette.getColor(cellStyle.getFillForegroundColor());
							HSSFColor hColor2 = palette.getColor(cellStyle.getFont(workbook).getColor());

							String bgColor = convertToStardColor(hColor);// 背景颜色
							short boldWeight = cellStyle.getFont(workbook).getFontHeight(); // 字体粗细
							short fontHeight = (short) (cellStyle.getFont(workbook).getFontHeight() / 2); // 字体大小
							String fontColor = convertToStardColor(hColor2); // 字体颜色
							if (bgColor != null&& !"".equals(bgColor.trim())) {
								tdStyle.append(" background-color:"+ bgColor + "; ");
							}
							if (fontColor != null&& !"".equals(fontColor.trim())) {
								tdStyle.append(" color:"+ fontColor + "; ");
							}
							tdStyle.append(" font-weight:"+ boldWeight + "; ");
							tdStyle.append(" font-size: "+ fontHeight + "%;");
							table.append(tdStyle + "\"");

							int width = (int) (sheet.getColumnWidth((int)cellNum) / 33); 
							int cellReginCol = 1;
							int cellReginRow = 1;
							mergeIndex = mergerFirstCellMap.get(locate);
							if(mergeIndex != null){
								index = mergeIndex.split("_");
								cellReginRow = Integer.valueOf(index[0]);
								cellReginCol =  Integer.valueOf(index[1]);
							}
							String align = convertAlignToHtml(cellStyle.getAlignmentEnum());
							String vAlign = convertVerticalAlignToHtml(cellStyle.getVerticalAlignmentEnum());
							
							if(cellReginCol > 1)
							{			
								for (int i = cellNum + 1; i < cellNum + cellReginCol; i++) {
									width += (int) (sheet.getColumnWidth(i) / 20);
								}
							}
							
							table.append(" colspan=\"" + cellReginCol + "\" rowspan=\"" + cellReginRow + "\"");
							table.append(" align=\"" + align+ "\" valign=\"" + vAlign+ "\" width=\"" + width + "\" ");
							table.append(">" + getCellValue(cell) + "</td>");
						}
					}
				}
				table.append("</tr>");
			}
		}// end for
		table.append("</table>");
		return table;
	}

	/**
	 * 取得单元格的值
	 * 
	 * @param cell
	 * @return
	 * @throws IOException
	 */
	private static Object getCellValue(HSSFCell cell) throws Exception {
		Object value = "";
		switch (cell.getCellType()) 
		{
		case HSSFCell.CELL_TYPE_BOOLEAN:
			value = cell.getBooleanCellValue();
			break;
					
		case HSSFCell.CELL_TYPE_FORMULA:
			try {
				BigDecimal bd = new BigDecimal(String.valueOf(cell.getNumericCellValue()));
				value = dfm.format(bd);
				
			} catch (Exception e) {
				try {
					value = cell.getRichStringCellValue();
				} catch (Exception e2) {
					value = "";
				}
			}
			break;
			
		case HSSFCell.CELL_TYPE_NUMERIC:
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				Date date = cell.getDateCellValue();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				value = sdf.format(date);
				
			} else {
				BigDecimal bd = null;
				try {
					bd = new BigDecimal(String.valueOf(cell.getNumericCellValue()));
				} catch (Exception e) {
					//e.printStackTrace();
					bd = new BigDecimal(0);
				}
				value = dfm.format(bd);
				
			}
			break;
			
		case HSSFCell.CELL_TYPE_STRING:
			value = cell.getRichStringCellValue().toString();
			break;

		default:
			value = "";
			break;
		} // end switch
		return value;
	}
	
	/****
	 * 获取合并单元格的MAP
	 * @param sheet
	 * @return
	 * @throws IOException
	 */
	public static HashMap<String, String> getMergerCellMap(HSSFSheet sheet) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		int sheetMergerCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergerCount; i++) 
		{
			CellRangeAddress cra = (CellRangeAddress) sheet.getMergedRegion(i);
			int firstRow = cra.getFirstRow(); // 合并单元格CELL起始行
			int firstCol = cra.getFirstColumn(); // 合并单元格CELL起始列
			int lastRow = cra.getLastRow(); // 合并单元格CELL结束行
			int lastCol = cra.getLastColumn(); // 合并单元格CELL结束列
			int colVal = lastCol - firstCol + 1; // 得到合并的列数
			int rowVal = lastRow - firstRow + 1; // 得到合并的行数
			for (int row = firstRow; row <= lastRow; row++) {
				for (int col = firstCol; col <= lastCol; col++) 
				{
					map.put(row + "_" + col, rowVal + "_" + colVal);
				}
			}
		}
		return map;
	}
	
	/****
	 * 获取合并单元格中的第一个单元格位置
	 * @param sheet
	 * @return
	 * @throws IOException
	 */
	public static HashMap<String, String> getMergerFirstCellMap(HSSFSheet sheet) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		int sheetMergerCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergerCount; i++) 
		{
			CellRangeAddress cra = (CellRangeAddress) sheet.getMergedRegion(i);
			int firstRow = cra.getFirstRow(); // 合并单元格CELL起始行
			int firstCol = cra.getFirstColumn(); // 合并单元格CELL起始列
			int lastRow = cra.getLastRow(); // 合并单元格CELL结束行
			int lastCol = cra.getLastColumn(); // 合并单元格CELL结束列
			int colVal = lastCol - firstCol + 1; // 得到合并的列数
			int rowVal = lastRow - firstRow + 1; // 得到合并的行数
			map.put(firstRow + "_" + firstCol, rowVal + "_" + colVal);
		}
		return map;
	}

	/**
	 * 判断单元格在不在合并单元格范围内，如果是，获取其合并的列数。
	 * 
	 * @param sheet
	 *            工作表
	 * @param cellRow
	 *            被判断的单元格的行号
	 * @param cellCol
	 *            被判断的单元格的列号
	 * @return
	 * @throws IOException
	 */
	public static int getMergerCellRegionCol(HSSFSheet sheet, int cellRow,int cellCol) throws IOException {
		int retVal = 1;
		int sheetMergerCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergerCount; i++) {
			CellRangeAddress cra = (CellRangeAddress) sheet.getMergedRegion(i);
			int firstRow = cra.getFirstRow(); // 合并单元格CELL起始行
			int firstCol = cra.getFirstColumn(); // 合并单元格CELL起始列
			int lastRow = cra.getLastRow(); // 合并单元格CELL结束行
			int lastCol = cra.getLastColumn(); // 合并单元格CELL结束列
			if (cellRow >= firstRow && cellRow <= lastRow) { // 判断该单元格是否是在合并单元格中
				if (cellCol >= firstCol && cellCol <= lastCol) {
					retVal = lastCol - firstCol + 1; // 得到合并的列数
					break;
				}
			}
		}
		return retVal;
	}

	/**
	 * 判断单元格是否是合并的单格，如果是，获取其合并的行数。
	 * 
	 * @param sheet
	 *            表单
	 * @param cellRow
	 *            被判断的单元格的行号
	 * @param cellCol
	 *            被判断的单元格的列号
	 * @return
	 * @throws IOException
	 */
	public static int getMergerCellRegionRow(HSSFSheet sheet, int cellRow,int cellCol) throws IOException {
		int retVal = 1;
		int sheetMergerCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergerCount; i++) {
			CellRangeAddress cra = (CellRangeAddress) sheet.getMergedRegion(i);
			int firstRow = cra.getFirstRow(); // 合并单元格CELL起始行
			int firstCol = cra.getFirstColumn(); // 合并单元格CELL起始列
			int lastRow = cra.getLastRow(); // 合并单元格CELL结束行
			int lastCol = cra.getLastColumn(); // 合并单元格CELL结束列
			if (cellRow >= firstRow && cellRow <= lastRow) { // 判断该单元格是否是在合并单元格中
				if (cellCol >= firstCol && cellCol <= lastCol) {
					retVal = lastRow - firstRow + 1; // 得到合并的行数
					break;
				}
			}
		}
		return retVal;
	}

	/**
	 * 单元格背景色转换
	 * 
	 * @param hc
	 * @return
	 */
	private static String convertToStardColor(HSSFColor hc) {
		StringBuffer sb = new StringBuffer("");
		if (hc != null) {
			int a = HSSFColor.AUTOMATIC.index;
			int b = hc.getIndex();
			if (a == b) {
				return null;
			}
			sb.append("#");
			for (int i = 0; i < hc.getTriplet().length; i++) {
				String str;
				String str_tmp = Integer.toHexString(hc.getTriplet()[i]);
				if (str_tmp != null && str_tmp.length() < 2) {
					str = "0" + str_tmp;
				} else {
					str = str_tmp;
				}
				sb.append(str);
			}
		}
		return sb.toString();
	}

	/**
	 * 单元格小平对齐
	 * 
	 * @param alignment
	 * @return
	 */
	private static String convertAlignToHtml(HorizontalAlignment alignment) {
		String align = "left";
		switch (alignment) {
		case LEFT:
			align = "left";
			break;
			
		case CENTER:
			align = "center";
			break;
			
		case RIGHT:
			align = "right";
			break;
			
		default:
			break;
		}
		return align;
	}

	/**
	 * 单元格垂直对齐
	 * 
	 * @param verticalAlignment
	 * @return
	 */
	private static String convertVerticalAlignToHtml(VerticalAlignment verticalAlignment) {
		String valign = "middle";
		switch (verticalAlignment) {
		case BOTTOM:
			valign = "bottom";
			break;
		case CENTER:
			valign = "middle";
			break;
		case TOP:
			valign = "top";
			break;
		default:
			break;
		}
		return valign;
	}

}
