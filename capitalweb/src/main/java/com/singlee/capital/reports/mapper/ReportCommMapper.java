package com.singlee.capital.reports.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.cap.base.vo.TradingAccountVo;
import com.singlee.capital.reports.model.ComprehensiveOperVo;
import com.singlee.capital.reports.model.InChargeVo;
import com.singlee.capital.reports.model.RepAssetLiabilityRatioVo;
import com.singlee.capital.reports.model.ReportInterbankInVo;
/**
 * 报表公共类
 * @author kf0742
 *
 */
public interface ReportCommMapper extends Mapper<TradingAccountVo> {
	
	public List<TradingAccountVo> selectAllTradingAccount();
	/**
	 * 同业投资情况表
	 * @return
	 */
	public List<ReportInterbankInVo> getReportInterbankAccount(Map<String,Object> params);
	//同业投资情况合计
	public List<ReportInterbankInVo> getReportInterbankhjAccount(Map<String,Object> params);
	//较上月本外合计
	public List<ReportInterbankInVo> getReportInterbankdifferAccount(Map<String,Object> params);
	
	
	/**
	 * 信用资产负债率指标
	 * @param params
	 * @return
	 */
	public List<RepAssetLiabilityRatioVo> getRepAssetLiabilityRatioAccount(Map<String,Object> params);
	
	/**
	 * 经营单位中收明细表
	 * @param params
	 * @return
	 */
	public List<InChargeVo> queryInCharge(Map<String,Object> params);
	
	/**
	 * 综合经营指标-指标数表
	 * @param params
	 * @return
	 */
	public List<ComprehensiveOperVo> queryComprehensiveOper(Map<String,Object> params);
	
	
}
