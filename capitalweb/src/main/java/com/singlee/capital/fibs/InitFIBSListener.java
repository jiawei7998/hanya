package com.singlee.capital.fibs;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.logger.LogConstants;
import com.singlee.capital.common.spring.InitListener;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.cron.model.TaJob;
import com.singlee.capital.cron.service.JobService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.interfacex.service.TiInterfaceInfoService;
import com.singlee.capital.system.service.BrccService;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InitFIBSListener extends InitListener{
	
	private static final Logger logger = LoggerFactory.getLogger("CTMS");
	static{
		JY.setLog(logger);
	}
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private JobService jobService;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	private BrccService brccService;
	@Override
	protected String sysname() {
		return "CTMS-【资金业务管理系统】";
	}
	@Autowired
	private RedissonClient redissonClient;

	@Override
	protected void init(ApplicationContext ac) {
		JY.info("··················正在缓存初始化·················");
		//字典初始化 
//		com.joyard.jc.util.Constants.initDict("com.singlee.capital.system.dict.DictConstants");
//		com.singlee.capital.system.dict.ExternalDictUtil.initDict("com.singlee.capital.system.dict.DictConstants");
		dictionaryGetService.getDictionaryArrayJsonString();
		JY.info("··················字典初始化成功····················");
		//清除所有缓存
		redissonClient.getKeys().flushall();

		List<TaJob> taJobLst = jobService.getTaJobLst();
		for (TaJob taJob :taJobLst){
			jobService.getJobById(taJob.getJobId());
			jobService.getTaJobConfigByJobId(new Integer(taJob.getJobId()));
		}
		JY.info("··················Jobs初始化成功····················");
		brccService.getAllTaBrcc();
		JY.info("··················Brcc初始化成功····················");
		// 业务日设置
 		//DayendDateService dayendDateService = (DayendDateService) ac.getBean("dayendDateService");
		//dayendDateService.setSettlementDate(dd.getDayendDate());
// 		dayendDateService.initDayendDate();
//		JY.info("··················业务日初始化成功····················");

		//注册ESB连接
//		ExchangeManager.getInstance().addListener(new JEsbListener());
//		JY.info("··················ESB连接初始化成功····················");
		//初始化指令配置
//		InstructionFactoryConfig.init();
//		JY.info("··················指令初始化成功····················");		
		//初始化清算配置	
		//ClearFactoryConfig.init();
//		JY.info("··················清算配置初始化成功····················");			
		//初始化核算转换配置
//		TransferExtFactoryConfig.init();
//		JY.info("··················核算转换配置初始化成功····················");			
		//初始化持仓结算配置
		//PositionSettleFactoryConfig.init();
//		JY.info("··················持仓结算配置初始化成功····················");			
		//初始化定时任务初始化
		//JobService jobService = (JobService) ac.getBean("jobService");
		jobService.init();
		JY.info("··················定时任务初始化成功····················");
		//初始化接口列表配置项

		TiInterfaceInfoService tiInterfaceInfoService = ac.getBean("tiInterfaceInfoServiceImpl",TiInterfaceInfoService.class);
		if(null != tiInterfaceInfoService){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("pageNumber", 1);
			map.put("pageSize", 9999);
			try {
				List<TiInterfaceInfo> tiInterfaceInfos = tiInterfaceInfoService.selectInterfaceInfoByName(map).getResult();
				for(TiInterfaceInfo tiInterfaceInfo  :  tiInterfaceInfos){
					LogConstants.INTERFACE_CONFIG_MAP.put(StringUtils.trimToEmpty(tiInterfaceInfo.getInfaceName()), tiInterfaceInfo);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new RException(e);
			}
		}
	}

}
