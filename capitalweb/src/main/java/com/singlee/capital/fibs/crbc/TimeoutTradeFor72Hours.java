package com.singlee.capital.fibs.crbc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.model.JobExcuLog;
import com.singlee.capital.interfacex.service.JobExcuService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.TradeManageService;

/**
 * CRBC定时任务
 * @author LyonChen
 * 调度 时间 在spring 中配置指定，
 */
public class TimeoutTradeFor72Hours implements CronRunnable{
	
	private TradeManageService tradeManageService = SpringContextHolder.getBean("tradeManageService");
	private ApproveManageService approveManageService = SpringContextHolder.getBean("approveManageService");;
	
	//核实单新建72小时无变化，就需要注销核实单--->注销审批单--->释放额度
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {

		HashMap<String,Object> map = new HashMap<String, Object>();
		String[] tradeStatusList = {DictConstants.ApproveStatus.New};
		map.put("tradeStatusList", tradeStatusList);
		List<TtTrdTrade> tradeList = tradeManageService.selectTtTrdTrade(map);
		
		String businessDate = DateUtil.getCurrentDateTimeAsString(); //取当前系统日期详细时间yyyy-MM-dd HH:mm:ss
		int checkDate = 0; 	//检查超时
		
		for(TtTrdTrade trade : tradeList){
			checkDate = DateUtil.hoursBetween(trade.getOperate_time(), businessDate);
			if(checkDate>72){
				tradeManageService.cancelTrade(trade.getTrade_id(), trade.getVersion_number(), "", "注销", "admin");
				approveManageService.cancelApprove(trade.getOrder_id(), "", "admin");
			}
		}
		
		return true;
	}
	
	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
	
	public void test(){
		JY.info("------------额度释放开始！---------");
		HashMap<String,Object> map = new HashMap<String, Object>();
		map.put("trade_id", "TRD201610253001000488");
		List<TtTrdTrade> tradeList = tradeManageService.selectTtTrdTrade(map);
		
		for(TtTrdTrade trade : tradeList){
			tradeManageService.cancelTrade(trade.getTrade_id(), trade.getVersion_number(), "", "注销", "admin");
			approveManageService.cancelApprove(trade.getOrder_id(), "", "admin");
		}
		
	}
	
}
