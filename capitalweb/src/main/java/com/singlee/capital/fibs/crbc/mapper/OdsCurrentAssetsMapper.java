package com.singlee.capital.fibs.crbc.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.fibs.crbc.model.OdsCurrentAssets;

/**
 * @description TODO
 * @author wuyc
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface OdsCurrentAssetsMapper extends Mapper<OdsCurrentAssets> {

	/**
	 * @author wuyc
	 */

	public List<Map<String,Object>> selectAssets();
	
	/**
	 * 插入活期资产数据
	 * @return 
	 */
	public void insertAsset(OdsCurrentAssets obj);
	
}