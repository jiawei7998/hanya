package com.singlee.capital.fibs.crbc.service;

public interface CurrentAccountQuartzService {
	
	public void executeAssets();
	
	public void executeLiabilities();
	
}
