package com.singlee.capital.fibs.crbc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fibs.crbc.mapper.OdsCurrentAssetsMapper;
import com.singlee.capital.fibs.crbc.mapper.OdsCurrentLiabilitiesMapper;
import com.singlee.capital.fibs.crbc.model.OdsCurrentAssets;
import com.singlee.capital.fibs.crbc.model.OdsCurrentLiabilities;
import com.singlee.capital.fibs.crbc.service.CurrentAccountQuartzService;


/**
 * @author wuyc
 * @company 杭州新利科技有限公司
 * @version 1.0
 */
@Service("currentAccountQuartzService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CurrentAccountQuartzServiceImpl implements CurrentAccountQuartzService{
	
	@Autowired
	private OdsCurrentLiabilitiesMapper odsCurrentLiabilitiesMapper;
	
	@Autowired
	private OdsCurrentAssetsMapper odsCurrentAssetsMapper;
	
	@Autowired
	private BatchDao batchdao;

	@Override
	public void executeAssets() {
		JY.info("自动备份活期资产账户数据开始。。。。");
		List<Map<String,Object>> listAsset = odsCurrentAssetsMapper.selectAssets();
		JY.info("总共"+listAsset.size()+"条数据");
		List<OdsCurrentAssets> targetList = new ArrayList<OdsCurrentAssets>();
		if(!listAsset.isEmpty()){
			for(int i=0;i<listAsset.size();i++){
				Map<String,Object> map = listAsset.get(i);
				OdsCurrentAssets asset = new OdsCurrentAssets();
				asset.setAcctDesc(ParameterUtil.getString(map, "ACCTDESC", ""));
				asset.setActualBal(ParameterUtil.getString(map, "ACTUALBAL", ""));
				asset.setActualBalCny(ParameterUtil.getString(map, "ACTUALBALCNY", ""));
				asset.setActualBalUsd(ParameterUtil.getString(map, "ACTUALBALUSD", ""));
				asset.setBranch(ParameterUtil.getString(map, "BRANCH", ""));
				asset.setCcy(ParameterUtil.getString(map, "CCY", ""));
				asset.setGlCode(ParameterUtil.getString(map, "GLCODE", ""));
				asset.setGlCodeDesc(ParameterUtil.getString(map, "GLCODEDESC", ""));
				asset.setCurrentDate(DateUtil.getCurrentDateAsString());
				targetList.add(asset);
			}
			JY.info("批处理活期资产开始******************");
			batchdao.batch("com.singlee.capital.fibs.crbc.mapper.OdsCurrentAssetsMapper.insertAsset", targetList, 100);
			JY.info("批处理活期资产结束--------------------");
		}
	}
	
	@Override
	public void executeLiabilities() {
		JY.info("自动备份活期负债账户数据开始。。。。");
		List<Map<String,Object>> listLiabilities = odsCurrentLiabilitiesMapper.selectLiabilities();
		JY.info("总共"+listLiabilities.size()+"条数据");
		List<OdsCurrentLiabilities> targetList = new ArrayList<OdsCurrentLiabilities>();
		if(!listLiabilities.isEmpty()){
			for(int i=0;i<listLiabilities.size();i++){
				Map<String,Object> map = listLiabilities.get(i);
				OdsCurrentLiabilities liabilities = new OdsCurrentLiabilities();
				liabilities.setAcctName(ParameterUtil.getString(map, "ACCTNAME", ""));
				liabilities.setAcctOpenDate(ParameterUtil.getString(map, "ACCTOPENDATE", ""));
				liabilities.setActualBal(ParameterUtil.getString(map, "ACTUALBAL", ""));
				liabilities.setActualBalCny(ParameterUtil.getString(map, "ACTUALBALCNY", ""));
				liabilities.setActualBalUsd(ParameterUtil.getString(map, "ACTUALBALUSD", ""));
				liabilities.setBranch(ParameterUtil.getString(map, "BRANCH", ""));
				liabilities.setCcy(ParameterUtil.getString(map, "CCY", ""));
				liabilities.setCurrentDate(DateUtil.getCurrentDateAsString());
				liabilities.setGlCodeDesc(ParameterUtil.getString(map, "GLCODEDESC", ""));
				liabilities.setGlCodeL(ParameterUtil.getString(map, "GLCODEL", ""));
				liabilities.setmDate(ParameterUtil.getString(map, "MDATE", ""));
				targetList.add(liabilities);
			}
			JY.info("批处理活期负债开始******************");
			batchdao.batch("com.singlee.capital.fibs.crbc.mapper.OdsCurrentLiabilitiesMapper.insertLiabilities", targetList, 100);
			JY.info("批处理活期负债结束--------------------");
		}
	}

}
