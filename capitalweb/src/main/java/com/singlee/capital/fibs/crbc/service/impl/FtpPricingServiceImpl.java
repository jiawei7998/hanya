package com.singlee.capital.fibs.crbc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.joyard.jc.util.DateUtils;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fibs.crbc.mapper.TtFtpCurveMapper;
import com.singlee.capital.fibs.crbc.model.TtFtpCurve;
import com.singlee.capital.fibs.crbc.model.TtFtpVirtual;
import com.singlee.capital.fibs.crbc.service.FtpPricingService;

/**
 * @className ftp定价
 * @author wuyc
 * @company 杭州新利科技有限公司
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FtpPricingServiceImpl implements FtpPricingService{

	@Autowired
	private TtFtpCurveMapper ttFtpCurveMapper;
	
	@Autowired
	private BatchDao batchdao;

	@Override
	public double fixingFtpPrice(String value_date, int term_days){
		JY.require(term_days>=0, "期限天数%d不对，请维护", term_days);
		// 确保是 升序 排列
		List<TtFtpCurve> list = ttFtpCurveMapper.listMaxFtpCurve(value_date);
		//JY.require(list!=null && list.size()>0 , "ftp曲线数据错误，请维护！");
		
		//Collections.sort(list); 
		// 1 如果 term_days能在list找到就返回
		double b = 0;
		if(list!=null && list.size()>0){
			int left = 0;
			int right = 0;
			if(list.size()==1){
				return list.get(0).getRef_rate();
			}
			for(int i=0;i<list.size();i++){
				int term = list.get(i).getTerm_days();
				if(term_days==term){
					return list.get(i).getRef_rate();
				}else if(term_days < term){
					left = i-1;
					right = i;
					break;
				}
			}
			
			// 3 取最后一个，最大的期限数据
			if(left ==0 && right == 0){
				b =  list.get(list.size()-1).getRef_rate();
			}else{
			// 2 取上下两个
				TtFtpCurve l =list.get(left);
				TtFtpCurve r = list.get(right);
				double rate = l.getRef_rate();
				double terms = l.getTerm_days();
				double rate1 = r.getRef_rate();
				double terms1 = r.getTerm_days();
				b =  (terms1-terms)*(rate1-rate)/(term_days-terms)+rate; 
			}
		}
		return b;
		
	}

	
	
	@Override
	public void ftpImport() {
		ttFtpCurveMapper.updateOrInsertFtpCurve();
		ttFtpCurveMapper.deleteFtpCurve();
		ttFtpCurveMapper.updateOrInsertFtpProfit();
		ttFtpCurveMapper.deleteFtpProfit();
	}
	
	@Override
	public void ftpRecalc(){
		ttFtpCurveMapper.deleteVirtual();
		List<Map<String,Object>> list = ttFtpCurveMapper.listFtpMultiresale();
		List<TtFtpVirtual> targetList = new ArrayList<TtFtpVirtual>();
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = list.get(i);
			String resDate = ParameterUtil.getString(map,"RES_DATE","");// 卖断 
			String dealNo = ParameterUtil.getString(map,"DEAL_NO","");
			String vDate = ParameterUtil.getString(map,"VALUE_DATE","");// 到期
			String mDate = ParameterUtil.getString(map,"M_DATE","");// 到期 
			double refRate = ParameterUtil.getDouble(map,"REF_RATE",0);// 实际利率
			double amt = ParameterUtil.getDouble(map,"AMT",0);//本金 
			int term = ParameterUtil.getInt(map, "TERM_DAYS", 0);// 期限 
			int basis = ParameterUtil.getInt(map, "BASIS",360);//			
			double ftprate = fixingFtpPrice(vDate,term);
			double sum_day = (refRate-ftprate)*(1.0/basis)*amt;
			long shouldDays = DateUtils.days(resDate, mDate);
			int count = 0;
			while(count<=shouldDays){
				double profit_sum=sum_day*(count+1);
				TtFtpVirtual tfv  = new TtFtpVirtual();
				tfv.setDeal_no(dealNo);
				tfv.setM_date(mDate);
				tfv.setSum_day(sum_day);	
				tfv.setProfit_sum(profit_sum);
				tfv.setRes_date(resDate);
				tfv.setLast_update(DateUtil.getCurrentDateTimeAsString());
				tfv.setRef_date(DateUtil.dateAdjust(resDate, count));
				targetList.add(tfv);
				count++;
			}		
			
		}		
		batchdao.batch("com.singlee.capital.fibs.crbc.mapper.TtFtpCurveMapper.insertVirtual", targetList, 100);
	}

}
