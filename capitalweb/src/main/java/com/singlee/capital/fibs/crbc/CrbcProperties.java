package com.singlee.capital.fibs.crbc;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;

/**
 * 华润银行的特殊参数获取
 * @author x230i
 *
 */
public class CrbcProperties {
	public static final String nas_fms_path;  //#财管系统NAS目录	//nas_fms_path=/filehome/fibs/fms
	public static final String nas_vats_path; // #增值税系统NAS目录	nas_vats_path=/filehome/fibs/vats
	
	public static final String tradeCode;
	public static final String scenarioCode;
	public static final String systemId;
	public static final String consumer_id;
	public static final String org_sys_id;
	
	public static final String program_id_um; 
	public static final String bizDate ;
	
	static{
		PropertiesConfiguration pcfg = null;
		try {
			 pcfg = PropertiesUtil.parseFile("crbc/crbc.system.properties");
		} catch (ConfigurationException e) {
			JY.raise(e.getMessage());
		}
		nas_fms_path = pcfg.getString("nas_fms_path", "/filehome/fibs/fms");
		nas_vats_path = pcfg.getString("nas_vats_path", "/filehome/fibs/vats");
		tradeCode= pcfg.getString("um.trade_code","");
		scenarioCode= pcfg.getString("um.scenario_code","");
		systemId= pcfg.getString("um.system_id","");
		program_id_um = pcfg.getString("um.program_id_um", "ECIF0101");
		bizDate =pcfg.getString("um.bizdate",null);
		consumer_id = pcfg.getString("um.consumer_id", null);
		org_sys_id = pcfg.getString("um.org_sys_id", null);
	}

}
