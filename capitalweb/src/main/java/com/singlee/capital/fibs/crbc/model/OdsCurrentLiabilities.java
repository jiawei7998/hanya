package com.singlee.capital.fibs.crbc.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ODS_CURRENT_LIABILITIES")
public class OdsCurrentLiabilities implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 活期负债ID
	 */
	@Id
	private String liabilitiesId;
	
	/**
	 * 开户行
	 */
	private String branch;
	
	/**
	 *负债科目代码
	 */
	private String glCodeL;
	
	/**
	 * 名称
	 */
	private String glCodeDesc;
	
	/**
	 * 账户名称
	 */
	private String acctName;
	
	/**
	 *货币
	 */
	private String ccy;
	
	/**
	 *可用余额
	 */
	private String actualBal;
	
	/**
	 *实际可用余额
	 */
	private String actualBalCny;
	
	/**
	 *实际可用余额(美元)
	 */
	private String actualBalUsd;
	
	/**
	 *开户日期
	 */
	private String acctOpenDate;
	
	/**
	 *到期日期
	 */
	private String mDate;
	
	/**
	 * 当前日期
	 */
	private String currentDate;

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getLiabilitiesId() {
		return liabilitiesId;
	}

	public void setLiabilitiesId(String liabilitiesId) {
		this.liabilitiesId = liabilitiesId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getGlCodeL() {
		return glCodeL;
	}

	public void setGlCodeL(String glCodeL) {
		this.glCodeL = glCodeL;
	}

	public String getGlCodeDesc() {
		return glCodeDesc;
	}

	public void setGlCodeDesc(String glCodeDesc) {
		this.glCodeDesc = glCodeDesc;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getActualBal() {
		return actualBal;
	}

	public void setActualBal(String actualBal) {
		this.actualBal = actualBal;
	}

	public String getActualBalCny() {
		return actualBalCny;
	}

	public void setActualBalCny(String actualBalCny) {
		this.actualBalCny = actualBalCny;
	}

	public String getActualBalUsd() {
		return actualBalUsd;
	}

	public void setActualBalUsd(String actualBalUsd) {
		this.actualBalUsd = actualBalUsd;
	}

	public String getAcctOpenDate() {
		return acctOpenDate;
	}

	public void setAcctOpenDate(String acctOpenDate) {
		this.acctOpenDate = acctOpenDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
}
