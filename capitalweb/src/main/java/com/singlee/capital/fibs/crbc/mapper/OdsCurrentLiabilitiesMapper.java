package com.singlee.capital.fibs.crbc.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.fibs.crbc.model.OdsCurrentLiabilities;

/**
 * @description TODO
 * @author wuyc
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface OdsCurrentLiabilitiesMapper extends Mapper<OdsCurrentLiabilities> {

	/**
	 * @author wuyc
	 */
	public List<Map<String,Object>> selectLiabilities();
	
}