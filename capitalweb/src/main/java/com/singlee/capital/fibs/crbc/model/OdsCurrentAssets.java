package com.singlee.capital.fibs.crbc.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ODS_CURRENT_ASSETS")
public class OdsCurrentAssets implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 活期资产ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_ODS_ASSETS.NEXTVAL FROM DUAL")
	private String assetsId;
	
	/**
	 * 开户行
	 */
	private String branch;
	
	/**
	 *科目号
	 */
	private String glCode;
	
	/**
	 * 名称
	 */
	private String glCodeDesc;
	
	/**
	 * 账户描述
	 */
	private String acctDesc;
	
	/**
	 *货币
	 */
	private String ccy;
	
	/**
	 *可用余额
	 */
	private String actualBal;
	
	/**
	 *实际可用余额
	 */
	private String actualBalCny;
	
	/**
	 *实际可用余额(美元)
	 */
	private String actualBalUsd;
	
	/**
	 * 当前日期
	 */
	private String currentDate;

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getAssetsId() {
		return assetsId;
	}

	public void setAssetsId(String assetsId) {
		this.assetsId = assetsId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	public String getGlCodeDesc() {
		return glCodeDesc;
	}

	public void setGlCodeDesc(String glCodeDesc) {
		this.glCodeDesc = glCodeDesc;
	}

	public String getAcctDesc() {
		return acctDesc;
	}

	public void setAcctDesc(String acctDesc) {
		this.acctDesc = acctDesc;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getActualBal() {
		return actualBal;
	}

	public void setActualBal(String actualBal) {
		this.actualBal = actualBal;
	}

	public String getActualBalCny() {
		return actualBalCny;
	}

	public void setActualBalCny(String actualBalCny) {
		this.actualBalCny = actualBalCny;
	}

	public String getActualBalUsd() {
		return actualBalUsd;
	}

	public void setActualBalUsd(String actualBalUsd) {
		this.actualBalUsd = actualBalUsd;
	}

	
}
