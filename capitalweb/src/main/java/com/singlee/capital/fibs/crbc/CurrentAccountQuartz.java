package com.singlee.capital.fibs.crbc;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.fibs.crbc.service.CurrentAccountQuartzService;

/**
 * 活期账户定时任务
 * @author WuYc
 * 调度 时间 在spring 中配置指定，
 */
public class CurrentAccountQuartz implements CronRunnable{
	
	private CurrentAccountQuartzService currentAccountQuartzService = SpringContextHolder.getBean("currentAccountQuartzService");
	
	
	//每天自动备份活期账户数据
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		//活期资产定时
		currentAccountQuartzService.executeAssets();
		//活期负债定时
		currentAccountQuartzService.executeLiabilities();
		return true;
	}

	@Override
	public void terminate() {}
	
}
