package com.singlee.capital.fibs.crbc.service;




public interface FtpPricingService {
	

	public double fixingFtpPrice(String value_date, int term_days);
	
	public void ftpImport();

	public void ftpRecalc();
	
}
