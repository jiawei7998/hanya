package com.singlee.capital.fibs.crbc.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.fibs.crbc.model.TtFtpCurve;
import com.singlee.capital.fibs.crbc.model.TtFtpVirtual;

/**
 * @projectName ftp定价
 * @description TODO
 * @author wuyc
 * @createDate 2016-11-09
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TtFtpCurveMapper extends Mapper<TtFtpCurve> {

	/**
	 * 根据起息日查询对象
	 * 
	 * @param vdate - 起息日
	 * @author wuyc
	 * @date 2016-11-09
	 */

	public List<TtFtpCurve> listMaxFtpCurve(String value_date);
	
	
	/**
	 * <!-- 把临时表的数据写入tt_ftp_curve表中 -->
	 * @return
	 */
	public int updateOrInsertFtpCurve();
	
	/**
	 * <!-- 把临时表的数据写入tt_ftp_curve表中 -->
	 */
	public int updateOrInsertFtpProfit();


	public void deleteFtpCurve();


	public void deleteFtpProfit();

	/**
	 * 查询所有的卖断虚拟记账的数据
	 * @return 
	 */
	public List<Map<String,Object>> listFtpMultiresale();
	
	/**
	 * 插入银行虚拟利润表
	 * @return 
	 */
	public void insertVirtual(TtFtpVirtual obj);
	
	/**
	 * 删除银行虚拟利润表
	 * @return 
	 */
	public void deleteVirtual();
	
}