package com.singlee.capital.fibs.crbc.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TT_FTP_CURVE")
public class TtFtpCurve implements Serializable, Comparable<TtFtpCurve>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 基准日
	 */
	private String ref_date;
	
	/**
	 * 起息日
	 */
	private String value_date;
	
	/**
	 * 期限
	 */
	private int term_days;
	
	/**
	 * 利率
	 */
	private double ref_rate;
	
	/**
	 * 流水主键
	 */
	@Id
	private String id;
	
	/**
	 * 更新日期
	 */
	private String update_time;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public String getValue_date() {
		return value_date;
	}

	public void setValue_date(String value_date) {
		this.value_date = value_date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public int getTerm_days() {
		return term_days;
	}

	public void setTerm_days(int term_days) {
		this.term_days = term_days;
	}

	public double getRef_rate() {
		return ref_rate;
	}

	public void setRef_rate(double ref_rate) {
		this.ref_rate = ref_rate;
	}

	@Override
	public int compareTo(TtFtpCurve o) {
		int d = this.value_date.compareTo(o.getValue_date());
		if(d == 0){
			return this.term_days>o.getTerm_days()?1:-1;
		}
		return d;
	}
	
}
