package com.singlee.capital.fibs.crbc.controller;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fibs.crbc.service.FtpPricingService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/FtpPricingController")
public class FtpPricingController extends CommonController {
	@Autowired
	private FtpPricingService ftpPricingService;
	
	/**
	 * 曲线数据
	 * @return
	 * @throws Exception
	 */
	 @RequestMapping(value = "/getCurveData")
	 @ResponseBody
	public double getFtpPrice(@RequestBody Map<String,String> paramMap){
		String value_date = ParameterUtil.getString(paramMap, "value_date", "");
		int term_days = Integer.parseInt(ParameterUtil.getString(paramMap, "term_days", ""));
		double d = ftpPricingService.fixingFtpPrice(value_date,term_days);
		return d;
	}
	 
}
