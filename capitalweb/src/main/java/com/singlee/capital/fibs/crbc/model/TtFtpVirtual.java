package com.singlee.capital.fibs.crbc.model;

import java.io.Serializable;

public class TtFtpVirtual implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 卖断日期
	 */
	private String res_date;
	
	/**
	 * 到期日期
	 */
	private String m_date;
	
	/**
	 *审批单号
	 */
	private String deal_no;
	
	/**
	 *日利润
	 */
	private double sum_day;
	
	/**
	 *累计利润
	 */
	private double profit_sum;
	
	/**
	 * 参考日期
	 */
	private String ref_date;
	
	/**
	 * 更新日期
	 */
	private String last_update;

	public String getRes_date() {
		return res_date;
	}

	public void setRes_date(String res_date) {
		this.res_date = res_date;
	}

	public String getM_date() {
		return m_date;
	}

	public void setM_date(String m_date) {
		this.m_date = m_date;
	}

	public String getDeal_no() {
		return deal_no;
	}

	public void setDeal_no(String deal_no) {
		this.deal_no = deal_no;
	}

	public double getSum_day() {
		return sum_day;
	}

	public void setSum_day(double sum_day) {
		this.sum_day = sum_day;
	}

	public double getProfit_sum() {
		return profit_sum;
	}

	public void setProfit_sum(double profit_sum) {
		this.profit_sum = profit_sum;
	}

	public String getLast_update() {
		return last_update;
	}

	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public String getRef_date() {
		return ref_date;
	}
	
}
