package com.singlee.capital.common.logger;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class LogConstants {

	/**
	 * 接口配置关系
	 */
	public final static Map<String, Object> INTERFACE_CONFIG_MAP = new HashMap<String, Object>();

	/**
	 * 
	 */
	public final static class LogAdcice{
		//登录
		public final static String  LOGIN = "0001";
		//机构修改
		public final static String  INST_UPDATE = "0002";
		//用户修改
		public final static String  USER_UPDATE = "0003";
		//角色修改
		public final static String  ROLE_UPDATE = "0004";
	}
}
