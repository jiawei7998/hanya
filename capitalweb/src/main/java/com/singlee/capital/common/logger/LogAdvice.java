package com.singlee.capital.common.logger;

import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.log.LogHandler;
import com.singlee.capital.common.log.MethodParam;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.model.TiInterfaceInfo;
import com.singlee.capital.system.mapper.TaLogMapper;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.system.service.LogFactory;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;




@Aspect
public class LogAdvice {
	
	@Autowired
	private LogHandler logHandler;
	@Autowired
	private TaLogMapper taLogMapper;
	
	@Pointcut("@annotation(com.singlee.capital.common.log.AutoLogMethod)")  
    public void autoLog() {  
    }
	
	@Around(value = "autoLog()")
	public Object aroundLog(ProceedingJoinPoint pjp) throws Throwable {  
		Field filed = pjp.getClass().getDeclaredField("methodInvocation");
		filed.setAccessible(true);
		Object methodInvocation = (Object)filed.get(pjp);
		Method method=methodInvocation.getClass().getMethod("getMethod");
		Method m=(Method)method.invoke(methodInvocation);
		String mn = pjp.getTarget().getClass().getName() + "."+ m.getName();
		Annotation[][] annotation = m.getParameterAnnotations();
		AutoLogMethod autoLogMethod = m.getAnnotation(AutoLogMethod.class);
		StringBuffer sb = new StringBuffer();Object requestParamsObject = null;
		for(int i=0;i<pjp.getArgs().length;i++){
			String paramName = "";
			for(Annotation a:annotation[i]){
				if(a.annotationType() == MethodParam.class){
					paramName = ((MethodParam)a).value();
					break;
				}
			}
			if(pjp.getArgs().length==1){//单个参数的时候可以进行初步处理
				requestParamsObject = pjp.getArgs()[i]==null?null:pjp.getArgs()[i];}
			sb.append("[arg"+i+"]["+paramName+"]: "+(pjp.getArgs()[i]==null?null:pjp.getArgs()[i].toString()));
		}
		JY.info("%s --> %s %s", mn, autoLogMethod.value(), sb.toString());
		
		try{
			Object retVal = pjp.proceed();
			if(null != LogConstants.INTERFACE_CONFIG_MAP.get(autoLogMethod.value()))
			{
				Object valueObject = LogConstants.INTERFACE_CONFIG_MAP.get(autoLogMethod.value());
				TiInterfaceInfo tiInterfaceInfo = null;
				if(valueObject instanceof TiInterfaceInfo)
				{
					SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
					if(StringUtils.isNotEmpty(SlSessionHelper.getUa(session))){
						TaLog log = LogFactory.getInstance().create(session, mn,  sb.toString(), mn);
//					    log.setReqMethod(tiInterfaceInfo.getInfaceName());
						log.setReqDesc(autoLogMethod.value());
						log.setRspCode(autoLogMethod.id());
						log.setRspDesc(autoLogMethod.value());
						LogAdviceUtil.getdealResult(autoLogMethod,pjp.getArgs(),log);
						//针对每个方法处理
						taLogMapper.insert(log);
					}

				}
				
			}
			logHandler.logOk(autoLogMethod.value(), sb.toString(), mn, retVal);
			return retVal;
		}catch(Throwable t){
			logHandler.logError(autoLogMethod.value(), sb.toString(), mn, t);
			throw t;
		}
	}
}
