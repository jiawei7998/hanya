package com.singlee.capital.common.logger;

import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

import java.util.Map;

public class LogAdviceUtil {


    public static void getdealResult(AutoLogMethod autoLogMethod, Object[] args, TaLog log) {

        if(LogConstants.LogAdcice.INST_UPDATE.equals(autoLogMethod.id())){//机构更改
            instUpdate(autoLogMethod,args,log);
        }else if(LogConstants.LogAdcice.USER_UPDATE.equals(autoLogMethod.id())){//用户修改
            userUpdate(autoLogMethod,args,log);
        }else if(LogConstants.LogAdcice.ROLE_UPDATE.equals(autoLogMethod.id())){//用户修改
            roleUpdate(autoLogMethod,args,log);
        }
    }



    /**
     * 保存用户变更记录
     * @param args
     */
    private static void userUpdate(AutoLogMethod autoLogMethod,Object[] args, TaLog log) {
        StringBuffer sb = new StringBuffer();
        if(args.length!=2){
            sb.append("该用户无操作");
        }else {
            Map<String, Object> postObj = (Map<String, Object>) args[0];
            TaUser oldUser = (TaUser) args[1];
            log.setRspDetail(autoLogMethod.value()+"|"+oldUser.getUserId()+"|"+oldUser.getUserName());
            if (oldUser != null) {
                //用户名称
                if (oldUser.getUserName() != null && !oldUser.getUserName().equals(postObj.get("userName"))) {
                    sb.append("<span style=\"font-weight:bold\"> 所属机构</span>：<span style=\"color:red\">原</span>："+
                        oldUser.getUserName()+"   <span style=\"color:red\">变更后</span>："+postObj.get("userName")+"<br>");
                }
                //交易员编号(OPICS)
                if (oldUser.getOpicsTrad() != null && !oldUser.getOpicsTrad().equals(postObj.get("opicsTrad"))) {
                    sb.append("<span style=\"font-weight:bold\"> 交易员编号(OPICS)</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getOpicsTrad()+"   <span style=\"color:red\">变更后</span>："+postObj.get("opicsTrad")+"<br>");
                }
                //交易员编号(CFETS)
                if (oldUser.getCfetsTrad() != null && !oldUser.getCfetsTrad().equals(postObj.get("cfetsTrad"))) {
                    sb.append("<span style=\"font-weight:bold\"> 交易员编号(CFETS)</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getCfetsTrad()+"   <span style=\"color:red\">变更后</span>："+postObj.get("cfetsTrad")+"<br>");
                }
                //用户状态
                if (oldUser.getIsActive() != null && !oldUser.getIsActive().equals(postObj.get("isActive"))) {
                    sb.append("<span style=\"font-weight:bold\"> 用户状态</span>：<span style=\"color:red\">原</span>："+
                            typeStatus(oldUser.getIsActive())+"   <span style=\"color:red\">变更后</span>："+typeStatus(postObj.get("isActive").toString())+"<br>");
                }
                //用户所属OPICS部门
                if (oldUser.getOpicsBr() != null && !oldUser.getOpicsBr().equals(postObj.get("opicsBr"))) {
                    sb.append("<span style=\"font-weight:bold\"> 用户所属OPICS部门</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getOpicsBr()+"   <span style=\"color:red\">变更后</span>："+postObj.get("opicsBr")+"<br>");
                }
                //所属机构
                if (oldUser.getInstId() != null && !oldUser.getInstId().equals(postObj.get("instId"))) {
                    sb.append("<span style=\"font-weight:bold\"> 所属机构</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getInstName()+"   <span style=\"color:red\">变更后</span>："+postObj.get("instName")+"<br>");
                }
                //用户角色
                if (oldUser.getRoles() != null && !oldUser.getUserName().equals(postObj.get("roles"))) {
                    sb.append("<span style=\"font-weight:bold\"> 用户角色</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getRoleNames()+"   <span style=\"color:red\">变更后</span>："+postObj.get("roleNames")+"<br>");
                }
                //联系方式
                if (oldUser.getUserFixedphone() != null && !oldUser.getUserFixedphone().equals(postObj.get("userFixedphone"))) {
                    sb.append("<span style=\"font-weight:bold\"> 联系方式</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getUserFixedphone()+"   <span style=\"color:red\">变更后</span>："+postObj.get("userFixedphone")+"<br>");
                }
                //备注
                if (oldUser.getUserMemo() != null && !oldUser.getUserMemo().equals(postObj.get("userMemo"))) {
                    sb.append("<span style=\"font-weight:bold\"> 备注</span>：<span style=\"color:red\">原</span>："+
                            oldUser.getUserMemo()+"   <span style=\"color:red\">变更后</span>："+postObj.get("userMemo")+"<br>");
                }
            }else {
                sb.append("未查询到修改前的数据");
            }
            log.setRspObject(sb.toString());
        }
    }

    /**
     * 保存机构变更记录
     * @param args
     */
    private static void instUpdate(AutoLogMethod autoLogMethod,Object[] args,TaLog log) {

        StringBuffer sb = new StringBuffer();
        if(args.length!=2){
            sb.append("该用户无操作");
        }else {
            TtInstitution newInst = (TtInstitution) args[0];
            TtInstitution oldInst = (TtInstitution) args[1];
            log.setRspDetail(autoLogMethod.value()+"|"+newInst.getInstId()+"|"+newInst.getInstFullname());
            if (oldInst != null) {
                if(!oldInst.getInstFullname().equals(newInst.getInstFullname())){
                    sb.append("<span style=\"font-weight:bold\"> 机构全称</span>：<span style=\"color:red\">原</span>："+
                            oldInst.getInstFullname()+"   <span style=\"color:red\">变更后</span>："+newInst.getInstFullname()+"<br>");
                }
                if(!oldInst.getInstName().equals(newInst.getInstName())){
                    sb.append("<span style=\"font-weight:bold\"> 机构简称</span>：<span style=\"color:red\">原</span>："+
                            oldInst.getInstName()+"   <span style=\"color:red\">变更后</span>："+newInst.getInstName()+"<br>");
                }
                if(!oldInst.getpInstName().equals(newInst.getpInstName())){
                    sb.append("<span style=\"font-weight:bold\"> 上级机构</span>：<span style=\"color:red\">原</span>："+
                            oldInst.getpInstName()+"    <span style=\"color:red\">变更后</span>："+newInst.getpInstName()+"<br>");
                }
                if(!oldInst.getInstType().equals(newInst.getInstType())){
                    sb.append("<span style=\"font-weight:bold\"> 机构类型</span>：<span style=\"color:red\">原</span>："+
                            typeTrans(oldInst.getInstType())+"    <span style=\"color:red\">变更后</span>："+typeTrans(newInst.getInstType())+"<br>");
                }
                if(!oldInst.getInstStatus().equals(newInst.getInstStatus())){
                    sb.append("<span style=\"font-weight:bold\"> 机构状态</span>：<span style=\"color:red\">原</span>："+
                            typeInstStatus(oldInst.getInstStatus())+"    <span style=\"color:red\">变更后</span>："+typeInstStatus(newInst.getInstStatus())+"<br>");
                }
                if(!oldInst.getThirdPartyId().equals(newInst.getThirdPartyId())){
                    sb.append("<span style=\"font-weight:bold\"> 交易映射机构[OPICS]</span>：<span style=\"color:red\">原</span>："+
                            oldInst.getThirdPartyId()+"    <span style=\"color:red\">变更后</span>："+newInst.getThirdPartyId()+"<br>");
                }
            } else {
                sb.append("未查询到修改前的数据");
            }
            log.setRspObject(sb.toString());
        }
    }

    /**
     * 保存角色变更记录
     * @param autoLogMethod
     * @param args
     * @param log
     */
    private static void roleUpdate(AutoLogMethod autoLogMethod, Object[] args, TaLog log) {
        StringBuffer sb = new StringBuffer();
        if(args.length!=2){
            sb.append("该用户无操作");
        }else {
            TaRole newRole = (TaRole) args[0];
            TaRole oldRole = (TaRole) args[1];
            log.setRspDetail(autoLogMethod.value()+"|"+newRole.getRoleId());
            if (newRole != null && oldRole != null) {
                //角色名称
                if (newRole.getRoleName() != null && !newRole.getRoleName().equals(oldRole.getRoleName())) {
                    sb.append("<span style=\"font-weight:bold\"> 角色名称</span>：<span style=\"color:red\">原</span>："+
                            oldRole.getRoleName()+"   <span style=\"color:red\">变更后</span>："+newRole.getRoleName()+"<br>");
                }
                //所属机构
                if (newRole.getInstId() != null && !newRole.getInstId().equals(oldRole.getInstId())) {
                    sb.append("<span style=\"font-weight:bold\"> 所属机构</span>：<span style=\"color:red\">原</span>："+
                            oldRole.getInstName()+"   <span style=\"color:red\">变更后</span>："+newRole.getInstName()+"<br>");
                }
                //是否启用
                if (newRole.getIsActive() != null && !newRole.getIsActive().equals(oldRole.getIsActive())) {
                    sb.append("<span style=\"font-weight:bold\"> 是否启用</span>：<span style=\"color:red\">原</span>："+
                            typeActive(oldRole.getIsActive())+"   <span style=\"color:red\">变更后</span>："+typeActive(newRole.getIsActive())+"<br>");
                }

            }else{
                sb.append("未查询到修改前的数据");
            }
        }
        log.setRspObject(sb.toString());
    }

    /**
     * 类型转化
     * @param InstType
     * @return
     */
    public static String typeTrans(String InstType){
        if("0".equals(InstType)){
            return "总行";
        }else if("1".equals(InstType)){
            return "分行";
        }else if("2".equals(InstType)){
            return "支行";
        }else if("9".equals(InstType)){
            return "部门";
        }
        return InstType;
    }

    public static String typeInstStatus(String instStatus){
        if("2".equals(instStatus)){
            return "启用";
        }else if("3".equals(instStatus)){
            return "停用";
        }
        return instStatus;
    }

    public static String typeActive(String isActive){
        if("0".equals(isActive)){
            return "否";
        }else if("1".equals(isActive)){
            return "是";
        }
        return isActive;
    }

    public static String typeStatus(String isActive){
        if("0".equals(isActive)){
            return "注销";
        }else if("1".equals(isActive)){
            return "正常";
        }
        return isActive;
    }
}
