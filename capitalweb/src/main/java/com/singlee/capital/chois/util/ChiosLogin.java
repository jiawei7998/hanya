package com.singlee.capital.chois.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.configuration.PropertiesConfiguration;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.adpt.util.EAIException;
import com.hanafn.eai.client.stdtmsg.StdTMsg;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.hanafn.eai.client.stdtmsg.StdTMsgValidator;
import com.hanafn.eai.client.stdtmsg.util.StdTMsgException;
import com.singlee.capital.common.util.PropertiesUtil;


public class ChiosLogin {
	
	public Date date =new Date();
	public SimpleDateFormat sdfdate = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
	public String msgTypeCode=null;//标准报文数据种类代码
	public String  msgTypeCodeLen;//标准报文数据长度
	public int  msgTypeCodeLen1;//标准报文数据长度
	public String sysName;//系统姓名
	public String handleCode;//处理代码
	public String channelType;//渠道类型
	public String channelId;//渠道ID
	public String gainInst;//获取机构
	public String cdate;//日期
	public String ctime;//时间
	public String keyTrade;//KEY1, 交易
	public String keyTradeNo;//KEY2 , 参考号
	public String serveHandleCode;//服务处理代码
	
	/**柜员登录***/
	public String tradeType;//交易类型
	public String name;//用户名
	public String pwd;//密码
	public String dist;//区分
	public String pwdLock;//密码锁定
	
	public String main() throws Exception {
		
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		//EAI接口ID
		String intfId = "OPS_CHS_DSS00001";

		String remsg=sendLogin(eaiDv, intfId);
		
		return remsg;

	}
	
	public String sendLogin(String eaiDv, String intfId) {
		byte[] returnData = null;
		String remsg = null;
		String sess=null;
		try {
			//String serviceUrl = "172.28.65.30";
			//最初发送系统代码
			String serviceUrl = "OPS";
			byte[] testData = makeLoginRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			System.out.println("eaiDv:--"+eaiDv);
			returnData = httpCilent.sendSychMsg(eaiDv, testData, serviceUrl);
			System.out.println("login返回信息："+new String(returnData, "UTF-8"));
			int len=(new String(returnData, "UTF-8")).length();
			 remsg=(new String(returnData, "UTF-8")).substring(len-220,len-218);
			System.out.println("remsg："+remsg);
			if(remsg.equals("10")) {
				 sess=(new String(returnData, "UTF-8")).substring(len-28,len-2);
				System.out.println("返回session："+sess);
				
			}else {
				String errmsg=(new String(returnData, "UTF-8")).substring(len-595,len-593);
				System.out.println("报文有误："+errmsg);
			}
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  sess;
	}
	
	public byte[] makeLoginRequstStdMsg(String intfId) throws StdTMsgException, Exception {
		
		
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		msg.setEaiIntfId(intfId);
		msg.setTrscSyncDvCd("S");
		msg.setStdTmsgPrgrNo("01");//转发系统节点代码
		msg.setRecvSvcCd("EIO1100");//接收服务代码
		msg.setExtnCrctNo("OPCS");//外部线路号
		
		PropertiesConfiguration stdtmsgSysValue;
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		msgTypeCode=stdtmsgSysValue.getString("msgTypeCode");
		msgTypeCodeLen=stdtmsgSysValue.getString("msgTypeCodeLen");
		msgTypeCodeLen1=stdtmsgSysValue.getInt("msgTypeCodeLen1");
		sysName=stdtmsgSysValue.getString("sysName");
		handleCode=stdtmsgSysValue.getString("handleCode");
		channelType=stdtmsgSysValue.getString("channelType");
		channelId=stdtmsgSysValue.getString("channelId");
		gainInst=stdtmsgSysValue.getString("gainInst");
		cdate=sdfdate.format(date);
		ctime=sdftimes.format(date);
		keyTrade=stdtmsgSysValue.getString("keyTrade");
		keyTradeNo=stdtmsgSysValue.getString("keyTradeNo");
		serveHandleCode=stdtmsgSysValue.getString("serveHandleCode");
		tradeType=stdtmsgSysValue.getString("tradeType");
		name=stdtmsgSysValue.getString("name");
		pwd=stdtmsgSysValue.getString("pwd");
		dist=stdtmsgSysValue.getString("dist");
		pwdLock=stdtmsgSysValue.getString("pwdLock");
		byte[] headerPacket = generator.makeHeader(msg);
		byte[] dataPacket=(msgTypeCode+msgTypeCodeLen+msgTypeCodeLen1+sysName+handleCode+channelType+channelId+gainInst+"                  "+cdate+ctime+keyTrade+keyTradeNo+"  "+serveHandleCode+tradeType+name+pwd+"       "+dist+pwdLock).getBytes();
		byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
		return totalPacket;
	}
	
	public void sendSychMsg(String eaiDv, String intfId) {

		try {
			String serviceUrl = "OPS";
			byte[] testData = makeSampleRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, testData, serviceUrl);
			System.out.println(new String(returnData, "UTF-8"));
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendAsynMsg(String eaiDv, String intfId) {

		try {
			String serviceUrl = "OPS";
			byte[] testData = makeSampleRequstStdMsg(intfId);
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			httpCilent.sendAsychMsg(eaiDv, testData, serviceUrl);
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		}
	}

	public void sendHealthCheckMsg(String eaiDv) {

		try {
			String serviceUrl = "OPS";
			byte[] reqData = makeSampleReqHealthCheckMsg();
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			byte[] result = httpCilent.sendHealthCheckMsg(eaiDv, reqData, serviceUrl);
			System.out.println(">>> Health Checking:"+new String(result, "UTF-8"));
		} catch (EAIException eaiExp) {
			eaiExp.printStackTrace();
			eaiExp.getMessage();
			Throwable cause = eaiExp.getCause();
			if (cause != null) {
				cause.printStackTrace();
			}
		} catch (StdTMsgException stdTMsgExp) {
			stdTMsgExp.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte[] makeSampleRequstStdMsg(String intfId) throws StdTMsgException {
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		msg.setEaiIntfId(intfId);
		byte[] headerPacket = generator.makeHeader(msg);
		byte[] dataPacket = "Dummy Data".getBytes();
		byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
		return totalPacket;

	}

	public byte[] makeSampleReqHealthCheckMsg() throws StdTMsgException {
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		new StdTMsgValidator().validateAll(msg);
		byte[] packet = generator.makeReqHealthCheckMsg(msg);
		return packet;
	}
}
