package com.singlee.capital.chois.util;

public class ChoisCommonBean {
	private String FXFIG_CJUM;
	private String FXFIG_AJUM;
	private String FXFIG_SJUM;
	private String FXFIG_OPNO;
	private String FXFIG_OPGB;
	private String FXFIG_IBIL;
	private String FXFIG_AMOD;
	private String FXFIG_TELL;
	private String FXFIG_SSID;
	private String FXFIG_SSIL;
	private String FXFIG_TELLNM;
	private String FXFIG_THGB;
	private String FXFIG_TYPE;
	private String FXFIG_JUM_CD;
	private String FXFIG_CCY;
	private String FXFIG_SSCK;
	public String getFXFIG_CJUM() {
		return FXFIG_CJUM;
	}
	public void setFXFIG_CJUM(String fXFIG_CJUM) {
		FXFIG_CJUM = fXFIG_CJUM;
	}
	public String getFXFIG_AJUM() {
		return FXFIG_AJUM;
	}
	public void setFXFIG_AJUM(String fXFIG_AJUM) {
		FXFIG_AJUM = fXFIG_AJUM;
	}
	public String getFXFIG_SJUM() {
		return FXFIG_SJUM;
	}
	public void setFXFIG_SJUM(String fXFIG_SJUM) {
		FXFIG_SJUM = fXFIG_SJUM;
	}
	public String getFXFIG_OPNO() {
		return FXFIG_OPNO;
	}
	public void setFXFIG_OPNO(String fXFIG_OPNO) {
		FXFIG_OPNO = fXFIG_OPNO;
	}
	public String getFXFIG_OPGB() {
		return FXFIG_OPGB;
	}
	public void setFXFIG_OPGB(String fXFIG_OPGB) {
		FXFIG_OPGB = fXFIG_OPGB;
	}
	public String getFXFIG_IBIL() {
		return FXFIG_IBIL;
	}
	public void setFXFIG_IBIL(String fXFIG_IBIL) {
		FXFIG_IBIL = fXFIG_IBIL;
	}
	public String getFXFIG_AMOD() {
		return FXFIG_AMOD;
	}
	public void setFXFIG_AMOD(String fXFIG_AMOD) {
		FXFIG_AMOD = fXFIG_AMOD;
	}
	public String getFXFIG_TELL() {
		return FXFIG_TELL;
	}
	public void setFXFIG_TELL(String fXFIG_TELL) {
		FXFIG_TELL = fXFIG_TELL;
	}
	public String getFXFIG_SSID() {
		return FXFIG_SSID;
	}
	public void setFXFIG_SSID(String fXFIG_SSID) {
		FXFIG_SSID = fXFIG_SSID;
	}
	public String getFXFIG_SSIL() {
		return FXFIG_SSIL;
	}
	public void setFXFIG_SSIL(String fXFIG_SSIL) {
		FXFIG_SSIL = fXFIG_SSIL;
	}
	public String getFXFIG_TELLNM() {
		return FXFIG_TELLNM;
	}
	public void setFXFIG_TELLNM(String fXFIG_TELLNM) {
		FXFIG_TELLNM = fXFIG_TELLNM;
	}
	public String getFXFIG_THGB() {
		return FXFIG_THGB;
	}
	public void setFXFIG_THGB(String fXFIG_THGB) {
		FXFIG_THGB = fXFIG_THGB;
	}
	public String getFXFIG_TYPE() {
		return FXFIG_TYPE;
	}
	public void setFXFIG_TYPE(String fXFIG_TYPE) {
		FXFIG_TYPE = fXFIG_TYPE;
	}
	public String getFXFIG_JUM_CD() {
		return FXFIG_JUM_CD;
	}
	public void setFXFIG_JUM_CD(String fXFIG_JUM_CD) {
		FXFIG_JUM_CD = fXFIG_JUM_CD;
	}
	public String getFXFIG_CCY() {
		return FXFIG_CCY;
	}
	public void setFXFIG_CCY(String fXFIG_CCY) {
		FXFIG_CCY = fXFIG_CCY;
	}
	public String getFXFIG_SSCK() {
		return FXFIG_SSCK;
	}
	public void setFXFIG_SSCK(String fXFIG_SSCK) {
		FXFIG_SSCK = fXFIG_SSCK;
	}
	
	
}
