package com.singlee.capital.chois.util;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

public class StringUtil {
	
	//工具方法传输字段类型字符串args，字符串S或数字N类型type，总长度num,小数点后位数m
		public String subStrOrNumber(String args,int num,int m) {
			String reslut = null;
			if(args.length()>0 || args==null || args.equals("")) {
				if(args != null) {
					if(num < 0) {
						throw new RuntimeException("补零后的总长度不能为负数");
					}
					if(num < m) {
						throw new RuntimeException("补零后的总长度不能小于小数点后的位数");
					}
					if(num < args.length()) {
						//throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
						args=args.substring(0, num);
					}
					//第一步，判断数值为正负
					if(args.contains("-")) {
						String fh = "-";
						//判断数值是否为整数，即小数点后是否有值
						if(args.contains(".")) {
							//截取小数点前的字符
							String args1 = args.substring(args.indexOf("-")+1, args.indexOf("."));
							//字符串小数点前左补0
							String args11 = StringUtils.leftPad(args1, num-m-1, "0");
							//截取小数点后的字符
							String args2 = args.substring(args.indexOf(".")+1,args.length());
							String args21 = null;
							if(args2.length() > m) {
								//字符串小数点后大于m则截取
								args21 = args2.substring(0, m);
							}else {
								//字符串小数点后右补0
								args21 = StringUtils.rightPad(args2, m, "0");
							}
							
							//结果reslut	
							reslut = fh+args11+args21;
						}else{
							//整数值时，先左补0
							String args1 = args.substring(args.indexOf("-")+1, args.length());
							
							String args11 = StringUtils.leftPad(args1, num-m-1, "0");
							//根据左补0后的值再右补0
							String args2 = StringUtils.rightPad(args11, num-1, "0");
							//结果reslut
							reslut = fh+args2;
						}
					}else {
						//判断数值是否为整数，即小数点后是否有值
						if(args.contains(".")) {
							//截取小数点前的字符
							String args1 = args.substring(0, args.indexOf("."));
							//字符串小数点前左补0
							String args11 = StringUtils.leftPad(args1, num-m, "0");
							//截取小数点后的字符
							String args2 = args.substring(args.indexOf(".")+1,args.length());
							String args21 = null;
							if(args2.length() > m) {
								//字符串小数点后大于m则截取
								args21 = args2.substring(0, m);
							}else {
								//字符串小数点后右补0
								args21 = StringUtils.rightPad(args2, m, "0");
							}
							//字符串小数点后右补0
	//						String args21 = StringUtils.rightPad(args2, m, "0");
							//结果reslut	
							reslut = args11+args21;
						}else{
							//整数值时，先左补0
							String args1 = StringUtils.leftPad(args, num-m, "0");
							//根据左补0后的值再右补0
							String args2 = StringUtils.rightPad(args1, num, "0");
							//结果reslut
							reslut = args2;
						}
					}			
				}else{
					reslut = StringUtils.rightPad("", num, "");
				}
				
			}
				
			return reslut;
		}
	//工具方法传输字段类型字符串args，总长度num
	public String subStrToString(String args,int num) {
		String reslut = null;
		if(args != null) {
			//判断字符串中是否含有中文（不含中文）
			if(args.length() == args.getBytes().length) {
				if(args.length() == num) {
					reslut = args;
				}else {
					if(num < 0) {
						throw new RuntimeException("补空后的总长度不能为负数");
					}
					if(num < args.length()) {
						throw new RuntimeException("所给字符串长度大于补空后字符串长度，补零失败");
					}
					reslut = StringUtils.rightPad(args, num, "");
				}
				
			}else {
				if(num < 0) {
					throw new RuntimeException("补空后的总长度不能为负数");
				}
				if(num < args.getBytes().length) {
					throw new RuntimeException("所给字符串长度大于补空后字符串长度，补零失败");
				}
				reslut = StringUtils.rightPad(args, num-args.length()-args.length(), "");
			}
			
		}else {
			reslut = StringUtils.rightPad("", num, "");
		}
		return reslut;
	}
	
	
	
	
	
			
	//工具方法18位
	public String subStr16Char(BigDecimal args) {
		String reslut = null;
		if(args != null) {
			//转换成字符串类型
			String args1 = String.valueOf(args);
			if(args1.indexOf(".")>0) {
				//截取小数点前的字符
				String args2 = args1.substring(0, args1.indexOf("."));
				//获取需要补齐的长度
				int args3 = 16-args1.length()+args2.length();
				//小数点前补齐后的数据
				String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
				//结果reslut	
				reslut = args4+args1.substring(args1.indexOf("."));
			}else{
				reslut = String.format("%016d", Integer.parseInt(args1));
			}
			
		}else {
			reslut = String.format("%016d", Integer.parseInt("0"));
		}

		return reslut;
	}
	
		


	//工具方法num位
	public String subStrChar(BigDecimal args,int num) {
		String reslut = null;
		if(args != null) {
			//转换成字符串类型
			String args1 = String.valueOf(args);
			if(args1.indexOf(".")>0) {
				//截取小数点前的字符
				String args2 = args1.substring(0, args1.indexOf("."));
				//获取需要补齐的长度
				int args3 = num-args1.length()+args2.length();
				//小数点前补齐后的数据
				String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
				//结果reslut	
				reslut = args4+args1.substring(args1.indexOf("."));
			}else{
				reslut = String.format("%0"+String.valueOf(num)+"d", Integer.parseInt(args1));
			}
			
		}else {
			reslut = String.format("%016d", Integer.parseInt("0"));
		}
		return reslut;
	}
				
	//num:金额，n：总长度，m:几位小数，left：true，right:true
	public   String fillZero(String num, int n,int m,boolean left,boolean right){
					String amt1=null;
					String amt2=null;
					String amt3=null;
					String fstr=null;
					String str=null;
					String result=null;
					 int rnum=0;		    
					int len=0;
					int zoreNum=0;
					if(num.contains("-")) {
						fstr=num.substring(0, 1);
						if(num.contains(".")) {
							 amt1=num.substring(0, num.indexOf("."));
							 amt3=amt1.substring(1, amt1.length());
							 amt2=num.substring(amt1.length()+1, num.length());
							 len=amt2.length();
							
							 zoreNum = n - amt3.length()-m-1;
						}else {
							amt3=num.substring(1, num.length());
							rnum=m;
							amt2="";
							zoreNum = n - amt3.length()-m-1;
						}
					  
					    if(n < 0){
					        throw new RuntimeException("补零后的长度不能为负数");
					    }
					    if(n < num.length()){
					        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
					    }
					    if(n == num.length()){
					        return num;
					    }
					   
					    if(m==len) {
					    	rnum=0;
					    }else if(m>len){
					    	rnum=m-len;
					    }else {
					    	str=amt2.substring(0,m);
					    	rnum=0;
					    	amt2=str;
					    }
					    StringBuilder sb1 = new StringBuilder("");
					    for(int i = 0; i < rnum; i++){
					        sb1.append("0");
					    }
					    if(right){
					    	 sb1.insert(0,amt2);
					      
					    }else{
					    	  sb1.append(amt2);
					    }
					    
					   
					    StringBuilder sb = new StringBuilder("");
					    for(int i = 0; i < zoreNum; i++){
					        sb.append("0");
					    }
					    if(left){
					        sb.append(amt3);
					    }else{
					        sb.insert(0,amt3);
					    }
					    result=fstr+sb.toString()+sb1.toString();
					}else {
						if(num.contains(".")) {
							 amt1=num.substring(0, num.indexOf("."));
							 
							 amt2=num.substring(amt1.length()+1, num.length());
							 len=amt2.length();
							  zoreNum = n - amt1.length()-m;
						}else {
							amt1=num;
							rnum=m;
							amt2="";
							zoreNum = n - amt1.length()-m;
						}
					  
					    if(n < 0){
					        throw new RuntimeException("补零后的长度不能为负数");
					    }
					    if(n < num.length()){
					        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
					    }
					    if(n == num.length()){
					        return num;
					    }
					   
					    if(m==len) {
					    	rnum=0;
					    }else if(m>len){
					    	rnum=m-len;
					    }else {
					    	str=amt2.substring(0,m);
					    	rnum=0;
					    	amt2=str;
					    }
					    StringBuilder sb1 = new StringBuilder("");
					    for(int i = 0; i < rnum; i++){
					        sb1.append("0");
					    }
					    if(right){
					    	 sb1.insert(0,amt2);
					      
					    }else{
					    	  sb1.append(amt2);
					    }
					    
					  
					    StringBuilder sb = new StringBuilder("");
					    for(int i = 0; i < zoreNum; i++){
					        sb.append("0");
					    }
					    if(left){
					        sb.append(amt1);
					    }else{
					        sb.insert(0,amt1);
					    }
					    result=sb.toString()+sb1.toString();
					}
					
					
				    return result;
				 
				}

}
