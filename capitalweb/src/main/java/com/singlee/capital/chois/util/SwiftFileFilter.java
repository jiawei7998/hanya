package com.singlee.capital.chois.util;

import java.io.File;
import java.io.FileFilter;

import org.springframework.stereotype.Service;
@Service
public class SwiftFileFilter implements FileFilter{

	private String filterFileName;
	/**
	 * @return the filterFileName
	 */
	public String getFilterFileName() {
		return filterFileName;
	}

	/**
	 * @param filterFileName the filterFileName to set
	 */
	public void setFilterFileName(String filterFileName) {
		this.filterFileName = filterFileName;
	}

	public static void main(String[] args) {
		SwiftFileFilter swiftFileFilter = new SwiftFileFilter();
		swiftFileFilter.setFilterFileName("imtf9.b01");
		File file = new File("C:/SWIFT");
		File[] fileList = file.listFiles();
		for(int i=0;i<fileList.length;i++)
			
			System.out.println(fileList[i].getName());
		
	}

	@Override
	public boolean accept(File file) {
		// TODO Auto-generated method stub
		if(file.getName().equalsIgnoreCase(filterFileName))
			return false;
		else
			return true;
	}
}
