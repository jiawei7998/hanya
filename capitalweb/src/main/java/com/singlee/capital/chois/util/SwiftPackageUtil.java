package com.singlee.capital.chois.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class SwiftPackageUtil {

	//private Logger logger = LogManager.getLogger(LogManager.MODEL_SWIFTLOG);

	public static Logger logger = LoggerFactory.getLogger("com");
	/**
	 * ��ȡĿ¼�ṹ�µ������ļ��� �ų�isNotFileName���ļ� fileFilter ��Ҫ�Ĺ���
	 * 
	 * @param path
	 * @param isNotFileName
	 * @param fileFilter
	 * @return
	 */
	public List<String> searchAllPathFileList(String path,
			String isNotFileName, FileFilter fileFilter) {
		List<String> fileList = new ArrayList<String>();
		File dirFile = new File(path);
		if (dirFile.exists()) {
			File[] fileArray = dirFile.listFiles(fileFilter);
			for (File fileName : fileArray) {
				if (fileName.isFile())
					fileList.add(fileName.getName());
			}
		}
		return fileList;
	}

	/**
	 * ���SWIFT����
	 * 
	 * @param filePathName
	 *            ������Ҫ��ֵ�ȫ·��
	 * @param splitAsscii
	 *            ��ֵ��ַ�ASSCII
	 * @param createSplitFileHeaderName
	 *            ��ֳ������ļ�ͷ����
	 * @param createSplitFilePath
	 *            ��ֳ������ļ���
	 * @param createSplitFileType
	 *            ��ֳ������ļ�����
	 * @return
	 */
	public boolean separatorFile(String filePathName, int splitAsscii,
			String createSplitFileHeaderName, String createSplitFilePath,
			String createSplitFileType) {
		logger.info("********************************BEGIN " + filePathName
				+ " SEPARATOR********************************");
		RandomAccessFile raf = null;
		FileOutputStream fos = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		List<String> separatorFileList = new ArrayList<String>();
		byte[] bt = new byte[1024];
		int len = 0;
		int count = 0;
		String fileName = "";
		try {
			raf = new RandomAccessFile(filePathName, "r");
			raf.seek(0);
			fileName = createSplitFileHeaderName
					+ simpleDateFormat.format(new Date())
					+ CreateSeq.getSwiftSeqFromProperties(
							"./config/swift.properties", "swiftSeq")
					+ (createSplitFileType == null ? "" : createSplitFileType);
			System.out.println("fileName:"+fileName);
			logger.info("FILE " + count + ":" + fileName);
			fos = new FileOutputStream(createSplitFilePath + fileName);
			separatorFileList.add(createSplitFilePath + fileName);
			while ((len = raf.read(bt)) > 0) {
				for (int i = 0; i < len; i++) {
					byte dollar = bt[i];

					if (dollar == splitAsscii) {
						Thread.sleep(100);
						fos.close();
						count++;
						fileName = createSplitFileHeaderName
								+ simpleDateFormat.format(new Date())
								+ CreateSeq.getSwiftSeqFromProperties(
										"./config/swift.properties",
										"swiftSeq") + (createSplitFileType == null ? ""
								: createSplitFileType);
						logger.info("FILE " + count + ":" + fileName);
						fos = new FileOutputStream(createSplitFilePath
								+ fileName);
						separatorFileList.add(createSplitFilePath + fileName);
					} else {
						fos.write(bt, i, 1);
					}

				}
			}
		} catch (Exception e) {
			logger.info("Exception:" + e.getMessage());
			try {
				if (fos != null)
					fos.close();
				if (raf != null)
					raf.close();
			} catch (Exception f) {
				f.printStackTrace();
			}
			deleteAllFiles(separatorFileList);
			return false;
		} finally {
			try {
				if (fos != null)
					fos.close();
				if (raf != null)
					raf.close();
			} catch (Exception f) {
				logger.info("Exception:" + f.getMessage());
			}
		}
		logger.info("********************************END " + filePathName
				+ " SEPARATOR********************************");
		return true;
	}

	public boolean checkFileIsNull(String filePathName) {
		File file = new File(filePathName);
		if (file.exists() && file.isFile() && file.length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean deleteAllFiles(List<String> fileList) {
		for (String fileNamePath : fileList) {
			File file = new File(fileNamePath);
			if (file.delete()) {
				logger.info("DELETE FILE FOR EXCEPTION:" + fileNamePath
						+ "SUCCESS.");
			} else {
				logger.info("DELETE FILE FOR EXCEPTION:" + fileNamePath
						+ "FAIL.");
				return false;
			}
		}
		return true;
	}

	public boolean moveOldSwiftFileToNewPath(String fileName, String oldPath,
			String newPath) {
		File oldFile = new File(oldPath + fileName);
		File fnewpath = new File(newPath);
		if (!fnewpath.exists())
			fnewpath.mkdirs();
		File fnew = new File(newPath + oldFile.getName() + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
		boolean moveOk = oldFile.renameTo(fnew);
		if (moveOk)
			logger.info(fileName + " MOVE TO NEW PATH:" + newPath + " SUCCESS["
					+ oldPath + "]");
		else {
			logger.info(fileName + " MOVE TO NEW PATH:" + newPath + " FAIL["
					+ oldPath + "]");
		}
		return moveOk;
	}
	
	public boolean moveOldSwiftFileToNewPathUpLoad(String fileName, String oldPath,
			String newPath) {
		File oldFile = new File(oldPath + fileName);
		File fnewpath = new File(newPath);
		if (!fnewpath.exists())
			fnewpath.mkdirs();
		File fnew = new File(newPath + oldFile.getName() +".bak");
		boolean moveOk = oldFile.renameTo(fnew);
		if (moveOk)
			logger.info(fileName + " MOVE TO NEW PATH:" + newPath + " SUCCESS["
					+ oldPath + "]");
		else {
			logger.info(fileName + " MOVE TO NEW PATH:" + newPath + " FAIL["
					+ oldPath + "]");
		}
		return moveOk;
	}

	public void reSetSwfitSeq()
	{
		SetSystemProperty.writeProperties("swiftSeq", "0");
		logger.info("swiftSeq:����0");
	}
	
	
}
