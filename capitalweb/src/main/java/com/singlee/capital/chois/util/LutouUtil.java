package com.singlee.capital.chois.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

public class LutouUtil {
	private String lsftpPath;
	private String	lsLocalPath;
	private String lsftpPort;
	private String	lsftpUser;
	private String	lsftpPwd;
	private String	lsftpIp;
	
	private String sylurl;
	private String	sylna;
	private String sylpwd;
	private String	sylpath;
	private String	sylLocalPath;
	private String	sylname;
	/**
     * 反射设置实体不同类型字段的值 <暂时只支持 日期 字符串 boolean Integer值设置 待扩建>
     *
     * @param field
     * @param obj
     * @param value
     * @throws Exception
     */
    public static void convertValue(Field field, Object obj, String value)
            throws Exception {
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if ("class java.lang.Integer".equals(field.getGenericType().toString())) {
            field.set(obj, Integer.parseInt(value));
        } else if ("boolean".equals(field.getGenericType().toString())) {
            field.set(obj, Boolean.parseBoolean(value));
        } else if ("class java.util.Date".equals(
                field.getGenericType().toString())) {
            field.set(obj, sim.parse(value));
        } else if ("class java.math.BigDecimal".equals(
                field.getGenericType().toString())) {
            field.set(obj, new BigDecimal(value));
        } else {
            field.set(obj, value);
        }

    }

    /**
     * 下载文件
     *
     * @return
     */
    public  void sftp(String filename) {
    	 Map<String ,Object> map=new HashMap<>();
    	  boolean flag =false;
		
		try {
			PropertiesConfiguration stdtmsgSysValue;
			stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
			lsftpPath= stdtmsgSysValue.getString("lsftpPath");
			lsLocalPath= stdtmsgSysValue.getString("lsLocalPath");
			lsftpPort= stdtmsgSysValue.getString("lsftpPort");
			lsftpUser= stdtmsgSysValue.getString("lsftpUser");
			lsftpPwd= stdtmsgSysValue.getString("lsftpPwd");
			lsftpIp= stdtmsgSysValue.getString("lsftpIp");
			
			Date spotDate = new Date();
	         String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
	    
	       // String fileName= "";
	        //本地文件文件保存目录
	       // String filePath = postDate.substring(0, 8) + "/";
	        //远程文件下载目录
	       //String fPath = "reports"+ "/";


	        JY.info("==================远程下载文件名为"+filename+"==============================");
	        JY.info("==================在本地创建文件夹，路径为：" + lsLocalPath + "==============================");
	        File filedir = new File(lsLocalPath);
	        //创建文件夹
	        if (!filedir.exists()) {
	            filedir.mkdirs();
	        }
	        //远程下载目录
	       // map.put("fPath",fPath);
	        //本地保存目录
	       // map.put("filePath",lsLocalPath);
	        //文件名称
	       // map.put("fileName",fileName);
	      
			JY.info("==================开始执行 准备连接sftp下载路透文件==============================");
	        // 初始化FTP
	         SingleeSFTPClient sftp = new SingleeSFTPClient(lsftpIp, 22, lsftpUser, lsftpPwd);
	        // 开始连接
	        sftp.connect();
	        // 下载文件 （远程下载目录、远程文件名、本地保存目录）
	        //List<String> filenames = sftp.batchDownLoadFile(lsftpPath,lsLocalPath,null, null,false);
	       
	         flag = sftp.downloadFile(lsftpPath,filename,lsLocalPath, filename);
		       JY.info("==================" + flag + "==============================");
	        sftp.disconnect();
	       
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 
       
    }


    //解压文件（压缩文件绝对路劲，解压后文件绝对路劲）
    public  static boolean doUncompressFile(String inFileName,String outFileName){
        try {
            if (!getExtension(inFileName).equalsIgnoreCase("gz")) {
                JY.info("文件格式必须是.gz结尾");
                return  false;
            }
            JY.info("====================解压文件绝对位置为:"+inFileName);
            JY.info("====================解压中====================");
            GZIPInputStream in = null;
            try {
                in = new GZIPInputStream(new FileInputStream(inFileName));
            } catch(FileNotFoundException e) {
                JY.info("压缩文件" + inFileName+"没有找到");
                return  false;
            }
            JY.info("Open the output file.");
            //解压文件绝对地址
//            String outFileName = getFileName(inFileName);
            JY.info("===================解压后文件的绝对路径为："+outFileName);
//            outFileName
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(outFileName);
            } catch (FileNotFoundException e) {
                JY.info("================Could not write to file. " + outFileName);
                return  false;
            }
            System.out.println("===================从压缩文件传输字节到输出文件========================");
            byte[] buf = new byte[1024];
            int len;
            while((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            JY.info("Closing the file and stream");
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
        
    }

    //获取文件格式（.gz）
    public static String getExtension(String f) {
        String ext = "";
        int i = f.lastIndexOf('.');

        if (i > 0 &&  i < f.length() - 1) {
            ext = f.substring(i+1);
        }
        return ext;
    }

    //获取解压后文件名称
    public static String getFileName(String f) {
        String fname = "";
        int i = f.lastIndexOf('.');

        if (i > 0 &&  i < f.length() - 1) {
            fname = f.substring(0,i);
        }
        return fname;
    }
    //获取远程绝对路径的文件名
    public static String getFullName(String f) {
        String ext = "";
        int i = f.lastIndexOf('/');

        if (i > 0 &&  i < f.length() - 1) {
            ext = f.substring(i+1);
        }
        return ext;
    }


    public  static String  getString(Row row, int i){
        if (row.getCell(i)==null){
            return "0";
        }
        row.getCell(i).setCellType(CellType.STRING);
        String name=row.getCell(i).getStringCellValue().trim();
        return 	name.trim();

    }

    public  static  BigDecimal  NBigDecimal(String str){
        if (str.equals("")){
            str="0";
        }
        return new BigDecimal(str);
    }

    /**
     * 从共享目录下载文件
     *
     * @param sourcePath 远端共享盘路径
     * @param targetPath 本地临时路径
     * @param fileName   文件名称
     * @param userName   远端共享盘登录用户名
     * @param password   远端共享盘登录密码
     */
    public   void smbGet() {
    	
    	// 输入流
        InputStream in = null;
        // 输出流
        OutputStream out = null;

        try {
        	
        	String lstmntdte= DateUtil.getCurrentDateAsString("yyyyMMdd");
        	PropertiesConfiguration stdtmsgSysValue;
    		stdtmsgSysValue = PropertiesUtil.parseFile("sftp.properties");
    		sylurl= stdtmsgSysValue.getString("sylurl");
    		sylna= stdtmsgSysValue.getString("sylna");
    		sylpwd= stdtmsgSysValue.getString("sylpwd");
    		sylpath= stdtmsgSysValue.getString("sylpath");
    		sylLocalPath= stdtmsgSysValue.getString("sylLocalPath");
    		//sylname=stdtmsgSysValue.getString("sylname")+lstmntdte+".xlsx";
    		 NtlmPasswordAuthentication auth = 
   	              new NtlmPasswordAuthentication(null, sylna, sylpwd);
            // 初始化远端共享盘路径
            //SmbFile sourceFile = new SmbFile("smb://" + sylna + ":" + sylpwd + "@" + sylpath+lstmntdte+"//" + sylname);
    		//String url="smb://"+sylurl+lstmntdte+"/"+"收益率曲线"+lstmntdte+".xlsx";
    		//System.out.println(url);
    		try {
    		 SmbFile sourceFile = new SmbFile("smb://"+sylurl+"路透/收益率_曲线_汇率/"+lstmntdte+"/"+"收益率曲线"+lstmntdte+".xlsx",auth);
    		 System.out.println(sourceFile.exists());
    		// boolean flag=sourceFile.isDirectory();
		    		if (sourceFile.exists()) {
		                //return;
		    			// 获取远端file名称
		                String sourceFileName = sourceFile.getName();
		                // 初始化本地文件对象
		                File targetFile = new File(sylLocalPath + File.separator + sourceFileName);
		                // 创建本地路径
		                if (!targetFile.exists()) {
		                    targetFile.getParentFile().mkdirs();
		                }
		                // 输入流
		                in = new BufferedInputStream(new SmbFileInputStream(sourceFile));
		                // 输出流
		                out = new BufferedOutputStream(new FileOutputStream(targetFile));
		                byte[] buffer = new byte[1024];
		                	while (in.read(buffer) != -1) {
		                    out.write(buffer);
		                    buffer = new byte[1024];
		                	}
		                	
		                    out.close();
		                    in.close();
		            	}else {
		            		return;
		            	}
    		} catch (Exception e) {
                e.printStackTrace();
    		}
        } catch (Exception e) {
            e.printStackTrace();
        }
                
           
        
    }

	public String getLsftpPath() {
		return lsftpPath;
	}

	public void setLsftpPath(String lsftpPath) {
		this.lsftpPath = lsftpPath;
	}

	public String getLsLocalPath() {
		return lsLocalPath;
	}

	public void setLsLocalPath(String lsLocalPath) {
		this.lsLocalPath = lsLocalPath;
	}

	public String getLsftpPort() {
		return lsftpPort;
	}

	public void setLsftpPort(String lsftpPort) {
		this.lsftpPort = lsftpPort;
	}

	public String getLsftpUser() {
		return lsftpUser;
	}

	public void setLsftpUser(String lsftpUser) {
		this.lsftpUser = lsftpUser;
	}

	public String getLsftpPwd() {
		return lsftpPwd;
	}

	public void setLsftpPwd(String lsftpPwd) {
		this.lsftpPwd = lsftpPwd;
	}

	public String getLsftpIp() {
		return lsftpIp;
	}

	public void setLsftpIp(String lsftpIp) {
		this.lsftpIp = lsftpIp;
	}

	

	public String getSylurl() {
		return sylurl;
	}

	public void setSylurl(String sylurl) {
		this.sylurl = sylurl;
	}

	public String getSylna() {
		return sylna;
	}

	public void setSylna(String sylna) {
		this.sylna = sylna;
	}

	public String getSylpwd() {
		return sylpwd;
	}

	public void setSylpwd(String sylpwd) {
		this.sylpwd = sylpwd;
	}

	public String getSylpath() {
		return sylpath;
	}

	public void setSylpath(String sylpath) {
		this.sylpath = sylpath;
	}

	public String getSylLocalPath() {
		return sylLocalPath;
	}

	public void setSylLocalPath(String sylLocalPath) {
		this.sylLocalPath = sylLocalPath;
	}

	public String getSylname() {
		return sylname;
	}

	public void setSylname(String sylname) {
		this.sylname = sylname;
	}
    
    
}
