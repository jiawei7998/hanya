package com.singlee.capital.chois.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.util.PropertiesUtil;
@Service
public class CreateSeq {

	public static String getSwiftSeqFromProperties(String filePath,String key) throws ConfigurationException
	{
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("swift.properties");
		String countString = "";
		int count = 0;
		countString = stdtmsgSysValue.getString("swiftSeq");
		if(Integer.valueOf(countString) >= 99990)
		{
			countString = "0";
		}
		count = Integer.valueOf(countString)+1;
		SetSystemProperty.writeProperties(key, String.valueOf(count));
		countString = String.format("%05d", count,5);
		return countString;
	}
	
	
	
}
