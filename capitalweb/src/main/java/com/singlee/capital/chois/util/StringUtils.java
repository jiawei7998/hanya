package com.singlee.capital.chois.util;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
@Service
public class StringUtils {
	
		//工具方法18位
		public String subStr18Char(BigDecimal args) {
			String reslut = null;
			if(args != null) {
				//转换成字符串类型
				String args1 = String.valueOf(args);
				if(args1.indexOf(".")>0) {
					//截取小数点前的字符
					String args2 = args1.substring(0, args1.indexOf("."));
					//获取需要补齐的长度
					int args3 = 18-args1.length()+args2.length();
					//小数点前补齐后的数据
					String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
					//结果reslut	
					reslut = args4+args1.substring(args1.indexOf("."));
				}else{
					reslut = String.format("%018d", Integer.parseInt(args1));
				}
				
			}else {
				reslut = String.format("%018d", Integer.parseInt("0"));
			}
			return reslut;
		}
		
		//工具方法18位
		public String subStr16Char(BigDecimal args) {
			String reslut = null;
			if(args != null) {
				//转换成字符串类型
				String args1 = String.valueOf(args);
				if(args1.indexOf(".")>0) {
					//截取小数点前的字符
					String args2 = args1.substring(0, args1.indexOf("."));
					//获取需要补齐的长度
					int args3 = 16-args1.length()+args2.length();
					//小数点前补齐后的数据
					String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
					//结果reslut	
					reslut = args4+args1.substring(args1.indexOf("."));
				}else{
					reslut = String.format("%016d", Integer.parseInt(args1));
				}
				
			}else {
				reslut = String.format("%016d", Integer.parseInt("0"));
			}
			return reslut;
		}
		//工具方法num位
		public String subStrChar(BigDecimal args,int num) {
					String reslut = null;
					if(args != null) {
						//转换成字符串类型
						String args1 = String.valueOf(args);
						if(args1.indexOf(".")>0) {
							//截取小数点前的字符
							String args2 = args1.substring(0, args1.indexOf("."));
							//获取需要补齐的长度
							int args3 = num-args1.length()+args2.length();
							//小数点前补齐后的数据
							String args4 = String.format("%0"+args3+"d", Integer.parseInt(args2));
							//结果reslut	
							reslut = args4+args1.substring(args1.indexOf("."));
						}else{
							reslut = String.format("%0"+String.valueOf(num)+"d", Integer.parseInt(args1));
						}
						
					}else {
						reslut = String.format("%016d", Integer.parseInt("0"));
					}
					return reslut;
				}
				
	//num:金额，n：总长度，m:几位小数，left：true，right:true
	public   String fillZero(String num, int n,int m,boolean left,boolean right){
		String result=null;		
			if(num!=null) {
					String amt1=null;
					String amt2=null;
					String amt3=null;
					String fstr=null;
					String str=null;
					
					 int rnum=0;		    
					int len=0;
					int zoreNum=0;
					if(num.contains("-")) {
						fstr=num.substring(0, 1);
						if(num.contains(".")) {
							 amt1=num.substring(0, num.indexOf("."));
							 amt3=amt1.substring(1, amt1.length());
							 amt2=num.substring(amt1.length()+1, num.length());
							 len=amt2.length();
							
							 if(n == num.length()){
								  zoreNum=n - amt1.length()-amt2.substring(0,m).length();
							   }else {
							    	 zoreNum = n - amt3.length()-m-1;
							   }
							
						}else {
							amt3=num.substring(1, num.length());
							rnum=m;
							amt2="";
							if(n == num.length()){
								return num;
							}else {
								zoreNum = n - amt3.length()-m-1;
							}
						}
					  
					    if(n < 0){
					        throw new RuntimeException("补零后的长度不能为负数");
					    }
					    if(n < num.length()){
					        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
					    }
					    /*if(n == num.length()){
					        return num;
					    }*/
					   
					    if(m==len) {
					    	rnum=0;
					    }else if(m>len){
					    	rnum=m-len;
					    }else {
					    	str=amt2.substring(0,m);
					    	rnum=0;
					    	amt2=str;
					    }
					    StringBuilder sb1 = new StringBuilder("");
					    for(int i = 0; i < rnum; i++){
					        sb1.append("0");
					    }
					    if(right){
					    	 sb1.insert(0,amt2);
					      
					    }else{
					    	  sb1.append(amt2);
					    }
					    
					   
					    StringBuilder sb = new StringBuilder("");
					    for(int i = 0; i < zoreNum; i++){
					        sb.append("0");
					    }
					    if(left){
					        sb.append(amt3);
					    }else{
					        sb.insert(0,amt3);
					    }
					    result=fstr+sb.toString()+sb1.toString();
					}else {
						if(num.contains(".")) {
							 amt1=num.substring(0, num.indexOf("."));
							 
							 amt2=num.substring(amt1.length()+1, num.length());
							 len=amt2.length();
							  
							  if(n == num.length()){
								  zoreNum=n - amt1.length()-amt2.substring(0,m).length();
							    }else {
							    	zoreNum = n - amt1.length()-m;
							    }
						}else {
							amt1=num;
							rnum=m;
							amt2="";
							if(n == num.length()){
								return num;
							}else {
								zoreNum = n - amt1.length()-m;
							}	
						}
					  
					    if(n < 0){
					        throw new RuntimeException("补零后的长度不能为负数");
					    }
					    if(n < num.length()){
					        throw new RuntimeException("所给字符串长度大于补零后字符串长度，补零失败");
					    }
					   
					   
					    if(m==len) {
					    	rnum=0;
					    }else if(m>len){
					    	rnum=m-len;
					    }else {
					    	str=amt2.substring(0,m);
					    	rnum=0;
					    	amt2=str;
					    }
					    StringBuilder sb1 = new StringBuilder("");
					    for(int i = 0; i < rnum; i++){
					        sb1.append("0");
					    }
					    if(right){
					    	 sb1.insert(0,amt2);
					      
					    }else{
					    	  sb1.append(amt2);
					    }
					    
					  
					    StringBuilder sb = new StringBuilder("");
					    for(int i = 0; i < zoreNum; i++){
					        sb.append("0");
					    }
					    if(left){
					        sb.append(amt1);
					    }else{
					        sb.insert(0,amt1);
					    }
					    result=sb.toString()+sb1.toString();
					}
					
					
				    
				 
				}else {
					result="";
				}
				return result;
	}
	
	public String setNum(String s) {
		int integer=Integer.valueOf(s);
		float i=(float)integer/1000000;
		String str=String.valueOf(i);
		return str;
	}
}
