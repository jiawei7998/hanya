package com.singlee.capital.chois.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsg;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.hanafn.eai.client.stdtmsg.util.StdTMsgException;
import com.singlee.capital.common.util.PropertiesUtil;

public class ChoisCommonMsg {
	public Date date =new Date();
	public SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy/MM/dd");
	public SimpleDateFormat sdfdate2 = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
	public String msgTypeCode=null;//标准报文数据种类代码
	public String  msgTypeCodeLen;//标准报文数据长度
	public int  msgTypeCodeLen1;//标准报文数据长度
	public String sysName;//系统姓名
	public String handleCode;//处理代码
	public String channelType;//渠道类型
	public String channelId;//渠道ID
	public String gainInst;//获取机构
	public String cdate;//日期
	public String ctime;//时间
	public String keyTrade;//KEY1, 交易
	public String keyTradeNo;//KEY2 , 参考号
	public String serveHandleCode;//服务处理代码
	public String FXFIG_CJUM;
	public String FXFIG_AJUM;
	public String FXFIG_SJUM;
	public String FXFIG_OPNO;
	public String FXFIG_OPGB;
	public String FXFIG_IBIL;
	public String FXFIG_AMOD;
	public String FXFIG_TELL;
	public String FXFIG_SSID;
	public String FXFIG_SSIL;
	public String FXFIG_TELLNM;
	public String FXFIG_THGB;
	public String FXFIG_TYPE;
	public String FXFIG_JUM_CD;
	public String FXFIG_CCY;
	
	public String FXFIG_SSCK;
	public String FXFIG_THID;
	
	public String makeMsg() throws Exception {
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		 
		msgTypeCode=stdtmsgSysValue.getString("msgTypeCode");
		msgTypeCodeLen=stdtmsgSysValue.getString("msgTypeCodeLen");
		msgTypeCodeLen1=stdtmsgSysValue.getInt("msgTypeCodeLen1");
		sysName=stdtmsgSysValue.getString("sysName");
		/*handleCode=stdtmsgSysValue.getString("handleCode");
		channelType=stdtmsgSysValue.getString("channelType");
		channelId=stdtmsgSysValue.getString("channelId");
		gainInst=stdtmsgSysValue.getString("gainInst");
		cdate=sdfdate.format(date);
		ctime=sdftimes.format(date);
		keyTrade=stdtmsgSysValue.getString("keyTrade");
		keyTradeNo=stdtmsgSysValue.getString("keyTradeNo");
		serveHandleCode=stdtmsgSysValue.getString("serveHandleCode");
		
		FXFIG_THID=stdtmsgSysValue.getString("FXFIG_THID");
		FXFIG_CJUM=stdtmsgSysValue.getString("FXFIG_CJUM");
		FXFIG_AJUM=stdtmsgSysValue.getString("FXFIG_AJUM");
		FXFIG_SJUM=stdtmsgSysValue.getString("FXFIG_SJUM");
		FXFIG_OPNO=stdtmsgSysValue.getString("FXFIG_OPNO");
		FXFIG_OPGB=stdtmsgSysValue.getString("FXFIG_OPGB");
		FXFIG_IBIL=stdtmsgSysValue.getString("FXFIG_IBIL");
		FXFIG_AMOD=stdtmsgSysValue.getString("FXFIG_AMOD");
		FXFIG_TELL=stdtmsgSysValue.getString("FXFIG_TELL");
		FXFIG_SSID=stdtmsgSysValue.getString("FXFIG_SSID");
		FXFIG_SSIL=stdtmsgSysValue.getString("FXFIG_SSIL");
		FXFIG_TELLNM=stdtmsgSysValue.getString("FXFIG_TELLNM");
		FXFIG_THGB=stdtmsgSysValue.getString("FXFIG_THGB");
		FXFIG_TYPE=stdtmsgSysValue.getString("FXFIG_TYPE");
		FXFIG_JUM_CD=stdtmsgSysValue.getString("FXFIG_JUM_CD");
		FXFIG_CCY=stdtmsgSysValue.getString("FXFIG_CCY");
		FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
		cdate=sdfdate.format(date);*/
		
		String msg=msgTypeCode+msgTypeCodeLen+msgTypeCodeLen1+sysName;
		return msg;
	}
	
	public String makeMsg2() throws Exception {
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		 
		/*msgTypeCode=stdtmsgSysValue.getString("msgTypeCode");
		msgTypeCodeLen=stdtmsgSysValue.getString("msgTypeCodeLen");
		msgTypeCodeLen1=stdtmsgSysValue.getInt("msgTypeCodeLen1");
		sysName=stdtmsgSysValue.getString("sysName");
		handleCode=stdtmsgSysValue.getString("handleCode");*/
		channelType=stdtmsgSysValue.getString("channelType");
		channelId=stdtmsgSysValue.getString("channelId");
		gainInst=stdtmsgSysValue.getString("gainInst");
		cdate=sdfdate2.format(date);
		ctime=sdftimes.format(date);
		keyTrade=stdtmsgSysValue.getString("keyTrade");
		keyTradeNo=stdtmsgSysValue.getString("keyTradeNo");
		/*serveHandleCode=stdtmsgSysValue.getString("serveHandleCode");
		
		FXFIG_THID=stdtmsgSysValue.getString("FXFIG_THID");
		FXFIG_CJUM=stdtmsgSysValue.getString("FXFIG_CJUM");
		FXFIG_AJUM=stdtmsgSysValue.getString("FXFIG_AJUM");
		FXFIG_SJUM=stdtmsgSysValue.getString("FXFIG_SJUM");
		FXFIG_OPNO=stdtmsgSysValue.getString("FXFIG_OPNO");
		FXFIG_OPGB=stdtmsgSysValue.getString("FXFIG_OPGB");
		FXFIG_IBIL=stdtmsgSysValue.getString("FXFIG_IBIL");
		FXFIG_AMOD=stdtmsgSysValue.getString("FXFIG_AMOD");
		FXFIG_TELL=stdtmsgSysValue.getString("FXFIG_TELL");
		FXFIG_SSID=stdtmsgSysValue.getString("FXFIG_SSID");
		FXFIG_SSIL=stdtmsgSysValue.getString("FXFIG_SSIL");
		FXFIG_TELLNM=stdtmsgSysValue.getString("FXFIG_TELLNM");
		FXFIG_THGB=stdtmsgSysValue.getString("FXFIG_THGB");
		FXFIG_TYPE=stdtmsgSysValue.getString("FXFIG_TYPE");
		FXFIG_JUM_CD=stdtmsgSysValue.getString("FXFIG_JUM_CD");
		FXFIG_CCY=stdtmsgSysValue.getString("FXFIG_CCY");
		FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
		cdate=sdfdate.format(date);*/
		
		String msg=channelType+channelId+gainInst+"                  "+cdate+ctime+keyTrade+keyTradeNo+"  ";
		return msg;
	}
	
	public String makeMsg3() throws Exception {
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		 
		/*msgTypeCode=stdtmsgSysValue.getString("msgTypeCode");
		msgTypeCodeLen=stdtmsgSysValue.getString("msgTypeCodeLen");
		msgTypeCodeLen1=stdtmsgSysValue.getInt("msgTypeCodeLen1");
		sysName=stdtmsgSysValue.getString("sysName");
		handleCode=stdtmsgSysValue.getString("handleCode");
		channelType=stdtmsgSysValue.getString("channelType");
		channelId=stdtmsgSysValue.getString("channelId");
		gainInst=stdtmsgSysValue.getString("gainInst");
		cdate=sdfdate.format(date);
		ctime=sdftimes.format(date);
		keyTrade=stdtmsgSysValue.getString("keyTrade");
		keyTradeNo=stdtmsgSysValue.getString("keyTradeNo");
		serveHandleCode=stdtmsgSysValue.getString("serveHandleCode");
		*/
		
		
		cdate=sdfdate.format(date);
		FXFIG_THID=stdtmsgSysValue.getString("FXFIG_THID");
		FXFIG_CJUM=stdtmsgSysValue.getString("FXFIG_CJUM");
		FXFIG_AJUM=stdtmsgSysValue.getString("FXFIG_AJUM");
		FXFIG_SJUM=stdtmsgSysValue.getString("FXFIG_SJUM");
		FXFIG_OPNO=stdtmsgSysValue.getString("FXFIG_OPNO");
		FXFIG_OPGB=stdtmsgSysValue.getString("FXFIG_OPGB");
		FXFIG_IBIL=stdtmsgSysValue.getString(cdate);
		FXFIG_AMOD=stdtmsgSysValue.getString("FXFIG_AMOD");
		FXFIG_TELL=stdtmsgSysValue.getString("FXFIG_TELL");
		FXFIG_SSID=stdtmsgSysValue.getString("FXFIG_SSID");
		FXFIG_SSIL=stdtmsgSysValue.getString(cdate);
		FXFIG_TELLNM=stdtmsgSysValue.getString("FXFIG_TELLNM");
		FXFIG_THGB=stdtmsgSysValue.getString("FXFIG_THGB");
		FXFIG_TYPE=stdtmsgSysValue.getString("FXFIG_TYPE");
		FXFIG_JUM_CD=stdtmsgSysValue.getString("FXFIG_JUM_CD");
		FXFIG_CCY=stdtmsgSysValue.getString("FXFIG_CCY");
		FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
		//cdate=sdfdate.format(date);
		
		String msg=FXFIG_THID+"      "+cdate+FXFIG_SSID+cdate+FXFIG_THGB+FXFIG_TYPE+FXFIG_JUM_CD+FXFIG_CJUM+FXFIG_AJUM+FXFIG_SJUM+FXFIG_OPNO+FXFIG_OPGB+FXFIG_TELL+FXFIG_CCY;
		
		return msg;
	}
	
	public byte[] makeHead() throws StdTMsgException, ConfigurationException {
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		//EAI接口ID
		String intfId = "OPS_CHS_DSS00001";
		StdTMsgGenerator generator = new StdTMsgGenerator();
		StdTMsg msg = generator.makeStdTMsgWithSysDefaultValue();
		msg.setEaiIntfId(intfId);
		msg.setTrscSyncDvCd("S");
		msg.setStdTmsgPrgrNo("01");//转发系统节点代码
		msg.setRecvSvcCd("EIO1100");//接收服务代码
		msg.setExtnCrctNo("OPCS");//外部线路号		
			
		byte[] headerPacket = generator.makeHeader(msg);
		
		return headerPacket;
	}
}
