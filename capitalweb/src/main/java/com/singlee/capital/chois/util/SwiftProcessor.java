package com.singlee.capital.chois.util;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;


@Service
public class SwiftProcessor {

	public static Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private SwiftPackageUtil swiftPackageUtil;
	
	@Autowired
	private FtpUtil ftpUtil;
	
	@Autowired
	private SwiftFileFilter swiftFileFilter;
	
	private String opicsSwiftPath;//OPICS SWIFT ������·��
	
	private int splitAsscii;//
	
	private String createSplitFileHeaderName;//
	
	private String createSplitFilePath;//
	
	private String createSplitFileType;//
	
	private String moveToNewPath;//
	

	private TaskExecutor taskExecutor;
	
	private int taskExecutorTime;
	
	private String upLoadBackUpPath;
	
	private static String swiftPostdate ;
	
	/**
	 * @return the upLoadBackUpPath
	 */
	public String getUpLoadBackUpPath() {
		return upLoadBackUpPath;
	}



	/**
	 * @param upLoadBackUpPath the upLoadBackUpPath to set
	 */
	public void setUpLoadBackUpPath(String upLoadBackUpPath) {
		this.upLoadBackUpPath = upLoadBackUpPath;
	}



	/**
	 * @return the taskExecutorTime
	 */
	public int getTaskExecutorTime() {
		return taskExecutorTime;
	}



	/**
	 * @param taskExecutorTime the taskExecutorTime to set
	 */
	public void setTaskExecutorTime(int taskExecutorTime) {
		this.taskExecutorTime = taskExecutorTime;
	}



	public SwiftProcessor(){}



	/**
	 * @return the opicsSwiftPath
	 */
	public String getOpicsSwiftPath() {
		return opicsSwiftPath;
	}



	/**
	 * @param opicsSwiftPath the opicsSwiftPath to set
	 */
	public void setOpicsSwiftPath(String opicsSwiftPath) {
		this.opicsSwiftPath = opicsSwiftPath;
	}


	/**
	 * @return the splitAsscii
	 */
	public int getSplitAsscii() {
		return splitAsscii;
	}



	/**
	 * @param splitAsscii the splitAsscii to set
	 */
	public void setSplitAsscii(int splitAsscii) {
		this.splitAsscii = splitAsscii;
	}



	/**
	 * @return the createSplitFileHeaderName
	 */
	public String getCreateSplitFileHeaderName() {
		return createSplitFileHeaderName;
	}

	/**
	 * @param createSplitFileHeaderName the createSplitFileHeaderName to set
	 */
	public void setCreateSplitFileHeaderName(String createSplitFileHeaderName) {
		this.createSplitFileHeaderName = createSplitFileHeaderName;
	}

	/**
	 * @return the createSplitFilePath
	 */
	public String getCreateSplitFilePath() {
		return createSplitFilePath;
	}

	/**
	 * @param createSplitFilePath the createSplitFilePath to set
	 */
	public void setCreateSplitFilePath(String createSplitFilePath) {
		this.createSplitFilePath = createSplitFilePath;
	}

	/**
	 * @return the createSplitFileType
	 */
	public String getCreateSplitFileType() {
		return createSplitFileType;
	}

	/**
	 * @param createSplitFileType the createSplitFileType to set
	 */
	public void setCreateSplitFileType(String createSplitFileType) {
		this.createSplitFileType = createSplitFileType;
	}

	/**
	 * @return the moveToNewPath
	 */
	public String getMoveToNewPath() {
		return moveToNewPath;
	}

	/**
	 * @param moveToNewPath the moveToNewPath to set
	 */
	public void setMoveToNewPath(String moveToNewPath) {
		this.moveToNewPath = moveToNewPath;
	}

	/**
	 * @return the taskExecutor
	 */
	public TaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	/**
	 * @param taskExecutor the taskExecutor to set
	 */
	public void setTaskExecutor(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	};
	/**
	 * @return the swiftPackageUtil
	 */
	public SwiftPackageUtil getSwiftPackageUtil() {
		return swiftPackageUtil;
	}

	/**
	 * @param swiftPackageUtil the swiftPackageUtil to set
	 */
	public void setSwiftPackageUtil(SwiftPackageUtil swiftPackageUtil) {
		this.swiftPackageUtil = swiftPackageUtil;
	}

	/**
	 * @return the ftpUtil
	 */
	public FtpUtil getFtpUtil() {
		return ftpUtil;
	}

	/**
	 * @param ftpUtil the ftpUtil to set
	 */
	public void setFtpUtil(FtpUtil ftpUtil) {
		this.ftpUtil = ftpUtil;
	}
	

	/**
	 * @return the swiftFileFilter
	 */
	public SwiftFileFilter getSwiftFileFilter() {
		return swiftFileFilter;
	}

	/**
	 * @param swiftFileFilter the swiftFileFilter to set
	 */
	public void setSwiftFileFilter(SwiftFileFilter swiftFileFilter) {
		this.swiftFileFilter = swiftFileFilter;
	}

	
	public void runOneProject() throws ConfigurationException
	{
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("swift.properties");
		
		opicsSwiftPath= stdtmsgSysValue.getString("opicsSwiftPath");
		splitAsscii= Integer.parseInt(stdtmsgSysValue.getString("splitAsscii"));
		createSplitFileHeaderName= stdtmsgSysValue.getString("createSplitFileHeaderName");
		createSplitFilePath= stdtmsgSysValue.getString("createSplitFilePath");
		moveToNewPath= stdtmsgSysValue.getString("moveToNewPath");
		taskExecutorTime= Integer.parseInt(stdtmsgSysValue.getString("taskExecutorTime"));
		upLoadBackUpPath= stdtmsgSysValue.getString("upLoadBackUpPath");
		swiftPostdate=stdtmsgSysValue.getString("swiftPostdate");
		//判断日期是否切换 如果切换了那么重置为0
		String nowPostdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		if(!nowPostdate.equals(swiftPostdate))
		{
			JY.info("日期切换重置SWIFT的SEQ=0["+swiftPostdate+"]");
			this.swiftPackageUtil.reSetSwfitSeq();
			SetSystemProperty.writeProperties("swiftPostdate", nowPostdate);
		}
		swiftPostdate = nowPostdate;
		
		//查找OPICS的SWIFT路径，并获得全部的列表
				List<String> fileNameList  = this.swiftPackageUtil.searchAllPathFileList(opicsSwiftPath, "", swiftFileFilter);
				if(fileNameList.size()>0) {
					for(String fileName:fileNameList)
					{
						String filePathName = this.opicsSwiftPath+fileName;
						//判断SWIFT文件是否为空
						if(!swiftPackageUtil.checkFileIsNull(filePathName))
						{
							JY.info("SWIFT PROCESS:"+filePathName+"IS NULL.");
							//将SWIFT原始空文件备份出去
							this.swiftPackageUtil.moveOldSwiftFileToNewPath(fileName,this.opicsSwiftPath,this.moveToNewPath);
						}else
						{
							//对SWIFT可用文件进行拆分
							if(this.swiftPackageUtil.separatorFile(filePathName, splitAsscii, createSplitFileHeaderName, createSplitFilePath, createSplitFileType))
							{
								this.swiftPackageUtil.moveOldSwiftFileToNewPath(fileName,this.opicsSwiftPath,this.moveToNewPath);
							}
						}
					}
				}
				
				//进行上传文件 并将上传完的文件归档
				List<String> upLoadFileList = new ArrayList<String>();
				swiftFileFilter.setFilterFileName("imtf9");
				upLoadFileList = this.swiftPackageUtil.searchAllPathFileList(createSplitFilePath, "", swiftFileFilter);
				if( upLoadFileList.size()>0) {
					for(String upLoadFileName:upLoadFileList)
					{
						if(this.ftpUtil.connectFtpServer())
							if(this.ftpUtil.upLoadLocalFile(createSplitFilePath+upLoadFileName, upLoadFileName))
							{
								this.swiftPackageUtil.moveOldSwiftFileToNewPathUpLoad(upLoadFileName,this.createSplitFilePath,this.upLoadBackUpPath);
							}
						this.ftpUtil.closeConnectFtpServer();
					}
				}
			
		
		
	}
	
	public void initProcessRunner()
	{	
		try {
			runOneProject();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
		
				
	}

}
