package com.singlee.capital.chois.util;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import  org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;


@Service
public class FtpUtil {
	
	//private Logger logger = LogManager.getLogger(LogManager.MODEL_SWIFTLOG);
	public static Logger logger = LoggerFactory.getLogger("com");
	public FtpUtil() {
		// TODO Auto-generated constructor stub
	}
	
	private String ftpIp;
	
	private int ftpPort;
	
	private String ftpUser;
	
	private String ftpPwd;
	
	private String ftpRoot;
	/**
	 * 
	 */
	private String ftpPath;
	
	
	private FTPClient ftpClient;


	public FtpUtil(String ftpIp, int ftpPort, String ftpUser, String ftpPwd,
			String ftpPath) {
		super();
		this.ftpIp = ftpIp;
		this.ftpPort = ftpPort;
		this.ftpUser = ftpUser;
		this.ftpPwd = ftpPwd;
		this.ftpPath = ftpPath;
	}

	public FTPClient getFtpClient() {
		return ftpClient;
	}

	public void setFtpClient(FTPClient ftpClient) {
		this.ftpClient = ftpClient;
	}

	public String getFtpIp() {
		return ftpIp;
	}

	public void setFtpIp(String ftpIp) {
		this.ftpIp = ftpIp;
	}

	public int getFtpPort() {
		return ftpPort;
	}

	public void setFtpPort(int ftpPort) {
		this.ftpPort = ftpPort;
	}

	public String getFtpUser() {
		return ftpUser;
	}

	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}

	public String getFtpPwd() {
		return ftpPwd;
	}

	public void setFtpPwd(String ftpPwd) {
		this.ftpPwd = ftpPwd;
	}

	public String getFtpPath() {
		return ftpPath;
	}

	public void setFtpPath(String ftpPath) {
		this.ftpPath = ftpPath;
	}
	/**
	 * FTP 
	 * @return
	 * @throws ConfigurationException 
	 */
	public boolean connectFtpServer() throws ConfigurationException{
		
		boolean flag = false;
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("swift.properties");
		try {
			
			//JY.info("FTP:"+"CONNECT TO FTP SERVER["+ftpIp+" "+ftpPort+"]"+"["+ftpUser+" "+ftpPwd+"].");
			ftpIp=stdtmsgSysValue.getString("ftpIp");
			ftpPort=Integer.parseInt(stdtmsgSysValue.getString("ftpPort"));
			ftpUser=stdtmsgSysValue.getString("ftpUser");
			ftpPwd=stdtmsgSysValue.getString("ftpPwd");
			ftpPath=stdtmsgSysValue.getString("ftpPath");
			
			ftpClient = new FTPClient();
			ftpClient.setRemoteHost(ftpIp);
			ftpClient.setRemotePort(ftpPort);
			ftpClient.setTransferBufferSize(10000);
            ftpClient.setTimeout(20000);
			ftpClient.setControlEncoding("GBK");
			if(ftpClient==null){
				ftpClient = new FTPClient();
				ftpClient.setRemoteHost(ftpIp);
				ftpClient.setRemotePort(ftpPort);
				ftpClient.setTransferBufferSize(10000);
	            ftpClient.setTimeout(20000);
				ftpClient.setControlEncoding("GBK");
			}		
			if(!ftpClient.connected()){
				ftpClient.connect();
				ftpClient.login(ftpUser, ftpPwd);			
				ftpClient.setConnectMode(FTPConnectMode.PASV);
				ftpClient.setType(FTPTransferType.BINARY);
			}		
			
			if(null != ftpPath && "" != ftpPath)
			{
				ftpClient.chdir(ftpPath);
				JY.info("FTP:"+"PATH CHANGE["+ftpPath+"]");
			}
			JY.info("FTP:"+"CONNECT TO FTP SERVER SUCCESS,NEXT UPLOAD FILES.");
			
			flag = true;
			
		} catch (IOException e) {
			
			JY.info("FTP IOException:"+e.getMessage());
			
		} catch (Exception ex)
		{			
			JY.info("FTP Exception:"+ex.getMessage());
			
		}
		
		return flag;
		
	}
	/**
	 * FTP 
	 * @return
	 */
	public boolean closeConnectFtpServer()
	{
		boolean flag = false;
		
		try {
			
			JY.info("FTP:"+"BEGIN TO CLOSE FTP SERVER.");
			
			if(null != ftpClient)
			
			ftpClient.quit();
			
			JY.info("FTP:"+"END TO CLOSE FTP SUCCESS.");
			
			flag = true;
			
		} catch (IOException e) {
			
			JY.info("FTP IOException:"+e.getMessage());
			
			e.printStackTrace();
			
		} catch (Exception ex)
		{
			
			JY.info("FTP Exception:"+ex.getMessage());
			
			ex.printStackTrace();
			
		}
		
		return flag;
	}
	/**
	 * FTP
	 * @param localFilePath 
	 * @param remoteFileName 
	 * @return
	 */
	public boolean upLoadLocalFile(String localFilePath,String remoteFileName)
	{
		JY.info("FTP:"+"BEGIN UPLOAD FILE[LOCAL:"+localFilePath+"]"+"[REMOTE:"+remoteFileName+"]");
		
		boolean flag = false; 
		
		java.io.File file_in = null;
		
		long total = 0;
		
		try {
			
			file_in = new java.io.File(localFilePath);
			
			total  = file_in.length();
			
			if(total <= 0 ) 
			{
				
				JY.info("FTP:"+"FTP FILE IS NULL.CHECK IT!");
				
				return false;
			}
			
			 ftpClient.put(localFilePath, remoteFileName, false);//put(remoteFileName);
			
			
			flag = true;
			
			
			} catch (IOException e) {
				
				JY.info("FTP IOException:"+e.getMessage());

				e.printStackTrace();
				
			} catch (Exception ex)
			{
				JY.info("FTP Exception:"+ex.getMessage());
				
				ex.printStackTrace();
				
			}finally{
				
			}

		
		return flag;
	}
	/**
	 * ����Զ��FTP �������ϵ��ļ�
	 * @param localFilePath
	 * @param remoteFileName
	 * @return
	 * @throws Exception 
	 */
	public boolean downLoadFtpServerFile(String localFilePath,String remoteFileName) throws Exception
	{
		boolean flag = false; 
		
		try {
			ftpClient.get(localFilePath, remoteFileName);
			
			flag = true;
			
			} catch (IOException e) {
				
				throw e;
				
			} catch (Exception ex)
			{
				
				throw ex;
				
			}finally
			{
			}

		return flag;
	}
	
	/**
	 * 
	 * @param remotePath 
	 * @param localFilePath
	 * @param remoteFileName
	 * @return
	 */
	public boolean downLoadFtpServerFilePath(String remotePath,String localFilePath,String remoteFileName)
	{
		boolean flag = false; 
		
		try {
			
			if (null != remotePath && "" != remotePath && remotePath.length() != 0) 
				
				ftpClient.chdir(remotePath);
			
			ftpClient.get(localFilePath,remoteFileName);
			
			
			flag = true;
			
			} catch (IOException e) {
			
				JY.info("error:"+e.getMessage());
				e.printStackTrace();
				
			} catch (Exception ex)
			{
				JY.info("error:"+ex.getMessage());
				ex.printStackTrace();
				
			}finally
			{
				
			}

		return flag;
	}
	
	 public   static   String   fileOrFolder(String   s)   {   
		 
	      String   str="";   
	      
	      StringTokenizer   st=new   StringTokenizer(s,"   ");   
	      
	      str=st.nextToken();  
	      
	      String time = "";
	      
	      while(st.hasMoreTokens())   
	      {
	    	  
	          s=st.nextToken();
	          
	          if(s.indexOf(":") != -1)
	        	  
	        	  time = s;
	          
	      }
	      
	      return   str.substring(0, 1).indexOf("-")==-1?"Folder:"+s:"File:"+s+"("+time+")";   
	      
	  }   
	
	
	public String getFtpRoot() {
		return ftpRoot;
	}

	public void setFtpRoot(String ftpRoot) {
		this.ftpRoot = ftpRoot;
	}
}
