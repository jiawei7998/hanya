package com.singlee.capital.fund.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.ProductApproveFund;

public interface ProductApproveFundMapper extends Mapper<ProductApproveFund>{
	
	public void deleteProductApproveFundDealno(String dealno);
	public int updateProductApproveFund(String dealno);
	public Page<ProductApproveFund> getProductApproveFundList(Map<String, Object> params, RowBounds rb);
	public ProductApproveFund getProductApproveFund(Map<String, Object> params);
	public Page<ProductApproveFund> searchPageProductApproveFund(Map<String, Object> params, RowBounds rb);
	public Page<ProductApproveFund> searchPageProductApproveFundFinished(Map<String, Object> params, RowBounds rb);
	public Page<ProductApproveFund> searchPageProductApproveFundMySelf(Map<String, Object> params, RowBounds rb);
	public int updateApproveStatus(Map<String, Object> params);
	public ProductApproveFund getFundInfoForApprove(@Param(value = "dealNo")String dealNo);
	
	public List<ProductApproveFund> getProductApproveFundListByFundCode(Map<String, Object> params);
}
