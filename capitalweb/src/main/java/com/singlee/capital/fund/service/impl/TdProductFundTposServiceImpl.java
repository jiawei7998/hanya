package com.singlee.capital.fund.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.fund.mapper.TdProductFundTposMapper;
import com.singlee.capital.fund.model.TdProductFundTpos;
import com.singlee.capital.fund.service.TdProductFundTposService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdProductFundTposServiceImpl implements TdProductFundTposService{
	
	@Resource
	TdProductFundTposMapper tdProductFundTposMapper;
	

	@Override
	public List<TdProductFundTpos> searchTdProductFundTpos(TdProductFundTpos tdProductFundTpos) {
		
		return tdProductFundTposMapper.selectAll();
	}

	@Override
	public int insertTdProductFundTpos(TdProductFundTpos tdProductFundTpos) {
		
		return tdProductFundTposMapper.insert(tdProductFundTpos);
	}

	@Override
	public int deleteTdProductFundTpos(TdProductFundTpos tdProductFundTpos) {
		tdProductFundTposMapper.deleteTdProductFundTposDealno(tdProductFundTpos.getSerialNo());
		return 1;
	}
	
	@Override
	public int updateTdProductFundTpos(TdProductFundTpos tdProductFundTpos) {
		return tdProductFundTposMapper.updateByPrimaryKey(tdProductFundTpos);
	}

	@Override
	public Page<TdProductFundTpos> getTdProductFundTposList(Map<String, Object> params, RowBounds rb) {
		return tdProductFundTposMapper.getTdProductFundTposList(params, rb);
	}

	
	
	@Override
	public int fundTposManager(Map<String, Object> map) {
		int num = 0;
		try {
			//交易驱动传递过来的对象
			TdProductFundTpos tpos = new TdProductFundTpos();
			BeanUtils.populate(tpos, map);
			//获取数据库中现有持仓情况
			TdProductFundTpos dbTpos = new TdProductFundTpos();
			dbTpos = tdProductFundTposMapper.getFundTpos(map);
			if(null != dbTpos){
				TdProductFundTpos newTpos = new TdProductFundTpos();
				newTpos.setSerialNo(dbTpos.getSerialNo());
				newTpos.setFundCode(dbTpos.getFundCode());
				newTpos.setInvtype(dbTpos.getInvtype());
				newTpos.setPrdNo(dbTpos.getPrdNo());
				newTpos.setCcy(dbTpos.getCcy());
				newTpos.setcNo(dbTpos.getcNo());
				if("0".equals(map.get("opType"))){ //申购
					//持仓份额
					newTpos.setUtQty(dbTpos.getUtQty().add(tpos.getUtQty()));
					//持仓金额
					newTpos.setUtAmt(dbTpos.getUtAmt().add(tpos.getUtAmt()));
				}
				if("1".equals(map.get("opType"))){ //赎回
					//持仓份额
					newTpos.setUtQty(dbTpos.getUtQty().subtract(tpos.getUtQty()));
					//持仓金额计算  赎回金额+赎回日分红金额
					newTpos.setUtAmt(dbTpos.getUtAmt().subtract(tpos.getUtAmt()));
					newTpos.setShAmt(tpos.getShAmt());
					//判断是否赎回未结转金额
					BigDecimal subAmt =  dbTpos.getUtAmt().subtract(tpos.getUtAmt());
					if(subAmt.compareTo(new BigDecimal(0)) < 0){ //需赎回未结转金额
						//未结转金额
						newTpos.setNcvAmt((dbTpos.getNcvAmt()==null?new BigDecimal(0):dbTpos.getNcvAmt()).add(subAmt));
					}
				}
				if("2".equals(map.get("opType"))){ //分红
					//已分红金额
					if(dbTpos.getShAmt() != null){
						newTpos.setShAmt(dbTpos.getShAmt().add(tpos.getShAmt()));
					}else{
						newTpos.setShAmt(tpos.getShAmt());
					}
					//已分红，未清算，将此金额放置未结转金额中
					if("2".equals(map.get("isSettl"))){
						//未结转金额
						if(null != dbTpos.getNcvAmt()){
							newTpos.setNcvAmt(dbTpos.getNcvAmt().add(tpos.getShAmt()));
						}else{
							newTpos.setNcvAmt(tpos.getShAmt());
						}
					}
				}
				if("3".equals(map.get("opType"))){ //红利再投
					//持仓份额
					newTpos.setUtQty(dbTpos.getUtQty().add(tpos.getShAmt()));
					newTpos.setUtAmt(dbTpos.getUtAmt().add(tpos.getShAmt()));
					//未结转金额
					/*if(null != dbTpos.getNcvAmt()){
						newTpos.setNcvAmt(dbTpos.getNcvAmt().subtract(tpos.getShAmt()));
					}else{
						newTpos.setNcvAmt(new BigDecimal(0).subtract(tpos.getShAmt()));
					}*/
				}
				if("4".equals(map.get("opType"))){ //基金转换
					if("1".equals(map.get("transType"))){//转入
						//持仓份额
						newTpos.setUtQty(dbTpos.getUtQty().add(tpos.getUtQty()));
						//持仓金额
						newTpos.setUtAmt(dbTpos.getUtAmt().add(tpos.getUtAmt()));
					}
					if("2".equals(map.get("transType"))){//转出
						//持仓份额
						newTpos.setUtQty(dbTpos.getUtQty().subtract(tpos.getUtQty()));
						//持仓金额
						newTpos.setUtAmt(dbTpos.getUtAmt().subtract(tpos.getUtAmt()));
					}
				}
				//更新到基金持仓表
				num = tdProductFundTposMapper.updateTdProductFundTpos(newTpos);
			}else{
				num = tdProductFundTposMapper.insert(tpos);
			}
		} catch (Exception e) {
			throw new RException(e.getMessage());
		} 
		return num;
	}

	@Override
	public TdProductFundTpos getFundTpos(Map<String, Object> params) {
		return tdProductFundTposMapper.getFundTpos(params);
	}

}
