package com.singlee.capital.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 基金存续期现金分红表
 * @author SL_LXY
 *
 */
@Entity
@Table(name = "TD_DURATION_FUND_CASHDIV")
public class TdDurationFundCashdiv implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//审批单编号 自动生成
	private String prdNo;//产品编号 序列自动生成
	private int version;//版本
	private String sponsor;//审批发起人
	private String sponInst;//审批发起机构
	private String aDate;//审批开始日期
	private String fundCode;//基金代码
	private String opType;//操作类型-1-现金分红
	private BigDecimal utQty;//当前持仓份额
	private String vDate;//分红确认日
	private BigDecimal cashdivAmt;//分红金额
	private String remark;//备注
	private String isSettl;//是否清算  1-清算 2-不清算
	private String tDate;//是否清算-1的时候有划款日期
	private String inOutFlag;//行内行外  1-行内  2-行外
	private String selfAccCode;//本方账号
	private String selfAccName;//本方账户名称
	private String selfBankCode;//本方开户行行号
	private String selfBankName;//本方开户行名称
	private String partyAccCode;//对手方账号
	private String partyAccName;//对手方账户名称
	private String partyBankCode;//对手方开户行行号
	private String partyBankName;//对手方开户行名称
	private String approveStatus;//审批单状态
	@Transient
	private String flowType;
	@Transient
	private String taskId;
	@Transient
	private String taskName;
	@Transient
	private String procInst;
	@Transient
	private String executionName;
	@Transient
	private String fundType;//债基类型
	@Transient
	private String cnName;//客户名称
	@Transient
	private String prdName;//产品名称
	@Transient
	private String userName;//用户名称
	@Transient
	private String instName;//机构名称
	@Transient
	private String fundName;//基金名称
	@Transient
	private String invType;//会计四分类
	private String ccy;
	private String cNo;
	private String dealDate;//交易日期
	//现金流类型
	private String cfType;
	
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public BigDecimal getUtQty() {
		return utQty;
	}
	public void setUtQty(BigDecimal utQty) {
		this.utQty = utQty;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public BigDecimal getCashdivAmt() {
		return cashdivAmt;
	}
	public void setCashdivAmt(BigDecimal cashdivAmt) {
		this.cashdivAmt = cashdivAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getIsSettl() {
		return isSettl;
	}
	public void setIsSettl(String isSettl) {
		this.isSettl = isSettl;
	}
	public String gettDate() {
		return tDate;
	}
	public void settDate(String tDate) {
		this.tDate = tDate;
	}
	public String getInOutFlag() {
		return inOutFlag;
	}
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	public String getSelfAccCode() {
		return selfAccCode;
	}
	public void setSelfAccCode(String selfAccCode) {
		this.selfAccCode = selfAccCode;
	}
	public String getSelfAccName() {
		return selfAccName;
	}
	public void setSelfAccName(String selfAccName) {
		this.selfAccName = selfAccName;
	}
	public String getSelfBankCode() {
		return selfBankCode;
	}
	public void setSelfBankCode(String selfBankCode) {
		this.selfBankCode = selfBankCode;
	}
	public String getSelfBankName() {
		return selfBankName;
	}
	public void setSelfBankName(String selfBankName) {
		this.selfBankName = selfBankName;
	}
	public String getPartyAccCode() {
		return partyAccCode;
	}
	public void setPartyAccCode(String partyAccCode) {
		this.partyAccCode = partyAccCode;
	}
	public String getPartyAccName() {
		return partyAccName;
	}
	public void setPartyAccName(String partyAccName) {
		this.partyAccName = partyAccName;
	}
	public String getPartyBankCode() {
		return partyBankCode;
	}
	public void setPartyBankCode(String partyBankCode) {
		this.partyBankCode = partyBankCode;
	}
	public String getPartyBankName() {
		return partyBankName;
	}
	public void setPartyBankName(String partyBankName) {
		this.partyBankName = partyBankName;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getProcInst() {
		return procInst;
	}
	public void setProcInst(String procInst) {
		this.procInst = procInst;
	}
	public String getExecutionName() {
		return executionName;
	}
	public void setExecutionName(String executionName) {
		this.executionName = executionName;
	}
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
}
