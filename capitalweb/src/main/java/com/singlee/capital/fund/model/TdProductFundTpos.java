package com.singlee.capital.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 基金存续期转换表
 * @author SL_LXY
 *
 */
@Entity
@Table(name = "TD_PRODUCT_FUND_TPOS")
public class TdProductFundTpos implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FUND_TPOS.NEXTVAL FROM DUAL")
	private String serialNo;//关联序列
	private String prdNo;//产品代码
	private String invtype;//四分类
	private String fundCode;//基金代码
	private BigDecimal utQty;//持仓份额
	private BigDecimal utAmt;//持仓金额
	private BigDecimal ncvAmt;//未结转金额
	private BigDecimal yield_8;//万份收益率
	private BigDecimal shAmt;//已分红金额
	private String postdate;//账务日期
	private String sponInst;//机构号
	private String revalAmt;//估值
	private String unplAmt;//未实现损益
	private String replAmt;//已实现损益
	private String cNo;
	private String ccy;//币种
	
	@Transient
	private String cName;
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public BigDecimal getUtQty() {
		return utQty;
	}
	public void setUtQty(BigDecimal utQty) {
		this.utQty = utQty;
	}
	public BigDecimal getUtAmt() {
		return utAmt;
	}
	public void setUtAmt(BigDecimal utAmt) {
		this.utAmt = utAmt;
	}
	public BigDecimal getNcvAmt() {
		return ncvAmt;
	}
	public void setNcvAmt(BigDecimal ncvAmt) {
		this.ncvAmt = ncvAmt;
	}
	public BigDecimal getYield_8() {
		return yield_8;
	}
	public void setYield_8(BigDecimal yield_8) {
		this.yield_8 = yield_8;
	}
	public BigDecimal getShAmt() {
		return shAmt;
	}
	public void setShAmt(BigDecimal shAmt) {
		this.shAmt = shAmt;
	}
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getRevalAmt() {
		return revalAmt;
	}
	public void setRevalAmt(String revalAmt) {
		this.revalAmt = revalAmt;
	}
	public String getUnplAmt() {
		return unplAmt;
	}
	public void setUnplAmt(String unplAmt) {
		this.unplAmt = unplAmt;
	}
	public String getReplAmt() {
		return replAmt;
	}
	public void setReplAmt(String replAmt) {
		this.replAmt = replAmt;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
}
