package com.singlee.capital.fund.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.fund.mapper.ProductApproveFundMapper;
import com.singlee.capital.fund.mapper.TaFundMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TaFund;
import com.singlee.capital.fund.service.TaFundService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaFundServiceImpl implements TaFundService{
	
	@Resource
	TaFundMapper TaFundMapper;
	
	@Resource
	ProductApproveFundMapper productApproveFundMapper;

	@Override
	public List<TaFund> searchTaFund(TaFund taFund) {
		
		return TaFundMapper.selectAll();
	}

	@Override
	public void insertTaFund(TaFund taFund) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("fundCode", taFund.getFundCode());
		TaFund fund=TaFundMapper.getTaFund(params);
		if(fund==null){
			TaFundMapper.insert(taFund);
		}else{
			JY.raiseRException("基金代码["+taFund.getFundCode()+"],已经存在，不能添加!");
		}
		
		
	}

	@Override
	public void deleteTaFund(TaFund taFund) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("fundCode",taFund.getFundCode());
		List<ProductApproveFund> list=productApproveFundMapper.getProductApproveFundListByFundCode(params);
		if(list.size()>0){
			JY.raiseRException("基金代码["+taFund.getFundCode()+"],还存在申购交易，不能删除!");
		}else{
			TaFundMapper.deleteTaFundByfundCode(taFund.getFundCode());
		}
	}

	@Override
	public int updateTaFund(TaFund taFund) {
		return TaFundMapper.updateByPrimaryKey(taFund);
	}

	@Override
	public Page<TaFund> getTaFundList(Map<String, Object> params, RowBounds rb) {
		return TaFundMapper.getTaFundList(params, rb);
	}
	
	@Override
	public TaFund getTaFund(Map<String, Object> params, RowBounds rb) {
		return TaFundMapper.getTaFund(params, rb);
	}
	@Override
	public TaFund getTaFund(Map<String, Object> params) {
		return TaFundMapper.getTaFund(params);
	}

}
