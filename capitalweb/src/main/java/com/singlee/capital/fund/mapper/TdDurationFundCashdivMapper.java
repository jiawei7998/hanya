package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;

public interface TdDurationFundCashdivMapper extends Mapper<TdDurationFundCashdiv>{
	
	public void deleteTdDurationFundCashdivDealno(String dealno);
	public int updateTdDurationFundCashdiv(String dealno);
	public Page<TdDurationFundCashdiv> getTdDurationFundCashdivList(Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdiv(
			Map<String, Object> params, RowBounds rb);
	public int updateApproveStatus(Map<String, Object> params);
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivFinished(
			Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivMySelf(
			Map<String, Object> params, RowBounds rb);
	public TdDurationFundCashdiv getTdDurationFundCashdivByDealNo(TdDurationFundCashdiv t);
}
