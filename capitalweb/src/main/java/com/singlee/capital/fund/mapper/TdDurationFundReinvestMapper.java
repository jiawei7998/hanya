package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundReinvest;

public interface TdDurationFundReinvestMapper extends Mapper<TdDurationFundReinvest>{
	
	public void deleteTdDurationFundReinvestDealno(String dealno);
	public int updateTdDurationFundReinvest(String dealno);
	public Page<TdDurationFundReinvest> getTdDurationFundReinvestList(Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvest(
			Map<String, Object> parame, RowBounds rb);
	public int updateApproveStatus(Map<String, Object> params);
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestFinished(
			Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestMySelf(
			Map<String, Object> params, RowBounds rb);
	public TdDurationFundReinvest getTdDurationFundReinvestByDealNo(TdDurationFundReinvest t);
}
