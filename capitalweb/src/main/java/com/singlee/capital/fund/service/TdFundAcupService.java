package com.singlee.capital.fund.service;

import java.util.Map;

public interface TdFundAcupService {
	
	/**
	 * 账务处理表
	 * @param params
	 * @return
	 */
	public String acupInterface(Map<String,Object> infoMap,Map<String,Object> amtMap);
	/**
	 * 插入每日金额表
	 * @param params
	 * @return
	 */
	public int insertTdAccTrdDaily(Map<String, Object> params);
	
	public int fundAcupManager(Object object);
}
