package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fund.model.TaFund;
import com.singlee.capital.fund.service.TaFundService;
import com.singlee.capital.system.controller.CommonController;

/**
 * 现金流controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/TaFundController")
public class TaFundController extends CommonController{
	
	@Resource 
	TaFundService taFundService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTaFund")
	public RetMsg<PageInfo<TaFund>> searchTaFund(@RequestBody Map<String,Object> map){
		Page<TaFund> page  = taFundService.getTaFundList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTaFund")
	public RetMsg<TaFund> getTaFund(@RequestBody Map<String,Object> map){
		TaFund page  = taFundService.getTaFund(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertTaFund")
	public RetMsg<Serializable> insertTaFund(@RequestBody TaFund map) {
		taFundService.insertTaFund(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTaFund")
	public RetMsg<Serializable> updateTaFund(@RequestBody TaFund map) {
		taFundService.updateTaFund(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteTaFund")
	public RetMsg<Serializable> deleteTaFund(@RequestBody TaFund map) {
		taFundService.deleteTaFund(map);
		return RetMsgHelper.ok();
	} 
}
