package com.singlee.capital.fund.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.mapper.TaFundDealMapper;
import com.singlee.capital.fund.mapper.TaFundMapper;
import com.singlee.capital.fund.model.TaFundDeal;
import com.singlee.capital.fund.service.TaFundDealService;

@SuppressWarnings("unused")
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaFundDealServiceImpl implements TaFundDealService{
	
	@Resource
	TaFundDealMapper taFundDealMapper;

	@Override
	public List<TaFundDeal> searchTaFundDeal(TaFundDeal TaFundDeal) {
		
		return taFundDealMapper.selectAll();
	}

	@Override
	public int insertTaFundDeal(TaFundDeal TaFundDeal) {
		
		return taFundDealMapper.insert(TaFundDeal);
	}

	@Override
	public int deleteTaFundDeal(TaFundDeal TaFundDeal) {
		taFundDealMapper.deleteTaFundDealBydealNo(TaFundDeal.getDealNo());
		return 1;
	}

	@Override
	public int updateTaFundDeal(TaFundDeal TaFundDeal) {
		return taFundDealMapper.updateByPrimaryKey(TaFundDeal);
	}

	@Override
	public Page<TaFundDeal> getTaFundDealList(Map<String, Object> params, RowBounds rb) {
		return taFundDealMapper.getTaFundDealList(params, rb);
	}

	@Override
	public TaFundDeal getTaFundDeal(Map<String, Object> params, RowBounds rb) {
		
		return taFundDealMapper.getTaFundDeal(params, rb);
	}

}
