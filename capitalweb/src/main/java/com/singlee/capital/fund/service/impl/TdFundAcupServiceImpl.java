package com.singlee.capital.fund.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;
import com.singlee.capital.fund.model.TdDurationFundRedeem;
import com.singlee.capital.fund.model.TdDurationFundReinvest;
import com.singlee.capital.fund.model.TdDurationFundTrans;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
@Service
public class TdFundAcupServiceImpl implements  TdFundAcupService{
	
	@Resource
	BookkeepingService bookkeepingService;
	@Resource
	TdAccTrdDailyMapper tdAccTrdDailyMapper;
	@Resource
	TdProductFundTposService tdProductFundTposService;
	
	@Override
	public String acupInterface(Map<String,Object> infoMap,Map<String,Object> amtMap) {
		InstructionPackage inst = new InstructionPackage();
		infoMap.put("invtype",(infoMap.get("invType") == null) ? infoMap.get("invtype") : infoMap.get("invType"));
		inst.setInfoMap(infoMap);//业务信息
		inst.setAmtMap(amtMap);//金额信息
		return bookkeepingService.generateEntry4Instruction(inst);
	}

	@Override
	public int insertTdAccTrdDaily(Map<String, Object> params) {
		TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
		BeanUtil.populate(TdAccTrdDaily.class, params);
		return tdAccTrdDailyMapper.insert(tdAccTrdDaily);
	}

	@Override
	public int fundAcupManager(Object object) {
		int result = 0;
		//基金申购
		if(object.getClass() == ProductApproveFund.class){
			ProductApproveFund fund = (ProductApproveFund)object;
			if(object != null){
				//插入每日金额过渡表
				TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(fund.getAmt().doubleValue());
				tdAccTrdDaily.setDealNo(fund.getDealNo());
				tdAccTrdDaily.setOpTime(fund.getDealDate());
				tdAccTrdDaily.setPostDate(fund.getDealDate());
				tdAccTrdDaily.setAccType(DurationConstants.AccType.APPLY_FUND_AMT);
				tdAccTrdDailyMapper.insert(tdAccTrdDaily);
				TdAccTrdDaily tdAccTrdDaily2 = new TdAccTrdDaily();
				tdAccTrdDaily2.setAccAmt(fund.getAmt().doubleValue());
				tdAccTrdDaily2.setDealNo(fund.getDealNo());
				tdAccTrdDaily2.setOpTime(fund.getDealDate());
				tdAccTrdDaily2.setPostDate(fund.getDealDate());
				tdAccTrdDaily2.setAccType(DurationConstants.AccType.APPLY_FUND_CASHDIV_AMT);
				int flag = tdAccTrdDailyMapper.insert(tdAccTrdDaily2);
				if(flag >0){
					//调用账务处理服务
					Map<String,Object> amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.APPLY_FUND_AMT,fund.getAmt());//放款会计事件
					amtMap.put(DurationConstants.AccType.APPLY_FUND_CASHDIV_AMT, fund.getCashdivAmt());
					this.acupInterface(BeanUtil.beanToMap(fund),amtMap);
					//基金持仓操作
					Map<String,Object> param = new HashMap<String, Object>();
					param.put("utAmt", fund.getAmt());
					param.put("utQty", fund.getShareAmt());
					param.put("fundCode", fund.getFundCode());
					param.put("postdate", fund.gettDate());
					param.put("prdNo", fund.getPrdNo());
					param.put("invtype", fund.getInvtype());
					param.put("ccy", fund.getCcy());
					param.put("cNo", fund.getcNo());
					param.put("opType", "0");//操作类型为申购
					result = tdProductFundTposService.fundTposManager(param);
				}else{
					result = 0;
				}
			}
		}
		//基金分红
		if(object.getClass() == TdDurationFundCashdiv.class){
			TdDurationFundCashdiv fund = (TdDurationFundCashdiv)object;
			if(object != null){
				//不管是否清算 都要出宣告日的账务
				//插入每日金额过渡表
				TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(fund.getCashdivAmt().doubleValue());
				tdAccTrdDaily.setDealNo(fund.getDealNo());
				tdAccTrdDaily.setOpTime(fund.getvDate());
				tdAccTrdDaily.setPostDate(fund.getvDate());
				tdAccTrdDaily.setAccType(DurationConstants.AccType.APPCASHDIV_FUND_AMT);
				int flag = tdAccTrdDailyMapper.insert(tdAccTrdDaily);
				if(flag >0){
					//调用账务处理服务
					Map<String,Object> amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.APPCASHDIV_FUND_AMT,fund.getCashdivAmt());//放款会计事件
					this.acupInterface(BeanUtil.beanToMap(fund),amtMap);
					//基金持仓操作
					Map<String,Object> param = new HashMap<String, Object>();
					param.put("shAmt", fund.getCashdivAmt());
					param.put("fundCode", fund.getFundCode());
					param.put("postdate", fund.gettDate());
					param.put("prdNo", fund.getPrdNo());
					param.put("cNo", fund.getcNo());
					param.put("invtype", fund.getInvType());
					param.put("opType", "2");//操作类型为分红
					param.put("ccy", fund.getCcy());
					result = tdProductFundTposService.fundTposManager(param);
				}else{
					result = 0;
				}
			}
		}
		//基金赎回
		if(object.getClass() == TdDurationFundRedeem.class){
			TdDurationFundRedeem fund = (TdDurationFundRedeem)object;
			if(object != null && "0".equals(fund.getIsSettl())){
				//插入每日金额过渡表
				TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(fund.getRedeemAmt().doubleValue());
				tdAccTrdDaily.setDealNo(fund.getDealNo());
				tdAccTrdDaily.setOpTime(fund.getvDate());
				tdAccTrdDaily.setPostDate(fund.getvDate());
				tdAccTrdDaily.setAccType(DurationConstants.AccType.REDEEM_FUND_AMT);
				tdAccTrdDailyMapper.insert(tdAccTrdDaily);
				
				TdAccTrdDaily amtdiv = new TdAccTrdDaily();
				amtdiv.setAccAmt(fund.getDividendAmt().doubleValue());
				amtdiv.setDealNo(fund.getDealNo());
				amtdiv.setOpTime(fund.getvDate());
				amtdiv.setPostDate(fund.getvDate());
				amtdiv.setAccType(DurationConstants.AccType.REDEEM_FUND_AMTDIV);
				int flag = tdAccTrdDailyMapper.insert(amtdiv);
				
				if(flag >0){
					//调用账务处理服务
					Map<String,Object> amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.REDEEM_FUND_AMT,fund.getRedeemAmt());//放款会计事件
					amtMap.put(DurationConstants.AccType.REDEEM_FUND_AMTDIV,fund.getDividendAmt());//放款会计事件
					this.acupInterface(BeanUtil.beanToMap(fund),amtMap);
					//基金持仓操作
					HashMap<String, Object> mapTp = new HashMap<String, Object>();
					mapTp.put("utAmt",fund.getRedeemAmt()); //赎回金额
					mapTp.put("utQty", fund.getRedeemQty());//赎回份额
					mapTp.put("shAmt", fund.getDividendAmt());//分红金额
					mapTp.put("fundCode", fund.getFundCode());//基金代码
					mapTp.put("opType", "1");//操作类型为赎回
					mapTp.put("cNo", fund.getcNo());
					mapTp.put("prdNo", fund.getPrdNo());
					mapTp.put("invtype", fund.getInvType());
					mapTp.put("ccy", fund.getCcy());
					result = tdProductFundTposService.fundTposManager(mapTp);
				}else{
					result = 0;
				}
			}
		}
		//基金转换
		if(object.getClass() == TdDurationFundTrans.class){
			TdDurationFundTrans fund = (TdDurationFundTrans)object;
			if(object != null){
				HashMap<String, Object> mapTp = new HashMap<String, Object>();
				mapTp.put("fundCode",fund.getInFundCode());//转入基金代码
				mapTp.put("utQty", fund.getTurnInQty());//转入基金份额
				mapTp.put("utAmt", fund.getTurnInAmt());//转入基金金额
				mapTp.put("opType", "4");//操作类型为4 基金转换
				mapTp.put("transType", "1");//1:转入 2:转出
				mapTp.put("prdNo", fund.getPrdNo());
				mapTp.put("cNo", fund.getcNo());
				mapTp.put("invtype", fund.getInvType());
				int i=tdProductFundTposService.fundTposManager(mapTp);
				mapTp = new HashMap<String, Object>();
				mapTp.put("fundCode",fund.getOutFundCode());//转入基金代码
				mapTp.put("utQty", fund.getTurnOutQty());//转入基金份额
				mapTp.put("utAmt", fund.getTurnOutAmt());//转入基金金额
				mapTp.put("opType", "4");//操作类型为4 基金转换
				mapTp.put("transType", "2");//1:转入 2:转出
				mapTp.put("prdNo", fund.getPrdNo());
				mapTp.put("cNo", fund.getcNo());
				mapTp.put("invtype", fund.getInvType());
				mapTp.put("ccy", fund.getCcy());
				int n = tdProductFundTposService.fundTposManager(mapTp);
				result = i+n;
			}
		}
		//基金红利再投
		if(object.getClass() == TdDurationFundReinvest.class){
			TdDurationFundReinvest fund = (TdDurationFundReinvest)object;
			if(object != null){
				//插入每日金额过渡表
				TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(fund.getCashdivAmt().doubleValue());
				tdAccTrdDaily.setDealNo(fund.getDealNo());
				tdAccTrdDaily.setOpTime(fund.getvDate());
				tdAccTrdDaily.setPostDate(fund.getvDate());
				tdAccTrdDaily.setAccType(DurationConstants.AccType.REINVEST_FUND_AMT);
				int flag = tdAccTrdDailyMapper.insert(tdAccTrdDaily);
				if(flag >0){
					//调用账务处理服务
					Map<String,Object> amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.REINVEST_FUND_AMT,fund.getCashdivAmt());//放款会计事件
					this.acupInterface(BeanUtil.beanToMap(fund),amtMap);
					//基金持仓操作
					HashMap<String, Object> mapTp = new HashMap<String, Object>();
					mapTp.put("shAmt",fund.getCashdivAmt()); //红利金额
					mapTp.put("fundCode", fund.getFundCode());//基金代码
					mapTp.put("opType", "3");//操作类型为红利再投
					mapTp.put("prdNo", fund.getPrdNo());
					mapTp.put("cNo", fund.getcNo());
					mapTp.put("invtype", fund.getInvType());
					mapTp.put("ccy", fund.getCcy());
					result = tdProductFundTposService.fundTposManager(mapTp);
				}else{
					result = 0;
				}
			}
		}
		return result;
	}
}
