package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TaFundDeal;

public interface TaFundDealMapper extends Mapper<TaFundDeal>{
	
	public void deleteTaFundDealBydealNo(String dealNo);
	public Page<TaFundDeal> getTaFundDealList(Map<String, Object> params, RowBounds rb);
	public TaFundDeal getTaFundDeal(Map<String, Object> params, RowBounds rb);
}
