package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.counterparty.mapper.TtCounterPartyAccMapper;
import com.singlee.capital.counterparty.model.TtCounterPartyAcc;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TaFundDeal;
import com.singlee.capital.fund.service.ProductApproveFundService;
import com.singlee.capital.fund.service.TaFundDealService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * 现金流controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/ProductApproveFundController")
public class ProductApproveFundController extends CommonController{
	
	@Resource 
	ProductApproveFundService productApproveFundService;
	/** 审批单管理dao **/
	@Resource
	private TrdOrderMapper approveManageDao;
	@Resource
	private TaFundDealService taFundDealService;
	/** 交易对手服务接口 spring 负责注入 */
	@Resource
	private TtCounterPartyAccMapper ttCounterPartyAccMapper;
	
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveFund")
	public RetMsg<PageInfo<ProductApproveFund>> searchProductApproveFund(@RequestBody Map<String,Object> map){
		Page<ProductApproveFund> page  = productApproveFundService.getProductApproveFundList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getProductApproveFund")
	public RetMsg<ProductApproveFund> getProductApproveFund(@RequestBody Map<String, Object> map){
		ProductApproveFund page  = productApproveFundService.getProductApproveFund(map);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertProductApproveFund")
	public RetMsg<Serializable> insertProductApproveFund(@RequestBody ProductApproveFund map) {
		HashMap<String, Object> mapSel = new HashMap<String, Object>();
		mapSel.put("trdtype", "HJ");
		String order_id = approveManageDao.getOrderId(mapSel);
		map.setDealNo(order_id);
		map.setVersion(1);
		productApproveFundService.insertProductApproveFund(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateProductApproveFund")
	public RetMsg<Serializable> updateProductApproveFund(@RequestBody ProductApproveFund map) {
		productApproveFundService.updateProductApproveFund(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteProductApproveFund")
	public RetMsg<Serializable> deleteProductApproveFund(@RequestBody ProductApproveFund map) {
		productApproveFundService.deleteProductApproveFund(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 *查询基金资料副本 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTaFundDeal")
	public RetMsg<TaFundDeal> searchTaFundDeal(@RequestBody Map<String,Object> map){
		TaFundDeal page  = taFundDealService.getTaFundDeal(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/searchCounterParty")
	public RetMsg<TtCounterPartyAcc> searchCounterParty(@RequestBody Map<String,Object> map){
		TtCounterPartyAcc vo = ttCounterPartyAccMapper.selectCounterPartyAcc(map);
		return RetMsgHelper.ok(vo);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveFundByMyself")
	public RetMsg<PageInfo<ProductApproveFund>> searchPageProductApproveFundByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<ProductApproveFund> page = productApproveFundService.searchPageProductApproveFundMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveFundUnfinished")
	public RetMsg<PageInfo<ProductApproveFund>> searchPageProductApproveFundUnfinished(@RequestBody Map<String,Object> params){
		Page<ProductApproveFund> page  = new Page<ProductApproveFund>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			String approveStatusArr =  DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId());  
			page = productApproveFundService.searchPageProductApproveFund(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveFundFinished")
	public RetMsg<PageInfo<ProductApproveFund>> searchPageProductApproveFundFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<ProductApproveFund> page = productApproveFundService.searchPageProductApproveFundFinished(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
//	public RetMsg<PageInfo<ProductApproveFund>>  searchPageProductApproveFundByPrdNoAndFundCode(@RequestBody Map<String,Object> params){
//		
//	}
	
	
}
