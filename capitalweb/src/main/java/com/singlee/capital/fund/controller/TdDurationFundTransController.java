package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.fund.model.TdDurationFundTrans;
import com.singlee.capital.fund.service.TdDurationFundTransService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;


/*
 * 
 */
@Controller
@RequestMapping(value = "/TdDurationFundTransController")
public class TdDurationFundTransController extends CommonController {
	
	@Resource
	TdDurationFundTransService TdDurationFundTransService;
	@Resource
	private TrdOrderMapper approveManageDao;
	
	@ResponseBody
	@RequestMapping(value = "/searchTdDurationFundTrans")
	public RetMsg<PageInfo<TdDurationFundTrans>> searchTdDurationFundTrans(@RequestBody Map<String,Object> map){
		Page<TdDurationFundTrans> page  = TdDurationFundTransService.getTdDurationFundTransList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	} 
	@ResponseBody
	@RequestMapping(value = "/getTdDurationFundTransByDealNo")
	public RetMsg<TdDurationFundTrans> getTdDurationFundTransByDealNo(@RequestBody Map<String,Object> map){
		TdDurationFundTrans tdDurationFundTrans = new TdDurationFundTrans();
		tdDurationFundTrans.setDealNo(ParameterUtil.getString(map, "dealNo", ""));
		tdDurationFundTrans  = TdDurationFundTransService.getTdDurationFundTransByDealNo(tdDurationFundTrans);
		return RetMsgHelper.ok(tdDurationFundTrans);
	}
	
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertTdDurationFundTrans")
	public RetMsg<Serializable> insertTdDurationFundTrans(@RequestBody HashMap<String, Object> map) {
		try {
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "SH");
			String order_id = approveManageDao.getOrderId(mapSel);
			TdDurationFundTrans deem = new TdDurationFundTrans();
			BeanUtils.populate(deem, map);
			deem.setDealNo(order_id);
			deem.setVersion(1);
			TdDurationFundTransService.insertTdDurationFundTrans(deem);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTdDurationFundTrans")
	public RetMsg<Serializable> updateTdDurationFundTrans(@RequestBody TdDurationFundTrans map) {
		TdDurationFundTransService.updateTdDurationFundTrans(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteTdDurationFundTrans")
	public RetMsg<Serializable> deleteProductApproveFund(@RequestBody TdDurationFundTrans map) {
		TdDurationFundTransService.deleteTdDurationFundTrans(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 发起审批
	 * @param map
	 * @return
	 */
	public RetMsg<Serializable> approveCommit(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundTransByMyself")
	public RetMsg<PageInfo<TdDurationFundTrans>> searchPageTdDurationFundTransByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TdDurationFundTrans> page = TdDurationFundTransService.searchPageTdDurationFundTranstMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundTransUnfinished")
	public RetMsg<PageInfo<TdDurationFundTrans>> searchPageTdDurationFundTransUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		params.put("insId", SlSessionHelper.getInstitutionId());  
		Page<TdDurationFundTrans> page = TdDurationFundTransService.searchPageTdDurationFundTranst(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundTransFinished")
	public RetMsg<PageInfo<TdDurationFundTrans>> TdDurationFundTrans(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdDurationFundTrans> page =  TdDurationFundTransService.searchPageTdDurationFundTranstFinished(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
}
