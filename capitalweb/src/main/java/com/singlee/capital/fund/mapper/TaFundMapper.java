package com.singlee.capital.fund.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TaFund;

public interface TaFundMapper extends Mapper<TaFund>{
	
	public void deleteTaFundByfundCode(String fundCode);
	public int updateTaFundByfundCode(String fundCode);
	public Page<TaFund> getTaFundList(Map<String, Object> params, RowBounds rb);
	public List<TaFund> getTaFundList(Map<String, Object> params);
	public TaFund getTaFund(Map<String, Object> params, RowBounds rb);
	public TaFund getTaFund(Map<String, Object> params);
}
