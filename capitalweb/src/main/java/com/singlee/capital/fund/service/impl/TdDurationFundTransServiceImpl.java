package com.singlee.capital.fund.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.mapper.TdDurationFundTransMapper;
import com.singlee.capital.fund.model.TdDurationFundTrans;
import com.singlee.capital.fund.service.TdDurationFundTransService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

@Service("tdDurationFundTransService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdDurationFundTransServiceImpl implements TdDurationFundTransService,SlbpmCallBackInteface,TaskEventInterface{
	
	@Resource
	TdDurationFundTransMapper tdDurationFundTransMapper;
	@Resource
	TdProductFundTposService tdProductFundTposService;

	@Override
	public List<TdDurationFundTrans> searchTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans) {
		
		return tdDurationFundTransMapper.selectAll();
	}

	@Override
	public int insertTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans) {
		int i= tdDurationFundTransMapper.insert(tdDurationFundTrans);
		/*int flag = 0;
		if(i>0){
			flag = this.fundTposCheckModel(tdDurationFundTrans);
		}*/
		return i;
	}

	@Override
	public int deleteTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans) {
		tdDurationFundTransMapper.deleteTdDurationFundTransDealno(tdDurationFundTrans.getDealNo());
		return 1;
	}
	
	@Override
	public int updateTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans) {
		return tdDurationFundTransMapper.updateByPrimaryKey(tdDurationFundTrans);
	}

	@Override
	public Page<TdDurationFundTrans> getTdDurationFundTransList(Map<String, Object> params, RowBounds rb) {
		return tdDurationFundTransMapper.getTdDurationFundTransList(params, rb);
	}
	
	@Override
	public TdDurationFundTrans getTdDurationFundTransByDealNo(TdDurationFundTrans tdDurationFundTrans) {
		return tdDurationFundTransMapper.getTdDurationFundTransByDealNo(tdDurationFundTrans);
	}
	
	
	public int fundTposCheckModel(TdDurationFundTrans tdDurationFundTrans){
		HashMap<String, Object> mapTp = new HashMap<String, Object>();
		mapTp = new HashMap<String, Object>();
		mapTp.put("fundCode",tdDurationFundTrans.getOutFundCode());//转出基金代码
		mapTp.put("utQty", tdDurationFundTrans.getTurnOutQty());//转出基金份额
		mapTp.put("utAmt", tdDurationFundTrans.getTurnOutAmt());//转出基金金额
		mapTp.put("opType", "4");//操作类型为4 基金转换
		mapTp.put("transType", "2");//1:转入 2:转出
		mapTp.put("prdNo", tdDurationFundTrans.getPrdNo());
		mapTp.put("invtype", tdDurationFundTrans.getOutInvType());
		int n = tdProductFundTposService.fundTposManager(mapTp);
		
		mapTp.put("ccy", tdDurationFundTrans.getCcy());
		mapTp.put("cNo", tdDurationFundTrans.getcNo());
		mapTp.put("fundCode",tdDurationFundTrans.getInFundCode());//转入基金代码
		mapTp.put("utQty", tdDurationFundTrans.getTurnInQty());//转入基金份额
		mapTp.put("utAmt", tdDurationFundTrans.getTurnInAmt());//转入基金金额
		mapTp.put("opType", "4");//操作类型为4 基金转换
		mapTp.put("transType", "1");//1:转入 2:转出
		mapTp.put("prdNo", tdDurationFundTrans.getPrdNo());
		mapTp.put("invtype", tdDurationFundTrans.getInInvType());
		mapTp.put("postdate",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		int i=tdProductFundTposService.fundTposManager(mapTp);
		
		int k = i+n;
		return k < 2 ? 0 :1;
	}

	@Override
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranst(
			Map<String, Object> params, RowBounds rb){
		return tdDurationFundTransMapper.searchPageTdDurationFundTranst(params,rb);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		map.put("approveStatus", status);
		//更新状态哦
		tdDurationFundTransMapper.updateApproveStatus(map);
		if("6".equals(status)){
			TdDurationFundTrans tdDurationFundTrans = tdDurationFundTransMapper.selectByPrimaryKey(map);
			this.fundTposCheckModel(tdDurationFundTrans);
		}
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstFinished(
			Map<String, Object> params, RowBounds rb) {
		// TODO Auto-generated method stub
		return tdDurationFundTransMapper.searchPageTdDurationFundTranstFinished(params, rb);
	}

	@Override
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstMySelf(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundTransMapper.searchPageTdDurationFundTranstMySelf(params, rb);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

}
