package com.singlee.capital.fund.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.fund.mapper.TdDurationFundRedeemMapper;
import com.singlee.capital.fund.model.TdDurationFundRedeem;
import com.singlee.capital.fund.service.TdDurationFundRedeemService;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

/**
 * 基金赎回
 * @author SL_LXY
 *
 */
@Service("tdDurationFundRedeemService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdDurationFundRedeemServiceImpl implements TdDurationFundRedeemService,SlbpmCallBackInteface,TaskEventInterface{
	
	@Resource
	TdDurationFundRedeemMapper tdDurationFundRedeemMapper;
	@Resource
	TdProductFundTposService tdProductFundTposService;
	@Resource
	TdFundAcupService tdFundAcupService;

	@Override
	public List<TdDurationFundRedeem> searchTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem) {
		
		return tdDurationFundRedeemMapper.selectAll();
	}

	@Override
	public int insertTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem) {
		tdDurationFundRedeem.setCfType(DictConstants.TrdType.FundRedeem);
		int i =tdDurationFundRedeemMapper.insert(tdDurationFundRedeem);
		return i;
	}

	@Override
	public int deleteTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem) {
		tdDurationFundRedeemMapper.deleteTdDurationFundRedeemDealno(tdDurationFundRedeem.getDealNo());
		return 1;
	}

	@Override
	public int updateTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem) {
		tdDurationFundRedeem.setCfType(DictConstants.TrdType.FundRedeem);
		return tdDurationFundRedeemMapper.updateByPrimaryKey(tdDurationFundRedeem);
	}

	@Override
	public Page<TdDurationFundRedeem> getTdDurationFundRedeemList(Map<String, Object> params, RowBounds rb) {
		return tdDurationFundRedeemMapper.getTdDurationFundRedeemList(params, rb);
	}
	
	/**
	 * 更新基金持仓操作
	 * @param tdDurationFundRedeem
	 * @return
	 */
	/*
	public int fundTposCheckModel(TdDurationFundRedeem tdDurationFundRedeem){
		HashMap<String, Object> mapTp = new HashMap<String, Object>();
		mapTp.put("utAmt",tdDurationFundRedeem.getRedeemAmt()); //赎回金额
		mapTp.put("utQty", tdDurationFundRedeem.getRedeemQty());//赎回份额
		mapTp.put("shAmt", tdDurationFundRedeem.getDividendAmt());//分红金额
		mapTp.put("fundCode", tdDurationFundRedeem.getFundCode());//基金代码
		mapTp.put("opType", "1");//操作类型为赎回
		return tdProductFundTposService.fundTposManager(mapTp);
	}
	 */
	@Override
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeem(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundRedeemMapper.searchPageTdDurationFundRedeem(params,rb);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		TdDurationFundRedeem tdDurationFundRedeem = new TdDurationFundRedeem();
		tdDurationFundRedeem.setDealNo(serial_no);
		return tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(BeanUtil.beanToMap(tdDurationFundRedeem));
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		map.put("approveStatus", status);
		TdDurationFundRedeem fund = tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(map);
		//更新状态哦
		if(status.equals(com.singlee.slbpm.dict.DictConstants.ApproveStatus.ApprovedPass)) {
			int result = tdFundAcupService.fundAcupManager(fund);
			if(result > 0){
				System.out.println("基金申购账务处理成功！");
			}
		}
		tdDurationFundRedeemMapper.updateApproveStatus(map);
		
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemFinished(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundRedeemMapper.searchPageTdDurationFundRedeemFinished(params, rb);
	}

	@Override
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemMySelf(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundRedeemMapper.searchPageTdDurationFundRedeemMySelf(params, rb);
	}

	@Override
	public TdDurationFundRedeem getTdDurationFundRedeem(
			Map<String, Object> params) {
		return tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(params);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }
}
