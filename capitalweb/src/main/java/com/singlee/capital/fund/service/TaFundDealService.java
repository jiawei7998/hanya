package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TaFundDeal;

public interface TaFundDealService {

	List<TaFundDeal> searchTaFundDeal(TaFundDeal TaFundDealDeal);
	Page<TaFundDeal> getTaFundDealList(Map<String, Object> params, RowBounds rb);
	TaFundDeal getTaFundDeal(Map<String, Object> params, RowBounds rb);
	int insertTaFundDeal(TaFundDeal TaFundDeal);
	int updateTaFundDeal(TaFundDeal TaFundDeal);
	int deleteTaFundDeal(TaFundDeal TaFundDeal);
}
