package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundRedeem;

public interface TdDurationFundRedeemMapper extends Mapper<TdDurationFundRedeem>{
	
	public void deleteTdDurationFundRedeemDealno(String dealno);
	public Page<TdDurationFundRedeem> getTdDurationFundRedeemList(Map<String, Object> params, RowBounds rb);
	Page<TdDurationFundRedeem> searchPageTdDurationFundRedeem(Map<String, Object> params, RowBounds rb);
	public int updateApproveStatus(Map<String, Object> params);
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemFinished(
			Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemMySelf(
			Map<String, Object> params, RowBounds rb);
	public TdDurationFundRedeem getTdDurationFundRedeemByDealNo(Map<String, Object> params);
}
