package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TaFund;

public interface TaFundService {

	List<TaFund> searchTaFund(TaFund taFund);
	Page<TaFund> getTaFundList(Map<String, Object> params, RowBounds rb);
	TaFund getTaFund(Map<String, Object> params, RowBounds rb);
	TaFund getTaFund(Map<String, Object> params);
	void insertTaFund(TaFund taFund);
	int updateTaFund(TaFund taFund);
	void deleteTaFund(TaFund taFund);
}
