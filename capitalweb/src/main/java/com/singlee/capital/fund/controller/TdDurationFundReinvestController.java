package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.fund.model.TdDurationFundReinvest;
import com.singlee.capital.fund.service.TdDurationFundReinvestService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;


/*
 * 
 */
@Controller
@RequestMapping(value = "/TdDurationFundReinvestController")
public class TdDurationFundReinvestController extends CommonController {
	
	@Resource
	TdDurationFundReinvestService tdDurationFundReinvestService;
	/**基金持仓操作 **/
	
	@ResponseBody
	@RequestMapping(value = "/searchTdDurationFundReinvest")
	public RetMsg<PageInfo<TdDurationFundReinvest>> searchTdDurationFundReinvest(@RequestBody Map<String,Object> map){
		Page<TdDurationFundReinvest> page  = tdDurationFundReinvestService.getTdDurationFundReinvestList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	} 
	
	@ResponseBody
	@RequestMapping(value = "/searchTdDurationFundReinvestByDealNo")
	public RetMsg<TdDurationFundReinvest> searchTdDurationFundReinvestByDealNo(@RequestBody Map<String,Object> map){
		TdDurationFundReinvest tdDurationFundReinvest = new TdDurationFundReinvest();
		tdDurationFundReinvest = tdDurationFundReinvestService.getTdDurationFundReinvestByDealNo(ParameterUtil.getString(map, "dealNo", ""));
		return RetMsgHelper.ok(tdDurationFundReinvest);
	} 
	
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertTdDurationFundReinvest")
	public RetMsg<Serializable> insertTdDurationFundReinvest(@RequestBody HashMap<String, Object> map) {
		tdDurationFundReinvestService.insertTdDurationFundReinvest(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTdDurationFundReinvest")
	public RetMsg<Serializable> updateTdDurationFundReinvest(@RequestBody TdDurationFundReinvest map) {
		tdDurationFundReinvestService.updateTdDurationFundReinvest(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteTdDurationFundReinvest")
	public RetMsg<Serializable> deleteProductApproveFund(@RequestBody TdDurationFundReinvest map) {
		tdDurationFundReinvestService.deleteTdDurationFundReinvest(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 发起审批
	 * @param map
	 * @return
	 */
	public RetMsg<Serializable> approveCommit(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundReinvestByMyself")
	public RetMsg<PageInfo<TdDurationFundReinvest>> searchPageTdDurationFundReinvestByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TdDurationFundReinvest> page = tdDurationFundReinvestService.searchPageTdDurationFundReinvestMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundReinvestUnfinished")
	public RetMsg<PageInfo<TdDurationFundReinvest>> searchPageTdDurationFundReinvestUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		params.put("insId", SlSessionHelper.getInstitutionId());  
		Page<TdDurationFundReinvest> page = tdDurationFundReinvestService.searchPageTdDurationFundReinvest(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundReinvestFinished")
	public RetMsg<PageInfo<TdDurationFundReinvest>> searchPageTdDurationFundReinvestFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdDurationFundReinvest> page =  tdDurationFundReinvestService.searchPageTdDurationFundReinvestFinished(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
}
