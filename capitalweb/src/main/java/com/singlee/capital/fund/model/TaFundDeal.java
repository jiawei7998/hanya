package com.singlee.capital.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TD_FUND_DEAL")
public class TaFundDeal implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;
	private String fundCode;
	
	private String fundName;
	private String estDate;
	private String invType;
	private String invName;
	private String managComp;
	private String custodian;
	private BigDecimal managRate;
	private BigDecimal trustRate;
	private BigDecimal applyRate;
	private BigDecimal redeemRate;
	private BigDecimal coverMode;
	private int coverDay;
	private String remark;
	private String fundCircle;
	private String ccy;
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getEstDate() {
		return estDate;
	}
	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	public String getInvName() {
		return invName;
	}
	public void setInvName(String invName) {
		this.invName = invName;
	}
	public String getManagComp() {
		return managComp;
	}
	public void setManagComp(String managComp) {
		this.managComp = managComp;
	}
	public String getCustodian() {
		return custodian;
	}
	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}
	public BigDecimal getManagRate() {
		return managRate;
	}
	public void setManagRate(BigDecimal managRate) {
		this.managRate = managRate;
	}
	public BigDecimal getTrustRate() {
		return trustRate;
	}
	public void setTrustRate(BigDecimal trustRate) {
		this.trustRate = trustRate;
	}
	public BigDecimal getApplyRate() {
		return applyRate;
	}
	public void setApplyRate(BigDecimal applyRate) {
		this.applyRate = applyRate;
	}
	public BigDecimal getRedeemRate() {
		return redeemRate;
	}
	public void setRedeemRate(BigDecimal redeemRate) {
		this.redeemRate = redeemRate;
	}
	public BigDecimal getCoverMode() {
		return coverMode;
	}
	public void setCoverMode(BigDecimal coverMode) {
		this.coverMode = coverMode;
	}
	public int getCoverDay() {
		return coverDay;
	}
	public void setCoverDay(int coverDay) {
		this.coverDay = coverDay;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFundCircle() {
		return fundCircle;
	}
	public void setFundCircle(String fundCircle) {
		this.fundCircle = fundCircle;
	}
	
}
