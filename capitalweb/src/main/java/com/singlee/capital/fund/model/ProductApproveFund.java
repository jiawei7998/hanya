package com.singlee.capital.fund.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TD_PRODUCT_APPROVE_FUND")
public class ProductApproveFund implements Serializable{
	
	/**
	 * tdDurationFundCashdiv
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//审批单编号 自动生成
	private String prdNo;//产品编号 序列自动生成
	private String dealType; //	审批单类型 1-审批 2-核实
	private String cNo;//交易对手编号 交易对手表的id
	private String contact;//联系人
	private String contactPhone;//	联系电话
	private String contactAddr;//联系地址
	private String conTitle;//产品名称
	private String sponsor;//	审批发起人
	private String sponInst;//	审批发起机构
	private String aDate;//审批开始日期
	private String ccy;//币种
	private BigDecimal amt;//本金(交易金额)
	private String dealDate;//交易日期
	private String vDate;//起息日期
	private String tDate;//"划款日期 (运营岗等待到划款日期到时才进行审批。若划款日期变更则要加签到业务岗。)"
	private String fundCode;//基金代码
	private String image;//资料影像
	private String lastUpdate;//更新时间
	private int version;//版本
	private String isActive;//是否有效
	private String selfAccCode;//本方账号
	private String selfAccName;//本方账户名称
	private String selfBankCode;//本方开户行行号
	private String selfBankName;//本方开户行名称
	private String partyAccCode;//对手方账号
	private String partyAccName;//对手方账户名称
	private String partyBankCode;//对手方开户行行号
	private String partyBankName;//对手方开户行名称
	private BigDecimal price;//交易价格
	private BigDecimal shareAmt;//交易份额（元）
	private String yieldDate;//参考收益率日期
	private String d7Yield;//参考收益率_%  最新公布的近7天
	private String platformInv;//平台投向
	private String eventuallyInv;//最终投向
	private String invtype;//资产会计四分类
	private String incomeType;//收益类型
	private String remark;//补充说明
	private String inOutFlag;//行内行外  1-行内  2-行外
	/**
	 * -- Add/modify columns 
		alter table TD_PRODUCT_APPROVE_FUND add party_ecif_code VARCHAR2(32);
		-- Add comments to the columns 
		comment on column TD_PRODUCT_APPROVE_FUND.party_ecif_code
		  is '参与方客户号';
	    <input id='partyEcifCode' type='hidden' />
	 */
	private String partyEcifCode;
	
	
	public String getPartyEcifCode() {
		return partyEcifCode;
	}
	public void setPartyEcifCode(String partyEcifCode) {
		this.partyEcifCode = partyEcifCode;
	}
	public String getAcctType() {
		return acctType;
	}
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	private String isSettl;//是否清算  1-清算 2-不清算
	private String approveStatus;//审批单状态
	private String fundType;//债基类型
	private BigDecimal cashdivAmt;//红利金额
	private String acctType;//清算方式 
	
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactAddr() {
		return contactAddr;
	}
	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}
	public String getConTitle() {
		return conTitle;
	}
	public void setConTitle(String conTitle) {
		this.conTitle = conTitle;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String gettDate() {
		return tDate;
	}
	public void settDate(String tDate) {
		this.tDate = tDate;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getSelfAccCode() {
		return selfAccCode;
	}
	public void setSelfAccCode(String selfAccCode) {
		this.selfAccCode = selfAccCode;
	}
	public String getSelfAccName() {
		return selfAccName;
	}
	public void setSelfAccName(String selfAccName) {
		this.selfAccName = selfAccName;
	}
	public String getSelfBankCode() {
		return selfBankCode;
	}
	public void setSelfBankCode(String selfBankCode) {
		this.selfBankCode = selfBankCode;
	}
	public String getSelfBankName() {
		return selfBankName;
	}
	public void setSelfBankName(String selfBankName) {
		this.selfBankName = selfBankName;
	}
	public String getPartyAccCode() {
		return partyAccCode;
	}
	public void setPartyAccCode(String partyAccCode) {
		this.partyAccCode = partyAccCode;
	}
	public String getPartyAccName() {
		return partyAccName;
	}
	public void setPartyAccName(String partyAccName) {
		this.partyAccName = partyAccName;
	}
	public String getPartyBankCode() {
		return partyBankCode;
	}
	public void setPartyBankCode(String partyBankCode) {
		this.partyBankCode = partyBankCode;
	}
	public String getPartyBankName() {
		return partyBankName;
	}
	public void setPartyBankName(String partyBankName) {
		this.partyBankName = partyBankName;
	}
	public String getYieldDate() {
		return yieldDate;
	}
	public void setYieldDate(String yieldDate) {
		this.yieldDate = yieldDate;
	}
	public String getD7Yield() {
		return d7Yield;
	}
	public void setD7Yield(String d7Yield) {
		this.d7Yield = d7Yield;
	}
	public String getPlatformInv() {
		return platformInv;
	}
	public void setPlatformInv(String platformInv) {
		this.platformInv = platformInv;
	}
	public String getEventuallyInv() {
		return eventuallyInv;
	}
	public void setEventuallyInv(String eventuallyInv) {
		this.eventuallyInv = eventuallyInv;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getIncomeType() {
		return incomeType;
	}
	public void setIncomeType(String incomeType) {
		this.incomeType = incomeType;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getInOutFlag() {
		return inOutFlag;
	}
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	public String getIsSettl() {
		return isSettl;
	}
	public void setIsSettl(String isSettl) {
		this.isSettl = isSettl;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getShareAmt() {
		return shareAmt;
	}
	public void setShareAmt(BigDecimal shareAmt) {
		this.shareAmt = shareAmt;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public BigDecimal getCashdivAmt() {
		return cashdivAmt;
	}
	public void setCashdivAmt(BigDecimal cashdivAmt) {
		this.cashdivAmt = cashdivAmt;
	}
	
}
