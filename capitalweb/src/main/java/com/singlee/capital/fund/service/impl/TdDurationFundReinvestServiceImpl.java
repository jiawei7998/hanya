package com.singlee.capital.fund.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.fund.mapper.TdDurationFundReinvestMapper;
import com.singlee.capital.fund.model.TdDurationFundReinvest;
import com.singlee.capital.fund.service.TdDurationFundReinvestService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

@Service("tdDurationFundReinvestService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdDurationFundReinvestServiceImpl implements TdDurationFundReinvestService,SlbpmCallBackInteface,TaskEventInterface{
	
	@Resource
	TdDurationFundReinvestMapper tdDurationFundReinvestMapper;
	@Resource
	TdFundAcupService tdFundAcupService;
	@Resource
	private TrdOrderMapper approveManageDao;

	@Override
	public List<TdDurationFundReinvest> searchTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest) {
		return tdDurationFundReinvestMapper.selectAll();
	}

	@Override
	public void insertTdDurationFundReinvest(@RequestBody HashMap<String, Object> map) {
		TdDurationFundReinvest deem = null;
		try {
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "SH");
			String order_id = approveManageDao.getOrderId(mapSel);
			deem = new TdDurationFundReinvest();
			BeanUtils.populate(deem, map);
			deem.setDealNo(order_id);
			deem.setVersion(1);
			tdDurationFundReinvestMapper.insert(deem);
		} catch (Exception e) {
			//e.printStackTrace();
			JY.raise(e.getMessage());
		} 
	}

	@Override
	public int deleteTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest) {
		tdDurationFundReinvestMapper.deleteTdDurationFundReinvestDealno(tdDurationFundReinvest.getDealNo());
		return 1;
	}
	
	@Override
	public int updateTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest) {
		return tdDurationFundReinvestMapper.updateByPrimaryKey(tdDurationFundReinvest);
	}

	@Override
	public Page<TdDurationFundReinvest> getTdDurationFundReinvestList(Map<String, Object> params, RowBounds rb) {
		return tdDurationFundReinvestMapper.getTdDurationFundReinvestList(params, rb);
	}

	@Override
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvest(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundReinvestMapper.searchPageTdDurationFundReinvest(params,rb);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		map.put("approveStatus", status);
		if("6".equals(status)){
			TdDurationFundReinvest tdDurationFundReinvest = new TdDurationFundReinvest();
			tdDurationFundReinvest.setDealNo(serial_no);
			tdDurationFundReinvest = tdDurationFundReinvestMapper.getTdDurationFundReinvestByDealNo(tdDurationFundReinvest);
			if(null !=tdDurationFundReinvest){
				//持仓更新，账务处理
				try {
					tdFundAcupService.fundAcupManager(tdDurationFundReinvest);
				} catch (Exception e) {
					//e.printStackTrace();
					throw new RException(e);
				}
			}
		}
		//更新状态哦
		tdDurationFundReinvestMapper.updateApproveStatus(map);
		
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestFinished(
			Map<String, Object> params, RowBounds rb) {
		// TODO Auto-generated method stub
		return tdDurationFundReinvestMapper.searchPageTdDurationFundReinvestFinished(params, rb);
	}

	@Override
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestMySelf(
			Map<String, Object> params, RowBounds rb) {
		// TODO Auto-generated method stub
		return tdDurationFundReinvestMapper.searchPageTdDurationFundReinvestMySelf(params, rb);
	}
	
	@Override
	public TdDurationFundReinvest getTdDurationFundReinvestByDealNo(String dealNo) {
		TdDurationFundReinvest tdDurationFundReinvest = new TdDurationFundReinvest();
		tdDurationFundReinvest.setDealNo(dealNo);
		return tdDurationFundReinvestMapper.getTdDurationFundReinvestByDealNo(tdDurationFundReinvest);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

}
