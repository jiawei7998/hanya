package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundRedeem;

public interface TdDurationFundRedeemService {
	/**
	 * 普通列表查询
	 * @param tdDurationFundRedeem
	 * @return
	 */
	List<TdDurationFundRedeem> searchTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem);
	/**
	 * 普通分页查询
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<TdDurationFundRedeem> getTdDurationFundRedeemList(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据dealNo查询
	 * @param params
	 * @param rb
	 * @return
	 */
	TdDurationFundRedeem getTdDurationFundRedeem(Map<String, Object> params);
	/**
	 * 新增
	 * @param tdDurationFundRedeem
	 * @return
	 */
	int insertTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem);
	/**
	 * 修改
	 * @param tdDurationFundRedeem
	 * @return
	 */
	int updateTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem);
	/**
	 * 删除
	 * @param tdDurationFundRedeem
	 * @return
	 */
	int deleteTdDurationFundRedeem(TdDurationFundRedeem tdDurationFundRedeem);
	/**
	 * 查询（待审批）
	 * @param params
	 * @param type
	 * @return
	 */
	Page<TdDurationFundRedeem> searchPageTdDurationFundRedeem(Map<String, Object> params, RowBounds rb);
	/**
	 * 查询（已完成）
	 * @param params
	 * @param type
	 * @return
	 */
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemFinished(
			Map<String, Object> params, RowBounds rb);
	/**
	 * 查询（发起）
	 * @param params
	 * @param type
	 * @return
	 */
	public Page<TdDurationFundRedeem> searchPageTdDurationFundRedeemMySelf(
			Map<String, Object> params, RowBounds rb);
}
