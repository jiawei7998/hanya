package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;
import com.singlee.capital.fund.service.TdDurationFundCashdivService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;


/*
 * 
 */
@Controller
@RequestMapping(value = "/TdDurationFundCashdivController")
public class TdDurationFundCashdivController extends CommonController {
	
	@Resource
	TdDurationFundCashdivService tdDurationFundCashdivService;
	@Resource
	private TrdOrderMapper approveManageDao;
	
	@ResponseBody
	@RequestMapping(value = "/searchTdDurationFundCashdiv")
	public RetMsg<PageInfo<TdDurationFundCashdiv>> searchTdDurationFundCashdiv(@RequestBody Map<String,Object> map){
		Page<TdDurationFundCashdiv> page  = tdDurationFundCashdivService.getTdDurationFundCashdivList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	} 
	
	@ResponseBody
	@RequestMapping(value = "/getTdDurationFundCashdivByDealNo")
	public RetMsg<TdDurationFundCashdiv> getTdDurationFundCashdivByDealNo(@RequestBody Map<String,Object> map){
		TdDurationFundCashdiv tdDurationFundCashdiv = new TdDurationFundCashdiv();
		tdDurationFundCashdiv.setDealNo(ParameterUtil.getString(map, "dealNo", ""));
		tdDurationFundCashdiv  = tdDurationFundCashdivService.getTdDurationFundCashdiv(tdDurationFundCashdiv);
		return RetMsgHelper.ok(tdDurationFundCashdiv);
	}
	
	
	
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertTdDurationFundCashdiv")
	public RetMsg<Serializable> insertTdDurationFundCashdiv(@RequestBody HashMap<String, Object> map) {
		try {
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "FH");
			String order_id = approveManageDao.getOrderId(mapSel);
			TdDurationFundCashdiv deem = new TdDurationFundCashdiv();
			BeanUtils.populate(deem, map);
			deem.setDealNo(order_id);
			deem.setVersion(1);
			deem.setDealDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			tdDurationFundCashdivService.insertTdDurationFundCashdiv(deem);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTdDurationFundCashdiv")
	public RetMsg<Serializable> updateTdDurationFundCashdiv(@RequestBody TdDurationFundCashdiv map) {
		tdDurationFundCashdivService.updateTdDurationFundCashdiv(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteTdDurationFundCashdiv")
	public RetMsg<Serializable> deleteProductApproveFund(@RequestBody TdDurationFundCashdiv map) {
		tdDurationFundCashdivService.deleteTdDurationFundCashdiv(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 发起审批
	 * @param map
	 * @return
	 */
	public RetMsg<Serializable> approveCommit(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundCashdivByMyself")
	public RetMsg<PageInfo<TdDurationFundCashdiv>> searchPageTdDurationFundCashdivByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TdDurationFundCashdiv> page = tdDurationFundCashdivService.searchPageTdDurationFundCashdivMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundCashdivUnfinished")
	public RetMsg<PageInfo<TdDurationFundCashdiv>> searchPageTdDurationFundCashdivUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		params.put("insId", SlSessionHelper.getInstitutionId());  
		Page<TdDurationFundCashdiv> page = tdDurationFundCashdivService.searchPageTdDurationFundCashdiv(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundCashdivFinished")
	public RetMsg<PageInfo<TdDurationFundCashdiv>> searchPageTdDurationFundCashdivFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdDurationFundCashdiv> page = tdDurationFundCashdivService.searchPageTdDurationFundCashdivFinished(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
}
