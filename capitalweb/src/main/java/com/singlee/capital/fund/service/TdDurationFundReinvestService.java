package com.singlee.capital.fund.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.web.bind.annotation.RequestBody;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundReinvest;

public interface TdDurationFundReinvestService {
	
	/**
	 * 普通列表查询 
	 * @param tdDurationFundReinvest
	 * @return
	 */
	List<TdDurationFundReinvest> searchTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest);
	/**
	 * 普通分页查询
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<TdDurationFundReinvest> getTdDurationFundReinvestList(Map<String, Object> params, RowBounds rb);
	/**
	 * 添加
	 * @param tdDurationFundReinvest
	 * @return
	 */
	void insertTdDurationFundReinvest(@RequestBody HashMap<String, Object> map);
	/**
	 * 修改
	 * @param tdDurationFundReinvest
	 * @return
	 */
	int updateTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest);
	/**
	 * 删除
	 * @param tdDurationFundReinvest
	 * @return
	 */
	int deleteTdDurationFundReinvest(TdDurationFundReinvest tdDurationFundReinvest);
	/**
	 * 分页查询(审批)
	 * @param params
	 * @param type
	 * @return
	 */
	Page<TdDurationFundReinvest> searchPageTdDurationFundReinvest(Map<String, Object> params, RowBounds rb);
	/**
	 * 分页查询(已完成)
	 * @param params
	 * @param type
	 * @return
	 */
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestFinished(
			Map<String, Object> params, RowBounds rb);
	/**
	 * 分页查询(已完成)
	 * @param params
	 * @param type
	 * @return
	 */
	public Page<TdDurationFundReinvest> searchPageTdDurationFundReinvestMySelf(
			Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据单号查询单笔交易
	 * @param dealNo
	 * @return
	 */
	public  TdDurationFundReinvest getTdDurationFundReinvestByDealNo(String dealNo);
}
