package com.singlee.capital.fund.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.mapper.Acctno407Mapper;
import com.singlee.capital.base.model.Acctno407;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.fund.mapper.ProductApproveFundMapper;
import com.singlee.capital.fund.mapper.TaFundDealMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TaFund;
import com.singlee.capital.fund.model.TaFundDeal;
import com.singlee.capital.fund.service.ProductApproveFundService;
import com.singlee.capital.fund.service.TaFundDealService;
import com.singlee.capital.fund.service.TaFundService;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceLogMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.CreditBeaRec;
import com.singlee.capital.interfacex.model.DebitBeaRec;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRs;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRec;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.service.TradeInterfaceService;
import com.singlee.capital.users.mapper.TdCustAccMapper;
import com.singlee.capital.users.model.TdCustAcc;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import com.singlee.slbpm.mapper.ProcTaskEventMapper;

@SuppressWarnings("unused")
@Service("productApproveFundService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductApproveFundServiceImpl implements ProductApproveFundService,SlbpmCallBackInteface,TaskEventInterface{
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private TiInterfaceLogMapper tiInterfacleLogMapper;
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private  TdCustAccMapper tdCustAccMapper;
	@Autowired
	TtInstitutionSettlsMapper ttInstitutionSettlsMapper;
	@Autowired
	private  Acctno407Mapper acctno407Mapper;
	@Autowired
	private TrdOrderMapper orderManageMapper;
	@Resource
	ProductApproveFundMapper productApproveFundMapper;
	@Resource
	TaFundService taFundService;
	@Resource
	TaFundDealService taFundDealService;
	@Resource
	TradeInterfaceService tradeInterfaceService;
	@Resource 
	TdProductFundTposService tdProductFundTposService;
	@Resource 
	TrdOrderMapper trdOrderMapper;
	@Resource
	TdFundAcupService tdFundAcupService;
	@Resource
	TdAccTrdDailyMapper tdAccTrdDailyMapper;
	@Resource
	TaFundDealMapper taFundDealMapper;
	//待办消息服务
	@Resource
	TaMessageService taMessageService;
	/** 流程环节事件dao*/
	@Autowired
	private ProcTaskEventMapper procTaskEventMapper;
	/** 额度处理业务层*/
	@Autowired
	private CreditManageService creditManageService;
	@Autowired
	private InstitutionService institutionService;
	
	@Override
	public List<ProductApproveFund> searchProductApproveFund(ProductApproveFund poductApproveFund) {
		
		return productApproveFundMapper.selectAll();
	}

	@Override
	public int insertProductApproveFund(ProductApproveFund map) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		map.setDealDate(sf.format(new Date()));
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		map.setLastUpdate(df.format(new Date()));
		int i = productApproveFundMapper.insert(map);
		//TaFund ta =new TaFund();
		//ta.setFundCode(map.getFundCode());
		//添加基金基础信息到副本表
		//List<TaFund> talist = taFundService.searchTaFund(ta);
		Map<String, Object> newmap=new HashMap<String, Object>();
		newmap.put("fundCode", map.getFundCode());
		TaFund talist=taFundService.getTaFund(newmap);
		if(talist!=null){
			TaFundDeal taFundDeal =  new TaFundDeal();
			taFundDeal.setApplyRate(talist.getApplyRate());
			taFundDeal.setCoverDay(talist.getCoverDay());
			taFundDeal.setCoverMode(talist.getCoverMode());
			taFundDeal.setCustodian(talist.getCustodian());
			taFundDeal.setDealNo(map.getDealNo());
			taFundDeal.setEstDate(talist.getEstDate());
			taFundDeal.setFundCode(talist.getFundCode());
			taFundDeal.setFundName(talist.getFundName());
			taFundDeal.setInvName(talist.getInvName());
			taFundDeal.setInvType(talist.getInvType());
			taFundDeal.setManagComp(talist.getManagComp());
			taFundDeal.setRedeemRate(talist.getRedeemRate());
			taFundDeal.setManagRate(talist.getManagRate());
			taFundDeal.setRemark(talist.getRemark());
			taFundDeal.setTrustRate(talist.getTrustRate());
			taFundDeal.setFundCircle(talist.getFundCircle());
			taFundDealService.insertTaFundDeal(taFundDeal);
		}
		return i;
	}

	@Override
	public int deleteProductApproveFund(ProductApproveFund productApproveFund) {
		productApproveFundMapper.deleteProductApproveFundDealno(productApproveFund.getDealNo());
		return 1;
	}

	@Override
	public int updateProductApproveFund(ProductApproveFund productApproveFund) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		productApproveFund.setLastUpdate(df.format(new Date()));
		df = new SimpleDateFormat("yyyy-MM-dd");
		productApproveFund.setDealDate(df.format(new Date()));
		int i=productApproveFundMapper.updateByPrimaryKey(productApproveFund);
		Map<String, Object> newmap=new HashMap<String, Object>();
		newmap.put("fundCode", productApproveFund.getFundCode());
		TaFund talist=taFundService.getTaFund(newmap);
		if(talist!=null && i>0){
			TaFundDeal taFundDeal =  new TaFundDeal();
			taFundDeal.setApplyRate(talist.getApplyRate());
			taFundDeal.setCoverDay(talist.getCoverDay());
			taFundDeal.setCoverMode(talist.getCoverMode());
			taFundDeal.setCustodian(talist.getCustodian());
			taFundDeal.setDealNo(productApproveFund.getDealNo());
			taFundDeal.setEstDate(talist.getEstDate());
			taFundDeal.setFundCode(talist.getFundCode());
			taFundDeal.setFundName(talist.getFundName());
			taFundDeal.setInvName(talist.getInvName());
			taFundDeal.setInvType(talist.getInvType());
			taFundDeal.setManagComp(talist.getManagComp());
			taFundDeal.setRedeemRate(talist.getRedeemRate());
			taFundDeal.setManagRate(talist.getManagRate());
			taFundDeal.setRemark(talist.getRemark());
			taFundDeal.setTrustRate(talist.getTrustRate());
			taFundDeal.setFundCircle(talist.getFundCircle());
			i=taFundDealMapper.updateByPrimaryKey(taFundDeal);
		}
		return i;
	}

	@Override
	public Page<ProductApproveFund> getProductApproveFundList(Map<String, Object> params, RowBounds rb) {
		return productApproveFundMapper.getProductApproveFundList(params, rb);
	}

	@Override
	public ProductApproveFund getProductApproveFund(
			Map<String, Object> params) {
		return productApproveFundMapper.getProductApproveFund(params);
	}

	@Override
	public Page<ProductApproveFund> searchPageProductApproveFund(
			Map<String, Object> params, RowBounds rb) {
		// + wzf - 2017.5.10 - 机构隔离
		// 登录人机构ID
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitutionId());
		
		// 根据登录人机构ID查询他的子机构
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> ttlist=new ArrayList<String>();
		for (TtInstitution ttInstitution : tlist) {
			ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
		}
		params.put("instIds", ttlist);
		return productApproveFundMapper.searchPageProductApproveFund(params,rb);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		Map<String,Object> params  = new HashMap<String, Object>();
		params.put("dealNo", serial_no);
		return productApproveFundMapper.getProductApproveFund(params);
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		map.put("approveStatus", status);
		//更新状态哦
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
		ProductApproveFund fund = productApproveFundMapper.getProductApproveFund(map);
		if("6".equals(status)){
			try{
				int result = tdFundAcupService.fundAcupManager(fund);
				if(result > 0){
					System.out.println("基金申购账务处理成功！");
				}
			} catch (Exception e) {
				JY.raise(e.getMessage());
				
			}
		}
		//更新状态哦
		productApproveFundMapper.updateApproveStatus(map);
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		ProductApproveFund main = productApproveFundMapper.getProductApproveFund(map);
		map.put("flow_id", flow_id);
		map.put("task_def_key", task_def_key);
		map.put("dealNo", serial_no);
		map.put("trade_id", serial_no);
		map.put("prd_code", main.getPrdNo());
		HashMap<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("order_id", serial_no);
//		TtTrdOrder ttTrdTrade  = this.orderManageMapper.selectOrderForOrderId(paramMap);
//		String beanName = DurationConstants.getBeanFromTrdType(ttTrdTrade.getTrdtype());
//		AbstractDurationService abstractDurationService = SpringContextHolder.getBean(beanName);
//		TdProductApproveMain  main  = abstractDurationService.getProductApproveMain(map);
		List<String> eventList = procTaskEventMapper.getProcTaskEvents(map);
		boolean flag = false;
		if(null != eventList){
			for (int i = 0; i < eventList.size(); i++) {
				if("CNY".equals(main.getCcy())){
					Map<String, Object> mapt = new HashMap<String, Object>();
					if("transfer".equals(eventList.get(i)) && main.getIsSettl().endsWith("1")){//放款 -IsSettl 1清算 2不清算
						//AcctType：清算方式  行内清算 1    行外清算 2
						TtInstitutionSettls ttInstitutionSettls = new TtInstitutionSettls();
						if(main.getAcctType().endsWith("1")){
							TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
							request.setCurrencyDr(main.getCcy());
							
							List<DebitBeaRec> recList = new ArrayList<DebitBeaRec>();
							//借方-新加的
							DebitBeaRec rec1 = new DebitBeaRec();
							mapt.put("ccy", main.getCcy().trim());
							mapt.put("instId", main.getSponInst());
//							mapt.put("ccy", "CNY");
							List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
							if(null == acct || acct.size()<=0){
								throw new RException("找不到："+main.getSponInst()+"机构"+main.getCcy()+"的407账号！");
							}
							rec1.setAcctNoDr(acct.get(0).getAcctNo());
							rec1.setDebitMedType(acct.get(0).getMediumType());
//							rec1.setAcctNoDr(dictionaryGetService.getTaDictByCodeAndKey("TransferAcct", "01").getDict_value());//407								
//							rec1.setDebitMedType(dictionaryGetService.getTaDictByCodeAndKey("TransferAcct", "02").getDict_value());
							rec1.setAmtDr(String.valueOf(main.getAmt()));
							recList.add(rec1);
									
							//借方
							DebitBeaRec rec = new DebitBeaRec();
							rec.setAcctNoDr(main.getSelfAccCode());//261=98						
							//查询98账户类型
							ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
							rec.setDebitMedType(ttInstitutionSettls.getMediumType());							
							rec.setAmtDr(String.valueOf(main.getAmt()));							
							recList.add(rec);							
							request.setDebitBeaRec(recList);
							request.setCurrencyCr(main.getCcy());
							
							
							List<CreditBeaRec> reccList = new ArrayList<CreditBeaRec>();
							//贷方-新加的
							CreditBeaRec recc1 = new CreditBeaRec();
							recc1.setAcctNoCr(main.getSelfAccCode());							
							//查询98账户类型
							ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
							recc1.setCreditMedType(ttInstitutionSettls.getMediumType());							
							recc1.setAmtCr(String.valueOf(main.getAmt()));
							reccList.add(recc1);
							//贷方
							CreditBeaRec recc = new CreditBeaRec();
							recc.setAcctNoCr(main.getPartyAccCode());						
							//查询客户账户类型
							Map<String, Object> newmap=new HashMap<String, Object>();
//							newmap.put("bankaccid", main.getPartyAccCode()) ;
//							newmap.put("custmerCode", main.getcNo());
							newmap.put("bankaccid", main.getPartyAccCode());
							newmap.put("custmerCode", main.getPartyEcifCode());
							newmap.put("cny", main.getCcy());
							TdCustAcc tdCustAcc = tdCustAccMapper.showTdCustAccByColumns(newmap);
							//TdCustAccCopy tdCustAccCopy = tdCustAccCopyMapper.showTdCustAccCopyByBankaccid(newmap);
							recc.setCreditMedType(tdCustAcc.getMediumType());													
							recc.setAmtCr(String.valueOf(main.getAmt()));						
							reccList.add(recc);							
													
							request.setCreditBeaRec(reccList);

							
							request.setFbNo(InterfaceCode.TI_FBID);
							request.setTxnType(InterfaceCode.TI_IFBM0001);
							request.setFeeBaseLogId(main.getDealNo());
							CommonRqHdr hdr = new CommonRqHdr();
							hdr.setRqUID(UUID.randomUUID().toString());
							hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
							hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
							hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
							request.setCommonRqHdr(hdr);
							try {
								TStBeaRepayAaaRs response = socketClientService.t24TStBeaRepayAaaRequest(request);
								//行内清算
								map.put("transfer", response.getCommonRsHdr().getStatusCode()+"|"+response.getCommonRsHdr().getServerStatusCode());
								
							} catch (Exception e) {
								if ("transfer".equals(eventList.get(i))) {
									throw new Exception("error行内清算："+e.getMessage());									
								}
							}
							
						}
						
						if(main.getAcctType().endsWith("2")){
							StringBuffer logbuffer = new  StringBuffer();
							logbuffer.append("No.1**************************************");
							List<DebitBeaRec> recList = new ArrayList<DebitBeaRec>();
							List<CreditBeaRec> reccList = new ArrayList<CreditBeaRec>();
							TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
							request.setCurrencyDr(main.getCcy());			
							logbuffer.append(String.format("%20s", "CurrencyDr:")).append(main.getCcy());
							DebitBeaRec rec1 = new DebitBeaRec();
							logbuffer.append(String.format("%20s", "DR:(借方)"));
							mapt.put("ccy", main.getCcy());mapt.put("instId", main.getSponInst());
							List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
							if(null == acct || acct.size()<=0){
								throw new RException("找不到："+main.getSponInst()+"机构"+main.getCcy()+"的407账号！");
							}
							rec1.setAcctNoDr(acct.get(0).getAcctNo());
							logbuffer.append(String.format("%20s", "AcctNoDr:")).append(acct.get(0).getAcctNo());
							rec1.setDebitMedType(acct.get(0).getMediumType());
							logbuffer.append(String.format("%20s", "DebitMedType:")).append(acct.get(0).getMediumType());
							rec1.setAmtDr(String.valueOf(main.getAmt()));
							logbuffer.append(String.format("%20s", "AmtDr:")).append(String.valueOf(main.getAmt()));
							recList.add(rec1);
							request.setDebitBeaRec(recList);
							request.setCurrencyCr(main.getCcy());		
							logbuffer.append(String.format("%20s", "CurrencyCr:")).append(main.getCcy());
							CreditBeaRec recc1 = new CreditBeaRec();
							logbuffer.append(String.format("%20s", "CR:(贷方)"));
							recc1.setAcctNoCr(main.getSelfAccCode());
							logbuffer.append(String.format("%20s", "AcctNoCr:")).append(main.getSelfAccCode());
							recc1.setAmtCr(String.valueOf(main.getAmt()));	
							logbuffer.append(String.format("%20s", "AmtCr:")).append(String.valueOf(main.getAmt()));
							ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
							recc1.setCreditMedType(ttInstitutionSettls.getMediumType());	
							logbuffer.append(String.format("%20s", "CreditMedType:")).append(ttInstitutionSettls.getMediumType());
							reccList.add(recc1);						
							request.setCreditBeaRec(reccList);
							request.setFbNo(InterfaceCode.TI_FBID);
							request.setTxnType(InterfaceCode.TI_IFBM0001);
							request.setFeeBaseLogId(main.getDealNo());
							CommonRqHdr hdr = new CommonRqHdr();
							hdr.setRqUID(UUID.randomUUID().toString());
							hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
							hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
							hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
							request.setCommonRqHdr(hdr);
							boolean runflag = true;
							try {
								TStBeaRepayAaaRs response = socketClientService.t24TStBeaRepayAaaRequest(request);
								map.put("transfer", response.getCommonRsHdr().getStatusCode()+"|"+response.getCommonRsHdr().getServerStatusCode());
								if(response.getCommonRsHdr().getStatusCode()=="000000"|| "000000".equals(response.getCommonRsHdr().getStatusCode())){
									runflag = true;
								}else {
									runflag = false;
								}
								logbuffer.append(String.format("%20s", "StatusCode:")).append(response.getCommonRsHdr().getStatusCode());
							} catch (Exception e) {
								if ("transfer".equals(eventList.get(i))) {
									throw new Exception("error行内清算："+e.getMessage());									
								}
							}finally{
								logbuffer.append("**************************************No.1");
								LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(logbuffer.toString());
							}
							if(runflag){
								logbuffer = new StringBuffer();
								logbuffer.append("No.2**************************************");
								CommonRqHdr commonRqHdr = new CommonRqHdr();
								commonRqHdr.setRqUID(UUID.randomUUID().toString());
								commonRqHdr.setChannelId(InterfaceCode.TI_CHANNELNO);
								commonRqHdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								commonRqHdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								commonRqHdr.setSPName("T24");
								UPPSCdtTrfRq request1 = new UPPSCdtTrfRq();
								request1.setCommonRqHdr(commonRqHdr);
								request1.setUserDefineTranCode(InterfaceCode.TI2ND_UPPSCdtTrf);
								request1.setPayPathCode(InterfaceCode.TI2ND_PayPathCode1001);
								request1.setPosEntryMode(InterfaceCode.TI2ND_PosEntryMode0);
								request1.setBusiType(InterfaceCode.TI2ND_BusiTypeA100);
								request1.setBusiKind(InterfaceCode.TI2ND_BusiKind02102);
								//查询大额支付帐号类型
								Map<String, Object> mapc = new HashMap<String, Object>();
								mapc.put("instId", main.getSponInst());
								mapc.put("instAcctNo",main.getSelfAccCode());
								mapc.put("ccy", main.getCcy());
								ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNoAndDealno(mapc);
								if(null == ttInstitutionSettls) {
									throw new RException(main.getSponInst()+":"+main.getSelfAccCode()+":"+main.getCcy()+"不存在机构清算账号！");
									}
								request1.setPayerAccType(ttInstitutionSettls.getPayerAccType());
								logbuffer.append(String.format("%20s", "PayerAccType:")).append(ttInstitutionSettls.getPayerAccType());
								request1.setPayerAcc(main.getSelfAccCode());
								logbuffer.append(String.format("%20s", "PayerAcc:")).append(main.getSelfAccCode());
								request1.setPayerName(main.getSelfAccName());
								logbuffer.append(String.format("%20s", "PayerName:")).append(main.getSelfAccName());
								request1.setPayerAccBank("");
								request1.setRealPayerAcc("");
								request1.setRealPayerName("");
								request1.setRealPayerAccType("");
								request1.setPayeeAccBank(main.getPartyBankCode());
								logbuffer.append(String.format("%20s", "PayeeAccBank:")).append(main.getPartyBankCode());
								request1.setPayeeName(main.getPartyAccName());
								logbuffer.append(String.format("%20s", "PayeeName:")).append(main.getPartyAccName());
								request1.setPayeeAccType("1");
								request1.setPayeeAcc(main.getPartyAccCode());
								logbuffer.append(String.format("%20s", "PayeeAcc:")).append(main.getPartyAccCode());
								request1.setCurrency(main.getCcy());
								logbuffer.append(String.format("%20s", "Currency:")).append(main.getCcy());
								request1.setAmount(String.valueOf(main.getAmt()));
								logbuffer.append(String.format("%20s", "Amount:")).append(String.valueOf(main.getAmt()));
								request1.setPostScript("");
								request1.setCommissionNo("0");
								request1.setChargeNo("0");
								request1.setRemark1("X");
								request1.setFeeBaseLogId(main.getDealNo());
								try {
									UPPSCdtTrfRs response1 = socketClientService.T2ndPaymentUPPSCdtTrfRequest(request1);
									map.put("transfer", response1.getCommonRsHdr().getStatusCode()+"|"+response1.getCommonRsHdr().getServerStatusCode());
									if("000000".equals(response1.getCommonRsHdr().getStatusCode())){
										//查询一次，若返回处理不成功，则后台JOB根据数据库中的状态轮询
										try {
												UPPSigBusiQueryRq request2 = new UPPSigBusiQueryRq();
												request2.setUserDefineTranCode(InterfaceCode.TI2ND_QueryUserDefineTranCode);
												request2.setOrigChannelCode(InterfaceCode.TI_CHANNELNO);
												request2.setOrigChannelDate(response1.getWorkDate());
												request2.setOrigFeeBaseLogId(main.getDealNo());
												UPPSigBusiQueryRs response2 = socketClientService.T2ndPaymentUPPSigBusiQueryRequest(request2);
												List<UPPSigBusiQueryRec> queryList = response2.getUPPSigBusiQueryRec();
												
												for(int k=0;k<queryList.size();k++){
													Map<String, Object> mapp = new HashMap<String, Object>();
													mapp.put("retcode", queryList.get(k).getCenterDealCode());
													mapp.put("retmsg",queryList.get(k).getCenterDealMsg());
													mapp.put("rquid",queryList.get(k).getChannelSerNo());
													mapp.put("agentserialno",queryList.get(k).getAgentSerialNo());
													mapp.put("centerdealcode", queryList.get(k).getCenterDealCode());
													mapp.put("centerdealmsg", queryList.get(k).getCenterDealMsg());
													mapp.put("payresult", queryList.get(k).getPayResult());
													mapp.put("workdate", queryList.get(k).getWorkDate());
													mapp.put("bankdate", queryList.get(k).getBankDate());
													mapp.put("bankid", queryList.get(k).getBankId());
													mapp.put("feeBaseLogid", queryList.get(k).getFeeBaseLogId());
													String srquid = ti2ndPaymentMapper.queryTi2ndPayment2(mapp).getSrquid();
													mapp.put("srquid", srquid);
													ti2ndPaymentMapper.update2ndPaymentRetMsg(mapp);
													tiInterfacleLogMapper.updateTiInterfaceRetcode(mapp);
													
													System.out.println("自助查询结果为：" + response2.getCommonRsHdr().getStatusCode()+"|"+response2.getCommonRsHdr().getServerStatusCode());
													LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("自助查询结果为：" + response2.getCommonRsHdr().getStatusCode()+"|"+response2.getCommonRsHdr().getServerStatusCode());
													LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("支付平台处理为: "+ queryList.get(k).getDealCode()+"|"+queryList.get(k).getDealMsg());
													LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("核心处理为: "+queryList.get(k).getCoreResult()+"|"+queryList.get(k).getBankDealCode()+"|"+queryList.get(k).getBankDealMsg());
													LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("人行处理为: "+queryList.get(k).getCenterDealCode()+"|"+queryList.get(k).getCenterDealMsg());
													
													if("2".equals(queryList.get(k).getPayResult())||"0".equals(queryList.get(k).getPayResult())){
														break;
													}
												}
										} catch (Exception e) {
											// TODO: handle exception
											LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(e.getMessage());
										}
										
									}
									logbuffer.append(String.format("%20s", "StatusCode:")).append(response1.getCommonRsHdr().getStatusCode());
									
								} catch (Exception e) {
									if ("transfer".equals(eventList.get(i))) {
										throw new Exception("error行外清算："+e.getMessage());									
									}
								}finally{
									logbuffer.append("**************************************No.2");
									LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(logbuffer.toString());
								}
							}
						}
					}
				
				}
			}
		}
		return null;
	}

	@Override
	public Page<ProductApproveFund> searchPageProductApproveFundFinished(
			Map<String, Object> params, RowBounds rb) {
		return productApproveFundMapper.searchPageProductApproveFundFinished(params, rb);
	}

	@Override
	public Page<ProductApproveFund> searchPageProductApproveFundMySelf(
			Map<String, Object> params, RowBounds rb) {
		return productApproveFundMapper.searchPageProductApproveFundMySelf(params, rb);
	}
	
	@Override
	public ProductApproveFund getFundInfoForApprove(String dealNo) {
		return productApproveFundMapper.getFundInfoForApprove(dealNo);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }
	
}
