package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundTrans;

public interface TdDurationFundTransService {

	List<TdDurationFundTrans> searchTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans);
	Page<TdDurationFundTrans> getTdDurationFundTransList(Map<String, Object> params, RowBounds rb);
	int insertTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans);
	int updateTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans);
	int deleteTdDurationFundTrans(TdDurationFundTrans tdDurationFundTrans);
	Page<TdDurationFundTrans> searchPageTdDurationFundTranst(Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstFinished(
			Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstMySelf(
			Map<String, Object> params, RowBounds rb);
	public TdDurationFundTrans getTdDurationFundTransByDealNo(TdDurationFundTrans tdDurationFundTrans) ;
}
