package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundTrans;

public interface TdDurationFundTransMapper extends Mapper<TdDurationFundTrans>{
	
	public void deleteTdDurationFundTransDealno(String dealno);
	public int updateTdDurationFundTrans(String dealno);
	public Page<TdDurationFundTrans> getTdDurationFundTransList(Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranst(Map<String, Object> params, RowBounds rb);
	public int updateApproveStatus(Map<String, Object> params);
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstFinished(
			Map<String, Object> params, RowBounds rb);
	public Page<TdDurationFundTrans> searchPageTdDurationFundTranstMySelf(
			Map<String, Object> params, RowBounds rb);
	public TdDurationFundTrans getTdDurationFundTransByDealNo(TdDurationFundTrans t);
	
}
