package com.singlee.capital.fund.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.fund.mapper.TdDurationFundCashdivMapper;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;
import com.singlee.capital.fund.service.TdDurationFundCashdivService;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

@Service("tdDurationFundCashdivService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdDurationFundCashdivServiceImpl implements TdDurationFundCashdivService,SlbpmCallBackInteface,TaskEventInterface{
	
	@Resource
	TdDurationFundCashdivMapper tdDurationFundCashdivMapper;
	@Resource 
	DayendDateService dayendDateService;
	@Resource
	TdAccTrdDailyMapper tdAccTrdDailyMapper; 
	@Resource
	TdFundAcupService tdFundAcupService;
	

	@Override
	public List<TdDurationFundCashdiv> searchTdDurationFundCashdiv(TdDurationFundCashdiv poductApproveFund) {
		
		return tdDurationFundCashdivMapper.selectAll();
	}

	@Override
	public int insertTdDurationFundCashdiv(TdDurationFundCashdiv tdDurationFundCashdiv) {
		tdDurationFundCashdiv.setCfType(DictConstants.TrdType.FundCashDiv);
		return tdDurationFundCashdivMapper.insert(tdDurationFundCashdiv);
	}

	@Override
	public int deleteTdDurationFundCashdiv(TdDurationFundCashdiv tdDurationFundCashdiv) {
		tdDurationFundCashdivMapper.deleteTdDurationFundCashdivDealno(tdDurationFundCashdiv.getDealNo());
		return 1;
	}

	@Override
	public int updateTdDurationFundCashdiv(TdDurationFundCashdiv tdDurationFundCashdiv) {
		tdDurationFundCashdiv.setCfType(DictConstants.TrdType.FundCashDiv);
		return tdDurationFundCashdivMapper.updateByPrimaryKey(tdDurationFundCashdiv);
	}

	@Override
	public Page<TdDurationFundCashdiv> getTdDurationFundCashdivList(Map<String, Object> params, RowBounds rb) {
		return tdDurationFundCashdivMapper.getTdDurationFundCashdivList(params, rb);
	}

	@Override
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdiv(
			Map<String, Object> params, RowBounds rb) {
		
		return tdDurationFundCashdivMapper.searchPageTdDurationFundCashdiv(params,rb);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		TdDurationFundCashdiv tdDurationFundCashdiv=new TdDurationFundCashdiv();
		tdDurationFundCashdiv.setDealNo(serial_no);
		return tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(tdDurationFundCashdiv);
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		map.put("approveStatus", status);
		if("6".equals(status)){//审批完成时出宣告日审批完成账务
			TdDurationFundCashdiv tdDurationFundCashdiv = new TdDurationFundCashdiv();
			tdDurationFundCashdiv.setDealNo(serial_no);
			tdDurationFundCashdiv = tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(tdDurationFundCashdiv);
//			String postDate = dayendDateService.getDayendDate();
//			TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();
//			tdAccTrdDaily.setAccAmt(tdDurationFundCashdiv.getCashdivAmt().doubleValue());
//			tdAccTrdDaily.setDealNo(tdDurationFundCashdiv.getDealNo());
//			tdAccTrdDaily.setOpTime(tdDurationFundCashdiv.getvDate());
//			tdAccTrdDaily.setPostDate(postDate);
//			tdAccTrdDaily.setAccType(DurationConstants.AccType.APPCASHDIV_FUND_AMT);
//			int flag = tdAccTrdDailyMapper.insert(tdAccTrdDaily);
//			if(flag >0){
//				//调用账务处理服务
//				Map<String,Object> amtMap = new HashMap<String, Object>();
//				amtMap.put(DurationConstants.AccType.APPCASHDIV_FUND_AMT,tdDurationFundCashdiv.getCashdivAmt());//放款会计事件
//				tdFundAcupService.acupInterface(BeanUtil.beanToMap(tdDurationFundCashdiv),amtMap);
//			}
			tdFundAcupService.fundAcupManager(tdDurationFundCashdiv);
		}
		//更新状态哦
		tdDurationFundCashdivMapper.updateApproveStatus(map);
		
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivFinished(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundCashdivMapper.searchPageTdDurationFundCashdivFinished(params, rb);
	}

	@Override
	public Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivMySelf(
			Map<String, Object> params, RowBounds rb) {
		return tdDurationFundCashdivMapper.searchPageTdDurationFundCashdivMySelf(params, rb);
	}
	@Override
	public TdDurationFundCashdiv getTdDurationFundCashdiv(
			TdDurationFundCashdiv tdDurationFundCashdiv) {
		return tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(tdDurationFundCashdiv);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

}
