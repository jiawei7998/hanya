package com.singlee.capital.fund.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.fund.model.TdDurationFundRedeem;
import com.singlee.capital.fund.service.TdDurationFundRedeemService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;


/*
 * 
 */
@Controller
@RequestMapping(value = "/TdDurationFundRedeemController")
public class TdDurationFundRedeemController extends CommonController {
	
	@Resource
	TdDurationFundRedeemService tdDurationFundRedeemService;
	@Resource
	private TrdOrderMapper approveManageDao;
	
	@ResponseBody
	@RequestMapping(value = "/searchTdDurationFundRedeem")
	public RetMsg<PageInfo<TdDurationFundRedeem>> searchTdDurationFundRedeem(@RequestBody Map<String,Object> map){
		Page<TdDurationFundRedeem> page  = tdDurationFundRedeemService.getTdDurationFundRedeemList(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	} 
	
	@ResponseBody
	@RequestMapping(value = "/getTdDurationFundRedeemByDealNo")
	public RetMsg<TdDurationFundRedeem> getTdDurationFundRedeemByDealNo(@RequestBody Map<String,Object> map){
		//先查询基金赎回是否有记录
		TdDurationFundRedeem tdDurationFundRedeem = new TdDurationFundRedeem();
		tdDurationFundRedeem.setDealNo(ParameterUtil.getString(map, "dealNo", ""));
		tdDurationFundRedeem = tdDurationFundRedeemService.getTdDurationFundRedeem(map);
		return RetMsgHelper.ok(tdDurationFundRedeem);
	} 
	
	
	/**
	 * 新增
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/insertTdDurationFundRedeem")
	public RetMsg<Serializable> insertTdDurationFundRedeem(@RequestBody HashMap<String, Object> map) {
		try {
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "SH");
			String order_id = approveManageDao.getOrderId(mapSel);
			TdDurationFundRedeem deem = new TdDurationFundRedeem();
			BeanUtils.populate(deem, map);
			deem.setDealNo(order_id);
			deem.setVersion(1);
			deem.setDealDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			tdDurationFundRedeemService.insertTdDurationFundRedeem(deem);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/updateTdDurationFundRedeem")
	public RetMsg<Serializable> updateTdDurationFundRedeem(@RequestBody TdDurationFundRedeem map) {
		
		tdDurationFundRedeemService.updateTdDurationFundRedeem(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */ 
	@ResponseBody
	@RequestMapping(value = "/deleteTdDurationFundRedeem")
	public RetMsg<Serializable> deleteProductApproveFund(@RequestBody TdDurationFundRedeem map) {
		tdDurationFundRedeemService.deleteTdDurationFundRedeem(map);
		return RetMsgHelper.ok();
	} 
	
	/**
	 * 发起审批
	 * @param map
	 * @return
	 */
	public RetMsg<Serializable> approveCommit(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundRedeemByMyself")
	public RetMsg<PageInfo<TdDurationFundRedeem>> searchPageTdDurationFundRedeemByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TdDurationFundRedeem> page = tdDurationFundRedeemService.searchPageTdDurationFundRedeemMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundRedeemUnfinished")
	public RetMsg<PageInfo<TdDurationFundRedeem>> searchPageTdDurationFundRedeemUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		params.put("insId", SlSessionHelper.getInstitutionId());  
		Page<TdDurationFundRedeem> page =  tdDurationFundRedeemService.searchPageTdDurationFundRedeem(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTdDurationFundRedeemFinished")
	public RetMsg<PageInfo<TdDurationFundRedeem>> searchPageTdDurationFundRedeemFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdDurationFundRedeem> page =  tdDurationFundRedeemService.searchPageTdDurationFundRedeemFinished(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
}
