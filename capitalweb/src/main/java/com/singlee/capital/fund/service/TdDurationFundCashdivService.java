package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;

/**
 * 基金分红处理接口
 * @author lxy
 *
 */
public interface TdDurationFundCashdivService {
	
	/**
	 * 普通查询
	 * @param TdDurationFundCashdiv
	 * @return
	 */
	List<TdDurationFundCashdiv> searchTdDurationFundCashdiv(TdDurationFundCashdiv TdDurationFundCashdiv);
	/**
	 * 根据dealNo查询
	 * @param TdDurationFundCashdiv
	 * @return
	 */
	TdDurationFundCashdiv getTdDurationFundCashdiv(TdDurationFundCashdiv TdDurationFundCashdiv);
	/**
	 * 查询单笔交易对象
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<TdDurationFundCashdiv> getTdDurationFundCashdivList(Map<String, Object> params, RowBounds rb);
	/**
	 * 添加
	 * @param TdDurationFundCashdiv
	 * @return
	 */
	int insertTdDurationFundCashdiv(TdDurationFundCashdiv TdDurationFundCashdiv);
	/**
	 * 修改
	 * @param TdDurationFundCashdiv
	 * @return
	 */
	int updateTdDurationFundCashdiv(TdDurationFundCashdiv TdDurationFundCashdiv);
	/**
	 * 删除
	 * @param TdDurationFundCashdiv
	 * @return
	 */
	int deleteTdDurationFundCashdiv(TdDurationFundCashdiv TdDurationFundCashdiv);
	/**
	 * 查询（待审批）
	 * @param params
	 * @param type 查询类型
	 * @return
	 */
	Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdiv(Map<String, Object> params, RowBounds rb);
	/**
	 * 查询（已完成）
	 * @param params
	 * @param type 查询类型
	 * @return
	 */
	Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivFinished(
			Map<String, Object> params, RowBounds rb);
	/**
	 * 查询（发起的）
	 * @param params
	 * @param type 查询类型
	 * @return
	 */
	Page<TdDurationFundCashdiv> searchPageTdDurationFundCashdivMySelf(
			Map<String, Object> params, RowBounds rb);
}
