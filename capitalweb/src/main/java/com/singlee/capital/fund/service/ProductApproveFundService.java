package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.ProductApproveFund;

public interface ProductApproveFundService {
	
	/**
	 * 查询交易集合
	 * @param productApproveFund
	 * @return
	 */
	List<ProductApproveFund> searchProductApproveFund(ProductApproveFund productApproveFund);
	/**
	 * 查询单笔交易对象
	 * @param params
	 * @return
	 */
	ProductApproveFund getProductApproveFund(Map<String, Object> params);
	/**
	 * 普通列表查询
	 * @param Map<String, Object> params, RowBounds rb
	 * @return
	 */
	Page<ProductApproveFund> getProductApproveFundList(Map<String, Object> params, RowBounds rb);
	/**
	 * 添加方法
	 * @param productApproveFund
	 * @return
	 */
	int insertProductApproveFund(ProductApproveFund productApproveFund);
	/**
	 * 修改方法
	 * @param productApproveFund
	 * @return
	 */
	int updateProductApproveFund(ProductApproveFund productApproveFund);
	/**
	 * 删除方法
	 * @param productApproveFund
	 * @return
	 */
	int deleteProductApproveFund(ProductApproveFund productApproveFund);
	/**
	 * 查询基金申购列表（待审批）
	 * @param params
	 * @param type 查询类型  
	 * @return
	 */
	Page<ProductApproveFund> searchPageProductApproveFund(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查询基金申购列表（已完成）
	 * @param params
	 * @param type 查询类型  
	 * @return
	 */
	Page<ProductApproveFund> searchPageProductApproveFundFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查询基金申购列表（我发起的）
	 * @param params
	 * @param type 查询类型  
	 * @return
	 */
	Page<ProductApproveFund> searchPageProductApproveFundMySelf(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查询基金元素（5合1 暂时只有币种）
	 * @param params
	 * @param type 查询类型  
	 * @return
	 */
	ProductApproveFund getFundInfoForApprove(String dealNo);
}
