package com.singlee.capital.fund.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdProductFundTpos;

public interface TdProductFundTposService {

	List<TdProductFundTpos> searchTdProductFundTpos(TdProductFundTpos tdProductFundTpos);
	Page<TdProductFundTpos> getTdProductFundTposList(Map<String, Object> params, RowBounds rb);
	int insertTdProductFundTpos(TdProductFundTpos tdProductFundTpos);
	int updateTdProductFundTpos(TdProductFundTpos tdProductFundTpos);
	int deleteTdProductFundTpos(TdProductFundTpos tdProductFundTpos);
	/**
	 * 基金持仓操作
	 * @return
	 */
	int fundTposManager(Map<String,Object> map);
	
	TdProductFundTpos getFundTpos(Map<String, Object> params);
}
