package com.singlee.capital.fund.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.fund.model.TdProductFundTpos;

public interface TdProductFundTposMapper extends Mapper<TdProductFundTpos>{
	
	public void deleteTdProductFundTposDealno(String dealno);
	public int updateTdProductFundTpos(TdProductFundTpos tdProductFundTpos);
	public Page<TdProductFundTpos> getTdProductFundTposList(Map<String, Object> params, RowBounds rb);
	//获取基金持仓情况
	public TdProductFundTpos getFundTpos(Map<String, Object> params);
}
