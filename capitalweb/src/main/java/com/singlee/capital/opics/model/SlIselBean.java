package com.singlee.capital.opics.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlIselBean implements Serializable{
	
	/**
	 * ISEL
	 */
	private static final long serialVersionUID = 4841323815441766031L;
	private SlInthBean inthBean;
	/**
	 * 分支代码
	 */
	private String br;
	/**
	 * 服务,定义每种交易类型标志
	 */
	private String server;
	/**
	 * 交易前端流水号<外部流水号>
	 */
	private String fedealno;
	/**
	 * 序列号,一般默认0
	 */
	private String seq;
	/**
	 * IN/OUT标识,一般默认I
	 */
	private String inoutind;
	
	private String secid;
	
	private BigDecimal clsgprice_8;
	
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInoutind() {
		return inoutind;
	}
	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public BigDecimal getClsgprice_8() {
		return clsgprice_8;
	}
	public void setClsgprice_8(BigDecimal clsgprice_8) {
		this.clsgprice_8 = clsgprice_8;
	}
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIselBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 收益
			inthBean.setBr(br);//
			inthBean.setServer(SlDealModule.ISEL.SERVER);
			inthBean.setSeq(SlDealModule.ISEL.SEQ);
			inthBean.setInoutind(SlDealModule.ISEL.INOUTIND);
			inthBean.setTag(SlDealModule.ISEL.TAG);
			inthBean.setDetail(SlDealModule.ISEL.DETAIL);
			inthBean.setPriority(SlDealModule.ISEL.PRIORITY);
			inthBean.setStatcode(SlDealModule.ISEL.STATCODE);
			inthBean.setSyst(SlDealModule.ISEL.SYST);
			inthBean.setIoper(SlDealModule.ISEL.IOPER);
		}
	}

}
