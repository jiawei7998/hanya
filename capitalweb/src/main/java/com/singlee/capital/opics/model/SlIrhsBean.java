package com.singlee.capital.opics.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlIrhsBean implements Serializable{
	
	/**
	 * IRHS
	 */
	private static final long serialVersionUID = 4841323815441766031L;
	private SlInthBean inthBean;
	private String br;
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String ratecode;
	private String effdate;
	private BigDecimal intrate_8;
	private String lstmntdte;
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInoutind() {
		return inoutind;
	}
	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getIntrate_8() {
		return intrate_8;
	}
	public void setIntrate_8(BigDecimal intrate_8) {
		this.intrate_8 = intrate_8;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIrhsBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 收益
			inthBean.setBr(br);//
			inthBean.setServer(SlDealModule.IRHS.SERVER);
			inthBean.setSeq(SlDealModule.IRHS.SEQ);
			inthBean.setInoutind(SlDealModule.IRHS.INOUTIND);
			inthBean.setTag(SlDealModule.IRHS.TAG);
			inthBean.setDetail(SlDealModule.IRHS.DETAIL);
			inthBean.setPriority(SlDealModule.IRHS.PRIORITY);
			inthBean.setStatcode(SlDealModule.IRHS.STATCODE);
			inthBean.setSyst(SlDealModule.IRHS.SYST);
			inthBean.setIoper(SlDealModule.IRHS.IOPER);
		}
	}

}
