package com.singlee.capital.opics.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlIycrBean implements Serializable{
	
	/**
	 * IYCR
	 */
	private static final long serialVersionUID = 4841323815441766031L;
	private SlInthBean inthBean;
	private String br;
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String ratetype;
	private String ccy;
	private String yieldcurve;
	private String mtystart;
	private String mtyend;
	private BigDecimal bidrate_8;
	private BigDecimal midrate_8;
	private BigDecimal offerrate_8;
	private String lstmntdte;
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInoutind() {
		return inoutind;
	}
	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}
	public String getRatetype() {
		return ratetype;
	}
	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getYieldcurve() {
		return yieldcurve;
	}
	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}
	public String getMtystart() {
		return mtystart;
	}
	public void setMtystart(String mtystart) {
		this.mtystart = mtystart;
	}
	public String getMtyend() {
		return mtyend;
	}
	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}
	public BigDecimal getBidrate_8() {
		return bidrate_8;
	}
	public void setBidrate_8(BigDecimal bidrate_8) {
		this.bidrate_8 = bidrate_8;
	}
	public BigDecimal getMidrate_8() {
		return midrate_8;
	}
	public void setMidrate_8(BigDecimal midrate_8) {
		this.midrate_8 = midrate_8;
	}
	public BigDecimal getOfferrate_8() {
		return offerrate_8;
	}
	public void setOfferrate_8(BigDecimal offerrate_8) {
		this.offerrate_8 = offerrate_8;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIycrBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 收益
			inthBean.setBr(br);//
			inthBean.setServer(SlDealModule.IYCR.SERVER);
			inthBean.setSeq(SlDealModule.IYCR.SEQ);
			inthBean.setInoutind(SlDealModule.IYCR.INOUTIND);
			inthBean.setTag(SlDealModule.IYCR.TAG);
			inthBean.setDetail(SlDealModule.IYCR.DETAIL);
			inthBean.setPriority(SlDealModule.IYCR.PRIORITY);
			inthBean.setStatcode(SlDealModule.IYCR.STATCODE);
			inthBean.setSyst(SlDealModule.IYCR.SYST);
			inthBean.setIoper(SlDealModule.IYCR.IOPER);
		}
	}

}
