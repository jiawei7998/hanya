package com.singlee.capital.loan.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.loan.model.TcLoanCheckbox;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;

public interface TcLoanCheckboxMapper extends Mapper<TcLoanCheckbox>{
	
	public Page<TcLoanCheckboxMap> searchTcLoanCheckbox(Map<String, Object> map , RowBounds rb);
	public int deleteTcLoanCheckboxByPrd(Map<String, Object> map);
	public int updateTcLoanCheckboxRole(Map<String, Object> map);
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxGroup(Map<String, Object> map , RowBounds rb);
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxByRole(Map<String, Object> map , RowBounds rb);
	public List<TcLoanCheckbox> getTcLoanCheckboxList(Map<String, Object> map);
	
	public List<TcLoanCheckboxMap> getTaskCheckboxSelect(Map<String, Object> map);
	public List<TcLoanCheckboxMap> getTaskCheckboxUnselect(Map<String, Object> map);
}
