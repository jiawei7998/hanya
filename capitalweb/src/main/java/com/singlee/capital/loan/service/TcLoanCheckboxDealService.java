package com.singlee.capital.loan.service;

import java.util.Map;

import com.singlee.capital.loan.model.TcLoanCheckboxMap;

public interface TcLoanCheckboxDealService {

	
	public int insertTcLoanCheckboxDeal(Map<String,Object> map);
	public int updateTcLoanCheckboxDeal(Map<String,Object> map);
	public int deleteTcLoanCheckboxDeal(Map<String,Object> map);
	public Map<String,Object> searchTcLoanCheckboxDeal(Map<String,Object> map);
	public TcLoanCheckboxMap getMaxTcLoanCheckbox(Map<String, Object> map);
}
