package com.singlee.capital.loan.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TC_LOAN_CHECKBOX_DEAL")
public class TcLoanCheckboxDeal implements Serializable{

	
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ckNo;
	private int ckType;
	private String ckContent;
	private int ckIf;
	private String ckPrd;
	private int ckSort;
	private String roleNo;
	private String dealNo;
	private String isChecked;
	private String userId;
	  
	public String getCkNo() {
		return ckNo;
	}
	public void setCkNo(String ckNo) {
		this.ckNo = ckNo;
	}
	public int getCkType() {
		return ckType;
	}
	public void setCkType(int ckType) {
		this.ckType = ckType;
	}
	public String getCkContent() {
		return ckContent;
	}
	public void setCkContent(String ckContent) {
		this.ckContent = ckContent;
	}
	public int getCkIf() {
		return ckIf;
	}
	public void setCkIf(int ckIf) {
		this.ckIf = ckIf;
	}
	public String getCkPrd() {
		return ckPrd;
	}
	public void setCkPrd(String ckPrd) {
		this.ckPrd = ckPrd;
	}
	public int getCkSort() {
		return ckSort;
	}
	public void setCkSort(int ckSort) {
		this.ckSort = ckSort;
	}
	public String getRoleNo() {
		return roleNo;
	}
	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getIsChecked() {
		return isChecked;
	}
	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
