package com.singlee.capital.loan.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.loan.model.TcLoanCheckbox;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;
import com.singlee.capital.loan.service.TcLoanCheckboxService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TcLoanCheckboxController")
public class TcLoanCheckboxController extends CommonController{
	
	@Resource
	TcLoanCheckboxService tcLoanCheckboxService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTcLoanCheckbox")
	public RetMsg<PageInfo<TcLoanCheckboxMap>> searchTcLoanCheckbox(@RequestBody Map<String,Object> map){
		Page<TcLoanCheckboxMap> page  = tcLoanCheckboxService.searchTcLoanCheckbox(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTcLoanCheckboxGroup")
	public RetMsg<PageInfo<TcLoanCheckboxMap>> searchTcLoanCheckboxGroup(@RequestBody Map<String,Object> map){
		Page<TcLoanCheckboxMap> page  = tcLoanCheckboxService.searchTcLoanCheckboxGroup(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTcLoanCheckboxByRole")
	public RetMsg<PageInfo<TcLoanCheckboxMap>> searchTcLoanCheckboxByRole(@RequestBody Map<String,Object> map){
		Page<TcLoanCheckboxMap> page  = tcLoanCheckboxService.searchTcLoanCheckboxByRole(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/insertTcLoanCheckbox")
	public RetMsg<Serializable> insertTcLoanCheckbox(@RequestBody Map<String,Object> map){
		try {
			String jsonStr = map.get("jsonStr").toString();
			List<TcLoanCheckbox> list = FastJsonUtil.parseArrays(jsonStr, TcLoanCheckbox.class);
			//先删除所有
			if(list.size() > 0){
				map.put("ckType", list.get(0).getCkType());
				map.put("ckPrd", list.get(0).getCkPrd());
				tcLoanCheckboxService.deleteTcLoanCheckboxByPrd(map);
			}
			for (int i = 0; i < list.size(); i++) {
				tcLoanCheckboxService.insertTcLoanCheckbox(list.get(i));
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateTcLoanCheckbox")
	public RetMsg<Serializable> updateTcLoanCheckbox(@RequestBody TcLoanCheckbox model){
		 tcLoanCheckboxService.updateTcLoanCheckbox(model);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTaskCheckboxSelect")
	public RetMsg<List<TcLoanCheckboxMap>> getTaskCheckboxSelect(@RequestBody Map<String,Object> map){
		List<TcLoanCheckboxMap> list = tcLoanCheckboxService.getTaskCheckboxSelect(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTaskCheckboxUnselect")
	public RetMsg<List<TcLoanCheckboxMap>> getTaskCheckboxUnselect(@RequestBody Map<String,Object> map){
		List<TcLoanCheckboxMap> list = tcLoanCheckboxService.getTaskCheckboxUnselect(map);
		return RetMsgHelper.ok(list);
	}
	
}
	
