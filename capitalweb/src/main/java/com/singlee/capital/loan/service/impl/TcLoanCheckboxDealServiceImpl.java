package com.singlee.capital.loan.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.loan.mapper.TcLoanCheckboxDealMapper;
import com.singlee.capital.loan.model.TcLoanCheckboxDeal;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;
import com.singlee.capital.loan.service.TcLoanCheckboxDealService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.mapper.TtFlowRoleUserMapMapper;
import com.singlee.slbpm.service.FlowQueryService;

@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service
public class TcLoanCheckboxDealServiceImpl implements TcLoanCheckboxDealService{
	
	@Resource
	TcLoanCheckboxDealMapper tcLoanCheckboxDealMapper;
	@Autowired
	TtFlowRoleUserMapMapper flowRoleUserMapMapper;//通过用户获得岗位
	@Autowired
	FlowQueryService flowQueryService; //流程节点关联查询checkList
	
	@Override
	public int deleteTcLoanCheckboxDeal(Map<String, Object> map) {
		int flag = tcLoanCheckboxDealMapper.deleteTcLoanCheckboxDeal(map);
		return flag;
	}

	@Override
	public int insertTcLoanCheckboxDeal(Map<String, Object> map) {
		String jsonStr = map.get("jsonStr").toString();
		List<TcLoanCheckboxDeal> list = FastJsonUtil.parseArrays(jsonStr, TcLoanCheckboxDeal.class);
		int flag = 0;
		if(list != null && list.size() >0){
			map.put("ckPrd", list.get(0).getCkPrd());
			map.put("userId", list.get(0).getUserId());
			map.put("dealNo", list.get(0).getDealNo());
			String flowType = ParameterUtil.getString(map, "flowType", "");
			String taskId = ParameterUtil.getString(map, "taskId", "");
			String curInstId = SlSessionHelper.getInstitution().getInstId();//当前用户机构
			String ckPrd = list.get(0).getCkPrd();
			//查询改环节下的审查要素类型
			try {
				//查询当前环节、当前用户所属审批角色
				Map<String,Object> flowMap = new HashMap<String, Object>();
				if(!"".equals(taskId)){
					flowMap = flowQueryService.getUserTaskSetting(taskId, ckPrd);
				}else{
					flowMap = flowQueryService.getFirstUserTaskSetting(ckPrd,flowType,curInstId);
				}
				@SuppressWarnings("unchecked")
				Map<String,String> lst_checklist = (Map<String, String>) flowMap.get("lst_checklist");
				@SuppressWarnings("unchecked")
				List<String> lst_flow_role = (List<String>) flowMap.get("lst_flow_role");
				if(lst_checklist!=null){
					String ck_type_list = ParameterUtil.getString(lst_checklist, "CK_TYPE", "");
					String[] ck_type_arr = ck_type_list.split(",");
					map.put("ck_type_list", ck_type_arr);
					if(lst_flow_role.size() > 0){
						map.put("roleNo", lst_flow_role.get(0));
					}
					//先删除
					flag = tcLoanCheckboxDealMapper.deleteTcLoanCheckboxDeal(map);
					//后添加
					map.put("loan_list", list);
				}
	 			flag = tcLoanCheckboxDealMapper.insertTcLoanCheckboxDeal(map);
			} catch (Exception e) {
				//e.printStackTrace();
				throw new RException(e);
			}
		}
		return flag;
	}
	
	
	@Override
	public Map<String,Object> searchTcLoanCheckboxDeal(
			Map<String, Object> map) {
		/**
		 * 通过当前的用户  判断是否在审批岗位上，且该审批岗位对应的环节上有checklist（产品）
		 */
		String curInstId = SlSessionHelper.getInstitution().getInstId();//当前用户机构
		String product_no = ParameterUtil.getString(map, "ckPrd", "");//产品
		String flow_type = ParameterUtil.getString(map, "flowType", "");//流程类型
		String taskId = ParameterUtil.getString(map, "taskId", "");//环节ID
		String dealNo =  ParameterUtil.getString(map, "dealNo", "");//交易编号
		String optype = ParameterUtil.getString(map, "optype", "");//操作类型
		Map<String,Object> resultMap = new HashMap<String, Object>();
		List<TcLoanCheckboxMap> list = new ArrayList<TcLoanCheckboxMap>();
		TcLoanCheckboxMap tcLoanCheckboxMap = new TcLoanCheckboxMap();
		try {
			if("".equals(taskId)){
				//1、通过产品号、流程类型、机构获取checklist
				//{lst_checklist={CK_PRD=308, CK_TYPE=1,2,3}, lst_flow_role=[4222]}
				Map<String,Object> flowMap = flowQueryService.getFirstUserTaskSetting(product_no, flow_type, curInstId);
				@SuppressWarnings("unchecked")
				Map<String,String> lst_checklist = (Map<String, String>) flowMap.get("lst_checklist");
				if(lst_checklist!=null){
					String lstPrdNo = ParameterUtil.getString(lst_checklist, "CK_PRD", "");
					String ck_type_list = ParameterUtil.getString(lst_checklist, "CK_TYPE", "");
					String[] ck_type_arr = ck_type_list.split(",");
					map.put("prdNo", lstPrdNo);//截取多个ckNo
					map.put("ck_type_list", ck_type_arr);
					map.put("dealNo", dealNo);
					if("".equals(dealNo)){
						list = tcLoanCheckboxDealMapper.getTcLoanCheckboxByPrdNo(map);
					}else{
						list = tcLoanCheckboxDealMapper.searchTcLoanCheckboxDeal(map);
						tcLoanCheckboxMap = tcLoanCheckboxDealMapper.getMaxTcLoanCheckboxByDealNo(map);
					}
				}
				if("detail".equals(optype)){
					map.put("dealNo", dealNo);
					list = tcLoanCheckboxDealMapper.searchTcLoanCheckboxDeal(map);
					tcLoanCheckboxMap = tcLoanCheckboxDealMapper.getMaxTcLoanCheckboxByDealNo(map);
				}
			}else{
				//2、通过流程id、环节ID获取checklist
				Map<String,Object> flowMap = flowQueryService.getUserTaskSetting(taskId, product_no);
				@SuppressWarnings("unchecked")
				Map<String,String> lst_checklist = (Map<String, String>) flowMap.get("lst_checklist");
				if(lst_checklist!=null){
					String lstPrdNo = ParameterUtil.getString(lst_checklist, "CK_PRD", "");
					String ck_type_list = ParameterUtil.getString(lst_checklist, "CK_TYPE", "");
					String[] ck_type_arr = ck_type_list.split(",");
					map.put("prdNo", lstPrdNo);//截取多个ckNo
					map.put("ck_type_list", ck_type_arr);
					list = tcLoanCheckboxDealMapper.getTcLoanCheckboxByCkno(map);
					tcLoanCheckboxMap = tcLoanCheckboxDealMapper.getMaxTcLoanCheckbox(map);
				}	
			}
			//查询表头
			resultMap.put("loanMode", tcLoanCheckboxMap);
			resultMap.put("loanList", list);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return resultMap;
	}

	@Override
	public int updateTcLoanCheckboxDeal(Map<String, Object> map) {
		TcLoanCheckboxDeal bean = new TcLoanCheckboxDeal();
		BeanUtil.populate(bean, map);
		return tcLoanCheckboxDealMapper.updateByPrimaryKey(bean);
	}

	@Override
	public TcLoanCheckboxMap getMaxTcLoanCheckbox(Map<String, Object> map) {
		return tcLoanCheckboxDealMapper.getMaxTcLoanCheckbox(map);
	}

}
