package com.singlee.capital.loan.mapper;


import java.util.List;
import java.util.Map;
import tk.mybatis.mapper.common.Mapper;
import com.singlee.capital.loan.model.TcLoanCheckboxDeal;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;

public interface TcLoanCheckboxDealMapper extends Mapper<TcLoanCheckboxDeal>{
	
	public List<TcLoanCheckboxMap> searchTcLoanCheckboxDeal(Map<String, Object> map);
	public int updateTcLoanCheckboxDeal(Map<String, Object> map);
	public int deleteTcLoanCheckboxDeal(Map<String, Object> map);
	public int insertTcLoanCheckboxDeal(Map<String,Object> map);
	public List<TcLoanCheckboxMap> getTcLoanCheckboxByCkno(Map<String, Object> map);
	public List<TcLoanCheckboxMap> getTcLoanCheckboxByPrdNo(Map<String, Object> map);
	public TcLoanCheckboxMap getMaxTcLoanCheckbox(Map<String, Object> map);
	public TcLoanCheckboxMap getMaxTcLoanCheckboxByDealNo(Map<String, Object> map);
}
