package com.singlee.capital.loan.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class TcLoanCheckboxMap implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_LOAN_CHECKBOX.NEXTVAL FROM DUAL")
	private String ckNo;//序列号
	private String ckType;//要点类型
	private String ckContent;//要点内容
	private String ckIf;//是否必选
	private String ckPrd;//产品类型
	private String ckSort;//排序字段
	private String roleNo;//角色ID
	private String prdName;//产品名称
	private String roleName;//角色名称 
	private String dealNo;//交易编号
	private String isChecked;//
	private String userId;
	private String userName;
	private String maxuserId;
	private String maxuserName;
	private String maxroleName;
	private String maxroleNo;
	
	public String getCkNo() {
		return ckNo;
	}
	public void setCkNo(String ckNo) {
		this.ckNo = ckNo;
	}
	public String getCkType() {
		return ckType;
	}
	public void setCkType(String ckType) {
		this.ckType = ckType;
	}
	public String getCkContent() {
		return ckContent;
	}
	public void setCkContent(String ckContent) {
		this.ckContent = ckContent;
	}
	public String getCkPrd() {
		return ckPrd;
	}
	public void setCkPrd(String ckPrd) {
		this.ckPrd = ckPrd;
	}
	public String getCkSort() {
		return ckSort;
	}
	public void setCkSort(String ckSort) {
		this.ckSort = ckSort;
	}
	public String getCkIf() {
		return ckIf;
	}
	public void setCkIf(String ckIf) {
		this.ckIf = ckIf;
	}
	public String getRoleNo() {
		return roleNo;
	}
	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getIsChecked() {
		return isChecked;
	}
	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMaxuserId() {
		return maxuserId;
	}
	public void setMaxuserId(String maxuserId) {
		this.maxuserId = maxuserId;
	}
	public String getMaxuserName() {
		return maxuserName;
	}
	public void setMaxuserName(String maxuserName) {
		this.maxuserName = maxuserName;
	}
	public String getMaxroleName() {
		return maxroleName;
	}
	public void setMaxroleName(String maxroleName) {
		this.maxroleName = maxroleName;
	}
	public String getMaxroleNo() {
		return maxroleNo;
	}
	public void setMaxroleNo(String maxroleNo) {
		this.maxroleNo = maxroleNo;
	}
	
}
