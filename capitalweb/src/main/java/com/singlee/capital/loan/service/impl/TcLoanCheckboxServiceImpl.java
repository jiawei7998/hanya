package com.singlee.capital.loan.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.loan.mapper.TcLoanCheckboxMapper;
import com.singlee.capital.loan.model.TcLoanCheckbox;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;
import com.singlee.capital.loan.service.TcLoanCheckboxService;

@Service
public class TcLoanCheckboxServiceImpl implements TcLoanCheckboxService{

	@Resource
	TcLoanCheckboxMapper tcLoanCheckboxMapper;
	
	@Override
	public int deleteTcLoanCheckbox(TcLoanCheckbox model) {
		return tcLoanCheckboxMapper.deleteByPrimaryKey(model);
	}

	@Override
	public int insertTcLoanCheckbox(TcLoanCheckbox model) {
		return tcLoanCheckboxMapper.insert(model);
	}

	@Override
	public Page<TcLoanCheckboxMap> searchTcLoanCheckbox(Map<String, Object> map,
			RowBounds rb) {
		return tcLoanCheckboxMapper.searchTcLoanCheckbox(map, rb);
	}

	@Override
	public int updateTcLoanCheckbox(TcLoanCheckbox model) {
		return tcLoanCheckboxMapper.updateByPrimaryKey(model);
	}

	@Override
	public int deleteTcLoanCheckboxByPrd(Map<String, Object> map) {
		return tcLoanCheckboxMapper.deleteTcLoanCheckboxByPrd(map);
	}

	@Override
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxGroup(
			Map<String, Object> map, RowBounds rb) {
		return tcLoanCheckboxMapper.searchTcLoanCheckboxGroup(map, rb);
	}

	@Override
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxByRole(
			Map<String, Object> map, RowBounds rb) {
		return tcLoanCheckboxMapper.searchTcLoanCheckboxByRole(map, rb);
	}

	@Override
	public int updateTcLoanCheckboxRole(Map<String, Object> map) {
		String ckTypeStr = map.get("ckType").toString();
		String ckPrdStr = map.get("ckPrd").toString();
		String roleNo = map.get("roleNo").toString();
		Map<String,Object> params = new HashMap<String, Object>();
		int i = 0;
		if(!"".equals(ckTypeStr) && !"".equals(ckPrdStr)){
			String[] ckTypeArr = ckTypeStr.split(",");
			String[] ckPrdArr = ckPrdStr.split(",");
			for (int j = 0; j < ckPrdArr.length; j++) {
				params.put("ckType", ckTypeArr[j]);
				params.put("ckPrd", ckPrdArr[j]);
				params.put("roleNo", roleNo);
				tcLoanCheckboxMapper.updateTcLoanCheckboxRole(params);
			}
			i=1;
		}
		return i;
	}

	@Override
	public List<TcLoanCheckbox> getTcLoanCheckboxList(Map<String, Object> map) {
		return null;
	}

	@Override
	public List<TcLoanCheckboxMap> getTaskCheckboxSelect(Map<String, Object> map) {
		return tcLoanCheckboxMapper.getTaskCheckboxSelect(map);
	}

	@Override
	public List<TcLoanCheckboxMap> getTaskCheckboxUnselect(
			Map<String, Object> map) {
		return tcLoanCheckboxMapper.getTaskCheckboxUnselect(map);
	}

}
