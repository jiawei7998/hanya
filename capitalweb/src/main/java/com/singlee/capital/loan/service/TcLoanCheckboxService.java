package com.singlee.capital.loan.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.loan.model.TcLoanCheckbox;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;

public interface TcLoanCheckboxService {

	public int insertTcLoanCheckbox(TcLoanCheckbox model);
	public int updateTcLoanCheckbox(TcLoanCheckbox model);
	public int deleteTcLoanCheckbox(TcLoanCheckbox model);
	public Page<TcLoanCheckboxMap> searchTcLoanCheckbox(Map<String, Object> map , RowBounds rb);
	public int deleteTcLoanCheckboxByPrd(Map<String, Object> map);
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxGroup(Map<String, Object> map , RowBounds rb);
	public Page<TcLoanCheckboxMap> searchTcLoanCheckboxByRole(Map<String, Object> map , RowBounds rb);
	public int updateTcLoanCheckboxRole(Map<String, Object> map);
	public List<TcLoanCheckbox> getTcLoanCheckboxList(Map<String, Object> map);
	
	public List<TcLoanCheckboxMap> getTaskCheckboxSelect(Map<String, Object> map);
	public List<TcLoanCheckboxMap> getTaskCheckboxUnselect(Map<String, Object> map);
	
}
