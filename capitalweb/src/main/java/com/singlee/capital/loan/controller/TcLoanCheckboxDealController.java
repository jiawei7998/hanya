package com.singlee.capital.loan.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.loan.model.TcLoanCheckboxMap;
import com.singlee.capital.loan.service.TcLoanCheckboxDealService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/TcLoanCheckboxDealController")
public class TcLoanCheckboxDealController extends CommonController{
	
	@Resource
	TcLoanCheckboxDealService tcLoanCheckboxDealService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTcLoanCheckboxDeal")
	public RetMsg<Map<String,Object>> searchTcLoanCheckboxDeal(@RequestBody Map<String,Object> map){
		Map<String,Object> page  = tcLoanCheckboxDealService.searchTcLoanCheckboxDeal(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/insertTcLoanCheckboxDeal")
	public RetMsg<Serializable> insertTcLoanCheckboxDeal(@RequestBody Map<String,Object> map){
		tcLoanCheckboxDealService.insertTcLoanCheckboxDeal(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getMaxTcLoanCheckbox")
	public RetMsg<TcLoanCheckboxMap> getMaxTcLoanCheckbox(@RequestBody Map<String,Object> map){
		TcLoanCheckboxMap page  = tcLoanCheckboxDealService.getMaxTcLoanCheckbox(map);
		return RetMsgHelper.ok(page);
	}
	
}
