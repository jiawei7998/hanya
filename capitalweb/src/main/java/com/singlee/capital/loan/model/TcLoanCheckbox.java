package com.singlee.capital.loan.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TC_LOAN_CHECKBOX")
public class TcLoanCheckbox implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_LOAN_CHECKBOX.NEXTVAL FROM DUAL")
	private String ckNo;//序列号
	private String ckType;//要点类型
	private String ckContent;//要点内容
	private String ckIf;//是否必选
	private String ckPrd;//产品类型
	private String ckSort;//排序字段
	
	public String getCkNo() {
		return ckNo;
	}
	public void setCkNo(String ckNo) {
		this.ckNo = ckNo;
	}
	public String getCkType() {
		return ckType;
	}
	public void setCkType(String ckType) {
		this.ckType = ckType;
	}
	public String getCkContent() {
		return ckContent;
	}
	public void setCkContent(String ckContent) {
		this.ckContent = ckContent;
	}
	public String getCkPrd() {
		return ckPrd;
	}
	public void setCkPrd(String ckPrd) {
		this.ckPrd = ckPrd;
	}
	public String getCkSort() {
		return ckSort;
	}
	public void setCkSort(String ckSort) {
		this.ckSort = ckSort;
	}
	public String getCkIf() {
		return ckIf;
	}
	public void setCkIf(String ckIf) {
		this.ckIf = ckIf;
	}
}
