package com.singlee.capital.calendar.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.market.mapper.CalendarDao;
import com.singlee.capital.market.model.TtCalendarDate;

public class CalendarUtil {

	/**
	 * 数据库中存在的所有的节假日
	 */
	private static List<TtCalendarDate> calendarList;

	/**
	 * 定义一个日期标杆，2018-01-01这天是星期一
	 */
	private static final String DATE_FLAG = "2018-01-01";

	@Autowired
	private static CalendarDao calendarDao;

	/**
	 * 判断输入日期是否为工作日，如果不是返回之前或之后第一个工作日
	 * 
	 * @param abj正数往后 ，负数往前
	 * @return
	 */
	public String getWeekday(String date) {
		// 判断工作日列表是否已经赋值
		if (calendarList == null) {
			getWeekends();
		}

		// 正数往后，负数往前
		int adj = 1;

		// 返回查询后的工作日
		// 如果不是2017年，则按周末为休息日处理
		if (!isNowYear(date)) {
			date = getWeekday(date, adj);
			return date;
		} else {
			for (TtCalendarDate t : calendarList) {
				if (t.getCal_date().equals(date)) {
					date = DateUtil.dateAdjust(date, adj);
				}
			}
		}
		return date;
	}

	/**
	 * 判断输日期是否为工作日，如果不是返回之前或之后第days个工作日 （主要的控制层，前台可以调用此方法）
	 * 
	 * @param abj 正数往后，负数往前
	 * @param day 相隔第days个工作日
	 * @return
	 */
	public String getWeekdayNearDay(String date, int days) {
		if (calendarList == null) {
			getWeekends();
		}

		// 初始化需要查询的日期
		int adj = 1;

		// 判断第一天是否是休息日，拿到工作日
		date = getWeekday(date);

		// 如果不是2017年，则按周末为休息日处理
		if (!isNowYear(date)) {
			// 拿到后面第days个工作日
			for (int ii = 0; ii < days; ii++) {
				do {
					date = DateUtil.dateAdjust(date, adj);
				} while (isWeekend(date));
			}
			return date;
		} else {
			// 拿到后面第days个工作日
			for (int iii = 0; iii < days; iii++) {
				do {
					// 此时的date为处理结果 return
					date = DateUtil.dateAdjust(date, adj);
				} while (isWeekend(calendarList, date));
			}
		}
		return date;
	}

	/**
	 * 拿到所有符合要求的日历
	 * 
	 * @param
	 * @return
	 */
	public static void getWeekends() {
		// TODO Auto-generated method stub
		TtCalendarDate cal = new TtCalendarDate();
		cal.setCal_code("CHINA_SZEX");
		cal.setCal_flag("0");
		calendarList = calendarDao.loadCalendarDate(cal);
	}

	/**
	 * 判断日期是否是节假日，数据库中存的休息日
	 * 
	 * @param calendarList
	 * @param tcd
	 * @return
	 */
	public static boolean isWeekend(List<TtCalendarDate> calendarList, String date) {
		for (TtCalendarDate t : calendarList) {
			if (t.getCal_date().equals(date)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断日期是否是周末（星期六、星期天）不包括其他法定假日
	 * 
	 * @return true\false
	 */
	public static final boolean isWeekend(String date) {
		int days = DateUtil.daysBetween(DATE_FLAG, date);
		int isWeek = days % 7;
		if ((isWeek == 5) || (isWeek == 6) || (isWeek == -1) || (isWeek == -2)) {
			return true;
		}
		return false;
	}

	/**
	 * 判断日期是不是2017年
	 */
	public static final boolean isNowYear(String date) {
		java.util.Date dateLate;
		Calendar ecal = Calendar.getInstance();
		try {
			dateLate = DateUtils.parseDate(date, "yyyy-MM-dd");
			ecal.setTime(dateLate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		boolean same = (ecal.get(Calendar.YEAR) == 2017);
		return same;
	}

	/**
	 * 判断两个日期是不是同一年
	 */
	public static final boolean sameYear(java.util.Date early, java.util.Date late) {
		Calendar ecal = Calendar.getInstance();
		Calendar lcal = Calendar.getInstance();
		ecal.setTime(early);
		lcal.setTime(late);

		boolean same = (ecal.get(Calendar.YEAR) == lcal.get(Calendar.YEAR));
		return same;
	}

	/**
	 * 在不是2017年的情况下 判断输入日期是否为工作日，如果不是返回之前或之后第一个工作日
	 * 
	 * @param date 传入的日期
	 * @param adj  正数往后，负数往前
	 * @return 返回工作日
	 */
	public static final String getWeekday(String date, int adj) {
		while (isWeekend(date)) {
			date = DateUtil.dateAdjust(date, adj, Calendar.DAY_OF_YEAR);
		}
		return date;
	}

	/**
	 * 当月第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * 当月最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		Date date2 = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.DAY_OF_MONTH, 0);
		return cal2.getTime();
	}

	/**
	 * 转换日期
	 * 
	 * @param dateStr 日期字符串
	 * @param format  日期的字符串格式如yyyyMMdd
	 * @return Date 日期
	 * @throws ParseException
	 * @throws ParseException 日期解析异常
	 */
	public static Date getUtilDate(String dateStr, String format) throws ParseException {
		if (dateStr == null || dateStr.length() == 0) {
			return null;
		}
		format = format == null ? "yyyy-MM-dd HH:mm:ss" : format;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(dateStr);
	}

	/**
	 * 转换日期
	 * 
	 * @param date   日期
	 * @param format 日期的字符串格式如yyyyMMdd
	 * @return 日期的字符串形式如yyyyMMdd
	 */
	public static String getDateStr(Date date, String format) {
		if (date == null) {
			return null;
		}
		format = format == null ? "yyyy-MM-dd HH:mm:ss" : format;
		if ("CNS".equals(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String[] str = sdf.format(date).split("-");
			return str[0] + "年" + str[1] + "月" + str[2] + "日";
		} else if ("SSS".equals(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			return sdf.format(date);
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
}
