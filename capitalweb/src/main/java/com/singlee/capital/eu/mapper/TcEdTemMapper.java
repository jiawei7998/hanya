package com.singlee.capital.eu.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TcEdTerm;

import tk.mybatis.mapper.common.Mapper;
/**
 * 额度期限查询
 * @author SINGLEE
 *
 */
public interface TcEdTemMapper extends Mapper<TcEdTerm>{
	/**
	 * 分页查询获得所以的期限
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TcEdTerm> getTcEdTermPage(Map<String, Object> map,RowBounds rowBounds);
	
}
