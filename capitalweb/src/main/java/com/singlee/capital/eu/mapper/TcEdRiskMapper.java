package com.singlee.capital.eu.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TcEdRisk;

public interface TcEdRiskMapper extends Mapper<TcEdRisk>{
	/**
	 * 获取所以的系统额度中类
	 * @param prdNo
	 * @return
	 */
	public Page<TcEdRisk> getTcEdRisksPage(Map<String, Object> map,RowBounds rowBounds);
	
	public List<TcEdRisk> getChooseEdRisks(Map<String, Object> map);
	
	public TcEdRisk getCRMSType(@Param("prdNo")String prdNo);
	
	public String getCreditCode(Map<String, Object> map);
	
}
