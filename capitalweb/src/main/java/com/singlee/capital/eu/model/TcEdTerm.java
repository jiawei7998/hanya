package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 额度期限表
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TC_ED_TERM")
public class TcEdTerm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 期限ID
	 */
	private String termId;
	/**
	 * 期限类型
	 */
	private String termType;
	/**
	 * 期限名称
	 */
	private String termName;
	
	/**
	 * VDATE+期限的实际到期日-业务到期日
	 * @return
	 */
	@Transient
	private int termDays;
	
	public int getTermDays() {
		return termDays;
	}
	public void setTermDays(int termDays) {
		this.termDays = termDays;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType;
	}
	public String getTermName() {
		return termName;
	}
	public void setTermName(String termName) {
		this.termName = termName;
	}
	@Override
	public String toString() {
		return "TcEdTem [termId=" + termId + ", termType=" + termType
				+ ", termName=" + termName + "]";
	}
	
}
