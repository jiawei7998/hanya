package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 额度中类 风险级别
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TC_ED_RISK")
public class TcEdRisk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 额度中类ID
	 */
	@Id
	private String creditId;
	/**
	 * 额度中类名称
	 */
	private String creditName;
	/**
	 * 额度风险级别
	 */
	private int riskLevel;
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
	public int getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}
	@Override
	public String toString() {
		return "TcEdRisk [creditId=" + creditId + ", creditName=" + creditName
				+ ", riskLevel=" + riskLevel + "]";
	}
	
	
}
