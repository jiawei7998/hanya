package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IFS_RISK_MANAGER")
public class OpicsRisk implements Serializable{
	
	private static final long serialVersionUID = 1L;
	/**
	 * 流水号
	 */
	private String dealNo;
	/**
	 * 产品编码
	 */
	private String productCode;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 校验准入
	 */
	private String checkAccess;
	/**
	 * 校验授信
	 */
	private String checkLimit;
	/**
	 * 校验单笔风险
	 */
	private String checkRisk;
	/**
	 * 经办员
	 */
	private String operator;
	/**
	 * 录入时间
	 * @return
	 */
	private String dateTime;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCheckAccess() {
		return checkAccess;
	}
	public void setCheckAccess(String checkAccess) {
		this.checkAccess = checkAccess;
	}
	public String getCheckLimit() {
		return checkLimit;
	}
	public void setCheckLimit(String checkLimit) {
		this.checkLimit = checkLimit;
	}
	public String getCheckRisk() {
		return checkRisk;
	}
	public void setCheckRisk(String checkRisk) {
		this.checkRisk = checkRisk;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
}
