package com.singlee.capital.eu.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TdEdCustBatchStatusMapper extends Mapper<TdEdCustBatchStatus> {
	/**
	 * 根据交易获得 （dealNo以外）的所有需要占用额度的交易
	 * 
	 * @param dealNo
	 * @return
	 */
	public List<EdInParams> getTdEdInParamsByDealNo(String dealNo);

	/**
	 * 根据交易删除交易关联客户的额度批次信息
	 * 
	 * @param dealNo
	 */
	public void deleteEdCustBatchLogByDealNo(String dealNo);

	/**
	 * 根据客户代码查找全部占用交易
	 * 
	 * @param custNo
	 * @return
	 */
	public List<EdInParams> getTdEdInParamsByCustNo(Map<String, Object> paramsMap);

	/**
	 * 删除额度批次最新信息
	 * 
	 * @param dealNo
	 */
	public void deleteEdCustBatchLogByCustNo(Map<String, Object> paramsMap);

	/**
	 * 根据交易获得 交易所辖的成功的额度占用信息
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List<EdInParams> getTdEdInParamsByDealNo2(Map<String, Object> paramsMap);

	/**
	 * 删除客户及流水号一致的数据
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean deleteTdEdInParamsByCustNos(Map<String, Object> paramsMap);

	/**
	 * 根据交易查询所以结果
	 * 
	 * @param paramsMap
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustBatchForDealNoOrPrdNo(Map<String, Object> paramsMap, RowBounds rowBounds);
	
	/**
	 * 获取额度占用历史数据
	 * @param paramsMap
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustLogForDealNoOrPrdNo(Map<String, Object> paramsMap, RowBounds rowBounds);

	
	
	/**
     * 获取额度占用历史数据
     * @param paramsMap
     * @param rowBounds
     * @return
     */
    public Page<TdEdCustBatchStatus> selectEdCustLogAll(Map<String, Object> paramsMap, RowBounds rowBounds);
	/**
	 * 获取额度占用历史数据用list
	 * @param paramsMap
	 * @param rowBounds
	 * @return
	 */
	public List<TdEdCustBatchStatus> selectEdCustLogForDealNoWithList(Map<String, Object> paramsMap);

	
	/**
     * 获取额度占用和释放消息用list
     * @param paramsMap
     * @param rowBounds
     * @return
     */
    public List<TdEdCustBatchStatus> selectEdCustLogListAll(Map<String, Object> paramsMap);


	 List<TdEdCustBatchStatus> selectEdCustLogListAlls(Map<String, Object> paramsMap);

	/**
	 * 获取存续期存在额度占用的数据
	 * 
	 * @param paramsMap
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustForDuration(Map<String, Object> paramsMap, RowBounds rowBounds);

	// 取所有额度批次信息
	List<TdEdCustBatchStatus> getAllCustBatch();
	//权重流水号关联信息增加
	public void DealNoAndWeightinsert(TdEdCustBatchStatus tdEdCustBatchStatus);
	
	/**
	 * 删除权重流水号关联的信息
	 * 
	 * @param dealNo
	 */
	public void DealNoAndWeightdelete(@Param(value="dealNo")String dealNo);
	
	/**
	 * 获取手工录入记录的DealNo
	 */
	List<TdEdCustBatchStatus> getAllMDateDeal(@Param(value="nowDate")String nowDate);
	
	/**
	 * 获取所有未到期的交易信息
	 */
	List<TdEdCustBatchStatus> getAllNotDuedateDeal(@Param(value="serial_no")String serial_no);
	
	/**
	 * 获取未到期的额度交易
	 * @param nowDate
	 * @return
	 */
	List<TdEdCustBatchStatus> getNotTodayMDateDeal(@Param(value="nowDate")String nowDate);
}