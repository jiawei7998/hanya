package com.singlee.capital.eu.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.mapper.TcEdTemMapper;
import com.singlee.capital.eu.model.TcEdTerm;
import com.singlee.capital.eu.service.TcEdTermService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcEdTermServiceImpl implements TcEdTermService {
	@Autowired
	TcEdTemMapper edTemMapper;
	@Override
	public Page<TcEdTerm> getTcEdTermsPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return edTemMapper.getTcEdTermPage(map, ParameterUtil.getRowBounds(map));
	}

}
