package com.singlee.capital.eu.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.eu.model.TdEdCustOp;

public interface TdEdCustOpMapper extends Mapper<TdEdCustOp>{

	public void deleteEdCustOpByDealNo(String dealNo);
	
	public void deleteEdCustOpByCustNo(Map<String,Object> paramsMap);
}
