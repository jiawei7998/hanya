package com.singlee.capital.eu.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.model.TdEdDealLog;

public interface TdEdDealLogMapper extends Mapper<TdEdDealLog>{

	/**
	 * <!-- 读取交易单编号 -->
	<select id="getTdEdDealLogBatchId" resultType="String" flushCache="true">
		SELECT BASEUTIL.GETEDID FROM DUAL
	</select>
	 */
	public String getTdEdDealLogBatchId();
	
	public void deleteTdEdDealLogByBatchId(String batchId);
	
	/**
	 * 根据客户号，额度编号，额度操作类型查询
	 * @return
	 */
	public Page<TdEdDealLog> getTdEdDealLogs(Map<String, Object> map,RowBounds rowBounds);
	/**
	 * 交易角度查询
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdDealLog> getTdEdDealLogsByDealNo(Map<String, Object> map,RowBounds rowBounds);
	
	//通过dealNo查询预占用信息
	public TdEdCustBatchStatus getPerusedDealNo(Map<String, Object> map);
	
	//更新预占用到占用明细
	public int updatePerusedStatus(Map<String, Object> map);

	//更新预占用总额度
	public int updatePerusedAmt(Map<String, Object> map);
	
	//跟新单笔状态记录
	public int updateCustStatus (Map<String, Object> map);
	
	
	public int updateNoUseDealStatus (@Param("dealNo")String dealNo);
	
	
	public void InsertDealLog(Map<String, Object> map);
	

	public void deleteDealLog(@Param("dealNo")String dealNo);
	
	
	public TdEdDealLog selectDealLog(@Param("dealNo")String dealNo);
}
