package com.singlee.capital.eu.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * IFBM 额度批次记录表
 * 
 * @author SINGLEE
 * 
 */
@Entity
@Table(name = "TD_ED_CUST_BATCH_STATUS")
public class TdEdCustBatchStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dealNo; // 业务流水号

	private String batchTId;// 额度操作总批次

	private String batchCId;// 额度操作子批次

	private String dealType; // 交易类型

	private String vDate;// 起息日

	private String mDate; // 到期日

	private String prdNo; // 产品代码

	private String partyId; // 参与方

	private double amt; // 批次操作的金额
	
	private double amtAll; // 批次操作的金额不占用权重

	private String ecifNo; // 客户代码

	private String batchRefNo; // 批次关联流水号

	private String batchEvent; // 批次事件

	private String batchStatus;// 批次状态

	private String batchTime;// 操作时间
	
	private String weight;//权重
	
	private String instId;//组织机构号
	
	private String edOpType;//额度操作类型
	
	private String currency;
	private String cfn;

	@Transient
	private String partyName;
	@Transient
	private String prdName;
	@Transient
	private String cnName;

	private String ecifName; // 客户代码

	private String acctngtype; // 客户类型

	private String acctdesc; // 客户类型描述

	private String instName;	//组织机构名称
	
	//额度统一代码编号
	@Transient
	private String custCreditId;
	
	//已占用额度
	@Transient
	private double usedAmt;
	
	//预占用额度
	@Transient
	private double preUsedAmt;
	
	@Transient
	private String operator;
	
	@Transient
	private String creditId;
	
	@Transient
	private String creditName;
	
	/**
	 * 客户可用授信额度总额
	 */
	@Transient
	private double avlAmtEcif;
	/**
	 * 授信产品可用授信额度
	 */
	@Transient
	private double avlAmtCredit;
	/**
	 * 授信产品可用授信额度总额
	 */
	@Transient
	private double loanAmtEcif;
	/**
	 * 授信产品可用额度总额
	 */
	@Transient
	private double loanAmtCredit;
	
	
	/**
     * 本次操作金额
     */
    @Transient
    private double edOpAmt;

	public String getCfn() {
		return cfn;
	}

	public void setCfn(String cfn) {
		this.cfn = cfn;
	}

	public String getEdOpType() {
        return edOpType;
    }

    public void setEdOpType(String edOpType) {
        this.edOpType = edOpType;
    }

    public double getEdOpAmt() {
        return edOpAmt;
    }

    public void setEdOpAmt(double edOpAmt) {
        this.edOpAmt = edOpAmt;
    }

    public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getBatchTId() {
		return batchTId;
	}

	public void setBatchTId(String batchTId) {
		this.batchTId = batchTId;
	}

	public String getBatchCId() {
		return batchCId;
	}

	public void setBatchCId(String batchCId) {
		this.batchCId = batchCId;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getEcifNo() {
		return ecifNo;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getBatchEvent() {
		return batchEvent;
	}

	public void setBatchEvent(String batchEvent) {
		this.batchEvent = batchEvent;
	}

	public String getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(String batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	public String getBatchTime() {
		return batchTime;
	}

	public void setBatchTime(String batchTime) {
		this.batchTime = batchTime;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCustCreditId() {
		return custCreditId;
	}

	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}

	public double getUsedAmt() {
		return usedAmt;
	}

	public void setUsedAmt(double usedAmt) {
		this.usedAmt = usedAmt;
	}

	public double getPreUsedAmt() {
		return preUsedAmt;
	}

	public void setPreUsedAmt(double preUsedAmt) {
		this.preUsedAmt = preUsedAmt;
	}

	public double getAmtAll() {
		return amtAll;
	}

	public void setAmtAll(double amtAll) {
		this.amtAll = amtAll;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public double getAvlAmtEcif() {
		return avlAmtEcif;
	}

	public void setAvlAmtEcif(double avlAmtEcif) {
		this.avlAmtEcif = avlAmtEcif;
	}

	public double getAvlAmtCredit() {
		return avlAmtCredit;
	}

	public void setAvlAmtCredit(double avlAmtCredit) {
		this.avlAmtCredit = avlAmtCredit;
	}

	public double getLoanAmtEcif() {
		return loanAmtEcif;
	}

	public void setLoanAmtEcif(double loanAmtEcif) {
		this.loanAmtEcif = loanAmtEcif;
	}

	public double getLoanAmtCredit() {
		return loanAmtCredit;
	}

	public void setLoanAmtCredit(double loanAmtCredit) {
		this.loanAmtCredit = loanAmtCredit;
	}

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

	public String getEcifName() {
		return ecifName;
	}

	public void setEcifName(String ecifName) {
		this.ecifName = ecifName;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getAcctdesc() {
		return acctdesc;
	}

	public void setAcctdesc(String acctdesc) {
		this.acctdesc = acctdesc;
	}
}