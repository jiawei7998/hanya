package com.singlee.capital.eu.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.service.TdEdCustBatchStatusService;
import com.singlee.hrbreport.service.ReportService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class TdEdCustBatchStatusServiceImpl implements
		TdEdCustBatchStatusService, ReportService {
	@Autowired
	TdEdCustBatchStatusMapper edCustBatchStatusMapper;
	@Override
	public Page<TdEdCustBatchStatus> selectEdCustBatchForDealNoOrPrdNo(
			Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		
		return edCustBatchStatusMapper.selectEdCustBatchForDealNoOrPrdNo(paramsMap, ParameterUtil.getRowBounds(paramsMap));
	}
	@Override
	public Page<TdEdCustBatchStatus> selectEdCustForDuration(
			Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		return edCustBatchStatusMapper.selectEdCustForDuration(paramsMap, ParameterUtil.getRowBounds(paramsMap));
	}

	
	@Override
	public Page<TdEdCustBatchStatus> selectEdCustLogForDealNoOrPrdNo(Map<String, Object> paramsMap) {
		RowBounds rowBounds=new RowBounds();
		int pageSize= (int) paramsMap.get("pageSize");
		if(pageSize !=-1){
			rowBounds=ParameterUtil.getRowBounds(paramsMap);
		}
		return edCustBatchStatusMapper.selectEdCustLogAll(paramsMap,rowBounds);
	}

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		return null;
	}

	//报表导出
	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		List<TdEdCustBatchStatus> list =null;
		list=edCustBatchStatusMapper.selectEdCustLogListAlls(paramer);
        Map<String,Object> map =new HashMap<>();
        map.put("list",list);
		return map;
	}
}
