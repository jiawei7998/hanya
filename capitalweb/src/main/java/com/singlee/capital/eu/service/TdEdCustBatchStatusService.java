package com.singlee.capital.eu.service;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;

import java.util.Map;
/**
 * 查询所以交易占用情况表服务
 * @author SINGLEE
 *
 */
public interface TdEdCustBatchStatusService {
	/**
	 * 根据dealNo or prdNo获得占用情况
	 * @param paramsMap
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustBatchForDealNoOrPrdNo(Map<String,Object> paramsMap);
	
	/**
	 * 获取存续期占额度的数据
	 * @param paramsMap
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustForDuration(Map<String,Object> paramsMap);
	
	/**
	 * dealNo or prdNo获取额度额度占用历史数据
	 * @param paramsMap
	 * @return
	 */
	public Page<TdEdCustBatchStatus> selectEdCustLogForDealNoOrPrdNo(Map<String,Object> paramsMap);

//	//报表导出
//	Map<String, Object> getReportExcel(Map<String, Object> paramer) ;
}
