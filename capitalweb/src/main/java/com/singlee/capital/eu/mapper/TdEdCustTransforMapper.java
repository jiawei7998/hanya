package com.singlee.capital.eu.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.eu.model.TdEdCustTransfor;

public interface TdEdCustTransforMapper  extends Mapper<TdEdCustTransfor>{

	public void deleteAllTdEdCustTransfor();
	
	public void updateProductCreditTransfor();
}
