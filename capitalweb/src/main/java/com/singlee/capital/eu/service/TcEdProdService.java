package com.singlee.capital.eu.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
/**
 * 额度产品关联匹配服务
 * @author SINGLEE
 *
 */
public interface TcEdProdService {
	
	Page<TcRproductCreditPojo> pageTcRproductCreditPojo(Map<String, Object> map);
	
	void deleteRproductCredit(Map<String, Object> map);
	
	void addRproductCredit(TcRproductCreditPojo tcRproductCreditPojo);
	
	void updateRproductCredit(TcRproductCreditPojo tcRproductCreditPojo);
}
