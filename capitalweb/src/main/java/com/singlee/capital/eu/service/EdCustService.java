package com.singlee.capital.eu.service;

import java.util.List;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.TdEdDealLog;

/**
 * 额度筛选+加工处理服务
 * @author SINGLEE
 *batchId 批次很重要，每次调用必须使用唯一批次
 *String batchId = edDealLogMapper.getTdEdDealLogBatchId();
 */
public interface EdCustService {
	/**
	 * 根据带入参数 进行额度占用，并将额度占用路径返回  额度占用检查
	 * @param edInParams
	 * @return
	 */
	public Pair<EdInParams,List<TdEdDealLog>> doEdOneOccpCheck(Pair<EdInParams,List<TdEdDealLog>> pair,EdInParams edInParams,String batchId,boolean isCopyTry);
	
	/**
	 * 将额度占用情况进行保存
	 * @param pair
	 */
	public boolean doEdOneInsertTdEdDealLog(Pair<EdInParams,List<TdEdDealLog>> pair,String batchId,boolean isCopyTry);
	
	/**
	 * 根据占用列表进行额度占用
	 * @param edInParams
	 * Pair<Boolean, List<String>>
	 * 返回是否占用成功 及 批次号
	 * @return
	 */
	public  Pair<Boolean, List<TdEdDealLog>> doEdOccp(List<EdInParams> edInParams,String batchEvent,String dealNo);
	public Pair<Boolean, List<TdEdDealLog>> doEdOccp(List<EdInParams> edInParams,String batchEvent,List<String> custNos,List<String> dealNos);
	/**
	 * 根据交易释放所有关联额度
	 * 异常需要回滚
	 * @param dealNo
	 * @return
	 */
	public boolean releaseEdByDealNo(String dealNo);
	/**
	 * 根据客户信息释放额度 
	 * 异常需要回滚
	 * @param dealNo
	 * @return
	 */
	public Pair<Boolean, List<TdEdDealLog>> releaseEdByCustNo(List<String> custNos,List<EdInParams> enEdInParams,List<String> dealNos);
	
	
	/*****************************************************************************************************************/
	public Pair<EdInParams,List<TdEdDealLog>> doEdOneOccpCheckTransfor(Pair<EdInParams,List<TdEdDealLog>> pair,EdInParams edInParams,String batchId,boolean isCopyTry);
	public List<Pair<EdInParams,List<TdEdDealLog>>> releaseEdByCustNoTransfor(List<String> custNos,List<EdInParams> enEdInParams,List<String> dealNos);
	public List<Pair<EdInParams,List<TdEdDealLog>>> doEdOccpTransfor(List<EdInParams> edInParams,String batchEvent,List<String> custNos,List<String> dealNos);
}
