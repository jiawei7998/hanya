package com.singlee.capital.eu.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.eu.model.TdEdCustCopy;

public interface TdEdCustCopyMapper extends Mapper<TdEdCustCopy>{

	/**
	 * 根据 客户号
	 * @param paramsMap
	 */
	public void insertEdCustCopy();
	public List<TdEdCustCopy> selectEdCustCopyForLock();
	public void deleteEdCustCopy();
	
}
