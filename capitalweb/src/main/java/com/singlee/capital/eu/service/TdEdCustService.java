package com.singlee.capital.eu.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.capital.eu.model.TdEdCust;

public interface TdEdCustService {
	/**
	 * 分页查询 TD_ED_CUST记录数据
	 * 根据 ORDER BY OP_TIME,ECIF_NUM
	 * @param map
	 * @return
	 */
	public Page<TdEdCust> getTdEdCustsPage(Map<String, Object> map);
	/**
	 * 分页查询 TD_ED_CUST记录明细 关联  TD_ED_CUST_OP数据
	 * 根据 ORDER BY OP_TIME,ECIF_NUM
	 * @param map
	 * @return
	 */
	public Page<TdEdCust> getTdEdCustDetailPage(Map<String, Object> map);
	
	
	public Page<TdEdCust> getEdCustForHandlePage(Map<String, Object> map);
	/**
	 * 额度查询
	 * @param map
	 * @return
	 */
	public Page<TdEdCust> getEdCustForQueryPage(Map<String, Object> map);
	public Page<TdEdCust> getEdCustForQueryDetailPage(Map<String, Object> map);
	
	
	public List<TdEdCust> getEdCustRiskListByEcifNum(String ecifNum);
	
	public RetMsg<Object> edCustSplitSave(Map<String, Object> map);
	
	
	/**
	 * 查询客户的总额度及总可用额度
	 * @param map
	 * @return 
	 */
	Page<List<Map<String,Object>>> getEdCustTotalAndAvlPage(Map<String, Object> map);
	
	/***
	 * 根据客户编号获取分页客户额度信息
	 * @param map
	 * @return
	 */
	public Page<TdEdCust>  getPageEdCustByEcifNum(Map<String, Object> map);
	
	
	/***
	 * 查询总授信额度最大的客户额度信息 
	 */
	Map<String, Object> getMaxLoanAmt(Map<String, Object> map);
	
	/**
	 * 保存权重信息
	 * @param map
	 * @return
	 */
	public RetMsg<Object> productWeightSave(Map<String, Object> map);
	
	/**
	 * 风险管理保存
	 * @param map
	 * @return
	 */
	public void riskManagerSave(Map<String, Object> map);
	
	/**
	 * 查询风险管理表
	 * @param map
	 * @return
	 */
	public Page<OpicsRisk> searchRiskManager(Map<String, Object> map);
}
