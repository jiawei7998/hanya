package com.singlee.capital.eu.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.TdEdDealLogService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdEdDealLogServiceImpl implements TdEdDealLogService{
	@Autowired
	TdEdDealLogMapper edDealLogMapper;
	@Override
	public Page<TdEdDealLog> getTdEdDealLogPage(
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		return edDealLogMapper.getTdEdDealLogs(map,ParameterUtil.getRowBounds(map));
	}
	@Override
	public Page<TdEdDealLog> getTdEdDealLogsByDealNo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return edDealLogMapper.getTdEdDealLogsByDealNo(map,ParameterUtil.getRowBounds(map));
	}

}
