package com.singlee.capital.eu.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.eu.mapper.TcEdProdMapper;
import com.singlee.capital.eu.mapper.TcEdRiskMapper;
import com.singlee.capital.eu.mapper.TcEdTemMapper;
import com.singlee.capital.eu.mapper.TdEdCustBatchLogMapper;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.mapper.TdEdCustCopyMapper;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.capital.eu.mapper.TdEdCustOpMapper;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.TcEdProd;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustService;
import com.singlee.capital.eu.service.TdEdCustOpService;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.eu.util.EdExceptionCode;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.mapper.TdCustEcifMapper;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EdCustServiceImpl implements EdCustService {

	@Autowired
	TdEdCustMapper edCustMapper;// 客户额度
	@Autowired
	TcEdProdMapper edProdMapper;// 产品与额度中类关系
	@Autowired
	TcEdTemMapper edTemMapper;// 期限
	@Autowired
	TdEdDealLogMapper edDealLogMapper;// 交易视角额度占用情况
	@Autowired
	BatchDao batchDao;// 批量
	@Autowired
	TdEdCustBatchLogMapper edCustBatchLogMapper;// 额度日志
	@Autowired
	TdEdCustOpService edCustOpService;// 额度操作
	@Autowired
	TdEdCustOpMapper edCustOpMapper;
	@Autowired
	TdEdCustBatchStatusMapper edCustBatchStatusMapper;// 批次记录
	@Autowired
	TdEdCustCopyMapper edCustCopyMapper;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;// 权重，组织机构

	@Autowired
	TdCustEcifMapper tdCustEcifMapper;

	@Autowired
	TcEdRiskMapper tcEdRiskMapper;

	@Autowired
	TdEdCustMapper tdEdCustMapper;

	@Override
	public boolean doEdOneInsertTdEdDealLog(Pair<EdInParams, List<TdEdDealLog>> pair, String batchId,
			boolean isCopyTry) {
		// TODO Auto-generated method stub
		// 查看 pair 端的额度left是否有余量；是否全部占用成功？
		if (pair.getLeft().isFlag()) {// 额度占用成功
			// 获得 pair端的right；进行额度情况更新
			List<TdEdDealLog> tdEdDealLogs = pair.getRight();

			for (int i = 0; i < tdEdDealLogs.size(); i++) {
				String creditId = tdEdDealLogs.get(i).getCreditId();// 获取授信额度产品编号
				String ecifNo = tdEdDealLogs.get(i).getEcifNo();// 获取客户编号
				double amt = tdEdDealLogs.get(i).getAmt();// 操作使用的金额

				Map<String, Object> edSumMap = new HashMap<String, Object>();
				edSumMap.put("custNo", ecifNo);
				TdEdCust tdEdCust = new TdEdCust();
				tdEdCust = tdEdCustMapper.getSumAmount(edSumMap);
				double avlAmt = tdEdCust.getAvlAmt() - amt;// 客户可用额度
				double loanAmt = tdEdCust.getLoanAmt();

				edSumMap.put("productType", creditId);
				TdEdCust tdEdCust2 = new TdEdCust();
				tdEdCust2 = tdEdCustMapper.getSumAmount(edSumMap);
				double avlAmt2 = tdEdCust2.getAvlAmt() - amt;// 客户+品种可用额度
				double loanAmt2 = tdEdCust2.getLoanAmt();

				// 获取客户底下产品额度
				tdEdDealLogs.get(i).setAvlAmtEcif(avlAmt);
				tdEdDealLogs.get(i).setLoanAmtEcif(loanAmt);
				tdEdDealLogs.get(i).setAvlAmtCredit(avlAmt2);
				tdEdDealLogs.get(i).setLoanAmtCredit(loanAmt2);

			}

			// 调用 交易额度日志存储
			batchDao.batch("com.singlee.capital.eu.mapper.TdEdDealLogMapper.insert", tdEdDealLogs);
			/**
			 * 记录批次操作前数据
			 */
			List<String> custCreditIds = new ArrayList<String>();

			if (tdEdDealLogs.size() > 0) {
				batchId = tdEdDealLogs.get(0).getBatchId();
			}
			for (TdEdDealLog tdEdDealLog : tdEdDealLogs) {
				custCreditIds.add(tdEdDealLog.getCustCreditId());
			}
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("batchId", batchId);
			paramsMap.put("custCreditIds", custCreditIds);
			paramsMap.put("batchType", DictConstants.BatchType.BATCH_BEFORE);
			edCustBatchLogMapper.insertTdEdCustBatchLogForBatch(paramsMap);// 批次前
			/**
			 * 循环查看 TdEdDealLog 中需要进行额度更新的操作 预占用 PRE_OCCUPANCY = "1"; 实际占用 ACT_OCCUPANCY =
			 * "2"; 预串用PRE_STRING = "3"; 实际串用ACT_STRING = "4"; 冻结FREEZE_IN = "5";
			 * 释放RELEASE_OUT = "6";
			 */
			for (TdEdDealLog tdEdDealLog : tdEdDealLogs) {
				if (isCopyTry) {
					edCustOpService.doEduCustOpByTypeCopy(tdEdDealLog, tdEdDealLog.getEdOpType());
				} else {
					edCustOpService.doEduCustOpByType(tdEdDealLog, tdEdDealLog.getEdOpType());
				}
			}
			paramsMap.put("batchType", DictConstants.BatchType.BATCH_AFTER);
			edCustBatchLogMapper.insertTdEdCustBatchLogForBatch(paramsMap);// 批次后
		} else {
			// 获得 pair端的right；进行额度情况更新
			List<TdEdDealLog> tdEdDealLogs = pair.getRight();
			if (tdEdDealLogs.size() > 0) {
				batchId = tdEdDealLogs.get(0).getBatchId();
			}
			// 调用 交易额度日志存储
			batchDao.batch("com.singlee.capital.eu.mapper.TdEdDealLogMapper.insert", tdEdDealLogs);
		}
		return pair.getLeft().isFlag();
	}

	@Override
	public Pair<EdInParams, List<TdEdDealLog>> doEdOneOccpCheck(Pair<EdInParams, List<TdEdDealLog>> pair,
			EdInParams edInParams, String batchId, boolean isCopyTry) {
		// TODO Auto-generated method stub
		// 第一步根据 paramsMap中的ecifNum+prdNo获得所有满足的客户总表记录
		List<TdEdDealLog> edDealLogs = new ArrayList<TdEdDealLog>();
		TdEdDealLog ownEdDealLog = null;// 应该占用的额度，除此之外都是串用
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("ecifNum", edInParams.getEcifNo());// ECIF客户号
		paramsMap.put("prdNo", edInParams.getPrdNo());// IFBM产品代码

		if ("443".equals(edInParams.getPrdNo())) {// 443,现劵买卖区分债券发行机构，进行不同的额度占用
			String ticketId = edInParams.getDealNo();// 获取交易流水号，然后查询债券债券发行机构的性质
			String ctype = tdCustEcifMapper.issuerTypeQry(ticketId);
			// 对于金融机构和不是金融机构的授信针对泰隆银行占用写死了

			Map<String, Object> edRiskMap = new HashMap<String, Object>();
			edRiskMap.put("prdNo", edInParams.getPrdNo());

			if ("B".equals(ctype) || "F".equals(ctype)) {// B银行，F金融机构
				edRiskMap.put("creditName", "金融企业");
				String productType = tcEdRiskMapper.getCreditCode(edRiskMap);
				// edInParams.setProdType(creditCode);
				paramsMap.put("productType", productType);// IFBM产品代码
			} else {
				edRiskMap.put("creditName", "非金融");
				String productType = tcEdRiskMapper.getCreditCode(edRiskMap);
				// edInParams.setProdType(creditCode);
				paramsMap.put("productType", productType);// IFBM产品代码
			}
		}

		// paramsMap.put("is_locked", true);//加锁
		/**
		 * SELECT C.CUST_CREDIT_ID,C.PRODUCT_TYPE,C.ECIF_NUM,
		 * C.TERM,C.TERM_TYPE,R.RISK_LEVEL,C.DUE_DATE AS ,C.LOAN_AMT,
		 * C.AVL_AMT,P.PROPERTY_L,D.CUR_DATE AS
		 * POST_DATE,(TO_DATE(C.DUE_DATE,'YYYY-MM-DD')-TO_DATE(D.CUR_DATE,'YYYY-MM-DD'))
		 * AS CREDIT_MDAYS FROM TD_ED_CUST C LEFT JOIN TC_ED_TERM T ON C.TERM=T.TERM_ID
		 * AND C.TERM_TYPE=T.TERM_TYPE LEFT JOIN TC_ED_RISK R ON
		 * C.PRODUCT_TYPE=R.CREDIT_ID LEFT JOIN TC_ED_PROD P ON C.PRODUCT_TYPE =
		 * P.CREDIT_ID ,TT_DAYEND_DATE D WHERE C.ECIF_NUM=#{ecifNum} AND
		 * P.PRODUCT_CODE=#{prdNo}
		 */

		List<TdEdCust> eduCusts = isCopyTry ? edCustMapper.selectTdEdCustsByEcifNumAndPrdNoCopy(paramsMap) : // 搜索TD_ED_CUST_COPY
				edCustMapper.selectTdEdCustsByEcifNumAndPrdNo(paramsMap);// 搜索TD_ED_CUST
		for (int i = 0; i < eduCusts.size(); i++)// 循环赋值 edMdate edMdays;postDate,creditMdate,creditMDays
		{
			// level 100-property_l
			eduCusts.get(i).setLevel(100 - Integer.parseInt(eduCusts.get(i).getPropertyL()));
			// 赋值 对年对月对日的额度确切期限截止日 edMdate
			if (DictConstants.TermType.Day.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.DAY,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Year.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.YEAR,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Month.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.MONTH,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Quarter.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.QUARTER,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else {
				throw new RException(EdExceptionCode.NotAccessTermTypeError);
			}
			// edMdate-mDate 的天数
			try {
				eduCusts.get(i).setEdMdays(daysBetween(eduCusts.get(i).getEdMdate(), edInParams.getmDate()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				throw new RException(EdExceptionCode.CalTermBetweenError);
			}
		}
		// 开始排序
		// riskLevel 3 2 1-风险最高
		// edMdays 100 0 -10 -期限最高
		// propertyL 3 2 1-优先级最低 level
		CollectionUtils.sort(eduCusts, true, "riskLevel", "edMdays", "level");

		// 排序完成开始额度占用
		/**
		 * 先判定是否存在产品对应的额度中类
		 */
		List<TcEdProd> tcEdProds = edProdMapper.selectTcEdProdsByPrdNo(edInParams.getPrdNo());
		if (tcEdProds.size() < 0) {
			edInParams.setFlag(false);
			edInParams.setResAmt(edInParams.getAmt());
			pair.setLeft(edInParams);
			pair.setRight(edDealLogs);
			return pair;
		}
		TdEdDealLog tdEdDealLog = null;
		boolean accessFlag = false;
		double orgAmt = edInParams.getAmt();// 需要占用的额度
		double orgAmtAll = edInParams.getAmtAll();// 需要不占用权重的额度
		/**
		 * 创建交易额度占用操作批次 用于记录一次操作
		 */

		for (TdEdCust cust : eduCusts) {
			tdEdDealLog = new TdEdDealLog();
			BeanUtil.copyNotEmptyProperties(tdEdDealLog, edInParams);// 拷贝输入头信息
			tdEdDealLog = copyProperty(tdEdDealLog, cust);// 拷贝额度基础信息
			// 开始逻辑处理
			tdEdDealLog.setBatchId(batchId);// 批次
			tdEdDealLog.setSeq(edDealLogs.size() + 1);// 序号

			accessFlag = false;
			for (TcEdProd edProd : tcEdProds) {
				if (StringUtils.equalsIgnoreCase(edProd.getCreditId(), tdEdDealLog.getCreditId())) {
					accessFlag = true;
					break;
				}
			}

			// 是否为新做交易
			String isNewDeal = tdEdDealLog.getIsNewDeal();
			if (isNewDeal != null && "1".equals(isNewDeal)) {

				/**
				 * 0、判断是否在产品 对应中类额度上配置
				 */
				if (!accessFlag) {// 额度中类不在产品配置中
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_NOM_PASS);
					tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
					tdEdDealLog.setEdOpAmt(0.00);// 不占额度
					edDealLogs.add(tdEdDealLog);
					continue;
				}
				/**
				 * 1、判断额度是否到期 是：则记录并进行吓一条处理
				 */
				// 20180402 此处修正为原始业务起息日与客户额度有效期进行比对，如果交易起息日小于客户额度有效期，那么仍旧允许被占用
				// tdEdDealLog.getCreditMdays()<0
				// 新增 5月版本 额度 修复额度释放时，若额度有效期已过，重新占用额度出错
				// tdEdDealLog.getCreditMdate() 为额度表的dueDate
				// tdEdDealLog.getvDate()交易起息日（为空则默认直通，不判定
				try {
					if (StringUtils.trimToNull(tdEdDealLog.getvDate()) == null
							|| compareDateDate(tdEdDealLog.getCreditMdate(), tdEdDealLog.getvDate(), "yyyy-MM-dd")) {
						/**
						 * 额度操作类型 edOpType; 操作时点 edOpTime; 额度操作金额 edOpAmt;
						 */
						tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_EXP_PASS);
						tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
						tdEdDealLog.setEdOpAmt(0.00);// 不占额度
						edDealLogs.add(tdEdDealLog);
						continue;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					JY.raise(e.getMessage() + "PlaningTools.compareDate函数异常，请检查" + tdEdDealLog.getEcifNo()
							+ "的额度到期日,不能为空或非日期!");
				}
				/**
				 * 2、开始额度中类是否可以占用所属期限 >0不能占用期限小的额度
				 */
				// 泰隆银行对额度的到期日和交易的到期日期不进行控制
				/*
				 * if(tdEdDealLog.getEdMdays()>0)//对年对月对日的处理 {
				 * tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_SMALL_PASS);
				 * tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
				 * tdEdDealLog.setEdOpAmt(0.00);//不占额度 edDealLogs.add(tdEdDealLog); continue; }
				 */
				/**
				 * 3、判断是否有可用额度 无：进行吓一条记录判定
				 */
				if (tdEdDealLog.getEdAvlamt() <= 0) {
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_ZERO_PASS);
					tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
					tdEdDealLog.setEdOpAmt(0.00);// 不占额度
					edDealLogs.add(tdEdDealLog);
					if (null == ownEdDealLog) {
						ownEdDealLog = new TdEdDealLog();
						BeanUtil.copyNotEmptyProperties(ownEdDealLog, tdEdDealLog);
						ownEdDealLog.setEdOpAmt(0);
					}
					continue;
				}

				/**
				 * 4、开始占用 先要判定是否有所属的额度期限，如果所属的额度期限没有，那么就需要串用其他额度期限
				 * 
				 */
				if (null != ownEdDealLog) {
					// 实际被串用
					if (null == StringUtils.trimToNull(tdEdDealLog.getDealType()) || DictConstants.DealType.Verify
							.equalsIgnoreCase(StringUtils.trimToNull(tdEdDealLog.getDealType()))) {
						tdEdDealLog.setEdOpType(DictConstants.EdOpType.ACT_STRING);
					} else {// 预被串用
						tdEdDealLog.setEdOpType(DictConstants.EdOpType.PRE_STRING);
					}
				} else {
					ownEdDealLog = new TdEdDealLog();
					BeanUtil.copyNotEmptyProperties(ownEdDealLog, tdEdDealLog);
					ownEdDealLog.setEdOpAmt(0);
					if (null == StringUtils.trimToNull(tdEdDealLog.getDealType()) || DictConstants.DealType.Verify
							.equalsIgnoreCase(StringUtils.trimToNull(tdEdDealLog.getDealType()))) {
						tdEdDealLog.setEdOpType(DictConstants.EdOpType.ACT_OCCUPANCY);
					} // 实际占用
					else {
						tdEdDealLog.setEdOpType(DictConstants.EdOpType.PRE_OCCUPANCY);
					} // 预占用
				}

				tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());

				if (PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt()) > 0) {// 需要占用的额度-满足条件的可用额度>0说明还需要进入下一个可用额度
					tdEdDealLog.setEdOpAmt(tdEdDealLog.getEdAvlamt());// 占额度
					orgAmt = PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt());// 剩余赋值
				}
				// 对占用基础授信额度和占用额度都进行比较
				/*
				 * if(PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt())>0 ||
				 * PlaningTools.sub(orgAmtAll, tdEdDealLog.getEdAvlamtAll())>0){
				 * //需要占用的基础授信额度-满足条件的可用基础授信额度>0说明还需要进入下一个可用额度
				 * //需要占用的额度-满足条件的可用额度>0说明还需要进入下一个可用额度
				 * tdEdDealLog.setEdOpAmt(tdEdDealLog.getEdAvlamt());//占额度 orgAmt =
				 * PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt());//剩余赋值 }
				 */
				else {
					tdEdDealLog.setEdOpAmt(orgAmt);// 占额度
					tdEdDealLog.setEdOpAmtAll(orgAmtAll);// 不占权重额度
					orgAmt = 0;
					if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.ACT_STRING)) {
						ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_ACT_STRING);// 实际串用别人的
						ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
						ownEdDealLog.setEdOpAmtAll(
								PlaningTools.add(ownEdDealLog.getEdOpAmtAll(), tdEdDealLog.getEdOpAmtAll()));
					} else if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.PRE_STRING)) {
						ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_PRE_STRING);// 预串用别人的
						ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
						ownEdDealLog.setEdOpAmtAll(
								PlaningTools.add(ownEdDealLog.getEdOpAmtAll(), tdEdDealLog.getEdOpAmtAll()));
					}
					edDealLogs.add(tdEdDealLog);
					break;
				}
				if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.ACT_STRING)) {
					ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_ACT_STRING);// 实际串用别人的
					ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
					ownEdDealLog
							.setEdOpAmtAll(PlaningTools.add(ownEdDealLog.getEdOpAmtAll(), tdEdDealLog.getEdOpAmtAll()));
				} else if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.PRE_STRING)) {
					ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_PRE_STRING);// 预串用别人的
					ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
					ownEdDealLog
							.setEdOpAmtAll(PlaningTools.add(ownEdDealLog.getEdOpAmtAll(), tdEdDealLog.getEdOpAmtAll()));
				}

			} else {// 已经发生过的交易不进行额度的校验
				tdEdDealLog.setEdOpType(DictConstants.EdOpType.PRE_OCCUPANCY);// 预占用
				tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
				tdEdDealLog.setEdOpAmt(orgAmt);// 占额度
				tdEdDealLog.setEdOpAmtAll(orgAmtAll);// 不占权重额度
				orgAmt = 0;
			}

			edDealLogs.add(tdEdDealLog);
		}
		if (orgAmt > 0) {// 还有剩余额度无法占用
			edInParams.setFlag(false);
			edInParams.setResAmt(orgAmt);
		} else {
			edInParams.setFlag(true);
		}
		pair.setLeft(edInParams);
		if (null != ownEdDealLog && ownEdDealLog.getEdOpAmt() > 0) {
			ownEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
			edDealLogs.add(ownEdDealLog);
		}
		pair.setRight(edDealLogs);
		return pair;
	}

	public TdEdDealLog copyProperty(TdEdDealLog tdEdDealLog, TdEdCust tdEdCust) {
		tdEdDealLog.setCustCreditId(tdEdCust.getCustCreditId());// 唯一编号
		tdEdDealLog.setCreditId(tdEdCust.getProductType());// 额度中类
		tdEdDealLog.setTermId(tdEdCust.getTerm());// 额度期限
		tdEdDealLog.setTermType(tdEdCust.getTermType());// 额度类型
		tdEdDealLog.setRiskLevel(tdEdCust.getRiskLevel());// 风险级别
		tdEdDealLog.setPropertyL(tdEdCust.getPropertyL());// 占用顺序
		tdEdDealLog.setEdMdate(tdEdCust.getEdMdate());// 额度可用期限截止日
		tdEdDealLog.setEdMdays(tdEdCust.getEdMdays());// 额度可占用天数 负数可用 正数排除
		tdEdDealLog.setPostDate(tdEdCust.getPostDate());// 账务日期
		tdEdDealLog.setCreditMdate(tdEdCust.getDueDate());// 额度有效期
		tdEdDealLog.setCreditMdays(tdEdCust.getCreditMdays());// 额度有效期天数
		tdEdDealLog.setEdAvlamt(tdEdCust.getAvlAmt());
		tdEdDealLog.setEdAvlamtAll(tdEdCust.getAvlAmtAll());
		return tdEdDealLog;
	}

	public static int daysBetween(String smdate, String bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(smdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(bdate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	public static String addOrSubMonth_Day(String datetime, Frequency frequency, int count) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(datetime);
		} catch (ParseException e) {
			// e.printStackTrace();
			throw new RException(e);
		}
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(frequency.getCalendarType(), frequency.getPace() * count);
		date = cl.getTime();
		return sdf.format(date);
	}

	public TdEdCustBatchStatus copyPropertyForEdCustBatchStatus(TdEdCustBatchStatus edCustBatchStatus,
			EdInParams edInParams) {
		edCustBatchStatus.setAmt(edInParams.getAmt());
		edCustBatchStatus.setAmtAll(edInParams.getAmtAll());
		edCustBatchStatus.setDealNo(edInParams.getDealNo());
		edCustBatchStatus.setBatchTime(DateUtil.getCurrentDateTimeAsString());
		edCustBatchStatus.setDealType(edInParams.getDealType());
		edCustBatchStatus.setEcifNo(edInParams.getEcifNo());
		edCustBatchStatus.setmDate(edInParams.getmDate());
		edCustBatchStatus.setPartyId(edInParams.getPartyId());
		edCustBatchStatus.setPrdNo(edInParams.getPrdNo());
		edCustBatchStatus.setvDate(edInParams.getvDate());
		edCustBatchStatus.setWeight(edInParams.getWeight());
		edCustBatchStatus.setInstName(edInParams.getInstFullName());
		edCustBatchStatus.setInstId(edInParams.getInstId());
		return edCustBatchStatus;
	}

	@Override
	public Pair<Boolean, List<TdEdDealLog>> doEdOccp(List<EdInParams> edInParams, String batchEvent, String dealNo) {
		// TODO Auto-generated method stub
		String batchTId = edDealLogMapper.getTdEdDealLogBatchId();// 总批次
		TdEdCustBatchStatus tdEdCustBatchStatus = null;
		boolean flag = true;// 标记
		Pair<EdInParams, List<TdEdDealLog>> pair = null; // 额度占用信息
		List<String> batchIdList = new ArrayList<String>();// 返回子批次信息
		List<TdEdDealLog> tdEdDealLogs = new ArrayList<TdEdDealLog>();
		List<Pair<EdInParams, List<TdEdDealLog>>> paList = new ArrayList<Pair<EdInParams, List<TdEdDealLog>>>();
		// 拷贝
		edCustCopyMapper.deleteEdCustCopy();// 删除总额度
		edCustCopyMapper.insertEdCustCopy();// 拷贝全量数据用于试算 表TD_ED_CUST 复制到TD_ED_CUST_COPY
		edCustMapper.updateAllEdCustForInitCopy(dealNo);

		// 额度占用信息列表
		for (EdInParams inParams : edInParams) {
			/**
			 * 分批次进行额度占用
			 */
			String batchCId = edDealLogMapper.getTdEdDealLogBatchId();
			batchIdList.add(batchCId);
			pair = new Pair<EdInParams, List<TdEdDealLog>>();
			doEdOneOccpCheck(pair, inParams, batchCId, true);
			paList.add(pair);
			if (!pair.getLeft().isFlag()) {// 有失败的额度检查
				flag = false;
			}
			doEdOneInsertTdEdDealLog(pair, batchCId, true);
			tdEdDealLogs.addAll(pair.getRight());

		}
		if (flag) {
			edCustMapper.updateAllEdCustForInit(dealNo);
			// A:删除额度使用细节
			edCustOpMapper.deleteEdCustOpByDealNo(dealNo);
			// B:删除关联的客户 status表数据，重新需要分配占用情况
			edCustBatchStatusMapper.deleteEdCustBatchLogByDealNo(dealNo);
			// C:

			for (Pair<EdInParams, List<TdEdDealLog>> tempPair : paList) {
				String batchId = tempPair.getRight().get(0).getBatchId();
				edDealLogMapper.deleteTdEdDealLogByBatchId(batchId);
				edCustBatchLogMapper.deleteTdEdCustBatchLogForBatchId(batchId);
				doEdOneInsertTdEdDealLog(tempPair, batchId, false);
				/**
				 * 持久化批次状态信息
				 */
				tdEdCustBatchStatus = new TdEdCustBatchStatus();
				copyPropertyForEdCustBatchStatus(tdEdCustBatchStatus, tempPair.getLeft());
				tdEdCustBatchStatus.setBatchCId(batchId);
				tdEdCustBatchStatus.setBatchTId(batchTId);
				tdEdCustBatchStatus.setBatchStatus(pair.getLeft().isFlag() ? DictConstants.BatchStatus.BATCH_SUC
						: DictConstants.BatchStatus.BATCH_FAL);
				tdEdCustBatchStatus.setBatchRefNo(tempPair.getLeft().getDealNo());
				tdEdCustBatchStatus.setBatchEvent(batchEvent);

				String dealNoNew = tdEdCustBatchStatus.getDealNo();
				List<EdInParams> creditList = ifsCfetsfxLendMapper.searchSingleInstInfo(dealNoNew);

				for (int i = 0; i < creditList.size(); i++) {
					tdEdCustBatchStatus.setWeight(creditList.get(i).getWeight());
					tdEdCustBatchStatus.setInstId(creditList.get(i).getInstId());
					tdEdCustBatchStatus.setInstName(creditList.get(i).getInstFullName());
				}
				tdEdCustBatchStatus.setBatchRefNo(tempPair.getLeft().getPrdNo());
				tdEdCustBatchStatus.setOperator(SlSessionHelper.getUserId());
				tdEdCustBatchStatus.setDealNo(dealNoNew);
				edCustBatchStatusMapper.DealNoAndWeightdelete(dealNoNew);
				edCustBatchStatusMapper.DealNoAndWeightinsert(tdEdCustBatchStatus);

				edCustBatchStatusMapper.insert(tdEdCustBatchStatus);
			}
		}
		edCustCopyMapper.deleteEdCustCopy();
		/**
		 * 返回占用信息列表
		 */
		Pair<Boolean, List<TdEdDealLog>> resultPair = new Pair<Boolean, List<TdEdDealLog>>();
		resultPair.setLeft(flag);
		resultPair.setRight(tdEdDealLogs);
		return resultPair;
	}

	@Override
	public boolean releaseEdByDealNo(String dealNo) {
		// TODO Auto-generated method stub
		/**
		 * 根据交易释放额度原理 1、找到交易所占用的客户额度情况 2、初始化该客户额度 3、重新占用（剔除dealNo)
		 */

		// 首先根据dealNo查询所有关联占用成功的额度状态信息
		List<EdInParams> edInParams = edCustBatchStatusMapper.getTdEdInParamsByDealNo(dealNo);
		// 重新占用
		Pair<Boolean, List<TdEdDealLog>> resultPair = doEdOccp(edInParams, "释放额度", dealNo);
		return resultPair.getLeft();
	}

	@Override
	public Pair<Boolean, List<TdEdDealLog>> releaseEdByCustNo(List<String> custNos, List<EdInParams> enEdInParams,
			List<String> dealNos) {
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("custNos", custNos);
		paramsMap.put("dealNos", dealNos);

		// 查询:表额度状态表TD_ED_CUST_BATCH_STATUS
		List<EdInParams> edInParams = edCustBatchStatusMapper.getTdEdInParamsByCustNo(paramsMap);
		if (null != enEdInParams && enEdInParams.size() > 0) {
			edInParams.addAll(enEdInParams);
		}
		CollectionUtils.sort(edInParams, false, "vDate");
		// 重新占用
		Pair<Boolean, List<TdEdDealLog>> resultPair = doEdOccp(edInParams, "释放额度", custNos, dealNos);
		return resultPair;
	}

	@Override
	public Pair<Boolean, List<TdEdDealLog>> doEdOccp(List<EdInParams> edInParams, String batchEvent,
			List<String> custNos, List<String> dealNos) {
		// TODO Auto-generated method stub
		String batchTId = edDealLogMapper.getTdEdDealLogBatchId();// 总批次
		TdEdCustBatchStatus tdEdCustBatchStatus = null;
		boolean flag = true;// 标记
		Pair<EdInParams, List<TdEdDealLog>> pair = null; // 额度占用信息
		List<String> batchIdList = new ArrayList<String>();// 返回子批次信息
		List<TdEdDealLog> tdEdDealLogs = new ArrayList<TdEdDealLog>();
		List<Pair<EdInParams, List<TdEdDealLog>>> paList = new ArrayList<Pair<EdInParams, List<TdEdDealLog>>>();
		// 拷贝
		edCustCopyMapper.deleteEdCustCopy();// 删除总额度 TD_ED_CUST_COPY
		edCustCopyMapper.insertEdCustCopy();// 拷贝全量数据用于试算
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("custNos", custNos);
		paramsMap.put("dealNos", dealNos);
		edCustMapper.updateAllEdByCustNoInitCopy(paramsMap); // 跟新TD_ED_CUST_COPY
		// 额度占用信息列表
		for (EdInParams inParams : edInParams) {
			/**
			 * 针对活期交易，自动生成1天的期限 300 且 Mdate is null
			 */
			if ("907".equalsIgnoreCase(inParams.getPrdNo()) && null == StringUtils.trimToNull(inParams.getmDate())) {
				inParams.setmDate(PlaningTools.addOrSubMonth_Day(inParams.getvDate(), Frequency.DAY, 1));
			}
			/**
			 * 分批次进行额度占用
			 */
			String batchCId = edDealLogMapper.getTdEdDealLogBatchId();
			batchIdList.add(batchCId);
			pair = new Pair<EdInParams, List<TdEdDealLog>>();
			doEdOneOccpCheck(pair, inParams, batchCId, true);
			paList.add(pair);
			if (!pair.getLeft().isFlag())// 有失败的额度检查
			{
				System.err.println("\r\nDealNo:" + inParams.getDealNo() + "\r\nEcifNo:" + inParams.getEcifNo()
						+ "\r\nPrdNo:" + inParams.getPrdNo() + "\r\nAmt:" + inParams.getAmt() + "\r\nvDate:"
						+ inParams.getvDate() + "\r\nmDate:" + inParams.getmDate());
				flag = false;
			}
			doEdOneInsertTdEdDealLog(pair, batchCId, true);
			tdEdDealLogs.addAll(pair.getRight());

		}
		if (flag) {
			// 删除额度占用记录
			// if((null != custNos && custNos.size()>0) || (null != dealNos &&
			// dealNos.size()>0))
			// edCustBatchStatusMapper.deleteTdEdInParamsByCustNoAndDealNos(paramsMap);
			if ((null != custNos && custNos.size() > 0)) {
				edCustBatchStatusMapper.deleteTdEdInParamsByCustNos(paramsMap);
			}
			edCustMapper.updateAllEdByCustNoInit(paramsMap);
			edCustOpMapper.deleteEdCustOpByCustNo(paramsMap);
			edCustBatchStatusMapper.deleteEdCustBatchLogByCustNo(paramsMap);
			for (Pair<EdInParams, List<TdEdDealLog>> tempPair : paList) {
				if (tempPair.getRight().size() <= 0) {
					continue;
				}
				String batchId = tempPair.getRight().get(0).getBatchId();
				edDealLogMapper.deleteTdEdDealLogByBatchId(batchId);
				edCustBatchLogMapper.deleteTdEdCustBatchLogForBatchId(batchId);
				doEdOneInsertTdEdDealLog(tempPair, batchId, false);
				/**
				 * 持久化批次状态信息
				 */
				tdEdCustBatchStatus = new TdEdCustBatchStatus();
				copyPropertyForEdCustBatchStatus(tdEdCustBatchStatus, tempPair.getLeft());
				tdEdCustBatchStatus.setBatchCId(batchId);
				tdEdCustBatchStatus.setBatchTId(batchTId);
				tdEdCustBatchStatus.setBatchStatus(pair.getLeft().isFlag() ? DictConstants.BatchStatus.BATCH_SUC
						: DictConstants.BatchStatus.BATCH_FAL);
				tdEdCustBatchStatus.setBatchRefNo(tempPair.getLeft().getDealNo());
				tdEdCustBatchStatus.setBatchEvent(batchEvent);
				// 增加权重流水对应表
				tdEdCustBatchStatus.setInstId(SlSessionHelper.getInstitutionId());
				tdEdCustBatchStatus.setInstName(tempPair.getLeft().getInstFullName());
				tdEdCustBatchStatus.setOperator(SlSessionHelper.getUserId());
				tdEdCustBatchStatus.setWeight(tempPair.getLeft().getWeight());
				tdEdCustBatchStatus.setPrdNo(tempPair.getLeft().getPrdNo());
				tdEdCustBatchStatus.setDealNo(tempPair.getLeft().getDealNo());
				String dealNo = tempPair.getLeft().getDealNo();
				edCustBatchStatusMapper.DealNoAndWeightdelete(dealNo);
				edCustBatchStatusMapper.DealNoAndWeightinsert(tdEdCustBatchStatus);

				edCustBatchStatusMapper.insert(tdEdCustBatchStatus);
			}
		}
		edCustCopyMapper.deleteEdCustCopy();
		/**
		 * 返回占用信息列表
		 */
		Pair<Boolean, List<TdEdDealLog>> resultPair = new Pair<Boolean, List<TdEdDealLog>>();
		resultPair.setLeft(flag);
		resultPair.setRight(tdEdDealLogs);
		return resultPair;
	}

	/****************************************************************************************************************************************/

	@Override
	public List<Pair<EdInParams, List<TdEdDealLog>>> releaseEdByCustNoTransfor(List<String> custNos,
			List<EdInParams> enEdInParams, List<String> dealNos) {
		// TODO Auto-generated method stub
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("custNos", custNos);
		paramsMap.put("dealNos", dealNos);

		List<EdInParams> edInParams = edCustBatchStatusMapper.getTdEdInParamsByCustNo(paramsMap);
		if (null != enEdInParams && enEdInParams.size() > 0) {
			edInParams.addAll(enEdInParams);
		}
		CollectionUtils.sort(edInParams, false, "vDate");
		// 重新占用
		List<Pair<EdInParams, List<TdEdDealLog>>> resultPair = doEdOccpTransfor(edInParams, "释放额度", custNos, dealNos);
		return resultPair;
	}

	@Override
	public List<Pair<EdInParams, List<TdEdDealLog>>> doEdOccpTransfor(List<EdInParams> edInParams, String batchEvent,
			List<String> custNos, List<String> dealNos) {
		// TODO Auto-generated method stub
		String batchTId = edDealLogMapper.getTdEdDealLogBatchId();// 总批次
		TdEdCustBatchStatus tdEdCustBatchStatus = null;
		boolean flag = true;// 标记
		Pair<EdInParams, List<TdEdDealLog>> pair = null; // 额度占用信息
		List<String> batchIdList = new ArrayList<String>();// 返回子批次信息
		List<TdEdDealLog> tdEdDealLogs = new ArrayList<TdEdDealLog>();
		List<Pair<EdInParams, List<TdEdDealLog>>> paList = new ArrayList<Pair<EdInParams, List<TdEdDealLog>>>();
		// 拷贝
		edCustCopyMapper.deleteEdCustCopy();// 删除总额度
		edCustCopyMapper.insertEdCustCopy();// 拷贝全量数据用于试算
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("custNos", custNos);
		paramsMap.put("dealNos", dealNos);
		edCustMapper.updateAllEdByCustNoInitCopy(paramsMap);
		// 额度占用信息列表
		for (EdInParams inParams : edInParams) {
			/**
			 * 针对活期交易，自动生成1天的期限 300 且 Mdate is null
			 */
			if ("300".equalsIgnoreCase(inParams.getPrdNo()) && null == StringUtils.trimToNull(inParams.getmDate())) {
				inParams.setmDate(PlaningTools.addOrSubMonth_Day(inParams.getvDate(), Frequency.DAY, 1));
			}
			/**
			 * 分批次进行额度占用
			 */
			String batchCId = edDealLogMapper.getTdEdDealLogBatchId();
			batchIdList.add(batchCId);
			pair = new Pair<EdInParams, List<TdEdDealLog>>();
			doEdOneOccpCheckTransfor(pair, inParams, batchCId, true);
			paList.add(pair);
			if (!pair.getLeft().isFlag())// 有失败的额度检查
			{
				System.err.println("\r\nDealNo:" + inParams.getDealNo() + "\r\nEcifNo:" + inParams.getEcifNo()
						+ "\r\nPrdNo:" + inParams.getPrdNo() + "\r\nAmt:" + inParams.getAmt() + "\r\nvDate:"
						+ inParams.getvDate() + "\r\nmDate:" + inParams.getmDate());
				flag = false;
			}
			doEdOneInsertTdEdDealLog(pair, batchCId, true);
			tdEdDealLogs.addAll(pair.getRight());

		}
		if (flag) {
			// 删除额度占用记录
			// if((null != custNos && custNos.size()>0) || (null != dealNos &&
			// dealNos.size()>0))
			// edCustBatchStatusMapper.deleteTdEdInParamsByCustNoAndDealNos(paramsMap);
			if ((null != custNos && custNos.size() > 0)) {
				edCustBatchStatusMapper.deleteTdEdInParamsByCustNos(paramsMap);
			}
			edCustMapper.updateAllEdByCustNoInit(paramsMap);
			edCustOpMapper.deleteEdCustOpByCustNo(paramsMap);
			edCustBatchStatusMapper.deleteEdCustBatchLogByCustNo(paramsMap);
			for (Pair<EdInParams, List<TdEdDealLog>> tempPair : paList) {
				if (tempPair.getRight().size() <= 0) {
					continue;
				}
				String batchId = tempPair.getRight().get(0).getBatchId();
				edDealLogMapper.deleteTdEdDealLogByBatchId(batchId);
				edCustBatchLogMapper.deleteTdEdCustBatchLogForBatchId(batchId);
				doEdOneInsertTdEdDealLog(tempPair, batchId, false);
				/**
				 * 持久化批次状态信息
				 */
				tdEdCustBatchStatus = new TdEdCustBatchStatus();
				copyPropertyForEdCustBatchStatus(tdEdCustBatchStatus, tempPair.getLeft());
				tdEdCustBatchStatus.setBatchCId(batchId);
				tdEdCustBatchStatus.setBatchTId(batchTId);
				tdEdCustBatchStatus.setBatchStatus(pair.getLeft().isFlag() ? DictConstants.BatchStatus.BATCH_SUC
						: DictConstants.BatchStatus.BATCH_FAL);
				tdEdCustBatchStatus.setBatchRefNo(tempPair.getLeft().getDealNo());
				tdEdCustBatchStatus.setBatchEvent(batchEvent);
				tdEdCustBatchStatus.setInstId(SlSessionHelper.getInstitutionId());
				tdEdCustBatchStatus.setInstName(tempPair.getLeft().getInstFullName());
				tdEdCustBatchStatus.setOperator(SlSessionHelper.getUserId());
				tdEdCustBatchStatus.setWeight(tempPair.getLeft().getWeight());
				tdEdCustBatchStatus.setPrdNo(tempPair.getLeft().getPrdNo());
				tdEdCustBatchStatus.setDealNo(tempPair.getLeft().getDealNo());
				String dealNo = tempPair.getLeft().getDealNo();
				edCustBatchStatusMapper.DealNoAndWeightdelete(dealNo);
				edCustBatchStatusMapper.DealNoAndWeightinsert(tdEdCustBatchStatus);

				edCustBatchStatusMapper.insert(tdEdCustBatchStatus);
			}
		}
		edCustCopyMapper.deleteEdCustCopy();
		/**
		 * 返回占用信息列表
		 */
		Pair<Boolean, List<TdEdDealLog>> resultPair = new Pair<Boolean, List<TdEdDealLog>>();
		resultPair.setLeft(flag);
		resultPair.setRight(tdEdDealLogs);
		return paList;
	}

	@Override
	public Pair<EdInParams, List<TdEdDealLog>> doEdOneOccpCheckTransfor(Pair<EdInParams, List<TdEdDealLog>> pair,
			EdInParams edInParams, String batchId, boolean isCopyTry) {
		// TODO Auto-generated method stub
		// 第一步根据 paramsMap中的ecifNum+prdNo获得所有满足的客户总表记录
		List<TdEdDealLog> edDealLogs = new ArrayList<TdEdDealLog>();
		TdEdDealLog ownEdDealLog = null;// 应该占用的额度，除此之外都是串用
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("ecifNum", edInParams.getEcifNo());// ECIF客户号
		paramsMap.put("prdNo", edInParams.getPrdNo());// IFBM产品代码
		// paramsMap.put("is_locked", true);//加锁
		/**
		 * SELECT C.CUST_CREDIT_ID,C.PRODUCT_TYPE,C.ECIF_NUM,
		 * C.TERM,C.TERM_TYPE,R.RISK_LEVEL,C.DUE_DATE AS ,C.LOAN_AMT,
		 * C.AVL_AMT,P.PROPERTY_L,D.CUR_DATE AS
		 * POST_DATE,(TO_DATE(C.DUE_DATE,'YYYY-MM-DD')-TO_DATE(D.CUR_DATE,'YYYY-MM-DD'))
		 * AS CREDIT_MDAYS FROM TD_ED_CUST C LEFT JOIN TC_ED_TERM T ON C.TERM=T.TERM_ID
		 * AND C.TERM_TYPE=T.TERM_TYPE LEFT JOIN TC_ED_RISK R ON
		 * C.PRODUCT_TYPE=R.CREDIT_ID LEFT JOIN TC_ED_PROD P ON C.PRODUCT_TYPE =
		 * P.CREDIT_ID ,TT_DAYEND_DATE D WHERE C.ECIF_NUM=#{ecifNum} AND
		 * P.PRODUCT_CODE=#{prdNo}
		 */

		List<TdEdCust> eduCusts = isCopyTry ? edCustMapper.selectTdEdCustsByEcifNumAndPrdNoCopy(paramsMap)
				: edCustMapper.selectTdEdCustsByEcifNumAndPrdNo(paramsMap);
		for (int i = 0; i < eduCusts.size(); i++)// 循环赋值 edMdate edMdays;postDate,creditMdate,creditMDays
		{
			// level 100-property_l
			eduCusts.get(i).setLevel(100 - Integer.parseInt(eduCusts.get(i).getPropertyL()));
			// 赋值 对年对月对日的额度确切期限截止日 edMdate
			if (DictConstants.TermType.Day.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.DAY,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Year.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.YEAR,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Month.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.MONTH,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else if (DictConstants.TermType.Quarter.equalsIgnoreCase(eduCusts.get(i).getTermType())) {// 天
				eduCusts.get(i).setEdMdate(addOrSubMonth_Day(edInParams.getvDate(), Frequency.QUARTER,
						Integer.parseInt(eduCusts.get(i).getTerm())));
			} else {
				throw new RException(EdExceptionCode.NotAccessTermTypeError);
			}
			// edMdate-mDate 的天数
			try {
				eduCusts.get(i).setEdMdays(daysBetween(eduCusts.get(i).getEdMdate(), edInParams.getmDate()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				throw new RException(EdExceptionCode.CalTermBetweenError);
			}
		}
		// 开始排序
		// riskLevel 3 2 1-风险最高
		// edMdays 100 0 -10 -期限最高
		// propertyL 3 2 1-优先级最低 level
		CollectionUtils.sort(eduCusts, true, "riskLevel", "edMdays", "level");

		// 排序完成开始额度占用
		/**
		 * 先判定是否存在产品对应的额度中类
		 */
		List<TcEdProd> tcEdProds = edProdMapper.selectTcEdProdsByPrdNo(edInParams.getPrdNo());
		if (tcEdProds.size() < 0) {
			edInParams.setFlag(false);
			edInParams.setResAmt(edInParams.getAmt());
			pair.setLeft(edInParams);
			pair.setRight(edDealLogs);
			return pair;
		}
		TdEdDealLog tdEdDealLog = null;
		boolean accessFlag = false;
		double orgAmt = edInParams.getAmt();// 需要占用的额度
		/**
		 * 创建交易额度占用操作批次 用于记录一次操作
		 */
		for (TdEdCust cust : eduCusts) {
			tdEdDealLog = new TdEdDealLog();
			BeanUtil.copyNotEmptyProperties(tdEdDealLog, edInParams);// 拷贝输入头信息
			tdEdDealLog = copyProperty(tdEdDealLog, cust);// 拷贝额度基础信息
			// 开始逻辑处理
			tdEdDealLog.setBatchId(batchId);// 批次
			tdEdDealLog.setSeq(edDealLogs.size() + 1);// 序号

			accessFlag = false;
			for (TcEdProd edProd : tcEdProds) {
				if (StringUtils.equalsIgnoreCase(edProd.getCreditId(), tdEdDealLog.getCreditId())) {
					accessFlag = true;
					break;
				}
			}
			/**
			 * 0、判断是否在产品 对应中类额度上配置
			 */
			if (!accessFlag) {// 额度中类不在产品配置中
				tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_NOM_PASS);
				tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
				tdEdDealLog.setEdOpAmt(0.00);// 不占额度
				edDealLogs.add(tdEdDealLog);
				continue;
			}
			/**
			 * 1、判断额度是否到期 是：则记录并进行吓一条处理
			 */
			/*
			 * if(tdEdDealLog.getCreditMdays()<0){
			 *//**
				 * 额度操作类型 edOpType; 操作时点 edOpTime; 额度操作金额 edOpAmt;
				 *//*
					 * tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_EXP_PASS);
					 * tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
					 * tdEdDealLog.setEdOpAmt(0.00);//不占额度 edDealLogs.add(tdEdDealLog); continue; }
					 */
			/**
			 * 2、开始额度中类是否可以占用所属期限 >0不能占用期限小的额度
			 */
			// 泰隆银行对额度的到期日期和交易的到期日期不进行控制
			/*
			 * if(tdEdDealLog.getEdMdays()>0)//对年对月对日的处理 {
			 * tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_SMALL_PASS);
			 * tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
			 * tdEdDealLog.setEdOpAmt(0.00);//不占额度 edDealLogs.add(tdEdDealLog); continue; }
			 */
			/**
			 * 3、判断是否有可用额度 无：进行吓一条记录判定
			 */
			if (tdEdDealLog.getEdAvlamt() <= 0) {
				tdEdDealLog.setEdOpType(DictConstants.EdOpType.ED_ZERO_PASS);
				tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
				tdEdDealLog.setEdOpAmt(0.00);// 不占额度
				edDealLogs.add(tdEdDealLog);
				if (null == ownEdDealLog) {
					ownEdDealLog = new TdEdDealLog();
					BeanUtil.copyNotEmptyProperties(ownEdDealLog, tdEdDealLog);
					ownEdDealLog.setEdOpAmt(0);
				}
				continue;
			}
			/**
			 * 4、开始占用 先要判定是否有所属的额度期限，如果所属的额度期限没有，那么就需要串用其他额度期限
			 * 
			 */
			if (null != ownEdDealLog) {
				if (null == StringUtils.trimToNull(tdEdDealLog.getDealType()) || DictConstants.DealType.Verify
						.equalsIgnoreCase(StringUtils.trimToNull(tdEdDealLog.getDealType()))) {
					// 实际被串用
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.ACT_STRING);
				} else {
					// 预被串用
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.PRE_STRING);
				}
			} else {
				ownEdDealLog = new TdEdDealLog();
				BeanUtil.copyNotEmptyProperties(ownEdDealLog, tdEdDealLog);
				ownEdDealLog.setEdOpAmt(0);
				if (null == StringUtils.trimToNull(tdEdDealLog.getDealType()) || DictConstants.DealType.Verify
						.equalsIgnoreCase(StringUtils.trimToNull(tdEdDealLog.getDealType()))) {
					// 实际占用
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.ACT_OCCUPANCY);
				} else {
					// 预占用
					tdEdDealLog.setEdOpType(DictConstants.EdOpType.PRE_OCCUPANCY);
				}
			}

			tdEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
			if (PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt()) > 0) {// 需要占用的额度-满足条件的可用额度>0说明还需要进入下一个可用额度
				tdEdDealLog.setEdOpAmt(tdEdDealLog.getEdAvlamt());// 占额度
				orgAmt = PlaningTools.sub(orgAmt, tdEdDealLog.getEdAvlamt());// 剩余赋值
			} else {
				tdEdDealLog.setEdOpAmt(orgAmt);// 占额度
				orgAmt = 0;
				if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.ACT_STRING)) {
					ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_ACT_STRING);// 实际串用别人的
					ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
				} else if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.PRE_STRING)) {
					ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_PRE_STRING);// 预串用别人的
					ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
				}
				edDealLogs.add(tdEdDealLog);
				break;
			}
			if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.ACT_STRING)) {
				ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_ACT_STRING);// 实际串用别人的
				ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
			} else if (tdEdDealLog.getEdOpType().equalsIgnoreCase(DictConstants.EdOpType.PRE_STRING)) {
				ownEdDealLog.setEdOpType(DictConstants.EdOpType.OWN_PRE_STRING);// 预串用别人的
				ownEdDealLog.setEdOpAmt(PlaningTools.add(ownEdDealLog.getEdOpAmt(), tdEdDealLog.getEdOpAmt()));
			}
			edDealLogs.add(tdEdDealLog);
		}
		if (orgAmt > 0) {// 还有剩余额度无法占用
			edInParams.setFlag(false);
			edInParams.setResAmt(orgAmt);
		} else {
			edInParams.setFlag(true);
		}
		pair.setLeft(edInParams);
		if (null != ownEdDealLog && ownEdDealLog.getEdOpAmt() > 0) {
			ownEdDealLog.setEdOpTime(DateUtil.getCurrentDateTimeAsString());
			edDealLogs.add(ownEdDealLog);
		}
		pair.setRight(edDealLogs);
		return pair;
	}

	/**
	 * 比较两个日期之间的大小
	 * 
	 * @param d1
	 * @param d2
	 * @return 前者大于后者返回true 反之false
	 */
	public boolean compareDateDate(String date1, String date2, String pattern) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);

		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int result = c1.compareTo(c2);
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}
}
