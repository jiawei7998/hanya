package com.singlee.capital.eu.service;

import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.TdEdCustAsg;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.financial.bean.SlCommonBean;

/**
 * 额度对外统一接口
 * @author SINGLEE
 *
 */
public interface EdCustManangeService {
	/**
	 * 额度切分服务  IFBM接收CRMS切分额度
	 * @param quotaSyncopateRecs
	 * 只能针对一个客户的切分LIST
	 * @return
	 */
	public RetMsg<Object> asegService(List<QuotaSyncopateRec> quotaSyncopateRecs);
	/**
	 * 流程中的额度占用
	 * 采用释放需要操作的客户信息，然后进行VDATE的占用顺序
	 * @param quotaSyncopateRecs
	 * @return
	 */
	public Pair<Boolean, List<TdEdDealLog>> eduOccpFlowService(List<EdInParams> enEdInParams,List<String> dealNos);
	/**
	 * 流程中的额度释放
	 * @param enEdInParams
	 * @return
	 */
	public boolean eduReleaseFlowService(String dealNo);
	
	/**
	 * 額度占用全量
	 * @return
	 */
	public boolean transforEduForImportDatas();
	public  List<Pair<EdInParams,List<TdEdDealLog>>> eduOccpFlowServiceTransfor(List<EdInParams> enEdInParams,List<String> dealNos);
	public boolean importCreditCustToDataBase(List<TdEdCustAsg> edCustAsgs);
	/**
	 * 产品统一占用额度接口
	 */
	public Pair<Boolean, List<TdEdDealLog>> unitaryLimitCccup(Map<String, String> paramMap,List<EdInParams> creditList);
	
	/**
	 * 选择占用规则
	 */
	public String chooseOccupy(Map<String, String> paramMap);
	
	/**
	 * 外汇限额控制
	 */
	public String limitForeignExchange(Map<String, String> paramMap);
	
	/**
	 * 外汇债券存单控制
	 */
	public String limitBondExchange(Map<String, String> paramMap,String inFlag);
	
	/**
	 * 将预到期明细转换成到期明细
	 */
	public String changeOccupyType(String dealNo);
	
	/**
	 * 日终到期释放额度
	 * 传入参数：
	 */
	public String ReleaseDueDateDeal();
	
	/**
	 * 衍生品当日重新占用
	 * @return
	 */
	public String occupyAgainDeal();
	
	/**
	 * 获取权重
	 * @param vDate
	 * @param mDate
	 * @param prdNo
	 * @return
	 * @throws Exception
	 */
	public String getNowDateWeight(String vDate,String mDate,String prdNo) throws Exception;
	
	/**
	 * 获取偏离度
	 * prdNo,
	 */
	public String queryRhisIntrate(Map<String, String> map);
	
	/**
	 *现券买卖DV01，久期，止损 
	 */
	public SlCommonBean queryMarketData(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	/**
	 *外汇即期止损 
	 */
	public String queryFxdSum(Map<String, Object> map)throws RemoteConnectFailureException, Exception;
	
}
