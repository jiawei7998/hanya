package com.singlee.capital.eu.service;

import com.singlee.capital.eu.model.TdEdDealLog;

/**
 * 额度详细操作表
 * @author SINGLEE
 *
 */
public interface TdEdCustOpService {
	/**
	 * 通过额度操作类别更改 额度操作明细
	 * @param edCustOp
	 * @param type
	 */
	public void doEduCustOpByType(TdEdDealLog tdEdDealLog,String type);
	
	public void doEduCustOpByTypeCopy(TdEdDealLog tdEdDealLog,String type);
}
