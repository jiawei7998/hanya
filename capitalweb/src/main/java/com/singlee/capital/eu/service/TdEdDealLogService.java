package com.singlee.capital.eu.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TdEdDealLog;

/**
 * 额度动作批次查询
 * @author SINGLEE
 *
 */
public interface TdEdDealLogService {
	/**
	 * 查询获得额度操作记录 TD_ED_DEAL_LOG
	 * @param map
	 * @return
	 */
	public Page<TdEdDealLog> getTdEdDealLogPage(Map<String, Object> map);
	
	public Page<TdEdDealLog> getTdEdDealLogsByDealNo(Map<String, Object> map);
}
