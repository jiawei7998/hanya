package com.singlee.capital.eu.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.eu.service.TdEdCustService;
import com.singlee.capital.eu.util.EdExceptionCode;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.mapper.TdCustEcifMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdEdCustServiceImpl implements TdEdCustService {
	@Autowired
	private TdEdCustMapper edCustMapper;
	@Autowired
	private EdCustManangeService custManangeService;
	@Autowired
	private TdCustEcifMapper custEcifMapper;

	@Override
	public Page<TdEdCust> getTdEdCustsPage(Map<String, Object> map) {
		return edCustMapper.getTdEdCustPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TdEdCust> getTdEdCustDetailPage(Map<String, Object> map) {
		return edCustMapper.getTdEdCustDetailPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TdEdCust> getEdCustForHandlePage(Map<String, Object> map) {
		return edCustMapper.getEdCustForHandlePage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TdEdCust> getEdCustForQueryPage(Map<String, Object> map) {
		return edCustMapper.getEdCustForQueryPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TdEdCust> getEdCustForQueryDetailPage(Map<String, Object> map) {
		return edCustMapper.getEdCustForQueryDetailPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TdEdCust> getEdCustRiskListByEcifNum(String ecifNum) {
		return edCustMapper.getEdCustRiskListByEcifNum(ecifNum);
	}

	@Override
	public RetMsg<Object> edCustSplitSave(Map<String, Object> map) {
		LogManager.getLogger(LogManager.MODEL_INTERFACEX)
				.info("======手工切分额度开始--客户代码custNo:" + map.get("ecifNum").toString());
		List<QuotaSyncopateRec> list = FastJsonUtil.parseArrays(map.get("EdCust").toString(), QuotaSyncopateRec.class);
		for (int i = 0; i < list.size(); i++) {
			String dueDate = list.get(i).getDueDate();
			String termType = list.get(i).getTermType();
			String term = list.get(i).getTerm();
			String dueDateLast = getDueDateLast(dueDate, termType, term);
			list.get(i).setDueDateLast(dueDateLast);
		}
		RetMsg<Object> retMsg = custManangeService.asegService(list);
		String LoanAmt = "";
		for (QuotaSyncopateRec quotaSyncopateRec : list) {
			LoanAmt += "[" + quotaSyncopateRec.getProductType() + "," + quotaSyncopateRec.getLoanAmt() + "]";
		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("***中类额度--" + LoanAmt);
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======手工切分额度结束--list.size:" + list.size());
		return retMsg;
	}

	public String getDueDateLast(String dueDate, String termType, String term) {
		String dueDateLast = "";
		if (DictConstants.TermType.Day.equalsIgnoreCase(termType)) {// 天
			dueDateLast = (addOrSubMonth_Day(dueDate, Frequency.DAY, Integer.parseInt(term)));
		} else if (DictConstants.TermType.Year.equalsIgnoreCase(termType)) {// 年
			dueDateLast = (addOrSubMonth_Day(dueDate, Frequency.YEAR, Integer.parseInt(term)));
		} else if (DictConstants.TermType.Month.equalsIgnoreCase(termType)) {// 月
			dueDateLast = (addOrSubMonth_Day(dueDate, Frequency.MONTH, Integer.parseInt(term)));
		} else if (DictConstants.TermType.Quarter.equalsIgnoreCase(termType)) {// 季
			dueDateLast = (addOrSubMonth_Day(dueDate, Frequency.QUARTER, Integer.parseInt(term)));
		} else {
			throw new RException(EdExceptionCode.NotAccessTermTypeError);
		}
		return dueDateLast;
	}

	public static String addOrSubMonth_Day(String datetime, Frequency frequency, int count) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(datetime);
		} catch (ParseException e) {
			// e.printStackTrace();
			throw new RException(e);
		}
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(frequency.getCalendarType(), frequency.getPace() * count);
		date = cl.getTime();
		return sdf.format(date);
	}

	@Override
	public Page<List<Map<String, Object>>> getEdCustTotalAndAvlPage(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);

		return edCustMapper.getEdCustTotalAndAvlPair(map, rb);
	}

	/**
	 * 根据客户编号获取分页客户额度信息
	 * 
	 * @see com.singlee.capital.eu.service.TdEdCustService#getPageEdCustByEcifNum(java.util.Map)
	 */
	@Override
	public Page<TdEdCust> getPageEdCustByEcifNum(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);

		return edCustMapper.getPageEdCustByEcifNum(map, rb);
	}

	/**
	 * 查询总授信额度最大的客户额度信息
	 * 
	 * @see com.singlee.capital.eu.service.TdEdCustService#getMaxLoanAmt(java.util.Map)
	 */
	@Override
	public Map<String, Object> getMaxLoanAmt(Map<String, Object> map) {
		return edCustMapper.getMaxLoanAmt(map);
	}

	/**
	 * 录入产品权重信息
	 */
	@Override
	public RetMsg<Object> productWeightSave(Map<String, Object> map) {

		@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) map.get("ProductWeight");
		String productCode = ParameterUtil.getString(map, "productCode", "");
		String branchId = ParameterUtil.getString(map, "branchId", "");
		// 当前用户id
		// String operator = SlSessionHelper.getUserId();

		custEcifMapper.deleteProtectByProductCode(productCode);
		custEcifMapper.addProtectByProductWeight(list, branchId);
		return null;
	}

	/**
	 * 风险管理信息
	 */
	@Override
	public void riskManagerSave(Map<String, Object> map) {
		@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) map.get("OpicsRisk");
		custEcifMapper.riskManagerDelete(list);
		// String dueDate = DateUtil.getCurrentDateTimeAsString();
		// 当前用户id
		// String operator = SlSessionHelper.getUserId();
		custEcifMapper.riskManagerSave(list);
	}

	// 查询指定交易明细
	@Override
	public Page<OpicsRisk> searchRiskManager(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<OpicsRisk> result = custEcifMapper.searchRiskManager(map, rowBounds);
		return result;
	}
}
