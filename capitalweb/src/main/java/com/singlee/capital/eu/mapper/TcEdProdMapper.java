package com.singlee.capital.eu.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcRproductCredit;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
import com.singlee.capital.eu.model.TcEdProd;

import tk.mybatis.mapper.common.Mapper;

public interface TcEdProdMapper extends Mapper<TcEdProd>{
	/**
	 * 根据产品获得 产品项下的所有配置的额度中类
	 * @param prdNo
	 * @return
	 */
	public List<TcEdProd> selectTcEdProdsByPrdNo(String prdNo);
	
	
	public Page<TcRproductCreditPojo> pageRproductCreditList(Map<String, Object> map, RowBounds rb);
	
	void deleteByProductCode(String productCode);
	
	List<TcRproductCredit> findByProductCodeAndCreditIdList(Map<String, Object> map);
	
	/**
	 * 额度中类编号获取本系统产品
	 * @param prdNo
	 * @return
	 */
	public List<TcEdProd> selectTcEdProdsByCreditId(@Param("creditId")String creditId);
	
}
