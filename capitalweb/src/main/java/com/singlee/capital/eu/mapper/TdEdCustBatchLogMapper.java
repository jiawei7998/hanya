package com.singlee.capital.eu.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.eu.model.TdEdCustBatchLog;

public interface TdEdCustBatchLogMapper extends Mapper<TdEdCustBatchLog>{
	/**
	 * 插入批量数据
	 * @param paramsMap
	 */
	public void insertTdEdCustBatchLogForBatch(Map<String, Object> paramsMap);
	
	public void deleteTdEdCustBatchLogForBatchId(String batchId);
}
