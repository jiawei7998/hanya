package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * IFBM客户切分额度表
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_ED_CUST_ASG")
public class TdEdCustAsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ecifNum;
	
	private String sysId;
	/**
	 * CRMS客户名称
	 */
	private String customer;
	/**
	 * 额度品种
	 */
	private String productType;
	/**
	 * 币种
	 */
	private String currency;
	/**
	 * 授信额度
	 */
	private double loanAmt;
	
	/**
	 * 期限ID
	 */
	private String term;
	/**
	 * 期限类型
	 */
	private String termType;
	/**
	 * 到期日
	 */
	private String dueDate;
	/**
	 * 状态
	 */
	private String cntrStatus;
	/**
	 * 操作时间
	 */
	private String opTime;
	public String getEcifNum() {
		return ecifNum;
	}
	public void setEcifNum(String ecifNum) {
		this.ecifNum = ecifNum;
	}
	public String getSysId() {
		return sysId;
	}
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getLoanAmt() {
		return loanAmt;
	}
	public void setLoanAmt(double loanAmt) {
		this.loanAmt = loanAmt;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getCntrStatus() {
		return cntrStatus;
	}
	public void setCntrStatus(String cntrStatus) {
		this.cntrStatus = cntrStatus;
	}
	public String getOpTime() {
		return opTime;
	}
	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}
	
	
}
