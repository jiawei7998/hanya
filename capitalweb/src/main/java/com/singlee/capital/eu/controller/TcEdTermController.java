package com.singlee.capital.eu.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.eu.model.TcEdTerm;
import com.singlee.capital.eu.service.TcEdTermService;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/edTermController")
public class TcEdTermController extends CommonController {
	@Autowired
	TcEdTermService edTermService;
	
	@ResponseBody
	@RequestMapping(value = "/getTcEdTermsPage")
	public RetMsg<PageInfo<TcEdTerm>> getTcEdTermsPage(@RequestBody Map<String,Object> map) {
		Page<TcEdTerm> list = edTermService.getTcEdTermsPage(map);
		return RetMsgHelper.ok(list);
	}
}
