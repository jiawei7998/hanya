package com.singlee.capital.eu.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TcEdTerm;

/**
 * 期限额度的使用服务
 * @author SINGLEE
 *
 */
public interface TcEdTermService {
	/**
	 * 
	 * @param map
	 * @return
	 */
	public Page<TcEdTerm> getTcEdTermsPage(Map<String, Object> map);
	
}
