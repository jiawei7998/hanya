package com.singlee.capital.eu.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.eu.model.TdEdDealLogTransfor;

public interface TdEdDealLogTransforMapper extends Mapper<TdEdDealLogTransfor>{

	public void deleteAllTdEdDealLogTransfor();
}
