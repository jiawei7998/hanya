package com.singlee.capital.eu.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.mapper.TcEdRiskMapper;
import com.singlee.capital.eu.model.TcEdRisk;
import com.singlee.capital.eu.service.TcEdRiskService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlLoanBean;
import com.singlee.financial.bean.SxedfpcxResBodyArray;
import com.singlee.financial.esb.tlcb.ISxedfpcxServer;

@Controller
@RequestMapping(value="/edRiskController")
public class TcEdRiskController extends CommonController {
	
	@Autowired
	TcEdRiskService edRiskService;
	
	@Autowired
	ISxedfpcxServer isxedfpcxserver; // 对公客户新建
	
	@Autowired
	TcEdRiskMapper tcEdRiskMapper;
	
	@ResponseBody
	@RequestMapping(value = "/getTcEdRisksPage")
	public RetMsg<PageInfo<TcEdRisk>> getTcEdRisksPage(@RequestBody Map<String,Object> map) {
		Page<TcEdRisk> list = edRiskService.getTcEdRisksPage(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/addTcEdRisk")
	public RetMsg<Serializable> addTcEdRisk(@RequestBody Map<String,Object> map) throws RException {
		try {
			TcEdRisk tcEdRisk = new TcEdRisk();
			BeanUtil.populate(tcEdRisk, map);
			edRiskService.addEdRisk(tcEdRisk);
			return RetMsgHelper.ok();
		} catch (RException e) {
			return RetMsgHelper.simple(e.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateTcEdRisk")
	public RetMsg<Serializable> updateTcEdRisk(@RequestBody Map<String,Object> map) throws RException {
		try {
			TcEdRisk tcEdRisk = new TcEdRisk();
			BeanUtil.populate(tcEdRisk, map);
			edRiskService.updateEdRisk(tcEdRisk);
			return RetMsgHelper.ok();
		} catch (RException e) {
			return RetMsgHelper.simple(e.getMessage());
		}
	}
	
	/**
	 * 删除额度品种风险级序
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTcEdRisk")
	public RetMsg<Serializable> deleteTcEdRisk(@RequestBody Map<String, Object> map) {
		RetMsg<TcEdRisk> retMsg=edRiskService.deleteTcEdRisk(map);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
	}
	
	@ResponseBody
	@RequestMapping(value = "/getChooseEdRisks")
	public RetMsg<List<TcEdRisk>> getChooseEdRisks(@RequestBody Map<String,Object> map) {
		List<TcEdRisk> list = edRiskService.getChooseEdRisks(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTcEdLimitPage")
	public RetMsg<List<Map<String,Object>>> getTcEdLimitPage(@RequestBody Map<String,Object> map) {
		SlLoanBean slLoanbean = new SlLoanBean();
		String custNo = ParameterUtil.getString(map, "custNo", "");
		slLoanbean.setCoreClientNo(custNo);
		slLoanbean.setUsername(SlSessionHelper.getUserId());
		slLoanbean = isxedfpcxserver.LN03020015(slLoanbean);
		List<SxedfpcxResBodyArray> list = slLoanbean.getQUERY_RESULT_ARRAY();
		List<Map<String,Object>> listToMap = new ArrayList<Map<String, Object>>();
		if("000000".equals(slLoanbean.getRetCode())){
			if(null == list){
				JY.raise("查询无对应的额度数据");
			}
			for(int i=0;i<list.size();i++){
				Map<String,Object> mapAdd = new HashMap<String, Object>();
				String creditName = list.get(i).getBUSS_NAME();
				String creditId = list.get(i).getLINE_SEQ_NO();
				String oPN_LMT = list.get(i).getOPN_LMT();
				String prdct_no = list.get(i).getPRDCT_NO();
				String crdtLmt = list.get(i).getCRDT_LMT();//授信额度
				mapAdd.put("creditName", creditName);
				mapAdd.put("creditId", creditId);
				mapAdd.put("opnLmt", oPN_LMT);//基础授信额度
				mapAdd.put("prdctNo",prdct_no);
				mapAdd.put("crdtLmt",crdtLmt);//授信额度
				listToMap.add(mapAdd);
			}
		}else{
			//查询授信额度失败，返回失败信息
			JY.raise(slLoanbean.getRetMsg());
		}
		
		return RetMsgHelper.ok(listToMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "/synchData")
	public RetMsg<List<Map<String,Object>>> synchData(@RequestBody String custNo) throws Exception {
		SlLoanBean slLoanbean = new SlLoanBean();
		//String custNo = ParameterUtil.getString(map, "custNo", "");
		slLoanbean.setCoreClientNo(custNo);
		slLoanbean.setUsername(SlSessionHelper.getUserId());
		slLoanbean = isxedfpcxserver.LN03020015(slLoanbean);
		List<SxedfpcxResBodyArray> list = slLoanbean.getQUERY_RESULT_ARRAY();
		
		List<Map<String,Object>> listToMap = new ArrayList<Map<String, Object>>();
		
		if("000000".equals(slLoanbean.getRetCode())){
			if(null == list){
				JY.raise("查询无对应的额度数据");
			}
			for(int i=0;i<list.size();i++){
				/*String creditName = list.get(i).getBUSS_NAME();//业务名称
				String prdctNo = list.get(i).getPRDCT_NO();//业务编号
				String creditId = list.get(i).getLINE_SEQ_NO();//额度信息流水号
	*/			String crdtLmt = list.get(i).getCRDT_LMT();//授信额度
				String opnLmt = list.get(i).getOPN_LMT();//敞口额度
				// 增加可用的额度
				String prdNo = list.get(i).getPRDCT_NO();//产品编号
				String startDate = list.get(i).getSTART_DATE();//开始时间
				String endDate = list.get(i).getEND_DATE();//结束时间
				String dueDate = startDate.substring(0,4)+"-"+startDate.substring(4,6)+"-"+startDate.substring(6,8);
				String dueDateLast = endDate.substring(0,4)+"-"+endDate.substring(4,6)+"-"+endDate.substring(6,8);
				String days = calCuteDate(startDate,endDate);//相差几天
				String ccy = list.get(i).getCCY();//币种
				String currency = "CNY";
				if("156".equals(ccy)){//和泰隆银行对应
					currency = "CNY";
				}else if("840".equals(ccy)){
					currency = "USD";
				}else if("978".equals(ccy)){
					currency = "EUR";
				}else if("826".equals(ccy)){
					currency = "GBP";
				}else if("392".equals(ccy)){
					currency = "JPY";
				}else if("344".equals(ccy)){
					currency = "HKD";
				}
				TcEdRisk tcEdRisk = tcEdRiskMapper.getCRMSType(prdNo);
				if(tcEdRisk == null || "".equals(tcEdRisk)){
					continue;
					//JY.raise("额度中类与产品未对应！");
				}
				Map<String,Object> mapAdd = new HashMap<String, Object>();
				mapAdd.put("creditName", tcEdRisk.getCreditName());
				mapAdd.put("productType", tcEdRisk.getCreditId());
				mapAdd.put("loanAmt", crdtLmt);
				mapAdd.put("loanAmtAll", crdtLmt);
				mapAdd.put("avlAmt", opnLmt);
				mapAdd.put("avlAmtAll", opnLmt);
				mapAdd.put("term", days);
				mapAdd.put("termName", days);
				mapAdd.put("termType", "05");
				mapAdd.put("currency", currency);
				mapAdd.put("dueDate", dueDate);
				mapAdd.put("dueDateLast", dueDateLast);
				listToMap.add(mapAdd);
			}
		}else{
			//查询授信额度失败，返回失败信息
			JY.raise(slLoanbean.getRetMsg());
		}
		return RetMsgHelper.ok(listToMap);
	}
	
	public static String calCuteDate(String startDay,String endDay) throws Exception{
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		Long c = sf.parse(endDay).getTime()-sf.parse(startDay).getTime();
		String days = c/1000/60/60/24 + "";//天
		return days;
	}
}