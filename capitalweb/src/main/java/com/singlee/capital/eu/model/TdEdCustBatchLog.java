package com.singlee.capital.eu.model;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * IFBM 額度批次记录表
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_ED_CUST_BATCH_LOG")
public class TdEdCustBatchLog implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String batchId;//额度操作批次
	
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	/**
	 * 额度唯一主键
	 */
	private String custCreditId;
	/**
	 * 额度中类
	 */
	private String creditId;
	/**
	 * 额度期限
	 */
	private String termId;
	/**
	 * 期限类型
	 */
	private String termType;
	/**
	 * 风险级别
	 */
	private int riskLevel;
	
	/**
	 * 优先级
	 */
	private String propertyL;
	/**
	 * 根据额度期限，类型，对年对月对日 时刻到期日
	 */
	private String edMdate;
	/**
	 * ED_MDATE-M_DATE 天数
	 */
	private int edMdays;
	/**
	 * 当前系统时间
	 */
	private String postDate;
	/**
	 * 额度到期日
	 */
	private String creditMdate;
	/**
	 * CREDIT_MDATE-系统时间 天数
	 */
	private int creditMDays;
	/**
	 * 操作时点可用额度
	 */
	private double edAvlamt;
	/**
	 * 额度操作类型
	 */
	private String edOpType;
	/**
	 * 操作时点
	 */
	private String edOpTime;
	/**
	 * 额度操作金额
	 */
	private double edOpAmt;
	
	/**
	 * 调整额度
	 */
	private double adjAmt;
	/**
	 * 串用额度
	 */
	private double astrAmt;
	/**
	 * 预串用额度
	 */
	private double preAstrAmt;
	/**
	 * 被串用额度
	 */
	private double acstrAmt;
	/**
	 * 预被串用额度
	 */
	private double preAcstrAmt;
	/**
	 * 占用额度
	 */
	private double usedAmt;
	
	/**
	 * 预占用额度
	 */
	private double preUsedAmt;
	/**
	 * 冻结额度
	 */
	private double frozenAmt;
	
	/**
	 * 操作时间
	 */
	private String opTime;
	
	public double getPreAstrAmt() {
		return preAstrAmt;
	}
	public void setPreAstrAmt(double preAstrAmt) {
		this.preAstrAmt = preAstrAmt;
	}
	public double getPreAcstrAmt() {
		return preAcstrAmt;
	}
	public void setPreAcstrAmt(double preAcstrAmt) {
		this.preAcstrAmt = preAcstrAmt;
	}
	
	public String getOpTime() {
		return opTime;
	}
	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}
	public double getPreUsedAmt() {
		return preUsedAmt;
	}
	public void setPreUsedAmt(double preUsedAmt) {
		this.preUsedAmt = preUsedAmt;
	}
	public double getAdjAmt() {
		return adjAmt;
	}
	public void setAdjAmt(double adjAmt) {
		this.adjAmt = adjAmt;
	}
	public double getAstrAmt() {
		return astrAmt;
	}
	public void setAstrAmt(double astrAmt) {
		this.astrAmt = astrAmt;
	}
	public double getAcstrAmt() {
		return acstrAmt;
	}
	public void setAcstrAmt(double acstrAmt) {
		this.acstrAmt = acstrAmt;
	}
	public double getUsedAmt() {
		return usedAmt;
	}
	public void setUsedAmt(double usedAmt) {
		this.usedAmt = usedAmt;
	}
	public double getFrozenAmt() {
		return frozenAmt;
	}
	public void setFrozenAmt(double frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType;
	}
	public int getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}
	
	public String getPropertyL() {
		return propertyL;
	}
	public void setPropertyL(String propertyL) {
		this.propertyL = propertyL;
	}
	public String getEdMdate() {
		return edMdate;
	}
	public void setEdMdate(String edMdate) {
		this.edMdate = edMdate;
	}
	public int getEdMdays() {
		return edMdays;
	}
	public void setEdMdays(int edMdays) {
		this.edMdays = edMdays;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getCreditMdate() {
		return creditMdate;
	}
	public void setCreditMdate(String creditMdate) {
		this.creditMdate = creditMdate;
	}
	public int getCreditMDays() {
		return creditMDays;
	}
	public void setCreditMDays(int creditMDays) {
		this.creditMDays = creditMDays;
	}
	public double getEdAvlamt() {
		return edAvlamt;
	}
	public void setEdAvlamt(double edAvlamt) {
		this.edAvlamt = edAvlamt;
	}
	public String getEdOpType() {
		return edOpType;
	}
	public void setEdOpType(String edOpType) {
		this.edOpType = edOpType;
	}
	public String getEdOpTime() {
		return edOpTime;
	}
	public void setEdOpTime(String edOpTime) {
		this.edOpTime = edOpTime;
	}
	public double getEdOpAmt() {
		return edOpAmt;
	}
	public void setEdOpAmt(double edOpAmt) {
		this.edOpAmt = edOpAmt;
	}
	
}
