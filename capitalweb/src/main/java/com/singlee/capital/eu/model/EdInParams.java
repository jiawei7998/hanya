package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Transient;

/**
 * 额度带入参数
 * @author SINGLEE
 *
 */
public class EdInParams implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 业务流水号
	 */
	private String dealNo;
	/**业务类型  1-预申请 2-正式申请
	 * 
	 */
	private String dealType;
	/**
	 * 额度判别-业务起息日
	 */
	private String vDate;
	/**
	 * 额度判别-业务到期日
	 */
	private String mDate;
	/**
	 * 业务品种
	 */
	private String prdNo;
	/**
	 * 参与方类型
	 */
	private String partyId;
	/**
	 * 业务需占用额度金额
	 */
	private double amt;
	
	/**
	 * 客户编号
	 */
	private String ecifNo;
	
	/**
	 * 产品代码
	 */
	private String productCode;
	@Transient
	private boolean flag;//是否额度占用成功
	@Transient
	private double resAmt;//剩余额度金额
	@Transient
	private String ecifName;
	
	@Transient
	private String weight;//权重
	@Transient
	private String instId;//组织机构号
	@Transient
	private String instFullName;//组织机构名称
	@Transient
	private String custNo;//占用授信主体编号
	@Transient
	private String ccyCode;//币种
	@Transient
	private String direction;//交易方向
	@Transient
	private String cost;//成本中心
	@Transient
	private String dealSource;//交易来源
	@Transient
	private String product;//产品代码
	@Transient
	private String prodType;//产品代码
	@Transient
	private String ecifCust;//对手机构
	@Transient
	private String applyNo;//单笔审批单号
	@Transient
	private String invtype;//账户类型
	@Transient
	private String bondCode;//债券代码
	@Transient
	private String acctngType;//会计类型
	@Transient
	private String settccy;//清算货币
	@Transient
	private String bondProperties;//债券性质/债券类型  ---国债，央票
	
	@Transient
	private String matdt;//债券到期日期
	
	//不加权重金额
	@Transient
	private double amtAll;
	
	//投资组合
	@Transient
	private String port;
	
	//交易编号
	@Transient
	private String ticketId;
	
	//是否新发起交易
	@Transient
	private String isNewDeal;
	
	//opics编号
	@Transient
	private String opicsNo;
	
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getInstFullName() {
		return instFullName;
	}
	public void setInstFullName(String instFullName) {
		this.instFullName = instFullName;
	}
	public String getEcifName() {
		return ecifName;
	}
	public void setEcifName(String ecifName) {
		this.ecifName = ecifName;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public double getResAmt() {
		return resAmt;
	}
	public void setResAmt(double resAmt) {
		this.resAmt = resAmt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getEcifNo() {
		return ecifNo;
	}
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDealSource() {
		return dealSource;
	}
	public void setDealSource(String dealSource) {
		this.dealSource = dealSource;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getEcifCust() {
		return ecifCust;
	}
	public void setEcifCust(String ecifCust) {
		this.ecifCust = ecifCust;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getBondCode() {
		return bondCode;
	}
	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}
	public String getAcctngType() {
		return acctngType;
	}
	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}
	public String getSettccy() {
		return settccy;
	}
	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}
	public String getBondProperties() {
		return bondProperties;
	}
	public void setBondProperties(String bondProperties) {
		this.bondProperties = bondProperties;
	}
	public String getMatdt() {
		return matdt;
	}
	public void setMatdt(String matdt) {
		this.matdt = matdt;
	}
	public double getAmtAll() {
		return amtAll;
	}
	public void setAmtAll(double amtAll) {
		this.amtAll = amtAll;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getIsNewDeal() {
		return isNewDeal;
	}
	public void setIsNewDeal(String isNewDeal) {
		this.isNewDeal = isNewDeal;
	}
	public String getOpicsNo() {
		return opicsNo;
	}
	public void setOpicsNo(String opicsNo) {
		this.opicsNo = opicsNo;
	}

	
}
