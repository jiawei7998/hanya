package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * IFBM客户额度操作细项
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_ED_CUST_OP")
public class TdEdCustOp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String custCreditId;
	
	/**
	 * 串用额度
	 */
	private double astrAmt;
	/**
	 * 预串用额度
	 */
	private double preAstrAmt;
	/**
	 * 被串用额度
	 */
	private double acstrAmt;
	/**
	 * 预被串用额度
	 */
	private double preAcstrAmt;
	/**
	 * 占用额度
	 */
	private double usedAmt;
	/**
	 * 预占用额度
	 */
	private double preUsedAmt;
	
	/**
	 * 操作时间
	 */
	private String opTime;
	
	
	private double preUsedAmtAll;
	
	public double getPreAstrAmt() {
		return preAstrAmt;
	}
	public void setPreAstrAmt(double preAstrAmt) {
		this.preAstrAmt = preAstrAmt;
	}
	public double getPreAcstrAmt() {
		return preAcstrAmt;
	}
	public void setPreAcstrAmt(double preAcstrAmt) {
		this.preAcstrAmt = preAcstrAmt;
	}
	public double getPreUsedAmt() {
		return preUsedAmt;
	}
	public void setPreUsedAmt(double preUsedAmt) {
		this.preUsedAmt = preUsedAmt;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	
	public double getAstrAmt() {
		return astrAmt;
	}
	public void setAstrAmt(double astrAmt) {
		this.astrAmt = astrAmt;
	}
	public double getAcstrAmt() {
		return acstrAmt;
	}
	public void setAcstrAmt(double acstrAmt) {
		this.acstrAmt = acstrAmt;
	}
	public double getUsedAmt() {
		return usedAmt;
	}
	public void setUsedAmt(double usedAmt) {
		this.usedAmt = usedAmt;
	}
	
	public String getOpTime() {
		return opTime;
	}
	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}
	public double getPreUsedAmtAll() {
		return preUsedAmtAll;
	}
	public void setPreUsedAmtAll(double preUsedAmtAll) {
		this.preUsedAmtAll = preUsedAmtAll;
	}
	
	
}
