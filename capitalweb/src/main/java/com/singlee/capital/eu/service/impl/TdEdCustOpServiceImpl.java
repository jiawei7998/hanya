package com.singlee.capital.eu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.eu.mapper.TdEdCustCopyMapper;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.capital.eu.mapper.TdEdCustOpMapper;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.capital.eu.model.TdEdCustCopy;
import com.singlee.capital.eu.model.TdEdCustOp;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.TdEdCustOpService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdEdCustOpServiceImpl implements TdEdCustOpService {
	@Autowired
	TdEdCustOpMapper custOpMapper;//子表
	@Autowired
	TdEdCustMapper eduCustMapper;//头表
	@Autowired
	TdEdCustCopyMapper edCustCopyMapper;
	@Override
	public void doEduCustOpByType(TdEdDealLog tdEdDealLog, String type) {
		// TODO Auto-generated method stub
		/**
		 * 循环查看 TdEdDealLog 中需要进行额度更新的操作
		 * 预占用 PRE_OCCUPANCY = "1";
		 * 实际占用 ACT_OCCUPANCY = "2";
		 * 预串用PRE_STRING = "3";
		 * 实际串用ACT_STRING = "4";
		 */
		TdEdCustOp otherCustOp = custOpMapper.selectByPrimaryKey(tdEdDealLog.getCustCreditId());
		//更新头表
		TdEdCust tdEdCust = eduCustMapper.selectByPrimaryKey(tdEdDealLog.getCustCreditId());
		boolean isNew = false;
		if(null == otherCustOp)
		{
			otherCustOp = new TdEdCustOp();	
			isNew = true;
		}
		otherCustOp.setCustCreditId(tdEdDealLog.getCustCreditId());
		otherCustOp.setOpTime(DateUtil.getCurrentTimeAsString());
		switch (Integer.parseInt(type)) {
		case 1:
			otherCustOp.setPreUsedAmt(PlaningTools.add(otherCustOp.getPreUsedAmt(), tdEdDealLog.getEdOpAmt()));
			otherCustOp.setPreUsedAmtAll(PlaningTools.add(otherCustOp.getPreUsedAmtAll(),tdEdDealLog.getEdOpAmtAll()));
			tdEdCust.setAvlAmt(PlaningTools.sub(tdEdCust.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCust.setAvlAmtAll(PlaningTools.sub(tdEdCust.getAvlAmtAll(),tdEdDealLog.getEdOpAmtAll()));
			break;
		case 2:
			otherCustOp.setUsedAmt(PlaningTools.add(otherCustOp.getUsedAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCust.setAvlAmt(PlaningTools.sub(tdEdCust.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCust.setAvlAmtAll(PlaningTools.sub(tdEdCust.getAvlAmtAll(),tdEdDealLog.getEdOpAmtAll()));
			break;
		case 3://预登记被串用
			otherCustOp.setPreAcstrAmt(PlaningTools.add(otherCustOp.getPreAcstrAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCust.setAvlAmt(PlaningTools.sub(tdEdCust.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			break;	
		case 4://登记被串用
			otherCustOp.setAcstrAmt(PlaningTools.add(otherCustOp.getAcstrAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCust.setAvlAmt(PlaningTools.sub(tdEdCust.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			break;	
		/***********************************************************************************************************/
		case 5://预登记自己串用其他人
			otherCustOp.setPreAstrAmt(PlaningTools.add(otherCustOp.getPreAstrAmt(), tdEdDealLog.getEdOpAmt()));
			break;
		case 6://预登记自己被串用
			otherCustOp.setAstrAmt(PlaningTools.add(otherCustOp.getAstrAmt(), tdEdDealLog.getEdOpAmt()));
			break;
		default:
			break;
		}
		tdEdCust.setOpTime(DateUtil.getCurrentDateTimeAsString());
		eduCustMapper.updateByPrimaryKey(tdEdCust);
		if(isNew){
			otherCustOp.setOpTime(DateUtil.getCurrentDateTimeAsString());
			custOpMapper.insert(otherCustOp);
		}
		else {
			otherCustOp.setOpTime(DateUtil.getCurrentDateTimeAsString());
			custOpMapper.updateByPrimaryKey(otherCustOp);
		}
		
		
	}
	@Override
	public void doEduCustOpByTypeCopy(TdEdDealLog tdEdDealLog, String type) {
		// TODO Auto-generated method stub
		TdEdCustCopy tdEdCustCopy = edCustCopyMapper.selectByPrimaryKey(tdEdDealLog.getCustCreditId());
		switch (Integer.parseInt(type)) {
		case 1:
			tdEdCustCopy.setAvlAmt(PlaningTools.sub(tdEdCustCopy.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCustCopy.setAvlAmtAll(PlaningTools.sub(tdEdCustCopy.getAvlAmtAll(),tdEdDealLog.getEdOpAmtAll()));
			break;
		case 2:
			tdEdCustCopy.setAvlAmt(PlaningTools.sub(tdEdCustCopy.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			tdEdCustCopy.setAvlAmtAll(PlaningTools.sub(tdEdCustCopy.getAvlAmtAll(),tdEdDealLog.getEdOpAmtAll()));
			break;
		case 3://预登记被串用
			tdEdCustCopy.setAvlAmt(PlaningTools.sub(tdEdCustCopy.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			break;	
		case 4://登记被串用
			tdEdCustCopy.setAvlAmt(PlaningTools.sub(tdEdCustCopy.getAvlAmt(), tdEdDealLog.getEdOpAmt()));
			break;	
		default:
			break;
		}
		edCustCopyMapper.updateByPrimaryKey(tdEdCustCopy);
	}
}
