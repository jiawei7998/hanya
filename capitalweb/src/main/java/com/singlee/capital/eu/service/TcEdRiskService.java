package com.singlee.capital.eu.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.eu.model.TcEdRisk;
/**
 * 额度中类服务
 * @author SINGLEE
 *
 */
public interface TcEdRiskService {
	/**
	 * 获取所以的额度中类
	 * @param map
	 * @return
	 */
	public Page<TcEdRisk> getTcEdRisksPage(Map<String, Object> map);
	/**
	 * 增加额度中类
	 * @param tcEdRisk
	 */
	public void addEdRisk(TcEdRisk tcEdRisk);
	/**
	 * 更新额度中类
	 * @param tcCredit
	 */
	public void updateEdRisk(TcEdRisk tcEdRisk);
	
	public RetMsg<TcEdRisk> deleteTcEdRisk(Map<String, Object> map);
	
	
	public List<TcEdRisk> getChooseEdRisks(Map<String, Object> map);

	
}
