package com.singlee.capital.eu.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.financial.bean.TdedCust;

public interface TdEdCustMapper extends Mapper<TdEdCust> {
	/**
	 * 根据ECIF客户号+产品代码获得 所有额度总表信息
	 * 
	 * @param paramsMap
	 *            ecifNum+prdNo
	 * @return
	 */
	public List<TdEdCust> selectTdEdCustsByEcifNumAndPrdNo(Map<String, Object> paramsMap);

	/**
	 * 试算
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List<TdEdCust> selectTdEdCustsByEcifNumAndPrdNoCopy(Map<String, Object> paramsMap);

	/**
	 * 根据传入的DEALNO去更新总额度表的信息为初始化状态
	 * 
	 * @param dealNo
	 */
	public void updateAllEdCustForInit(String dealNo);

	public void updateAllEdCustForInitCopy(String dealNo);

	/**
	 * 根据客户代码初始化额度信息
	 */
	public void updateAllEdByCustNoInit(Map<String, Object> paramsMap);

	public void updateAllEdByCustNoInitCopy(Map<String, Object> paramsMap);

	/**
	 * 分页查询获得额度表的总记录
	 * 
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCust> getTdEdCustPage(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 查询客户项下的明细额度
	 * 
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCust> getTdEdCustDetailPage(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 根据客户号、额度类型、币种、期限
	 * 
	 * @param map
	 * @return
	 */
	public TdEdCust getTdEdCust(Map<String, Object> map);

	/**
	 * 弹出框 的新逻辑 手工额度筛选的
	 * 
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCust> getEdCustForHandlePage(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 全表加锁
	 */
	public List<TdEdCust> selectEdCustsForLocked();

	/**
	 * 额度查询
	 */
	public Page<TdEdCust> getEdCustForQueryPage(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 额度查询细项
	 * 
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TdEdCust> getEdCustForQueryDetailPage(Map<String, Object> map, RowBounds rowBounds);

	public void deleteAllEdCustOpForEcifNum(String ecifNum);

	public void deleteAllEdCustForEcifNum(String ecifNum);

	/**
	 * 根据客户查询出所有的额度信息
	 * 
	 * @param ecifNum
	 * @return
	 */
	public List<TdEdCust> getEdCustByEcifNum(String ecifNum);

	public void deleteAllEdCust();

	public List<TdEdCust> getEdCustRiskListByEcifNum(String ecifNum);

	Page<List<Map<String, Object>>> getEdCustTotalAndAvlPair(Map<String, Object> params, RowBounds rb);

	Page<TdEdCust> getPageEdCustByEcifNum(Map<String, Object> params, RowBounds rb);

	/***
	 * 查询总授信额度最大的客户额度信息
	 */
	Map<String, Object> getMaxLoanAmt(Map<String, Object> map);

	/**
	 * 取汇总台账
	 */
	List<TdedCust> getGatherEd();
	
	/**
	 * 通过ecifNo或授信产品额度查询额度
	 */
	public TdEdCust getSumAmount(Map<String, Object> map);
	
}