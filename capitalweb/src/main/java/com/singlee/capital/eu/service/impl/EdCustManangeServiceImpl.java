package com.singlee.capital.eu.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.participant.mapper.TcProductParticipantMapper;
import com.singlee.capital.base.trade.service.TdProductParticipantService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.eu.mapper.TdEdCustAsgMapper;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.capital.eu.mapper.TdEdCustTransforMapper;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.mapper.TdEdDealLogTransforMapper;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.capital.eu.model.ProductWeight;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.capital.eu.model.TdEdCustAsg;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.model.TdEdCustTransfor;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.model.TdEdDealLogTransfor;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.eu.service.EdCustService;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductCustCreditMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.service.ProductCustCreditService;
import com.singlee.capital.users.mapper.TdCustEcifMapper;
import com.singlee.financial.bean.SlCommonBean;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IEdServer;
import com.singlee.ifs.mapper.BondLimitTemplateMapper;
import com.singlee.ifs.mapper.IfsCfetsfxFwdMapper;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsCfetsfxOptionMapper;
import com.singlee.ifs.mapper.IfsCfetsfxSwapMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.mapper.IfsLimitTemplateMapper;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.model.IfsCfetsfxSwap;
import com.singlee.ifs.model.IfsCreditRiskreview;
import com.singlee.ifs.model.IfsTradeAccess;
import com.singlee.ifs.service.IfsLimitService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EdCustManangeServiceImpl implements EdCustManangeService {
	@Autowired
	TdEdCustMapper edCustMapper;// 额度信息表
	@Autowired
	DayendDateService dateService;
	@Autowired
	EdCustService edCustService;
	@Autowired
	TdProductCustCreditMapper productCustCreditMapper;
	@Autowired
	TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	BatchDao batchDao;
	@Autowired
	ProductCustCreditService productCustCreditService;
	@Autowired
	TcProductParticipantMapper tcProductParticipantMapper;
	@Autowired
	TdProductParticipantService participantService;
	@Autowired
	TdEdDealLogTransforMapper edDealLogTransforMapper;
	@Autowired
	TdEdCustTransforMapper edCustTransforMapper;
	@Autowired
	TdEdCustAsgMapper edCustAsgMapper;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Autowired
	IfsLimitService ifsLimitService;
	@Autowired
	IfsLimitTemplateMapper ifsLimitTemplateMapper;
	@Autowired
	private TdCustEcifMapper tdCustEcifMapper;
	@Autowired
	IEdServer iEdServer;
	@Autowired
	IAcupServer acupServer;
	@Autowired
	TdEdDealLogMapper tdEdDealLogMapper;
	@Autowired
	TdEdCustBatchStatusMapper tdEdCustBatchStatusMapper;

	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;

	@Autowired
	IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;

	@Autowired
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;

	@Autowired
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;

	@Autowired
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;

	@Autowired
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;

	@Autowired
	IBaseServer iBaseServer;

	@Override
	public RetMsg<Object> asegService(List<QuotaSyncopateRec> quotaSyncopateRecs) {
		// TODO Auto-generated method stub
		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度切分成功", "");
		/**
		 * 暴力输出；删除切分数据所包含ECIFNUM的客户所有额度信息TD_ED_CUST
		 * 
		 * 再进行额度占用
		 */
		// 1、循环quotaSyncopateRecs的客户数
		try {
			edCustMapper.selectEdCustsForLocked();// 锁表
			LogManager.getLogger(LogManager.MODEL_INTERFACEX)
					.info("***********************[asegService]****************************");
			List<TdEdCust> resultCusts = new ArrayList<TdEdCust>();// String postDate = dateService.getNextDayendDate();
			List<String> custNos = new ArrayList<String>();
			TdEdCust insertEdCust = null;
			Map<String, Object> paramMaps = new HashMap<String, Object>();
			for (QuotaSyncopateRec quotaSyncopateRec : quotaSyncopateRecs) {
				custNos.add(quotaSyncopateRec.getEcifNum());
				// 必须是IFBM 01的额度才接受
				if (!"01".equalsIgnoreCase(quotaSyncopateRec.getSysId())) {
					LogManager.getLogger(LogManager.MODEL_INTERFACEX)
							.info("[asegService]<>01 not ifbm:" + BeanUtils.describe(quotaSyncopateRec));
					continue;
				}
				/*
				 * //已到期的额度切分去掉 if(PlaningTools.daysBetween(postDate,
				 * quotaSyncopateRec.getDueDate().replace("/","-"))<=0){
				 * LogManager.getLogger(LogManager.MODEL_INTERFACEX).
				 * info("[asegService]already maturity:"+BeanUtils.describe(quotaSyncopateRec));
				 * continue; }
				 */

				/**
				 * SELECT * FROM TD_ED_CUST WHERE ECIF_NUM=#{ecifNum} AND
				 * PRODUCT_TYPE=#{productType} AND CURRENCY=#{currency} AND TERM=#{term} AND
				 * TERM_TYPE=#{termType}
				 */
				paramMaps.clear();
				paramMaps.put("ecifNum", StringUtils.trimToEmpty(quotaSyncopateRec.getEcifNum()));
				paramMaps.put("productType", StringUtils.trimToEmpty(quotaSyncopateRec.getProductType()));
				paramMaps.put("currency", StringUtils.trimToEmpty(quotaSyncopateRec.getCurrency()));
				paramMaps.put("term", StringUtils.trimToEmpty(quotaSyncopateRec.getTerm()));
				paramMaps.put("termType", StringUtils.trimToEmpty(quotaSyncopateRec.getTermType()));
				// 查询额度信息
				insertEdCust = edCustMapper.getTdEdCust(paramMaps);

				if (null == insertEdCust) {
					insertEdCust = new TdEdCust();
				}
				insertEdCust.setEcifNum(quotaSyncopateRec.getEcifNum());
				insertEdCust.setCustomer(quotaSyncopateRec.getCustomer());
				insertEdCust.setProductType(quotaSyncopateRec.getProductType());
				insertEdCust.setCurrency(quotaSyncopateRec.getCurrency());
				insertEdCust.setLoanAmt(new BigDecimal(quotaSyncopateRec.getLoanAmt()).doubleValue());
				insertEdCust.setTerm(quotaSyncopateRec.getTerm());
				insertEdCust.setTermType(quotaSyncopateRec.getTermType());
				insertEdCust.setDueDate(quotaSyncopateRec.getDueDate().replace("/", "-"));
				insertEdCust.setDueDateLast(quotaSyncopateRec.getDueDateLast().replace("/", "-"));
				insertEdCust.setCntrStatus(quotaSyncopateRec.getCntrStatus());
				insertEdCust.setOpTime(DateUtil.getCurrentDateAsString());
				insertEdCust.setAvlAmt(new BigDecimal(quotaSyncopateRec.getLoanAmt()).doubleValue());
				insertEdCust.setOperator(SlSessionHelper.getUserId());
				insertEdCust.setLoanAmtAll(new BigDecimal(quotaSyncopateRec.getLoanAmtAll()).doubleValue());
				insertEdCust.setAvlAmtAll(new BigDecimal(quotaSyncopateRec.getLoanAmtAll()).doubleValue());
				LogManager.getLogger(LogManager.MODEL_INTERFACEX)
						.info("[asegService]:" + BeanUtils.describe(insertEdCust));
				resultCusts.add(insertEdCust);
			}
			// 去重复
			HashSet<String> hashSet = new HashSet<String>(custNos);
			custNos.clear();
			custNos.addAll(hashSet);
			/**
			 * 删除客户下的所有额度信息
			 */
			for (String custNo : custNos) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======准备切分删除客户代码custNo:" + custNo);
				// 删除表TD_ED_CUST_OP中交易对手在TD_ED_CUST的数据
				edCustMapper.deleteAllEdCustOpForEcifNum(custNo);
				// 删除TD_ED_CUST的数据
				edCustMapper.deleteAllEdCustForEcifNum(custNo);
			}
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======准备切分试算的客户代码:" + custNos.toArray());
			if (null != resultCusts && resultCusts.size() > 0) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======准备切分试算的客户额度条数:" + resultCusts.size());
				batchDao.batch("com.singlee.capital.eu.mapper.TdEdCustMapper.insert", resultCusts);
				Pair<Boolean, List<TdEdDealLog>> pair = edCustService.releaseEdByCustNo(custNos, null, null);
				if (!pair.getLeft()) {
					retMsg.setCode("000130");
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======切分失败:" + custNos.toArray());
					for (TdEdDealLog tdEdDealLog : pair.getRight()) {
						LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(BeanUtils.describe(tdEdDealLog));
					}
					throw new RException(retMsg.toJsonString());
				} else {
					LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======切分成功:" + custNos.toArray());
				}
			} else {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======准备切分试算的客户额度条数为0");
			}

		} catch (Exception e) {
			// TODO: handle exception
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(e);
		} finally {
			LogManager.getLogger(LogManager.MODEL_INTERFACEX)
					.info("***********************[asegService]****************************");
		}
		return retMsg;
	}

	@Override
	public Pair<Boolean, List<TdEdDealLog>> eduOccpFlowService(List<EdInParams> enEdInParams, List<String> dealNos) {
		// 表锁
		try {
			edCustMapper.selectEdCustsForLocked();
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			throw new RException(e);
		}
		// TODO Auto-generated method stub
		List<String> custNos = new ArrayList<String>();
		for (EdInParams edInParams : enEdInParams) {
			custNos.add(edInParams.getEcifNo());
		}
		if (custNos.size() > 0) {
			// 去重复
			HashSet<String> hashSet = new HashSet<String>(custNos);
			custNos.clear();
			custNos.addAll(hashSet);
		}
		return edCustService.releaseEdByCustNo(custNos, enEdInParams, dealNos);
	}

	@Override
	public List<Pair<EdInParams, List<TdEdDealLog>>> eduOccpFlowServiceTransfor(List<EdInParams> enEdInParams,
			List<String> dealNos) {
		// 表锁
		try {
			edCustMapper.selectEdCustsForLocked();
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			throw new RException(e);
		}
		// TODO Auto-generated method stub
		List<String> custNos = new ArrayList<String>();
		for (EdInParams edInParams : enEdInParams) {
			custNos.add(edInParams.getEcifNo());
		}
		if (custNos.size() > 0) {
			// 去重复
			HashSet<String> hashSet = new HashSet<String>(custNos);
			custNos.clear();
			custNos.addAll(hashSet);
		}
		return edCustService.releaseEdByCustNoTransfor(custNos, enEdInParams, dealNos);
	}

	@Override
	public boolean eduReleaseFlowService(String dealNo) {
		// 表锁
		edCustMapper.selectEdCustsForLocked();
		// TODO Auto-generated method stub
		return edCustService.releaseEdByDealNo(dealNo);
	}

	@Override
	public boolean transforEduForImportDatas() {
		// TODO Auto-generated method stub
		// CREATE TABLE TD_ED_DEAL_LOG_TRANSFOR AS SELECT * FROM TD_ED_DEAL_LOG
		// 第一步查詢出所有TD_PRODUCT_APPROVE_MAIN 中不包含TRD ORD的交易數據 且 狀態為12
		/**
		 * SELECT <include refid="columns"><property name="alias" value="T"/></include>
		 * FROM TD_PRODUCT_APPROVE_MAIN T
		 * <include refid="join"><property name="alias" value="T"/></include> WHERE
		 * T.DEAL_NO NOT LIKE 'TRD%' AND T.DEAL_NO NOT LIKE 'ORD%'
		 */
		boolean resultBoolean = true;
		List<TdProductApproveMain> productApproveMains = productApproveMainMapper.getAllTransforDeals();
		edCustTransforMapper.deleteAllTdEdCustTransfor();
		edDealLogTransforMapper.deleteAllTdEdDealLogTransfor();
		productCustCreditMapper.deleteAllTdProductCustCredits();

		TdEdCustTransfor edCustTransfor = null;
		/**
		 * 循环参与方 先生成所需要占用额度的数据
		 */
		if (null != productApproveMains && productApproveMains.size() > 0) {
			for (TdProductApproveMain productApproveMain : productApproveMains) {
				List<TdProductApproveSub> tdSubs = participantService
						.transProductParticipantAsApproveSubs(BeanUtil.beanToMap(productApproveMain));
				Map<String, String> tdSubsMap = new HashMap<String, String>();
				for (TdProductApproveSub subs : tdSubs) {
					tdSubsMap.put(subs.getSubFldName(), subs.getSubFldValue());
				}
				try {
					batchDao.batch("com.singlee.capital.trade.mapper.TdProductCustCreditMapper.insert",
							productCustCreditService.getProductCustCreditForDeal(productApproveMain, tdSubsMap));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					throw new RException(e);
				}
			}
			// 更新汇率
			edCustTransforMapper.updateProductCreditTransfor();
			// 将所有的需要占用额度的额度EdInParams取出来
			/**
			 * SELECT TPCC.DEAL_NO,TPAM.DEAL_TYPE,TPAM.V_DATE,TPAM.M_DATE,
			 * TPAM.PRD_NO,TPCC.PARTY_ID,TPCC.CREDIT_AMT AS AMT, TPCC.CUSTMER_CODE AS
			 * ECIF_NO FROM TD_PRODUCT_CUST_CREDIT TPCC LEFT JOIN TD_PRODUCT_APPROVE_MAIN
			 * TPAM ON TPCC.DEAL_NO=TPAM.DEAL_NO WHERE TPCC.DEAL_NO NOT LIKE 'TRD%' AND
			 * TPCC.DEAL_NO NOT LIKE 'ORD%'
			 */
			List<EdInParams> creditList = productCustCreditMapper.getEdInParamsForProductCustCreditForTransfor();
			List<EdInParams> allEdInParams = new ArrayList<EdInParams>();// 最终需要占用额度的客户信息
			Map<String, Object> paramMaps = new HashMap<String, Object>();
			for (EdInParams edInParams : creditList) {
				if (null != StringUtils.trimToNull(edInParams.getEcifNo()))// ECIF_NO不能为空
				{
					paramMaps.clear();
					// 判断 Ecif客户号是否在 td_ed_cust里面存在;
					if (edCustMapper.getEdCustByEcifNum(edInParams.getEcifNo()).size() > 0)// 必须存在客户切分额度
					{
						allEdInParams.add(edInParams);
					} else {
						paramMaps.put("dealNo", edInParams.getDealNo());
						paramMaps.put("custmerCode", edInParams.getEcifNo());
						// 删除占用记录，避免后续重占出问题；
						productCustCreditMapper.deleteTdProductCustCreditByDealNoAndCustNo(paramMaps);
						System.out.println(edInParams.getEcifNo() + "==不存在客户切分额度信息，不会进入占用操作！");
						// 插入不成功的数据
						edCustTransfor = new TdEdCustTransfor();
						try {
							org.springframework.beans.BeanUtils.copyProperties(edInParams, edCustTransfor);
//							BeanUtils.copyProperties(edCustTransfor, edInParams);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							throw new RException(e);
						}
						edCustTransfor.setDescr("客户切分额度信息不存在，不会进入占用操作！");
						edCustTransforMapper.insert(edCustTransfor);
					}
				}

			}

			// 每次额度重新占用的时候需要重置额度
			List<String> custNos = new ArrayList<String>();
			List<String> dealNos = new ArrayList<String>();
			for (EdInParams edInParam : allEdInParams) {
				dealNos.add(edInParam.getDealNo());
				custNos.add(edInParam.getEcifNo());
			}
			Map<String, Object> custNoMap = new HashMap<String, Object>();
			custNoMap.put("custNos", custNos);
			edCustMapper.updateAllEdByCustNoInit(custNoMap);

			if (null != allEdInParams && allEdInParams.size() > 0) {
				// 所有额度占用情况信息
				List<Pair<EdInParams, List<TdEdDealLog>>> pairResult = eduOccpFlowServiceTransfor(allEdInParams,
						dealNos);
				for (Pair<EdInParams, List<TdEdDealLog>> pair : pairResult) {
					List<TdEdDealLog> dealLogs = pair.getRight();
					if (null != pair.getLeft() && !pair.getLeft().isFlag()) {
						resultBoolean = false;
						// 插入不成功的数据
						edCustTransfor = new TdEdCustTransfor();
						try {
							org.springframework.beans.BeanUtils.copyProperties(pair.getLeft(),edCustTransfor);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							throw new RException(e);
						}
						edCustTransfor.setDescr("客户额度占用失败！");
						edCustTransforMapper.insert(edCustTransfor);
					}
					if (null != dealLogs && dealLogs.size() > 0) {
						List<TdEdDealLogTransfor> dealLogTransfors = new ArrayList<TdEdDealLogTransfor>();
						TdEdDealLogTransfor tdEdDealLogTransfor = null;
						for (TdEdDealLog edDealLog : dealLogs) {
							tdEdDealLogTransfor = new TdEdDealLogTransfor();
							try {
								org.springframework.beans.BeanUtils.copyProperties(edDealLog,tdEdDealLogTransfor);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
								throw new RException(e);
							}
							dealLogTransfors.add(tdEdDealLogTransfor);
						}
						batchDao.batch("com.singlee.capital.eu.mapper.TdEdDealLogTransforMapper.insert",
								dealLogTransfors);
					}
				}
			}
		}
		return resultBoolean;
	}

	@Override
	public boolean importCreditCustToDataBase(List<TdEdCustAsg> edCustAsgs) {
		// TODO Auto-generated method stub
		edCustAsgMapper.deleteAllTdEdCustAsg();
		edCustMapper.deleteAllEdCust();
		batchDao.batch("com.singlee.capital.eu.mapper.TdEdCustAsgMapper.insert", edCustAsgs);
		edCustAsgMapper.insertTdEdCustFromAsg();
		return true;
	}

	@Override
	public Pair<Boolean, List<TdEdDealLog>> unitaryLimitCccup(Map<String, String> paramMap,
			List<EdInParams> creditList) {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		List<String> dealNos = new ArrayList<String>();
		dealNos.add(serial_no);
		String product_type = ParameterUtil.getString(paramMap, "product_type", "");

		for (int i = 0; i < creditList.size(); i++) {
			creditList.get(i).setPrdNo(product_type);
			creditList.get(i).setDealType(DictConstants.DealType.Approve);
			creditList.get(i).setIsNewDeal("1");
		}
		Pair<Boolean, List<TdEdDealLog>> pair = eduOccpFlowService(creditList, dealNos);
		return pair;
	}

	/**
	 * 获取各个产品的风险管理详情
	 */
	@Override
	public String chooseOccupy(Map<String, String> paramMap) {
		String message = null;
		String prdNo = ParameterUtil.getString(paramMap, "product_type", "");
		String operator = SlSessionHelper.getUserId();
		Boolean checkAccessFlag = true;// 准入标签，false就不校验
		Boolean checkAccessFlagBK = true;// 校验准入黑名单标签，false就不校验
		Boolean checkLimitFlag = true;// 授信标签，false就不校验
		Boolean checkRiskFlag = false;// 单笔风险审查，false就不校验，默认不进行校验

		List<OpicsRisk> OpicsRiskList = new ArrayList<OpicsRisk>();
		OpicsRiskList = ifsCfetsfxLendMapper.searchProductSupervise(prdNo, operator);

		// 校验准入
		String checkAccess = null;
		// 校验授信
		String checkLimit = null;
		// 校验单笔风险
		String checkRisk = null;
		for (int i = 0; i < OpicsRiskList.size(); i++) {
			checkAccess = OpicsRiskList.get(i).getCheckAccess();
			checkLimit = OpicsRiskList.get(i).getCheckLimit();
			checkRisk = OpicsRiskList.get(i).getCheckRisk();

			String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
			// 搜索视图
			List<EdInParams> creditList = new ArrayList<EdInParams>();
			creditList = ifsCfetsfxLendMapper.searchAllInfo(serial_no);

			for (int k = 0; k < creditList.size(); k++) {

				double amt = creditList.get(k).getAmt();// 交易金额
				double amtAll = creditList.get(k).getAmt();// 交易金额不加权重值
				String direction = creditList.get(k).getDirection();
				String currencyPair = creditList.get(k).getCcyCode();// 币种或者货币对
				if (currencyPair.length() > 2) {
					currencyPair = currencyPair.substring(0, 3);
				}
				if ("100".equals(currencyPair)) {
					currencyPair = "JPY";
				}
				creditList.get(k).setCcyCode(currencyPair);
				String currency = creditList.get(k).getCcyCode();

				String rate = "1";
				if (!"CNY".equals(currency)) {
					Map<String, String> mapRate = new HashMap<String, String>();
					mapRate.put("ccy", currency);
					mapRate.put("br", "01");// 机构号，送01
					BigDecimal dayRate = acupServer.queryRate(mapRate);
					if (dayRate == null || "".equals(dayRate)) {
						JY.raise("货币" + currency + "不存在汇率");
					}
					rate = dayRate + "";// 以后需要修改为BigDecimal
				}

				// 汇率乘以金额
				amt = Double.parseDouble(rate) * amt;
				amtAll = amt;

				// 衍生品实时更新额度
				if ("391".equals(prdNo) || "432".equals(prdNo) || "441".equals(prdNo) || "433".equals(prdNo)) {
					// 391外汇远期,432外汇掉期,433人民币期权,441利率互换
					String startDate = creditList.get(k).getvDate();// 业务起始日期
					String dueDate = creditList.get(k).getmDate();// 业务到期日期
					String weight = "100";
					try {
						weight = getNowDateWeight(startDate, dueDate, prdNo);
					} catch (Exception e) {
						e.printStackTrace();
					}
					amt = amt * Double.parseDouble(weight) / 100;// 名义本金乘上权重

					// 衍生品加上估值
					// 获取当前日期和昨日日期
					String isNight = ParameterUtil.getString(paramMap, "isNight", "");// 为1时，时日终定时，获取当日估值,其他值获取昨日估值
					String effDate = ifsOpicsSettleManageMapper.getCurDate();

					if (!"1".equals(isNight)) {
						effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
					}
					// String nowDateTime = DateUtil.getCurrentDateAsString();日终时计算
					/*
					 * Calendar cal=Calendar.getInstance(); cal.add(Calendar.DATE,-1); Date
					 * time=cal.getTime(); String yesterday = new
					 * SimpleDateFormat("yyyy-MM-dd").format(time);
					 */
					String opicsNo = creditList.get(k).getOpicsNo();
					Map<String, String> mapValue = new HashMap<String, String>();
					mapValue.put("dealno", opicsNo);
					mapValue.put("postdate", effDate);
					mapValue.put("prdNo", prdNo);
					BigDecimal mapValueLast = acupServer.queryValue(mapValue);

					if (mapValueLast != null) {
						double valueCal = Double.parseDouble(mapValueLast + "");

						if ("432".equals(prdNo)) {// 对于人民币掉期的交易，估值为近端的估值加上远端估值,先进行估值相加，值为正则为正，为负则取零
							IfsCfetsfxSwap ifsCfetsfxSwap = new IfsCfetsfxSwap();
							ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);// 人命币掉期获取远端opics账号
							String opicsNo2 = ifsCfetsfxSwap.getFarDealNo();// 远端opics编号
							if ("".equals(opicsNo2) || opicsNo2 == null) {
								opicsNo2 = Integer.parseInt(opicsNo) + 1 + "";// 远端交易的opics账号，获取近端交易的opicsNo加1
							}

							mapValue.put("dealno", opicsNo2);
							mapValue.put("prdNo", prdNo);
							BigDecimal mapValueLast2 = acupServer.queryValue(mapValue);
							valueCal = Double.parseDouble(mapValueLast2 + "") + valueCal;
						}

						Map<String, Object> mapProfit = new HashMap<String, Object>();
						mapProfit.put("profitLoss", valueCal + "");
						mapProfit.put("ticketId", serial_no);
						// 将估值插入数据库表中
						if ("391".equals(prdNo)) {// 人民币远期
							// ifs_cfetsfx_fwd
							ifsCfetsfxFwdMapper.updateProfitLess(mapProfit);
						} else if ("432".equals(prdNo)) {// 人民币掉期
							// IFS_CFETSFX_SWAP
							// 再增加一个far_deal_no
							// 增加一个profit_Loss
							ifsCfetsfxSwapMapper.updateProfitLess(mapProfit);
						} else if ("433".equals(prdNo)) {// 人民币期权
							// IFS_CFETSFX_OPTION
							ifsCfetsfxOptionMapper.updateProfitLess(mapProfit);
						} else if ("441".equals(prdNo)) {// 利率互换
							// IFS_CFETSRMB_IRS
							ifsCfetsrmbIrsMapper.updateProfitLess(mapProfit);
						}

						if (valueCal < 0) {
							valueCal = 0;
						}
						amt = valueCal + amt;// 上一日估值加上今日本金乘权重
					}
				} else {
					amt = amt * Double.parseDouble(creditList.get(k).getWeight()) / 100;
				}
				creditList.get(k).setAmt(amt);// 金额赋值
				creditList.get(k).setAmtAll(amtAll);// 金额不计算权重

				if ("452".equals(prdNo)) {// 存单发行
					checkAccessFlag = false;
					checkLimitFlag = false;
					checkAccessFlagBK = true;
				}
				if ("P".equals(direction)) {// 哪些不需要校验白名单准入checkAccessFlag,单笔风险
					if ("438".equals(prdNo) || "444".equals(prdNo) || "441".equals(prdNo) || "445".equals(prdNo)) {
						// 438外币拆借,444信用拆借,441利率互换,445债券借贷
						checkAccessFlag = false;
					}
				} else {
					if ("411".equals(prdNo) || "443".equals(prdNo) || "391".equals(prdNo) || "432".equals(prdNo)
							|| "433".equals(prdNo) || "451".equals(prdNo) || "400".equals(prdNo)) {
						// 411人命币即期,442买断式回购，443现券买卖，446质押式回购,，391人民币远期，
						// 432人民币掉期，433人民币期权,451外币债,400衍生品
						// 对于质押式和买断式回购不区分方向，都进行准入校验
						checkAccessFlag = false;
					}
				}

				if ("S".equals(direction)) {// 哪些不需要校验黑名单checkAccessFlagBK
					if ("438".equals(prdNo) || "444".equals(prdNo) || "441".equals(prdNo) || "445".equals(prdNo)) {
						// 438外币拆借,444信用拆借,441利率互换,445债券借贷
						checkAccessFlagBK = false;
					}
				} else {
					if ("411".equals(prdNo) || "442".equals(prdNo) || "443".equals(prdNo) || "446".equals(prdNo)
							|| "391".equals(prdNo) || "432".equals(prdNo) || "433".equals(prdNo) || "451".equals(prdNo)
							|| "400".equals(prdNo)) {
						// 411人命币即期,442买断式回购，443现券买卖，446质押式回购,，391人民币远期，
						// 432人民币掉期，433人民币期权,451外币债,400衍生品
						checkAccessFlagBK = false;
					}
				}

				if ("P".equals(direction)) {// 哪些不需要校验授信checkLimitFlag
					if ("438".equals(prdNo) || "444".equals(prdNo) || "441".equals(prdNo) || "445".equals(prdNo)) {
						// 438外币拆借,444信用拆借,441利率互换,445债券借贷
						checkLimitFlag = false;
					}
				} else {
					if ("442".equals(prdNo) || "443".equals(prdNo) || "446".equals(prdNo) || "451".equals(prdNo)
							|| "400".equals(prdNo)) {
						// 411人命币即期,442买断式回购，443现券买卖，446质押式回购，391人民币远期，
						// 432人民币掉期，433人民币期权,451外币债,400衍生品
						checkLimitFlag = false;
					}
					String pureFlag = ParameterUtil.getString(paramMap, "pureFlag", "");// 清算之后值为1
					if ("443".equals(prdNo) || "451".equals(prdNo)) {
						// 清算之后的交易债券交易卖出进行限额
						// 443现券买卖,451外币债,442买断式回购
						if ("1".equals(pureFlag)) {
							checkLimitFlag = true;
						}
					}
				}

				if ("443".equals(prdNo) || "451".equals(prdNo)) {// 对于国债SE-GZ，地方政府债SE-DFZFZ，和央行票据SE-YP不占用授信
					// 441利率互换，442买断式回购，443现券买卖，445债券借贷，446质押式回购，451外币债
					Map<String, Object> messageMap = new HashMap<String, Object>();
					messageMap.put("ticketId", serial_no);
					List<EdInParams> creditListLimit = ifsCfetsfxLendMapper.limitBondExchangeInfo(messageMap);
					for (int j = 0; j < creditListLimit.size(); j++) {
						String bondProperties = creditListLimit.get(j).getBondProperties();
						if ("SE-GZ".equals(bondProperties) || "SE-DFZFZ".equals(bondProperties)
								|| "SE-YP".equals(bondProperties)) {
							checkLimitFlag = false;
						}
					}
				}

				if ("P".equals(direction)) {// 哪些校验单笔风险checkRiskFlag
					// 442买断式回购，443现券买卖，451外币债
					if ("442".equals(prdNo) || "443".equals(prdNo) || "446".equals(prdNo) || "451".equals(prdNo)
							|| "400".equals(prdNo)) {
						// 411人命币即期,442买断式回购，443现券买卖，446质押式回购,，391人民币远期，
						// 432人民币掉期，433人民币期权,451外币债,400衍生品
						checkRiskFlag = true;
					}
				} else {
					if ("438".equals(prdNo) || "444".equals(prdNo) || "441".equals(prdNo) || "445".equals(prdNo)) {
						// 438外币拆借,444信用拆借,441利率互换,445债券借贷
						checkRiskFlag = true;
					}
				}

			}

			if ("1".equals(checkLimit) && checkLimitFlag) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("===========校验授信额度开始========");

				Pair<Boolean, List<TdEdDealLog>> pair = unitaryLimitCccup(paramMap, creditList);
				if (pair.getLeft() == false) {
					message = "授信额度不足";
					JY.raise(message);
				} else if (pair.getLeft()) {
					message = "额度占用成功";
				}
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("===========校验授信额度结束========");
			}

			if ("1".equals(checkAccess)) {// 校验准入白名单，
				// if("1".equals(checkAccess) &&
				// checkAccessFlag){//校验准入，按方向校验，出钱的方向进行校验，12/3日修改为不管方向都判断
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验准入额度白名单开始========");

				String custNo = null;
				for (int j = 0; j < creditList.size(); j++) {
					custNo = creditList.get(j).getEcifCust();// 交易对手信息
					if ("".equals(custNo) || null == custNo) {
						JY.raise("校验准入时,该交易对手的ecif客户号不存在。");
					}
					IfsTradeAccess ifsTradeAccess = ifsLimitTemplateMapper.querySingleCustNoLimit(custNo, prdNo);

					if (ifsTradeAccess == null || "".equals(ifsTradeAccess)) {
						JY.raise("该客户的同业准入校验失败。");
						LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验准入额度失败，交易对手号：" + custNo);
					}
					message = "校验准入额度成功！";
				}

				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验准入额度白名单结束========");
			}

			if (checkAccessFlagBK) {// 校验准入黑名单
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验额度黑名单开始========");
				String custNo = null;
				for (int j = 0; j < creditList.size(); j++) {
					custNo = creditList.get(j).getEcifCust();// 交易对手信息
					if (custNo == null || "".equals(custNo)) {
						JY.raise("该客户ecif客户号未维护");
					}
					IfsTradeAccess ifsTradeAccess = ifsLimitTemplateMapper.querySingleCustNoLimitBK(custNo, prdNo);

					if (ifsTradeAccess == null || "".equals(ifsTradeAccess)) {
						message = "校验准入黑名单成功！";
					} else {
						JY.raise("该客户为黑名单用户。");
						message = "校验准入黑名单失败！";
						LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验准入黑名单失败，交易对手号：" + custNo);
					}
				}

			}

			if ("1".equals(checkRisk) && checkRiskFlag) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验单笔风险开始=======");

				for (int j = 0; j < creditList.size(); j++) {
					String applyNo = creditList.get(j).getApplyNo();// 单笔风险审查单号
					IfsCreditRiskreview ifsCreditRiskreview = ifsLimitTemplateMapper.querySingleDealLimit(applyNo);
					if (ifsCreditRiskreview == null || "".equals(ifsCreditRiskreview)) {
						JY.raise("审批单号为" + applyNo + "的单笔风险校验失败！");
					}
					String status = ifsCreditRiskreview.getTranStatus();
					if (!"2".equals(status)) {

						JY.raise("审批单号为" + applyNo + "的单笔风险校验失败。交易状态不为审批完成！");
					}
				}
				message = "校验单笔风险成功！";

				LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======校验单笔风险结束========");
			}
		}
		return message;
	}

	/**
	 * 检验外汇限额
	 */
	@Override
	public String limitForeignExchange(Map<String, String> paramMap) {
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======外汇校验限额 开始============");
		String message = null;
		String product_type = ParameterUtil.getString(paramMap, "product_type", "");// 产品编号
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String code = ParameterUtil.getString(paramMap, "code", "");
		// 搜索视图
		List<EdInParams> creditList = ifsCfetsfxLendMapper.searchForeignExchangeInfo(serial_no);
		String port = "";
		for (int i = 0; i < creditList.size(); i++) {// 额度限制

			creditList.get(i).setPrdNo(product_type);
			creditList.get(i).setDealType(DictConstants.DealType.Approve);
			Map<String, Object> occupyLimitMap = new HashMap<String, Object>();
			occupyLimitMap.put("ticketId", creditList.get(i).getDealNo());// 交易编号
			occupyLimitMap.put("code", code);// 0,是债券，1是外汇
			occupyLimitMap.put("trader", SlSessionHelper.getUserId());// 交易员
			occupyLimitMap.put("prd", creditList.get(i).getProduct());// 产品代码
			occupyLimitMap.put("prdType", creditList.get(i).getProdType());// 产品类型
			occupyLimitMap.put("direction", creditList.get(i).getDirection());// 交易方向
			occupyLimitMap.put("dealSource", creditList.get(i).getDealSource());// 交易来源
			occupyLimitMap.put("cost", creditList.get(i).getCost());// 成本中心
			occupyLimitMap.put("prdNo", product_type);// 产品编号
			port = creditList.get(i).getPort();// 投资组合
			String ccyPair = creditList.get(i).getCcyCode();
			if (!ccyPair.contains("CNY")) {// 外汇交易，外币对外币的交易不进行外汇的限额
				creditList.get(i).setAmt(0.0);
			}
			if (creditList.get(i).getCcyCode().length() > 2) {
				ccyPair = ccyPair.substring(0, 3);
			}
			if ("100".equals(ccyPair)) {
				occupyLimitMap.put("ccy", "JPY");// 币种
			} else {
				occupyLimitMap.put("ccy", ccyPair);// 币种
			}
			// 获取汇率

			String currency = (String) occupyLimitMap.get("ccy");

			double rate = 1.0;
			if (!"CNY".equals(currency)) {
				Map<String, String> mapRate = new HashMap<String, String>();
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				rate = Double.parseDouble(dayRate + "");// 以后需要修改为BigDecimal
			}
			double amt = creditList.get(i).getAmt() * rate;
			occupyLimitMap.put("amt", amt);// 交易金额

			String vDate = creditList.get(i).getvDate();// 交易起始日期
			String mDate = creditList.get(i).getmDate();// 交易到期日
			String term = null;
			try {
				term = getIntervalDay(vDate, mDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			occupyLimitMap.put("term", term);// 交易到期日期和今天相差多少天

			if (!"".equals(port) && port != null) {
				if ("FXZY".equals(port)) {
					// 期限类型,只对自营交易进行额度的控制
					Map<String, Object> b = ifsLimitService.occupyLimit(occupyLimitMap);
					String errorCode = ParameterUtil.getString(b, "errorCode", "");
					if ("ERROR".equals(errorCode)) {
						String errorMessage = ParameterUtil.getString(b, "errorMesg", "");
						JY.raise(errorMessage);
					}
				}
			}

		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======外汇交易,交易编号" + serial_no + "校验限额结束============");
		return message;
	}

	/**
	 * 检验债券存单控制
	 */
	@Override
	public String limitBondExchange(Map<String, String> paramMap, String inFlag) {
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======债券存单校验限额 开始============");
		String message = null;
		String prdNo = ParameterUtil.getString(paramMap, "product_type", "");
		String product_type = ParameterUtil.getString(paramMap, "product_type", "");// 产品编号
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String code = ParameterUtil.getString(paramMap, "code", "");
		// 搜索视图
		Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("ticketId", serial_no);
		List<EdInParams> creditList = ifsCfetsfxLendMapper.limitBondExchangeInfo(messageMap);
		for (int i = 0; i < creditList.size(); i++) {// 额度限制

			creditList.get(i).setPrdNo(product_type);
			creditList.get(i).setDealType(DictConstants.DealType.Approve);
			Map<String, Object> occupyLimitMap = new HashMap<String, Object>();
			occupyLimitMap.put("ticketId", creditList.get(i).getDealNo());// 交易编号
			occupyLimitMap.put("code", code);// 0,是债券，1是外汇
			occupyLimitMap.put("trader", SlSessionHelper.getUserId());// 交易员
			occupyLimitMap.put("direction", creditList.get(i).getDirection());// 交易方向
			occupyLimitMap.put("dealSource", creditList.get(i).getDealSource());// 交易来源
			occupyLimitMap.put("cost", creditList.get(i).getCost());// 成本中心
			occupyLimitMap.put("prd", creditList.get(i).getProduct());// 产品代码
			occupyLimitMap.put("prdType", creditList.get(i).getProdType());// 产品类型
			occupyLimitMap.put("invtype", creditList.get(i).getInvtype());// 账户类型
			occupyLimitMap.put("bondCode", creditList.get(i).getBondCode());// 债券代码
			// occupyLimitMap.put("acctngType", creditList.get(i).getProdType());//会计类型
			occupyLimitMap.put("settccy", creditList.get(i).getSettccy());// 清算货币
			occupyLimitMap.put("bondProperties", creditList.get(i).getBondProperties());// 债券性质/债券类型 ---国债，央票
			occupyLimitMap.put("amt", creditList.get(i).getAmt());// 交易金额
			occupyLimitMap.put("prdNo", prdNo);// 系统产品编号
			occupyLimitMap.put("matdt", creditList.get(i).getMatdt());// 债券到期日

			String vDate = creditList.get(i).getvDate();// 交易起始日期
			String mDate = creditList.get(i).getmDate();// 交易到期日
			String term = null;
			try {
				term = getIntervalDay(vDate, mDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			occupyLimitMap.put("term", term);// 交易到期日期和今天相差多少天

			// String direction = creditList.get(i).getDirection();//交易方向
			// if(("P".equals(direction) && "0".equals(inFlag)) ||("S".equals(direction) &&
			// "1".equals(inFlag))){
			// 对于买入方向，只是审批台提交时进行限额计算，对于卖出方向通过清算之后再进行限额计算,2018/10/30所有现劵买卖都在交易员提交时占用限额
			Map<String, Object> b = ifsLimitService.occupyLimit(occupyLimitMap);
			String errorCode = ParameterUtil.getString(b, "errorCode", "");
			if ("ERROR".equals(errorCode)) {
				String errorMessage = ParameterUtil.getString(b, "errorMesg", "");
				// JY.raiseRException(errorMessage);
				JY.raise(errorMessage);
			}
		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======债券存单,交易编号" + serial_no + "校验限额结束============");
		return message;
	}

	/**
	 * 计算交易日期和起始相差多少天 返回字符串天数
	 * 
	 */
	public static String getIntervalDay(String vDate, String mDate) throws Exception {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// Date date1 = new Date();
		Date date1 = sdf.parse(mDate);
		Date date2 = sdf.parse(mDate);
		String days = (date2.getTime() - date1.getTime()) / (24 * 3600 * 1000) + "";
		return days;
	}

	/**
	 * 计算交易的权重值
	 * 
	 */
	@Override
	public String getNowDateWeight(String vDate, String mDate, String prdNo) throws Exception {
		String weight = "100";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageNumber", 1);
		map.put("pageSize", 20);
		map.put("productCode", prdNo);

		List<ProductWeight> weightList = new ArrayList<ProductWeight>();
		weightList = tdCustEcifMapper.getProductList(map);
		for (int i = 0; i < weightList.size(); i++) {
			double ruleMax = Double.parseDouble(weightList.get(i).getRuleMax());
			double ruleMin = Double.parseDouble(weightList.get(i).getRuleMin());
			String containsMax = weightList.get(i).getContainsMax();
			String containsMin = weightList.get(i).getContainsMin();
			double totalDay = Double.parseDouble(getIntervalDay(vDate, mDate));
			if (ruleMax == 0) {
				if (totalDay > ruleMin) {
					weight = weightList.get(i).getWeight();
					break;
				} else if (totalDay == ruleMin && "1".equals(containsMin)) {
					weight = weightList.get(i).getWeight();
					break;
				}
			}
			if (ruleMin == 0) {
				if (totalDay < ruleMax) {
					weight = weightList.get(i).getWeight();
					break;
				} else if (totalDay == ruleMax && "1".equals(containsMax)) {
					weight = weightList.get(i).getWeight();
					break;
				}
			}
			if (totalDay < ruleMax && totalDay > ruleMin) {
				weight = weightList.get(i).getWeight();
				break;
			}
			if (totalDay == ruleMin && "1".equals(containsMin)) {
				weight = weightList.get(i).getWeight();
				break;
			}
			if (totalDay == ruleMax && "1".equals(containsMax)) {
				weight = weightList.get(i).getWeight();
				break;
			}
		}
		return weight;
	}

	/**
	 * 正式交易将预占用转换成占用明细
	 * 
	 */
	@Override
	public String changeOccupyType(String dealNo) {
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======审批单通过，修改预占用为占用============");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("dealNo", dealNo);
		TdEdCustBatchStatus tdEdCustBatchStatus = tdEdDealLogMapper.getPerusedDealNo(paramMap);
		if (tdEdCustBatchStatus != null) {
			paramMap.put("batchCId", tdEdCustBatchStatus.getBatchCId());
			paramMap.put("prdNo", tdEdCustBatchStatus.getPrdNo());
			paramMap.put("custCreditId", tdEdCustBatchStatus.getCustCreditId());
			double amt = tdEdCustBatchStatus.getAmt();// 交易金额
			double usedAmt = tdEdCustBatchStatus.getUsedAmt() + amt;// 占用金额
			double preUsedAmt = tdEdCustBatchStatus.getPreUsedAmt() - amt;// 预占用金额

			DecimalFormat df = new DecimalFormat("#.00");
			paramMap.put("usedAmt", df.format(usedAmt));
			paramMap.put("preUsedAmt", df.format(preUsedAmt));

			// 更新额度状态表信息
			tdEdDealLogMapper.updateCustStatus(paramMap);// 更新TD_ED_CUST_BATCH_STATUS状态DEAL_TYPE为2
			tdEdDealLogMapper.updatePerusedStatus(paramMap);// 更新预占用到占用明细，TD_ED_DEAL_LOG的状态ED_OP_TYPE为2
			tdEdDealLogMapper.updatePerusedAmt(paramMap);// 更新预占用总额度到占用额度
		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("======审批单通过，修改预占用为占用，修改结束============");
		String status = "Success";
		return status;

	}

	@Override
	public String ReleaseDueDateDeal() {

		String nowDate = DateUtil.getCurrentDateAsString();// 获取当前时间
		LogManager.getLogger(LogManager.MODEL_INTERFACEX)
				.info("===========开始对" + nowDate + " 到期的额度进行释放===============");

		String dealNo = null;// 流程中的交易流水号
		List<TdEdCustBatchStatus> allMDateDeal = tdEdCustBatchStatusMapper.getAllMDateDeal(nowDate);

		for (int i = 0; i < allMDateDeal.size(); i++) {
			try {
				dealNo = allMDateDeal.get(i).getDealNo();
				// 额度释放接口
				eduReleaseFlowService(dealNo);
				// 到期释放债券
				bondLimitTemplateMapper.deleteOverTimeBond(dealNo);
			} catch (Exception e) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX)
						.info("==========交易流水号为：" + dealNo + "的额度释放失败！============");
				e.printStackTrace();
			}
		}
		LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("===========" + nowDate + " 到期的额度释放完成===============");

		return null;
	}

	@Override
	public String occupyAgainDeal() {

		String serial_no = null;
		String prdNo = null;
		String currencyPair = null;
		String currency = null;
		String opicsNo = null;
		double amt = 0.0;
		List<EdInParams> creditList = new ArrayList<EdInParams>();
		creditList = ifsCfetsfxLendMapper.searchAllInfo(serial_no);// 查询所有交易
		for (int k = 0; k < creditList.size(); k++) {

			prdNo = creditList.get(k).getPrdNo();
			if ("391".equals(prdNo) || "432".equals(prdNo) || "441".equals(prdNo) || "433".equals(prdNo)) {
				// 391外汇远期,432外汇掉期,433人民币期权,441利率互换
				serial_no = creditList.get(k).getDealNo();
				opicsNo = creditList.get(k).getOpicsNo();
				currencyPair = creditList.get(k).getCcyCode();
				amt = creditList.get(k).getAmt();

				List<TdEdCustBatchStatus> allMDateDeal = tdEdCustBatchStatusMapper.getAllNotDuedateDeal(serial_no);
				if (allMDateDeal == null || "".equals(allMDateDeal)) {// 没有授信的交易不重新占用
					continue;
				}

				if (currencyPair.length() > 2) {
					currencyPair = currencyPair.substring(0, 3);
				}
				if ("100".equals(currencyPair)) {
					currencyPair = "JPY";
				}
				creditList.get(k).setCcyCode(currencyPair);
				currency = creditList.get(k).getCcyCode();

				String rate = "1";
				if (!"CNY".equals(currency)) {
					Map<String, String> mapRate = new HashMap<String, String>();
					mapRate.put("ccy", currency);
					mapRate.put("br", "01");// 机构号，送01
					BigDecimal dayRate = acupServer.queryRate(mapRate);
					if (dayRate == null || "".equals(dayRate)) {
						// JY.raise("货币"+currency+"不存在汇率");
						LogManager.getLogger(LogManager.MODEL_INTERFACEX)
								.info("===========交易编号" + serial_no + "汇率查找失败===============");
						dayRate = new BigDecimal("1");
						// continue;
					}
					rate = dayRate + "";// 以后需要修改为BigDecimal
				}

				// 汇率乘以金额
				amt = Double.parseDouble(rate) * amt;
				String startDate = creditList.get(k).getvDate();// 业务起始日期
				String dueDate = creditList.get(k).getmDate();// 业务到期日期
				String weight = "100";
				try {
					weight = getNowDateWeight(startDate, dueDate, prdNo);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				amt = amt * Double.parseDouble(weight) / 100;// 名义本金乘上权重

				// 衍生品加上估值
				String nowDateTime = DateUtil.getCurrentDateAsString();// 日终时计算
				// nowDateTime = nDaysAfterOneDateString(nowDateTime,-1);//获取上一日日期

				Map<String, String> mapValue = new HashMap<String, String>();
				mapValue.put("dealno", opicsNo);
				mapValue.put("postdate", nowDateTime);
				mapValue.put("prdNo", prdNo);
				BigDecimal mapValueDue = acupServer.queryValue(mapValue);
				double valueCal = 0;
				if (mapValueDue != null) {
					valueCal = Double.parseDouble(mapValueDue + "");

					if ("432".equals(prdNo)) {// 对于人民币掉期的交易，估值为近端的估值加上远端估值,先进行估值相加，值为正则为正，为负则取零

						IfsCfetsfxSwap ifsCfetsfxSwap = new IfsCfetsfxSwap();
						ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);// 人命币掉期获取远端opics账号
						String opicsNo2 = ifsCfetsfxSwap.getFarDealNo();// 远端opics编号
						if ("".equals(opicsNo2) || opicsNo2 == null) {
							opicsNo2 = Integer.parseInt(opicsNo) + 1 + "";// 远端交易的opics账号，获取近端交易的opicsNo加1
						}

						mapValue.put("dealno", opicsNo2);
						BigDecimal mapValueLast2 = acupServer.queryValue(mapValue);
						valueCal = Double.parseDouble(mapValueLast2 + "") + valueCal;
					}

					Map<String, Object> mapProfit = new HashMap<String, Object>();
					mapProfit.put("profitLoss", valueCal + "");
					// 将估值插入数据库表中
					if ("391".equals(prdNo)) {// 人民币远期
						// ifs_cfetsfx_fwd
						ifsCfetsfxFwdMapper.updateProfitLess(mapProfit);
					} else if ("432".equals(prdNo)) {// 人民币掉期
						// IFS_CFETSFX_SWAP
						// 再增加一个far_deal_no
						// 增加一个profit_Loss
						ifsCfetsfxSwapMapper.updateProfitLess(mapProfit);
					} else if ("433".equals(prdNo)) {// 人民币期权
						// IFS_CFETSFX_OPTION
						ifsCfetsfxOptionMapper.updateProfitLess(mapProfit);
					} else if ("441".equals(prdNo)) {// 利率互换
						// IFS_CFETSRMB_IRS
						ifsCfetsrmbIrsMapper.updateProfitLess(mapProfit);
					}

				}
				if (valueCal < 0) {
					valueCal = 0;
				}
				amt = valueCal + amt;// 今日估值加上今日本金乘权重

				creditList.get(k).setAmt(amt);// 金额赋值

			} else {
				continue;
			}

			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("serial_no", serial_no);
			paramMap.put("product_type", prdNo);

			List<EdInParams> creditListLast = new ArrayList<EdInParams>();
			creditListLast = ifsCfetsfxLendMapper.searchAllInfo(serial_no);

			Pair<Boolean, List<TdEdDealLog>> pair = unitaryLimitCccup(paramMap, creditListLast);
			if (pair.getLeft() == false) {
				LogManager.getLogger(LogManager.MODEL_INTERFACEX)
						.info("===========交易编号" + serial_no + "额度占用失败 ===============");
			} else if (pair.getLeft()) {

			}
		}

		return null;
	}

	// 给定一个日期型字符串，返回加减n天后的日期型字符串
	public String nDaysAfterOneDateString(String basicDate, int n) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date tmpDate = null;
		try {
			tmpDate = df.parse(basicDate);
		} catch (Exception e) {
			// 日期型字符串格式错误
		}
		long nDay = (tmpDate.getTime() / (24 * 60 * 60 * 1000) + 1 + n) * (24 * 60 * 60 * 1000);
		tmpDate.setTime(nDay);

		return df.format(tmpDate);
	}

	// 获取产品偏离度
	@Override
	public String queryRhisIntrate(Map<String, String> map) {

		String result = "";
		String rateCode = "";
		// String prdNo = map.get("prdNo");// 产品代码
		String prdNo = ParameterUtil.getString(map, "prdNo", "");// 产品代码
		if ("".equals(prdNo)) {
			return rateCode;
		}
		DecimalFormat df = new DecimalFormat("#0.00000000");

		// String effDate=map.get("effDate");//前置时间
		// 信用拆借：拆借利率，质押式回购、买断式回购：回购利率,
		Map<String, Object> mapIntRate = new HashMap<String, Object>();
		// mapIntRate.put("rateCode", rateCode);
		// mapIntRate.put("effDate", effDate);
		mapIntRate.put("br", "01");// 机构号，送01
		String fontRate = "";
		BigDecimal strRate = new BigDecimal(0);
		// if("cbt".equals(prdNo)){
		if ("443".equals(prdNo)) {
			// 现券买卖
			fontRate = ParameterUtil.getString(map, "fontRate", "");// 前台页面净价
			String bondCode = ParameterUtil.getString(map, "bondCode", "");// 债券代码
			if ("".equals(fontRate) || "".equals(bondCode)) {
				return rateCode;
			}
			// fontRate = map.get("cleanPrice");
			mapIntRate.put("secid", map.get("bondCode"));// 债券代码
			mapIntRate.put("cleanPrice", fontRate);// 净价
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			mapIntRate.put("effDate", effDate);
			strRate = acupServer.querySeclPrice(mapIntRate);
		}
		if ("452".equals(prdNo)) {
			// 存单发行
			String depositCode = ParameterUtil.getString(map, "depositCode", "");// 债券代码
			String benchmarkcurvename = ParameterUtil.getString(map, "benchmarkcurvename", "");// 参考收益率
			if ("".equals(depositCode) || "".equals(benchmarkcurvename)) {
				return rateCode;
			}

			// String depositCode = map.get("depositCode");// 债券代码
			mapIntRate.put("secid", depositCode);// 债券代码
			fontRate = map.get("benchmarkcurvename");// 参考收益率
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			mapIntRate.put("effDate", effDate);
			strRate = acupServer.querySeclRate(mapIntRate);
		}
		Double intRate = 1.0;// shibor，libor，上一日定盘，回购价格，上一日中间价
		if ("444".equals(prdNo)) {
			// 信用拆借
			String tradingProduct = ParameterUtil.getString(map, "tradingProduct", "");// 交易品种
			String rate = ParameterUtil.getString(map, "rate", "");// 拆借利率
			if ("".equals(tradingProduct) || "".equals(rate)) {
				return rateCode;
			}

			if (tradingProduct.contains("001")) {
				rateCode = "SHLBON";
			} else if (tradingProduct.contains("007")) {
				rateCode = "SHLBSW";
			} else if (tradingProduct.contains("014")) {
				rateCode = "SHLB2W";
			} else if (tradingProduct.contains("1M")) {
				rateCode = "SHLB1M";
			} else if (tradingProduct.contains("2M")) {// 2个月没有曲线取值为3个月的值
				rateCode = "SHLB3M";
			} else if (tradingProduct.contains("3M")) {
				rateCode = "SHLB3M";
			}
			mapIntRate.put("rateCode", rateCode);
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
			mapIntRate.put("effDate", effDate);
			strRate = acupServer.queryRhisIntrate(mapIntRate);
			fontRate = map.get("rate");// 前台页面拆借利率
		}
		if ("446".equals(prdNo)) {
			// 是质押式回购
			String tradingProduct = ParameterUtil.getString(map, "tradingProduct", "");// 交易品种
			String repoRate = ParameterUtil.getString(map, "repoRate", "");// 回购利率
			if ("".equals(tradingProduct) || "".equals(repoRate)) {
				return rateCode;
			}

			// String tradingProduct = map.get("tradingProduct");// 交易品种
			if ("R001".equals(tradingProduct)) {
				rateCode = "FR001";
			} else if ("R007".equals(tradingProduct)) {
				rateCode = "FR007";
			} else {
				rateCode = "FR014";
			}
			mapIntRate.put("rateCode", rateCode);
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
			mapIntRate.put("effDate", effDate);
			strRate = acupServer.queryRhisIntrate(mapIntRate);
			fontRate = map.get("repoRate");// 前台回购利率
		}
		if ("442".equals(prdNo)) {
			// 是买断式回购
			String tradingProduct = ParameterUtil.getString(map, "tradingProduct", "");// 交易品种
			String repoRate = ParameterUtil.getString(map, "repoRate", "");// 回购利率
			if ("".equals(tradingProduct) || "".equals(repoRate)) {
				return rateCode;
			}

			// String tradingProduct = map.get("tradingProduct");// 交易品种
			if ("OR001".equals(tradingProduct)) {
				rateCode = "FR001";
			} else if ("OR007".equals(tradingProduct)) {
				rateCode = "FR007";
			} else {
				rateCode = "FR014";
			}
			mapIntRate.put("rateCode", rateCode);
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
			mapIntRate.put("effDate", effDate);
			strRate = acupServer.queryRhisIntrate(mapIntRate);
			fontRate = map.get("repoRate");// 前台回购利率
		}
		if ("411".equals(prdNo)) {
			// 人民币即期
			String currencyPair = ParameterUtil.getString(map, "currencyPair", "");// 货币对
			String price = ParameterUtil.getString(map, "price", "");// 成交利率
			if ("".equals(currencyPair) || "".equals(price)) {
				return rateCode;
			}

			// String currencyPair = map.get("currencyPair");// 货币对
			String currency = currencyPair.substring(0, 3);
			String currency2 = currencyPair.substring(4, 7);
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
			BigDecimal rate = new BigDecimal(1);
			if (!"CNY".equals(currency)) {
				Map<String, String> mapRate = new HashMap<String, String>();
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				mapRate.put("effDate", effDate);
				rate = acupServer.queryRate(mapRate);
				if (rate == null || "".equals(rate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
			}
			BigDecimal rate2 = new BigDecimal(1);
			if (!"CNY".equals(currency2)) {
				Map<String, String> mapRate = new HashMap<String, String>();
				mapRate.put("ccy", currency2);
				mapRate.put("br", "01");// 机构号，送01
				mapRate.put("effDate", effDate);
				rate2 = acupServer.queryRate(mapRate);
				if (rate2 == null || "".equals(rate2)) {
					JY.raise("货币" + currency2 + "不存在汇率");
				}
			}
			strRate = rate.divide(rate2, 8, RoundingMode.HALF_UP);
			/*
			 * mapIntRate.put("effDate", effDate);
			 * 
			 * //获取上一日汇率中间价 strRate = acupServer.queryRhisIntrate(mapIntRate);
			 */
			fontRate = map.get("price");// 成交利率
		}
		if ("438".equals(prdNo)) {
			// 外币拆借
			rateCode = ParameterUtil.getString(map, "rateCode", "");// 利率代码
			String price = ParameterUtil.getString(map, "price", "");// 前台页面拆借利率
			if ("".equals(rateCode) || "".equals(price)) {
				return "";
			}

			rateCode = map.get("rateCode");// 利率代码
			mapIntRate.put("rateCode", rateCode);
			String effDate = ifsOpicsSettleManageMapper.getCurDate();
			mapIntRate.put("effDate", effDate);
			effDate = nDaysAfterOneDateString(effDate, -1);// 获取上一日日期
			strRate = acupServer.queryRhisIntrate(mapIntRate);
			fontRate = map.get("rate");// 前台页面拆借利率
		}
		if (strRate != null && !"".equals(strRate)) {
			intRate = Double.parseDouble(strRate + "");
			if (intRate == 0) {
				return result;
			}
			double iboRate = Double.parseDouble(fontRate);
			double resultd = (iboRate - intRate) / intRate;
			result = df.format(resultd) + "";
		}
		return result;
	}

	// 获取止损，久期，DV01
	@Override
	public SlCommonBean queryMarketData(Map<String, Object> map) throws RemoteConnectFailureException, Exception {

		String code = ParameterUtil.getString(map, "code", "");
		if ("A".equals(code)) {
			code = "SEN_AFS";
		} else if ("T".equals(code)) {
			code = "SEN_TB";
		}
		map.put("code", code);
		SlCommonBean slCommonBean = iBaseServer.queryMarketData(map);

		return slCommonBean;
	}

	// 获取外汇止损限额
	@Override
	public String queryFxdSum(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		String oprTime = ifsOpicsSettleManageMapper.getCurDate().substring(0, 4);
		map.put("year", oprTime);
		BigDecimal FxdSum = acupServer.queryFxdSum(map);
		return FxdSum + "";
	}

}
