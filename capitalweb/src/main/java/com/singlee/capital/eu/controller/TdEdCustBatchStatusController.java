package com.singlee.capital.eu.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.service.TdEdCustBatchStatusService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value="/edCustBatchStatusController")
public class TdEdCustBatchStatusController extends CommonController{
	@Autowired
	private TdEdCustBatchStatusService  edCustBatchStatusService;
	
	@ResponseBody
	@RequestMapping(value = "/getTdCustBatchStatusPage")
	public RetMsg<PageInfo<TdEdCustBatchStatus>> getTdCustBatchStatusPage(@RequestBody Map<String,Object> map) {
		Page<TdEdCustBatchStatus> list = edCustBatchStatusService.selectEdCustBatchForDealNoOrPrdNo(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/selectEdCustForDuration")
	public RetMsg<PageInfo<TdEdCustBatchStatus>> selectEdCustForDuration(@RequestBody Map<String,Object> map) {
		Page<TdEdCustBatchStatus> list = edCustBatchStatusService.selectEdCustForDuration(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/selectEdCustLogForDealNoOrPrdNo")
	public RetMsg<PageInfo<TdEdCustBatchStatus>> selectEdCustLogForDealNoOrPrdNo(@RequestBody Map<String,Object> map) {
		Page<TdEdCustBatchStatus> list = edCustBatchStatusService.selectEdCustLogForDealNoOrPrdNo(map);
		return RetMsgHelper.ok(list);
	}
	
}
