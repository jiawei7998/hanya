package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * 额度产品匹配交易产品
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TC_ED_PROD")
public class TcEdProd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 产品代码
	 */
	private String productCode;
	/**
	 * 额度中类ID
	 */
	private String creditId;
	/**
	 * 额度中类优先级
	 */
	private int propertyL;
	
	/**
	 * 业务类型名称
	 */
	@Transient
	private String prdName;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public int getPropertyL() {
		return propertyL;
	}
	public void setPropertyL(int propertyL) {
		this.propertyL = propertyL;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
	
}
