package com.singlee.capital.eu.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.capital.eu.model.ProductWeight;
import com.singlee.capital.eu.model.TdEdCust;
import com.singlee.capital.eu.service.TdEdCustService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.mapper.TdCustEcifMapper;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.ifs.service.IfsLimitService;
@Controller
@RequestMapping(value="/edCustController")
public class TdEdCustController extends CommonController {
	
	@Autowired
	private TdEdCustService tdEdCustService;
	@Autowired
	private TdCustEcifMapper custEcifMapper;
	
	@Autowired
	IfsLimitService ifsLimitService;
	
	/**
	 * 查询获得额度总信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdEdCustsPage")
	public RetMsg<PageInfo<TdEdCust>> getTdEdCustsPage(@RequestBody Map<String,Object> map) {
		map.put("expDate", map.get("expDate").toString().replace("-", ""));
		Page<TdEdCust> list = tdEdCustService.getTdEdCustsPage(map);
		return RetMsgHelper.ok(list);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getTdEdCustDetailPage")
	public RetMsg<PageInfo<TdEdCust>> getTdEdCustDetailPage(@RequestBody Map<String,Object> map) {
		Page<TdEdCust> list = tdEdCustService.getTdEdCustDetailPage(map);
		return RetMsgHelper.ok(list);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getEdCustForHandlePage")
	public RetMsg<PageInfo<TdEdCust>> getEdCustForHandlePage(@RequestBody Map<String,Object> map) {
		Page<TdEdCust> list = tdEdCustService.getEdCustForHandlePage(map);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 额度查询界面
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getEdCustForQueryPage")
	public RetMsg<PageInfo<TdEdCust>> getEdCustForQueryPage(@RequestBody Map<String, Object> map) {
		Page<TdEdCust> list = tdEdCustService.getEdCustForQueryPage(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getEdCustForQueryDetailPage")
	public RetMsg<PageInfo<TdEdCust>> getEdCustForQueryDetailPage(@RequestBody Map<String, Object> map) {
		Page<TdEdCust> list = tdEdCustService.getEdCustForQueryDetailPage(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getEdCustRiskListByEcifNum")
	public RetMsg<List<TdEdCust>> getEdCustRiskListByEcifNum(@RequestBody Map<String, Object> map) {
		String ecifNum=map.get("ecifNum").toString();
		List<TdEdCust> list = tdEdCustService.getEdCustRiskListByEcifNum(ecifNum);
		return RetMsgHelper.ok(list);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/edCustSplitSave")
	public RetMsg<RetMsg<Object>> edCustSplitSave(@RequestBody Map<String, Object> map) {
		//通过授信的主体自动纳入准入清单
		ifsLimitService.insertLimitToAccess(map); 
		RetMsg<Object> retMsg=tdEdCustService.edCustSplitSave(map);
		return RetMsgHelper.ok(retMsg);
	}
	
	
	
	/***
	 * 获取客户总的授信额度及总的可用额度
	 * @param map
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value="/getEdCustTotalAndAvlPage")
	public RetMsg<PageInfo<List<Map<String,Object>>>> getEdCustTotalAndAvlPage(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tdEdCustService.getEdCustTotalAndAvlPage(map));
	}
	
	/***
	 * 根据客户编号获取分页客户额度信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getPageEdCustByEcifNum")
	public RetMsg<PageInfo<TdEdCust>> getPageEdCustByEcifNum(@RequestBody Map<String, Object> map) {
		
		Page<TdEdCust> page = tdEdCustService.getPageEdCustByEcifNum(map);
		return RetMsgHelper.ok(page);
	}
	
	/***
	 * 查询总授信额度最大的客户额度信息 
	 */
	@ResponseBody
	@RequestMapping(value = "/getMaxLoanAmt")
	RetMsg<Map<String, Object>> getMaxLoanAmt(Map<String, Object> map){
		Map<String, Object> data = tdEdCustService.getMaxLoanAmt(map);
		return RetMsgHelper.ok(data);
	}
	
	/**
	 * 产品风险权重规则保存
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/productWeightSave")
	public RetMsg<RetMsg<Object>> productWeightSave(@RequestBody Map<String, Object> map) {

		RetMsg<Object> retMsg=tdEdCustService.productWeightSave(map);
		return RetMsgHelper.ok(retMsg);
	}
	
	/**
	 * 风险管理保存
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/riskManagerSave")
	public RetMsg<Serializable> riskManagerSave(@RequestBody Map<String, Object> map) {
		tdEdCustService.riskManagerSave(map);
		return RetMsgHelper.ok();
	}
	
	//查询单笔交易详情
	@ResponseBody
	@RequestMapping(value = "/searchRiskManager")
	public RetMsg<PageInfo<OpicsRisk>> searchRiskManager(@RequestBody Map<String,Object> map){
		Page<OpicsRisk> page = tdEdCustService.searchRiskManager(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response){
		String ua = request.getHeader("User-Agent");
		String filename = "风险管理.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			
			ExcelUtil e = downloadExcel();
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
		
	};
	
	/**
	 * Excel文件下载内容
	 */
	public ExcelUtil downloadExcel() throws Exception {
		OpicsRisk bean = new OpicsRisk();
		List<OpicsRisk> acupList = custEcifMapper.searchRiskManagerEXP();
		
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			
			int sheet = 0;
			int row = 0;
			
			// 表格sheet名称
			e.getWb().createSheet("风险管理");
			
			// 设置表头字段名
			e.writeStr(sheet,row, "A",center, "产品编号");     
			e.writeStr(sheet,row, "B",center, "产品名称"); 
			e.writeStr(sheet,row, "C",center, "校验准入"); 
			e.writeStr(sheet,row, "D",center, "校验授信");       
			e.writeStr(sheet,row, "E",center, "校验单笔风险");   
			e.writeStr(sheet,row, "F", center,"操作员");   
			e.writeStr(sheet,row, "G",center, "保存时间");   
			
			//设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			
			//设置默认行高
//			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			
			for (int i = 0; i < acupList.size(); i++) {
				bean = acupList.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getProductCode());
				e.writeStr(sheet, row, "B", center, bean.getProductName());
				e.writeStr(sheet, row, "C", center, bean.getCheckAccess());
				e.writeStr(sheet, row, "D", center, bean.getCheckLimit());
				e.writeStr(sheet, row, "E", center, bean.getCheckRisk());
				e.writeStr(sheet, row, "F", center, bean.getOperator());
				e.writeStr(sheet, row, "G", center, bean.getDateTime());
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}
	
}
