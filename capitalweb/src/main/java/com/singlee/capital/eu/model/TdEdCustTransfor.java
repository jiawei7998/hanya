package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 额度带入参数
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_ED_CUST_TRANSFOR")
public class TdEdCustTransfor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 业务流水号
	 */
	private String dealNo;
	/**业务类型  1-预申请 2-正式申请
	 * 
	 */
	private String dealType;
	/**
	 * 额度判别-业务起息日
	 */
	private String vDate;
	/**
	 * 额度判别-业务到期日
	 */
	private String mDate;
	/**
	 * 业务品种
	 */
	private String prdNo;
	/**
	 * 参与方类型
	 */
	private String partyId;
	/**
	 * 业务需占用额度金额
	 */
	private double amt;
	
	/**
	 * 客户编号
	 */
	private String ecifNo;
	
	private String descr;
	
	private String ecifName;
	
	public String getEcifName() {
		return ecifName;
	}
	public void setEcifName(String ecifName) {
		this.ecifName = ecifName;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	/**
	 * 产品代码
	 */
	private String productCode;
	@Transient
	private boolean flag;//是否额度占用成功
	@Transient
	private double resAmt;//剩余额度金额
	
	
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public double getResAmt() {
		return resAmt;
	}
	public void setResAmt(double resAmt) {
		this.resAmt = resAmt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getEcifNo() {
		return ecifNo;
	}
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	
}
