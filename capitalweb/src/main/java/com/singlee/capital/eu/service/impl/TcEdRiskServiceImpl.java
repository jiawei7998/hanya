package com.singlee.capital.eu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcRproductCreditMapper;
import com.singlee.capital.eu.mapper.TcEdRiskMapper;
import com.singlee.capital.eu.model.TcEdRisk;
import com.singlee.capital.eu.service.TcEdRiskService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcEdRiskServiceImpl implements TcEdRiskService {
	@Autowired
	TcEdRiskMapper edRiskMapper;
	
	@Autowired
	TcRproductCreditMapper tcRproductCreditMapper;
	
	@Override
	public Page<TcEdRisk> getTcEdRisksPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		
		return edRiskMapper.getTcEdRisksPage(map, ParameterUtil.getRowBounds(map));
	}
	@Override
	public void addEdRisk(TcEdRisk tcEdRisk) {
		// TODO Auto-generated method stub
		TcEdRisk edRisk = edRiskMapper.selectByPrimaryKey(tcEdRisk.getCreditId());
		if(edRisk != null) {
			JY.raiseRException("额度品种已存在");
		}
		edRiskMapper.insert(tcEdRisk);
	}
	@Override
	public void updateEdRisk(TcEdRisk tcEdRisk) {
		// TODO Auto-generated method stub
		TcEdRisk edRisk = edRiskMapper.selectByPrimaryKey(tcEdRisk.getCreditId());
		if(edRisk == null) {
			JY.raiseRException("额度品种不存在");
		}
		edRiskMapper.updateByPrimaryKey(tcEdRisk);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcEdRisk> deleteTcEdRisk(Map<String, Object> map) throws RException {
		String creditId = ParameterUtil.getString(map, "creditId", "");
		TcEdRisk tcRiskCredit = edRiskMapper.selectByPrimaryKey(creditId);
		RetMsg<TcEdRisk> retMsg = new RetMsg<TcEdRisk>(RetMsgHelper.codeOk, "额度品种风险级序删除成功", "", tcRiskCredit);
		if(tcRiskCredit == null) {
			retMsg.setCode("000300");
			retMsg.setDesc("风险级序不存在");
		}
		edRiskMapper.deleteByPrimaryKey(creditId);
		//删除匹配关系
		tcRproductCreditMapper.deleteByCreditId(creditId);
		return retMsg;
	}
	@Override
	public List<TcEdRisk> getChooseEdRisks(Map<String, Object> map) {
		String creditIds = ParameterUtil.getString(map, "creditIds", "");
		String[] creditIdArray = creditIds.split(",");
		List<String> creditIdList = new ArrayList<String>();
		for(int i = 0; i < creditIdArray.length; i++) {
			creditIdList.add(creditIdArray[i]);
		}
		map.put("creditIdList", creditIdList);
		return edRiskMapper.getChooseEdRisks(map);
	}
}
