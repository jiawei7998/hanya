package com.singlee.capital.eu.controller;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.TdEdDealLogService;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/edCustBatchLogController")
public class TdEdCustBatchLogController extends CommonController {
	@Autowired
	private TdEdDealLogService edDealLogService;
	
	@Autowired
	TdEdCustBatchStatusMapper tdEdCustBatchStatusMapper;
	
	@ResponseBody
	@RequestMapping(value = "/getTdEdCustBatchLogPage")
	public RetMsg<PageInfo<TdEdDealLog>> getTdEdCustBatchLogPage(@RequestBody Map<String,Object> map) {
		Page<TdEdDealLog> list = edDealLogService.getTdEdDealLogPage(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTdEdDealLogsByDealNo")
	public RetMsg<PageInfo<TdEdDealLog>> getTdEdDealLogsByDealNo(@RequestBody Map<String,Object> map) {
		Page<TdEdDealLog> list = edDealLogService.getTdEdDealLogsByDealNo(map);
		return RetMsgHelper.ok(list);
	}
	
	
	/**
	 * 授信额度占用明细Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request,
			HttpServletResponse response) {
		String ua = request.getHeader("User-Agent");
		String batchTime = request.getParameter("batchTime").substring(0,10); //到期日期
		String filename = "授信额度占用信息.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*="
					+ encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			ExcelUtil e = downloadExcel(batchTime);
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}

	};

	/**
	 * 客户准入 Excel文件下载内容
	 */
	public ExcelUtil downloadExcel(String batchTime) throws Exception {
		TdEdCustBatchStatus bean = new TdEdCustBatchStatus();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("batchTime", batchTime);
		List<TdEdCustBatchStatus> list = tdEdCustBatchStatusMapper.selectEdCustLogForDealNoWithList(map); // 查数据
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA, 200);
			CellStyle center = e.getDefaultCenterStrCellStyle();

			int sheet = 0;
			int row = 0;

			// 表格sheet名称
			e.getWb().createSheet("客户准入表");

			// 设置表头字段名
			e.writeStr(sheet, row, "A", center, "额度编号");
			e.writeStr(sheet, row, "B", center, "客户名称");
			e.writeStr(sheet, row, "C", center, "客户授信额度(元)");
			e.writeStr(sheet, row, "D", center, "客户授信可用额度(元)");
			e.writeStr(sheet, row, "E", center, "额度编号");
			e.writeStr(sheet, row, "F", center, "额度品种");
			e.writeStr(sheet, row, "G", center, "授信品种额度(元)");
			e.writeStr(sheet, row, "H", center, "授信品种可用额度(元)");
			e.writeStr(sheet, row, "I", center, "审批单号");
			e.writeStr(sheet, row, "J", center, "起息日");
			e.writeStr(sheet, row, "K", center, "到期日");
			e.writeStr(sheet, row, "L", center, "使用金额SUM(元)");

			// 设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(11, 20 * 256);

			for (int i = 0; i < list.size(); i++) {
				bean = list.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getCreditId());
				e.writeStr(sheet, row, "B", center, bean.getCnName());
				DecimalFormat df = new DecimalFormat("#0.00");
				BigDecimal loanAmtEcif=new BigDecimal(bean.getLoanAmtEcif()); 
				e.writeStr(sheet, row, "C", center, df.format(loanAmtEcif)+"");
				BigDecimal avlAmtEcif=new BigDecimal(bean.getLoanAmtEcif()); 
				e.writeStr(sheet, row, "D", center, df.format(avlAmtEcif)+"");
				e.writeStr(sheet, row, "E", center, bean.getCreditId());
				e.writeStr(sheet, row, "F", center, bean.getCreditName());
				BigDecimal loanAmtCredit=new BigDecimal(bean.getLoanAmtCredit());
				e.writeStr(sheet, row, "G", center, df.format(loanAmtCredit)+"");
				BigDecimal avlAmtCredit=new BigDecimal(bean.getAvlAmtCredit());
				e.writeStr(sheet, row, "H", center, df.format(avlAmtCredit)+"");
				e.writeStr(sheet, row, "I", center, bean.getDealNo());
				e.writeStr(sheet, row, "J", center, bean.getvDate());
				e.writeStr(sheet, row, "K", center, bean.getmDate());
				BigDecimal amt=new BigDecimal(bean.getAmt()); 
				e.writeStr(sheet, row, "L", center, df.format(amt)+"");
				//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				//String date = sdf.format(bean.getAPPROVAL_DATE());
				//e.writeStr(sheet, row, "F", center, date);
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}
}
