package com.singlee.capital.eu.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
import com.singlee.capital.eu.service.TcEdProdService;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/edProdController")
public class TcEdProdController extends CommonController{
	
	@Autowired
	private TcEdProdService tcRproductCreditService;
	
	/**
	 * 查询业务品种对应额度品种信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/tcRproductCreditList")
	public RetMsg<PageInfo<TcRproductCreditPojo>> tcRproductCreditList(@RequestBody Map<String, Object> map) {
		Page<TcRproductCreditPojo> result = tcRproductCreditService.pageTcRproductCreditPojo(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除业务品种对应额度品种配置
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteRproductCredit")
	public RetMsg<Serializable> deleteRproductCredit(@RequestBody Map<String, Object> map) {
		tcRproductCreditService.deleteRproductCredit(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增业务品种对应额度品种配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addRproductCredit")
	public RetMsg<Serializable> addRproductCredit(@RequestBody TcRproductCreditPojo tcRproductCreditPojo) {
		try {
			tcRproductCreditService.addRproductCredit(tcRproductCreditPojo);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple("NACK", "关系已存在");
		}
	}
	
	/**
	 * 更新业务品种对应额度品种配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateRproductCredit")
	public RetMsg<Serializable> updateRproductCredit(@RequestBody TcRproductCreditPojo tcRproductCreditPojo) {
		try {
			tcRproductCreditService.updateRproductCredit(tcRproductCreditPojo);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple("NACK", "关系已存在");
		}
	}
}
