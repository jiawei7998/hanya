package com.singlee.capital.eu.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
import com.singlee.capital.eu.mapper.TcEdProdMapper;
import com.singlee.capital.eu.model.TcEdProd;
import com.singlee.capital.eu.service.TcEdProdService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcEdProdServiceImpl implements TcEdProdService{
	@Autowired
	private TcEdProdMapper edProdMapper;

	@Override
	public Page<TcRproductCreditPojo> pageTcRproductCreditPojo(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TcRproductCreditPojo> page = edProdMapper.pageRproductCreditList(map, rb);
		return sortPage(page);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteRproductCredit(Map<String, Object> map) {
		String productCode = ParameterUtil.getString(map, "productCode", "");
		edProdMapper.deleteByProductCode(productCode);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) throws RException {
		insertRproductCredit(tcRproductCreditPojo);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) throws RException {
		// 先删除产品代码下的所有关系
		edProdMapper.deleteByProductCode(tcRproductCreditPojo.getProductCode());
		// 将新的关系添加
		insertRproductCredit(tcRproductCreditPojo);
	}

	private void insertRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) {
		String[] propertyLArray = tcRproductCreditPojo.getPropertyLs().split(",");
		String[] creditIdArray = tcRproductCreditPojo.getCreditIds().split(",");
		List<String> creditIdList = new ArrayList<String>();
		creditIdList = Arrays.asList(creditIdArray);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("productCode", tcRproductCreditPojo.getProductCode());
		map.put("creditIdList", creditIdList);
		if(!edProdMapper.findByProductCodeAndCreditIdList(map).isEmpty()) {
			JY.raiseRException("关系已存在");
		}
		TcEdProd rproductCredit = new TcEdProd();
		for (int i = 0; i < creditIdArray.length; i++) {
			rproductCredit.setProductCode(tcRproductCreditPojo.getProductCode());
			rproductCredit.setCreditId(creditIdArray[i]);
			int level = Integer.parseInt(propertyLArray[i]);
			rproductCredit.setPropertyL(level);
			edProdMapper.insert(rproductCredit);
		}
	}
	
	/**
	 * 按照优先级进行排序
	 * @param page
	 * @return
	 */
	private Page<TcRproductCreditPojo> sortPage(Page<TcRproductCreditPojo> page) {
		for(int i = 0; i < page.size(); i++) {
			TcRproductCreditPojo product = page.get(i);
			String propertyLs = product.getPropertyLs();
			String[] propertyArray = propertyLs.split(",");
			String[] creditIdArray = product.getCreditIds().split(",");
			String[] creditNameArray = product.getCreditNames().split(",");
			String propertyTemp;
			String creditIdTemp;
			String creditNameTemp;
			for(int j = 1; j < propertyArray.length; j++) {
				for(int k = 0; k < propertyArray.length - j; k++) {
					if(Integer.parseInt(propertyArray[k]) > Integer.parseInt(propertyArray[k + 1])) {
						propertyTemp = propertyArray[k];
						propertyArray[k] = propertyArray[k + 1];
						propertyArray[k + 1] = propertyTemp;
						
						creditIdTemp = creditIdArray[k];
						creditIdArray[k] = creditIdArray[k + 1];
						creditIdArray[k + 1] = creditIdTemp;
						
						creditNameTemp = creditNameArray[k];
						creditNameArray[k] = creditNameArray[k + 1];
						creditNameArray[k + 1] = creditNameTemp;
					}
				}
			}
			StringBuilder creditIdSb = new StringBuilder(creditIdArray.length * 2);
			StringBuilder creditNameSb = new StringBuilder(creditNameArray.length * 2);
			StringBuilder propertySb = new StringBuilder(propertyArray.length * 2);
			int maxLength = creditIdArray.length - 1;
			for(int j = 0; j < maxLength; j++) {
				creditIdSb.append(creditIdArray[j]).append(",");
				creditNameSb.append(creditNameArray[j]).append(",");
				propertySb.append(propertyArray[j]).append(",");
			}
			creditIdSb.append(creditIdArray[maxLength]);
			creditNameSb.append(creditNameArray[maxLength]);
			propertySb.append(propertyArray[maxLength]);
			product.setCreditIds(creditIdSb.toString());
			product.setCreditNames(creditNameSb.toString());
			product.setPropertyLs(propertySb.toString());
			page.set(i, product);
		}
		return page;
	}
}
