package com.singlee.capital.eu.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "IFS_PROTECT_WEIGHT")
public class ProductWeight implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 交易编号
	 */
	private Long productWeightId;
	/**
	 * 产品编号
	 */
	private String productCode;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 规则最小值
	 */
	private String ruleMin;
	/**
	 * 是否包含最小值
	 */
	private String containsMin;
	/**
	 * 规则最大值
	 */
	private String ruleMax;
	/**
	 * 是否包含最大值
	 */
	private String containsMax;
	/**
	 * 到期日
	 */
	private String dueDate;
	/**
	 * 权重值
	 */
	private String weight;
	/**
	 * 经办员
	 */
	private String operator;
	/**
	 * 录入时间
	 */
	private String dateTime;
	/**
	 * 组织机构号
	 */
	private String branchId;
	/**
	 * 交易日期
	 */
	@Transient
	private String curDate;
	
	@Transient
	private String weightOne;
	
	@Transient
	private String weightTwo;
	
	@Transient
	private String weightThree;
	
	
	public Long getProductWeightId() {
		return productWeightId;
	}
	public void setProductWeightId(Long productWeightId) {
		this.productWeightId = productWeightId;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getCurDate() {
		return curDate;
	}
	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getRuleMin() {
		return ruleMin;
	}
	public void setRuleMin(String ruleMin) {
		this.ruleMin = ruleMin;
	}
	public String getContainsMin() {
		return containsMin;
	}
	public void setContainsMin(String containsMin) {
		this.containsMin = containsMin;
	}
	public String getRuleMax() {
		return ruleMax;
	}
	public void setRuleMax(String ruleMax) {
		this.ruleMax = ruleMax;
	}
	public String getContainsMax() {
		return containsMax;
	}
	public void setContainsMax(String containsMax) {
		this.containsMax = containsMax;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getWeightOne() {
		return weightOne;
	}
	public void setWeightOne(String weightOne) {
		this.weightOne = weightOne;
	}
	public String getWeightTwo() {
		return weightTwo;
	}
	public void setWeightTwo(String weightTwo) {
		this.weightTwo = weightTwo;
	}
	public String getWeightThree() {
		return weightThree;
	}
	public void setWeightThree(String weightThree) {
		this.weightThree = weightThree;
	}
	
}
