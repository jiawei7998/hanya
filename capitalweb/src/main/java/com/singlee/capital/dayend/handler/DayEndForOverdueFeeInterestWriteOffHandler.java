package com.singlee.capital.dayend.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
import com.singlee.capital.trade.acc.service.AccForTradeAccessService;
@Service("dayEndForOverdueFeeInterestWriteOffHandler")
public class DayEndForOverdueFeeInterestWriteOffHandler extends CommonDayendFlowHandler {
	@Autowired
	AccForTradeAccessService accessService;
	@Override
	public void execute(SlSession session){
		accessService.getNeedFeeWriteOffDatesForOverdueStepOne(null);
		
	}
}
