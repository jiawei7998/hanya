package com.singlee.capital.dayend.handler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.mapper.TtT24FloatRateMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRq;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRs;
import com.singlee.capital.interfacex.model.TtT24FloatRate;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
@Service("dayEndForFloatingRateUpdateHandler")
public class DayEndForFloatingRateUpdateHandler extends CommonDayendFlowHandler{
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private  DictionaryGetService dictionaryGetService;
	@Autowired
	private TtT24FloatRateMapper floatRateMapper;
	@Autowired
	DayendDateService dateService;
	@Override
	public void execute(SlSession session){
		List<TaDictVo> list=dictionaryGetService.getTaDictByCode("rateCode");
		for (TaDictVo taDictVo : list) {
			TCoLoansIntInqRq request = new TCoLoansIntInqRq();
			request.setInterestKey(taDictVo.getDict_key().trim());
			request.setCurrency("CNY");
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			request.setCommonRqHdr(hdr);
			TCoLoansIntInqRs model = null;
			try {
				model = socketClientService.t24TCoLoansIntInqRequest(request);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				//throw new RException(ParameterUtil.getString(null, "rateCode", "")+"利率异常，查无信息");
				LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
			}
			if(model!= null && model.getTCoLoansIntInqRec().size() >0){
				TtT24FloatRate t24FloatRate=new TtT24FloatRate();
				t24FloatRate.setRateCode(taDictVo.getDict_key());
				t24FloatRate.setInputDate(dateService.getTtDayendDate().getCurDate());
				TtT24FloatRate floatRate=floatRateMapper.getFloatRateByRateCode(t24FloatRate);//查看是否存在记录
				if(model.getTCoLoansIntInqRec().get(0).getLoanRate()!=null && model.getTCoLoansIntInqRec().get(0).getCurrentRate()!=null){
					t24FloatRate.setCurrentRate(Double.parseDouble(model.getTCoLoansIntInqRec().get(0).getCurrentRate()));
					t24FloatRate.setLoanRate(Double.parseDouble(model.getTCoLoansIntInqRec().get(0).getLoanRate()));
					t24FloatRate.setPayPeriod(model.getTCoLoansIntInqRec().get(0).getPayPeriod()!=null?model.getTCoLoansIntInqRec().get(0).getPayPeriod():"");
					t24FloatRate.setInputTime(new SimpleDateFormat("HHmmss").format(new Date()));
					if(floatRate==null){
						floatRateMapper.insert(t24FloatRate);
					}else{
						floatRateMapper.updateFloatrateByRateCode(t24FloatRate);
					}
				}
			}else{//没有获取到，取最新的浮动代码作为利率
				TtT24FloatRate t24FloatRate=new TtT24FloatRate();
				t24FloatRate.setRateCode(taDictVo.getDict_key());
				t24FloatRate.setInputDate(dateService.getTtDayendDate().getCurDate());//系统日期
				TtT24FloatRate floatRate=floatRateMapper.getFloatRateByRateCode(t24FloatRate);//查看是否存在
				if(floatRate==null){//如果不存在，取最新值
					floatRate=floatRateMapper.getMaxFloatRateByRateCode(t24FloatRate);
					if(floatRate!=null){
						t24FloatRate.setCurrentRate(floatRate.getCurrentRate());
						t24FloatRate.setLoanRate(floatRate.getLoanRate());
						t24FloatRate.setPayPeriod(floatRate.getPayPeriod());
						t24FloatRate.setInputTime(new SimpleDateFormat("HHmmss").format(new Date()));
					}
				}
			}
		}
	}
}
