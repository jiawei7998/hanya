package com.singlee.capital.dayend.handler;

import org.springframework.stereotype.Service;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
import com.singlee.capital.interfacex.service.GtpFileService;
@Service("dayEndForGtpGlAcupFileCreateHandler")
public class DayEndForGtpGlAcupFileCreateHandler extends CommonDayendFlowHandler {

	@Override
	public void execute(SlSession session){
		/**
		 *  1、GTP配置IFBM与总账和VAT；
			2、文件命名格式：IFBMledgerYYYY_ppp.dat
		      (YYYY代表年；ppp该年度内每天数据文件对应的会计期间1月1日->001)；
		                通配符：IFBMledger*.dat
		 */
		GtpFileService gtpFileService = SpringContextHolder.getBean("gtpFileService");
		RetMsg<Object> retmsg = new RetMsg<Object>(RetMsgHelper.codeOk,"GTP总账成功！");
		System.out.println(retmsg.getCode());
		if(null == retmsg || !"error.common.0000".equalsIgnoreCase(retmsg.getCode())){
			throw new RException("总账文件异常："+retmsg.toJsonString());
		}
		try {
			retmsg = gtpFileService.GtpGLOutputDataWrite();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
			JY.debug(e.getMessage());
			throw new RException(e);
		}
		LogManager.getLogger(LogManager.MODEL_BATCH).info(retmsg.getCode());
	}
}
