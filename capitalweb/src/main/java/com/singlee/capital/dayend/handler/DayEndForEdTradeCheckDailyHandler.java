package com.singlee.capital.dayend.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
@Service("dayEndForEdTradeCheckDailyHandler")
public class DayEndForEdTradeCheckDailyHandler extends CommonDayendFlowHandler {
	@Autowired
	DayendDateService dateService;
	@Override
	public void execute(SlSession session){
		dateService.getDayendDate();//当前业务日
		
	}
}
