package com.singlee.capital.dayend.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.bookkeeping.service.TbkManualEntryService;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
@Service("dayEndForYearendEntryHandler")
public class DayEndForYearendEntryHandler extends CommonDayendFlowHandler {
	@Autowired
	TbkManualEntryService tbkManualEntryService;
	@Override
	public void execute(SlSession session){
		tbkManualEntryService.createYearEntry2();
	}
}
