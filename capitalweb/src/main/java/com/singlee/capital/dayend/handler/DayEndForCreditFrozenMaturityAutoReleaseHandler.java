package com.singlee.capital.dayend.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.credit.service.CreditFrozenService;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
@Service("dayEndForCreditFrozenMaturityAutoReleaseHandler")
public class DayEndForCreditFrozenMaturityAutoReleaseHandler extends CommonDayendFlowHandler {
	@Autowired
	CreditFrozenService creditFrozenService;
	@Override
	public void execute(SlSession session){
		/**
		 * 额度释放
		 * select T.*
		FROM TC_CUST_CREDIT_FROZEN T
		where T.state = '1' AND T.Approve_Status != '18'
		and not exists(select * from TC_CUST_CREDIT_FROZEN x where x.old_deal_no=t.deal_no and x.Approve_Status in ('3','4','5','6'))
		*/
		creditFrozenService.getCreditFrozenListForMaturityAutoRelease(null);
	}
}
