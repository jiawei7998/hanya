package com.singlee.capital.dayend.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.cap.bookkeeping.service.TBkSubjectBalanceSumService;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.dayend.service.impl.CommonDayendFlowHandler;
@Service("dayEndForUpdateSubjBalanceHandler")
public class DayEndForUpdateSubjBalanceHandler extends CommonDayendFlowHandler {
	@Autowired
	TBkSubjectBalanceSumService tBkSubjectBalanceSumService;
	@Override
	public void execute(SlSession session){
		tBkSubjectBalanceSumService.updateBeforeBalance();
	}
}
