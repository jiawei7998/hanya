package com.singlee.capital.credit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcCustCreditAdjMapper;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;
import com.singlee.capital.credit.service.CreditAdjService;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditAdjServiceImpl implements CreditAdjService {
	
	@Autowired
	private TcCustCreditAdjMapper custCreditAdjMapper;

	@Autowired
	private TrdOrderMapper approveManageDao;
	
	@Autowired
	private InstitutionService institutionService;

	@Autowired
	private TtInstitutionMapper institutionMapper;

	
	@Override
	public void addCustCreditAdj(Map<String,Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		String trdtype = DictConstants.CREDIT_TYPE.ADJU;
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("trdtype", trdtype);
		String order_id = approveManageDao.getOrderId(hashMap);
		map.put("dealNo", order_id);
		/*map.put("inputDate", DateUtil.getCurrentDateAsString());*/
		map.put("inputTime", DateUtil.getCurrentTimeAsString());
		map.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		custCreditAdjMapper.createCustCreditAdj(map);
	}

	/**
	 * 得到额度调整发起列表
	 */
	@Override
	public Page<CustCreditAdjBean> getCreditAdjList(Map<String, Object> map) {
		HashMap<String, Object> instParams =new HashMap<String, Object>();
		instParams.put("instId", SlSessionHelper.getInstitutionId());
		TtInstitution ttinstitu =new TtInstitution();
		ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
		// 根据机构主键获取机构信息
		/*ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);*/
		//start  修改bug:查询机构要根据 instId 与 branchId
		HashMap<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("instId", SlSessionHelper.getInstitutionId());
		instMap.put("branchId", map.get("branchId"));
		ttinstitu = institutionMapper.getInstByBranchId(instMap);
		//end
		
		
		
		
		if(!("0".equals(ttinstitu.getInstType()))){
			// 根据登录人机构ID查询他的子机构
			List <TtInstitution> tlist = institutionService.searchChildrenInst(instParams);
			List<String> ttlist=new ArrayList<String>();
			for (TtInstitution ttInstitution : tlist) {
				ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
			}
			map.put("tttlist", ttlist);
		}
		return custCreditAdjMapper.getCreditAdjList(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public Page<CustCreditAdjBean> getCreditAdjListFinish(Map<String, Object> map) {
		return custCreditAdjMapper.getCreditAdjListFinish(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public Page<CustCreditAdjBean> getCreditAdjListMine(Map<String, Object> map) {
		return custCreditAdjMapper.getCreditAdjListMine(map, ParameterUtil.getRowBounds(map));
	}
	
	

	@Override
	public void deleteCreditAdj(Map<String, Object> map) {
		custCreditAdjMapper.deleteByDealNo(map);
	}

	@Override
	public void updateCreditAdj(Map<String, Object> map) {
		map.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		custCreditAdjMapper.updateByDealNo(map);
	}

	@Override
	public Page<CustCreditAdjBean> getAdjDetailByCredit(Map<String, Object> map) {
		return custCreditAdjMapper.getAdjDetailByCredit(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public CustCreditAdjBean getCreditAdjByDealNo(String serial_no) {
		return custCreditAdjMapper.getCreditAdjByDealNo(serial_no);
	}

	@Override
	public void statusChange(Map<String,Object> status) {
		status.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		custCreditAdjMapper.statusChange(status);
	}

	@Override
	public List<CustCreditAdjBean> getAdjDetailByCreditAll(
			Map<String, Object> map) {
		return custCreditAdjMapper.getAdjDetailByCreditAll(map);
	}
	
}
