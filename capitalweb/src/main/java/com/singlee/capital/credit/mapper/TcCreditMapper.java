package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcCredit;
import com.singlee.capital.credit.pojo.CustCreditBean;

import tk.mybatis.mapper.common.Mapper;

public interface TcCreditMapper extends Mapper<TcCredit> {

	/**
	 * 分页查询额度品种列表
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TcCredit> pageTcCreditList(Map<String, Object> map, RowBounds rowBounds);

	Page<CustCreditBean> getCustCreditList(Map<String, Object> map, RowBounds rowBounds);
	
	/**
	 * 更新额度品种
	 * @param tcCredit
	 */
	public void updateCredit(TcCredit tcCredit);
	
	public Page<TcCredit> pageCredit(Map<String, Object> map, RowBounds rowBounds);
	
	public List<TcCredit> getChooseCredit(Map<String, Object> map);
	
	public void deleteTcCreditAll(String creditId);
}
