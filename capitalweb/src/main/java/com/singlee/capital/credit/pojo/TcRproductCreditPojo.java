package com.singlee.capital.credit.pojo;

import java.io.Serializable;

public class TcRproductCreditPojo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private String creditIds;
	
	private String creditNames;
	
	private String propertyLs;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCreditIds() {
		return creditIds;
	}

	public void setCreditIds(String creditIds) {
		this.creditIds = creditIds;
	}

	public String getCreditNames() {
		return creditNames;
	}

	public void setCreditNames(String creditNames) {
		this.creditNames = creditNames;
	}

	public String getPropertyLs() {
		return propertyLs;
	}

	public void setPropertyLs(String propertyLs) {
		this.propertyLs = propertyLs;
	}
}
