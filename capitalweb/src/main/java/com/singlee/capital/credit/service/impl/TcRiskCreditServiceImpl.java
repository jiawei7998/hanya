package com.singlee.capital.credit.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcRiskCreditMapper;
import com.singlee.capital.credit.model.TcRiskCredit;
import com.singlee.capital.credit.service.TcRiskCreditService;

@Service
public class TcRiskCreditServiceImpl implements TcRiskCreditService {
	
	@Autowired
	private TcRiskCreditMapper tcRiskCreditMapper;

	@Override
	public Page<TcRiskCredit> pageTcRiskCreditList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return tcRiskCreditMapper.pageRiskCreditList(map, rb);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcRiskCredit> deleteTcRiskCredit(Map<String, Object> map) throws RException {
		String creditId = ParameterUtil.getString(map, "creditId", "");
		TcRiskCredit tcRiskCredit = tcRiskCreditMapper.selectByPrimaryKey(creditId);
		RetMsg<TcRiskCredit> retMsg = new RetMsg<TcRiskCredit>(RetMsgHelper.codeOk, "额度品种风险级序删除成功", "", tcRiskCredit);
		if(tcRiskCredit == null) {
			retMsg.setCode("000300");
			retMsg.setDesc("风险级序不存在");
		}
		tcRiskCreditMapper.deleteByPrimaryKey(creditId);
		return retMsg;
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcRiskCredit> addTcRiskCredit(TcRiskCredit tcRiskCredit) throws RException {
		TcRiskCredit riskCredit = tcRiskCreditMapper.selectByPrimaryKey(tcRiskCredit.getCreditId());
		RetMsg<TcRiskCredit> retMsg = new RetMsg<TcRiskCredit>(RetMsgHelper.codeOk, "额度品种风险级序新增成功", "", tcRiskCredit);
		if(riskCredit == null) {
			tcRiskCreditMapper.insert(tcRiskCredit);
		}else{
			retMsg.setCode("000100");
			retMsg.setDesc("风险级序已存在");
		}
		return retMsg;
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcRiskCredit> updateTcRiskCredit(TcRiskCredit tcRiskCredit) {
		TcRiskCredit riskCredit = tcRiskCreditMapper.selectByPrimaryKey(tcRiskCredit.getCreditId());
		RetMsg<TcRiskCredit> retMsg = new RetMsg<TcRiskCredit>(RetMsgHelper.codeOk, "额度品种风险级序修改成功", "", tcRiskCredit);
		if(riskCredit == null) {
			retMsg.setCode("000200");
			retMsg.setDesc("风险级序不存在");
		}else{
			tcRiskCreditMapper.updateTcRiskCredit(tcRiskCredit);
		}
		return retMsg;
	}
}
