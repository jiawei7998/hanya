package com.singlee.capital.credit.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.pojo.CustCreditSegBean;
import com.singlee.capital.credit.service.CreditSegService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;

@Controller
@RequestMapping(value = "/CustCreditSegController")
public class CustCreditSegController extends CommonController {

	@Autowired
	CreditSegService creditSegService;
	
	/**
	 * 新增一笔额度切分审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addCustCreditSeg")
	public RetMsg<Serializable> addCustCreditSeg(@RequestBody Map<String,Object> map) {
		creditSegService.addCustCreditSeg(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到发起的额度切分审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditSegList")
	public RetMsg<PageInfo<CustCreditSegBean>> getCreditSegList(@RequestBody Map<String,Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.New);
		Page<CustCreditSegBean> list = creditSegService.getCreditSegList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到发起的额度切分审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getSegDetailByCreditAll")
	public RetMsg<List<CustCreditSegBean>> getSegDetailByCreditAll(@RequestBody Map<String,Object> map) {
		List<CustCreditSegBean> seglist = creditSegService.getSegDetailByCreditAll(map);
		return RetMsgHelper.ok(seglist);
	}
	
	/**
	 * 得到待审批的额度切分审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditSegWaitApprove")
	public RetMsg<PageInfo<CustCreditSegBean>> getCreditSegWaitApprove(@RequestBody Map<String,Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.WaitApprove);
		map.put("approveStatus_Approving", DictConstants.ApproveStatus.Approving);
		Page<CustCreditSegBean> list = creditSegService.getCreditSegList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到审批完成的额度切分审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditSegApprovedPass")
	public RetMsg<PageInfo<CustCreditSegBean>> getCreditSegApprovedPass(@RequestBody Map<String,Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.ApprovedPass);
		Page<CustCreditSegBean> list = creditSegService.getCreditSegList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 删除额度切分审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCreditSeg")
	public RetMsg<Serializable> deleteCreditSeg(@RequestBody Map<String,Object> map) {
		creditSegService.deleteCreditSeg(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改额度切分审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditSeg")
	public RetMsg<Serializable> updateCreditSeg(@RequestBody Map<String,Object> map) {
		creditSegService.updateCreditSeg(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 拿到额度品种的切分详情
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSegDetailByCredit")
	public RetMsg<PageInfo<CustCreditSegBean>> getSegDetailByCredit(@RequestBody Map<String,Object> map) {
		Page<CustCreditSegBean> list = creditSegService.getSegDetailByCredit(map);
		return RetMsgHelper.ok(list);
	}
	
}
