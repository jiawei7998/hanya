package com.singlee.capital.credit.dict;
/**
 * 授信模块数据字典项
 * @author SINGLEE
 *
 */
public class DictConstants {

	
	//占用类型
	public final static class CREDIT_USE_TYPE{
		/***
		 * 占用
		 */
		public static final Integer USE = 1;
		
		/***
		 * 串用
		 */
		public static final Integer STRUSE = 2;
		
	}
	
	//占用状态
	public final static class CREDIT_USE_STATE{
		
		/***
		 * 预先占用
		 */
		public static final Integer PRE_USDED = 1;
		
		/***
		 * 占用
		 */
		public static final Integer USDED = 2;
		
		
		/***
		 * 释放
		 */
		public static final Integer RELEASE = 3;
	
		
	}
	
	//冻结状态
	public final static class CREDIT_FREEZE_STATE{
		
		/***
		 * 冻结
		 */
		public static final Integer FREEZE = 1;
		
		
		/***
		 * 解冻
		 */
		public static final Integer UNFREEZE = 2;
	
	}
	
	
	public final static class CREDIT_RECORD_STATE{
		
		/***
		 * 是历史记录
		 */
		public static final String HISTORY_Y = "Y";
		
		/***
		 * 不是历史记录
		 */
		public static final String HISTORY_N = "N";
		
	}
	
	//额度类型
	public final static class CREDIT_TYPE{
		
		/***
		 * 额度占用关系编号
		 */
		public static final String RNO = "CRNO";

		/***
		 * 额度切分
		 */
		public static final String SEGM = "SEGM";
		
		/***
		 * 额度调整
		 */
		public static final String ADJU = "ADJU";
		
		/***
		 * 额度冻结
		 */
		public static final String FROZ = "FROZ";
		/**
		 * 额度解冻
		 */
		public static final String UN_FROZ = "UFRZ";
		/***
		 * 额度占用
		 */
		public static final String USED = "USED";
		
		/***
		 * 额度占用日志
		 */
		public static final String CREDIT_LOG = "CLOG";
		
	}
	
	//额度操作类型
	public final static class CREDIT_OPER_TYPE{
		
		/***
		 * 占用
		 */
		public static final Integer USDED = 1;
		
		
		/***
		 * 释放
		 */
		public static final Integer RELEASE = 2;
		
		/***
		 * 冻结
		 */
		public static final Integer FREEZE = 3;
		
		/***
		 * 解冻
		 */
		public static final Integer UNFREEZE = 4;
		
		/***
		 * 切分
		 */
		public static final Integer ASEG = 5;
		
		/***
		 * 调整
		 */
		public static final Integer ADJ = 6;
		
		
		
	}
	
	//额度线上\线下
	public final static class CREDIT_OFFLINE{
		/***
		 * 线下
		 */
		public static final String YES = "Y";
		
		/***
		 * 线上
		 */
		public static final String NO = "N";
		
		
	}
	//放款类型
	public final static class LoanType{
		
		public static final String NORMAL_PAY = "01";//正常放款、
		public static final String ADJUST_PAY="02";//放款修改、
		public static final String REVERSE_PAY="03";//冲正
	}
	//通知类型
	public final static class InfoFlag{
		
		public static final String INTER_ACTIVE_SUCCESS = "01";//01-交互，
		public static final String INTER_ACTIVE_FAIL="02";//02-交互失败，
		public static final String VERIFY_SUCCESS="03";//03-复核通过，
		public static final String VERIFY_REJECT="04";//04-复核拒绝
	}
}
