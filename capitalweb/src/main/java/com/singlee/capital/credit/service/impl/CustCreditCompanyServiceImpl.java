package com.singlee.capital.credit.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.mapper.TcCustCreditCompanyMapper;
import com.singlee.capital.credit.pojo.CustCreditCompany;
import com.singlee.capital.credit.service.CustCreditCompanyService;

@Service
public class CustCreditCompanyServiceImpl implements CustCreditCompanyService {

	@Autowired
	TcCustCreditCompanyMapper custCreditCompanyMapper;
	
	@Override
	public Page<CustCreditCompany> getCustCeditCompanyList(
			Map<String, Object> map) {
		return custCreditCompanyMapper.getCustCeditCompanyList(map);
	}

	@Override
	public CustCreditCompany getCustCeditCompany(CustCreditCompany custCreditCompany) {
		return custCreditCompanyMapper.selectOne(custCreditCompany);
	}

	@Override
	public void insertCustCeditCompany(CustCreditCompany custCreditCompany) {
		custCreditCompanyMapper.insert(custCreditCompany);
	}

	@Override
	public void updateCustCreditCompany(CustCreditCompany custCreditCompany) {
		custCreditCompanyMapper.updateByPrimaryKey(custCreditCompany);
	}

}
