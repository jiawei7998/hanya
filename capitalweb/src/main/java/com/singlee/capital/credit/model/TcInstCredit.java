package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TC_INST_CREDIT")
public class TcInstCredit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_INST_CREDIT.NEXTVAL FROM DUAL")
	private String creditNo;
	// 客户编号
	private String instId;
	
	// 客户名称
	private String instName;
	
	// 扣减额度客户编号
	private String subInstId;
	
	// 扣减额度客户名称
	private String subInstName;
	
	// 备注
	private String comm;
	
	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getSubInstId() {
		return subInstId;
	}

	public void setSubInstId(String subInstId) {
		this.subInstId = subInstId;
	}

	public String getSubInstName() {
		return subInstName;
	}

	public void setSubInstName(String subInstName) {
		this.subInstName = subInstName;
	}

	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public String getCreditNo() {
		return creditNo;
	}

	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

}
