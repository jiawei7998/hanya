package com.singlee.capital.credit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcCustCreditFrozenMapper;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;
import com.singlee.capital.credit.service.CreditFrozenService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.capital.eu.model.TdEdCust;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditFrozenServiceImpl implements CreditFrozenService {

	@Autowired
	TcCustCreditFrozenMapper creditFrozenMapper;

	@Autowired
	private TrdOrderMapper approveManageDao;
	@Autowired
	private TdEdCustMapper edCustMapper;
	@Autowired
	private DayendDateService dateService;
	/**
	 * 新增冻结额度
	 * @param map
	 */
	@Override
	public void addCustCreditFrozen(Map<String, Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		String trdtype = "";
		int state = ParameterUtil.getInt(map, "state", 0);
		if(state == (DictConstants.CREDIT_FREEZE_STATE.UNFREEZE)){
			trdtype = DictConstants.CREDIT_TYPE.UN_FROZ;
		}else {
			trdtype = DictConstants.CREDIT_TYPE.FROZ;
			double frozenAmt = ParameterUtil.getDouble(map, "frozenAmt", 0.00);
			String custName = ParameterUtil.getString(map, "custName", "");
			TdEdCust edCust = edCustMapper.selectByPrimaryKey(ParameterUtil.getString(map, "custCreditId", ""));
			if(frozenAmt>0){
				if(frozenAmt+edCust.getAdjAmt()+edCust.getFrozenAmt()>edCust.getLoanAmt())
				{
					throw new RException(custName+" " + edCust.getTerm()+"<br/>总额度："+edCust.getLoanAmt()
							+"<br/>已调整："+edCust.getAdjAmt()
							+"<br/>已冻结："+edCust.getFrozenAmt()
							+"<br/>现冻结："+frozenAmt+" 之和超过总额度！");
				}
			}
		}
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("trdtype", trdtype);
		String order_id = approveManageDao.getOrderId(hashMap);
		map.put("dealNo", order_id);
		map.put("inputTime", DateUtil.getCurrentTimeAsString());
		map.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		
		creditFrozenMapper.addCustCreditFrozen(map);
	}

	/**
	 * 得到额度冻结审批列表
	 */
	@Override
	public Page<CustCreditFrozenBean> getCreditFrozenList(
			Map<String, Object> map) {
		return creditFrozenMapper.getCreditFrozenList(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public Page<CustCreditFrozenBean> getCreditFrozenListMine(
			Map<String, Object> map) {
		return creditFrozenMapper.getCreditFrozenListMine(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public Page<CustCreditFrozenBean> getCreditFrozenListFinish(
			Map<String, Object> map) {
		return creditFrozenMapper.getCreditFrozenListFinish(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public void deleteCreditFrozen(Map<String, Object> map) {
		creditFrozenMapper.deleteByDealNo(map);
	}

	@Override
	public void updateCreditFrozen(Map<String, Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		int state = ParameterUtil.getInt(map, "state", 0);
		if(state == (DictConstants.CREDIT_FREEZE_STATE.UNFREEZE)){
			
		}else {
			double frozenAmt = ParameterUtil.getDouble(map, "frozenAmt", 0.00);
			String custName = ParameterUtil.getString(map, "custName", "");
			TdEdCust edCust = edCustMapper.selectByPrimaryKey(ParameterUtil.getString(map, "custCreditId", ""));
			if(frozenAmt>0){
				if(frozenAmt+edCust.getAdjAmt()+edCust.getFrozenAmt()>edCust.getLoanAmt())
				{
					throw new RException(custName+" " + edCust.getTerm()+"<br/>总额度："+edCust.getLoanAmt()
							+"<br/>已调整："+edCust.getAdjAmt()
							+"<br/>已冻结："+edCust.getFrozenAmt()
							+"<br/>现冻结："+frozenAmt+" 之和超过总额度！");
				}
			}
		}
		map.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		creditFrozenMapper.updateByDealNo(map);
	}

	@Override
	public void statusChange(Map<String, Object> status) {
		status.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		creditFrozenMapper.statusChange(status);
	}

	@Override
	public CustCreditFrozenBean getCreditFrozenByDealNo(String serial_no) {
		return creditFrozenMapper.getCreditFrozenByDealNo(serial_no);
	}

	@Override
	public Page<CustCreditFrozenBean> getFrozenDetailByCredit(
			Map<String, Object> map) {
		return creditFrozenMapper.getFrozenDetailByCredit(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<CustCreditFrozenBean> getFrozenDetailByCreditAll(
			Map<String, Object> map) {
		return creditFrozenMapper.getFrozenDetailByCreditAll(map);
	}

	@Override
	public Page<CustCreditFrozenBean> getCreditFrozenListForDialog(
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		return creditFrozenMapper.getCreditFrozenListForDialog(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<CustCreditFrozenBean> getCreditFrozenListForDetail(
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		return creditFrozenMapper.getCreditFrozenListForDetail(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public void getCreditFrozenListForMaturityAutoRelease(
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		String postDate = dateService.getDayendDate();
		List<CustCreditFrozenBean> creditFrozenBeans = this.creditFrozenMapper.getCreditFrozenListForMaturityAutoRelease(map);
		for(CustCreditFrozenBean creditFrozenBean:creditFrozenBeans){
			//判断是否到期   creditFrozenBean.getMdate()>postDate 则continue
			if(DateUtil.daysBetween(postDate,  creditFrozenBean.getMdate())>0) {
				continue;}
			//判定目前 creditFrozenBean所处的状态  6 则处理 其他直接重置为18
			if(com.singlee.capital.system.dict.DictConstants.ApproveStatus.ApprovedPass.equalsIgnoreCase(String.valueOf(creditFrozenBean.getApproveStatus()))){
				TdEdCust edCust = edCustMapper.selectByPrimaryKey(creditFrozenBean.getCustCreditId());
				if(null != edCust){
					edCust.setAvlAmt(edCust.getAvlAmt()+
							(edCust.getFrozenAmt()-creditFrozenBean.getFrozenAmt().doubleValue()>=0?creditFrozenBean.getFrozenAmt().doubleValue():edCust.getFrozenAmt()));
					edCust.setFrozenAmt(edCust.getFrozenAmt()-creditFrozenBean.getFrozenAmt().doubleValue()>=0?edCust.getFrozenAmt()-creditFrozenBean.getFrozenAmt().doubleValue():0);
					edCustMapper.updateByPrimaryKey(edCust);
				}
			}
			creditFrozenBean.setApproveStatus(Integer.parseInt(com.singlee.capital.system.dict.DictConstants.ApproveStatus.ApprovedMaturityExp));
			creditFrozenMapper.updateByPrimaryKey(creditFrozenBean);
		}
	}

}
