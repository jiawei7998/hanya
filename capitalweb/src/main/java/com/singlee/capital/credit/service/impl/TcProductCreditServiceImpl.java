package com.singlee.capital.credit.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcProductCreditMapper;
import com.singlee.capital.credit.model.TcProductCredit;
import com.singlee.capital.credit.service.TcProductCreditService;

@Service
public class TcProductCreditServiceImpl implements TcProductCreditService {
	
	@Autowired
	TcProductCreditMapper tcProductCreditMapper;

	@Override
	public Page<TcProductCredit> pageCreditProduct(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		return tcProductCreditMapper.pageCreditProduct(map, rowBounds);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteCreditProduct(Map<String, Object> map) throws RException {
		String productCode = ParameterUtil.getString(map, "productCode", "");
		TcProductCredit credit = tcProductCreditMapper.selectByPrimaryKey(productCode);
		if(credit == null) {
			JY.raiseRException("产品不存在");
		}
		tcProductCreditMapper.deleteByPrimaryKey(productCode);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addProductCredit(TcProductCredit tcProductCredit) throws RException {
		TcProductCredit product = tcProductCreditMapper.selectByPrimaryKey(tcProductCredit);
		if (product != null) {
			JY.raiseRException("产品已存在");
		}
		tcProductCreditMapper.insert(tcProductCredit);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateProductCredit(TcProductCredit tcProductCredit) throws RException {
		TcProductCredit product = tcProductCreditMapper.selectByPrimaryKey(tcProductCredit);
		if (product == null) {
			JY.raiseRException("产品不存在");
		}
		tcProductCreditMapper.updateByPrimaryKey(tcProductCredit);
	}
}
