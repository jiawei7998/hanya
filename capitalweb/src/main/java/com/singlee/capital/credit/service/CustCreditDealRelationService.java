package com.singlee.capital.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditUseDetailVo;


public interface CustCreditDealRelationService {


	Page<CustCreditUseDetailVo> getUseDetailByCredit(Map<String, Object> map);

	List<CustCreditUseDetailVo> getUseDetailByCreditAll(Map<String, Object> map);

	
}
