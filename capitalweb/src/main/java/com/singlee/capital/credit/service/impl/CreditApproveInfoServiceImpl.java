package com.singlee.capital.credit.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcCreditApproveInfoMapper;
import com.singlee.capital.credit.model.TcCreditApproveInfo;
import com.singlee.capital.credit.service.CreditApproveInfoService;

@Service
public class CreditApproveInfoServiceImpl implements CreditApproveInfoService {

	@Autowired
	private TcCreditApproveInfoMapper tcCreditApproveInfoMapper;
	
	@Override
	public Page<TcCreditApproveInfo> getCreditApproveInfoList(
			Map<String, Object> map) {
		return tcCreditApproveInfoMapper.getCreditApproveInfoList(map, ParameterUtil.getRowBounds(map));
	}

}
