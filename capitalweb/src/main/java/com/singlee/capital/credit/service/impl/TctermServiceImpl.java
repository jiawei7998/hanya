package com.singlee.capital.credit.service.impl;

import java.text.DecimalFormat;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TctermMapper;
import com.singlee.capital.credit.model.Tcterm;
import com.singlee.capital.credit.service.TctermService;

@Service
public class TctermServiceImpl implements TctermService{
	
	private static DecimalFormat df = new DecimalFormat("#.0000");
	
	@Resource
	private TctermMapper tctermMapper;

	@Override
	public Page<Tcterm> selectTcterm(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return tctermMapper.selectTcterm(map, rb);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteCredit(Map<String, Object> map) throws RException {
		String termId = ParameterUtil.getString(map, "termId", "");
		Tcterm tcterm = tctermMapper.selectByPrimaryKey(termId);
		if(tcterm == null) {
			JY.raiseRException("期限配置不存在");
		}
		tctermMapper.deleteByPrimaryKey(termId);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addCredit(Tcterm tcterm) throws RException {
		Tcterm term = tctermMapper.selectByPrimaryKey(tcterm.getTermId());
		if(term != null) {
			JY.raiseRException("期限配置已存在");
		}
		tcterm.setMonths(getMonths(tcterm.getTermId()));
		tctermMapper.insert(tcterm);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateCredit(Tcterm tcterm) throws RException {
		Tcterm term = tctermMapper.selectByPrimaryKey(tcterm.getTermId());
		if(term == null) {
			JY.raiseRException("期限配置不存在");
		}
		tcterm.setMonths(getMonths(tcterm.getTermId()));
		tctermMapper.updateCredit(tcterm);
	}
	
	private String getMonths(String termId) {
		String months = "";
		if(termId.toLowerCase().indexOf("d") > -1) {
			months = df.format(Double.parseDouble(termId.substring(0, termId.length() - 1)) / 31);
		}
		if(termId.toLowerCase().indexOf("m") > -1) {
			months = df.format(Double.parseDouble(termId.substring(0, termId.length() - 1)));
		}
		if(termId.toLowerCase().indexOf("y") > -1) {
			if(termId.indexOf("+") == -1) {
				months = df.format(Double.parseDouble(termId.substring(0, termId.length() - 1)) * 12);
			} else {
				months = df.format(100 * 12);
			}
		}
		if(termId.toLowerCase().indexOf("w") > -1) {
			months = df.format(Double.parseDouble(termId.substring(0, termId.length() - 1)) * 7 / 31);
		}
		return months;
	}
}
