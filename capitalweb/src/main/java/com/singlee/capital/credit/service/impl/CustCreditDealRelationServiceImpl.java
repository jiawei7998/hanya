package com.singlee.capital.credit.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcCustCreditDealRelationMapper;
import com.singlee.capital.credit.pojo.CustCreditUseDetailVo;
import com.singlee.capital.credit.service.CustCreditDealRelationService;

@Service
public class CustCreditDealRelationServiceImpl implements
		CustCreditDealRelationService {

	@Autowired
	TcCustCreditDealRelationMapper ccdrm;
	
	/**
	 * 额度占用冻结详情
	 */
	@Override
	public Page<CustCreditUseDetailVo> getUseDetailByCredit(
			Map<String, Object> map) {
		Page<CustCreditUseDetailVo> list = ccdrm.getUseDetailByCredit(map, ParameterUtil.getRowBounds(map));
		return list;
	}

	@Override
	public List<CustCreditUseDetailVo> getUseDetailByCreditAll(
			Map<String, Object> map) {
		List<CustCreditUseDetailVo> list = ccdrm.getUseDetailByCreditAll(map);
		return list;
	}

	

}
