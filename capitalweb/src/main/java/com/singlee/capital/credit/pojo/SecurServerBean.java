package com.singlee.capital.credit.pojo;

import java.io.Serializable;

import javax.persistence.Table;


/*****
 * 授信额度调整
 * @author kf0738
 *
 */
@Table(name="IFS_SECUR_SERVER")
public class SecurServerBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	  //流水号
	  private String dealno;
	//原始本金
	  private String prinamt;
	//利息金额
	  private String purchintamt;
	//收益金额
	  private String proceedamt;
	//原始本金确认
	  private String prinamtComfire;
	//利息金额确认
	  private String purchintamtComfire;
	//收益金额确认
	  private String proceedamtComfire;
	//状态：1经办，2授权
	  private String status;
	//操作员
	  private String operator;
	  //操作时间
	  private String oprTime;
	//时候存在标志，0不存在，1存在
	  private String existFlag;
	//备用字段1
	  private String nouse1;
	//备用字段2
	  private String nouse2;
	//备用字段3
	  private String nouse3;
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getPrinamt() {
		return prinamt;
	}
	public void setPrinamt(String prinamt) {
		this.prinamt = prinamt;
	}
	public String getPurchintamt() {
		return purchintamt;
	}
	public void setPurchintamt(String purchintamt) {
		this.purchintamt = purchintamt;
	}
	public String getProceedamt() {
		return proceedamt;
	}
	public void setProceedamt(String proceedamt) {
		this.proceedamt = proceedamt;
	}

	public String getPrinamtComfire() {
		return prinamtComfire;
	}
	public void setPrinamtComfire(String prinamtComfire) {
		this.prinamtComfire = prinamtComfire;
	}
	public String getPurchintamtComfire() {
		return purchintamtComfire;
	}
	public void setPurchintamtComfire(String purchintamtComfire) {
		this.purchintamtComfire = purchintamtComfire;
	}
	public String getProceedamtComfire() {
		return proceedamtComfire;
	}
	public void setProceedamtComfire(String proceedamtComfire) {
		this.proceedamtComfire = proceedamtComfire;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getNouse1() {
		return nouse1;
	}
	public void setNouse1(String nouse1) {
		this.nouse1 = nouse1;
	}
	public String getNouse2() {
		return nouse2;
	}
	public void setNouse2(String nouse2) {
		this.nouse2 = nouse2;
	}
	public String getNouse3() {
		return nouse3;
	}
	public void setNouse3(String nouse3) {
		this.nouse3 = nouse3;
	}
	public String getOprTime() {
		return oprTime;
	}
	public void setOprTime(String oprTime) {
		this.oprTime = oprTime;
	}
	public String getExistFlag() {
		return existFlag;
	}
	public void setExistFlag(String existFlag) {
		this.existFlag = existFlag;
	}
	  
	  
}
