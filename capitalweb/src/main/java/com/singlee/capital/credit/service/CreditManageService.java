package com.singlee.capital.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;
import com.singlee.capital.credit.pojo.CustCreditBean;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;
import com.singlee.capital.credit.pojo.CustCreditSegBean;
import com.singlee.capital.credit.pojo.CustCreditUseBean;
import com.singlee.capital.credit.pojo.TcCustCreditDetailLog;
import com.singlee.capital.credit.pojo.TcCustCreditMainLog;
import com.singlee.capital.interfacex.model.ApprovalChangeRec;
import com.singlee.capital.interfacex.model.ApprovalInqRs;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;

/****
 * 
 * ifbm
 * @author kf0738
 *
 */
public interface CreditManageService {
	
	/****
	 * 额度切分
	 * @throws Exception
	 */
	public RetMsg<CustCreditSegBean> creditQuotaAseg(CustCreditSegBean bean)throws Exception;
	
	/****
	 * 额度调整
	 * @throws Exception
	 */
	public RetMsg<CustCreditAdjBean> creditQuotaAdj(CustCreditAdjBean bean)throws Exception;
	
	/****
	 * 额度冻结
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaFreeze(CustCreditFrozenBean bean)throws Exception;

	
	/****
	 * 额度解冻
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaUnFreeze(CustCreditFrozenBean bean)throws Exception;

	/****
	 * 额度预占用
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaPreUsed(CustCreditUseBean creditUseBean)throws Exception;
	
	/****
	 * 额度解除预占用
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaPreUsedRelease(CustCreditUseBean creditUseBean)throws Exception;
	
	/****
	 * 额度占用
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaUsed(CustCreditUseBean creditUseBean)throws Exception;
	
	/****
	 * 额度释放
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaRelease(CustCreditUseBean creditUseBean)throws Exception;

	/****
	 * map
	 * @param map
	 * @return
	 */
	public Page<CustCreditBean> getCustCreditList(Map<String, Object> map);
	
	public List<CustCreditBean> getCustCreditListAll(Map<String, Object> map);

	/**
	 * 按客户拿到他的各项金额汇总
	 * @param map
	 */
	public Page<CustCreditBean> getCreditListByCust(Map<String, Object> map);
	
	/**
	 * 按客户拿到他的各项金额汇总
	 * @param map
	 */
	public List<CustCreditBean> getCreditListByCustNo(Map<String, Object> map);
	
	public RetMsg<Object> quotaRelease(CustCreditUseBean creditUseBean)throws Exception;
	
	
	/****
	 * 额度切分
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaAsegForCrms(List<QuotaSyncopateRec> quotaSyncopateRecs)throws Exception;
	
	/****
	 * 额度占用
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaUsedForCrms(Map<String, Object> params)throws Exception;
	
	/****
	 * 额度释放
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaReleaseForCrms(Map<String, Object> params)throws Exception;
	
	/****
	 * 处理 批复信息查询结果
	 */
	public void creditApprovalInq(ApprovalInqRs approvalInqRs,String ecifno);
	
	/****
	 * 处理 批复信息变更结果
	 */
//	public void creditApprovalChange(ApprovalChangeRq approvalChangeRqs);
	public RetMsg<Object> creditApprovalChange(List<ApprovalChangeRec> approveList);

	public List<TcCustCreditMainLog> getCustCreditMainLog(Map<String, Object> map);

	public List<TcCustCreditDetailLog> getCustCreditDetailLog(Map<String, Object> map);
	
	/****
	 * 线下交易额度占用
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaUsedForOffLine(CustCreditUseBean creditUseBean)throws Exception;
	
	/****
	 * 线下交易额度释放
	 * @throws Exception
	 */
	public RetMsg<Object> creditQuotaReleaseForOffLine(CustCreditUseBean creditUseBean)throws Exception;
	
	public Page<TcCustCreditMainLog> getLogByCustCreditId(Map<String, Object> map);
	
	public RetMsg<Object> addCreditId(String productCode, String creditId);
	
}

