package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditCompany;

public interface CustCreditCompanyService {

	Page<CustCreditCompany> getCustCeditCompanyList(Map<String, Object> map);
	
	CustCreditCompany getCustCeditCompany(CustCreditCompany custCreditCompany);
	
	void insertCustCeditCompany(CustCreditCompany custCreditCompany);
	
	void updateCustCreditCompany(CustCreditCompany custCreditCompany);
	
}
