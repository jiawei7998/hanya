package com.singlee.capital.credit.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.model.TcCredit;
import com.singlee.capital.credit.model.TcInstCredit;
import com.singlee.capital.credit.model.TcProductCredit;
import com.singlee.capital.credit.model.TcRiskCredit;
import com.singlee.capital.credit.model.Tcterm;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;
import com.singlee.capital.credit.pojo.CustCreditDealBean;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;
import com.singlee.capital.credit.pojo.CustCreditSegBean;
import com.singlee.capital.credit.pojo.CustCreditUseBean;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.credit.service.TcCreditService;
import com.singlee.capital.credit.service.TcInstCreditService;
import com.singlee.capital.credit.service.TcProductCreditService;
import com.singlee.capital.credit.service.TcRiskCreditService;
import com.singlee.capital.credit.service.TcRproductCreditService;
import com.singlee.capital.credit.service.TctermService;

/**
 * @projectName 同业业务管理系统
 * @className 同业额度管理控制器
 * @description TODO
 * @author liuxiaoyi
 * @createDate 2017-5-13 下午16:16:28	
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/CreditManagerController")
public class CreditManagerController {
	@Resource
	private TctermService tctermService;
	@Autowired
	private TcProductCreditService tcProductCreditService;
	@Autowired
	private TcCreditService tcCreditService;
	@Autowired
	private TcRiskCreditService tcRiskCreditService;
	@Autowired
	private TcRproductCreditService tcRproductCreditService;
	@Autowired
	private TcInstCreditService tcInstCreditService;
	@Autowired
	private CreditManageService creditManageService;
	/**
	 * 查询期限配置
	 * @param tcterm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQueryList")
	public RetMsg<PageInfo<Tcterm>> creditQueryList(@RequestBody Map<String, Object> map){
		Page<Tcterm> page = tctermService.selectTcterm(map);
		return RetMsgHelper.ok(page);
	} 
	
	/**
	 * 
	 * @param 删除期限配置
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCredit")
	public RetMsg<Serializable> deleteCredit(@RequestBody Map<String, Object> map) {
		tctermService.deleteCredit(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增期限配置
	 * @param tcterm
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addCredit")
	public RetMsg<Serializable> addCredit(@RequestBody Tcterm tcterm) {
		try {
			tctermService.addCredit(tcterm);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple("NACK", "期限配置已存在");
		}
	}
	
	/**
	 * 修改期限配置
	 * @param tcterm
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCredit")
	public RetMsg<Serializable> updateCredit(@RequestBody Tcterm tcterm) {
		try {
			tctermService.updateCredit(tcterm);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.ok("NACK", "期限配置不存在");
		}
	}
	
	/**
	 * 查询产品信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/creditProductList")
	public RetMsg<PageInfo<TcProductCredit>> creditProductPageQuery(@RequestBody Map<String, Object> map) {
		Page<TcProductCredit> result = tcProductCreditService.pageCreditProduct(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除产品
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteProduct")
	public RetMsg<Serializable> deleteProduct(@RequestBody Map<String, Object> map) {
		tcProductCreditService.deleteCreditProduct(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增产品
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addCreditProduct")
	public RetMsg<Serializable> addCreditProduct(@RequestBody TcProductCredit tcProductCredit) throws RException {
		tcProductCreditService.addProductCredit(tcProductCredit);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新产品信息
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditProduct")
	public RetMsg<Serializable> updateCreditProduct(@RequestBody TcProductCredit tcProductCredit) throws RException {
		tcProductCreditService.updateProductCredit(tcProductCredit);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询额度品种信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/tcCreditList")
	public RetMsg<PageInfo<TcCredit>> tcCreditList(@RequestBody Map<String, Object> map) {
		Page<TcCredit> result = tcCreditService.pageTcCreditList(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 查询所有额度品种
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/allTcCredit")
	public RetMsg<List<TcCredit>> getCreditList() {
		List<TcCredit> result = tcCreditService.allTcCreditList();
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除额度品种
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTcCredit")
	public RetMsg<Serializable> deleteTcCredit(@RequestBody Map<String, Object> map) {
		try {
			tcCreditService.deleteTcCredit(map);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple(e.getMessage());
		}
		
	}
	
	/**
	 * 新增额度品种
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addTcCredit")
	public RetMsg<Serializable> addTcCredit(@RequestBody TcCredit tcCredit) throws RException {
		try {
			tcCreditService.addTcCredit(tcCredit);
			return RetMsgHelper.ok();
		} catch (RException e) {
			return RetMsgHelper.simple(e.getMessage());
		}
	}
	
	/**
	 * 更新额度品种信息
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTcCredit")
	public RetMsg<Serializable> updateTcCredit(@RequestBody TcCredit tcCredit) throws RException {
		try {
			tcCreditService.updateTcCredit(tcCredit);
			return RetMsgHelper.ok();
		} catch (RException e) {
			return RetMsgHelper.simple(e.getMessage());
		}
		
	}
	
	/**
	 * 查询额度品种风险级序
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/tcRiskCreditList")
	public RetMsg<PageInfo<TcRiskCredit>> tcRiskCreditList(@RequestBody Map<String, Object> map) {
		Page<TcRiskCredit> result = tcRiskCreditService.pageTcRiskCreditList(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除额度品种风险级序
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTcRiskCredit")
	public RetMsg<Serializable> deleteTcRiskCredit(@RequestBody Map<String, Object> map) {
		RetMsg<TcRiskCredit> retMsg=tcRiskCreditService.deleteTcRiskCredit(map);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
	}
	
	/**
	 * 新增额度品种
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addTcRiskCredit")
	public RetMsg<Serializable> addTcRiskCredit(@RequestBody TcRiskCredit tcRiskCredit) throws RException {
		RetMsg<TcRiskCredit> retMsg=tcRiskCreditService.addTcRiskCredit(tcRiskCredit);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
	}
	
	/**
	 * 更新额度品种信息
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTcRiskCredit")
	public RetMsg<Serializable> updateTcRiskCredit(@RequestBody TcRiskCredit tcRiskCredit) throws RException {
		RetMsg<TcRiskCredit> retMsg=tcRiskCreditService.updateTcRiskCredit(tcRiskCredit);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
	}
	
	/**
	 * 查询法人机构额度占用配置信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/instCreditList")
	public RetMsg<PageInfo<TcInstCredit>> tcInstCreditList(@RequestBody Map<String, Object> map) {
		Page<TcInstCredit> result = tcInstCreditService.pageInstCreditList(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除法人机构额度占用配置
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteInstCredit")
	public RetMsg<Serializable> deleteInstCredit(@RequestBody Map<String, Object> map) {
		tcInstCreditService.deleteInstCredit(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增法人机构额度占用配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addInstCredit")
	public RetMsg<Serializable> addInstCredit(@RequestBody TcInstCredit tcInstCredit) throws RException {
		RetMsg<TcInstCredit> retMsg=tcInstCreditService.addInstCredit(tcInstCredit);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
		//return RetMsgHelper.ok();
	}
	
	/**
	 * 更新法人机构额度占用配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateInstCredit")
	public RetMsg<Serializable> updateInstCredit(@RequestBody TcInstCredit tcInstCredit) throws RException {
		RetMsg<TcInstCredit> retMsg=tcInstCreditService.updateInstCredit(tcInstCredit);
		return RetMsgHelper.simple(retMsg.getCode(),retMsg.getDesc());
		//return RetMsgHelper.ok();
	}
	
	/**
	 * 查询业务品种对应额度品种信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/tcRproductCreditList")
	public RetMsg<PageInfo<TcRproductCreditPojo>> tcRproductCreditList(@RequestBody Map<String, Object> map) {
		Page<TcRproductCreditPojo> result = tcRproductCreditService.pageTcRproductCreditPojo(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 删除业务品种对应额度品种配置
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteRproductCredit")
	public RetMsg<Serializable> deleteRproductCredit(@RequestBody Map<String, Object> map) {
		tcRproductCreditService.deleteRproductCredit(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增业务品种对应额度品种配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/addRproductCredit")
	public RetMsg<Serializable> addRproductCredit(@RequestBody TcRproductCreditPojo tcRproductCreditPojo) {
		try {
			tcRproductCreditService.addRproductCredit(tcRproductCreditPojo);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple("NACK", "关系已存在");
		}
	}
	
	/**
	 * 更新业务品种对应额度品种配置
	 * @param tcProductCredit
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/updateRproductCredit")
	public RetMsg<Serializable> updateRproductCredit(@RequestBody TcRproductCreditPojo tcRproductCreditPojo) {
		try {
			tcRproductCreditService.updateRproductCredit(tcRproductCreditPojo);
			return RetMsgHelper.ok();
		} catch (Exception e) {
			return RetMsgHelper.simple("NACK", "关系已存在");
		}
	}
	
	/**
	 * 额度切分
	 * @param segBean
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQuotaAseg")
	public RetMsg<CustCreditSegBean> creditQuotaAseg(@RequestBody CustCreditSegBean segBean) {
		try {
			return creditManageService.creditQuotaAseg(segBean);
		} catch (Exception e) {
			JY.error("额度切分出错", e);
			return new RetMsg<CustCreditSegBean>("999999", "额度切分出错:"+e.getMessage());
		}
		
	}
	
	/**
	 * 额度调整
	 * @param segBean
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQuotaAdj")
	public RetMsg<CustCreditAdjBean> creditQuotaAdj(@RequestBody CustCreditAdjBean adjBean) {
		try {
			return creditManageService.creditQuotaAdj(adjBean);
		} catch (Exception e) {
			JY.error("额度调整出错", e);
			return new RetMsg<CustCreditAdjBean>("999999", "额度调整出错:"+e.getMessage());
		}
		
	}
	
	/**
	 * 额度冻结、解冻
	 * @param segBean
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQuotaFreeze")
	public RetMsg<Object> creditQuotaFreeze(@RequestBody CustCreditFrozenBean frozenBean) {
		try {
			if(frozenBean.getState().equals(DictConstants.CREDIT_FREEZE_STATE.UNFREEZE)) {
				return creditManageService.creditQuotaUnFreeze(frozenBean);
			} else {
				return creditManageService.creditQuotaFreeze(frozenBean);
			}
		} catch (Exception e) {
			JY.error("额度冻结出错", e);
			return new RetMsg<Object>("999999", "额度冻结出错:"+e.getMessage());
		}
		
	}
	
	/**
	 * 额度占用、释放
	 * @param segBean
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQuotaUse")
	public RetMsg<Object> creditQuotaUse(@RequestBody CustCreditDealBean creditDealBean) {
		CustCreditUseBean creditUseBean = new CustCreditUseBean();
		creditUseBean.setCustNo(creditDealBean.getCustNo());
		creditUseBean.setCustName(creditDealBean.getCustName());
		creditUseBean.setAmt(new BigDecimal(creditDealBean.getAmt()));
		creditUseBean.setProduct_code(creditDealBean.getProductCode());
		creditUseBean.setVdate(creditDealBean.getVdate().substring(0, 10));
		creditUseBean.setMdate(creditDealBean.getMdate().substring(0, 10));
		try {
			if(creditDealBean.getState() == DictConstants.CREDIT_USE_STATE.RELEASE) 
			{
				creditUseBean.setDealNo(creditDealBean.getOldDealNo());
				return creditManageService.creditQuotaReleaseForOffLine(creditUseBean);
			} else {
				creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
				creditUseBean.setDealNo(creditDealBean.getDealNo());
				return creditManageService.creditQuotaUsedForOffLine(creditUseBean);
			}
		} catch (Exception e) {
			JY.error("额度占用出错", e);
			return new RetMsg<Object>("999999", "额度占用出错:"+e.getMessage());
		}	
		
	}
	
	
	/**	 * 额度占用、释放
	 * @param segBean
	 */
	@ResponseBody
	@RequestMapping(value = "/creditQuotaUseTest")
	public RetMsg<Object> creditQuotaUseTest(@RequestBody CustCreditDealBean creditDealBean) {
		CustCreditUseBean creditUseBean = new CustCreditUseBean();
		creditUseBean.setCustNo(creditDealBean.getCustNo());
		creditUseBean.setCustName(creditDealBean.getCustName());
		creditUseBean.setAmt(new BigDecimal(creditDealBean.getAmt()));
		creditUseBean.setProduct_code(creditDealBean.getProductCode());
		creditUseBean.setVdate(creditDealBean.getVdate().substring(0, 10));
		creditUseBean.setMdate(creditDealBean.getMdate().substring(0, 10));
		try {
			if(creditDealBean.getState() == DictConstants.CREDIT_USE_STATE.RELEASE) 
			{
				creditUseBean.setDealNo(creditDealBean.getOldDealNo());
				creditUseBean.setDealNo("00001");
				creditUseBean.setAmt(new BigDecimal(2000000));
				return creditManageService.creditQuotaRelease(creditUseBean);
			} else {
				creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
				creditUseBean.setDealNo(creditDealBean.getDealNo());
				
				for (int i = 0; i < 100; i++) 
				{
					System.out.println(i);
					creditUseBean.setDealNo(StringUtil.leftFillStr(String.valueOf(i+1), 5, "0"));
					creditUseBean.setAmt(new BigDecimal(3500000));
					creditManageService.creditQuotaUsed(creditUseBean);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new RException(e);
		}
		return null;
	}
	
	/**
	 * 查询未选择的额度品种
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pageTcCredit")
	public RetMsg<PageInfo<TcCredit>> pageTcCredit(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tcCreditService.pageTcCredit(map));
	}
	
	/**
	 * 查询已选择的额度品种
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/findChooseCredit")
	public RetMsg<List<TcCredit>> getChooseCredit(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tcCreditService.getChooseCredit(map));
	}
	
}
