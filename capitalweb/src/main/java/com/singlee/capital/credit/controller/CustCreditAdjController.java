package com.singlee.capital.credit.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;
import com.singlee.capital.credit.service.CreditAdjService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;

@Controller
@RequestMapping(value = "/CustCreditAdjController")
public class CustCreditAdjController extends CommonController {

	@Autowired
	CreditAdjService creditAdjService;
	
	/**
	 * 新增一笔额度调整的审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addCustCreditAdj")
	public RetMsg<Serializable> addCustCreditAdj(@RequestBody Map<String,Object> map) {
		creditAdjService.addCustCreditAdj(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到发起的额度调整列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getAdjDetailByCreditAll")
	public RetMsg<List<CustCreditAdjBean>> getAdjDetailByCreditAll(@RequestBody Map<String,Object> map) {
		List<CustCreditAdjBean> adjlist = creditAdjService.getAdjDetailByCreditAll(map);
		return RetMsgHelper.ok(adjlist);
	}
	
	/**
	 * 得到发起的额度调整列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditAdjList")
	public RetMsg<PageInfo<CustCreditAdjBean>> getCreditAdjList(@RequestBody Map<String,Object> map) {
		Page<CustCreditAdjBean> list = creditAdjService.getCreditAdjListMine(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到待审批的额度调整列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditAdjWaitApprove")
	public RetMsg<PageInfo<CustCreditAdjBean>> getCreditAdjWaitApprove(@RequestBody Map<String,Object> map) {
		String approveStatus = DictConstants.ApproveStatus.New;
		map.put("approveStatus", approveStatus.split(","));
		Page<CustCreditAdjBean> list = creditAdjService.getCreditAdjList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到已审批的额度调整列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditAdjApprovedPass")
	public RetMsg<PageInfo<CustCreditAdjBean>> getCreditAdjApprovedPass(@RequestBody Map<String,Object> map) {
		Page<CustCreditAdjBean> list = creditAdjService.getCreditAdjListFinish(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 删除一笔额度调整的审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCreditAdj")
	public RetMsg<Serializable> deleteCreditAdj(@RequestBody Map<String,Object> map) {
		creditAdjService.deleteCreditAdj(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新一笔额度调整的审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditAdj")
	public RetMsg<Serializable> updateCreditAdj(@RequestBody Map<String,Object> map) {
		creditAdjService.updateCreditAdj(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 拿到额度品种的调整详情
	 * @param 额度品种
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAdjDetailByCredit")
	public RetMsg<PageInfo<CustCreditAdjBean>> getAdjDetailByCredit(@RequestBody Map<String,Object> map) {
		Page<CustCreditAdjBean> list = creditAdjService.getAdjDetailByCredit(map);
		return RetMsgHelper.ok(list);
	}
	
}
