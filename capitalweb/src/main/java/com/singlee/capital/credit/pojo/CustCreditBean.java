package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/*****
 * 授信额度
 * @author kf0738
 *
 */
@Entity
@Table(name="TC_CUST_CREDIT")
public class CustCreditBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	/***
	 * 
	 * 客户号、客户名称、额度品种、额度名称、已生效的额度切分、额度切分调整（增加减少）、用途（票据系统、线上资金交易、其他（录入文字））
	 */
	private String          custCreditId	;            //客户额度唯一编号     
	private String          custNo	        ;            //客户编号             
	private String          custName	      ;            //客户名称             
	private String          creditId	      ;            //额度种类编号         
	private String          creditName	    ;            //额度种类名称         
	private BigDecimal      creditAmt	    ;            //授信额度             
	private BigDecimal      quotaSegAmt	  ;            //已切分额度             
	private BigDecimal      quotaAdjAmt	  ;            //额度调整             
	private BigDecimal      quotaStrAmt	  ;            //串用额度             
	private BigDecimal      quotaCstrAmt	  ;            //被串用额度           
	private BigDecimal      quotaUsedAmt	  ;            //已用金额             
	private BigDecimal      quotaFrozenAmt	;            //冻结金额             
	private BigDecimal      quotaAvlAmt	  ;            //可用金额   
	private String          termId	        ;            //业务期限             
	private Integer         weight	        ;            //权重值               
	private String	        expDate	        ;            //额度到期日           
	private String          remark1	        ;            //备注字段             
	private String          remark2	        ;            //备注字段       
	private String            operDate;//操作日期
	private BigDecimal      useAmt;
	private BigDecimal      releaseAmt;//释放金额
	private BigDecimal      strAmt	  ;            //串用额度             
	private BigDecimal      cstrAmt	  ;            //被串用额度           
	
	private String ecifNum			;				//客户ECIF号
	private String sysId			;				//切分系统
	private String currency			;				//币种
	//private String Term				;				//期限
	//private String TermType			;				//期限类型 01-年 02-半年 03-季 04-月 05-日
	private String cntrStatus		;				//额度状态 01-未生效 02-已生效 03-已终结 04-已冻结
	@Transient
	private String progress;
	
	
	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}
	@Override
	public String toString()
	{
		return "[" + creditName + ","+termId + "]";
	}
	
	public BigDecimal getStrAmt() {
		return strAmt;
	}

	public void setStrAmt(BigDecimal strAmt) {
		this.strAmt = strAmt;
	}

	public BigDecimal getCstrAmt() {
		return cstrAmt;
	}

	public void setCstrAmt(BigDecimal cstrAmt) {
		this.cstrAmt = cstrAmt;
	}

	public BigDecimal getUseAmt() {
		return useAmt;
	}

	public void setUseAmt(BigDecimal useAmt) {
		this.useAmt = useAmt;
	}

	public BigDecimal getReleaseAmt() {
		return releaseAmt;
	}

	public void setReleaseAmt(BigDecimal releaseAmt) {
		this.releaseAmt = releaseAmt;
	}

	public String getOperDate() {
		return operDate;
	}
	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}
	
	
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
	
	public BigDecimal getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	public BigDecimal getQuotaSegAmt() {
		return quotaSegAmt;
	}
	public void setQuotaSegAmt(BigDecimal quotaSegAmt) {
		this.quotaSegAmt = quotaSegAmt;
	}
	public BigDecimal getQuotaAdjAmt() {
		return quotaAdjAmt;
	}
	public void setQuotaAdjAmt(BigDecimal quotaAdjAmt) {
		this.quotaAdjAmt = quotaAdjAmt;
	}
	public BigDecimal getQuotaStrAmt() {
		return quotaStrAmt;
	}
	public void setQuotaStrAmt(BigDecimal quotaStrAmt) {
		this.quotaStrAmt = quotaStrAmt;
	}
	public BigDecimal getQuotaCstrAmt() {
		return quotaCstrAmt;
	}
	public void setQuotaCstrAmt(BigDecimal quotaCstrAmt) {
		this.quotaCstrAmt = quotaCstrAmt;
	}
	public BigDecimal getQuotaUsedAmt() {
		return quotaUsedAmt;
	}
	public void setQuotaUsedAmt(BigDecimal quotaUsedAmt) {
		this.quotaUsedAmt = quotaUsedAmt;
	}
	public BigDecimal getQuotaFrozenAmt() {
		return quotaFrozenAmt;
	}
	public void setQuotaFrozenAmt(BigDecimal quotaFrozenAmt) {
		this.quotaFrozenAmt = quotaFrozenAmt;
	}
	public BigDecimal getQuotaAvlAmt() {
		return quotaAvlAmt;
	}
	public void setQuotaAvlAmt(BigDecimal quotaAvlAmt) {
		this.quotaAvlAmt = quotaAvlAmt;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
	public String getEcifNum() {
		return ecifNum;
	}

	public void setEcifNum(String ecifNum) {
		this.ecifNum = ecifNum;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCntrStatus() {
		return cntrStatus;
	}

	public void setCntrStatus(String cntrStatus) {
		this.cntrStatus = cntrStatus;
	}

	public String getTermType() {
		if(this.termId == null) {
			return "";
		}
		
		String termType = this.termId.substring(this.termId.length()-1, this.termId.length());
		String termT = "";
		if("Y".equals(termType)) {
			termT = "01";
		} else if("V".equals(termType)) {
			termT = "02";
		} else if("Q".equals(termType)) {
			termT = "03";
		} else if("M".equals(termType)) {
			termT = "04";
		} else if("D".equals(termType)) {
			termT = "05";
		}
		return termT;
	}
	
	public String getTerm() {
		if(this.termId == null) {
			return "";
		}
		String term = this.termId.substring(0, this.termId.length()-1);
		return term;
	}

}
