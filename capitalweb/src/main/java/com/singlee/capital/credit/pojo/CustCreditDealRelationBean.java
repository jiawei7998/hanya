package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/*****
 * 授信额度和交易关联关系类 
 * @author kf0738
 *
 */
public class CustCreditDealRelationBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String          rno;//占用关系编号
    private String          dealNo; //交易编号 
    private String          custNo;      //客户编号            
    private String          productCode; //交易产品类型
	private String          custCreditId;      //占用的 客户额度编号
	private String          orgCustCreditId;   //原客户额度编号
	private Integer         weight ;//额度占用权重
	private String          history; //是否是历史记录 Y , N
	private BigDecimal      useAmt; //占用金额
	private BigDecimal      releasedAmt; //释放金额
	private Integer         useType; //占用类型:1-占用 2-串用
	private Integer         useState; //占用状态:1-已占用 2-已部分释放 3-已释放
	private String            vdate; //起息日
	private String            mdate;   //到期日
	private String          inputDate;//记录日期
	private String          inputTime;//记录时间
	private String          remark;//备注信息
	private Date            modifyDate;//修改日期
	private boolean         updateFlag = false;
	private BigDecimal      amt;
	private BigDecimal      avlAmt;
	
	/*****
	 * 
	 * @param dealNo              交易编号
	 * @param custNo;             客户编号
	 * @param productCode         交易产品类型
	 * @param custCreditId        占用的客户额度编号
	 * @param orgCustCreditId     原客户额度编号
	 * @param seq                 额度占用顺序
	 * @param useAmt              占用金额
	 * @param releasedAmt         释放金额
	 * @param useType             占用类型:1-占用 2-串用
	 * @param useState            占用状态:1-已占用 2-已释放
	 * @param vdate             起息日
	 * @param mdate             到期日
	 * @param inputDate           记录日期
	 * @param inputTime           记录时间
	 * @param remark              备注信息
	 */
	public CustCreditDealRelationBean(String rno,String dealNo,String custNo,String productCode,String custCreditId,
			String orgCustCreditId, Integer weight, String history,BigDecimal useAmt,BigDecimal releasedAmt,
			Integer useType, Integer useState, String vdate,String mdate, String inputDate,
			String inputTime, String remark) {
		super();
		this.rno = rno;
		this.dealNo = dealNo;
		this.custNo = custNo;
		this.productCode = productCode;
		this.custCreditId = custCreditId;
		this.orgCustCreditId = orgCustCreditId;
		this.weight = weight;
		this.history = history;
		this.useAmt = useAmt;
		this.releasedAmt = releasedAmt;
		this.useType = useType;
		this.useState = useState;
		this.vdate = vdate;
		this.mdate = mdate;
		this.inputDate = inputDate;
		this.inputTime = inputTime;
		this.remark = remark;
	}
	
	public CustCreditDealRelationBean(){}
	
	public String getDesc()
	{
		return "";
	}
	
	public BigDecimal getAvlAmt() {
		return avlAmt;
	}

	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public String getRno() {
		return rno;
	}

	public void setRno(String rno) {
		this.rno = rno;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public BigDecimal getReleasedAmt() {
		return releasedAmt;
	}

	public void setReleasedAmt(BigDecimal releasedAmt) {
		this.releasedAmt = releasedAmt;
	}

	public BigDecimal getUseAmt() {
		return useAmt;
	}
	public void setUseAmt(BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	
	public String getOrgCustCreditId() {
		return orgCustCreditId;
	}

	public void setOrgCustCreditId(String orgCustCreditId) {
		this.orgCustCreditId = orgCustCreditId;
	}

	
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getUseType() {
		return useType;
	}
	public void setUseType(Integer useType) {
		this.useType = useType;
	}
	public Integer getUseState() {
		return useState;
	}
	public void setUseState(Integer useState) {
		this.useState = useState;
	}
	
	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	

}
