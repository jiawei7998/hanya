package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;

import tk.mybatis.mapper.common.Mapper;

public interface TcCustCreditFrozenMapper extends Mapper<CustCreditFrozenBean>{

	
	void addCustCreditFrozen(Map<String, Object> map);

	/** 
	 * 得到额度冻结审批列表
	 */
	Page<CustCreditFrozenBean> getCreditFrozenList(Map<String, Object> map,
			RowBounds rowBounds);
	Page<CustCreditFrozenBean> getCreditFrozenListMine(Map<String, Object> map,
			RowBounds rowBounds);
	Page<CustCreditFrozenBean> getCreditFrozenListFinish(Map<String, Object> map,
			RowBounds rowBounds);
	
	void deleteByDealNo(Map<String,Object> map);

	void updateByDealNo(Map<String, Object> map);

	void statusChange(Map<String, Object> status);
	
	Page<CustCreditFrozenBean> getFrozenDetailByCredit(Map<String, Object> map,
			RowBounds rowBounds);

	CustCreditFrozenBean getCreditFrozenByDealNo(String dealNo);
	
	/** 20180523新增：根据map中条件（dealNo,branchId）查询实体对象*/
	CustCreditFrozenBean getCreditFrozenByMap(Map<String, Object> map);

	List<CustCreditFrozenBean> getFrozenDetailByCreditAll(
			Map<String, Object> map);

	
	Page<CustCreditFrozenBean> getCreditFrozenListForDialog(Map<String, Object> map,
			RowBounds rowBounds);
	
	/**
	 * 查询冻结交易<>18（过期失效）的交易
	 * 如果已经是6的，那么需要把冻结的释放，并查看额度总表是否有冻结的；
	 * 如果是其他的则直接修改未18
	 * @param map
	 * @return
	 */
	List<CustCreditFrozenBean> getCreditFrozenListForMaturityAutoRelease(Map<String, Object> map);
	/**
	 * 获得冻结明细
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<CustCreditFrozenBean> getCreditFrozenListForDetail(Map<String, Object> map,
			RowBounds rowBounds);
}
