package com.singlee.capital.credit.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcCreditApproveInfoMapper;
import com.singlee.capital.credit.mapper.TcCustCreditCompanyMapper;
import com.singlee.capital.credit.mapper.TcCustCreditDealRelationMapper;
import com.singlee.capital.credit.mapper.TcCustCreditDetailLogMapper;
import com.singlee.capital.credit.mapper.TcCustCreditFrozenMapper;
import com.singlee.capital.credit.mapper.TcCustCreditMainLogMapper;
import com.singlee.capital.credit.mapper.TcCustCreditMapper;
import com.singlee.capital.credit.mapper.TcCustCreditTempMapper;
import com.singlee.capital.credit.mapper.TcInstCreditMapper;
import com.singlee.capital.credit.model.TcCreditApproveInfo;
import com.singlee.capital.credit.model.TcInstCredit;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;
import com.singlee.capital.credit.pojo.CustCreditBean;
import com.singlee.capital.credit.pojo.CustCreditCompany;
import com.singlee.capital.credit.pojo.CustCreditDealRelationBean;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;
import com.singlee.capital.credit.pojo.CustCreditSegBean;
import com.singlee.capital.credit.pojo.CustCreditUseBean;
import com.singlee.capital.credit.pojo.TcCustCreditDetailLog;
import com.singlee.capital.credit.pojo.TcCustCreditMainLog;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.interfacex.model.ApprovalChangeRec;
import com.singlee.capital.interfacex.model.ApprovalInqRec;
import com.singlee.capital.interfacex.model.ApprovalInqRs;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditManageServiceImpl implements CreditManageService {

	@Autowired
	private TcCustCreditFrozenMapper tcCustCreditFrozenMapper;

	@Autowired
	private TcCreditApproveInfoMapper tcCreditApproveInfoMapper;

	@Autowired
	private TcCustCreditMapper tcCustCreditMapper;

	@Autowired
	TcCustCreditTempMapper tcCustCreditTempMapper;

	@Autowired
	private TrdOrderMapper trdOrderMapper;

	@Autowired
	private TcCustCreditMainLogMapper mainLogMapper;

	@Autowired
	private TcCustCreditDetailLogMapper detailLogMapper;

	@Autowired
	private TcCustCreditDealRelationMapper tcCustCreditDealRelationMapper;

	@Autowired
	private ProductApproveService productApproveService;

	@Autowired
	private TcCustCreditMainLogMapper tcCustCreditMainLogMapper;

	@Autowired
	private TcCustCreditDetailLogMapper tcCustCreditDetailLogMapper;

	@Autowired
	private TcCustCreditCompanyMapper tcCustCreditCompanyMapper;

	@Autowired
	private TcInstCreditMapper tcInstCreditMapper;

	private BigDecimal zero = BigDecimal.ZERO;

	private static DecimalFormat nf = new DecimalFormat("#.##");

	private static String nline = "\r\n";

	@Override
	public RetMsg<CustCreditSegBean> creditQuotaAseg(CustCreditSegBean segBean) throws Exception {
		return quotaAseg(segBean);
	}

	/****
	 * 锁住客户记录
	 * 
	 * @param custNo
	 */
	public void selectCreditStateForLock(String custNo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("is_locked", true);
		map.put("cno", custNo);
		tcCustCreditDealRelationMapper.selectCreditStateForLock(map);
	}

	public RetMsg<CustCreditSegBean> quotaAseg(CustCreditSegBean segBean) throws Exception {
		segBean.setCustNo(getInstId(segBean.getCustNo()));
		selectCreditStateForLock(segBean.getCustNo());

		RetMsg<CustCreditSegBean> retMsg = new RetMsg<CustCreditSegBean>(RetMsgHelper.codeOk, "额度切分成功", "", segBean);
		// 额度切分
		// 获取额度信息
		String custCreditId = segBean.getCustCreditId();
		CustCreditBean creditBean = tcCustCreditMapper.queryCustCreditById(custCreditId);
		if (creditBean == null) {
			retMsg.setCode("000100");
			retMsg.setDesc("无法获取客户额度信息");
			return retMsg;
		}
		// 判断额度有效日期
		if (segBean.getOperDate() != null && segBean.getOperDate().compareTo(creditBean.getExpDate()) > 0) {
			retMsg.setCode("000110");
			retMsg.setDesc("客户" + creditBean.getCustNo() + "的" + creditBean.getCreditName() + "("
					+ creditBean.getTermId() + ")额度已经到期,无法切分额度");
			return retMsg;
		}

		/****
		 * 正：增加切分（小于等于可用于切分金额）；负：减少切分（小于等于已切分金额）
		 */
		// 如果增加切分，增加金额大于可用额度，返回
		if ((segBean.getSegAmt().compareTo(zero) > 0)
				&& (segBean.getSegAmt().compareTo(creditBean.getQuotaAvlAmt()) > 0)) {
			retMsg.setCode("000120");
			retMsg.setDesc("操作失败:增加切分额度大于可用额度");
			return retMsg;
		}
		// 如果减少切分,减少金额大于已经切分金额，返回
		if ((segBean.getSegAmt().compareTo(zero) <= 0)
				&& (segBean.getSegAmt().abs().compareTo(creditBean.getQuotaSegAmt()) > 0)) {
			retMsg.setCode("000130");
			retMsg.setDesc("操作失败:减少切分金额大于可用于切分的额度");
			return retMsg;
		}

		if (segBean.getrDealno() != null) {
			// 如果发起审批时已经冻结额度,则审批通过后解冻额度
			CustCreditFrozenBean frozenBean = tcCustCreditFrozenMapper.getCreditFrozenByDealNo(segBean.getrDealno());
			if (frozenBean != null && frozenBean.getState().equals(DictConstants.CREDIT_FREEZE_STATE.FREEZE)) {
				creditQuotaUnFreeze(frozenBean);
			}

		}

		// 切分额度
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("custCreditId", segBean.getCustCreditId());
		params.put("segAmt", segBean.getSegAmt());
		tcCustCreditMapper.updateQuotaAseg(params);

		BigDecimal avlAmt = creditBean.getQuotaAvlAmt().subtract(segBean.getSegAmt());

		TcCustCreditMainLog creditMainLog = createTcCreditMainLog(null, creditBean.getCustNo(),
				DictConstants.CREDIT_OPER_TYPE.ASEG, segBean.getCustCreditId(), segBean.getSegAmt(), avlAmt, null,
				"额度切分");
		tcCustCreditMainLogMapper.insert(creditMainLog);
		// 减少切分,可用额度增加,需要解除额度串用
		if (segBean.getSegAmt().compareTo(zero) <= 0) {
			creditQuotaReleaseStrByCreditId(creditBean.getCustNo(), creditBean.getCreditId(), creditMainLog);
		}
		return retMsg;
	}

	@Override
	public RetMsg<CustCreditAdjBean> creditQuotaAdj(CustCreditAdjBean adjBean) throws Exception {
		adjBean.setCustNo(getInstId(adjBean.getCustNo()));
		selectCreditStateForLock(adjBean.getCustNo());

		RetMsg<CustCreditAdjBean> retMsg = new RetMsg<CustCreditAdjBean>(RetMsgHelper.codeOk, "额度调整成功", "", adjBean);
		// 额度调整
		// 获取额度信息
		CustCreditBean creditBean = tcCustCreditMapper.queryCustCreditById(adjBean.getCustCreditId());
		if (creditBean == null) {
			retMsg.setCode("000100");
			retMsg.setDesc("无法获取客户额度信息");
			return retMsg;
		}
		// 判断额度有效日期
		if (adjBean.getOperDate() != null && adjBean.getOperDate().compareTo(creditBean.getExpDate()) > 0) {
			retMsg.setCode("000110");
			retMsg.setDesc("客户" + creditBean.getCustNo() + "的" + creditBean.getCreditName() + "("
					+ creditBean.getTermId() + ")额度已经到期,无法调整额度");
			return retMsg;
		}

		/****
		 * 正：增加授信（小于已调整金额 ）；负：减少授信（小于等于可用额度）
		 */
		// 如果增加调整，增加金额大于已经调整金额，返回
		if ((adjBean.getAdjAmt().compareTo(zero) > 0)
				&& (adjBean.getAdjAmt().compareTo(creditBean.getQuotaAvlAmt()) > 0)) {
			retMsg.setCode("000120");
			retMsg.setDesc("操作失败:增加调整金额大于可用额度");
			return retMsg;
		}
		// 如果减少调整,减少金额大于可用金额，返回
		if ((adjBean.getAdjAmt().compareTo(zero) <= 0)
				&& (adjBean.getAdjAmt().abs().compareTo(creditBean.getQuotaAdjAmt()) > 0)) {
			retMsg.setCode("000130");
			retMsg.setDesc("操作失败:减少调整金额大于可用于调整的额度");
			return retMsg;
		}

		if (adjBean.getrDealno() != null) {
			// 如果发起审批时已经冻结额度,则审批通过后解冻额度
			CustCreditFrozenBean frozenBean = tcCustCreditFrozenMapper.getCreditFrozenByDealNo(adjBean.getrDealno());
			if (frozenBean != null && frozenBean.getState().equals(DictConstants.CREDIT_FREEZE_STATE.FREEZE)) {
				creditQuotaUnFreeze(frozenBean);
			}
		}

		// 调整额度
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("custCreditId", adjBean.getCustCreditId());
		params.put("adjAmt", adjBean.getAdjAmt());
		tcCustCreditMapper.updateQuotaAdjAmt(params);

		BigDecimal avlAmt = creditBean.getQuotaAvlAmt().subtract(adjBean.getAdjAmt());

		TcCustCreditMainLog tcCustCreditMainLog = createTcCreditMainLog(null, creditBean.getCustNo(),
				DictConstants.CREDIT_OPER_TYPE.ADJ, adjBean.getCustCreditId(), adjBean.getAdjAmt(), avlAmt, null,
				"额度调整");
		tcCustCreditMainLogMapper.insert(tcCustCreditMainLog);
		if (adjBean.getAdjAmt().compareTo(zero) <= 0) {
			creditQuotaReleaseStrByCreditId(creditBean.getCustNo(), creditBean.getCreditId(), tcCustCreditMainLog);
		}
		return retMsg;

	}

	@Override
	public RetMsg<Object> creditQuotaFreeze(CustCreditFrozenBean frozenBean) throws Exception {
		frozenBean.setCustNo(getInstId(frozenBean.getCustNo()));
		selectCreditStateForLock(frozenBean.getCustNo());

		// 额度冻结
		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度冻结成功", "", frozenBean);
		CustCreditBean creditBean = tcCustCreditMapper.queryCustCreditById(frozenBean.getCustCreditId());
		if (creditBean == null) {
			retMsg.setCode("000100");
			retMsg.setDesc("额度信息不存在");
			return retMsg;
		}
		// 检查客户额度到期日
		if (frozenBean.getOperDate() != null && frozenBean.getOperDate().compareTo(creditBean.getExpDate()) > 0) {
			retMsg.setCode("000110");
			retMsg.setDesc("客户" + creditBean.getCustNo() + "额度" + creditBean.getCreditName() + "("
					+ creditBean.getTermId() + ")已经到期,无法冻结额度");
			return retMsg;
		}
		// 冻结额度判断
		if (frozenBean.getFrozenAmt().compareTo(creditBean.getQuotaAvlAmt()) > 0) {
			retMsg.setCode("000120");
			retMsg.setDesc("冻结额度大于可用额度");
			return retMsg;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("custCreditId", frozenBean.getCustCreditId());
		params.put("frozenAmt", frozenBean.getFrozenAmt());
		tcCustCreditMapper.updateFrozenAmt(params);

		BigDecimal avlAmt = creditBean.getQuotaAvlAmt().subtract(frozenBean.getFrozenAmt());

		TcCustCreditMainLog tcCustCreditMainLog = createTcCreditMainLog(null, creditBean.getCustNo(),
				DictConstants.CREDIT_OPER_TYPE.FREEZE, creditBean.getCustCreditId(), frozenBean.getFrozenAmt(), avlAmt,
				null, "额度冻结");
		tcCustCreditMainLogMapper.insert(tcCustCreditMainLog);
		return retMsg;
	}

	@Override
	public RetMsg<Object> creditQuotaUnFreeze(CustCreditFrozenBean frozenBean) throws Exception {
		frozenBean.setCustNo(getInstId(frozenBean.getCustNo()));
		selectCreditStateForLock(frozenBean.getCustNo());
		// 额度解冻
		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度解冻成功", "", frozenBean);
		CustCreditBean creditBean = tcCustCreditMapper.queryCustCreditById(frozenBean.getCustCreditId());
		if (creditBean == null) {
			retMsg.setCode("000100");
			retMsg.setDesc("额度信息不存在");
			return retMsg;
		}
		// 额度解冻金额判断
		if (frozenBean.getFrozenAmt().compareTo(creditBean.getQuotaFrozenAmt()) > 0) {
			retMsg.setCode("000120");
			retMsg.setDesc("额度解冻失败:解冻额度大于冻结额度");
			return retMsg;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("custCreditId", frozenBean.getCustCreditId());
		params.put("frozenAmt", frozenBean.getFrozenAmt());
		tcCustCreditMapper.updateUnFrozenAmt(params);

		BigDecimal avlAmt = creditBean.getQuotaAvlAmt().add(frozenBean.getFrozenAmt());

		TcCustCreditMainLog tcCustCreditMainLog = createTcCreditMainLog(null, creditBean.getCustNo(),
				DictConstants.CREDIT_OPER_TYPE.UNFREEZE, creditBean.getCustCreditId(), frozenBean.getFrozenAmt(),
				avlAmt, null, "额度解冻");
		tcCustCreditMainLogMapper.insert(tcCustCreditMainLog);

		creditQuotaReleaseStrByCreditId(creditBean.getCustNo(), creditBean.getCreditId(), tcCustCreditMainLog);
		return retMsg;
	}

	@Override
	public RetMsg<Object> creditQuotaPreUsed(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.PRE_USDED);
		return quotaUsed(creditUseBean);
	}

	@Override
	public RetMsg<Object> creditQuotaPreUsedRelease(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.PRE_USDED);
		return quotaRelease(creditUseBean);
	}

	@Override
	public RetMsg<Object> creditQuotaUsed(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
		return quotaUsed(creditUseBean);
	}

	public RetMsg<Object> quotaUsed(CustCreditUseBean creditUseBean) throws Exception {
		try {
			creditUseBean.setCustNo(getInstId(creditUseBean.getCustNo()));
			selectCreditStateForLock(creditUseBean.getCustNo());

			RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度占用成功", "", null);

			if (creditUseBean.isFinancial() == false) {
				CustCreditCompany c = new CustCreditCompany();
				c.setCustNo(creditUseBean.getCustNo());
				c.setAmt(creditUseBean.getAmt().doubleValue());
				c.setUseState(creditUseBean.getUseState());
				c.setInputDate(DateUtil.getCurrentDateAsString());
				c.setInputTime(DateUtil.getCurrentTimeAsString());
				c.setMdfDate(DateUtil.getCurrentDateTimeAsString());
				tcCustCreditCompanyMapper.insert(c);
				return retMsg;
			}
			// 额度验证
			HashMap<String, Object> params = null;
			boolean isTrue = true;
			// 先判定是否有预占用
			TdProductApproveMain productApproveMain = productApproveService
					.getProductApproveActivated(creditUseBean.getDealNo());
			if (StringUtils.equalsIgnoreCase(creditUseBean.getDealType(),
					com.singlee.capital.system.dict.DictConstants.DealType.Verify)
					&& StringUtils.isNotEmpty(productApproveMain.getRefNo())) {
				// 查看业务是否有预申请编号，并且是走到了核实阶段
				// 判断预占用是否存在
				params = new HashMap<String, Object>();
				params.put("dealNo", productApproveMain.getRefNo());// 预占用编号
				params.put("custNo", creditUseBean.getCustNo());
				params.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal freezeAmt = tcCustCreditDealRelationMapper.queryPreUsedAmtByDealNoCust(params);
				if (freezeAmt != null && freezeAmt.compareTo(zero) > 0) {
					creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);// 设置成预占用
					creditUseBean.setRefNo(creditUseBean.getDealNo());// 设置成为正式编号
					creditUseBean.setDealNo(productApproveMain.getRefNo());// 设置成为关联的审批单号
					isTrue = false;
				}
			}

			// 先检查是否有额度预占用
			if (DictConstants.CREDIT_USE_STATE.USDED.equals(creditUseBean.getUseState()) && productApproveMain != null
					&& StringUtils.isNotEmpty(productApproveMain.getRefNo())) {
				// 判断预占用额度是否足够占用
				params = new HashMap<String, Object>();
				params.put("dealNo", creditUseBean.getDealNo());// 审批单号
				params.put("refNo", creditUseBean.getRefNo());// 正式单号
				params.put("custNo", creditUseBean.getCustNo());
				params.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal freezeAmt = tcCustCreditDealRelationMapper.queryPreUsedAmtByDealNoCust(params);
				BigDecimal balance = null;
				if (freezeAmt != null && freezeAmt.compareTo(zero) > 0) {
					balance = creditUseBean.getAmt().subtract(freezeAmt);
					int flag = balance.compareTo(zero);
					// 占用大于预占用 将预占用金额状态改为占用,再继续占用剩余金额
					if (flag > 0) {
						HashMap<String, Object> params1 = new HashMap<String, Object>();
						params1.put("custNo", creditUseBean.getCustNo());
						params1.put("product_code", creditUseBean.getProduct_code());
						params1.put("vdate", creditUseBean.getVdate());
						params1.put("term", creditUseBean.getTermForMonth());
						// 查询该交易对手可用额度总和
						BigDecimal avlAmt = tcCustCreditMapper.queryCustCreditsAvlAmt(params1);
						if (creditUseBean.getAmt().compareTo(avlAmt) > 0) {
							retMsg.setCode("000110");
							retMsg.setDesc("操作失败:当前可用额度为" + avlAmt + ",可用额度不足");
							return retMsg;
						}
						tcCustCreditDealRelationMapper.updatePreUsedAmtToUse(params);
						creditUseBean.setAmt(balance);

					} else if (flag == 0) {// 正好相等直接修改状态
						tcCustCreditDealRelationMapper.updatePreUsedAmtToUse(params);

					} else {// 小于，需要释放多余额度,再修改预占用额度状态为占用
						CustCreditUseBean pre_use = new CustCreditUseBean();
						pre_use.setAmt(balance.abs());
						pre_use.setCustNo(creditUseBean.getCustNo());
						pre_use.setDealNo(creditUseBean.getDealNo());
						pre_use.setVdate(creditUseBean.getVdate());
						pre_use.setUseState(DictConstants.CREDIT_USE_STATE.PRE_USDED);
						// 释放预占用的多余额度
						quotaRelease(pre_use);
						// 修改预占用状态为占用
						tcCustCreditDealRelationMapper.updatePreUsedAmtToUse(params);

					}

				}
				System.out.println(StringUtils.equalsIgnoreCase(creditUseBean.getRefNo(), ""));
				creditUseBean.setDealNo(
						(creditUseBean.getRefNo() == null) ? creditUseBean.getDealNo() : creditUseBean.getRefNo());

				params = new HashMap<String, Object>();
				params.put("dealNo", creditUseBean.getDealNo());
				params.put("custNo", creditUseBean.getCustNo());
				params.put("useState", creditUseBean.getUseState());
				BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params);
				// 如果已经占用额度,交易第二次占用额度,不能重复占用所有额度,只能占用差额部分
				if (totalUsedAmt.compareTo(zero) > 0) {
					// 如果占用金额大于已经占用金额,则占用差额部分
					int flag = creditUseBean.getAmt().compareTo(totalUsedAmt);
					if (flag > 0) {
						creditUseBean.setAmt(creditUseBean.getAmt().subtract(totalUsedAmt));

					} else if (flag == 0) {
						// 和第一次占用额度金额相同,则直接返回占用成功
						if (!isTrue) {
							TcCustCreditMainLog tcCustCreditMainLog = createTcCreditMainLog(creditUseBean.getDealNo(),
									creditUseBean.getCustNo(), DictConstants.CREDIT_OPER_TYPE.USDED, null,
									creditUseBean.getAmt(), null, creditUseBean.getOffLine(), "额度占用");
							tcCustCreditMainLogMapper.insert(tcCustCreditMainLog);

							params = new HashMap<String, Object>();
							params.put("dealno", productApproveMain.getRefNo());
							List<TcCustCreditDetailLog> detaillogList = tcCustCreditDetailLogMapper
									.selectCustCreditDetailLogByDealno(params);
							List<TcCustCreditDetailLog> newDetailLogList = new ArrayList<TcCustCreditDetailLog>();
							for (TcCustCreditDetailLog custCredit : detaillogList) {
								custCredit.setDealNo(tcCustCreditMainLog.getDealNo());
								custCredit.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
								custCredit.setLogId(tcCustCreditMainLog.getLogId());
								newDetailLogList.add(custCredit);
							}
							tcCustCreditDetailLogMapper.insertDetailLogs(newDetailLogList);
						}
						return retMsg;

					} else {
						// 本次占用金额小于第一次占用金额,释放多余额度
						creditUseBean.setAmt(totalUsedAmt.subtract(creditUseBean.getAmt()));
						return creditQuotaRelease(creditUseBean);

					}
				}
			}

			params = new HashMap<String, Object>();
			params.put("custNo", creditUseBean.getCustNo());
			params.put("product_code", creditUseBean.getProduct_code());
			params.put("vdate", creditUseBean.getVdate());
			params.put("term", creditUseBean.getTermForMonth());
			// 查询该交易对手可用额度总和
			BigDecimal avlAmt = tcCustCreditMapper.queryCustCreditsAvlAmt(params);
			if (creditUseBean.getAmt().compareTo(avlAmt) > 0) {
				retMsg.setCode("000110");
				retMsg.setDesc("操作失败:可用额度不足");
				return retMsg;
			}

			// 获取对应的额度
			BigDecimal totalUseAmt = creditUseBean.getAmt();
			// 如果占用额度小于可用额度,则直接占用,否则需要占用其他额度
			BigDecimal useAmt = null;
			CustCreditBean creditBean = null;

			// map用于判断是否是串用额度
			// 配置的额度种类中第一个额度信息为应该占用的额度,其他期限的额度为串用额度
			HashMap<String, CustCreditBean> map = new HashMap<String, CustCreditBean>();
			CustCreditBean temp = null;

			List<CustCreditDealRelationBean> relationBeans = new ArrayList<CustCreditDealRelationBean>();
			CustCreditDealRelationBean relationBean = null;
			CustCreditBean tempBean = null;
			List<CustCreditBean> creditUseBeans = new ArrayList<CustCreditBean>();

			params = new HashMap<String, Object>();
			params.put("custNo", creditUseBean.getCustNo());
			params.put("product_code", creditUseBean.getProduct_code());
			params.put("term", creditUseBean.getTermForMonth());
			params.put("vdate", creditUseBean.getVdate());
			// 按顺序查询系统中配置过交易与额度类型对应关系的额度
			List<CustCreditBean> creditBeans = tcCustCreditMapper.queryCustCredits(params);
			Integer creditUseType = null;

			StringBuffer msg = new StringBuffer();

			params = new HashMap<String, Object>();
			params.put("trdtype", DictConstants.CREDIT_TYPE.RNO);
			String rno = trdOrderMapper.getOrderId(params);

			for (int i = 0; i < creditBeans.size(); i++) {
				creditBean = creditBeans.get(i);
				temp = map.get(creditBean.getCreditId());
				if (temp == null) {
					temp = creditBean;
					map.put(creditBean.getCreditId(), temp);
				}

				// 如果没有可用额度,则串用其他额度
				if (creditBean.getQuotaAvlAmt().compareTo(zero) == 0) {
					useAmt = zero;

				} else if (totalUseAmt.compareTo(creditBean.getQuotaAvlAmt()) > 0) {
					// 如果需要占用的额度大于当前额度记录可用额度，则占用全部可用额度
					useAmt = creditBean.getQuotaAvlAmt();

				} else {
					// 占用交易额度
					useAmt = totalUseAmt;
				}

				// 剩余未占用的额度
				totalUseAmt = totalUseAmt.subtract(useAmt);
				// 判断是否是串用额度
				if (temp.getTermId().equals(creditBean.getTermId())) {
					// 正常占用额度操作,如果还有剩余金额未占用额度，则剩余金额为串用其他额度的金额
					creditUseType = DictConstants.CREDIT_USE_TYPE.USE;

					temp.setQuotaStrAmt(zero);
					temp.setQuotaCstrAmt(zero);
					temp.setQuotaUsedAmt(useAmt);
					creditUseBeans.add(temp);

					msg.append("占用").append(creditBean.toString()).append("额度:").append(formatBigDecimal(useAmt))
							.append(nline);

				} else {
					// 串用的额度操作,占用的金额 为被串用的金额
					creditUseType = DictConstants.CREDIT_USE_TYPE.STRUSE;

					tempBean = new CustCreditBean();
					tempBean.setCustCreditId(creditBean.getCustCreditId());
					tempBean.setQuotaStrAmt(zero);
					tempBean.setQuotaCstrAmt(useAmt);
					tempBean.setQuotaUsedAmt(useAmt);
					creditUseBeans.add(tempBean);

					// 更新应该占用额度的串用金额
					temp.setQuotaStrAmt(temp.getQuotaStrAmt().add(useAmt));

					msg.append("串用").append(creditBean.toString()).append("额度:").append(formatBigDecimal(useAmt))
							.append(nline);
				}

				if (useAmt.compareTo(zero) != 0) {
					// 保存交易与额度占用关系
					relationBean = new CustCreditDealRelationBean(rno, creditUseBean.getDealNo(),
							creditUseBean.getCustNo(), creditUseBean.getProduct_code(), creditBean.getCustCreditId(),
							temp.getCustCreditId(), creditBean.getWeight(), DictConstants.CREDIT_RECORD_STATE.HISTORY_N,
							useAmt, zero, creditUseType, creditUseBean.getUseState(), creditUseBean.getVdate(),
							creditUseBean.getMdate(), DateUtil.getCurrentDateAsString(),
							DateUtil.getCurrentTimeAsString(), null);

					relationBean.setUpdateFlag(true);
					relationBean.setAmt(useAmt);

					// 计算当前额度剩余金额
					relationBean.setAvlAmt(creditBean.getQuotaAvlAmt().subtract(useAmt));
					relationBeans.add(relationBean);
				}

				if (totalUseAmt.compareTo(zero) <= 0) {
					// 如果额度占用完成则直接跳出循环
					break;
				}
			} // end for

			// 保存交易与额度关联关系
			if (relationBeans.size() > 0) {
				tcCustCreditDealRelationMapper.insertRelationBeans(relationBeans);

				TcCustCreditMainLog tcCustCreditMainLog = createTcCreditMainLog(creditUseBean.getDealNo(),
						creditUseBean.getCustNo(), DictConstants.CREDIT_OPER_TYPE.USDED, null, creditUseBean.getAmt(),
						null, creditUseBean.getOffLine(), "额度占用");
				tcCustCreditMainLogMapper.insert(tcCustCreditMainLog);

				tcCustCreditDetailLogMapper
						.insertDetailLogs(createCreditDetailLogs(relationBeans, tcCustCreditMainLog, null));

			}
			if (creditUseBeans.size() > 0) {
				tcCustCreditMapper.updateUseAmtForBatch(creditUseBeans);
			}
			retMsg.setDesc("额度占用成功");
			retMsg.setDetail(msg.toString());
			return retMsg;
		} catch (Exception e) {
			throw e;
		}
	}

	/****
	 * 
	 * 日志关系
	 * 
	 * @param dealNo         交易编号
	 * @param custNo         客户号
	 * @param operType       操作类型
	 * @param cust_credit_id 额度唯一编号
	 * @param amt            金额
	 * @param avlAmt         额度可用余额
	 * @param offLine        是否是线下交易
	 * @param remark         备注
	 * @return
	 */
	public TcCustCreditMainLog createTcCreditMainLog(String dealNo, String custNo, Integer operType,
			String cust_credit_id, BigDecimal amt, BigDecimal avlAmt, String offLine, String remark) {
		TcCustCreditMainLog mainLog = new TcCustCreditMainLog();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("trdtype", DictConstants.CREDIT_TYPE.CREDIT_LOG);
		mainLog.setLogId(trdOrderMapper.getOrderId(params));
		mainLog.setDealNo(dealNo);
		mainLog.setCustId(custNo);
		mainLog.setCustCreditId(cust_credit_id);
		mainLog.setOperType(operType);
		mainLog.setOperAmt(amt);
		mainLog.setAvlAmt(avlAmt);
		mainLog.setOffLine(offLine == null ? DictConstants.CREDIT_OFFLINE.NO : offLine);
		mainLog.setInputDate(DateUtil.getCurrentDateAsString());
		mainLog.setInputTime(DateUtil.getCurrentTimeAsString());
		mainLog.setRemark(remark);
		return mainLog;
	}

	private List<TcCustCreditDetailLog> createCreditDetailLogs(List<CustCreditDealRelationBean> relationBeans,
			TcCustCreditMainLog mainLog, String remark) {
		List<TcCustCreditDetailLog> list = new ArrayList<TcCustCreditDetailLog>();
		TcCustCreditDetailLog detailLog = null;
		CustCreditDealRelationBean relationBean = null;
		for (int i = 0; i < relationBeans.size(); i++) {
			relationBean = relationBeans.get(i);
			if (relationBean.isUpdateFlag()) {
				detailLog = new TcCustCreditDetailLog();
				detailLog.setDealNo(relationBean.getDealNo());
				detailLog.setCustCreditId(relationBean.getCustCreditId());
				detailLog.setOrgCustCreditId(relationBean.getOrgCustCreditId());
				detailLog.setSeq(i + 1);
				detailLog.setUseState(relationBean.getUseState());
				detailLog.setUseType(relationBean.getUseType());
				detailLog.setAmt(relationBean.getAmt());
				detailLog.setAvlAmt(relationBean.getAvlAmt());
				detailLog.setLogId(mainLog.getLogId());
				detailLog.setRemark(remark);
				list.add(detailLog);
			}
		}
		return list;
	}

	/**
	 * 通过客户查询可用明细
	 */
	@Override
	public Page<TcCustCreditMainLog> getLogByCustCreditId(Map<String, Object> map) {
		return mainLogMapper.getLogByCustCreditId(map, ParameterUtil.getRowBounds(map));
	}

	private String formatBigDecimal(BigDecimal b) {
		return nf.format(b.doubleValue());
	}

	/**
	 * 额度释放
	 */
	@Override
	public RetMsg<Object> creditQuotaRelease(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
		return quotaRelease(creditUseBean);
	}

	/**
	 * 额度释放
	 */
	@Override
	public RetMsg<Object> quotaRelease(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setCustNo(getInstId(creditUseBean.getCustNo()));
		selectCreditStateForLock(creditUseBean.getCustNo());

		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度释放成功", "", null);
		if (creditUseBean.isFinancial() == false) {
			CustCreditCompany c = new CustCreditCompany();
			c.setCustNo(creditUseBean.getCustNo());
			c.setAmt(creditUseBean.getAmt().doubleValue());
			c.setUseState(creditUseBean.getUseState());
			c.setInputDate(DateUtil.getCurrentDateAsString());
			c.setInputTime(DateUtil.getCurrentTimeAsString());
			c.setMdfDate(DateUtil.getCurrentDateTimeAsString());
			tcCustCreditCompanyMapper.insert(c);
			return retMsg;
		}

		BigDecimal totalReleaseAmt = creditUseBean.getAmt(); // 获取释放总金额
		// 判断是否有足够的释放金额
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("dealNo", creditUseBean.getDealNo());
		params.put("custNo", creditUseBean.getCustNo());
		params.put("useState", creditUseBean.getUseState());
		BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params);
		if (totalUsedAmt.doubleValue() <= 0) {
			return retMsg;
		}
		if (null != totalUsedAmt && totalUsedAmt.compareTo(totalReleaseAmt) < 0) {
			creditUseBean.setAmt(totalReleaseAmt);
			/*
			 * retMsg.setCode("000100");
			 * retMsg.setDesc("操作失败:当前客户占用额度为:"+totalUsedAmt+",小于释放的金额:"+totalReleaseAmt);
			 * return retMsg;
			 */
		}

		BigDecimal releaseAmt = null; // 当前释放额度
		BigDecimal quotaStrAmt = zero; // 串用额度
		BigDecimal quotaCstrAmt = zero; // 被串用额度
		CustCreditDealRelationBean relationBean = null;

		// 查询该笔交易占用的额度列表
		List<CustCreditDealRelationBean> relationBeans = tcCustCreditDealRelationMapper
				.selectCreditDealRelationsRelease(params);

		List<CustCreditDealRelationBean> tempRelationBeans = new ArrayList<CustCreditDealRelationBean>();
		CustCreditDealRelationBean tempRelatioBean = null;

		HashMap<String, CustCreditBean> tempCreditMap = new HashMap<String, CustCreditBean>();
		CustCreditBean tempCreditBean = null;

		for (int i = 0; i < relationBeans.size(); i++) {
			relationBean = relationBeans.get(i);
			if (totalReleaseAmt.compareTo(relationBean.getUseAmt()) > 0) {
				// 如果总释放金额大于当前占用额度金额,当前释放金额 = 当前占用额度金额
				releaseAmt = relationBean.getUseAmt();
			} else {
				// 如果总释放金额小于等于当前占用额度金额,当前释放金额 = 总释放金额
				releaseAmt = totalReleaseAmt;
			}
			quotaStrAmt = zero;
			quotaCstrAmt = zero;

			// 判断是否是被串用的额度
			if (DictConstants.CREDIT_USE_TYPE.STRUSE.equals(relationBean.getUseType())) {
				// 如果是被串用的额度 则需要更新额度表中被串用字段
				quotaStrAmt = zero;
				quotaCstrAmt = releaseAmt;

				// 对应的应该占用的额度的 串用额度字段需要更新
				// 根据OrgCustCreditId获取原额度信息
				if (relationBean.getOrgCustCreditId() != null) {
					tempCreditBean = tempCreditMap.get(relationBean.getOrgCustCreditId());
					if (tempCreditBean == null) {
						tempCreditBean = new CustCreditBean();
						tempCreditBean.setCustCreditId(relationBean.getOrgCustCreditId());
						tempCreditBean.setReleaseAmt(zero);
						tempCreditBean.setQuotaStrAmt(zero);
						tempCreditBean.setQuotaCstrAmt(zero);
						tempCreditMap.put(relationBean.getOrgCustCreditId(), tempCreditBean);
					}
					// 设置需要释放的串用金额
					tempCreditBean.setQuotaStrAmt(tempCreditBean.getQuotaStrAmt().add(releaseAmt));
				}

			}
			tempRelatioBean = new CustCreditDealRelationBean();
			tempRelatioBean.setDealNo(relationBean.getDealNo());
			tempRelatioBean.setCustCreditId(relationBean.getCustCreditId());
			tempRelatioBean.setOrgCustCreditId(relationBean.getOrgCustCreditId());
			tempRelatioBean.setUseState(DictConstants.CREDIT_USE_STATE.RELEASE);
			tempRelatioBean.setReleasedAmt(releaseAmt);
			// 对应额度记录的可用金额
			tempRelatioBean.setAvlAmt(releaseAmt);

			tempRelatioBean.setUpdateFlag(true);
			tempRelatioBean.setAmt(releaseAmt);
			tempRelationBeans.add(tempRelatioBean);

			tempCreditBean = tempCreditMap.get(relationBean.getCustCreditId());
			if (tempCreditBean == null) {
				tempCreditBean = new CustCreditBean();
				tempCreditBean.setCustCreditId(relationBean.getCustCreditId());
				tempCreditBean.setQuotaStrAmt(quotaStrAmt);
				tempCreditBean.setQuotaCstrAmt(quotaCstrAmt);
				tempCreditMap.put(relationBean.getCustCreditId(), tempCreditBean);
			}
			tempCreditBean.setReleaseAmt(releaseAmt);

			totalReleaseAmt = totalReleaseAmt.subtract(releaseAmt);
			if (totalReleaseAmt.compareTo(zero) <= 0) {
				// 如果额度释放完成则直接跳出循环
				break;
			}

		} // end for

		if (tempRelationBeans.size() > 0) {
			params = new HashMap<String, Object>();
			params.put("dealNo", creditUseBean.getDealNo());
			params.put("custNo", creditUseBean.getCustNo());
			params.put("relationBeans", tempRelationBeans);
			tcCustCreditDealRelationMapper.updateReleaseAmtBatch(params);

			TcCustCreditMainLog mainLog = createTcCreditMainLog(creditUseBean.getDealNo(), creditUseBean.getCustNo(),
					DictConstants.CREDIT_OPER_TYPE.RELEASE, null, creditUseBean.getAmt(), null,
					creditUseBean.getOffLine(), "额度释放");
			tcCustCreditMainLogMapper.insert(mainLog);

			tcCustCreditDetailLogMapper.insertDetailLogs(createCreditDetailLogs(tempRelationBeans, mainLog, null));

			if (tempCreditMap.size() > 0) {
				List<CustCreditBean> tempCreditBeans = new ArrayList<CustCreditBean>();
				tempCreditBeans.addAll(tempCreditMap.values());
				tcCustCreditMapper.updateReleaseAmtBatch(tempCreditBeans);
				// 额度解除串用
				creditQuotaReleaseStr(creditUseBean.getCustNo(), creditUseBean.getVdate(), mainLog);
			}
		}
		return retMsg;
	}

	/****
	 * 解除额度串用 增加某条额度可用金额后需要解除额度串用
	 * 
	 * @param custNo 客户号
	 * @param 调整的授信号 creditId
	 * @return
	 */
	public RetMsg<Object> creditQuotaReleaseStrByCreditId(String custNo, String creditId, TcCustCreditMainLog mainLog)
			throws Exception {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("custNo", custNo);
		// params.put("creditId",creditId);
		return creditQuotaReleaseStr(tcCustCreditDealRelationMapper.selectDealRelationsByCustCredit(params), mainLog);
	}

	/****
	 * 解除额度串用 释放某笔交易额度之后 需要解除与该笔交易有相同交易对手的交易的额度串用
	 * 
	 * @param custNo 客户号
	 * @param weight 起息日
	 * @return
	 */
	public RetMsg<Object> creditQuotaReleaseStr(String custNo, String vdate, TcCustCreditMainLog mainLog)
			throws Exception {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("custNo", custNo);
		params.put("vdate", vdate);
		return creditQuotaReleaseStr(tcCustCreditDealRelationMapper.selectDealRelationsByCust(params), mainLog);
	}

	/***
	 * 额度解除串用
	 * 
	 * @param relationBeans
	 * @return
	 */
	public RetMsg<Object> creditQuotaReleaseStr(List<CustCreditDealRelationBean> relationBeans,
			TcCustCreditMainLog mainLog) throws Exception {
		// 额度解除串用
		HashMap<String, CustCreditBean> orgCreditMap = null;
		HashMap<String, CustCreditBean> creditMapForUpdate = null;
		HashMap<String, CustCreditDealRelationBean> relationMap = null;
		CustCreditDealRelationBean relationBean = null;
		CustCreditBean tempCredit = null;
		CustCreditBean tempCredit_1 = null;
		CustCreditUseBean tempCreditUseBean = new CustCreditUseBean();
		CustCreditDealRelationBean tempRelationBean = null;
		String rno = null;
		List<CustCreditDealRelationBean> dealRelationBeans = null;
		HashMap<String, List<CustCreditDealRelationBean>> dealRelationMap = new LinkedHashMap<String, List<CustCreditDealRelationBean>>();
		// 根据交易对手和起息日查询出需要重新占用额度的交易,结果按起息日升序排序,结果包含所有有串用额度情况的交易
		HashMap<String, Object> params = new HashMap<String, Object>();
		List<CustCreditDealRelationBean> list = null;
		for (int i = 0; i < relationBeans.size(); i++) {
			list = dealRelationMap.get(relationBeans.get(i).getDealNo());
			if (list == null) {
				list = new ArrayList<CustCreditDealRelationBean>();
				dealRelationMap.put(relationBeans.get(i).getDealNo(), list);
			}
			list.add(relationBeans.get(i));
		}

		// 遍历每笔交易
		for (Entry<String, List<CustCreditDealRelationBean>> entry : dealRelationMap.entrySet()) {
			orgCreditMap = new HashMap<String, CustCreditBean>();
			creditMapForUpdate = new HashMap<String, CustCreditBean>();
			relationMap = new HashMap<String, CustCreditDealRelationBean>();

			dealRelationBeans = entry.getValue();
			tempCreditUseBean.setDealNo(entry.getKey());
			tempCreditUseBean.setCustNo(dealRelationBeans.get(0).getCustNo());
			tempCreditUseBean.setProduct_code(dealRelationBeans.get(0).getProductCode());
			tempCreditUseBean.setVdate(dealRelationBeans.get(0).getVdate());
			tempCreditUseBean.setMdate(dealRelationBeans.get(0).getMdate());

			rno = dealRelationBeans.get(0).getRno();
			// 交易的额度占用关系
			dealRelationBeans = entry.getValue();
			for (int i = 0; i < dealRelationBeans.size(); i++) {
				relationMap.put(dealRelationBeans.get(i).getCustCreditId(), dealRelationBeans.get(i));
			}

			params = new HashMap<String, Object>();
			params.put("custNo", tempCreditUseBean.getCustNo());
			params.put("product_code", tempCreditUseBean.getProduct_code());
			params.put("term", tempCreditUseBean.getTermForMonth());
			params.put("vdate", tempCreditUseBean.getVdate());
			// 按顺序查询系统中配置过交易与额度类型对应关系的额度
			List<CustCreditBean> creditBeans = tcCustCreditMapper.queryCustCredits(params);
			CustCreditBean credit = null;
			BigDecimal tempUseAmt = null;
			String orgCustCreditId = null;
			boolean isUpate = false;
			for (int i = 0; i < dealRelationBeans.size(); i++) {
				relationBean = dealRelationBeans.get(i);

				// 总占用金额
				if (relationBean.getUseAmt().compareTo(zero) == 0) {
					continue;
				}

				// 按照权重从高到低循环对应的授信额度
				for (int j = 0; j < creditBeans.size(); j++) {
					credit = creditBeans.get(j);
					// orgCreditMap中保存每个授信额度种类中权重最高的授信记录,用于区分是否是串用
					if (orgCreditMap.get(credit.getCreditId()) == null) {
						orgCreditMap.put(credit.getCreditId(), credit);
					}
					// 比较占用的额度权重 与查询的当前额度权重,如果占用的权重大于或等于当前额度权重 说明当前占用的额度不需要往前占用 则跳出循环
					if (relationBean.getWeight() >= credit.getWeight()) {
						break;

					} else {
						// 可用额度为0 继续下一个循环
						if (credit.getQuotaAvlAmt().compareTo(zero) == 0) {
							continue;
						}
						isUpate = true;

						// 当前占用金额小于等于 上一级额度可用金额 将当前授信金额全部往前移动
						if (relationBean.getUseAmt().compareTo(credit.getQuotaAvlAmt()) <= 0) {
							tempUseAmt = relationBean.getUseAmt();

						} else {// 当前占用金额大于上一级额度可用金额 则将当前占用金额移动一部分占用上一级额度
							tempUseAmt = credit.getQuotaAvlAmt();
						}

						// 原占用关系更新,释放原额度金额
						relationBean.setReleasedAmt(nullToZero(relationBean.getReleasedAmt()).add(tempUseAmt));
						relationBean.setAvlAmt(nullToZero(relationBean.getAvlAmt()).add(tempUseAmt));

						relationBean.setUseAmt(relationBean.getUseAmt().subtract(tempUseAmt));

						relationBean.setUseState(DictConstants.CREDIT_USE_STATE.RELEASE);
						relationBean.setUpdateFlag(true);
						relationBean.setAmt(tempUseAmt);

						// 释放
						tempCredit = creditMapForUpdate.get(relationBean.getCustCreditId());
						if (tempCredit == null) {
							tempCredit = new CustCreditBean();
							tempCredit.setCustCreditId(relationBean.getCustCreditId());
							tempCredit.setReleaseAmt(zero);
							tempCredit.setUseAmt(zero);
							tempCredit.setStrAmt(zero);
							tempCredit.setCstrAmt(zero);
							creditMapForUpdate.put(relationBean.getCustCreditId(), tempCredit);
						}
						tempCredit.setReleaseAmt(nullToZero(tempCredit.getReleaseAmt()).add(tempUseAmt));
						// 释放串用金额和被串用金额
						if (DictConstants.CREDIT_USE_TYPE.STRUSE.equals(relationBean.getUseType())) {
							tempCredit.setCstrAmt(nullToZero(tempCredit.getCstrAmt()).subtract(tempUseAmt));
							if (relationBean.getOrgCustCreditId() != null) {
								tempCredit_1 = creditMapForUpdate.get(relationBean.getOrgCustCreditId());
								if (tempCredit_1 == null) {
									tempCredit_1 = new CustCreditBean();
									tempCredit_1.setCustCreditId(relationBean.getOrgCustCreditId());
									tempCredit_1.setUseAmt(zero);
									tempCredit_1.setReleaseAmt(zero);
									tempCredit_1.setStrAmt(zero);
									tempCredit_1.setCstrAmt(zero);
									creditMapForUpdate.put(relationBean.getOrgCustCreditId(), tempCredit_1);
								}
								tempCredit_1.setStrAmt(nullToZero(tempCredit_1.getStrAmt()).subtract(tempUseAmt));
							}

						}

						// 新增占用关系

						// 应该占用的额度号
						orgCustCreditId = orgCreditMap.get(credit.getCreditId()).getCustCreditId();

						tempRelationBean = relationMap.get(credit.getCustCreditId());
						if (tempRelationBean == null) {
							tempRelationBean = new CustCreditDealRelationBean(rno, relationBean.getDealNo(),
									relationBean.getCustNo(), relationBean.getProductCode(), credit.getCustCreditId(),
									orgCustCreditId, credit.getWeight(), "N", zero, zero,
									credit.getCustCreditId().equals(orgCustCreditId) ? DictConstants.CREDIT_USE_TYPE.USE
											: DictConstants.CREDIT_USE_TYPE.STRUSE,
									DictConstants.CREDIT_USE_STATE.USDED, tempCreditUseBean.getVdate(),
									tempCreditUseBean.getMdate(), DateUtil.getCurrentDateAsString(),
									DateUtil.getCurrentTimeAsString(), null);

							relationMap.put(credit.getCustCreditId(), tempRelationBean);
						}
						tempRelationBean.setUseAmt(nullToZero(tempRelationBean.getUseAmt()).add(tempUseAmt));
						// 设置可用金额
						tempRelationBean.setAvlAmt(nullToZero(credit.getQuotaAvlAmt()).subtract(tempUseAmt));

						tempRelationBean.setUpdateFlag(true);
						tempRelationBean.setAmt(tempUseAmt);
						// MODIFY 20170831 LOUHQ
						relationMap.put(tempRelationBean.getCustCreditId(), tempRelationBean);
						// 新增额度占用,更新额度信息
						tempCredit = creditMapForUpdate.get(credit.getCustCreditId());
						if (tempCredit == null) {
							tempCredit = credit;
							tempCredit.setReleaseAmt(zero);
							tempCredit.setUseAmt(zero);
							tempCredit.setStrAmt(zero);
							tempCredit.setCstrAmt(zero);
							creditMapForUpdate.put(tempCredit.getCustCreditId(), tempCredit);
						}
						tempCredit.setUseAmt(nullToZero(tempCredit.getUseAmt()).add(tempUseAmt));
						credit.setQuotaAvlAmt(nullToZero(credit.getQuotaAvlAmt()).subtract(tempUseAmt));
						// 是被串用额度
						if (!tempCredit.getCustCreditId().equals(orgCustCreditId)) {
							// 被串用额度增加
							tempCredit.setCstrAmt(nullToZero(credit.getCstrAmt()).add(tempUseAmt));
							// 串用额度增加
							tempCredit_1 = creditMapForUpdate.get(orgCustCreditId);
							if (tempCredit_1 == null) {
								tempCredit_1 = new CustCreditBean();
								tempCredit_1.setCustCreditId(orgCustCreditId);
								tempCredit_1.setUseAmt(zero);
								tempCredit_1.setReleaseAmt(zero);
								tempCredit_1.setStrAmt(zero);
								tempCredit_1.setCstrAmt(zero);
								creditMapForUpdate.put(orgCustCreditId, tempCredit_1);
							}
							tempCredit_1.setStrAmt(nullToZero(tempCredit_1.getStrAmt()).add(tempUseAmt));
						}

						// 如果当前额度已经全部释放 则跳循环
						if (relationBean.getUseAmt().compareTo(zero) <= 0) {
							break;
						}

					}

				} // end for 额度信息循环
			} // end for 单笔交易占用关系循环

			if (isUpate) {
				if (relationMap.size() > 0) {
					params = new HashMap<String, Object>();
					params.put("dealNo", tempCreditUseBean.getDealNo());
					params.put("custNo", tempCreditUseBean.getCustNo());
					tcCustCreditDealRelationMapper.deleteRelationByDealno(params);

					dealRelationBeans = new ArrayList<CustCreditDealRelationBean>();
					dealRelationBeans.addAll(relationMap.values());
					tcCustCreditDealRelationMapper.insertRelationBeans(dealRelationBeans);

					// 保存日志
					tcCustCreditDetailLogMapper
							.insertDetailLogs(createCreditDetailLogs(dealRelationBeans, mainLog, "额度解串跟踪"));
				}

				if (creditMapForUpdate.size() > 0) {
					creditBeans = new ArrayList<CustCreditBean>();
					creditBeans.addAll(creditMapForUpdate.values());
					tcCustCreditMapper.updateCreditBatch(creditBeans);
				}
			}
		} // end for 交易循环

		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度释放成功", "", null);
		return retMsg;
	}

	/****
	 * 从客户额度信息列表中筛选未到期的额度子列表 筛选条件为custNo,product_code,term,vdate
	 * 
	 * @param creditBeans
	 * @return
	 */
	public List<CustCreditBean> querySubCreditList(List<CustCreditBean> creditBeans) {

		return null;
	}

	/****
	 * 根据客户号获取 该客户应该占用的额度号
	 * 
	 * @param custNo
	 * @return
	 */
	private String getInstId(String custNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("instId", custNo);
		List<TcInstCredit> list = tcInstCreditMapper.getInstCreditList(map);
		if (list.size() == 0) {
			return custNo;
		} else {
			return list.get(0).getSubInstId();
		}
	}

	private BigDecimal nullToZero(BigDecimal o) {
		return o == null ? zero : o;
	}

	@Override
	public Page<CustCreditBean> getCustCreditList(Map<String, Object> map) {
		Page<CustCreditBean> list = tcCustCreditMapper.getCustCreditList(map, ParameterUtil.getRowBounds(map));
		return list;
	}

	@Override
	public Page<CustCreditBean> getCreditListByCust(Map<String, Object> map) {
		return tcCustCreditMapper.getCreditListByCust(map, ParameterUtil.getRowBounds(map));
	}

	// 切分接口
	@Override
	public RetMsg<Object> creditQuotaAsegForCrms(List<QuotaSyncopateRec> quotaSyncopateRecs) throws Exception {
		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度切分成功", "");

		// 解析接口参数
		List<CustCreditBean> tempList = new ArrayList<CustCreditBean>();
		CustCreditBean tempCust = new CustCreditBean();
		CustCreditBean custCredit = new CustCreditBean();
		QuotaSyncopateRec quotaS = new QuotaSyncopateRec();
		BigDecimal temp = null;
		for (int i = 0; i < quotaSyncopateRecs.size(); i++) {
			quotaS = quotaSyncopateRecs.get(i);
			tempCust.setCustNo(quotaS.getEcifNum());
			tempCust.setCreditId(quotaS.getProductType());
			String term = getTermId(quotaS.getTerm(), quotaS.getTermType());
			tempCust.setTermId(term);
			temp = new BigDecimal(quotaS.getLoanAmt());
			tempCust.setCreditAmt(temp);
			tempCust.setQuotaAvlAmt(temp);
			tempCust.setExpDate(quotaS.getDueDate());
			tempCust.setCntrStatus(quotaS.getCntrStatus());
			tempCust.setRemark1(quotaS.getSector() + "|" + quotaS.getTerm() + "|" + quotaS.getTermType());
			tempCust.setCurrency(quotaS.getCurrency());

			/*
			 * tempCust.setEcifNum(quotaS.getCustomer());
			 * 
			 * //去重存放数据 //检查同一客户额度是否已存在 custCredit =
			 * tcCustCreditTempMapper.queryCustCreditByInfo(tempCust); if(custCredit ==
			 * null) { //存放切分数据至临时表 tcCustCreditTempMapper.addCustCreditTemp(tempCust); }
			 * else { //修改临时表中对应客户额度 tcCustCreditTempMapper.updateCustCreditTemp(tempCust);
			 * }
			 */

//			tempCust.setEcifNum(quotaS.getCustomer());

			// Customer 传来的是CRMS系统中该客户对应的客户号，而非ECIF号
			tempCust.setEcifNum(quotaS.getEcifNum());

			// 满足切分系统是同业切额度类型在我们的范围内，才将其筛选至临时表中
			// 如果切分系统是同业系统
			if ("01".equals(quotaS.getSysId())) {
				// 如果传来的额度类型不在我们的中类中
				if (tcCustCreditTempMapper.queryCreditType(tempCust) > 0) {
					// 去重存放数据
					// 检查同一客户额度是否已存在
					custCredit = tcCustCreditTempMapper.queryCustCreditByInfo(tempCust);
					if (custCredit == null) {
						// 存放切分数据至临时表
						tcCustCreditTempMapper.addCustCreditTemp(tempCust);
					} else {
						// 修改临时表中对应客户额度
						tcCustCreditTempMapper.updateCustCreditTemp(tempCust);
					}
				}
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cust_no", quotaS.getEcifNum());
		// 拿到切分额度信息和对应的客户信息，额度信息
		tempList = tcCustCreditTempMapper.getCustCreditTemp(map);
		// 检查同步的数据是否存在问题
		for (int i = 0; i < tempList.size(); i++) {
			tempCust = tempList.get(i);
			// 检查客户是否存在
			if (tempCust.getCustName() == null || "".equals(tempCust.getCustName().trim())) {
				retMsg.setCode("001410");
				retMsg.setDesc("切分失败:切分客户不存在");
				return retMsg;
			}
			// 检查额度是否存在
			if (tempCust.getCreditName() == null || "".equals(tempCust.getCreditName().trim())) {
				retMsg.setCode("001411");
				retMsg.setDesc("切分失败:切分额度类型不存在");
				return retMsg;
			}
			custCredit = tcCustCreditMapper.queryCustCreditByInfo(tempCust);
			// 如果减少切分,减少金额大于已经切分金额，返回
			if (custCredit != null && tempCust.getCreditAmt().compareTo(zero) > 0
					&& tempCust.getCreditAmt().compareTo(custCredit.getCreditAmt()) < 0 && custCredit.getCreditAmt()
							.subtract(tempCust.getCreditAmt()).compareTo(custCredit.getQuotaAvlAmt()) > 0) {
				retMsg.setCode("000130");
				retMsg.setDesc("操作失败:新授信减少额度大于可用的额度");
				return retMsg;
			}
		}

		// 切分
		for (int i = 0; i < tempList.size(); i++) {
			tempCust = tempList.get(i);
			custCredit = tcCustCreditMapper.queryCustCreditByInfo(tempCust);

			if (custCredit != null) {
				/****
				 * 正：增加切分（小于等于可用于切分金额）；负：减少切分（小于等于已切分金额）
				 */
				// 如果增加切分，增加金额大于可用额度，返回
//				if ((tempCust.getCreditAmt().compareTo(zero) > 0)
//						&& (tempCust.getCreditAmt().compareTo(
//								custCredit.getCreditAmt()) < 0)) {
//					retMsg.setCode("000120");
//					retMsg.setDesc("操作失败:新授信总额度小于已授信总额度");
//					return retMsg;
//				}

				// 切分额度
//				Map<String, Object> params = new HashMap<String, Object>();
//				params.put("custCreditId", custCredit.getCustCreditId());
//				params.put("segAmt", tempCust.getCreditAmt());
//				tcCustCreditMapper.updateQuotaAseg(params);
				// 得到可用额度
				BigDecimal avlAmt = custCredit.getQuotaAvlAmt()
						.subtract(custCredit.getCreditAmt().subtract(tempCust.getCreditAmt()));
				// .subtract(tempCust.getCreditAmt());
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("custCreditId", custCredit.getCustCreditId());
				params.put("QUOTAAVL_AMT", avlAmt);
				params.put("CREDIT_AMT", tempCust.getCreditAmt());
				params.put("EXPDATE", tempCust.getExpDate());
				tcCustCreditMapper.updateQuotaAvl(params);

				TcCustCreditMainLog creditMainLog = createTcCreditMainLog(null, custCredit.getCustNo(),
						DictConstants.CREDIT_OPER_TYPE.ASEG, custCredit.getCustCreditId(), tempCust.getCreditAmt(),
						avlAmt, null, "额度切分");
				tcCustCreditMainLogMapper.insert(creditMainLog);
				// 减少切分,可用额度增加,需要解除额度串用
//				if (tempCust.getCreditAmt().compareTo(zero) <= 0) {
				creditQuotaReleaseStrByCreditId(custCredit.getCustNo(), custCredit.getCreditId(), creditMainLog);
//				}
			} else {
				// 添加数据至客户额度表
				tcCustCreditMapper.insertCustCreditToDb(tempCust);
				// 拿到额度表中id
				custCredit = tcCustCreditMapper.queryCustCreditByInfo(tempCust);

				TcCustCreditMainLog creditMainLog = new TcCustCreditMainLog();
				// 解串
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("custNo", custCredit.getCustNo());
				params.put("creditId", custCredit.getCreditId());
				creditQuotaReleaseStr(tcCustCreditDealRelationMapper.selectDealRelationsByCustCredit(params),
						creditMainLog);
			}

//			return retMsg;
		}

//		tcCustCreditDealRelationMapper.cleanCreditLock();
//		tcCustCreditDealRelationMapper.synchCreditLock();
		return retMsg;
	}

	/**
	 * 增加额度品种会影响之前的交易
	 * 
	 * @param productCode,creditId
	 */
	@SuppressWarnings("unused")
	@Override
	public RetMsg<Object> addCreditId(String productCode, String creditId) {
		RetMsg<Object> retMsg = new RetMsg<Object>(RetMsgHelper.codeOk, "额度品种增加成功", "");
		try {
			// 拿到增加额度品种的产品会影响到的客户。
			List<String> custList = tcCustCreditDealRelationMapper.getDealRelationsByProduct(productCode);

			for (int i = 0; i < custList.size(); i++) {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("custNo", custList.get(i));
				params.put("creditId", creditId);

				TcCustCreditMainLog creditMainLog = new TcCustCreditMainLog();
				// 解串所有额度产品影响的交易
				return creditQuotaReleaseStr(tcCustCreditDealRelationMapper.selectDealRelationsByCustCredit(params),
						creditMainLog);

			}
		} catch (Exception e) {
			// e.printStackTrace();
			retMsg.setCode("000130");
			retMsg.setDesc("操作失败:增加额度品种失败");
			return retMsg;
		}
		return retMsg;
	}

	public RetMsg<Object> updateCreditSort() {
		return null;
	}

	@Override
	public RetMsg<Object> creditQuotaUsedForCrms(Map<String, Object> params) throws Exception {
		try {
			RetMsg<Object> ret = new RetMsg<Object>("000000", "处理成功");
			/****
			 * LoanSummaryId 借据号（LD号） CustBrNo 业务流水号（同业系统） TransDesc 合同号 LoanAmt 放款金额
			 * Currency 币种 LoanType 放款类型 正常放款（01），修改（02），冲正（03） CrmsLoanId 放款号 InfoFlag 通知类型
			 */
			CustCreditUseBean creditUseBean = new CustCreditUseBean();
			creditUseBean.setDealNo(String.valueOf(params.get("CustBrNo")));
			creditUseBean.setAmt(new BigDecimal(String.valueOf(params.get("LoanAmt"))));
			creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);

			TdProductApproveMain main = productApproveService.getProductApproveActivated(creditUseBean.getDealNo());
			if (main == null) {
				ret.setCode("000001");
				ret.setDesc("交易" + creditUseBean.getDealNo() + "不存在");
				return ret;
			}
			main.setCrmsLoadId(params.get("CrmsLoanId").toString());
			if (params.get("LoanSummaryId") == null || "".equals(params.get("LoanSummaryId"))) {
				main.setLdNo("");
			} else {
				main.setLdNo(String.valueOf(params.get("LoanSummaryId")));
			}
			productApproveService.updateMainRefNo(main);// 更新LD编号，如果存在则更新;

			creditUseBean.setCustNo(main.getcNo());
			creditUseBean.setMdate(main.getmDate());
			creditUseBean.setVdate(main.getvDate());
			creditUseBean.setProduct_code(String.valueOf(main.getPrdNo()));
			// 298 企信
			creditUseBean.setFinancial(main.getPrdNo() == 298 ? false : true);// 企信只做记录
			String LoanType = String.valueOf(params.get("LoanType"));
			String InfoFlag = String.valueOf(params.get("InfoFlag"));
			// 正常放款,额度占用,交互完成 NORMAL_PAY + INTER_ACTIVE_SUCCESS
			if (DictConstants.LoanType.NORMAL_PAY.equals(LoanType)
					&& DictConstants.InfoFlag.INTER_ACTIVE_SUCCESS.equals(InfoFlag)) {
				// 交互成功的时候 预占用 额度，因为后续的多次放款也不能比第一次放款金额大；- 业务确认
				HashMap<String, Object> params1 = new HashMap<String, Object>();
				params1.put("dealNo", creditUseBean.getDealNo());
				params1.put("custNo", creditUseBean.getCustNo());
				params1.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
				if (totalUsedAmt.compareTo(zero) > 0) {
					// 预占用金额-修改金额 小于0,返回失败
					if (new BigDecimal(
							PlaningTools.sub(totalUsedAmt.doubleValue(), creditUseBean.getAmt().doubleValue()))
									.compareTo(zero) < 0) {
						ret.setCode("000002");
						ret.setDesc("再次放款额度必须小于已预占用额度");
						return ret;
					}
				} else {
					return creditQuotaPreUsed(creditUseBean);
				}
			} else if (DictConstants.LoanType.ADJUST_PAY.equals(LoanType)
					&& DictConstants.InfoFlag.INTER_ACTIVE_SUCCESS.equals(InfoFlag)) {
				// 都不做处理；除了需要判定额度修改值是否与第一次修改值有差
				// 查看是否有预占用金额 修改金额对于原预占用金额，返回失败
				HashMap<String, Object> params1 = new HashMap<String, Object>();
				params1.put("dealNo", creditUseBean.getDealNo());
				params1.put("custNo", creditUseBean.getCustNo());
				params1.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
				if (new BigDecimal(PlaningTools.sub(totalUsedAmt.doubleValue(), creditUseBean.getAmt().doubleValue()))
						.compareTo(zero) < 0) {// 预占用金额-修改金额 小于0,返回失败
					ret.setCode("000002");
					ret.setDesc("修改后的额度必须小于已预占用额度");
					return ret;
				}
				/*
				 * else{//额度已经占用,需要释放多余的额度 //查询已经占用的额度,修改后的额度必须小于已占用额度 params1.put("useState",
				 * DictConstants.CREDIT_USE_STATE.USDED); totalUsedAmt =
				 * tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
				 * if(totalUsedAmt.compareTo(creditUseBean.getAmt()) < 0) {
				 * ret.setCode("000002"); ret.setDesc("修改后的额度必须小于已占用额度"); return ret;
				 * 
				 * }else{ //释放多余额度
				 * creditUseBean.setAmt(totalUsedAmt.subtract(creditUseBean.getAmt())); return
				 * quotaRelease(creditUseBean);
				 * 
				 * } }
				 */
				// 正常放款 复核通过，说明CRMS联动T24成功，额度从预占用转为占用
			} else if (DictConstants.LoanType.NORMAL_PAY.equals(LoanType)
					&& DictConstants.InfoFlag.VERIFY_SUCCESS.equals(InfoFlag)) {
				HashMap<String, Object> params1 = new HashMap<String, Object>();
				params1.put("dealNo", creditUseBean.getDealNo());
				params1.put("custNo", creditUseBean.getCustNo());
				params1.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
				if (totalUsedAmt.compareTo(creditUseBean.getAmt()) < 0) {
					ret.setCode("000002");
					ret.setDesc("确认放款的金额必须小于预占用放款金额");
					return ret;

				} else {
					// 预占用转 占用
					RetMsg<Object> retMsg = quotaUsed(creditUseBean);
					if (retMsg.getCode().equalsIgnoreCase(RetMsgHelper.codeOk)) {
						// 释放多余额度
						if (totalUsedAmt.compareTo(creditUseBean.getAmt()) > 0) {
							creditUseBean.setAmt(totalUsedAmt.subtract(creditUseBean.getAmt()));
							return quotaRelease(creditUseBean);
						}
					}

				}
				// 冲正,释放交易 冲正复核通过 说明交易正式结束，释放额度
			} else if (DictConstants.LoanType.REVERSE_PAY.equals(LoanType)
					&& DictConstants.InfoFlag.VERIFY_SUCCESS.equals(InfoFlag)) {
				// 判断目前交易是否处在预占用
				HashMap<String, Object> params1 = new HashMap<String, Object>();
				params1.put("dealNo", creditUseBean.getDealNo());
				params1.put("custNo", creditUseBean.getCustNo());
				params1.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
				BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
				if (totalUsedAmt.compareTo(zero) > 0) {
                    return creditQuotaPreUsedRelease(creditUseBean);
                } else {
					params1.put("useState", DictConstants.CREDIT_USE_STATE.USDED);
					totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
					if (totalUsedAmt.compareTo(zero) > 0) {
						return quotaRelease(creditUseBean);
					}
				}

			}
			return ret;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public RetMsg<Object> creditQuotaReleaseForCrms(Map<String, Object> params) throws Exception {
		RetMsg<Object> ret = new RetMsg<Object>("000000", "交易成功");
		/*****
		 * LoanSummaryId 借据号 A 40 RetAmt 还款金额 DECIMAL 26,8 Currency 币种 A 20
		 */
		Map<String, Object> param1 = new HashMap<String, Object>();
		param1.put("isActive", com.singlee.capital.system.dict.DictConstants.YesNo.YES);
		param1.put("ldNo", String.valueOf(params.get("LoanSummaryId")));
		TdProductApproveMain main = productApproveService.getProductApproveByCondition(param1);
		if (main == null) {
			ret.setCode("000001");
			ret.setDesc("借据号为" + String.valueOf(params.get("LoanSummaryId")) + "的交易不存在");
			return ret;
		}

		CustCreditUseBean creditUseBean = new CustCreditUseBean();
		creditUseBean.setDealNo(main.getDealNo());
		creditUseBean.setCustNo(main.getcNo());
		creditUseBean.setAmt(new BigDecimal(String.valueOf(params.get("RetAmt"))));

		// 查看是否有预占用金额
		HashMap<String, Object> params1 = new HashMap<String, Object>();
		params1.put("dealNo", creditUseBean.getDealNo());
		params1.put("custNo", creditUseBean.getCustNo());
		params1.put("useState", DictConstants.CREDIT_USE_STATE.PRE_USDED);
		BigDecimal totalUsedAmt = tcCustCreditDealRelationMapper.queryUsedAmtByCustNo(params1);
		if (totalUsedAmt.compareTo(zero) > 0) {// 有预占用金额则释放预占用金额
			creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.PRE_USDED);

		} else {// 额度已经占用,需要释放占用金额
			creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);

		}
		return quotaRelease(creditUseBean);
	}

	// 批复信息查询（同业->CRMS）
	@Override
	public void creditApprovalInq(ApprovalInqRs approvalInqRs, String ecifno) {
		// 解析报文数据
		for (ApprovalInqRec rec : approvalInqRs.getApprovalInqRec()) {

			TcCreditApproveInfo creditApproveInfo = new TcCreditApproveInfo();
			creditApproveInfo.setPartyName(rec.getPartyName());
			creditApproveInfo.setClientNo(rec.getClientNo());
			creditApproveInfo.setOrgNum(rec.getOrgNum());
			creditApproveInfo.setUserNum(rec.getUserNum());
			creditApproveInfo.setApproveId(rec.getApproveId());
			creditApproveInfo.setOriBatchNo(rec.getOriBatchNo());
			creditApproveInfo.setProductType(rec.getProductType());
			creditApproveInfo.setAccountProperty(rec.getAccountProperty());
			creditApproveInfo.setLoanAmt(Double.parseDouble(rec.getLoanAmt()));
			creditApproveInfo.setLoanCreditAmt(Double.parseDouble(rec.getLoanCreditAmt()));
			creditApproveInfo.setLyed(Double.parseDouble(rec.getLyed()));
			creditApproveInfo.setLoanApplyed(Double.parseDouble(rec.getLoanApplyed()));
			creditApproveInfo.setCurrency(rec.getCurrency());
			creditApproveInfo.setTenor(rec.getTenor());
			creditApproveInfo.setExtenTermUnit(rec.getExtenTermUnit());
			creditApproveInfo.setSameCustName(rec.getSameCustName());
			creditApproveInfo.setValidDate(rec.getValidDate());
			creditApproveInfo.setRateType(rec.getRateType());
			creditApproveInfo.setRateReceiveType(rec.getRateReceiveType());
			creditApproveInfo.setRate(Double.parseDouble(rec.getRate()));
			creditApproveInfo.setFinalApprvResult(rec.getFinalApprvResult());
			creditApproveInfo.setSiteNo(rec.getSiteNo());
			creditApproveInfo.setLimitType(rec.getLimitType());
			creditApproveInfo.setLoanAssuKind(rec.getLoanAssuKind());
			creditApproveInfo.setEcifNo(ecifno);
			// 判断数据是否已存在
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("clientNo", creditApproveInfo.getClientNo());
			map.put("productType", creditApproveInfo.getProductType());
			map.put("tenor", creditApproveInfo.getTenor());
			map.put("extenTermUnit", creditApproveInfo.getExtenTermUnit());
			TcCreditApproveInfo creditApproveInfos = tcCreditApproveInfoMapper.getByClientNo(map);
			if (creditApproveInfos == null) {
				// 如果不存在，新增数据
				tcCreditApproveInfoMapper.createCreditApproveInfo(creditApproveInfo);
			} else {
				// 如果已存在，修改数据
				tcCreditApproveInfoMapper.updateCreditApproveInfo(creditApproveInfo);
			}
		}

	}

	// 批复信息变更（CRMS->同业）
	@SuppressWarnings("unused")
	@Override
	public RetMsg<Object> creditApprovalChange(List<ApprovalChangeRec> approveList) {
		RetMsg<Object> ret = new RetMsg<Object>("000000", "交易成功");

		// 解析接口参数
		List<TcCreditApproveInfo> tempList = new ArrayList<TcCreditApproveInfo>();
		TcCreditApproveInfo creditApproveInfo = new TcCreditApproveInfo();
		// 解析接口参数
		for (int i = 0; i < approveList.size(); i++) {
			creditApproveInfo.setPartyName(approveList.get(i).getPartyName());
			creditApproveInfo.setClientNo(approveList.get(i).getClientNo());
			creditApproveInfo.setOrgNum(approveList.get(i).getOrgNum());
			creditApproveInfo.setUserNum(approveList.get(i).getUserNum());
			creditApproveInfo.setApproveId(approveList.get(i).getApproveId());
			creditApproveInfo.setOriBatchNo(approveList.get(i).getOriBatchNo());
			creditApproveInfo.setProductType(approveList.get(i).getProductType());
			creditApproveInfo.setAccountProperty(approveList.get(i).getAccountProperty());
			creditApproveInfo.setLoanAmt(Double.parseDouble(
					approveList.get(i).getLoanAmt() == null || "".equals(approveList.get(i).getLoanAmt()) ? "0"
							: approveList.get(i).getLoanAmt()));
			creditApproveInfo.setLoanCreditAmt(Double.parseDouble(
					approveList.get(i).getLoanCreditAmt() == null || "".equals(approveList.get(i).getLoanCreditAmt())
							? "0"
							: approveList.get(i).getLoanCreditAmt()));
			creditApproveInfo.setLyed(Double
					.parseDouble(approveList.get(i).getLyed() == null || "".equals(approveList.get(i).getLyed()) ? "0"
							: approveList.get(i).getLyed()));
			creditApproveInfo.setLoanApplyed(Double.parseDouble(
					approveList.get(i).getLoanApplyed() == null || "".equals(approveList.get(i).getLoanApplyed()) ? "0"
							: approveList.get(i).getLoanApplyed()));
			creditApproveInfo.setCurrency(approveList.get(i).getCurrency());
			creditApproveInfo.setTenor(approveList.get(i).getTenor());
			creditApproveInfo.setExtenTermUnit(approveList.get(i).getExtenTermUnit());
			creditApproveInfo.setSameCustName(approveList.get(i).getSameCustName());
			creditApproveInfo.setValidDate(approveList.get(i).getValidDate());
			creditApproveInfo.setRateType(approveList.get(i).getRateType());
			creditApproveInfo.setRateReceiveType(approveList.get(i).getRateReceiveType());
			creditApproveInfo.setRate(Double
					.parseDouble(approveList.get(i).getRate() == null || "".equals(approveList.get(i).getRate()) ? "0"
							: approveList.get(i).getRate()));
			creditApproveInfo.setFinalApprvResult(approveList.get(i).getFinalApprvResult());
			creditApproveInfo.setSiteNo(approveList.get(i).getSiteNo());
			creditApproveInfo.setLimitType(approveList.get(i).getLimitType());
			creditApproveInfo.setLoanAssuKind(approveList.get(i).getLoanAssuKind());

			// 判断数据是否已存在
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("clientNo", creditApproveInfo.getClientNo());
			map.put("productType", creditApproveInfo.getProductType());
			map.put("tenor", creditApproveInfo.getTenor());
			map.put("extenTermUnit", creditApproveInfo.getExtenTermUnit());
			TcCreditApproveInfo creditApproveInfos = tcCreditApproveInfoMapper.getByClientNo(map);
			if (creditApproveInfos == null) {
				// 如果不存在，新增数据
				tcCreditApproveInfoMapper.createCreditApproveInfo(creditApproveInfo);
			} else {
				// 如果已存在，修改数据
				tcCreditApproveInfoMapper.updateCreditApproveInfo(creditApproveInfo);
			}
		}
		return ret;
	}

	@Override
	public List<TcCustCreditMainLog> getCustCreditMainLog(Map<String, Object> map) {
		return mainLogMapper.selectTcCustCreditMainLogs(map);
	}

	@Override
	public List<TcCustCreditDetailLog> getCustCreditDetailLog(Map<String, Object> map) {
		return detailLogMapper.selectTcCustCreditDetailLog(map);
	}

	@Override
	public List<CustCreditBean> getCreditListByCustNo(Map<String, Object> map) {
		return tcCustCreditMapper.getCreditListByCustNo(map);
	}

	@Override
	public RetMsg<Object> creditQuotaUsedForOffLine(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
		creditUseBean.setOffLine(DictConstants.CREDIT_OFFLINE.YES);
		return quotaUsed(creditUseBean);
	}

	@Override
	public RetMsg<Object> creditQuotaReleaseForOffLine(CustCreditUseBean creditUseBean) throws Exception {
		creditUseBean.setUseState(DictConstants.CREDIT_USE_STATE.USDED);
		creditUseBean.setOffLine(DictConstants.CREDIT_OFFLINE.YES);
		return quotaRelease(creditUseBean);
	}

	@Override
	public List<CustCreditBean> getCustCreditListAll(Map<String, Object> map) {
		return tcCustCreditMapper.getCustCreditListAll(map);
	}

	public void log(String info) {
		System.out.println(info);
	}

	public String getTermId(String term, String termType) {
		if (term == null || termType == null) {
			return "";
		}

		String termT = "";
		if ("01".equals(termType)) {
			termT = "Y";
		} else if ("02".equals(termType)) {
			termT = "V";
		} else if ("03".equals(termType)) {
			termT = "Q";
		} else if ("04".equals(termType)) {
			termT = "M";
		} else if ("05".equals(termType)) {
			termT = "D";
		}

		return term + termT;

	}

}
