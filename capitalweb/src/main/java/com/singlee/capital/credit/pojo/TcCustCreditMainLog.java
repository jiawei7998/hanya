package com.singlee.capital.credit.pojo;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="TC_CUST_CREDIT_MAIN_LOG")
public class TcCustCreditMainLog {
	@Id
	private String     logId         ; 
	private String     dealNo        ; 
	private String     custId       ; 
	private String     custCreditId; 
	private Integer     operType     ; 
	private BigDecimal operAmt      ;
	private String     offLine;
	private String     inputDate;
	private String     inputTime;
	private String     remark        ;
	private BigDecimal      avlAmt;
	
	public BigDecimal getAvlAmt() {
		return avlAmt;
	}
	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getLogId() {
		return logId;
	}
	public void setLogId(String logId) {
		this.logId = logId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	public Integer getOperType() {
		return operType;
	}
	public void setOperType(Integer operType) {
		this.operType = operType;
	}
	public BigDecimal getOperAmt() {
		return operAmt;
	}
	public void setOperAmt(BigDecimal operAmt) {
		this.operAmt = operAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getOffLine() {
		return offLine;
	}
	public void setOffLine(String offLine) {
		this.offLine = offLine;
	}
	@Override
	public String toString() {
		return "TcCustCreditMainLog [logId=" + logId + ", dealNo=" + dealNo
				+ ", custId=" + custId + ", custCreditId=" + custCreditId
				+ ", operType=" + operType + ", operAmt=" + operAmt
				+ ", inputDate=" + inputDate + ", inputTime=" + inputTime
				+ ", remark=" + remark + "]";
	}
	
}
