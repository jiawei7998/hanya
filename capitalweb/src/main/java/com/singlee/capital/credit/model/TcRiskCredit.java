package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 额度品种风险级序
 * @author liyimin
 *
 */
@Entity
@Table(name = "TC_RISK_CREDIT")
public class TcRiskCredit implements Serializable {

	private static final long serialVersionUID = 1L;

	// 额度代码
	@Id
	private String creditId;
	
	// 额度名称
	private String creditName;
	
	// 风险级序
	private Integer riskLevel;
	
	// 风险权重
	private Integer wValue;

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public Integer getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(Integer riskLevel) {
		this.riskLevel = riskLevel;
	}

	public Integer getWValue() {
		return wValue;
	}

	public void setWValue(Integer wValue) {
		this.wValue = wValue;
	}
}
