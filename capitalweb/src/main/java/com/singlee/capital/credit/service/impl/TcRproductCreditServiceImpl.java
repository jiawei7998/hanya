package com.singlee.capital.credit.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcRproductCreditMapper;
import com.singlee.capital.credit.model.TcRproductCredit;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;
import com.singlee.capital.credit.service.TcRproductCreditService;

@Service
public class TcRproductCreditServiceImpl implements TcRproductCreditService {
	
	@Autowired
	private TcRproductCreditMapper tcRproductCreditMapper;

	@Override
	public Page<TcRproductCreditPojo> pageTcRproductCreditPojo(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TcRproductCreditPojo> page = tcRproductCreditMapper.pageRproductCreditList(map, rb);
		return page;
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteRproductCredit(Map<String, Object> map) {
		String productCode = ParameterUtil.getString(map, "productCode", "");
		tcRproductCreditMapper.deleteByProductCode(productCode);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) throws RException {
		insertRproductCredit(tcRproductCreditPojo);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) throws RException {
		// 先删除产品代码下的所有关系
		tcRproductCreditMapper.deleteByProductCode(tcRproductCreditPojo.getProductCode());
		// 将新的关系添加
		insertRproductCredit(tcRproductCreditPojo);
	}

	private void insertRproductCredit(TcRproductCreditPojo tcRproductCreditPojo) {
		String[] propertyLArray = tcRproductCreditPojo.getPropertyLs().split(",");
		String[] creditIdArray = tcRproductCreditPojo.getCreditIds().split(",");
		List<String> creditIdList = new ArrayList<String>();
		creditIdList = Arrays.asList(creditIdArray);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("productCode", tcRproductCreditPojo.getProductCode());
		map.put("creditIdList", creditIdList);
		if(!tcRproductCreditMapper.findByProductCodeAndCreditIdList(map).isEmpty()) {
			JY.raiseRException("关系已存在");
		}
		TcRproductCredit rproductCredit = new TcRproductCredit();
		for (int i = 0; i < creditIdArray.length; i++) {
			rproductCredit.setProductCode(tcRproductCreditPojo.getProductCode());
			rproductCredit.setCreditId(creditIdArray[i]);
			int level = Integer.parseInt(propertyLArray[i]);
			rproductCredit.setPropertyL(level);
			rproductCredit.setwValue(50-level);
			tcRproductCreditMapper.insert(rproductCredit);
		}
	}
}
