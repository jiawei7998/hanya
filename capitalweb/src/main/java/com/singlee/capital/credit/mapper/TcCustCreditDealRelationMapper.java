package com.singlee.capital.credit.mapper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditDealRelationBean;
import com.singlee.capital.credit.pojo.CustCreditUseBean;
import com.singlee.capital.credit.pojo.CustCreditUseDetailVo;

public interface TcCustCreditDealRelationMapper extends Mapper<CustCreditDealRelationBean>{

	/****
	 * 保存交易与额度占用关联关系 
	 * @param relationBeans
	 */
	void insertRelationBeans(List<CustCreditDealRelationBean> relationBeans);

	/****
	 * 查询交易占用的额度列表信息
	 * @param params
	 * @return
	 */
	List<CustCreditDealRelationBean> selectCreditDealRelationsRelease(HashMap<String, Object> params);

	/****
	 * 释放额度,更新关联表状态
	 * @param params
	 */
	void updateReleaseAmt(HashMap<String, Object> params);

	/****
	 * 根据交易对手和起息日查询出需要重新占用额度的交易,结果按起息日升序排序,结果包含所有有串用额度情况的交易
	 * @param params
	 * @return
	 */
	List<CustCreditUseBean> selectCustUsedCredits(HashMap<String, Object> params);
	
	/****
	 * 根据交易对手和起息日检索出所有需要重新占用额度的记录   修改额度占用状态 ,释放所有额度
	 */
	void updateCreditRelationsForRelease();

	/****
	 * 批量释放额度，更新额度占用关系
	 * @param params
	 */
	void updateReleaseAmtBatch(HashMap<String, Object> params);

	List<CustCreditDealRelationBean> selectDealRelations(HashMap<String, Object> params);
	
	List<CustCreditDealRelationBean> selectDealRelationsByCust(HashMap<String, Object> params);

	void updateRelationToHistory(HashMap<String, Object> params);

	void deleteRelationByDealno(HashMap<String, Object> params);

	List<CustCreditDealRelationBean> selectDealRelationsByCustCredit(HashMap<String, Object> params);
	
	public BigDecimal queryPreUsedAmtByDealNoCust(HashMap<String, Object> params);

	/****
	 * 
	 * @param dealNo
	 */
	public void updatePreUsedAmtToUse(Map<String, Object> map);
	
	/****
	 * 
	 * @param params
	 * @return
	 */
	public BigDecimal queryUsedAmtByCustNo(HashMap<String, Object> params);

	Page<CustCreditUseDetailVo> getUseDetailByCredit(Map<String, Object> map, RowBounds rowBounds);
	
	Page<CustCreditUseDetailVo> getStrDetailByCredit(Map<String, Object> map, RowBounds rowBounds);

	List<CustCreditUseDetailVo> getUseDetailByCreditAll(Map<String, Object> map);
	
	public String selectCreditStateForLock(Map<String, Object> map);
	
	/**
	 * 同步额度的客户同步至额度锁
	 */
	public void synchCreditLock();
	
	/**
	 * 清空额度锁
	 */
	public void cleanCreditLock();
	
	/** 
	 * 拿到做过某个产品类型的所有客户
	 */
	public List<String> getDealRelationsByProduct(String productCode);
	
}
