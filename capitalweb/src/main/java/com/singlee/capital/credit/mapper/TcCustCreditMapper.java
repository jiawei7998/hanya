package com.singlee.capital.credit.mapper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditBean;

public interface TcCustCreditMapper extends Mapper<CustCreditBean>{

	/****
	 * 根据客户额度唯一编号查找额度信息
	 * @param custCreditId
	 * @return
	 */
	public CustCreditBean queryCustCreditById(String custCreditId);


	Page<CustCreditBean> getCustCreditList(Map<String, Object> map, RowBounds rowBounds);
	
	/****
	 * 额度切分
	 * @param params
	 */
	public void updateQuotaAseg(Map<String, Object> params);


	public void updateQuotaAvl(Map<String, Object> params);
	/****
	 * 额度调整
	 * @param params
	 */
	public void updateQuotaAdjAmt(Map<String, Object> params);

	
	/****
	 * 额度冻结
	 * @param params
	 */
	public void updateFrozenAmt(Map<String, Object> params);

	/***
	 * 额度解冻
	 * @param params
	 */
	public void updateUnFrozenAmt(Map<String, Object> params);

	/***
	 * 查询符合条件的客户额度信息
	 * @param params
	 * @return
	 */
	public List<CustCreditBean> queryCustCredits(Map<String, Object> params);

	/***
	 * 查询符合条件的客户额度信息
	 * @param 客户号，额度编号，额度期限
	 * @return
	 */
	public CustCreditBean queryCustCreditByInfo(CustCreditBean ccb);

	public List<CustCreditBean> queryCustCreditList(CustCreditBean ccb);
	/****
	 * 查询符合条件的客户可用额度总和
	 * @param params
	 * @return
	 */
	public BigDecimal queryCustCreditsAvlAmt(Map<String, Object> params);

	/****
	 * 额度占用
	 * @param params
	 */
	public void updateUseAmt(Map<String, Object> params);

	/****
	 * 按顺序查询系统中 没有配置过 交易与额度类型对应关系的额度
	 * @param params
	 * @return
	 */
	public List<CustCreditBean> queryCustNoMapsCredits(Map<String, Object> params);

	/****
	 * 释放额度
	 * @param params
	 */
	public void updateReleaseAmt(HashMap<String, Object> params);

	/***
	 * 
	 * 释放需要重新占用额度的交易 对应的额度占用关系中的所有额度
	 */
	public void updateCustCreditForReleaseByRelations();

	/****
	 * 批量 额度占用
	 * @param creditUseBeans
	 */
	public void updateUseAmtForBatch(List<CustCreditBean> creditUseBeans);

	/****
	 * 批量释放额度
	 * @param tempCreditBeans
	 */
	public void updateReleaseAmtBatch(List<CustCreditBean> tempCreditBeans);

	/****
	 * 批量更新额度占用和释放信息
	 * @param creditBeans
	 */
	public void updateCreditBatch(List<CustCreditBean> creditBeans);


	public Page<CustCreditBean> getCreditListByCust(Map<String, Object> map,
			RowBounds rowBounds);


	public List<CustCreditBean> getCreditListByCustNo(Map<String, Object> map);


	public List<CustCreditBean> getCustCreditListAll(Map<String, Object> map);
	
	public void setCustCeditList(List<CustCreditBean> creditBeans);
	
	public void insertCustCreditToDb(CustCreditBean custCreditBean);

}
