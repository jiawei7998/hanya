package com.singlee.capital.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;


public interface CreditAdjService {

	void addCustCreditAdj(Map<String,Object> map);

	Page<CustCreditAdjBean> getCreditAdjList(Map<String, Object> map);

	Page<CustCreditAdjBean> getCreditAdjListFinish(Map<String, Object> map);

	Page<CustCreditAdjBean> getCreditAdjListMine(Map<String, Object> map);
	
	void deleteCreditAdj(Map<String, Object> map);

	void updateCreditAdj(Map<String, Object> map);

	Page<CustCreditAdjBean> getAdjDetailByCredit(Map<String, Object> map);
	
	List<CustCreditAdjBean> getAdjDetailByCreditAll(Map<String, Object> map);

	CustCreditAdjBean getCreditAdjByDealNo(String serial_no);

	void statusChange(Map<String,Object> approveMap);

	
}
