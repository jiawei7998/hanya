package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;

public interface TcRproductCreditService {
	Page<TcRproductCreditPojo> pageTcRproductCreditPojo(Map<String, Object> map);
	
	void deleteRproductCredit(Map<String, Object> map);
	
	void addRproductCredit(TcRproductCreditPojo tcRproductCreditPojo);
	
	void updateRproductCredit(TcRproductCreditPojo tcRproductCreditPojo);
}
