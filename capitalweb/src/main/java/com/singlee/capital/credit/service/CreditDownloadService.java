package com.singlee.capital.credit.service;

import java.util.HashMap;

import com.singlee.capital.credit.util.ExcelUtil;

public interface CreditDownloadService {

	ExcelUtil downloadExcel(HashMap<String, Object> map);

	ExcelUtil downloadDetailExcel(HashMap<String, Object> map);

	ExcelUtil downloadExcelAll(HashMap<String, Object> map);

}
