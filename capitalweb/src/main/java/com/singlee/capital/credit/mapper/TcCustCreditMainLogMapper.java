package com.singlee.capital.credit.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.TcCustCreditMainLog;

public interface TcCustCreditMainLogMapper extends Mapper<TcCustCreditMainLog>{

	/****
	 * 查询交易占用的额度列表信息
	 * @param params
	 * @return
	 */
	List<TcCustCreditMainLog> selectTcCustCreditMainLogs(Map<String, Object> params);

	/**
	 * 通过客户查询可用明细
	 */
	Page<TcCustCreditMainLog> getLogByCustCreditId(Map<String, Object> params,RowBounds rowBounds);

}
