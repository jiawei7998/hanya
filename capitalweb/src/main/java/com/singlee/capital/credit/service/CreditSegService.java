package com.singlee.capital.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditSegBean;

public interface CreditSegService {

	void addCustCreditSeg(Map<String,Object> map);

	Page<CustCreditSegBean> getCreditSegList(Map<String, Object> map);

	void deleteCreditSeg(Map<String, Object> map);

	void updateCreditSeg(Map<String, Object> map);

	Page<CustCreditSegBean> getSegDetailByCredit(Map<String, Object> map);
	
	List<CustCreditSegBean> getSegDetailByCreditAll(Map<String, Object> map);

	void statusChange(Map<String, Object> approveMap);

	CustCreditSegBean getCreditSegByDealNo(String serial_no);


	
}
