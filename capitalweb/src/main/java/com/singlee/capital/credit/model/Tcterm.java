package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 额度期限配置
 * @author liuxiaoyi
 *
 */
@Entity
@Table(name="TC_TERM")
public class Tcterm implements Serializable,Comparable<Tcterm>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//期限代码
	@Id
	private String termId;
	//期限（天）
	private int termDays;
	//解释
	private String termName;
	//权重
	private int wValue;
	
	private String months;
	
	

	public String getTermId() {
		return termId;
	}



	public void setTermId(String termId) {
		this.termId = termId;
	}



	public int getTermDays() {
		return termDays;
	}



	public void setTermDays(int termDays) {
		this.termDays = termDays;
	}



	public String getTermName() {
		return termName;
	}



	public void setTermName(String termName) {
		this.termName = termName;
	}



	public int getwValue() {
		return wValue;
	}



	public void setwValue(int wValue) {
		this.wValue = wValue;
	}



	public String getMonths() {
		return months;
	}



	public void setMonths(String months) {
		this.months = months;
	}



	@Override
	public int compareTo(Tcterm o) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
