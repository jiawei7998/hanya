package com.singlee.capital.credit.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;

public interface CreditFrozenService {

	void addCustCreditFrozen(Map<String,Object> map);

	Page<CustCreditFrozenBean> getFrozenDetailByCredit(Map<String, Object> map);

	Page<CustCreditFrozenBean> getCreditFrozenList(Map<String, Object> map);

	void deleteCreditFrozen(Map<String, Object> map);

	void updateCreditFrozen(Map<String, Object> map);

	void statusChange(Map<String, Object> approveMap);
	
	CustCreditFrozenBean getCreditFrozenByDealNo(String serial_no);

	List<CustCreditFrozenBean> getFrozenDetailByCreditAll(Map<String, Object> map);

	Page<CustCreditFrozenBean> getCreditFrozenListMine(Map<String, Object> map);

	Page<CustCreditFrozenBean> getCreditFrozenListFinish(Map<String, Object> map);
	
	Page<CustCreditFrozenBean> getCreditFrozenListForDialog(Map<String, Object> map);
	/**
	 * 获得冻结明细
	 * @param map
	 * @return
	 */
	Page<CustCreditFrozenBean> getCreditFrozenListForDetail(Map<String, Object> map);
	/**
	 * 冻结到期失效
	 * @param map
	 */
	public void getCreditFrozenListForMaturityAutoRelease(Map<String, Object> map);
}
