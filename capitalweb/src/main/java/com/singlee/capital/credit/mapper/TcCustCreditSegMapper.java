package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditSegBean;

public interface TcCustCreditSegMapper  extends Mapper<CustCreditSegBean>{

	void addCustCrditSeg(Map<String, Object> map);

	/**
	 * 得到额度切分审批列表
	 */
	Page<CustCreditSegBean> getCreditSegList(Map<String, Object> map,
			RowBounds rowBounds);
	
	void deleteByDealNo(Map<String,Object> map);

	void updateByDealNo(Map<String, Object> map);

	Page<CustCreditSegBean> getSegDetailByCredit(Map<String, Object> map,
			RowBounds rowBounds);

	void statusChange(Map<String, Object> status);

	CustCreditSegBean getCreditSegByDealNo(String dealNo);

	List<CustCreditSegBean> getSegDetailByCreditAll(Map<String, Object> map);


}
