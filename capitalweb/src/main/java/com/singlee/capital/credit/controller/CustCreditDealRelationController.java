package com.singlee.capital.credit.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.pojo.CustCreditUseDetailVo;
import com.singlee.capital.credit.service.CustCreditDealRelationService;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/CustCreditDealRelationController")
public class CustCreditDealRelationController extends CommonController {

	@Autowired
	CustCreditDealRelationService custCreditDealRelationService;

	/**
	 * 得到客户占用详情
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getUseDetailByCredit")
	public RetMsg<PageInfo<CustCreditUseDetailVo>> getUseDetailByCredit(@RequestBody Map<String,Object> map) {
		Page<CustCreditUseDetailVo> list = custCreditDealRelationService.getUseDetailByCredit(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到发起的额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getUseDetailByCreditAll")
	public RetMsg<List<CustCreditUseDetailVo>> getUseDetailByCreditAll(@RequestBody Map<String,Object> map) {
		List<CustCreditUseDetailVo> uselist = custCreditDealRelationService.getUseDetailByCreditAll(map);
		return RetMsgHelper.ok(uselist);
	}
	
	
}
