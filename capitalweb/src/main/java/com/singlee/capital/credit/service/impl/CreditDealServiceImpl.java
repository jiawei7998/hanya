package com.singlee.capital.credit.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcCustCreditDealMapper;
import com.singlee.capital.credit.pojo.CustCreditDealBean;
import com.singlee.capital.credit.service.CreditDealService;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditDealServiceImpl implements CreditDealService {
	
	@Autowired
	TcCustCreditDealMapper creditDealMapper;
	
	@Autowired
	private TrdOrderMapper approveManageDao;
	@Autowired
	TdEdDealLogMapper edDealLogMapper;
	@Override
	public void addCustCreditDeal(Map<String, Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		String trdtype = DictConstants.CREDIT_TYPE.USED;
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("trdtype", trdtype);
		String order_id = approveManageDao.getOrderId(hashMap);
		map.put("dealNo", order_id);
		/*map.put("inputDate", DateUtil.getCurrentDateAsString());*/
		map.put("inputTime", DateUtil.getCurrentTimeAsString());
		map.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		creditDealMapper.insert(mapTocreditDeal(map));
	}
	
	/**
	 * map转换成需要的TdOverDueConfirm对象
	 * @param map
	 * @return
	 */
	public CustCreditDealBean mapTocreditDeal(Map<String,Object> map){
		CustCreditDealBean creditDealBean = new CustCreditDealBean();
		try {
			BeanUtil.populate(creditDealBean, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		return creditDealBean;
	}

	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<CustCreditDealBean> getCreditDealListApprove(
			Map<String, Object> map) {
		return creditDealMapper.getCreditDealListApprove(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<CustCreditDealBean> getCreditDealListFinish(
			Map<String, Object> map) {
		return creditDealMapper.getCreditDealListFinish(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 得到额度占用审批列表
	 */
	@Override
	public Page<CustCreditDealBean> getCreditDealListMine(
			Map<String, Object> map) {
		return creditDealMapper.getCreditDealListMine(map, ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public void deleteCreditDeal(Map<String, Object> map) {
		creditDealMapper.deleteByPrimaryKey(map.get("dealNo"));
	}

	@Override
	public void updateCreditDeal(Map<String, Object> map) {
		map.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		creditDealMapper.updateByMap(map);
	}

	@Override
	public void statusChange(Map<String,Object> status) {
		status.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		creditDealMapper.statusChange(status);
	}

	@Override
	public CustCreditDealBean getCreditDealByDealNo(String serial_no) {
		return creditDealMapper.getCreditDealByDealNo(serial_no);
	}

	@Override
	public Page<CustCreditDealBean> getCreditDealAll(Map<String, Object> map) {
		return creditDealMapper.getCreditDealAll(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<CustCreditDealBean> queryCreditReleaseDeal(Map<String, Object> map, RowBounds rowBounds) {
		return creditDealMapper.queryCreditReleaseDeal(map, rowBounds);
	}

	@Override
	public CustCreditDealBean queryCreditReleaseDealByDealno(Map<String, Object> map) {
		return creditDealMapper.queryCreditReleaseDealByDealno(map);
	}

	@Override
	public CustCreditDealBean getCreditDealByDealNoAddWeight(String serial_no) {
		return creditDealMapper.getCreditDealByDealNoAddWeight(serial_no);
	}
	
}
