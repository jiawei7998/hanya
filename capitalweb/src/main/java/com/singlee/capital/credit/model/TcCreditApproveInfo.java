package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TC_CUST_CREDIT_APPROVE_INFO")
public class TcCreditApproveInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String partyName;		//客户名称
	private String clientNo;		//客户编号
	private String orgNum;			//送审单位
	private String userNum;			//客户经理
	private String approveId;		//批复编号
	private String oriBatchNo;		//原批复编号
	private String productType;		//业务品种
	private String accountProperty;	//业务发生性质
	private Double loanAmt;			//授信额度
	private Double loanCreditAmt;	//敞口额度
	private Double lyed;			//备用额度
	private Double loanApplyed;		//再融资额度
	private String currency;		//币种
	private String tenor;			//期限
	private String extenTermUnit;	//期限单位
	private String sameCustName;	//所属平台客户名称
	private String validDate;		//批复生效日期
	private String rateType;		//利率类型
	private String rateReceiveType;	//利息收取方式
	private Double rate;			//利率
	private String finalApprvResult;//审批结论
	private String siteNo;			//终批机构
	private String limitType;		//是否循环
	private String loanAssuKind;	//担保方式及内容
	
	private String ecifNo;
	
	
	public String getEcifNo() {
		return ecifNo;
	}
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getClientNo() {
		return clientNo;
	}
	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}
	public String getOrgNum() {
		return orgNum;
	}
	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}
	public String getUserNum() {
		return userNum;
	}
	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}
	public String getApproveId() {
		return approveId;
	}
	public void setApproveId(String approveId) {
		this.approveId = approveId;
	}
	public String getOriBatchNo() {
		return oriBatchNo;
	}
	public void setOriBatchNo(String oriBatchNo) {
		this.oriBatchNo = oriBatchNo;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getAccountProperty() {
		return accountProperty;
	}
	public void setAccountProperty(String accountProperty) {
		this.accountProperty = accountProperty;
	}
	public Double getLoanAmt() {
		return loanAmt;
	}
	public void setLoanAmt(Double loanAmt) {
		this.loanAmt = loanAmt;
	}
	public Double getLoanCreditAmt() {
		return loanCreditAmt;
	}
	public void setLoanCreditAmt(Double loanCreditAmt) {
		this.loanCreditAmt = loanCreditAmt;
	}
	public Double getLyed() {
		return lyed;
	}
	public void setLyed(Double lyed) {
		this.lyed = lyed;
	}
	public Double getLoanApplyed() {
		return loanApplyed;
	}
	public void setLoanApplyed(Double loanApplyed) {
		this.loanApplyed = loanApplyed;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getExtenTermUnit() {
		return extenTermUnit;
	}
	public void setExtenTermUnit(String extenTermUnit) {
		this.extenTermUnit = extenTermUnit;
	}
	public String getSameCustName() {
		return sameCustName;
	}
	public void setSameCustName(String sameCustName) {
		this.sameCustName = sameCustName;
	}
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getRateReceiveType() {
		return rateReceiveType;
	}
	public void setRateReceiveType(String rateReceiveType) {
		this.rateReceiveType = rateReceiveType;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getFinalApprvResult() {
		return finalApprvResult;
	}
	public void setFinalApprvResult(String finalApprvResult) {
		this.finalApprvResult = finalApprvResult;
	}
	public String getSiteNo() {
		return siteNo;
	}
	public void setSiteNo(String siteNo) {
		this.siteNo = siteNo;
	}
	public String getLimitType() {
		return limitType;
	}
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	public String getLoanAssuKind() {
		return loanAssuKind;
	}
	public void setLoanAssuKind(String loanAssuKind) {
		this.loanAssuKind = loanAssuKind;
	}

	
	
}
