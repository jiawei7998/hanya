package com.singlee.capital.credit.pojo;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="TC_CUST_CREDIT_DETAIL_LOG")
public class TcCustCreditDetailLog {
	@Id
	private String      logId              ;  
	private Integer     seq                ;  
	private String      dealNo             ;  
	private Integer     useState          ;  
	private Integer     useType; //占用类型:1-占用 2-串用
	private BigDecimal  amt           ;  
	private String      custCreditId     ;  
	private String      custCreditName     ;
	private String      custCreditTerm     ;
	private String      orgCustCreditId ;
	private String      orgCustCreditName     ;
	private String      orgCustCreditTerm     ;
	private String      remark;
	private BigDecimal      avlAmt;
	
	public BigDecimal getAvlAmt() {
		return avlAmt;
	}
	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getUseType() {
		return useType;
	}
	public void setUseType(Integer useType) {
		this.useType = useType;
	}
	public String getLogId() {
		return logId;
	}
	public String getCustCreditName() {
		return custCreditName;
	}
	public void setCustCreditName(String custCreditName) {
		this.custCreditName = custCreditName;
	}
	public String getCustCreditTerm() {
		return custCreditTerm;
	}
	public void setCustCreditTerm(String custCreditTerm) {
		this.custCreditTerm = custCreditTerm;
	}
	public String getOrgCustCreditName() {
		return orgCustCreditName;
	}
	public void setOrgCustCreditName(String orgCustCreditName) {
		this.orgCustCreditName = orgCustCreditName;
	}
	public String getOrgCustCreditTerm() {
		return orgCustCreditTerm;
	}
	public void setOrgCustCreditTerm(String orgCustCreditTerm) {
		this.orgCustCreditTerm = orgCustCreditTerm;
	}
	public void setLogId(String logId) {
		this.logId = logId;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getUseState() {
		return useState;
	}
	public void setUseState(Integer useState) {
		this.useState = useState;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	public String getOrgCustCreditId() {
		return orgCustCreditId;
	}
	public void setOrgCustCreditId(String orgCustCreditId) {
		this.orgCustCreditId = orgCustCreditId;
	}
	@Override
	public String toString() {
		return "TcCustCreditDetailLog [logId=" + logId + ", seq=" + seq
				+ ", dealNo=" + dealNo + ", useState=" + useState + ", amt="
				+ amt + ", custCreditId=" + custCreditId + ", orgCustCreditId="
				+ orgCustCreditId + "]";
	}
	
	
	
	

}
