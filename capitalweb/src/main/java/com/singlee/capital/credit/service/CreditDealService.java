package com.singlee.capital.credit.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditDealBean;


public interface CreditDealService {

	void addCustCreditDeal(Map<String,Object> map);

	void deleteCreditDeal(Map<String, Object> map);

	void updateCreditDeal(Map<String, Object> map);

	void statusChange(Map<String,Object> approveMap);

	CustCreditDealBean getCreditDealByDealNo(String serial_no);

	Page<CustCreditDealBean> getCreditDealAll(Map<String, Object> map);
	
	Page<CustCreditDealBean> queryCreditReleaseDeal(Map<String, Object> map,RowBounds rowBounds);

	CustCreditDealBean queryCreditReleaseDealByDealno(Map<String, Object> map);

	Page<CustCreditDealBean> getCreditDealListFinish(Map<String, Object> map);

	Page<CustCreditDealBean> getCreditDealListApprove(Map<String, Object> map);

	Page<CustCreditDealBean> getCreditDealListMine(Map<String, Object> map);
	
	/**
	 * 获取手手工占用详情，金额为 金额*权重/100
	 */
	CustCreditDealBean getCreditDealByDealNoAddWeight(String dealNo);
	
}
