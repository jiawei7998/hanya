package com.singlee.capital.credit.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.model.TcCreditApproveInfo;
import com.singlee.capital.credit.pojo.CustCreditBean;
import com.singlee.capital.credit.pojo.TcCustCreditDetailLog;
import com.singlee.capital.credit.pojo.TcCustCreditMainLog;
import com.singlee.capital.credit.service.CreditApproveInfoService;
import com.singlee.capital.credit.service.CreditDownloadService;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.interfacex.model.ApprovalInqRq;
import com.singlee.capital.interfacex.model.ApprovalInqRs;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/CustCreditController")
public class CustCreditController extends CommonController {

	
	@Autowired
	CreditManageService creditManageService;
	
	@Autowired
	CreditApproveInfoService creditApproveInfoService;
	
	@Autowired
	SocketClientService socketClientService;
	
	@Autowired
	CreditDownloadService creditDownloadService;
	
	/**
	 * 新增客户额度详情
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addCustCredit")
	public RetMsg<Serializable> addCustCredit(@RequestBody Map<String,Object> map) {
		//creditManageService.addCustCredit(map);
		return RetMsgHelper.ok();
		
	}
	
	/**
	 * 得到客户额度详情列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditList")
	public RetMsg<PageInfo<CustCreditBean>> getCreditList(@RequestBody Map<String,Object> map) {
		Page<CustCreditBean> list = creditManageService.getCustCreditList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到客户额度详情列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditListAll")
	public RetMsg<List<CustCreditBean>> getCreditListAll(@RequestBody Map<String,Object> map) {
		List<CustCreditBean> list = creditManageService.getCustCreditListAll(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到指定客户的额度详情
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditListByCust")
	public RetMsg<PageInfo<CustCreditBean>> getCreditListByCust(@RequestBody Map<String,Object> map) {
		map.put("expDate", map.get("expDate").toString().replace("-", ""));
		Page<CustCreditBean> list = creditManageService.getCreditListByCust(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 删除客户额度详情
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCredit")
	public RetMsg<Serializable> deleteCredit(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新客户额度详情
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCredit")
	public RetMsg<Serializable> updateCredit(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到额度批复信息列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditApproveInfoList")
	public RetMsg<PageInfo<TcCreditApproveInfo>> getCreditApproveInfoList(@RequestBody Map<String,Object> map) {
		Page<TcCreditApproveInfo> list = creditApproveInfoService.getCreditApproveInfoList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 同步额度批复信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/synchCreditApproveInfo")
	public RetMsg<Serializable> synchCreditApproveInfo(@RequestBody Map<String,Object> map) {
		StringBuffer sb = new StringBuffer();
		ApprovalInqRs approvalInqRs = new ApprovalInqRs();
		try {
			CommonRqHdr commonRqHdr = new CommonRqHdr();
			commonRqHdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			commonRqHdr.setRqUID(UUID.randomUUID().toString());
			commonRqHdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			commonRqHdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			
			ApprovalInqRq request = new ApprovalInqRq();
			request.setCommonRqHdr(commonRqHdr);
			request.setEcifNum((String) map.get("clientNo"));
			
			approvalInqRs = socketClientService.CrmsApprovalInqRequest(request);
			if(!approvalInqRs.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
				sb.append("数据同步失败！"+approvalInqRs.getCommonRsHdr().getServerStatusCode());
			}else{
				creditManageService.creditApprovalInq(approvalInqRs,(String) map.get("clientNo"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return RetMsgHelper.ok(StringUtils.isEmpty(sb.toString())?"数据同步成功":sb.toString());
	}
	
	/**
	 * 拿到交易日志主信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCustCreditMainLog")
	public RetMsg<List<TcCustCreditMainLog>> getCustCreditMainLog(@RequestBody Map<String,Object> map) {
		List<TcCustCreditMainLog> list = creditManageService.getCustCreditMainLog(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 通过客户额度号拿到交易日志信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getLogByCustCreditId")
	public RetMsg<List<TcCustCreditMainLog>> getLogByCustCreditId(@RequestBody Map<String,Object> map) {
		List<TcCustCreditMainLog> list = creditManageService.getLogByCustCreditId(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 拿到交易日志主信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCustCreditMainLogList")
	public List<TcCustCreditMainLog> getCustCreditMainLogList(@RequestBody Map<String,Object> map) {
		return creditManageService.getCustCreditMainLog(map);
	}
	
	/**
	 * 拿到交易日志详细信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCustCreditDetailLog")
	public RetMsg<List<TcCustCreditDetailLog>> getCustCreditDetailLog(@RequestBody Map<String,Object> map) {
		List<TcCustCreditDetailLog> list = creditManageService.getCustCreditDetailLog(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 导出客户额度Excel
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/downloadExcel")
	public void downloadExcel(HttpServletRequest request, HttpServletResponse response){
		HashMap<String,Object> map = new HashMap<String,Object>();

		String ua = request.getHeader("User-Agent");
		String filename = "客户额度信息.xlsx";
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			//e1.printStackTrace();
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");

		try {
			ExcelUtil e = creditDownloadService.downloadExcel(map);
			
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
			
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		
	};
	
	/**
	 * 导出指定客户的额度详情Excel
	 * @param request
	 * @param response
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/downloadDetailExcel")
	public void downloadDetailExcel(HttpServletRequest request, HttpServletResponse response){
		HashMap map = requestParamToMap(request);
		
		
		//配置Response
		String ua = request.getHeader("User-Agent");
		String filename = "客户额度详情信息.xlsx";
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			//e1.printStackTrace();
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");

		try {
			ExcelUtil e = creditDownloadService.downloadDetailExcel(map);
			
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
			
			
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		
	};
	
	/**
	 * 导出所有客户额度Excel
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/downloadExcelAll")
	public void downloadExcelAll(HttpServletRequest request, HttpServletResponse response){
		HashMap<String,Object> map = new HashMap<String,Object>();

		String ua = request.getHeader("User-Agent");
		String filename = "客户额度明细信息.xlsx";
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			//e1.printStackTrace();
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");

		try {
			ExcelUtil e = creditDownloadService.downloadExcelAll(map);
			
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
			
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		
	};

	
	/**
	 * 读取request中的参数,转换为map对象
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static HashMap requestParamToMap(HttpServletRequest request) {
		HashMap parameters = new HashMap();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) {
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			if (!StringUtils.isEmpty(paramValue)) {
				parameters.put(paramName, paramValue);
			}
		}
		return parameters;
	}
	
}
