package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.credit.model.TcInstCredit;

public interface TcInstCreditService {
	Page<TcInstCredit> pageInstCreditList(Map<String, Object> map);
	
	void deleteInstCredit(Map<String, Object> map);
	
	RetMsg<TcInstCredit> addInstCredit(TcInstCredit tcInstCredit);
	
	RetMsg<TcInstCredit> updateInstCredit(TcInstCredit tcInstCredit);
}
