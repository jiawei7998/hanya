package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*****
 * 授信额度调整
 * @author kf0738
 *
 */
@Table(name="TC_CUST_CREDIT_ADJ")
public class CustCreditAdjBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	private String          dealNo	;                    //流水号
	private String          custCreditId;                //授信额度唯一编号
	private String          custNo	        ;            //客户编号
	private String          custName	      ;            //客户名称
	private String          creditId	      ;            //额度种类编号
	private String          creditName	    ;            //额度种类名称
	private String          term	    ;                  //额度期限
	private BigDecimal      creditAmt	    ;              //授信额度
	private BigDecimal      quotaAvlAmt;                      //可用额度
	private BigDecimal      quotaAdjAmt;                       //已经调整额度
	private BigDecimal      adjAmt;                       //调整额度
	private String          reason;                      //调整说明
	private String          ioper	       ;               //经办人
	private String          institution	    ;            //机构   
	private String          remark	      ;              //备注
	private String          rDealno; //关联冻结额度Dealno
	private String            operDate;//操作日期
	@Transient
	private String			taskId;
	
	private int approveStatus;
	private String  inputTime;
	private String  inputDate;
	private String  modifyDate;
	
	@Transient
	private String sponInstName;//20180529新加
	
	
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public int getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getOperDate() {
		return operDate;
	}
	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}
	
	public String getrDealno() {
		return rDealno;
	}
	public void setrDealno(String rDealno) {
		this.rDealno = rDealno;
	}
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public BigDecimal getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	public BigDecimal getQuotaAvlAmt() {
		return quotaAvlAmt;
	}
	public void setQuotaAvlAmt(BigDecimal quotaAvlAmt) {
		this.quotaAvlAmt = quotaAvlAmt;
	}
	public BigDecimal getQuotaAdjAmt() {
		return quotaAdjAmt;
	}
	public void setQuotaAdjAmt(BigDecimal quotaAdjAmt) {
		this.quotaAdjAmt = quotaAdjAmt;
	}
	public BigDecimal getAdjAmt() {
		return adjAmt;
	}
	public void setAdjAmt(BigDecimal adjAmt) {
		this.adjAmt = adjAmt;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSponInstName() {
		return sponInstName;
	}
	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}
	
	

	

}
