package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;
import com.singlee.capital.credit.pojo.CustCreditBean;


public interface TcCustCreditTempMapper extends Mapper<CustCreditBean> {

	public void insertCustCreditTemp(List<CustCreditBean> list);
	
	public void addCustCreditTemp(CustCreditBean list);
	
	/***
	 * 查询符合条件的客户额度信息
	 * @param 客户号，额度编号，额度期限
	 * @return
	 */
	public CustCreditBean queryCustCreditByInfo(CustCreditBean ccb);
	
	public List<CustCreditBean> getCustCreditTemp(Map<String, Object> map);
	
	public void deleteAll();
	
	public void updateCustCreditTemp(CustCreditBean ccb);
	
	public int queryCreditType(CustCreditBean ccb);
	
}
