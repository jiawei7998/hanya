package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Date;

import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;

/*****
 * 授信额度占用
 * @author kf0738
 *
 */
public class CustCreditUseBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String          dealNo;            //交易编号
	private String          custNo;            //客户编号             
	private String          custName;          //客户名称             
	private String          product_code;      //业务品种           
	private BigDecimal      quotaStrAmt	 ;            //串用额度             
	private BigDecimal      quotaCstrAmt	  ;            //被串用额度           
	private BigDecimal      amt;                    //金额        
	private String            vdate;                       //起息日
	private String            mdate;                       //到期日
	private String          remark1;            //备注字段             
	private String          remark2;            //备注字段       
	private BigDecimal      term;
	private String          operDate;//操作日期
	private Integer         useState;
	private String          offLine;
	private boolean         isFinancial;
	private final long day = 24 * 3600 * 1000;
	private String orderId;
	private String dealType;//审批类型-1事前审批-占用  2-正式审批
	private String refNo;//关联号  预占用转换为准正式占用的 TRD编号

	
	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public BigDecimal getTermForMonth()
	{
		Date m_date = DateUtil.parse(mdate);
		Date v_date = DateUtil.parse(vdate);
		int days = (int)((m_date.getTime() - v_date.getTime()) / day);
		if(days <= 29)
		{
			return new BigDecimal(days / 31.00).setScale(2, RoundingMode.DOWN);

		}else{
			int month = 0;
			try {
				String vdateTemp = vdate;
				while(PlaningTools.compareDate2(mdate, vdateTemp, "yyyy-MM-dd"))
				{
					month=month+1;
					vdateTemp=PlaningTools.addOrSubMonth_Day(vdate, Frequency.MONTH, month);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new RException(e);
			}
			return new BigDecimal(month);
		}
	}
	
	
	public boolean isFinancial() {
		return isFinancial;
	}


	public void setFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}


	public String getOffLine() {
		return offLine;
	}


	public void setOffLine(String offLine) {
		this.offLine = offLine;
	}


	public Integer getUseState() {
		return useState;
	}


	public void setUseState(Integer useState) {
		this.useState = useState;
	}


	public BigDecimal getQuotaStrAmt() {
		return quotaStrAmt;
	}

	public void setQuotaStrAmt(BigDecimal quotaStrAmt) {
		this.quotaStrAmt = quotaStrAmt;
	}

	public BigDecimal getQuotaCstrAmt() {
		return quotaCstrAmt;
	}

	public void setQuotaCstrAmt(BigDecimal quotaCstrAmt) {
		this.quotaCstrAmt = quotaCstrAmt;
	}

	public BigDecimal getTerm() {
		return term;
	}

	public void setTerm(BigDecimal term) {
		this.term = term;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getOperDate() {
		return operDate;
	}
	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
	

}
