package com.singlee.capital.credit.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcCreditApproveInfo;

public interface TcCreditApproveInfoMapper extends Mapper<TcCreditApproveInfo>{

	/**
	 * 创建额度批复信息
	 */
	void createCreditApproveInfo(TcCreditApproveInfo map);

	/**
	 * 得到额度批复信息列表
	 */
	Page<TcCreditApproveInfo> getCreditApproveInfoList(Map<String, Object> map,
			RowBounds rowBounds);
	
	/**
	 * 通过客户编号删除额度批复信息
	 */
	void deleteByClientNo(Map<String,Object> map);

	/**
	 * 通过客户编号更新额度批复信息
	 */
	void updateCreditApproveInfo(TcCreditApproveInfo map);

	TcCreditApproveInfo getByClientNo(Map<String, Object> map);

}
