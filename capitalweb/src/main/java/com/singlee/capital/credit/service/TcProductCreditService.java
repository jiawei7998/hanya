package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcProductCredit;

public interface TcProductCreditService {

	Page<TcProductCredit> pageCreditProduct(Map<String, Object> map);
	
	void deleteCreditProduct(Map<String, Object> map);
	
	void addProductCredit(TcProductCredit tcProductCredit);
	
	void updateProductCredit(TcProductCredit tcProductCredit);
}
