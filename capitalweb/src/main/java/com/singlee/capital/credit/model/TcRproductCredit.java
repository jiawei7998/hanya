package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * 业务品种对应额度品种设置
 * @author liyimin
 *
 */
@Entity
@Table(name = "TC_RPRODUCT_CREDIT")
public class TcRproductCredit implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// 产品代码
	private String productCode;
	
	// 产品名称
	private String productName;
	
	// 额度品种
	private String creditId;
	
	// 额度名称
	private String creditName;
	
	// 优先级
	private Integer propertyL;
	
	// 风险权重
	private Integer wValue;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public Integer getPropertyL() {
		return propertyL;
	}

	public void setPropertyL(Integer propertyL) {
		this.propertyL = propertyL;
	}

	public Integer getwValue() {
		return wValue;
	}

	public void setwValue(Integer wValue) {
		this.wValue = wValue;
	}
	
}
