package com.singlee.capital.credit.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcCreditMapper;
import com.singlee.capital.credit.model.TcCredit;
import com.singlee.capital.credit.service.TcCreditService;

@Service("tcCreditService")
public class TcCreditServiceImpl implements TcCreditService {
	
	@Autowired
	private TcCreditMapper tcCreditMapper;
	
	@Autowired
	private BatchDao batchDao;

	@Override
	public Page<TcCredit> pageTcCreditList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		return tcCreditMapper.pageTcCreditList(map, rowBounds);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteTcCredit(Map<String, Object> map) throws RException {
		String creditId = ParameterUtil.getString(map, "creditId", "");
		TcCredit credit = tcCreditMapper.selectByPrimaryKey(creditId);
		if(credit == null) {
			JY.raiseRException("额度品种不存在");
		}
		tcCreditMapper.deleteByPrimaryKey(creditId);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addTcCredit(TcCredit tcCredit) throws RException {
		TcCredit credit = tcCreditMapper.selectByPrimaryKey(tcCredit.getCreditId());
		if(credit != null) {
			JY.raiseRException("额度品种已存在");
		}
		tcCreditMapper.insert(tcCredit);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateTcCredit(TcCredit tcCredit) throws RException {
		TcCredit credit = tcCreditMapper.selectByPrimaryKey(tcCredit.getCreditId());
		if(credit == null) {
			JY.raiseRException("额度品种不存在");
		}
		tcCreditMapper.updateCredit(tcCredit);
	}

	@Override
	public List<TcCredit> allTcCreditList() {
		return tcCreditMapper.selectAll();
	}

	@Override
	public Page<TcCredit> pageTcCredit(Map<String, Object> map) {
		return tcCreditMapper.pageCredit(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TcCredit> getChooseCredit(Map<String, Object> map) {
		String creditIds = ParameterUtil.getString(map, "creditIds", "");
		String[] creditIdArray = creditIds.split(",");
		List<String> creditIdList = new ArrayList<String>();
		for(int i = 0; i < creditIdArray.length; i++) {
			creditIdList.add(creditIdArray[i]);
		}
		map.put("creditIdList", creditIdList);
		return tcCreditMapper.getChooseCredit(map);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void batchImportTcCredit(List<TcCredit> tcCredits) throws Exception {
		// TODO Auto-generated method stub
		tcCreditMapper.deleteTcCreditAll("");
	
		batchDao.batch("com.singlee.capital.credit.mapper.TcCreditMapper.insert", tcCredits);
		
	}
}
