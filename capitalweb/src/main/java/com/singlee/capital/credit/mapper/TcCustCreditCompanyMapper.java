package com.singlee.capital.credit.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditCompany;


public interface TcCustCreditCompanyMapper extends Mapper<CustCreditCompany> {
	
	public Page<CustCreditCompany> getCustCeditCompanyList(Map<String, Object> map);
	
}
