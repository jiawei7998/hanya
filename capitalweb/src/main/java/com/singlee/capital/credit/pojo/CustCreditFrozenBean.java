package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*****
 * 授信额度冻结
 * @author kf0738
 *
 */
@Table(name="TC_CUST_CREDIT_FROZEN")
public class CustCreditFrozenBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	private String          dealNo	;                    //流水号        
	private String          custNo	        ;            //客户编号      
	private String          custName	      ;            //客户名称      
	private String          custCreditId	      ;            //客户额度编号  
	private String          term	    ;                  //额度期限
	private String          dueDate;//额度起始日
	private String            vdate	    ;                  //冻结起息日    
	private String            mdate	  ;                    //冻结到期日    
	private BigDecimal      frozenAmt	  ;                //冻结金额    解冻金额  
	//private BigDecimal      oldFrozenAmt	  ;                //冻结金额    解冻金额 
	private String oldIoper;
	@Transient
	private String oldSponInstName;
	private String aDate;
	private String oldInstitution;
	private String remark;
	
	private String dueDateLast;	//额度冻结有效期
	
	
	public String getDueDateLast() {
		return dueDateLast;
	}
	public void setDueDateLast(String dueDateLast) {
		this.dueDateLast = dueDateLast;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOldInstitution() {
		return oldInstitution;
	}
	public void setOldInstitution(String oldInstitution) {
		this.oldInstitution = oldInstitution;
	}
	public String getOldIoper() {
		return oldIoper;
	}
	public void setOldIoper(String oldIoper) {
		this.oldIoper = oldIoper;
	}
	
	public String getOldSponInstName() {
		return oldSponInstName;
	}
	public void setOldSponInstName(String oldSponInstName) {
		this.oldSponInstName = oldSponInstName;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	private String            creditId	    ;                  //冻结起息日    
	private String            creditName	  ;                    //冻结到期日    
	private BigDecimal      creditAmt	  ;                //冻结金额    解冻金额  
	private BigDecimal      quotaAvlAmt	  ;                //冻结金额    解冻金额 
	private String          ioper	  ;                    //经办人        
	private String          institution	  ;              //经营单位    
	private String          oldDealNo	  ;              //被修改的交易
	private Integer         state	  ;                    //状态          
	private String          reason	;                    //备注       
	@Transient
	private String          operDate;//操作日期
	@Transient
	private String			taskId;
	private int approveStatus;
	private String  inputTime;
	private String  inputDate;
	private String  modifyDate;
	@Transient
	private String sponInstName;
	
	private double quotaFrozenAmt;
	
	public double getQuotaFrozenAmt() {
		return quotaFrozenAmt;
	}
	public void setQuotaFrozenAmt(double quotaFrozenAmt) {
		this.quotaFrozenAmt = quotaFrozenAmt;
	}
	public String getSponInstName() {
		return sponInstName;
	}
	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public int getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	public String getOperDate() {
		return operDate;
	}
	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
	
	public BigDecimal getFrozenAmt() {
		return frozenAmt;
	}
	public void setFrozenAmt(BigDecimal frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getOldDealNo() {
		return oldDealNo;
	}
	public void setOldDealNo(String oldDealNo) {
		this.oldDealNo = oldDealNo;
	}
	
	public String getCustCreditId() {
		return custCreditId;
	}
	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
	public BigDecimal getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	public BigDecimal getQuotaAvlAmt() {
		return quotaAvlAmt;
	}
	public void setQuotaAvlAmt(BigDecimal quotaAvlAmt) {
		this.quotaAvlAmt = quotaAvlAmt;
	}
	
	

}
