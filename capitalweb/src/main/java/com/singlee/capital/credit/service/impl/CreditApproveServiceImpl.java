package com.singlee.capital.credit.service.impl;

import com.singlee.capital.credit.service.CreditApproveService;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.mapper.HrbCreditLimitOccupyMapper;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;
import com.singlee.hrbextra.credit.service.HrbCreditCustService;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.hrbextra.credit.service.HrbCreditPrdRelationService;
import com.singlee.ifs.mapper.BondLimitTemplateMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Transactional(value = "transactionManager", rollbackFor = Exception.class)
@Service("creditApproveService")
public class CreditApproveServiceImpl implements CreditApproveService, SlbpmCallBackInteface, TaskEventInterface {

    //额度占用
    @Autowired
    private HrbCreditLimitOccupyService limitOccupy;
    @Autowired
    HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;
    @Autowired
    BondLimitTemplateMapper bondLimitTemplateMapper;
    @Autowired
    HrbCreditLimitOccupyMapper hrbCreditLimitOccupyMapper;
    @Autowired
    HrbCreditPrdRelationService hrbCreditPrdRelationService;


    @Autowired
    HrbCreditCustService hrbCreditCustService;
    @Autowired
    HrbCreditCustMapper hrbCreditCustMapper;

    @Override
    public Object getBizObj(String flow_type, String flow_id, String serial_no) {
        return null;
    }

    /**
     * 流程审批过程，业务实现方法
     */
    @Override
    public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {
        String creditType = serial_no.substring(11, 15);
        if (creditType == null || "".equals(creditType.trim())) {
            return;
        }
        Map<String, Object> approveMap = new HashMap<String, Object>();
        approveMap.put("approveStatus", status);
        approveMap.put("dealNo", serial_no);
        //查询审批信息表
        HrbCreditLimitOccupy hrbCreditLimitOccupy = hrbCreditLimitOccupyMapper.getCreditDealAllByDealNo(serial_no);
        //保存审批
        limitOccupy.statusChange(approveMap);
        //审批通过,开始更改额度
        if (ApproveOrderStatus.ApprovedPass.equals(status)) {
            //审批完成之后的产品表和总额表的操作
            Map<String, Object> map = new HashMap<>();
            map.put("serial_no", serial_no);
            map.put("OldNew", "0");
            map.put("currency", hrbCreditLimitOccupy.getCurrency());

            Map<String, Object> remap = new HashMap<>();
            remap.put("custNo", hrbCreditLimitOccupy.getCreditsubno());
            remap.put("custType", hrbCreditLimitOccupy.getCreTypeName());
            HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
            map.put("currencyChange", hrbCreditCustQuota.getCurrency());

            limitOccupy.AmtChange(map);
        }
    }

    @Override
    public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id,
                                         String task_def_key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub

    }

}
