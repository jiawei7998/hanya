package com.singlee.capital.credit.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcRiskCredit;

import tk.mybatis.mapper.common.Mapper;

public interface TcRiskCreditMapper extends Mapper<TcRiskCredit> {

	/**
	 * 查询额度品种风险级序列表
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TcRiskCredit> pageRiskCreditList(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 更新额度品种风险级序
	 * @param tcRiskCredit
	 */
	public void updateTcRiskCredit(TcRiskCredit tcRiskCredit);
}
