package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcProductCredit;

import tk.mybatis.mapper.common.Mapper;

public interface TcProductCreditMapper extends Mapper<TcProductCredit> {
	
	/**
	 * 分页查询额度产品配置信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TcProductCredit> pageCreditProduct(Map<String, Object> map, RowBounds rb);
	
	/**
     * 查询产品 
     * @param map
     * @param rb
     * @return
     */
    public List<TcProductCredit> CreditProductOne(Map<String, Object> map);

	/**
	 *
	 * @param map
	 * @return
	 */
	public TcProductCredit selectCreditProductOne(Map<String, Object> map);
	
	/**
	 * 更新产品
	 * @param tcProductCredit
	 */
	public void updateProductCredit(TcProductCredit tcProductCredit);
}
