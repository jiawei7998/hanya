package com.singlee.capital.credit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.mapper.TcInstCreditMapper;
import com.singlee.capital.credit.model.TcInstCredit;
import com.singlee.capital.credit.service.TcInstCreditService;

@Service
public class TcInstCreditServiceImpl implements TcInstCreditService {
	
	@Autowired
	private TcInstCreditMapper tcInstCreditMapper;

	@Override
	public Page<TcInstCredit> pageInstCreditList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return tcInstCreditMapper.pageInstCreditList(map, rb);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteInstCredit(Map<String, Object> map) {
		tcInstCreditMapper.deleteByInstId(map);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcInstCredit> addInstCredit(TcInstCredit tcInstCredit) {
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("instId", tcInstCredit.getInstId());
		map.put("subInstId", tcInstCredit.getSubInstId());
		RetMsg<TcInstCredit> retMsg = new RetMsg<TcInstCredit>(RetMsgHelper.codeOk, "法人机构额度占用配置新增成功", "", tcInstCredit);
		List<TcInstCredit> list=tcInstCreditMapper.getInstCreditList(map);
		if(list.size()>0){
			retMsg.setCode("000001");
			retMsg.setDesc("法人机构额度占用配置已存在");
		}else{
			tcInstCreditMapper.insert(tcInstCredit);
		}
		return retMsg;
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public RetMsg<TcInstCredit> updateInstCredit(TcInstCredit tcInstCredit) {
		String subInstId=tcInstCredit.getSubInstId().split(",")[0];
		String subInstId_old=tcInstCredit.getSubInstId().split(",")[1];
		tcInstCredit.setSubInstId(subInstId);
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("instId", tcInstCredit.getInstId());
		map.put("subInstId", tcInstCredit.getSubInstId());
		RetMsg<TcInstCredit> retMsg = new RetMsg<TcInstCredit>(RetMsgHelper.codeOk, "法人机构额度占用配置修改成功", "", tcInstCredit);
		List<TcInstCredit> list=tcInstCreditMapper.getInstCreditList(map);
		if(list.size()>0){
			if(tcInstCredit.getSubInstId().equals(subInstId_old)){
				tcInstCreditMapper.updateByInstId(tcInstCredit);
			}else{
				retMsg.setCode("000001");
				retMsg.setDesc("法人机构额度占用配置已存在,不能修改！");
			}
		}else{
			tcInstCreditMapper.updateByInstId(tcInstCredit);
		}
		return retMsg;
	}
}
