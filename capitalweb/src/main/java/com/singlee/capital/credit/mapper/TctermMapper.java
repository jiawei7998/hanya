package com.singlee.capital.credit.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.Tcterm;
import tk.mybatis.mapper.common.Mapper;


public interface TctermMapper extends Mapper<Tcterm>{
	/**
	 * 查询期限配置
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<Tcterm> selectTcterm(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 删除期限配置
	 * @param termId
	 */
	public void deleteCredit(String termId);
	
	/**
	 * 更新期限配置
	 * @param tcterm
	 */
	public void updateCredit(Tcterm tcterm);
}
