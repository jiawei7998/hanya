package com.singlee.capital.credit.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.credit.pojo.CustCreditUseBean;
import com.singlee.capital.credit.service.CreditManageService;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.externalInterface.CreditInterface;

public class CustCreditInterfaceServiceImpl implements CreditInterface {

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private CreditManageService creditManageService;
	
	@Override
	public Boolean UseCredit(String serial_no) {
		
		CustCreditUseBean creditUseBean = parse(serial_no);
		
		try {
			creditManageService.creditQuotaUsed(creditUseBean);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		
		return true;
	}

	@Override
	public Boolean ReleaseCredit(String serial_no) {
		CustCreditUseBean creditUseBean = parse(serial_no);
		
		try {
			creditManageService.creditQuotaRelease(creditUseBean);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		
		return true;
	}
	
	private CustCreditUseBean parse(String dealNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dealNo", dealNo);
		TdProductApproveMain productApprove = productApproveService.getProductApproveByCondition(params);
		CustCreditUseBean creditUseBean = new CustCreditUseBean();
		creditUseBean.setDealNo(productApprove.getDealNo());
		creditUseBean.setCustNo(productApprove.getcNo());
		creditUseBean.setProduct_code(productApprove.getConTitle());
		creditUseBean.setAmt(new BigDecimal(productApprove.getAmt()));
		creditUseBean.setVdate(productApprove.getvDate());
		creditUseBean.setMdate(productApprove.getmDate());
		
		return creditUseBean;
	}

	@Override
	public int getCreditPeriod(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

}
