package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditDealBean;
import com.singlee.financial.bean.LoanBean;

import tk.mybatis.mapper.common.Mapper;

public interface TcCustCreditDealMapper extends Mapper<CustCreditDealBean> {

	/**
	 * 得到额度占用审批列表
	 */
	Page<CustCreditDealBean> getCreditDealListApprove(Map<String, Object> map, RowBounds rowBounds);

	Page<CustCreditDealBean> getCreditDealListFinish(Map<String, Object> map, RowBounds rowBounds);

	Page<CustCreditDealBean> getCreditDealListMine(Map<String, Object> map, RowBounds rowBounds);

	void deleteByDealNo(Map<String, Object> map);

	void statusChange(Map<String, Object> status);

	CustCreditDealBean getCreditDealByDealNo(String dealNo);

	Page<CustCreditDealBean> getCreditDealAll(Map<String, Object> map, RowBounds rowBounds);

	Page<CustCreditDealBean> queryCreditReleaseDeal(Map<String, Object> map, RowBounds rowBounds);

	CustCreditDealBean queryCreditReleaseDealByDealno(Map<String, Object> map);

	void updateByMap(Map<String, Object> map);

	/**
	 * 取明细台账
	 */
	List<LoanBean> getDetailEd();
	
	//交易金额增加权重，金额值为 金额*权重/100
	CustCreditDealBean getCreditDealByDealNoAddWeight(String dealNo);
	
	//由手工释放由新交易流水号查询到老交易流水号
	public String getTicketIdOld (@Param("serial_no")String serial_no);
	
}