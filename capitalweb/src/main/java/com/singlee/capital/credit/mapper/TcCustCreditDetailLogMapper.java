package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.credit.pojo.TcCustCreditDetailLog;

public interface TcCustCreditDetailLogMapper extends Mapper<TcCustCreditDetailLog>{

	/****
	 * 保存额度占用日志明细
	 * @param relationBeans
	 */
	void insertDetailLogs(List<TcCustCreditDetailLog> creditDetailLogs);

	/****
	 * 查询交易占用的额度列表信息
	 * @param params
	 * @return
	 */
	List<TcCustCreditDetailLog> selectTcCustCreditDetailLog(Map<String, Object> params);

	
	List<TcCustCreditDetailLog> selectCustCreditDetailLogByDealno(Map<String, Object> params);
}
