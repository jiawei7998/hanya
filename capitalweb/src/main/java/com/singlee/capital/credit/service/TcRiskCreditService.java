package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.credit.model.TcRiskCredit;

public interface TcRiskCreditService {

	Page<TcRiskCredit> pageTcRiskCreditList(Map<String, Object> map);
	
	RetMsg<TcRiskCredit> deleteTcRiskCredit(Map<String, Object> map);
	
	RetMsg<TcRiskCredit> addTcRiskCredit(TcRiskCredit tcRiskCredit);
	
	RetMsg<TcRiskCredit> updateTcRiskCredit(TcRiskCredit tcRiskCredit);
}
