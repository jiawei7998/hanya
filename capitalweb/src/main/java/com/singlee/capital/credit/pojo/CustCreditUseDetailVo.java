package com.singlee.capital.credit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

public class CustCreditUseDetailVo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String dealNo;
	private String productName;
	private String custName;
	private String custNo;
	private String creditName;
	private String creditNo;
	private String productCode;
	private BigDecimal      useAmt; //占用金额
	private BigDecimal      releasedAmt; //释放金额
	private String vDate;
	private String mDate;
	private String inputDate;
	private String inputTime;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public String getCreditNo() {
		return creditNo;
	}

	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getUseAmt() {
		return useAmt;
	}

	public void setUseAmt(BigDecimal useAmt) {
		this.useAmt = useAmt;
	}

	public BigDecimal getReleasedAmt() {
		return releasedAmt;
	}

	public void setReleasedAmt(BigDecimal releasedAmt) {
		this.releasedAmt = releasedAmt;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
}
