package com.singlee.capital.credit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.dict.DictConstants;
import com.singlee.capital.credit.mapper.TcCustCreditSegMapper;
import com.singlee.capital.credit.pojo.CustCreditSegBean;
import com.singlee.capital.credit.service.CreditSegService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CreditSegServiceImpl implements CreditSegService {

	@Autowired
	TcCustCreditSegMapper custCreditSegMapper;

	@Autowired
	private TrdOrderMapper approveManageDao;
	
	@Override
	public void addCustCreditSeg(Map<String, Object> map) {
		map.put("approveStatus", com.singlee.slbpm.dict.DictConstants.ApproveStatus.New);
		String trdtype = DictConstants.CREDIT_TYPE.SEGM;
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("trdtype", trdtype);
		String order_id = approveManageDao.getOrderId(hashMap);
		map.put("dealNo", order_id);
		map.put("inputDate", DateUtil.getCurrentDateAsString());
		map.put("inputTime", DateUtil.getCurrentTimeAsString());
		map.put("modifyDate",DateUtil.getCurrentDateTimeAsString());
		custCreditSegMapper.addCustCrditSeg(map);
	}

	/**
	 * 得到额度切分审批列表
	 */
	@Override
	public Page<CustCreditSegBean> getCreditSegList(Map<String, Object> map) {
		Page<CustCreditSegBean> list = custCreditSegMapper.getCreditSegList(map, ParameterUtil.getRowBounds(map));
		return list;
	}

	@Override
	public void deleteCreditSeg(Map<String, Object> map) {
		custCreditSegMapper.deleteByDealNo(map);
	}

	@Override
	public void updateCreditSeg(Map<String, Object> map) {
		map.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		custCreditSegMapper.updateByDealNo(map);
	}

	@Override
	public Page<CustCreditSegBean> getSegDetailByCredit(Map<String, Object> map) {
		Page<CustCreditSegBean> list = custCreditSegMapper.getSegDetailByCredit(map, ParameterUtil.getRowBounds(map));
		return list;
	}

	@Override
	public void statusChange(Map<String, Object> status) {
		status.put("modifyDate", DateUtil.getCurrentDateTimeAsString());
		custCreditSegMapper.statusChange(status);
	}

	@Override
	public CustCreditSegBean getCreditSegByDealNo(String serial_no) {
		return custCreditSegMapper.getCreditSegByDealNo(serial_no);
	}

	@Override
	public List<CustCreditSegBean> getSegDetailByCreditAll(
			Map<String, Object> map) {
		List<CustCreditSegBean> list = custCreditSegMapper.getSegDetailByCreditAll(map);
		return list;
	}

}
