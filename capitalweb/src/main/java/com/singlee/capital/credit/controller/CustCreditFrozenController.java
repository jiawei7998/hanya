package com.singlee.capital.credit.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.credit.pojo.CustCreditFrozenBean;
import com.singlee.capital.credit.service.CreditFrozenService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/CustCreditFrozenController")
public class CustCreditFrozenController extends CommonController {

	@Autowired
	CreditFrozenService creditFrozenService;
	
	/**
	 * 新增一笔额度冻结审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addCustCreditFrozen")
	public RetMsg<Serializable> addCustCreditFrozen(@RequestBody Map<String,Object> map) {
		if(map.get("oldFrozenAmt") == null) {
			map.put("oldFrozenAmt",map.get("frozenAmt"));
		}
		creditFrozenService.addCustCreditFrozen(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到发起额度冻结审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditFrozenListMine")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getCreditFrozenListMine(@RequestBody Map<String,Object> map) {
		map.put("ioper", SlSessionHelper.getUserId());
		Page<CustCreditFrozenBean> list = creditFrozenService.getCreditFrozenListMine(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getCreditFrozenListForDialog")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getCreditFrozenListForDialog(@RequestBody Map<String,Object> map) {
		map.put("ioper", SlSessionHelper.getUserId());
		Page<CustCreditFrozenBean> list = creditFrozenService.getCreditFrozenListForDialog(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getCreditFrozenListForDetail")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getCreditFrozenListForDetail(@RequestBody Map<String,Object> map) {
		map.put("ioper", SlSessionHelper.getUserId());
		Page<CustCreditFrozenBean> list = creditFrozenService.getCreditFrozenListForDetail(map);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 得到发起额度冻结审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getFrozenDetailByCreditAll")
	public RetMsg<List<CustCreditFrozenBean>> getFrozenDetailByCreditAll(@RequestBody Map<String,Object> map) {
		List<CustCreditFrozenBean> frolist = creditFrozenService.getFrozenDetailByCreditAll(map);
		return RetMsgHelper.ok(frolist);
	}
	
	/**
	 * 得到待审批额度冻结审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditFrozenList")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getCreditFrozenList(@RequestBody Map<String,Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.WaitApprove);
		map.put("approveStatus_Approving", DictConstants.ApproveStatus.Approving);
		Page<CustCreditFrozenBean> list = creditFrozenService.getCreditFrozenList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到审批完成额度冻结审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditFrozenListFinish")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getCreditFrozenApprovedPass(@RequestBody Map<String,Object> map) {
		map.put("approveStatus", DictConstants.ApproveStatus.ApprovedPass);
		Page<CustCreditFrozenBean> list = creditFrozenService.getCreditFrozenListFinish(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 删除额度冻结审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCreditFrozen")
	public RetMsg<Serializable> deleteCreditFrozen(@RequestBody Map<String,Object> map) {
		creditFrozenService.deleteCreditFrozen(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改额度冻结审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditFrozen")
	public RetMsg<Serializable> updateCreditFrozen(@RequestBody Map<String,Object> map) {
		creditFrozenService.updateCreditFrozen(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 拿到额度品种的冻结详情
	 * @param 额度品种
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getFrozenDetailByCredit")
	public RetMsg<PageInfo<CustCreditFrozenBean>> getFrozenDetailByCredit(@RequestBody Map<String,Object> map) {
		Page<CustCreditFrozenBean> list = creditFrozenService.getFrozenDetailByCredit(map);
		return RetMsgHelper.ok(list);
	}
	
}
