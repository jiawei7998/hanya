package com.singlee.capital.credit.pojo;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "TC_CUST_CREDIT_DEAL")
public class CustCreditDealBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	// 交易流水号
	private String dealNo;
	// 业务品种
	private String productCode;
	// 业务名称
	private String productName;
	// 交易对手编号
	private String custNo;
	// 交易对手名称
	private String custName;
	// 经营单位
	private String institution;
	// 起息日
	private String vdate;
	// 到期日
	private String mdate;
	// 金额
	private double amt;
	// 录入理由
	private String reason;
	// 备注1
	private String remark1;
	// 备注2
	private String remark2;
	// 状态
	private int state;
	// 老的处理编号
	private String oldDealNo;
	// 老的金额
	private String oldAmt;
	// 操作员
	private String ioper;
	// 是否是线下手工录入的交易
	private String offLine;
	// 审批状态
	private int approveStatus;
	// 修改日期
	private String modifyDate;
	// 上送日期
	private String inputDate;
	// 上送时间
	private String inputTime;
	//权重
	private String weight;
	
	
	@Transient
	private String taskId;

	@Transient
	private String sponInstName;// 20180529新加

	
	
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getOffLine() {
		return offLine;
	}

	public void setOffLine(String offLine) {
		this.offLine = offLine;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public int getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getOldDealNo() {
		return oldDealNo;
	}

	public void setOldDealNo(String oldDealNo) {
		this.oldDealNo = oldDealNo;
	}

	public String getOldAmt() {
		return oldAmt;
	}

	public void setOldAmt(String oldAmt) {
		this.oldAmt = oldAmt;
	}

	public String getSponInstName() {
		return sponInstName;
	}

	public void setSponInstName(String sponInstName) {
		this.sponInstName = sponInstName;
	}
}