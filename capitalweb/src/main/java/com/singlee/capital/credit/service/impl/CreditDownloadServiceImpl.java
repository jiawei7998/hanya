package com.singlee.capital.credit.service.impl;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.credit.pojo.*;
import com.singlee.capital.credit.service.*;
import com.singlee.capital.credit.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
@SuppressWarnings("deprecation")
@Service
public class CreditDownloadServiceImpl implements CreditDownloadService {

	@Autowired
	CreditManageService creditManageService;
	
	@Autowired
	CustCreditDealRelationService custCreditDealRelationService;
	
	@Autowired
	CreditFrozenService creditFrozenService;
	
	@Autowired
	CreditAdjService creditAdjService;
	
	@Autowired
	CreditSegService creditSegService;

	
	@Override
	public ExcelUtil downloadExcel(HashMap<String, Object> map) {
		List<CustCreditBean> list = creditManageService.getCreditListByCustNo(map);
		CustCreditBean bean = new CustCreditBean();
		ExcelUtil e = null;
		try {
			//表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CreationHelper creationHelper = e.getWb().getCreationHelper();
			Hyperlink link = null;
			CellStyle center = e.getDefaultCenterStrCellStyle();
			int sheet = 0;
			int sheets = sheet;
			int row = 0;
			int rowindex = 0;
			e.getWb().createSheet("客户额度信息表");
			e.writeStr(sheet,row, "A",center, "客户编号");     
			e.writeStr(sheet,row, "B",center, "客户名称"); 
			e.writeStr(sheet,row, "C",center, "授信额度"); 
			e.writeStr(sheet,row, "D",center, "额度切分");       
			e.writeStr(sheet,row, "E",center, "额度调整");   
			e.writeStr(sheet,row, "F", center,"已用金额");   
			e.writeStr(sheet,row, "G",center, "冻结金额");   
			e.writeStr(sheet,row, "H", center,"可用金额");       
			e.writeStr(sheet,row, "I", center,"备注"); 
			//设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			//设置默认行高
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			
			for (int j = 0; j < list.size(); j++) {
				if(j % 100 == 0) {
					sheets++;
					rowindex = createExcelSheet(e,sheets,rowindex,center);
				}
				bean = list.get(j);
				row++;
				e.writeStr(sheet, row, "A", center, bean.getCustNo());
				e.writeStr(sheet, row, "B", center, bean.getCustName());
				e.writeDouble(sheet, row, "C", e.getDefaultDouble2CellStyle(),bean.getCreditAmt().doubleValue());
				e.writeDouble(sheet,row,"D",e.getDefaultDouble2CellStyle(),bean.getQuotaSegAmt().doubleValue());
				e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),bean.getQuotaAdjAmt().doubleValue());
				e.writeDouble(sheet,row,"F",e.getDefaultDouble2CellStyle(),bean.getQuotaUsedAmt().doubleValue());
				e.writeDouble(sheet, row, "G", e.getDefaultDouble2CellStyle(),bean.getQuotaFrozenAmt().doubleValue());
				e.writeDouble(sheet,row,"H",e.getDefaultDouble2CellStyle(),bean.getQuotaAvlAmt().doubleValue());
				e.writeStr(sheet, row, "I", center, bean.getRemark1());
				
				//进入额度详情链接
				Cell cell = e.getSheetAt(sheet).getRow(row).getCell(1);
				link.setAddress("客户额度详情"+ sheets + "!A" + rowindex);
				cell.setHyperlink(link);
				
				map.put("custNo", bean.getCustNo());
				List<CustCreditBean> lists = creditManageService.getCustCreditListAll(map);
				rowindex = createExcelChildren(e,sheets,rowindex,lists,center);
			}
			list.clear();
			
			
			return e;
			
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
	}

	@Override
	public ExcelUtil downloadDetailExcel(HashMap<String, Object> map) {
		List<CustCreditBean> list = creditManageService.getCustCreditListAll(map);
		CustCreditBean bean = new CustCreditBean();
		ExcelUtil e = null;
		try {
			
			//表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CreationHelper creationHelper = e.getWb().getCreationHelper();
			Hyperlink link = null;
			CellStyle center = e.getDefaultCenterStrCellStyle();
			int row = 0;
			int adjrow = 1;
			int segrow = 1;
			int frorow = 1;
			int userow = 1;
			
			e.getWb().createSheet("客户额度详情表");
			e.writeStr(0,row, "A",center, "客户名称"); 
			e.writeStr(0,row, "B",center, "额度品种"); 
			e.writeStr(0,row, "C",center, "授信额度"); 
			e.writeStr(0,row, "D",center, "额度切分");       
			e.writeStr(0,row, "E",center, "额度调整");   
			e.writeStr(0,row, "F", center,"已用金额");   
			e.writeStr(0,row, "G",center, "冻结金额");   
			e.writeStr(0,row, "H", center,"可用金额");  
			e.writeStr(0,row, "I",center, "串用金额");   
			e.writeStr(0,row, "J", center,"被串用金额");
			e.writeStr(0,row, "K", center,"备注");
			//设置列宽
			e.getSheetAt(0).setColumnWidth(0, 20 * 256);
			e.getSheetAt(0).setColumnWidth(1, 20 * 256);
			e.getSheetAt(0).setColumnWidth(2, 20 * 256);
			e.getSheetAt(0).setColumnWidth(3, 20 * 256);
			e.getSheetAt(0).setColumnWidth(4, 20 * 256);
			e.getSheetAt(0).setColumnWidth(5, 20 * 256);
			e.getSheetAt(0).setColumnWidth(6, 20 * 256);
			e.getSheetAt(0).setColumnWidth(7, 20 * 256);
			e.getSheetAt(0).setColumnWidth(8, 20 * 256);
			e.getSheetAt(0).setColumnWidth(9, 20 * 256);
			e.getSheetAt(0).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			
			for (int j = 0; j < list.size(); j++) {
				if(j == 0) {
					createDetailExcelSheet(e,center);
				}
				bean = list.get(j);
				row++;
				e.writeStr(0, row, "A", center, bean.getCustName());
				e.writeStr(0, row, "B", center, bean.getCreditName());
				e.writeDouble(0, row, "C", e.getDefaultDouble2CellStyle(),bean.getCreditAmt().doubleValue());
				e.writeDouble(0,row,"D",e.getDefaultDouble2CellStyle(),bean.getQuotaSegAmt().doubleValue());
				e.writeDouble(0, row, "E", e.getDefaultDouble2CellStyle(),bean.getQuotaAdjAmt().doubleValue());
				e.writeDouble(0,row,"F",e.getDefaultDouble2CellStyle(),bean.getQuotaUsedAmt().doubleValue());
				e.writeDouble(0, row, "G", e.getDefaultDouble2CellStyle(),bean.getQuotaFrozenAmt().doubleValue());
				e.writeDouble(0,row,"H",e.getDefaultDouble2CellStyle(),bean.getQuotaAvlAmt().doubleValue());
				e.writeDouble(0, row, "I", e.getDefaultDouble2CellStyle(),bean.getQuotaStrAmt().doubleValue());
				e.writeDouble(0,row,"J",e.getDefaultDouble2CellStyle(),bean.getQuotaCstrAmt().doubleValue());
				e.writeStr(0, row, "K", center, bean.getRemark1());
				
				map.put("custCreditId", bean.getCustCreditId());
				List<CustCreditAdjBean> adjlist = creditAdjService.getAdjDetailByCreditAll(map);
				if(adjlist.size() != 0) {
					//进入额度调整详情链接
					Cell cell = e.getSheetAt(0).getRow(row).getCell(4);
					link.setAddress("客户额度详情调整明细表!A" + adjrow);
					cell.setHyperlink(link);
					adjrow = createAdjExcelChildren(e,1,adjrow,center,adjlist);
				}
				
				List<CustCreditSegBean> seglist = creditSegService.getSegDetailByCreditAll(map);
				if(seglist.size() != 0) {
					//进入额度切分详情链接
					Cell cell = e.getSheetAt(0).getRow(row).getCell(3);
					link.setAddress("客户额度详情切分明细表!A" + segrow);
					cell.setHyperlink(link);
					segrow = createSegExcelChildren(e,2,segrow,center,seglist);
				}
				
				List<CustCreditFrozenBean> frolist = creditFrozenService.getFrozenDetailByCreditAll(map);
				if(frolist.size() != 0) {
					//进入额度冻结详情链接
					Cell cell = e.getSheetAt(0).getRow(row).getCell(6);
					link.setAddress("客户额度详情调冻结细表!A" + frorow);
					cell.setHyperlink(link);
					frorow = createFroExcelChildren(e,3,frorow,center,frolist);
				}
				
				List<CustCreditUseDetailVo> uselist = custCreditDealRelationService.getUseDetailByCreditAll(map);
				if(uselist.size() != 0) {
					//进入额度占用详情链接
					Cell cell = e.getSheetAt(0).getRow(row).getCell(5);
					link.setAddress("客户额度详情占用明细表!A" + userow);
					cell.setHyperlink(link);
					userow = createUseExcelChildren(e,4,userow,center,uselist);
				}
				
			}
			list.clear();
			
			return e;
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
	}
	
	/**
	 * 下载所有客户详情表格
	 */
	@Override
	public ExcelUtil downloadExcelAll(HashMap<String, Object> map) {
		List<CustCreditBean> list = creditManageService.getCustCreditListAll(map);
		CustCreditBean bean = new CustCreditBean();
		ExcelUtil e = null;
		try {
			//表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			int row = 0;
			
			e.getWb().createSheet("客户额度详情表");
			e.writeStr(0,row, "A",center, "客户名称"); 
			e.writeStr(0,row, "B",center, "额度品种"); 
			e.writeStr(0,row, "C",center, "授信额度"); 
			e.writeStr(0,row, "D",center, "额度切分");       
			e.writeStr(0,row, "E",center, "额度调整");   
			e.writeStr(0,row, "F", center,"已用金额");   
			e.writeStr(0,row, "G",center, "冻结金额");   
			e.writeStr(0,row, "H", center,"可用金额");  
			e.writeStr(0,row, "I",center, "串用金额");   
			e.writeStr(0,row, "J", center,"被串用金额");
			e.writeStr(0,row, "K", center,"备注");
			//设置列宽
			e.getSheetAt(0).setColumnWidth(0, 20 * 256);
			e.getSheetAt(0).setColumnWidth(1, 20 * 256);
			e.getSheetAt(0).setColumnWidth(2, 20 * 256);
			e.getSheetAt(0).setColumnWidth(3, 20 * 256);
			e.getSheetAt(0).setColumnWidth(4, 20 * 256);
			e.getSheetAt(0).setColumnWidth(5, 20 * 256);
			e.getSheetAt(0).setColumnWidth(6, 20 * 256);
			e.getSheetAt(0).setColumnWidth(7, 20 * 256);
			e.getSheetAt(0).setColumnWidth(8, 20 * 256);
			e.getSheetAt(0).setColumnWidth(9, 20 * 256);
			e.getSheetAt(0).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			
			for (int j = 0; j < list.size(); j++) {
				
				
				bean = list.get(j);
				row++;
				e.writeStr(0, row, "A", center, bean.getCustName());
				e.writeStr(0, row, "B", center, bean.getCreditName());
				e.writeDouble(0, row, "C", e.getDefaultDouble2CellStyle(),bean.getCreditAmt().doubleValue());
				e.writeDouble(0,row,"D",e.getDefaultDouble2CellStyle(),bean.getQuotaSegAmt().doubleValue());
				e.writeDouble(0, row, "E", e.getDefaultDouble2CellStyle(),bean.getQuotaAdjAmt().doubleValue());
				e.writeDouble(0,row,"F",e.getDefaultDouble2CellStyle(),bean.getQuotaUsedAmt().doubleValue());
				e.writeDouble(0, row, "G", e.getDefaultDouble2CellStyle(),bean.getQuotaFrozenAmt().doubleValue());
				e.writeDouble(0,row,"H",e.getDefaultDouble2CellStyle(),bean.getQuotaAvlAmt().doubleValue());
				e.writeDouble(0, row, "I", e.getDefaultDouble2CellStyle(),bean.getQuotaStrAmt().doubleValue());
				e.writeDouble(0,row,"J",e.getDefaultDouble2CellStyle(),bean.getQuotaCstrAmt().doubleValue());
				e.writeStr(0, row, "K", center, bean.getRemark1());
			}
			//设置默认行高
			e.getSheetAt(0).setDefaultRowHeightInPoints(20);
			return e;
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
	}

	/**
	 * 生成额度每页的首行
	 * @param e
	 * @param sheet
	 * @param row
	 * @param center
	 * @return
	 */
	private static int createExcelSheet(ExcelUtil e,int sheet,int row,CellStyle center) {
		try {
			e.getWb().createSheet("客户额度详情"+sheet);
			e.writeStr(sheet,row, "A",center, "客户名称"); 
			e.writeStr(sheet,row, "B",center, "额度品种"); 
			e.writeStr(sheet,row, "C",center, "授信额度"); 
			e.writeStr(sheet,row, "D",center, "额度切分");       
			e.writeStr(sheet,row, "E",center, "额度调整");   
			e.writeStr(sheet,row, "F", center,"已用金额");   
			e.writeStr(sheet,row, "G",center, "冻结金额");   
			e.writeStr(sheet,row, "H", center,"可用金额");  
			e.writeStr(sheet,row, "I",center, "串用金额");   
			e.writeStr(sheet,row, "J", center,"被串用金额");
			e.writeStr(sheet,row, "K", center,"备注");
			//设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(9, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(10, 20 * 256);
			
			//设置默认行高
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			
			return row+2;
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
	}
	
	/**
	 * 生成客户额度详情的信息
	 * @param e
	 * @param sheet
	 * @param row
	 * @param list
	 * @param center
	 * @return
	 */
	private static int createExcelChildren(ExcelUtil e,int sheet,int row,List<CustCreditBean> list,CellStyle center) {
		CreationHelper creationHelper = e.getWb().getCreationHelper();
		Hyperlink link = null;
		if(list == null) {
			return row;
		}
		CustCreditBean bean = new CustCreditBean();
		try {
			//返回首页链接
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			e.writeStr(sheet, row, "A", center, "<<返回目录");
			Cell cell = e.getSheetAt(sheet).getRow(row).getCell(0);
			link.setAddress("客户额度信息表!A1");
			cell.setHyperlink(link);
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			
		for (int j = 0; j < list.size(); j++) {
			bean = list.get(j);
			row++;
			e.writeStr(sheet, row, "A", center, bean.getCustName());
			e.writeStr(sheet, row, "B", center, bean.getCreditName());
			e.writeDouble(sheet, row, "C", e.getDefaultDouble2CellStyle(),bean.getCreditAmt().doubleValue());
			e.writeDouble(sheet,row,"D",e.getDefaultDouble2CellStyle(),bean.getQuotaSegAmt().doubleValue());
			e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),bean.getQuotaAdjAmt().doubleValue());
			e.writeDouble(sheet,row,"F",e.getDefaultDouble2CellStyle(),bean.getQuotaUsedAmt().doubleValue());
			e.writeDouble(sheet, row, "G", e.getDefaultDouble2CellStyle(),bean.getQuotaFrozenAmt().doubleValue());
			e.writeDouble(sheet,row,"H",e.getDefaultDouble2CellStyle(),bean.getQuotaAvlAmt().doubleValue());
			e.writeDouble(sheet, row, "I", e.getDefaultDouble2CellStyle(),bean.getQuotaStrAmt().doubleValue());
			e.writeDouble(sheet,row,"J",e.getDefaultDouble2CellStyle(),bean.getQuotaCstrAmt().doubleValue());
			e.writeStr(sheet, row, "K", center, bean.getRemark1());
			
		}
		list.clear();
		} catch (Exception e1) {
			throw new RException(e1);
		}
		return row+2;
	}

	/** 生成额度每页的首行/新增设置列宽
	 * @param e
	 * @param sheet
	 * @param row
	 * @param center
	 * @return
	 */
	private static void createDetailExcelSheet(ExcelUtil e,CellStyle center) {
		try {
			//新建调整详情表
			e.getWb().createSheet("客户额度详情调整明细表");
			e.getSheetAt(1).setColumnWidth(0, 20 * 256);
			e.getSheetAt(1).setColumnWidth(1, 25 * 256);
			e.getSheetAt(1).setColumnWidth(2, 20 * 256);
			e.getSheetAt(1).setColumnWidth(3, 20 * 256);
			e.getSheetAt(1).setColumnWidth(4, 20 * 256);
			e.getSheetAt(1).setColumnWidth(5, 20 * 256);
			e.getSheetAt(1).setColumnWidth(6, 20 * 256);
			e.getSheetAt(1).setColumnWidth(7, 20 * 256);
			e.getSheetAt(1).setColumnWidth(8, 20 * 256);
			e.getSheetAt(1).setColumnWidth(9, 20 * 256);
			e.getSheetAt(1).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(1).setDefaultRowHeightInPoints(20);
			//额度调整页头
			e.writeStr(1,0, "A",center, "客户名称"); 
			e.writeStr(1,0, "B",center, "交易编号"); 
			e.writeStr(1,0, "C",center, "额度类型编号"); 
			e.writeStr(1,0, "D",center, "额度类型名称");       
			e.writeStr(1,0, "E",center, "授信额度");   
			e.writeStr(1,0, "F", center,"可用调整额度");  
			e.writeStr(1,0, "G", center,"调整额度"); 
			e.writeStr(1,0, "H",center, "调整原因");   
			e.writeStr(1,0, "I", center,"调整发起人");  
			e.writeStr(1,0, "J",center, "发起人机构");  
			
			//新建切分详情表
			e.getWb().createSheet("客户额度详情切分明细表");
			e.getSheetAt(2).setColumnWidth(0, 20 * 256);
			e.getSheetAt(2).setColumnWidth(1, 25 * 256);
			e.getSheetAt(2).setColumnWidth(2, 20 * 256);
			e.getSheetAt(2).setColumnWidth(3, 20 * 256);
			e.getSheetAt(2).setColumnWidth(4, 20 * 256);
			e.getSheetAt(2).setColumnWidth(5, 20 * 256);
			e.getSheetAt(2).setColumnWidth(6, 20 * 256);
			e.getSheetAt(2).setColumnWidth(7, 20 * 256);
			e.getSheetAt(2).setColumnWidth(8, 20 * 256);
			e.getSheetAt(2).setColumnWidth(9, 20 * 256);
			e.getSheetAt(2).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(2).setDefaultRowHeightInPoints(20);
			//额度切分页头
			e.writeStr(2,0, "A",center, "客户名称"); 
			e.writeStr(2,0, "B",center, "交易编号"); 
			e.writeStr(2,0, "C",center, "额度类型编号"); 
			e.writeStr(2,0, "D",center, "额度类型名称");       
			e.writeStr(2,0, "E",center, "授信额度");   
			e.writeStr(2,0, "F", center,"可用切分额度");  
			e.writeStr(2,0, "G", center,"切分额度");
			e.writeStr(2,0, "H",center, "调整原因");   
			e.writeStr(2,0, "I", center,"调整发起人");  
			e.writeStr(2,0, "J",center, "发起人机构");  
			
			//新建冻结详情表
			e.getWb().createSheet("客户额度详情冻结明细表");
			e.getSheetAt(3).setColumnWidth(0, 20 * 256);
			e.getSheetAt(3).setColumnWidth(1, 25 * 256);
			e.getSheetAt(3).setColumnWidth(2, 20 * 256);
			e.getSheetAt(3).setColumnWidth(3, 20 * 256);
			e.getSheetAt(3).setColumnWidth(4, 20 * 256);
			e.getSheetAt(3).setColumnWidth(5, 20 * 256);
			e.getSheetAt(3).setColumnWidth(6, 20 * 256);
			e.getSheetAt(3).setColumnWidth(7, 20 * 256);
			e.getSheetAt(3).setColumnWidth(8, 20 * 256);
			e.getSheetAt(3).setColumnWidth(9, 20 * 256);
			e.getSheetAt(3).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(3).setDefaultRowHeightInPoints(20);
			//额度冻结页头
			e.writeStr(3,0, "A",center, "客户名称"); 
			e.writeStr(3,0, "B",center, "交易编号"); 
			e.writeStr(3,0, "C",center, "额度类型编号"); 
			e.writeStr(3,0, "D",center, "额度类型名称");       
			e.writeStr(3,0, "E",center, "授信额度");   
			e.writeStr(3,0, "F", center,"可用调整额度");  
			e.writeStr(3,0, "G", center,"调整额度"); 
			e.writeStr(3,0, "H",center, "调整原因");   
			e.writeStr(3,0, "I", center,"调整发起人");  
			e.writeStr(3,0, "J",center, "发起人机构");    
			e.writeStr(3,0, "K", center,"起息日");  
			e.writeStr(3,0, "L",center, "到期日");
			
			//新建占用详情表
			e.getWb().createSheet("客户额度详情占用明细表");
			e.getSheetAt(4).setColumnWidth(0, 20 * 256);
			e.getSheetAt(4).setColumnWidth(1, 25 * 256);
			e.getSheetAt(4).setColumnWidth(2, 20 * 256);
			e.getSheetAt(4).setColumnWidth(3, 20 * 256);
			e.getSheetAt(4).setColumnWidth(4, 20 * 256);
			e.getSheetAt(4).setColumnWidth(5, 20 * 256);
			e.getSheetAt(4).setColumnWidth(6, 20 * 256);
			e.getSheetAt(4).setColumnWidth(7, 20 * 256);
			e.getSheetAt(4).setColumnWidth(8, 20 * 256);
			e.getSheetAt(4).setColumnWidth(9, 20 * 256);
			e.getSheetAt(4).setColumnWidth(10, 20 * 256);
			//设置默认行高
			e.getSheetAt(4).setDefaultRowHeightInPoints(20);
			//额度占用页头
			e.writeStr(4,0, "A",center, "客户名称"); 
			e.writeStr(4,0, "B",center, "交易编号"); 
			e.writeStr(4,0, "C",center, "产品编号"); 
			e.writeStr(4,0, "D",center, "产品名称");       
			e.writeStr(4,0, "E",center, "占用金额");   
			e.writeStr(4,0, "F", center,"释放金额");  
			e.writeStr(4,0, "G", center,"交易日期"); 
			e.writeStr(4,0, "H",center, "交易时间");      
			e.writeStr(4,0, "I", center,"起息日");  
			e.writeStr(4,0, "J",center, "到期日");
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
		return;
	}
	
	/**
	 * 生成客户额度详情调整明细的信息
	 * @param e
	 * @param sheet
	 * @param row
	 * @param list
	 * @param center
	 * @return
	 */
	private static int createAdjExcelChildren(ExcelUtil e,int sheet,int row,CellStyle center
								,List<CustCreditAdjBean> adjlist) {
		CreationHelper creationHelper = e.getWb().getCreationHelper();
		Hyperlink link = null;

		CustCreditAdjBean adjbean = new CustCreditAdjBean();
		try {
			//返回首页链接
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			e.writeStr(sheet, row, "A", center, "<<返回目录");
			Cell cell = e.getSheetAt(sheet).getRow(row).getCell(0);
			link.setAddress("客户额度详情表!A1");
			cell.setHyperlink(link);
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			
			//额度调整内容
			for (int j = 0; j < adjlist.size(); j++) {
				adjbean = adjlist.get(j);
				row++;
				e.writeStr(sheet, row, "A", center, adjbean.getCustName());
				e.writeStr(sheet, row, "B", center, adjbean.getDealNo());
				e.writeStr(sheet, row, "C", center, adjbean.getCreditId());
				e.writeStr(sheet, row, "D", center, adjbean.getCreditName());
				e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),adjbean.getCreditAmt().doubleValue());
				e.writeDouble(sheet, row, "F", e.getDefaultDouble2CellStyle(),adjbean.getQuotaAvlAmt().doubleValue());
				e.writeDouble(sheet,row,"G",e.getDefaultDouble2CellStyle(),adjbean.getQuotaAdjAmt().doubleValue());
				e.writeStr(sheet, row, "H", center, adjbean.getReason());
				e.writeStr(sheet,row,"I", center, adjbean.getIoper());
				e.writeStr(sheet, row, "J",  center, adjbean.getInstitution());
			}
			
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
		return row+2;
	}
	
	 /** 生成客户额度详情切分明细的信息
	 * @param e
	 * @param sheet
	 * @param row
	 * @param center
	 * @param seglist
	 * @return
	 */
	private static int createSegExcelChildren(ExcelUtil e,int sheet,int row,CellStyle center
								,List<CustCreditSegBean> seglist) {
		CreationHelper creationHelper = e.getWb().getCreationHelper();
		Hyperlink link = null;

		CustCreditSegBean segbean = new CustCreditSegBean();
		try {
			//返回首页链接
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			e.writeStr(sheet, row, "A", center, "<<返回目录");
			Cell cell = e.getSheetAt(sheet).getRow(row).getCell(0);
			link.setAddress("客户额度详情表!A1");
			cell.setHyperlink(link);
			
			//额度切分内容
			for (int j = 0; j < seglist.size(); j++) {
				segbean = seglist.get(j);
				row++;
				e.writeStr(sheet, row, "A", center, segbean.getCustName());
				e.writeStr(sheet, row, "B", center, segbean.getDealNo());
				e.writeStr(sheet, row, "C", center, segbean.getCreditId());
				e.writeStr(sheet, row, "D", center, segbean.getCreditName());
				e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),segbean.getCreditAmt().doubleValue());
				e.writeDouble(sheet, row, "F", e.getDefaultDouble2CellStyle(),segbean.getQuotaAvlAmt().doubleValue());
				e.writeDouble(sheet,row,"G",e.getDefaultDouble2CellStyle(),segbean.getQuotaSegAmt().doubleValue());
				e.writeStr(sheet, row, "H", center, segbean.getUse());
				e.writeStr(sheet,row,"I", center, segbean.getIoper());
				e.writeStr(sheet, row, "J",  center, segbean.getInstitution());
			}
			
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
		return row+2;
	}
	 
	 /** 生成客户额度详情冻结明细的信息
	 * @param e
	 * @param sheet
	 * @param row
	 * @param list
	 * @param center
	 * @return
	 */
	private static int createFroExcelChildren(ExcelUtil e,int sheet,int row,CellStyle center
								,List<CustCreditFrozenBean> frolist) {
		CreationHelper creationHelper = e.getWb().getCreationHelper();
		Hyperlink link = null;

		CustCreditFrozenBean frobean = new CustCreditFrozenBean();
		try {
			//返回首页链接
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			e.writeStr(sheet, row, "A", center, "<<返回目录");
			Cell cell = e.getSheetAt(sheet).getRow(row).getCell(0);
			link.setAddress("客户额度详情表!A1");
			cell.setHyperlink(link);
			
			//额度冻结内容
			for (int j = 0; j < frolist.size(); j++) {
				frobean = frolist.get(j);
				row++;
				e.writeStr(sheet, row, "A", center, frobean.getCustName());
				e.writeStr(sheet, row, "B", center, frobean.getDealNo());
				e.writeStr(sheet, row, "C", center, frobean.getCreditId());
				e.writeStr(sheet, row, "D", center, frobean.getCreditName());
				e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),frobean.getCreditAmt().doubleValue());
				e.writeDouble(sheet, row, "F", e.getDefaultDouble2CellStyle(),frobean.getQuotaAvlAmt().doubleValue());
				e.writeDouble(sheet,row,"G",e.getDefaultDouble2CellStyle(),frobean.getFrozenAmt().doubleValue());
				e.writeStr(sheet, row, "H", center, frobean.getReason());
				e.writeStr(sheet,row,"I", center, frobean.getIoper());
				e.writeStr(sheet, row, "J",  center, frobean.getInstitution());
				e.writeStr(sheet,row,"K", center, frobean.getVdate());
				e.writeStr(sheet, row, "L",  center, frobean.getMdate());
			}
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
		return row+2;
	}
	
	/** 生成客户额度详情占用明细的信息
	 * @param e
	 * @param sheet
	 * @param row
	 * @param list
	 * @param center
	 * @return
	 */
	private static int createUseExcelChildren(ExcelUtil e,int sheet,int row,CellStyle center
								,List<CustCreditUseDetailVo> uselist) {
		CreationHelper creationHelper = e.getWb().getCreationHelper();
		Hyperlink link = null;

		CustCreditUseDetailVo usebean = new CustCreditUseDetailVo();
		try {
			//返回首页链接
			e.getSheetAt(sheet).setDefaultRowHeightInPoints(20);
			e.writeStr(sheet, row, "A", center, "<<返回目录");
			Cell cell = e.getSheetAt(sheet).getRow(row).getCell(0);
			link.setAddress("客户额度详情表!A1");
			cell.setHyperlink(link);
			
			//额度占用内容
			for (int j = 0; j < uselist.size(); j++) {
				usebean = uselist.get(j);
				row++;
				e.writeStr(sheet, row, "A", center, usebean.getCustName());
				e.writeStr(sheet, row, "B", center, usebean.getDealNo());
				e.writeStr(sheet, row, "C", center, usebean.getProductCode());
				e.writeStr(sheet, row, "D", center, usebean.getProductName());
				e.writeDouble(sheet, row, "E", e.getDefaultDouble2CellStyle(),usebean.getUseAmt().doubleValue());
				e.writeDouble(sheet, row, "F", e.getDefaultDouble2CellStyle(),usebean.getReleasedAmt().doubleValue());
				e.writeStr(sheet,row,"G",center, usebean.getInputDate());
				e.writeStr(sheet, row, "H", center, usebean.getInputTime());
				e.writeStr(sheet,row,"I", center, usebean.getvDate());
				e.writeStr(sheet, row, "J",  center, usebean.getmDate());
			}
			
		} catch (Exception e1) {
			//e.printStackTrace();
			throw new RException(e1);
		}
		return row+2;
	}
	
	

}
