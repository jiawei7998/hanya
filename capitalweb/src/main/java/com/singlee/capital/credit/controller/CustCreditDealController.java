package com.singlee.capital.credit.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.pojo.CustCreditDealBean;
import com.singlee.capital.credit.service.CreditDealService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/CustCreditDealController")
public class CustCreditDealController extends CommonController {

	@Autowired
	CreditDealService creditDealService;
	
	/**
	 * 新增一笔额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/addCustCreditDeal")
	public RetMsg<Serializable> addCustCreditDeal(@RequestBody Map<String,Object> map) {
		if(map.get("oldAmt") == null) {
			map.put("oldAmt",map.get("amt"));
		}
		creditDealService.addCustCreditDeal(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到发起的额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealList")
	public RetMsg<PageInfo<CustCreditDealBean>> getCreditDealList(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<CustCreditDealBean> list = creditDealService.getCreditDealListMine(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到待审批额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealWaitApprove")
	public RetMsg<PageInfo<CustCreditDealBean>> getCreditDealWaitApprove(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<CustCreditDealBean> list = creditDealService.getCreditDealListApprove(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 得到审批完成额度占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealApprovedPass")
	public RetMsg<PageInfo<CustCreditDealBean>> getCreditDealApprovedPass(@RequestBody Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		Page<CustCreditDealBean> list = creditDealService.getCreditDealListFinish(map);
		return RetMsgHelper.ok(list);
	}
	
	
	/**
	 * 删除一笔额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCreditDeal")
	public RetMsg<Serializable> deleteCreditDeal(@RequestBody Map<String,Object> map) {
		creditDealService.deleteCreditDeal(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 得到所有占用审批列表
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealAll")
	public RetMsg<PageInfo<CustCreditDealBean>> getCreditDealAll(@RequestBody Map<String,Object> map) {
		Page<CustCreditDealBean> list = creditDealService.getCreditDealAll(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 修改额度占用审批
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCreditDeal")
	public RetMsg<Serializable> updateCreditDeal(@RequestBody Map<String,Object> map) {
		creditDealService.updateCreditDeal(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询所有有余额的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCreditReleaseDeal")
	public RetMsg<PageInfo<CustCreditDealBean>> queryCreditReleaseDeal(@RequestBody Map<String,Object> map) {
		Page<CustCreditDealBean> list = creditDealService.queryCreditReleaseDeal(map,ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 查询需要释放额度的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCreditReleaseDealByDealno")
	public RetMsg<CustCreditDealBean> queryCreditReleaseDealByDealno(@RequestBody Map<String,Object> map) {
		CustCreditDealBean bean = creditDealService.queryCreditReleaseDealByDealno(map);
		return RetMsgHelper.ok(bean);
	}
	
	/**
	 * 查询需要释放额度的交易
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/getCreditDealByDealNo")
	public RetMsg<CustCreditDealBean> getCreditDealByDealNo(@RequestBody Map<String,Object> map) {
		CustCreditDealBean bean = creditDealService.getCreditDealByDealNo(String.valueOf(map.get("dealNo")));
		return RetMsgHelper.ok(bean);
	}
}
