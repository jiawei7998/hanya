package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcInstCredit;

public interface TcInstCreditMapper extends Mapper<TcInstCredit> {

	public Page<TcInstCredit> pageInstCreditList(Map<String, Object> map, RowBounds rb);
	
	public List<TcInstCredit> getInstCreditList(Map<String, Object> map);
	
	public void deleteByInstId(Map<String, Object> map);
	
	public void updateByInstId(TcInstCredit tcInstCredit);
}
