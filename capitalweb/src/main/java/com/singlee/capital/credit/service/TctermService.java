package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.Tcterm;


public interface TctermService {
	Page<Tcterm> selectTcterm(Map<String, Object> map);
	
	void deleteCredit(Map<String, Object> map);
	
	void addCredit(Tcterm tcterm);
	
	void updateCredit(Tcterm tcterm);
}
