package com.singlee.capital.credit.mapper;

import java.util.Map;

import com.singlee.capital.credit.pojo.SecurServerBean;

import tk.mybatis.mapper.common.Mapper;

public interface SecurServerMapper extends Mapper<SecurServerBean>{

	//由dealNo查找修改金额详情 
	public SecurServerBean getAdjAmtList(Map<String, Object> map);
	
	public void updateByDealNo(Map<String, Object> map);
	
	public void insertAdjAmtList(Map<String, Object> map);
	
	public void deleteByDealNo(String dealno);
	
}
