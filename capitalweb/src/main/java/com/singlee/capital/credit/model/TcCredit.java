package com.singlee.capital.credit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *  额度品种
 * @author liyimin
 *
 */
@Entity
@Table(name = "TC_CREDIT")
public class TcCredit implements Serializable {

	private static final long serialVersionUID = 1L;

	// 额度编号
	@Id
	private String creditId;
	
	// 额度品种
	private String creditName;

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}
}
