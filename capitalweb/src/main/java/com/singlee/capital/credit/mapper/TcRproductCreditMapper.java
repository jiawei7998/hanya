package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcRproductCredit;
import com.singlee.capital.credit.pojo.TcRproductCreditPojo;

import tk.mybatis.mapper.common.Mapper;

public interface TcRproductCreditMapper extends Mapper<TcRproductCredit> {
	
	public Page<TcRproductCreditPojo> pageRproductCreditList(Map<String, Object> map, RowBounds rb);
	
	void deleteByProductCode(String productCode);
	
	void deleteByCreditId(String creditId);
	
	List<TcRproductCredit> findByProductCodeAndCreditIdList(Map<String, Object> map);
}
