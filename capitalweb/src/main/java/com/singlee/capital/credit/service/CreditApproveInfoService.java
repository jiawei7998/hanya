package com.singlee.capital.credit.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.model.TcCreditApproveInfo;

public interface CreditApproveInfoService {

	Page<TcCreditApproveInfo> getCreditApproveInfoList(Map<String, Object> map);

}
