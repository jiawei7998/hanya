package com.singlee.capital.credit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.credit.pojo.CustCreditAdjBean;

import tk.mybatis.mapper.common.Mapper;

public interface TcCustCreditAdjMapper extends Mapper<CustCreditAdjBean>{

	void createCustCreditAdj(Map<String,Object> map);

	/**
	 * 得到额度调整审批列表
	 */
	Page<CustCreditAdjBean> getCreditAdjList(Map<String, Object> map,
			RowBounds rowBounds);
	Page<CustCreditAdjBean> getCreditAdjListFinish(Map<String, Object> map,
			RowBounds rowBounds);
	Page<CustCreditAdjBean> getCreditAdjListMine(Map<String, Object> map,
			RowBounds rowBounds);
	
	void deleteByDealNo(Map<String,Object> map);

	void updateByDealNo(Map<String, Object> map);

	Page<CustCreditAdjBean> getAdjDetailByCredit(Map<String, Object> map,
			RowBounds rowBounds);

	void statusChange(Map<String, Object> status);

	CustCreditAdjBean getCreditAdjByDealNo(String dealNo);

	List<CustCreditAdjBean> getAdjDetailByCreditAll(Map<String, Object> map);

	
}
