package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.users.model.TdProductModel;

public interface TdProductModelService {

	/**
	 * 汪泽峰 2017-5-25 分页查询产品
	 * 
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdProductModel> showAllTdProductModelService(
			Map<String, Object> map);

	/**
	 * 申请产品交易
	 * @param map
	 */
//	public void applyTdProductModelService(Map<String, Object> map);
	public RetMsg<Object> applyTdProductModelService(Map<String, Object> map);
	
	/**
	 * 同步产品保存
	 * @param map
	 */
	public RetMsg<Object> sameTdProductModelService(Map<String, Object> map);
	
	
	/**
	 * 同步产品交易
	 * @param map
	 */
	public RetMsg<Object> syncSameTdProductModelService(Map<String, Object> map);
	
	/**
	 * 修改产品交易
	 * @param map
	 */
//	public void updateTdProductModelService(Map<String, Object> map);
	public RetMsg<Object> updateTdProductModelService(Map<String, Object> map);
	
}
