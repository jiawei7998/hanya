package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.EcifBosoCustomerManager;

public interface EcifBosoCustomerMService {
		
	/**
	 * 条件查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<EcifBosoCustomerManager> selectByParamsService(Map<String, Object> map);

}
