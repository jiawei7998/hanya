package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TD_CUST_IB")
public class TdCustIb implements Serializable {

	/**
	 * 同业信息 - 汪泽峰 2017-5-17
	 */
	
	private static final long serialVersionUID = 4276568367960134692L;
	
	@Id
	private String custmerCode;
	private String ctlType;
	private String ioType;
	private String fcciType;
	private String fcciCode;
	private String supInst;
	private String npcCode;
	private String npcDesc;
	private String rwaCode;
	private String rwaDesc;
	private String resCountryCd;
	private String resCountryDesc;
	private String statusSys;
	private String acctngType;
	private String cbrcReltype;
	private String shReltype;
	@Transient
	private String cnName;
	
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public String getCtlType() {
		return ctlType;
	}
	public void setCtlType(String ctlType) {
		this.ctlType = ctlType;
	}
	public String getIoType() {
		return ioType;
	}
	public void setIoType(String ioType) {
		this.ioType = ioType;
	}
	public String getFcciType() {
		return fcciType;
	}
	public void setFcciType(String fcciType) {
		this.fcciType = fcciType;
	}
	public String getFcciCode() {
		return fcciCode;
	}
	public void setFcciCode(String fcciCode) {
		this.fcciCode = fcciCode;
	}
	public String getSupInst() {
		return supInst;
	}
	public void setSupInst(String supInst) {
		this.supInst = supInst;
	}
	public String getNpcCode() {
		return npcCode;
	}
	public void setNpcCode(String npcCode) {
		this.npcCode = npcCode;
	}
	public String getNpcDesc() {
		return npcDesc;
	}
	public void setNpcDesc(String npcDesc) {
		this.npcDesc = npcDesc;
	}
	public String getRwaCode() {
		return rwaCode;
	}
	public void setRwaCode(String rwaCode) {
		this.rwaCode = rwaCode;
	}
	public String getRwaDesc() {
		return rwaDesc;
	}
	public void setRwaDesc(String rwaDesc) {
		this.rwaDesc = rwaDesc;
	}
	public String getResCountryCd() {
		return resCountryCd;
	}
	public void setResCountryCd(String resCountryCd) {
		this.resCountryCd = resCountryCd;
	}
	public String getResCountryDesc() {
		return resCountryDesc;
	}
	public void setResCountryDesc(String resCountryDesc) {
		this.resCountryDesc = resCountryDesc;
	}
	public String getStatusSys() {
		return statusSys;
	}
	public void setStatusSys(String statusSys) {
		this.statusSys = statusSys;
	}
	public String getAcctngType() {
		return acctngType;
	}
	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}
	public String getCbrcReltype() {
		return cbrcReltype;
	}
	public void setCbrcReltype(String cbrcReltype) {
		this.cbrcReltype = cbrcReltype;
	}
	public String getShReltype() {
		return shReltype;
	}
	public void setShReltype(String shReltype) {
		this.shReltype = shReltype;
	}
	
	
}
