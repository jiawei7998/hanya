package com.singlee.capital.users.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAtCopy;

public interface TdCustAtCopyMapper extends Mapper<TdCustAtCopy> {
	/**
	 * 汪泽峰 -2017-5-17
	 * 根据客户号分页查询客户准入信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopy(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 汪泽峰 -2017-5-22
	 * 修改客户准入信息
	 * @param map
	 */
	
	
	
	public void updateTdCustAtCopy(Map<String, Object> map);
	
	/**
	 * 汪泽峰 -2017-5-22
	 * 新增客户准入信息
	 * @param map
	 */
	public void insertTdCustAtCopy(Map<String, Object> map);
	
	
	
	public void deleteTdCustAtCopy(Map<String, Object> map);
	
	/**
	 * 汪泽峰 -2017-7-19
	 * 查询已办列表
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopyCompleted(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 汪泽峰 -2017-7-19
	 * 查询我发起的列表
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopyMine(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 查询是否已存在准入信息正在审批中
	 * @param map
	 * @return
	 */
	public List<TdCustAtCopy> showSameAtCopy(Map<String, Object> map);
	
}
