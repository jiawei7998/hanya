package com.singlee.capital.users.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.users.mapper.TdCustAccCopyMapper;
import com.singlee.capital.users.mapper.TdCustAccMapper;
import com.singlee.capital.users.model.TdCustAcc;
import com.singlee.capital.users.model.TdCustAccCopy;
import com.singlee.capital.users.service.TdCustAccCopyService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
@Service("tdCustAccCopyServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustAccCopyServiceImpl implements TdCustAccCopyService,SlbpmCallBackInteface,TaskEventInterface {
	@Autowired
	private TdCustAccCopyMapper TcaMapper;
	@Autowired
	private TdCustAccMapper tdCustAccMapper;
	/**
	 * 汪泽峰 2017-5-18
	 * 分页查询客户划款信息
	 */
	@Override
	public Page<TdCustAccCopy> showTdCustAccCopyService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustAccCopy> result=TcaMapper.showTdCustAccCopy(map, rowBounds);
		return result;
	}
	
	/**
	 * 汪泽峰 2017-5-19
	 * 新增划款信息
	 */
	@Override
	public void insertTdCustAccCopyService(Map<String, Object> map) {
		TdCustAccCopy TdCustAccCopy = new TdCustAccCopy();
		ParentChildUtil.HashMapToClass(map, TdCustAccCopy);
		TcaMapper.insert(TdCustAccCopy);
	}
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改划款信息
	 */
	@Override
	public void updateCustAccCopyService(Map<String, Object> map) {
		TcaMapper.updateCustAccCopy(map);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
			String custmerCode = serial_no.split("ACC")[0];
			String code = serial_no.split("ACC")[1];
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("custmerCode", custmerCode);
			map.put("code", code);
			map.put("state", status);
			map.put("updatetime", DateUtil.getCurrentDateTimeAsString());  //设置审批状态发生改变的时间
			TcaMapper.updateCustAccCopy(map);
			if("6".equals(status)){
				Map<String, Object> map1 = new HashMap<String, Object>();
				map1.put("code", map.get("code"));
				map1.put("custmerCode", map.get("custmerCode"));
				map1.put("pageSize", "10");
				map1.put("pageNumber", "1");
				//map.put("status", "1");   //变为启用状态 插入主表
				RowBounds rowBounds = ParameterUtil.getRowBounds(map1);
				Page<TdCustAccCopy> result = TcaMapper.showTdCustAccCopyMine(map1, rowBounds);
				TdCustAccCopy tdCustAccCopy = result.get(0);
				tdCustAccCopy.setUpdatetime(DateUtil.getCurrentDateTimeAsString());  //设置审批状态发生改变的时间
				
				Map<String, Object> map3 = new HashMap<String, Object>();
				map3.put("code",tdCustAccCopy.getCode());
				TdCustAcc tca = tdCustAccMapper.showTdCustAccByColumns(map3);
				if(tca != null){ //如果主表中已存在该笔审批  则先删除旧的 再插入更新的
					tdCustAccMapper.delete(tca);
				}
				
				Map<String, Object> map2 = ParentChildUtil.ClassToHashMap(tdCustAccCopy);
				//map2.put("state", "6");   
				//map2.put("status", "1");  //流程通过 设置成启用状态
				TdCustAcc tdCustAcc = new TdCustAcc();
				ParentChildUtil.HashMapToClass(map2, tdCustAcc);
				tdCustAccMapper.insert(tdCustAcc);
			}
			
			
		
	}

	
	/**
	 * 汪泽峰 2017-5-21
	 * 删除划款信息
	 */
	@Override
	public void deleteCustAccCopyService(Map<String, Object> map) {
		TdCustAccCopy TdCustAccCopy = new TdCustAccCopy();
		ParentChildUtil.HashMapToClass(map, TdCustAccCopy);
		TcaMapper.deleteByPrimaryKey(TdCustAccCopy);
	}

	

	@Override
	public Page<TdCustAccCopy> showTdCustAccCopyCompletedService(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return TcaMapper.showTdCustAccCopyCompleted(map, rb);
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeCustAccCopyStatusService(Map<String, Object> map) {
		TcaMapper.changeCustAccCopyStatus(map);
	}

	@Override
	public Page<TdCustAccCopy> showTdCustAccCopyMineService(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return TcaMapper.showTdCustAccCopyMine(map, rb);
	}

	@Override
	public List<TdCustAccCopy> showSameAccCopyService(Map<String, Object> map) {
		
		return TcaMapper.showSameAccCopy(map);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

	
	



}
