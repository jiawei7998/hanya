package com.singlee.capital.users.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.model.TdCustContacts;
import com.singlee.capital.users.service.TdCustContactsService;

@Controller
@RequestMapping(value = "/TdCustContactsController")
public class TdCustContactsController extends CommonController {
	@Autowired
	private TdCustContactsService tdCustContactsService;
	/**
	 * 汪泽峰 2017-5-18
	 * 分页查询客户联系人信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdCustContactsPage")
	public RetMsg<PageInfo<TdCustContacts>> getTdCustContacts(@RequestBody Map<String,Object> map){
		Page<TdCustContacts> page = tdCustContactsService.showAllTdCustContactsService(map);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 汪泽峰 2017-5-22
	 * 保存客户联系人信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTdCustContacts")
	public RetMsg<Boolean> saveTdCustContacts(@RequestBody Map<String,Object> map){
		Map<String , Object> map1=new HashMap<String, Object>();
		map1.put("custmerCode", map.get("custmerCode"));
		map1.put("name", map.get("name"));
		Page<TdCustContacts> page = tdCustContactsService.showAllTdCustContactsService(map1);
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		map.put("updatetime", dateFormat.format(now));
		if(page.getResult().size()==1){
			tdCustContactsService.updateTdCustContactsService(map);
			return RetMsgHelper.ok(true);
		}else{
			tdCustContactsService.insertTdCustContactsService(map);
			return RetMsgHelper.ok(false);
		}
		
	}
	/**
	 * 汪泽峰 2017-5-22
	 * 删除客户联系人信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTdCustContacts")
	public RetMsg<Serializable> deleteTdCustContacts(@RequestBody Map<String,Object> map){
		tdCustContactsService.deleteTdCustContactsService(map);
		return RetMsgHelper.ok();
	}
}
