package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.users.mapper.TdCustAtMapper;
import com.singlee.capital.users.model.TdCustAt;
import com.singlee.capital.users.service.TdCustAtService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustAtServiceImpl implements TdCustAtService {
	@Autowired
	private TdCustAtMapper tdCustAtMapper;
	
	/**
	 * 汪泽峰 - 2017-5-17
	 * 分页查询准入信息
	 */
	@Override
	public Page<TdCustAt> showAllTdCustAtService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustAt> result=tdCustAtMapper.showAllTdCustAt(map, rowBounds);
		return result;
	}
	@Override
	public Page<TdCustAt> showAllTdCustAtServiceNew(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustAt> result=tdCustAtMapper.showAllTdCustAtNew(map, rowBounds);
		return result;
	}
	
	/**
	 * 汪泽峰 - 2017-5-22
	 * 修改客户准入信息
	 */
	@Override
	public void updateTdCustAtService(Map<String, Object> map) {
		map.put("version",Integer.parseInt(map.get("version").toString()) -1);
		map.put("isActive","0");
		tdCustAtMapper.updateTdCustAt(map);
	}
	
	/**
	 * 汪泽峰 - 2017-5-22
	 * 插入客户准入信息
	 */
	@Override
	public void insertTdCustAtService(Map<String, Object> map) {
		int vers=Integer.parseInt(map.get("version").toString()); 
		map.put("version",vers+1+"");
		tdCustAtMapper.insertTdCustAt(map);
	}

	

}
