package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TD_CUST_AT_COPY")
public class TdCustAtCopy implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * -汪泽峰 2017-5-17
	 * 客户准入信息
	 */
	
	
	@Id
	private String custmerCode;
	private Integer version;
	private String atType;
	private String atDate;
	private String atDecInst;
	private String atDealno;
	private String atRemark;
	private String isActive;
	private String state;
	private String operateUser;
	private String atDate1;
	@Transient
	private String taskId;
	@Transient
	private String instFullname;
	
	
	
	public String getAtDate1() {
		return atDate1;
	}
	public void setAtDate1(String atDate1) {
		this.atDate1 = atDate1;
	}
	public String getInstFullname() {
		return instFullname;
	}
	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public String getOperateUser() {
		return operateUser;
	}
	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getAtType() {
		return atType;
	}
	public void setAtType(String atType) {
		this.atType = atType;
	}
	public String getAtDate() {
		return atDate;
	}
	public void setAtDate(String atDate) {
		this.atDate = atDate;
	}
	public String getAtDecInst() {
		return atDecInst;
	}
	public void setAtDecInst(String atDecInst) {
		this.atDecInst = atDecInst;
	}
	public String getAtDealno() {
		return atDealno;
	}
	public void setAtDealno(String atDealno) {
		this.atDealno = atDealno;
	}
	public String getAtRemark() {
		return atRemark;
	}
	public void setAtRemark(String atRemark) {
		this.atRemark = atRemark;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	
}
