package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.EcifBosoCustomerManager;

public interface EcifBosoCustomerMMapper extends Mapper<EcifBosoCustomerManager>{
	
	
	/**
	 * 条件查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<EcifBosoCustomerManager> selectByParams(Map<String, Object> map,RowBounds rb);

	
	
}
