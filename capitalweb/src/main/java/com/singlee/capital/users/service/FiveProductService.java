package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.FiveProduct;

public interface FiveProductService {
	/**
	 * 根据登陆人机构显示审批 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductByLoaderService(Map<String, Object> map);
	
	/**
	 * 新增一笔审批
	 * @param map
	 */
	public void addFiveProductService(Map<String, Object> map);
	
	/**
	 * 修改审批状态
	 * @param map
	 */
	public void updateFpStateService(Map<String, Object> map);
	
	/**
	 * 根据登陆人显示已审批 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductCompleted(Map<String, Object> map);
	
	/**
	 * 根据登陆人显示我发起的 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductMineService(Map<String, Object> map);
	
	/**
	 * 根据条件删除五级产品
	 * @param map
	 */
	public void deleteFiveProductService(Map<String, Object> map);
	
}
