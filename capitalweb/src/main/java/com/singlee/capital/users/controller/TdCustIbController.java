package com.singlee.capital.users.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.model.TdCustEcif;
import com.singlee.capital.users.model.TdCustIb;
import com.singlee.capital.users.service.TdCustEcifService;
import com.singlee.capital.users.service.TdCustIbService;

@Controller
@RequestMapping(value = "/TdCustIbController")
public class TdCustIbController extends CommonController {
	@Autowired
	private TdCustIbService tdCustIbService;
	@Autowired
	private TdCustEcifService tdCustEcifService;
	
	/**
	 * 汪泽峰 2017-5-17
	 * 根据客户号查询客户同业信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdCustIb")
	public RetMsg<TdCustIb> getTdCustIbByCusCode(@RequestBody Map<String,Object> map){
		TdCustIb tdcustib= tdCustIbService.showTdCustIbByCusCodeService(map);
		return RetMsgHelper.ok(tdcustib);
	}
	//查询客户号
	@ResponseBody
	@RequestMapping(value = "/getEcifId")
	public RetMsg<List<TdCustEcif>> getEcifId(@RequestBody Map<String,Object> map){
		List<TdCustEcif> list = tdCustEcifService.showAllEcifIdService();
		return RetMsgHelper.ok(list);
	}
	
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改同业信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTdCustIb")
	public RetMsg<Serializable> updateTdCustIb(@RequestBody Map<String,Object> map){
		Map<String, Object> map1= new HashMap<String, Object>();
		map1.put("custmerCode", map.get("custmerCode"));
		if(tdCustIbService.showTdCustIbByCusCodeService(map1)==null){
			tdCustIbService.insertTdCustIbService(map);
		}else{
			tdCustIbService.updateTdCustIbService(map);
		}
		return RetMsgHelper.ok();
	}
}
