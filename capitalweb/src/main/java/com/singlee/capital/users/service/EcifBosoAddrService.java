package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.EcifBosoAddress;

public interface EcifBosoAddrService  {
		
	/**
	 * 条件查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<EcifBosoAddress> selectAddrByParamsService(Map<String, Object> map);

	
}
