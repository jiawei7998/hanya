package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAt;

public interface TdCustAtService {
	
	/**
	 * 汪泽峰 2017-5-17
	 * 分页查询准入信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAt> showAllTdCustAtService(Map<String, Object> map);
	
	public Page<TdCustAt> showAllTdCustAtServiceNew(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改客户准入信息
	 * @param map
	 */
	public void updateTdCustAtService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 插入客户准入信息
	 * @param map
	 */
	public void insertTdCustAtService(Map<String, Object> map);
	
	
}
