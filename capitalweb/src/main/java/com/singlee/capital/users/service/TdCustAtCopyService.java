package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAtCopy;

public interface TdCustAtCopyService {
	
	/**
	 * 汪泽峰 2017-5-17
	 * 分页查询准入信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopyService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改客户准入信息
	 * @param map
	 */
	public void updateTdCustAtCopyService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 插入客户准入信息
	 * @param map
	 */
	public void insertTdCustAtCopyService(Map<String, Object> map);
	
	
	
	public void deleteTdCustAtCopyService(Map<String, Object> map);
	
	
	/**
	 * 汪泽峰 -2017-7-19
	 * 查询已办列表
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopyCompletedService(Map<String, Object> map);
	

	/**
	 * 汪泽峰 -2017-7-19
	 * 查询我发起的列表
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAtCopy> showAllTdCustAtCopyMineService(Map<String, Object> map);
	
	
	/**
	 * 查询是否已存在准入信息正在审批中
	 * @param map
	 * @return
	 */
	public List<TdCustAtCopy> showSameAtCopyService(Map<String, Object> map);
	
}
