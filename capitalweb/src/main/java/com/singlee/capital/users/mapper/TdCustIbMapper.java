package com.singlee.capital.users.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.users.model.TdCustIb;

public interface TdCustIbMapper extends Mapper<TdCustIb> {
	/**
	 * 根据客户号查询客户同业信息
	 *  - 汪泽峰 2017-5-17
	 * @param map
	 * @return
	 */
	TdCustIb showTdCustIbByCusCode(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改同业信息
	 * @param map
	 */
	void updateTdCustIb(Map<String, Object> map);
	/**
	 * 插入同业信息
	 * @param map
	 */
	void insertTdCustIb(Map<String, Object> map);
	
	void deleteSPVIb(Map<String, Object> map);
	/**
	 * 根据客户号查询机构法人信息表
	 * @param ecifNo
	 * @return
	 */
	TdCustIb getCustIbFcciCodeByEcifNo(String ecifNo);
	/**
	 * 
	 */
	void deleteAllCustIbs();
	/**
	 * 
	 * @param custIb
	 */
	void insertCustIbs(TdCustIb custIb);
}
