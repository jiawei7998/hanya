package com.singlee.capital.users.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.model.FiveProduct;
import com.singlee.capital.users.model.FiveType;
import com.singlee.capital.users.service.FiveDownService;
import com.singlee.capital.users.service.FiveProductService;
import com.singlee.capital.users.service.FiveTypeService;

@Controller
@RequestMapping(value = "/FiveTypeController")
public class FiveTypeController extends CommonController {
	@Autowired
	private FiveTypeService fiveTypeService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private FiveProductService fiveProductService;
	@Autowired
	private FiveDownService fiveDownService;
	

	//根据登陆用户机构查询所有五级分类
	@ResponseBody
	@RequestMapping(value = "/getFiveByLoader")
	public RetMsg<PageInfo<FiveType>> getFiveByLoader(@RequestBody Map<String,Object> map){
		String institutionId = SlSessionHelper.getInstitutionId();
		List<String> list = institutionService.childrenInstitutionIds(institutionId);
		list.add(institutionId);
		map.put("list", list);
		Page<FiveType> page = fiveTypeService.showFiveTypeByLoaderService(map);
		for (FiveType fiveType : page) {
			fiveType.setClassifyAdvice("正常");
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件搜索 已审批的五级分类
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/SearchFiveType")
	public RetMsg<PageInfo<FiveType>> SearchFiveType(@RequestBody Map<String,Object> map){
		Page<FiveType> page = fiveTypeService.SearchFiveTypeService(map);
		return RetMsgHelper.ok(page);
	}
	
	
	//提交五级分类
		@ResponseBody
		@RequestMapping(value = "/addFiveProduct")
		public RetMsg<Serializable> addFiveProduct(@RequestBody Map<String,Object> map){
			String rand = (Math.random()+"").substring(2, 8);
			String productNo = "FT"+DateUtil.getCurrentCompactDateTimeAsString()+rand;
			List<FiveType> list = new ArrayList<FiveType>();
			
			List<FiveType> list1=FastJsonUtil.parseArrays(FastJsonUtil.toJSONString(map.get("add")), FiveType.class); 
			for (FiveType fiveType : list1) {
				fiveType.setProductNo(productNo);  //生成的产品编号
				fiveType.setAssignee(SlSessionHelper.getUserId());  //初分人
//				fiveType.setApprove(SlSessionHelper.getUserId());   //复合人
				list.add(fiveType);
			}
			fiveTypeService.addFiveTypeService(list);  //批量插入五级分类
			FiveProduct fp = new FiveProduct();
			System.out.println(SlSessionHelper.getInstitutionType()+"===");
			fp.setProductNo(productNo);
			fp.setProductType("五级分类");
			fp.setSponInst(SlSessionHelper.getInstitutionId());
			fp.setOperateUser(SlSessionHelper.getUserId());
			fp.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			fp.setState("3");  //设置审批状态为新建
			
			fp.setYears(DateUtil.getCurrentDateAsString().split("-")[0]);
			String mounth = DateUtil.getCurrentDateTimeAsString().split("-")[1];
			if("01".equals(mounth)|| "02".equals(mounth)|| "03".equals(mounth)){
				fp.setSeason("1");
			}else if("04".equals(mounth)|| "05".equals(mounth)|| "06".equals(mounth)){
				fp.setSeason("2");
			}else if("07".equals(mounth)|| "08".equals(mounth)|| "09".equals(mounth)){
				fp.setSeason("3");
			}else{
				fp.setSeason("4");
			}
			Map<String, Object> map1 = ParentChildUtil.ClassToHashMap(fp);
			fiveProductService.addFiveProductService(map1);
			return RetMsgHelper.ok();
		}
		
		/**
		 * 
		 * 删除已提交的五级分类 
		 * @param map
		 * @return
		 */
		@ResponseBody
		@RequestMapping(value = "/deleteFiveProduct")
		public RetMsg<Serializable> deleteFiveProduct(@RequestBody Map<String,Object> map){
			fiveProductService.deleteFiveProductService(map);
			fiveTypeService.deleteFiveTypeService(map);
			return RetMsgHelper.ok();
		}
		
		
		//根据登陆人  显示待审批产品 我发起的
		@ResponseBody
		@RequestMapping(value = "/showWaitFw")
		public RetMsg<PageInfo<FiveProduct>> showWaitFw(@RequestBody Map<String,Object> map){
			map.put("operateUser", SlSessionHelper.getUserId());
			//map.put("state", "3");
			Page<FiveProduct> page = fiveProductService.showFiveProductMineService(map);
			
			Map<String,Object> map1 = new HashMap<String, Object>();
			for (FiveProduct fiveProduct : page) {
				map1.put("productNo", fiveProduct.getProductNo());
				List<FiveType> list = fiveTypeService.FiveTypeDetails(map1);
				fiveProduct.setNumbers(list.size()+"");
			}
			
			return RetMsgHelper.ok(page);
		}
		
		//根据登陆人  显示待审批产品 带审批的
		@ResponseBody
		@RequestMapping(value = "/showWaitFd")
		public RetMsg<PageInfo<FiveProduct>> showWaitFd(@RequestBody Map<String,Object> map){
			map.put("userId", SlSessionHelper.getUserId());
			map.put("state", "4");
			map.put("state1", "45");
			Page<FiveProduct> page = fiveProductService.showFiveProductByLoaderService(map);
			Map<String,Object> map1 = new HashMap<String, Object>();
			for (FiveProduct fiveProduct : page) {
				map1.put("productNo", fiveProduct.getProductNo());
				List<FiveType> list = fiveTypeService.FiveTypeDetails(map1);
				fiveProduct.setNumbers(list.size()+"");
			}
			return RetMsgHelper.ok(page);
		}
				
		//根据登陆人  显示待审批产品 已审批的
		@ResponseBody
		@RequestMapping(value = "/showWaitFy")
		public RetMsg<PageInfo<FiveProduct>> showWaitFy(@RequestBody Map<String,Object> map){
			map.put("userId", SlSessionHelper.getUserId());
//			map.put("state", "6");
//			map.put("state2", "678");
			Page<FiveProduct> page = fiveProductService.showFiveProductCompleted(map);
			Map<String,Object> map1 = new HashMap<String, Object>();
			for (FiveProduct fiveProduct : page) {
				map1.put("productNo", fiveProduct.getProductNo());
				List<FiveType> list = fiveTypeService.FiveTypeDetails(map1);
				fiveProduct.setNumbers(list.size()+"");
			}
			return RetMsgHelper.ok(page);
		}
		
		//根据FP编号 展开五级分类
		@ResponseBody
		@RequestMapping(value = "/FiveTypeDetailsByProductNo")
		public RetMsg<List<FiveType>> FiveTypeDetailsByProductNo(@RequestBody Map<String,Object> map){
			List<FiveType> list = fiveTypeService.FiveTypeDetails(map);
			return RetMsgHelper.ok(list);
		}
		
		/**
		 * 导出页面五级分类Excel
		 * @param request
		 * @param response
		 */
		@ResponseBody
		@RequestMapping(value = "/downloadFiveExcelAll")
		public void downloadFiveExcelAll(HttpServletRequest request, HttpServletResponse response){
			HashMap<String,Object> map = new HashMap<String,Object>();
			
			String institutionId = SlSessionHelper.getInstitutionId();
			List<String> list = institutionService.childrenInstitutionIds(institutionId);
			list.add(institutionId);
			map.put("list", list);
			String index = request.getParameter("pageNumber");
			map.put("pageNumber", index);
			map.put("pageSize", "10");
			
			String ua = request.getHeader("User-Agent");
			String filename = "五级分类.xlsx";
			String encode_filename = "";
			try {
				encode_filename = URLEncoder.encode(filename, "utf-8");
			} catch (UnsupportedEncodingException e1) {
				//e1.printStackTrace();
				throw new RException(e1.getMessage());
			}
			response.setContentType("application/vnd.ms-excel");
			if (ua != null && ua.indexOf("Firefox/") >= 0) {
				response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
			} else {
				response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
			}
			response.setHeader("Connection", "close");

			try {
				ExcelUtil e = fiveDownService.downFiveExcelAll(map);
				
				OutputStream ouputStream = response.getOutputStream();
				e.getWb().write(ouputStream);
				ouputStream.flush();
				ouputStream.close();
				
			} catch (Exception e) {
				throw new RException(e);
			}
			
		};

		
		//上传Excel
				@ResponseBody
				@RequestMapping(value = "/readExcel")
				public RetMsg<List<Map<String, Object>>> readExcel(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
					List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
						// 1.文件获取
						List<UploadedFile> uploadedFileList = null;
						uploadedFileList = FileManage.requestExtractor(request);
						// 2.交验并处理成excel
						
						for (UploadedFile uploadedFile : uploadedFileList) {
							JY.ensure("xls".equals(uploadedFile.getType())||"xlsx".equals(uploadedFile.getType()), "上传文件类型错误.");
							String name = uploadedFile.getFullname();
							InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
							list = fiveDownService.uploadExcel(name, is);
						}
					
					return RetMsgHelper.ok(list);
				}
	
	
		//只能 上传2003的xsl格式
//		@RequestMapping(value = "/readExcel1")
//		@ResponseBody
//		public RetMsg<List<Map<String,Object>>> readExcel1(MultipartHttpServletRequest request, HttpServletResponse response) {
//			
//			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
//			try {
//				// 获取excel的工作簿
//				Workbook wb = parseWorkBook(request);
//				//读取数据，并转化为List保存
//				list = excelImportService.readExcel(wb, "五级分类");
//			} catch (Exception e) {
//				JY.error(e);
//			}
//			return RetMsgHelper.ok(list);
//		}
//		  /**
//	     * 解析xls
//	     */
//		public Workbook parseWorkBook(MultipartHttpServletRequest request) throws Exception {
//			// 1.文件获取
//			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
//			// 2.交验并处理成excel
//			List<Workbook> excelList = new LinkedList<Workbook>();
//			for (UploadedFile uploadedFile : uploadedFileList) {
//				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
//				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
//				excelList.add(book);
//			}
//			JY.require(excelList.size() == 1, "只能处理一份excel");
//			return excelList.get(0);
//
//		}
		
		
		
		
		
		
}
