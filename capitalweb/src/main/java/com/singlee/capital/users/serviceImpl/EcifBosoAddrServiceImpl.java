package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.model.EcifBosoAddress;
import com.singlee.capital.users.mapper.EcifBosoAddrMapper;
import com.singlee.capital.users.service.EcifBosoAddrService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EcifBosoAddrServiceImpl implements EcifBosoAddrService {
	
	@Autowired
	private EcifBosoAddrMapper ecifBosoAddrMapper;
	
	
	
	@Override
	public Page<EcifBosoAddress> selectAddrByParamsService(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<EcifBosoAddress> result = ecifBosoAddrMapper.selectAddrByParams(map, rb);
		return result;
	}

}
