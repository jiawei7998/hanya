package com.singlee.capital.users.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.model.TdCustAt;
import com.singlee.capital.users.model.TdCustAtCopy;
import com.singlee.capital.users.service.TdCustAtCopyService;
import com.singlee.capital.users.service.TdCustAtService;

@Controller
@RequestMapping(value = "/TdCustAtController")
@RestController
public class TdCustAtController extends CommonController {
	@Autowired
	private TdCustAtService tdCustAtService;
	@Autowired
	private TdCustAtCopyService tdCustAtCopyService;
	/**
	 * 汪泽峰 2017-5-17
	 * 分页查询准入信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/searchTdCustAtPage")
	public RetMsg<PageInfo<TdCustAt>> getTdCustAt(@RequestBody Map<String,Object> map){
		Page<TdCustAt> page = tdCustAtService.showAllTdCustAtService(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 汪泽峰 2017-5-22
	 * 提交客户准入信息
	 * @param map
	 */
	@RequestMapping(value = "/updateTdCustAtCopy")
	public RetMsg<Boolean> updateTdCustAtCopy(@RequestBody Map<String,Object> map){
		Map<String, Object> map1= new HashMap<String, Object>();
		//修改日期格式
		map.put("atDate", map.get("atDate").toString().split("-")[0]+map.get("atDate").toString().split("-")[1]+map.get("atDate").toString().split("-")[2]);
		map.put("atDate1", map.get("atDate1").toString().split("-")[0]+map.get("atDate1").toString().split("-")[1]+map.get("atDate1").toString().split("-")[2]);
		if("".equals(map.get("version"))){
			map.put("version", 1000);
		}
		map1.put("custmerCode", map.get("custmerCode"));
		map1.put("state3", "345");
		map1.put("atType", map.get("atType"));
		if(tdCustAtCopyService.showSameAtCopyService(map1).size()>0){
			//如果存在正在审批或者新建的任务
			return RetMsgHelper.ok(false);
		}
		else{
			map.put("state", "3");
			map.put("version", Integer.parseInt(map.get("version").toString())+1);
			map.put("isActive", "0");
			map.put("operateUser", SlSessionHelper.getUserId()); 
			tdCustAtCopyService.insertTdCustAtCopyService(map);
			return RetMsgHelper.ok(true);
		}
	}
	

	@RequestMapping(value = "/showWaitAtw")
	public RetMsg<PageInfo<TdCustAtCopy>> showWaitAtw(@RequestBody Map<String,Object> map){
		if(map.containsKey("atDate")){
		map.put("atDate", map.get("atDate").toString().replace("-", ""));
		}
		map.put("operateUser", SlSessionHelper.getUserId());
		//map.put("state", "3");
		Page<TdCustAtCopy> res = tdCustAtCopyService.showAllTdCustAtCopyMineService(map);
		return RetMsgHelper.ok(res);
	}
	
	@RequestMapping(value = "/showWaitAtd")
	public RetMsg<PageInfo<TdCustAtCopy>> showWaitAtd(@RequestBody Map<String,Object> map){
		if(map.containsKey("atDate")){
			map.put("atDate", map.get("atDate").toString().replace("-", ""));
			}
		map.put("userId", SlSessionHelper.getUserId());
		map.put("state", "4");
		map.put("state1", "45");
		Page<TdCustAtCopy> res = tdCustAtCopyService.showAllTdCustAtCopyService(map);
		return RetMsgHelper.ok(res);
	}
	
	@RequestMapping(value = "/showWaitAty")
	public RetMsg<PageInfo<TdCustAtCopy>> showWaitAty(@RequestBody Map<String,Object> map){
		if(map.containsKey("atDate")){
			map.put("atDate", map.get("atDate").toString().replace("-", ""));
			}
		map.put("userId", SlSessionHelper.getUserId());
//		map.put("state", "6");
//		map.put("state2", "678");
		Page<TdCustAtCopy> res = tdCustAtCopyService.showAllTdCustAtCopyCompletedService(map);
		return RetMsgHelper.ok(res);
	}
	
	@RequestMapping(value = "/deletetTdCustAtCopy")
	public RetMsg<Serializable> deletetTdCustAtCopy(@RequestBody Map<String,Object> map){
		tdCustAtCopyService.deleteTdCustAtCopyService(map);
		return RetMsgHelper.ok();
	}
	
}
