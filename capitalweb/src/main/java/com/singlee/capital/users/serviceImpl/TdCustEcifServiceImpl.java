package com.singlee.capital.users.serviceImpl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.eu.model.ProductWeight;
import com.singlee.capital.users.mapper.TdCustEcifMapper;
import com.singlee.capital.users.model.TdCustEcif;
import com.singlee.capital.users.service.TdCustEcifService;
import com.singlee.ifs.model.IfsOpicsCust;
/**
 * ECIF的serviceimpl层 
 * 汪泽峰 - 2017-5-16
 * @author lush
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustEcifServiceImpl implements TdCustEcifService {
	@Autowired
	private TdCustEcifMapper TcMapper;
	/**
	 * 分页查询TdCustEcif - 汪泽峰  2017-5-16
	 */
	@Override
	public Page<TdCustEcif> showAllTdCustEcifservice(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		if(map.containsKey("cnName")&&map.get("cnName")!=null){
			String cnName=map.get("cnName").toString().trim();
			if(cnName.indexOf(" ")>0){
				String[] cnNameArr=cnName.split(" ");
				String cnNameStr="";
				int j=0;
				for(int i=0;i<cnNameArr.length;i++){
					if(cnNameArr[i].length()>0){
						map.put("cnName"+j, cnNameArr[i]);
						cnNameStr +=cnNameArr[i];
						j++;
					}
				}
				if (j<=5){
					map.remove("cnName");
					map.put("partyName"+j,cnNameStr);
				}else{
					map.remove("cnName");
					map.put("cnName",cnNameStr);
				}
			}
		}
		Page<TdCustEcif> result=TcMapper.showAllTdCustEcif(map, rowBounds);
		return result;
	}
	@Override
	public void saveTdCustEcifService(Map<String, Object> map) {
		TcMapper.saveTdCustEcif(map);
	}
	@Override
	public List<TdCustEcif> showAllEcifIdService() {
		return TcMapper.selectAll();
	}
	@Override
	public void insertEcifService(Map<String, Object> map) {
		TdCustEcif tce = new TdCustEcif();
		ParentChildUtil.HashMapToClass(map, tce);
		TcMapper.insert(tce);
	}
	
	@Override
	public Page<TdCustEcif> showAllSPVService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustEcif> result=TcMapper.showAllSPV(map, rowBounds);
		return result;
	}
	@Override
	public void deleteSPVService(Map<String, Object> map) {
		TcMapper.deleteByPrimaryKey(map);
	}
	@Override
	public Page<TdCustEcif> showAllECIFSPVService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustEcif> result=TcMapper.showAllECIFSPV(map, rowBounds);
		return result;
	}
	
	@Override
	public List<TdCustEcif> getTdCustEcifSettleList(Map<String, Object> map) {
		return TcMapper.getTdCustEcifSettleList(map);
	}
	
	@Override
	public Page<IfsOpicsCust> showAllIfsOpicsCust(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsCust> result=TcMapper.showAllIfsOpicsCust(map, rowBounds);
		return result;
	}
	
	//查询所有交易名称
	@Override
	public Page<TcProduct> searchAllProtectName(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TcProduct> result=TcMapper.searchAllProtectName(map, rowBounds);
		return result;
	}
	
	//查询指定交易明细
	@Override
	public Page<ProductWeight> searchSingleProtectName(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<ProductWeight> result=TcMapper.searchSingleProtectName(map, rowBounds);
		return result;
	}
	

	//查询衍生品交易明细
	@Override
	public Page<ProductWeight> searchAllForeignProtectName(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<ProductWeight> result=TcMapper.searchAllForeignProtectName(map, rowBounds);
		return result;
	}
	
}


