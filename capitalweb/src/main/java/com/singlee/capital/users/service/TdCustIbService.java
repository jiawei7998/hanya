package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.users.model.TdCustIb;

public interface TdCustIbService {
	/**
	 * 根据客户号查询同业信息
	 * -汪泽峰 2017-5-17
	 * @param map
	 * @return
	 */
	TdCustIb showTdCustIbByCusCodeService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改同业信息
	 * @param map
	 */
	void updateTdCustIbService(Map<String, Object> map);
	
	void insertTdCustIbService(Map<String, Object> map);
	
	void deleteSPVIbService(Map<String, Object> map);
	
	void insertTdCustIbs(List<TdCustIb> custIbs);
}
