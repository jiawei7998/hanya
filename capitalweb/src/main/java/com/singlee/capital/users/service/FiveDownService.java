package com.singlee.capital.users.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.credit.util.ExcelUtil;

public interface FiveDownService {
	
	ExcelUtil downFiveExcelAll(HashMap<String, Object> map);
	
	
	public List<Map<String, Object>> uploadExcel(String type, InputStream is);
	
}
