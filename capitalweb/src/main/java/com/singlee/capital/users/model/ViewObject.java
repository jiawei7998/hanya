package com.singlee.capital.users.model;

import java.io.Serializable;

public class ViewObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String party_id;  //客户代码
	private String party_name;       //CN客户名称
	private String is_active;        //GB客户名称
	private String party_code;       //客户类型
	private String core_custno;      //同业客户标识
	private String sf_cpty_country;  //国别
	private String at_type;          //准入级别(准入信息 td_cust_at)
	private String at_date;          //准入级别有效期(td_cust_at)
	private String fcci_code;        //所属一级法人(同业信息  td_cust_ib)
	private String fcci_codeName;    //所属一级法人中文名字
	
	private String partyContacts1;  //联系人1
	
	private String partyContacts2;  //联系人2
	
	private String merType;  //行业类别
	private String merTypeName;   //行业类别中文名字 
	
	private String strent;  // 所在街道

	
	
	
	
	public String getMerTypeName() {
		return merTypeName;
	}
	public void setMerTypeName(String merTypeName) {
		this.merTypeName = merTypeName;
	}
	public String getMerType() {
		return merType;
	}
	public void setMerType(String merType) {
		this.merType = merType;
	}
	public String getStrent() {
		return strent;
	}
	public void setStrent(String strent) {
		this.strent = strent;
	}
	public String getPartyContacts1() {
		return partyContacts1;
	}
	public void setPartyContacts1(String partyContacts1) {
		this.partyContacts1 = partyContacts1;
	}
	public String getPartyContacts2() {
		return partyContacts2;
	}
	public void setPartyContacts2(String partyContacts2) {
		this.partyContacts2 = partyContacts2;
	}
	public String getFcci_codeName() {
		return fcci_codeName;
	}
	public void setFcci_codeName(String fcci_codeName) {
		this.fcci_codeName = fcci_codeName;
	}
	
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
	public String getParty_name() {
		return party_name;
	}
	public void setParty_name(String party_name) {
		this.party_name = party_name;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}
	public String getParty_code() {
		return party_code;
	}
	public void setParty_code(String party_code) {
		this.party_code = party_code;
	}
	public String getCore_custno() {
		return core_custno;
	}
	public void setCore_custno(String core_custno) {
		this.core_custno = core_custno;
	}
	public String getSf_cpty_country() {
		return sf_cpty_country;
	}
	public void setSf_cpty_country(String sf_cpty_country) {
		this.sf_cpty_country = sf_cpty_country;
	}
	public String getAt_type() {
		return at_type;
	}
	public void setAt_type(String at_type) {
		this.at_type = at_type;
	}
	public String getAt_date() {
		return at_date;
	}
	public void setAt_date(String at_date) {
		this.at_date = at_date;
	}
	public String getFcci_code() {
		return fcci_code;
	}
	public void setFcci_code(String fcci_code) {
		this.fcci_code = fcci_code;
	}
	
	
	
	
}
