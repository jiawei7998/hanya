package com.singlee.capital.users.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.model.TdCustLayered;
import com.singlee.capital.users.service.TdCustLayeredService;

@Controller
@RequestMapping(value = "/TdCustLayeredController")
public class TdCustLayeredController extends CommonController {
	@Autowired
	private TdCustLayeredService tdCustLayeredService;
	
	/**
	 * 汪泽峰 -2017-5-18
	 * 客户分层信息查询
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdCustLayeredPage")
	public RetMsg<PageInfo<TdCustLayered>> getTdCustEcif(@RequestBody Map<String,Object> map){
		Page<TdCustLayered> page = tdCustLayeredService.showAllTdCustLayered(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改客户分层信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTdCustEcif")
	public RetMsg<Serializable> updateTdCustEcif(@RequestBody Map<String,Object> map){
		map.put("layDate", map.get("layDate").toString().split("-")[0]+map.get("layDate").toString().split("-")[1]);
		if("".equals(map.get("version"))){
			map.put("version", "10000");
			map.put("isActive", "1");
			tdCustLayeredService.insertTdCustLayeredService(map);  //插入一条新的数据，版本号+1
		}else{
			tdCustLayeredService.insertTdCustLayeredService(map);  //插入一条新的数据，版本号+1
			tdCustLayeredService.updateTdCustLayeredService(map);  //将原来的数据改为不可用
		}
		return RetMsgHelper.ok();
		
	}
	
}
