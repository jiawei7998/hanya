package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TD_PRODUCT_MODEL")
public class TdProductModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String msgTypId;
	private String productId;
	private String prdName;
	private String busType;
	private String sector;
	private String depCode;
	private String cardCrDt;
	private String enddDate;
	private String begDate;
	private String dateEnd;
	private String introducer;
	private String applyName;
	private String mntnType;
	private String dsCompanyname;
	private String orgCode;
	private String legalEntTyp;
	private String legalEntNo;
	private String relflg;
	private String custSaleCode;
	private String relName;
	private String businoticeflg;
	private String applOrg;
	private String clcRegionName;
	private String riskLev;
	private String relCusName;
	private String billStatus;
	private String statuss;
	private String workStateCode;
	private String grpRelCutCode;
	private String detailId;
	private String channelType;
	public String getMsgTypId() {
		return msgTypId;
	}
	public void setMsgTypId(String msgTypId) {
		this.msgTypId = msgTypId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getDepCode() {
		return depCode;
	}
	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}
	public String getCardCrDt() {
		return cardCrDt;
	}
	public void setCardCrDt(String cardCrDt) {
		this.cardCrDt = cardCrDt;
	}
	public String getEnddDate() {
		return enddDate;
	}
	public void setEnddDate(String enddDate) {
		this.enddDate = enddDate;
	}
	public String getBegDate() {
		return begDate;
	}
	public void setBegDate(String begDate) {
		this.begDate = begDate;
	}
	public String getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}
	public String getIntroducer() {
		return introducer;
	}
	public void setIntroducer(String introducer) {
		this.introducer = introducer;
	}
	public String getApplyName() {
		return applyName;
	}
	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}
	public String getMntnType() {
		return mntnType;
	}
	public void setMntnType(String mntnType) {
		this.mntnType = mntnType;
	}
	public String getDsCompanyname() {
		return dsCompanyname;
	}
	public void setDsCompanyname(String dsCompanyname) {
		this.dsCompanyname = dsCompanyname;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getLegalEntTyp() {
		return legalEntTyp;
	}
	public void setLegalEntTyp(String legalEntTyp) {
		this.legalEntTyp = legalEntTyp;
	}
	public String getLegalEntNo() {
		return legalEntNo;
	}
	public void setLegalEntNo(String legalEntNo) {
		this.legalEntNo = legalEntNo;
	}
	public String getRelflg() {
		return relflg;
	}
	public void setRelflg(String relflg) {
		this.relflg = relflg;
	}
	public String getCustSaleCode() {
		return custSaleCode;
	}
	public void setCustSaleCode(String custSaleCode) {
		this.custSaleCode = custSaleCode;
	}
	public String getRelName() {
		return relName;
	}
	public void setRelName(String relName) {
		this.relName = relName;
	}
	public String getBusinoticeflg() {
		return businoticeflg;
	}
	public void setBusinoticeflg(String businoticeflg) {
		this.businoticeflg = businoticeflg;
	}
	public String getApplOrg() {
		return applOrg;
	}
	public void setApplOrg(String applOrg) {
		this.applOrg = applOrg;
	}
	public String getClcRegionName() {
		return clcRegionName;
	}
	public void setClcRegionName(String clcRegionName) {
		this.clcRegionName = clcRegionName;
	}
	public String getRiskLev() {
		return riskLev;
	}
	public void setRiskLev(String riskLev) {
		this.riskLev = riskLev;
	}
	public String getRelCusName() {
		return relCusName;
	}
	public void setRelCusName(String relCusName) {
		this.relCusName = relCusName;
	}
	public String getBillStatus() {
		return billStatus;
	}
	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}
	public String getStatuss() {
		return statuss;
	}
	public void setStatuss(String statuss) {
		this.statuss = statuss;
	}
	public String getWorkStateCode() {
		return workStateCode;
	}
	public void setWorkStateCode(String workStateCode) {
		this.workStateCode = workStateCode;
	}
	public String getGrpRelCutCode() {
		return grpRelCutCode;
	}
	public void setGrpRelCutCode(String grpRelCutCode) {
		this.grpRelCutCode = grpRelCutCode;
	}
	public String getDetailId() {
		return detailId;
	}
	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}
	public String getChannelType() {
		return channelType;
	}
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	
	
	
	
}
