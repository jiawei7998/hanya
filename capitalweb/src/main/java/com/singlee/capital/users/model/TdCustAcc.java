package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TD_CUST_ACC")
public class TdCustAcc implements Serializable {

	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款路径
	 */
	private static final long serialVersionUID = 1035538213938491333L;
	
	private String custmerCode;
	private String bankaccid;
	private String cny;
	private String status;
	private String bankaccname;
	private String openBankLargeAccno;
	private String openBankName;
	private String updatetime;
	private String operateUser;
	private String state;
	@Id
	private String code;
	
	private String mediumType;
	
	@Transient
	private String custmerName;
	
	
	public String getCustmerName() {
		return custmerName;
	}
	public void setCustmerName(String custmerName) {
		this.custmerName = custmerName;
	}
	public String getMediumType() {
		return mediumType;
	}
	public void setMediumType(String mediumType) {
		this.mediumType = mediumType;
	}
	public String getOperateUser() {
		return operateUser;
	}
	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public String getBankaccid() {
		return bankaccid;
	}
	public void setBankaccid(String bankaccid) {
		this.bankaccid = bankaccid;
	}
	public String getCny() {
		return cny;
	}
	public void setCny(String cny) {
		this.cny = cny;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBankaccname() {
		return bankaccname;
	}
	public void setBankaccname(String bankaccname) {
		this.bankaccname = bankaccname;
	}
	public String getOpenBankLargeAccno() {
		return openBankLargeAccno;
	}
	public void setOpenBankLargeAccno(String openBankLargeAccno) {
		this.openBankLargeAccno = openBankLargeAccno;
	}
	public String getOpenBankName() {
		return openBankName;
	}
	public void setOpenBankName(String openBankName) {
		this.openBankName = openBankName;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	
	
}
