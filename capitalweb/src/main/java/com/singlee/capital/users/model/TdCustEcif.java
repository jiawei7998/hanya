package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TD_CUST_ECIF")
public class TdCustEcif implements Serializable {

	/**
	 * 汪泽峰 2017.5.16
	 * 客户ECIF信息表实体类
	 */
	private static final long serialVersionUID = 4145717727038385902L;

	@Id
	private String custmerCode;
	private String cnName;
	private String gbName;
	private String custType;
	private String idType;
	private String idNumber;
	private String inputDt;
	private String sfCptyCountry;
	private String merType;
	private String strent;
	private String gbstreet;
	private String bankType;
	private String manager;
	private String managerProp;
	private String statusEcif;
	private String tyCustId;
	
	@Transient
	private String fcciCode;
	@Transient
	private String fcciName;
	@Transient
	private String custmerCode1;
	
	
	
	public String getCustmerCode1() {
		return custmerCode1;
	}
	public void setCustmerCode1(String custmerCode1) {
		this.custmerCode1 = custmerCode1;
	}
	public String getFcciCode() {
		return fcciCode;
	}
	public void setFcciCode(String fcciCode) {
		this.fcciCode = fcciCode;
	}
	public String getFcciName() {
		return fcciName;
	}
	public void setFcciName(String fcciName) {
		this.fcciName = fcciName;
	}
	public String getTyCustId() {
		return tyCustId;
	}
	public void setTyCustId(String tyCustId) {
		this.tyCustId = tyCustId;
	}
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getGbName() {
		return gbName;
	}
	public void setGbName(String gbName) {
		this.gbName = gbName;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getInputDt() {
		return inputDt;
	}
	public void setInputDt(String inputDt) {
		this.inputDt = inputDt;
	}
	public String getSfCptyCountry() {
		return sfCptyCountry;
	}
	public void setSfCptyCountry(String sfCptyCountry) {
		this.sfCptyCountry = sfCptyCountry;
	}
	public String getMerType() {
		return merType;
	}
	public void setMerType(String merType) {
		this.merType = merType;
	}
	public String getStrent() {
		return strent;
	}
	public void setStrent(String strent) {
		this.strent = strent;
	}
	public String getGbstreet() {
		return gbstreet;
	}
	public void setGbstreet(String gbstreet) {
		this.gbstreet = gbstreet;
	}
	public String getBankType() {
		return bankType;
	}
	public void setBankType(String bankType) {
		this.bankType = bankType;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getManagerProp() {
		return managerProp;
	}
	public void setManagerProp(String managerProp) {
		this.managerProp = managerProp;
	}
	public String getStatusEcif() {
		return statusEcif;
	}
	public void setStatusEcif(String statusEcif) {
		this.statusEcif = statusEcif;
	}
	
	
	
	
	
}
