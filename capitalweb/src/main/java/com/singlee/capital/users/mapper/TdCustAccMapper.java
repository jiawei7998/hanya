package com.singlee.capital.users.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAcc;

public interface TdCustAccMapper extends Mapper<TdCustAcc>{
	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAcc> showAllTdCustAcc(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAcc> showFinishTdCustAcc(Map<String, Object> map,RowBounds rb);
	
	
	public Page<TdCustAcc> showTdCustAccMain(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 汪泽峰 2017-5-19
	 * 添加划款信息
	 * @param map
	 */
	public void addTdCustAcc(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改划款信息
	 * @param map
	 */
	public void updateCustAcc(Map<String, Object> map);
	
	
	/**
	 * 汪泽峰 2017-5-21
	 * 删除划款信息
	 * @param map
	 */
	public void deleteCustAcc(Map<String, Object> map);
	
	
	
	/**
	 * 根据条件精确查询一条划款
	 * @param map
	 * @return
	 */
	public TdCustAcc showTdCustAccByColumns(Map<String, Object> map);
	
	/**
	 * 查询重复划款路径
	 * @param map
	 * @return
	 */
	public List<TdCustAcc> showSameAcc(Map<String, Object> map);
	
	//查询资金账户信息
	public Page<TdCustAcc> showAllCustAcc(Map<String, Object> map,RowBounds rb);
	
	public List<TdCustAcc> showFinishTdCustAcc(Map<String, Object> map);

}
