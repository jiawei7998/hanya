package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustLayered;

public interface TdCustLayeredMapper extends Mapper<TdCustLayered> {
	/**
	 * -汪泽峰 2017-5-17
	 * 根据客户号分页查询分层信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustLayered> showAllTdCustLayered(Map<String, Object> map,RowBounds rb);

	public void updateTdCustLayered(Map<String, Object> map);
	
	public void insertTdCustLayered(Map<String, Object> map);
}
