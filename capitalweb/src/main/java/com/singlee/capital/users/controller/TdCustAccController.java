package com.singlee.capital.users.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRq;
import com.singlee.capital.interfacex.model.TDpAcctTypeInqRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.model.TdCustAcc;
import com.singlee.capital.users.model.TdCustAccCopy;
import com.singlee.capital.users.service.TdCustAccCopyService;
import com.singlee.capital.users.service.TdCustAccService;

@Controller
@RequestMapping(value = "/TdCustAccController")
public class TdCustAccController extends CommonController {
	@Autowired
	private TdCustAccService tdCustAccService;
	@Autowired
	private TdCustAccCopyService tdCustAccCopyService;
	@Autowired
	private SocketClientService socketClientService;
	/**
	 * 汪泽峰 2017-5-18
	 * 分页查询客户划款信息
	 * @param map
	 * @return
	 */
	@ResponseBody			
	@RequestMapping(value = "/searchTdCustAccPage")
	public RetMsg<PageInfo<TdCustAcc>> searchTdCustAccPage(@RequestBody Map<String,Object> map){
		Page<TdCustAcc> page = tdCustAccService.showAllTdCustAccService(map);
		return RetMsgHelper.ok(page);
	}
	
	
	
	
	/**
	 * 汪泽峰 2017-5-18
	 * 分页查询客户划款信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdCustAccMain")
	public RetMsg<PageInfo<TdCustAcc>> searchTdCustAccMain(@RequestBody Map<String,Object> map){
		Page<TdCustAcc> page = tdCustAccService.showTdCustAccMainService(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 汪泽峰 2017-5-19
	 * 提交 划款信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTdCustAccCopy")
	public RetMsg<Boolean> insertTdCustAccCopy(@RequestBody Map<String,Object> map){
		//showTdCustAccCopy匹配到相同划款数据 返回false
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("custmerCode", map.get("custmerCode"));
		map1.put("bankaccid", map.get("bankaccid"));
		map1.put("cny", map.get("cny"));
		map1.put("openBankLargeAccno", map.get("openBankLargeAccno"));
		//map1.put("status", map.get("status"));
		if(("updatec".equals(map.get("updatec")))){  //如果是修改
			map.put("updatetime", DateUtil.getCurrentDateTimeAsString());
			map.put("operateUser",SlSessionHelper.getUserId());
			map.put("state", "3");
			//修改审批状态为3新建  以及是否启用
			tdCustAccCopyService.changeCustAccCopyStatusService(map);
			return RetMsgHelper.ok(true);
		}
		if(tdCustAccService.showSameAccService(map1).size()>0){
			return RetMsgHelper.ok(false);
		}
		map.put("updatetime", DateUtil.getCurrentDateTimeAsString());
		map.put("operateUser",SlSessionHelper.getUserId());
		map.put("state", "3");
		map.put("status", "1");   //新建审批 默认为启用
		tdCustAccCopyService.insertTdCustAccCopyService(map);
		return RetMsgHelper.ok(true);
	}
	
	/**
	 * 汪泽峰 2017-5-21
	 * 删除 划款信息
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTdCustAcc")
	public RetMsg<Serializable> deleteTdCustAcc(@RequestBody Map<String,Object> map){
		tdCustAccService.deleteCustAccService(map);
		return RetMsgHelper.ok();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/showWaitAccw")
	public RetMsg<PageInfo<TdCustAccCopy>> showWaitAccw(@RequestBody Map<String,Object> map){
		map.put("operateUser", SlSessionHelper.getUserId());
		//map.put("state", "3");
		Page<TdCustAccCopy> res = tdCustAccCopyService.showTdCustAccCopyMineService(map);
		return RetMsgHelper.ok(res);
	}
	
	@ResponseBody
	@RequestMapping(value = "/showWaitAccd")
	public RetMsg<PageInfo<TdCustAccCopy>> showWaitAccd(@RequestBody Map<String,Object> map){
		map.put("userId", SlSessionHelper.getUserId());
		map.put("state", "4");
		map.put("state1", "45");
		Page<TdCustAccCopy> res = tdCustAccCopyService.showTdCustAccCopyService(map);
		return RetMsgHelper.ok(res);
	}
	
	@ResponseBody
	@RequestMapping(value = "/showWaitAccy")
	public RetMsg<PageInfo<TdCustAccCopy>> showWaitAccy(@RequestBody Map<String,Object> map){
		map.put("userId", SlSessionHelper.getUserId());
//		map.put("state", "6");
//		map.put("state2", "678");
		Page<TdCustAccCopy> res = tdCustAccCopyService.showTdCustAccCopyCompletedService(map);
		return RetMsgHelper.ok(res);
	}
	
	/**
	 * 汪泽峰 2017-6-27
	 * 删除 划款信息 (附表)
	 * @param map
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTdCustAccCopy")
	public RetMsg<Serializable> deleteTdCustAccCopy(@RequestBody Map<String,Object> map){
		tdCustAccCopyService.deleteCustAccCopyService(map);
		return RetMsgHelper.ok();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/checkBanaccid")
	public RetMsg<TDpAcctTypeInqRs> checkBanaccid(@RequestBody Map<String,Object> map) throws Exception {
		TDpAcctTypeInqRq tti = new TDpAcctTypeInqRq();
		tti.setMedAcctNo(map.get("accid").toString());
		TDpAcctTypeInqRs tat = new TDpAcctTypeInqRs(); 
		tat = socketClientService.t24TdpAcctTypeInqRequest(tti);
		return RetMsgHelper.ok(tat);
	}
	
	//查询资金账户列表
	@ResponseBody
	@RequestMapping(value = "/searchCustAccPage")
	public RetMsg<PageInfo<TdCustAcc>> getCustAccPage(@RequestBody Map<String,Object> map){
		Page<TdCustAcc> page = tdCustAccService.showAllCustAccService(map);
		return RetMsgHelper.ok(page);
	}

	//添加资金账户信息
	@ResponseBody
	@RequestMapping(value = "/insertTdCustAcc")
	public RetMsg<Boolean> addTdCustA(@RequestBody Map<String,Object> map){
		RetMsg<Boolean> ret =  RetMsgHelper.ok(true);
		map.put("updatetime", DateUtil.getCurrentDateTimeAsString());
		map.put("operateUser",SlSessionHelper.getUserId());
		map.put("status", "1");
		map.put("state", "6");
		List<TdCustAcc> list = tdCustAccService.showFinishTdCustAcc(map);
		if(list != null && list.size() > 0){
			ret.setDetail("110");
		}else{
			ret.setDetail("000");
			tdCustAccService.addTdCustAccService(map);
		}
		return ret;
	}

	//更新资金账户信息
	@ResponseBody
	@RequestMapping(value = "/updateCustAccService")
	public RetMsg<Boolean> updateCustAccService(@RequestBody Map<String,Object> map){
		map.put("updatetime", DateUtil.getCurrentDateTimeAsString());
		map.put("operateUser",SlSessionHelper.getUserId());
		map.put("state", "6");
		tdCustAccService.updateCustAccService(map);
		return RetMsgHelper.ok(true);
	}
	@ResponseBody
	@RequestMapping(value = "/deleteTdCustAccs")
	public RetMsg<Serializable> deleteTdCustEcif(@RequestBody Map<String,Object> map){
		tdCustAccService.deleteCustAccServices(map);
		return RetMsgHelper.ok();
	}
}

