package com.singlee.capital.users.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.capital.eu.model.ProductWeight;
import com.singlee.capital.users.model.TdCustEcif;
import com.singlee.ifs.model.IfsOpicsCust;

public interface TdCustEcifMapper extends Mapper<TdCustEcif> {
	
	/**
	 * 分页查询ECIF
	 * 汪泽峰 - 2017-5-16
	 * @return
	 */
	public Page<TdCustEcif> showAllTdCustEcif(Map<String, Object> map,RowBounds rb);
	
	public void saveTdCustEcif(Map<String, Object> map);
	
	public List<TdCustEcif> showAllEcifId();
	
	public void insertEcif(Map<String, Object> map);
	
	public Page<TdCustEcif> showAllSPV(Map<String, Object> map,RowBounds rb);
	
	public void deleteSPV(Map<String, Object> map);
	
	public Page<TdCustEcif> showAllECIFSPV(Map<String, Object> map,RowBounds rb);
	
	public List<TdCustEcif> getTdCustEcifSettleList(Map<String, Object> map);
	
	public void insertTdCustEcifFromBos();
	
	public Page<IfsOpicsCust> showAllIfsOpicsCust(Map<String, Object> map,RowBounds rb);
	
	//查询所有交易的名称和ID
	public Page<TcProduct> searchAllProtectName(Map<String, Object> map,RowBounds rb);
	
	//查询指定产品编号详情
	public Page<ProductWeight> searchSingleProtectName(Map<String, Object> map,RowBounds rb);
	
	//查询指定产品编号详情
	public Page<ProductWeight> searchAllForeignProtectName(Map<String, Object> map,RowBounds rb);
	
	// 根据productCode删除权重数据
	public void deleteProtectByProductCode(String productCode);
	
	//增加比例权重数据operator,dateTime
	public void addProtectByProductWeight(@Param("list") List<Object> list,@Param("branchId") String branchId );
	
	public void riskManagerSave(@Param("list") List<Object> list);
	
	public void riskManagerDelete(@Param("list") List<Object> list);
	
	public Page<OpicsRisk> searchRiskManager(Map<String, Object> map,RowBounds rb);
	
	//查询需要excel导出的数据
	public List<OpicsRisk> searchRiskManagerEXP();
	
	//查询所有权重信息
	public List<ProductWeight> getProductList(Map<String, Object> map);
	
	//查询债券发行机构性质
	public String issuerTypeQry(String ticketId);
	
}
