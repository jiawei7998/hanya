package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.model.EcifBosoCustomerManager;
import com.singlee.capital.users.mapper.EcifBosoCustomerMMapper;
import com.singlee.capital.users.service.EcifBosoCustomerMService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EcifBosoCustomerMServiceImpl implements EcifBosoCustomerMService {
	
	@Autowired
	private EcifBosoCustomerMMapper ecifBosoCustomerMMapper;
	
	
	@Override
	public Page<EcifBosoCustomerManager> selectByParamsService(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<EcifBosoCustomerManager> result = ecifBosoCustomerMMapper.selectByParams(map, rb);
		return result;
	}

}
