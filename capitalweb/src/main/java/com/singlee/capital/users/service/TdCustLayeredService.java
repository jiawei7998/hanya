package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustLayered;

public interface TdCustLayeredService {
	/**
	 * 汪泽峰 2017-5-18
	 * 分层信息
	 * @param map
	 * @return
	 */
	public Page<TdCustLayered> showAllTdCustLayered(Map<String, Object> map);
	
	public void updateTdCustLayeredService(Map<String, Object> map);
	
	public void insertTdCustLayeredService(Map<String, Object> map);
}
