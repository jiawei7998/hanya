package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TD_CUST_LAYERED")
public class TdCustLayered implements Serializable {

	/**
	 * 汪泽峰 2017-5-17
	 * 分层信息
	 */
	private static final long serialVersionUID = -4703902584792912489L;
	
	
	private String custmerCode;
	@Id
	private Integer version;
	private String layType;
	private String layDate;
	private String isActive;
	
	
	
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getLayType() {
		return layType;
	}
	public void setLayType(String layType) {
		this.layType = layType;
	}
	public String getLayDate() {
		return layDate;
	}
	public void setLayDate(String layDate) {
		this.layDate = layDate;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	
}
