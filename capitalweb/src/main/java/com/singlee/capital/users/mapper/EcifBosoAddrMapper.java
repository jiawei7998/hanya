package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.EcifBosoAddress;

public interface EcifBosoAddrMapper extends Mapper<EcifBosoAddress>{
	
	
	/**
	 * 条件查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<EcifBosoAddress> selectAddrByParams(Map<String, Object> map,RowBounds rb);

	
	
}
