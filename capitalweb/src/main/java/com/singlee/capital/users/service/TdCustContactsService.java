package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustContacts;

public interface TdCustContactsService {
		
	/**
	 * 汪泽峰 2017-5-18
	 * 客户联系人查询
	 * @param map
	 * @return
	 */
	public Page<TdCustContacts> showAllTdCustContactsService(Map<String, Object> map);
	/**
	 * 汪泽峰 2017-5-22
	 * 新增联系人信息
	 * @param map
	 */
	public void insertTdCustContactsService(Map<String, Object> map);
	/**
	 * 汪泽峰 2017-5-22
	 * 修改联系人信息
	 * @param map
	 */
	public void updateTdCustContactsService(Map<String, Object> map);
	/**
	 * 汪泽峰 2017-5-22
	 * 删除联系人信息
	 * @param map
	 */
	public void deleteTdCustContactsService(Map<String, Object> map);
}
