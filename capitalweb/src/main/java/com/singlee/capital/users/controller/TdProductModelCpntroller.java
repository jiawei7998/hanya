package com.singlee.capital.users.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.model.TdProductModel;
import com.singlee.capital.users.service.TdProductModelService;

@Controller
@RequestMapping(value = "/TdProductModelCpntroller")
public class TdProductModelCpntroller extends CommonController{
	@Autowired
	private TdProductModelService tdProductModelService;
	
	
	@ResponseBody
	@RequestMapping(value = "/getTdProductModel")
	public RetMsg<PageInfo<TdProductModel>> getTdProductModel(@RequestBody Map<String,Object> map){
		Page<TdProductModel> page = tdProductModelService.showAllTdProductModelService(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 产品申请
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/applyTdProductModel")
	public RetMsg<Object> applyTdProductModel(@RequestBody Map<String,Object> map){
//		//申请产品 是否要判断 本地库中  是否已存在该产品号 
//		String rand = ((Math.random()*1000000)+"").substring(0, 5);
//		map.put("msgTypId", rand);
		RetMsg<Object> retmsg = tdProductModelService.applyTdProductModelService(map);
		return retmsg;
	}
	
	/**
	 * 产品同步保存
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/sameTdProductModel")
	public RetMsg<Object> asameTdProductModel(@RequestBody Map<String,Object> map){
		RetMsg<Object> retmsg = tdProductModelService.sameTdProductModelService(map);
		return retmsg;
	}
	
	/**
	 * 产品同步
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/syncSameTdProductModel")
	public RetMsg<Object> syncSameTdProductModel(@RequestBody Map<String,Object> map){
		RetMsg<Object> retmsg = tdProductModelService.syncSameTdProductModelService(map);
		return retmsg;
	}
	
	/**
	 * 产品修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTdProductModel")
	public RetMsg<Object> updateTdProductModel(@RequestBody Map<String,Object> map){
		RetMsg<Object> retmsg = tdProductModelService.updateTdProductModelService(map);
		return retmsg;
	}
	
}
