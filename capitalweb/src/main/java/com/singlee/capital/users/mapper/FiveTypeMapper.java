package com.singlee.capital.users.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.FiveType;

public interface FiveTypeMapper extends Mapper<FiveType>{
	/**
	 * 根据登陆人机构显示五级分类 
	 * @return
	 */
	public Page<FiveType> showFiveTypeByLoader(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 条件查询五级分类
	 * @return
	 */
	public Page<FiveType> SearchFiveType(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 新增五级分类产品
	 * @param map
	 */
	public void addFiveType(List<FiveType> list);
	
	/**
	 * 五级分类 展开详情 
	 * @param map
	 * @return
	 */
	public List<FiveType> FiveTypeDetails(Map<String, Object> map);
	
	
	/**
	 * 修改五级分类的 复合人
	 * @param map
	 */
	public void updateFTapprove(Map<String, Object> map);
	
	/**
	 * 根据条件删除五级分类
	 * @param map
	 */
	public void deleteFiveType(Map<String, Object> map);
	
	
}
