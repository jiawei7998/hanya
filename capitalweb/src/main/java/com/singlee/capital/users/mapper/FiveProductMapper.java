package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.FiveProduct;

public interface FiveProductMapper extends Mapper<FiveProduct>{
	/**
	 * 根据登陆人机构显示待审批 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductByLoader(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 新增一笔审批
	 * @param map
	 */
	public void addFiveProduct(Map<String, Object> map);
	
	/**
	 * 修改审批状态
	 * @param map
	 */
	public void updateFpState(Map<String, Object> map);
	
	/**
	 * 根据登陆人显示已审批 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductCompleted(Map<String, Object> map,RowBounds rb);
	
	
	
	
	/**
	 * 根据登陆人显示我发起的 
	 * @return
	 */
	public Page<FiveProduct> showFiveProductMine(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 根据条件删除五级产品
	 * @param map
	 */
	public void deleteFiveProduct(Map<String, Object> map);

}
