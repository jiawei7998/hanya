package com.singlee.capital.users.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.users.mapper.TdCustIbMapper;
import com.singlee.capital.users.model.TdCustIb;
import com.singlee.capital.users.service.TdCustIbService;
/**
 * 汪泽峰 2017-5-17
 * 同业信息查询
 * @author lush
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustIbServiceImpl implements TdCustIbService {
	@Autowired
	private TdCustIbMapper TciMapper;
	@Autowired
	private BatchDao batchDao;
	
	/**
	 * 根据客户号查询客户的同业信息
	 * 汪泽峰 2017-5-17
	 */
	@Override
	public TdCustIb showTdCustIbByCusCodeService(Map<String, Object> map) {
		return TciMapper.showTdCustIbByCusCode(map);
	}
	/**
	 * 修改同业信息
	 * 汪泽峰 2017-5-22
	 */
	@Override
	public void updateTdCustIbService(Map<String, Object> map) {
		TdCustIb tcIb=new TdCustIb();
		ParentChildUtil.HashMapToClass(map, tcIb);
		TciMapper.updateByPrimaryKey(tcIb);
	}
	@Override
	public void insertTdCustIbService(Map<String, Object> map) {
		TdCustIb tcIb=new TdCustIb();
		ParentChildUtil.HashMapToClass(map, tcIb);
		TciMapper.insert(tcIb);
	}
	@Override
	public void deleteSPVIbService(Map<String, Object> map) {
		TciMapper.deleteByPrimaryKey(map);
	}
	@Override
	public void insertTdCustIbs(List<TdCustIb> custIbs) {
		// TODO Auto-generated method stub
		TciMapper.deleteAllCustIbs();
		batchDao.batch("com.singlee.capital.users.mapper.TdCustIbMapper.insertCustIbs", custIbs);
	}

}
