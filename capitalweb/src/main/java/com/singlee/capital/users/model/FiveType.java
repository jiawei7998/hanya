package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FIVE_TYPE")
public class FiveType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dealNo;        //订单编号
	private String prdName;		//业务种类

	private String partyName;    //借款企业个人
	private String tradeCredit;   //同业增信方
	private String procAmt;			//交易金额 
	private String vDate;			//起息日
	private String mDate;			//支息日
	private String classifyAdvice;	//分类意见
	private String impairmentResults;//减值结果
	private String assignee;		//初分人
	private String approve;			//复核人
	private String productNo;      //业务编号
	
	@Transient
	private String prdNo;   //业务种类代码
	@Transient
	private String operateUser;
	@Transient
	private String instFullname;
	@Transient
	private String updateTime;
	@Transient
	private String sponInst;

	
	
	

	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getOperateUser() {
		return operateUser;
	}
	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}
	public String getInstFullname() {
		return instFullname;
	}
	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getProductNo() {
		return productNo;
	}
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getTradeCredit() {
		return tradeCredit;
	}
	public void setTradeCredit(String tradeCredit) {
		this.tradeCredit = tradeCredit;
	}
	public String getProcAmt() {
		return procAmt;
	}
	public void setProcAmt(String procAmt) {
		this.procAmt = procAmt;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getClassifyAdvice() {
		return classifyAdvice;
	}
	public void setClassifyAdvice(String classifyAdvice) {
		this.classifyAdvice = classifyAdvice;
	}
	public String getImpairmentResults() {
		return impairmentResults;
	}
	public void setImpairmentResults(String impairmentResults) {
		this.impairmentResults = impairmentResults;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getApprove() {
		return approve;
	}
	public void setApprove(String approve) {
		this.approve = approve;
	}
	
	
	
}
