package com.singlee.capital.users.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.users.mapper.TdCustAtCopyMapper;
import com.singlee.capital.users.mapper.TdCustAtMapper;
import com.singlee.capital.users.model.TdCustAtCopy;
import com.singlee.capital.users.service.TdCustAtCopyService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
@Service("tdCustAtCopyService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustAtCopyServiceImpl implements TdCustAtCopyService,SlbpmCallBackInteface,TaskEventInterface {
	@Autowired
	private TdCustAtCopyMapper tdCustAtCopyMapper;
	@Autowired
	private TdCustAtMapper tdCustAtMapper;

	@Override
	public Page<TdCustAtCopy> showAllTdCustAtCopyService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustAtCopy> result=tdCustAtCopyMapper.showAllTdCustAtCopy(map, rowBounds);
		return result;
	}

	@Override
	public void updateTdCustAtCopyService(Map<String, Object> map) {
		map.put("version",Integer.parseInt(map.get("version").toString())-1);
		map.put("isActive","0");
		tdCustAtCopyMapper.updateTdCustAtCopy(map);
	}

	@Override
	public void insertTdCustAtCopyService(Map<String, Object> map) {
//		String serial_no=map.get("custmerCode").toString()+":"+map.get("version").toString();
//		List<ProcDefInfo> list= flowDefService.listProcDef("", "21", "1", "150001",SlSessionHelper.getInstitution().getInstId());
//		JY.require(list.size() == 1, "流程匹配失败.");
//		flowOpService.startNewFlowInstance(SlSessionHelper.getUserId(), list.get(0).getFlow_id(), "21", serial_no, "", false, false, "", "tdCustAtCopyService", "");
//			int vers=Integer.parseInt(map.get("version").toString()); 
//			map.put("version",vers+1+"");
		tdCustAtCopyMapper.insertTdCustAtCopy(map);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
			System.out.println("CHANGE事件|||||||||||||||||||||||||||||||||||||||||||||");
			
			String custmerCode = serial_no.split("AT")[0];
			String version = serial_no.split("AT")[1];
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("custmerCode", custmerCode);
			map1.put("version", Integer.parseInt(version));
			map1.put("state", status);
			tdCustAtCopyMapper.updateTdCustAtCopy(map1);  //修改审批 状态为带审批
			
		if("6".equals(status)){
			//附表 数据 转移 到 主表 
			//将客户号和版本号 转换成 map条件 
//			TdCustAtCopy act = new TdCustAtCopy();
//			ParentChildUtil.HashMapToClass(map1, act); 
			//根据客户号和版本号 审批状态为5(审批中)获得唯一一条数据
//			map1.put("state", "6");
			System.out.println(custmerCode+"====================================================");
			map1.remove("state");
			RowBounds rb = ParameterUtil.getRowBounds(map1);
			List<TdCustAtCopy> result = tdCustAtCopyMapper.showAllTdCustAtCopyMine(map1, rb);
			TdCustAtCopy tdCustAtCopy = result.get(0);
			
			//判断主表有没有该客户的审批信息 
			Map<String, Object> maps = new HashMap<String, Object>();
			maps.put("custmerCode", custmerCode);
			maps.put("atType", tdCustAtCopy.getAtType());    //版本号
			RowBounds rb1 = ParameterUtil.getRowBounds(maps);
			if(tdCustAtMapper.showAllTdCustAt(maps, rb1).size()>0){
				//修改主表中的该客户的所有审批状态为不可用
				maps.put("isActive", "0");
				tdCustAtMapper.updateTdCustAt(maps);
			}
			//将完成审批的数据 插入主表 
			Map<String, Object> map2 = ParentChildUtil.ClassToHashMap(tdCustAtCopy);
			map2.put("state", "6");
			map2.put("isActive", "1");
			tdCustAtMapper.insertTdCustAt(map2);
			//删除附表数据
//			tdCustAtCopyMapper.delete(act);
		}
		else if ("8".equals(status)){
			//撤消  此条信息报废  不能占用version 仅存放在td_cust_at_copy表中
			map1.put("version1", 0);
			tdCustAtCopyMapper.updateTdCustAtCopy(map1);
		}
	}

	@Override
	public void deleteTdCustAtCopyService(Map<String, Object> map) {
		TdCustAtCopy TdCustAtCopy = new TdCustAtCopy();
		ParentChildUtil.HashMapToClass(map, TdCustAtCopy);
		tdCustAtCopyMapper.delete(TdCustAtCopy);
	}

	

	@Override
	public Page<TdCustAtCopy> showAllTdCustAtCopyCompletedService(
			Map<String, Object> map) {
		RowBounds rowbounds = ParameterUtil.getRowBounds(map);
		return tdCustAtCopyMapper.showAllTdCustAtCopyCompleted(map, rowbounds);
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TdCustAtCopy> showSameAtCopyService(Map<String, Object> map) {
		List<TdCustAtCopy> list = tdCustAtCopyMapper.showSameAtCopy(map);
		return list;
	}

	@Override
	public Page<TdCustAtCopy> showAllTdCustAtCopyMineService(
			Map<String, Object> map) {
		RowBounds rowbounds = ParameterUtil.getRowBounds(map);
		return tdCustAtCopyMapper.showAllTdCustAtCopyMine(map, rowbounds);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }

	

}
