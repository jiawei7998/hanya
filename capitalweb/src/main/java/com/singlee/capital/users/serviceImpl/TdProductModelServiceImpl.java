package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.interfacex.model.CloudProApplyTransRq;
import com.singlee.capital.interfacex.model.CloudProApplyTransRs;
import com.singlee.capital.interfacex.model.CloudProModTransRq;
import com.singlee.capital.interfacex.model.CloudProModTransRs;
import com.singlee.capital.interfacex.model.CloudProSynchTransRq;
import com.singlee.capital.interfacex.model.CloudProSynchTransRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.users.mapper.TdProductModelMapper;
import com.singlee.capital.users.model.TdProductModel;
import com.singlee.capital.users.service.TdProductModelService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdProductModelServiceImpl implements TdProductModelService {
	@Autowired
	private TdProductModelMapper tdmMapper;
	@Resource
	SocketClientService socketClientService;
	
	
	@Override
	public Page<TdProductModel> showAllTdProductModelService(
			Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdProductModel> result=tdmMapper.showAllTdProductModel(map, rowBounds);
		return result;
	}

	/**
	 * 申请产品
	 */
	@Override
	public RetMsg<Object> applyTdProductModelService(Map<String, Object> map) {
		RetMsg<Object> retmsg = new RetMsg<Object>("000000", "成功，接口未调用", "");
		TdProductModel tpm = new TdProductModel();
//		ParentChildUtil.HashMapToClass(map, tpm);
//		tdmMapper.insert(tpm);
		CloudProApplyTransRq request = new CloudProApplyTransRq();
		try {
			request.setProductId(map.get("productId")==""?"":map.get("productId").toString());
			request.setPrdName(map.get("prdName")==""?"":map.get("prdName").toString());
			request.setBusType(map.get("busType")==""?"":map.get("busType").toString());
			request.setSector(map.get("sector")==""?"":map.get("sector").toString());
			request.setDepCode(map.get("depCode")==""?"":map.get("depCode").toString());
			request.setCardCrDt(map.get("cardCrDt")==""?"":map.get("cardCrDt").toString().replace("-", "").replace("-", ""));
			request.setBegDate(map.get("begDate")==""?"":map.get("begDate").toString().replace("-", "").replace("-", ""));
			
			CloudProApplyTransRs response = socketClientService.CloudProApplyTransRequest(request);
			
			if(response == null || StringUtil.isEmpty(response.getProductId())){
				JY.raise("核心中没有返回统一产品编号");
			}
			//入库
//			Map<String, Object> map1 = new HashMap<String, Object>();
			map.put("msgTypId", response.getMsgTypId()==null|| "".equals(response.getMsgTypId())?"":response.getMsgTypId());
			map.put("productId", response.getProductId()==null|| "".equals(response.getProductId())?"":response.getProductId());
			map.put("prdName", response.getPrdName()==null|| "".equals(response.getPrdName())?"":response.getPrdName());
			map.put("busType", response.getBusType()==null|| "".equals(response.getBusType())?"":response.getBusType());
			map.put("sector", response.getSector()==null|| "".equals(response.getSector())?"":response.getSector());
			map.put("depCode", response.getDepCode()==null|| "".equals(response.getDepCode())?"":response.getDepCode());
			map.put("cardCrDt", response.getCardCrDt()==null|| "".equals(response.getCardCrDt())?"":response.getCardCrDt());
			map.put("begDate", response.getBegDate()==null|| "".equals(response.getBegDate())?"":response.getBegDate());
			map.put("enddDate", map.get("enddDate")==""?"":map.get("enddDate").toString().replace("-", "").replace("-", ""));
			map.put("dateEnd", map.get("dateEnd")==""?"":map.get("dateEnd").toString().replace("-", "").replace("-", ""));
			tpm = new TdProductModel();
			ParentChildUtil.HashMapToClass(map, tpm);
			tdmMapper.insert(tpm);
			
			
			retmsg.setCode("000000".equals(response.getCommonRsHdr().getStatusCode())?"error.common.0000":response.getCommonRsHdr().getStatusCode());
			retmsg.setDesc(response.getCommonRsHdr().getServerStatusCode());
			System.out.println(response.getCommonRsHdr().getStatusCode()+"||"+response.getCommonRsHdr().getServerStatusCode());
			return retmsg;
		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());			
		}
		return retmsg;
	}
	
	
	/**
	 * 同步产品
	 */   
	@Override
	public RetMsg<Object> syncSameTdProductModelService(Map<String, Object> map) {
		RetMsg<Object> retmsg = new RetMsg<Object>("000000", "成功，接口未调用", "");
		TdProductModel tpm = new TdProductModel();
		CloudProSynchTransRq request = new CloudProSynchTransRq();
		try {
			request.setProductId(map.get("productId")==""?"":map.get("productId").toString());
			request.setMsgTypId(map.get("msgTypId")==""?"":map.get("msgTypId").toString());
			
			CloudProSynchTransRs response = socketClientService.CloudProSynchTransRequest(request);
			//入库
			map.put("msgTypId", response.getMsgTypId()==""?"":response.getMsgTypId());
			map.put("productId", response.getProductId()==""?"":response.getProductId());
			map.put("prdName", response.getPrdName()==""?"":response.getPrdName());
			map.put("busType", response.getBusType()==""?"":response.getBusType());
			map.put("sector", response.getSector()==""?"":response.getSector());
			map.put("depCode", response.getDepCode()==""?"":response.getDepCode());
			map.put("cardCrDt", response.getCardCrDt()==""?"":response.getCardCrDt());
			map.put("begDate", response.getBegDate()==""?"":response.getBegDate());
			map.put("enddDate", response.getEnddDate()==""?"":response.getEnddDate());
			map.put("dateEnd", response.getDateEnd()==""?"":response.getDateEnd());
			map.put("introducer", response.getIntroducer()==""?"":response.getIntroducer());
			map.put("applyName", response.getApplyName()==""?"":response.getApplyName());
			map.put("mntnType", response.getMntnType()==""?"":response.getMntnType());
			map.put("dsCompanyname", response.getDsCompanyname()==""?"":response.getDsCompanyname());
			map.put("orgCode", response.getOrgCode()==""?"":response.getOrgCode());
			map.put("legalEntTyp", response.getLegalEntTyp()==""?"":response.getLegalEntTyp());
			map.put("legalEntNo", response.getLegalEntNo()==""?"":response.getLegalEntNo());
			map.put("relflg", response.getRelflg()==""?"":response.getRelflg());
			map.put("custSaleCode", response.getCustSaleCode()==""?"":response.getCustSaleCode());
			map.put("relName", response.getRelName()==""?"":response.getRelName());
			map.put("businoticeflg", response.getBusinoticeflg()==""?"":response.getBusinoticeflg());
			map.put("applOrg", response.getApplOrg()==""?"":response.getApplOrg());
			map.put("clcRegionName", response.getClcRegionName()==""?"":response.getClcRegionName());
			map.put("riskLev", response.getRiskLev()==""?"":response.getRiskLev());
			map.put("relCusName", response.getRelCusName()==""?"":response.getRelCusName());
			map.put("billStatus", response.getBillStatus()==""?"":response.getBillStatus());
			map.put("statuss", response.getStatuss()==""?"":response.getStatuss());
			map.put("workStateCode", response.getWorkStateCode()==""?"":response.getWorkStateCode());
			map.put("grpRelCutCode", response.getGrpRelCutCode()==""?"":response.getGrpRelCutCode());
			map.put("detailId", response.getDetailId()==""?"":response.getDetailId());
			map.put("channelType", response.getChannelType()==""?"":response.getChannelType());
			tpm = new TdProductModel();
			ParentChildUtil.HashMapToClass(map, tpm);
			tdmMapper.updateByPrimaryKey(tpm);
			
			retmsg.setCode("000000".equals(response.getCommonRsHdr().getStatusCode())?"error.common.0000":response.getCommonRsHdr().getStatusCode());
			retmsg.setDesc(response.getCommonRsHdr().getServerStatusCode());
			System.out.println(response.getCommonRsHdr().getStatusCode()+"||"+response.getCommonRsHdr().getServerStatusCode());
			return retmsg;
		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());
			
		}
		return retmsg;
	}
	
	/**
	 * 同步产品保存
	 */   
	@Override
	public RetMsg<Object> sameTdProductModelService(Map<String, Object> map) {
		RetMsg<Object> retmsg = new RetMsg<Object>("000000", "成功", "");
		TdProductModel tpm = new TdProductModel();
		ParentChildUtil.HashMapToClass(map, tpm);
		try {
			tdmMapper.updateByPrimaryKey(tpm);
		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());
		}
		
		
		return retmsg;
	}

	/**
	 * 修改产品
	 */   
	@Override
	public RetMsg<Object> updateTdProductModelService(Map<String, Object> map) {
		RetMsg<Object> retmsg = new RetMsg<Object>("000000", "成功，接口未调用", "");
		TdProductModel tpm = new TdProductModel();
		ParentChildUtil.HashMapToClass(map, tpm);
//		tdmMapper.updateByPrimaryKey(tpm);
		CloudProModTransRq request = new CloudProModTransRq();
		try {
			request.setProductId(map.get("productId")==""?"":map.get("productId").toString());
			request.setMsgTypId(map.get("msgTypId")==""?"":map.get("msgTypId").toString());
			request.setPrdName(map.get("prdName")==""?"":map.get("prdName").toString());
			request.setBusType(map.get("busType")==""?"":map.get("busType").toString());
			request.setSector(map.get("sector")==""?"":map.get("sector").toString());
			request.setDepCode(map.get("depCode")==""?"":map.get("depCode").toString());
			request.setCardCrDt(map.get("cardCrDt")==""?"":map.get("cardCrDt").toString());
			request.setBegDate(map.get("begDate")==""?"":map.get("begDate").toString());
			
			CloudProModTransRs response = socketClientService.CloudProModTransRequest(request);
			tdmMapper.updateByPrimaryKey(tpm);
			
			
			retmsg.setCode("000000".equals(response.getCommonRsHdr().getStatusCode())?"error.common.0000":response.getCommonRsHdr().getStatusCode());
			retmsg.setDesc(response.getCommonRsHdr().getServerStatusCode());
			System.out.println(response.getCommonRsHdr().getStatusCode()+"||"+response.getCommonRsHdr().getServerStatusCode());
			return retmsg;
		} catch (Exception e) {
			// TODO: handle exception
			retmsg.setCode("999999");
			retmsg.setDesc(e.getMessage());
			
		}
		return retmsg;
	}
	

}
