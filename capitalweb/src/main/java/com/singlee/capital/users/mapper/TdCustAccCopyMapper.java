package com.singlee.capital.users.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAccCopy;

public interface TdCustAccCopyMapper extends Mapper<TdCustAccCopy>{
	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款信息
	 * @param map
	 * @param rb
	 * @return
	 */
	//public Page<TdCustAccCopy> showAllTdCustAccCopy(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 汪泽峰 2017-5-19
	 * 添加划款信息
	 * @param map
	 */
	public void insertTdCustAccCopy(Map<String, Object> map);
	
	public Page<TdCustAccCopy> showTdCustAccCopy(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改审批状态
	 * @param map
	 */
	public void updateCustAccCopy(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-7-20
	 * 修改已完成的划款信息
	 * @param map
	 */
	public void changeCustAccCopyStatus (Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-6-27
	 * 删除浮标划款信息
	 * @param map
	 */
	public void deleteCustAccCopy(Map<String, Object> map);
	
	
	public Page<TdCustAccCopy> showTdCustAccCopyCompleted(Map<String, Object> map,RowBounds rb);
	

	public Page<TdCustAccCopy> showTdCustAccCopyMine(Map<String, Object> map,RowBounds rb);
	
	
	/**
	 * 查询是否存在 正在审批中的划款
	 * @param map
	 * @return
	 */
	public List<TdCustAccCopy> showSameAccCopy(Map<String, Object> map);
	
	
	public TdCustAccCopy showTdCustAccCopyByBankaccid(Map<String, Object> map);
	
}
