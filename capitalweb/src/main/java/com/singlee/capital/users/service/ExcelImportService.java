package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import jxl.Workbook;

public interface ExcelImportService {
	/**
	 * 读取excel对应sheet，返回对应List
	 * @param wb
	 * @param sheetName
	 * @return
	 */
	
	public List<Map<String, Object>> readExcel(Workbook wb, String sheetName);


}
