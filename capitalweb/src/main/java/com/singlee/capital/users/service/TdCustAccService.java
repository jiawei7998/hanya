package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAcc;

public interface TdCustAccService {
	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAcc> showAllTdCustAccService(Map<String, Object> map);
	
	
	public Page<TdCustAcc> showTdCustAccMainService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-18
	 * 添加划款信息
	 * @param map
	 */
	public void addTdCustAccService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改划款信息
	 * @param map
	 */
	public void updateCustAccService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-21
	 * 删除划款信息
	 * @param map
	 */
	public void deleteCustAccService(Map<String, Object> map);
	
	/**
	 * 根据条件精确查询一条划款
	 * @param map
	 * @return
	 */
	public TdCustAcc showTdCustAccByColumnsService(Map<String, Object> map);
	
	/**
	 * 查询重复划款路径
	 * @param map
	 * @return
	 */
	public List<TdCustAcc> showSameAccService(Map<String, Object> map);

	// 查询资金账户信息
	public Page<TdCustAcc> showAllCustAccService(Map<String, Object> map);


	public void deleteCustAccServices(Map<String, Object> map);
	
	public List<TdCustAcc> showFinishTdCustAcc(Map<String, Object> map);
}
