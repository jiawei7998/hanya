package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.model.EcifBosoIdentifier;
import com.singlee.capital.users.mapper.EcifBosoIdentifMapper;
import com.singlee.capital.users.service.EcifBosoIdentifService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EcifBosoIdentifServiceImpl implements EcifBosoIdentifService {
	@Autowired
	private EcifBosoIdentifMapper ecifBosoIdentifMapper;
	@Override
	public Page<EcifBosoIdentifier> selectbosIdByParams(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<EcifBosoIdentifier> result = ecifBosoIdentifMapper.selectbosIdByParams(map, rb);
		return result;
	}
	

}
