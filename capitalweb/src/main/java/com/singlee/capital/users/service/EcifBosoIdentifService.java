package com.singlee.capital.users.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.EcifBosoIdentifier;

public interface EcifBosoIdentifService {
		
	/**
	 * 条件查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<EcifBosoIdentifier> selectbosIdByParams(Map<String, Object> map);

}
