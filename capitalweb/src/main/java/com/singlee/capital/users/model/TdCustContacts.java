package com.singlee.capital.users.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TD_CUST_CONTACTS")
public class TdCustContacts implements Serializable {

	/**
	 * 汪泽峰 2017-5-18
	 * 客户联系人
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	private String custmerCode;
	private String name;
	private String position;
	private String tel;
	private String cellphone;
	private String email;
	private String fax;
	private String im;
	private String address;
	private String postcode;
	private String updatetime;
	private String attachId;
	
	
	
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getCellphone() {
		return cellphone;
	}
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getIm() {
		return im;
	}
	public void setIm(String im) {
		this.im = im;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public String getAttachId() {
		return attachId;
	}
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	
	
	
}
