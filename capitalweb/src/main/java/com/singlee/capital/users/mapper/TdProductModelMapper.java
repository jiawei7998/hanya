package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdProductModel;

public interface TdProductModelMapper extends Mapper<TdProductModel> {
	/**
	 * 汪泽峰 2017-5-25
	 * 分页查询产品 
	 * 
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdProductModel> showAllTdProductModel(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 申请产品交易
	 * @param map
	 */
	public void applyTdProductModel(Map<String, Object> map);
	
	/**
	 * 修改产品交易
	 * @param map
	 */
	public void updateTdProductModel(Map<String, Object> map);
	
	
	
}
