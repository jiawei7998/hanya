package com.singlee.capital.users.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustContacts;


public interface TdCustContactsMapper extends Mapper<TdCustContacts> {
	/**
	 * 汪泽峰 2017-5-18
	 * 查询客户联系人信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustContacts> showAllTdCustContacts(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 新增客户联系人信息
	 * @param map
	 */
	public void insertTdCustContacts(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改客户联系人信息
	 * @param map
	 */
	public void updateTdCustContacts(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 删除客户联系人信息
	 * @param map
	 */
	public void deleteTdCustContacts(Map<String, Object> map);
	
}
