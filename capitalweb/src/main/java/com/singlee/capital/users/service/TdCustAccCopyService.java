package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.TdCustAccCopy;

public interface TdCustAccCopyService {
	/**
	 * 汪泽峰 2017-5-18
	 * 客户划款信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TdCustAccCopy> showTdCustAccCopyService(Map<String, Object> map);
	
	
	
	/**
	 * 汪泽峰 2017-5-18
	 * 添加划款信息
	 * @param map
	 */
	public void insertTdCustAccCopyService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-5-22
	 * 修改审批状太
	 * @param map
	 */
	public void updateCustAccCopyService(Map<String, Object> map);
	
	/**
	 * 汪泽峰 2017-7-20
	 * 修改已完成的划款信息
	 * @param map
	 */
	public void changeCustAccCopyStatusService (Map<String, Object> map);
	/**
	 * 汪泽峰 2017-6-27
	 * 删除浮标划款信息
	 * @param map
	 */
	public void deleteCustAccCopyService(Map<String, Object> map);
	
	
	public Page<TdCustAccCopy> showTdCustAccCopyCompletedService(Map<String, Object> map);
	
	

	public Page<TdCustAccCopy> showTdCustAccCopyMineService(Map<String, Object> map);
	
	
	/**
	 * 查询是否存在 正在审批中的划款
	 * @param map
	 * @return
	 */
	public List<TdCustAccCopy> showSameAccCopyService(Map<String, Object> map);
	
	
}
