package com.singlee.capital.users.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.users.model.FiveType;

public interface FiveTypeService {
	
	/**
	 * 根据登陆人机构显示五级分类 
	 * @return
	 */
	public Page<FiveType> showFiveTypeByLoaderService(Map<String, Object> map);
	
	
	/**
	 * 条件查询五级分类
	 * @return
	 */
	public Page<FiveType> SearchFiveTypeService(Map<String, Object> map);
	
	/**
	 * 新增五级分类产品
	 * @param map
	 */
	public void addFiveTypeService(List<FiveType> list);
	
	/**
	 * 五级分类 展开详情 
	 * @param map
	 * @return
	 */
	public List<FiveType> FiveTypeDetails(Map<String, Object> map);
	

	/**
	 * 修改五级分类的 复合人
	 * @param map
	 */
	public void updateFTapproveService(Map<String, Object> map);
	
	/**
	 * 根据条件删除五级分类
	 * @param map
	 */
	public void deleteFiveTypeService(Map<String, Object> map);
}
