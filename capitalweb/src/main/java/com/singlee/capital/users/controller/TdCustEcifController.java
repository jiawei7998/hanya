package com.singlee.capital.users.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.participant.service.TcProductParticipantService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.credit.util.ExcelUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.model.ProductWeight;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.interfacex.model.AcctOfficerRec;
import com.singlee.capital.interfacex.model.AddrAppRec;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRq;
import com.singlee.capital.interfacex.model.C167CoCorpCusInqRs;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.EcifBosoAddress;
import com.singlee.capital.interfacex.model.EcifBosoCustomerManager;
import com.singlee.capital.interfacex.model.EcifBosoIdentifier;
import com.singlee.capital.interfacex.model.TCDInqTypeRec;
import com.singlee.capital.interfacex.model.TCustDetInqRq;
import com.singlee.capital.interfacex.model.TCustDetInqRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.users.mapper.TdCustEcifMapper;
import com.singlee.capital.users.model.TdCustAt;
import com.singlee.capital.users.model.TdCustContacts;
import com.singlee.capital.users.model.TdCustEcif;
import com.singlee.capital.users.model.TdCustIb;
import com.singlee.capital.users.model.ViewObject;
import com.singlee.capital.users.service.EcifBosoAddrService;
import com.singlee.capital.users.service.EcifBosoCustomerMService;
import com.singlee.capital.users.service.EcifBosoIdentifService;
import com.singlee.capital.users.service.TdCustAtService;
import com.singlee.capital.users.service.TdCustContactsService;
import com.singlee.capital.users.service.TdCustEcifService;
import com.singlee.capital.users.service.TdCustIbService;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbCbtMapper;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.model.IfsOpicsCust;

@Controller
@RequestMapping(value = "/TdCustEcifController")
public class TdCustEcifController extends CommonController {
	@Autowired
	private TdCustEcifService tdCustEcifService;
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private TdCustAtService tdCustAtService;
	@Autowired
	private TdCustIbService tdCustIbService;
	@Autowired
	private EcifBosoCustomerMService ecifBosoCustomerMService;
	@Autowired
	private EcifBosoIdentifService ecifBosoIdentifService;
	@Autowired
	private EcifBosoAddrService ecifBosoAddrService;
	@Autowired
	private TdCustContactsService tdCustContactsService;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	TcProductParticipantService tcProductParticipantService;
	@Autowired
	private TdCustEcifMapper tdCustEcifMapper;
	@Autowired
	TdEdCustBatchStatusMapper tdEdCustBatchStatusMapper;
	@Autowired
	private EdCustManangeService edCustManangeService;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	/**
	 * 获取用户ECIF信息页面 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdCustEcifPage")
	public RetMsg<PageInfo<TdCustEcif>> getTdCustEcifPage(@RequestBody Map<String,Object> map){
		Page<TdCustEcif> page = tdCustEcifService.showAllTdCustEcifservice(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 获取用户ECIF信息页面 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getECIFSPVPage")
	public RetMsg<PageInfo<TdCustEcif>> getECIFSPVPage(@RequestBody Map<String,Object> map){
		Page<TdCustEcif> page = tdCustEcifService.showAllECIFSPVService(map);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 获取用户SPV信息页面 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSPVPage")
	public RetMsg<List<TdCustEcif>> getSPVPage(@RequestBody Map<String,Object> map){
		Page<TdCustEcif> page = tdCustEcifService.showAllSPVService(map);
		List<TdCustEcif> lists= new ArrayList<TdCustEcif>();
		for (TdCustEcif tdCustEcif : page) {
			TdCustEcif tce = tdCustEcif;
			
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("custmerCode",tce.getCustmerCode());
			TdCustIb ti = tdCustIbService.showTdCustIbByCusCodeService(map2);
			if(ti!= null){
				if(ti.getFcciCode()!=null){
					//所属一级法人代码
					tce.setFcciCode(ti.getFcciCode());				
					//所属一级法人中文名
					Map<String, Object> map3 = new HashMap<String, Object>();
					map3.put("custmerCode", ti.getFcciCode());
					Page<TdCustEcif> tce1 = tdCustEcifService.showAllECIFSPVService(map3);
					tce.setFcciName(tce1.get(0).getCnName());
				}
			}
			lists.add(tce);
		}
		return RetMsgHelper.ok(lists);
	}
	
	/**
	 * SPV修改新增
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveSPV")
	public RetMsg<Serializable> saveSPV(@RequestBody Map<String,Object> map){
		
		if(map.get("custmerCode") == null || map.get("custmerCode") == ""){
			String rand1 = Math.random()+"";
			String rand ="SPV"+DateUtil.getCurrentDateAsString().replace("-", "")+rand1.replace(".","").substring(0,4);
			//新增spv用户
			map.put("custmerCode", rand);
			map.put("custType", "SPV");
			tdCustEcifService.insertEcifService(map);  //插入一条SPV用户
			
			//插入一级法人信息到同业表
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("custmerCode", rand);
			map2.put("fcciCode", map.get("fcciCode"));
			tdCustIbService.insertTdCustIbService(map2);
		}
		else{
			//修改spv用户
			map.put("custType", "SPV");
			tdCustEcifService.saveTdCustEcifService(map);
			//修改一级法人信息
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("custmerCode", map.get("custmerCode"));
			map2.put("fcciCode", map.get("fcciCode"));
			tdCustIbService.updateTdCustIbService(map2);
			
		}
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * SPV删除 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSPV")
	public RetMsg<Serializable> deleteSPV(@RequestBody Map<String,Object> map){
		tdCustEcifService.deleteSPVService(map);
		tdCustIbService.deleteSPVIbService(map);
		return RetMsgHelper.ok();
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/searchTdCustEcifPaged")
	public RetMsg<PageInfo<ViewObject>> getTdCustEcifPaged(@RequestBody Map<String,Object> map){
		//条件查询  转化成ECIF对应条件
		map.put("custmerCode", ParameterUtil.getString(map,"party_shortname",""));
		map.put("cnName", ParameterUtil.getString(map,"party_name",""));
		List<ViewObject> list = new ArrayList<ViewObject>();
		Page<TdCustEcif> page = tdCustEcifService.showAllTdCustEcifservice(map);
		Page<ViewObject> page2=new Page<ViewObject>(page.getPageNum(), page.getPageSize());
		page2.setStartRow(page.getStartRow());
		page2.setEndRow(page.getEndRow());
		page2.setTotal(page.getTotal());
		page2.setPages(page.getPages());
		page2.setPageSizeZero(page.getPageSizeZero());
		TaDictVo vot =null;ViewObject vo=null;
		for (TdCustEcif tce : page) {
			vo = new ViewObject();
			vo.setParty_id(tce.getCustmerCode());
			vo.setParty_name(tce.getCnName());
			vo.setIs_active(tce.getGbName());
			vo.setParty_code(tce.getCustType());
			vo.setCore_custno(tce.getTyCustId());  //同业客户标识
			vo.setSf_cpty_country(tce.getSfCptyCountry()); //国别
			vo.setMerType(tce.getMerType()); //行业类别
			if(tce.getMerType()!=null){
				vot = dictionaryGetService.getTaDictByCodeAndKey("MT0001",tce.getMerType());
				vo.setMerTypeName(vot==null?tce.getMerType():vot.getDict_value());
			}
			vo.setStrent(tce.getStrent());  //所在街道
			
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("isActive", "1");
			map1.put("custmerCode",tce.getCustmerCode());
			Page<TdCustAt> p1 = tdCustAtService.showAllTdCustAtServiceNew(map1);
			if(p1.size()>0){
				TdCustAt tc = p1.get(0);
				vo.setAt_type(tc.getAtType());                 //准入级别
				vo.setAt_date(tc.getAtDate());					//准入级别有效期
			}
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("custmerCode",tce.getCustmerCode());
			TdCustIb ti = tdCustIbService.showTdCustIbByCusCodeService(map2);
			if(ti!= null){
				if(ti.getFcciCode()!=null){
					//所属一级法人代码(存入)
					vo.setFcci_code(ti.getFcciCode());				
					//所属一级法人中文名(前台显示)
					Map<String, Object> map3 = new HashMap<String, Object>();
					map3.put("custmerCode", ti.getFcciCode());
					Page<TdCustEcif> tce1 = tdCustEcifService.showAllTdCustEcifservice(map3);
					vo.setFcci_codeName(tce1.get(0).getCnName());
				}
			}
			//带出联系人信息
			List<TdCustContacts> tcc = tdCustContactsService.showAllTdCustContactsService(map2);
				if(tcc.size()==1){
					vo.setPartyContacts1(tcc.get(0).getName()==null?"":tcc.get(0).getName());
				}else if(tcc.size()>=2){
					vo.setPartyContacts1(tcc.get(0).getName()==null?"":tcc.get(0).getName());
					vo.setPartyContacts2(tcc.get(1).getName()==null?"":tcc.get(1).getName());
				}else{
					vo.setPartyContacts1("");
					vo.setPartyContacts2("");
				}
				
			
				
			list.add(vo);
			
		}
		page2.addAll(list);
		return RetMsgHelper.ok(page2);
	}
	
	/**
	 * ECIF信息同步保存
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTdCustEcif")
	public RetMsg<Serializable> saveTdCustEcif(@RequestBody Map<String,Object> map){
		map.put("custmerCode", map.get("custmerCode1"));
		map.remove("custmerCode1");
		Map<String, Object> map1= new HashMap<String, Object>();
		map1.put("custmerCode", map.get("custmerCode"));
		if(tdCustEcifService.showAllTdCustEcifservice(map1).size()>0){
			tdCustEcifService.saveTdCustEcifService(map);
		}else{
			tdCustEcifService.insertEcifService(map);
		}
		
		return RetMsgHelper.ok();
	}
	
	/**
	 * 接口同步ECIF信息
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdCustEcifById")
	public RetMsg<TdCustEcif> getTdCustEcifById(@RequestBody Map<String,Object> map) throws Exception{
		TdCustEcif tcEcif = new TdCustEcif();
		CommonRqHdr commonRqHdr = new CommonRqHdr();
		commonRqHdr.setChannelId("S89");
		commonRqHdr.setRqUID(UUID.randomUUID().toString());
		String s1=DateUtil.getCurrentDateAsString().replace("-", "");
		String s2=DateTimeUtil.getLocalTime().replace(":", "");
		commonRqHdr.setTranDate(s1);
		commonRqHdr.setTranTime(s2);
		TCustDetInqRq request = new TCustDetInqRq();
		request.setCommonRqHdr(commonRqHdr);
		
		if(map.get("custmerCode").toString().trim() != ""){
			//根据客户号查询
			request.setCustmerCode(map.get("custmerCode").toString());
		}else{
			//根据客户名  客户号  证件类型  证件号 查询 
			request.setIdNum(map.get("idNumber").toString());
			request.setIdType(map.get("idType").toString());
			request.setCNName(map.get("cnName").toString());
		}
		TCustDetInqRs ccir = new TCustDetInqRs();
		ccir=socketClientService.ecifTCustDetInqRequest(request);
		
		if(!("000000".equals(ccir.getCommonRsHdr().getStatusCode()))){
			tcEcif.setManager(ccir.getCommonRsHdr().getServerStatusCode());
			
		}
		if("1".equals(ccir.getTyCustId())){
			//查询同业客户
			tcEcif.setCustmerCode(ccir.getCustmerCode());
			tcEcif.setCustmerCode1(ccir.getCustmerCode());
			tcEcif.setCnName(ccir.getCNName());
			tcEcif.setGbName(ccir.getGBName());
			tcEcif.setCustType(ccir.getCustType());
			tcEcif.setIdType(ccir.getIDType().trim());
			tcEcif.setIdNumber(ccir.getIDNumber());
			tcEcif.setInputDt("-");
			tcEcif.setBankType("-");
			List<TCDInqTypeRec> tcdList = ccir.getTCDInqTypeRec();
			for (TCDInqTypeRec tcdInqTypeRec : tcdList) {
				tcEcif.setSfCptyCountry(tcdInqTypeRec.getAddCountry());
			}
			tcEcif.setStrent(ccir.getCnStreet());
			tcEcif.setMerType(ccir.getIndustry());
			tcEcif.setGbstreet(ccir.getGbstreet());
			tcEcif.setManager(ccir.getMgrId());
			tcEcif.setManagerProp(ccir.getPercent());
			tcEcif.setStatusEcif("1");
			tcEcif.setTyCustId(ccir.getTyCustId());
		}
		else{
			CommonRqHdr commonRqHdr1 = new CommonRqHdr();
			commonRqHdr1.setChannelId("S89");
			commonRqHdr1.setRqUID(UUID.randomUUID().toString());
			commonRqHdr1.setTranDate(DateUtil.getCurrentDateAsString().replace("-", ""));
			commonRqHdr1.setTranTime(DateTimeUtil.getLocalTime().replace(":", ""));
			//查询非同业客户
			C167CoCorpCusInqRq request1=new C167CoCorpCusInqRq();
			request1.setCommonRqHdr(commonRqHdr1);
			if(map.get("custmerCode").toString().trim() != ""){
				//根据客户号查询
				request1.setCustomer(map.get("custmerCode").toString());
			}else{
				//根据  客户号  证件类型  证件号 查询 
				request1.setIdCode(map.get("idNumber").toString());
				request1.setPrsnLegalType(map.get("idType").toString());
			}
			C167CoCorpCusInqRs ccrs=socketClientService.EcifC167CoCorpCusInqRequest(request1);
			if(!("000000".equals(ccrs.getCommonRsHdr().getStatusCode()))){
				tcEcif.setManager(ccrs.getCommonRsHdr().getServerStatusCode());
				
			}
			List<AddrAppRec> list=ccrs.getAddrAppRec();
			if(list.size()>0){
				for (AddrAppRec addrAppRec : list) {
					//选取循环体中的第一个
					tcEcif.setStrent(ccrs.getStrentDoor());
					tcEcif.setSfCptyCountry(addrAppRec.getAddCountry());
					break;
				}
			}
			
			tcEcif.setGbstreet(ccrs.getGBStreet());
			tcEcif.setCustmerCode(ccrs.getCustomer());
			tcEcif.setCustmerCode1(ccrs.getCustomer());
			tcEcif.setCnName(ccrs.getCNName1());
			tcEcif.setGbName(ccrs.getGBName1());
			tcEcif.setCustType(ccrs.getCustType());
			tcEcif.setIdType(ccrs.getIdType());
			tcEcif.setIdNumber(ccrs.getIdNo());
			tcEcif.setInputDt("-");
			tcEcif.setBankType("-");
			
			List <AcctOfficerRec> lista = ccrs.getAcctOfficerRec();
			if(lista.size()>0){
				for (AcctOfficerRec acctOfficerRec : lista) {
					//选取循环体中的第一个
					tcEcif.setManager(acctOfficerRec.getAcctOfficer());
					tcEcif.setManagerProp(acctOfficerRec.getMainAeAlPer());
					break;
				}
			}
			tcEcif.setMerType(ccrs.getIndustry());
			tcEcif.setGbstreet(ccrs.getGBStreet());
			tcEcif.setStatusEcif("1");
			tcEcif.setTyCustId(ccrs.getTCustFlag());
		}
		return RetMsgHelper.ok(tcEcif);
	}
	
	/**
	 * 条件查询 客户经理 信息 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/selectManagerByParams")
	public RetMsg<PageInfo<EcifBosoCustomerManager>> selectManagerByParams(@RequestBody Map<String,Object> map){
		Page<EcifBosoCustomerManager> page = ecifBosoCustomerMService.selectByParamsService(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询 证件 信息 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/selectIdByparams")
	public RetMsg<PageInfo<EcifBosoIdentifier>> selectIdByparams(@RequestBody Map<String,Object> map){
		Page<EcifBosoIdentifier> page = ecifBosoIdentifService.selectbosIdByParams(map);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 条件查询 地址 信息 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAddressByParam")
	public RetMsg<PageInfo<EcifBosoAddress>> selectAddressByParam(@RequestBody Map<String,Object> map){
		Page<EcifBosoAddress> page = ecifBosoAddrService.selectAddrByParamsService(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTdCustEcifSettleList")
	public RetMsg<List<TdCustEcif>> getTdCustEcifSettleList(@RequestBody Map<String,Object> map){
		List<TdCustEcif> list=new ArrayList<TdCustEcif>();
		String [] custmerCodeArr=map.get("custmerCodeArr").toString().split(",");
		map.put("custmerCodeArr", custmerCodeArr);
		List<TdCustEcif> ecifList=tdCustEcifService.getTdCustEcifSettleList(map);
		//从页面获取参与类型
		/*String [] legent=map.get("legent").toString().split(",");
		for (String string : legent) {
			String [] custmerCode=string.split("@");
			for (TdCustEcif tdCustEcif : ecifList) {
				if(custmerCode[0].trim().equals(tdCustEcif.getCustmerCode().trim())){
					tdCustEcif.setIdType(custmerCode[1]);
					list.add(tdCustEcif);
				}
			}
		}*/
		
		//查询参与方类型与客户类型配对应关系
		String partyCodeArr=map.get("partyCodeArr").toString();
		String [] partyCode= partyCodeArr.split(",");
		Page<TcProductParticipant> pageParticipant=tcProductParticipantService.getProductParticipantList(map);
		TdCustEcif tdCustEcifnew=null;
		for (String string : partyCode) {
			String [] custmerCode= string.split("-");
			tdCustEcifnew=new TdCustEcif();
			TcProductParticipant tcProductParticipantnew=null;
			for (TdCustEcif tdCustEcif : ecifList) {
				if(tdCustEcif.getCustmerCode().trim().equals(custmerCode[1].trim())){
					tdCustEcifnew=tdCustEcif;
					break;
				}
			}
			for (TcProductParticipant tcProductParticipant : pageParticipant) {
				if(tcProductParticipant.getPartyId().trim().equals(custmerCode[0].split("_")[0].trim())){
					tcProductParticipantnew=tcProductParticipant;
					break;
				}
			}
			if(tdCustEcifnew!=null && tcProductParticipantnew!=null){
				tdCustEcifnew.setIdType(tcProductParticipantnew.getPartyName());
				list.add(tdCustEcifnew);
				/*
				System.out.println("tdCustEcifnew "+tdCustEcifnew.getIdType()+" "+tdCustEcifnew.getCustmerCode());
				for (TdCustEcif tdCustEcif : list) {
					System.out.println("tdCustEcif :"+tdCustEcif.getIdType()+" "+tdCustEcif.getCustmerCode());
				}*/
			}
		}
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/showAllIfsOpicsCust")
	public RetMsg<PageInfo<IfsOpicsCust>> showAllIfsOpicsCust(@RequestBody Map<String,Object> map){
		//条件查询  转化成ECIF对应条件
		map.put("custmerCode", ParameterUtil.getString(map,"party_shortname",""));
		map.put("cnName", ParameterUtil.getString(map,"party_name",""));
		Page<IfsOpicsCust> page = tdCustEcifService.showAllIfsOpicsCust(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询所有交易名称
	@ResponseBody
	@RequestMapping(value = "/searchAllProtectName")
	public RetMsg<PageInfo<TcProduct>> searchAllProtectName(@RequestBody Map<String,Object> map){
		//条件查询  转化成ECIF对应条件
		map.put("custmerCode", ParameterUtil.getString(map,"party_shortname",""));
		map.put("cnName", ParameterUtil.getString(map,"party_name",""));
		Page<TcProduct> page = tdCustEcifService.searchAllProtectName(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询单笔交易详情
	@ResponseBody
	@RequestMapping(value = "/searchSingleProtectName")
	public RetMsg<PageInfo<ProductWeight>> searchSingleProtectName(@RequestBody Map<String,Object> map){

		Page<ProductWeight> page = tdCustEcifService.searchSingleProtectName(map);
		return RetMsgHelper.ok(page);
	}
	
	//查询单笔交易详情
	@ResponseBody
	@RequestMapping(value = "/searchAllForeignProtectName")
	public RetMsg<PageInfo<ProductWeight>> searchAllForeignProtectName(@RequestBody Map<String,Object> map){
		Page<ProductWeight> page = tdCustEcifService.searchAllForeignProtectName(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 客户准入  导出Excel
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response){
		String ua = request.getHeader("User-Agent");
		String filename = "产品权重信息列表.xlsx"; // 文件名
		String encode_filename = "";
		try {
			encode_filename = URLEncoder.encode(filename, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			throw new RException(e1.getMessage());
		}
		response.setContentType("application/vnd.ms-excel");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
		}
		response.setHeader("Connection", "close");
		try {
			ExcelUtil e = downloadExcel();
			OutputStream ouputStream = response.getOutputStream();
			e.getWb().write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			throw new RException(e);
		}
		
	};
	
	/**
	 * 客户准入  Excel文件下载内容
	 */
	public ExcelUtil downloadExcel() throws Exception {
		ProductWeight bean = new ProductWeight();
		List<ProductWeight> list =  new ArrayList<ProductWeight>();
		Map<String,Object> map = new HashMap<String, Object>();
		list = tdCustEcifMapper.getProductList(map); // 查数据
		
		ExcelUtil e = null;
		try {
			// 表头
			e = new ExcelUtil(ExcelUtil.TYPE_2007_BIG_DATA,200);
			CellStyle center = e.getDefaultCenterStrCellStyle();
			
			int sheet = 0;
			int row = 0;
			
			// 表格sheet名称
			e.getWb().createSheet("产品权重详情列表");
			
			// 设置表头字段名
			e.writeStr(sheet,row, "A",center, "产品编号");     
			e.writeStr(sheet,row, "B",center, "产品名称"); 
			e.writeStr(sheet,row, "C",center, "规则最小值（天）"); 
			e.writeStr(sheet,row, "D",center, "是否包含规则最小值");       
			e.writeStr(sheet,row, "E",center, "规则最大值（天）");   
			e.writeStr(sheet,row, "F", center,"是否包含规则最大值");
			e.writeStr(sheet,row, "G",center, "权重值%");       
			e.writeStr(sheet,row, "H",center, "操作员");   
			e.writeStr(sheet,row, "I", center,"操作时间"); 
			
			//设置列宽
			e.getSheetAt(sheet).setColumnWidth(0, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(1, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(2, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(3, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(4, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(5, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(6, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(7, 20 * 256);
			e.getSheetAt(sheet).setColumnWidth(8, 20 * 256);
			
			for (int i = 0; i < list.size(); i++) {
				bean = list.get(i);
				row++;
				// 插入数据
				e.writeStr(sheet, row, "A", center, bean.getProductCode());
				e.writeStr(sheet, row, "B", center, bean.getProductName());
				e.writeStr(sheet, row, "C", center, bean.getRuleMin());
				String containsMin = bean.getContainsMin();
				if("1".equals(containsMin)){
					containsMin = "不包含";
				}else{
					containsMin = "包含";
				}
				e.writeStr(sheet, row, "D", center, containsMin);
				e.writeStr(sheet, row, "E", center, bean.getRuleMax());
				String containsMax = bean.getContainsMax();
				if("1".equals(containsMax)){
					containsMax = "不包含";
				}else{
					containsMax = "包含";
				}
				e.writeStr(sheet, row, "F", center, containsMax);
				e.writeStr(sheet, row, "G", center, bean.getWeight());
				e.writeStr(sheet, row, "H", center, bean.getOperator());
				e.writeStr(sheet, row, "I", center, bean.getDateTime());
				/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String date = sdf.format(bean.getAPPROVAL_DATE());*/
				
			}
			return e;
		} catch (Exception e1) {
			throw new RException(e1);
		}
	}
	
	/**
	 * 更新衍生品权重占用情况
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCustLoanAmt")
	public String updateCustLoanAmt(@RequestBody String clino) {
		String msg = "";
		try {
			
			String nowDate = "";
			String serial_no = null;
			String product_type = null;
			List<TdEdCustBatchStatus> allMDateDeal = new ArrayList<TdEdCustBatchStatus>();
			allMDateDeal= tdEdCustBatchStatusMapper.getNotTodayMDateDeal(nowDate);//获取到期日在今日之后的交易
			for(int i=0;i<allMDateDeal.size();i++){
				serial_no = allMDateDeal.get(i).getDealNo();//获得交易流水号
				product_type = allMDateDeal.get(i).getPrdNo();//获得交易编号
				if("391".equals(product_type)||"432".equals(product_type)||"441".equals(product_type)||"433".equals(product_type)){
					//391外汇远期,432外汇掉期,433人民币期权,441利率互换
					Map<String, String> paramMap = new HashMap<String, String>();
					paramMap.put("serial_no", serial_no);
					paramMap.put("product_type", product_type);
					
					//先释放再占用
					edCustManangeService.eduReleaseFlowService(serial_no);
					edCustManangeService.chooseOccupy(paramMap);
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return msg;
	}
	
	/**
	 * 更新衍生品权重占用情况
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCustBondAmt")
	public void updateCustBondAmt(@RequestBody String clino) {
		try {
			
			String serial_no = null;
			String product_type = null;
			
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("branchId", DictConstants.Branch.ID);
			List<IfsCfetsrmbCbt> ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchCfetsrmbCbtWithList(map);
			for(int i=0;i<ifsCfetsrmbCbt.size();i++){
				serial_no = ifsCfetsrmbCbt.get(i).getTicketId();//获得交易流水号
				product_type = "443";//交易编号，//443现劵买卖
				
				Map<String, String> paramMap = new HashMap<String, String>();
				paramMap.put("serial_no", serial_no);
				paramMap.put("product_type", product_type);
				
				//先释放再占用
				edCustManangeService.eduReleaseFlowService(serial_no);
				edCustManangeService.chooseOccupy(paramMap);
				//债券限额占用
				paramMap.put("code", "0");//债券限额
				
				String inFlag = "0";//作为判断标识符，为0时是审批台发起，为1为清算后发起
				edCustManangeService.limitBondExchange(paramMap,inFlag);
				
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}
	
	
}
