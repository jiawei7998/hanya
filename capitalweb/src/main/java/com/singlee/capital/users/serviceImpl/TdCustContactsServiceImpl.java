package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.users.mapper.TdCustContactsMapper;
import com.singlee.capital.users.model.TdCustContacts;
import com.singlee.capital.users.service.TdCustContactsService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustContactsServiceImpl implements TdCustContactsService {
	
	@Autowired
	private TdCustContactsMapper TcMapper;
	
	/**
	 * 汪泽峰 2017-5-18
	 * 客户联系人信息查看
	 */
	@Override
	public Page<TdCustContacts> showAllTdCustContactsService(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustContacts> result=TcMapper.showAllTdCustContacts(map, rowBounds);
		return result;
	}
	/**
	 * 汪泽峰 2017-5-22
	 * 新增客户联系人
	 */
	@Override
	public void insertTdCustContactsService(Map<String, Object> map) {
		TdCustContacts tdContacts=new TdCustContacts();
		ParentChildUtil.HashMapToClass(map, tdContacts);
		TcMapper.insert(tdContacts);
	}
	/**
	 * 汪泽峰 2017-5-22
	 * 客户联系人修改
	 */
	@Override
	public void updateTdCustContactsService(Map<String, Object> map) {
		TcMapper.updateTdCustContacts(map);
	}
	/**
	 * 汪泽峰 2017-5-22
	 * 删除客户联系人信息
	 * 
	 */
	@Override
	public void deleteTdCustContactsService(Map<String, Object> map) {
		TdCustContacts tdContacts=new TdCustContacts();
		ParentChildUtil.HashMapToClass(map, tdContacts);
		TcMapper.delete(tdContacts);
	}

}
