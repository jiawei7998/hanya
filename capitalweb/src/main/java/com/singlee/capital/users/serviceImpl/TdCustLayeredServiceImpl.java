package com.singlee.capital.users.serviceImpl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.users.mapper.TdCustLayeredMapper;
import com.singlee.capital.users.model.TdCustLayered;
import com.singlee.capital.users.service.TdCustLayeredService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdCustLayeredServiceImpl implements TdCustLayeredService {
	@Autowired
	private TdCustLayeredMapper TclMapper;
	
	/**
	 * 汪泽峰 2017-5-18
	 * 客户分层信息
	 */
	@Override
	public Page<TdCustLayered> showAllTdCustLayered(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdCustLayered> result=TclMapper.showAllTdCustLayered(map, rowBounds);
		return result;
	}

	@Override
	public void updateTdCustLayeredService(Map<String, Object> map) {
		map.put("version",Integer.parseInt(map.get("version").toString()) -1);
		map.put("isActive","0");
		TclMapper.updateTdCustLayered(map);
	}

	@Override
	public void insertTdCustLayeredService(Map<String, Object> map) {
		int vers=Integer.parseInt(map.get("version").toString()); 
		map.put("version",vers+1+"");
		TclMapper.insertTdCustLayered(map);
		
	}

}
