package com.singlee.capital.users.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.users.service.ExcelImportService;
@Service("excelImportService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ExcelImportServiceImpl implements ExcelImportService{
	
	
	
	@Override
	public List<Map<String, Object>> readExcel(Workbook wb, String sheetName) {
		StringBuffer sb = new StringBuffer();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			Sheet sheet1 = wb.getSheet(sheetName);
			if (sheet1 == null) {
				sb.append("无名字为'" + sheetName +"'的sheet！");
				return list;
			}
			for(int r = 1; r < sheet1.getRows(); r++){
				@SuppressWarnings("unused")
				String title = getCellContent(sheet1, r, 0);
				Map<String, Object> item = new HashMap<String, Object>();
				item.put("seq", r);
				for (int c = 0; c < sheet1.getColumns(); c++) {
					String value = getCellContent(sheet1, r, c);
					if(c==0){
						item.put("dealNo", value);
					}else if(c==1){
						item.put("prdName", value);
					}else if(c==2){
						item.put("partyName",value);
					}else if(c==3){
						item.put("tradeCredit", value);
					}else if(c==4){
						item.put("procAmt", value);
					}else if(c==5){
						item.put("vDate", value);
					}else if(c==6){
						item.put("mDate", value);
					}else if(c==7){
						item.put("partyName",value);
					}else if(c==8){
						item.put("tradeCredit", value);
					}else if(c==9){
						item.put("classifyAdvice", value);
					}else if(c==10){
						item.put("impairmentResults", value);
					}else if(c==11){
						item.put("assignee", value);
					}else if(c==12){
						item.put("approve", value);
					}
					
				}
				list.add(item);
			}
		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return list;
	}

	/**
     * 获取sheet单元格内容
	 * 取sheet中r行c列的内容
	 * @param sheet
	 * @param r
	 * @param c
	 * @return r行c列的内容
	 * @author wzf
	 */
	private String getCellContent(Sheet sheet, int r, int c) {
		String a = "";
		int rowLength = sheet.getRows();
		// 传入的r数大于总行数
		if (r >= rowLength) {
			return "";
		}
		Cell[] row = sheet.getRow(r);
		try {
			if (row != null) {
				if (row.length <= c) {
					// a = null;
				} else {
					if(row[c].getType() == CellType.DATE){
					   DateCell dc = (DateCell)row[c];
					   Date date = dc.getDate();
					   SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd");
					    a = ds.format(date);
					}else{
						a = row[c].getContents();
					}
					
				}
			} else {
				// a = null;
			}
		} catch (Exception e) {
			JY.error(e);
			// System.out.println(r+"  "+c);
		}
		return a;
	}


}
