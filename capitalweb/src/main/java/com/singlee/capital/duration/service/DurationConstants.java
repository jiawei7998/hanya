package com.singlee.capital.duration.service;

import com.singlee.capital.system.dict.DictConstants;

import java.util.HashMap;
import java.util.Map;
/**
 * 存续期变量控制器
 * @author SINGLEE
 *
 */
public class DurationConstants {
	/**
	 * 每日计提利息是否结转
	 * @author SINGLEE
	 *
	 */
	public final static class INTEREST_TYPE{
	  public final static String  REC_TYPE_YES = "1";
	  public final static String  REC_TYPE_NO = "2";
	 }
	/**
	 * formStatus
	 * @author SINGLEE
	 */
	public final static class FORM_STATUS{
	  public final static String  HIDDEN = "hidden";//隐藏
	  public final static String  WRITABLE = "writable";//写
	 }
	public final static class RateType{
	    /**利息*/
	  public final static String  ACCRU_TYPE = "1";
	    /**中收 */
	  public final static String  CHARGE_TYPE = "0";
	 }
	
	public final static class AccType{//TD_ACC_TRD_DAILY ACC_TYPE类型
		public final static String  INTEREST_FEE_NORMAL ="interestF_Fee_Normal";//费用每日计提
		public final static String  INTEREST_FEE_NORMAL_ADJ ="interestF_Fee_Normal_Adj";//费用每日计提
		public final static String  INTEREST_FEE_OVERDUE="interestOF_Fee_Overdue";//逾期费用计提冲销
		
		public final static String  INTEREST_NORMAL ="interest_Normal";//正常每日计提
		public final static String  INTEREST_NORMAL_ADJ ="interest_Normal_Adj";//计提调整
		public final static String  INTEREST_OVERDUE="interestO_Overdue";//逾期每日计提
		public final static String  INTEREST_ADJBYRATECHANGE="adj_ByRateChage_Interest";//利率变更引起的调整
		public final static String  INTEREST_ADJBYOVERDUE="adj_Overdue_Interest";//逾期引起的利率调整
		public final static String  INTEREST_AMORDAILY = "amor_Interest";//每日摊销
		public final static String  INTEREST_AMORDAILY_ADJ = "amor_Interest_Adj";//摊销调整
		public final static String  INTEREST_FEE_AMORDAILY = "amorF_Fee_Interest";//费用每日摊销
		public final static String  INTEREST_FEE_AMORDAILY_ADJ = "amorF_Fee_Interest_Adj";//费用每日摊销
		
		public final static String  AM_AMT = "am_Amt";//提前还本的本金(元)
		public final static String  AM_INT = "am_Int";//提前还本的利息(元)
		public final static String  AM_PENALTY = "am_Penalty";//提前还款违约金(元)
		public final static String  AM_RETURN_IAMT = "am_ReturnIamt";//先收息前提下的归还利息(元)
		public final static String  AM_INT_ADJ = "am_IntAdj";//应收息调整-提前到期
		public final static String  AM_AMOR_ADJ = "am_AmorAdj";//摊销调整
		//基金相关金额类型定义
		public final static String APPLY_FUND_AMT="apply_fund_amt";//基金申购金额
		public final static String APPCASHDIV_FUND_AMT="appcashdiv_fund_amt";//宣告日审批完成分红金额
		public final static String CASHDIV_FUND_AMT="cashdiv_fund_amt";//基金分红金额
		public final static String REDEEM_FUND_AMT="redeem_fund_amt";//基金赎回金额
		public final static String REDEEM_FUND_AMTDIV="redeem_fund_amtDiv";//赎回确认红利金额
		public final static String REINVEST_FUND_AMT="reinvest_fund_amt";//红利再投金额
		public final static String REINVEST_FUND_AMTTPOS="reinvest_fund_amtTpos";//红利再投基金持仓金额
		public final static String TRANS_FUND_AMT="trans_fund_amt";//基金转换金额
		public final static String APPLY_FUND_CASHDIV_AMT="apply_fund_cashDiv_amt";//基金申购时含的未结转红利金额
		
		//正常放款金额
		public final static String LOAN_AMT = "loan_Amt";//正常放款本金
		public final static String LOAN_PROCAMT = "loan_ProcAmt";//实际放款金额
		public final static String LOAN_DISAMT = "loan_DisAmt";//正常放款本金-实际放款金额 部分
		public final static String REV_PROCAMT = "rev_ProcAmt";//实际收款金额
		public final static String INTEREST_AMOR = "interest_Amor";//应收利息
		public final static String DIS_INTAMT= "dis_intAmt";//利息收入
		public final static String REVAL_AMT ="reval_Amt";//估值金额
		//费用
		public final static String FEE_ALONEFEERATE = "fee_alonefeerate";//独立监督费
		public final static String FEE_SERVICERATE = "fee_servicerate";//服务费
		public final static String FEE_PROXYRATE = "fee_proxyrate";//代理咨询费
		public final static String FEE_CUSTODFEERATE = "fee_custodfeerate";//托管费
		public final static String FEE_CHARGERATE = "fee_chargerate";//中收收益
		public final static String FEE_FEERATE = "fee_feerate";//杂费
		public final static String FEE_CUSTODFEERATE2 = "fee_custodfeerate2";//托管费2
		public final static String FEE_OTHERFEERATE = "fee_otherfeerate";//其他费
		public final static String FEE_FINADVFEERATE = "fee_finadvfeerate";//财务顾问费
		public final static String FEE_SUPERVISIONRATE = "fee_Supervisionrate";//监督费

		public final static String FEE_AMT_INTADJ = "fee_amt_IntAdj";//费用调整
		
		//违约金
		public final static String LIQUIDATED_DAMAGES_AMT = "liquidated_damages_amt";//违约金
		
		public final static String  AMT_PRINCIPAL = "amt_Principal";//本金
		public final static String  AMT_INTEREST = "amt_Int";//利息
		//逾期罚息
		public final static String  AMT_INTEREST_OD_LATECHARGE = "amt_IntOd_latecharge";//逾期罚息
		public final static String  AMT_PRINCIPAL_OD = "amt_PrincipalOd";//逾期本金
		public final static String  AMT_INTEREST_OD = "amt_IntOd";//逾期利息
		public final static String  AMT_INTEREST_ADJ = "amt_IntAdj";//利息调整
		public final static String  AMT_INTEREST_ADJ_OD = "amt_IntAdjOd";//逾期利息调整
		public final static String  AMT_INTEREST_INTADJ = "amt_Int_Adj";//到期还本收息少收的利息调整
		public final static String  AMT_INTEREST_INTADJ_OD = "amt_Int_AdjOd";//逾期收息少收的利息调整
		//手续费
		public final static String FEE = "fee";
		//申购确认金额
		public final static String LOAN_VAMT ="loan_Vamt";
		//红利再投金额
		public final static String  LOAN_DRT = "loan_Drt";//利息
		//分红金额
		public final static String  LOAN_BOND = "loan_Bond";//利息
		//结息
		public final static String  SETTL_INT = "settl_Int";//结息金额
		//结息确认
		public final static String  CONFIRM_SETTL_INT = "confirm_settl_Int";//结息确认利息
		public final static String  CONFIRM_SETTL_INTADJ = "confirm_settl_IntAdj";//结息确认利息调整
		//收息
		public final static String  INCOME_INT = "income_Int";//利息
		public final static String  INCOME_INTADJ = "income_IntAdj";//利息调整
		
		public final static String  TRANSFOR_INT = "transfor_Int";//迁移利息
		
		public final static String  MTM_VAL = "mtm_val";//公允价值
		public final static String  REVMTM_VAL = "revmtm_val";//冲销前一日

		//剩余成本
		public final static String  REMAIN_AMT = "remain_Amt";//利息

		//估值
		public final static String  LOAN_ASSMT = "loan_assmt";//利息
		//上一日估值
		public final static String  LOAN_ASSMT_LAST = "loan_assmt_last";//利息
	}
	
	public static Map<String, String> dealFeeTypeMap =new HashMap<String,String>();
	static{
		dealFeeTypeMap.put("aloneFeerate", DurationConstants.AccType.FEE_ALONEFEERATE);
		dealFeeTypeMap.put("serviceRate", DurationConstants.AccType.FEE_SERVICERATE);
		dealFeeTypeMap.put("proxyRate", DurationConstants.AccType.FEE_PROXYRATE);
		dealFeeTypeMap.put("custodFeerate", DurationConstants.AccType.FEE_CUSTODFEERATE);
		dealFeeTypeMap.put("chargeRate", DurationConstants.AccType.FEE_CHARGERATE);
		dealFeeTypeMap.put("feeRate", DurationConstants.AccType.FEE_FEERATE);
		dealFeeTypeMap.put("custodFeerate2", DurationConstants.AccType.FEE_CUSTODFEERATE2);
		dealFeeTypeMap.put("otherFeerate", DurationConstants.AccType.FEE_OTHERFEERATE);
		dealFeeTypeMap.put("finAdvFeerate", DurationConstants.AccType.FEE_FINADVFEERATE);
		dealFeeTypeMap.put("supervisionRate", DurationConstants.AccType.FEE_SUPERVISIONRATE);
	}
	public static Map<String,String> beanNameMap=new HashMap<String,String>();
	static{
		beanNameMap.put(DictConstants.TrdType.CustomPassageWay, "tdFeesPassAgewayServiceImpl");//通道计划
		beanNameMap.put(DictConstants.TrdType.CustomTradeExpire, "tradeExpireServiceImpl");//资产到期
		beanNameMap.put(DictConstants.TrdType.AccountTradeOffset, "tradeOffsetServiceImpl");//交易冲正
		beanNameMap.put(DictConstants.TrdType.CustomRateChange, "rateChangeServiceImpl");//利率变更
		beanNameMap.put(DictConstants.TrdType.CustomRateReset, "rateResetServiceImpl");//利率重订
		beanNameMap.put(DictConstants.TrdType.CustomDealEnd, "TdBalanceServiceImpl");//交易结清
		
		beanNameMap.put(DictConstants.TrdType.CustomAdvanceMaturity, "advanceMaturityServiceImpl");//提前还款
		beanNameMap.put(DictConstants.TrdType.Custom, "tradeCustomServiceImpl");//正式交易
		beanNameMap.put(DictConstants.TrdType.AbssetForEnterpriceCredit, "tradeCustomServiceImpl");//融贷通
		beanNameMap.put(DictConstants.TrdType.LiabilitiesDepositRegular, "tradeCustomServiceImpl");//正式交易(负债)
		beanNameMap.put(DictConstants.TrdType.CustomTradeExtend, "tradeExtendServiceImpl");//到期展期
		beanNameMap.put(DictConstants.TrdType.CustomOutRightSale, "tradeOutrightSaleServiceImpl");//资产卖断（单笔）
		beanNameMap.put(DictConstants.TrdType.CustomTradeOverDue, "tdOverDueConfirmServiceImpl");//交易逾期
		beanNameMap.put(DictConstants.TrdType.CustomCashFlowChange, "transforScheduleServiceImpl");//本金计划调整
		beanNameMap.put(DictConstants.TrdType.CustomCashFlowChangeInt, "transforScheduleIntServiceImpl");//收息计划调整
		beanNameMap.put(DictConstants.TrdType.CustomCashFlowChangeFee, "transforScheduleFeeServiceImpl");//费用计划调整
		beanNameMap.put(DictConstants.TrdType.CustomInvFiIpTransFor, "tdInvFiipApproveServiceImpl");//投资占比调整
		beanNameMap.put(DictConstants.TrdType.ApproveCancel, "approveCancelServiceImpl");//撤销交易
		beanNameMap.put(DictConstants.TrdType.ApproveOverDraft, "tradeCustomServiceImpl");//同业透支
		beanNameMap.put(DictConstants.TrdType.CustomInterestSettleConfirm, "interestSettleServiceImpl");//活期结息确认
		beanNameMap.put(DictConstants.TrdType.CustomAdvanceMaturityCurrent, "advanceMaturityCurrentServiceImpl");//活期提前支取
		beanNameMap.put(DictConstants.TrdType.FeeCashFlowHandle_SINGLE, "tdFeeHandleCashflowServiceImpl");//费用补录-单例
	}
	public static String getBeanFromTrdType(String trdType){
		String beanName = beanNameMap.get(trdType);
		return beanName;
	}
	
	/**
	 * 用来初始化 不同存续期事件对应的jsp路径
	 */
	public static Map<String, String> durationJSPMap =  new HashMap<String, String>();
	static{
		durationJSPMap.put("TD_CASHFLOW_FEE", "./new/cashflow/feeCashFlowEdit.jsp");//费用
		durationJSPMap.put("TD_CASHFLOW_INTEREST", "./new/cashflow/IntCashFlowEdit.jsp");//利息
		durationJSPMap.put("TD_CASHFLOW_CAPITAL", "./new/cashflow/CapitalCashFlowEdit.jsp");//本金
		durationJSPMap.put("TD_CASHFLOW_AMOR", "./new/cashflow/AmorCashFlow.jsp");//摊销
		durationJSPMap.put("TD_TRADE_EXTEND", "./duration/query/QueryTradeExtendList.jsp");//展期
		durationJSPMap.put("TD_ADVANCE_MATURITY", "./duration/query/QueryAdvanceMaturityList.jsp");//提前到期
		durationJSPMap.put("TD_OVERDUE_CONFIRM", "./duration/query/QueryOverdueConfirmList.jsp");//逾期
		durationJSPMap.put("TD_TRASFOR_SCHEDULE", "");//还本计划变更
		durationJSPMap.put("TD_TRASFOR_SCHEDULE_INT", "");//收息计划变更
		durationJSPMap.put("TD_TRASFOR_SCHEDULE_FEE", "");//费用计划变更
		durationJSPMap.put("TD_OUTRIGHT_SALE", "");//卖断
		durationJSPMap.put("TT_RATE_RANGE", "./new/cashflow/RateRangeDetail.jsp");//利率区间
		durationJSPMap.put("TT_AMT_RANGE", "./new/cashflow/AmtRangeDetail.jsp");//真实还本现金流
		durationJSPMap.put("TD_RATE_CHANGE", "./duration/query/QueryRateChangeList.jsp");//利率变更
		durationJSPMap.put("TBK_ENTRY", "./duration/query/QueryTbkEntryList.jsp");//会计分录
	}
}
