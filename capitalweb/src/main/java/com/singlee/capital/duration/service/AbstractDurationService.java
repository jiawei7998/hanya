package com.singlee.capital.duration.service;

import java.util.Map;

import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * 存续期必须实现该类
 * 用于获取存续期交易对应的主交易
 * @author SINGLEE
 *
 */
public abstract class AbstractDurationService {
	/**
	 * 定义查询存续期关联的原始正式交易
	 * @param params
	 * @return
	 */
	public abstract TdProductApproveMain getProductApproveMain(Map<String, Object> params);
	/**
	 * 存续期完成时需要添加原交易事件（记录）
	 * @param trdEvent
	 */
	public abstract void saveTrdEvent(Map<String, Object> params);
	
	/**
	 * 存续期审批结束后需要操作的动作
	 * @param params
	 */
	public abstract void saveCompleteDuration(Map<String, Object> params);
	
}
