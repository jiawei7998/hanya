package com.singlee.capital.external.jc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.joyard.jc.dataservice.MarketDS;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.market.model.TtMktBondValuing;
import com.singlee.capital.market.service.MktBondValuingService;

/**
 * 
 * @author LyonChen
 * 
 */
@Component
public class MarketDSImpl implements MarketDS {

	@Autowired
	private MktBondValuingService mktBondValuingService;
	
	@Override
	public double latestBondCNBD(InstrumentId inst, String base) {
		TtMktBondValuing val = mktBondValuingService.latestMktBondValuing(inst.getI_code(), 
				inst.getA_type(),inst.getM_type(), base);
		return val==null?Double.NaN:val.getFull_price();
	}

}
