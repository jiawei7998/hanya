package com.singlee.capital.external.jc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.joyard.jc.dataservice.CurveDS;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.market.mapper.CurveDao;

/**
 * 
 * @author LyonChen
 * 
 */

@Component
public class CurveDSImpl implements CurveDS {
	@Autowired
	private CurveDao curveDao;
	/**
	 * 获得 指定曲线代码，指定日期 的曲线数据
	 * 
	 * @param code曲线代码
	 * @param refDate
	 *            基准日期
	 * @return 期限，曲线值的 list
	 */
	@Override
	public List<Pair<Double, Double>> queryCurve(String code, String refDate) {
		Map<String, String> map = new HashMap<String,String>();
		map.put("irc_code", code);
		map.put("beg_date", refDate);
		return curveDao.loadNoRiskCurveRate(map);
		
//		
//		List<Pair<Double, Double>> list = new ArrayList<Pair<Double, Double>>();
//		list.add(new Pair<Double, Double>(0.0, 0.018905));
//		list.add(new Pair<Double, Double>(0.08, 0.024672));
//		list.add(new Pair<Double, Double>(0.1, 0.026103));
//		list.add(new Pair<Double, Double>(0.17, 0.030425));
//		list.add(new Pair<Double, Double>(0.2, 0.031571));
//		list.add(new Pair<Double, Double>(0.25, 0.032087));
//		list.add(new Pair<Double, Double>(0.3, 0.032788));
//		list.add(new Pair<Double, Double>(0.4, 0.033643));
//		list.add(new Pair<Double, Double>(0.5, 0.034114));
//		list.add(new Pair<Double, Double>(0.6, 0.034612));
//		list.add(new Pair<Double, Double>(0.7, 0.035054));
//		list.add(new Pair<Double, Double>(0.75, 0.035223));
//		list.add(new Pair<Double, Double>(0.8, 0.035404));
//		list.add(new Pair<Double, Double>(0.9, 0.035672));
//		list.add(new Pair<Double, Double>(1.0, 0.035782));
//		list.add(new Pair<Double, Double>(1.1, 0.035868));
//		list.add(new Pair<Double, Double>(1.2, 0.035936));
//		list.add(new Pair<Double, Double>(1.3, 0.03599));
//		list.add(new Pair<Double, Double>(1.4, 0.03603));
//		list.add(new Pair<Double, Double>(1.5, 0.036059));
//		list.add(new Pair<Double, Double>(1.6, 0.036079));
//		list.add(new Pair<Double, Double>(1.7, 0.036091));
//		list.add(new Pair<Double, Double>(1.8, 0.036097));
//		list.add(new Pair<Double, Double>(1.9, 0.0361));
//		list.add(new Pair<Double, Double>(2.0, 0.0361));
//		list.add(new Pair<Double, Double>(2.1, 0.0361));
//		list.add(new Pair<Double, Double>(2.2, 0.036099));
//		list.add(new Pair<Double, Double>(2.3, 0.036097));
//		list.add(new Pair<Double, Double>(2.4, 0.036096));
//		list.add(new Pair<Double, Double>(2.5, 0.036094));
//		list.add(new Pair<Double, Double>(2.6, 0.036093));
//		list.add(new Pair<Double, Double>(2.7, 0.036092));
//		list.add(new Pair<Double, Double>(2.8, 0.036091));
//		list.add(new Pair<Double, Double>(2.9, 0.036103));
//		list.add(new Pair<Double, Double>(3.0, 0.036138));
//		list.add(new Pair<Double, Double>(3.1, 0.036196));
//		list.add(new Pair<Double, Double>(3.2, 0.036272));
//		list.add(new Pair<Double, Double>(3.3, 0.036364));
//		list.add(new Pair<Double, Double>(3.4, 0.03647));
//		list.add(new Pair<Double, Double>(3.5, 0.036586));
//		list.add(new Pair<Double, Double>(3.6, 0.036711));
//		list.add(new Pair<Double, Double>(3.7, 0.036841));
//		list.add(new Pair<Double, Double>(3.8, 0.036975));
//		list.add(new Pair<Double, Double>(3.9, 0.037108));
//		list.add(new Pair<Double, Double>(4.0, 0.037239));
//		list.add(new Pair<Double, Double>(4.1, 0.037365));
//		list.add(new Pair<Double, Double>(4.2, 0.037483));
//		list.add(new Pair<Double, Double>(4.3, 0.03759));
//		list.add(new Pair<Double, Double>(4.4, 0.03768));
//		list.add(new Pair<Double, Double>(4.5, 0.037752));
//		list.add(new Pair<Double, Double>(4.6, 0.037812));
//		list.add(new Pair<Double, Double>(4.7, 0.037869));
//		list.add(new Pair<Double, Double>(4.8, 0.037929));
//		list.add(new Pair<Double, Double>(4.9, 0.037994));
//		list.add(new Pair<Double, Double>(5.0, 0.038056));
//		list.add(new Pair<Double, Double>(5.1, 0.038113));
//		list.add(new Pair<Double, Double>(5.2, 0.038167));
//		list.add(new Pair<Double, Double>(5.3, 0.038217));
//		list.add(new Pair<Double, Double>(5.4, 0.038265));
//		list.add(new Pair<Double, Double>(5.5, 0.03831));
//		list.add(new Pair<Double, Double>(5.6, 0.038354));
//		list.add(new Pair<Double, Double>(5.7, 0.038397));
//		list.add(new Pair<Double, Double>(5.8, 0.038439));
//		list.add(new Pair<Double, Double>(5.9, 0.03848));
//		list.add(new Pair<Double, Double>(6.0, 0.038522));
//		list.add(new Pair<Double, Double>(6.1, 0.038565));
//		list.add(new Pair<Double, Double>(6.2, 0.038609));
//		list.add(new Pair<Double, Double>(6.3, 0.038654));
//		list.add(new Pair<Double, Double>(6.4, 0.038689));
//		list.add(new Pair<Double, Double>(6.5, 0.038711));
//		list.add(new Pair<Double, Double>(6.6, 0.038723));
//		return list;
	}

}
