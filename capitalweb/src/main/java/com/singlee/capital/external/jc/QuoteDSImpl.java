package com.singlee.capital.external.jc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.joyard.jc.dataservice.QuoteDS;
import com.joyard.jc.time.Date;
import com.joyard.jc.time.DateSeries;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.market.mapper.IrDao;
import com.singlee.capital.market.model.TtMktIrSeries;

/**
 * 
 * @author LyonChen
 * 
 */

@Component 
public class QuoteDSImpl implements QuoteDS {

	
	@Autowired
	private IrDao irDao; 
	/**
	 * 返回一年期定存的 利率序列 
	 */
	@Override
	public DateSeries<Double> queryQuote(String begin, String end,
			InstrumentId id) {
		Map<String, String> map = new HashMap<String,String>();
		map.put("i_code", id.getI_code());
		map.put("a_type", id.getA_type());
		map.put("m_type", id.getM_type());
		map.put("beg_date", begin);
		map.put("end_date", end);
		
		List<TtMktIrSeries> list = irDao.loadSeries(map);
		DateSeries<Double> ds = new DateSeries<Double>();
		for(TtMktIrSeries ir: list){
			ds.put(new Date(ir.getBeg_date()), ir.getS_close());
		}
		return ds;
	}
	@Override
	public List<TtMktIrSeries> queryQuote(String fixingDate, InstrumentId id) {
		Map<String, String> map = new HashMap<String,String>();
		map.put("i_code", id.getI_code());
		map.put("a_type", id.getA_type());
		map.put("m_type", id.getM_type());
		map.put("key_date", fixingDate);
		return irDao.selectSeriesByDate(map);
	}

}
