package com.singlee.capital.external.jc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.joyard.jc.dataservice.CalendarDS;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.market.mapper.CalendarDao;
import com.singlee.capital.market.model.TtCalendar;
import com.singlee.capital.market.model.TtCalendarDate;

/**
 * 
 * @author LyonChen
 * 
 */
@Component
public class CalendarDSImpl implements CalendarDS {

	/**
	 * 
	 */
	@Autowired
	private CalendarDao calMapper;

	@Override
	public List<String> queryCalendarCode() {
		List<String> ret = new ArrayList<String>();
		List<TtCalendar> list = calMapper.loadCalendar();
		for(TtCalendar cal : list){
			ret.add(cal.getCal_code());
		}
		//ret.add(CalendarService.Market_CHINA_IB);
		//ret.add(CalendarService.Market_CHINA_EX);
		return ret ;
	}

	@Override
	public List<String> loadCalendarDate(String code) {
		List<String> ret = new ArrayList<String>();
		TtCalendarDate cal = new TtCalendarDate();
		cal.setCal_code(code);
		cal.setCal_flag("0"); //只关注节假日
		List<TtCalendarDate> list = calMapper.loadCalendarDate(cal);
		for(TtCalendarDate cd : list){
			ret.add(cd.getCal_date());
		}
		return ret ;
	}

	@Override
	public void mergeCalendarDate(List<Pair<String, String>> dateList) {
		calMapper.mergeCalendarDate(dateList);
	}
	
	
	
}

