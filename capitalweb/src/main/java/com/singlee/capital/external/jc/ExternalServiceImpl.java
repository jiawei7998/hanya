package com.singlee.capital.external.jc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joyard.jc.mapper.VtInstrumentMapper;
import com.joyard.jc.pojo.RepoFpMLVo;
import com.joyard.jc.service.ExternalService;
import com.singlee.capital.common.pojo.InstrumentId;
import com.singlee.capital.common.pojo.InstrumentIdGeneric;

@Service("externalService")
public class ExternalServiceImpl implements ExternalService {
	
	
	@Autowired
	private VtInstrumentMapper vm;
	@Override
	public InstrumentIdGeneric<String> getInstrumentIdFpML(InstrumentId id) {
		InstrumentIdGeneric<String> i = new InstrumentIdGeneric<String>(id);
		i.setT(vm.getFpml(id.getI_code(),id.getA_type(), id.getM_type()));
		return i;
	}

	@Override
	public RepoFpMLVo getRepoFpMLVo(InstrumentId id) {
		return null;
	}

	@Override
	public double getValuationCNBD(InstrumentId id, String date) {
		return 0;
	}

	@Override
	public double getValuationNPCNBD(InstrumentId id, String date) {
		return 0;
	}

}
