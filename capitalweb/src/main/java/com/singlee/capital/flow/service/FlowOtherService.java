package com.singlee.capital.flow.service;

import java.util.HashMap;

public interface FlowOtherService {
	
	/**
	 * 个人审批操作
	 * @param userId					用户ID
	 * @param taskId					任务ID
	 * @param completeType				类型   【-1:不通过  0：继续    1：通过 】
	 * @param specific_user 			指定用户
	 * @param msg						备注信息
	 * @param back_to					回退的目标节点
	 * @param data						保存的数据 HashMap<String,Object>
	 */
	public void completeAndSave(String user_id, String task_id, String op_type, String specific_user, String msg, String back_to, HashMap<String,Object> data) throws Exception;
}
