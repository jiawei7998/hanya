package com.singlee.capital.flow.service.impl;

import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.flow.service.FlowOtherService;
import com.singlee.capital.fund.mapper.*;
import com.singlee.capital.fund.model.*;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductCustCreditMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.slbpm.pojo.bo.TdFlowTaskEvent;
import com.singlee.slbpm.service.FlowOpService;
import com.singlee.slbpm.service.FlowQueryService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowOtherServiceImpl implements FlowOtherService {

	/** 审批流程操作接口 */
	@Autowired
	private FlowOpService flowOpService;

	/** 审批流程查询接口 */
	@Autowired
	private FlowQueryService flowQueryService;

	@Autowired
	private ProductService productService;

	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	private ProductApproveFundMapper ProductApproveFundMapper;
	@Autowired
	private TdProductCustCreditMapper productCustCreditMapper;
	@Autowired
	private TdDurationFundCashdivMapper tdDurationFundCashdivMapper;
	@Autowired
	private TdDurationFundRedeemMapper tdDurationFundRedeemMapper;
	@Autowired
	private TdDurationFundReinvestMapper tdDurationFundReinvestMapper;
	@Autowired
	private TdDurationFundTransMapper tdDurationFundTransMapper;

	/**
	 * 个人审批操作&&更新交易信息
	 * 
	 * @param userId        用户ID
	 * @param taskId        任务ID
	 * @param completeType  类型 【-1:不通过 0：继续 1：通过 】
	 * @param specific_user 指定用户
	 * @param msg           备注信息
	 * @param back_to       回退的目标节点
	 * @param data          保存的数据 HashMap<String,Object>
	 */
	@Override
	public void completeAndSave(String user_id, String task_id, String op_type, String specific_user, String msg,
			String back_to, HashMap<String, Object> data) throws Exception {
		// 注意：参数user_id为当前登陆用户；data内的userId为该笔业务的提交人。两者不同
		String prdNo = ParameterUtil.getString(data, "prdNo", "");
		// 用于区分基金业务，其他业务空置即可
		String ftype = ParameterUtil.getString(data, "ftype", "");

		if ("1".equals(ftype)) {
			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 查询产品详细信息
			Map<String, Object> fundParam = new HashMap<String, Object>();
			fundParam.put("dealNo", ParameterUtil.getString(data, "dealNo", ""));
			ProductApproveFund fund = ProductApproveFundMapper.getProductApproveFund(fundParam);
			ProductApproveFund approveFund = new ProductApproveFund();
			BeanUtils.populate(approveFund, data);
			if (fund != null) {
				BeanUtil.copyNotEmptyProperties(fund, approveFund);
			}
			// 刷新
			ProductApproveFundMapper.updateByPrimaryKey(fund);
			// 执行审批操作
			flowOpService.completePersonalTask(user_id, task_id, null,op_type, specific_user, msg, back_to);

		} else if ("2".equals(ftype)) {
			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 查询产品详细信息

			TdDurationFundCashdiv param = new TdDurationFundCashdiv();
			param.setDealNo(ParameterUtil.getString(data, "dealNo", ""));
			TdDurationFundCashdiv cashdiv = tdDurationFundCashdivMapper.getTdDurationFundCashdivByDealNo(param);
			TdDurationFundCashdiv fundCashdiv = new TdDurationFundCashdiv();
			BeanUtils.populate(fundCashdiv, data);
			if (cashdiv != null) {
				BeanUtil.copyNotEmptyProperties(cashdiv, fundCashdiv);
			}
			cashdiv.setCfType(DictConstants.TrdType.FundCashDiv);
			// 刷新
			tdDurationFundCashdivMapper.updateByPrimaryKey(cashdiv);
			// 执行审批操作
			flowOpService.completePersonalTask(user_id, task_id, null,op_type, specific_user, msg, back_to);
		} else if ("3".equals(ftype)) {
			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 查询产品详细信息
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("dealNo", ParameterUtil.getString(data, "dealNo", ""));
			TdDurationFundRedeem cashdiv = tdDurationFundRedeemMapper.getTdDurationFundRedeemByDealNo(param);
			TdDurationFundRedeem fundRedeem = new TdDurationFundRedeem();
			if (data.containsKey("redeemPrice") && "".equals(data.get("redeemPrice"))) {
				data.put("redeemPrice", 0);
			}
			BeanUtils.populate(fundRedeem, data);
			if (cashdiv != null) {
				BeanUtil.copyNotEmptyProperties(cashdiv, fundRedeem);
			}
			cashdiv.setCfType(DictConstants.TrdType.FundRedeem);
			// 刷新
			tdDurationFundRedeemMapper.updateByPrimaryKey(cashdiv);

			// 执行审批操作
			flowOpService.completePersonalTask(user_id, task_id, null,op_type, specific_user, msg, back_to);

		} else if ("4".equals(ftype)) {
			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 查询产品详细信息
			TdDurationFundReinvest param = new TdDurationFundReinvest();
			param.setDealNo(ParameterUtil.getString(data, "dealNo", ""));
			TdDurationFundReinvest cashdiv = tdDurationFundReinvestMapper.getTdDurationFundReinvestByDealNo(param);
			TdDurationFundReinvest fundReinvest = new TdDurationFundReinvest();
			BeanUtils.populate(fundReinvest, data);
			if (cashdiv != null) {
				BeanUtil.copyNotEmptyProperties(cashdiv, fundReinvest);
			}
			// 刷新
			tdDurationFundReinvestMapper.updateByPrimaryKey(cashdiv);
			// 执行审批操作
			flowOpService.completePersonalTask(user_id, task_id, null,op_type, specific_user, msg, back_to);

		} else if ("5".equals(ftype)) {
			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 查询产品详细信息
			TdDurationFundTrans param = new TdDurationFundTrans();
			param.setDealNo(ParameterUtil.getString(data, "dealNo", ""));
			TdDurationFundTrans cashdiv = tdDurationFundTransMapper.getTdDurationFundTransByDealNo(param);
			TdDurationFundTrans fundTrans = new TdDurationFundTrans();
			BeanUtils.populate(fundTrans, data);
			if (cashdiv != null) {
				BeanUtil.copyNotEmptyProperties(cashdiv, fundTrans);
			}
			// 刷新
			tdDurationFundTransMapper.updateByPrimaryKey(cashdiv);
			// 执行审批操作
			flowOpService.completePersonalTask(user_id, task_id,null, op_type, specific_user, msg, back_to);
		} else {
			// 1.获取表单状态信息
			Map<String, Object> fieldParams = new HashMap<String, Object>();
			fieldParams.put("type", "approve");
			fieldParams.put("userId", ParameterUtil.getString(data, "userId", ""));
			fieldParams.put("prdNo", prdNo);
			fieldParams.put("serialNo", ParameterUtil.getString(data, "dealNo", ""));
			fieldParams.put("taskId", task_id);
			List<TcFieldset> tcFieldsetList = productService.getProductFormFieldsetField(fieldParams);
			// 2。记录手工修改的额度列表；前提需要含有 {'task_id':
			// ProductApprove_${param.type}_${param.prdNo}.tabOptions.param.task_id}
			fieldParams.put("task_id", task_id);
			List<TdFlowTaskEvent> tdFlowTaskEvents = flowQueryService.getFlowTaskEventSelectByTask(fieldParams);

			for (TdFlowTaskEvent tdFlowTaskEvent : tdFlowTaskEvents)// 循环查询是否存在 quotaManualOccupy
			{

			}
			// 3.执行审批操作
			flowOpService.completePersonalTask(user_id, task_id,null, op_type, specific_user, msg, back_to);

			if (!op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Continue)
					&& !op_type.equalsIgnoreCase(DictConstants.FlowCompleteType.Pass)) {
				return;
			}
			// 4.查询产品详细信息
			TdProductApproveMain tdProductApproveMain = productApproveMainMapper
					.getProductApproveMainActivated(ParameterUtil.getString(data, "dealNo", ""));

			// 5。循环遍历所有表单属性，找出所有formStatus = "writable"的读写状态表单 并替换新值
			for (TcFieldset tcFieldset : tcFieldsetList) {
				for (TcField tcField : tcFieldset.getFields()) {
					if (DurationConstants.FORM_STATUS.WRITABLE.equals(tcField.getFormStatus())) {
						String fldValue = ParameterUtil.getString(data, tcField.getFldName(), "");
						BeanUtils.copyProperty(tdProductApproveMain, tcField.getFldName(), fldValue);
					}
				}
			}

			// 5。刷新
			productApproveMainMapper.updateByPrimaryKey(tdProductApproveMain);
		}
	}
}
