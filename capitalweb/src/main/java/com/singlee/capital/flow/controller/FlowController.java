package com.singlee.capital.flow.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.approve.pojo.OrderEditVo;
import com.singlee.capital.approve.service.ApproveInterfaceService;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.flow.service.FlowOtherService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaUserInfoMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TradeInterfaceService;
import com.singlee.ifs.mapper.BondLimitTemplateMapper;
import com.singlee.ifs.mapper.IfsLimitTemplateMapper;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.*;
import com.singlee.slbpm.pojo.vo.*;
import com.singlee.slbpm.service.*;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * 审批流程相关控制器 请求前缀 /FlowController/
 * 
 * @author Libo
 * 
 */
@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
@Controller
@RequestMapping(value = "/FlowController", method = RequestMethod.POST)
public class FlowController extends CommonController {

	@Autowired
	private ProductApproveService productApproveService;

	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;

	/** 审批流程定义接口 */
	@Autowired
	private FlowDefService flowDefService;

	/** 审批流程查询接口 */
	@Autowired
	private FlowQueryService flowQueryService;

	/** 审批流程日志接口 */
	@Autowired
	private FlowLogService flowLogService;

	/** 审批流程操作接口 */
	@Autowired
	private FlowOpService flowOpService;

	/** 审批流程其他接口 */
	@Autowired
	private FlowService flowService;

	/** 审批角色接口 */
	@Autowired
	private FlowRoleService flowRoleService;

	/** capital内的流程附加接口service */
	@Autowired
	private FlowOtherService flowOtherService;

//	@Autowired
//	private TaskService taskService;
	@Autowired
	private ApproveInterfaceService approveInterfaceService;
	@Autowired
	private TradeInterfaceService TradeInterfaceService;
//	@Autowired
//	private LimitEngine limitEngine;
//	@Autowired
//	private CreditService creditService;
	@Autowired
	private RepositoryService repositoryService;

	/**
	 * 待办消息管理
	 */
	@Autowired
	private TaMessageService taMessageService;

	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;

	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;

	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;
	@Autowired
	IfsLimitTemplateMapper ifsLimitTemplateMapper;

	@Autowired
	TaUserMapper taUserMapper;

	public static Map<String, String> trdTypeDictMap = new HashMap<String, String>();
	static {
		// 1-后缀为事前审批 4-后缀为正式申请 存续期只有正式申请
		trdTypeDictMap.put("3001_1", "4");// 1 业务预审
		trdTypeDictMap.put("3001_TDEXT_4", "4");// 10 到期展期
		trdTypeDictMap.put("3001_TDEXP_4", "4");// 资产到期
		trdTypeDictMap.put("3001_PASS_4", "4");// 通道计划
		trdTypeDictMap.put("3003_4", "4");// 11 交易冲正
		trdTypeDictMap.put("3001_PREEND_4", "4");// 13 提前还款
		trdTypeDictMap.put("3001_CFCHANGE_INT_4", "4");// 14 收息计划调整
		trdTypeDictMap.put("3001_OD_4", "4");// 16 交易逾期
		trdTypeDictMap.put("3001_RC_4", "4");// 17 利率变更
		trdTypeDictMap.put("3001_RR_4", "4");// 利率重订
		trdTypeDictMap.put("3001_ORSALE_4", "4");// 18 资产卖断
		trdTypeDictMap.put("3001_CFCHANGE_4", "4");// 19 还本计划调整
		trdTypeDictMap.put("3001_4", "4");// 4 业务放款
		trdTypeDictMap.put("3001_DR_4", "4");// 4 业务放款
		trdTypeDictMap.put("3001_CFCHANGE_FEE_4", "4");// 15 中收计划调整
		trdTypeDictMap.put("3002_4", "4");// 33 审批单撤销
		trdTypeDictMap.put("3001_EC_4", "4");// 4 企业信用类金融资产受益权
		trdTypeDictMap.put("3001_InterSettle_4", "4");// 50 活期结息确认
		trdTypeDictMap.put("3001_PREEND_CURRENT_4", "4");// 51 活期提前支取
		trdTypeDictMap.put("3001_OVERDRAFT_4", "4");// 同业透支
		trdTypeDictMap.put("3001_FCFS_4", "4");// 费用补录-单例
		trdTypeDictMap.put("3001_FCFSCX_4", "4");// 费用补录-存续
		trdTypeDictMap.put("3001_INVFITRANSFOR_4", "4");// 投资占比调整
		trdTypeDictMap.put("3001_DEALEND_4", "4");// 结清
	}

	/**
	 * 判断最后一个人工节点
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/isLastManualStep")
	@ResponseBody
	public String isLastManualStep(@RequestBody Map<String, Object> paramMap) throws Exception {

//        JSONObject result = new JSONObject();
		String task_id = ParameterUtil.getString(paramMap, "task_id", "");
		String feeBaseLogid = ParameterUtil.getString(paramMap, "dealNo", "");
		Map<String, Object> map = new HashMap<String, Object>();
		boolean bIsLastStep = flowQueryService.isLastManualStep2(task_id);
		UPPSCdtTrfRq ut = null;
		if (bIsLastStep) {
			// 是最后一步
			map.put("feeBaseLogid", feeBaseLogid);
			ut = ti2ndPaymentMapper.queryTi2ndPayment2(map);
			if (ut == null) {
				return null;
			} else {
				if ("".equals(ut.getRetMsg()) || ut.getRetMsg() == null) {
					if ("0".equals(ut.getPayResult())) {
						ut.setRetMsg("放款失败");
					}
					if ("1".equals(ut.getPayResult())) {
						ut.setRetMsg("处理中");
					}
					if ("2".equals(ut.getPayResult())) {
						ut.setRetMsg("放款成功");
					}
				}
			}
//        	result.put("message", ut.getRetMsg());
//        	ut.setRetMsg("交易成功");
		}
		return ut.getRetMsg();
	}

	/**
	 * 流程定义
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/flowDefine")
	@ResponseBody
	public List<String> flowDefine(HttpEntity<String> request) throws Exception {

		String json = request.getBody();

// + FIXME - 2017.5.7 - 杨阳 - 流程定义的增删改查目前暂时在外部工程实现
//		FlowDefineInfoVo flowDefineInfoVo = FastJsonUtil.parseObject(json, FlowDefineInfoVo.class);
//		flowDefineInfoVo.setJson_data(json);
//		if(StringUtil.isNotEmpty(flowDefineInfoVo.getFlow_id())){
//			FlowDefineInfoVo oldFlow = flowService.getFlowDefine(flowDefineInfoVo.getFlow_id(), null);
//			if(oldFlow != null){
//				flowDefineInfoVo.setVersion((Integer.parseInt(oldFlow.getVersion())+1)+"");
//			}
//		}
//		flowService.addNewFlowDefine(flowDefineInfoVo,this.getThreadLocalUserId());
// - FIXME - 2017.5.7 - 杨阳 - 流程定义的增删改查目前暂时在外部工程实现

		List<String> list = new ArrayList<String>();
		list.add(json);
		return list;
	}

	/**
	 * 取最新的审批流程定义列表 map参数： flow_id 流程id flow_name 流程名称 flow_type 流程类型 is_active 是否启用
	 * limit_scene 档位场景的模板id pageNumber 页码 pageSize 条数
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchFlowDefine")
	@ResponseBody
	public RetMsg<PageInfo<?>> searchFlowDefine(@RequestBody Map<String, String> paramMap) throws Exception {

		// Yang Yang - 2017.5.19 - 这个函数应该是用不到了
		throw new RException("该功能已废弃");

//		String flow_id=ParameterUtil.getString(paramMap, "flow_id", "");
//		String flow_name=ParameterUtil.getString(paramMap, "flow_name", "");
//		String flow_type=ParameterUtil.getString(paramMap, "flow_type", "");
//		String product_type=ParameterUtil.getString(paramMap, "product_type", "");
//		String trd_type=ParameterUtil.getString(paramMap, "trd_type", "");
//		String limit_scene=ParameterUtil.getString(paramMap, "limit_scene", "");
//		String flow_scene=ParameterUtil.getString(paramMap, "flow_scene", "");
//		String is_active=ParameterUtil.getString(paramMap, "active", "");
//		String flow_belong=ParameterUtil.getString(paramMap, "flow_belong", "");
//		String secu_acc_path_id = null;
//		int pageNum=ParameterUtil.getInt(paramMap, "pageNumber", 1);
//		int pageSize=ParameterUtil.getInt(paramMap, "pageSize", 15);
//		return RetMsgHelper.ok(flowService.searchAllHighVersionFlowDefine(flow_id, flow_name, flow_type,product_type,trd_type,flow_belong,secu_acc_path_id,is_active, flow_scene, limit_scene, pageNum, pageSize));
	}

	/**
	 * 获取流程的步骤信息
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getFlowStepInfo")
	@ResponseBody
	public List<Map<String, Object>> getFlowStepInfo(@RequestBody Map<String, String> paramMap) throws Exception {
		String flow_id = ParameterUtil.getString(paramMap, "flow_id", "");
		// String step_id=ParameterUtil.getString(paramMap, "step_id", "");
		int version = ParameterUtil.getInt(paramMap, "version", 1);

		ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
		// 2.2搜索流程定义
		List<ProcessDefinition> processDefitionList = processDefinitionQuery.processDefinitionKey(flow_id).list();
		ProcessDefinitionImpl definitionimpl = null;
		for (ProcessDefinition d : processDefitionList) {
			if (d.getVersion() == version) {
				definitionimpl = (ProcessDefinitionImpl) d;
			}
		}
		List<Map<String, Object>> stepInfo = new ArrayList<Map<String, Object>>();

		// + 杨阳 - 2017/5/6 - 引擎迁移临时修改
//      List<? extends Activity> list = definitionimpl.getActivities();
//		for (Activity activity : list) {
//			ActivityImpl a = (ActivityImpl)activity;
//			List<VariableDefinitionImpl> vlist = a.getVariableDefinitions();
//			Map<String,Object> vMap = new HashMap<String,Object>();
//			for(VariableDefinitionImpl variableDefinition:vlist){
//				vMap.put(variableDefinition.getName(), variableDefinition.getInitExpression().getExpressionString());
//			}
		List<ActivityImpl> list = definitionimpl.getActivities();
		for (ActivityImpl a : list) {
			Map<String, Object> vMap = a.getVariables();
			// - 杨阳 - 2017/5/6 - 引擎迁移临时修改

			if (vMap.containsKey("roles")) {
				vMap.put("users", new ArrayList<FlowRoleUserMapVo>());
				for (String role_id : vMap.get("roles").toString().split(",")) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("role_id", role_id);
					List<FlowRoleUserMapVo> flowRoleUserMapVoList = flowRoleService.listFlowRoleUserMap(map);
					((ArrayList) vMap.get("users")).addAll(flowRoleUserMapVoList);
				}
			}
			stepInfo.add(vMap);
			System.out.println(a);
			// + 杨阳 - 2017/5/6 - 引擎迁移临时修改
//			System.out.println(vlist);             
			System.out.println(vMap);
			// - 杨阳 - 2017/5/6 - 引擎迁移临时修改
		}
		return stepInfo;
	}

	/**
	 * 获取可用的流程列表，主要用于提交审批时获取能够选择的审批列表 map 参数： serial_no 外部序号【非空】通常为业务审批单号 flow_type
	 * 流程类型【非空】
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getAvailableFlowList")
	@ResponseBody
	public List<ProcDefInfo> getAvailableFlowList(@RequestBody Map<String, String> paramMap) throws Exception {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});

		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_type = ParameterUtil.getString(paramMap, "flow_type", "");
		String product_type = ParameterUtil.getString(paramMap, "product_type", "");
		String trd_type = ParameterUtil.getString(paramMap, "trd_type", "");
		String flow_belong = "";
		if (StringUtil.isEmpty(serial_no) || StringUtil.isEmpty(flow_type)) {
			throw new RException("参数不能为空");
		}
//		List<FlowDefineInfoVo> list =  new ArrayList<FlowDefineInfoVo>();
		String beanName = null;
		Map<String, Object> beanObject = new HashMap<String, Object>();
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");

		// 业务审批流程
		if (DictConstants.FlowType.BusinessApproveFlow.equals(flow_type)) {
			OrderEditVo order = approveInterfaceService.selectOrderEditVo(serial_no);
			trd_type = order.getTrdtype() + "_" + DictConstants.FlowType.BusinessApproveFlow;
			/*
			 * if((!DictConstants.TrdType.CustomMultiResale.equals(trd_type)) &&
			 * (!DictConstants.TrdType.DurationOffset.equals(trd_type))){
			 * TdProductApproveMain productApproveMain =
			 * productApproveServiceImpl.getProductApproveActivated(order.getI_code());
			 * product_type = productApproveMain.getProduct().getPrdType(); }
			 */
//			List<TcProductType> ProductTypeList= productTypeService.getParentProductType(product_type);
//			product_type = ProductTypeList.get(0).getPrdTypeNo()+"";
			engine.put("order", order);
			beanName = DurationConstants.getBeanFromTrdType(order.getTrdtype());
			beanObject.put("dealNo", order.getOrder_id());
			/*
			 * TtAccInCash accInCash = accInCashService.getAcc(order.getSelf_incashaccid());
			 * if(accInCash == null) throw new
			 * RException("内部资金账号获取失败："+order.getSelf_incashaccid()); flow_belong =
			 * accInCash.getInstId();
			 */
		} else if (DictConstants.FlowType.VerifyApproveFlow.equals(flow_type)) {
			HashMap<String, Object> tradeMap = new HashMap<String, Object>();
			tradeMap.put("trade_id", serial_no);
			tradeMap.put("is_active", DictConstants.YesNo.YES);
			TrdTradeVo tradeVo = TradeInterfaceService.selectTrdTradeVoByMap(tradeMap);
			trd_type = tradeVo.getTrdtype() + "_" + DictConstants.FlowType.VerifyApproveFlow;
			/*
			 * if((!DictConstants.TrdType.CustomMultiResale.equals(trd_type)) &&
			 * (!DictConstants.TrdType.DurationOffset.equals(trd_type))){
			 * TdProductApproveMain productApproveMain =
			 * productApproveServiceImpl.getProductApproveActivated(tradeVo.getI_code());
			 * product_type = productApproveMain.getProduct().getPrdType(); }
			 */
			engine.put("trade", tradeVo);
			beanName = DurationConstants.getBeanFromTrdType(tradeVo.getTrdtype());
			beanObject.put("dealNo", tradeVo.getTrade_id());
			/*
			 * TtAccInCash accInCash =
			 * accInCashService.getAcc(tradeVo.getSelf_incashaccid()); if(accInCash == null)
			 * throw new RException("内部资金账号获取失败："+tradeVo.getSelf_incashaccid());
			 * flow_belong = accInCash.getInstId();
			 */
		} else if (DictConstants.FlowType.SettleApproveFlow.equals(flow_type)) {

		}

		flow_belong = SlSessionHelper.getInstitutionId();

		List<ProcDefInfo> list = new ArrayList<ProcDefInfo>();

		// 1、判定：trdType 与数据字典项 SELECT * FROM TA_DICT WHERE DICT_ID ='FlowType';
		if (null != trd_type && !"".equalsIgnoreCase(trd_type)) {
			flow_type = trdTypeDictMap.get(trd_type);// 存续期业务需要重新定位 flow_type
			if (null != beanName && !"".equalsIgnoreCase(beanName) && null != beanObject) {
				// 找寻交易的产品ID
				AbstractDurationService abstractDurationService = SpringContextHolder.getBean(beanName);
				if (null != abstractDurationService.getProductApproveMain(beanObject)) {
					product_type = String.valueOf(abstractDurationService.getProductApproveMain(beanObject).getPrdNo());
				}
			}
		}
//		list = flowService.searchAllHighVersionFlowDefine("", "", flow_type,product_type,trd_type,flow_belong,"","1", "", "", 1,100000);
		list = flowDefService.listProcDef("", flow_type, "1", product_type, flow_belong);

		// + FIXME - 杨阳 - 2017.5.8 - 谁能解释一下这块是干什么的？

//		try {
//			engine.put("FlowIndexCalcUtil", new FlowIndexCalcUtil()); 
//			//boolean d = (Boolean)engine.eval("FlowIndexCalcUtil.getTotalAmount(order) < 100");
//	        engine.eval("Array.prototype.indexOf  = function(ele) {for(var i=0;i<this.length;i++){if(this[i] == ele) return i} return -1; }");
//	        engine.eval("Array.prototype.has  = function(ele) {return this.indexOf(ele)>-1; }");
//	        for(int i=list.size()-1;i>=0;i--){
//	        	try {
//					FlowDefineInfoVo flowDefine = list.get(i);
//					String el = flowDefine.getRule_el_data();
//					if(!StringUtils.isEmpty(el)){
//						boolean result = (Boolean)engine.eval(el); 
//						if(!result){
//							list.remove(i);
//						}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//	        }
//		} catch (ScriptException e1) {
//		} 

		// - FIXME - 杨阳 - 2017.5.8 - 谁能解释一下这块是干什么的?

		/* 业务审批，需要过滤出符合条件的预授权 */
//		if(flow_type.equals(DictConstants.FlowType.BusinessApproveFlow.value())){
//			//调用预授权检查接口
//			List<TrdTradeVo> trades = new ArrayList<TrdTradeVo>();
//			TradeManager mgr = new TradeManager();
//			//获取审批单信息
//			TtTrdOrder order = approveInterfaceService.selectTtTrdOrder(serial_no);
//			TrdTradeVo trade = mgr.getTradeByOrder(order);
//			trades.add(trade);
//			LimitCheckResult checkResult = limitEngine.limitCheck(trades, DictConstants.LimitKind.PreAuthRule.value(), Constants.LimitCallType.APPFLOW.toString(),false);
//			List<PreAuthVo> preAuthVolist = checkResult.getPreAuthPassList();
//			for(PreAuthVo vo : preAuthVolist){
//				FlowDefineInfoVo defineVo = new FlowDefineInfoVo();
//				defineVo.setFlow_id(vo.getP_auth_id());
//				defineVo.setFlow_type("");
//				defineVo.setFlow_name(vo.getP_auth_name());
//				list.add(defineVo);
//			}
//			TtAccInCash accInCash = accInCashService.getAcc(order.getSelf_incashaccid());
//			if(accInCash == null) throw new RException("内部资金账号获取失败："+order.getSelf_incashaccid());
//			String flow_belong = accInCash.getInst_id();
//			List<FlowDefineInfoVo> defineVoList = flowService.searchAllHighVersionFlowDefine("", "", flow_type,flow_belong,"", DictConstants.YesNo.YES.value(), "", "", 1, 10000).getRows();
//			for(FlowDefineInfoVo defineVo:defineVoList){
//				List<String> idList =new ArrayList<String>();
//				if(null != defineVo.getFlow_scene() && !"".equals(defineVo.getFlow_scene())){
//					Collections.addAll(idList, defineVo.getFlow_scene().split(","));
//					idList.remove("");
//				}
//				if(limitEngine.tradeMatchLimitScenes(trades, defineVo.getSecu_acc_path_id(), idList)){
//					list.add(defineVo);
//				}
//			}
//			
//		}else if(flow_type.equals(DictConstants.FlowType.PreAuthApproveFlow.value())){
//			List<FlowDefineInfoVo> defineVoList = flowService.searchAllHighVersionFlowDefine("", "", flow_type,"","",DictConstants.YesNo.YES.value(), "","", 1, 10000).getRows();
//			list.addAll(defineVoList);
//		}
//		
		return list;
	}

	/**
	 * 获取指定的唯一一条流程定义的基本属性 map 参数： serial_no 外部序号 若有该参数，则忽略其他参数，其他参数可为空，否则其他参数必输
	 * flow_id 流程id 【与serial_no不同时为空】 version 版本号 【与serial_no不同时为空】
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getOneFlowDefineBaseInfo")
	@ResponseBody
	public RetMsg<ProcDefInfo> getOneFlowDefineBaseInfo(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_id = ParameterUtil.getString(paramMap, "flow_id", "");
		String version = ParameterUtil.getString(paramMap, "version", "");
		if (StringUtil.isEmpty(serial_no)) {
			if (StringUtil.isEmpty(flow_id) || StringUtil.isEmpty(version)) {
				throw new RException("参数错误");
			}
		} else {
			TtFlowSerialMap flowSerialMap = flowQueryService.getFlowSerialMap(serial_no);
			if (null == flowSerialMap) {
				throw new RException("未找到对应流程");
			}
			flow_id = flowSerialMap.getFlow_id();
			version = flowSerialMap.getVersion();
		}
		return RetMsgHelper.ok(flowQueryService.getFlowDefine(flow_id, version));
	}

	/**
	 * 取审批日志
	 * 
	 * @return map 参数： serial_no 外部流水号
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveLog")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> approveLog(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		if (StringUtil.isEmpty(serial_no)) {
			throw new RException("查询日志出错参数不能为空");
		}

		// 2017/6/14 - yangyang - 从流程引擎获取事件日志
//		List<ApproveLogVo> list = flowLogService.searchFlowLog("", "", serial_no, "", "", "",false);
		List<Map<String, Object>> list = flowLogService.getFlowlogBySerialNo("fsdgsrg");
		return RetMsgHelper.ok(list);
	}

	/**
	 * 取个人的任务列表
	 * 
	 * @param request map参数： user_id 用户【必输】
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchTaskPreview")
	@ResponseBody
	public RetMsg<?> searchTaskPreview(@RequestBody Map<String, Object> paramMap) throws Exception {
		paramMap.put("user_id", this.getThreadLocalUser().getUserId());
		RowBounds rowBounds = ParameterUtil.getRowBounds(paramMap);
		Page<HashMap<String, Object>> list = flowQueryService.searchTaskPreview(paramMap, rowBounds);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 取个人的任务列表
	 * 
	 * @param request map参数： user_id 用户【必输】
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPersonalTask")
	@ResponseBody
	public RetMsg<?> getPersonalTask(@RequestBody Map<String, String> paramMap) throws Exception {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});

		String user_id = this.getThreadLocalUser().getUserId();
		List<Map> taskList = flowService.findPersonalTasks(user_id);
//		for(int i=taskList.size()-1;i>=size;i--){
//			taskList.remove(i);
//		}
//		//取出所有的审批单号，一次获取所有的审批单信息
//		HashMap<String, Map<String,Object>> taskParamMap= new HashMap<String, Map<String,Object>>();
//		List<String> serialNoList = new ArrayList<String>();
//		HashMap<String, OrderEditVo> OrderEditVoMap = new HashMap<String, OrderEditVo>();
//		for(Task task:taskList){
//			TaskImpl parentTask = ((TaskImpl)task).getSuperTask();
//			Set<String> variableNames = new HashSet<String>();
//			variableNames.add("flow_type");
//			variableNames.add("serial_no");
//			variableNames.add("auth_range");
//			variableNames.add("limit_level");
//			Map<String,Object> taskParam = taskService.getVariables(parentTask.getId(), variableNames);
//			taskParamMap.put(task.getId(), taskParam);
//			serialNoList.add(taskParam.get("serial_no").toString());
//		}
//		if(serialNoList !=null && serialNoList.size() > 0){
//			OrderEditVoMap = approveInterfaceService.selectOrderEditVoHashMap(serialNoList);
//		}
//		
//		for(Task task:taskList){
//			Map<String,Object> oneParam = new HashMap<String,Object>();
//			oneParam.putAll(taskParamMap.get(task.getId()));
//			oneParam.put("task_id",task.getId());
//			
//	        if(DictConstants.FlowType.RiskApproveFlow.compare(oneParam.get("flow_type").toString())){
//	        	//将风险预审的类型转换为业务审批
//	        	oneParam.put("flow_type", DictConstants.FlowType.BusinessApproveFlow.value());
//	        }
//	        
//			//获取审批单信息
//			if(DictConstants.FlowType.PreAuthApproveFlow.compare(oneParam.get("flow_type").toString())){
//				//预授权
//				PreAuthVo preAuthVo = preAuthService.searchPreAuth(oneParam.get("serial_no").toString(), "", "", "", "", "", "", "", 1, 1).getRows().get(0);
//				oneParam.put("i_name", preAuthVo.getP_auth_name());
//				oneParam.put("total_amount", "");
//				oneParam.put("net_amount", "");
//				oneParam.put("ytm", "");
//				oneParam.put("party_name", "");
//				oneParam.put("user_name", "");
//				oneParam.put("trader", "");
//				oneParam.put("order_time", preAuthVo.getUpdate_time());			
//				oneParam.put("fst_time", preAuthVo.getBegin_date());				
//				oneParam.put("end_time", preAuthVo.getEnd_date());
//				oneParam.put("folder", "");
//				oneParam.put("status", preAuthVo.getStatus());
//			}else if(DictConstants.FlowType.BusinessApproveFlow.compare(oneParam.get("flow_type").toString())){
//				//业务审批单
//				OrderEditVo order = OrderEditVoMap.get(oneParam.get("serial_no").toString());
//				if(order != null){
//					oneParam.put("i_name", order.getI_name());
//					oneParam.put("total_amount", order.getTotalamount());
//					oneParam.put("net_amount", order.getNetamount());
//					oneParam.put("ytm", order.getYtm());
//					oneParam.put("party_name", order.getParty_Shortname());
//					oneParam.put("user_name", order.getUser_name());
//					oneParam.put("trader", order.getSelf_traderid());
//					oneParam.put("trader_name", order.getSelf_tradername());					
//					oneParam.put("order_time", order.getOperate_time());				
//					oneParam.put("fst_time", order.getFst_settle_date());				
//					oneParam.put("end_time", order.getEnd_settle_date());
//					oneParam.put("folder", order.getFolder());
//					oneParam.put("status", order.getOrder_status());
//				}
//			}
//			list.add(oneParam);
//		}

		return RetMsgHelper.ok(taskList);
	}

	/**
	 * 取个人已审批列表
	 * 
	 * @param request map参数： user_id 用户【必输】
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPersonalApprovedLog")
	@ResponseBody
	public RetMsg<PageInfo<Map>> getPersonalApprovedLog(@RequestBody Map<String, String> paramMap) throws Exception {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
		String beginDate = ParameterUtil.getString(paramMap, "begin_date", "");
		String endDate = ParameterUtil.getString(paramMap, "end_date", "");
		int pageNum = ParameterUtil.getInt(paramMap, "pageNumber", 1);
		int pageSize = ParameterUtil.getInt(paramMap, "pageSize", 10000);
		String user_id = this.getThreadLocalUser().getUserId();
		Page<Map> logListPh = flowService.findPersonalApprovedLog(user_id, beginDate, endDate, pageNum, pageSize);
		/*
		 * for(ApproveLogVo logVo : logListPh.getRows()){
		 * 
		 * Map<String,Object> param = ParentChildUtil.ClassToHashMap(logVo);
		 * //查询一些审批单的信息 String instance_id = param.get("instance_id").toString();
		 * param.put("serial_no", instance_id.split("\\.")[1]);
		 * if(DictConstants.FlowType.PreAuthApproveFlow.compare(param.get("flow_type").
		 * toString())){ //预授权 //PagingHelper<PreAuthVo> ph =
		 * preAuthService.searchPreAuth(param.get("serial_no").toString(), "", "", "",
		 * "", "", "", "", 1, 1); PreAuthVo preAuthVo
		 * =preAuthService.getPreAuthById(param.get("serial_no").toString());
		 * if(preAuthVo != null){ // PreAuthVo preAuthVo = ph.getRows().get(0);
		 * param.put("i_name", preAuthVo.getP_auth_name()); param.put("total_amount",
		 * ""); param.put("net_amount", ""); param.put("ytm", "");
		 * param.put("party_name", ""); param.put("user_name",
		 * preAuthVo.getCreate_user_name()); param.put("trader",
		 * preAuthVo.getCreate_user()); param.put("trader_name",
		 * preAuthVo.getCreate_user_name()); param.put("order_time",
		 * preAuthVo.getUpdate_time()); param.put("fst_time",
		 * preAuthVo.getBegin_date()); param.put("end_time", preAuthVo.getEnd_date());
		 * param.put("folder", ""); param.put("status", preAuthVo.getStatus());
		 * 
		 * } }else
		 * if(DictConstants.FlowType.BusinessApproveFlow.compare(param.get("flow_type").
		 * toString())){ //业务审批单 OrderEditVo order =
		 * approveInterfaceService.selectOrderEditVo(param.get("serial_no").toString());
		 * if(order!=null){ param.put("i_name", order.getI_name());
		 * param.put("total_amount", order.getTotalamount()); param.put("net_amount",
		 * order.getNetamount()); param.put("ytm", order.getYtm());
		 * param.put("party_name", order.getParty_Shortname()); param.put("user_name",
		 * order.getUser_name()); param.put("trader", order.getSelf_traderid());
		 * param.put("trader_name", order.getSelf_tradername()); param.put("order_time",
		 * order.getOperate_time()); param.put("fst_time", order.getFst_settle_date());
		 * param.put("end_time", order.getEnd_settle_date()); param.put("folder",
		 * order.getFolder()); param.put("status", order.getOrder_status()); } }
		 * 
		 * list.add(param); }
		 */
//		Page<Map<String,Object>> ph = new Page<Map<String,Object>>();
//		ph.setPageSize(pageSize);
//		ph.setPageNum(pageNum);
////		ph.setTotal(logListPh.getTotal());
//		ph.addAll(0,list);
		return RetMsgHelper.ok(logListPh);
	}

	/**
	 * 提交审批
	 * 
	 * @param request map参数： serial_no 外部流水号【必输】 flow_type 流程类型【必输】 flow_id 流程id【必输】
	 *                字典项000043 审批流程类型 is_pass_with_pre_auth 业务审批是否使用预授权通过，0：否 1：是
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveCommit")
	@ResponseBody
	public RetMsg<?> approveCommit(@RequestBody Map<String, String> paramMap) throws Exception {
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		String flow_type = ParameterUtil.getString(paramMap, "flow_type", "");
		String flow_id = ParameterUtil.getString(paramMap, "flow_id", "");
		String specific_user = ParameterUtil.getString(paramMap, "specific_user", "");
		String approve_msg = ParameterUtil.getString(paramMap, "approve_msg", "");
		String prd_no = ParameterUtil.getString(paramMap, "product_type", "");
//		String is_pass_with_pre_auth = ParameterUtil.getString(paramMap,"is_pass_with_pre_auth","0");
		String user_id = this.getThreadLocalUserId();
		String limit_level = "";
		String callBackService = null;
		if (DictConstants.FlowType.BusinessApproveFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "approveManageService");
			OrderEditVo order = approveInterfaceService.selectOrderEditVo(serial_no);
			if (order != null && !DictConstants.ApproveStatus.New.equals(order.getOrder_status())) {
				return RetMsgHelper.excetpion("-100", "该业务状态已变更", new RException());
			}
		} else if (DictConstants.FlowType.VerifyApproveFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tradeManageService");
			HashMap<String, Object> tradeMap = new HashMap<String, Object>();
			tradeMap.put("trade_id", serial_no);
			tradeMap.put("is_active", DictConstants.YesNo.YES);
			TrdTradeVo tradeVo = TradeInterfaceService.selectTrdTradeVoByMap(tradeMap);
			if (tradeVo != null && !DictConstants.ApproveStatus.New.equals(tradeVo.getTrade_status())) {
				return RetMsgHelper.excetpion("-100", "该业务状态已变更", new RException());
			}
		} else if (DictConstants.FlowType.SettleApproveFlow.equals(flow_type)) {
			/*
			 * SettleInstVo settleInstVo =
			 * settleInstService.getSettleInstByInstrId(serial_no); if(settleInstVo != null
			 * && !(DictConstants.SettleInstState.WAITSTART.equals(settleInstVo.getStatus())
			 * || DictConstants.SettleInstState.RETURN.equals(settleInstVo.getStatus()))){
			 * return RetMsgHelper.excetpion("-100", "该指令状态已变更", new RException()); }
			 */
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "settleInstService");
		} else if (DictConstants.FlowType.QuotaApproveFlow.equals(flow_type)
				|| DictConstants.FlowType.QuotaAdjFlow.equals(flow_type)
				|| DictConstants.FlowType.QuotaFrozenFlow.equals(flow_type)
				|| DictConstants.FlowType.QuotaSegFlow.equals(flow_type)
				|| DictConstants.FlowType.QuotaUsedFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "creditApproveService");
		} else if (DictConstants.FlowType.PathApproveFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdCustAccCopyServiceImpl");
		} else if (DictConstants.FlowType.AccountApproveFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "accDepositCurrentService");
		} else if (DictConstants.FlowType.InvestFollowupFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "trdInvestFollowupService");
		} else if (DictConstants.FlowType.fundAppFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "productApproveFundService");
		} else if (DictConstants.FlowType.fundCashdivFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdDurationFundCashdivService");
		} else if (DictConstants.FlowType.fundRedeemFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdDurationFundRedeemService");
		} else if (DictConstants.FlowType.fundReinvestFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdDurationFundReinvestService");
		} else if (DictConstants.FlowType.fundTransFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdDurationFundTransService");
		} else if (DictConstants.FlowType.tdCustAtFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tdCustAtCopyService");
		} else if (DictConstants.FlowType.FiveType.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "FiveTypeServiceImpl");
		} else if (DictConstants.FlowType.ordersortingFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, callBackService, "tdOrdersortingApproveService");
		} else if (DictConstants.FlowType.ManualAdjFlow.equals(flow_type)
				|| DictConstants.FlowType.YearEndFlow.equals(flow_type)) {
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "tbkManualEntryService");
		} else if (DictConstants.FlowType.PreApprove.equals(flow_type)) {// 事前审批
			callBackService = ParameterUtil.getString(paramMap, "callBackService", "preApproveService");
		}

		// 提交流程
		flowOpService.startNewFlowInstance(user_id, flow_id, flow_type, serial_no, specific_user, false, false,
				limit_level, callBackService, approve_msg, prd_no);

		// 发送消息提醒
		/*Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("callBackService", callBackService);// 回调Service
		messageMap.put("serial_no", serial_no);// 交易编号
		messageMap.put("flow_type", flow_type);// 流程类型
		messageMap.put("flow_id", flow_id);// 流程ID
		messageMap.put("user_id", SlSessionHelper.getUserId());
		messageMap.put("type", "approveCommit");
		taMessageService.insertTaMessage(messageMap);*/

		return RetMsgHelper.ok();
//		//机构轧帐完成后，不允许提交审批
//		approveInterfaceService.rollingAccountCheck();
//				
//		LimitCheckResult limitCheckResult = null;
////		String json = request.getBody();
////		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
////				json, new TypeReference<Map<String,String>>(){});
//
//		String serial_no = ParameterUtil.getString(paramMap,"serial_no","");
//		String flow_type = ParameterUtil.getString(paramMap,"flow_type","");
//		String flow_id = ParameterUtil.getString(paramMap,"flow_id","");
//		String is_pass_with_pre_auth = ParameterUtil.getString(paramMap,"is_pass_with_pre_auth","0");
//		
//		if(StringUtil.isEmpty(serial_no) 
//				|| StringUtil.isEmpty(flow_type)
//			|| StringUtil.isEmpty(flow_id)){
//			throw new RException("参数不能为空");
//		}
//
//		String user_id = this.getThreadLocalUser().getUserId();
//		boolean need_risk_approve = false;	//是否需要风险预审
//		boolean pre_auth_passed = false;    //是否预授权直接通过
//		String limit_level="1";//档位
//		if(DictConstants.FlowType.PreAuthApproveFlow.compare(flow_type)){
//			/*预授权审批*/
//			String callBackService = "preAuthService";
//			limit_level = "2";
//			flowService.startNewFlowInstance(user_id, flow_id, flow_type, serial_no, need_risk_approve, false, limit_level, callBackService);
//			limitCheckResult = new LimitCheckResult();
//			limitCheckResult.setPass(true);
//			limitCheckResult.setEmpty(true);
//		}else if(DictConstants.FlowType.BusinessApproveFlow.compare(flow_type)){
//			/*业务审批单审批*/
//			String callBackService = "approveManageService";
//			//获取审批单信息
//			List<TrdTradeVo> trades = new ArrayList<TrdTradeVo>();
//			TtTrdOrder order = approveInterfaceService.selectTtTrdOrder(serial_no);
//			if(!order.getOrder_date().equalsIgnoreCase(settlementDate)){
//				//throw new RException("审批单日期与当前业务日不一致");
//			}
//			TradeManager mgr = new TradeManager();
//			TrdTradeVo trade = mgr.getTradeByOrder(order);
//			trades.add(trade);
//			//调用限额风险检查接口
//			limitCheckResult = limitEngine.limitCheck(trades, DictConstants.LimitKind.NormalLimit.value(), Constants.LimitCallType.APPFLOW, false);
//			if(!limitCheckResult.isPass()){
//				return limitCheckResult;
//			}
//			//调用信贷接口占用授信额度
//			String erorInfo = creditService.creditHandle(trade, false,DictConstants.CreditSourceType.Approve.value(),false);
//			if(!StringUtil.isNullOrEmpty(erorInfo)){
//				throw new RException("信贷额度处理出错！" + erorInfo);
//			}
//			try {
//				/*   1:预授权直接通过     否则为正常审批流程*/
//				if("1".equals(is_pass_with_pre_auth)){
//					pre_auth_passed = true;
//				}else{
//					FlowDefineInfoVo defineVo =  flowService.searchAllHighVersionFlowDefine(flow_id, "","","","", "", "", "", 1, 1).getRows().get(0);
//					String limit_scene = defineVo.getLimit_scene();
//					if(null==limit_scene) limit_scene="";
//					//取最大的档位
//					limit_level = ""+limitEngine.getMaxFlowLimitResult(trades, Arrays.asList(limit_scene.split(",")));
//				}
//				flowService.startNewFlowInstance(user_id, flow_id, flow_type, serial_no, need_risk_approve, pre_auth_passed, limit_level, callBackService);
//			} catch (Exception e) {
//				creditService.creditHandle(trade, true,DictConstants.CreditSourceType.Approve.value(),false);
//				throw new RException(e);
//			}
//		}
//		else{
//			throw new RException("审批类型错误");
//		}
//		
//		return limitCheckResult;
	}

	/**
	 * 撤销审批
	 * 
	 * @param request map参数： serial_no 外部流水
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/approveCancel")
	@ResponseBody
	public RetMsg<Serializable> approveCancel(@RequestBody Map<String, String> paramMap) throws Exception {
		String serialNo = ParameterUtil.getString(paramMap, "serial_no", "");
		flowOpService.ApproveCancel(serialNo);
		return RetMsgHelper.ok();
	}

	/**
	 * 审批操作 map参数 complete_type 操作类型【非空】[-1:不通过 0：继续 1：通过] task_id 任务id【非空】 msg 审批信息
	 * 
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */

	@RequestMapping(value = "/approve")
	@ResponseBody
	public RetMsg<Serializable> approve(@RequestBody Map<String, String> paramMap) throws Exception {
		String user_id = this.getThreadLocalUser().getUserId();
		String task_id = ParameterUtil.getString(paramMap, "task_id", "");
		String op_type = ParameterUtil.getString(paramMap, "op_type", "");
		String msg = ParameterUtil.getString(paramMap, "msg", "");
		String specific_user = ParameterUtil.getString(paramMap, "specific_user", "");
		String back_to = ParameterUtil.getString(paramMap, "back_to", "");
		HashMap<String, Object> data = new HashMap<String, Object>();
		String productData = ParameterUtil.getString(paramMap, "data", "");
		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");

		if (StringUtil.isEmpty(user_id) || StringUtil.isEmpty(task_id) || StringUtil.isEmpty(op_type)) {
			throw new RException("参数不能为空");
		}
		// 校验任务是否存在
		Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
		// 根据流程实例ID找到业务映射记录
		Long x = Long.valueOf(task.getProcessInstanceId());
		//id根据
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, x);
		String dealNo = flowSerMap.getSerial_no();
		// "2"为审批跳转操作

		if ("2".equalsIgnoreCase(op_type)) {
			List<String> userList = Arrays.asList(specific_user.split(","));
			flowService.invitationWithTask(task_id, userList);
		} else {
			if (StringUtil.isNotEmpty(productData)) {
				data = (HashMap) FastJsonUtil.parseObject(productData, Map.class);
				flowOtherService.completeAndSave(user_id, task_id, op_type, specific_user, msg, back_to, data);
			} else {
				// 额度释放
				// 审批驳回，op_type -2，1审批通过,99退回发起
				/*
				 * if("99".equalsIgnoreCase(op_type)){
				 * edCustManangeService.eduReleaseFlowService(dealNo);
				 * bondLimitTemplateMapper.deleteOverTimeBond(dealNo);//删除债券
				 * ifsLimitTemplateMapper.deleteTemplateByDealNo(dealNo); }
				 */
				flowOpService.completePersonalTask(user_id, task_id,serial_no, op_type, specific_user, msg, back_to);
			}
		}
		// 发送消息提醒
		/*Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("task_id", task_id);// 环节ID
		messageMap.put("user_id", SlSessionHelper.getUserId());
		messageMap.put("type", "approve");
		messageMap.put("serial_no", dealNo);// 交易编号
		taMessageService.insertTaMessage(messageMap);*/
		return RetMsgHelper.ok();
	}

	/**
	 * 获取外部事件
	 * 
	 * @return List<Map<String,Object>>
	 * @throws Exception
	 */
	@RequestMapping(value = "/getFlowExAction")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> getFlowExAction(@RequestBody Map<String, String> paramMap)
			throws Exception {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();// flowService.getAllActions();
		return RetMsgHelper.ok(list);
	}

	/**
	 * 通过外部流水号获取流程定义ID和流程实例ID
	 * 
	 * @param paramMap serial_no 外部流水号(审批单号)
	 * 
	 * @return Map<String, String> serial_no 外部流水号（审批单号） flow_id 流程定义ID instance_id
	 *         流程实例ID
	 * 
	 * @throws Exception
	 * @author 杨阳
	 */
	@RequestMapping(value = "/getFlowIdAndInstanceIdBySerialNo")
	@ResponseBody
	public RetMsg<?> getFlowIdAndInstanceIdBySerialNo(@RequestBody Map<String, String> paramMap) throws Exception {

		String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
		Map<String, String> map = flowQueryService.getFlowIdAndInstanceIdBySerialNo(serial_no);
		return RetMsgHelper.ok(map);
	}

	/**
	 * 查询某个产品类别下的流程定义列表（启用状态）
	 * 
	 * @param product_type 产品类型
	 * @return List<ProcDefInfo> 流程定义对象列表
	 * @author 杨阳 & wangchen
	 */
	@RequestMapping(value = "/searchProcDefByPrdType")
	@ResponseBody
	public RetMsg<List<ProcDefInfo>> searchProcDefByPrdType(@RequestBody Map<String, String> param) throws Exception {

		String product_type = ParameterUtil.getString(param, "product_type",
				ParameterUtil.getString(param, "productType", ""));
		String flow_type = ParameterUtil.getString(param, "flow_type", ParameterUtil.getString(param, "flowType", ""));
		List<ProcDefInfo> list = flowDefService.searchProcDefByPrdType(product_type, flow_type);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 根据流程定义查找对应的机构列表
	 * 
	 * @param flow_id 流程ID
	 * @return List<String> 机构ID列表
	 * @author 杨阳 wangchen 2017.5.17
	 */
	@RequestMapping(value = "/getInstitutionsByFlowId")
	@ResponseBody
	public RetMsg<List<String>> getInstitutionsByFlowId(@RequestBody Map<String, String> param) throws Exception {
		String flow_id = ParameterUtil.getString(param, "flow_id", null);
		String product_type = ParameterUtil.getString(param, "product_type", null);
		List<String> list = flowDefService.getInstitutionsByFlowId(flow_id, product_type);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询流程定义列表
	 * 
	 * @param flow_id
	 * @param flow_type
	 * @param is_active
	 * @param product_type
	 * @param flow_belong
	 * @return List<String>
	 * @author 杨阳 & wangchen 2017.5.19
	 */
	@RequestMapping(value = "/listProcDef")
	@ResponseBody
	public RetMsg<List<ProcDefInfo>> listProcDef(@RequestBody Map<String, String> param) throws Exception {
		String flow_name = ParameterUtil.getString(param, "flow_name", null);
		String flow_type = ParameterUtil.getString(param, "flow_type", null);
		String is_active = ParameterUtil.getString(param, "is_active", null);
		String product_type = ParameterUtil.getString(param, "product_type", null);
		String flow_belong = ParameterUtil.getString(param, "flow_belong", null);
		List<ProcDefInfo> list = flowDefService.listProcDef(flow_name, flow_type, is_active, product_type, flow_belong);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询流程定义列表
	 * 
	 * @param flow_id
	 * @param flow_type
	 * @param is_active
	 * @param product_type
	 * @param flow_belong
	 * @return List<String>
	 * @author 杨阳 & wangchen 2017.5.19
	 */
	@RequestMapping(value = "/searchProcDefByType")
	@ResponseBody
	public RetMsg<List<ProcDefInfo>> searchProcDefByType(@RequestBody Map<String, String> param) throws Exception {
		String flow_type = ParameterUtil.getString(param, "flow_type", null);
		List<ProcDefInfo> list = flowDefService.searchProcDefByType(flow_type);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 新增流程关系
	 * 
	 * @param flow_id      流程ID
	 * @param version      版本
	 * @param product_type 产品类型
	 * @return List<String> 机构ID列表
	 * @author 杨阳 & wangchen 2017.5.19
	 */
	@RequestMapping(value = "/bindProcDefProps")
	@ResponseBody
	public RetMsg<Serializable> bindProcDefProps(@RequestBody Map<String, String> param) throws Exception {
		String flow_id = ParameterUtil.getString(param, "flow_id", null);
		String version = ParameterUtil.getString(param, "version", null);
		String flow_type = ParameterUtil.getString(param, "flow_type", "");
		List<String> instIds = new ArrayList<String>();
		for (String item : ParameterUtil.getString(param, "instIds", "").split(",")) {
			if (StringUtil.isNotEmpty(item)) {
				instIds.add(item);
			}
		}
		String product_type = ParameterUtil.getString(param, "product_type", null);
		try{
			flowDefService.bindProcDefProps(flow_id, version, product_type, instIds,flow_type);
		}catch (Exception e) {
			return RetMsgHelper.fail(e.getMessage());
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 删除流程关系
	 * 
	 * @param flow_id      流程ID
	 * @param version      版本
	 * @param product_type 产品类型
	 * @author 杨阳 & wangchen 2017.5.19cc
	 */
	@RequestMapping(value = "/unbindProcDefProps")
	@ResponseBody
	public RetMsg<Serializable> unindProcDefProps(@RequestBody Map<String, String> param) throws Exception {
		String flow_id = ParameterUtil.getString(param, "flow_id", null);
		String product_type = ParameterUtil.getString(param, "product_type", null);
		flowDefService.unbindProcDefProps(flow_id, null, product_type);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询下一个人工审批任务节点的审批候选人 注：如果下个节点为互斥或并行节点，则返回空，界面上就不能指定下一个节点的审批人
	 * 
	 * @param flowId 流程定义ID
	 * @param stepId 任务实例ID（步骤）
	 * @return List<ApproveUser> 审批用户列表
	 * @author Yang Yang & wangchen 2017/5/23
	 */
	@RequestMapping(value = "/getNextStepCandidatesList")
	@ResponseBody
	public RetMsg<List<ApproveUser>> getNextStepCandidatesList(@RequestBody Map<String, String> param)
			throws Exception {
		String flow_id = ParameterUtil.getString(param, "flow_id", null);
		String step_id = ParameterUtil.getString(param, "step_id", null);
		String serial_no = ParameterUtil.getString(param, "serial_no", null);
		Map<String, Object> result = flowQueryService.getNextStepCandidatesList(flow_id, step_id, serial_no);
		List<ApproveUser> list = (List<ApproveUser>) result.get("userList");
		return RetMsgHelper.ok(list);
	}

	/**
	 * 加签
	 * 
	 * @param taskId 任务实例ID（步骤）
	 * @return List<String> 加签人LIST
	 * @author Yang Yang & wangchen 2017/5/27
	 */
	@RequestMapping(value = "/invitationWithTask")
	@ResponseBody
	public RetMsg<Serializable> invitationWithTask(@RequestBody Map<String, String> param) throws Exception {

		String user_id = SlSessionHelper.getUserId();
		String task_id = ParameterUtil.getString(param, "task_id", null);
		String to_user_ids = ParameterUtil.getString(param, "user_id", "");
		// 校验任务是否存在
		Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
		// 根据流程实例ID找到业务映射记录
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, null, null,
				Long.valueOf(task.getProcessInstanceId()));
		String dealNo = flowSerMap.getSerial_no();
		flowOpService.completePersonalTask(user_id, task_id, null,"2", to_user_ids, "", "");
		// 发送消息提醒
		/*Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("task_id", task_id);// 环节ID
		messageMap.put("user_id", SlSessionHelper.getUserId());
		messageMap.put("type", "invitationWithTask");
		messageMap.put("approveUser", to_user_ids);
		messageMap.put("serial_no", dealNo);// 交易编号
		taMessageService.insertTaMessage(messageMap);*/
		return RetMsgHelper.ok();
	}

	/**
	 * 挂起/激活
	 * 
	 * @param action 动作（suspend/activate）
	 * @return task_id 任务实例ID
	 * @author Yang Yang & wangchen 2017/6/7
	 */
	@RequestMapping(value = "/hangupTask")
	@ResponseBody
	public RetMsg<Serializable> hangupTask(@RequestBody Map<String, String> param) throws Exception {

		String action = ParameterUtil.getString(param, "action", "");
		String instanceId = ParameterUtil.getString(param, "instanceId", "");
		String reason = ParameterUtil.getString(param, "reason", "");

		if (StringUtils.equals("suspend", action)) {
			flowOpService.suspendFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		} else {
			flowOpService.activateFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 挂起/激活
	 * 
	 * @param action 动作（suspend/activate）
	 * @return task_id 任务实例ID
	 * @author Yang Yang & wangchen 2017/6/7
	 */
	@RequestMapping(value = "/flowSuspendProcess")
	@ResponseBody
	public RetMsg<Serializable> flowSuspendProcess(@RequestBody Map<String, String> param) throws Exception {

		String action = "suspend";
		String instanceId = ParameterUtil.getString(param, "instanceId", "");
		String reason = ParameterUtil.getString(param, "reason", "");

		if (StringUtils.equals("suspend", action)) {
			flowOpService.suspendFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		} else {
			flowOpService.activateFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 激活
	 * 
	 * @param action 动作（activate）
	 * @return task_id 任务实例ID
	 * @author Yang Yang & wangchen 2017/6/7
	 */
	@RequestMapping(value = "/flowContinueProcess")
	@ResponseBody
	public RetMsg<Serializable> flowContinueProcess(@RequestBody Map<String, String> param) throws Exception {

		String action = "activate";
		String instanceId = ParameterUtil.getString(param, "instanceId", "");
		String reason = ParameterUtil.getString(param, "reason", "");

		if (StringUtils.equals("suspend", action)) {
			flowOpService.suspendFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		} else {
			flowOpService.activateFlowByTaskId(instanceId, SlSessionHelper.getUserId(), reason);
		}
		return RetMsgHelper.ok();
	}

	/**
	 * 所有进行中的流程，包含挂起的
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "ongoingProcessInstances")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> ongoingProcessInstances(@RequestBody Map<String, String> param)
			throws Exception {
		List<Map<String, Object>> result = flowService.searchProcessInstance();
		return RetMsgHelper.ok(result);
	}

	/**
	 * 所有待办任务
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "todoTasks")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> todoTasks(@RequestBody Map<String, String> param) throws Exception {
		List<Map<String, Object>> result = flowService.searchTaskWaitting();
		return RetMsgHelper.ok(result);
	}

	@RequestMapping(value = "searchTaskWaitting")
	@ResponseBody
	public RetMsg<PageInfo<TaskWaitingView>> searchTaskWaitting(@RequestBody Map<String, Object> param)
			throws Exception {
		param.put("userId", this.getThreadLocalUser().getUserId());
		Page<TaskWaitingView> result = flowService.searchTaskWaittingForConsole(param);
		return RetMsgHelper.ok(result);
	}

	/**
	 * 转办操作
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "transferTask")
	@ResponseBody
	public RetMsg<Serializable> transferTask(@RequestBody Map<String, String> param) throws Exception {
		String taskId = ParameterUtil.getString(param, "taskId", "");
		String toUserId = ParameterUtil.getString(param, "toUserId", "");
		flowOpService.transferTask(taskId, toUserId);
		return RetMsgHelper.ok();
	}

	/**
	 * 添加事件
	 */
	@RequestMapping(value = "/addFlowTaskEvent")
	@ResponseBody
	public RetMsg<Serializable> addFlowTaskEvent(@RequestBody Map<String, Object> param) throws Exception {
		flowQueryService.addFlowTaskEvent(param);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改事件
	 */
	@RequestMapping(value = "/updateFlowTaskEvent")
	@ResponseBody
	public RetMsg<Serializable> updateFlowTaskEvent(@RequestBody Map<String, Object> param) throws Exception {
		flowQueryService.updateFlowTaskEvent(param);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除事件
	 */
	@RequestMapping(value = "/deleteFlowTaskEvent")
	@ResponseBody
	public RetMsg<Serializable> deleteFlowTaskEvent(@RequestBody Map<String, Object> param) throws Exception {
		flowQueryService.deleteFlowTaskEvent(param);
		return RetMsgHelper.ok();
	}

	/**
	 * 查询事件列表
	 */
	@RequestMapping(value = "/getFlowTaskEvent")
	@ResponseBody
	public RetMsg<PageInfo<TdFlowTaskEvent>> getFlowTaskEvent(@RequestBody Map<String, Object> param) throws Exception {
		Page<TdFlowTaskEvent> list = flowQueryService.getFlowTaskEvent(param);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询已选择事件列表
	 */
	@RequestMapping(value = "/getFlowTaskEventSelect")
	@ResponseBody
	public RetMsg<List<TdFlowTaskEvent>> getFlowTaskEventSelect(@RequestBody Map<String, Object> param)
			throws Exception {
		List<TdFlowTaskEvent> list = flowQueryService.getFlowTaskEventSelect(param);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询未选择事件列表
	 */
	@RequestMapping(value = "/getFlowTaskEventUnselect")
	@ResponseBody
	public RetMsg<List<TdFlowTaskEvent>> getFlowTaskEventUnselect(@RequestBody Map<String, Object> param)
			throws Exception {
		List<TdFlowTaskEvent> list = flowQueryService.getFlowTaskEventUnselect(param);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询已选择检查框列表
	 */
	@RequestMapping(value = "/getFlowCheckListSelect")
	@ResponseBody
	public RetMsg<List<TdFlowTaskEvent>> getFlowCheckListSelect(@RequestBody Map<String, Object> param)
			throws Exception {
		List<TdFlowTaskEvent> list = flowQueryService.getUserTaskEvents(param);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 添加流程节点定义
	 */
	@RequestMapping(value = "/addFlowTaskDefine")
	@ResponseBody
	public RetMsg<Serializable> addFlowTaskDefine(@RequestBody Map<String, Object> param) throws Exception {
		String[] events = StringUtil.isNotEmpty(ParameterUtil.getString(param, "events", ""))
				? ParameterUtil.getString(param, "events", "").split(",")
				: null;
		String flowId = ParameterUtil.getString(param, "flowId", "");
		String taskId = ParameterUtil.getString(param, "taskId", "");
		String prdCode = ParameterUtil.getString(param, "prdCode", "");
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("task_def_key", taskId);
			map.put("flow_id", flowId);
			map.put("prd_code", prdCode);

			// 删除对应关系
			flowDefService.deleteCheckListByTask(map);
			flowDefService.deleteEventByTask(map);

			// 添加对应关系
			if (events != null) {
				List<String> eventList = new ArrayList<String>();
				for (String event : events) {
					eventList.add(event);
				}
				flowDefService.setProcTaskEvents(flowId, "1", taskId, prdCode, eventList);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new RException(e);
		}

		return RetMsgHelper.ok();
	}

	/**
	 * 通过产品和流程类型查询流程列表 productType,flowType
	 */
	@RequestMapping(value = "/getFlowListByInfo")
	@ResponseBody
	public RetMsg<List<ProcDefInfo>> getFlowListByInfo(@RequestBody Map<String, Object> param) throws Exception {
		String flowType = ParameterUtil.getString(param, "flowType", "");
		String productType = ParameterUtil.getString(param, "productType", "");
		List<ProcDefInfo> list = flowDefService.listProcDef(null, flowType, null, productType, null);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 通过流程id查询流程节点列表 productType,flowType
	 */
	@RequestMapping(value = "/getFlowTaskListFlowId")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> getFlowTaskListFlowId(@RequestBody Map<String, Object> param)
			throws Exception {
		String flowId = ParameterUtil.getString(param, "flowId", "");
		List<Map<String, Object>> list = flowQueryService.getFlowActivities(flowId);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveFinished")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageProductApproveFinished(
			@RequestBody Map<String, Object> params) {
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params, 2);
		return RetMsgHelper.ok(page);

	}

	/**
	 * 查询挂起日志
	 */
	@RequestMapping(value = "/getSuspendlog")
	@ResponseBody
	public RetMsg<List<ProcSuspensionLog>> getSuspendlog(@RequestBody Map<String, Object> param) throws Exception {
		List<ProcSuspensionLog> list = new ArrayList<ProcSuspensionLog>();
		String serial_no = ParameterUtil.getString(param, "serial_no", "");
		list = flowLogService.getSuspendlogBySerialNo(serial_no);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询Assignee
	 */
	@RequestMapping(value = "/getAssignee")
	@ResponseBody
	public RetMsg<List<String>> getAssignee(@RequestBody Map<String, Object> param) throws Exception {
		List<String> list = new ArrayList<String>(16);
		String serial_no = ParameterUtil.getString(param, "fedealno", "");
		list = flowLogService.selectBySerialNo(serial_no);
		if (list!=null&&list.size()>0){
			List<String> listUserName = taUserMapper.getAllUserByUserId(list);
			return RetMsgHelper.ok(listUserName);
		}
		return RetMsgHelper.ok(list);
	}

	/**
	 * 获取流程人工任务节点上的绑定表单ID
	 */
	@RequestMapping(value = "/getUserTaskForm")
	@ResponseBody
	public RetMsg<ProcTaskForm> getUserTaskForm(@RequestBody Map<String, Object> param) throws Exception {
		String procDefId = ParameterUtil.getString(param, "procDefId", "");
		String taskDefKey = ParameterUtil.getString(param, "taskDefKey", "");
		String prdCode = ParameterUtil.getString(param, "prdCode", "");
		ProcTaskForm result = flowQueryService.getUserTaskForm(procDefId, taskDefKey, prdCode);
		return RetMsgHelper.ok(result);
	}

	/**
	 * 获取流程人工任务节点上的绑定表单ID
	 */
	@RequestMapping(value = "/getProcDefPropMap")
	@ResponseBody
	public RetMsg<List<ProcDefPropMapVo>> getProcDefPropMap(@RequestBody Map<String, Object> param) throws Exception {
		List<ProcDefPropMapVo> result = flowQueryService.getProcDefPropMap(param);
		return RetMsgHelper.ok(result);
	}

	/**
	 * 获取当前登录人的推送消息
	 */
	@RequestMapping(value = "/getNotification")
	@ResponseBody
	public RetMsg<List<TdNotificationVo>> getNotification(@RequestBody Map<String, Object> param) throws Exception {
		param.put("to_user_id", SlSessionHelper.getUserId());
		List<TdNotificationVo> result = flowQueryService.getNotification(param);
		return RetMsgHelper.ok(result);
	}

	@RequestMapping(value = "searchDurationTaskWaittingForConsole")
	@ResponseBody
	public RetMsg<PageInfo<TaskWaitingView>> searchDurationTaskWaittingForConsole(
			@RequestBody Map<String, Object> param) throws Exception {
		param.put("userId", this.getThreadLocalUser().getUserId());
		Page<TaskWaitingView> result = flowService.searchDurationTaskWaittingForConsole(param);
		return RetMsgHelper.ok(result);
	}

	@RequestMapping(value = "searchDurationTaskWaittingCountForConsole")
	@ResponseBody
	public RetMsg<Integer> searchDurationTaskWaittingCountForConsole(@RequestBody Map<String, Object> param)
			throws Exception {
		param.put("userId", this.getThreadLocalUser().getUserId());
		return RetMsgHelper.ok(flowService.searchDurationTaskWaittingCountForConsole(param));
	}

	/**
	 * 查询用户在流程下所属的岗位
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getUserRoleListByFlow")
	@ResponseBody
	public RetMsg<List<Map<String, Object>>> getUserRoleListByFlow(@RequestBody Map<String, Object> param)
			throws Exception {
		String user_id = ParameterUtil.getString(param, "userId", "");
		String flow_id = ParameterUtil.getString(param, "flowId", "");
		List<Map<String, Object>> result = flowRoleService.getUserRoleListByFlow(user_id, flow_id);
		return RetMsgHelper.ok(result);
	}

	/**
	 * 获取流程人工任务节点上的绑定表单ID
	 */
	@RequestMapping(value = "/getUserTaskUrl")
	@ResponseBody
	public RetMsg<ProcTaskUrl> getUserTaskUrl(@RequestBody Map<String, Object> param) throws Exception {
		String procDefId = ParameterUtil.getString(param, "procDefId", "");
		String taskDefKey = ParameterUtil.getString(param, "taskDefKey", "");
		String prdCode = ParameterUtil.getString(param, "prdCode", "");
		String taskId = ParameterUtil.getString(param, "taskId", "");

		ProcTaskUrl result = flowQueryService.getUserTaskUrl(procDefId, taskDefKey, prdCode, taskId);
		return RetMsgHelper.ok(result);
	}

	/**
	 * 删除url
	 */
	@RequestMapping(value = "/deleteFlowTaskUrl")
	@ResponseBody
	public RetMsg<Serializable> deleteFlowTaskUrl(@RequestBody Map<String, Object> param) throws Exception {
		flowDefService.deleteUrlByTask(param);
		return RetMsgHelper.ok();
	}

	/**
	 * 添加url
	 */
	@RequestMapping(value = "/addFlowTaskDefineUrl")
	@ResponseBody
	public RetMsg<Serializable> addFlowTaskDefineUrl(@RequestBody Map<String, Object> param) throws Exception {
		String flowId = ParameterUtil.getString(param, "flow_id", "");
		String taskDefKey = ParameterUtil.getString(param, "task_def_key", "");
		String prdCode = ParameterUtil.getString(param, "prd_no", "");
		String urls = ParameterUtil.getString(param, "urls", "");
		String version = flowId.split(":")[1];
		flowDefService.setProcTaskFormUrl(flowId, version, taskDefKey, prdCode, urls);
		return RetMsgHelper.ok();
	}

	/**
	 * 发起人撤销
	 */
	@RequestMapping(value = "/reback")
	@ResponseBody
	public RetMsg<Serializable> reback(@RequestBody Map<String, Object> param) throws Exception {
		String serialNo = ParameterUtil.getString(param, "serial_no", "");
		flowOpService.ApproveCancel(serialNo);
		return RetMsgHelper.ok();
	}

	/**
	 * 撤回/召回
	 */
	@RequestMapping(value = "/recall")
	@ResponseBody
	public RetMsg<Serializable> recall(@RequestBody Map<String, Object> param) throws Exception {
		String serialNo = ParameterUtil.getString(param, "serial_no", "");
		flowOpService.recall(serialNo);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据流程编号查询关联产品信息
	 */
	@RequestMapping(value = "/getPrdInfoByFlow")
	@ResponseBody
	public RetMsg<List<ProcDefPropMapVo>> getPrdInfoByFlow(@RequestBody Map<String, Object> param) throws Exception {
		List<ProcDefPropMapVo> list = flowDefService.getPrdInfoByFlow(param);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 删除流程定义（伪删除，将is_active改为0）
	 * 
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	@RequestMapping(value = "/deleteFlowDef")
	@ResponseBody
	public RetMsg<Serializable> deleteFlowDef(@RequestBody Map<String, Object> param) throws Exception {
		flowDefService.deleteFlowDef(param);
		return RetMsgHelper.ok();
	}
}
