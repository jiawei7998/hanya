package com.singlee.capital.flow.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import org.activiti.engine.RuntimeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.approve.pojo.OrderEditVo;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.service.ProductApproveFundService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;
import com.singlee.capital.trade.mapper.TdProductApproveOverDraftMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.ProductCustCreditService;
import com.singlee.capital.users.model.TdCustAtCopy;
import com.singlee.capital.users.service.TdCustAtCopyService;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.service.IFSService;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.mapper.CFtRdpConfMapper;
import com.singlee.refund.mapper.CFtRdpMapper;
import com.singlee.refund.mapper.CFtReddMapper;
import com.singlee.refund.mapper.CFtReinMapper;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.model.CFtRdp;
import com.singlee.refund.model.CFtRdpConf;
import com.singlee.refund.model.CFtRedd;
import com.singlee.refund.model.CFtRein;
import com.singlee.slbpm.externalInterface.CreditInterface;
import com.singlee.slbpm.externalInterface.QualificationInterface;
import com.singlee.slbpm.externalInterface.RiskInterface;
import com.singlee.slbpm.service.FlowQueryService;

/**
 * 流程指标计算工具
 * 
 * @author Libo
 *
 */
@Service("FlowIndexCalcUtil")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowIndexCalcUtil implements RiskInterface, CreditInterface, QualificationInterface {

	/**
	 * 当日总额度
	 */
	private Double totalAmount;

	@Autowired
	private ApproveManageService approveManageService;

	@Autowired
	private ProductApproveService productApproveService;

	@Autowired
	private ProductApproveFundService productApproveFundService;

	@Autowired
	private ProductCustCreditService productCustCreditService;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private FlowQueryService flowQueryService;

	@Autowired
	private TdCustAtCopyService tdCustAtCopyService;

	@Autowired
	private AdvanceMaturityCurrentService advanceMaturityCurrentService;

	@Autowired
	private TdProductApproveOverDraftMapper productApproveOverDraftMapper;

	// 注入流程监控表
	@Autowired
	IfsFlowMonitorMapper ifsFlowMonitorMapper;

	// 信用拆借
	@Autowired
	private IfsCfetsrmbIboMapper cfetsrmbIboMapper;

	// 人民币即期
	@Autowired
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;

	// 人民币远期
	@Autowired
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;

	// 人命币外币掉期
	@Autowired
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;

	// 人民币期权
	@Autowired
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;

	// 外币拆借
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;

	// 利率互换
	@Autowired
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;

	// 买断式回购
	@Autowired
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;

	// 现劵买卖
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	// 债券远期
	@Autowired
	IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	// 债券
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;

	// 债券借贷
	@Autowired
	IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper;
	// 债券借贷事前审批
	@Autowired
	IfsApprovermbSlMapper ifsApprovermbSlMapper;
	// 同业存款
	@Autowired
	IfsApprovermbDepositMapper ifsApprovermbDepositMapper;

	// 质押式回购
	@Autowired
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;

	// 基金申购
	@Autowired
	CFtAfpMapper cFtAfpMapper;
	// 基金申购
	@Autowired
	CFtAfpConfMapper cFtAfpConfMapper;
	@Autowired
	CFtRdpMapper cFtRdpMapper;
	//赎回确认
	@Autowired
	CFtRdpConfMapper cFtRdpConfMapper;
	//现金分红
	@Autowired
	CFtReddMapper cFtReddMapper;
	//红利再投
	@Autowired
	CFtReinMapper cFtReinMapper;

	//存放同业(对俄)
	@Autowired
	IfsFxDepositoutMapper ifsFxDepositoutMapper;

	//存放同业
	@Autowired
	IfsRmbDepositoutMapper ifsRmbDepositoutMapper;

	@Autowired
	IAcupServer acupServer;

	// 用户限额
	@Autowired
	IfsQuotaMapper ifsQuotaMapper;

	// 存单发行
	@Autowired
	IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;
	// 字典库
	TaDictVoMapper taDictVoMapper;
	// 流程特殊用户
	@Autowired
	private IFSService cfetsFlowUser;

	/**
	 * 流程中取当日总额度
	 * 
	 * @return
	 */
	public double getTotalAmount(OrderEditVo order) {
		if (totalAmount != null) {
			return totalAmount;
		}
		totalAmount = approveManageService.getTotamAmount();
		return totalAmount;
	}

	/**
	 * 风险等级判断接口实现
	 * 
	 * @return 0-低 1-中 2-高
	 */
	@Override
	public int getRiskLevel(String serial_no) {
		// TODO - 风险等级判断接口实现
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(serial_no);
		if (null == productApproveMain) {
			throw new RException("原业务单据异常:" + serial_no);}
		return Integer.parseInt(productApproveMain.getRiskLevel());
	}

	/**
	 * 准入资格判断接口实现
	 * 
	 * @param serial_no
	 * @return 0-不符合 1-符合
	 */
	@Override
	public int checkQualification(String serial_no) {
		Map<String, Object> result = new HashMap<String, Object>();
		// 查询是否货基，交易单号带hj的视为基金交易
		int isHJ = serial_no.indexOf("HJ");
		int isFH = serial_no.indexOf("FH");
		int isSH = serial_no.indexOf("SH");
		if (isHJ > -1 || isFH > -1 || isSH > -1) {
			// 货基有特殊的查询方法
			ProductApproveFund productApproveFund = productApproveFundService.getFundInfoForApprove(serial_no);
			if (null == productApproveFund) {
				throw new RException("原业务单据异常:" + serial_no);}
			result = BeanUtil.beanToMap(productApproveFund);
		} else {
			// 普通自定义产品的查询方法
			String ref_serial_no = this.getRefSerialNo(serial_no);
			TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(ref_serial_no);
			if (null == productApproveMain) {
				throw new RException("原业务单据异常:" + serial_no);}
			result = BeanUtil.beanToMap(productApproveMain);
		}
		return productCustCreditService.getProductJudgeAccessLevel(result) == true ? 1 : 0;
	}

	@Override
	public int checkSourceOfMoney(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int checkInstitutionType(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Boolean UseCredit(String serial_no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean ReleaseCredit(String serial_no) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 期限额度
	 */
	@Override
	public int getCreditPeriod(String serial_no) {
		// TODO Auto-generated method stub
		String ref_serial_no = this.getRefSerialNo(serial_no);
		Map<String, String> process = flowQueryService.getFlowIdAndInstanceIdBySerialNo(ref_serial_no);
		String processInstanceId = ParameterUtil.getString(process, "instance_id", "");
		String term3YFlag = runtimeService.getVariable(processInstanceId, "term3YFlag").toString();
		boolean isTerm3YFlag = Boolean.parseBoolean(String.valueOf(term3YFlag));
		if (isTerm3YFlag) {
			return 9999999;}
		else {
			return 0;}
	}

	@Override
	public int getFinType(String serial_no) {
		// 存放同业活期提前支取也会走正式申请流程，先判断是否有原交易号
		String ref_serial_no = this.getRefSerialNo(serial_no);
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(ref_serial_no);
		if (null == productApproveMain) {
			throw new RException("原业务单据异常:" + serial_no);}
		// LC_TYPE N VARCHAR2(32) Y 理财类型
		// lcsType 0-保本 1-保本浮动 2-非保本
		return StringUtils.trimToNull(productApproveMain.getLcType()) == null ? 0
				: Integer.parseInt(productApproveMain.getLcType());
	}

	// 客户准入级别 0:临时 1:A级 2:B级 3:C级
	@Override
	public int getCustomerLevel(String serial_no) {
		String custId = serial_no.split("AT")[0];
		int custVer = Integer.parseInt(serial_no.split("AT")[1]);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("custmerCode", custId);
		map.put("version", custVer);
		List<TdCustAtCopy> custResult = tdCustAtCopyService.showSameAtCopyService(map);
		if (custResult == null || custResult.size() == 0) {
			return 0;
		}
		if ("4".equals(custResult.get(0).getAtType())) {
			return 0;
		} else {
			return Integer.parseInt(custResult.get(0).getAtType());
		}

	}

	@Override
	public int isAmtSatisfied(String serial_no) {
		// TODO - 是否满足交易金额 1-是 0-否
		// 查询是否货基，交易单号带hj的视为基金交易
		int isHJ = serial_no.indexOf("HJ");
		int isFH = serial_no.indexOf("FH");
		int isSH = serial_no.indexOf("SH");
		if (isHJ > -1 || isFH > -1 || isSH > -1) {
			// 货基有特殊的查询方法
			ProductApproveFund productApproveFund = productApproveFundService.getFundInfoForApprove(serial_no);
			if (null == productApproveFund) {
				throw new RException("原业务单据异常:" + serial_no);}
			// 获取金额线
			double amtLine = productApproveService.getProductAmtLine(productApproveFund.getPrdNo().toString(),
					productApproveFund.getCcy());
			if (Double.parseDouble(productApproveFund.getAmt().toString()) >= amtLine) {
				return 1;
			} else {
				return 0;
			}
		} else {
			String ref_serial_no = this.getRefSerialNo(serial_no);
			TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(ref_serial_no);
			// 同业透支特殊处理
			if (productApproveMain.getPrdNo() == 906) {
				TdProductApproveOverDraft productApproveOverDraft = new TdProductApproveOverDraft();
				productApproveOverDraft.setDealNo(productApproveMain.getDealNo());
				productApproveOverDraft.setVersion(productApproveMain.getVersion());
				productApproveOverDraft = productApproveOverDraftMapper.selectByPrimaryKey(productApproveOverDraft);
				if (null != productApproveOverDraft) {
					// 同业透支用启用额度代替本金
					productApproveMain.setAmt(productApproveOverDraft.getEnableAmt());
				}
			}
			// 获取金额线
			double amtLine = productApproveService.getProductAmtLine(productApproveMain.getPrdNo().toString(),
					productApproveMain.getCcy());
			if (productApproveMain.getAmt() >= amtLine) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public int needLoan(String serial_no) {
		// TODO - 是否需要放款 1-是 0-否
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(serial_no);
		// 获取该产品是否需要放款
		boolean isNeedLoan = productApproveService.getProductIsNeedLoan(productApproveMain.getPrdNo().toString());
		if (isNeedLoan) {
			return 1;}
		else {
			return 0;}
	}

	@Override
	public int getCurrency(String serial_no) {
		// 查询是否货基，交易单号带hj的视为基金交易
		int isHJ = serial_no.indexOf("HJ");
		int isFH = serial_no.indexOf("FH");
		int isSH = serial_no.indexOf("SH");
		if (isHJ > -1 || isFH > -1 || isSH > -1) {
			// 货基有特殊的查询方法
			ProductApproveFund productApproveFund = productApproveFundService.getFundInfoForApprove(serial_no);
			if (null == productApproveFund) {
				throw new RException("原业务单据异常:" + serial_no);}
			if (DictConstants.Currency.CNY.equals(productApproveFund.getCcy())) {
				return 1;
			} else {
				return 0;
			}
		} else {
			// TODO - 币种 1-人民币 0-外币
			String ref_serial_no = this.getRefSerialNo(serial_no);
			TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(ref_serial_no);
			if (DictConstants.Currency.CNY.equals(productApproveMain.getCcy())) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	//
	private String getRefSerialNo(String serial_no) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dealNo", serial_no);
		TdAdvanceMaturityCurrent tdAdvanceMaturityCurrent = advanceMaturityCurrentService
				.getAdvanceMaturityCurrentById(serial_no);
		if (tdAdvanceMaturityCurrent == null) {
			return serial_no;
		} else if (StringUtil.isNullOrEmpty(tdAdvanceMaturityCurrent.getRefNo())) {
			return serial_no;
		} else {
			return tdAdvanceMaturityCurrent.getRefNo();
		}
	}

	/**
	 * 泰隆交易金额分支
	 */
	@Override
	public double getTotalAmount(String serial_no) {
		// 默认是0
		double amt = 0.0;
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		if ("rmbspt".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbspt是外汇即期，单位是（元）
			IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(serial_no);
			//金额1
			amt = Double.parseDouble(ifsCfetsfxSpt.getBuyAmount().toString());
			//交易货币
			String currency = ifsCfetsfxSpt.getOpicsccy();

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {
				
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			
			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}
		}
		if ("rmbfwd".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbfwd是外汇远期，单位是（元）
			IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(serial_no);
			//金额1
			amt = Double.parseDouble(ifsCfetsfxFwd.getBuyAmount().toString());
			//交易货币
			String currency = ifsCfetsfxFwd.getOpicsccy();

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}
		}
		if ("rmbswap".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbswap是外汇掉期，单位是（元）
			IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);
			double amt1 = Double.parseDouble(ifsCfetsfxSwap.getNearPrice());
			double amt2 = Double.parseDouble(ifsCfetsfxSwap.getFwdPrice());
			if (amt1 > amt2) {
				amt = amt1;
			} else {
				amt = amt2;
			}
			//交易货币
			String currency = ifsCfetsfxSwap.getOpicsccy();

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			
			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}
			
		}
		if ("ccyLending".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是外币拆借，单位是外币
			IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);

			amt = ifsCfetsfxLend.getAmount().doubleValue();
			
			String currency = ifsCfetsfxLend.getCurrency();
			
			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			
			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}

		}
		if ("ibo".equals(ifsFlowMonitor.getPrdNo()) || "tyibo".equals(ifsFlowMonitor.getPrdNo()) || "fxtyibo".equals(ifsFlowMonitor.getPrdNo())) {
			// ibo是信用拆借，单位是（万元）
			IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(serial_no);
			amt = ifsCfetsrmbIbo.getAmt().doubleValue() * 10000;
		}
		if ("ccyDepositout".equals(ifsFlowMonitor.getPrdNo())) {
			// 存放同业(对俄),单位是元
			IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchByTicketId(serial_no);
			amt = ifsFxDepositout.getAmt().doubleValue();

			String currency = ifsFxDepositout.getCurrency();

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}

			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}

		}
		if ("rmbDepositOut".equals(ifsFlowMonitor.getPrdNo())) {
			// 存放同业,单位是元
			IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(serial_no);
			amt = ifsRmbDepositout.getAmt().doubleValue();

			String currency = ifsRmbDepositout.getCurrency();

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}

			String rateCCType = acupServer.queryRateCCType("01", currency, "M");
			if(!StringUtil.isEmptyString(rateCCType)) {
				if("M".equals(rateCCType)) {
					amt = Double.parseDouble(new BigDecimal(amt).multiply(new BigDecimal(rate)) + "");
				}else {
					amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
				}
			}else {
				JY.raise("货币" + currency + "不存在汇率计算方式");
			}
		}
		if ("IBD".equals(ifsFlowMonitor.getPrdNo())||"IBDOFF".equals(ifsFlowMonitor.getPrdNo())) {
			// IBD是同业存款，单位是（元）
			IfsApprovermbDeposit deposit = ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(serial_no);
			amt = deposit.getAmt().doubleValue();
		}
		if ("cbt".equals(ifsFlowMonitor.getPrdNo()) || "debt".equals(ifsFlowMonitor.getPrdNo())|| "mbs".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbCbt.getTotalFaceValue() + "") * 10000;
		}
		if ("sl".equals(ifsFlowMonitor.getPrdNo())) {
			if ("1".equals(ifsFlowMonitor.getTradeType())) {
				// sl是债券借贷，单位是（万元）
				Map<String, String> map = new HashMap<String, String>();
				map.put("ticketId", serial_no);
				map.put("branchId", DictConstants.Branch.ID);
				IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.selectById(map);
				amt = Double.parseDouble(ifsCfetsrmbSl.getUnderlyingQty() + "") * 10000;
			} else {
				Map<String, String> map = new HashMap<String, String>();
				map.put("ticketId", serial_no);
				map.put("branchId", DictConstants.Branch.ID);
				IfsApprovermbSl ifsApprovermbSl = ifsApprovermbSlMapper.searchBondLon(serial_no);
				amt = Double.parseDouble(ifsApprovermbSl.getUnderlyingQty() + "") * 10000;
			}
		}
		if ("dp".equals(ifsFlowMonitor.getPrdNo()) || "dpcd".equals(ifsFlowMonitor.getPrdNo())) {
			// dp是债券发行，单位是（元） dpcd是存单发行，单位是（元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.selectById(map);
			if (ifsCfetsrmbDp != null) {
				if (ifsCfetsrmbDp.getActualAmount() != null) {
					amt = Double.parseDouble(ifsCfetsrmbDp.getActualAmount().toString());
				}
			}
		}
		if ("cr".equals(ifsFlowMonitor.getPrdNo()) || "CRJY".equals(ifsFlowMonitor.getPrdNo())
				|| "CRCB".equals(ifsFlowMonitor.getPrdNo()) || "CRZQ".equals(ifsFlowMonitor.getPrdNo())
				|| "CRZD".equals(ifsFlowMonitor.getPrdNo()) || "CRGD".equals(ifsFlowMonitor.getPrdNo())) {
			// cr是质押式回购，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbCr.getUnderlyingQty() + "") * 10000;

		}
		if ("or".equals(ifsFlowMonitor.getPrdNo())) {
			// or是买断式回购，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbOr.getTotalFaceValue() + "") * 10000;
		}
		if ("irs".equals(ifsFlowMonitor.getPrdNo()) || "irsDerive".equals(ifsFlowMonitor.getPrdNo())) {
			// irs是利率互换，单位是（万元），irsDerive是结构衍生，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			IfsCfetsrmbIrs IfsCfetsrmbIrs = ifsCfetsrmbIrsMapper.selectById(map);
			if (IfsCfetsrmbIrs != null) {
				amt = Double.parseDouble(IfsCfetsrmbIrs.getLastQty() + "") * 10000;
				String currency = IfsCfetsrmbIrs.getCcy();
				Map<String, String> mapRate = new HashMap<String, String>();
				String rate = "1";
				if (!"CNY".equals(currency)) {

					mapRate.put("ccy", currency);
					mapRate.put("br", "01");// 机构号，送01
					BigDecimal dayRate = acupServer.queryRate(mapRate);
					if (dayRate == null || "".equals(dayRate)) {
						JY.raise("货币" + currency + "不存在汇率");
					}
					rate = dayRate + "";// 以后需要修改为BigDecimal
				}

				// 汇率乘以金额
				amt = Double.parseDouble(rate) * amt;
			}
		}

		if ("rmbopt".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是人民币期权，单位是（元）
			IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(serial_no);
			amt = Double.parseDouble(ifsCfetsfxOption.getBuyNotionalAmount().toString());// 名义本金

			String currency = ifsCfetsfxOption.getCurrencyPair().substring(0, 3);// 货币对
			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			// 金额乘汇率
			amt = Double.parseDouble(rate) * amt;
		}
		if ("bfwd".equals(ifsFlowMonitor.getPrdNo())) {
			// bfwd债券远期
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = ifsCfetsrmbBondfwdMapper.selectByMap(map);
			amt = Double.parseDouble(ifsCfetsrmbBondfwd.getSettlementAmount() + "");
		}
		if (TradeConstants.TrdType.CURY_FUND_AFP.equals(ifsFlowMonitor.getPrdNo()) ||
				TradeConstants.TrdType.BD_FUND_AFP.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.FUND_AFP.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金申购
			CFtAfp cFtAfp = cFtAfpMapper.selectByPrimaryKey(serial_no);
			amt = cFtAfp.getAmt().doubleValue();
		}
		if (TradeConstants.TrdType.CURY_FUND_AFPCONF.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.BD_FUND_AFPCONF.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.FUND_AFPCONF.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金申购份额确认
			CFtAfpConf cFtAfpConf = cFtAfpConfMapper.selectByPrimaryKey(serial_no);
			amt = cFtAfpConf.getAmt().doubleValue();
		}
		if (TradeConstants.TrdType.CURY_FUND_RDP.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.BD_FUND_RDP.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.FUND_RDP.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金赎回
			CFtRdp cFtRdp = cFtRdpMapper.selectByPrimaryKey(serial_no);
			amt = cFtRdp.getShareAmt().doubleValue();
		}
		if (TradeConstants.TrdType.CURY_FUND_RDPCONF.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.BD_FUND_RDPCONF.equals(ifsFlowMonitor.getPrdNo())||
				TradeConstants.TrdType.FUND_RDPCONF.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金赎回份额确认
			CFtRdpConf cFtRdpConf = cFtRdpConfMapper.selectByPrimaryKey(serial_no);
			amt = cFtRdpConf.getAmt().doubleValue();
		}
		if (TradeConstants.TrdType.FUND_REDD.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金现金分红
			CFtRedd cFtRedd = cFtReddMapper.selectByPrimaryKey(serial_no);
			amt = cFtRedd.getAmt().doubleValue();
		}
		if (TradeConstants.TrdType.FUND_REIN.equals(ifsFlowMonitor.getPrdNo())) {
			// 基金红利再投
			CFtRein cFtRein = cFtReinMapper.selectByPrimaryKey(serial_no);
			amt = cFtRein.geteAmt().doubleValue();
		}
		return amt;
	}

	/**
	 * 泰隆交易方向分支
	 */
	@Override
	public int getMyDir(String serial_no) {
		int result = 1;// P为1，S为2
		String myDir = "P";// 如果没有默认是P
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		Map<String, String> map = new HashMap<String, String>();
		map.put("ticketId", serial_no);
		map.put("branchId", DictConstants.Branch.ID);
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			if (ifsCfetsrmbCbt != null) {
				myDir = ifsCfetsrmbCbt.getMyDir();
			}
		}
		if ("cr".equals(ifsFlowMonitor.getPrdNo())) {
			// cr是质押式回购
			IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.selectById(map);
			if (ifsCfetsrmbCr != null) {
				myDir = ifsCfetsrmbCr.getMyDir();
				if ("P".equals(myDir)) {
					myDir = "S";
				} else {
					myDir = "P";
				}
			}
		}
		if ("or".equals(ifsFlowMonitor.getPrdNo())) {
			// or是买断式回购
			IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.selectById(map);
			if (ifsCfetsrmbOr != null) {
				myDir = ifsCfetsrmbOr.getMyDir();
				if ("P".equals(myDir)) {
					myDir = "S";
				} else {
					myDir = "P";
				}
			}
		}
		if ("sl".equals(ifsFlowMonitor.getPrdNo())) {
			// sl是债券借贷
			IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.selectById(map);
			if (ifsCfetsrmbSl != null) {
				myDir = ifsCfetsrmbSl.getMyDir();
			}
		}
		if ("ibo".equals(ifsFlowMonitor.getPrdNo())) {
			// ibo是信用拆借
			IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(serial_no);
			if (ifsCfetsrmbIbo != null) {
				myDir = ifsCfetsrmbIbo.getMyDir();
			}
		}
		if ("ccyLending".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是外币拆借
			IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);
			if (ifsCfetsfxLend != null) {
				myDir = ifsCfetsfxLend.getMyDir();
			}
		}
		if ("S".equals(myDir)) {
			result = 2;
		}
		return result;
	}

	/**
	 * 泰隆债券类型分支
	 */
	@Override
	public int getBondProperties(String serial_no) {
		int result = 6;// 默认其他
		String bondProperties = "";
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		Map<String, String> map = new HashMap<String, String>();
		map.put("ticketId", serial_no);
		map.put("branchId", DictConstants.Branch.ID);
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			if (ifsCfetsrmbCbt != null) {
				IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbCbt.getBondCode());
				if (ifsOpicsBond != null) {
					bondProperties = ifsOpicsBond.getBondproperties() + ",";
				}
			}
		}
		if ("dp".equals(ifsFlowMonitor.getPrdNo())) {
			// dp是存单发行
			Map<String, String> map1 = new HashMap<String, String>();
			map1.put("ticketId", serial_no);
			IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.selectById(map1);
			if (ifsCfetsrmbDp != null) {
				IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbDp.getDepositCode());
				if (ifsOpicsBond != null) {
					bondProperties = ifsOpicsBond.getBondproperties() + ",";
				}
			}
		}
		if (!"".equals(bondProperties)) {
			if ("SE-ZFZCJGZ,SE-GZ,SE-DFZFZ,SE-YP,SE-ZCXYHZ,".contains(bondProperties)) {
				result = 1;// 利率债
			} else if ("SE-QYZ,SE-GSZ,SE-ZP,SE-DR,SE-CDR,SE-DXGJ,".contains(bondProperties)) {
				result = 2;// 非金融机构信用债
			} else if ("SE-SYYHZ,SE-SYYHCJZ,SE-SYYHHHZ,SE-BXGSZ,SE-ZQGSZ,SE-ZQGSDR,SE-QTHRJGZ,SE-SYYHEJZ,"
					.contains(bondProperties)) {
				result = 3;// 金融机构债券
			} else if ("SE-CD,".contains(bondProperties)) {
				result = 4;// 同业存单
			} else if ("SE-ZJHABS,SE-YJHABS,SE-JYSXHABN,".contains(bondProperties)) {
				result = 5;// 资产支持证券、ABN
			} else {
				result = 6;// 其他
			}
		}
		return result;
	}

	/**
	 * 产品代码
	 */
	@Override
	public int getProduct(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * 产品类型
	 */
	@Override
	public int getProdType(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * 成本中心
	 */
	@Override
	public int getCost(String serial_no) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * 泰隆风险程度分支
	 */
	@Override
	public int getRiskDegree(String serial_no) {
		int result = 0;
		String bondProperties = "0";// 默认：不适用
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		Map<String, String> map = new HashMap<String, String>();
		map.put("ticketId", serial_no);
		map.put("branchId", DictConstants.Branch.ID);
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			if (ifsCfetsrmbCbt != null) {
				bondProperties = ifsCfetsrmbCbt.getRiskDegree() == null ? "" : ifsCfetsrmbCbt.getRiskDegree();
			}
		}
		if (!"".equals(bondProperties)) {
			result = Integer.parseInt(bondProperties);
		}
		return result;
	}

	/**
	 * 泰隆主体评级分支
	 */
	@Override
	public int getBondRating(String serial_no) {
		int result = 14;
		String bondRating = "14";// 默认主体评级：无评级
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		Map<String, String> map = new HashMap<String, String>();
		map.put("ticketId", serial_no);
		map.put("branchId", DictConstants.Branch.ID);
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			if (ifsCfetsrmbCbt != null) {
				IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbCbt.getBondCode());
				if (ifsOpicsBond != null) {
					bondRating = ifsOpicsBond.getBondrating() == null ? "" : ifsOpicsBond.getBondrating();
				}
			}
		}
		if (!"".equals(bondRating)) {
			result = Integer.parseInt(bondRating);
		}
		return result;
	}

	/**
	 * 币种分支
	 */
	@Override
	public int getTlCurrency(String serial_no) {
		int result = 1;
		String bondRating = "CNY";
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		Map<String, String> map = new HashMap<String, String>();
		map.put("ticketId", serial_no);
		map.put("branchId", DictConstants.Branch.ID);
		if ("ccyLending".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是外币拆借
			IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);
			if (ifsCfetsfxLend != null) {
				bondRating = ifsCfetsfxLend.getCurrency() == null ? "CNY" : ifsCfetsfxLend.getCurrency();
			}
		}
		Map<String, Integer> map1 = new HashMap<String, Integer>();
		map1.put("CNY", 1);
		map1.put("USD", 2);
		map1.put("EUR", 3);
		map1.put("JPY", 4);
		map1.put("HKD", 5);
		map1.put("GBP", 6);
		map1.put("RUB", 7);
		map1.put("MYR", 8);
		map1.put("CAD", 9);
		map1.put("AUD", 10);
		map1.put("CHF", 11);
		map1.put("SGD", 12);
		result = map1.get(bondRating);
		if ("".equals(result)) {
			result = 1;
		}
		return result;
	}

	/**
	 * 币种分支
	 */
	@Override
	public int isTrader(String serial_no) {
		int result = 0;// 默认不满足
		double fixedValue = 0;// 流程设置值
		double amt = 0;// 业务金额
		String condition = "";
		String userId = "";
		String productType = "";
		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);
		if ("ibo".equals(ifsFlowMonitor.getPrdNo())) {
			// ibo是信用拆借（单位万元）
			IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(serial_no);
			amt = ifsCfetsrmbIbo.getAmt().doubleValue() * 10000;
			userId = ifsCfetsrmbIbo.getSponsor();
			productType = "444";
		}
		if ("rmbspt".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbspt是外汇即期，单位是（元）
			IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(serial_no);
			amt = Double.parseDouble(ifsCfetsfxSpt.getBuyAmount().toString());
			String currency = ifsCfetsfxSpt.getCurrencyPair().substring(0, 3);// 由币种查询汇率
			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			// 金额乘以汇率
			amt = Double.parseDouble(rate) * amt;
			mapRate.put("ccy", "USD");
			BigDecimal dayRate2 = acupServer.queryRate(mapRate);
			if (dayRate2 == null || "".equals(dayRate2)) {
				JY.raise("货币" + currency + "不存在汇率");
			} else {
				rate = dayRate2 + "";// 以后需要修改为BigDecimal
			}
			amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
			userId = ifsCfetsfxSpt.getSponsor();
			productType = "411";
		}
		if ("rmbfwd".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbfwd是外汇远期，单位是（元）
			IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(serial_no);
			amt = Double.parseDouble(ifsCfetsfxFwd.getBuyAmount().toString());

			String currency = ifsCfetsfxFwd.getCurrencyPair().substring(0, 3);// 由币种查询汇率

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			// 汇率乘以金额
			amt = Double.parseDouble(rate) * amt;

			mapRate.put("ccy", "USD");
			BigDecimal dayRate2 = acupServer.queryRate(mapRate);
			if (dayRate2 == null || "".equals(dayRate2)) {
				JY.raise("货币" + currency + "不存在汇率");
			} else {
				rate = dayRate2 + "";// 以后需要修改为BigDecimal
			}
			amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
			userId = ifsCfetsfxFwd.getSponsor();
			productType = "391";
		}
		if ("rmbswap".equals(ifsFlowMonitor.getPrdNo())) {
			// rmbswap是外汇掉期，单位是（元）
			IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);
			double amt1 = Double.parseDouble(ifsCfetsfxSwap.getNearPrice());
			double amt2 = Double.parseDouble(ifsCfetsfxSwap.getFwdPrice());
			if (amt1 > amt2) {
				amt = amt1;
			} else {
				amt = amt2;
			}

			String currency = ifsCfetsfxSwap.getCurrencyPair().substring(0, 3);// 近端货币对

			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {

				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			// 金额除汇率折美元
			amt = Double.parseDouble(rate) * amt;

			mapRate.put("ccy", "USD");
			BigDecimal dayRate2 = acupServer.queryRate(mapRate);
			if (dayRate2 == null || "".equals(dayRate2)) {
				JY.raise("货币" + currency + "不存在汇率");
			} else {
				rate = dayRate2 + "";// 以后需要修改为BigDecimal
			}
			amt = Double.parseDouble(new BigDecimal(amt).divide(new BigDecimal(rate), 4, RoundingMode.HALF_UP) + "");
			userId = ifsCfetsfxSwap.getSponsor();
			productType = "432";
		}
		if ("rmbopt".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是人民币期权，单位是（元）
			IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(serial_no);
			amt = Double.parseDouble(ifsCfetsfxOption.getBuyNotionalAmount().toString());// 名义本金

			String currency = ifsCfetsfxOption.getCurrencyPair().substring(0, 3);// 货币对
			Map<String, String> mapRate = new HashMap<String, String>();
			String rate = "1";
			if (!"CNY".equals(currency)) {
				mapRate.put("ccy", currency);
				mapRate.put("br", "01");// 机构号，送01
				BigDecimal dayRate = acupServer.queryRate(mapRate);
				if (dayRate == null || "".equals(dayRate)) {
					JY.raise("货币" + currency + "不存在汇率");
				}
				rate = dayRate + "";// 以后需要修改为BigDecimal
			}
			// 金额乘汇率
			amt = Double.parseDouble(rate) * amt;
			userId = ifsCfetsfxOption.getSponsor();
			productType = "433";
		}
		if ("ccyLending".equals(ifsFlowMonitor.getPrdNo())) {
			// ccyLending是外币拆借，单位是外币
			IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);
			amt = ifsCfetsfxLend.getAmount().doubleValue();
			userId = ifsCfetsfxLend.getSponsor();
			productType = "438";
		}
		if ("irs".equals(ifsFlowMonitor.getPrdNo())) {
			// irs是利率互换，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			IfsCfetsrmbIrs IfsCfetsrmbIrs = ifsCfetsrmbIrsMapper.selectById(map);
			if (IfsCfetsrmbIrs != null) {
				amt = Double.parseDouble(IfsCfetsrmbIrs.getLastQty() + "") * 10000;
				String currency = IfsCfetsrmbIrs.getCcy();
				Map<String, String> mapRate = new HashMap<String, String>();
				String rate = "1";
				if (!"CNY".equals(currency)) {

					mapRate.put("ccy", currency);
					mapRate.put("br", "01");// 机构号，送01
					BigDecimal dayRate = acupServer.queryRate(mapRate);
					if (dayRate == null || "".equals(dayRate)) {
						JY.raise("货币" + currency + "不存在汇率");
					}
					rate = dayRate + "";// 以后需要修改为BigDecimal
				}

				// 汇率乘以金额
				amt = Double.parseDouble(rate) * amt;
				userId = IfsCfetsrmbIrs.getSponsor();
				productType = "441";
			}
		}
		if ("or".equals(ifsFlowMonitor.getPrdNo())) {
			// or是买断式回购，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbOr.getTotalFaceValue() + "") * 10000;
			userId = ifsCfetsrmbOr.getSponsor();
			productType = "442";
		}
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {
			// cbt现券买卖，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbCbt.getTotalFaceValue() + "") * 10000;
			userId = ifsCfetsrmbCbt.getSponsor();
			productType = "443";
		}
		if ("sl".equals(ifsFlowMonitor.getPrdNo())) {
			// sl是债券借贷，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbSl.getMarginTotalAmt() + "") * 10000;
			userId = ifsCfetsrmbSl.getSponsor();
			productType = "445";
		}
		if ("cr".equals(ifsFlowMonitor.getPrdNo())) {
			// cr是质押式回购，单位是（万元）
			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.selectById(map);
			amt = Double.parseDouble(ifsCfetsrmbCr.getUnderlyingQty() + "") * 10000;
			userId = ifsCfetsrmbCr.getSponsor();
			productType = "446";
		}
		IfsCfetsFlowUser ifsCfetsFlowUser = null;
		if (!"".equals(userId) && !"".equals(productType)) {
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("userId", userId);
			map1.put("productType", productType);
			ifsCfetsFlowUser = cfetsFlowUser.queryCfetsFlowUserById(map1);
			if (ifsCfetsFlowUser != null) {
				condition = ifsCfetsFlowUser.getCondition();
				fixedValue = Double.valueOf(ifsCfetsFlowUser.getAmt());
			}
		}
		if (!"".equals(condition)) {
			if ("1".equals(condition) && amt < fixedValue) {
				result = 1;
			} else if ("2".equals(condition) && amt <= fixedValue) {
				result = 1;
			} else if ("3".equals(condition) && amt > fixedValue) {
				result = 1;
			} else if ("4".equals(condition) && amt >= fixedValue) {
				result = 1;
			}
		}
		return result;
	}

	@Override
	public int isLimitAmount(String serial_no) {

		int result = 0;// 默认不限额
		IfsQuotaKey key = new IfsQuotaKey();

		double amt = getTotalAmount(serial_no);// 获取交易金额

		IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(serial_no);

		if ("rmbspt".equals(ifsFlowMonitor.getPrdNo())) {// 外汇即期

			IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(serial_no);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsfxSpt.getProduct());
			key.setProdType(ifsCfetsfxSpt.getProdType());
			key.setCost(ifsCfetsfxSpt.getCost());
		}
		if ("rmbfwd".equals(ifsFlowMonitor.getPrdNo())) {// 外汇远期

			IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(serial_no);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsfxFwd.getProduct());
			key.setProdType(ifsCfetsfxFwd.getProdType());
			key.setCost(ifsCfetsfxFwd.getCost());
		}
		if ("rmbswap".equals(ifsFlowMonitor.getPrdNo())) {// 外汇掉期期

			IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsfxSwap.getProduct());
			key.setProdType(ifsCfetsfxSwap.getProdType());
			key.setCost(ifsCfetsfxSwap.getCost());
		}
		if ("ibo".equals(ifsFlowMonitor.getPrdNo())) {// 信用拆借

			IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(serial_no);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbIbo.getProduct());
			key.setProdType(ifsCfetsrmbIbo.getProdType());
			key.setCost(ifsCfetsrmbIbo.getCost());
		}
		if ("irs".equals(ifsFlowMonitor.getPrdNo())) {// 利率互换

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			IfsCfetsrmbIrs IfsCfetsrmbIrs = ifsCfetsrmbIrsMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(IfsCfetsrmbIrs.getProduct());
			key.setProdType(IfsCfetsrmbIrs.getProdType());
			key.setCost(IfsCfetsrmbIrs.getCost());
		}
		if ("or".equals(ifsFlowMonitor.getPrdNo())) {// 买断式回购

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbOr.getProduct());
			key.setProdType(ifsCfetsrmbOr.getProdType());
			key.setCost(ifsCfetsrmbOr.getCost());
		}
		if ("cr".equals(ifsFlowMonitor.getPrdNo())) {// 质押式回购

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbCr.getProduct());
			key.setProdType(ifsCfetsrmbCr.getProdType());
			key.setCost(ifsCfetsrmbCr.getCost());
		}
		if ("cbt".equals(ifsFlowMonitor.getPrdNo())) {// 现券买卖

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbCbt.getProduct());
			key.setProdType(ifsCfetsrmbCbt.getProdType());
			key.setCost(ifsCfetsrmbCbt.getCost());
		}
		if ("sl".equals(ifsFlowMonitor.getPrdNo())) {// 债券借贷

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			map.put("branchId", DictConstants.Branch.ID);
			IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbSl.getProduct());
			key.setProdType(ifsCfetsrmbSl.getProdType());
			key.setCost(ifsCfetsrmbSl.getCost());
		}
		if ("dp".equals(ifsFlowMonitor.getPrdNo())) {// 存单发行

			Map<String, String> map = new HashMap<String, String>();
			map.put("ticketId", serial_no);
			IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.selectById(map);

			key.setUserId(SlSessionHelper.getUserId());
			key.setProduct(ifsCfetsrmbDp.getProduct());
			key.setProdType(ifsCfetsrmbDp.getProdType());
			key.setCost(ifsCfetsrmbDp.getCost());
		}

		IfsQuota ifsQuota = ifsQuotaMapper.selectByPrimaryKey(key);
		if (null == ifsQuota || null == ifsQuota.getQuota()) {
			return result;
		}

		double quota = Double.parseDouble(ifsQuota.getQuota().multiply(new BigDecimal("100000000")).toString());

		if (amt > quota) {
			return 1;
		} else {
			return result;
		}

	}
}
