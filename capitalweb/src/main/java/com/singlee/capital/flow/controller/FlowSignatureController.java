package com.singlee.capital.flow.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 电子签名
 */
@Controller
@RequestMapping(value = "/FlowSignatureController", method = RequestMethod.POST)
public class FlowSignatureController extends CommonController {

    @RequestMapping(value = "/base64ToImages")
    @ResponseBody
    public RetMsg<?>  base64ToImages(@RequestBody Map<String,Object> paramMap){

        return RetMsgHelper.ok();
    }
}
