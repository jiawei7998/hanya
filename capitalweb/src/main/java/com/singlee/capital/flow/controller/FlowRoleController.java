package com.singlee.capital.flow.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.util.CollectionUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.service.UserService;
import com.singlee.slbpm.pojo.bo.TtFlowOperation;
import com.singlee.slbpm.pojo.bo.TtFlowRole;
import com.singlee.slbpm.pojo.bo.TtFlowRoleAuth;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapHisVo;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapVo;
import com.singlee.slbpm.service.FlowRoleService;
 
/**
 * 审批流程角色相关控制器   请求前缀 /FlowRoleController/
 * 
 * @author LyonChen
 * 
 */
@Controller
@RequestMapping(value = "/FlowRoleController", method = RequestMethod.POST)
public class FlowRoleController extends CommonController {

	/**
	 * 审批角色 服务 spring注入
	 */
	@Autowired
	private FlowRoleService  flowRoleService;

	@Autowired
	public SessionService sessionService;
	@Autowired
	public UserService userService;
	
	
	/**
	 * 获得 转授权 列表
	 * map 参数：
	 * 		role_id 	角色id [可为空]
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/listFlowRoleUserMap")
	@ResponseBody
	public RetMsg<List<FlowRoleUserMapVo>> listFlowRoleUserMap(@RequestBody Map<String,Object> paramMap) throws IOException {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
		String role_id = ParameterUtil.getString(paramMap,"role_id","");
		if(StringUtil.isEmpty(role_id)){
			paramMap.put("user_id",this.getThreadLocalUser().getUserId());
		}
		List<FlowRoleUserMapVo> list = flowRoleService.listFlowRoleUserMap(paramMap);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 清空 转授权 列表
	 * map 参数：
	 * 		role_id 	角色id [可为空]
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetFlowRoleUserMap")
	@ResponseBody
	public RetMsg<?> resetFlowRoleUserMap(@RequestBody Map<String,Object> paramMap) throws IOException {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
		paramMap.put("user_id",this.getThreadLocalUser().getUserId());
		flowRoleService.resetFlowRoleUserMap(paramMap);
		return RetMsgHelper.ok();
	}
	/**
	 * 保存 转授权 列表
	 * list 参数 ：
	 * 			user_id;// 角色名称   传上来，不关注，使用后台的session中获得userid
	 * 			role_id;// 角色ID
	 *			auth_userid;// 授权用户ID 空为没有转授权
	 *			auth_begdate;// 授权开始日期
	*			auth_enddate;// 授权结束日期
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveFlowRoleUserMap")
	@ResponseBody
	public RetMsg<Serializable> saveFlowRoleUserMap(@RequestBody List<Map<String,String>> list)  throws IOException {
//		String json = request.getBody();
//		List<Map<String,String>> list = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<List<Map<String,String>>>(){});
		if(list!=null && list.size()>0){
			List<FlowRoleUserMapVo> mapList = new LinkedList<FlowRoleUserMapVo>();
			for(Map<String,String> map : list){
				FlowRoleUserMapVo vo = new FlowRoleUserMapVo();
				vo.setAuth_begdate(ParameterUtil.getString(map,"auth_begdate",""));
				vo.setAuth_enddate(ParameterUtil.getString(map,"auth_enddate",""));
				vo.setAuth_userid(ParameterUtil.getString(map,"auth_userid",""));
				vo.setRole_id(ParameterUtil.getString(map,"role_id",""));
				vo.setUser_id(ParameterUtil.getString(map,"user_id",""));
				//判断用户是否有审批列表的页面权限
//				if(StringUtil.notNullOrEmpty(vo.getAuth_userid()) &&
//						!sessionService.checkUserRoleWithModuleId(vo.getAuth_userid(), "100001")){
//					UserVo userVo = userService.selectUser(vo.getAuth_userid());
//					throw new JYException((userVo!=null?userVo.getUser_name():"")+"["+vo.getAuth_userid()+"] 没有[审批列表]页面权限");
//				}
				mapList.add(vo);
			}
			String user_id = this.getThreadLocalUser().getUserId();
			flowRoleService.saveFlowRoleUserMap(user_id, mapList);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询所有审批角色
	 * @param request
	 * map参数：
	 * 		role_id		角色id   【可空】
	 * 		role_name	角色名称  【可空】
	 * 		memo		备注		【可空】
	 * 		inst_id		机构id	【可空】
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/listFlowRole")
	@ResponseBody
	public RetMsg<?> listFlowRole(@RequestBody Map<String,String> paramMap) throws IOException {
		String roleId=ParameterUtil.getString(paramMap, "role_id", "");
		String roleName=ParameterUtil.getString(paramMap, "role_name", "");
		String memo=ParameterUtil.getString(paramMap, "memo", "");
		String instId=ParameterUtil.getString(paramMap, "inst_id", "");
		List<TtFlowRole> list = flowRoleService.searchFlowRole(roleId, roleName, memo, instId);
		List<Map<String,Object>> ret = new ArrayList<Map<String,Object>>();
		if(list!=null){
			for(TtFlowRole tfr : list){
				Map<String,Object> map = ParentChildUtil.ClassToHashMap(tfr);
				Map<String,Object> pMap = new HashMap<String,Object>();
				pMap.put("role_id",tfr.getRole_id());
				List<FlowRoleUserMapVo> userList = flowRoleService.listFlowRoleUserMap(pMap);
//				StringBuffer users = new StringBuffer();
//				if(userList!=null){
//					for(FlowRoleUserMapVo v : userList){
//						users.append(v.getUser_name()).append(",");
//					}
//				}
				map.put("user_list", CollectionUtil.joinCollectionProperty(userList,"user_name",","));
				ret.add(map);
			}
		}
		return RetMsgHelper.ok(ret);
	}
	
	/**
	 * 查询所有审批角色
	 * @param request
	 * map参数：
	 * 		role_id		角色id   【可空】
	 * 		role_name	角色名称  【可空】
	 * 		memo		备注		【可空】
	 * 		inst_id		机构id	【可空】
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/listFlowRolePage")
	@ResponseBody
	public RetMsg<?> listFlowRolePage(@RequestBody Map<String,Object> paramMap) throws IOException {
		Page<TtFlowRole> page = flowRoleService.searchFlowRolePage(paramMap);
		if(page!=null){
			for(TtFlowRole tfr : page.getResult()){
				Map<String,Object> pMap = new HashMap<String,Object>();
				pMap.put("role_id",tfr.getRole_id());
				List<FlowRoleUserMapVo> userList = flowRoleService.listFlowRoleUserMap(pMap);
//				StringBuffer users = new StringBuffer();
//				if(userList!=null){
//					for(FlowRoleUserMapVo v : userList){
//						users.append(v.getUser_name()).append(",");
//					}
//				}
				tfr.setUser_list(CollectionUtil.joinCollectionProperty(userList,"user_name",","));
			}
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 角色信息编辑（保存、修改）
	 * @param request
	 * map 参数：
	 * 		role_id		角色id
	 * 		role_name	角色名称 【非空】
	 * 		memo		备注
	 * 		inst_id		机构
	 * @throws IOException
	 */
	@RequestMapping(value = "/roleEdit")
	@ResponseBody
	public RetMsg<?> roleEdit(@RequestBody TtFlowRole flowRole) throws IOException {
//		String json = request.getBody();
//		TtFlowRole flowRole = JacksonUtil.readJson2Entity(json,	TtFlowRole.class);
		if(flowRole.getRole_id() == null || "".equals(flowRole.getRole_id())){
			flowRoleService.addNewFlowRole(flowRole);
		}else{
			flowRoleService.modifyFlowRole(flowRole);
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 角色信息删除
	 * @param request
	 * map参数：
	 * 		role_id	角色ID 
	 * @throws IOException
	 */
	@RequestMapping(value = "/roleDel")
	@ResponseBody
	public RetMsg<?> roleDel(@RequestBody Map<String,String> paramMap) throws IOException {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
		String roleId = ParameterUtil.getString(paramMap,"role_id","");
		if("".equals(roleId)){
			throw new RException("角色ID不能为空");
		}
		flowRoleService.deleteFlowRole(roleId);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 角色--用户对应关系修改
	 * @param request
	 * @throws IOException
	 */
	@RequestMapping(value = "/modifyRoleUserMap")
	@ResponseBody
	public RetMsg<?> modifyRoleUserMap(@RequestBody Map<String,String> paramMap) throws IOException {
//		String json = request.getBody();
//		Map<String,String> paramMap = JacksonUtil.readJson2EntityWithTypeReference(
//				json, new TypeReference<Map<String,String>>(){});
		String role_id = ParameterUtil.getString(paramMap,"role_id","");
		String user_ids = ParameterUtil.getString(paramMap,"user_ids","");
		if("".equals(role_id)){
			throw new RException("角色ID不能为空");
		}
		flowRoleService.modifyFlowRoleUserMap(role_id, user_ids);
		return RetMsgHelper.ok();
	}
	/**
	 * 角色--用户对应关系添加
	 * @param paramMap
	 * @throws IOException
	 * */
	@RequestMapping(value = "/addRoleUserMap")
	@ResponseBody
	public RetMsg<?> addRoleUserMap(@RequestBody Map<String,String> paramMap) throws IOException {
		String role_id = ParameterUtil.getString(paramMap,"role_id","");
		String user_ids = ParameterUtil.getString(paramMap,"user_ids","");
		if("".equals(role_id)){
			throw new RException("角色ID不能为空");
		}
		
		flowRoleService.modifyFlowRoleUserMap(role_id, user_ids);
		return RetMsgHelper.ok();
	}
	/**
	 * 角色--用户对应关系删除
	 * @param paramMap
	 * @throws IOException
	 * */
	@RequestMapping(value = "/deleteRoleUserMap")
	@ResponseBody
	public RetMsg<?> deleteRoleUserMap(@RequestBody Map<String,String> paramMap) throws IOException {
		String role_id = ParameterUtil.getString(paramMap,"role_id","");
		String user_ids = ParameterUtil.getString(paramMap,"user_ids","");
		if("".equals(role_id)){
			throw new RException("角色ID不能为空");
		}
		
		flowRoleService.deleteFlowRoleUserMap(role_id, user_ids);
		return RetMsgHelper.ok();
	}
	/**
	 * 转授权历史查询
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/pageFlowRoleUserMapHis")
	@ResponseBody
	public RetMsg<PageInfo<FlowRoleUserMapHisVo>> pageFlowRoleUserMapHis(@RequestBody Map<String,Object> paramMap) throws IOException {
		Page<FlowRoleUserMapHisVo> list = flowRoleService.pageFlowRoleUserMapHis(paramMap);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 审批角色权限查询
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/getFlowOperations")
	@ResponseBody
	public RetMsg<List<TtFlowOperation>> getFlowOperations(@RequestBody Map<String,Object> paramMap) throws IOException {
		List<TtFlowOperation> list = flowRoleService.getFlowOperations();
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 查询单个审批角色拥有的审批权限
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/getFlowRoleOperations")
	@ResponseBody
	public RetMsg<List<TtFlowRoleAuth>> getFlowRoleOperations(@RequestBody Map<String,Object> paramMap) throws IOException {
		String roleId = ParameterUtil.getString(paramMap, "roleId", "");
		JY.require(StringUtil.isNotEmpty(roleId), "审批角色ID不得为空.");
		List<TtFlowRoleAuth> list = flowRoleService.getFlowRoleOperations(roleId);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 保存审批角色权限
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/setFlowRoleOperations")
	@ResponseBody
	public RetMsg<Serializable> setFlowRoleOperations(@RequestBody Map<String,Object> paramMap) throws IOException {
		String roleId = ParameterUtil.getString(paramMap, "roleId", "");
		String opIds = ParameterUtil.getString(paramMap, "opIds", "");
		JY.require(StringUtil.isNotEmpty(roleId), "审批角色ID不得为空.");
		flowRoleService.setFlowRoleOperations(roleId,opIds);
		return RetMsgHelper.ok();
	}
	
	/**
	 * add by wangchen on 2017/5/22
	 * 根据登录用户查询审批权限
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(value = "/getUserFlowOperations")
	@ResponseBody
	public RetMsg<List<String>> getUserFlowOperations(@RequestBody Map<String,Object> paramMap) throws Exception {
		
		String task_id = ParameterUtil.getString(paramMap, "task_id", "");
		List<String> list = flowRoleService.getUserFlowOperations(task_id);
		return RetMsgHelper.ok(list);
	}
	
}