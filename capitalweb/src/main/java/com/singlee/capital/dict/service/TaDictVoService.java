package com.singlee.capital.dict.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.dict.model.TaDictVo;

public interface TaDictVoService {
	List<TaDictVo> searchTaDict(TaDictVo TaDict);
	public List<TaDictVo> getTaDictList(Map<String, Object> params);
	TaDictVo getTaDict(Map<String, Object> params, RowBounds rb);
	int insertTaDict(TaDictVo TaDict);
	int updateTaDict(Map<String, Object> TaDict);
	int deleteTaDict(TaDictVo TaDict);
	public Page<TaDictVo> getTaDictParent(Map<String, Object> params, RowBounds rb);
	public List<TaDictVo> getTaDictByDictId(Map<String, Object> params);
	
	List<TaDictVo> getSecondMT0001(Map<String, Object> map);
	List<TaDictVo> getThirdMT0001(Map<String, Object> map);
	
	List<TaDictVo> getTadictTree(String dictId);
}
