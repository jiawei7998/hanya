package com.singlee.capital.dict.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.singlee.capital.dict.model.TaDictItem;

public interface TaDictItemService {
	List<TaDictItem> searchTaDictItem(TaDictItem taDictItem);
	List<TaDictItem> getTaDictItemList(Map<String, Object> params);
	TaDictItem getTaDictItem(Map<String, Object> params, RowBounds rb);
	int insertTaDictItem(TaDictItem taDictItem);
	int updateTaDictItem(TaDictItem taDictItem);
	int deleteTaDictItem(TaDictItem taDictItem);
	void updateTaDictItem(Map<String, Object> params);
	void deleteTaDictItem(Map<String, Object> params);
}
