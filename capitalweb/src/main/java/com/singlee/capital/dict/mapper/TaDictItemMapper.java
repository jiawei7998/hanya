package com.singlee.capital.dict.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.dict.model.TaDictItem;

import tk.mybatis.mapper.common.Mapper;


public interface TaDictItemMapper extends Mapper<TaDictItem>{
	public List<TaDictItem> getTaDictItemList(Map<String, Object> params);
	public void updateTaDictItem(Map<String, Object> params);
	public void  deleteTaDictItem(Map<String, Object> params);
	public TaDictItem getTaDictItem(Map<String, Object> params);
	
}
