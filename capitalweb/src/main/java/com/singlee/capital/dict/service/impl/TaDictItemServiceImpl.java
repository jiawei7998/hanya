package com.singlee.capital.dict.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.dict.mapper.TaDictItemMapper;
import com.singlee.capital.dict.model.TaDictItem;
import com.singlee.capital.dict.service.TaDictItemService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaDictItemServiceImpl implements TaDictItemService{
	
	@Resource
	TaDictItemMapper taDictItemItemMapper;

	@Override
	public int deleteTaDictItem(TaDictItem TaDictItem) {
		return taDictItemItemMapper.delete(TaDictItem);
	}

	@Override
	public TaDictItem getTaDictItem(Map<String, Object> params, RowBounds rb) {
		return taDictItemItemMapper.getTaDictItem(params);
	}

	@Override
	public List<TaDictItem> getTaDictItemList(Map<String, Object> params) {
		return taDictItemItemMapper.getTaDictItemList(params);
	}

	@Override
	public int insertTaDictItem(TaDictItem taDictItem) {
		return taDictItemItemMapper.insert(taDictItem);
	}

	@Override
	public List<TaDictItem> searchTaDictItem(TaDictItem taDictItem) {
		return taDictItemItemMapper.select(taDictItem);
	}

	@Override
	public int updateTaDictItem(TaDictItem taDictItem) {
		return taDictItemItemMapper.updateByPrimaryKey(taDictItem);
	}

	@Override
	public void updateTaDictItem(Map<String, Object> params) {
		taDictItemItemMapper.updateTaDictItem(params);
	}

	@Override
	public void deleteTaDictItem(Map<String, Object> params) {
		taDictItemItemMapper.deleteTaDictItem(params);
	}

}
