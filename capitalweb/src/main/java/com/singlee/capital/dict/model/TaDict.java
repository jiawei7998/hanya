package com.singlee.capital.dict.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;


/**
 * 涉及的字典
 * 
 * @author
 */

@Entity
@Table(name = "TA_DICT")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TaDict implements Serializable {

	private static final long serialVersionUID = -8873918631756295031L;

	/** 字典编码 */
	private String dict_id;
	/** 字典名称 */
	private String dict_name;
	/** 字典所属模块 */
	private String dict_module;
	/** 字典描述 */
	private String dict_desc;
	/**
	 * 字典项列表 private Map<String,TaDictItem> itemMap;
	 */
	/**
	 * 数据库构造使用 private List<TaDictItem> dict_items;
	 */
	
	@Transient	/** 前台vo对象 */
	List<TaDictItem> dictItems = new ArrayList<TaDictItem>();


	public List<TaDictItem> getDictItems() {
		return dictItems;
	}

	public void setDictItems(List<TaDictItem> dictItems) {
		this.dictItems = dictItems;
	}

	public String getDict_id() {
		return dict_id;
	}

	public void setDict_id(String dictId) {
		dict_id = dictId;
	}

	public String getDict_name() {
		return dict_name;
	}

	public void setDict_name(String dictName) {
		dict_name = dictName;
	}

	public String getDict_desc() {
		return dict_desc;
	}

	public void setDict_desc(String dictDesc) {
		dict_desc = dictDesc;
	}

	public String getDict_module() {
		return dict_module;
	}

	public void setDict_module(String dict_module) {
		this.dict_module = dict_module;
	}


}
