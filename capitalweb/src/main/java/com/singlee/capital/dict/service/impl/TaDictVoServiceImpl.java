package com.singlee.capital.dict.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.TaDictItemService;
import com.singlee.capital.dict.service.TaDictVoService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaDictVoServiceImpl implements TaDictVoService{
	
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Resource
	TaDictItemService taDictItemService;

	@Override
	public int deleteTaDict(TaDictVo taDict) {
		int n = taDictVoMapper.delete(taDict);
		if(n>0){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("dict_id", taDict.getDict_id());
			taDictItemService.deleteTaDictItem(params);
		}
		return n;
	}

	@Override
	public TaDictVo getTaDict(Map<String, Object> params, RowBounds rb) {
		return taDictVoMapper.getTaDict(params);
	}

	@Override
	public List<TaDictVo> getTaDictList(Map<String, Object> params) {
		return taDictVoMapper.getTaDictVoList(params);
	}

	@Override
	public int insertTaDict(TaDictVo taDict) {
		return taDictVoMapper.insert(taDict);
	}

	@Override
	public List<TaDictVo> searchTaDict(TaDictVo taDict) {
		return taDictVoMapper.select(taDict);
	}

	@Override
	public int updateTaDict(Map<String, Object> taDict) {
		return taDictVoMapper.updateTaDict(taDict);
	}

	@Override
	public Page<TaDictVo> getTaDictParent(Map<String, Object> params, RowBounds rb) {
		return taDictVoMapper.getTaDictParent(params,rb);
	}

	@Override
	public List<TaDictVo> getTaDictByDictId(Map<String, Object> params) {
		
		return taDictVoMapper.getTaDictByDictId(params);
	}
	
	@Override
	public List<TaDictVo> getSecondMT0001(Map<String, Object> map) {
		return taDictVoMapper.getSecondMT0001(map);
	}

	@Override
	public List<TaDictVo> getThirdMT0001(Map<String, Object> map) {
		return taDictVoMapper.getThirdMT0001(map);
	}
	
	@Override
	public List<TaDictVo> getTadictTree(String dictId) {
		return taDictVoMapper.getTadictTree(dictId);
	}

}
