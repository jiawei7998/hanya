package com.singlee.capital.dict.mapper;

import java.util.List;

import com.singlee.capital.dict.model.TaDict;

import tk.mybatis.mapper.common.Mapper;


public interface TaDictGetMapper extends Mapper<TaDict>{
	List<TaDict> loadAllDictionary();
}
