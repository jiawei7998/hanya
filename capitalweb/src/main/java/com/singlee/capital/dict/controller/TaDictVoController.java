package com.singlee.capital.dict.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.dict.model.TaDictItem;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.dict.service.TaDictItemService;
import com.singlee.capital.dict.service.TaDictVoService;
import com.singlee.capital.system.controller.CommonController;


@Controller
@RequestMapping(value = "/TaDictController")
public class TaDictVoController extends CommonController {
	
	@Resource
	TaDictVoService taDictService;
	@Resource
	TaDictItemService taDictItemService;
	@Resource
	DictionaryGetService dictionaryGetService;

	
	@ResponseBody
	@RequestMapping(value = "/getTaDictParent")
	public RetMsg<PageInfo<TaDictVo>> getTaDictParent(@RequestBody Map<String,Object> map){
		Page<TaDictVo> page  = taDictService.getTaDictParent(map, ParameterUtil.getRowBounds(map));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTaDictByDictId")
	public RetMsg<List<TaDictVo>> getTaDictByDictId(@RequestBody Map<String,Object> map){
		List<TaDictVo> page  = taDictService.getTaDictByDictId(map);
		page = TreeUtil.mergeChildrenList(page, "-");
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/getTaDictByDictIdForList")
	public RetMsg<List<TaDictVo>> getTaDictByDictIdForList(@RequestBody Map<String,Object> map){
		List<TaDictVo> page  = taDictService.getTaDictByDictId(map);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/searchTaDict")
	public RetMsg<List<TaDictVo>> searchTaDict(@RequestBody Map<String,Object> map){
		List<TaDictVo> page  = taDictService.getTaDictList(map);
		page = TreeUtil.mergeChildrenList(page, "-");
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTaDictItem")
	public RetMsg<List<TaDictItem>> searchTaDictItem(@RequestBody Map<String,Object> map){
		List<TaDictItem> page  = taDictItemService.getTaDictItemList(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/insertTaDictVo")
	public RetMsg<Serializable> insertTaDictVo(@RequestBody TaDictVo map){
		taDictService.insertTaDict(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/insertTaDictItem")
	public RetMsg<Serializable> insertTaDictItem(@RequestBody TaDictItem map){
		taDictItemService.insertTaDictItem(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateTaDictItem")
	public RetMsg<Serializable> updateTaDictItem(@RequestBody Map<String,Object> map){
		taDictItemService.updateTaDictItem(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateTaDictVo")
	public RetMsg<Serializable> updateTaDictVo(@RequestBody Map<String, Object> map){
		taDictService.updateTaDict(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteTaDict")
	public RetMsg<Serializable> deleteTaDict(@RequestBody TaDictVo map){
		taDictService.deleteTaDict(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteTaDictItem")
	public RetMsg<Serializable> deleteTaDictItem(@RequestBody Map<String,Object> map){
		taDictItemService.deleteTaDictItem(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDictVoModel")
	public RetMsg<TaDictVo> getDictVoModel(@RequestBody Map<String,Object> map){
		TaDictVo vo = taDictService.getTaDict(map, null);
		return RetMsgHelper.ok(vo);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDictItmeModel")
	public RetMsg<TaDictItem> getDictItmeModel(@RequestBody Map<String,Object> map){
		TaDictItem vo = taDictItemService.getTaDictItem(map, null);
		return RetMsgHelper.ok(vo);
	}
	
	/**
	 * 汪泽峰 2017-6-5
	 * 获取所有国别 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllCountry")
	public RetMsg<List<TaDictVo>> getAllCountry(@RequestBody Map<String,Object> map){
		List<TaDictVo> list = taDictService.getTaDictList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 一次获取多个个字典的完整列表
	 * @throws IOException 
	 * 
	 */
	@RequestMapping(value = "/dictionary.js")
	public void selectAllDict(HttpServletRequest request, HttpServletResponse response) throws IOException  {
		response.setContentType("application/javascript;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print("CommonUtil.serverData.dictionary=");
		String jsonStr = dictionaryGetService.getDictionaryArrayJsonString();
		out.print(jsonStr);
		out.close();
	}
	
	/**
	 * 刷新数据字典列表
	 * @throws IOException 
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/refreshDictionary")
	public RetMsg<Serializable> refreshDictionary(@RequestBody Map<String,Object> map) throws IOException
	{
		//更新缓存暂时无用<目前主要界面使用dictionary.js>
		dictionaryGetService.updateDictToEhCache();
		//更新数据字典JS文件
		dictionaryGetService.updateDictionaryArrayJsonStringCache();
		return RetMsgHelper.ok();
	}
	
	/**
	 * 一次获取多个个字典的完整列表
	 * @throws IOException 
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/getTaDictByCodeAndKeyMap")
	public RetMsg<Serializable> getTaDictByCodeAndKeyMap(@RequestBody Map<String,Object> map, HttpServletResponse response) throws IOException  {
        Object s = map.get("dicts");//获取的前台数组
        List<String> list = JSONArray.parseArray(s.toString(), String.class);
        String jsonStr = dictionaryGetService.getTaDictByCodeAndKeyMap(list);
    	return RetMsgHelper.ok(jsonStr);
	}
	
	/**
	 * 获取二级类别
	 * @param map  前一位字母
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSecondMT0001")
	public RetMsg<List<TaDictVo>> getSecondMT0001(@RequestBody Map<String,Object> map){
		List<TaDictVo> list = taDictService.getSecondMT0001(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获取三级类别
	 * @param map  前三位字母
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getThirdMT0001")
	public RetMsg<List<TaDictVo>> getThirdMT0001(@RequestBody Map<String,Object> map){
		List<TaDictVo> list = taDictService.getThirdMT0001(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTadictTree")
	public List<Map<String,String>> getTadictTree(@RequestBody Map<String,Object> map){
		String dictId=map.get("dictId").toString();
		List<TaDictVo> list = taDictService.getTadictTree(dictId);
		List<Map<String,String>> list1 = new ArrayList<Map<String,String>>();
		for(TaDictVo t :list){
			Map<String,String> map1 = new HashMap<String,String>();
			map1.put("id",t.getDict_key());
			map1.put("text", t.getDict_value());
			map1.put("pid", t.getDict_parentid());
			list1.add(map1);
		}
		return list1;
	}
}
