package com.singlee.capital.dict.service;

import java.util.List;

import com.singlee.capital.dict.model.TaDictVo;

public interface DictionaryGetService {
	public String getDictionaryJsonString();
	public List<TaDictVo> getDictToEhCache();
	public List<TaDictVo> updateDictToEhCache();
	public List<TaDictVo> getTaDictByCode(String code);
	public TaDictVo getTaDictByCodeAndKey(String code,String key);
	public TaDictVo getTaDictByCodeAndValue(String code,String value);
	
	public String getDictionaryArrayJsonString();
	
	public String updateDictionaryArrayJsonStringCache();
	
	public String getTaDictByCodeAndKeyMap(List<String> keyValues);
}
