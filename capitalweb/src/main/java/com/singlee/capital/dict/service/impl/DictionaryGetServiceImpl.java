package com.singlee.capital.dict.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dict.mapper.TaDictGetMapper;
import com.singlee.capital.dict.model.TaDict;
import com.singlee.capital.dict.model.TaDictItem;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.dict.service.TaDictVoService;

@Service
public class DictionaryGetServiceImpl implements DictionaryGetService {
	private static final Pattern p = Pattern.compile("\\d{6}");

	private static final String cacheName = "com.singlee.capital.system.dictionary";

	@Autowired
	private TaDictGetMapper tdm;
	@Resource
	private TaDictVoService taDictVoService;

	@Override
	public String getDictionaryJsonString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		List<TaDict> list = tdm.loadAllDictionary();
		for (TaDict d : list) {
			sb.append(getJsonByTaDict(d, null));
			sb.append(",\n");
			Matcher m = p.matcher(d.getDict_id());
			if (m.matches()) {
				sb.append(getJsonByTaDict(d, d.getDict_name()));
				sb.append(",");
			}
		}
		sb.append("}");
		return sb.toString();
	}

	private String getJsonByTaDict(TaDict clz, String name) {
		StringBuffer sb = new StringBuffer();
		sb.append("\"");
		if (name != null) {
			sb.append(name);
		} else {
			sb.append(clz.getDict_id());
		}
		sb.append("\":");
		sb.append("{");
		List<TaDictItem> fs = clz.getDictItems();
		boolean first = true;
		for (TaDictItem f : fs) {
			try {
				if (!first) {
					sb.append(",");
				} else {
					first = false;
				}
//				String folder = f.getDict_folder();
				// "CM_SP_BND":{"text":"普通债券","folder":["1","2","5"]}
				sb.append("\"").append(f.getDict_key()).append("\":{\"text\":\"").append(f.getDict_value())
						.append("\",\"folder\":\"").append("1").append("\",\"parentid\":\"")
						.append(f.getDict_parentid()).append("\"}");
			} catch (Exception e) {
				JY.raiseRException(clz.getDict_name() + " dictionary error!" + e.getMessage());
			}
		}
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 将数据字典加载到缓存中
	 */
	@Override
	@Cacheable(value = cacheName, key = "'dictCache'")
	public List<TaDictVo> getDictToEhCache() {
		// 获取所有字典项数据
		List<TaDictVo> list = taDictVoService.getTaDictList(null);
		return list;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */

	/**
	 * 为了 代理 能进入
	 * 
	 * @return
	 */
	private DictionaryGetServiceImpl myself() {
		return SpringContextHolder.getBean(DictionaryGetServiceImpl.class);
	}

	/**
	 * 更新字典项
	 * 
	 * @return
	 */
	@Override
	@CachePut(value = cacheName,key = "'dictCache'")
	public List<TaDictVo> updateDictToEhCache() {
		// 获取所有字典项数据
		List<TaDictVo> list = taDictVoService.getTaDictList(null);
		return list;
	}

	/**
	 * 获取字典项
	 * 
	 * @param code
	 * @return
	 */
	@Override
	public List<TaDictVo> getTaDictByCode(String code) {
		List<TaDictVo> list = myself().getDictToEhCache();
		List<TaDictVo> relist = new ArrayList<TaDictVo>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getDict_id().equals(code)) {
				relist.add(list.get(i));
			}
		}
		return relist;
	}

	/**
	 * 获取字典项
	 * 
	 * @param code
	 * @return
	 */
	@Override
	public TaDictVo getTaDictByCodeAndKey(String code, String key) {
		List<TaDictVo> list = myself().getDictToEhCache();
		List<TaDictVo> relist = new ArrayList<TaDictVo>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getDict_id().equals(code) && list.get(i).getDict_key().equals(key)) {
				relist.add(list.get(i));
			}
		}
		return relist.size() > 0 ? relist.get(0) : null;
	}

	/**
	 * 获取字典项
	 * 
	 * @param code
	 * @return
	 */
	@Override
	public TaDictVo getTaDictByCodeAndValue(String code, String value) {
		List<TaDictVo> list = myself().getDictToEhCache();
		List<TaDictVo> relist = new ArrayList<TaDictVo>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getDict_id().equals(code) && list.get(i).getDict_value().equals(value)) {
				relist.add(list.get(i));
			}
		}
		return relist.size() > 0 ? relist.get(0) : null;
	}

	@Override
	@Cacheable(value = cacheName, key="'dictJsonCache'")
	public String getDictionaryArrayJsonString() {
		return getDictionaryArrayJson();
	}

	// 更新字典JS
	@Override
	@CachePut(value = cacheName, key="'dictJsonCache'")
	public String updateDictionaryArrayJsonStringCache() {
		return getDictionaryArrayJson();
	}

	/****
	 * 获取数据字典JS字符串
	 * 
	 * @return
	 */
	private String getDictionaryArrayJson() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		List<TaDict> list = tdm.loadAllDictionary();
		int count = 0;
		for (TaDict d : list) {
			count++;
			sb.append(getJsonArrayByTaDict(d, null));
			if (count == list.size()) {
				sb.append("\n");
			} else {
				sb.append(",\n");
			}

			Matcher m = p.matcher(d.getDict_id());
			if (m.matches()) {
				sb.append(getJsonArrayByTaDict(d, d.getDict_name()));
				if (count != list.size()) {
					sb.append(",");
				}
			}
		}
		sb.append("}");
		JY.info("获取数据字典JS字符串初始化完成");
		return sb.toString();
	}

	// "a":[{"id":"是","text":1},"0":{"id":"否","text":1}]
	private String getJsonArrayByTaDict(TaDict clz, String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("\"");
		if (name != null) {
			sb.append(name);
		} else {
			sb.append(clz.getDict_id());
		}
		sb.append("\":");
		sb.append("[");
		List<TaDictItem> fs = clz.getDictItems();
		boolean first = true;
		for (TaDictItem f : fs) {
			try {
				if (!first) {
					sb.append(",");
				} else {
					first = false;
				}
				sb.append("{\"id\":\"")
						.append(f.getDict_key())
						.append("\",\"text\":\"")
						.append(f.getDict_value())
						.append("\"}");
			} catch (Exception e) {
				JY.raiseRException(clz.getDict_name() + " dictionary error!" + e.getMessage());
			}
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public String getTaDictByCodeAndKeyMap(List<String> keyValues) {
		TaDictVo dictVo = null;
		String[] keyValue = null;
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		for (String str : keyValues) {
			keyValue = str.split("#");
			dictVo = getTaDictByCodeAndKey(keyValue[1], keyValue[2]);
			if (dictVo != null) {
				sb.append(keyValue[0]).append(":");
				sb.append("[{id:'").append(dictVo.getDict_key()).append("',text:'").append(dictVo.getDict_value())
						.append("'}],");
			}
		} // end for
		return sb.substring(0, sb.length() - 1) + "}";
	}

}
