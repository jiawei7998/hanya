package com.singlee.capital.dict.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.singlee.capital.common.util.TreeInterface;


/**
 * 字典表TA_DICT
 * @author SL_LXY
 *
 */
@Entity
@Table(name = "TA_DICT")
public class TaDictVo implements Serializable,TreeInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String dict_id; //字典项关键字
	private String dict_name;//字典项名称
	private String dict_module;//字典项模块
	private String dict_desc;//字典项描述
	private String dict_parentid;//字典父节点
	private String dict_key;//代码值
	private String dict_value;//代码名称
	private String dict_level;//代码级别
	private String is_active;//是否启用
	
	@Transient
	private List<TaDictVo> children;
	
	public List<TaDictVo> getChildren() {
		return children;
	}
	public void setChildren(List<TaDictVo> children) {
		this.children = children;
	}
	public String getDict_id() {
		return dict_id;
	}
	public void setDict_id(String dictId) {
		dict_id = dictId;
	}
	public String getDict_name() {
		return dict_name;
	}
	public void setDict_name(String dictName) {
		dict_name = dictName;
	}
	public String getDict_module() {
		return dict_module;
	}
	public void setDict_module(String dictModule) {
		dict_module = dictModule;
	}
	public String getDict_desc() {
		return dict_desc;
	}
	public void setDict_desc(String dictDesc) {
		dict_desc = dictDesc;
	}
	public String getDict_parentid() {
		return dict_parentid;
	}
	public void setDict_parentid(String dictParentid) {
		dict_parentid = dictParentid;
	}
	public String getDict_key() {
		return dict_key;
	}
	public void setDict_key(String dictKey) {
		dict_key = dictKey;
	}
	public String getDict_value() {
		return dict_value;
	}
	public void setDict_value(String dictValue) {
		dict_value = dictValue;
	}
	public String getDict_level() {
		return dict_level;
	}
	public void setDict_level(String dictLevel) {
		dict_level = dictLevel;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String isActive) {
		is_active = isActive;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TaDictVo>) list);
	}
	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	@Override
	public String getIconCls() {
		return "";
	}
	@Override
	public String getId() {
		return dict_key;
	}
	@Override
	public String getParentId() {
		return getDict_parentid();
	}
	@Override
	public int getSort() {
		return StringUtils.length(getDict_id());
	}
	@Override
	public String getText() {
		return dict_key;
	}
	@Override
	public String getValue() {
		return dict_value;
	}
	
}
