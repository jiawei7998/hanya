package com.singlee.capital.dict.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;


import com.github.pagehelper.Page;
import com.singlee.capital.dict.model.TaDictVo;
import tk.mybatis.mapper.common.Mapper;


public interface TaDictVoMapper extends Mapper<TaDictVo>{
	public List<TaDictVo> getTaDictVoList(Map<String, Object> params);
	
	public Page<TaDictVo> getTaDictParent(Map<String, Object> params, RowBounds rb);
	
	public List<TaDictVo> getTaDictByDictId(Map<String, Object> params);
	
	TaDictVo getTaDict(Map<String, Object> params);
	
	int updateTaDict(Map<String, Object> params);
	
	public List<TaDictVo> getSecondMT0001(Map<String, Object> map);

	public List<TaDictVo> getThirdMT0001(Map<String, Object> map);
	
	public List<String> getTextByDictId(@Param("dictId")String dictId);
	
	public List<TaDictVo> getTadictTree(String dictId);
}
