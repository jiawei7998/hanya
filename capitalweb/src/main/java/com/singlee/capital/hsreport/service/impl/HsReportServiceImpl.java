package com.singlee.capital.hsreport.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.hsreport.service.HsReportService;
import com.singlee.capital.hsreport.util.ExportUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlHsReportBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.financial.opics.IHsReportServer;
import com.singlee.financial.pojo.component.RetStatusEnum;

@Service
public class HsReportServiceImpl implements HsReportService {

	@Autowired
	IHsReportServer reportServer;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public SlOutSonBean exportData(HttpServletResponse rep,
			SlHsReportBean hsReport) {
		SlOutSonBean ssb = null;
		try {
			hsReport.setOper(SlSessionHelper.getUserId());
			// hsReport.setBr(SlSessionHelper.getInstitutionId());

			// 1, 通过opics接口生成并返回excel数据
			ssb = reportServer.getHsReportData(hsReport);

			if (ssb.getRetStatus().equals(RetStatusEnum.S)) {
				// 2, 通过export方法生成excel
				List<Map> list = (List<Map>) ssb.getDataMap().get("data");
				ssb = ExportUtil.export(hsReport.getExcelname(), hsReport.getTemplate(), list,false);
				if (ssb.getRetStatus().equals(RetStatusEnum.S)) {
					// 3,输出excel
					byte[] b = (byte[]) ssb.getDataMap().get("data");
					ExportUtil.download(rep, hsReport.getExcelname(), b, hsReport.getExporttype());
				}
			}

			return ssb;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new SlOutSonBean(RetStatusEnum.F, "error.hsReport.0006", "报表导出异常.." + e);
		}
	}

}
