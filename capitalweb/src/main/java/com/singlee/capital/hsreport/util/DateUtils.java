package com.singlee.capital.hsreport.util;

import java.text.ParseException; 
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * 项目名称：BJGJS
 * 类名称：DateUtils
 * 类描述：日期工具类
 * 创建人：liyd
 * 创建时间：2014-4-2上午11:12:24
 *
 * @version
 */

public class DateUtils {
	
	public final static String FMT_DATE_DEFAULT = "yyyy-MM-dd";
	public final static String FMT_TIME_DEFAULT = "HH:mm:ss";
	public final static String FMT_DATETIME_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	
	public static String nowDate() {
		return nowDate("-");
	}
	
	/**
	 * 计算两个时间相差多少单位
	 * @param type 单位类型 天-D,小时-H,分钟-M,秒-S
	 * @param startTime
	 * @param endTime
	 * @param format
	 * @return
	 */
	public static long dateDiff(String type,String startTime,String endTime,String format){
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000*24*60*60; //一天的毫秒数
		long nh = 1000*60*60; //一小时的毫秒数
		long nm = 1000*60; //一分钟的毫秒数
		long ns = 1000; //一秒钟的毫秒数
		
		long diff = 0;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		long day = diff/nd;
		long hour = diff%nd/nh;
		long min = diff%nd%nh/nm;
		long sec = diff%nd%nh%nm/ns;
		long result = 0;
		
		if("D".equals(type)){
			result = day;
		}else if("H".equals(type)){
			result = hour;
		}else if("M".equals(type)){
			result = min;
		}else if("S".equals(type)){
			result = sec;
		}
		
		return result;
	}

	public static Date getUtilDate(String dateStr, String format) throws ParseException {
		if (dateStr == null || dateStr.length() == 0) {
			return null;
		}
		format = format == null ? FMT_DATETIME_DEFAULT : format;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(dateStr);
	}
	public static String nowDate(String sper) {
		GregorianCalendar Time = new GregorianCalendar();

		String s_nowD = "";
		String s_nowM = "";
		if (sper == null) {
			sper = "-";
		} else {
			sper = sper.trim();
		}

		int nowY = Time.get(1);
		int nowM = Time.get(2) + 1;
		s_nowM = StringUtils.leftPad(nowM, 2, '0');

		int nowD = Time.get(5);
		s_nowD = StringUtils.leftPad(nowD, 2, '0');

		String result = nowY + sper + s_nowM + sper + s_nowD;

		return result;
	}

	public static String nowChDate() {
		GregorianCalendar Time = new GregorianCalendar();
		int nowY = Time.get(1);
		int nowM = Time.get(2) + 1;
		int nowD = Time.get(5);

		return nowY + "年" + nowM + "月" + nowD + "日";
	}

	public static String nowTime() {
		return nowTime(":");
	}

	public static String nowTime(String sperate) {
		GregorianCalendar Time = new GregorianCalendar();

		int i_hour = Time.get(10);
		int i_tag = Time.get(9);
		if ((i_tag == 1) && (i_hour < 12)) {
			i_hour += 12;
		}
		int i_minutes = Time.get(12);
		int i_seconds = Time.get(13);
		String s_time = "";
		s_time = StringUtils.leftPad(i_hour, 2, '0');
		s_time = s_time + sperate;

		s_time = s_time + StringUtils.leftPad(i_minutes, 2, '0');
		s_time = s_time + sperate;

		s_time = s_time + StringUtils.leftPad(i_seconds, 2, '0');

		return s_time;
	}

	public static String nowChTime() {
		GregorianCalendar Time = new GregorianCalendar();

		int i_hour = Time.get(10);
		int i_tag = Time.get(9);
		if ((i_tag == 1) && (i_hour < 12)) {
			i_hour += 12;
		}
		int i_minutes = Time.get(12);
		int i_seconds = Time.get(13);

		return i_hour + "时" + i_minutes + "分" + i_seconds + "秒";
	}

	public static String nowDateTime() {
		String s_DateTime = "";

		s_DateTime = nowDateTime("-", " ", ":");

		return s_DateTime;
	}

	public static String nowDateTime(String dateSpe, String midSpe,
			String timeSpe) {
		String s_DateTime = "";
		if (midSpe == null) {
			midSpe = " ";
		}
		s_DateTime = nowDate(dateSpe) + midSpe + nowTime(timeSpe);
		return s_DateTime;
	}

	public static String nowChDateTime() {
		String s_DateTime = "";

		s_DateTime = nowChDate() + " " + nowChTime();

		return s_DateTime;
	}

	public static String getNextDateOfDays(String date, int days) {
		date = formatDate(date, "/");
		@SuppressWarnings("deprecation")
		Date dt = new Date(date);

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dt);
		cal.add(5, days);
		int nowY = cal.get(1);
		int nowM = cal.get(2) + 1;
		int nowD = cal.get(5);
		return nowY + StringUtils.leftPad(nowM, 2, '0')
				+ StringUtils.leftPad(nowD, 2, '0');
	}

	public static String getPreDateOfDays(String date, int days) {
		return getNextDateOfDays(date, -1 * days);
	}

	public static String formatDateTime(String dateTime) {
		return formatDateTime(dateTime, "-", " ", ":");
	}

	public static String formatDateTime(String dateTime, String dateSpe,
			String midSpe, String timeSpe) {
		String s = dateTime;
		if ((StringUtils.isNotEmpty(dateTime)) && (dateTime.length() >= 14)) {
			if (dateSpe == null) {
				dateSpe = "-";
				}
			else {
				dateSpe = dateSpe.trim();
			}
			if (midSpe == null) {
				midSpe = " ";
			}
			if (timeSpe == null) {
				timeSpe = ":";
				}
			else {
				timeSpe = timeSpe.trim();
			}
			s = dateTime.substring(0, 4) + dateSpe + dateTime.substring(4, 6)
					+ dateSpe + dateTime.substring(6, 8) + midSpe
					+ dateTime.substring(8, 10) + timeSpe
					+ dateTime.substring(10, 12) + timeSpe
					+ dateTime.substring(12, 14);

			if (dateTime.length() == 17) {
				s = s + timeSpe + dateTime.substring(14, 17);
			}
		}

		return s;
	}

	public static String formatChDateTime(String dateTime) {
		String s = "";
		if ((StringUtils.isNotEmpty(dateTime)) && (dateTime.length() >= 14)) {
			s = dateTime.substring(0, 4) + "年" + dateTime.substring(4, 6) + "月"
					+ dateTime.substring(6, 8) + "日 "
					+ dateTime.substring(8, 10) + "时"
					+ dateTime.substring(10, 12) + "分"
					+ dateTime.substring(12, 14) + "秒";

			if (dateTime.length() == 17) {
				s = s + dateTime.substring(14, 17) + "毫秒";
			}
		}
		return s;
	}

	public static String formatDate(String date) {
		return formatDate(date, "-");
	}

	public static String formatDate(String date, String dateSpe) {
		String s = date;
		if ((StringUtils.isNotEmpty(date)) && (date.length() == 8)) {
			if (dateSpe == null) {
				dateSpe = "-";
				}
			else {
				dateSpe = dateSpe.trim();
				}
			s = date.substring(0, 4) + dateSpe + date.substring(4, 6) + dateSpe
					+ date.substring(6, 8);
		}
		return s;
	}

	public static String formatChDate(String date) {
		String s = date;
		if ((StringUtils.isNotEmpty(date)) && (date.length() == 8)) {
			s = date.substring(0, 4) + "年" + date.substring(4, 6) + "月"
					+ date.substring(6, 8) + "日";
		}
		return s;
	}

	public static String formatTime(String time) {
		return formatTime(time, ":");
	}

	public static String formatTime(String time, String timeSpe) {
		String s = time;
		if ((StringUtils.isNotEmpty(time)) && (time.length() >= 6)) {
			if (timeSpe == null) {
				timeSpe = ":";
				}
			else {
				timeSpe = timeSpe.trim();
				}
			s = time.substring(0, 2) + timeSpe + time.substring(2, 4) + timeSpe
					+ time.substring(4, 6);

			if (time.length() == 9) {
				s = s + timeSpe + time.substring(6, 9);
			}
		}
		return s;
	}

	public static String formatChTime(String time) {
		String s = time;
		if ((StringUtils.isNotEmpty(time)) && (time.length() >= 6)) {
			s = time.substring(0, 2) + "时" + time.substring(2, 4) + "分"
					+ time.substring(4, 6) + "秒";

			if (time.length() == 9) {
				s = s + time.substring(6, 9) + "毫秒";
			}
		}
		return s;
	}

	public static String normalizeDate(String date) {
		String s = date;

		if ((StringUtils.isNotEmpty(date)) && (date.length() != 8)) {
			s = date.replaceAll("-", "");
		}

		return s;
	}

	public static String normalizeDateTime(String dateTime) {
		String s = dateTime;

		if ((StringUtils.isNotEmpty(dateTime)) && (dateTime.length() != 14)) {
			s = dateTime.replaceAll("-", "");
			s = s.replaceAll(":", "");
			s = s.replaceAll(" ", "");
		}

		return s;
	}

	public static double getNumberOfDaysTimes(String fromDate, String toDate) {
		String strFromDate = "";
		String strToDate = "";

		if (fromDate.length() == 8) {
			strFromDate = formatDate(fromDate, "/");
			}
		else if (fromDate.length() == 14) {
			strFromDate = formatDateTime(fromDate, "/", " ", ":");
		}
		if (toDate.length() == 8) {
			strToDate = formatDate(toDate, "/");
			}
		else if (toDate.length() == 14) {
			strToDate = formatDateTime(toDate, "/", " ", ":");
		}
		@SuppressWarnings("deprecation")
		Date dt1 = new Date(strToDate);
		@SuppressWarnings("deprecation")
		Date dt2 = new Date(strFromDate);
		double days = dt1.getTime() - dt2.getTime();
		return days / 60.0D / 60.0D / 1000.0D / 24.0D;
	}

	public static int getNumberOfDays(String fromDate, String toDate) {
		double days = getNumberOfDaysTimes(fromDate, toDate);

		return (int) days;
	}

	public static double getDecimalDays(double days, int workHours) {
		double result = 0.0D;

		if ((int) days == days) {
			return days;
		}
		double hours = workHours / 48.0D;
		int idays = (int) days;
		double d = idays + hours;
		if (days >= d) {
			result = idays + 1.0D;
		}
		else {
			result = idays + 0.5D;
		}
		return result;
	}
	
	/**
	 * 转换日期 Date -> 20080101
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期的字符串格式如yyyyMMdd
	 * @return 日期的字符串形式如yyyyMMdd
	 */
	public static String getDateStr(Date date, String format) {
		if (date == null) {
			return null;
			}
		format = format == null ? "yyyy-MM-dd HH:mm:ss" : format;
		if ("CNS".equals(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String[] str = sdf.format(date).split("-");
			return str[0] + "年" + str[1] + "月" + str[2] + "日";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	 public static Date strToDate(String strDate) {
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		 ParsePosition pos = new ParsePosition(0);
		 Date strtodate = formatter.parse(strDate, pos);
		 
		 return strtodate;
		 
	 } 
	 
	 /**
	  * 得到一个时间延后或前移几天的时间,nowdate为时间,delay为前移或后延的天数
	  */
	 public static String getNextDay(String nowdate, int delay){
		 try{
			 SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			 String mdate = "";
			 Date d = strToDate(nowdate);
			 long myTime = (d.getTime() / 1000) + delay * 24 * 60 * 60;
			 d.setTime(myTime * 1000);
			 mdate = format.format(d);
	 
			 return mdate;
		 }catch(Exception e){
			 return "";
		 }
	 } 
	 
	 public static int daysBetween(String smdate,String bdate) throws ParseException{  
	        
		 SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");  
		 Calendar cal = Calendar.getInstance();    
		 cal.setTime(sdf.parse(smdate));    
		 long time1 = cal.getTimeInMillis();                 
		 
		 cal.setTime(sdf.parse(bdate));    
		 long time2 = cal.getTimeInMillis();         
		 
		 long between_days = (time2-time1)/(1000*3600*24);  
	            
		 return Integer.parseInt(String.valueOf(between_days));     
	   
	 }  
	 
	 /**
	  * 获取一个小时前的时间
	  * @param datetime yyyyMMddHHmm
	  * @return
	  */
	 public static String getOneHoursAgoTime(String datetime){
		 String oneHourAgoTime = "";
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		 
		 try {
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(sdf.parse(datetime));
		 cal.add(Calendar.HOUR, -1);
		 
		 oneHourAgoTime = sdf.format(cal.getTime());
		 
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 return oneHourAgoTime;
	 }
	 
	public static void main(String[] args) throws Exception {
//		System.out.println(formatChTime("121825999"));
//		System.out.println(formatTime("121825999"));
//		System.out.println(formatDateTime("20090330121825999"));
//		System.out.println(formatChDateTime("20090330121825999"));
//		
//		System.out.println("convert = " + getDateStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
//		System.out.println(nowChDate()+nowChTime());
//		
//		System.out.println("ddd = " + DateUtils.getNextDay("20131010",0));
//		
//		System.out.println("days = " + daysBetween("20130925","20131010"));
		
//		System.out.println(getOneHoursAgoTime("20130301000032"));
//		
//		System.out.println(nowDateTime());
	
		System.out.println(formatDate("20131128", "-"));
	
	}
	
	//昨天日期
	public static String getyesterday(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		return yesterday;
	}
}