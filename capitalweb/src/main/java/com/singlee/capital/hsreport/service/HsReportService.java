package com.singlee.capital.hsreport.service;

import javax.servlet.http.HttpServletResponse;

import com.singlee.financial.bean.SlHsReportBean;
import com.singlee.financial.bean.SlOutSonBean;

public interface HsReportService {
	SlOutSonBean exportData(HttpServletResponse rep, SlHsReportBean hsReport);
}
