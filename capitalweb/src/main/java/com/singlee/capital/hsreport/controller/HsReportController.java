package com.singlee.capital.hsreport.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.hsreport.service.HsReportService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlHsReportBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.financial.page.PageInfo;

@Controller
@RequestMapping(value = "/HsReportController")
public class HsReportController extends CommonController {

	@Autowired
	private HsReportService service;

	/**
	 * 导出
	 * 
	 * @param
	 * @return @
	 */
	@ResponseBody
	@RequestMapping(value = "/export")
	public SlOutSonBean export(SlHsReportBean hsReport,HttpServletRequest request, HttpServletResponse response) {
		return service.exportData(response, hsReport);
	}
	
	
	/**
	 * 交易台账查询
	 * 
	 * */
	/*@ResponseBody
	@RequestMapping(value = "/searchgageList")
	public RetMsg<PageInfo<Gage>> searchAcupList(@RequestBody Map<String, String> params) {
		// 调用总账发送接口
		PageInfo<Gage> page = service.getGageList(params);
		return RetMsgHelper.ok(page);
	}
	*/

}
