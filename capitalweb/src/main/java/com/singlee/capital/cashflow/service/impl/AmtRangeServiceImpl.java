package com.singlee.capital.cashflow.service.impl;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.service.AmtRangeService;
@Service
public class AmtRangeServiceImpl implements AmtRangeService {
	@Autowired
	TdAmtRangeMapper amtRangeMapper;
	@Override
	public Page<TdAmtRange> queryAmtRangesByDealNo(
			Map<String, Object> params, RowBounds rb) {
		// TODO Auto-generated method stub
		//refNo
		return this.amtRangeMapper.pageAmtRangesByDealNo(params, rb) ;
	}

}
