package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 真实交易对应的本金计划表
 * @author SINGLEE
 *
 */
@Entity
@Table(name = "TM_APPROVE_CAPITAL")
public class TmApproveCapital implements Serializable,Comparable<TmApproveCapital> {
	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_CAPITAL.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	
	
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	private String repaymentSdate;//计划还本日期
	private String cfType;// 现金流类型
	private String payDirection;// 收付方向
	private int seqNumber;// 序列
	private double repaymentSamt;// 现金流事件
	private String dealNo;
	private int version;
	private String cfEvent;
	
	public String getCfEvent() {
		return cfEvent;
	}
	public void setCfEvent(String cfEvent) {
		this.cfEvent = cfEvent;
	}
	public String getRepaymentSdate() {
		return repaymentSdate;
	}
	public void setRepaymentSdate(String repaymentSdate) {
		this.repaymentSdate = repaymentSdate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public double getRepaymentSamt() {
		return repaymentSamt;
	}
	public void setRepaymentSamt(double repaymentSamt) {
		this.repaymentSamt = repaymentSamt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	private String repaymentTdate;//实际还本日期
	private double repaymentTamt;//实际还本金额
	private String refNo;//真实交易流水号
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	@Override
	public int compareTo(TmApproveCapital o) {
		int c1 = this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
		if(c1 == 0 ){
			int c2 = this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
			if(c2==0){
				int c3 =this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}
	public String getRepaymentTdate() {
		return repaymentTdate;
	}
	public void setRepaymentTdate(String repaymentTdate) {
		this.repaymentTdate = repaymentTdate;
	}
	public double getRepaymentTamt() {
		return repaymentTamt;
	}
	public void setRepaymentTamt(double repaymentTamt) {
		this.repaymentTamt = repaymentTamt;
	}
	@Override
	public String toString() {
		return super.toString()+"TdCashflowCapital [repaymentTdate=" + repaymentTdate + ", repaymentTamt="
				+ repaymentTamt + "]";
	}
	
	
}
