package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

@Entity
@Table(name = "TD_CASHFLOW_DAILY_INTEREST")
public class TdCashflowDailyInterest extends CashflowDailyInterest implements Serializable, Comparable<TdCashflowDailyInterest> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public int compareTo(TdCashflowDailyInterest o) {
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			return c2 ; 
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
}
