package com.singlee.capital.cashflow.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.cashflow.service.RateRangeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.duration.service.DurationConstants.RateType;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdRateChangeMapper;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdRateChange;
import com.singlee.capital.trade.service.ProductApproveService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RateRangeServiceImpl implements RateRangeService {
	
	@Autowired
	private TdRateRangeMapper rateRangeMapper;//实际利率区间
	@Autowired
	private DayendDateService dayendDateService;//系统日期表
	@Autowired
	private BatchDao batchDao;//批量表
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;//计划表
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;//计划表
	@Autowired
	private TdFeesPassAgewayDailyMapper tdFeesPassAgewayDailyMapper;//通道计提
	@Autowired
	private ProductApproveService productApproveService;//主交易表
	@Autowired
	private TdAmtRangeMapper amtRangeMapper;//实际还本
	@Autowired
	private TdRateChangeMapper rateChangeMapper;//利率变更
	@Autowired
	private TdCashflowCapitalMapper cashflowCapitalMapper;
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;//每日计提中间表
	@Autowired
	private TrdTposMapper tposMapper;//持仓表
	@Autowired
	private BookkeepingService bookkeepingService;
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	private TcProductMapper productMapper;
	/**
	 * 影响生效日之后的所有期限的利率
	 * params[effectDate,changeRate]-TdRateChange
	 * [dealNo,version]
	 */
	@Override
	public void updateRateRangeByRateChange(Map<String, Object> params) {
		//获取该交易所有利率区间数据
		try{
			String effectDate = ParameterUtil.getString(params, "effectDate", dayendDateService.getDayendDate());
			/***************************************************************************
			 * 判定利率变更生效日与当前账务日期大小  小于账务日期，表明是倒起息交易，需要出利息调整
			 */
			if(PlaningTools.compareDate2(dayendDateService.getDayendDate(),effectDate,"yyyy-MM-dd"))//倒起息
			{
				//TD_ACC_TRD_DAILY 增加一条调整的计提
				/** 获取所含利率区间
				 * SELECT * FROM TT_RATE_RANGE WHERE ('2017-08-04'<END_DATE AND '2017-08-04'>=BEGIN_DATE)
					OR  ('2017-08-25'<=END_DATE AND '2017-08-25'>BEGIN_DATE)
				        得到利率区间  将变更利率与区间所在利率之差进行 计提
				        得到剩余本金区间，截止当前日期的
				 */
				params.put("startDate", effectDate);
				params.put("endDate", dayendDateService.getDayendDate());
				List<TdRateRange> rateRanges = this.rateRangeMapper.geTdRateRangesBetweenDay(params);
				for(int i = 0 ; i <rateRanges.size() ;i++)
				{//采用原利率减去 新调整利率 得出的结果即为差额部分，可以为负数利率
					rateRanges.get(i).setExecRate(PlaningTools.sub(ParameterUtil.getDouble(params, "changeRate", 0.00),rateRanges.get(i).getExecRate()));
				}
				TdProductApproveMain productApproveMain = null;//重新查询原交易数据
				if(!"".equals(ParameterUtil.getString(params, "refNo", ""))){
		        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
		            JY.require(productApproveMain!=null, "原交易信息不存在");
		        }else{
		        	throw new RException("原交易信息不存在！");
		        }
				if(productApproveMain.getIntType().equals(DictConstants.intType.chargeAfter))//后收息 才需要调整利息
				{
					//封装利率区间 带入计算公式
					List<InterestRange> interestRanges = new ArrayList<InterestRange>();
					InterestRange interestRange = null;
					for(TdRateRange rateRange : rateRanges){
						interestRange = new InterestRange();
						interestRange.setStartDate(rateRange.getBeginDate());
						interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
						interestRange.setRate(rateRange.getExecRate());
						interestRanges.add(interestRange);
					}
					//封装本金区间  带入计算公式
					List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
					//从实际本金现金流表获取本金 递减区间
					List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(params);
					PrincipalInterval principalInterval = null;
					double tempAmt = productApproveMain.getAmt();//本金
					if(amtRanges.size() > 0){//发生过还本 那么从实际TT_AMT_RANGE还本区间表获取还本区间
						int i = 0;
						for(i =0 ;i <amtRanges.size();i++)
						{
							principalInterval = new PrincipalInterval();
							if(i==0){//第一条还本区间起始日 设置为交易的起息日
								principalInterval.setStartDate(productApproveMain.getvDate());}
							else {
								principalInterval.setStartDate(amtRanges.get(i-1).getExecDate());}
							principalInterval.setResidual_Principal(tempAmt);
							principalInterval.setEndDate(amtRanges.get(i).getExecDate());
							principalIntervals.add(principalInterval);
							tempAmt = PlaningTools.sub(tempAmt, amtRanges.get(i).getExecAmt());//本金递减
						}
						principalInterval = new PrincipalInterval();
						principalInterval.setStartDate(amtRanges.get(i-1).getExecDate());
						principalInterval.setResidual_Principal(tempAmt);
						principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.YEAR, 10));//无限逾期10年 (逾期情况)
						principalIntervals.add(principalInterval);
					}
					else {//没有发生真实还款时
						principalInterval = new PrincipalInterval();
						principalInterval.setStartDate(productApproveMain.getvDate());
						principalInterval.setResidual_Principal(tempAmt);
						principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.YEAR, 10));//无限逾期10年(逾期情况)
						principalIntervals.add(principalInterval);
					}
					//查询得到在利率变更期影响到的收息计划数据
					List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntBetweenDay(params);
					/*
					 * 循环重算收息计划之间的利息
					 */
					Collections.sort(cashflowInterests);
					for(int i=0;i< cashflowInterests.size();i++)
					{
						cashflowInterests.get(i).setExecuteRate(ParameterUtil.getDouble(params, "changeRate", 0.00));
						cashflowInterests.get(i).setInterestAmt(PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, cashflowInterests.get(i).getRefBeginDate(), cashflowInterests.get(i).getRefEndDate(), cashflowInterests.get(i).getRefBeginDate(), 
								DayCountBasis.equal(productApproveMain.getBasis())));
					}
					batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper.updateByPrimaryKey", cashflowInterests);
					
					double amInt = 0.00;
					try {//倒起息差额计算
						amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, effectDate, dayendDateService.getDayendDate(), effectDate, 
								DayCountBasis.equal(productApproveMain.getBasis()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						JY.require(true,"计算区间差额计提出错"+e.getMessage());
					}   
					//从数据库获得利率变更交易
					TdRateChange rateChange = rateChangeMapper.getRateChangeById(ParameterUtil.getString(params, "dealNo", ""));//利率变更交易自身的交易流水
					rateChange.setAdjintamt(amInt);
					rateChangeMapper.updateByPrimaryKey(rateChange);
					if(amInt != 0){
						/**
						 * 生成利率调整分录中间表TD_ACC_TRD_DAILY
						 */
						TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(rateChange.getRefNo());
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_ADJBYRATECHANGE);//利率变更
						accTrdDaily.setAccAmt(amInt);
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						accTrdDaily.setRefNo(rateChange.getDealNo());
						accTrdDailyMapper.insert(accTrdDaily);
						
						//调用账务接口
						Map<String, Object> amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.INTEREST_ADJBYRATECHANGE, accTrdDaily.getAccAmt());
						
						InstructionPackage instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							accTrdDaily.setAcctNo(flowId);
							accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
						}
					
						/**
						 * 更改头寸表  1、判定交易是否逾期  如果是：那么就需要冲减逾期利息
						 * 2、如果非逾期交易，那么需要冲减应计利息
						 */
						//不逾期情况处理
						Map<String, Object> tposMap = new HashMap<String, Object>();
						tposMap.put("dealNo", rateChange.getRefNo());
						tposMap.put("version", rateChange.getVersion());
						TdTrdTpos tpos = tposMapper.getTdTrdTposByKey(tposMap);
						tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), amInt));
						tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), amInt));
						tposMapper.updateByPrimaryKey(tpos);
					}
				}
			}
			Map<String, Object> rateMap = new HashMap<String, Object>();
			rateMap.put("refNo", ParameterUtil.getString(params, "refNo", "X"));
			List<TdRateRange> rateChanges = rateRangeMapper.getRateRanges(rateMap);//refNo是真实的交易
			TdRateRange changeRange = null;  List<TdRateRange> resultRateRanges  =  new ArrayList<TdRateRange>();
			for(TdRateRange rateRange:rateChanges){
				if(PlaningTools.compareDate2(rateRange.getEndDate(),effectDate,"yyyy-MM-dd") && PlaningTools.compareDate(effectDate,rateRange.getBeginDate(),"yyyy-MM-dd")){
					changeRange = rateRange;
				}else {
					if(PlaningTools.compareDate2(rateRange.getBeginDate(),effectDate,"yyyy-MM-dd"))//生效区间起息日>利率变更期限
					{
						rateRange.setExecRate(ParameterUtil.getDouble(params, "changeRate", 0.00));
						resultRateRanges.add(rateRange);
					}
				}
			}
			if(null != changeRange)
			{
				//插入新增利率
				TdRateRange addRange = new TdRateRange();
				addRange.setBeginDate(effectDate);
				addRange.setExecRate(ParameterUtil.getDouble(params, "changeRate", 0.00));
				addRange.setEndDate(changeRange.getEndDate());
				addRange.setDealNo(changeRange.getDealNo());
				addRange.setVersion(changeRange.getVersion());
				addRange.setAccrulType(DurationConstants.RateType.ACCRU_TYPE);
				//修改被改变扩展的利率
				changeRange.setEndDate(effectDate);
				if(PlaningTools.compareDate3(changeRange.getBeginDate(), effectDate, "yyyy-MM-dd")) {
					rateRangeMapper.deleteByPrimaryKey(changeRange);}
				else {
					rateRangeMapper.updateByPrimaryKey(changeRange);}
				rateRangeMapper.insert(addRange);
				batchDao.batch("com.singlee.capital.cashflow.mapper.TdRateRangeMapper.updateByPrimaryKey", resultRateRanges);
			}
			//重置收息计划
			reSetInterestPlan(params);
			
			//是否修改原交易利率
			if(!"true".equals(ParameterUtil.getString(params, "notChangeTradeRate", "")))
			{
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("dealNo", ParameterUtil.getString(params, "refNo", ""));
				paramMap.put("contractRate", ParameterUtil.getDouble(params, "changeRate", 0.00));
				productApproveMainMapper.updateContractRate(paramMap);
				
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			JY.debug(e.getMessage());
		}
	}
	
	
	/****
	 * 自动重新计算利息
	 * @param params
	 */
	public void reSetInterestPlan(Map<String, Object> params)throws Exception
	{
		String effectDate = ParameterUtil.getString(params, "effectDate", dayendDateService.getDayendDate());
		
		TdProductApproveMain productApproveMain = null;//重新查询原交易数据
		if(!"".equals(ParameterUtil.getString(params, "refNo", ""))){
        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeAfter))//后收息 才需要调整利息
		{
			//获取利率区间
			List<TdRateRange> rateRanges = this.rateRangeMapper.getRateRanges(params);
			
			//封装利率区间 带入计算公式
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();
			InterestRange interestRange = null;
			for(TdRateRange rateRange : rateRanges)
			{
				interestRange = new InterestRange();
				interestRange.setStartDate(rateRange.getBeginDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(rateRange.getExecRate());
				interestRanges.add(interestRange);
			}
			//封装本金区间  带入计算公式
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
			//从实际本金现金流表获取本金 递减区间
			//List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(params);
			//获取还本计划
			List<CashflowCapital> cashflowCapitals=cashflowCapitalMapper.getCapitalListByRef(params);
			PrincipalInterval principalInterval=null;
			double tempAmt =0f;
			int count =0 ; //将本金计划翻译成本金剩余区间
			Collections.sort(cashflowCapitals);
			HashMap<String, PrincipalInterval> prHashMap = new HashMap<String, PrincipalInterval>();
			
			String endDate = null;
			for(CashflowCapital tmCashflowCapital : cashflowCapitals)
			{
				endDate = PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1);
				if(prHashMap.get(endDate) != null)
				{
					prHashMap.get(endDate).setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					
				}else{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?productApproveMain.getvDate():cashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);

					prHashMap.put(principalInterval.getEndDate(), principalInterval);
				}
				
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			/*PrincipalInterval principalInterval = null;
			double tempAmt = productApproveMain.getAmt();//本金
			if(amtRanges.size() > 0){//发生过还本 那么从实际TT_AMT_RANGE还本区间表获取还本区间
				int i = 0;
				for(i =0 ;i <amtRanges.size();i++)
				{
					principalInterval = new PrincipalInterval();
					if(i==0)//第一条还本区间起始日 设置为交易的起息日
						principalInterval.setStartDate(productApproveMain.getvDate());
					else
						principalInterval.setStartDate(amtRanges.get(i-1).getExecDate());
					principalInterval.setResidual_Principal(tempAmt);
					//本金算头不算尾
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(amtRanges.get(i).getExecDate(), Frequency.DAY, -1));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.sub(tempAmt, amtRanges.get(i).getExecAmt());//本金递减
				}
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(amtRanges.get(i-1).getExecDate());
				principalInterval.setResidual_Principal(tempAmt);
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.YEAR, 10));//无限逾期10年 (逾期情况)
				principalIntervals.add(principalInterval);
			}
			else {//没有发生真实还款时
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(productApproveMain.getvDate());
				principalInterval.setResidual_Principal(tempAmt);
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.YEAR, 10));//无限逾期10年(逾期情况)
				principalIntervals.add(principalInterval);
			}*/
			//查询得到在利率变更期影响到的收息计划数据
			Map<String, Object> params_1 = new HashMap<String, Object>();
			params_1.put("refNo", params.get("refNo"));
			params_1.put("startDate", effectDate);
			List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntByDate(params_1);
			if(cashflowInterests == null || cashflowInterests.size() == 0){
				return;
			}
			
			/*
			 * 循环重算收息计划之间的利息
			 */
			Collections.sort(cashflowInterests);
			
			String i_BPdate = null;
			List<CashflowDailyInterest> cashflowDailyInterests = new ArrayList<CashflowDailyInterest>();
			//重新计算计提利息
			for(int i = 0;i < cashflowInterests.size();i++)
			{
				if(effectDate.compareTo(cashflowInterests.get(i).getRefBeginDate()) >= 0)
				{
					i_BPdate = effectDate;
				}else{
					i_BPdate = cashflowInterests.get(i).getRefBeginDate();
				}
				
				PlaningTools.fun_Calculation_interval(interestRanges, 
						principalIntervals, 
						cashflowDailyInterests, 
						i_BPdate, 
						cashflowInterests.get(i).getRefEndDate(),
						effectDate,
						DayCountBasis.equal(productApproveMain.getBasis()));
				
			}
			//删除原始计提
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("startDate", effectDate);
			cashflowDailyInterestMapper.deleteCashflowDailyInterest(map);
			
			for (CashflowDailyInterest c : cashflowDailyInterests) 
			{
				c.setDealNo(productApproveMain.getDealNo());
			}
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", cashflowDailyInterests);
			
			//查询通道利息
			params.put("refBeginDate", cashflowInterests.size() > 0 ? cashflowInterests.get(0).getRefBeginDate() : effectDate);
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
			
			params.put("dealNo", productApproveMain.getDealNo());
			params.put("startDate", cashflowInterests.get(0).getRefBeginDate());
			cashflowDailyInterests = cashflowDailyInterestMapper.selectByDealNoAndDate(params);
	
			//根据每日计提 和通道计提 重新计算收息计划的金额
			for(int i = 0;i < cashflowInterests.size();i++)
			{
				cashflowInterests.get(i).setExecuteRate(ParameterUtil.getDouble(params, "changeRate", 0.00));
				
				PlaningTools.fun_reset_intPlan_except_fess(principalIntervals, cashflowDailyInterests, 
						cashflowInterests.get(i), productApproveMain.getvDate(), DayCountBasis.equal(productApproveMain.getBasis()), 
						fAgewayDailies);
			}
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper.updateByPrimaryKey", cashflowInterests);
		}
	}
	/**
	 * 交易放款之后进行利率处理
	 */
	@Override
	public void createRateRangeByCompleteTrade(Map<String, Object> params) {
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		BeanUtil.populate(productApproveMain, params);
		try {
			productApproveMain.setProduct(productMapper.getProductById(String.valueOf(productApproveMain.getPrdNo())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!"2".equals(productApproveMain.getProduct().getPrdTerm())){//1定期 2活期   活期不产生收息计划表   update by shiting 20170829
			//前后收息一样了；
			List<TdRateRange> rateRanges = new ArrayList<TdRateRange>();
			TdRateRange rateRange = null;
			List<CashflowInterest> cashflowInterests = cashflowInterestMapper.getInterestList(params);
			for(CashflowInterest cashflowInterest : cashflowInterests)
			{
				rateRange = new TdRateRange();
				rateRange.setDealNo(cashflowInterest.getDealNo());
				rateRange.setVersion(cashflowInterest.getVersion());
				rateRange.setBeginDate(cashflowInterest.getRefBeginDate());
				rateRange.setEndDate(cashflowInterest.getRefEndDate());
				rateRange.setExecRate(cashflowInterest.getExecuteRate());
				rateRange.setAccrulType(RateType.ACCRU_TYPE);
				rateRanges.add(rateRange);
			}
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdRateRangeMapper.insert", rateRanges);
			/*
			if(productApproveMain.getIntType().equals(DictConstants.intType.chargeBefore)){
				*//**
				 * 先收息是没有收息计划表的，无利率区间
				 *//*
				TdRateRange rateRange = new TdRateRange();
				rateRange.setDealNo(productApproveMain.getDealNo());
				rateRange.setVersion(productApproveMain.getVersion());
				rateRange.setBeginDate(productApproveMain.getvDate());
				rateRange.setEndDate(productApproveMain.getmDate());
				rateRange.setExecRate(productApproveMain.getContractRate());
				rateRange.setAccrulType(RateType.ACCRU_TYPE);
				rateRangeMapper.insert(rateRange);
			}else{
				
			}*/
		}
	}
	@Override
	public Page<TdRateRange> queryRateRangesByDealNo(
			Map<String, Object> params, RowBounds rb) {
		// TODO Auto-generated method stub
		//refNo
		return this.rateRangeMapper.pageRateRangesByDealNo(params, rb) ;
	}

}
