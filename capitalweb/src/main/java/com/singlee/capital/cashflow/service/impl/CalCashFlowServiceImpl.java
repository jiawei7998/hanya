package com.singlee.capital.cashflow.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.AmorBean;
import com.singlee.capital.base.cal.tools.IrrUtil;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowFeeInterestMapper;
import com.singlee.capital.cashflow.mapper.TmApproveCapitalMapper;
import com.singlee.capital.cashflow.mapper.TmApproveFeeMapper;
import com.singlee.capital.cashflow.mapper.TmApproveInterestMapper;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.*;
import com.singlee.capital.cashflow.model.parent.CashflowAmor;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.CollectionUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.eu.util.CollectionUtils;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayMapper;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 审批过程中现金流SERVICE
 * @author SINGLEE
 * @version 201705
 */
@SuppressWarnings({"unchecked","rawtypes"})
@Service
public class CalCashFlowServiceImpl implements CalCashFlowService {
	
	@Autowired
	private BatchDao batchDao;
	@Autowired
	TcProductMapper productMapper;
	@Autowired
	TmApproveCapitalMapper approveCapitalMapper;
	@Autowired
	TmApproveInterestMapper approveInterestMapper;
	@Autowired
	TmApproveFeeMapper approveFeeMapper;
	@Autowired
	CashflowFeeService cashflowFeeService;
	@Autowired
	ProductApproveService productApproveService;
	@Autowired
	TdCashflowFeeInterestMapper cashflowFeeInterestMapper;
	@Autowired
	TdFeesPassAgewayService tdFeesPassAgewayService;
	@Autowired
	TdFeesPassAgewayMapper feesPassagewyMapper;
	@Autowired
	private TdFeesPassAgewayDailyMapper tdFeesPassAgewayDailyMapper;//通道计提
	
	
	@Override
	public void updateByHandleCaptitalAmt(Map<String, Object> params) {
		/**
		 * 实体化界面字段为TdProductApproveMain
		 */
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		try{
			BeanUtil.populate(productApproveMain, params);
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
		productApproveMain = productApproveService.getProductApproveActivated(productApproveMain.getDealNo());
		/**
		 * 获得界面还本计划 TABLE列表数据
		 */
		String capcashtable = ParameterUtil.getString(params, "tmCashflowCapitals","");
		List<CashflowCapital> tmCashflowCapitals = FastJsonUtil.parseArrays(capcashtable, CashflowCapital.class);
		HashMap<String, Object> tempMap = new HashMap<String, Object>();
		for (CashflowCapital cashflowCapital : tmCashflowCapitals) 
		{
			if(cashflowCapital.getRepaymentSdate() != null && cashflowCapital.getRepaymentSdate().length() > 10)
			{
				cashflowCapital.setRepaymentSdate(cashflowCapital.getRepaymentSdate().substring(0, 10));
			}
			if(tempMap.get(cashflowCapital.getRepaymentSdate()) != null){
				JY.raise("计划还本日期不能重复!");
				return;
			}else{
				tempMap.put(cashflowCapital.getRepaymentSdate(), cashflowCapital);
			}
		}
		
		/**
		 * 删除之前数据存在的数据(还本计划，利息计提、收息计划）
		 */
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).deleteCapitalList(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
		/**
		 * 定义 还本计划，利息计提、收息计划
		 */
		//采用新的模式进行收息计划的产生；20170316
		List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();//返回的收息计划
		List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		int count =0 ; //将本金计划翻译成本金剩余区间
		Collections.sort(tmCashflowCapitals);
		for(CashflowCapital tmCashflowCapital : tmCashflowCapitals){
			principalInterval = new PrincipalInterval();
			principalInterval.setStartDate(count==0?productApproveMain.getvDate():tmCashflowCapitals.get(count-1).getRepaymentSdate());
			principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
			principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
			principalIntervals.add(principalInterval);
			tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
			count++;
		}
		/**
		 * 封装利率区间
		 */
		InterestRange interestRange = new InterestRange();
		interestRange.setStartDate(productApproveMain.getvDate());interestRange.setEndDate(productApproveMain.getmDate());interestRange.setRate(productApproveMain.getContractRate());
		interestRanges.add(interestRange);
		int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
		try{
			//重新计算通道
			HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
			List<TdFeesPassAgeway> feesPassAgeways = feesPassagewyMapper.getTdPassageWayByRefNo(params);
			tdFeesPassAgewayService.createPassWayFeesPlan(productApproveMain, params, passAwayMap, principalIntervals, feesPassAgeways);
			List<TdFeesPassAgewayDaily> agewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
			agewayDailies.addAll(passAwayMap.values());
			/**
			 * 判定利随本清模式，如果利随本清，那么只需要统一计算即可；
			 */
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre()))==com.singlee.capital.base.cal.Frequency.ONCE)
			{
				PlaningTools.calPlanScheduleExceptFess(interestRanges,principalIntervals,tmCashflowDailyInterests,tmCashflowInterests,
					productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
				    productApproveMain.getAmt(),productApproveMain.getContractRate(),
				    com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),
				    DayCountBasis.valueOf(basicType),0,agewayDailies);
		
			}else
			{/**
				 * 假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行计息及周期判定
				 */
				//起息日-第一次付息日
				TmCashflowInterest fistPlanSchedule  = new TmCashflowInterest();
				if(!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(), "yyyy-MM-dd"))
				{
					fistPlanSchedule.setSeqNumber(1);
					fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
					fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
					fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
					
					PlaningTools.fun_Calculation_interval_except_fess(interestRanges, principalIntervals, tmCashflowDailyInterests, 
							fistPlanSchedule, productApproveMain.getvDate(), DayCountBasis.valueOf(basicType), agewayDailies);
					
					fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					fistPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					tmCashflowInterests.add(fistPlanSchedule);
				}
				
				PlaningTools.calPlanScheduleExceptFess(interestRanges, principalIntervals, tmCashflowDailyInterests, tmCashflowInterests,
						productApproveMain.getsDate(),productApproveMain.geteDate(), productApproveMain.getvDate(), 
						productApproveMain.getAmt(), productApproveMain.getContractRate(), 
						com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())), 
						DayCountBasis.valueOf(basicType), productApproveMain.getnDays(), agewayDailies);
				
				//最后一次付息日-到期日
				if(!PlaningTools.compareDate3(productApproveMain.getmDate(), productApproveMain.geteDate(), "yyyy-MM-dd"))
				{
					TmCashflowInterest lastPlanSchedule  = new TmCashflowInterest();
					lastPlanSchedule.setSeqNumber(tmCashflowInterests.size()+1);
					lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
					lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
					lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
					
					PlaningTools.fun_Calculation_interval_except_fess(interestRanges, principalIntervals, tmCashflowDailyInterests, 
							lastPlanSchedule, productApproveMain.getvDate(), DayCountBasis.valueOf(basicType), agewayDailies);
					
					lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					lastPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					tmCashflowInterests.add(lastPlanSchedule);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		/*****************************************************************************************************************/
		
		//利息计划  关联交易表单及状态数据设置
		String dealNO = ParameterUtil.getString(params, "dealNo", "");
		for(int j = 0;j<tmCashflowInterests.size();j++)
		{
			tmCashflowInterests.get(j).setDealNo(dealNO);
			tmCashflowInterests.get(j).setCfType("Interest");
			tmCashflowInterests.get(j).setPayDirection("Recevie");
			tmCashflowInterests.get(j).setVersion(productApproveMain.getVersion());
		}
		//利息明细  设置交易关联项
		for(int i=0;i<tmCashflowDailyInterests.size();i++)
		{
			tmCashflowDailyInterests.get(i).setDealNo(dealNO);
			tmCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
		}
		for(int i=0;i<tmCashflowCapitals.size();i++)
		{
			tmCashflowCapitals.get(i).setDealNo(dealNO);
			tmCashflowCapitals.get(i).setVersion(productApproveMain.getVersion());
		}
		//批量插入调整后的本金计划
		batchDao.batch(getSqlForCashFlow(params)+"CashflowCapitalMapper.insert", tmCashflowCapitals);
		//cashflowFeeService.updateByHandleCaptitalAmt(BeanUtil.beanToMap(productApproveMain), principalIntervals);
		//前后收息均改为利息模式20171029
		if(StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType()) || StringUtils.equalsIgnoreCase("2", productApproveMain.getIntType())){
			//批量插入调整后的利息计划
			batchDao.batch(getSqlForCashFlow(params)+"CashflowInterestMapper.insert", tmCashflowInterests);
			//批量插入调整后的每日计提
			batchDao.batch(getSqlForCashFlow(params)+"CashflowDailyInterestMapper.insert", tmCashflowDailyInterests);
			return;
		}
		/**
		 * 本金计划、利息每日计提收益、通道每日计提收益、变化的利率区间
		 * 决定摊销的收益率
		 */
		if(productApproveMain.getDisAmt()!=0){//生成折溢价摊销表
			//将本金计划进行map化  日期/bean
		    Map<String,CashflowCapital> tmcMaps = Maps.uniqueIndex(tmCashflowCapitals, new Function <CashflowCapital,String>(){
		    	@Override
		          public String apply(CashflowCapital tmCashflowCapital){
		            return tmCashflowCapital.getRepaymentSdate();   
		    }});  
		    ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowAmorMapper")).deletCashflowAmor(params);
			List<AmorBean> amorBeans = new ArrayList<AmorBean>();//摊销现金流
			List<CashflowAmor> tmCashflowAmors = new ArrayList<CashflowAmor>();//摊销数据库现金流表
			try{
				//重算 根据利率得现金流 因为摊销与还本无关 与实际的收益率存在偏差
				for(int i =0;i<tmCashflowInterests.size();i++) {
					tmCashflowInterests.get(i).setInterestAmt(
							PlaningTools.fun_Calculation_interval_Range2(interestRanges, tmCashflowInterests.get(i).getRefBeginDate(), tmCashflowInterests.get(i).getRefEndDate(), tmCashflowInterests.get(i).getRefBeginDate(), DayCountBasis.valueOf(basicType), 100.00)
							);
				}
				amorBeans = IrrUtil.calAmorBeans(amorBeans, tmCashflowInterests, interestRanges, 
					productApproveMain.getvDate(), productApproveMain.getmDate(), 
					PlaningTools.div(productApproveMain.getProcAmt(), productApproveMain.getAmt()/100, 12), 
					productApproveMain.getBasis(), 
					100);//获得现金流（摊销）
				TmCashflowAmor tmCashflowAmor = null; double syAmt = productApproveMain.getAmt();
				count = 0;double sumAmorAmt = 0.0;
				for(AmorBean amorBean:amorBeans){//拷贝
					tmCashflowAmor = new TmCashflowAmor();
					org.springframework.beans.BeanUtils.copyProperties(amorBean, tmCashflowAmor);
					tmCashflowAmor.setDealNo(productApproveMain.getDealNo());
					tmCashflowAmor.setVersion(productApproveMain.getVersion());
					
					double jsdAmt = (tmcMaps.get(amorBean.getPostdate())==null?0.00:tmcMaps.get(amorBean.getPostdate()).getRepaymentSamt());
					
					//摊销调整  还本的时候需要计算  还本金额所含的部分未实现摊销部分
					/**
					 * 1、先计算出还本的金额占全部本金的比例
					 * 2、计算还本部分的已摊销总金额
					 * 3、计算还本部分所需要摊销的总摊销额
					 * 4、  3-2=摊销调整
					 */
					if(jsdAmt >0 ){
						double sumDisYAmt = PlaningTools.div(productApproveMain.getDisAmt()*jsdAmt,productApproveMain.getAmt());
						double alreadyTxAmt = PlaningTools.div(PlaningTools.mul(Math.abs(PlaningTools.sub(PlaningTools.div(productApproveMain.getProcAmt(), productApproveMain.getAmt()/100, 12),tmCashflowAmors.get(count-1).getSettAmt())),syAmt/100.00)*jsdAmt,syAmt);
						tmCashflowAmor.setAdjAmorAmt(PlaningTools.div(sumDisYAmt-alreadyTxAmt,1,2));
					}else {
						tmCashflowAmor.setAdjAmorAmt(0.00);
					}
					//摊销金额  本金/100   *  摊销基数（每100面值所需要摊销的价值）		
					syAmt = (syAmt-jsdAmt);
					if(count!=amorBeans.size()-1) {
                        tmCashflowAmor.setActAmorAmt(PlaningTools.div(PlaningTools.mul(tmCashflowAmor.getAmorAmt(),syAmt/100.00), 1, 2));
                    } else {
						tmCashflowAmor.setActAmorAmt(PlaningTools.sub(productApproveMain.getDisAmt(), sumAmorAmt));
					}
					sumAmorAmt = PlaningTools.add(PlaningTools.add(sumAmorAmt, tmCashflowAmor.getActAmorAmt()),tmCashflowAmor.getAdjAmorAmt());
					tmCashflowAmors.add(tmCashflowAmor);
					count++;
				}
				batchDao.batch(getSqlForCashFlow(params)+"CashflowAmorMapper.insert", tmCashflowAmors);
			}catch (Exception e) {
				e.printStackTrace();
				JY.raise(e.getMessage());
			}
		}
		
	}
	@Override
	public void updateCashFlowInterestByHandle(Map<String, Object> params) {
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		//利息明细
		List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();
		List<CashflowInterest> resultList = new ArrayList<CashflowInterest>();//最终的收息计划列表，需要最后用于持久化
		HashMap<String, CashflowInterest> dateSplitHashMap = new HashMap<String, CashflowInterest>();
		List<CashflowCapital> tmCashflowCapitals = new ArrayList<CashflowCapital>();
		List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间列表
		try{
			BeanUtil.populate(productApproveMain, params);
			/**
			 * 得到界面的serch_form数据  收息计划修改;转换为JAVABEAN
			 */
			String tmCashflowInterest = ParameterUtil.getString(params, "tmCashflowInterest","");
			TmCashflowInterest pageBean=FastJsonUtil.parseObject(tmCashflowInterest, TmCashflowInterest.class);
			
			tmCashflowCapitals = ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).getCapitalList(params);//获得本金现金流
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			PrincipalInterval principalInterval=null;double tempAmt =0f;
			/**
			 * 将本金现金流 翻译为 本金变化区间
			 */
			int count  = 0;
			for(CashflowCapital tmCashflowCapital : tmCashflowCapitals){
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(count==0?productApproveMain.getvDate():tmCashflowCapitals.get(count-1).getRepaymentSdate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
				principalIntervals.add(principalInterval);
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			/**
			 * 获得更改利息计划之前的数据库数据，因为需要分割计划
			 * 这里无法实现从起息日间隔区间进行计提演算，只能按分区间进行演算计提，保证每个区间的计提准确
			 */
			tmCashflowInterests = ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).getInterestList(params);
			
			dateSplitHashMap.put(pageBean.getRefBeginDate(), pageBean);
			for(CashflowInterest temp : tmCashflowInterests)
			{
				/**
				 * 拆分计划为 起息日 到期日
				 * 保留 起息日 到期日 分别小于/大于  更改过的 起息日和到期日
				 */
				if(PlaningTools.compareDate2(pageBean.getRefBeginDate(), temp.getRefBeginDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate2(temp.getRefBeginDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate(pageBean.getRefEndDate(), temp.getRefBeginDate(), "yyyy-MM-dd")
						&& PlaningTools.compareDate(temp.getRefEndDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(pageBean.getRefEndDate(), temp);
				}
				
			}
			dateSplitHashMap.put(productApproveMain.getmDate(), tmCashflowInterests.get(tmCashflowInterests.size()-1));
			/**
			 * 进行排序
			 */
			
			List<Map.Entry<String, CashflowInterest>> arrayList = new ArrayList(dateSplitHashMap.entrySet());
			Collections.sort(arrayList, new Comparator() {
				@Override   
				public int compare(Object o1, Object o2) {
				    Map.Entry obj1 = (Map.Entry) o1;
				    Map.Entry obj2 = (Map.Entry) o2;
				    try {
						return PlaningTools.compareDate2(obj1.toString(), obj2.toString(), "yyyy-MM-dd")==true?1:0;
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						return 0;
					}
				   }
			 });
			/**
			 * 创建利率区间
			 */
			
			InterestRange interestRange = null;CashflowInterest tmCashflowInterest2 = null;
			for(int i=0 ; i<arrayList.size()-1;i++){
				interestRange = new InterestRange();
				interestRange.setStartDate(arrayList.get(i).getKey());
				interestRange.setRate(arrayList.get(i).getValue().getExecuteRate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(arrayList.get(i+1).getKey(), Frequency.DAY, -1));
				interestRanges.add(interestRange);			
			}
			
			params.put("refNo", productApproveMain.getDealNo());
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fessList = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
			
			for(int i=0 ; i<arrayList.size()-1;i++){
				tmCashflowInterest2 = new TmCashflowInterest();
				tmCashflowInterest2.setDayCounter(pageBean.getDayCounter());
				tmCashflowInterest2.setDealNo(productApproveMain.getDealNo());
				tmCashflowInterest2.setExecuteRate(arrayList.get(i).getValue().getExecuteRate());
				tmCashflowInterest2.setRefBeginDate(arrayList.get(i).getKey());
				tmCashflowInterest2.setRefEndDate(arrayList.get(i+1).getKey());
				tmCashflowInterest2.setTheoryPaymentDate(PlaningTools.compareDateEqual(arrayList.get(i).getValue().getRefEndDate(), arrayList.get(i+1).getKey(), "yyyy-MM-dd")?arrayList.get(i).getValue().getTheoryPaymentDate():arrayList.get(i+1).getKey());
				tmCashflowInterest2.setCfType("Interest");
				tmCashflowInterest2.setPayDirection("Recevie");
				
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges, principalIntervals, tmCashflowDailyInterests, tmCashflowInterest2, productApproveMain.getvDate(), DayCountBasis.equal(pageBean.getDayCounter()), fessList);
				
				tmCashflowInterest2.setVersion(productApproveMain.getVersion());
				tmCashflowInterest2.setSeqNumber(i+1);
				resultList.add(tmCashflowInterest2);
			}
			((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
			((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
	
			for(int i =0;i<tmCashflowDailyInterests.size();i++)
			{
				tmCashflowDailyInterests.get(i).setDealNo(productApproveMain.getDealNo());
				tmCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
			}
			batchDao.batch(getSqlForCashFlow(params)+"CashflowInterestMapper.insert", resultList);
			batchDao.batch(getSqlForCashFlow(params)+"CashflowDailyInterestMapper.insert", tmCashflowDailyInterests);
		}catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		/**
		 * 本金计划、利息每日计提收益、通道每日计提收益、变化的利率区间
		 * 决定摊销的收益率
		 */
		/**
		 * 查看是否存在折溢价
		 */
		if(productApproveMain.getDisAmt()!=0){//生成折溢价摊销表
			//将本金计划进行map化  日期/bean
			Map<String,CashflowCapital> tmcMaps = Maps.uniqueIndex(tmCashflowCapitals, new Function <CashflowCapital,String> () {  
				@Override  
				public String apply(CashflowCapital tmCashflowCapital) {  
		            return tmCashflowCapital.getRepaymentSdate();   
		    }});  
		    
			((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowAmorMapper")).deletCashflowAmor(params);
			List<AmorBean> amorBeans = new ArrayList<AmorBean>();//摊销现金流
			List<TmCashflowAmor> tmCashflowAmors = new ArrayList<TmCashflowAmor>();//摊销数据库现金流表
			try{
				//重算 根据利率得现金流 因为摊销与还本无关 与实际的收益率存在偏差
				int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
				for(int i =0;i<tmCashflowInterests.size();i++) {
					tmCashflowInterests.get(i).setInterestAmt(
							PlaningTools.fun_Calculation_interval_Range2(interestRanges, tmCashflowInterests.get(i).getRefBeginDate(), tmCashflowInterests.get(i).getRefEndDate(), tmCashflowInterests.get(i).getRefBeginDate(), DayCountBasis.valueOf(basicType), 100.00)
							);
				}
				amorBeans = IrrUtil.calAmorBeans(amorBeans, tmCashflowInterests, interestRanges, 
					productApproveMain.getvDate(), productApproveMain.getmDate(), 
					PlaningTools.div(productApproveMain.getProcAmt(), productApproveMain.getAmt()/100, 12), 
					productApproveMain.getBasis(), 
					100);//获得现金流（摊销）
				TmCashflowAmor tmCashflowAmor = null; double syAmt = productApproveMain.getAmt();
				int count = 0;double sumAmorAmt = 0.0;
				for(AmorBean amorBean:amorBeans){//拷贝
					tmCashflowAmor = new TmCashflowAmor();
					org.springframework.beans.BeanUtils.copyProperties(amorBean, tmCashflowAmor);
					tmCashflowAmor.setDealNo(productApproveMain.getDealNo());
					tmCashflowAmor.setVersion(productApproveMain.getVersion());
					
					double jsdAmt = (tmcMaps.get(amorBean.getPostdate())==null?0.00:tmcMaps.get(amorBean.getPostdate()).getRepaymentSamt());
					
					//摊销调整  还本的时候需要计算  还本金额所含的部分未实现摊销部分
					/**
					 * 1、先计算出还本的金额占全部本金的比例
					 * 2、计算还本部分的已摊销总金额
					 * 3、计算还本部分所需要摊销的总摊销额
					 * 4、  3-2=摊销调整
					 */
					if(jsdAmt >0 ){
						double sumDisYAmt = PlaningTools.div(productApproveMain.getDisAmt()*jsdAmt,productApproveMain.getAmt());
						double alreadyTxAmt = PlaningTools.div(PlaningTools.mul(Math.abs(PlaningTools.sub(PlaningTools.div(productApproveMain.getProcAmt(), productApproveMain.getAmt()/100, 12),tmCashflowAmors.get(count-1).getSettAmt())),syAmt/100.00)*jsdAmt,syAmt);
						tmCashflowAmor.setAdjAmorAmt(PlaningTools.div(sumDisYAmt-alreadyTxAmt,1,2));
					}else {
						tmCashflowAmor.setAdjAmorAmt(0.00);
					}
					//摊销金额  本金/100   *  摊销基数（每100面值所需要摊销的价值）		
					syAmt = (syAmt-jsdAmt);
					if(count!=amorBeans.size()-1) {
                        tmCashflowAmor.setActAmorAmt(PlaningTools.div(PlaningTools.mul(tmCashflowAmor.getAmorAmt(),syAmt/100.00), 1, 2));
                    } else {
						tmCashflowAmor.setActAmorAmt(PlaningTools.sub(productApproveMain.getDisAmt(), sumAmorAmt));
					}
					sumAmorAmt = PlaningTools.add(PlaningTools.add(sumAmorAmt, tmCashflowAmor.getActAmorAmt()),tmCashflowAmor.getAdjAmorAmt());
					tmCashflowAmors.add(tmCashflowAmor);
					count++;
				}
				batchDao.batch(getSqlForCashFlow(params)+"CashflowAmorMapper.insert", tmCashflowAmors);
			}catch (Exception e) {
				e.printStackTrace();
				JY.raise(e.getMessage());
			}
		}
	}
	
	@Override
	public List<CashflowInterest> createInterestCashFlowForSaveApprove(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges,Map<String, Object> params) {
		
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		List<CashflowInterest> pList = new ArrayList<CashflowInterest>();//返回的收息计划
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		
		try{
			BeanUtil.populate(productApproveMain, params);
			//做提前判定，前收息的时候，还本收息必须都是一次性的
			if(StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType()))//前收息
			{
				if(!(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre()))==com.singlee.capital.base.cal.Frequency.ONCE))
				{
					throw new RException("交易录入选择前收息时，收息频率必须选择一次性!");
				}
				if(!(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getAmtFre()))==com.singlee.capital.base.cal.Frequency.ONCE))
				{
					throw new RException("交易录入选择前收息时，还本频率必须选择一次性!");
				}
			}
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			/**
			 * 判定利随本清模式，如果利随本清，那么只需要统一计算即可；
			 */
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre()))==com.singlee.capital.base.cal.Frequency.ONCE)
			{
				PlaningTools.calPlanSchedule(interestRanges,principalIntervals,detailSchedules,pList,
				productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
				productApproveMain.getAmt(),productApproveMain.getContractRate(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType));
		
			} else {
				TmCashflowInterest fistPlanSchedule  = new TmCashflowInterest();//假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行计息及周期判定
				//判断第一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(), "yyyy-MM-dd")){
					fistPlanSchedule.setSeqNumber(1);
					fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
					fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
					fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
					fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,detailSchedules,fistPlanSchedule.getRefBeginDate(),fistPlanSchedule.getRefEndDate(),productApproveMain.getvDate(),DayCountBasis.valueOf(basicType)));
					fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					fistPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					pList.add(fistPlanSchedule);
				}

				 PlaningTools.calPlanSchedule(interestRanges,principalIntervals,detailSchedules,pList,
						productApproveMain.getsDate(),productApproveMain.geteDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
						productApproveMain.getAmt(),productApproveMain.getContractRate(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType));
				//最后一次付息日-到期日  判断最后一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.geteDate(), productApproveMain.getmDate(), "yyyy-MM-dd")){
					TmCashflowInterest lastPlanSchedule  = new TmCashflowInterest();
					lastPlanSchedule.setSeqNumber(pList.size()+1);
					lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
					lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
					lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
					lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,detailSchedules,lastPlanSchedule.getRefBeginDate(),lastPlanSchedule.getRefEndDate(),productApproveMain.getvDate(),DayCountBasis.valueOf(basicType)));
					lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					lastPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					pList.add(lastPlanSchedule);
				}
			
			}
			//利息计划
			String dealNO = ParameterUtil.getString(params, "dealNo", "");
			for(int i=0 ; i< pList.size();i++)
			{
				pList.get(i).setDealNo(dealNO);
				pList.get(i).setCfType("Interest");
				pList.get(i).setPayDirection("Recevie");
				pList.get(i).setVersion(productApproveMain.getVersion());
			}
			//利息明细
			for(int j=0;j<detailSchedules.size();j++)
			{
				detailSchedules.get(j).setDealNo(dealNO);
				detailSchedules.get(j).setVersion(productApproveMain.getVersion());
			}
			//if(StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType())) return pList;//先收息的前提下无利息计提  不存储
			CollectionUtils.sort(pList, false, "refEndDate");
			if(null != StringUtils.trimToNull(productApproveMain.getMeDate())) {
				pList.get(pList.size()-1).setTheoryPaymentDate(productApproveMain.getMeDate());
			}
			batchDao.batch(getSqlForCashFlow(params)+"CashflowInterestMapper.insert", pList);
			batchDao.batch(getSqlForCashFlow(params)+"CashflowDailyInterestMapper.insert", detailSchedules);
		} catch (Exception e) {
			//e.printStackTrace();
			JY.raise(e.getMessage());
		}
		return  pList;
	}
	@Override
	public List<CashflowCapital> createCapitalCashFlowForSaveApprove(Map<String, Object> params) {
		
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		List<CashflowCapital> principalSchedules = new ArrayList<CashflowCapital>();
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
			int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			//还本计划
			PlaningTools.calPrincipalSchedule(principalSchedules,productApproveMain.getvDate(),productApproveMain.getmDate(),
					productApproveMain.getAmt(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getAmtFre())),DayCountBasis.valueOf(basicType));
			String dealNO = ParameterUtil.getString(params, "dealNo", "");
			//本金计划
			for(int k=0;k<principalSchedules.size();k++)
			{
				principalSchedules.get(k).setCfType(DictConstants.CashFlowType.Principal);
				principalSchedules.get(k).setDealNo(dealNO);
				principalSchedules.get(k).setVersion(productApproveMain.getVersion());
				principalSchedules.get(k).setPayDirection("Recevie");
			}
			CollectionUtils.sort(principalSchedules, false, "repaymentSdate");
			if(null != StringUtils.trimToNull(productApproveMain.getMeDate())) {
				principalSchedules.get(principalSchedules.size()-1).setRepaymentSdate(productApproveMain.getMeDate());
			}
			batchDao.batch(getSqlForCashFlow(params)+"CashflowCapitalMapper.insert", principalSchedules);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}		
		return principalSchedules;
	}
	@Override
	public void createAmorCashFlowForSaveApprove(List<CashflowInterest> pList,List<CashflowCapital> principalSchedules,List<InterestRange> interestRanges,Map<String, Object> params) {
		
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
			//if(!StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType())) return;//后收息的情况下无需出摊销
			
			Map<String,CashflowCapital> tmcMaps = Maps.uniqueIndex(principalSchedules, new Function <CashflowCapital,String> () {  
				@Override  
				public String apply(CashflowCapital tmCashflowCapital) {  
		            return tmCashflowCapital.getRepaymentSdate();   
		    }});  
			((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowAmorMapper")).deletCashflowAmor(params);
			
			if(productApproveMain.getDisAmt() == 0) {return;}//折溢价金额为0不出摊销
			
			List<AmorBean> amorBeans = new ArrayList<AmorBean>();//摊销现金流
			List<TmCashflowAmor> tmCashflowAmors = new ArrayList<TmCashflowAmor>();//摊销数据库现金流表
			for(int i = 0 ;i<pList.size();i++){
				pList.get(i).setInterestAmt(PlaningTools.div(pList.get(i).getInterestAmt(), productApproveMain.getAmt()/100));
			}

			amorBeans = IrrUtil.calAmorBeans(amorBeans, pList, interestRanges, 
				productApproveMain.getvDate(), productApproveMain.getmDate(), 
				PlaningTools.div(productApproveMain.getProcAmt(), productApproveMain.getAmt()/100, 12), 
				productApproveMain.getBasis(), 
				100);//获得现金流（摊销）
			TmCashflowAmor tmCashflowAmor = null; double syAmt = productApproveMain.getAmt();
			int count = 0;double sumAmorAmt = 0.00;
			for(AmorBean amorBean:amorBeans){//拷贝
				tmCashflowAmor = new TmCashflowAmor();
				org.springframework.beans.BeanUtils.copyProperties(amorBean, tmCashflowAmor);
				tmCashflowAmor.setDealNo(productApproveMain.getDealNo());
				tmCashflowAmor.setVersion(productApproveMain.getVersion());
				
				double jsdAmt = (tmcMaps.get(amorBean.getPostdate())==null?0.00:tmcMaps.get(amorBean.getPostdate()).getRepaymentSamt());
				
				//摊销调整  还本的时候需要计算  还本金额所含的部分未实现摊销部分
				/**
				 * 1、先计算出还本的金额占全部本金的比例
				 * 2、计算还本部分的已摊销总金额
				 * 3、计算还本部分所需要摊销的总摊销额
				 * 4、  3-2=摊销调整
				 */
				if(jsdAmt >0 ){
					double sumDisYAmt = PlaningTools.div(productApproveMain.getDisAmt()*jsdAmt,productApproveMain.getAmt());
					double alreadyTxAmt = PlaningTools.div(Math.abs(tmCashflowAmors.get(count).getSettAmt()-productApproveMain.getDisAmt()),syAmt/100.00);
					tmCashflowAmor.setAdjAmorAmt(PlaningTools.div(sumDisYAmt-alreadyTxAmt,1,2));
				}else {
					tmCashflowAmor.setAdjAmorAmt(0.00);
				}
				//摊销金额  本金/100   *  摊销基数（每100面值所需要摊销的价值）		
				syAmt = (syAmt-jsdAmt);
				if(count!=amorBeans.size()-1) {
                    tmCashflowAmor.setActAmorAmt(PlaningTools.div(PlaningTools.mul(tmCashflowAmor.getAmorAmt(),syAmt/100.00), 1, 2));
                } else {
					tmCashflowAmor.setActAmorAmt(PlaningTools.sub(productApproveMain.getDisAmt(), sumAmorAmt));
				}
				sumAmorAmt = PlaningTools.add(PlaningTools.add(sumAmorAmt, tmCashflowAmor.getActAmorAmt()),tmCashflowAmor.getAdjAmorAmt());
				tmCashflowAmors.add(tmCashflowAmor);
				System.out.println("count:"+count);
				count++;
			}
			batchDao.batch(getSqlForCashFlow(params)+"CashflowAmorMapper.insert", tmCashflowAmors);
		
		}catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
	}
	
	@Override
	public void createAllCashFlowsForSaveApprove(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges, Map<String, Object> params) {
		
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).deleteCapitalList(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
		this.createInterestCashFlowForSaveApprove(principalIntervals, interestRanges, params);
		this.createCapitalCashFlowForSaveApprove(params);
		//无需在进行摊销处理，采用与利息计提一致的处理20171026
		/*this.createAmorCashFlowForSaveApprove(
				this.createInterestCashFlowForSaveApprove(principalIntervals, interestRanges, params), 
				this.createCapitalCashFlowForSaveApprove(params), interestRanges, params);*/
	}
	/**
	 * 将审批的交易现金流拷贝给核实交易单
	 */
	@Override
	public void createAllCashFlowsForTradCopy(Map<String, Object> params) {
		
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).insertCashFlowCapCopy(params);
		//((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowAmorMapper")).insertCashFlowAmorDayCopy(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).insertCashFlowIntDayCopy(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).insertCashFlowIntCopy(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowFeeMapper")).insertCashFlowFeeCopy(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyFeeMapper")).insertCashFlowFeeDayCopy(params);
	}
	/**
	 * 审批和核实交易使用的查询每日计提的方法
	 */
	@Override
	public Page<?> getInterestDailyCashFlowForApprove(
			Map<String, Object> params, RowBounds rb) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).pageDailyInterestList(params, ParameterUtil.getRowBounds(params));
	}
	/**
	 * 审批和核实查询计提计划的方法
	 */
	@Override
	public List<?> getInterestCashFlowForApprove(Map<String, Object> params) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).getInterestList(params);
	}
	/**
	 * 审批和核实查询本金计划的方法
	 */
	@Override
	public List<?> getCapitalCashFlowForApprove(Map<String, Object> params) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).getCapitalList(params);
	}
	/**
	 * 审批和核实查询每日摊销计划方法
	 */
	@Override
	public Page<?> pageTmDailyTmCashflowAmorList(
			Map<String, Object> map, RowBounds rb) {
		
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(map)+"CashflowAmorMapper")).pageDailyCashflowAmorList(map, rb);
	}
	
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
			return "com.singlee.capital.cashflow.mapper.Td";}
		else {
			return "com.singlee.capital.cashflow.mapper.Tm";}
	}
	
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
			return "td";}
		else {
			return "tm";}
	}
	@Override
	public void deleteAllCashFlowsForDeleteDeal(Map<String, Object> params) {
		//((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowAmorMapper")).deletCashflowAmor(params);
		/**
		 * 删除之前数据存在的数据(还本计划，利息计提、收息计划）
		 */
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).deleteCapitalList(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
		
	}
	@Override
	public List<TmApproveCapital> getApproveCapitalForApprove(
			Map<String, Object> params) {
		return this.approveCapitalMapper.getApproveCapitalList(params);
	}
	@Override
	public List<TmApproveInterest> getApproveInterestForApprove(
			Map<String, Object> params) {
		return this.approveInterestMapper.getTmApproveInterestList(params);
	}
	
	@Override
	public List<TmApproveFee> getApproveFeeForApprove(
			Map<String, Object> params) {
		return this.approveFeeMapper.getTmApproveFeeList(params);
	}
	
	@Override
	public List<?> getfeeCashFlowForApprove(Map<String, Object> params) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowFeeMapper")).getInterestList(params);
	}
	@Override
	public Page<?> getFeeDailyCashFlowForApprove(Map<String, Object> params,
			RowBounds rb) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyFeeMapper")).pageDailyInterestList(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	public void createAllCashFlowsExpFees(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges, Map<String, Object> params,
			TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap) 
	{
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).deleteCapitalList(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
		
		List<TdFeesPassAgewayDaily> ls = new ArrayList<TdFeesPassAgewayDaily>();
		ls.addAll(passAwayMap.values());
		List<CashflowInterest> cashflowInterests = this.createInterestCashFlowExpFees(principalIntervals, interestRanges, params,productApproveMain,ls);
		List<CashflowCapital> capitals = this.createCapitalCashFlowForSaveApprove(params);
		for (InterestRange interestRange : interestRanges) 
		{
			interestRange.setRate(productApproveMain.getActuralRate());
		}
		this.createAmorCashFlowForSaveApprove(cashflowInterests, capitals, interestRanges, params);
	}
	
	@Override
	public List<CashflowInterest> createInterestCashFlowExpFees(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges,Map<String, Object> params,TdProductApproveMain productApproveMainOrg,List<TdFeesPassAgewayDaily> tdFeesPassAgewayDailies) {
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		List<CashflowInterest> pList = new ArrayList<CashflowInterest>();//返回的收息计划
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			/**
			 * 判定利随本清模式，如果利随本清，那么只需要统一计算即可；
			 */
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre()))==com.singlee.capital.base.cal.Frequency.ONCE)
			{
				PlaningTools.calPlanScheduleExceptFess(interestRanges,principalIntervals,detailSchedules,pList,
				productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
				productApproveMain.getAmt(),productApproveMain.getContractRate(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType),productApproveMain.getnDays(),tdFeesPassAgewayDailies);
		
			}else
			{
				TmCashflowInterest fistPlanSchedule  = new TmCashflowInterest();//假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行计息及周期判定
				//判断第一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(), "yyyy-MM-dd")){
					fistPlanSchedule.setSeqNumber(1);
					fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
					fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
					fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
					fistPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					
					PlaningTools.fun_Calculation_interval_except_fess(interestRanges,principalIntervals,detailSchedules,fistPlanSchedule,productApproveMain.getvDate(),DayCountBasis.valueOf(basicType),tdFeesPassAgewayDailies);
					
					pList.add(fistPlanSchedule);
				}
				
				PlaningTools.calPlanScheduleExceptFess(interestRanges,principalIntervals,detailSchedules,pList,
						productApproveMain.getsDate(),productApproveMain.geteDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
						productApproveMain.getAmt(),productApproveMain.getContractRate(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType),productApproveMain.getnDays(),tdFeesPassAgewayDailies);
				
				//最后一次付息日-到期日  判断最后一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.geteDate(), productApproveMain.getmDate(), "yyyy-MM-dd")){
					TmCashflowInterest lastPlanSchedule  = new TmCashflowInterest();
					lastPlanSchedule.setSeqNumber(pList.size()+1);
					lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
					lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
					lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
					lastPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
					lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					
					PlaningTools.fun_Calculation_interval_except_fess(interestRanges,principalIntervals,detailSchedules,lastPlanSchedule,productApproveMain.getvDate(),DayCountBasis.valueOf(basicType),tdFeesPassAgewayDailies);
					
					pList.add(lastPlanSchedule);
				}
			
			}
			//利息计划
			BigDecimal intAmt = new BigDecimal(0);
			String dealNO = ParameterUtil.getString(params, "dealNo", "");
			for(int i=0 ; i< pList.size();i++)
			{
				pList.get(i).setDealNo(dealNO);
				pList.get(i).setCfType("Interest");
				pList.get(i).setPayDirection("Recevie");
				pList.get(i).setVersion(productApproveMain.getVersion());
				
				intAmt = intAmt.add(new BigDecimal(pList.get(i).getInterestAmt()));
			}
			
			if(tdFeesPassAgewayDailies.size() > 0)
			{
				try {
					//rate = int * 360 / (amt * term)
					double rate = intAmt.multiply(new BigDecimal(DayCountBasis.equal(productApproveMainOrg.getBasis()).getDenominator()))
							.divide(new BigDecimal(productApproveMainOrg.getAmt()).multiply(new BigDecimal(productApproveMainOrg.getTerm())),6,BigDecimal.ROUND_DOWN).doubleValue();
				
					productApproveMainOrg.setActuralRate(rate);
				} catch (Exception e) {
					productApproveMainOrg.setActuralRate(productApproveMainOrg.getContractRate());
					e.printStackTrace();
				}
				
			}else{
				productApproveMainOrg.setActuralRate(productApproveMainOrg.getContractRate());
				
			}
			
			
			//利息明细
			for(int j=0;j<detailSchedules.size();j++)
			{
				detailSchedules.get(j).setDealNo(dealNO);
				detailSchedules.get(j).setVersion(productApproveMain.getVersion());
			}
			if(StringUtils.equalsIgnoreCase("1", productApproveMain.getIntType())) {return pList;}//先收息的前提下无利息计提  不存储
			batchDao.batch(getSqlForCashFlow(params)+"CashflowInterestMapper.insert", pList);
			batchDao.batch(getSqlForCashFlow(params)+"CashflowDailyInterestMapper.insert", detailSchedules);
		} catch (Exception e) {
			e.printStackTrace();
			JY.error(""+productApproveMain.getProductCode(), e);
			JY.raise(e.getMessage());
		}
		return  pList;
	}
	
	@Override
	public Object getCashFlowFeeInterest(Map<String, Object> map) {
		return cashflowFeeInterestMapper.getInterestList(map);
	}
	
	@Override
	public Page<?> getCashFlowFeeInterest(Map<String, Object> map, RowBounds rb) {
		return cashflowFeeInterestMapper.getInterestList(map, rb);
	}
	
	@Override
	public Page<CashflowInterest> getInterestList(Map<String, Object> params,RowBounds rb) {
		return ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).getInterestList(params,rb);
	}
	@Override
	public void createIntCashFlowsExpFees(List<CashflowCapital> capitals,List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges, Map<String, Object> params,
			TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap) 
	{
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);
		
		List<TdFeesPassAgewayDaily> ls = new ArrayList<TdFeesPassAgewayDaily>();
		ls.addAll(passAwayMap.values());
		
		List<CashflowInterest> cashflowInterests = this.createInterestCashFlowExpFees(principalIntervals, interestRanges, params,productApproveMain,ls);
		for (InterestRange interestRange : interestRanges) 
		{
			interestRange.setRate(productApproveMain.getActuralRate());
		}
		this.createAmorCashFlowForSaveApprove(cashflowInterests, capitals, interestRanges, params);
		
	}

	@Override
	public void createHrbCashFLow(List<PrincipalInterval> principalIntervals, List<InterestRange> interestRanges, Map<String, Object> params) {
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).deleteCapitalList(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(params);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowInterestMapper")).deleteInterestList(params);

		List<CashflowCapital> lists = this.createCapitalCashFlowForSaveApproveHrb(params);
		List<PrincipalInterval> newPrincipals = new ArrayList<PrincipalInterval>();//本金变化区间
		String vDate = ParameterUtil.getString(params,"vDate",null);//起息日
		String mDate = ParameterUtil.getString(params,"mDate",null);
		double amt = ParameterUtil.getDouble(params,"amt",0.0);

		double leaverAmt=amt;
		for(CashflowCapital p:lists){
			PrincipalInterval principalInterval = new PrincipalInterval();
			principalInterval.setStartDate(vDate);
			principalInterval.setEndDate(p.getRepaymentSdate());
			principalInterval.setResidual_Principal(leaverAmt);
			newPrincipals.add(principalInterval);
			vDate = p.getRepaymentSdate();
			leaverAmt = leaverAmt - p.getRepaymentSamt();

		}
		this.createInterestCashFlowForSaveApprove(newPrincipals, interestRanges, params);
	}

	@Override
	public List<CashflowCapital> createCapitalCashFlowForSaveApproveHrb(Map<String, Object> params) {

		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		List<CashflowCapital> principalSchedules = new ArrayList<CashflowCapital>();
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
			int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			//还本计划
			PlaningTools.calPrincipalScheduleHrb(principalSchedules,productApproveMain.getvDate(),productApproveMain.getmDate(),
					productApproveMain.getAmt(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getAmtFre())),DayCountBasis.valueOf(basicType));
			String dealNO = ParameterUtil.getString(params, "dealNo", "");
			//本金计划
			for(int k=0;k<principalSchedules.size();k++)
			{
				principalSchedules.get(k).setCfType(DictConstants.CashFlowType.Principal);
				principalSchedules.get(k).setDealNo(dealNO);
				principalSchedules.get(k).setVersion(productApproveMain.getVersion());
				principalSchedules.get(k).setPayDirection("Recevie");
			}
			CollectionUtils.sort(principalSchedules, false, "repaymentSdate");
			if(null != StringUtils.trimToNull(productApproveMain.getMeDate())) {
				principalSchedules.get(principalSchedules.size()-1).setRepaymentSdate(productApproveMain.getMeDate());
			}
			batchDao.batch(getSqlForCashFlow(params)+"CashflowCapitalMapper.insert", principalSchedules);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		return principalSchedules;
	}


	/**
	 * 根据传入的自定义本金计划，按照传入的付息日，生成付息计划和付息明细
	 *
	 */
	@Override
	public List<CashflowInterest>  calInterestFlowListHrb(List<InterestRange> interestRanges,Map<String,Object> param){
		List<CashflowInterest> ibIntPlanList = JSON.parseArray(param.get("intPlan").toString(), CashflowInterest.class);
		List<CashflowCapital> ibAmtPlanList = JSON.parseArray(param.get("amtPlan").toString(), CashflowCapital.class);

		List<PrincipalInterval> newPrincipals = new ArrayList<PrincipalInterval>();//本金变化区间
		String vDate = ParameterUtil.getString(param,"vDate",null);
		String mDate = ParameterUtil.getString(param,"mDate",null);
		double amt = ParameterUtil.getDouble(param,"amt",0.0);
		String basis = ParameterUtil.getString(param,"basis","");
		int basicType = DictConstants.DayCounter.Actual360.equals(basis)?1:DictConstants.DayCounter.Actual365.equals(basis)?2:3;

		try {
		//整理本金计划
			double leaverAmt=amt;
			String capVdate = vDate;
			CollectionUtils.sort(ibAmtPlanList, false, "repaymentSdate");
			for(CashflowCapital p:ibAmtPlanList){
				PrincipalInterval principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(capVdate);
				principalInterval.setEndDate(p.getRepaymentSdate().substring(0,10));
				principalInterval.setResidual_Principal(leaverAmt);
				newPrincipals.add(principalInterval);
				capVdate = p.getRepaymentSdate();
				leaverAmt = leaverAmt - p.getRepaymentSamt();
			}

			//格式化日期,再按日期排序
			for (CashflowInterest interest : ibIntPlanList) {
				if(interest.getRefEndDate().length() > 10){
					interest.setRefEndDate(interest.getRefEndDate().substring(0,10));
				}
			}
			CollectionUtils.sort(ibIntPlanList, false, "refEndDate");
			System.out.println(ibIntPlanList);//测试

			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			//生成付息计划
			String vIntDate = vDate;
			int seq = 1;
			String dealNo = ParameterUtil.getString(param, "dealNo", null);
			for (CashflowInterest interest : ibIntPlanList) {
				interest.setDealNo(dealNo);
				interest.setSeqNumber(seq++);
				interest.setRefBeginDate(vIntDate);
				interest.setTheoryPaymentDate(interest.getRefEndDate());
				interest.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,newPrincipals,detailSchedules,interest.getRefBeginDate(),interest.getRefEndDate(),vDate,DayCountBasis.valueOf(basicType)));
				interest.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
				interest.setExecuteRate(interestRanges.get(0).getRate());
				interest.setCfType("Interest");
				interest.setPayDirection("Recevie");
				interest.setVersion(0);
				vIntDate = interest.getRefEndDate();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(ibIntPlanList);//测试

		//判断第一次付息日与起息日如果相等，那么需要排除
//		if(!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(), "yyyy-MM-dd")){
//			fistPlanSchedule.setSeqNumber(1);
//			fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
//			fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
//			fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
//			fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,detailSchedules,fistPlanSchedule.getRefBeginDate(),fistPlanSchedule.getRefEndDate(),productApproveMain.getvDate(),DayCountBasis.valueOf(basicType)));
//			fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
//			fistPlanSchedule.setExecuteRate(productApproveMain.getContractRate());
//			pList.add(fistPlanSchedule);
//		}

		return ibIntPlanList;
	}

	/**
	 * 保存页面自定义的本金和利息计划，
	 */
	@Override
	public void saveCaptialAndInterestCashFlowHrb(Map<String,Object> param){
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(param)+"CashflowCapitalMapper")).deleteCapitalList(param);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(param)+"CashflowDailyInterestMapper")).deleteCashflowDailyInterest(param);
		((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(param)+"CashflowInterestMapper")).deleteInterestList(param);

		List<CashflowInterest> ibIntPlanList = JSON.parseArray(param.get("intPlan").toString(), CashflowInterest.class);
		List<CashflowCapital> ibAmtPlanList = JSON.parseArray(param.get("amtPlan").toString(), CashflowCapital.class);
		String dealNo = ParameterUtil.getString(param, "dealNo", null);
		//给本金计划赋值
		int seq = 1;
		CollectionUtils.sort(ibAmtPlanList, false, "repaymentSdate");//保证本金计划顺序
		for (CashflowCapital cashflowCapital : ibAmtPlanList) {
			cashflowCapital.setSeqNumber(seq++);
			cashflowCapital.setCfType(DictConstants.CashFlowType.Principal);
			cashflowCapital.setPayDirection("Recevie");
			cashflowCapital.setDealNo(dealNo);
		}

		CollectionUtils.sort(ibIntPlanList, false, "refEndDate");//保证现金流的顺序

		batchDao.batch(getSqlForCashFlow(param)+"CashflowCapitalMapper.insert", ibAmtPlanList);
		batchDao.batch(getSqlForCashFlow(param)+"CashflowInterestMapper.insert", ibIntPlanList);
	}
}
