package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

@Entity
@Table(name = "TD_CASHFLOW_FEE_INTEREST")
public class TdCashflowFeeInterest extends CashflowInterest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String feeCustNo;
	
	private String feeCustName;

	public String getFeeCustNo() {
		return feeCustNo;
	}

	public void setFeeCustNo(String feeCustNo) {
		this.feeCustNo = feeCustNo;
	}

	public String getFeeCustName() {
		return feeCustName;
	}

	public void setFeeCustName(String feeCustName) {
		this.feeCustName = feeCustName;
	}

	
	
	
}
