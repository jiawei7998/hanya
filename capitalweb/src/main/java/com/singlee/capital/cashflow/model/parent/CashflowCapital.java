package com.singlee.capital.cashflow.model.parent;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CashflowCapital implements Serializable , Comparable<CashflowCapital>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_CAPITAL.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	
	
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	private String repaymentSdate;//计划还本日期
	private String cfType;// 现金流类型
	private String payDirection;// 收付方向
	private int seqNumber;// 序列
	private double repaymentSamt;// 现金流事件
	private String dealNo;
	private int version;
	public String getRepaymentSdate() {
		return repaymentSdate;
	}
	public void setRepaymentSdate(String repaymentSdate) {
		this.repaymentSdate = repaymentSdate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public double getRepaymentSamt() {
		return repaymentSamt;
	}
	public void setRepaymentSamt(double repaymentSamt) {
		this.repaymentSamt = repaymentSamt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		return "CashflowCapital [repaymentSdate=" + repaymentSdate
				+ ", cfType=" + cfType + ", payDirection=" + payDirection
				+ ", seqNumber=" + seqNumber + ", repaymentSamt="
				+ repaymentSamt + ", dealNo=" + dealNo + ", version=" + version
				+ "]";
	}
	@Override
	public int compareTo(CashflowCapital o) {
		int c1 = this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
		if(c1 == 0 ){
			int c2 = this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
			if(c2==0){
				int c3 =this.getRepaymentSdate().compareTo(o.getRepaymentSdate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}
}
