package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

/**
 * 现金流 类
 * 临时存储审批路径下的利息计划
 * 
 * @author
 * 
 */

@Entity
@Table(name = "TM_CASHFLOW_INTEREST")
public class TmCashflowInterest extends CashflowInterest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return super.toString();
	}
}
