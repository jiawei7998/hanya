package com.singlee.capital.cashflow.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdAmtRange;

public interface AmtRangeService {
	/**
	 * 根据业务流水查询系统目前最新的实际还本现金流
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdAmtRange>  queryAmtRangesByDealNo(Map<String,Object> params,RowBounds rb);
	
}
