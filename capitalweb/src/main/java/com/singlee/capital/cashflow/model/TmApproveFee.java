package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TM_APPROVE_FEE")
public class TmApproveFee  implements Serializable, Comparable<TmApproveFee> {
	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_INTEREST.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	
	private String dayCounter; //计息基准
	private String refBeginDate;// 开始日
	private String refEndDate;// 结束日
	private String theoryPaymentDate;// 理论收付日期
	private String cfType;// 现金流类型
	private String payDirection;// 收付方向
	private double interestAmt;// 应计利息
	private String dealNo;//交易流水
	private int version;//版本号
	private int seqNumber; //期号
	private double executeRate;// 利率
	private String refNo;
	private String cfBase;//是否是底层资产
	private String feeDealNo;//底层资产名称
	
	public String getCfBase() {
		return cfBase;
	}
	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	
	public String getFeeDealNo() {
		return feeDealNo;
	}
	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	public String getDayCounter() {
		return dayCounter;
	}
	public void setDayCounter(String dayCounter) {
		this.dayCounter = dayCounter;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public String getTheoryPaymentDate() {
		return theoryPaymentDate;
	}
	public void setTheoryPaymentDate(String theoryPaymentDate) {
		this.theoryPaymentDate = theoryPaymentDate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public double getInterestAmt() {
		return interestAmt;
	}
	public void setInterestAmt(double interestAmt) {
		this.interestAmt = interestAmt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public double getExecuteRate() {
		return executeRate;
	}
	public void setExecuteRate(double executeRate) {
		this.executeRate = executeRate;
	}
	private double actualRamt;//实际收到利息
	private String actualDate;//本区间段已计提至日期
	
	public double getActualRamt() {
		return actualRamt;
	}
	public void setActualRamt(double actualRamt) {
		this.actualRamt = actualRamt;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	@Override
	public int compareTo(TmApproveFee o) {
		int c1 = this.getTheoryPaymentDate().compareTo(o.getTheoryPaymentDate());
		if(c1 == 0 ){
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			if(c2==0){
				int c3 =this.getRefEndDate().compareTo(o.getRefEndDate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}
}
