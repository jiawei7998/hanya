package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

/**
 * 每日计提利息
 * 
 * @author xuhui
 * 
 */

@Entity
@Table(name = "TM_CASHFLOW_DAILY_INTEREST")
public class TmCashflowDailyInterest extends CashflowDailyInterest implements Serializable, Comparable<TmCashflowDailyInterest> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public int compareTo(TmCashflowDailyInterest o) {
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			return c2 ; 
	}

	@Override
	public String toString() {
		return super.toString()+"TmCashflowDailyInterest []";
	}
	
}
