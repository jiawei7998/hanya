package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

@Entity
@Table(name = "TD_CASHFLOW_INTEREST")
public class TdCashflowInterest extends CashflowInterest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private double actualRamt;//实际收到利息
	@Transient
	private String actualDate;//本区间段已计提至日期
	@Transient
	private String confirmFlag;//确认标识 1--已确认, null--未确认
	
	@Transient
	private double amt;//本金
	@Transient
	private String prdName;//产品名称
	@Transient
	private String partyAccCode;//对手方账号
	@Transient
	private String partyBankName;//对手方开户行名称
	@Transient
	private String instName;//营销机构
	
	
	
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getPartyAccCode() {
		return partyAccCode;
	}
	public void setPartyAccCode(String partyAccCode) {
		this.partyAccCode = partyAccCode;
	}
	public String getPartyBankName() {
		return partyBankName;
	}
	public void setPartyBankName(String partyBankName) {
		this.partyBankName = partyBankName;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getConfirmFlag() {
		return confirmFlag;
	}
	public void setConfirmFlag(String confirmFlag) {
		this.confirmFlag = confirmFlag;
	}
	public double getActualRamt() {
		return actualRamt;
	}
	public void setActualRamt(double actualRamt) {
		this.actualRamt = actualRamt;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
}
