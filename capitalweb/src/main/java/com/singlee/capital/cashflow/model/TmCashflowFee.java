package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

/**
 * 费用表-集成现金流表
 * @author
 * 
 */

@Entity
@Table(name = "TM_CASHFLOW_FEE")
public class TmCashflowFee extends CashflowInterest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cfBase;//是否是底层资产
	
	private String feeDealNo;//底层资产名称
	
	
	public String getCfBase() {
		return cfBase;
	}

	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	
	public String getFeeDealNo() {
		return feeDealNo;
	}

	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
