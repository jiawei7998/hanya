package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 交易利率区间表  
 * 执行交易的利率表
 * 记录发生的利率区间
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TT_RATE_RANGE")
public class TdRateRange implements Serializable ,Comparable<TdRateRange>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_RATE_RANGE.NEXTVAL FROM DUAL")
	private String id;// 现金流序列号 PK
	
	private String dealNo;//交易流水
	
	private int version;//版本号
	
	private String beginDate;//生效日
	
	private String endDate;//截止日
	
	private double execRate;//执行利率
	
	private String accrulType;//利率类型  1-利息
	
	

	public String getAccrulType() {
		return accrulType;
	}

	public void setAccrulType(String accrulType) {
		this.accrulType = accrulType;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getExecRate() {
		return execRate;
	}

	public void setExecRate(double execRate) {
		this.execRate = execRate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TdRateRange [id=" + id + ", dealNo=" + dealNo + ", version="
				+ version + ", beginDate=" + beginDate + ", endDate=" + endDate
				+ ", execRate=" + execRate + "]";
	}

	@Override
	public int compareTo(TdRateRange o) {
		// TODO Auto-generated method stub
		int c1 = this.getBeginDate().compareTo(o.getBeginDate());
		if(c1 == 0 ){
			int c2 = this.getBeginDate().compareTo(o.getBeginDate());
			if(c2==0){
				int c3 =this.getBeginDate().compareTo(o.getBeginDate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}
	
	
}
