package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 交易还本执行表
 * 记录发生的还款情况
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TT_AMT_RANGE")
public class TdAmtRange implements Serializable ,Comparable<TdAmtRange>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_AMT_RANGE.NEXTVAL FROM DUAL")
	private String id;// 现金流序列号 PK
	
	private String dealNo;//交易流水
	
	private int version;//版本号
	
	private String execDate;//生效日
	
	private double execAmt;//执行本金
	
	private String amtType;//本金类型  1-计划还本 2-提前还本 3-正常到期还本 4-逾期还本
	
	private String refNo;//关联号
	
	private String refTable;//关联表
	
	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getRefTable() {
		return refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getExecDate() {
		return execDate;
	}

	public void setExecDate(String execDate) {
		this.execDate = execDate;
	}

	public double getExecAmt() {
		return execAmt;
	}

	public void setExecAmt(double execAmt) {
		this.execAmt = execAmt;
	}

	public String getAmtType() {
		return amtType;
	}

	public void setAmtType(String amtType) {
		this.amtType = amtType;
	}

	@Override
	public int compareTo(TdAmtRange o) {
		// TODO Auto-generated method stub
		int c1 = this.getExecDate().compareTo(o.getExecDate());
		if(c1 == 0 ){
			int c2 = this.getExecDate().compareTo(o.getExecDate());
			if(c2==0){
				int c3 =this.getExecDate().compareTo(o.getExecDate());
				return c3;
			}
			return c2 ; 
		}else {
			return c1;
		}
	}

	@Override
	public String toString() {
		return "TdAmtRange [id=" + id + ", dealNo=" + dealNo + ", version="
				+ version + ", execDate=" + execDate + ", execAmt=" + execAmt
				+ ", amtType=" + amtType + ", refNo=" + refNo + ", refTable="
				+ refTable + "]";
	}
	
	
}
