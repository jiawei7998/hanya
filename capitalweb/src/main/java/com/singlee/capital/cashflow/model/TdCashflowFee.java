package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.cashflow.model.parent.CashflowInterest;

@Entity
@Table(name = "TD_CASHFLOW_FEE")
public class TdCashflowFee extends CashflowInterest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private double actualRamt;//实际收到利息
	@Transient
	private String actualDate;//本区间段已计提至日期
	
	private String cfBase;//是否是底层资产
	
	private String feeDealNo;//底层资产名称
	
	
	public String getCfBase() {
		return cfBase;
	}

	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	
	
	public String getFeeDealNo() {
		return feeDealNo;
	}

	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}

	public double getActualRamt() {
		return actualRamt;
	}
	public void setActualRamt(double actualRamt) {
		this.actualRamt = actualRamt;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
}
