package com.singlee.capital.cashflow.model.parent;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CashflowAmor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_INTEREST.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK

	public String getCashflowId() {
		return cashflowId;
	}

	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}

	private String dealNo;//交易流水
	
	private int version;//版本号
	/**
	 * 期数
	 */
	private int sheOrder;
	
	private String postdate;
	/**
	 * 应付利息
	 * 面值*票面利率
	 */
	private double interestAmt;
	/**
	 * 实际利息
	 * 摊销金额*实际利率
	 */
	private double actualIntAmt;
	/**
	 * 摊销金额
	 * 应计利息-实际利息
	 */
	private double amorAmt;
	/**
	 * 未摊销金额
	 * 折溢价-已摊销金额
	 */
	private double unAmorAmt;
	/**
	 * 摊余成本
	 * 与实际利率计算实际利息
	 */
	private double settAmt;
	public double getInterestAmt() {
		return interestAmt;
	}
	public void setInterestAmt(double interestAmt) {
		this.interestAmt = interestAmt;
	}
	public double getActualIntAmt() {
		return actualIntAmt;
	}
	public void setActualIntAmt(double actualIntAmt) {
		this.actualIntAmt = actualIntAmt;
	}
	public double getAmorAmt() {
		return amorAmt;
	}
	public void setAmorAmt(double amorAmt) {
		this.amorAmt = amorAmt;
	}
	public double getUnAmorAmt() {
		return unAmorAmt;
	}
	public void setUnAmorAmt(double unAmorAmt) {
		this.unAmorAmt = unAmorAmt;
	}
	public double getSettAmt() {
		return settAmt;
	}
	public void setSettAmt(double settAmt) {
		this.settAmt = settAmt;
	}
	public int getSheOrder() {
		return sheOrder;
	}
	public void setSheOrder(int sheOrder) {
		this.sheOrder = sheOrder;
	}
	
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	private double actAmorAmt;
	private double adjAmorAmt;
	
	
	public double getActAmorAmt() {
		return actAmorAmt;
	}
	public void setActAmorAmt(double actAmorAmt) {
		this.actAmorAmt = actAmorAmt;
	}
	public double getAdjAmorAmt() {
		return adjAmorAmt;
	}
	public void setAdjAmorAmt(double adjAmorAmt) {
		this.adjAmorAmt = adjAmorAmt;
	}
	private double dp1;
	
	private double dp2;
	
	private double yeild;
	public double getDp1() {
		return dp1;
	}
	public void setDp1(double dp1) {
		this.dp1 = dp1;
	}
	public double getDp2() {
		return dp2;
	}
	public void setDp2(double dp2) {
		this.dp2 = dp2;
	}
	public double getYeild() {
		return yeild;
	}
	public void setYeild(double yeild) {
		this.yeild = yeild;
	}
	@Override
	public String toString() {
		return "CashflowAmor [dealNo=" + dealNo + ", version=" + version
				+ ", sheOrder=" + sheOrder + ", postdate=" + postdate
				+ ", interestAmt=" + interestAmt + ", actualIntAmt="
				+ actualIntAmt + ", amorAmt=" + amorAmt + ", unAmorAmt="
				+ unAmorAmt + ", settAmt=" + settAmt + ", actAmorAmt="
				+ actAmorAmt + ", adjAmorAmt=" + adjAmorAmt + ", dp1=" + dp1
				+ ", dp2=" + dp2 + ", yeild=" + yeild + "]";
	}
	
}
