package com.singlee.capital.cashflow.model.parent;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CashflowDailyInterest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dayCounter; //计息基准
	private String refBeginDate;// 开始日
	private String refEndDate;// 结束日
	private double principal;// 本金
	private double interest;// 应计利息
	private int seqNumber; //期号
	private String dealNo;
	private int version;
	private double actualRate;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_CASHFLOW_DAILY_INTEREST.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	

	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	public double getActualRate() {
		return actualRate;
	}
	public void setActualRate(double actualRate) {
		this.actualRate = actualRate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getDayCounter() {
		return dayCounter;
	}
	public void setDayCounter(String dayCounter) {
		this.dayCounter = dayCounter;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	@Override
	public String toString() {
		return "CashflowDailyInterest [dayCounter=" + dayCounter
				+ ", refBeginDate=" + refBeginDate + ", refEndDate="
				+ refEndDate + ", principal=" + principal + ", interest="
				+ interest + ", seqNumber=" + seqNumber + ", dealNo=" + dealNo
				+ ", version=" + version + ", actualRate=" + actualRate + "]";
	}
	
}
