package com.singlee.capital.cashflow.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.service.AmtRangeService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/AmtRangeController")
public class AmtRangeController extends CommonController{
	@Autowired
	private AmtRangeService amtRangeService;
	
	@ResponseBody
	@RequestMapping(value="/queryAmtRangeForDealNo")
	public RetMsg<PageInfo<TdAmtRange>> queryAmtRangeForDealNo(@RequestBody Map<String, Object> params){
		Page<TdAmtRange> ranges = this.amtRangeService.queryAmtRangesByDealNo(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(ranges);
	}
}
