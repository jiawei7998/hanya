package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

public interface TdCashflowDailyInterestMapper extends Mapper<TdCashflowDailyInterest>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */

	/**
	 * 获取每日计提利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowDailyInterest> pageDailyInterestList(Map<String, Object> map, RowBounds rb);
	/**
	 * 删除计提现金流
	 * @param map
	 */
    @Override
    void deleteCashflowDailyInterest(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowIntDayCopy(Map<String, Object> map);
	
	/**
	 * 數據庫已有的版本+1
	 * @param map
	 */
	void updateDailyInterestVersionAddOne(Map<String, Object> map);
	
	
	List<CashflowDailyInterest> selectByDealNoAndDate(Map<String, Object> params);
	
	/**
	 * 获取每日计提利息现金流列表
	 * @param map
	 * @return
	 */
	List<CashflowDailyInterest> pageDailyInterestList(Map<String, Object> map);
	
	double selectInterestByDealNoAndDate(Map<String, Object> map);
}