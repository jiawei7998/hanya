package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;

public interface TdCashflowCapitalMapper extends Mapper<TdCashflowCapital>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	/**
	 *  SELECT  *
		FROM TM_CASHFLOW_CAPITAL T WHERE DEAL_NO=#{dealNo} AND VERSION=#{version}
		ORDER BY to_number(SEQ_NUMBER)
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowCapital> getCapitalList(Map<String, Object> map);
	
	@Override
    void deleteCapitalList(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowCapCopy(Map<String, Object> map);
	
	
	/**
	 * 删除还本计划中未实现真实收款的计划，改为本金计划审批完成的数据
	 * @param map
	 */
	void deleteForApproveNotActual(Map<String, Object> map);
	
	/**
	 * 修改还本计划
	 * @param capital
	 */
	void updateCashflowCapitalById(TdCashflowCapital capital);
	/**
	 * 获得与展期交易起息日最接近的还本计划
	 * @param paramsMap
	 * @return
	 */
	List<TdCashflowCapital> getApproveTradeExtendNearCaplital(Map<String, Object> paramsMap);
	/**
	 * 获得计划日期>=条件日期的还本计划
	 * @param paramsMap
	 * @return
	 */
	List<TdCashflowCapital> getCaplitalsBigThanDate(Map<String, Object> paramsMap);
	
	/**
	 * 查询获得还本计划-未收取列表
	 * @param paramsMap
	 * @return
	 */
	List<TdCashflowCapital>  getCaplitalsIsNotRecive(Map<String, Object> paramsMap);
	/**
	 * 根据 流水号和序列号返回还本计划
	 * @param paramsMap
	 * @return
	 */
	public TdCashflowCapital getCapitalByDealNoAndSeqNumber(Map<String, Object> paramsMap);
	
	public void insertAllProperty(Map<String, Object> paramsMap);
	
	List<CashflowCapital> getCapitalListByRef(Map<String, Object> beanToMap);
	
	
	List<TdCashflowCapital> getCapitalListByDate(Map<String, Object> map);

    Page<CashflowCapital> getCapitalPage(Map<String, Object> params, RowBounds rowBounds);
}