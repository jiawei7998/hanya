package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;

public interface TmCashflowInterestMapper extends Mapper<TmCashflowInterest> , CashFlowMapper{
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowInterest> getInterestList(Map<String, Object> map);
	
	/**
	 * 删除计提计划现金流
	 * @param params
	 */
    @Override
    void deleteInterestList(Map<String, Object> params);

}