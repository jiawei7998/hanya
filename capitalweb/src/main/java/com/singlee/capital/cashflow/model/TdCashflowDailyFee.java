package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

@Entity
@Table(name = "TD_CASHFLOW_DAILY_FEE")
public class TdCashflowDailyFee extends CashflowDailyInterest implements Serializable, Comparable<TdCashflowDailyFee> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public int compareTo(TdCashflowDailyFee o) {
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			return c2 ; 
	}
	private String cfType;// 现金流类型
	
	private String cfBase;//是否是底层资产
	
	private String feeDealNo;//底层资产名称
	
	
	public String getCfBase() {
		return cfBase;
	}

	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}

	public String getFeeDealNo() {
		return feeDealNo;
	}

	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}

	public String getCfType() {
		return cfType;
	}

	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
