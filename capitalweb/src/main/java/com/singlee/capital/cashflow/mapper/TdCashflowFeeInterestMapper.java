package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowFeeInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;

public interface TdCashflowFeeInterestMapper extends Mapper<TdCashflowFeeInterest>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowInterest> getInterestList(Map<String, Object> map);
	
	@Override
    void deleteInterestList(Map<String, Object> map);

	List<TdCashflowFeeInterest> getFeeInterestByGlEDate(Map<String, Object> map);
	
	@Override
    Page<CashflowInterest> getInterestList(Map<String, Object> map, RowBounds rb);
	
}