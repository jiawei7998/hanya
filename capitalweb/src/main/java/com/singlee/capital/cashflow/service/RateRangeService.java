package com.singlee.capital.cashflow.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdRateRange;

public interface RateRangeService {
	/**
	 * 更新利率区间-利率调整
	 */
	public void updateRateRangeByRateChange(Map<String,Object> params);
	/**
	 * 交易完成审批-生成对应的利率区间
	 * @param cashflowInterests
	 */
	public void createRateRangeByCompleteTrade(Map<String, Object> params);
	/**
	 * 根据业务流水查询系统目前最新的利率区间
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdRateRange>  queryRateRangesByDealNo(Map<String,Object> params,RowBounds rb);
	
}
