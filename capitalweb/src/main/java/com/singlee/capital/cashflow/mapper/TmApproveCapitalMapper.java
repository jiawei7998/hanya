package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.model.TmApproveCapital;

public interface TmApproveCapitalMapper extends Mapper<TmApproveCapital>{
	
	List<TmApproveCapital> getApproveCapitalList(Map<String, Object> map); 
	
	void deleteApproveCapitalList(Map<String, Object> map);
	/**
	 * 计划变更审批通过后调整原计划表
	 */
	void insertApproveCapitalToTd(Map<String, Object> map);
}
