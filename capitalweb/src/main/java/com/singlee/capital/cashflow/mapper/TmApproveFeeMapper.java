package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.model.TmApproveFee;

public interface TmApproveFeeMapper extends Mapper<TmApproveFee>{
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	List<TmApproveFee> getTmApproveFeeList(Map<String, Object> map);
	
	
	void deleteTmApproveFeeList(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
	void insertTmApproveFeeCopy(Map<String, Object> map);
	
	void insertApproveCapitalToTd(Map<String, Object> map);

}