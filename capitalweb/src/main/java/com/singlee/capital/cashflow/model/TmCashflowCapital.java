package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowCapital;

/**
 * 现金流(利息) 类
 * 
 * @author wang
 * 
 */

@Entity
@Table(name = "TM_CASHFLOW_CAPITAL")
public class TmCashflowCapital extends CashflowCapital implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
