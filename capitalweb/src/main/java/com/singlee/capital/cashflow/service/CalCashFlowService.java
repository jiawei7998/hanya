package com.singlee.capital.cashflow.service;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.cashflow.model.TmApproveCapital;
import com.singlee.capital.cashflow.model.TmApproveFee;
import com.singlee.capital.cashflow.model.TmApproveInterest;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 临时审批计划现金流计算方法
 * @author SINGLEE
 * @version 201705
 *
 */
public interface CalCashFlowService {
	/**
	 * 还本计划手动更改还本金额
	 * 触发联动修改收息计划/摊销计划调整
	 * @param params
	 */
	public void updateByHandleCaptitalAmt(Map<String,Object> params);
	
	/**
	 * 收息计划手工更改
	 * 直接影响数据库 
	 * 收息计划+每日计提+摊销重算
	 * @param params
	 */
	public void updateCashFlowInterestByHandle(Map<String,Object> params);
	/**
	 * 每日计划
	 * @param params
	 * @return
	 */
	public Page<?> getInterestDailyCashFlowForApprove(Map<String,Object> params,RowBounds rb);
	
	public Page<?> getFeeDailyCashFlowForApprove(Map<String,Object> params,RowBounds rb);
	
	/**
	 * 审批过程中使用的利息计划查询
	 * @param params
	 */
	public List<?> getInterestCashFlowForApprove(Map<String,Object> params);
	
	/**
	 * 审批过程中使用的还本计划查询
	 * @param params
	 */
	public List<?> getCapitalCashFlowForApprove(Map<String,Object> params);
	/**
	 * 审批过程中使用的收费计划查询
	 * @param params
	 * @return
	 */
	public List<?> getfeeCashFlowForApprove(Map<String,Object> params);
	
	/**
	 * 审批过程中使用计划变更数据
	 * @param params
	 */
	public List<TmApproveCapital> getApproveCapitalForApprove(Map<String,Object> params);
	
	/**
	 * 审批过程中使用计划变更数据
	 * @param params
	 * @return
	 */
	public List<TmApproveInterest> getApproveInterestForApprove(Map<String,Object> params);
	
	public List<TmApproveFee> getApproveFeeForApprove(Map<String,Object> params);
	/**
	 * 审批过程中使用的收息计划查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<?> pageTmDailyTmCashflowAmorList(Map<String, Object> map, RowBounds rb);

	/**
	 * 业务审批创建保存时调用生成收息计划（saveApprove）
	 * @param params
	 * @return
	 */
	public List<?> createInterestCashFlowForSaveApprove(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges,Map<String,Object> params);
	/**
	 * 业务审批创建保存时调用生成还本计划（saveApprove）
	 * @param params
	 */
	public List<?> createCapitalCashFlowForSaveApprove(Map<String,Object> params);
		
	/**
	 * 业务审批创建保存时调用生成摊销日计划（saveApprove）
	 * @param pList  利息计划
	 * @param principalSchedules 本金计划
	 * @param interestRanges 利率区间
	 * @param params
	 */
	public void createAmorCashFlowForSaveApprove(List<CashflowInterest> pList,List<CashflowCapital> principalSchedules,List<InterestRange> interestRanges,Map<String, Object> params); 
	/**
	 * 审批保存的综合按钮
	 * @param principalIntervals
	 * @param interestRanges
	 * @param params
	 */
	public void createAllCashFlowsForSaveApprove(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges,Map<String,Object> params);
	/**
	 * 审批到核实的COPY方法
	 * @param params
	 */
	public void createAllCashFlowsForTradCopy(Map<String,Object> params);
	/**
	 * 删除所有的现金流
	 * @param params
	 */
	public void deleteAllCashFlowsForDeleteDeal(Map<String, Object> params);

	public void createAllCashFlowsExpFees(List<PrincipalInterval> principalIntervals,
			List<InterestRange> interestRanges, Map<String, Object> params,
			TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap);
	
	/**
	 * 业务审批创建保存时调用生成收息计划（saveApprove）
	 * @param params
	 * @return
	 */
	public List<?> createInterestCashFlowExpFees(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges,Map<String, Object> params,TdProductApproveMain productApproveMainOrg,List<TdFeesPassAgewayDaily> tdFeesPassAgewayDailies);
	
	
	public Object getCashFlowFeeInterest(Map<String, Object> map);
	
	
	public Page<?> getCashFlowFeeInterest(Map<String, Object> map, RowBounds rb);
	
	public Page<CashflowInterest> getInterestList(Map<String, Object> map, RowBounds rb);
	
	public void createIntCashFlowsExpFees(List<CashflowCapital> cashflowCapitals,List<PrincipalInterval> principalIntervals,
			List<InterestRange> interestRanges, Map<String, Object> params,
			TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap);

	void createHrbCashFLow(List<PrincipalInterval> principalIntervals, List<InterestRange> interestRanges, Map<String, Object> map);
	public List<CashflowCapital> createCapitalCashFlowForSaveApproveHrb(Map<String, Object> params);

	public void saveCaptialAndInterestCashFlowHrb(Map<String,Object> param);

	public List<CashflowInterest>  calInterestFlowListHrb(List<InterestRange> interestRanges,Map<String,Object> param);
}
