package com.singlee.capital.cashflow.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmCashflowDailyFee;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

public interface TmCashflowDailyFeeMapper extends Mapper<TmCashflowDailyFee>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */

	/**
	 * 获取每日计提利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowDailyInterest> pageDailyInterestList(Map<String, Object> map, RowBounds rb);
	/**
	 * 删除计提现金流
	 * @param map
	 */
	void deleteCashflowDailyFee(Map<String, Object> map);
	
}