package com.singlee.capital.cashflow.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * 处理费用的service类
 * @author SINGLEE
 *
 */
public interface CashflowFeeService {

	
	/**
	 * 生成费用-根据TC_PRODUCT_FEE_CAL的规则
	 * 进行判定哪些费用是需要进入计提的，按照各自费用频率计息基础去完成分拣
	 * 如果没有费用各自的计息基础和频率，那么直接匹配交易
	 * 
	 * @param paramsmMap（TdProductApproveMain）
	 */
	public void createFeeForProductDeals(Map<String, Object> paramsmMap);
	
	/**
	 * 通过本金调整引起的费用重新计算
	 * @param paramsmMap  交易MAP
	 * @param principalIntervals 本金区间
	 */
	public void updateByHandleCaptitalAmt(Map<String, Object> paramsMap,List<PrincipalInterval> principalIntervals);
	
	
	public void createFeeForNostro(Map<String, Object> params, TdProductApproveMain productApproveMain);
	
}
