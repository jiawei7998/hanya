package com.singlee.capital.cashflow.mapper.parent;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.parent.CashflowAmor;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;

public abstract interface CashFlowMapper {
	/**
	 * 删除所属本金现金流
	 * @param params
	 */
	void deleteCapitalList(Map<String, Object> params);
	/**
	 * 删除每日计提现金流
	 * @param params
	 */
	void deleteCashflowDailyInterest(Map<String, Object> params);
	/**
	 * 删除计提计划现金流
	 * @param params
	 */
	void deleteInterestList(Map<String, Object> params);
	/**
	 * 删除摊销现金流
	 */
	void deletCashflowAmor(Map<String, Object> params);
	
	/**
	 * 返回计划现金流
	 * @param map
	 * @return
	 */
	List<CashflowInterest> getInterestList(Map<String, Object> map);
	/**
	 * 返回本金现金流
	 * @param map
	 * @return
	 */
	List<CashflowCapital> getCapitalList(Map<String, Object> map); 
	
	Page<CashflowAmor> pageDailyCashflowAmorList(Map<String, Object> map, RowBounds rb);
	
	Page<CashflowDailyInterest> pageDailyInterestList(Map<String, Object> map, RowBounds rb);
	
	void insertCashFlowAmorDayCopy(Map<String, Object> map);
	
	void insertCashFlowCapCopy(Map<String, Object> map);
	
	void insertCashFlowIntDayCopy(Map<String, Object> map);
	
	void insertCashFlowIntCopy(Map<String, Object> map);
	
	void insertCashFlowFeeCopy(Map<String, Object> map);
	
	void insertCashFlowFeeDayCopy(Map<String, Object> map);
	
	/**
	 * 返回计划现金流
	 * @param map
	 * @return
	 */
	Page<CashflowInterest> getInterestList(Map<String, Object> map, RowBounds rb);
	
}
