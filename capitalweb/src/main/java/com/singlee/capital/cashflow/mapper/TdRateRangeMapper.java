package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdRateRange;
/**
 * 只有真实交易审批通过后才有该表的记录
 * @author SINGLEE
 *
 */
public interface TdRateRangeMapper extends Mapper<TdRateRange>{
	/**
	 * 查询获得某交易向下的所有TdRateRange数据
	 * @param params
	 * @return
	 */
	public List<TdRateRange> getRateRanges(Map<String, Object> params);
	/**
	 *  删除某交易向下的利率区间数据
	 * @param params
	 */
	public void deleteRateRanges(Map<String, Object> params);
	
	/**查询返回在2个约定区间内的利率区间
	 * refNo  version
	 * startDate endDate
	 * @return
	 */
	public List<TdRateRange> geTdRateRangesBetweenDay(Map<String, Object> params);
	
	/**
	 * 根据流水号获得最新的利率区间
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<TdRateRange> pageRateRangesByDealNo(Map<String, Object> map, RowBounds rb);
}
