package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

/**
 * 每日费用计提
 * 
 */

@Entity
@Table(name = "TM_CASHFLOW_DAILY_FEE")
public class TmCashflowDailyFee extends CashflowDailyInterest implements Serializable, Comparable<TmCashflowDailyFee> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cfType;// 现金流类型
	
	public String getCfType() {
		return cfType;
	}

	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	private String cfBase;//是否是底层资产
	
	private String feeDealNo;//底层资产名称
	
	
	public String getCfBase() {
		return cfBase;
	}

	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	
	public String getFeeDealNo() {
		return feeDealNo;
	}

	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}

	@Override
	public int compareTo(TmCashflowDailyFee o) {
			int c2 = this.getRefBeginDate().compareTo(o.getRefBeginDate());
			return c2 ; 
	}


	@Override
	public String toString() {
		return super.toString()+"TmCashflowDailyFee []";
	}
	
}
