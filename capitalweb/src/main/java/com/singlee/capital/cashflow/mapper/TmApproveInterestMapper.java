package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.model.TmApproveInterest;

public interface TmApproveInterestMapper extends Mapper<TmApproveInterest>{
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	List<TmApproveInterest> getTmApproveInterestList(Map<String, Object> map);
	
	
	void deleteTmApproveInterestList(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
	void insertTmApproveInterestCopy(Map<String, Object> map);
	
	void insertApproveCapitalToTd(Map<String, Object> map);

}