package com.singlee.capital.cashflow.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowDailyFee;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;

public interface TdCashflowDailyFeeMapper extends Mapper<TdCashflowDailyFee>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */

	/**
	 * 获取每日计提利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowDailyInterest> pageDailyInterestList(Map<String, Object> map, RowBounds rb);
	/**
	 * 删除计提现金流
	 * @param map
	 */
	void deleteTdCashflowDailyFee(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowFeeDayCopy(Map<String, Object> map);
	
}