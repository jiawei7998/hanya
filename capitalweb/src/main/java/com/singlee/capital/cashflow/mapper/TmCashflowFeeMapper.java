package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmCashflowFee;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
/**
 * 费用审批表
 * @author SINGLEE
 *
 */
public interface TmCashflowFeeMapper extends Mapper<TmCashflowFee> ,CashFlowMapper{
	
	/**
	 * 获取费用现金流列表
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowInterest> getInterestList(Map<String, Object> map);
	
	/**
	 * 删除计提计划现金流
	 * @param params
	 */
	void deleteTmCashflowFeeList(Map<String, Object> params);

}