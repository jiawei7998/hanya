package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdAmtRange;
/**
 * 实际还本记录表
 * @author SINGLEE
 *
 */
public interface TdAmtRangeMapper extends Mapper<TdAmtRange>{
	/**
	 * 获取全部实际还本记录
	 * @param params
	 * @return
	 */
	public List<TdAmtRange> getAmtRanges(Map<String, Object> params);
	
	Page<TdAmtRange> pageAmtRangesByDealNo(Map<String, Object> map, RowBounds rb);
	/**
	 * 根据 dealNo  id  ld_no查询唯一数据
	 * @param params
	 * @return
	 */
	public TdAmtRange getAmtRangeByIdAndLdAndDealNo(Map<String, Object> params);
}
