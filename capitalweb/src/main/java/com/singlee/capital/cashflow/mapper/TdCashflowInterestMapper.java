package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;

public interface TdCashflowInterestMapper extends Mapper<TdCashflowInterest>,CashFlowMapper {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowInterest> getInterestList(Map<String, Object> map);
	
	
	@Override
    void deleteInterestList(Map<String, Object> map);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowIntCopy(Map<String, Object> map);
	
	/**
	 * 查询出需要重新计算的收息区间 （还本计划调整审批）
	 * @param map
	 * @return
	 */
	List<TdCashflowInterest> getInterestListForApprove(Map<String, Object> map);
	/**
	 * 删除未收息确认的计划数据（计划变更调整审批）
	 * @param map
	 */
	void deleteForApproveNotActual(Map<String, Object> map);
	
	void updateCashflowInterestById(TdCashflowInterest cashflowInterest);
	/**
	 * 根据 日期查询出未进行收息处理的计划
	 * @param map
	 * @return
	 */
	public List<TdCashflowInterest> geTdCashFlowIntBetweenDay(Map<String, Object> map);
	
	/**
	 * 获取收息计划中未实际发生收息的计划，且 REF_BEGINDATE>=EFFECTDATE
	 * @param map
	 * @return
	 */
	List<TdCashflowInterest>  getCashflowInterestsIsNotTrueRecive(Map<String, Object> map);
	/**
	 * 根据流水和序列查询得到某条利息计划
	 * @param map
	 * @return
	 */
	public TdCashflowInterest  getInterestByDealNoAndSeqNumber(Map<String, Object> map);
	
	
	/**
	 * 根据 日期查询出未进行收息处理的计划
	 * @param map
	 * @return
	 */
	public List<TdCashflowInterest> geTdCashFlowIntByDate(Map<String, Object> map);
	
	
	
	
	/**
	 * 获取收息计划确认列表
	 * @param map
	 * @return
	 */
	Page<TdCashflowInterest> pageCfComfirm(Map<String, Object> map, RowBounds rb);
	/**
	 * 修改收息计划确认状态
	 * @param interest
	 */
	boolean updateConfirmFlag(List<TdCashflowInterest> tdCashflowInterest);
	
	List<TdCashflowInterest> queryPrintList(Map<String, Object> map);
	
	public List<TdCashflowInterest> getInterestListUnconfirmed(Map<String, Object> map);
	
	/**
	 * 获取利息现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowInterest> getInterestList(Map<String, Object> map, RowBounds rb);
	
	public String getMaxActualDate(Map<String, Object> map);

	Page<CashflowInterest> getInterestPage(Map<String, Object> params, RowBounds rb);
}