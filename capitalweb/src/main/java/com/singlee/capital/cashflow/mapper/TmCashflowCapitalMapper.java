package com.singlee.capital.cashflow.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmCashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;

public interface TmCashflowCapitalMapper extends Mapper<TmCashflowCapital>,CashFlowMapper{
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	/**
	 *  SELECT  *
		FROM TM_CASHFLOW_CAPITAL T WHERE DEAL_NO=#{dealNo} AND VERSION=#{version}
		ORDER BY to_number(SEQ_NUMBER)
	 * @param map
	 * @return
	 */
    @Override
    List<CashflowCapital> getCapitalList(Map<String, Object> map);
	
	@Override
    void deleteCapitalList(Map<String, Object> map);
}