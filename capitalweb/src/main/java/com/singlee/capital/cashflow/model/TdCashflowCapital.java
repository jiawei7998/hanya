package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.cashflow.model.parent.CashflowCapital;

/**
 * 真实交易对应的本金计划表
 * @author SINGLEE
 *
 */
@Entity
@Table(name = "TD_CASHFLOW_CAPITAL")
public class TdCashflowCapital extends CashflowCapital implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private String repaymentTdate;//实际还本日期
	@Transient
	private double repaymentTamt;//实际还本金额
	@Transient
	private String cfEvent;
	
	public String getCfEvent() {
		return cfEvent;
	}
	public void setCfEvent(String cfEvent) {
		this.cfEvent = cfEvent;
	}
	public String getRepaymentTdate() {
		return repaymentTdate;
	}
	public void setRepaymentTdate(String repaymentTdate) {
		this.repaymentTdate = repaymentTdate;
	}
	public double getRepaymentTamt() {
		return repaymentTamt;
	}
	public void setRepaymentTamt(double repaymentTamt) {
		this.repaymentTamt = repaymentTamt;
	}
	@Override
	public String toString() {
		return super.toString()+"TdCashflowCapital [repaymentTdate=" + repaymentTdate + ", repaymentTamt="
				+ repaymentTamt + "]";
	}
	
	
}
