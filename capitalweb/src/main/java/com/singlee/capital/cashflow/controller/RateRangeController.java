package com.singlee.capital.cashflow.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.service.RateRangeService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value="/RateRangeController")
public class RateRangeController extends CommonController{
	@Autowired
	private RateRangeService rateRangeService;
	
	@ResponseBody
	@RequestMapping(value="/queryRateRangeForDealNo")
	public RetMsg<PageInfo<TdRateRange>> queryRateRangeForDealNo(@RequestBody Map<String, Object> params){
		Page<TdRateRange> ranges = this.rateRangeService.queryRateRangesByDealNo(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(ranges);
	}
}
