package com.singlee.capital.cashflow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.capital.cashflow.model.parent.CashflowAmor;
/**
 * 用于记录摊销数据
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TM_CASHFLOW_AMOR")
public class TmCashflowAmor extends CashflowAmor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6152324569813017536L;
	

	@Override
	public String toString() {
		return super.toString();
	}
	
	
}
