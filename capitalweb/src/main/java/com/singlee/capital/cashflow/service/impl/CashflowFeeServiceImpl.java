package com.singlee.capital.cashflow.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyFeeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowFeeMapper;
import com.singlee.capital.cashflow.mapper.TmCashflowDailyFeeMapper;
import com.singlee.capital.cashflow.mapper.TmCashflowFeeMapper;
import com.singlee.capital.cashflow.model.TdCashflowDailyFee;
import com.singlee.capital.cashflow.model.TdCashflowFee;
import com.singlee.capital.cashflow.model.TmCashflowDailyFee;
import com.singlee.capital.cashflow.model.TmCashflowFee;
import com.singlee.capital.cashflow.model.TmCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TdAssetSpecificBondMapper;
import com.singlee.capital.trade.mapper.TdProductFeeCalMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.model.TdAssetSpecificBond;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.model.TdProductFeeCal;
import com.singlee.capital.trade.model.TdProductFeeDeal;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CashflowFeeServiceImpl implements CashflowFeeService {

	@Autowired
	private TdProductFeeCalMapper productFeeCalMapper;// 获得费用配置表
	@Autowired
	private TmCashflowFeeMapper tmCashflowFeeMapper;
	@Autowired
	private TdCashflowFeeMapper tdCashflowFeeMapper;
	@Autowired
	private TdProductFeeDealMapper productFeeDealMapper;// 费用交易
	@Autowired
	private TmCashflowDailyFeeMapper tmCashflowDailyFeeMapper;
	@Autowired
	private TdCashflowDailyFeeMapper tdCashflowDailyFeeMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TdAssetSpecificBondMapper assetSpecificBondMapper;

	@Override
	public void createFeeForProductDeals(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		// 获取产品项下的所有费用配置列表；去掉contractRate
		// 判定FEE_CEXP是否为1-true，是那么进入计算
		// 判定是否是前收息费用模式，如果是 那么 生成的费用计划为1条，区间为划款日-划款日
		// 判定为后收息，那么需要看是否有自己的频率和计息基础，有那么按照自己的出（VDATE-MDATE）之间的计划
		// 没有，那么按照主交易的模式出 （VDATE-MDATE）之间的计划
		String dealType = ParameterUtil.getString(paramsMap, "dealType", "1");
		// 每次需要清除费用计划对应的费用交易
		productFeeDealMapper.deleteProductFeeDeals(paramsMap);
		// 删除费用计划及每日费用计提
		if (DictConstants.DealType.Approve.equalsIgnoreCase(dealType)) {
			tmCashflowFeeMapper.deleteTmCashflowFeeList(paramsMap);// 每次必须先清理掉费用数据表数据
			tmCashflowDailyFeeMapper.deleteCashflowDailyFee(paramsMap);// 每次必须先清理掉费用数据表数据
		} else {
			tdCashflowFeeMapper.deleteTdCashflowFeeList(paramsMap);
			tdCashflowDailyFeeMapper.deleteTdCashflowDailyFee(paramsMap);// 每次必须先清理掉费用数据表数据
		}

		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		BeanUtil.populate(productApproveMain, paramsMap);
		// 费用现金流
		List<TmCashflowFee> cashflowFees = new ArrayList<TmCashflowFee>();
		List<TmCashflowDailyFee> cashflowDailyFees = new ArrayList<TmCashflowDailyFee>();
		// 费用交易
		TdProductFeeDeal tdProductFeeDeal = null;

		TmCashflowFee tmCashflowFee = null;
		TmCashflowDailyFee tmCashflowDailyFee = null;
		paramsMap.put("cfBase", "0");
		List<TdProductFeeCal> productFeeCals = this.productFeeCalMapper.getProductFeeCalsByPrdNo(paramsMap);
		// 封装给脚本引擎使用
		Map<String, Object> feeMap = new HashMap<String, Object>();
		Object object = productApproveMain.getParticipantApprove();
		if (object != null) {
			/**
			 * 循环参与方
			 */
			List<TdProductApproveSub> tdSubs = FastJsonUtil.parseArrays(object.toString(), TdProductApproveSub.class);
			for (TdProductApproveSub subs : tdSubs) {
				paramsMap.put(subs.getSubFldName(), subs.getSubFldValue());
			}
		}
		feeMap.put("feeMap", paramsMap);
		try {
			for (TdProductFeeCal productFeeCal : productFeeCals) {
				if ("contractRate".equalsIgnoreCase(productFeeCal.getFeeType())) {
					continue;
				}
				if (calcBkIndex(feeMap, productFeeCal.getIsFeeDaily())) {// 1-true 表示需要进入计提的费率
					tmCashflowFee = new TmCashflowFee();
					// 获取配置项的收费模式
					if (ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x")
							.equalsIgnoreCase(DictConstants.intType.chargeBefore))// 先收费
					{
						tmCashflowFee.setCfType(productFeeCal.getFeeType());
						tmCashflowFee
								.setDayCounter(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
						tmCashflowFee.setDealNo(productApproveMain.getDealNo());
						tmCashflowFee
								.setExecuteRate(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
						tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
						tmCashflowFee.setRefBeginDate(productApproveMain.getvDate());// 划款日
						tmCashflowFee.setRefEndDate(productApproveMain.getmDate());//
						tmCashflowFee.setSeqNumber(1);
						tmCashflowFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
						tmCashflowFee.setTheoryPaymentDate(productApproveMain.gettDate());// 预计回款日
						tmCashflowFee.setVersion(productApproveMain.getVersion());
						double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);
						if (null != productFeeCal.getFeeCbeforeAmt()) {
							tmCashflowFee.setInterestAmt(
									ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCbeforeAmt(), 0.00));
						} else {
							// 计算收费一次性金额
							tmCashflowFee.setInterestAmt(new BigDecimal(PlaningTools
									.div(PlaningTools.mul(PlaningTools.mul(feeAmt, tmCashflowFee.getExecuteRate())// 金额*利率
											,
											PlaningTools.daysBetween(productApproveMain.getvDate(),
													productApproveMain.getmDate())),
											DayCountBasis.equal(tmCashflowFee.getDayCounter()).getDenominator()// 计息基础
									)).setScale(2, java.math.BigDecimal.ROUND_HALF_UP).doubleValue());// 保留2位数
						}
						/************************* 赋值费用交易 ************************/
						tdProductFeeDeal = new TdProductFeeDeal();
						tdProductFeeDeal.setAccruedTint(0.00);
						tdProductFeeDeal.setActualTint(0.00);
						tdProductFeeDeal.setCfBase("0");// 非底层资产
						tdProductFeeDeal.setCfName("");
						tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
						tdProductFeeDeal
								.setFeeCamt(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00));
						tdProductFeeDeal
								.setFeeCbasis(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
						tdProductFeeDeal.setFeeCbeforeAmt(tmCashflowFee.getInterestAmt());
						tdProductFeeDeal
								.setFeeCfre(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x"));
						tdProductFeeDeal
								.setFeeCtype(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
						tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
						tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
						tdProductFeeDeal
								.setFeeRate(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
						tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
						tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
						tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
						tdProductFeeDeal.setVersion(productApproveMain.getVersion());
						productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);
						tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
						cashflowFees.add(tmCashflowFee);

					} else {
						/**
						 * 假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行费用及周期判定
						 */
						List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();// 返回的每日计提
						List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();// 返回的 收费计划

						/**
						 * TC_PRODUCT_FEE_CAL表获得所有的费用配置项[计息基础、费率、收费频率、费用计算本金]
						 */
						String basicType1 = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x");// 计息基础
						double feeRate = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00);// 费率
						String intFre = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x");// 收费频率
						double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);// 计费使用的本金

						/************************* 赋值费用交易 ************************/
						tdProductFeeDeal = new TdProductFeeDeal();
						tdProductFeeDeal.setAccruedTint(0.00);
						tdProductFeeDeal.setActualTint(0.00);
						tdProductFeeDeal.setCfBase("0");// 非底层资产
						tdProductFeeDeal.setCfName("");
						tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
						tdProductFeeDeal.setFeeCamt(feeAmt);
						tdProductFeeDeal.setFeeCbasis(basicType1);
						tdProductFeeDeal.setFeeCbeforeAmt(0.00);
						tdProductFeeDeal.setFeeCfre(intFre);
						tdProductFeeDeal
								.setFeeCtype(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
						tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
						tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
						tdProductFeeDeal.setFeeRate(feeRate);
						tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
						tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
						tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
						tdProductFeeDeal.setVersion(productApproveMain.getVersion());
						productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);

						/**
						 * 生成交易的时候 费用只会出现1个区间 [VDATE,MDATE,FEERATE]
						 */
						List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 费用变化区间 固定的
						InterestRange interestRange = new InterestRange();
						interestRange.setStartDate(productApproveMain.getvDate());
						interestRange.setEndDate(productApproveMain.getmDate());
						interestRange.setRate(feeRate);
						interestRanges.add(interestRange);
						/**
						 * 生成交易的时候 费用本金只会出现1个区间 [VDATE,MDATE,FEEAMT]
						 */
						List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();// 本金变化区间
																										// 初始的时候只要一个
						PrincipalInterval principalInterval = new PrincipalInterval();
						principalInterval.setStartDate(productApproveMain.getvDate());
						principalInterval.setEndDate(productApproveMain.getmDate());
						principalInterval.setResidual_Principal(feeAmt);
						principalIntervals.add(principalInterval);
						/**
						 * [通过费用的计息频率 推演 实际的 basis]
						 */
						int basis = DictConstants.DayCounter.Actual360.equals(basicType1) ? 1
								: DictConstants.DayCounter.Actual365.equals(basicType1) ? 2 : 3;

						// 后收息的时候按照计提的模式算一遍
						if (com.singlee.capital.base.cal.Frequency
								.valueOf(Integer.parseInt(intFre)) == com.singlee.capital.base.cal.Frequency.ONCE) {
							PlaningTools.calPlanSchedule(interestRanges, principalIntervals, tmCashflowDailyInterests,
									tmCashflowInterests, productApproveMain.getvDate(), productApproveMain.getmDate(),
									productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
									feeAmt, feeRate,
									com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
									DayCountBasis.valueOf(basis));
						} else {
							// 起息日-第一个收费时间点的区间
							if (!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(),
									"yyyy-MM-dd")) {
								TmCashflowFee fistPlanSchedule = new TmCashflowFee();
								fistPlanSchedule.setSeqNumber(1);
								fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
								fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
								fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
								fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,
										principalIntervals, tmCashflowDailyInterests,
										fistPlanSchedule.getRefBeginDate(), fistPlanSchedule.getRefEndDate(),
										productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
								fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
								fistPlanSchedule.setExecuteRate(feeRate);
								tmCashflowInterests.add(fistPlanSchedule);
							}
							PlaningTools.calPlanSchedule(interestRanges, principalIntervals, tmCashflowDailyInterests,
									tmCashflowInterests, productApproveMain.getsDate(), productApproveMain.geteDate(),
									productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
									feeAmt, feeRate,
									com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
									DayCountBasis.valueOf(basis));
							// 最后一次收费日-到期日
							if (!PlaningTools.compareDate3(productApproveMain.getmDate(), productApproveMain.geteDate(),
									"yyyy-MM-dd")) {
								TmCashflowInterest lastPlanSchedule = new TmCashflowInterest();
								lastPlanSchedule.setSeqNumber(tmCashflowInterests.size() + 1);
								lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
								lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
								lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
								lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,
										principalIntervals, tmCashflowDailyInterests,
										lastPlanSchedule.getRefBeginDate(), lastPlanSchedule.getRefEndDate(),
										productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
								lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
								lastPlanSchedule.setExecuteRate(feeRate);
								tmCashflowInterests.add(lastPlanSchedule);
							}
						}
						/**
						 * 组装到统一的费用 LIST(计算的时候采用利息的父类）
						 */
						for (CashflowInterest cashflowInterest : tmCashflowInterests) {

							tmCashflowFee = new TmCashflowFee();
							BeanUtil.copyNotEmptyProperties(tmCashflowFee, cashflowInterest);
							tmCashflowFee.setDealNo(productApproveMain.getDealNo());
							tmCashflowFee.setCfType(productFeeCal.getFeeType());
							tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
							tmCashflowFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
							tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
							tmCashflowFee.setVersion(productApproveMain.getVersion());
							cashflowFees.add(tmCashflowFee);
						}
						// 费用计提明细
						for (int j = 0; j < tmCashflowDailyInterests.size(); j++) {
							tmCashflowDailyFee = new TmCashflowDailyFee();
							BeanUtil.copyNotEmptyProperties(tmCashflowDailyFee, tmCashflowDailyInterests.get(j));
							tmCashflowDailyFee.setDealNo(productApproveMain.getDealNo());
							tmCashflowDailyFee.setVersion(productApproveMain.getVersion());
							tmCashflowDailyFee.setCfType(productFeeCal.getFeeType());
							tmCashflowDailyFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
							tmCashflowDailyFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
							cashflowDailyFees.add(tmCashflowDailyFee);
						}
					}

				}
			}
			/*********************************************************************************************************************************/
			// 基础资产开始获取费用情况
			paramsMap.put("cfBase", "1");
			productFeeCals = this.productFeeCalMapper.getProductFeeCalsByPrdNo(paramsMap);
			// 获取该资产下属的所有基础资产
			if (productFeeCals.size() > 0) {
				List<TdAssetSpecificBond> assetSpecificBonds = assetSpecificBondMapper
						.getTdAssetSpecificBondList(paramsMap);
				for (TdAssetSpecificBond assetSpecificBond : assetSpecificBonds) {
					feeMap.put("feeMap", BeanUtil.beanToHashMap(assetSpecificBond));
					for (TdProductFeeCal productFeeCal : productFeeCals) {
						if ("contractRate".equalsIgnoreCase(productFeeCal.getFeeType())) {
							continue;
						}
						if (calcBkIndex(feeMap, productFeeCal.getIsFeeDaily())) {// 1-true 表示需要进入计提的费率
							tmCashflowFee = new TmCashflowFee();
							// 获取配置项的收费模式
							if (ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x")
									.equalsIgnoreCase(DictConstants.intType.chargeBefore))// 先收费
							{
								tmCashflowFee.setCfType(productFeeCal.getFeeType());
								tmCashflowFee.setDayCounter(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
								tmCashflowFee.setDealNo(productApproveMain.getDealNo());
								tmCashflowFee.setExecuteRate(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
								tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
								tmCashflowFee.setRefBeginDate(productApproveMain.getvDate());// 划款日
								tmCashflowFee.setRefEndDate(productApproveMain.getmDate());//
								tmCashflowFee.setSeqNumber(1);
								tmCashflowFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
								tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
								tmCashflowFee.setTheoryPaymentDate(productApproveMain.gettDate());// 预计回款日
								tmCashflowFee.setVersion(productApproveMain.getVersion());
								double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);
								if (null != productFeeCal.getFeeCbeforeAmt()) {
									tmCashflowFee.setInterestAmt(
											ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCbeforeAmt(), 0.00));
								} else {
									// 计算收费一次性金额
									tmCashflowFee.setInterestAmt(new BigDecimal(PlaningTools.div(
											PlaningTools.mul(PlaningTools.mul(feeAmt, tmCashflowFee.getExecuteRate())// 金额*利率
													,
													PlaningTools.daysBetween(productApproveMain.getvDate(),
															productApproveMain.getmDate())),
											DayCountBasis.equal(tmCashflowFee.getDayCounter()).getDenominator()// 计息基础
									)).setScale(2, java.math.BigDecimal.ROUND_HALF_UP).doubleValue());// 保留2位数
								}
								/************************* 赋值费用交易 ************************/
								tdProductFeeDeal = new TdProductFeeDeal();
								tdProductFeeDeal.setAccruedTint(0.00);
								tdProductFeeDeal.setActualTint(0.00);
								tdProductFeeDeal.setCfBase("1");// 非底层资产
								tdProductFeeDeal.setCfName(assetSpecificBond.getBaseAssetname());
								tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
								tdProductFeeDeal.setFeeCamt(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00));
								tdProductFeeDeal.setFeeCbasis(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
								tdProductFeeDeal.setFeeCbeforeAmt(tmCashflowFee.getInterestAmt());
								tdProductFeeDeal.setFeeCfre(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x"));
								tdProductFeeDeal.setFeeCtype(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
								tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
								tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
								tdProductFeeDeal.setFeeRate(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
								tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
								tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
								tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
								tdProductFeeDeal.setVersion(productApproveMain.getVersion());
								productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);
								tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
								cashflowFees.add(tmCashflowFee);

							} else {
								/**
								 * 假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行费用及周期判定
								 */
								List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();// 返回的每日计提
								List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();// 返回的
																												// 收费计划

								/**
								 * TC_PRODUCT_FEE_CAL表获得所有的费用配置项[计息基础、费率、收费频率、费用计算本金]
								 */
								String basicType1 = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(),
										"x");// 计息基础
								double feeRate = ParameterUtil.getDouble(BeanUtil.beanToHashMap(assetSpecificBond),
										productFeeCal.getFeeType(), 0.00);// 费率
								String intFre = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x");// 收费频率
								double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);// 计费使用的本金
								/************************* 赋值费用交易 ************************/
								tdProductFeeDeal = new TdProductFeeDeal();
								tdProductFeeDeal.setAccruedTint(0.00);
								tdProductFeeDeal.setActualTint(0.00);
								tdProductFeeDeal.setCfBase("1");// 非底层资产
								tdProductFeeDeal.setCfName(assetSpecificBond.getBaseAssetname());
								tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
								tdProductFeeDeal.setFeeCamt(feeAmt);
								tdProductFeeDeal.setFeeCbasis(basicType1);
								tdProductFeeDeal.setFeeCbeforeAmt(0.00);
								tdProductFeeDeal.setFeeCfre(intFre);
								tdProductFeeDeal.setFeeCtype(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
								tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
								tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
								tdProductFeeDeal.setFeeRate(feeRate);
								tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
								tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
								tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
								tdProductFeeDeal.setVersion(productApproveMain.getVersion());
								productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);

								/**
								 * 生成交易的时候 费用只会出现1个区间 [VDATE,MDATE,FEERATE]
								 */
								List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 费用变化区间 固定的
								InterestRange interestRange = new InterestRange();
								interestRange.setStartDate(productApproveMain.getvDate());
								interestRange.setEndDate(productApproveMain.getmDate());
								interestRange.setRate(feeRate);
								interestRanges.add(interestRange);
								/**
								 * 生成交易的时候 费用本金只会出现1个区间 [VDATE,MDATE,FEEAMT]
								 */
								List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();// 本金变化区间
																												// 初始的时候只要一个
								PrincipalInterval principalInterval = new PrincipalInterval();
								principalInterval.setStartDate(productApproveMain.getvDate());
								principalInterval.setEndDate(productApproveMain.getmDate());
								principalInterval.setResidual_Principal(feeAmt);
								principalIntervals.add(principalInterval);
								/**
								 * [通过费用的计息频率 推演 实际的 basis]
								 */
								int basis = DictConstants.DayCounter.Actual360.equals(basicType1) ? 1
										: DictConstants.DayCounter.Actual365.equals(basicType1) ? 2 : 3;

								// 后收息的时候按照计提的模式算一遍
								if (com.singlee.capital.base.cal.Frequency.valueOf(
										Integer.parseInt(intFre)) == com.singlee.capital.base.cal.Frequency.ONCE) {
									PlaningTools.calPlanSchedule(interestRanges, principalIntervals,
											tmCashflowDailyInterests, tmCashflowInterests,
											productApproveMain.getvDate(), productApproveMain.getmDate(),
											productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
											feeAmt, feeRate,
											com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
											DayCountBasis.valueOf(basis));
								} else {
									// 起息日-第一个收费时间点的区间
									if (!PlaningTools.compareDate3(productApproveMain.getvDate(),
											productApproveMain.getsDate(), "yyyy-MM-dd")) {
										TmCashflowFee fistPlanSchedule = new TmCashflowFee();
										fistPlanSchedule.setSeqNumber(1);
										fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
										fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
										fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
										fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(
												interestRanges, principalIntervals, tmCashflowDailyInterests,
												fistPlanSchedule.getRefBeginDate(), fistPlanSchedule.getRefEndDate(),
												productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
										fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
										fistPlanSchedule.setExecuteRate(feeRate);
										tmCashflowInterests.add(fistPlanSchedule);
									}
									PlaningTools.calPlanSchedule(interestRanges, principalIntervals,
											tmCashflowDailyInterests, tmCashflowInterests,
											productApproveMain.getsDate(), productApproveMain.geteDate(),
											productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
											feeAmt, feeRate,
											com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
											DayCountBasis.valueOf(basis));
									// 最后一次收费日-到期日
									if (!PlaningTools.compareDate3(productApproveMain.getmDate(),
											productApproveMain.geteDate(), "yyyy-MM-dd")) {
										TmCashflowInterest lastPlanSchedule = new TmCashflowInterest();
										lastPlanSchedule.setSeqNumber(tmCashflowInterests.size() + 1);
										lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
										lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
										lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
										lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(
												interestRanges, principalIntervals, tmCashflowDailyInterests,
												lastPlanSchedule.getRefBeginDate(), lastPlanSchedule.getRefEndDate(),
												productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
										lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
										lastPlanSchedule.setExecuteRate(feeRate);
										tmCashflowInterests.add(lastPlanSchedule);
									}
								}
								/**
								 * 组装到统一的费用 LIST(计算的时候采用利息的父类）
								 */
								for (CashflowInterest cashflowInterest : tmCashflowInterests) {

									tmCashflowFee = new TmCashflowFee();
									BeanUtil.copyNotEmptyProperties(tmCashflowFee, cashflowInterest);
									tmCashflowFee.setDealNo(productApproveMain.getDealNo());
									tmCashflowFee.setCfType(productFeeCal.getFeeType());
									tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
									tmCashflowFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
									tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
									tmCashflowFee.setVersion(productApproveMain.getVersion());
									cashflowFees.add(tmCashflowFee);
								}
								// 费用计提明细
								for (int j = 0; j < tmCashflowDailyInterests.size(); j++) {
									tmCashflowDailyFee = new TmCashflowDailyFee();
									BeanUtil.copyNotEmptyProperties(tmCashflowDailyFee,
											tmCashflowDailyInterests.get(j));
									tmCashflowDailyFee.setDealNo(productApproveMain.getDealNo());
									tmCashflowDailyFee.setVersion(productApproveMain.getVersion());
									tmCashflowDailyFee.setCfType(productFeeCal.getFeeType());
									tmCashflowDailyFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
									tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
									cashflowDailyFees.add(tmCashflowDailyFee);
								}
							}

						}
					}
				}

			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			JY.raise(e.getLocalizedMessage());
		}
		if (cashflowFees.size() > 0) {
			batchDao.batch(getSqlForCashFlow(paramsMap) + "CashflowFeeMapper.insert", cashflowFees);
			batchDao.batch(getSqlForCashFlow(paramsMap) + "CashflowDailyFeeMapper.insert", cashflowDailyFees);
		}
	}

	private static ScriptEngineManager manager = new ScriptEngineManager();
	private static ScriptEngine engine;

	/**
	 * 计算记账指标
	 * 
	 * @param map
	 * @param expression
	 * @return
	 */
	public static boolean calcBkIndex(Map<String, Object> map, String expression) {
		if (engine == null) {
			engine = manager.getEngineByName("js");
		}
		for (String key : map.keySet()) {
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			return Boolean.parseBoolean(val.toString());
		} catch (Exception e) {
			JY.raise(e.getLocalizedMessage());
		}
		return false;
	}

	@Override
	public void updateByHandleCaptitalAmt(Map<String, Object> paramsMap, List<PrincipalInterval> principalIntervals) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		// 获取产品项下的所有费用配置列表；去掉contractRate
		// 判定FEE_CEXP是否为1-true，是那么进入计算
		// 判定是否是前收息费用模式，如果是 那么 生成的费用计划为1条，区间为划款日-划款日
		// 判定为后收息，那么需要看是否有自己的频率和计息基础，有那么按照自己的出（VDATE-MDATE）之间的计划
		// 没有，那么按照主交易的模式出 （VDATE-MDATE）之间的计划

		String dealType = ParameterUtil.getString(paramsMap, "dealType", "1");
		// 每次需要清除费用计划对应的费用交易
		productFeeDealMapper.deleteProductFeeDeals(paramsMap);
		// 删除费用计划及每日费用计提
		if (DictConstants.DealType.Approve.equalsIgnoreCase(dealType)) {
			tmCashflowFeeMapper.deleteTmCashflowFeeList(paramsMap);// 每次必须先清理掉费用数据表数据
			tmCashflowDailyFeeMapper.deleteCashflowDailyFee(paramsMap);// 每次必须先清理掉费用数据表数据
		} else {
			tdCashflowFeeMapper.deleteTdCashflowFeeList(paramsMap);
			tdCashflowDailyFeeMapper.deleteTdCashflowDailyFee(paramsMap);// 每次必须先清理掉费用数据表数据
		}

		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		BeanUtil.populate(productApproveMain, paramsMap);
		// 将参与方进行map化
		if (null != productApproveMain.getParticipantApprove()
				&& productApproveMain.getParticipantApprove().size() > 0) {
			for (TdProductApproveSub productApproveSub : productApproveMain.getParticipantApprove()) {
				paramsMap.put(productApproveSub.getSubFldName(), productApproveSub.getSubFldValue());
			}
		}
		// 费用现金流
		List<TmCashflowFee> cashflowFees = new ArrayList<TmCashflowFee>();
		List<TmCashflowDailyFee> cashflowDailyFees = new ArrayList<TmCashflowDailyFee>();
		// 费用交易
		TdProductFeeDeal tdProductFeeDeal = null;

		TmCashflowFee tmCashflowFee = null;
		TmCashflowDailyFee tmCashflowDailyFee = null;
		paramsMap.put("cfBase", "0");
		List<TdProductFeeCal> productFeeCals = this.productFeeCalMapper.getProductFeeCalsByPrdNo(paramsMap);
		// 封装给脚本引擎使用
		Map<String, Object> feeMap = new HashMap<String, Object>();
		feeMap.put("feeMap", paramsMap);
		try {
			for (TdProductFeeCal productFeeCal : productFeeCals) {
				if ("contractRate".equalsIgnoreCase(productFeeCal.getFeeType())) {
					continue;
				}
				if (calcBkIndex(feeMap, productFeeCal.getIsFeeDaily())) {// 1-true 表示需要进入计提的费率
					tmCashflowFee = new TmCashflowFee();
					// 获取配置项的收费模式
					if (ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x")
							.equalsIgnoreCase(DictConstants.intType.chargeBefore))// 先收费
					{
						tmCashflowFee.setCfType(productFeeCal.getFeeType());
						tmCashflowFee
								.setDayCounter(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
						tmCashflowFee.setDealNo(productApproveMain.getDealNo());
						tmCashflowFee
								.setExecuteRate(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
						tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
						tmCashflowFee.setRefBeginDate(productApproveMain.getvDate());// 划款日
						tmCashflowFee.setRefEndDate(productApproveMain.getmDate());//
						tmCashflowFee.setSeqNumber(1);
						tmCashflowFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
						tmCashflowFee.setTheoryPaymentDate(productApproveMain.gettDate());// 预计回款日
						tmCashflowFee.setVersion(productApproveMain.getVersion());
						double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);
						if (null != productFeeCal.getFeeCbeforeAmt()) {
							tmCashflowFee.setInterestAmt(
									ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCbeforeAmt(), 0.00));
						} else {
							// 计算收费一次性金额
							tmCashflowFee.setInterestAmt(new BigDecimal(PlaningTools
									.div(PlaningTools.mul(PlaningTools.mul(feeAmt, tmCashflowFee.getExecuteRate())// 金额*利率
											,
											PlaningTools.daysBetween(productApproveMain.getvDate(),
													productApproveMain.getmDate())),
											DayCountBasis.equal(tmCashflowFee.getDayCounter()).getDenominator()// 计息基础
									)).setScale(2, java.math.BigDecimal.ROUND_HALF_UP).doubleValue());// 保留2位数
						}
						/************************* 赋值费用交易 ************************/
						tdProductFeeDeal = new TdProductFeeDeal();
						tdProductFeeDeal.setAccruedTint(0.00);
						tdProductFeeDeal.setActualTint(0.00);
						tdProductFeeDeal.setCfBase("0");// 非底层资产
						tdProductFeeDeal.setCfName("");
						tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
						tdProductFeeDeal
								.setFeeCamt(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00));
						tdProductFeeDeal
								.setFeeCbasis(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
						tdProductFeeDeal.setFeeCbeforeAmt(tmCashflowFee.getInterestAmt());
						tdProductFeeDeal
								.setFeeCfre(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x"));
						tdProductFeeDeal
								.setFeeCtype(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
						tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
						tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
						tdProductFeeDeal
								.setFeeRate(ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
						tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
						tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
						tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
						tdProductFeeDeal.setVersion(productApproveMain.getVersion());
						productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);
						tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
						cashflowFees.add(tmCashflowFee);

					} else {
						/**
						 * 假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行费用及周期判定
						 */
						List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();// 返回的每日计提
						List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();// 返回的 收费计划

						/**
						 * TC_PRODUCT_FEE_CAL表获得所有的费用配置项[计息基础、费率、收费频率、费用计算本金]
						 */
						String basicType1 = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x");// 计息基础
						double feeRate = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00);// 费率
						String intFre = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x");// 收费频率
						double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);// 计费使用的本金

						/************************* 赋值费用交易 ************************/
						tdProductFeeDeal = new TdProductFeeDeal();
						tdProductFeeDeal.setAccruedTint(0.00);
						tdProductFeeDeal.setActualTint(0.00);
						tdProductFeeDeal.setCfBase("0");// 非底层资产
						tdProductFeeDeal.setCfName("");
						tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
						tdProductFeeDeal.setFeeCamt(feeAmt);
						tdProductFeeDeal.setFeeCbasis(basicType1);
						tdProductFeeDeal.setFeeCbeforeAmt(0.00);
						tdProductFeeDeal.setFeeCfre(intFre);
						tdProductFeeDeal
								.setFeeCtype(ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
						tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
						tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
						tdProductFeeDeal.setFeeRate(feeRate);
						tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
						tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
						tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
						tdProductFeeDeal.setVersion(productApproveMain.getVersion());
						productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);

						/**
						 * 生成交易的时候 费用只会出现1个区间 [VDATE,MDATE,FEERATE]
						 */
						List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 费用变化区间 固定的
						InterestRange interestRange = new InterestRange();
						interestRange.setStartDate(productApproveMain.getvDate());
						interestRange.setEndDate(productApproveMain.getmDate());
						interestRange.setRate(feeRate);
						interestRanges.add(interestRange);
						/**
						 * 生成交易的时候 费用本金只会出现1个区间 [VDATE,MDATE,FEEAMT]
						 */
						/*
						 * List<PrincipalInterval> principalIntervals = new
						 * ArrayList<PrincipalInterval>();//本金变化区间 初始的时候只要一个 PrincipalInterval
						 * principalInterval = new PrincipalInterval();
						 * principalInterval.setStartDate(productApproveMain.getvDate());
						 * principalInterval.setEndDate(productApproveMain.getmDate());
						 * principalInterval.setResidual_Principal(feeAmt);
						 * principalIntervals.add(principalInterval);
						 */
						/**
						 * [通过费用的计息频率 推演 实际的 basis]
						 */
						int basis = DictConstants.DayCounter.Actual360.equals(basicType1) ? 1
								: DictConstants.DayCounter.Actual365.equals(basicType1) ? 2 : 3;

						// 后收息的时候按照计提的模式算一遍
						if (com.singlee.capital.base.cal.Frequency
								.valueOf(Integer.parseInt(intFre)) == com.singlee.capital.base.cal.Frequency.ONCE) {
							PlaningTools.calPlanSchedule(interestRanges, principalIntervals, tmCashflowDailyInterests,
									tmCashflowInterests, productApproveMain.getvDate(), productApproveMain.getmDate(),
									productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
									feeAmt, feeRate,
									com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
									DayCountBasis.valueOf(basis));
						} else {
							// 起息日-第一个收费时间点的区间
							if (!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(),
									"yyyy-MM-dd")) {
								TmCashflowFee fistPlanSchedule = new TmCashflowFee();
								fistPlanSchedule.setSeqNumber(1);
								fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
								fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
								fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
								fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,
										principalIntervals, tmCashflowDailyInterests,
										fistPlanSchedule.getRefBeginDate(), fistPlanSchedule.getRefEndDate(),
										productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
								fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
								fistPlanSchedule.setExecuteRate(feeRate);
								tmCashflowInterests.add(fistPlanSchedule);
							}
							PlaningTools.calPlanSchedule(interestRanges, principalIntervals, tmCashflowDailyInterests,
									tmCashflowInterests, productApproveMain.getsDate(), productApproveMain.geteDate(),
									productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
									feeAmt, feeRate,
									com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
									DayCountBasis.valueOf(basis));
							// 最后一次收费日-到期日
							if (!PlaningTools.compareDate3(productApproveMain.getmDate(), productApproveMain.geteDate(),
									"yyyy-MM-dd")) {
								TmCashflowInterest lastPlanSchedule = new TmCashflowInterest();
								lastPlanSchedule.setSeqNumber(tmCashflowInterests.size() + 1);
								lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
								lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
								lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
								lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,
										principalIntervals, tmCashflowDailyInterests,
										lastPlanSchedule.getRefBeginDate(), lastPlanSchedule.getRefEndDate(),
										productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
								lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
								lastPlanSchedule.setExecuteRate(feeRate);
								tmCashflowInterests.add(lastPlanSchedule);
							}
						}
						/**
						 * 组装到统一的费用 LIST(计算的时候采用利息的父类）
						 */
						for (CashflowInterest cashflowInterest : tmCashflowInterests) {

							tmCashflowFee = new TmCashflowFee();
							BeanUtil.copyNotEmptyProperties(tmCashflowFee, cashflowInterest);
							tmCashflowFee.setDealNo(productApproveMain.getDealNo());
							tmCashflowFee.setCfType(productFeeCal.getFeeType());
							tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
							tmCashflowFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
							tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
							tmCashflowFee.setVersion(productApproveMain.getVersion());
							cashflowFees.add(tmCashflowFee);
						}
						// 费用计提明细
						for (int j = 0; j < tmCashflowDailyInterests.size(); j++) {
							tmCashflowDailyFee = new TmCashflowDailyFee();
							BeanUtil.copyNotEmptyProperties(tmCashflowDailyFee, tmCashflowDailyInterests.get(j));
							tmCashflowDailyFee.setDealNo(productApproveMain.getDealNo());
							tmCashflowDailyFee.setVersion(productApproveMain.getVersion());
							tmCashflowDailyFee.setCfType(productFeeCal.getFeeType());
							tmCashflowDailyFee.setCfBase(DictConstants.YesNo.NO);// 非底层资产
							tmCashflowDailyFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
							cashflowDailyFees.add(tmCashflowDailyFee);
						}
					}

				}
			}
			/*********************************************************************************************************************************/
			// 基础资产开始获取费用情况
			paramsMap.put("cfBase", "1");
			productFeeCals = this.productFeeCalMapper.getProductFeeCalsByPrdNo(paramsMap);
			// 获取该资产下属的所有基础资产
			if (productFeeCals.size() > 0) {
				List<TdAssetSpecificBond> assetSpecificBonds = assetSpecificBondMapper
						.getTdAssetSpecificBondList(paramsMap);
				for (TdAssetSpecificBond assetSpecificBond : assetSpecificBonds) {
					feeMap.put("feeMap", BeanUtil.beanToHashMap(assetSpecificBond));
					for (TdProductFeeCal productFeeCal : productFeeCals) {
						if ("contractRate".equalsIgnoreCase(productFeeCal.getFeeType())) {
							continue;
						}
						if (calcBkIndex(feeMap, productFeeCal.getIsFeeDaily())) {// 1-true 表示需要进入计提的费率
							tmCashflowFee = new TmCashflowFee();
							// 获取配置项的收费模式
							if (ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x")
									.equalsIgnoreCase(DictConstants.intType.chargeBefore))// 先收费
							{
								tmCashflowFee.setCfType(productFeeCal.getFeeType());
								tmCashflowFee.setDayCounter(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
								tmCashflowFee.setDealNo(productApproveMain.getDealNo());
								tmCashflowFee.setExecuteRate(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
								tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
								tmCashflowFee.setRefBeginDate(productApproveMain.getvDate());// 划款日
								tmCashflowFee.setRefEndDate(productApproveMain.getmDate());//
								tmCashflowFee.setSeqNumber(1);
								tmCashflowFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
								tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
								tmCashflowFee.setTheoryPaymentDate(productApproveMain.gettDate());// 预计回款日
								tmCashflowFee.setVersion(productApproveMain.getVersion());
								double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);
								if (null != productFeeCal.getFeeCbeforeAmt()) {
									tmCashflowFee.setInterestAmt(
											ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCbeforeAmt(), 0.00));
								} else {
									// 计算收费一次性金额
									tmCashflowFee.setInterestAmt(new BigDecimal(PlaningTools.div(
											PlaningTools.mul(PlaningTools.mul(feeAmt, tmCashflowFee.getExecuteRate())// 金额*利率
													,
													PlaningTools.daysBetween(productApproveMain.getvDate(),
															productApproveMain.getmDate())),
											DayCountBasis.equal(tmCashflowFee.getDayCounter()).getDenominator()// 计息基础
									)).setScale(2, java.math.BigDecimal.ROUND_HALF_UP).doubleValue());// 保留2位数
								}
								/************************* 赋值费用交易 ************************/
								tdProductFeeDeal = new TdProductFeeDeal();
								tdProductFeeDeal.setAccruedTint(0.00);
								tdProductFeeDeal.setActualTint(0.00);
								tdProductFeeDeal.setCfBase("1");// 非底层资产
								tdProductFeeDeal.setCfName(assetSpecificBond.getBaseAssetname());
								tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
								tdProductFeeDeal.setFeeCamt(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00));
								tdProductFeeDeal.setFeeCbasis(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(), "x"));
								tdProductFeeDeal.setFeeCbeforeAmt(tmCashflowFee.getInterestAmt());
								tdProductFeeDeal.setFeeCfre(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x"));
								tdProductFeeDeal.setFeeCtype(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
								tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
								tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
								tdProductFeeDeal.setFeeRate(
										ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeType(), 0.00));
								tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
								tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
								tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
								tdProductFeeDeal.setVersion(productApproveMain.getVersion());
								productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);
								tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());
								cashflowFees.add(tmCashflowFee);

							} else {
								/**
								 * 假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行费用及周期判定
								 */
								List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();// 返回的每日计提
								List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();// 返回的
																												// 收费计划

								/**
								 * TC_PRODUCT_FEE_CAL表获得所有的费用配置项[计息基础、费率、收费频率、费用计算本金]
								 */
								String basicType1 = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCbasis(),
										"x");// 计息基础
								double feeRate = ParameterUtil.getDouble(BeanUtil.beanToHashMap(assetSpecificBond),
										productFeeCal.getFeeType(), 0.00);// 费率
								String intFre = ParameterUtil.getString(paramsMap, productFeeCal.getFeeCfre(), "x");// 收费频率
								double feeAmt = ParameterUtil.getDouble(paramsMap, productFeeCal.getFeeCamt(), 0.00);// 计费使用的本金
								/************************* 赋值费用交易 ************************/
								tdProductFeeDeal = new TdProductFeeDeal();
								tdProductFeeDeal.setAccruedTint(0.00);
								tdProductFeeDeal.setActualTint(0.00);
								tdProductFeeDeal.setCfBase("1");// 非底层资产
								tdProductFeeDeal.setCfName(assetSpecificBond.getBaseAssetname());
								tdProductFeeDeal.setDealNo(productApproveMain.getDealNo());
								tdProductFeeDeal.setFeeCamt(feeAmt);
								tdProductFeeDeal.setFeeCbasis(basicType1);
								tdProductFeeDeal.setFeeCbeforeAmt(0.00);
								tdProductFeeDeal.setFeeCfre(intFre);
								tdProductFeeDeal.setFeeCtype(
										ParameterUtil.getString(paramsMap, productFeeCal.getFeeCtype(), "x"));
								tdProductFeeDeal.setFeeMdate(productApproveMain.getmDate());
								tdProductFeeDeal.setFeeName(productFeeCal.getFeeName());
								tdProductFeeDeal.setFeeRate(feeRate);
								tdProductFeeDeal.setFeeType(productFeeCal.getFeeType());
								tdProductFeeDeal.setFeeVdate(productApproveMain.getvDate());
								tdProductFeeDeal.setPrdNo(String.valueOf(productApproveMain.getPrdNo()));
								tdProductFeeDeal.setVersion(productApproveMain.getVersion());
								productFeeDealMapper.insetProductFeeDeal(tdProductFeeDeal);

								/**
								 * 生成交易的时候 费用只会出现1个区间 [VDATE,MDATE,FEERATE]
								 */
								List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 费用变化区间 固定的
								InterestRange interestRange = new InterestRange();
								interestRange.setStartDate(productApproveMain.getvDate());
								interestRange.setEndDate(productApproveMain.getmDate());
								interestRange.setRate(feeRate);
								interestRanges.add(interestRange);
								/**
								 * 生成交易的时候 费用本金只会出现1个区间 [VDATE,MDATE,FEEAMT]
								 */
								/*
								 * List<PrincipalInterval> principalIntervals = new
								 * ArrayList<PrincipalInterval>();//本金变化区间 初始的时候只要一个 PrincipalInterval
								 * principalInterval = new PrincipalInterval();
								 * principalInterval.setStartDate(productApproveMain.getvDate());
								 * principalInterval.setEndDate(productApproveMain.getmDate());
								 * principalInterval.setResidual_Principal(feeAmt);
								 * principalIntervals.add(principalInterval);
								 */
								/**
								 * [通过费用的计息频率 推演 实际的 basis]
								 */
								int basis = DictConstants.DayCounter.Actual360.equals(basicType1) ? 1
										: DictConstants.DayCounter.Actual365.equals(basicType1) ? 2 : 3;

								// 后收息的时候按照计提的模式算一遍
								if (com.singlee.capital.base.cal.Frequency.valueOf(
										Integer.parseInt(intFre)) == com.singlee.capital.base.cal.Frequency.ONCE) {
									PlaningTools.calPlanSchedule(interestRanges, principalIntervals,
											tmCashflowDailyInterests, tmCashflowInterests,
											productApproveMain.getvDate(), productApproveMain.getmDate(),
											productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
											feeAmt, feeRate,
											com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
											DayCountBasis.valueOf(basis));
								} else {
									// 起息日-第一个收费时间点的区间
									if (!PlaningTools.compareDate3(productApproveMain.getvDate(),
											productApproveMain.getsDate(), "yyyy-MM-dd")) {
										TmCashflowFee fistPlanSchedule = new TmCashflowFee();
										fistPlanSchedule.setSeqNumber(1);
										fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
										fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
										fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
										fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(
												interestRanges, principalIntervals, tmCashflowDailyInterests,
												fistPlanSchedule.getRefBeginDate(), fistPlanSchedule.getRefEndDate(),
												productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
										fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
										fistPlanSchedule.setExecuteRate(feeRate);
										tmCashflowInterests.add(fistPlanSchedule);
									}
									PlaningTools.calPlanSchedule(interestRanges, principalIntervals,
											tmCashflowDailyInterests, tmCashflowInterests,
											productApproveMain.getsDate(), productApproveMain.geteDate(),
											productApproveMain.getvDate(), // 计算第一次与最后一次收费日之间的设定
											feeAmt, feeRate,
											com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(intFre)),
											DayCountBasis.valueOf(basis));
									// 最后一次收费日-到期日
									if (!PlaningTools.compareDate3(productApproveMain.getmDate(),
											productApproveMain.geteDate(), "yyyy-MM-dd")) {
										TmCashflowInterest lastPlanSchedule = new TmCashflowInterest();
										lastPlanSchedule.setSeqNumber(tmCashflowInterests.size() + 1);
										lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
										lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
										lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
										lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(
												interestRanges, principalIntervals, tmCashflowDailyInterests,
												lastPlanSchedule.getRefBeginDate(), lastPlanSchedule.getRefEndDate(),
												productApproveMain.getvDate(), DayCountBasis.valueOf(basis)));
										lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basis).getMolecule());
										lastPlanSchedule.setExecuteRate(feeRate);
										tmCashflowInterests.add(lastPlanSchedule);
									}
								}
								/**
								 * 组装到统一的费用 LIST(计算的时候采用利息的父类）
								 */
								for (CashflowInterest cashflowInterest : tmCashflowInterests) {

									tmCashflowFee = new TmCashflowFee();
									BeanUtil.copyNotEmptyProperties(tmCashflowFee, cashflowInterest);
									tmCashflowFee.setDealNo(productApproveMain.getDealNo());
									tmCashflowFee.setCfType(productFeeCal.getFeeType());
									tmCashflowFee.setPayDirection(DictConstants.SettleCashInstBussType.RECEIVE);
									tmCashflowFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
									tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
									tmCashflowFee.setVersion(productApproveMain.getVersion());
									cashflowFees.add(tmCashflowFee);
								}
								// 费用计提明细
								for (int j = 0; j < tmCashflowDailyInterests.size(); j++) {
									tmCashflowDailyFee = new TmCashflowDailyFee();
									BeanUtil.copyNotEmptyProperties(tmCashflowDailyFee,
											tmCashflowDailyInterests.get(j));
									tmCashflowDailyFee.setDealNo(productApproveMain.getDealNo());
									tmCashflowDailyFee.setVersion(productApproveMain.getVersion());
									tmCashflowDailyFee.setCfType(productFeeCal.getFeeType());
									tmCashflowDailyFee.setCfBase(DictConstants.YesNo.YES);// 是底层资产
									tmCashflowFee.setFeeDealNo(tdProductFeeDeal.getFeeDealNo());// 是底层资产
									cashflowDailyFees.add(tmCashflowDailyFee);
								}
							}

						}
					}
				}

			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			JY.raise(e.getLocalizedMessage());
		}
		if (cashflowFees.size() > 0) {
			batchDao.batch(getSqlForCashFlow(paramsMap) + "CashflowFeeMapper.insert", cashflowFees);
			batchDao.batch(getSqlForCashFlow(paramsMap) + "CashflowDailyFeeMapper.insert", cashflowDailyFees);
		}

	}

	public String getSqlForCashFlow(Map<String, Object> params) {
		if (DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
			return "com.singlee.capital.cashflow.mapper.Td";
		} else {
			return "com.singlee.capital.cashflow.mapper.Tm";
		}
	}

	@Override
	public void createFeeForNostro(Map<String, Object> params, TdProductApproveMain productApproveMain) {
		try {
			if (PlaningTools.compareDate(productApproveMain.getNostroVdate(), productApproveMain.gettDate(),
					"yyyy-MM-dd")) {
				tdCashflowFeeMapper.deleteTdCashflowFeeList(params);
				tdCashflowDailyFeeMapper.deleteTdCashflowDailyFee(params);
				return;
			}

			// 保存 SAVE 直接生成现金流计划
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();// 本金变化区间
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 利率变化区间

			PrincipalInterval principalInterval = new PrincipalInterval();
			principalInterval.setStartDate(productApproveMain.getNostroVdate());
			principalInterval.setEndDate(productApproveMain.gettDate());
			principalInterval.setResidual_Principal(productApproveMain.getAmt());
			principalIntervals.add(principalInterval);

			InterestRange interestRange = new InterestRange();
			interestRange.setStartDate(productApproveMain.getNostroVdate());
			interestRange.setEndDate(productApproveMain.gettDate());
			interestRange.setRate(productApproveMain.getNostroRate());
			interestRanges.add(interestRange);

			int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis()) ? 1
					: DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis()) ? 2 : 3;

			List<TdCashflowFee> cashflowFees = new ArrayList<TdCashflowFee>();
			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();// 返回的每日计提

			TdCashflowFee fistPlanSchedule = new TdCashflowFee();
			fistPlanSchedule.setSeqNumber(1);
			fistPlanSchedule.setRefBeginDate(productApproveMain.getNostroVdate());
			fistPlanSchedule.setRefEndDate(productApproveMain.gettDate());
			fistPlanSchedule.setTheoryPaymentDate(productApproveMain.gettDate());

			fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges, principalIntervals,
					detailSchedules, fistPlanSchedule.getRefBeginDate(), fistPlanSchedule.getRefEndDate(),
					productApproveMain.getNostroVdate(), DayCountBasis.valueOf(basicType)));
			fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
			fistPlanSchedule.setExecuteRate(productApproveMain.getNostroRate());
			fistPlanSchedule.setActuralRate(productApproveMain.getNostroRate());
			fistPlanSchedule.setDealNo(productApproveMain.getDealNo());
			fistPlanSchedule.setCfType("nostro");
			cashflowFees.add(fistPlanSchedule);

			List<TdCashflowDailyFee> cashflowDailyFees = new ArrayList<TdCashflowDailyFee>();
			TdCashflowDailyFee fees = null;
			for (int i = 0; i < detailSchedules.size(); i++) {
				fees = new TdCashflowDailyFee();
				fees.setSeqNumber(detailSchedules.get(i).getSeqNumber());
				fees.setVersion(detailSchedules.get(i).getVersion());
				fees.setActualRate(detailSchedules.get(i).getActualRate());
				fees.setDayCounter(detailSchedules.get(i).getDayCounter());
				fees.setInterest(detailSchedules.get(i).getInterest());
				fees.setPrincipal(detailSchedules.get(i).getPrincipal());
				fees.setRefBeginDate(detailSchedules.get(i).getRefBeginDate());
				fees.setRefEndDate(detailSchedules.get(i).getRefEndDate());
				fees.setDealNo(productApproveMain.getDealNo());
				cashflowDailyFees.add(fees);
			}

			tdCashflowFeeMapper.deleteTdCashflowFeeList(params);
			tdCashflowDailyFeeMapper.deleteTdCashflowDailyFee(params);

			if (PlaningTools.compareDate2(productApproveMain.gettDate(), productApproveMain.getNostroVdate(),
					"yyyy-MM-dd")) {
				if (cashflowFees.size() > 0) {
					batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowFeeMapper.insert", cashflowFees);
				}

				if (cashflowDailyFees.size() > 0) {
					batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyFeeMapper.insert",
							cashflowDailyFees);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			JY.error("" + productApproveMain.getProductCode(), e);
			JY.raise(e.getMessage());
		}
	}
}
