package com.singlee.capital.cashflow.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmCashflowAmor;
import com.singlee.capital.cashflow.model.parent.CashflowAmor;


public interface TmCashflowAmorMapper extends Mapper<TmCashflowAmor>,CashFlowMapper {

	/**
	 * 获取每日摊销现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowAmor> pageDailyCashflowAmorList(Map<String, Object> map, RowBounds rb);
	
	
	@Override
    void deletCashflowAmor(Map<String, Object> map) ;
}
