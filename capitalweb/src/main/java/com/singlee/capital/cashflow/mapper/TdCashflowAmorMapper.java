package com.singlee.capital.cashflow.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowAmor;
import com.singlee.capital.cashflow.model.parent.CashflowAmor;


public interface TdCashflowAmorMapper extends Mapper<TdCashflowAmor>,CashFlowMapper {

	/**
	 * 获取每日摊销现金流列表
	 * @param map
	 * @return
	 */
    @Override
    Page<CashflowAmor> pageDailyCashflowAmorList(Map<String, Object> map, RowBounds rb);
	
	TdCashflowAmor selectAmorAmtByDealNo(Map<String, Object> map);
	
	
	@Override
    void deletCashflowAmor(Map<String, Object> map) ;
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
    @Override
    void insertCashFlowAmorDayCopy(Map<String, Object> map);
}
