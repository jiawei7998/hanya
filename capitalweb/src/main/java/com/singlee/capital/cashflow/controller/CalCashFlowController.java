package com.singlee.capital.cashflow.controller;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
/**
 * 审批过程中改动还本计划
 * @author SINGLEE
 * @version 201705
 */
@Controller
@RequestMapping(value = "/tmCalCashFlowController")
public class CalCashFlowController extends CommonController {
	@Autowired
	CalCashFlowService tmCalCashFlowService;
	
	/**
	 * 还本计划手工修改 更新数据库
	 * 计算收息计划和摊销计划
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateCapitalCashFlowsForHandle")
	public String  updateCapitalCashFlowsForHandle(@RequestBody Map<String,Object> params){
		tmCalCashFlowService.updateByHandleCaptitalAmt(params);
		return "SUCCESS";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/updateCashFlowInterestByHandle")
	public String updateCashFlowInterestByHandle(@RequestBody Map<String,Object> params){
		tmCalCashFlowService.updateCashFlowInterestByHandle(params);
		return "SUCCESS";
	}
	@ResponseBody
	@RequestMapping(value = "/getInterestCashFlowForApprove")
	public RetMsg<?> getInterestCashFlowForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getInterestCashFlowForApprove(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getCapitalCashFlowForApprove")
	public RetMsg<?> getCapitalCashFlowForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getCapitalCashFlowForApprove(map));
	}
	
	/**
	 * 本金计划变更拿取审批拷贝数据
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getApproveCapitalForApprove")
	public RetMsg<?> getApproveCapitalForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getApproveCapitalForApprove(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getfeeCashFlowForApprove")
	public RetMsg<?> getfeeCashFlowForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getfeeCashFlowForApprove(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getApproveInterestForApprove")
	public RetMsg<?> getApproveInterestForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getApproveInterestForApprove(map));
	}
	@ResponseBody
	@RequestMapping(value = "/getApproveFeeForApprove")
	public RetMsg<?> getApproveFeeForApprove(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getApproveFeeForApprove(map));
	}
	@ResponseBody
	@RequestMapping(value = "/queryAmorList")
	public RetMsg<?> queryAmorList(@RequestBody Map<String,Object> params) throws RException {
		if(null != params.get("startDate")){
			String startDate = String.valueOf(params.get("startDate"));
			if(startDate.length() > 10){
				params.put("startDate",startDate.substring(0, 10));
			}
		}
		
		if(null != params.get("endDate")){
			String endDate = String.valueOf(params.get("endDate"));
			if(endDate.length() > 10){
				params.put("endDate",endDate.substring(0, 10));
			}
		}
		
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<?> page = tmCalCashFlowService.pageTmDailyTmCashflowAmorList(params, rowBounds);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 根据对象查询现金流列表
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageIntDailyInterest")
	public RetMsg<?> searchPageIntDailyInterest(@RequestBody Map<String,Object> params) throws RException {
		Page<?> page = tmCalCashFlowService.getInterestDailyCashFlowForApprove(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageIntDailyFee")
	public RetMsg<?> searchPageIntDailyFee(@RequestBody Map<String,Object> params) throws RException {
		Page<?> page = tmCalCashFlowService.getFeeDailyCashFlowForApprove(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getCashFlowFeeInterest")
	public RetMsg<?> getCashFlowFeeInterest(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getCashFlowFeeInterest(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getCashFlowFeeInterestPage")
	public RetMsg<?> getCashFlowFeeInterestPage(@RequestBody Map<String,Object> params) throws RException {
		if(null != params.get("theoryPaymentDate1")){
			String theoryPaymentDate1 = String.valueOf(params.get("theoryPaymentDate1"));
			if(theoryPaymentDate1.length() > 10){
				params.put("theoryPaymentDate1",theoryPaymentDate1.substring(0, 10));
			}
		}
		
		if(null != params.get("theoryPaymentDate2")){
			String theoryPaymentDate2 = String.valueOf(params.get("theoryPaymentDate2"));
			if(theoryPaymentDate2.length() > 10){
				params.put("theoryPaymentDate2",theoryPaymentDate2.substring(0, 10));
			}
		}
		
		Page<?> page = tmCalCashFlowService.getCashFlowFeeInterest(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getInterestCashFlowPageForApprove")
	public RetMsg<?> getInterestCashFlowPageForApprove(@RequestBody Map<String,Object> params){
		return RetMsgHelper.ok(this.tmCalCashFlowService.getInterestList(params,ParameterUtil.getRowBounds(params)));
	}
}
