package com.singlee.capital.data.transfor.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataTransforConstants {

	/**
	 * 读取UDAS的EXCEL产品-（中文，属性）关系
	 * prdNo,(name,beanProperty)
	 */
	public static Map<String, List<UdasExcelBean>> udasFieldAndPropertyHashMap = new HashMap<String, List<UdasExcelBean>>();
	/**
	 * sheet页对应的产品代码
	 */
	public static Map<String,String> sheetMappingProductMap = new HashMap<String, String>();
	static{
		sheetMappingProductMap.put("资管受益权投资", "905");
		sheetMappingProductMap.put("企业信用受益权投资", "298");
		sheetMappingProductMap.put("保理收款权投资", "903");
		//sheetMappingProductMap.put("信用证受益权", "291");
		sheetMappingProductMap.put("债权投资计划", "286");
		sheetMappingProductMap.put("同业保理", "293");
		sheetMappingProductMap.put("存单质押受益权投资", "288");
		sheetMappingProductMap.put("理财投资", "290");
		sheetMappingProductMap.put("票据资管受益权", "294");
		sheetMappingProductMap.put("证券收益凭证投资", "246");
		sheetMappingProductMap.put("金融机构资管投资_特定债券", "287");
		sheetMappingProductMap.put("同业福费廷", "292");
		sheetMappingProductMap.put("再保理", "295");
		sheetMappingProductMap.put("融贷通", "299");
		sheetMappingProductMap.put("同业借款","308");
		sheetMappingProductMap.put("存放同业定期","306");
		sheetMappingProductMap.put("存放同业活期","907");
		sheetMappingProductMap.put("信用保证保险项下金融资产受益权投资业务", "904");
	}
	public static Map<String,String> productMappingSheetMap = new HashMap<String, String>();
	static{
		productMappingSheetMap.put("905","资管受益权投资");
		productMappingSheetMap.put("298","企业信用受益权投资");
		productMappingSheetMap.put("903","保理收款权投资");
		//productMappingSheetMap.put("291","信用证受益权");
		productMappingSheetMap.put("286","债权投资计划");
		productMappingSheetMap.put("293","同业保理");
		productMappingSheetMap.put("288","存单质押受益权投资");
		productMappingSheetMap.put("290","理财投资");
		productMappingSheetMap.put("294","票据资管受益权");
		productMappingSheetMap.put("246","证券收益凭证投资");
		productMappingSheetMap.put("287","金融机构资管投资_特定债券");
		productMappingSheetMap.put("292","同业福费廷");
		productMappingSheetMap.put("295","再保理");
		productMappingSheetMap.put("299","融贷通");
		productMappingSheetMap.put("308","同业借款");
		productMappingSheetMap.put("306","存放同业定期");
		productMappingSheetMap.put("907","存放同业活期");
		productMappingSheetMap.put("904","信用保证保险项下金融资产受益权投资业务");
	}
}
