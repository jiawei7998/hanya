package com.singlee.capital.data.transfor.mapper;
import java.util.List;
import java.util.Map;

import com.singlee.capital.data.transfor.model.UdasExcelBean;

import tk.mybatis.mapper.common.Mapper;

public interface UdasExcelBeanMapper extends Mapper<UdasExcelBean>{
	/**
	 * 删除产品项下的配置
	 * @param paramsMap
	 */
	void deleteUdasExcelBeanByPrdNo(Map<String, Object> paramsMap);
	/**
	 * 查询产品项下的配置
	 * @param paramsMap
	 * @return
	 */
	List<UdasExcelBean> queryAllUdasExcelBeansByPrdNo(Map<String, Object> paramsMap);
}
