package com.singlee.capital.data.transfor.service.impl;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.participant.mapper.TcProductParticipantMapper;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.service.FieldService;
import com.singlee.capital.base.trade.mapper.TdProductParticipantMapper;
import com.singlee.capital.base.trade.model.TdProductParticipant;
import com.singlee.capital.base.trade.service.TdProductParticipantService;
import com.singlee.capital.cashflow.mapper.TdCashflowAmorMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdCashflowAmor;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.data.transfor.mapper.TcCustEcifMapper;
import com.singlee.capital.data.transfor.mapper.UdasExcelBeanMapper;
import com.singlee.capital.data.transfor.model.DataTransforConstants;
import com.singlee.capital.data.transfor.model.ProductUdasExcelBean;
import com.singlee.capital.data.transfor.model.UdasExcelBean;
import com.singlee.capital.data.transfor.service.UdasExcelBeanService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.fund.mapper.ProductApproveFundMapper;
import com.singlee.capital.fund.mapper.TaFundMapper;
import com.singlee.capital.fund.mapper.TdProductFundTposMapper;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TaFund;
import com.singlee.capital.fund.model.TdProductFundTpos;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.loan.mapper.TcLoanCheckboxDealMapper;
import com.singlee.capital.loan.model.TcLoanCheckboxDeal;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdAssetLcMapper;
import com.singlee.capital.trade.mapper.TdAssetSpecificBondMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetBillMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetBlMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetMapper;
import com.singlee.capital.trade.mapper.TdBaseRiskMapper;
import com.singlee.capital.trade.mapper.TdProductApproveCrmsMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductApproveProtocolsMapper;
import com.singlee.capital.trade.mapper.TdProductApproveSubMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.mapper.TdRiskAssetMapper;
import com.singlee.capital.trade.mapper.TdRiskSlowReleaseMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdAssetLc;
import com.singlee.capital.trade.model.TdAssetSpecificBond;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdBaseAssetBill;
import com.singlee.capital.trade.model.TdBaseAssetBl;
import com.singlee.capital.trade.model.TdBaseRisk;
import com.singlee.capital.trade.model.TdProductApproveCrms;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveProtocols;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.model.TdRiskAsset;
import com.singlee.capital.trade.model.TdRiskSlowRelease;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.impl.TradeCustomServiceImpl;
import com.singlee.slbpm.service.FlowQueryService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UdasExcelBeanServiceImpl implements UdasExcelBeanService {
	@Autowired
	BatchDao batchDao;
	@Autowired
	UdasExcelBeanMapper udasExcelBeanMapper;
	@Autowired
	FieldService fieldService;
	@Autowired
	private DictionaryGetService dictionaryGetService;
	@Autowired
	TrdTradeMapper tradeMapper;
	@Autowired
	TdProductParticipantService tdProductParticipantService;
	@Autowired
	TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	ProductApproveService productApproveService;
	@Autowired
	private TdProductApproveSubMapper productApproveSubMapper;
	@Autowired
	private TdBaseAssetMapper baseAssetMapper;
	@Autowired
	private TdBaseAssetBillMapper baseAssetBillMapper;
	@Autowired
	private TdRiskAssetMapper riskAssetMapper;
	@Autowired
	private TdRiskSlowReleaseMapper riskSlowReleaseMapper;
	@Autowired
	private TdAssetLcMapper assetLcMapper;
	@Autowired
	private TdProductApproveProtocolsMapper productApproveProtocolsMapper;
	@Autowired
	private TdAssetSpecificBondMapper assetSpecificBondMapper;
	@Autowired
	private TcLoanCheckboxDealMapper tcLoanCheckboxDealMapper;
	@Autowired
	TdProductFeeDealMapper productFeeDealMapper;
	@Autowired
	FlowQueryService flowQueryService;
	@Autowired
	private TdBaseAssetBlMapper baseAssetBlMapper;
	@Autowired
	private TdBaseRiskMapper baseRiskMapper;
	@Autowired
	TtInstitutionMapper institutionMapper;
	@Autowired
	TcCustEcifMapper custEcifMapper;
	@Autowired
	TdProductParticipantMapper tdProductParticipantMapper;
	@Autowired
	TdProductApproveCrmsMapper tdProductApproveCrmsMapper;
	@Autowired
	TcProductParticipantMapper tcProductParticipantMapper;
	@Autowired
	TdRateRangeMapper tdRateRangeMapper;
	@Autowired
	private CalCashFlowService calCashFlowService;
	@Autowired
	TrdTposMapper trdTposMapper;
	@Autowired
	TdCashflowAmorMapper tdCashflowAmorMapper;
	@Autowired
	DayendDateService dateService;
	@Autowired
	TradeCustomServiceImpl tradeCustomServiceImpl;
	@Autowired
	TaFundMapper taFundMapper;
	@Autowired
	TdProductFundTposMapper tdProductFundTposMapper;
	@Autowired
	private BookkeepingService bookkeepingService;
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;
	@Autowired
	ProductApproveFundMapper productApproveFundMapper;
	@Autowired
	private TrdTposMapper tposMapper;
	@Autowired
	TdFundAcupService tdFundAcupService;
	@Autowired
	private TrdOrderMapper approveManageDao;
	@Autowired
	TdCashflowInterestMapper tdCashflowInterestMapper;

	@Override
	public void doUdasExcelPropertyToDataBase(List<ProductUdasExcelBean> productUdasExcelBeans) {
		// TODO Auto-generated method stub

		if (productUdasExcelBeans.size() > 0) {
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			for (ProductUdasExcelBean productUdasExcelBean : productUdasExcelBeans) {
				paramsMap.put("prdNo", productUdasExcelBean.getPrdNo());
				udasExcelBeanMapper.deleteUdasExcelBeanByPrdNo(paramsMap);
				batchDao.batch("com.singlee.capital.data.transfor.mapper.UdasExcelBeanMapper.insert",
						productUdasExcelBean.getUdasExcelBeans());
			}
		}
	}

	@Override
	public List<UdasExcelBean> queryUdasExcelBeansByPrdNo(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		return this.udasExcelBeanMapper.queryAllUdasExcelBeansByPrdNo(paramsMap);
	}

	@Override
	public void doUdasExcelDealsToDataBase(Sheet sheet) throws Exception {
		// TODO Auto-generated method stub
		// 循环 sheet获取每一片业务的数据
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String sheetName = sheet.getName();// 获得sheet的名称
		List<TdProductApproveMain> productApproveMains = new ArrayList<TdProductApproveMain>();
		TdProductApproveMain tdProductApproveMain = null;
		NumberCell numberCell = null;
		DateCell date = null;
		TaDictVo taDictVo = null;
		// 查看sheet的页是否在 判断sheet页是否是需要读取的产品匹配对应关系
		if (null != DataTransforConstants.sheetMappingProductMap.get(sheetName)) {
			// 获得sheet页产品代码
			String prodNo = DataTransforConstants.sheetMappingProductMap.get(sheetName);
			paramsMap.put("prdNo", prodNo);
			// 从TC_FIELD自定义产品表中获取各属性的字段类型及预定义与自定义类型
			List<TcField> tcFields = fieldService.getChoosedFieldListForPrdNo(paramsMap);
			// 将tcFields转换为map类型
			Map<String, TcField> tcFieldMap = new HashMap<String, TcField>();
			for (TcField tcField : tcFields) {
				tcFieldMap.put(tcField.getFldName(), tcField);
			}
			int rowCount = sheet.getRows();
			// 获取产品下的匹配IFBM关系列表 TC_UDAS_EXCEL_MAPPING//产品项下的配置UDAS匹配关系
			List<UdasExcelBean> udasExcelBeans = DataTransforConstants.udasFieldAndPropertyHashMap.get(prodNo);
			// 无匹配关系，那么直接返回
			if (null == udasExcelBeans || udasExcelBeans.size() <= 0) {
				return;
			}
			List<TdProductApproveSub> productApproveSubs = null;
			TdProductApproveSub tdProductApproveSub = null;
			List<TdProductParticipant> tdProductParticipants = null;
			TdProductParticipant tdProductParticipant = null;
			List<TdBaseAsset> tdBaseAssets = null;
			TdBaseAsset baseAsset = null;// 存单质押
			TdProductApproveCrms tdProductApproveCrms = null;
			if (rowCount > 0) {
				// 开始准备导入数据
				for (int i = 1; i < rowCount; i++) {// 第一行是UDAS名称标
					// 判断读取的所在行的行标属性
					tdProductApproveMain = new TdProductApproveMain();
					productApproveSubs = new ArrayList<TdProductApproveSub>();
					tdProductParticipants = new ArrayList<TdProductParticipant>();
					tdBaseAssets = new ArrayList<TdBaseAsset>();
					baseAsset = new TdBaseAsset();// 288存单质押
					tdProductApproveCrms = new TdProductApproveCrms();
					// 第一行为UDAS属性
					int colCount = sheet.getColumns();
					for (int j = 0; j < colCount; j++) {
						String colNames = StringUtils.trimToEmpty(sheet.getCell(j, 0).getContents());

						for (UdasExcelBean udasExcelBean : udasExcelBeans) {// 循环是否存在配置

							if (StringUtils.equalsIgnoreCase(colNames, udasExcelBean.getSheetColName())) {// 如果存在
								String propertyField = udasExcelBean.getPropertyField();// beans属性
								if ("3".equals(udasExcelBean.getFieldType())) {// 基础资产 导入
									Cell cell = sheet.getCell(j, i);
									String value = "";
									if (cell.getType().equals(CellType.NUMBER)
											|| cell.getType().equals(CellType.NUMBER_FORMULA)) {
										numberCell = (NumberCell) cell;
										value = String.valueOf(numberCell.getValue());
									} else if (cell.getType().equals(CellType.DATE)
											|| cell.getType().equals(CellType.DATE_FORMULA)) {
										date = (DateCell) cell;
										value = sdf.format(date.getDate());
									} else {
										value = cell.getContents();
									}
									if (null != StringUtils.trimToNull(udasExcelBean.getFieldTypeDict())) {
										taDictVo = dictionaryGetService.getTaDictByCodeAndValue(
												StringUtils.trimToNull(udasExcelBean.getFieldTypeDict()), value);
										value = (taDictVo == null ? value : taDictVo.getDict_key());
									}
									if ("288".equalsIgnoreCase(prodNo)) {// 存单质押
										BeanUtils.copyProperty(baseAsset, propertyField, value);
									}
									continue;
								}
								if ("4".equals(udasExcelBean.getFieldType())) {// 企信融贷通
									Cell cell = sheet.getCell(j, i);
									String value = "";
									if (cell.getType().equals(CellType.NUMBER)
											|| cell.getType().equals(CellType.NUMBER_FORMULA)) {
										numberCell = (NumberCell) cell;
										value = String.valueOf(numberCell.getValue());

									} else if (cell.getType().equals(CellType.DATE)
											|| cell.getType().equals(CellType.DATE_FORMULA)) {
										date = (DateCell) cell;
										value = sdf.format(date.getDate());
									} else {
										value = cell.getContents();
									}
									if (null != StringUtils.trimToNull(udasExcelBean.getFieldTypeDict())) {
										taDictVo = dictionaryGetService.getTaDictByCodeAndValue(
												StringUtils.trimToNull(udasExcelBean.getFieldTypeDict()), value);
										value = (taDictVo == null ? value : taDictVo.getDict_key());
									}
									BeanUtils.copyProperty(tdProductApproveCrms, propertyField, value);
								}
								if (propertyField.startsWith("P0")) {// 参与方配置 插入参与方交易表
									tdProductParticipant = new TdProductParticipant();
									tdProductParticipant.setPartyId(propertyField);
									tdProductParticipant.setPartyCode(
											StringUtils.trimToEmpty(sheet.getCell(j - 1, i).getContents()));
									tdProductParticipant
											.setPartyName(StringUtils.trimToEmpty(sheet.getCell(j, i).getContents()));
									tdProductParticipants.add(tdProductParticipant);
									// 根据中文获得ECIF号 目前暂时无法获得
									continue;
								}
								if ("298".equalsIgnoreCase(prodNo) || "299".equalsIgnoreCase(prodNo)) {// 特殊处理
									if ("ldNo".equalsIgnoreCase(propertyField)) {
										BeanUtils.copyProperty(tdProductApproveMain, propertyField,
												sheet.getCell(j, i).getContents());
									}
									// 判定字段类型 textbox combobox datebox numberbox
									// 先判定EXCEL的数据类型，然后根据 字段类型 textbox combobox datebox numberbox进行转换
									Cell cell = sheet.getCell(j, i);
									String value = "";
									if (cell.getType().equals(CellType.NUMBER)
											|| cell.getType().equals(CellType.NUMBER_FORMULA)) {
										numberCell = (NumberCell) cell;
										value = String.valueOf(numberCell.getValue());

									} else if (cell.getType().equals(CellType.DATE)
											|| cell.getType().equals(CellType.DATE_FORMULA)) {
										date = (DateCell) cell;
										value = sdf.format(date.getDate());
									} else {
										value = cell.getContents();
									}
									// 对value进行再一次的判定
									if (null != StringUtils.trimToNull(udasExcelBean.getFieldTypeDict())) {// 数据字典项//
																											// 将value中文转换为key值
										taDictVo = dictionaryGetService
												.getTaDictByCodeAndValue(udasExcelBean.getFieldTypeDict(), value);
										value = (taDictVo == null ? value : taDictVo.getDict_key());
									}
									// 进行判定是否是自定义还是预定义
									if (null == StringUtils.trimToNull(udasExcelBean.getFieldType())) {
										// 判断 propertyFieldTrueField.getFldName()是否是 sponInst 是 那么需要反向获得机构号
										BeanUtils.copyProperty(tdProductApproveMain, udasExcelBean.getPropertyField(),
												value);
									}
								} else {
									// 通过 propertyField从 tcFieldMap获得改字段对应的属性情况
									TcField propertyFieldTrueField = tcFieldMap.get(propertyField);
									if ("ldNo".equalsIgnoreCase(propertyField)) {
										BeanUtils.copyProperty(tdProductApproveMain, propertyField,
												sheet.getCell(j, i).getContents());
									}
									if (null != propertyFieldTrueField) {
										// 判定字段类型 textbox combobox datebox numberbox
										// 先判定EXCEL的数据类型，然后根据 字段类型 textbox combobox datebox numberbox进行转换
										Cell cell = sheet.getCell(j, i);
										String value = "";
										if (cell.getType().equals(CellType.NUMBER)
												|| cell.getType().equals(CellType.NUMBER_FORMULA)) {
											if ("numberbox".equalsIgnoreCase(propertyFieldTrueField.getFormType())) {
												numberCell = (NumberCell) cell;
												if (null != propertyFieldTrueField.getFormTimes()) {
													double valueBigDecimal = numberCell.getValue();
													value = String.valueOf(PlaningTools.div(valueBigDecimal,
															propertyFieldTrueField.getFormTimes().doubleValue(), 6));
												} else {
													value = String.valueOf(numberCell.getValue());
												}
											} else {
												value = cell.getContents();
											}
										} else if (cell.getType().equals(CellType.DATE)
												|| cell.getType().equals(CellType.DATE_FORMULA)) {
											date = (DateCell) cell;
											value = sdf.format(date.getDate());
										} else {
											value = cell.getContents();
										}
										// 对value进行再一次的判定// 数据字典项// 将value中文转换为key值
										if ("combobox".equalsIgnoreCase(propertyFieldTrueField.getFormType())) {
											taDictVo = dictionaryGetService
													.getTaDictByCodeAndValue(propertyFieldTrueField.getDefKey(), value);
											value = (taDictVo == null ? value : taDictVo.getDict_key());
										}
										// 进行判定是否是自定义还是预定义
										if (propertyFieldTrueField.getFldType()
												.equalsIgnoreCase(DictConstants.fldType.PredefineField)) {
											BeanUtils.copyProperty(tdProductApproveMain,
													propertyFieldTrueField.getFldName(), value);
										} else// 自定义
										{
											tdProductApproveSub = new TdProductApproveSub();
											tdProductApproveSub.setSubFldName(propertyFieldTrueField.getFldName());
											tdProductApproveSub.setSubFldValue(value);
											productApproveSubs.add(tdProductApproveSub);
										}
									}
								}
							}
						}
					}
					tdProductApproveMain.setPrdNo(Integer.parseInt(prodNo));
					tdProductApproveMain.setaDate(DateUtil.getCurrentDateAsString());
					baseAsset.setSeq(1);// 目前只有一只存单
					tdBaseAssets.add(baseAsset); // 存单质押
					tdProductApproveMain.setProductApproveCrms(tdProductApproveCrms);
					tdProductApproveMain.setBaseAssets(tdBaseAssets);
					tdProductApproveMain.setTdProductParticipants(tdProductParticipants);
					// 设置自定义
					tdProductApproveMain.setProductApproveSubs(productApproveSubs);
					productApproveMains.add(tdProductApproveMain);// 添加
				}
				// 循环 productApproveMains 进行持久化处理
				Map<String, Object> paramsMap2 = new HashMap<String, Object>();// int count = 0;
				HashMap<String, Object> map = new HashMap<String, Object>();
				for (TdProductApproveMain insertMain : productApproveMains) {
					System.out.println("------------" + insertMain.getDealNo());
					System.out.println("------------" + insertMain.getIntType());
					/**
					 * 1、插入相应的 TT_trd_tradde表
					 */
					if (insertMain.getDealNo() == null || "".equalsIgnoreCase(insertMain.getDealNo())) {
						continue;
					}
					TtTrdTrade ttTrdTrade = new TtTrdTrade();
					map.put("trdtype", "T");
					ttTrdTrade
							.setTrade_id((insertMain.getDealNo() == null || "".equalsIgnoreCase(insertMain.getDealNo()))
									? this.tradeMapper.getTradeId(map)
									: insertMain.getDealNo());
					insertMain.setDealNo(ttTrdTrade.getTrade_id());
					ttTrdTrade.setVersion_number(0);
					ttTrdTrade.setTrade_time(DateUtil.getCurrentDateAsString());
					ttTrdTrade.setTrdtype(DictConstants.TrdType.Custom);
					ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Verified);
					ttTrdTrade.setIs_active(DictConstants.YesNo.YES);
					insertMain.setIsActive(DictConstants.YesNo.YES);
					insertMain.setSponsor(SlSessionHelper.getUserId());
					ttTrdTrade.setOperator_id("SYSTEM");
					ttTrdTrade.setOperate_time(DateUtil.getCurrentDateAsString());
					insertMain.setDealType(DictConstants.DealType.Verify);
					System.out.println("***************" + insertMain.getDealNo());
					paramsMap2.clear();
					paramsMap2.put("trade_id", insertMain.getDealNo());
					if (null != tradeMapper.selectOneTradeIdorOrderId(paramsMap2) || insertMain.getAmt() <= 0) {
						if (insertMain.getPrdNo() == 298 || insertMain.getPrdNo() == 299) {// 特殊处理
							System.out.println(insertMain.getDealNo());
						}
						continue;
						/*
						 * ttTrdTrade.setTrade_id(ttTrdTrade.getTrade_id()+"SEQ"+count);
						 * insertMain.setDealNo(ttTrdTrade.getTrade_id());
						 */
					}
					tradeMapper.insertTrade(ttTrdTrade);// 插入头信息
					if (insertMain.getIntType() != null && !"".equals(insertMain.getIntType())) {
						// 1.将参与方持久化
						for (int p = 0; p < insertMain.getTdProductParticipants().size(); p++) {
							insertMain.getTdProductParticipants().get(p).setDealNo(insertMain.getDealNo());
							insertMain.getTdProductParticipants().get(p).setVersion("0");
						}
						tdProductParticipantService
								.insertBatchForTdProductParticipant(insertMain.getTdProductParticipants());
						// 2.向main表中插入数据
						System.out.println(insertMain.toString());
						productApproveMainMapper.insert(insertMain);
						// 3.向sub表插入数据
						updateProductApproveSub(insertMain);
						if (null != insertMain.getProductApproveCrms()) {
							insertMain.getProductApproveCrms().setDealNo(insertMain.getDealNo());
							insertMain.getProductApproveCrms().setVersion(insertMain.getVersion());
							this.tdProductApproveCrmsMapper.insert(insertMain.getProductApproveCrms());
						}
						TdRateRange rate = new TdRateRange();
						rate.setDealNo(insertMain.getDealNo());
						rate.setVersion(insertMain.getVersion());
						rate.setBeginDate(insertMain.getvDate());
						rate.setEndDate(insertMain.getmDate());
						rate.setExecRate(insertMain.getContractRate());
						tdRateRangeMapper.insert(rate);
						if (insertMain.getIntType().equals(DictConstants.intType.chargeAfter)) {
							// 内核计算
							/***
							 * if(insertMain.getPrdNo() == 294 && insertMain.getInvtype().equals("3")){
							 * if(!StringUtils.equalsIgnoreCase(String.valueOf(insertMain.getPrdNo()),
							 * "907")){//活期业务无预置现金流(目前只有存放同业活期) List<PrincipalInterval> principalIntervals =
							 * new ArrayList<PrincipalInterval>();//本金变化区间 List<InterestRange>
							 * interestRanges = new ArrayList<InterestRange>();//利率变化区间 PrincipalInterval
							 * principalInterval = new PrincipalInterval();
							 * principalInterval.setStartDate(insertMain.getvDate());principalInterval.setEndDate(insertMain.getmDate());principalInterval.setResidual_Principal(insertMain.getAmt());
							 * principalIntervals.add(principalInterval); InterestRange interestRange = new
							 * InterestRange();
							 * interestRange.setStartDate(insertMain.getvDate());interestRange.setEndDate(insertMain.getmDate());interestRange.setRate(insertMain.getContractRate());
							 * interestRanges.add(interestRange);
							 * calCashFlowService.createAllCashFlowsForSaveApprove(principalIntervals,
							 * interestRanges, BeanUtil.beanToMap(insertMain)); } TdTrdTpos tpos = new
							 * TdTrdTpos(); tpos.setDealNo(insertMain.getDealNo());
							 * tpos.setSettlAmt(insertMain.getAmt()); tpos.setConctAmt(insertMain.getAmt());
							 * tpos.setProcAmt(insertMain.getProcAmt());
							 * tpos.setVersion(insertMain.getVersion()); this.tposMapper.insert(tpos);
							 * //4.出账
							 * tradeCustomServiceImpl.saveCompleteDuration(BeanUtil.beanToMap(insertMain));
							 * }
							 ***/
						} else if (insertMain.getIntType().equals(DictConstants.intType.chargeBefore)) {
							if (!StringUtils.equalsIgnoreCase(String.valueOf(insertMain.getPrdNo()), "907")) {// 活期业务无预置现金流(目前只有存放同业活期)
								List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();// 本金变化区间
								List<InterestRange> interestRanges = new ArrayList<InterestRange>();// 利率变化区间
								PrincipalInterval principalInterval = new PrincipalInterval();
								principalInterval.setStartDate(insertMain.getvDate());
								principalInterval.setEndDate(insertMain.getmDate());
								principalInterval.setResidual_Principal(insertMain.getAmt());
								principalIntervals.add(principalInterval);
								InterestRange interestRange = new InterestRange();
								interestRange.setStartDate(insertMain.getvDate());
								interestRange.setEndDate(insertMain.getmDate());
								interestRange.setRate(insertMain.getContractRate());
								interestRanges.add(interestRange);
								calCashFlowService.createAllCashFlowsForSaveApprove(principalIntervals, interestRanges,
										BeanUtil.beanToMap(insertMain));
							}
							HashMap<String, Object> amorMap = new HashMap<String, Object>();
							amorMap.put("dealNo", insertMain.getDealNo());
							amorMap.put("startDate", insertMain.getvDate());
							amorMap.put("endDate", dateService.getDayendDate());
							System.out.println("==========" + insertMain.getDealNo());
							System.out.println("==========" + insertMain.getPrdNo());
							System.out.println("==========" + insertMain.getvDate());
							TdCashflowAmor tmCashflowAmors = tdCashflowAmorMapper.selectAmorAmtByDealNo(amorMap);
							TdTrdTpos tpos = new TdTrdTpos();
							tpos.setDealNo(insertMain.getDealNo());// 业务流水号
							tpos.setVersion(insertMain.getVersion());// 版本号
							tpos.setSettlAmt(insertMain.getProcAmt());// 
							tpos.setConctAmt(insertMain.getAmt());//
							tpos.setRetrnAmt(0);//
							tpos.setAmorTint(tmCashflowAmors.getActAmorAmt());// 已摊销金额
							tpos.setProcAmt(insertMain.getProcAmt());//
							tpos.setUnamorInt(
									PlaningTools.sub(insertMain.getIntAmt(), tmCashflowAmors.getActAmorAmt()));// 未摊销金额
							trdTposMapper.insert(tpos);
							// 4.出账
							// tradeCustomServiceImpl.saveCompleteDuration(BeanUtil.beanToMap(insertMain));
							String postdate = dateService.getDayendDate();
							// 调用账务接口
							Map<String, Object> amtMap = new HashMap<String, Object>();
							List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
							TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(insertMain.getAmt());
							accTrdDaily.setAccType(DurationConstants.AccType.LOAN_AMT);
							accTrdDaily.setDealNo(insertMain.getDealNo());// 业务正式申请交易单
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setPostDate(postdate);
							accTrdDaily.setRefNo(insertMain.getRefNo());
							accTrdDailies.add(accTrdDaily);
							amtMap.put(DurationConstants.AccType.LOAN_AMT, accTrdDaily.getAccAmt());
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(insertMain.getProcAmt() + tpos.getAmorTint());
							accTrdDaily.setAccType(DurationConstants.AccType.LOAN_PROCAMT);
							accTrdDaily.setDealNo(insertMain.getDealNo());// 业务正式申请交易单
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setPostDate(postdate);
							accTrdDaily.setRefNo(insertMain.getRefNo());
							accTrdDailies.add(accTrdDaily);
							amtMap.put(DurationConstants.AccType.LOAN_PROCAMT, accTrdDaily.getAccAmt());
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tpos.getUnamorInt());
							accTrdDaily.setAccType(DurationConstants.AccType.LOAN_DISAMT);
							accTrdDaily.setDealNo(insertMain.getDealNo());// 业务正式申请交易单
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setPostDate(postdate);
							accTrdDaily.setRefNo(insertMain.getRefNo());
							accTrdDailies.add(accTrdDaily);
							amtMap.put(DurationConstants.AccType.LOAN_DISAMT, accTrdDaily.getAccAmt());
							InstructionPackage instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(insertMain));
							instructionPackage.setAmtMap(amtMap);
							String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if (null != flowId && !"".equals(flowId)) {// 更新回执会计套号 每笔交易
								for (int i = 0; i < accTrdDailies.size(); i++) {
									accTrdDailies.get(i).setAcctNo(flowId);
									// accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
								}
							}
							// 持久化数据
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert",
									accTrdDailies);
						}
					} else {
						// 1.将参与方持久化
						for (int p = 0; p < insertMain.getTdProductParticipants().size(); p++) {
							insertMain.getTdProductParticipants().get(p).setDealNo(insertMain.getDealNo());
							insertMain.getTdProductParticipants().get(p).setVersion("0");
						}
						tdProductParticipantService
								.insertBatchForTdProductParticipant(insertMain.getTdProductParticipants());
						// 2.向main表中插入数据
						System.out.println(insertMain.toString());
						productApproveMainMapper.insert(insertMain);
						// 3.向sub表插入数据
						updateProductApproveSub(insertMain);
						if (null != insertMain.getProductApproveCrms()) {
							insertMain.getProductApproveCrms().setDealNo(insertMain.getDealNo());
							insertMain.getProductApproveCrms().setVersion(insertMain.getVersion());
							this.tdProductApproveCrmsMapper.insert(insertMain.getProductApproveCrms());
						}
						TdRateRange rate = new TdRateRange();
						rate.setDealNo(insertMain.getDealNo());// 交易流水
						rate.setVersion(insertMain.getVersion());
						rate.setBeginDate(insertMain.getvDate());
						rate.setEndDate(insertMain.getmDate());
						rate.setExecRate(insertMain.getContractRate());
						tdRateRangeMapper.insert(rate);
					}
				}
			}
		} else if ("基金持仓".equals(sheetName)) {
			TdProductFundTpos fundTpos = null;
			int rowCount = sheet.getRows();
			for (int i = 1; i < rowCount; i++) {// 第一行是UDAS名称标
				// 判断读取的所在行的行标属性
				fundTpos = new TdProductFundTpos();
				ProductApproveFund fund = new ProductApproveFund();
				// 第一行为UDAS属性
				int colCount = sheet.getColumns();
				for (int j = 0; j < colCount; j++) {
					String colNames = StringUtils.trimToEmpty(sheet.getCell(j, 0).getContents());
					if ("产品代码".equals(colNames)) {
						fundTpos.setPrdNo(readValue(sheet, i, j).substring(0, 3));
					} else if ("四分类".equals(colNames)) {
						fundTpos.setInvtype("3");
					} else if ("基金代码".equals(colNames)) {
						fundTpos.setFundCode(readValue(sheet, i, j));
					} else if ("持仓份额".equals(colNames)) {
						fundTpos.setUtQty(new BigDecimal(readValue(sheet, i, j)));
					} else if ("持仓金额".equals(colNames)) {
						fundTpos.setUtAmt("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("未结转金额".equals(colNames)) {
						fundTpos.setNcvAmt("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("币种".equals(colNames)) {
						fundTpos.setCcy(readValue(sheet, i, j));
					} else if ("ECIF号".equals(colNames)) {
						fundTpos.setcNo(readValue(sheet, i, j));
					} else if ("机构号".equals(colNames)) {
						fund.setSponInst(readValue(sheet, i, j));
					}
				}
				if (fundTpos.getUtAmt().doubleValue() > 0) {
					HashMap<String, Object> mapSel = new HashMap<String, Object>();
					mapSel.put("trdtype", "HJ");
					String order_id = approveManageDao.getOrderId(mapSel);
					fund.setDealNo(order_id);
					// tdProductFundTposMapper.insert(fundTpos);
					fund.setPrdNo(fundTpos.getPrdNo());
					fund.setDealType(DictConstants.DealType.Verify); // 审批单类型 1-审批 2-核实
					fund.setcNo(fundTpos.getcNo());// 交易对手编号 交易对手表的id
					fund.setCcy(fundTpos.getCcy());// 币种
					fund.setAmt(fundTpos.getUtAmt());// 本金(交易金额)
					fund.setFundCode(fundTpos.getFundCode());// 基金代码
					fund.setVersion(0);// 版本
					fund.settDate("2017-10-01");
					fund.setDealDate("2017-10-01");
					fund.setIsActive(DictConstants.YesNo.YES);// 是否有效
					fund.setShareAmt(fundTpos.getUtQty());// 交易份额（元）
					fund.setInvtype(fundTpos.getInvtype());// 资产会计四分类
					productApproveFundMapper.insert(fund);

					// fund =
					// productApproveFundMapper.getProductApproveFundList(BeanUtil.beanToMap(fund),
					// ParameterUtil.getRowBounds(BeanUtil.beanToMap(fund))).get(0);
					int result = tdFundAcupService.fundAcupManager(fund);
					if (result > 0) {
						System.out.println("基金申购账务处理成功！");
					}
				}
			}

		} else if ("基金基础信息".equals(sheetName)) {
			TaFund fund = null;
			int rowCount = sheet.getRows();
			for (int i = 1; i < rowCount; i++) {// 第一行是UDAS名称标
				// 判断读取的所在行的行标属性
				fund = new TaFund();
				// 第一行为UDAS属性
				int colCount = sheet.getColumns();
				for (int j = 0; j < colCount; j++) {
					String colNames = StringUtils.trimToEmpty(sheet.getCell(j, 0).getContents());
					if ("基金代码".equals(colNames)) {
						fund.setFundCode(readValue(sheet, i, j));
					} else if ("基金名称".equals(colNames)) {
						fund.setFundName(readValue(sheet, i, j));
					} else if ("基金成立日期".equals(colNames)) {
						fund.setEstDate(readValue(sheet, i, j));
					} else if ("投资类型".equals(colNames)) {
						fund.setInvType("3");
					} else if ("基金类型".equals(colNames)) {
						fund.setFundType(readValue(sheet, i, j));
					} else if ("币种".equals(colNames)) {
						fund.setCcy(readValue(sheet, i, j));
					} else if ("投资名称".equals(colNames)) {
						fund.setInvName(readValue(sheet, i, j));
					} else if ("管理公司".equals(colNames)) {
						fund.setManagComp(readValue(sheet, i, j));
					} else if ("基金托管人".equals(colNames)) {
						fund.setCustodian(readValue(sheet, i, j));
					} else if ("管理费率".equals(colNames)) {
						fund.setManagRate("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("托管费率".equals(colNames)) {
						fund.setTrustRate("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("申购费率".equals(colNames)) {
						fund.setApplyRate("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("赎回费率".equals(colNames)) {
						fund.setRedeemRate("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("份额结转方式".equals(colNames)) {
						fund.setCoverMode("".equals(readValue(sheet, i, j)) ? new BigDecimal(0)
								: new BigDecimal(readValue(sheet, i, j)));
					} else if ("份额结转日期".equals(colNames)) {
						fund.setCoverDay(
								"".equals(readValue(sheet, i, j)) ? 1 : Integer.valueOf(readValue(sheet, i, j)));
					} else if ("备注".equals(colNames)) {
						fund.setRemark(readValue(sheet, i, j));
					} else if ("基金周期".equals(colNames)) {

					}
				}
				taFundMapper.insert(fund);
			}
		}

	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	public void updateProductApproveSub(TdProductApproveMain productApproveMain) {

		// 审批子表
		List<TdProductApproveSub> productApproveSubs = productApproveMain.getProductApproveSubs();
		productApproveSubMapper.deleteProductApproveSubByDealNo(productApproveMain.getDealNo());
		if (productApproveSubs != null && productApproveSubs.size() > 0) {
			for (TdProductApproveSub productApproveSub : productApproveSubs) {
				productApproveSub.setGenMainNo(productApproveMain.getDealNo());
				productApproveSubMapper.insert(productApproveSub);
			}
		}
		// 基础资产
		List<TdBaseAsset> baseAssets = productApproveMain.getBaseAssets();
		baseAssetMapper.deleteBaseAssetByDealNo(productApproveMain.getDealNo());
		if (baseAssets != null && baseAssets.size() > 0) {
			for (TdBaseAsset baseAsset : baseAssets) {
				baseAsset.setDealNo(productApproveMain.getDealNo());
				baseAssetMapper.insert(baseAsset);
			}
		}
		// 保理收款权基础资产
		List<TdBaseAssetBl> baseAssetBls = productApproveMain.getBaseAssetBlInfos();
		baseAssetBlMapper.deleteTdBaseAssetBlByDealNo(productApproveMain.getDealNo());
		if (baseAssetBls != null && baseAssetBls.size() > 0) {
			for (TdBaseAssetBl baseAssetBl : baseAssetBls) {
				baseAssetBl.setDealNo(productApproveMain.getDealNo());
				baseAssetBl.setVersion(productApproveMain.getVersion());
				baseAssetBlMapper.insert(baseAssetBl);
			}
		}
		// 票据基础资产
		List<TdBaseAssetBill> baseAssetBills = productApproveMain.getBaseAssetBills();
		baseAssetBillMapper.deleteBaseAssetBillByDealNo(productApproveMain.getDealNo());
		if (baseAssetBills != null && baseAssetBills.size() > 0) {
			for (TdBaseAssetBill baseAssetBill : baseAssetBills) {
				baseAssetBill.setDealNo(productApproveMain.getDealNo());
				baseAssetBillMapper.insert(baseAssetBill);
			}
		}

		// 信用证基础资产
		List<TdAssetLc> tAssetLcs = productApproveMain.getBaseAssetLCs();
		assetLcMapper.deleteTdAssetLcByDealNo(productApproveMain.getDealNo());
		if (tAssetLcs != null && tAssetLcs.size() > 0) {
			for (TdAssetLc assetLc : tAssetLcs) {
				assetLc.setDealNo(productApproveMain.getDealNo());
				assetLcMapper.insert(assetLc);
			}
		}
		// 风险资产
		List<TdRiskAsset> riskAssets = productApproveMain.getRiskAssets();
		riskAssetMapper.deleteRiskAssetByDealNo(productApproveMain.getDealNo());
		if (riskAssets != null && riskAssets.size() > 0) {
			for (TdRiskAsset riskAsset : riskAssets) {
				riskAsset.setDealNo(productApproveMain.getDealNo());
				riskAssetMapper.insert(riskAsset);
			}
		}
		// 缓释信息
		List<TdBaseRisk> baseRisks = productApproveMain.getBaseRisks();
		baseRiskMapper.deleteBaseRiskByDealNo(productApproveMain.getDealNo());
		if (baseRisks != null && baseRisks.size() > 0) {
			for (TdBaseRisk baseRisk : baseRisks) {
				baseRisk.setDealNo(productApproveMain.getDealNo());
				baseRiskMapper.insert(baseRisk);
			}
		}
		// 风险缓释
		List<TdRiskSlowRelease> riskSlowReleases = productApproveMain.getRiskSlowReleases();
		riskSlowReleaseMapper.deleteRiskSlowReleaseByDealNo(productApproveMain.getDealNo());
		if (riskSlowReleases != null && riskSlowReleases.size() > 0) {
			for (TdRiskSlowRelease riskSlowRelease : riskSlowReleases) {
				riskSlowRelease.setDealNo(productApproveMain.getDealNo());
				riskSlowReleaseMapper.insert(riskSlowRelease);
			}
		}
		// 合同协议
		List<TdProductApproveProtocols> productApproveProtocols = productApproveMain.getProductProcotols();
		productApproveProtocolsMapper.deleteProductApproveProtocolsByDealNo(productApproveMain.getDealNo());
		if (productApproveProtocols != null && productApproveProtocols.size() > 0) {
			for (TdProductApproveProtocols productApproveProtocols2 : productApproveProtocols) {
				productApproveProtocols2.setDealNo(productApproveMain.getDealNo());
				productApproveProtocols2.setVersion(productApproveMain.getVersion());
				productApproveProtocolsMapper.insert(productApproveProtocols2);
			}
		}

		// 特定债券
		List<TdAssetSpecificBond> assetSpecificBonds = productApproveMain.getBaseAssetSpecificBonds();
		assetSpecificBondMapper.deleteTdAssetSpecificBondByDealNo(productApproveMain.getDealNo());
		if (assetSpecificBonds != null && assetSpecificBonds.size() > 0) {
			for (TdAssetSpecificBond assetSpecificBond : assetSpecificBonds) {
				assetSpecificBond.setDealNo(productApproveMain.getDealNo());
				assetSpecificBond.setVersion(productApproveMain.getVersion());
				assetSpecificBondMapper.insert(assetSpecificBond);
			}
		}

		// CheckList审查信息
		List<TcLoanCheckboxDeal> list = productApproveMain.getTcLoanCheckboxDeal();
		if (list != null && list.size() > 0) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ckPrd", list.get(0).getCkPrd());
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("userId", list.get(0).getUserId());
			try {
				// 查询当前环节、当前用户所属审批角色
				Map<String, Object> flowMap = flowQueryService.getFirstUserTaskSetting(list.get(0).getCkPrd(),
						productApproveMain.getDealType(), SlSessionHelper.getInstitution().getInstId());
				@SuppressWarnings("unchecked")
				List<String> lst_flow_role = (List<String>) flowMap.get("lst_flow_role");
				if (lst_flow_role != null) {
					if (lst_flow_role.size() > 0) {
						map.put("roleNo", lst_flow_role.get(0));
					}
				}
				// 先删除
				tcLoanCheckboxDealMapper.deleteTcLoanCheckboxDeal(map);
				map.put("loan_list", list);
				// 查询该用户的审批角色，一个用户可以多个审批角色，查看历史时便于区分
				tcLoanCheckboxDealMapper.insertTcLoanCheckboxDeal(map);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public String readValue(Sheet sheet, int i, int j) {
		NumberCell numberCell = null;
		DateCell date = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Cell cell = sheet.getCell(j, i);
		String value = "";
		if (cell.getType().equals(CellType.NUMBER) || cell.getType().equals(CellType.NUMBER_FORMULA)) {
			numberCell = (NumberCell) cell;
			value = String.valueOf(numberCell.getValue());

		} else if (cell.getType().equals(CellType.DATE) || cell.getType().equals(CellType.DATE_FORMULA)) {
			date = (DateCell) cell;
			value = sdf.format(date.getDate());
		} else {
			value = cell.getContents();
		}
		return value;
	}

	@Override
	public void exportDealsFromIfbmToExcel(String path) {
		// TODO Auto-generated method stub
		// 1、调用 查询出所有的自定义交易数据
		WritableWorkbook book = null;
		int count = 0;
		int flag = 0;
		try {
			book = Workbook.createWorkbook(new File(path));
			List<TdProductApproveMain> productApproveMains = null;
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			List<TdProductParticipant> productParticipants = null;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Iterator<String> iterator = DataTransforConstants.productMappingSheetMap.keySet().iterator();
			Label partyName = null;
			Label guaName = null;
			Label fieldName = null;
			Number numberCell = null;
			Date date = null;
			Object resultObject = null;
			jxl.write.DateFormat df = new jxl.write.DateFormat("yyyy/MM/dd");
			jxl.write.DateTime cell = null;
			Label otherField = null;
			List<TdProductApproveSub> productApproveSubs = null;
			Map<String, Object> productApproveSubMap = new HashMap<String, Object>();
			List<TcField> tcFields = null;
			WritableSheet sheet = null;
			List<UdasExcelBean> udasExcelBeans = null;
			Label LDNO = null;
			Label ECIFNO = null;
			TdProductApproveCrms tdProductApproveCrms = null;
			while (iterator.hasNext()) {
				paramsMap.clear();
				String prdNo = iterator.next();
				paramsMap.put("prdNo", prdNo);
				String prdName = DataTransforConstants.productMappingSheetMap.get(prdNo);
				// 根据产品号创建sheet页
				sheet = book.createSheet(prdName, count);
				flag = 0;
				// 从TC_FIELD自定义产品表中获取各属性的字段类型及预定义与自定义类型
				if ("298".equalsIgnoreCase(prdNo) || "299".equalsIgnoreCase(prdNo)) {
					udasExcelBeans = DataTransforConstants.udasFieldAndPropertyHashMap.get(prdNo);
				} else {
					tcFields = fieldService.getChoosedFieldListForPrdNo(paramsMap);
				}
				int row = 1;// 行
				productApproveMains = productApproveMainMapper.getAllProductApproveMainForActive(paramsMap);
				if (productApproveMains == null || productApproveMains.size() <= 0) {// 没有数据的前提下，需要进行模板的导出
					int cols = 0;// 列
					if (flag == 0) {
						LDNO = new Label(cols, 0, "LD编号");// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
						sheet.addCell(LDNO);
					}
					List<TcProductParticipant> participants = tcProductParticipantMapper
							.getProductParticipantList(paramsMap, ParameterUtil.getRowBounds(paramsMap)).getResult();
					cols++;
					for (TcProductParticipant tcProductParticipant : participants) {
						// 写入参与方信息
						if (flag == 0) {
							ECIFNO = new Label(cols, 0, tcProductParticipant.getPartyName() + "ECIFNO");// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
							sheet.addCell(ECIFNO);
							cols++;
							partyName = new Label(cols, 0, tcProductParticipant.getPartyName());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
							sheet.addCell(partyName);
						}
						cols++;
					}
					for (TcField tcField : tcFields) {
						if (flag == 0) {
							fieldName = new Label(cols, 0, tcField.getFormName());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
							sheet.addCell(fieldName);
						}
						cols++;
					}
				} else {
                    for (TdProductApproveMain tdProductApproveMain : productApproveMains) {
                        tdProductApproveCrms = new TdProductApproveCrms();
                        tdProductApproveCrms.setDealNo(tdProductApproveMain.getDealNo());
                        tdProductApproveCrms.setVersion(tdProductApproveMain.getVersion());
                        tdProductApproveCrms = this.tdProductApproveCrmsMapper.selectByPrimaryKey(tdProductApproveCrms);
                        // SUB自定义转为MAP
                        productApproveSubMap.clear();
                        paramsMap.put("genMainNo", tdProductApproveMain.getDealNo());
                        productApproveSubs = this.productApproveSubMapper.getProductApproveSubList(paramsMap);
                        if (null != productApproveSubs && productApproveSubs.size() > 0) {
                            for (TdProductApproveSub productApproveSub : productApproveSubs) {
                                productApproveSubMap.put(productApproveSub.getSubFldName(),
                                        productApproveSub.getSubFldValue());
                            }
                        }

                        paramsMap.put("dealNo", tdProductApproveMain.getDealNo());
                        productParticipants = tdProductParticipantMapper
                                .getProductParticipantsForPrdNoDealNo(paramsMap);
                        int cols = 0;// 列
                        if (flag == 0) {
                            LDNO = new Label(cols, 0, "LD编号");// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                            sheet.addCell(LDNO);
                        }
                        LDNO = new Label(cols, row, BeanUtils.getProperty(tdProductApproveMain, "ldNo"));
                        sheet.addCell(LDNO);
                        cols++;
                        for (TdProductParticipant participant : productParticipants) {
                            // 写入参与方信息
                            if (flag == 0) {
                                ECIFNO = new Label(cols, 0, participant.getPartyName() + "ECIFNO");// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                sheet.addCell(ECIFNO);
                                cols++;
                                partyName = new Label(cols, 0, participant.getPartyName());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                sheet.addCell(partyName);
                            } else {
                                guaName = new Label(cols, row, participant.getPartyCode());
                                sheet.addCell(guaName);
                                cols++;
                            }
                            guaName = new Label(cols, row, participant.getGuaName());
                            sheet.addCell(guaName);
                            cols++;
                        }
                        if ("298".equalsIgnoreCase(prdNo) || "299".equalsIgnoreCase(prdNo))// 特殊处理
                        {
                            for (UdasExcelBean udasExcelBean : udasExcelBeans) {
                                if (flag == 0) {
                                    fieldName = new Label(cols, 0, udasExcelBean.getIfbmFormName());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                    sheet.addCell(fieldName);
                                }
                                try {
                                    if (null == StringUtils.trimToNull(udasExcelBean.getIfbmType())) {
                                        BeanUtils.getProperty(tdProductApproveMain, udasExcelBean.getIfbmBeanProp());
                                    } else {
                                        if (null != tdProductApproveCrms) {
                                            BeanUtils.getProperty(tdProductApproveCrms,
                                                    udasExcelBean.getIfbmBeanProp());
                                        }
                                    }
                                } catch (Exception e) {
                                    // TODO: handle exception
                                    continue;
                                }
                                if (StringUtils.trimToEmpty(udasExcelBean.getIfbmBeanProp()).toLowerCase()
                                        .contains("amt"))// 数据字典项 将value中文转换为key值
                                {
                                    resultObject = (null == StringUtils.trimToNull(udasExcelBean.getIfbmType())
                                            ? BeanUtils.getProperty(tdProductApproveMain,
                                                    udasExcelBean.getIfbmBeanProp())
                                            : (null != tdProductApproveCrms
                                                    ? BeanUtils.getProperty(tdProductApproveCrms,
                                                            udasExcelBean.getIfbmBeanProp())
                                                    : ""));

                                    if (null != StringUtils
                                            .trimToNull(resultObject == null ? "" : resultObject.toString())) {
                                        numberCell = new Number(cols, row, Double.parseDouble(resultObject.toString()));
                                        sheet.addCell(numberCell);
                                    }
                                } else {
                                    resultObject = (null == StringUtils.trimToNull(udasExcelBean.getIfbmType())
                                            ? BeanUtils.getProperty(tdProductApproveMain,
                                                    udasExcelBean.getIfbmBeanProp())
                                            : (null != tdProductApproveCrms
                                                    ? BeanUtils.getProperty(tdProductApproveCrms,
                                                            udasExcelBean.getIfbmBeanProp())
                                                    : ""));
                                    otherField = new Label(cols, row,
                                            resultObject == null ? "" : resultObject.toString());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                    sheet.addCell(otherField);
                                }
                                cols++;
                            }
                        } else {
                            // 真实的域 需要看域的类型情况
                            if (null != tcFields && tcFields.size() > 0) {
                                for (TcField tcField : tcFields) {
                                    if (flag == 0) {
                                        fieldName = new Label(cols, 0, tcField.getFormName());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                        sheet.addCell(fieldName);
                                    }
                                    try {
                                        if (tcField.getFldType()
                                                .equalsIgnoreCase(DictConstants.fldType.PredefineField)) {
                                            BeanUtils.getProperty(tdProductApproveMain, tcField.getFldName());
                                        }
                                    } catch (Exception e) {
                                        // TODO: handle exception
                                        continue;
                                    }
                                    if ("numberbox".equalsIgnoreCase(tcField.getFormType())) {// 数据字典项 将value中文转换为key值
                                        resultObject = tcField.getFldType()
                                                .equalsIgnoreCase(DictConstants.fldType.PredefineField)
                                                        ? BeanUtils.getProperty(tdProductApproveMain,
                                                                tcField.getFldName())
                                                        : productApproveSubMap.get(tcField.getFldName());
                                        if (null != StringUtils
                                                .trimToNull(resultObject == null ? "" : resultObject.toString())) {
                                            numberCell = new Number(cols, row,
                                                    Double.parseDouble(resultObject.toString()));
                                            sheet.addCell(numberCell);
                                        }
                                    } else if ("datebox".equalsIgnoreCase(tcField.getFormType())) {// 数据字典项将value中文转换为key值
                                        resultObject = tcField.getFldType()
                                                .equalsIgnoreCase(DictConstants.fldType.PredefineField)
                                                        ? BeanUtils.getProperty(tdProductApproveMain,
                                                                tcField.getFldName())
                                                        : productApproveSubMap.get(tcField.getFldName());
                                        if (null != StringUtils
                                                .trimToNull(resultObject == null ? "" : resultObject.toString())) {
                                            try {
                                                date = format.parse(resultObject.toString());
                                            } catch (Exception e) {
                                                // e.printStackTrace();
                                                throw new RException(e);
                                            }
                                            cell = new jxl.write.DateTime(cols, row, date, new WritableCellFormat(df));
                                            sheet.addCell(cell);
                                        }
                                    } else {
                                        resultObject = tcField.getFldType()
                                                .equalsIgnoreCase(DictConstants.fldType.PredefineField)
                                                        ? BeanUtils.getProperty(tdProductApproveMain,
                                                                tcField.getFldName())
                                                        : productApproveSubMap.get(tcField.getFldName());
                                        otherField = new Label(cols, row,
                                                resultObject == null ? "" : resultObject.toString());// 用label（文本类型）来创建单元格,三个参数分别为：列数、行数、内容
                                        sheet.addCell(otherField);
                                    }
                                    cols++;
                                }
                            }
                        }
                        row++;
                        flag++;
                    }
                }
				count++;
			}
			book.write();
		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != book) {
				try {
					book.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					throw new RException(e);
				}
			}
		}
	}
}
