package com.singlee.capital.data.transfor.service;

import java.util.List;

/**
 * T24的数据迁移文件
 * @author SINGLEE
 *
 */
public interface T24DataTranforService {
	/**
	 * 读取提前还本数据
	文件1格式：
	IFBM-PRITRN-迁移日八位日期
	内容:
	LD流水@!@本金还款金额@!@是否转逾(Y/N)
	
	EXAMPLE：
	1.文件名：IFBM-PRITRN-20170711
	内容:
	LD1629830091@!@666.00@!@Y
	 */
	/** T24提前还款转换 头寸和利息的处理;
	 * @param filepath  读文件地址  /
	 * @param filebak   备份文件地址 /
	 * @param indexof   文件名起始匹配符 !@!
	 * @param encoding  编码格式 UTF-8
	 * @param postdate  YYYYMMDD 日期
	 */
	public void doT24AdanceMaturityAmtToBalance(String filepath,String filebak,String splitStr,String encoding,String postdate);
	
	/**
	 * 将还款计划载入IFBM
	 * @param filepath
	 * @param filebak
	 * @param splitStr
	 * @param encoding
	 * @param postdate
	 */
	public void doT24ScheduleToDataBase(String filepath,String filebak,String splitStr,String encoding,String postdate);
	/**
	 * 读取T24文件，筛选过滤器
	 * @param filepath
	 * @param filebak
	 * @param splitStr
	 * @param encoding
	 * @param startWith
	 * @return
	 */
	public List<String> readT24FileToStringArrays(String filepath,String filebak,String splitStr,String encoding,String startWith);
	
	
}
