package com.singlee.capital.data.transfor.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.data.transfor.model.T24LdSchDet;

public interface T24LdSchDetMapper extends Mapper<T24LdSchDet>{

	public void deleteAllT24LdSchDets(Map<String, Object> paramsMap);
}
