package com.singlee.capital.data.transfor.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="TC_UDAS_EXCEL_MAPPING")
public class UdasExcelBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String prdNo;//产品代码
	
	private String prdNm;//产品名称
	
	private String sheetColName;//列名
	
	private String propertyFieldName;//属性名
	
	private String propertyField;//tdProductApproveMain属性名称
	
	private String fieldType;//1-参与方  2-tdProductApproveMain  3-子表	

	private String fieldTypeDict; //子表的数据字典
	
	private String ifbmFormName;//IFBM表单栏位
	
	private String ifbmBeanProp;//IFBM表单属性
	
	private String ifbmType;
	
	
	public String getIfbmType() {
		return ifbmType;
	}

	public void setIfbmType(String ifbmType) {
		this.ifbmType = ifbmType;
	}

	public String getIfbmFormName() {
		return ifbmFormName;
	}

	public void setIfbmFormName(String ifbmFormName) {
		this.ifbmFormName = ifbmFormName;
	}

	public String getIfbmBeanProp() {
		return ifbmBeanProp;
	}

	public void setIfbmBeanProp(String ifbmBeanProp) {
		this.ifbmBeanProp = ifbmBeanProp;
	}

	public String getFieldTypeDict() {
		return fieldTypeDict;
	}

	public void setFieldTypeDict(String fieldTypeDict) {
		this.fieldTypeDict = fieldTypeDict;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPrdNm() {
		return prdNm;
	}

	public void setPrdNm(String prdNm) {
		this.prdNm = prdNm;
	}

	public String getPropertyFieldName() {
		return propertyFieldName;
	}

	public void setPropertyFieldName(String propertyFieldName) {
		this.propertyFieldName = propertyFieldName;
	}

	public String getSheetColName() {
		return sheetColName;
	}

	public void setSheetColName(String sheetColName) {
		this.sheetColName = sheetColName;
	}

	public String getPropertyField() {
		return propertyField;
	}

	public void setPropertyField(String propertyField) {
		this.propertyField = propertyField;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "UdasExcelBean [sheetColName=" + sheetColName
				+ ", propertyFieldName=" + propertyFieldName
				+ ", propertyField=" + propertyField + ", fieldType="
				+ fieldType + "]";
	}
}
