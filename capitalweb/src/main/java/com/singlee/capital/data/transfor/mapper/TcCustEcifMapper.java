package com.singlee.capital.data.transfor.mapper;

import com.singlee.capital.data.transfor.model.TcCustEcif;

import tk.mybatis.mapper.common.Mapper;

public interface TcCustEcifMapper extends Mapper<TcCustEcif>{
	/**
	 * 根据UDAS的名称查询得到对应的 TcCustEcif
	 * @param udasName
	 * @return
	 */
	TcCustEcif getCustEcifByUdasName(String udasName);
}
