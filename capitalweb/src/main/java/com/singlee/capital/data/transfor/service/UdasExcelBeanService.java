package com.singlee.capital.data.transfor.service;

import java.util.List;
import java.util.Map;

import jxl.Sheet;

import com.singlee.capital.data.transfor.model.ProductUdasExcelBean;
import com.singlee.capital.data.transfor.model.UdasExcelBean;

public interface UdasExcelBeanService {
	/**
	 * 将总配置传入，持久化
	 * @param productUdasExcelBeans
	 */
	public void doUdasExcelPropertyToDataBase(List<ProductUdasExcelBean> productUdasExcelBeans);
	/**
	 * 
	 * @param sheet
	 */
	public void doUdasExcelDealsToDataBase(Sheet sheet) throws Exception;
	
	public List<UdasExcelBean> queryUdasExcelBeansByPrdNo(Map<String, Object> paramsMap);
	
	/**
	 * 
	  ---1、
	  SELECT T.PARTY_NAME,TP.PARTY_ID,TD.PARTY_CODE,TD.PARTY_NAME AS TRUE_NAME FROM TC_PRODUCT_PARTICIPANT TP 
	  LEFT JOIN TC_PARTICIPANT T ON TP.PARTY_ID=T.PARTY_ID
	  LEFT JOIN TD_PRODUCT_PARTICIPANT TD ON TP.PARTY_ID=TD.PARTY_ID
	  WHERE TP.PRD_NO='288' AND TD.DEAL_NO = 'CD20150303' ORDER BY TO_NUMBER(TP.SORT_NO)
	  ---2、
	  SELECT TFM.FORM_NAME,TF.FLD_NAME,TF.FLD_TYPE,TF.FORM_TYPE,TF.DEF_KEY FROM TC_FORM_FIELDSET_FIELD_MAP  TFM
	  LEFT JOIN TC_FIELD TF ON TFM.FLD_NO = TF.FLD_NO
	  LEFT JOIN TC_FORM_FIELDSET_MAP TFF ON TFM.FDS_NO = TFF.FDS_NO
	  WHERE TFF.FORM_NO IN
	  (SELECT FORM_NO FROM TC_PRODUCT_FORM_MAP WHERE PRD_NO='288')
	  AND TFM.FORM_NO IN
	  (SELECT FORM_NO FROM TC_PRODUCT_FORM_MAP WHERE PRD_NO='288')
	  ORDER BY TO_NUMBER(TFF.FORM_INDEX),TO_NUMBER(TFM.FORM_INDEX)
	 */
	/**
	 * 导出全部的数据到EXCEL表
	 */
	public void exportDealsFromIfbmToExcel(String path);
}
