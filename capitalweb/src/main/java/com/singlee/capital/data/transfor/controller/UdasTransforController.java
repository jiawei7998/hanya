package com.singlee.capital.data.transfor.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.singlee.cap.base.model.TbkAmount;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.data.transfor.model.DataTransforConstants;
import com.singlee.capital.data.transfor.model.ProductUdasExcelBean;
import com.singlee.capital.data.transfor.model.UdasExcelBean;
import com.singlee.capital.data.transfor.service.T24DataTranforService;
import com.singlee.capital.data.transfor.service.UdasExcelBeanService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.eu.model.TdEdCustAsg;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.users.model.TdCustIb;
import com.singlee.capital.users.service.TdCustIbService;

@Controller
@RequestMapping(value = "/UdasTransforController")
public class UdasTransforController extends CommonController {
	@Autowired
	UdasExcelBeanService udasExcelBeanService;
	@Autowired
	T24DataTranforService t24DataTranforService;
	@Autowired
	protected DayendDateService dayendDateService;
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdCustIbService custIbService;
	@Autowired
	DayendDateService dateService;

	@ResponseBody
	@RequestMapping(value = "/importEdCustForTransfor")
	public RetMsg<?> importEdCustForTransfor(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(edCustManangeService.transforEduForImportDatas());
	}

	@ResponseBody
	@RequestMapping(value = "/importCreditDataFromExcel")
	public RetMsg<?> importCreditDataFromExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		Workbook wb = null;
		try {
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			int rowCount = sheets[0].getRows();// 行数
			List<TdEdCustAsg> edCustAsgs = new ArrayList<TdEdCustAsg>();
			TdEdCustAsg teAsg = null;
			for (int i = 2; i < rowCount; i++) { // rows=2开始 第二行开始读取
				teAsg = new TdEdCustAsg();
				// EcifNum SysId Sector Customer ProductType Currency LoanAmt Term TermType
				// DueDate CntrStatus
				teAsg.setEcifNum(StringUtils.trimToEmpty(sheets[0].getCell(0, i).getContents()));
				teAsg.setSysId(StringUtils.trimToEmpty(sheets[0].getCell(1, i).getContents()));
				teAsg.setCustomer(StringUtils.trimToEmpty(sheets[0].getCell(3, i).getContents()));
				teAsg.setProductType(StringUtils.trimToEmpty(sheets[0].getCell(4, i).getContents()));
				teAsg.setCurrency(StringUtils.trimToEmpty(sheets[0].getCell(5, i).getContents()));
				teAsg.setLoanAmt(new BigDecimal(sheets[0].getCell(6, i).getContents().replace(",", "")).doubleValue());
				teAsg.setTerm(StringUtils.trimToEmpty(sheets[0].getCell(7, i).getContents()));
				teAsg.setTermType(StringUtils.trimToEmpty(sheets[0].getCell(8, i).getContents()));
				teAsg.setDueDate(StringUtils.trimToEmpty(sheets[0].getCell(9, i).getContents()));
				teAsg.setCntrStatus(StringUtils.trimToEmpty(sheets[0].getCell(10, i).getContents()));
				teAsg.setOpTime(StringUtils.trimToEmpty(sheets[0].getCell(11, i).getContents()));
				edCustAsgs.add(teAsg);
			}
			edCustManangeService.importCreditCustToDataBase(edCustAsgs);
			/**
			 * 一级法人信息带入
			 */
			rowCount = sheets[1].getRows();// 行数
			List<TdCustIb> custIbs = new ArrayList<TdCustIb>();
			TdCustIb custIb = null;
			for (int i = 1; i < rowCount; i++) { // rows=2开始 第二行开始读取
				if (null == StringUtils.trimToNull(sheets[1].getCell(0, i).getContents())) {
					continue;
				}
				custIb = new TdCustIb();
				custIb.setCustmerCode(StringUtils.trimToEmpty(sheets[1].getCell(0, i).getContents()));
				custIb.setFcciCode(StringUtils.trimToEmpty(sheets[1].getCell(1, i).getContents()));
				custIbs.add(custIb);
			}
			custIbService.insertTdCustIbs(custIbs);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return RetMsgHelper.ok(false);
		} finally {
			if (null != wb) {
				wb.close();
			}
		}
		return RetMsgHelper.ok(true);
	}

	/**
	 * 读取UDAS与IFBM之间的配置EXCEL
	 * 
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/importUdasConfig")
	public RetMsg<List<ProductUdasExcelBean>> importUdasConfig(MultipartHttpServletRequest request,
			HttpServletResponse response) {
		// 1、读取EXCEL UDAS与IFBM之间对应关系配置表 文件获取
		Workbook wb = null;
		List<ProductUdasExcelBean> productUdasExcelBeans = new ArrayList<ProductUdasExcelBean>();
		try {
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			List<UdasExcelBean> udasExcelBeans = null;
			ProductUdasExcelBean productUdasExcelBean = null;
			for (Sheet sheet : sheets) {
				String sheetName = sheet.getName();// 获得sheet的名称
				if (null != DataTransforConstants.sheetMappingProductMap.get(sheetName))// 判断sheet页是否是需要读取的产品匹配对应关系
				{
					String prodNo = DataTransforConstants.sheetMappingProductMap.get(sheetName);// 获得sheet页产品代码
					// A列和D列 为最终的配置列
					int rowCount = sheet.getRows();// 行数
					UdasExcelBean udasExcelBean = null;
					udasExcelBeans = new ArrayList<UdasExcelBean>();
					for (int i = 1; i < rowCount; i++) { // rows=1开始 第二行开始读取
						udasExcelBean = new UdasExcelBean();
						udasExcelBean.setPrdNm(sheetName);
						udasExcelBean.setPrdNo(prodNo);
						// if(null == StringUtils.trimToNull(sheet.getCell(0, i).getContents()))
						// continue;
						udasExcelBean.setSheetColName(StringUtils.trimToEmpty(sheet.getCell(0, i).getContents()));// A
						udasExcelBean.setPropertyFieldName(StringUtils.trimToEmpty(sheet.getCell(2, i).getContents()));// C
						udasExcelBean.setPropertyField(StringUtils.trimToEmpty(sheet.getCell(3, i).getContents()));// D
						udasExcelBean.setFieldType(StringUtils.trimToEmpty(sheet.getCell(4, i).getContents()));// E
						udasExcelBean.setFieldTypeDict(StringUtils.trimToEmpty(sheet.getCell(5, i).getContents()));// F
						udasExcelBean.setIfbmFormName(StringUtils.trimToEmpty(sheet.getCell(6, i).getContents()));// G
						udasExcelBean.setIfbmBeanProp(StringUtils.trimToEmpty(sheet.getCell(7, i).getContents()));// H
						udasExcelBean.setIfbmType(StringUtils.trimToEmpty(sheet.getCell(8, i).getContents()));// I
						udasExcelBeans.add(udasExcelBean);
					}
					productUdasExcelBean = new ProductUdasExcelBean();
					productUdasExcelBean.setPrdNm(sheetName);
					productUdasExcelBean.setPrdNo(prodNo);
					productUdasExcelBean.setUdasExcelBeans(udasExcelBeans);
					productUdasExcelBeans.add(productUdasExcelBean);
					DataTransforConstants.udasFieldAndPropertyHashMap.put(prodNo, udasExcelBeans);// 保存产品对应的读取UDAS数据的配置
				}
			}
			udasExcelBeanService.doUdasExcelPropertyToDataBase(productUdasExcelBeans);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != wb) {
				wb.close();
			}
		}
		return RetMsgHelper.ok(productUdasExcelBeans);// 返回前台展示配置

		// 2、根据sheet页对应的产品从数据库查询出产品对应的TdProductApproveMain的所有属性
		/**
		 * SELECT NAME1,NAME2 FROM ( SELECT T.PARTY_NAME AS NAME1,TP.PARTY_ID AS NAME2
		 * FROM TC_PRODUCT_PARTICIPANT TP LEFT JOIN TC_PARTICIPANT T ON
		 * TP.PARTY_ID=T.PARTY_ID WHERE TP.PRD_NO = '905' ORDER BY
		 * TO_NUMBER(TP.SORT_NO)) UNION ALL SELECT NAME1,NAME2 FROM ( SELECT
		 * TFM.FORM_NAME AS NAME1,TF.FLD_NAME AS NAME2 FROM TC_FORM_FIELDSET_FIELD_MAP
		 * TFM LEFT JOIN TC_FIELD TF ON TFM.FLD_NO = TF.FLD_NO LEFT JOIN
		 * TC_FORM_FIELDSET_MAP TFF ON TFM.FDS_NO = TFF.FDS_NO WHERE TFF.FORM_NO IN
		 * (SELECT FORM_NO FROM TC_PRODUCT_FORM_MAP WHERE PRD_NO='905') AND TFM.FORM_NO
		 * IN (SELECT FORM_NO FROM TC_PRODUCT_FORM_MAP WHERE PRD_NO='905') ORDER BY
		 * TO_NUMBER(TFF.FORM_INDEX),TO_NUMBER(TFM.FORM_INDEX) )
		 */
		// 3、根据产品读取相应sheet页的每一行数据，根据列名得到UdasExcelBean
		// 循环产品下的所有 UdasExcelBean 并采用beanUtil进行属性赋值
		// 根据 UdasExcelBean的fieldType=2进行 主交易表的导入;

	}

	@ResponseBody
	@RequestMapping(value = "/importUdasDataExcelIntoIfbmDataBase")
	public RetMsg<?> importUdasDataExcelIntoIfbmDataBase(MultipartHttpServletRequest request,
			HttpServletResponse response) {
		Workbook wb = null;
		try {
			List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			List<Workbook> excelList = new LinkedList<Workbook>();
			for (UploadedFile uploadedFile : uploadedFileList) {
				JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
				excelList.add(book);
			}
			JY.require(excelList.size() == 1, "只能处理一份excel");
			wb = excelList.get(0);
			Sheet[] sheets = wb.getSheets();// 获得卡片sheet数量
			for (Sheet sheet : sheets) {

				udasExcelBeanService.doUdasExcelDealsToDataBase(sheet);
			}
			// udasExcelBeanService.exportDealsFromIfbmToExcel("C:/OUTEXCEL/IFBM导出模板.xls");

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			throw new RException(e);
		} finally {
			if (null != wb) {
				wb.close();
			}
		}
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/importT24Data")
	public RetMsg<Serializable> importT24Data(@RequestBody TbkAmount tbkAmount) {
		t24DataTranforService.doT24AdanceMaturityAmtToBalance("/ifbm/transfor/", "/ifbm/transfor_bak/", "@!@", "UTF-8",
				dateService.getDayendDate());
		t24DataTranforService.doT24ScheduleToDataBase("/ifbm/transfor/", "/ifbm/transfor_bak/", "@!@", "UTF-8",
				dateService.getDayendDate());
		return RetMsgHelper.ok();
	}

}
