package com.singlee.capital.data.transfor.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="T24_LD_SCH_DET")
public class T24LdSchDet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dId;//放款编号
	private String dCust;//客户号

	private String drawdownAccount;//放款帐号

	private String prinLiqAcct;//本金还款帐号

	private String prodCat;//门类代码

	private String valueDate;//放款日

	private String maturity;//放款到期日

	private String currentRate;//当前利率
	
	private String ldStatus;//状态

	private String odStatus;//逾期状态

	private String dispDate;//还款日期

	private double amtDue; //本期总还款额

	private double principal;//本金

	private double intAmt;//利息

	private double commission;//佣金

	private double fee;//费用

	private double chg;//手续费

	private double runningBal;//所欠款项

	private String idNo;//放款计划序号

	public String getCurrentRate() {
		return currentRate;
	}

	public void setCurrentRate(String currentRate) {
		this.currentRate = currentRate;
	}

	public String getdId() {
		return dId;
	}

	public void setdId(String dId) {
		this.dId = dId;
	}

	public String getdCust() {
		return dCust;
	}

	public void setdCust(String dCust) {
		this.dCust = dCust;
	}

	public String getDrawdownAccount() {
		return drawdownAccount;
	}

	public void setDrawdownAccount(String drawdownAccount) {
		this.drawdownAccount = drawdownAccount;
	}

	public String getPrinLiqAcct() {
		return prinLiqAcct;
	}

	public void setPrinLiqAcct(String prinLiqAcct) {
		this.prinLiqAcct = prinLiqAcct;
	}

	public String getProdCat() {
		return prodCat;
	}

	public void setProdCat(String prodCat) {
		this.prodCat = prodCat;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getMaturity() {
		return maturity;
	}

	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}

	public String getLdStatus() {
		return ldStatus;
	}

	public void setLdStatus(String ldStatus) {
		this.ldStatus = ldStatus;
	}

	public String getOdStatus() {
		return odStatus;
	}

	public void setOdStatus(String odStatus) {
		this.odStatus = odStatus;
	}

	public String getDispDate() {
		return dispDate;
	}

	public void setDispDate(String dispDate) {
		this.dispDate = dispDate;
	}

	public double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(double amtDue) {
		this.amtDue = amtDue;
	}

	public double getPrincipal() {
		return principal;
	}

	public void setPrincipal(double principal) {
		this.principal = principal;
	}

	public double getIntAmt() {
		return intAmt;
	}

	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getChg() {
		return chg;
	}

	public void setChg(double chg) {
		this.chg = chg;
	}

	public double getRunningBal() {
		return runningBal;
	}

	public void setRunningBal(double runningBal) {
		this.runningBal = runningBal;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	
	

}
