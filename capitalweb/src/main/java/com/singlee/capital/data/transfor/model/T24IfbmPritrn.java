package com.singlee.capital.data.transfor.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TC_T24_IFBM_PRITRN")
public class T24IfbmPritrn implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String ldNo;
	private double ldAmt;
	private String isOd;
	private String subjCode;
	public String getLdNo() {
		return ldNo;
	}
	public void setLdNo(String ldNo) {
		this.ldNo = ldNo;
	}
	public double getLdAmt() {
		return ldAmt;
	}
	public void setLdAmt(double ldAmt) {
		this.ldAmt = ldAmt;
	}
	public String getIsOd() {
		return isOd;
	}
	public void setIsOd(String isOd) {
		this.isOd = isOd;
	}
	public String getSubjCode() {
		return subjCode;
	}
	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}
	
}
