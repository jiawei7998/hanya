package com.singlee.capital.data.transfor.model;

import java.io.Serializable;
import java.util.List;
/**
 * 用于前台配置展示的JAVABEAN
 * @author SINGLEE
 *
 */
public class ProductUdasExcelBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prdNo;//产品代码
	private String prdNm;//产品名称
	
	private List<UdasExcelBean> udasExcelBeans;

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPrdNm() {
		return prdNm;
	}

	public void setPrdNm(String prdNm) {
		this.prdNm = prdNm;
	}

	public List<UdasExcelBean> getUdasExcelBeans() {
		return udasExcelBeans;
	}

	public void setUdasExcelBeans(List<UdasExcelBean> udasExcelBeans) {
		this.udasExcelBeans = udasExcelBeans;
	}
	
	

}
