package com.singlee.capital.data.transfor.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * Transform数据移型使用
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TC_ECIF_UDAS")
public class TcCustEcif implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ecifNo;
	private String nameSt;
	private String nameNt;
	public String getEcifNo() {
		return ecifNo;
	}
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	public String getNameSt() {
		return nameSt;
	}
	public void setNameSt(String nameSt) {
		this.nameSt = nameSt;
	}
	public String getNameNt() {
		return nameNt;
	}
	public void setNameNt(String nameNt) {
		this.nameNt = nameNt;
	}
	
	
	
}
