package com.singlee.capital.data.transfor.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.data.transfor.mapper.T24IfbmInttrnMapper;
import com.singlee.capital.data.transfor.mapper.T24IfbmPritrnMapper;
import com.singlee.capital.data.transfor.mapper.T24LdSchDetMapper;
import com.singlee.capital.data.transfor.model.T24IfbmInttrn;
import com.singlee.capital.data.transfor.model.T24IfbmPritrn;
import com.singlee.capital.data.transfor.model.T24LdSchDet;
import com.singlee.capital.data.transfor.service.T24DataTranforService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.interfacex.model.AcupXmlBean;
import com.singlee.capital.interfacex.model.ReadAcupXmlFactory;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;

@Service("t24DataTranforService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class T24DataTranforServiceImpl implements T24DataTranforService {
	@Autowired
	private TrdTposMapper tposMapper;// 交易持仓
	@Autowired
	private T24IfbmPritrnMapper t24IfbmPritrnMapper;
	@Autowired
	private T24IfbmInttrnMapper t24IfbmInttrnMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private T24LdSchDetMapper t24LdSchDetMapper;
	@Autowired
	private TdCashflowCapitalMapper tdCashflowCapitalMapper;
	@Autowired
	private TdCashflowInterestMapper tdCashflowInterestMapper;
	@Autowired
	private TdAmtRangeMapper tdAmtRangeMapper;// 实际现金流
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	@Autowired
	InstitutionService institutionService;
	@Autowired
	private BookkeepingService bookkeepingService;
//	@Autowired
//	DayendDateService dateService;

	@Override
	public void doT24AdanceMaturityAmtToBalance(String filepath, String filebak, String splitStr, String encoding,
			String postdate) {
		// TODO Auto-generated method stub
		/********** 1 IFBM-PRITRN-迁移日八位日期 **************************************/
//		postdate = dateService.getDayendDate();
		// 解析 retstrList
		TdTrdTpos trdTpos = null;
		T24IfbmInttrn t24IfbmInttrn = null;
		T24IfbmPritrn t24IfbmPritrn = null;
		TdProductApproveMain main = null;
		List<String> retstrList = readT24FileToStringArrays(filepath, filebak, splitStr, encoding, "IFBM-PRITRN-");
		if (null != retstrList && retstrList.size() > 0) {
			for (String tempStr : retstrList) {
				String[] strings = tempStr.split(splitStr);
				try {
					/******************** 保存数据 ***************************/
					String ldNo = strings[0];
					Double ldAmt = strings[2] != null && !"".equals(strings[2]) ? Double.parseDouble(strings[2]) : 0.00;
					String isOd = strings[3] != null && !"".equals(strings[3]) ? strings[3] : "N";
					String subjCode = strings[4].replaceAll("-", "0");
					t24IfbmPritrn = new T24IfbmPritrn();
					t24IfbmPritrn.setLdNo(ldNo);
					t24IfbmPritrn.setLdAmt(ldAmt);
					t24IfbmPritrn.setIsOd(isOd);
					t24IfbmPritrn.setSubjCode(subjCode);
					if (null != this.t24IfbmPritrnMapper.selectByPrimaryKey(t24IfbmPritrn)) {
						this.t24IfbmPritrnMapper.deleteByPrimaryKey(t24IfbmPritrn);
					}
					this.t24IfbmPritrnMapper.insert(t24IfbmPritrn);
					/*********************** 更新头寸 *************************/

					main = new TdProductApproveMain();
					main.setLdNo(ldNo);
					main = tdProductApproveMainMapper.getProductApproveMain(BeanUtil.beanToMap(main));

					if (main != null) {
						trdTpos = new TdTrdTpos();
						trdTpos.setDealNo(main.getDealNo());
						trdTpos.setVersion(0);// 初始版本
						trdTpos.setSettlAmt(ldAmt);
						trdTpos.setConctAmt(main.getAmt());
						trdTpos.setProcAmt(main.getProcAmt());
						if ("Y".equalsIgnoreCase(isOd)) {
							trdTpos.setOverdueDays(1);// 默认1天逾期
						}
						this.tposMapper.insert(trdTpos);// 持久化头寸

						// 调用账务接口
						Map<String, Object> amtMap = new HashMap<String, Object>();
						List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
						TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(ldAmt);
						accTrdDaily.setAccType(DurationConstants.AccType.LOAN_AMT);
						accTrdDaily.setDealNo(main.getDealNo());// 业务正式申请交易单
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(postdate);
						accTrdDaily.setRefNo(main.getRefNo());
						accTrdDailies.add(accTrdDaily);
						amtMap.put(DurationConstants.AccType.LOAN_AMT, ldAmt);
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(ldAmt);
						accTrdDaily.setAccType(DurationConstants.AccType.LOAN_PROCAMT);
						accTrdDaily.setDealNo(main.getDealNo());// 业务正式申请交易单
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(postdate);
						accTrdDaily.setRefNo(main.getRefNo());
						accTrdDailies.add(accTrdDaily);
						amtMap.put(DurationConstants.AccType.LOAN_PROCAMT, ldAmt);
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(ldAmt);
						accTrdDaily.setAccType(DurationConstants.AccType.LOAN_PROCAMT);
						accTrdDaily.setDealNo(main.getDealNo());// 业务正式申请交易单
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(postdate);
						accTrdDaily.setRefNo(main.getRefNo());
						accTrdDailies.add(accTrdDaily);
						amtMap.put(DurationConstants.AccType.LOAN_DISAMT, ldAmt);

						InstructionPackage instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(main));
						instructionPackage.setAmtMap(amtMap);
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if (null != flowId && !"".equals(flowId)) {// 更新回执会计套号 每笔交易
							for (int i = 0; i < accTrdDailies.size(); i++) {
								accTrdDailies.get(i).setAcctNo(flowId);
								// accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
							}
						}
						// 持久化数据
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert",
								accTrdDailies);
					}
				} catch (Exception e) {
					// TODO: handle exception
					// e.printStackTrace();
					throw new RException(tempStr + "导入错误！" + e.getMessage());
				}
			}
		}
		/********* 2 IFBM-INTTRN-迁移日八位日期 **************************************/
		retstrList = readT24FileToStringArrays(filepath, filebak, splitStr, encoding, "IFBM-INTTRN-");
		if (null != retstrList && retstrList.size() > 0) {
			for (String tempStr : retstrList) {
				String[] strings = tempStr.split(splitStr);
				try {
					/******************** 保存数据 ***************************/

					String ldNo = strings[0];
					Double ldIAmt = strings[2] != null && !"".equals(strings[2])
							? Math.abs(Double.parseDouble(strings[2]))
							: 0.00;
					String ldVdate = strings[3] != null && !"".equals(strings[3]) ? strings[3] : "-";
					String ldMdate = strings[4] != null && !"".equals(strings[4]) ? strings[4] : "-";
					String isOd = strings[5] != null && !"".equals(strings[5]) ? strings[5] : "N";
					String subjCode = strings[6] != null && !"".equals(strings[6]) ? strings[6].replace("-", "0") : "N";

					t24IfbmInttrn = new T24IfbmInttrn();
					t24IfbmInttrn.setLdNo(ldNo);
					t24IfbmInttrn.setLdIamt(ldIAmt);
					t24IfbmInttrn.setLdVdate(ldVdate);
					t24IfbmInttrn.setLdMdate(ldMdate);
					t24IfbmInttrn.setIsOd(isOd);
					t24IfbmInttrn.setSubjCode(subjCode);
					if (null != this.t24IfbmInttrnMapper.selectByPrimaryKey(t24IfbmInttrn)) {
						this.t24IfbmInttrnMapper.deleteByPrimaryKey(t24IfbmInttrn);
					}
					this.t24IfbmInttrnMapper.insert(t24IfbmInttrn);
					/*********************** 更新头寸 *************************/

					main = new TdProductApproveMain();
					main.setLdNo(ldNo);
					main = tdProductApproveMainMapper.getProductApproveMain(BeanUtil.beanToMap(main));
					if (main != null) {
						trdTpos = new TdTrdTpos();
						trdTpos.setDealNo(main.getDealNo());
						trdTpos.setVersion(0);// 初始版本
						trdTpos = this.tposMapper.selectByPrimaryKey(trdTpos);
						if (null != trdTpos) {
							trdTpos.setAccruedTint(Math.abs(ldIAmt));
							trdTpos.setActualTint(Math.abs(ldIAmt));
							this.tposMapper.updateByPrimaryKey(trdTpos);// 持久化头寸 利息
						}

						// 调用账务接口
						Map<String, Object> amtMap = new HashMap<String, Object>();
						List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
						TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(ldIAmt);
						accTrdDaily.setAccType(DurationConstants.AccType.TRANSFOR_INT);
						accTrdDaily.setDealNo(main.getDealNo());// 业务正式申请交易单
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(postdate);
						accTrdDaily.setRefNo(main.getRefNo());
						accTrdDailies.add(accTrdDaily);
						amtMap.put(DurationConstants.AccType.TRANSFOR_INT, ldIAmt);

						InstructionPackage instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(main));
						instructionPackage.setAmtMap(amtMap);
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if (null != flowId && !"".equals(flowId)) {// 更新回执会计套号 每笔交易
							for (int i = 0; i < accTrdDailies.size(); i++) {
								accTrdDailies.get(i).setAcctNo(flowId);
								// accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
							}
						}
						// 持久化数据
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert",
								accTrdDailies);
					}
				} catch (Exception e) {
					// TODO: handle exception
//				e.printStackTrace();
					throw new RException(tempStr + "导入错误！" + e.getMessage());
				}
			}
		}
	}

	public String FillUp(String value, String chars, int num) {
		if (value.length() >= num) {
			return value.substring(0, num);
		}
		String fill = "";
		for (int i = 0; i < num - value.length(); i++) {
			fill = fill + chars;
		}
		return fill + value;
	}

	public HashMap<Integer, AcupXmlBean> queryAcupXmlConfig(String XmlPath) throws Exception {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();
		Document xmlDoc = reader.read(new File(XmlPath));
		HashMap<Integer, AcupXmlBean> headerHashMap = new HashMap<Integer, AcupXmlBean>();
		ReadAcupXmlFactory.getElementList(xmlDoc.getRootElement(), headerHashMap);
		return headerHashMap;
	}

	/**
	 * hashMap深拷贝方法
	 * 
	 * @param acupHashMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<Integer, AcupXmlBean> deepCopy(HashMap<Integer, AcupXmlBean> acupHashMap) throws Exception {
		HashMap<Integer, AcupXmlBean> hashMap = new HashMap<Integer, AcupXmlBean>();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(acupHashMap);
		oos.close();
		bos.close();
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		hashMap = (HashMap<Integer, AcupXmlBean>) ois.readObject();
		ois.close();
		bis.close();
		return hashMap;
	}

	@Override
	public List<String> readT24FileToStringArrays(String filepath, String filebak, String splitStr, String encoding,
			final String startWith) {
		// TODO Auto-generated method stub
		BufferedReader br = null;
		List<String> retstrList = null;
		try {
			File file = new File(filepath);
			// 判断文件夹是否存在,如果不存在则创建文件夹
			if (!file.exists()) {
				System.out.println("[readfile]-读文件的文件夹路径不存在：" + filepath);
				return null;
			}
			// 文件过滤
			File[] files = file.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					if (pathname.isDirectory()) {
                        return false;
                    } else {
						String name = pathname.getName();
						if (name.startsWith(startWith)) {
							return true;
						} else {
							return false;
						}
					}
				}
			});
			// 判断文件路径下是否存在符合的文件
			if (files.length <= 0 || null == files) {
				System.out.println("[readfile]-本次扫描路径下不存在文件：" + filepath);
				return null;
			}
			if (files.length > 0) {
				for (File rfile : files) {
					retstrList = new ArrayList<String>();
					br = new BufferedReader(new InputStreamReader(new FileInputStream(rfile), encoding));
					String data = null;
					while ((data = br.readLine()) != null) {
						retstrList.add(data);
					}
					if (br != null) {
						br.close();
					}
					/**
					 * 判断备份的文件路径是否存在
					 */
					if (filebak.length() > 0) {
						File bakFile = new File(filebak + "/" + rfile.getName());
						if (bakFile.isFile()) {
							String bakFileName = bakFile.getAbsoluteFile() + "."
									+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
							System.out.println("存在文件，进行重命名掉！" + bakFileName);
							bakFile.renameTo(new File(bakFileName));
						}
						if (rfile.renameTo(bakFile)) {
							System.out.println("文件备份成功!");
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[readfile]-读文件：" + e.getMessage());
			throw new RException("[readfile]-读文件：" + e.getMessage());
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
//					e.printStackTrace();
			}
		}
		return retstrList;
	}

	@Override
	public void doT24ScheduleToDataBase(String filepath, String filebak, String splitStr, String encoding,
			String postdate) {
		// TODO Auto-generated method stub
//		postdate = dateService.getDayendDate();
		List<String> retstrList = readT24FileToStringArrays(filepath, filebak, splitStr, encoding, "IFBM-SCH-");
		T24LdSchDet t24LdSchDet = null;
		List<T24LdSchDet> t24LdSchDets = null;
		if (retstrList.size() > 0) {
			t24LdSchDets = new ArrayList<T24LdSchDet>();
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			this.t24LdSchDetMapper.deleteAllT24LdSchDets(paramsMap);
			for (String tempStr : retstrList) {
				String[] strings = tempStr.split(splitStr);
				t24LdSchDet = new T24LdSchDet();
				t24LdSchDet.setdId(strings[0]);
				t24LdSchDet.setdCust(strings[1]);
				t24LdSchDet.setDrawdownAccount(strings[2]);
				t24LdSchDet.setPrinLiqAcct(strings[3]);
				t24LdSchDet.setProdCat(strings[4]);
				t24LdSchDet.setValueDate(strings[5]);
				t24LdSchDet.setMaturity(strings[6]);
				t24LdSchDet.setCurrentRate(strings[7] != null && !"".equals(strings[7]) ? strings[7] : "0");
				t24LdSchDet.setLdStatus(strings[8]);
				t24LdSchDet.setOdStatus(strings[9]);
				t24LdSchDet.setDispDate(strings[10]);
				t24LdSchDet.setAmtDue(Double.parseDouble(strings[11]));
				t24LdSchDet.setPrincipal(Double.parseDouble(strings[12]));
				t24LdSchDet.setIntAmt(Double.parseDouble(strings[13]));
				t24LdSchDet.setCommission(Double.parseDouble(strings[14]));
				t24LdSchDet.setFee(Double.parseDouble(strings[15]));
				t24LdSchDet.setChg(Double.parseDouble(strings[16]));
				t24LdSchDet.setRunningBal(Double.parseDouble(strings[17]));
				t24LdSchDet.setIdNo(strings[18]);
				t24LdSchDets.add(t24LdSchDet);
			}
			batchDao.batch("com.singlee.capital.data.transfor.mapper.T24LdSchDetMapper.insert", t24LdSchDets);
		}
		TdAmtRange tdAmtRange = null;
		TdCashflowCapital tdCashflowCapital = null;
		TdCashflowInterest tdCashflowInterest = null;
		if (t24LdSchDets.size() > 0) {
			for (T24LdSchDet tDet : t24LdSchDets) {
				TdProductApproveMain main = new TdProductApproveMain();
				main.setLdNo(tDet.getdId());
				main = tdProductApproveMainMapper.getProductApproveMain(BeanUtil.beanToMap(main));
				if (main != null) {
					System.out.println("----------------------------------" + tDet.getdId());
					// 转换日之前的是 已发生 ，其他是计划
					try {
						if (PlaningTools.compareDate(postdate.replace("-", ""), tDet.getDispDate(), "yyyyMMdd")) {
							// 实际的还本现金流
							if (tDet.getPrincipal() > 0) {
								tdAmtRange = new TdAmtRange();
								tdAmtRange.setDealNo(main.getDealNo());// LD号
								tdAmtRange.setAmtType("1");
								tdAmtRange.setExecAmt(tDet.getPrincipal());
								tdAmtRange.setExecDate(tDet.getDispDate());
								tdAmtRange.setId(tDet.getIdNo());
								tdAmtRange.setRefNo(tDet.getdId());
								tdAmtRange.setRefTable("T24_LD_SCH_DET");
								tdAmtRange.setVersion(0);
								this.tdAmtRangeMapper.insert(tdAmtRange);
							}
						}
						// 全 还本计划
						if (tDet.getPrincipal() > 0) {
							tdCashflowCapital = new TdCashflowCapital();
							tdCashflowCapital.setCashflowId(tDet.getIdNo());
							tdCashflowCapital.setCfEvent("T24");
							tdCashflowCapital.setCfType("Principal");
							tdCashflowCapital.setDealNo(main.getDealNo());
							tdCashflowCapital.setPayDirection("Recieve");
							tdCashflowCapital.setRepaymentSamt(tDet.getPrincipal());
							tdCashflowCapital.setRepaymentSdate(DateTimeUtil.standardDate(tDet.getDispDate()));
							if (PlaningTools.compareDate(postdate.replace("-", ""), tDet.getDispDate(), "yyyyMMdd")) {
								tdCashflowCapital.setRepaymentTamt(tDet.getPrincipal());
								tdCashflowCapital.setRepaymentTdate(DateTimeUtil.standardDate(tDet.getDispDate()));
							}
							tdCashflowCapital.setSeqNumber(Integer.parseInt(tDet.getIdNo()));
							tdCashflowCapital.setVersion(0);
							this.tdCashflowCapitalMapper.insert(tdCashflowCapital);
						}
						// 全 利息计划
						if (tDet.getIntAmt() > 0) {
							tdCashflowInterest = new TdCashflowInterest();
							tdCashflowInterest.setSeqNumber(Integer.parseInt(tDet.getIdNo()));
							tdCashflowInterest.setDealNo(main.getDealNo());
							List<CashflowInterest> list = tdCashflowInterestMapper
									.getInterestList(BeanUtil.beanToMap(tdCashflowInterest));
							if (list.size() <= 0) {
								tdCashflowInterest = new TdCashflowInterest();
								if (PlaningTools.compareDate(postdate.replace("-", ""), tDet.getDispDate(),
										"yyyyMMdd")) {
									tdCashflowInterest.setActualDate(DateTimeUtil.standardDate(tDet.getDispDate()));
									tdCashflowInterest.setActualRamt(tDet.getIntAmt());
								}
								tdCashflowInterest.setCashflowId(tDet.getIdNo());
								tdCashflowInterest.setCfType("T24");
								tdCashflowInterest.setDayCounter("Actual/360");
								tdCashflowInterest.setDealNo(main.getDealNo());
								tdCashflowInterest.setExecuteRate(Double.parseDouble(tDet.getCurrentRate()));
								tdCashflowInterest.setInterestAmt(tDet.getIntAmt());
								tdCashflowInterest.setPayDirection("Recieve");
								// tdCashflowInterest.setRefBeginDate(tDet.getDispDate());
								tdCashflowInterest.setRefEndDate(DateTimeUtil.standardDate(tDet.getDispDate()));
								tdCashflowInterest.setTheoryPaymentDate(DateTimeUtil.standardDate(tDet.getDispDate()));
								tdCashflowInterest.setSeqNumber(Integer.parseInt(tDet.getIdNo()));
								tdCashflowInterest.setVersion(0);
								this.tdCashflowInterestMapper.insert(tdCashflowInterest);
							}
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
						try {
							throw new RException(BeanUtils.describe(tDet).toString() + "-异常!");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							// e1.printStackTrace();
						}
					}
				} else {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(tDet.getdId());
				}
			}

		}
	}

}
