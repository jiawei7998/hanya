package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachTree;
import com.singlee.capital.base.model.TcAttachTreeNodeVo;
import com.singlee.capital.base.model.TcAttachTreeTemplate;
import com.singlee.capital.base.service.AttachTreeService;
import com.singlee.capital.base.service.AttachTreeTemplateService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

/**
 * @projectName 同业业务管理系统
 * @className 附件树控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:39:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/AttachTreeController")
public class AttachTreeController extends CommonController {
	
	@Autowired
	private AttachTreeService attachTreeService;
	
	@Autowired
	private AttachTreeTemplateService attachTreeTemplateService;
	
	@ResponseBody
	@RequestMapping(value = "/searchAttachTreeList")
	public RetMsg<PageInfo<TcAttachTree>> searchAttachTreeList(@RequestBody Map<String, String> params) throws RException {
		Page<TcAttachTree> page = attachTreeService.searchAttachTree(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchAttachTree")
	public RetMsg<List<TcAttachTreeTemplate>> searchAttachTree(@RequestBody Map<String, String> params) throws RException {
		List<TcAttachTreeTemplate> list = attachTreeTemplateService.searchAttachTreeTemplate(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/createAttachTree")
	public RetMsg<Serializable> createAttachTree(@RequestBody TcAttachTree attachTree) {
		attachTreeService.createAttachTree(attachTree);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/editAttachTree")
	public RetMsg<Serializable> editAttachTree(@RequestBody Map<String, String> params){
		attachTreeService.updateAttachTree(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/removeAttachTree")
	public RetMsg<Serializable> removeAttachTree(@RequestBody String[] treeIds) {
		attachTreeService.deleteAttachTree(treeIds);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/createAttachTreeNode")
	public RetMsg<TcAttachTreeTemplate> createAttachTreeNode(HttpServletRequest request, HttpServletResponse response) throws RException {
		String parentId = ParameterUtil.getString(request.getParameterMap(), "parentId", null);
		TcAttachTreeTemplate attachTreeTemplate = attachTreeTemplateService.createAttachTreeNode(parentId);
		return RetMsgHelper.ok(attachTreeTemplate);
	}

	@ResponseBody
	@RequestMapping(value = "/editAttachTreeNode")
	public RetMsg<TcAttachTreeTemplate> editAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		String text = ParameterUtil.getString(request.getParameterMap(), "text", null);
		TcAttachTreeTemplate attachTreeTemplate = attachTreeTemplateService.updateAttachTreeNode(id, text);
		return RetMsgHelper.ok(attachTreeTemplate);
	}
	
	@ResponseBody
	@RequestMapping(value = "/destoryAttachTreeNode")
	public RetMsg<Serializable> removeAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		attachTreeTemplateService.deleteAttachTreeNode(id);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/dndAttachTreeNode")
	public RetMsg<Serializable> dndAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		String targetId = ParameterUtil.getString(request.getParameterMap(), "targetId", null);
		String point = ParameterUtil.getString(request.getParameterMap(), "point", null);
		attachTreeTemplateService.dndAttachTreeNode(id, targetId, point);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询可选节点与附件树（可关联附件树）
	 */
	@ResponseBody
	@RequestMapping(value = "/getTcAttachTreeNodeVo")
	public RetMsg<PageInfo<TcAttachTreeNodeVo>> getTcAttachTreeNodeVo(@RequestBody Map<String, String> params) throws RException {
		Page<TcAttachTreeNodeVo> page =  attachTreeService.getTcAttachTreeNodeVo(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
}
