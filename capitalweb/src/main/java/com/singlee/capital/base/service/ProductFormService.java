package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.model.TcProductForm;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品表单映射服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-30 下午5:45:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface ProductFormService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品字段分组映射对象列表
	 * @author Hunter
	 * @date 2016-8-30
	 */
	public List<TcProductForm> getProductFormList (Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品字段分组映射对象列表
	 * @author Hunter
	 * @date 2016-8-30
	 */
	public List<TcForm> getFormList (Map<String, Object> params);
	
}
