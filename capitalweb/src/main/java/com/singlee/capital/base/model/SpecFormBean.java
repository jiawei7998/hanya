package com.singlee.capital.base.model;

import java.io.Serializable;

/**
 * 配置自定义表单根据环节显示不同的栏位
 * @author SINGLEE
 *
 */
public class SpecFormBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String formStatus;
	private String fldNo;
	public String getFormStatus() {
		return formStatus;
	}
	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}
	public String getFldNo() {
		return fldNo;
	}
	public void setFldNo(String fldNo) {
		this.fldNo = fldNo;
	}
	
}
