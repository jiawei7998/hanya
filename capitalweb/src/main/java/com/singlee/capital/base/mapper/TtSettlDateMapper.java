package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TtSettlDate;

import tk.mybatis.mapper.common.Mapper;

public interface TtSettlDateMapper extends Mapper<TtSettlDate>{
	
	public Page<TtSettlDate> TtSettlDatePage(Map<String, Object> param,RowBounds rowBounds);
	
	public List<TtSettlDate> TtSettlDatePage(Map<String, Object> param);
	
	public List<TtSettlDate> TtSettlDatePageFromDict(Map<String, Object> param);
	
	public void deleteAll();

}
