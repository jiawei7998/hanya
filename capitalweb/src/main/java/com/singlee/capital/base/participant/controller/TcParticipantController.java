package com.singlee.capital.base.participant.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcParticipant;
import com.singlee.capital.base.participant.service.TcParticipantService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping("/TcParticipantController")
public class TcParticipantController extends CommonController {
	@Autowired
	TcParticipantService tcParticipantService;
	/**
	 * 展示列表数据
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParticipantList")
	public RetMsg<PageInfo<TcParticipant>> searchParticipantList(@RequestBody Map<String, Object> params) {
		Page<TcParticipant> page = tcParticipantService.getParticipantList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增参与方类型
	 * @param tcParticipant
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addType")
	public RetMsg<Serializable> addType(@RequestBody TcParticipant tcParticipant) {
		tcParticipantService.addParticipant(tcParticipant);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除参与方类型
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteType")
	public RetMsg<Serializable> deleteParticipant(@RequestBody Map<String, Object> map) {
		tcParticipantService.deleteParticipant(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新参与方类型
	 * @param tcParticipant
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateType")
	public RetMsg<Serializable> updateType(@RequestBody TcParticipant tcParticipant) {
		tcParticipantService.updateParticipant(tcParticipant);
		return RetMsgHelper.ok();
	}
}
