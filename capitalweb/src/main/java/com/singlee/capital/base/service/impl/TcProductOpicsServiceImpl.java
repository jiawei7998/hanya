package com.singlee.capital.base.service.impl;

import com.singlee.capital.base.mapper.TcProductOpicsMapper;
import com.singlee.capital.base.model.TcProductOpics;
import com.singlee.capital.base.service.TcProductOpicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class TcProductOpicsServiceImpl implements TcProductOpicsService {

    @Autowired
    private TcProductOpicsMapper tcProductOpicsMapper;


    @Override
    public TcProductOpics selectByPrdCode(String fprdCode) {
        return tcProductOpicsMapper.selectByPrdCode(fprdCode);
    }
}
