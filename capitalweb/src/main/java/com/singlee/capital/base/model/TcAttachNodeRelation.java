package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;
/**
 * 节点树关系表
 * @author carey
 *
 */
@Entity
@Table(name = "TC_ATTACH_RELATION")
public class TcAttachNodeRelation  implements Serializable, TreeInterface{
	
	private static final long serialVersionUID = 1L;
	
	private String attachId;
	private String parId;
	private String mark;
	private Integer deleteFlag;
	@Transient
	private String nodeName; 
	@Transient
	private String treeName;
	public String getAttachId() {
		return attachId;
	}
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	public String getParId() {
		return parId;
	}
	public void setParId(String parId) {
		this.parId = parId;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public Integer getDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getParentId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void children(List<? extends TreeInterface> list) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<? extends TreeInterface> children() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int getSort() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String getIconCls() {
		return "";
	}
}
