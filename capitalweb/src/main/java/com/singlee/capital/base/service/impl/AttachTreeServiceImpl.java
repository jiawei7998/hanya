package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcAttachTreeMapper;
import com.singlee.capital.base.mapper.TcAttachTreeTemplateMapper;
import com.singlee.capital.base.model.TcAttachTree;
import com.singlee.capital.base.model.TcAttachTreeNodeVo;
import com.singlee.capital.base.service.AttachTreeService;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * @className 附件树服务
 * @description TODO
 * @author Lyon
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AttachTreeServiceImpl implements AttachTreeService {

	@Autowired
	private TcAttachTreeMapper attachTreeMapper;

	@Autowired
	private TcAttachTreeTemplateMapper attachTreeTemplateMapper;
	
	@Override
	public Page<TcAttachTree> searchAttachTree(Map<String, String> params,RowBounds rb) {
		return attachTreeMapper.selectTcAttachTree(params,rb);
	}

	@Override
	public void createAttachTree(TcAttachTree attachTree) {
		if (Integer.valueOf(attachTree.getRefType()) > 100) {
			Map<String, String> params = new HashMap<String, String>();
			params.put("refType", attachTree.getRefType());
			Integer i = attachTreeMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raise("非自定业务的附件树只允许定义一次");
			}
		}
		TaUser user = SlSessionHelper.getUser();
		attachTree.setCreateUserId(user.getUserId());
		attachTree.setCreateUserName(user.getUserName());
		attachTree.setCreateInstId(user.getInstId());
		attachTree.setCreateInstName(user.getInstName());
		attachTree.setCreateTime(DateUtil.getCurrentDateTimeAsString());
		attachTreeMapper.insert(attachTree);
		
		attachTreeTemplateMapper.updateTcAttachTreeTemplateByIdCascade(attachTree.getTreeId(), attachTree.getRefType());
	}

	@Override
	public void updateAttachTree(Map<String, String> params) {
		String refType = ParameterUtil.getString(params, "refType", "");
		if (Integer.valueOf(refType) > 100) {
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("refType", refType);
			Integer i = attachTreeMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raise("非自定业务的附件树只允许定义一次");
			}
		}
		TaUser user = SlSessionHelper.getUser();
		params.put("updateUserId", user.getUserId());
		params.put("updateUserName", user.getUserName());
		params.put("updateInstId", user.getInstId());
		params.put("updateInstName", user.getInstName());
		params.put("updateTime", DateUtil.getCurrentDateTimeAsString());
		attachTreeMapper.updateByMap(params);
		
		attachTreeTemplateMapper.updateTcAttachTreeTemplateByIdCascade(ParameterUtil.getString(params, "treeId", ""), ParameterUtil.getString(params, "refType", "0"));
	}

	@Override
	public void deleteAttachTree(String[] attachTreeIds) {
		for (int i = 0; i < attachTreeIds.length; i++) {
			attachTreeMapper.deleteByPrimaryKey(attachTreeIds[i]);
		}
	}

	@Override
	public Page<TcAttachTreeNodeVo> getTcAttachTreeNodeVo(
			Map<String, String> params, RowBounds rb) {
		return attachTreeMapper.getTcAttachTreeNodeVo(params,rb);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
