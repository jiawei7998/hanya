package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.capital.base.model.TcAttachTreeTemplate;
import com.singlee.capital.common.spring.mybatis.MyMapper;


/**
 * 
 * @author x230i
 *
 */
public interface TcAttachTreeTemplateMapper extends MyMapper<TcAttachTreeTemplate> {
	
	/**
	 * 根据指定的关联类型 和关联 id 获得附件列表 
	 * @param params
	 * @return
	 */
	public List<TcAttachTreeTemplate> selectTcAttachTreeTemplate(Map<String, String> params);
	
	/**
	 * 根据ID进行树的级联更新
	 * @param treeId
	 * @param refType
	 */
	public void updateTcAttachTreeTemplateByIdCascade(@Param("treeId") String treeId, @Param("refType") String refType);
	
	/**
	 * 根据ID进行树的级联删除
	 * @param treeId
	 */
	public int deleteTcAttachTreeTemplateByIdCascade(@Param("treeId") String treeId);
	
	/**
	 * 清除没有关联类型的默认根节点 
	 * @return
	 */
	public int clearTcAttachTreeTemplate();
}