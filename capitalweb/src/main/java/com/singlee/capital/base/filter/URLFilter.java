package com.singlee.capital.base.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.system.model.TaModule;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.ModuleService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
/**
 * 原理是，将所有的地址中包含JSP的访问拦截；
 */
public class URLFilter implements Filter{

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterch) throws IOException, ServletException {
        // TODO Auto-generated method stub
        HttpServletRequest httpreq = (HttpServletRequest) request;
        //HttpServletResponse httpres = (HttpServletResponse) response;
        String url = httpreq.getRequestURL().toString();
        url = url.substring(url.lastIndexOf("/")+1,StringUtils.contains(url, "?")?url.lastIndexOf("?"):url.length());
        if(url.indexOf("jsp") > 0 && url.indexOf("index_approve.jsp")<0&& url.indexOf("login_")<0)   //判断地址中是否包含"JSP"
        {
        	WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(httpreq.getSession().getServletContext()); 
        	ModuleService moduleService =  webApplicationContext.getBean("moduleServiceImpl",ModuleService.class);
        	List<TaModule> modules = moduleService.getTotalPageUrls();
        	boolean allBoolean = false;
        	for(TaModule module:modules){
        		if(StringUtils.contains(StringUtils.trimToNull(module.getModuleUrl()), url.toString())){
        			allBoolean = true;
    				break;
    			}
        	}
        	if(allBoolean){
	        	//判定权限
	        	TaUser user = SlSessionHelper.getUser(SessionStaticUtil.getSlSessionByHttp(httpreq));
	        	if(null != user){
		        	
		    		List<TaModule> page = moduleService.getMenuUrlsByUserId(user.getUserId());
		    		boolean flag = false;
		    		for(TaModule taModule : page){
		    			if(StringUtils.contains(StringUtils.trimToNull(taModule.getModuleUrl()), url.toString())){
		    				flag = true;
		    				break;
		    			}
		    		}
		    		if(flag) {
						filterch.doFilter(request, response);
					} else{
		    			//httpres.sendRedirect(SystemProperties.iamgate_httpUrl);20180103
		    			filterch.doFilter(request, response);
		    		}
	        	}else {
	        		filterch.doFilter(request, response); 
				}
        	}else{
        		filterch.doFilter(request, response); 
        	}
        }else{ //不包含JSP，则继续执行
            filterch.doFilter(request, response);   
        }
        
    }
    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // TODO Auto-generated method stub
        
    }

}