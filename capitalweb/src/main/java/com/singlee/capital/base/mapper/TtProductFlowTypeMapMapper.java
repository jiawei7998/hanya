package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.model.TtProductFlowTypeMap;

public interface TtProductFlowTypeMapMapper extends Mapper<TtProductFlowTypeMap> {
	
	List<TtProductFlowTypeMap> getFlowTypeByProductType(Map<String, Object> map);

	void removeFlowTypeByProductType(Map<String, Object> map);
	
}