package com.singlee.capital.base.xml.tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.common.exception.RException;

public class XmlFormat {
	public static String format(String str) throws Exception {
		StringWriter out = null;
		StringReader in = null;
		XMLWriter writer = null;
		try {
			SAXReader reader = new SAXReader();
			// System.out.println(reader);
			// 注释：创建一个串的字符输入流
			in = new StringReader(str);
			Document doc = reader.read(in);
			// System.out.println(doc.getRootElement());
			// 注释：创建输出格式
			OutputFormat formater = OutputFormat.createPrettyPrint();
			// formater=OutputFormat.createCompactFormat();
			// 注释：设置xml的输出编码
			formater.setEncoding("utf-8");
			// 注释：创建输出(目标)
			out = new StringWriter();
			// 注释：创建输出流
			writer = new XMLWriter(out, formater);
			// 注释：输出格式化的串到目标中，执行后。格式化后的串保存在out中。
			writer.write(doc);

			writer.close();
			out.close();
			in.close();
//		    System.out.println(out.toString());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("XmlFormat.format(String str)：" + e.getMessage());
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("XmlFormat.format(String str)：" + e.getMessage());
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				throw new RException(e);
			}
		}

		// 注释：返回我们格式化后的结果
		return out.toString();
	}

	public static String returnMsg(String reponsexml) throws Exception {
		try {
			int pos = reponsexml.indexOf("<?xml version");
			reponsexml = reponsexml.substring(pos);
			if (reponsexml.contains(
					" xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"")) {
				reponsexml = reponsexml.replace(
						" xmlns=\"http://www.bankofshanghai.com/BOSFX/2010/08\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bankofshanghai.com/BOSFX/2010/08 BOSFX2.0.xsd\"",
						"");
			}

		} catch (Exception e) {
			// TODO: handle exception
			LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("XmlFormat.returnMsg(String str)：" + e.getMessage());
		}

//		System.out.println("TExCurrencyAllInqRs反馈报文："+XmlFormat.format(reponsexml));
		return XmlFormat.format(reponsexml);
	}

	public static String convertToXml(Object obj) {

		return null;
	}
}
