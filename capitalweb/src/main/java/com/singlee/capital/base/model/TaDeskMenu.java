package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="TA_DESK_MENU")
public class TaDeskMenu implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 工作台ID
	 */
	private String deskId;
	/**
	 * 工作台名称
	 */
	private String deskName;
	/**
	 * 工作台图标
	 */
	private String deskIcon;
	/**
	 * 银行归属ID
	 */
	private String branchId;
	/**
	 * URL地址
	 */
	private String deskUrl;
	/**
	 * 排序规则
	 * @return
	 */
	private Integer deskSort;
	/**
	 * 是否启用标志
	 */
	private String isActive;
	public String getDeskId() {
		return deskId;
	}
	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}
	public String getDeskName() {
		return deskName;
	}
	public void setDeskName(String deskName) {
		this.deskName = deskName;
	}
	public String getDeskIcon() {
		return deskIcon;
	}
	public void setDeskIcon(String deskIcon) {
		this.deskIcon = deskIcon;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getDeskUrl() {
		return deskUrl;
	}
	public void setDeskUrl(String deskUrl) {
		this.deskUrl = deskUrl;
	}
	public Integer getDeskSort() {
		return deskSort;
	}
	public void setDeskSort(Integer deskSort) {
		this.deskSort = deskSort;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	

}
