package com.singlee.capital.base.controller;

import com.singlee.capital.base.model.TcProductOpics;
import com.singlee.capital.base.service.TcProductOpicsService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 根据产品编号查询Opics要素
 */
@Controller
@RequestMapping(value = "/TcProductOpicsController")
public class TcProductOpicsController extends CommonController {

    @Autowired
    private TcProductOpicsService tcProductOpicsService;


    @ResponseBody
    @RequestMapping(value = "/selectByPrdCode")
    public RetMsg<TcProductOpics> selectByPrdCode(@RequestBody Map<String, Object> params) {
        String fprdCode = ParameterUtil.getString(params, "fprdCode", "");
        TcProductOpics tcProductOpics = tcProductOpicsService.selectByPrdCode(fprdCode);
        return RetMsgHelper.ok(tcProductOpics);
    }


}
