package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 附件表 
 * 
 * @author x230i
 *
 */
@Entity
@Table(name = "TC_ATTACH")
public class TcAttach implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_ATTACH.NEXTVAL FROM DUAL")
	private String attachId ;//	N	VARCHAR2(32)	N			附件ID
	private String refType;//REF_TYPE	N	VARCHAR2(20)	Y			所属关联类型
	private String refId;//REF_ID	N	VARCHAR2(32)	Y			所属关联编号
	private String fieldName;//FIELD_NAME	N	VARCHAR2(32)	Y			关联字段名
	private String contentType;//CONTENT_TYPE	N	VARCHAR2(32)	N			附件内容类型
	private int contentSize;//CONTENT_SIZE	N	NUMBER	N			附件尺寸
	private String attachName;//ATTACH_NAME	N	VARCHAR2(256)	N			附件名称
	private String attachStatus;//ATTACH_STATUS	N	VARCHAR2(10)	N			附件状态
	private String attachType;//ATTACH_TYPE	N	VARCHAR2(10)	Y			附件类型（BLOB/还是路径方式）
	private String attachPath;//ATTACH_PATH	N	VARCHAR2(256)	Y			路径
	private byte[] attachBlob;//ATTACH_BLOB	N	BLOB	Y			
	private String createUserId;
	private String createUserName;
	private String createInstId;
	private String createInstName;
	private String createTime;
	private String updateUserId;
	private String updateUserName;
	private String updateInstId;
	private String updateInstName;
	private String updateTime;
	
	public String getAttachId() {
		return attachId;
	}
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentSize() {
		return contentSize;
	}
	public void setContentSize(int contentSize) {
		this.contentSize = contentSize;
	}
	public String getAttachName() {
		return attachName;
	}
	public void setAttachName(String attachName) {
		this.attachName = attachName;
	}
	public String getAttachStatus() {
		return attachStatus;
	}
	public void setAttachStatus(String attachStatus) {
		this.attachStatus = attachStatus;
	}
	public String getAttachType() {
		return attachType;
	}
	public void setAttachType(String attachType) {
		this.attachType = attachType;
	}
	public String getAttachPath() {
		return attachPath;
	}
	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateInstId() {
		return createInstId;
	}
	public void setCreateInstId(String createInstId) {
		this.createInstId = createInstId;
	}
	public String getCreateInstName() {
		return createInstName;
	}
	public void setCreateInstName(String createInstName) {
		this.createInstName = createInstName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	public String getUpdateInstId() {
		return updateInstId;
	}
	public void setUpdateInstId(String updateInstId) {
		this.updateInstId = updateInstId;
	}
	public String getUpdateInstName() {
		return updateInstName;
	}
	public void setUpdateInstName(String updateInstName) {
		this.updateInstName = updateInstName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public byte[] getAttachBlob() {
		return attachBlob;
	}
	public void setAttachBlob(byte[] attachBlob) {
		this.attachBlob = attachBlob;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TcAttach [attachId=");
		builder.append(attachId);
		builder.append(", refType=");
		builder.append(refType);
		builder.append(", refId=");
		builder.append(refId);
		builder.append(", contentType=");
		builder.append(contentType);
		builder.append(", attachName=");
		builder.append(attachName);
		builder.append(", attachStatus=");
		builder.append(attachStatus);
		builder.append("]");
		return builder.toString();
	}
	
	


}
