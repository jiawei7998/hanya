package com.singlee.capital.base.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaDeskMenu;
import com.singlee.capital.base.model.vo.MenuDeskTopVo;
import com.singlee.capital.base.model.vo.MenuVo;
import com.singlee.capital.base.model.vo.TaDeskMenuVo;
import com.singlee.capital.base.model.vo.TreeMenuVo;
import com.singlee.capital.base.service.DeskMenuService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
/**
 * 银行归属台面菜单
 * @author SINGLEE
 *
 */
@Controller
@RequestMapping(value = "/deskMenuController")
public class DeskMenuController {
	@Autowired
	DeskMenuService deskMenuService;

	/**
	 * 按照银行归属ID获得其台面菜单
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getDeskMenus")
	public RetMsg<?> getDeskMenus(@RequestBody Map<String,Object> params){
		String roleIdStr = ParameterUtil.getString(params, "roleIds", null);
		params.put("roleIds",JSON.parseArray(roleIdStr,String.class));
		List<TaDeskMenuVo> deskMenus = deskMenuService.getAllTaDeskMenuByIds(params);
		return RetMsgHelper.ok(deskMenus);
	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getDeskMenuByBranchIdAndDeskId")
	public RetMsg<TaDeskMenu> getDeskMenuByBranchIdAndDeskId(@RequestBody Map<String,Object> params){
		TaDeskMenu deskMenu = deskMenuService.getDeskMenuByBranchIdAndDeskId(params);
		return RetMsgHelper.ok(deskMenu);
	}
	/**
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateDeskBranchId")
	public RetMsg<Serializable> updateDeskBranchId(@RequestBody Map<String,Object> params){
		 deskMenuService.updateDeskBranchId(params);
		return RetMsgHelper.ok();
	}
	 

	/**
	 * 关联ta_module获得子资源
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getDeskMenuPage")
	public RetMsg<?> getDeskMenuPage(@RequestBody Map<String,Object> params){
		String roleIdStr = ParameterUtil.getString(params, "roleIds", null);
		params.put("roleIds",JSON.parseArray(roleIdStr,String.class));
		List<MenuDeskTopVo> deskMenuPage = deskMenuService.getAllMenuDeskTopVos(params);
		return RetMsgHelper.ok(deskMenuPage);
	}
	@ResponseBody
	@RequestMapping(value = "/getAllTreeMenuVos")
	public RetMsg<?> getAllTreeMenuVos(@RequestBody Map<String,Object> params){
		List<TreeMenuVo> deskMenuPage = deskMenuService.getAllTreeMenuVos(params);
		return RetMsgHelper.ok(deskMenuPage);
	}
	
	/**
	 * 关联ta_module获得子资源
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllModuleWhitOutDeskId")
	public RetMsg<?> getAllModuleWhitOutDeskId(@RequestBody Map<String,Object> params){
		List<MenuVo> deskMenuPage = deskMenuService.getAllModuleWhitOutDeskId(params);
		return RetMsgHelper.ok(deskMenuPage);
	}
	
	/**
	 * 根据角色Id 机构Id 查询菜单台子
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getDeskMenuByRoleIdAndInstId")
	public RetMsg<PageInfo<TaDeskMenu>> getDeskMenuByRoleIdAndInstId(@RequestBody Map<String,Object> params){
		Page<TaDeskMenu> list =deskMenuService.getDeskMenuByRoleIdAndInstId(params);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 根据银行ID 查询所拥有的台子
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllMenuByBranchId")
	RetMsg<PageInfo<TaDeskMenu>> getAllMenuByBranchId(@RequestBody Map<String,Object> params){
		Page<TaDeskMenu> list =deskMenuService.getAllMenuByBranchId( params);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/saveDeskByBranchId")
	public RetMsg<Serializable> saveDeskByBranchId(@RequestBody Map<String,Object> params){
		 deskMenuService.saveDeskByBranchId(params);
		return RetMsgHelper.ok();
	}
	/**
	 * 
	 * 关联tc_product获得交易资源
	 */
	@ResponseBody
	@RequestMapping(value = "/getDeskTradeMenuPage")
	public RetMsg<?> getDeskTradeMenuPage(@RequestBody Map<String,Object> params){
		List<MenuDeskTopVo> deskMenuPage = deskMenuService.getDeskTradeMenuPage(params);
		return RetMsgHelper.ok(deskMenuPage);
	}
	
	
}
