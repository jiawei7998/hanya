package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;
/**
 * @projectName 同业业务管理系统
 * @className 自定义产品控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午11:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/ProductController")
public class ProductController extends CommonController {
	
	@Autowired
	private ProductService productService;
	
	@Resource
	private DictionaryGetService dictionaryGetService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProduct")
	public RetMsg<PageInfo<TcProduct>> searchPageProduct(@RequestBody Map<String,Object> params){
		Page<TcProduct> page = productService.getProductPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增
	 * 
	 * @param product - 产品对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/addProduct")
	public RetMsg<Serializable> addProduct(@RequestBody TcProduct product) {
		productService.createProduct(product);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * 
	 * @param product - 产品对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/editProduct")
	public RetMsg<Serializable> editProduct(@RequestBody TcProduct product) {
		productService.updateProduct(product);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 * 
	 * @param prdNos - 产品主键集合
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/removeProduct")
	public RetMsg<Serializable> removeProduct(@RequestBody String[] prdNos) {
		productService.deleteProduct(prdNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 根据ID查询单个明细
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author wang
	 * @date 2016-8-12
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductById")
	public RetMsg<TcProduct> searchProductById(@RequestBody Map<String,Object> params){
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		TcProduct product = productService.getProductById(prdNo);
		return RetMsgHelper.ok(product);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductFormFieldsetField")
	public RetMsg<List<TcFieldset>> searchProductFormFieldsetField(@RequestBody Map<String,Object> params){
		List<TcFieldset> formFieldsets = productService.getProductFormFieldsetField(params);
		return RetMsgHelper.ok(formFieldsets);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getParticipantFormFieldsetField")
	public RetMsg<List<TcFieldset>> getParticipantFormFieldsetField(@RequestBody Map<String,Object> params){
		List<TcFieldset> formFieldsets = productService.getParticipantFormFieldsetField(params);
		return RetMsgHelper.ok(formFieldsets);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPageProductNotChoose")
	public RetMsg<PageInfo<TcProduct>> getPageProductNotChoose(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(productService.getPageProductNotChoose(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPageProductChoose")
	public RetMsg<List<TcProduct>> getPageProductChoose(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(productService.getPageProductChoose(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchProduct")
	public RetMsg<List<TcProduct>> getProduct(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(productService.searchProduct(params));
	}
	
	
	/**
	 *查询所有产品及扩展产品 
	 *@author lijing
	 *@since 20180122
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getProductAndExt")
	public RetMsg<List<TcProduct>> getProductAndExt(@RequestBody Map<String,Object> params) {
		return RetMsgHelper.ok(productService.getAllProductAndExt(params));
	}
	
	
	/**
     *获取该客户某个业务剩余的占用额度和已经释放的额度
     
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getBalance")
    public RetMsg<List<HrbCreditCustProd>> getBalance(@RequestBody Map<String,Object> params) {
        return RetMsgHelper.ok(productService.getBalance(params));
    }
    
	
	
	
	
	
	
	
	/**
	 * 条件查询列表 转换为HTML
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductFormFieldsetFieldToHtml")
	public RetMsg<Serializable> searchProductFormFieldsetFieldToHtml(HttpServletRequest request,@RequestBody Map<String,Object> params){
		List<TcFieldset> formFieldsets = productService.getProductFormFieldsetField(params);
		String ret = null;
		try {
			ret = parseForm(request,formFieldsets, Double.valueOf(params.get("textAreaWidth").toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return RetMsgHelper.ok("succ",ret);
	}
	
	private String parseForm(HttpServletRequest request,List<TcFieldset> fieldsets,double textAreaWidth) throws Exception{
		StringBuffer container = new StringBuffer();
		TcFieldset fieldset = null;
		String tr = null;
		int fix = 0;
		StringBuffer content = null;
		
		SlSession slSession = SessionStaticUtil.getSlSessionByHttp(request);
		//当前session登陆用户
		TaUser sessionUser = SlSessionHelper.getUser(slSession)==null?new TaUser():SlSessionHelper.getUser(slSession);
		//当前session登陆用户的机构
		TtInstitution sessionInstitution = SlSessionHelper.getInstitution(slSession)==null?new TtInstitution():SlSessionHelper.getInstitution(slSession);
		//系统日期
		String bizDate = DayendDateServiceProxy.getInstance().getSettlementDate();
	
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("_bizDate", bizDate);
		valueMap.put("_userId", StringUtils.trimToEmpty(sessionUser.getUserId()));
		valueMap.put("_userName", StringUtils.trimToEmpty(sessionUser.getUserName()));
		valueMap.put("_instId", StringUtils.trimToEmpty(sessionUser.getInstId()));
		valueMap.put("_instName", StringUtils.trimToEmpty(sessionInstitution.getInstName()));
		valueMap.put("_userFixedphone", StringUtils.trimToEmpty(sessionUser.getUserFixedphone()));
		valueMap.put("_userCellphone", StringUtils.trimToEmpty(sessionUser.getUserCellphone()));
		
		//String id = null;
		for (int i = 0; i < fieldsets.size(); i++) 
		{
			fieldset = fieldsets.get(i);
			tr = "";
			fix = 0; 
			content = new StringBuffer(); 
			if(fieldset.getAtachFormid() != null  && !"".equals(fieldset.getAtachFormid())){
				content.append("<fieldset><legend>" + fieldset.getFdsName() + "</legend>");
				content.append("</fieldset><br/>");
				container.append(content);
				
			}else if (fieldset.getFields().size() > 0) 
			{
				if(fieldset.getCustFlag() != null && "1".equals(fieldset.getCustFlag())){
					content.append("<fieldset><legend>" + fieldset.getFdsName() + "</legend><table style='width:100%'><input style='width:70%;' type='hidden' id="+fieldset.getFdsNo()+"_"+fieldset.getCustFlag()+" name="+fieldset.getFdsNo()+"_"+fieldset.getCustFlag()+" value="+fieldset.getCustFlag()+"/>");
				
				}else{
					content.append("<fieldset id=\"fieldset"+ i +"\" name=\"fieldset"+ i +"\"><legend>" + fieldset.getFdsName() + "</legend><table style='width:100%'>");
				}
				TcField field = null;
				String tempWidth = "180";
				String componets = "";
				for (int j = 0; j < fieldset.getFields().size(); j++) 
				{
					field = fieldset.getFields().get(j);
					if("componet".equals(field.getFormType()))
					{
						componets += "<input id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-textbox\" visible=\"false\"/> ";
						fix -= 1;
						continue;
					}
					
					if ((j + fix)%2 == 0) {//第一列需要加入行开始符 
						tr = "<tr>";
					}
					
					if (field.getFormCols() != null && field.getFormCols() == 2 && (j + fix)%2 == 1) {//占位两列且当前为第二列的需要换行
						tr += "</tr><tr>";
					} else if (field.getFormCols() != null && field.getFormCols() == 2 && (j + fix)%2 == 0) {
						fix += 1;//当出现占两列的字段时，会影响后续字段的位置，+1进行修正
					}
					tr += "<td id='" + field.getFldName() + "_label' width=\"15%\" valign=\"middle\" ";
					if ("1".equals(field.getIsMandatory())) {
						tr += " class='red-star' ";
					}
					if ("0".equals(field.getFldNo())) {
						tr += "></td>";
					} else {
						tr += ">" + field.getFormName() + "</td>";
					}
					
					if(field.getFormType() != null && "hidden".equals(field.getFormType())){
						tr += "<td id='" + field.getFldName() + "_td'><input style='width:70%;' id='" + field.getFldName() + "' name='" + field.getFldName() + "' type=\"" + field.getFormType() +"\" value=\"" + field.getDefValue() + "\"/></td>";
						
					}else if("textbox".equals(field.getFormType())){
						tempWidth = "180";
						if (field.getFormCols() != null && field.getFormCols() == 2) {
							tr += "<td id='" + field.getFldName() + "_td' colspan='3'><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-textbox\" required=\"" + ("1".equals(field.getIsMandatory())  ? "true" : "false") + "\" ";
							tempWidth = (textAreaWidth > 0 ? textAreaWidth : 750) + "";
						} else {
							tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-textbox\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\" ";
						}
						tr += " height=\"" + (field.getFormRows() == null ? 1 : field.getFormRows()) * 22 + "\" width=\"" + tempWidth + "\" ";
						
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								//tr += " value=\"eval('" + field.getDefValue() + "')\"";
								tr += " value=\"" + valueMap.get(field.getDefValue()) + "\"";
								
							} else {
								tr += " value=\"" + field.getDefValue() + "\"";
							}
						}
						
						if (field.getDefText() != null) {
							if (field.getDefText().indexOf("_") > -1) {
								tr += " text=\"" + valueMap.get(field.getDefText()) + "\"";
								
							} else {
								tr += " text=\"" + field.getDefText() + "\"";
							}
						}
						
						if (field.getFormLength() != null && field.getFormLength() > 0) {
							tr += " maxLength = \"" + field.getFormLength() + "\" ";
						}
						if (field.getFormValid() != null && !"".equals(field.getFormValid())) {
							tr += " onvalidation=\"CommonUtil.onValidation(e,'" + field.getFormValid() + "',["+field.getValidParam()+"])\" allowLimitValue=\"false\" format=\"#,0.00\" allowLimitValue=\"false\" ";  
						}
						
						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())){
							tr += " enabled=\"false\" ";
						}
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : ("<span id='" + field.getFldName() + "_span'></span>")) + "</td>";
					
					}else if("buttonedit".equals(field.getFormType())){
						tempWidth = "180";
						if (field.getFormCols() != null && field.getFormCols() == 2) {
							tr += "<td id='" + field.getFldName() + "_td' colspan='3'><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-buttonedit\" iconCls=\"icon-search\" required=\"" + ("1".equals(field.getIsMandatory())  ? "true" : "false") + "\" ";
							tempWidth = (textAreaWidth > 0 ? textAreaWidth : 750) + "";
						} else {
							tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-buttonedit\" iconCls=\"icon-search\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\" ";
						}
						tr += " allowInput=\"false\" ";
						
						tr += " height=\"" + (field.getFormRows() == null ? 1 : field.getFormRows()) * 22 + "\" width=\"" + tempWidth + "\" ";
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								tr += " value=\"" + valueMap.get(field.getDefValue()) + "\"";
								
							} else {
								tr += " value=\"" + field.getDefValue() + "\"";
							}
						}
						
						if (field.getDefText() != null) {
							if (field.getDefText().indexOf("_") > -1) {
								tr += " text=\"" + valueMap.get(field.getDefText()) + "\"";
								
							} else {
								tr += " text=\"" + field.getDefText() + "\"";
							}
						}
						
						if(field.getComponet() != null){
							tr += " onbuttonclick=\"" + field.getComponet() + "\"";
						}
						
						if (field.getFormLength() != null && field.getFormLength() > 0) {
							tr += " maxLength = \"" + field.getFormLength() + "\" ";
						}
						
						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())){
							tr += " enabled=\"false\" ";
						}
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : "") + "</td>";
						
					}else if("numberbox".equals(field.getFormType())){
						
						tempWidth = "180";
						if (field.getFormCols() != null && field.getFormCols() == 2) {
							tr += "<td id='" + field.getFldName() + "_td' colspan='3'><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-spinner\" changeOnMousewheel=\"false\" required=\"" + ("1".equals(field.getIsMandatory())  ? "true" : "false") + "\" ";
							//tr += ", width:" + (textAreaWidth > 0 ? textAreaWidth : 750);
							tempWidth = (textAreaWidth > 0 ? textAreaWidth : 750) + "";
						} else {
							tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id=\"" + field.getFldName() + "\" name=\"" + field.getFldName() + "\" class=\"mini-spinner\" changeOnMousewheel=\"false\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\" ";
						}
						tr += " height=\"" + (field.getFormRows() == null ? 1 : field.getFormRows()) * 22 + "\" width=\"" + tempWidth + "\" ";
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								//tr += " value=\"eval('" + field.getDefValue() + "')\"";
								tr += " value=\"" + valueMap.get(field.getDefValue()) + "\"";
							} else {
								tr += " value=\"" + field.getDefValue() + "\"";
							}
						}
						
						if (field.getFormLength() != null && field.getFormLength() > 0) {
							tr += " maxLength = \"" + field.getFormLength() + "\" ";
						}
						
						String max = "999999999999";
						if (field.getFormMax() != null) {
							max = field.getFormMax();
						}
						
						String min = "-99999999999";
						if (field.getFormMin() != null) {
							min = field.getFormMin();
						}
						
						tr += " onvalidation=\"CommonUtil.onValidation(e,'numberRange',[" + min + "," + max + "])\" allowLimitValue=\"false\" "; 
						if(field.getFormTimes() != null){
							tr += " data-options=\"{formTimes:'"+field.getFormTimes()+"'}\" ";
							if(field.getFormTimes().compareTo(new BigDecimal(100)) == 0)
							{
								tr += " format=\"p4\" ";
							}
							
						}else{
							tr += " format=\"#,0.00\" "; 
							
						}

						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())){
							tr += " enabled=\"false\" ";
						}
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : "") + "</td>";
						
					}else if("datebox".equals(field.getFormType())){
						
						tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id='" + field.getFldName() + "' name='" + field.getFldName() + "' class=\"mini-datepicker\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\"" + " height=\"200\" width = \"180\" editable = \"false\"";
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								//tr += " value = eval('" + field.getDefValue() + "')";
								tr += " value=\"" + valueMap.get(field.getDefValue()) + "\"";
							} else {
								tr += " value =\"" + field.getDefValue() + "\"";
							}
						}
						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())) {
							tr += " enabled=\"false\" ";
						}
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : "") + "</td>";
						
					}else if("combobox".equals(field.getFormType())){
						tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id='" + field.getFldName() + "' name='" + field.getFldName() + "' class=\"mini-combobox\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\" width=\"180\" ";
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								tr += " data = \"\"  value=\"" + valueMap.get(field.getDefValue()) + "\"";
								
							} else {
								//data = "[{'id':'A0000','text':'农、林、牧、渔业'}]"
								String text = "";
								try {
									TaDictVo dictVo = getTaDictByCodeAndKey(field.getDefKey(), field.getDefValue());
									if(dictVo != null){
										text = dictVo.getDict_value();
									}
								} catch (Exception e) {
									text = "";
								}
								
								tr += " data = \"[{'id':'"+field.getDefValue()+"','text':'"+text+"'}]\"  value = \"" + field.getDefValue() + "\" ";
							}
						}
						if (field.getIsDisabled() != null &&  "1".equals(field.getIsDisabled())) {
							tr += " enabled=\"false\" ";
						}
						
						if (field.getDefUrl() != null && field.getDefUrl().length() > 0) {
							tr += " url = \"" + field.getDefUrl() + "\" ";
						} else {
							//data-options="{dict:'CommonUtil.serverData.dictionary.GBT4754_2011',dictStatus:'init'}"
							tr += " onclick=\"CommonUtil.onclickOption({type:'1',obj:this})\" data-options=\"{dict:'"+field.getDefKey()+"',dictStatus:'init'}\" ";
						}
						tr += " showNullItem=\"true\"  allowInput=\"false\" emptyText=\"请选择...\" ";
						
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : "") + "</td>";
						
					}else if("combotree".equals(field.getFormType())){
						tr += "<td id='" + field.getFldName() + "_td' width=\"35%\"><input style='width:70%;' id='" + field.getFldName() + "' name='" + field.getFldName() + "' class=\"mini-treeselect\" required=\"" + ("1".equals(field.getIsMandatory()) ? "true" : "false") + "\" width=\"180\"";
						if (field.getDefValue() != null) {
							if (field.getDefValue().indexOf("_") > -1) {
								tr += " value=\"" + valueMap.get(field.getDefValue()) + "\"";
								
							} else {
								tr += " value = \"" + field.getDefValue() + "\" ";
							}
						}
						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())) {
							tr += " enabled=\"false\" ";
						}
						
						if (field.getDefUrl() != null) {
							tr += " url = \"" + field.getDefUrl() + "\" ";
							
						} else {
							//tr += ", dict:'" + field.getDefKey() + "'";
							tr += " data = \"CommonUtil.serverData.dictionary." + field.getDefKey() + "\" ";
							/*if (eval("CommonUtil.serverData." + field.getDefKey())) {
								tr += ", data:CommonUtil.serverData." + field.getDefKey();
							} else {
								
							}*/
						}
						tr += " />" + (field.getFormTips() != null &&  field.getFormTips().length() > 0 ? ("<span id='" + field.getFldName() + "_span' data-tooltip=\"" + field.getFormTips() + "\" data-placement=\"top\">...</span>") : "") + "</td>";
						
					}else if("componet".equals(field.getFormType())){
//						tr += "<td><a id='" + field.getFldName() + "' class='easyui-sllinkbutton' data-options=\"plain:false,iconCls:'icon-search'";
//						if (field.getIsDisabled() != null && "1".equals(field.getIsDisabled())) {
//							tr += ", disabled:true";
//						}
//						//tr += "\" data-json=\"" + $.toJSON(field).replace(/"/g, "'") + "\">" + field.formName + "管理</a>" + (field.formTips ? "&nbsp;<span class='tips'>" + field.formTips : "</span>") + "</td>";
//					    tr += "\" data-json=\"" + JSONObject.toJSONString(field,SerializerFeature.WriteMapNullValue).replaceAll("\"", "'") + "\">" + field.getFormName() + "管理</a>" + (field.getFormTips() != null && field.getFormTips().length() > 0 ? "&nbsp;<span class='tips'>" + field.getFormTips() : "</span>") + "</td>";
						//tr += "<td width=\"35%\"></td>";
						//tr = "";
						
					}else if("emptybox".equals(field.getFormType())){
						tr += "<td width=\"35%\"></td>";
						
					}
					
					if ((j + fix)%2 == 0 && j == fieldset.getFields().size() - 1) {//第一列且最后一个属性加入结束符
						tr += "<td></td><td></td></tr>";
						content.append(tr);
					} else {
						if ((j + fix)%2 == 0 && field.getFormCols() != null && field.getFormCols() == 2) {//第一列且占两列的需要换行 
							tr += "</tr>";
							content.append(tr);
						} else if ((j + fix) % 2 == 1) {//第二列需要加入行结束符
							tr += "</tr>";
							content.append(tr);
						}
					}
				}// end for
				content.append(componets);
				content.append("</table></fieldset><br/>");
				container.append(content);
			} // end if
		}// end for
		System.out.println("-----------------------------------------------"+container.toString());
		return container.toString();
	}
	
	private TaDictVo getTaDictByCodeAndKey(String code,String key){
		List<TaDictVo> list = dictionaryGetService.getDictToEhCache();
		List<TaDictVo> relist = new ArrayList<TaDictVo>();
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getDict_id().equals(code) && list.get(i).getDict_key().equals(key)){
				relist.add(list.get(i));
			}
		}
		return relist.get(0);
	}
	
}
