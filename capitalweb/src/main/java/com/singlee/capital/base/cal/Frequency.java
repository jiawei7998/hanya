package com.singlee.capital.base.cal;

import java.util.GregorianCalendar;

/**
 * 频率
 * @author SINGLEE
 *
 */
public enum  Frequency {

	MONTH(1,GregorianCalendar.MONTH,2),//月
	YEAR(1,GregorianCalendar.YEAR,1),//年
	HALFYEAR(6,GregorianCalendar.MONTH,3),//半年
	ONCE(1,0,4),//利随本清
    CUSTOM(0,0,5),//自定义
    QUARTER(3,GregorianCalendar.MONTH,6),//季度
    DAY(1,GregorianCalendar.DATE,7);//天
    

    private int pace ;//步伐
    private int calendarType;//GregorianCalendar 类型
    private int value;

    private Frequency( int pace,int calendarType,int value) {
    	
    	this.pace = pace;

        this.calendarType = calendarType;
        
        this.value = value;

    }

	public int getPace() {
		return pace;
	}

	public void setPace(int pace) {
		this.pace = pace;
	}

	public int getCalendarType() {
		return calendarType;
	}

	public void setCalendarType(int calendarType) {
		this.calendarType = calendarType;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
    
	public static Frequency valueOf(int type) {    //    手写的从int到enum的转换函数
        switch (type) {
        case 12:
            return MONTH;
        case 1:
            return YEAR;
        case 2:
            return HALFYEAR;
        case 0:
        	return ONCE;
        case 5:
        	return CUSTOM;
        case 4:
        	return QUARTER;
        case 365:
        	return DAY;
        default:
        	return null;
        	
        }
    }
    
}
