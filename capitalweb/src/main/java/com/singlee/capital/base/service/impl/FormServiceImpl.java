package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcFormFieldsetFieldMapper;
import com.singlee.capital.base.mapper.TcFormFieldsetMapper;
import com.singlee.capital.base.mapper.TcFormMapper;
import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.model.TcFormFieldset;
import com.singlee.capital.base.model.TcFormFieldsetField;
import com.singlee.capital.base.service.FormService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FormServiceImpl implements FormService {
	
	@Autowired
	private TcFormMapper formMapper;
	@Autowired
	private TcFormFieldsetMapper formFieldsetMapper;
	@Autowired
	private TcFormFieldsetFieldMapper formFieldsetFieldMapper;
	
	@Override
	public TcForm getFormById(String formNo) {
		return formMapper.getFormById(formNo);
	}

	@Override
	public Page<TcForm> getFormPage(Map<String, Object> params) {
		return formMapper.getFormList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	@AutoLogMethod(value="创建自定义表单")
	public synchronized void createForm(TcForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		Integer i = 0;
		//判断表单名称是否存在
		if (StringUtil.isNotEmpty(form.getFormName())) {
			params.put("formName", form.getFormName());
			i = formMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("表单名称已存在！");
			}
		}
		form.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		//保存表单
		formMapper.insert(form);
		//保存表单关联信息
		addFormAssociation(form);
	}

	@Override
	@AutoLogMethod(value="修改自定义表单")
	public synchronized void updateForm(TcForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		Integer i = 0;
		//判断表单名称是否存在
		if (StringUtil.isNotEmpty(form.getFormName())) {
			params.put("formNo", form.getFormNo());
			params.put("formName", form.getFormName());
			i = formMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("表单名称已存在！");
			}
		}
		form.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		//更新菜单信息
		formMapper.updateByPrimaryKey(form);
		//更新表单字段
		removeFormAssociation(form.getFormNo());
		addFormAssociation(form);
	}

	@Override
	@AutoLogMethod(value="删除自定义表单")
	public void deleteForm(String[] formNos) {
		for (int i = 0; i < formNos.length; i++) {
			formMapper.deleteByPrimaryKey(formNos[i]);
			removeFormAssociation(formNos[i]);
		}
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	private void removeFormAssociation(String formNo) {
		//删除原有绑定数据
		formFieldsetMapper.deleteFormFieldsetByFormId(formNo);
		formFieldsetFieldMapper.deleteFormFieldsetFieldByFormId(formNo);
	}
	
	private void addFormAssociation(TcForm form) {
		//新增新绑定数据
		//预定义字段
		List<TcFormFieldset> predefinedFieldsets = form.getPredefinedFieldsets();
		if(predefinedFieldsets != null) {
			for (TcFormFieldset formFieldset : predefinedFieldsets) {
				formFieldset.setFormNo(form.getFormNo());
				formFieldsetMapper.insert(formFieldset);
				List<TcFormFieldsetField> formFieldsetFields = formFieldset.getFormFieldsetFields();
				for (TcFormFieldsetField formFieldsetField : formFieldsetFields) {
					formFieldsetField.setFormNo(form.getFormNo());
					formFieldsetField.setFdsNo(formFieldset.getFdsNo());
					formFieldsetFieldMapper.insert(formFieldsetField);
				}
			}
		}
		//自定义字段
		List<TcFormFieldset> customizedFieldsets = form.getCustomizedFieldsets();
		if(customizedFieldsets != null) {
			for (TcFormFieldset formFieldset : customizedFieldsets) {
				formFieldset.setFormNo(form.getFormNo());
				formFieldsetMapper.insert(formFieldset);
				List<TcFormFieldsetField> formFieldsetFields = formFieldset.getFormFieldsetFields();
				for (TcFormFieldsetField formFieldsetField : formFieldsetFields) {
					formFieldsetField.setFormNo(form.getFormNo());
					formFieldsetField.setFdsNo(formFieldset.getFdsNo());
					formFieldsetFieldMapper.insert(formFieldsetField);
				}
			}
		}
	}
}
