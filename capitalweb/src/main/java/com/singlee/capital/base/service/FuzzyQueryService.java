package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.counterparty.model.CounterPartyFuzzyVo;
import com.singlee.capital.counterparty.model.PayBank;
import com.singlee.capital.market.model.TtMktBondIssuer;

/**
 * 模糊查询业务层
 * @author gm
 *
 */
public interface FuzzyQueryService {

	/**
	 * 支付行号 模糊查询
	 * @param map
	 * 	bk_name：
	 *  limit：显示数量
	 * @return
	 */
	public List<TtMktBondIssuer> selectBondIssuer(String bk_name,int limit);

	public List<PayBank> selectPayBank(Map<String, Object> map);
	/**
	 * 交易对手模糊查询
	 * @param map
	 * 	party_name:交易对手名称
	 *  limit：显示记录数
	 *  hasCashAcc:是否关联资金账户
	 * @return
	 */
	public List<CounterPartyFuzzyVo> selectCounterParty(String party_name,int limit,boolean hasCashAcc);

}
