package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.mapper.TcAttachTreeRefMapper;
import com.singlee.capital.base.mapper.TcAttachTreeRefVoMapper;
import com.singlee.capital.base.model.TcAttachTreeFile;
import com.singlee.capital.base.model.TcAttachTreeRef;
import com.singlee.capital.base.service.TcAttachTreeFileService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.controller.CommonController;

/**
 * @projectName 同业业务管理系统
 * @className 附件树控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:39:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/TcAttachTreeFileController")
public class TcAttachTreeFileController extends CommonController {
	
	@Resource
	TcAttachTreeFileService TcAttachTreeFileService;
	@Resource
	TcAttachTreeRefMapper tcAttachTreeRefMapper;
	@Resource
	TcAttachTreeRefVoMapper tcAttachTreeRefVoMapper;
	
	@ResponseBody
	@RequestMapping(value = "/insertTcAttachTreeFile")
	public RetMsg<Serializable> insertTcAttachTreeFile(@RequestBody Map<String,String> ref) {
		try {
			String jsonStr = ref.get("selectArrToDB").toString();
			String prdNo = ref.get("prdNo").toString();
			if(!"".equals(jsonStr)){
				List<TcAttachTreeFile> list =FastJsonUtil.parseArrays(jsonStr, TcAttachTreeFile.class);
				if(!"".equals(prdNo)){
					TcAttachTreeRef  treeRef= tcAttachTreeRefMapper.getTcAttachTreeRefByPrd(ref);
					if(treeRef!=null){
						ref.put("treeId",treeRef.getTreeId());
						List<TcAttachTreeFile>  listref= tcAttachTreeRefVoMapper.getTcAttachTreeRefVo(ref);
						for (int i = 0; i < listref.size(); i++) {
							TcAttachTreeFile file = new TcAttachTreeFile();
							BeanUtil.copyNotEmptyProperties(file,listref.get(i));
							if(list.size() > 0){
								file.setRefId(list.get(i).getRefId());
								file.setRefName(list.get(i).getRefId());
							}
							list.add(file);
						}
						Map<String,Object> map = new HashMap<String, Object>();
						map.put("refId", ref.get("refId"));
						TcAttachTreeFileService.deleteTcAttachTreeFileByDealNo(map);
						TcAttachTreeFileService.insertTcAttachTreeFilebatch(list);
					}
				}

			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteTcAttachTreeFef")
	public RetMsg<Serializable> deleteTcAttachTreeFef(@RequestBody TcAttachTreeFile ref) throws RException {
		TcAttachTreeFileService.deleteTcAttachTreeFef(ref);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTcAttachTreeFile")
	public RetMsg<TcAttachTreeFile> getTcAttachTreeFile(@RequestBody TcAttachTreeFile ref) throws RException {
		TcAttachTreeFile list = TcAttachTreeFileService.getTcAttachTreeFileById(ref);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTcAttachTreeFile")
	public RetMsg<List<TcAttachTreeFile>> searchTcAttachTreeFile(@RequestBody Map<String,Object> ref) throws RException {
		List<TcAttachTreeFile> list = TcAttachTreeFileService.searchTcAttachTreeFile(ref);
		return RetMsgHelper.ok(list);
	}
	
}
