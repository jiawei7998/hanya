package com.singlee.capital.base.participant.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.mapper.TcProductParticipantMapper;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.participant.service.TcProductParticipantService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
@Service
public class TcProductParticipantServiceImpl implements
		TcProductParticipantService {
	@Autowired
	TcProductParticipantMapper tcProductParticipantMapper;
	@Override
	public Page<TcProductParticipant> getProductParticipantList(
			Map<String, Object> params) {
		return tcProductParticipantMapper.getProductParticipantList(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteProductParticipant(Map<String, Object> map) throws RException {
		List<TcProductParticipant> productParticipantList = tcProductParticipantMapper.getProductParticipantByPrdNoAndPartyId(map);
		if(productParticipantList == null || productParticipantList.isEmpty()) {
			JY.raiseRException("关系不存在");
		}
		tcProductParticipantMapper.deleteByPrdNoAndPartyId(map);
		// 将排序关系之后的排序减1
		TcProductParticipant productParticipant = productParticipantList.get(0);
		Map<String, Object> sortMap = new HashMap<String, Object>();
		sortMap.put("prdNo", productParticipant.getPrdNo());
		sortMap.put("sortNo", productParticipant.getSortNo());
		tcProductParticipantMapper.updateSortByPrdNoAndSort(sortMap);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addProductParticipant(TcProductParticipant tcProductParticipant) throws RException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prdNo", tcProductParticipant.getPrdNo());
		map.put("partyId", tcProductParticipant.getPartyId());
		map.put("sort", tcProductParticipant.getSortNo());
		List<TcProductParticipant> tcProductParticipantList = tcProductParticipantMapper.getProductParticipantByPrdNoAndPartyId(map);
		if(!tcProductParticipantList.isEmpty()) {
			JY.raiseRException("关系已存在");
		}
		List<TcProductParticipant> tcProductParticipants = tcProductParticipantMapper.getProductParticipantBySort(map);
		if(!tcProductParticipants.isEmpty()) {
			JY.raiseRException("排序顺序已存在");
		}
		tcProductParticipantMapper.addProductParticipant(tcProductParticipant);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateProductParticipant(TcProductParticipant tcProductParticipant) throws RException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prdNo", tcProductParticipant.getPrdNo());
		map.put("partyId", tcProductParticipant.getPartyId());
		map.put("sort", tcProductParticipant.getSortNo());
		List<TcProductParticipant> tcProductParticipantList = tcProductParticipantMapper.getProductParticipantByPrdNoAndPartyId(map);
		if(tcProductParticipantList == null || tcProductParticipantList.isEmpty()) {
			JY.raiseRException("关系不存在");
		}
		tcProductParticipantMapper.deleteByPrdNoAndPartyId(map);
		List<TcProductParticipant> tcProductParticipants = tcProductParticipantMapper.getProductParticipantBySort(map);
		if(!tcProductParticipants.isEmpty()) {
			JY.raiseRException("排序顺序已存在");
		}
		if("是".equals(tcProductParticipant.getJudgeCounterparty())) {
			tcProductParticipant.setJudgeCounterparty(DictConstants.YesNo.YES);
		}
		if("否".equals(tcProductParticipant.getJudgeCounterparty())) {
			tcProductParticipant.setJudgeCounterparty(DictConstants.YesNo.NO);
		}
		tcProductParticipantMapper.addProductParticipant(tcProductParticipant);
	}

	@Override
	public List<TcProductParticipant> getProductParticipantCounterparty(Map<String, Object> map) {
		return tcProductParticipantMapper.getProductParticipantCounterparty(map);
	}
}
