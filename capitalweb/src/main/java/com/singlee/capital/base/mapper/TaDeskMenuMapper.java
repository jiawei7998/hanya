package com.singlee.capital.base.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaDeskMenu;
import com.singlee.capital.base.model.vo.MenuVo;
import com.singlee.capital.system.model.TaRoleDeskMap;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaDeskMenuMapper extends Mapper<TaDeskMenu>{
	/**
	 * 获得工作台的列表
	 * @return
	 */
	List<TaDeskMenu> getAllTaDeskMenuByIds(Map<String, Object> paramsMap);
	Integer getDeskMaxSort(Map<String, Object> paramsMap);
	void saveDeskByBranchId(TaDeskMenu taDeskMenu);
	String getDeskMax(Map<String, Object> paramsMap);
	void updateDeskBranchId(Map<String, Object> map);
	public TaDeskMenu getDeskMenuByBranchIdAndDeskId(Map<String, Object> paramsMap);
	/**
	 * 获得交易台的菜单
	 * @param branchId
	 * @return
	 */
	List<MenuVo> getTradeMenuByDeskId(Map<String, Object> paramsMap);
	
	List<MenuVo> getTradeModulesByPid(String modulePid);
	List<MenuVo> getTradeModulesByPid2(String modulePid);
	/**
	 * 根据用户、机构、角色、行号筛选出子菜单
	 * 1.当以上条件加上deskId时：则查询当前台子一级子菜单
	 * 2.当以上条件不加上deskId时：则查询出当前所有module_url is not 的有效资源
	 * 
	 * @param paramsMap
	 * @return
	 */
	List<MenuVo> getModulesByIdsAndDeskId(Map<String, Object> paramsMap);
	
	List<MenuVo> getModulesByPid(Map<String, Object> paramsMap);
	Page<TaDeskMenu> getDeskMenuByRoleIdAndInstId(Map<String, Object> map,RowBounds rb);
	Page<TaDeskMenu> getAllMenuByBranchId(Map<String, Object> map,RowBounds rb);
	
	//插入角色台子表
	void insertTaRoleDeskMap(TaRoleDeskMap taRoleDeskMap);

	/**
	 * 查询菜单与角色的关联关系
	 * @param paramsMap
	 * @return
	 */
	List<String> getTaRoleByModuleId(Map<String, Object> paramsMap);

}
