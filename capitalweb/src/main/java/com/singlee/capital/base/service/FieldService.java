package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;

/**
 * @projectName 同业业务管理系统
 * @className 自定义字段服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface FieldService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param fldNo - 字段主键
	 * @return 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public TcField getFieldById(String fldNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public Page<TcField> getFieldPage (Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public List<TcField> getFieldList (Map<String, Object> params);
	
	/**
	 * 新增
	 * 
	 * @param field - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void createField(TcField field);
	
	/**
	 * 修改
	 * 
	 * @param field - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void updateField(Map<String,Object> params);
	
	/**
	 * 删除
	 * 
	 * @param fieldNoArray - 字段主键列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void deleteField(String[] fldNos);
	
	public List<TcField> getChoosedFieldListForPrdNo(Map<String, Object> map);
}
