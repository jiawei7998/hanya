package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcProductFormMapper;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.base.mapper.TcProductTypeMapper;
import com.singlee.capital.base.model.SpecFormBean;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.model.TcProductForm;
import com.singlee.capital.base.model.TcProductType;
import com.singlee.capital.base.service.FieldsetService;
import com.singlee.capital.base.service.ProductFormService;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbextra.credit.mapper.HrbCreditLimitOccupyMapper;
import com.singlee.hrbextra.credit.mapper.HrbCreditPrdRelationMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.slbpm.mapper.ProcTaskFormMapper;
import com.singlee.slbpm.pojo.bo.ProcTaskForm;
import com.singlee.slbpm.service.FlowQueryService;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductServiceImpl implements ProductService {

	@Autowired
	private TcProductMapper productMapper;
	@Autowired
	private TcProductTypeMapper productTypeMapper;
	@Autowired
	private TcProductFormMapper productFormMapper;
	@Autowired
	private ProductFormService productFormService;
	@Autowired
	private FieldsetService fieldsetService;
	@Autowired
	private ProcTaskFormMapper procTaskFormMapper;
	@Autowired
	private FlowQueryService flowQueryService;

	@Autowired
	private HrbCreditPrdRelationMapper hrbCreditPrdRelationMapper;

	@Autowired
	private HrbCreditLimitOccupyMapper hrbCreditLimitOccupyMapper;

	@Override
	public TcProduct getProductById(String prdNo) {
		return productMapper.getProductById(prdNo);
	}

	@Override
	public Page<TcProduct> getProductPage(Map<String, Object> params) {
		return productMapper.getProductList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TcProduct> getProductList(Map<String, Object> params) {
		return productMapper.getProductList(params);
	}

	@Override
	public List<TcFieldset> getProductFormFieldsetField(Map<String, Object> params) {
		String formType = ParameterUtil.getString(params, "formType", "");
		String fdsType = ParameterUtil.getString(params, "fdsType", "");
		TcProductForm productForm = null;
		List<TcFieldset> formFieldsets = null;
		List<TcProductForm> productForms = productFormService.getProductFormList(params);
		for (TcProductForm tcProductForm : productForms) {
			if (formType.equals(tcProductForm.getForm().getFormType())) {
				productForm = tcProductForm;
			}
		}
		boolean approveAndTraderType = true;
		/**
		 * 增加 审批和核实表单是否一致的数据字典项 approveAndTraderType=true;
		 */
		if (approveAndTraderType && productForms.size() > 0) {
			productForm = productForms.get(0);
		}
		if (productForm != null) {
			HashMap<String, Object> param = new HashMap<String, Object>();

			// edit by wangchen on 2017/7/13
			// 新增 查询当前用户对应审批角色
			String type = ParameterUtil.getString(params, "type", "");
			String userId = ParameterUtil.getString(params, "userId", "");
			String task_def_key = "";
			Map<String, String> map = new HashMap<String, String>();
			if (("approve".equals(type) || "detail".equals(type)) && !userId.equals(SlSessionHelper.getUserId())) {
				String taskId = ParameterUtil.getString(params, "taskId", "");
				task_def_key = flowQueryService.getTaskKeyByTaskId(taskId);
				String prdNo = ParameterUtil.getString(params, "prdNo", "");
				map = flowQueryService
						.getFlowIdAndInstanceIdBySerialNo(ParameterUtil.getString(params, "serialNo", ""));
				if (map != null) {
					String flow_id = ParameterUtil.getString(map, "flow_id", "");
					try {
						ProcTaskForm procTaskForm = flowQueryService.getUserTaskForm(flow_id, task_def_key, prdNo);
						JY.require(procTaskForm != null, "匹配自定义表单失败,将使用默认表单设置.");
						param.put("formNo", procTaskForm.getForm_no());
					} catch (Exception e) {
						// e.printStackTrace();
						param.put("formNo", productForm.getFormNo());
						// throw new RException(e);
					}
				} else {
					param.put("formNo", productForm.getFormNo());
				}
			} else {
				param.put("formNo", productForm.getFormNo());
			}
			// end

			param.put("fdsType", fdsType);
			formFieldsets = fieldsetService.getFormFieldsetList(param);
			/**
			 * 进入筛选器，把需要隐藏的栏位全部设置为hidden，把需要编辑的栏位全部打上特殊标记 20170803
			 */
			String serialNo = ParameterUtil.getString(params, "serialNo", "");
			if (StringUtils.isNotEmpty(serialNo) && StringUtils.isNotBlank(serialNo) && map != null) {
				// 表明在流程中
				// TtFlowSerialMap ttFlowSerialMap =
				// flowQueryService.getFlowSerialMap(serialNo);
				/**
				 * 查询流程人工任务节点绑定的自定义表单
				 * 
				 * @param map flow_id 流程定义ID task_def_key 流程任务节点KEY prd_code 产品编号
				 * @return
				 */
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				paramsMap.put("flow_id", ParameterUtil.getString(map, "flow_id", ""));
				paramsMap.put("task_def_key", task_def_key);
				paramsMap.put("prd_code", ParameterUtil.getString(params, "prdNo", ""));
				List<ProcTaskForm> procTaskForms = procTaskFormMapper.getProcTaskForm(paramsMap);
				String resultJson = "";
				if (procTaskForms.size() > 0) {// 表示配置了自定义表单栏位配置
					for (ProcTaskForm procTaskForm : procTaskForms) {
						if (StringUtils.isNotEmpty(procTaskForm.getForm_fields_json())
								&& StringUtils.isNotBlank(procTaskForm.getForm_fields_json())) {
							resultJson = procTaskForm.getForm_fields_json();
							break;
						}
					}
				}
				if (StringUtils.isNotEmpty(resultJson) && StringUtils.isNotBlank(resultJson)) {
					// 将JSON数据转换为 List<SpecFormBean>
					List<SpecFormBean> specFormBeans = FastJsonUtil.parseArrays(resultJson, SpecFormBean.class);
					// 轮询 formFieldsets
					List<TcField> fields = null;
					for (TcFieldset fieldset : formFieldsets) {
						fields = fieldset.getFields();// 得到fieldset下的域
						for (TcField tcField : fields)// 循环栏位
						{
							for (SpecFormBean specFormBean : specFormBeans) {
								if (StringUtils.equals(tcField.getFldNo(), specFormBean.getFldNo())) {
									if (StringUtils.equals(specFormBean.getFormStatus(),
											DurationConstants.FORM_STATUS.HIDDEN)) {
										tcField.setFormType("hidden");
									}
									if (StringUtils.equals(specFormBean.getFormStatus(),
											DurationConstants.FORM_STATUS.WRITABLE)) {
										tcField.setFormStatus(specFormBean.getFormStatus());// 可写
									}
								}
							}
						}

					}
				}
			}
		} else {
			JY.raiseRException("产品未设置对应表单");
		}
		return formFieldsets;
	}

	/**
	 * 20170517 获得参与方的配置信息
	 */
	@Override
	public List<TcFieldset> getParticipantFormFieldsetField(Map<String, Object> params) {
		List<TcFieldset> formFieldsets = fieldsetService.getFormPartyFieldsetList(params);
		return formFieldsets;
	}

	@Override
	@AutoLogMethod(value = "创建自定义产品")
	public synchronized void createProduct(TcProduct product) {
		Map<String, Object> params = new HashMap<String, Object>();
		Integer i = 0;
		// 判断产品名称是否存在
		if (StringUtil.isNotEmpty(product.getPrdName())) {
			params.put("prdName", product.getPrdName());
			i = productMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("产品名称已存在！");
			}
		}
		// 计算菜单顺序号（逻辑待优化）
		params.clear();
		params.put("prdType", product.getPrdType());
		params.put("sort", "MODULE_SORTNO");
		params.put("order", "DESC");

		List<TcProduct> products = productMapper.getProductList(params);
		TcProduct p = null;
		if (products != null && products.size() != 0) {
			p = products.get(0);
		}
		Integer order = 1;
		if (p != null) {
			order += p.getModuleSortno();
		}
		// 保存产品
		productMapper.insert(product);// 预先生成prdNo
		// 取prdRootTypeNo
		List<TcProductType> productTypes = productTypeMapper.getParentProductType(product.getPrdType());
		Integer prdRootType = null;
		if (productTypes.size() > 0) {
			prdRootType = productTypes.get(0).getPrdTypeNo();
		} else {
			JY.raiseRException("产品根类型不存在！");
		}
		// 挂靠菜单
		product.setModuleId(product.getPrdType() + "-" + order);
		product.setModulePid(product.getPrdType());
		product.setModuleName(product.getPrdName());
		product.setModuleSortno(order);
		product.setModuleUrl(
				"trade/ProudctApproveManage.jsp?prdNo=" + product.getPrdNo() + "&prdName=" + product.getPrdName()
						+ "&prdProp=" + product.getPrdProp() + "&prdRootType=" + String.valueOf(prdRootType));
		product.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		// 更新菜单信息
		productMapper.updateByPrimaryKey(product);
		// 保存产品关联信息
		addProductAssociation(product);
	}

	@Override
	@AutoLogMethod(value = "修改自定义产品")
	public synchronized void updateProduct(TcProduct product) {
		Map<String, Object> params = new HashMap<String, Object>();
		Integer i = 0;
		// 判断产品名称是否存在
		if (StringUtil.isNotEmpty(product.getPrdName())) {
			params.put("prdNo", product.getPrdNo());
			params.put("prdName", product.getPrdName());
			i = productMapper.judgeDuplicate(params);
			if (i > 0) {
				JY.raiseRException("产品名称已存在！");
			}
		}
		TcProduct originalProduct = productMapper.getProductById(product.getPrdNo());
		Integer order = 1;
		if (!originalProduct.getPrdType().equals(product.getPrdType())) {
			// 计算菜单顺序号（逻辑待优化）
			params.clear();
			params.put("prdType", product.getPrdType());
			params.put("sort", "MODULE_SORTNO");
			params.put("order", "DESC");

			List<TcProduct> products = productMapper.getProductList(params);
			TcProduct p = null;
			if (products != null && products.size() != 0) {
				p = products.get(0);
			}
			if (p != null) {
				order += p.getModuleSortno();
			}
		} else {
			order = originalProduct.getModuleSortno();
		}

		// 取prdRootTypeNo
		List<TcProductType> productTypes = productTypeMapper.getParentProductType(product.getPrdType());
		Integer prdRootType = null;
		if (productTypes.size() > 0) {
			prdRootType = productTypes.get(0).getPrdTypeNo();
		} else {
			JY.raiseRException("产品根类型不存在！");
		}
		// 挂靠菜单
		product.setModuleId(product.getPrdType() + "-" + order);
		product.setModulePid(product.getPrdType());
		product.setModuleName(product.getPrdName());
		product.setModuleSortno(order);
		product.setModuleUrl("trade/ProudctApproveManage.jsp?prdNo=" + product.getPrdNo() + "&prdName="
				+ product.getPrdName() + "&prdProp=" + product.getPrdProp() + "&prdRootType=" + prdRootType);
		product.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		// 更新菜单信息
		productMapper.updateByPrimaryKey(product);
		// 更新产品字段
		removeProductAssociation(product.getPrdNo());
		addProductAssociation(product);
	}

	@Override
	@AutoLogMethod(value = "删除自定义产品")
	public void deleteProduct(String[] prdNos) {
		for (int i = 0; i < prdNos.length; i++) {
			productMapper.deleteByPrimaryKey(prdNos[i]);
			removeProductAssociation(prdNos[i]);
		}
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	private void removeProductAssociation(String prdNo) {
		// 删除原有绑定数据
		productFormMapper.deleteProductFormByProductId(prdNo);
	}

	private void addProductAssociation(TcProduct product) {
		// 绑定数据
		List<TcForm> forms = product.getForms();
		if (forms != null) {
			int i = 1;
			for (TcForm form : forms) {
				TcProductForm productForm = new TcProductForm();
				productForm.setPrdNo(product.getPrdNo());
				productForm.setFormNo(form.getFormNo());
				productForm.setFormIndex(i);
				productForm.setFlowRoleId(form.getFlowRoleId());
				productFormMapper.insert(productForm);
				i++;
			}
		}
	}

	@Override
	public List<TcProduct> getProductByName(String prdName) {
		List<TcProduct> tcProduct = productMapper.getProductByName(prdName);
		if (tcProduct.size() == 0) {
			JY.raiseRException("产品名称不存在！");
		} else if (tcProduct.size() > 1) {
			JY.raiseRException("发现多条数据请核实数据是否异常!");
		}
		return tcProduct;
	}

	@Override
	public Page<TcProduct> getPageProductNotChoose(Map<String, Object> map) {
		return productMapper.getPageProductNotChoose(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TcProduct> getPageProductChoose(Map<String, Object> map) {
		return productMapper.getPageProductChoose(map);
	}

	@Override
	public List<TcProduct> searchProduct(Map<String, Object> params) {
		if (params.get("isoccupy") != null) {
			List<TcProduct> list = productMapper.getProductListRelease(params);
			return list;
		} else {
			List<TcProduct> list = productMapper.getProductList(params);
			return list;
		}
	}

	/**
	 * 获取该客户某个业务剩余的占用额度和已经释放的额度
	 *
	 * @return
	 */
	@Override
	public List<HrbCreditCustProd> getBalance(Map<String, Object> params) {

		return hrbCreditPrdRelationMapper.getHrbCreditQueryOne(params);
	}

	/**
	 * 查询所有产品及扩展产品
	 * 
	 * @author lijing
	 * @since 20180122
	 * @return
	 */
	@Override
	public List<TcProduct> getAllProductAndExt(Map<String, Object> params) {
		return productMapper.getAllProductAndExt(params);
	}

	@Override
	public boolean checkIsAcctAcup(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TcProduct product = this.productMapper.getProductById(ParameterUtil.getString(params, "prdNo", "X"));
		if (null == product) {
			return true;
		} else {
			if (StringUtils.trimToEmpty(product.getIsAcup()).equalsIgnoreCase(DictConstants.YesNo.YES)) {
				return useLoop(StringUtils.split(product.getAcupCcys(), ","),
						ParameterUtil.getString(params, "ccy", "CNY"));
			}
		}
		return false;
	}

	public synchronized boolean useLoop(String[] arr, String targetStr) {
		for (String s : arr) {
			if (StringUtils.equalsIgnoreCase(s, targetStr)) {
				return true;
			}
		}
		return false;
	}
}
