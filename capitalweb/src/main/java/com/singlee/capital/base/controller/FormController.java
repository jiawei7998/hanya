package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.service.FormService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
/**
 * @projectName 同业业务管理系统
 * @className 自定义表单控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午11:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/FormController")
public class FormController extends CommonController {
	
	@Autowired
	private FormService formService;
	
	/**
	 * 根据ID查询单个明细
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author hunter
	 * @date 2016-10-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFormById")
	public RetMsg<TcForm> searchFormById(@RequestBody Map<String,Object> params){
		String formNo = ParameterUtil.getString(params, "formNo", "");
		TcForm form = formService.getFormById(formNo);
		return RetMsgHelper.ok(form);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageForm")
	public RetMsg<PageInfo<TcForm>> searchPageForm(@RequestBody Map<String,Object> params){
		Page<TcForm> page = formService.getFormPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增
	 * 
	 * @param form - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/addForm")
	public RetMsg<Serializable> addForm(@RequestBody TcForm form) {
		formService.createForm(form);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * 
	 * @param form - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/editForm")
	public RetMsg<Serializable> editForm(@RequestBody TcForm form) {
		formService.updateForm(form);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 * 
	 * @param form - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/removeForm")
	public RetMsg<Serializable> removeForm(@RequestBody String[] formNos) {
		formService.deleteForm(formNos);
		return RetMsgHelper.ok();
	}
	
}
