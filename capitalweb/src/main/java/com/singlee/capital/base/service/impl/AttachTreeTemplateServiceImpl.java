package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcAttachTreeTemplateMapper;
import com.singlee.capital.base.model.TcAttachTreeTemplate;
import com.singlee.capital.base.service.AttachTreeTemplateService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.common.util.TreeUtil;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AttachTreeTemplateServiceImpl implements AttachTreeTemplateService {

	@Autowired
	private TcAttachTreeTemplateMapper attachTreeTemplateMapper;

	@Override
	public List<TcAttachTreeTemplate> searchAttachTreeTemplate(Map<String, String> params) {
		String refId = ParameterUtil.getString(params, "refId", null);
		String refType = ParameterUtil.getString(params, "refType", null);
		if (StringUtils.isEmpty(refId) && StringUtils.isEmpty(refType)) {
			TcAttachTreeTemplate appachTeeeTemlate = new TcAttachTreeTemplate();
			appachTeeeTemlate.setTreeName("默认根节点");
			appachTeeeTemlate.setTreePid("0");
			appachTeeeTemlate.setSort(0);
			appachTeeeTemlate.setRefType("0");
			attachTreeTemplateMapper.insert(appachTeeeTemlate);
			appachTeeeTemlate.setRefId(appachTeeeTemlate.getTreeId());
			attachTreeTemplateMapper.updateByPrimaryKey(appachTeeeTemlate);
			params.put("refId", appachTeeeTemlate.getTreeId());
		}
		return TreeUtil.mergeChildrenList(attachTreeTemplateMapper.selectTcAttachTreeTemplate(params), "0");
	}
	
	@Override
	@AutoLogMethod(value = "创建附件树")
	public TcAttachTreeTemplate createAttachTreeNode(String parentId) {
		TcAttachTreeTemplate parentNode = new TcAttachTreeTemplate();
		parentNode.setTreeId(parentId);
		parentNode = attachTreeTemplateMapper.selectByPrimaryKey(parentNode);
		TcAttachTreeTemplate thisNode = new TcAttachTreeTemplate();
		thisNode.setRefId(parentNode.getRefId());
		thisNode.setRefType(parentNode.getRefType());
		thisNode.setTreePid(parentId);
		thisNode.setTreeName("untitled");
		int i = 1;
		i = dealSort(i, parentId);
		thisNode.setSort(i);
		attachTreeTemplateMapper.insert(thisNode);
		return thisNode;
	}

	@Override
	@AutoLogMethod(value = "修改附件树")
	public TcAttachTreeTemplate updateAttachTreeNode(String id, String text) {
		TcAttachTreeTemplate tdAttachTreeMap = new TcAttachTreeTemplate();
		tdAttachTreeMap.setTreeId(id);
		tdAttachTreeMap = attachTreeTemplateMapper.selectByPrimaryKey(tdAttachTreeMap);
		tdAttachTreeMap.setTreeName(StringUtil.isNotEmpty(text)?text:"untitled");
		attachTreeTemplateMapper.updateByPrimaryKeySelective(tdAttachTreeMap);
		return tdAttachTreeMap;
	}

	@Override
	@AutoLogMethod(value = "删除附件树")
	public void deleteAttachTreeNode(String id) {
		TcAttachTreeTemplate tdAttachTreeMap = new TcAttachTreeTemplate();
		tdAttachTreeMap.setTreeId(id);
		tdAttachTreeMap = attachTreeTemplateMapper.selectByPrimaryKey(tdAttachTreeMap);
		attachTreeTemplateMapper.deleteTcAttachTreeTemplateByIdCascade(id);
	}

	@Override
	@AutoLogMethod(value = "拖拽附件树")
	public void dndAttachTreeNode(String id, String targetId, String point) {
		//根据节点取当前节点信息
		TcAttachTreeTemplate thisNode = new TcAttachTreeTemplate();
		thisNode.setTreeId(id);
		thisNode = attachTreeTemplateMapper.selectByPrimaryKey(thisNode);
		//根据目标节点编号取出目标节点信息
		TcAttachTreeTemplate targetNode = new TcAttachTreeTemplate();
		targetNode.setTreeId(targetId);
		targetNode = attachTreeTemplateMapper.selectByPrimaryKey(targetNode);
		if ("append".equals(point)) {
			//修改当前节点的父节点编号为目标节点的节点编号
			thisNode.setTreePid(targetId);
			int i = 1;
			i = dealSort(i, targetId);
			thisNode.setSort(i);
			attachTreeTemplateMapper.updateByPrimaryKey(thisNode);
		} else if ("top".equals(point)) {
			//修改排序
			int i = 1;
			thisNode.setTreePid(targetNode.getTreePid());
			thisNode.setSort(i++);
			i = dealSort(i, targetNode.getTreePid());
			attachTreeTemplateMapper.updateByPrimaryKey(thisNode);
		} else if ("bottom".equals(point)) {
			//修改排序
			int i = 1;
			i = dealSort(i, targetNode.getTreePid());
			thisNode.setTreePid(targetNode.getTreePid());
			thisNode.setSort(i);
			attachTreeTemplateMapper.updateByPrimaryKey(thisNode);
		}
	}

	@Override
	public void clearAttachTreeTemplate() {
		attachTreeTemplateMapper.clearTcAttachTreeTemplate();
	}
	
	/***
	 * ----以上为对外服务-------------------------------
	 */
	private int dealSort (int i, String parentId) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("treePid", parentId);
		List<TcAttachTreeTemplate> attachTreeMapList = attachTreeTemplateMapper.selectTcAttachTreeTemplate(params);
		for (TcAttachTreeTemplate atm : attachTreeMapList) {
			atm.setSort(i++);
			attachTreeTemplateMapper.updateByPrimaryKey(atm);
		}
		return i;
	}

}
