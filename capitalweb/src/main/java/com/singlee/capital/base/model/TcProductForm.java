package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @projectName 同业业务管理系统
 * @className 产品表单映射
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-10 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_PRODUCT_FORM_MAP")
public class TcProductForm implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8959326413220115475L;
	/**
	 * 产品编号 序列自动生成
	 */
	@Id
	private String prdNo;
	/**
	 * 表单编号 序列自动生成
	 */
	private String formNo;
	/**
	 * 表单顺序
	 */
	private Integer formIndex;
	

	/**
	 * 审批角色ID （添加审批角色wzf）
	 */
	private String flowRoleId;
	
	public String getFlowRoleId() {
		return flowRoleId;
	}
	public void setFlowRoleId(String flowRoleId) {
		this.flowRoleId = flowRoleId;
	}
	
	
	/**
	 * 关联表单字段分组
	 */
	@Transient
	private TcForm form;
	
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getFormNo() {
		return formNo;
	}
	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}
	public Integer getFormIndex() {
		return formIndex;
	}
	public void setFormIndex(Integer formIndex) {
		this.formIndex = formIndex;
	}
	public TcForm getForm() {
		return form;
	}
	public void setForm(TcForm form) {
		this.form = form;
	}
}
