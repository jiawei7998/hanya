package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachNode;
import com.singlee.capital.base.model.TcAttachNodeRelation;
import com.singlee.capital.base.service.AttachNodeServer;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping(value = "/AttachNodeController")
public class AttachNodeController extends CommonController {
	@Autowired
	private AttachNodeServer attachNodeServer;
	/**
	 * 查询节点
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchAttachNode")
	public RetMsg<PageInfo<TcAttachNode>> searchAttachNode(@RequestBody Map<String,Object> params){
		Page<TcAttachNode> page = attachNodeServer.search(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/createAttachNode")
	public RetMsg<Serializable> createAttacheNode(@RequestBody TcAttachNode params) {
		attachNodeServer.createAttachNode(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateAttachNode")
	public RetMsg<Serializable> updateAttachNode(@RequestBody TcAttachNode params) {
		attachNodeServer.updateAttachNode(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteAttachNode")
	public RetMsg<Serializable> deleteAttachNode(@RequestBody TcAttachNode params) {
		attachNodeServer.deleteAttachNode(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value= "/searchAttachNodeRelationList")
	public RetMsg<List<TcAttachNodeRelation>> searchAttachNodeRelationList(@RequestBody Map<String,Object> params){
		List <TcAttachNodeRelation> list = attachNodeServer.searchAttachNodeRelationList(params);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/addAttachRelation")
	public RetMsg<Serializable> addAttachRelation(@RequestBody Map<String,Object> params){
		attachNodeServer.addAttachRelation(params);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/delAttachRelation")
	public RetMsg<Serializable> delAttachRelation(@RequestBody Map<String,Object> params){
		attachNodeServer.delAttachRelation(params);
		return RetMsgHelper.ok();
	}
}
