package com.singlee.capital.base.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaskInMap;
import com.singlee.capital.base.model.TaskInWaiting;

public interface TaskInWaitingService {

	public Page<TaskInWaiting> searchTaskInWaitingPage(Map<String,Object> map);
	
	public TaskInMap getTaskInWaitingCount(Map<String, Object> map);
}
