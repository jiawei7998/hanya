package com.singlee.capital.base.participant.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.participant.service.TcProductParticipantService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping("/TcProductParticipantController")
public class TcProductParticipantController extends CommonController {
	@Autowired
	TcProductParticipantService tcProductParticipantService;
	/**
	 * 展示列表数据
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductParticipantList")
	public RetMsg<PageInfo<TcProductParticipant>> searchProductParticipantList(@RequestBody Map<String, Object> params) {
		Page<TcProductParticipant> page = tcProductParticipantService.getProductParticipantList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除产品-参与方
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteProductParticipant")
	public RetMsg<Serializable> deleteProductParticipant(@RequestBody Map<String, Object> map) {
		tcProductParticipantService.deleteProductParticipant(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增产品-参与方
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addProductParticipant")
	public RetMsg<Serializable> addProductParticipant(@RequestBody TcProductParticipant tcProductParticipant) {
		tcProductParticipantService.addProductParticipant(tcProductParticipant);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新产品-参与方
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateProductParticipant")
	public RetMsg<Serializable> updateProductParticipant(@RequestBody TcProductParticipant tcProductParticipant) {
		tcProductParticipantService.updateProductParticipant(tcProductParticipant);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getProductParticipantCounterparty")
	public RetMsg<List<TcProductParticipant>> getProductParticipantCounterparty(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tcProductParticipantService.getProductParticipantCounterparty(map));
	}
}
