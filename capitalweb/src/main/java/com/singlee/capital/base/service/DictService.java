package com.singlee.capital.base.service;

import java.util.List;

import com.singlee.capital.base.pojo.DictForCombobox;


/**
 * 字典项管理
 * @author cz
 * 
 */
public interface DictService {


	/** 列出大类 */
	List<DictForCombobox> listBigKind();
	/** 列出小类 */
	List<DictForCombobox> listSmallKind(String bigKind);
	/** 根据客户类别列出大类 */
	List<DictForCombobox> listBigKindByCode(String kind_code);

}