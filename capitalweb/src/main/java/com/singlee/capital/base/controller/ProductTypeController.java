package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.model.TcProductType;
import com.singlee.capital.base.model.TtProductFlowTypeMap;
import com.singlee.capital.base.service.ProductTypeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.flow.pojo.vo.ProductFlowTreeView;
import com.singlee.capital.system.controller.CommonController;

/**
 * 用户相关
 * @author Lyon Chen 
 */
@Controller
@RequestMapping(value = "/ProductTypeController")
public class ProductTypeController extends CommonController {

	@Autowired
	private ProductTypeService productTypeService;

	@ResponseBody
	@RequestMapping(value = "/searchProductType")
	public RetMsg<List<TcProductType>> searchProductType() throws RException {
		List<TcProductType> list = productTypeService.search();			
		return RetMsgHelper.ok(list);
	}

	@ResponseBody
	@RequestMapping(value = "/searchProductTypeList")
	public List<TcProductType> searchProductTypeList() throws RException {
		List<TcProductType> list = productTypeService.search();			
		return list;
	}
	@ResponseBody
	@RequestMapping(value = "/searchProductTypeFlowView")
	public RetMsg<List<ProductFlowTreeView>> searchProductTypeFlowView() throws RException {
		List<ProductFlowTreeView> list = productTypeService.searchProductTypeFlowView();			
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/addProductType")
	public RetMsg<Serializable> addProductType(@RequestBody TcProductType productType) throws RException {
		productTypeService.addProductType(productType);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/editProductType")
	public RetMsg<Serializable> editProductType(@RequestBody TcProductType productType) throws RException {
		productTypeService.updateProductType(productType);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFlowTypeByProductType")
	public RetMsg<List<TtProductFlowTypeMap>> getFlowTypeByProductType(@RequestBody Map<String,Object> params){
		List<TtProductFlowTypeMap> formFieldsets = productTypeService.getFlowTypeByProductType(params);
		return RetMsgHelper.ok(formFieldsets);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveFlowTypeByProductType")
	public RetMsg<?> saveFlowTypeByProductType(@RequestBody Map<String,Object> params){
		productTypeService.saveFlowTypeByProductType(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchProductTypeListByMap")
	public List<TcProductType> searchProductTypeListByMap(@RequestBody Map<String,Object> params) throws RException {
		List<TcProductType> list = productTypeService.getProductTypeList(params);
		return list;
	}
}
