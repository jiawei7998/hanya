package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachNode;
import com.singlee.capital.base.model.TcAttachNodeRelation;

import tk.mybatis.mapper.common.Mapper;

public interface TcAttachNodeMapper  extends Mapper<TcAttachNode> {
	/**
	 * 
	 * @param params
	 * @return
	 */
	public Page<TcAttachNode> search(Map<String, Object> params,RowBounds rb);
	
	/**
	 * 查询附件树下节点
	 * @param params
	 * @return
	 */
	public List <TcAttachNodeRelation> searchAttachNodeRelationList(Map<String, Object> params);
	
	/**
	 * 新增关系配置
	 * @param params
	 */
	public void insertRealtion(Map<String, Object> params);
	
	/**
	 * 删除关系配置
	 * @param params
	 */
	public void delAttachRelation(Map<String, Object> params);
}
