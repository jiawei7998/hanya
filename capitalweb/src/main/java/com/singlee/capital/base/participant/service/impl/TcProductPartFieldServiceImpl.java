package com.singlee.capital.base.participant.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcFieldMapper;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.participant.mapper.TcProductPartFieldMapper;
import com.singlee.capital.base.participant.model.TcProductPartField;
import com.singlee.capital.base.participant.pojo.ProductPartFieldPojo;
import com.singlee.capital.base.participant.service.TcProductPartFieldService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
@Service
public class TcProductPartFieldServiceImpl implements TcProductPartFieldService {
	@Autowired
	TcProductPartFieldMapper tcProductPartFieldMapper;
	@Autowired
	TcFieldMapper tcFieldMapper;
	
	@Override
	public Page<TcProductPartField> getProductPartFieldList(
			Map<String, Object> params) {
		return tcProductPartFieldMapper.getProductPartFieldList(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteProductPartField(Map<String, Object> map) throws RException {
		List<TcProductPartField> productPartFieldList = tcProductPartFieldMapper.findProductPartFieldByPrdNoAndPartyId(map);
		if(productPartFieldList == null || productPartFieldList.isEmpty()) {
			JY.raiseRException("配置关系不存在");
		}
		tcProductPartFieldMapper.deleteProductPartField(map);
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addProductPartField(ProductPartFieldPojo productPartFieldPojo) throws RException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prdNo", productPartFieldPojo.getPrdNo());
		map.put("partyId", productPartFieldPojo.getPartyId());
		List<TcProductPartField> productPartFieldList = tcProductPartFieldMapper.findProductPartFieldByPrdNoAndPartyId(map);
		if(!productPartFieldList.isEmpty()) {
			JY.raiseRException("配置关系已存在");
		}
		insertProductPartField(productPartFieldPojo);
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateProductPartField(ProductPartFieldPojo productPartFieldPojo) throws RException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prdNo", productPartFieldPojo.getPrdNo());
		map.put("partyId", productPartFieldPojo.getPartyId());
		// 先删除所有配置关系
		tcProductPartFieldMapper.deleteProductPartField(map);
		insertProductPartField(productPartFieldPojo);
	}
	
	private void insertProductPartField(ProductPartFieldPojo productPartFieldPojo) {
		String formNos = productPartFieldPojo.getFldNos();
		String[] formNoArray = formNos.split(",");
		TcProductPartField tcProductPartField = new TcProductPartField();
		tcProductPartField.setPrdNo(productPartFieldPojo.getPrdNo());
		tcProductPartField.setPartyId(productPartFieldPojo.getPartyId());
		for(int i = 0; i < formNoArray.length; i++) {
			tcProductPartField.setFldNo(formNoArray[i]);
			tcProductPartField.setSortNo(i + 1);
			tcProductPartFieldMapper.addProductPartField(tcProductPartField);
		}
	}

	@Override
	public Page<TcField> getFieldList(Map<String, Object> params) {
		return tcFieldMapper.getFieldList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TcField> geChooseFieldList(Map<String, Object> map) {
		String[] fldNoArray = ParameterUtil.getString(map, "fldNos", "").split(",");
		List<Integer> fldNoList = new ArrayList<Integer>();
		for(int i = 0; i < fldNoArray.length; i++) {
			fldNoList.add(Integer.parseInt(fldNoArray[i]));
		}
		map.put("fldNoList", fldNoList);
		return tcFieldMapper.getChoosedFieldList(map);
	}
}
