package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.trade.model.ProductApproveParamVo;
import com.singlee.capital.trade.model.TdProductApproveAmtLine;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-1 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcProductMapper extends Mapper<TcProduct> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param prdNo - 产品主键
	 * @return 产品对象
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public TcProduct getProductById(String prdNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品对象列表
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public Page<TcProduct> getProductList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 产品对象列表
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public List<TcProduct> getProductList(Map<String, Object> params);
	
	/**
     * 可释放的产品列表（额度释放）
     * 
     * @param params - 请求参数
     * @return 产品对象列表
     * @author ...
     * @date 2021
     */
    public List<TcProduct> getProductListRelease(Map<String, Object> params);
    
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 记录数
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public int judgeDuplicate(Map<String, Object> params);
	
	/**
	 * 根据产品名称查产品对象
	 * @param params - 请求参数
	 * @return 产品对象列表
	 * @author Wuyc
	 * @date 2016-12-1
	 */
	public List<TcProduct> getProductByName(String prdName);
	
	/**
	 *查询所有产品及扩展产品 
	 *@author lijing
	 *@since 20180122
	 * @return
	 */
	List<TcProduct>	getAllProductAndExt(Map<String, Object> map);
	
	
	
	
	/**
	 * 查询未被机构选中的产品信息
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	public Page<TcProduct> getPageProductNotChoose(Map<String, Object> map, RowBounds rowBounds);
	
	/**
	 * 查询已经被机构选中的产品信息
	 * @param map
	 * @return
	 */
	List<TcProduct>	getPageProductChoose(Map<String, Object> map);
	
	/**
	 * 查询所有产品审批参数(是否需要放款)
	 * @return
	 */
	public List<ProductApproveParamVo>	getLoanParam();
	
	/**
	 * 查询所有产品审批参数(送交行领导金额线)
	 * @return
	 */
	public List<TdProductApproveAmtLine>	getAmtLineParam(@Param(value = "prdNo")String prdNo);
}