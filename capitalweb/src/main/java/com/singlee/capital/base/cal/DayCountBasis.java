package com.singlee.capital.base.cal;
/**
 * 计息基础
 * @author SINGLEE
 *
 */
public enum DayCountBasis {

	A360("Actual/360",360,1),A365("Actual/365",365,2),A366("A366",366,3),DoubleActual("Actual/Actual (ISMA)",0,4);
	
	private String molecule;//分子
	
	private int Denominator;//分母
	
	private int type;//1=A360  2=A365 3=A366  4=A/A
	
	public String getMolecule() {
		return molecule;
	}

	public void setMolecule(String molecule) {
		this.molecule = molecule;
	}

	public int getDenominator() {
		return Denominator;
	}

	public void setDenominator(int denominator) {
		Denominator = denominator;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	private DayCountBasis( String molecule,int Denominator,int type) {
    	
    	this.molecule = molecule;
    	
        this.Denominator = Denominator;
        
        this.type = type;

    }
	
	public static DayCountBasis valueOf(int type) {    //    手写的从int到enum的转换函数
        switch (type) {
        case 1:
            return A360;
        case 2:
            return A365;
        case 3:
            return A366;
        case 4:
        	return DoubleActual;
        default:
        	return null;
        	
        }
    }
	public static DayCountBasis equal(int type) {    //    手写的从int到enum的转换函数
		 switch (type) {
	        case 360:
	            return A360;
	        case 365:
	            return A365;
	        case 366:
	            return A366;
	        default:
	        	return null;
	        	
	        }
    }
	
	public static DayCountBasis equal(String type) {    //    手写的从int到enum的转换函数
        if("Actual/365".equals(type)) {
            return A365;}
        if("Actual/360".equals(type)) {
            return A360;}
        if("Actual/Actual (ISMA)".equals(type)) {
        	return DoubleActual;}
        return null;
    }
}
