package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 字段分组
 * @description TODO
 * @author 倪航
 * @createDate 2016-8-30 下午2:45:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_FIELDSET")
public class TcFieldset implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2491044209566652786L;
	/**
	 * 字段分组编号 序列自动生成
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_FIELDSET.NEXTVAL FROM DUAL")
	private String fdsNo;
	/**
	 * 字段分组名称 
	 */
	private String fdsName;
	/**
	 * 字段分组类型 
	 */
	private String fdsType;
	/**
	 * 启用标志 0-禁用 1-启用
	 */
	private String useFlag;
	/**
	 * 更新时间
	 */
	private String lastUpdate;
	@Transient
	private String custFlag;
	/**
	 * 关联字段
	 */
	@Transient
	private List<TcField> fields;
	
	@Transient
	private List<TcFieldset> fieldSets;
	
	
	public String getCustFlag() {
		return custFlag;
	}
	public void setCustFlag(String custFlag) {
		this.custFlag = custFlag;
	}
	public List<TcFieldset> getFieldSets() {
		return fieldSets;
	}
	public void setFieldSets(List<TcFieldset> fieldSets) {
		this.fieldSets = fieldSets;
	}
	private String atachFormid;
	
	public String getAtachFormid() {
		return atachFormid;
	}
	public void setAtachFormid(String atachFormid) {
		this.atachFormid = atachFormid;
	}
	public String getFdsNo() {
		return fdsNo;
	}
	public void setFdsNo(String fdsNo) {
		this.fdsNo = fdsNo;
	}
	public String getFdsName() {
		return fdsName;
	}
	public void setFdsName(String fdsName) {
		this.fdsName = fdsName;
	}
	public String getFdsType() {
		return fdsType;
	}
	public void setFdsType(String fdsType) {
		this.fdsType = fdsType;
	}
	public String getUseFlag() {
		return useFlag;
	}
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public List<TcField> getFields() {
		return fields;
	}
	public void setFields(List<TcField> fields) {
		this.fields = fields;
	}
}