

package com.singlee.capital.base.participant.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcProductParticipant;

public interface TcProductParticipantMapper extends Mapper<TcProductParticipant> {

	/**
	 * 查询出参与方与产品挂钩的信息
	 * @param params 条件
	 * @param rb
	 * @return
	 */
	public Page<TcProductParticipant> getProductParticipantList(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据产品ID查询出所属的参与方
	 * @param params
	 * @return
	 */
	public List<TcProductParticipant> getProductParticipantListByPrdNo(Map<String, Object> params);
	
	/**
	 * 根据产品ID和参与方代码查询关系
	 * @param map
	 * @return
	 */
	public List<TcProductParticipant> getProductParticipantByPrdNoAndPartyId(Map<String, Object> map);
	
	/**
	 * 根据产品ID和参与方代码删除关系
	 * @param map
	 */
	public void deleteByPrdNoAndPartyId(Map<String, Object> map);
	
	/**
	 * 更新排序-1
	 * @param map
	 */
	public void updateSortByPrdNoAndSort(Map<String, Object> map);
	
	/**
	 * 新增产品参与方关系
	 * @param tcProductParticipant
	 */
	public void addProductParticipant(TcProductParticipant tcProductParticipant);
	
	/**
	 * 根据排序顺序查询
	 * @param map
	 * @return
	 */
	public List<TcProductParticipant> getProductParticipantBySort(Map<String, Object> map);
	
	/**
	 * 查询是否已绑定交易对手
	 * @param map
	 * @return
	 */
	public List<TcProductParticipant> getProductParticipantCounterparty(Map<String, Object> map);
	
}
