package com.singlee.capital.base.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.counterparty.model.CounterPartyFuzzyVo;
import com.singlee.capital.counterparty.model.PayBank;
import com.singlee.capital.market.model.TtMktBondIssuer;


/**
 * 模糊查询dao
 * @author gm
 *
 */
public interface FuzzyQueryMapper {

	/**
	 * 发行人模糊查询
	 * @param map
	 * 	issuer_name：发行人名称
	 *  limit：显示数量
	 * @return
	 */
	public List<TtMktBondIssuer> selectBondIssuer(HashMap<String, Object> map);
	
	/**
	 * 支付行号 模糊查询
	 * @param map
	 * 	bk_name：行号 ，行名
	 *  limit：显示数量
	 * @return
	 */
	public List<PayBank> selectPayBank(Map<String, Object> map);
	/**
	 * 交易对手模糊查询
	 * @param map
	 * 	party_name:交易对手名称
	 * 	limit:显示数量
	 * @return
	 */
	public List<CounterPartyFuzzyVo> selectCounterParty(HashMap<String, Object> map);
	
}
