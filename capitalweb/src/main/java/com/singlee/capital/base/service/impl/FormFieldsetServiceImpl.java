package com.singlee.capital.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcFormFieldsetMapper;
import com.singlee.capital.base.model.TcFormFieldset;
import com.singlee.capital.base.service.FormFieldsetService;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单字段映射服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FormFieldsetServiceImpl implements FormFieldsetService {
	
	@Autowired
	private TcFormFieldsetMapper formFieldsetMapper;
	
	@Override
	public List<TcFormFieldset> getFormFieldsetList(Map<String, Object> params) {
		return formFieldsetMapper.getFormFieldsetList(params);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
