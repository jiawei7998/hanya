package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcProductType;
import com.singlee.capital.flow.pojo.vo.ProductFlowTreeView;

public interface TcProductTypeMapper extends Mapper<TcProductType> {
	
	Page<TcProductType> pageProductType(Map<String,Object> map, RowBounds rb);
	
	List<TcProductType> listProductType(Map<String,Object> map);
	
	List<TcProductType> getParentProductType(@Param("prd_type_no")String prd_type_no);
	
	List<ProductFlowTreeView> searchProductTypeFlowView();
}