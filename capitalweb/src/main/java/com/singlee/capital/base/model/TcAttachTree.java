package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @projectName 同业业务管理系统
 * @className 附件树
 * @description TODO
 * @author 倪航
 * @createDate 2017-1-3 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_ATTACH_TREE")
public class TcAttachTree implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String treeId ;
	private String treeName;
	private String refType;
	private String isTemplate;
	private String treeDesc;
	private String createUserId;
	private String createUserName;
	private String createInstId;
	private String createInstName;
	private String createTime;
	private String updateUserId;
	private String updateUserName;
	private String updateInstId;
	private String updateInstName;
	private String updateTime;
	public String getTreeId() {
		return treeId;
	}
	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getIsTemplate() {
		return isTemplate;
	}
	public void setIsTemplate(String isTemplate) {
		this.isTemplate = isTemplate;
	}
	public String getTreeDesc() {
		return treeDesc;
	}
	public void setTreeDesc(String treeDesc) {
		this.treeDesc = treeDesc;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateInstId() {
		return createInstId;
	}
	public void setCreateInstId(String createInstId) {
		this.createInstId = createInstId;
	}
	public String getCreateInstName() {
		return createInstName;
	}
	public void setCreateInstName(String createInstName) {
		this.createInstName = createInstName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	public String getUpdateInstId() {
		return updateInstId;
	}
	public void setUpdateInstId(String updateInstId) {
		this.updateInstId = updateInstId;
	}
	public String getUpdateInstName() {
		return updateInstName;
	}
	public void setUpdateInstName(String updateInstName) {
		this.updateInstName = updateInstName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
}
