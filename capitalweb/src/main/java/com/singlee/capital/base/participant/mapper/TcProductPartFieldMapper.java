package com.singlee.capital.base.participant.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcProductPartField;
import com.singlee.capital.base.participant.pojo.ProductPartFieldPojo;

public interface TcProductPartFieldMapper extends Mapper<TcProductPartField> {

	/**
	 * 查询出参与方与产品挂钩的信息
	 * @param params 条件
	 * @param rb
	 * @return
	 */
	public Page<TcProductPartField> getProductPartFieldList(Map<String, Object> params, RowBounds rb);

	/**
	 * 删除产品参与方字段配置
	 * @param map
	 */
	public void deleteProductPartField(Map<String, Object> map);
	
	/**
	 * 更新产品参与方字段配置
	 * @param productPartFieldPojo
	 */
	public void updateProductPartField(ProductPartFieldPojo productPartFieldPojo);
	
	/**
	 * 根据产品ID和参与方代码查询相关配置
	 * @param map
	 * @return
	 */
	public List<TcProductPartField> findProductPartFieldByPrdNoAndPartyId(Map<String, Object> map);
	
	/**
	 * 新增
	 * @param tcProductPartField
	 */
	public void addProductPartField(TcProductPartField tcProductPartField);
}
