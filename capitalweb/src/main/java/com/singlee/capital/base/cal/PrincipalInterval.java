package com.singlee.capital.base.cal;

import java.io.Serializable;

/**
 * 本金区间
 * @author SINGLEE
 *
 *区间起始日	区间截止日	计划	实际	实际还款日期
  2016/12/21	2017/1/21	100000	？	？
 
 再某某区间内  包含起始，包含结尾  的剩余本金
 假设业务： 2016/12/21~2017/02/21  总本金为200000
 在2017/01/28日做了还本100000
 那么
 区间模式(具体看业务是否以还本的实际还本日期作为利息的计提标准)
[2016/12/21,2017/01/27]  200000
[2017/01/28,2017/02/21]  100000

 */
public class PrincipalInterval implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4399239164763861394L;
	
	private String startDate;//区间起始日期
	
	private String endDate;//区间截止日期
	
	private double residual_Principal;//剩余本金

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getResidual_Principal() {
		return residual_Principal;
	}

	public void setResidual_Principal(double residual_Principal) {
		this.residual_Principal = residual_Principal;
	}
}
