package com.singlee.capital.base.participant.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.participant.model.TcProductPartField;
import com.singlee.capital.base.participant.pojo.ProductPartFieldPojo;

public interface TcProductPartFieldService {
	/**
	 * 查询产品 参与方 栏位配置
	 * @param params
	 * @return
	 */
	public Page<TcProductPartField> getProductPartFieldList(Map<String, Object> params);
	
	/**
	 * 删除产品参与方字段配置
	 * @param map
	 */
	public void deleteProductPartField(Map<String, Object> map);
	
	/**
	 * 新增产品参与方字段配置
	 * @param productPartFieldPojo
	 */
	public void addProductPartField(ProductPartFieldPojo productPartFieldPojo);
	
	/**
	 * 更新产品参与方字段配置
	 * @param productPartFieldPojo
	 */
	public void updateProductPartField(ProductPartFieldPojo productPartFieldPojo);
	
	/**
	 * 查询未选中的表单域
	 * @param params
	 * @return
	 */
	public Page<TcField> getFieldList(Map<String, Object> params);
	
	/**
	 * 查询已选中的表单域
	 * @param map
	 * @return
	 */
	public List<TcField> geChooseFieldList(Map<String, Object> map);
}
