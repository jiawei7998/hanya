package com.singlee.capital.base.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TaDeskMenuMapper;
import com.singlee.capital.base.model.TaDeskMenu;
import com.singlee.capital.base.model.vo.MenuDeskTopVo;
import com.singlee.capital.base.model.vo.MenuVo;
import com.singlee.capital.base.model.vo.TaDeskMenuVo;
import com.singlee.capital.base.model.vo.TreeMenuVo;
import com.singlee.capital.base.service.DeskMenuService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaRoleDeskMap;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DeskMenuServiceImpl implements DeskMenuService {
	@Autowired
	TaDeskMenuMapper deskMenuMapper;
	@Override
	public List<TaDeskMenuVo> getAllTaDeskMenuByIds(Map<String, Object> map) {
		//台子信息
		List<TaDeskMenuVo> taDeskMenuVos = new ArrayList<TaDeskMenuVo>();
		//可以看到的台子
		List<TaDeskMenu> menus = deskMenuMapper.getAllTaDeskMenuByIds(map);
		TaDeskMenuVo deskMenuVo = null;
		for(TaDeskMenu taDeskMenu : menus)
		{
			deskMenuVo = new TaDeskMenuVo();
			deskMenuVo.setIcon(taDeskMenu.getDeskIcon());
			deskMenuVo.setName(taDeskMenu.getDeskId());
			deskMenuVo.setText(taDeskMenu.getDeskName());
			deskMenuVo.setUrl(taDeskMenu.getDeskUrl());
			deskMenuVo.setUid(taDeskMenu.getDeskId());
			taDeskMenuVos.add(deskMenuVo);
		}
		return taDeskMenuVos;
	}

	@Override
	public List<MenuDeskTopVo> getAllMenuDeskTopVos(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		List<MenuDeskTopVo> menuDeskTopVos = new ArrayList<MenuDeskTopVo>();
		//查询生效的菜单
		paramsMap.put("isActive", DictConstants.YesNo.YES);
		List<MenuVo> taModules = deskMenuMapper.getModulesByIdsAndDeskId(paramsMap);
		MenuDeskTopVo menuDeskTopVo = null;
		for(MenuVo module : taModules){
			menuDeskTopVo = new MenuDeskTopVo();
			menuDeskTopVo.setModuleType(module.getModuleName());
			paramsMap.put("modulePid", module.getModuleId());
			menuDeskTopVo.setMenus(deskMenuMapper.getModulesByPid(paramsMap));
			menuDeskTopVos.add(menuDeskTopVo);
		}
		return menuDeskTopVos;
	}
	
	@Override
	public List<MenuVo> getAllModuleWhitOutDeskId(Map<String, Object> paramsMap) {
		return deskMenuMapper.getModulesByIdsAndDeskId(paramsMap);
	}

	@Override
	public List<MenuDeskTopVo> getDeskTradeMenuPage(
			Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		/**
		 * 1、从 TC_PRODUCT_TYPE 获取关联 TA_DESK_MENU的主交易资源头
		 * 2、根据资源头的PRD_TYPE_NO关联 TC_PRODUCT获取子资源
		 */
		String type = ParameterUtil.getString(paramsMap, "type", "1");
		List<MenuDeskTopVo> menuDeskTopVos = new ArrayList<MenuDeskTopVo>();
		List<MenuVo> taModules = deskMenuMapper.getTradeMenuByDeskId(paramsMap);
		MenuDeskTopVo menuDeskTopVo = null;
		for(MenuVo module : taModules){
			menuDeskTopVo = new MenuDeskTopVo();
			menuDeskTopVo.setModuleType(module.getModuleName());
			if(type.equals(DictConstants.DealType.Approve)) {
				menuDeskTopVo.setMenus(deskMenuMapper.getTradeModulesByPid(module.getModuleId()));}
			else {
				menuDeskTopVo.setMenus(deskMenuMapper.getTradeModulesByPid2(module.getModuleId()));}
			menuDeskTopVos.add(menuDeskTopVo);
		}
		return menuDeskTopVos;
	}
	

	/**
	 * 根据角色Id 机构Id 查询菜单台子
	 */
	@Override
	
	public Page<TaDeskMenu> getDeskMenuByRoleIdAndInstId(Map<String, Object> params) {
		TaUser user =SlSessionHelper.getUser();
		if(user!=null && user.getBranchId()!=null){
			params.put("userId",user.getUserId());
			params.put("bankId", user.getBranchId());
			Page<TaDeskMenu> deskMenuList=deskMenuMapper.getDeskMenuByRoleIdAndInstId(params,ParameterUtil.getRowBounds(params));
			return deskMenuList;
		}
		return null;
	}

	@Override
	public Page<TaDeskMenu> getAllMenuByBranchId(Map<String, Object> params) {
		
		return deskMenuMapper.getAllMenuByBranchId(params,ParameterUtil.getRowBounds(params));
	}

	@Override
	public void saveDeskByBranchId(Map<String, Object> map) {
		TaDeskMenu taDeskMenu = new TaDeskMenu();
		String deskMax=deskMenuMapper.getDeskMax(map);
		taDeskMenu.setBranchId((String)map.get("branchId"));
		Integer sert=Integer.valueOf(deskMax)+1;
		String str = String.format("%06d", sert);
		taDeskMenu.setDeskId("M"+str);
		taDeskMenu.setDeskUrl((String)map.get("deskUrl"));
		taDeskMenu.setDeskName((String)map.get("deskName"));
		taDeskMenu.setDeskIcon("icon-xitong3");//设置系统默认台子图标，此图标必须有，并且不可删
		//获取排序
		Integer deskSort= deskMenuMapper.getDeskMaxSort(map)+1;
		taDeskMenu.setDeskSort(deskSort);
		taDeskMenu.setIsActive("1");
		deskMenuMapper.saveDeskByBranchId(taDeskMenu);
		//插入ta_desk_menu后，还要插入TA_ROLE_DESK_MENU_MAP才会真正在左边台子处显示
		String roleId = ParameterUtil.getString(map, "roleId", "");
		TaRoleDeskMap taRoleDeskMap = new TaRoleDeskMap();
		taRoleDeskMap.setRoleId(roleId);
		taRoleDeskMap.setDeskId("M"+str);
		taRoleDeskMap.setAccessFlag("1");
		taRoleDeskMap.setDeskUrl("./menu.jsp");
		deskMenuMapper.insertTaRoleDeskMap(taRoleDeskMap);
		
	}

	@Override
	public TaDeskMenu getDeskMenuByBranchIdAndDeskId(Map<String, Object> map) {
		TaDeskMenu taDeskMenu=deskMenuMapper.getDeskMenuByBranchIdAndDeskId(map);
		return taDeskMenu;
	}

	@Override
	public void updateDeskBranchId(Map<String, Object> map) {
		 deskMenuMapper.updateDeskBranchId(map);
	}

	@Override
	public List<TreeMenuVo> getAllTreeMenuVos(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		List<TreeMenuVo> menuDeskTopVos = new ArrayList<TreeMenuVo>();
		List<MenuVo> taModules = deskMenuMapper.getModulesByIdsAndDeskId(paramsMap);
		TreeMenuVo menuDeskTopVo = null;TreeMenuVo oMenuVo = null;
		for(MenuVo module : taModules){
			menuDeskTopVo = new TreeMenuVo();
			menuDeskTopVo.setText(module.getModuleName());
			menuDeskTopVo.setId(module.getModuleId());
			menuDeskTopVo.setIconCls("sl-deskCust-black");
			menuDeskTopVos.add(menuDeskTopVo);
			paramsMap.put("modulePid", module.getModuleId());
			List<MenuVo> other2 = deskMenuMapper.getModulesByPid(paramsMap);
			for(MenuVo other:other2) {
				oMenuVo = new TreeMenuVo();
				oMenuVo.setText(other.getModuleName());
				oMenuVo.setId(other.getModuleId());
				oMenuVo.setPid(menuDeskTopVo.getId());
				oMenuVo.setUrl(other.getModuleUrl());
				oMenuVo.setIconCls("icon-node");
				menuDeskTopVos.add(oMenuVo);
			}
			
		}
		return menuDeskTopVos;
	}


}
