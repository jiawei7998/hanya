package com.singlee.capital.base.service.impl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.pojo.DictForCombobox;
import com.singlee.capital.base.service.DictService;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.counterparty.mapper.CounterPartyDao;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.counterparty.model.TtCounterPartyKind;
import com.singlee.capital.system.dict.DictConstants;


/**
 * 字典项管理 
 * 
 * 交易对手的大小类的维护也在这里处理 
 * 
 * @author gm
 * 
 */
@Service("dictService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DictServiceImpl implements DictService {

	public final static Log log = LogFactory.getLog(DictServiceImpl.class);
	
	public DictServiceImpl() {
	}

	@Autowired
	private CounterPartyDao counterPartyDao;
	
	@Override
	public List<DictForCombobox> listBigKind() {
		return toDict(counterPartyDao.selectCounterPartyBigKind(),DictConstants.PartyKind.BigKind);
	}

	/**
	 * 取客户小类
	 */
	@Override
	public List<DictForCombobox> listSmallKind(String bigKind) {
		List<String> bigKindList = StringUtil.checkEmptyNull(bigKind)?null:Arrays.asList(bigKind.split(","));
		List<TtCounterPartyKind> list = counterPartyDao.selectCounterPartySmallKind(bigKindList);
		return toDict(list,DictConstants.PartyKind.SmallKind);
	}

	private List<DictForCombobox> toDict(List<?> list,String kindType){
		List<DictForCombobox> ret = new LinkedList<DictForCombobox>();
		if(list==null) {return ret;}
		if(DictConstants.PartyKind.BigKind.contains(kindType) || DictConstants.PartyKind.SmallKind.contains(kindType)){
			for(Object kind : list){
				ret.add(new DictForCombobox(((TtCounterPartyKind)kind).getKind_code(),((TtCounterPartyKind)kind).getKind_name()));
			}
		}
		else if(DictConstants.PartyKind.Party.contains(kindType)){
			for(Object party : list){
				ret.add(new DictForCombobox(((TtCounterParty)party).getParty_id(),((TtCounterParty)party).getParty_name()));
			}
		}
		return ret;
	}

	@Override
	public List<DictForCombobox> listBigKindByCode(String kind_code) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 根据客户类别获取大类
	 *//*
	@Override
	public List<DictForCombobox> listBigKindByCode(String kind_code) {
		return toDict(counterPartyDao.selectCounterPartyBigKindByCode(kind_code),DictConstants.PartyKind.BigKind);
	}*/
}
