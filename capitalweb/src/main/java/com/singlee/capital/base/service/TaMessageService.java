package com.singlee.capital.base.service;

import java.util.Map;


import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaMessage;

public interface TaMessageService {
	
	
	public Page<TaMessage> selectTaMessage(Map<String, Object> params);
	
	public int deleteTaMessage(Map<String, Object> params);
	
	public int updateTaMessage(Map<String, Object> params);
	
	public int insertTaMessage(Map<String, Object> params);
	
	public int selectTaMessageCount(Map<String, Object> params);
	
}
