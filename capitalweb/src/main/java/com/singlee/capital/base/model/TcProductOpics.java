package com.singlee.capital.base.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/13 19:26
 * @description：${description}
 * @modified By：
 * @version:
 */

/**
 * 前置opics产品参数关联关系表
 */

@Entity
@Table(name = "TC_PRODUCT_OPICS")
public class TcProductOpics implements Serializable {
    /**
     * 前置产品编号
     */
    private String prdCode;

    /**
     * OPICS产品大类
     */
    private String pcode;

    /**
     * OPICS产品小类
     */
    private String ptype;

    /**
     * OPICS会计类型
     */
    private String acty;

    /**
     * OPICS成本中心
     */
    private String costcent;

    /**
     * OPICS投资组合
     */
    private String portfolio;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    public String getPrdCode() {
        return prdCode;
    }

    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getActy() {
        return acty;
    }

    public void setActy(String acty) {
        this.acty = acty;
    }

    public String getCostcent() {
        return costcent;
    }

    public void setCostcent(String costcent) {
        this.costcent = costcent;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}