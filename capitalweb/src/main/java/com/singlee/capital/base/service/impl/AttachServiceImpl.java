package com.singlee.capital.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcAttachMapper;
import com.singlee.capital.base.mapper.TdSmartDocumentAttachMapper;
import com.singlee.capital.base.model.TcAttach;
import com.singlee.capital.base.model.TdSmartDocumentAttach;
import com.singlee.capital.base.service.AttachService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * @className 附件服务
 * @description TODO
 * @author Lyon
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service(value="AttachService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AttachServiceImpl implements AttachService {

	@Autowired
	private TcAttachMapper attachMapper;
	@Autowired
	private TdSmartDocumentAttachMapper smartDocumentAttachMapper;

	@Override
	public TcAttach getAttach(String id) {
		return attachMapper.selectByPrimaryKey(id);
	}

	@Override
	@AutoLogMethod(value = "创建附件")
	public void createAttach(TdSmartDocumentAttach a) {
		a.setCreateTime(DateTimeUtil.getLocalDateTime());
		smartDocumentAttachMapper.insert(a);
	}

	@Override
	@AutoLogMethod(value = "修改附件")
	public void updateAttach(TcAttach a) {
		a.setUpdateTime(DateTimeUtil.getLocalDateTime());
		attachMapper.updateByPrimaryKeySelective(a);
	}

	@Override
	@AutoLogMethod(value = "删除附件")
	public void deleteAttach(String attachId) {
		String userId = SlSessionHelper.getUserId();
		TcAttach tcAttach = new TcAttach();
		tcAttach.setAttachId(attachId);
		tcAttach = attachMapper.selectByPrimaryKey(tcAttach);
		if (!userId.equals(tcAttach.getCreateUserId())) {
			JY.raise("非本人上传附件不允许删除。");
		}
		attachMapper.deleteByPrimaryKey(attachId);
	}

	@Override
	public List<TcAttach> searchAttach(String refType, String refId) {
		return attachMapper.selectTcAttach(refType, refId);
	}

	@Override
	public void clearAttach() {
		attachMapper.clearTcAttach();
	}

	@Override
	@AutoLogMethod(value = "创建附件")
	public void createAttach(TcAttach a) {
		a.setCreateTime(DateTimeUtil.getLocalDateTime());
		attachMapper.insert(a);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
