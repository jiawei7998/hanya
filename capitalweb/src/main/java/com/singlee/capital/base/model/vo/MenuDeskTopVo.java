package com.singlee.capital.base.model.vo;

import java.io.Serializable;
import java.util.List;

public class MenuDeskTopVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 直接关联TA_DESK_MENU 的DESK_ID BRANCH_ID
	 */
	private String moduleType;
	/**
	 * TA_MODULE的节点
	 */
	private List<MenuVo> menus;

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public List<MenuVo> getMenus() {
		return menus;
	}

	public void setMenus(List<MenuVo> menus) {
		this.menus = menus;
	}

}
