package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;


import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TtMktCalendarsDate;

/**
 * 日历服务-接口
 * @author lihuabing
 *
 */
public interface CalendarsDateService {
	
	/**
	 * 分页查询
	 */
	public Page<TtMktCalendarsDate> TtMktCalendarDatePage();
	
	/**
	 * 添加交易日历
	 * @param ttMktCalendarDate
	 */
	public String insertTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	/**
	 * 删除交易日历
	 * @param ttMktCalendarDate
	 */
	public void deleteTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	/**
	 * 修改交易日历
	 * @param ttMktCalendarDate
	 */
	public void updateTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	public List<TtMktCalendarsDate> queryTtMktCalendarDate();
	public List<TtMktCalendarsDate> queryListTtMktCalendarDate();
	
	public int queryTtMktCalendarDateById(TtMktCalendarsDate ttMktCalendarDate);
	public boolean updateCalendarService(String countryCode, String workYear) throws Exception;
	
	public List<TtMktCalendarsDate> getTtMktCalendarByMonth(Map<String, Object> param)  throws Exception;

	Page<TtMktCalendarsDate> TtMktCalendarDatePage(Map<String, Object> param);
}
