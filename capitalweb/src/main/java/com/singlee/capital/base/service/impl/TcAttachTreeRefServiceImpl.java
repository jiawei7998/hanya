package com.singlee.capital.base.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcAttachTreeRefMapper;
import com.singlee.capital.base.model.TcAttachTreeRef;
import com.singlee.capital.base.service.TcAttachTreeRefService;

/**
 * @className 附件树服务
 * @description TODO
 * @author Lyon
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcAttachTreeRefServiceImpl implements TcAttachTreeRefService {
	
	@Resource
	TcAttachTreeRefMapper tcAttachTreeRefMapper;

	@Override
	public int insertTcAttachTreeFef(TcAttachTreeRef ref) {
		int flag = 0;
		//查询是否已存在记录
		TcAttachTreeRef tcAttachTreeRef = tcAttachTreeRefMapper.selectByPrimaryKey(ref.getRefId());
		if(null != tcAttachTreeRef && !("".equals(tcAttachTreeRef.getRefId()))){//存在即修改
			flag = tcAttachTreeRefMapper.updateByPrimaryKey(ref);
		}else{//不存在进行添加
			flag = tcAttachTreeRefMapper.insert(ref);
		}
		return flag;
	}

	@Override
	public int updateTcAttachTreeFef(TcAttachTreeRef ref) {
		return tcAttachTreeRefMapper.updateByPrimaryKey(ref);
	}

	@Override
	public int deleteTcAttachTreeFef(TcAttachTreeRef ref) {
		return tcAttachTreeRefMapper.delete(ref);
	}

	@Override
	public TcAttachTreeRef getTcAttachTreeRefById(TcAttachTreeRef ref) {
		return tcAttachTreeRefMapper.selectByPrimaryKey(ref);
	}
	
}
