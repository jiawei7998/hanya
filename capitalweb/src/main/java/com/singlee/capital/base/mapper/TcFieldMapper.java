package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;

/**
 * @projectName 同业业务管理系统
 * @className 自定义字段持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcFieldMapper extends Mapper<TcField> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param fldNo - 字段主键
	 * @return 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public TcField getFieldById(String fldNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 字段对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public Page<TcField> getFieldList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public Page<TcField> getFieldList(Map<String, Object> params);
	
	/**
	 * 查询产品参与人已绑定的表单域
	 * @param map
	 * @return
	 */
	public List<TcField> getChoosedFieldList(Map<String, Object> map);
	/**
	 * 根据产品prdNo获得该产品下配置的数据项
	 * @param map
	 * @return
	 */
	public List<TcField> getChoosedFieldListForPrdNo(Map<String, Object> map);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public int updateByPKSelective(Map<String,Object> params);
}