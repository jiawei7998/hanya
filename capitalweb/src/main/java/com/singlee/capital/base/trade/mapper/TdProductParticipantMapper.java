package com.singlee.capital.base.trade.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.trade.model.TdProductParticipant;

public interface TdProductParticipantMapper extends Mapper<TdProductParticipant>{
	/**
	 * 根据DEALNO 查询得到所属的参与方列表
	 * @param params
	 * @return
	 */
	public List<TdProductParticipant> getTdProductParticipantsByDealno(Map<String, Object> params);
	/**
	 * 根据传入的DEALNO 删除关联的参与方    修改/删除 单据的时候
	 * @param params
	 */
	public void deleteTdProductPartyByDealno(Map<String, Object> params);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
	public void insertProductPartyCopy(Map<String, Object> map);
	
	/**
	 * 数据移型使用
	 * @param map
	 * @return
	 */
	public List<TdProductParticipant> getProductParticipantsForPrdNoDealNo(Map<String, Object> map);
	
}
