package com.singlee.capital.base.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.trade.model.TdProductParticipant;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveSub;

public interface TdProductParticipantService {
	/**
	 * 批量将 参与方插入数据库
	 * @param tdProductParticipants
	 */
	public void insertBatchForTdProductParticipant(List<TdProductParticipant> tdProductParticipants) ;
	
	/**
	 * 根据交易单批量插入参与方数据
	 * @param productApproveMain
	 */
	public void insertParticipantByDeal(TdProductApproveMain productApproveMain);
	
	/**
	 * 根据传入的参数获得参与方信息，将参与方数据进行MAP转换，进而构建TdProductApproveSub
	 * @param params
	 * @return
	 */
	public List<TdProductApproveSub> transProductParticipantAsApproveSubs(Map<String, Object> params);
	/**
	 * 删除参与方信息
	 * @param params
	 */
	public void deleteProductParticipantByDealNo(Map<String, Object> params);
}
