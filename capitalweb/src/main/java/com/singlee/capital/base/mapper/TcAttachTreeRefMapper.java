package com.singlee.capital.base.mapper;

import java.util.Map;

import com.singlee.capital.base.model.TcAttachTreeRef;
import com.singlee.capital.common.spring.mybatis.MyMapper;

/**
 * 
 * @author liuxy
 *
 */
public interface TcAttachTreeRefMapper extends MyMapper<TcAttachTreeRef> {
	
	public TcAttachTreeRef getTcAttachTreeRefByPrd(Map<String, String> map);
}