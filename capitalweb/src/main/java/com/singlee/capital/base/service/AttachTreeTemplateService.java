package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcAttachTreeTemplate;


/**
 * @projectName 
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:41:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AttachTreeTemplateService {

	/**
	 * 查询 
	 * @param params - 查询参数
	 * @return 附件树对象集合
	 */
	List<TcAttachTreeTemplate> searchAttachTreeTemplate(Map<String, String> params);
	/**
	 * 创建
	 * 
	 * @param parentId - 父节点ID
	 * @return 附件树对象
	 */
	public TcAttachTreeTemplate createAttachTreeNode(String parentId);
	
	/**
	 * 修改
	 * 
	 * @param id - 节点ID
	 * @param text - 节点内容
	 * @return 附件树对象
	 */
	public TcAttachTreeTemplate updateAttachTreeNode(String id, String text);
	
	/**
	 * 删除
	 * 
	 * @param id - 节点ID
	 */
	public void deleteAttachTreeNode(String id);
	
	/**
	 * 拖拽
	 * 
	 * @param id - 拖拽的节点ID。
	 * @param targetId - 拖拽到的节点ID。
	 * @param point - 指明释放操作，可用值有：'append','top' 或 'bottom'。
	 */
	public void dndAttachTreeNode(String id, String targetId, String append);
	
	/**
	 * 清理未关联的附件树模版
	 * 
	 */
	public void clearAttachTreeTemplate();
}
