package com.singlee.capital.base.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.model.TcFormFieldsetField;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单字段分组字段映射持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-30 下午3:15:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcFormFieldsetFieldMapper extends Mapper<TcFormFieldsetField> {

	/**
	 * 根据字段分组编号删除字段分组字段映射
	 * 
	 * @param prdNo - 表单编号
	 * @author Hunter
	 * @date 2016-8-30
	 */
	public void deleteFormFieldsetFieldByFormId(String formNo);
}