package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 自定义表单
 * @author xuhui
 *
 */
@Entity
@Table(name = "TC_FORM")
public class TcForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5664759144106057424L;
	
	/**
	 * 表单编号
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_FORM.NEXTVAL FROM DUAL")
	private String formNo;
	/**
	 * 所属机构
	 */
	private String formInst;
	/**
	 * 表单名称
	 */
	private String formName;
	/**
	 * 表单类型
	 */
	private String formType;
	/**
	 * 表单描述
	 */
	private String formDesc;
	/**
	 * 是否模板 0-否 1-是
	 */
	private String isTemplate;
	/**
	 * 启用标志 0-禁用 1-启用
	 */
	private String useFlag;
	
	/**
	 * 机构名称中文
	 */
	@Transient
	private String formInstName;
	/**
	 * 预定义字段集合
	 */
	@Transient
	private List<TcFormFieldset> predefinedFieldsets;
	
	/**
	 * 产品自定义字段
	 */
	@Transient
	private List<TcFormFieldset> customizedFieldsets;
	/**
	 * 更新时间
	 */
	
	
	private String lastUpdate;
	
	/**
	 * 审批角色ID，作为媒介接收前台传过来的flowroleid wzf
	 */
	@Transient
	private String flowRoleId;
	
	
	public String getFlowRoleId() {
		return flowRoleId;
	}
	public void setFlowRoleId(String flowRoleId) {
		this.flowRoleId = flowRoleId;
	}
	
	
	public String getFormNo() {
		return formNo;
	}
	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}
	public String getFormInst() {
		return formInst;
	}
	public void setFormInst(String formInst) {
		this.formInst = formInst;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getFormDesc() {
		return formDesc;
	}
	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}
	public String getIsTemplate() {
		return isTemplate;
	}
	public void setIsTemplate(String isTemplate) {
		this.isTemplate = isTemplate;
	}
	public String getUseFlag() {
		return useFlag;
	}
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getFormInstName() {
		return formInstName;
	}
	public void setFormInstName(String formInstName) {
		this.formInstName = formInstName;
	}
	public List<TcFormFieldset> getPredefinedFieldsets() {
		return predefinedFieldsets;
	}
	public void setPredefinedFieldsets(List<TcFormFieldset> predefinedFieldsets) {
		this.predefinedFieldsets = predefinedFieldsets;
	}
	public List<TcFormFieldset> getCustomizedFieldsets() {
		return customizedFieldsets;
	}
	public void setCustomizedFieldsets(List<TcFormFieldset> customizedFieldsets) {
		this.customizedFieldsets = customizedFieldsets;
	}
}
