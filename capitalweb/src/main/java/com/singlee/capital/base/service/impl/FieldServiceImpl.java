package com.singlee.capital.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcFieldMapper;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.service.FieldService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;

/**
 * @projectName 同业业务管理系统
 * @className 自定义字段服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FieldServiceImpl implements FieldService {
	
	@Autowired
	private TcFieldMapper fieldMapper;
	
	@Override
	public TcField getFieldById(String fldNo) {
		return fieldMapper.getFieldById(fldNo);
	}

	@Override
	public Page<TcField> getFieldPage(Map<String, Object> params) {
		return fieldMapper.getFieldList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TcField> getFieldList(Map<String, Object> params) {
		return fieldMapper.getFieldList(params);
	}
	
	@Override
	@AutoLogMethod(value="创建自定义字段")
	public void createField(TcField field) {
		field.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		fieldMapper.insert(field);
	}

	@Override
	@AutoLogMethod(value="修改自定义字段")
	public void updateField(Map<String,Object> params){
		params.put("lastUpdate", DateUtil.getCurrentDateTimeAsString());
		fieldMapper.updateByPKSelective(params);
	}

	@Override
	@AutoLogMethod(value="删除自定义字段")
	public void deleteField(String[] fldNos) {
		for (int i = 0; i < fldNos.length; i++) {
			fieldMapper.deleteByPrimaryKey(fldNos[i]);
		}
	}

	@Override
	public List<TcField> getChoosedFieldListForPrdNo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return this.fieldMapper.getChoosedFieldListForPrdNo(map);
	}


	/***
	 * ----以上为对外服务-------------------------------
	 */
}
