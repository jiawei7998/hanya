package com.singlee.capital.base.trade.model;

import java.io.Serializable;
/**
 * 参与方-交易 关联表
 * @author SINGLEE
 *
 */
public class TdProductParticipant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//交易流水
	private String dealNo;
	//版本
	private String version;
	//是否有效
	private String isActive;
	//参与方类型
	private String partyId;
	//客户编号
	private String partyCode;
	//客户名称
	private String partyName;
	//准入级别
	private String accessLevel;
	//受托方类型
	private String trusteeType;
	//客户行业
	private String industryType;
	//所在地区
	private String location;
	//联系人1
	private String partyContacts1;
	//联系人2
	private String partyContacts2;
	
	private String guaName;
	
	public String getGuaName() {
		return guaName;
	}
	public void setGuaName(String guaName) {
		this.guaName = guaName;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyCode() {
		return partyCode;
	}
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getAccessLevel() {
		return accessLevel;
	}
	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}
	public String getTrusteeType() {
		return trusteeType;
	}
	public void setTrusteeType(String trusteeType) {
		this.trusteeType = trusteeType;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPartyContacts1() {
		return partyContacts1;
	}
	public void setPartyContacts1(String partyContacts1) {
		this.partyContacts1 = partyContacts1;
	}
	public String getPartyContacts2() {
		return partyContacts2;
	}
	public void setPartyContacts2(String partyContacts2) {
		this.partyContacts2 = partyContacts2;
	}
	@Override
	public String toString() {
		return "TdProductParticipant [dealNo=" + dealNo + ", version="
				+ version + ", isActive=" + isActive + ", partyId=" + partyId
				+ ", partyCode=" + partyCode + ", partyName=" + partyName
				+ ", accessLevel=" + accessLevel + ", trusteeType="
				+ trusteeType + ", industryType=" + industryType
				+ ", location=" + location + ", partyContacts1="
				+ partyContacts1 + ", partyContacts2=" + partyContacts2 + "]";
	}
	
}
