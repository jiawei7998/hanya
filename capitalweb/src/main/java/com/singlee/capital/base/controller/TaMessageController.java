package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaMessage;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/TaMessageController")
public class TaMessageController extends CommonController{
	@Resource
	TaMessageService taMessageService;
	
	@ResponseBody
	@RequestMapping(value = "/selectTaMessage")
	public RetMsg<PageInfo<TaMessage>> selectTaMessage(@RequestBody Map<String,Object> params) throws RException {
		params.put("userId", SlSessionHelper.getUserId());
		Page<TaMessage> page = taMessageService.selectTaMessage(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateTaMessage")
	public RetMsg<Serializable> updateTaMessage(@RequestBody Map<String,Object> params) throws RException {
		taMessageService.updateTaMessage(params);
		return RetMsgHelper.ok();
	}
}
