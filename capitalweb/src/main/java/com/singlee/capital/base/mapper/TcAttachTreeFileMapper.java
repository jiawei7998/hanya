package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcAttachTreeFile;
import com.singlee.capital.common.spring.mybatis.MyMapper;

/**
 * 
 * @author liuxy
 *
 */
public interface TcAttachTreeFileMapper extends MyMapper<TcAttachTreeFile> {
	
	public List<TcAttachTreeFile> searchTcAttachTreeFile(Map<String,Object> ref);
	public int insertTcAttachTreeFilebatch(List<TcAttachTreeFile> list);
	public int deleteTcAttachTreeFileByDealNo(Map<String,Object> ref);
}