package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_SETTL_DATE")
public class TtSettlDate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String settlDate;
	private String settlFlag;
	public String getSettlDate() {
		return settlDate;
	}
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}
	public String getSettlFlag() {
		return settlFlag;
	}
	public void setSettlFlag(String settlFlag) {
		this.settlFlag = settlFlag;
	}
	
}
