package com.singlee.capital.base.participant.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.participant.model.TcProductPartField;
import com.singlee.capital.base.participant.pojo.ProductPartFieldPojo;
import com.singlee.capital.base.participant.service.TcProductPartFieldService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
@Controller
@RequestMapping("/TcProductPartFieldController")
public class TcProductPartFieldController extends CommonController {
	@Autowired
	TcProductPartFieldService tcProductPartFieldService;
	
	/**
	 * 查询产品参与方字段配置
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductPartFieldList")
	public RetMsg<PageInfo<TcProductPartField>> searchProductPartFieldList(@RequestBody Map<String, Object> params) {
		Page<TcProductPartField> page = tcProductPartFieldService.getProductPartFieldList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除产品参与方字段配置
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteProductPartField")
	public RetMsg<Serializable> deleteProductPartField(@RequestBody Map<String, Object> map) {
		tcProductPartFieldService.deleteProductPartField(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增产品参与方字段配置
	 * @param productPartFieldPojo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addProductPartField")
	public RetMsg<Serializable> addProductPartField(@RequestBody ProductPartFieldPojo productPartFieldPojo) {
		tcProductPartFieldService.addProductPartField(productPartFieldPojo);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新产品参与方字段配置
	 * @param productPartFieldPojo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateProductPartField")
	public RetMsg<Serializable> updateProductPartField(@RequestBody ProductPartFieldPojo productPartFieldPojo) {
		tcProductPartFieldService.updateProductPartField(productPartFieldPojo);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchFieldPage")
	public RetMsg<PageInfo<TcField>> searchFieldPage(@RequestBody Map<String,Object> params){
		return RetMsgHelper.ok(tcProductPartFieldService.getFieldList(params));
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchFieldList")
	public RetMsg<List<TcField>> searchFieldList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(tcProductPartFieldService.geChooseFieldList(map));
	}
}
