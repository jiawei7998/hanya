package com.singlee.capital.base.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TtMktCalendarsDateMapper;
import com.singlee.capital.base.model.TtMktCalendarsDate;
import com.singlee.capital.base.service.CalendarsDateService;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRq;
import com.singlee.capital.interfacex.model.TCoHolidayAllInqRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
@SuppressWarnings("rawtypes")
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public  class CalendarsDateServiceImpl implements CalendarsDateService {
	@Autowired
	private TtMktCalendarsDateMapper calendarDateMapper;

	@Autowired
	private SocketClientService socketClientService;
	
	@Autowired
	private BatchDao batchDao;

	@Override
	public String insertTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate) {
		int t = calendarDateMapper.queryTtMktCalendarDateById(ttMktCalendarDate);
		if(t != 0 ){
			JY.info(MsgUtils.getMessage("error.trade.0002"));
			return "日期("+ttMktCalendarDate.getCalDate()+"),"+"日历代码("+ttMktCalendarDate.getCalCode()+")已经存在";
		}else{
			calendarDateMapper.insertTtMktCalendarDate(ttMktCalendarDate);
			return "000";
		}
	}

	@Override
	public void deleteTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate) {
		calendarDateMapper.deleteTtMktCalendarDate(ttMktCalendarDate);
	}

	@Override
	public void updateTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate) {
		calendarDateMapper.updateTtMktCalendarDate(ttMktCalendarDate);
	}

	@Override
	public Page<TtMktCalendarsDate> TtMktCalendarDatePage(
			Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		Page<TtMktCalendarsDate> params = calendarDateMapper.TtMktCalendarDatePage(param, rowBounds);
		return params;
	}

	
	
	@Override
	public List<TtMktCalendarsDate> queryTtMktCalendarDate() {
		List<TtMktCalendarsDate> list = calendarDateMapper.queryTtMktCalendarDate();
		return list;
	}
	
	@Override
	public List<TtMktCalendarsDate> queryListTtMktCalendarDate() {
		List<TtMktCalendarsDate> list = calendarDateMapper.queryListTtMktCalendarDate();
		return list;
	}
	
	@Override
	public boolean updateCalendarService(String countryCode, String workYear)
			throws Exception {
		// TODO Auto-generated method stub
		TCoHolidayAllInqRq request = new TCoHolidayAllInqRq();
		CommonRqHdr commonRqHdr = new CommonRqHdr();
		commonRqHdr.setChannelId("S89");
		commonRqHdr.setRqUID(UUID.randomUUID().toString());
		commonRqHdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		commonRqHdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
		request.setCommonRqHdr(commonRqHdr);
		request.setCountryCode(countryCode);
		request.setWorkYear(workYear);
		TCoHolidayAllInqRs response = socketClientService.t24TCoHolidayAllInqRequest(request);
		List<TtMktCalendarsDate> ttMktCalendarsDateList = new ArrayList<TtMktCalendarsDate>();
		TtMktCalendarsDate date = null;
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		System.out.println(response.getCommonRsHdr().getServerStatusCode());
		if(!response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){return false;}
		int days = 0;
		Map<?, ?> map = BeanUtils.describe(response);
		Iterator<?> iter = map.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry)iter.next();
			if("mth".equals(((String)entry.getKey()).substring(0,3).toLowerCase())){
				String key = ((String)entry.getKey()).substring(3);
				String value =(String)entry.getValue();
				calendar.setTime(format.parse(workYear+"-"+key));
				days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
				
				for(int i=1; i<=days; i++){
					date = new TtMktCalendarsDate();
					date.setCalDate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyyMMdd").parse(workYear+key+i)));
					date.setCalCode(countryCode);
					date.setCalFlag("H".equals(String.valueOf(value.charAt(i-1)))?"0":"1");//H 0 表示假期 W 1 工作日
					ttMktCalendarsDateList.add(date);
				}
			}
		}
		TtMktCalendarsDate ttMktCalendarsDate = new TtMktCalendarsDate();
		ttMktCalendarsDate.setCalCode(countryCode);
		calendarDateMapper.deleteTtMktCalendarDate(ttMktCalendarsDate);
		batchDao.batch("com.singlee.capital.base.mapper.TtMktCalendarsDateMapper.insertTtMktCalendarDate", ttMktCalendarsDateList);
		
		return true;
	}

	@Override
	public List<TtMktCalendarsDate> getTtMktCalendarByMonth(Map<String, Object> param) throws Exception {
		Date bizDate=CalendarUtil.getUtilDate(param.get("bizDate").toString().trim(),"yyyy-MM-dd");
		//Date bizDate=new SimpleDateFormat("yyyy-MM-dd").parse(param.get("bizDate").toString());
		param.put("startDate", CalendarUtil.getDateStr(CalendarUtil.getFirstDayOfMonth(bizDate),"yyyy-MM-dd"));
		param.put("endDate", CalendarUtil.getDateStr(CalendarUtil.getLastDayOfMonth(bizDate),"yyyy-MM-dd"));
		
		//param.put("startDate", param.get("bizDate").toString().substring(0, 4)+"-01-01");
		//param.put("endDate",param.get("bizDate").toString().substring(0, 4)+"-12-31");
		
		List<TtMktCalendarsDate> list = calendarDateMapper.getTtMktCalendarByMonth(param);
		return list;
	}

	@Override
	public Page<TtMktCalendarsDate> TtMktCalendarDatePage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int queryTtMktCalendarDateById(TtMktCalendarsDate ttMktCalendarDate) {
		int count = calendarDateMapper.queryTtMktCalendarDateById(ttMktCalendarDate);
		return count;
	}

	
}
