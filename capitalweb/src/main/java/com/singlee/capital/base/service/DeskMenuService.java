package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaDeskMenu;
import com.singlee.capital.base.model.vo.MenuDeskTopVo;
import com.singlee.capital.base.model.vo.MenuVo;
import com.singlee.capital.base.model.vo.TaDeskMenuVo;
import com.singlee.capital.base.model.vo.TreeMenuVo;
/**
 * TA_DESK_MENU接口服务方法
 * @author SINGLEE
 *
 */
public interface DeskMenuService {
	/**
	 * 获取台子信息
	 * @param map
	 * @return
	 */
	List<TaDeskMenuVo> getAllTaDeskMenuByIds(Map<String, Object> map);
	void updateDeskBranchId(Map<String, Object> map);
	void saveDeskByBranchId(Map<String, Object> map);
	TaDeskMenu getDeskMenuByBranchIdAndDeskId(Map<String, Object> map);
	/**
	 * 获取所有二级台子对应的资源列表
	 * @param paramsMap
	 * @return
	 */
	List<MenuDeskTopVo> getAllMenuDeskTopVos(Map<String, Object> paramsMap);
	
	/**
	 * 获得交易资源
	 * @param paramsMap
	 * @return
	 */
	List<MenuDeskTopVo> getDeskTradeMenuPage(Map<String, Object> paramsMap);
	Page<TaDeskMenu> getDeskMenuByRoleIdAndInstId(Map<String,Object> params);
	Page<TaDeskMenu> getAllMenuByBranchId(Map<String,Object> params);
	/**
	 * 获取所有有效的菜单资源
	 * @return
	 */
	List<MenuVo> getAllModuleWhitOutDeskId(Map<String, Object> paramsMap);
	/**
	 * 获得树形折叠菜单
	 * @param paramsMap
	 * @return
	 */
	public List<TreeMenuVo> getAllTreeMenuVos(Map<String, Object> paramsMap) ;
}
