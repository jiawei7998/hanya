package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @projectName 同业业务管理系统
 * @className 表单字段分组字段映射
 * @description TODO
 * @author 倪航
 * @createDate 2016-8-2 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_FORM_FIELDSET_FIELD_MAP")
public class TcFormFieldsetField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -501266280692839114L;
	/**
	 * 表单编号 序列自动生成
	 */
	@Id
	private String formNo;
	/**
	 * 字段分组编号 序列自动生成
	 */
	@Id
	private String fdsNo;
	/**
	 * 字段编号 序列自动生成
	 */
	@Id
	private String fldNo;
	/**
	 * 表单索引 生成表单时的顺序
	 */
	private Integer formIndex;
	/**
	 * 表单名称 表单中展示的名称
	 */
	private String formName;
	/**
	 * 表单提示 表单中的提示信息
	 */
	private String formTips;
	/**
	 * 是否必填 表单中设置必填校验及星号标志
	 */
	private String isMandatory;
	/**
	 * 是否禁用
	 */
	private String isDisabled;
	/**
	 * 关键字 数据字典的字典类型编号
	 */
	private String defKey;
	/**
	 * 默认值
	 */
	private String defValue;
	/**
	 * 默认文本
	 */
	private String defText;
	/**
	 * 关联附件树
	 */
	private String attachTreeTemp;
	
	public String getFormNo() {
		return formNo;
	}
	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}
	public String getFdsNo() {
		return fdsNo;
	}
	public void setFdsNo(String fdsNo) {
		this.fdsNo = fdsNo;
	}
	public String getFldNo() {
		return fldNo;
	}
	public void setFldNo(String fldNo) {
		this.fldNo = fldNo;
	}
	public Integer getFormIndex() {
		return formIndex;
	}
	public void setFormIndex(Integer formIndex) {
		this.formIndex = formIndex;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormTips() {
		return formTips;
	}
	public void setFormTips(String formTips) {
		this.formTips = formTips;
	}
	public String getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getIsDisabled() {
		return isDisabled;
	}
	public void setIsDisabled(String isDisabled) {
		this.isDisabled = isDisabled;
	}
	public String getDefKey() {
		return defKey;
	}
	public void setDefKey(String defKey) {
		this.defKey = defKey;
	}
	public String getDefValue() {
		return defValue;
	}
	public void setDefValue(String defValue) {
		this.defValue = defValue;
	}
	public String getDefText() {
		return defText;
	}
	public void setDefText(String defText) {
		this.defText = defText;
	}
	public String getAttachTreeTemp() {
		return attachTreeTemp;
	}
	public void setAttachTreeTemp(String attachTreeTemp) {
		this.attachTreeTemp = attachTreeTemp;
	}
	
}
