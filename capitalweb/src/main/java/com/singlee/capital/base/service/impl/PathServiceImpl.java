package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.acc.mapper.TtTrdAccAdjustMapper;
import com.singlee.capital.acc.model.TtTrdAccAdjust;
import com.singlee.capital.base.service.TrdAccAdjustService;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.counterparty.mapper.TtCounterPartyAccMapper;
import com.singlee.capital.counterparty.model.TtCounterPartyAcc;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;

@Service("pathService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PathServiceImpl implements TrdAccAdjustService,SlbpmCallBackInteface {
	
	@Resource
	private TtTrdAccAdjustMapper adjustMapper;
	
	@Resource
	private TtCounterPartyAccMapper ttCounterPartyAccMapper;
	
	@Resource
	private DayendDateService dayendDateService;

	/**
	 * 流程状态变更（流程审批回掉）
	 * @param flow_type 流程类型
	 * @param flow_id 流程编号
	 * @param serial_no 审批单号
	 * @param status 状态
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		TtTrdAccAdjust accAdj = new TtTrdAccAdjust();
		accAdj.setAdjId(serial_no);
		accAdj = adjustMapper.selectByPrimaryKey(accAdj);
		if(DictConstants.FlowType.PathApproveFlow.equals(flow_type)){
			accAdj.setStatus(status);
		}
		adjustMapper.updateByPrimaryKey(accAdj);
		if(DictConstants.ApproveStatus.New.equals(status)){
			
		}else if(DictConstants.ApproveStatus.WaitApprove.equals(status)){
			
		}else if(DictConstants.ApproveStatus.ApprovedNoPass.equals(status)){
			
		}else if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
			//审批通过，把信息保存到账户表中
			String operationType = accAdj.getOperationType();
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("adjId", accAdj.getAdjId());
			map.put("party_id", accAdj.getAdjId());
			map.put("open_bank_large_accno", DictConstants.OperationType.delete.contains(operationType)?accAdj.getOpenBankCodeOld():accAdj.getOpenBankCode());
			map.put("bankacid", DictConstants.OperationType.delete.contains(operationType)?accAdj.getBankaccidOld():accAdj.getBankaccid());
			TtCounterPartyAcc accInfo = ttCounterPartyAccMapper.selectCounterPartyAcc(map);
			if(DictConstants.OperationType.delete.contains(operationType)){
				JY.require(accInfo != null, "找不到目标交易对手账户");
				accInfo.setParty_id(accAdj.getPartyId());
				accInfo.setOpen_bank_large_accno(accAdj.getOpenBankCode());
				accInfo.setOpen_bank_name(accAdj.getOpenBankName());
				accInfo.setCny(accAdj.getCurrency());
				accInfo.setBankaccid(accAdj.getBankaccid());
				accInfo.setBankaccname(accAdj.getBankaccname());
				accInfo.setUpdatetime(DateUtil.getCurrentDateTimeAsString());
				ttCounterPartyAccMapper.delete(accInfo);
			}else {
				JY.require(accInfo == null, "数据库内已有相同的目标交易对手账户");
				accInfo = new TtCounterPartyAcc();
				accInfo.setParty_id(accAdj.getPartyId());
				accInfo.setOpen_bank_large_accno(accAdj.getOpenBankCode());
				accInfo.setOpen_bank_name(accAdj.getOpenBankName());
				accInfo.setStatus(DictConstants.Status.Enabled);
				accInfo.setBankaccid(accAdj.getBankaccid());
				accInfo.setBankaccname(accAdj.getBankaccname());
				accInfo.setUpdatetime(DateUtil.getCurrentDateTimeAsString());
				accInfo.setCny(accAdj.getCurrency());
				if(DictConstants.OperationType.add.contains(operationType)){
					accInfo.setStatus(DictConstants.YesNo.YES);
					accInfo.setOpen_date(dayendDateService.getDayendDate());
					ttCounterPartyAccMapper.insert(accInfo);
				}else if (DictConstants.OperationType.edit.contains(operationType)){
					accInfo.setBankaccidOld(accAdj.getBankaccidOld());
					ttCounterPartyAccMapper.updateCounterPartyAcc(accInfo);
				}
			} 
			
		}else if(DictConstants.ApproveStatus.Cancle.equals(status)){
			
		}
		
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		TtTrdAccAdjust accAdj = new TtTrdAccAdjust();
		accAdj.setAdjId(serial_no);
		accAdj = adjustMapper.selectByPrimaryKey(accAdj);
		return accAdj;
	}

}
