package com.singlee.capital.base.service;

import com.singlee.capital.base.model.TcAttachTreeRef;


/**
 * @projectName 
 * @className 附件树服务
 * @description TODO
 * @author 刘小燚
 * @createDate 2017-6-3 下午3:25:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcAttachTreeRefService {

	public int insertTcAttachTreeFef(TcAttachTreeRef ref);
	
	public int updateTcAttachTreeFef(TcAttachTreeRef ref);
	
	public int deleteTcAttachTreeFef(TcAttachTreeRef ref);
	
	public TcAttachTreeRef getTcAttachTreeRefById(TcAttachTreeRef ref);
	
}
