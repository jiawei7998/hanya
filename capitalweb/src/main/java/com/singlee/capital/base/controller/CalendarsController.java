package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TtMktCalendarsDate;
import com.singlee.capital.base.service.CalendarsDateService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

/**
 * 交易日历控制器层
 * 
 * @author lihuabing
 *
 */
@Controller
@RequestMapping(value = "/CalendarsController")
public class CalendarsController extends CommonController {
	@Autowired
	private CalendarsDateService calendarsDateService;

	@ResponseBody
	@RequestMapping(value = "/uploadCalendarService")
	public RetMsg<Serializable> updateCalendarService(@RequestBody Map<String, Object> map) {

		StringBuffer sb = new StringBuffer();
		try {
			String workYear = (String) map.get("workYear");
			String countryCode = (String) map.get("countryCode");
			System.out.println("==========" + workYear + countryCode);
			boolean flag = calendarsDateService.updateCalendarService(countryCode, workYear);
			if (!flag) {
				sb.append("接口停用或查询出现异常！");
			} else {
				sb.append("");
			}

		} catch (Exception e) {
			JY.error(e);
			sb.append(e.getMessage());
		}
		return RetMsgHelper.ok(StringUtils.isEmpty(sb.toString()) ? "导入成功" : sb.toString());
	}

	/**
	 * 分页查询交易日历列表
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pageCalendarDate")
	public RetMsg<PageInfo<TtMktCalendarsDate>> pageCalendarDate(@RequestBody Map<String, Object> map) {
		Page<TtMktCalendarsDate> list = calendarsDateService.TtMktCalendarDatePage(map);
		return RetMsgHelper.ok(list);
	}

	/*
	 * 新增交易日历
	 * 
	 * @param ttMktCalendarDate
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtMktCalendarDate")
	public RetMsg<Serializable> insertTtMktCalendarDate(@RequestBody TtMktCalendarsDate ttMktCalendarDate) {
		String mgs = calendarsDateService.insertTtMktCalendarDate(ttMktCalendarDate);
		if ("000".equalsIgnoreCase(mgs)) {
			return RetMsgHelper.ok();
		} else {
			return RetMsgHelper.simple("001", mgs);
		}
	}

	/**
	 * 删除交易日历
	 * 
	 * @param ttMktCalendarDate
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTtMktCalendarDate")
	public RetMsg<Serializable> deleteTtMktCalendarDate(@RequestBody Map<String, Object> map) {
		String calDate = ParameterUtil.getString(map, "calDate", "");
		String calCode = ParameterUtil.getString(map, "calCode", "");
		TtMktCalendarsDate ttMktCalendarsDate = new TtMktCalendarsDate();
		ttMktCalendarsDate.setCalCode(calCode);
		ttMktCalendarsDate.setCalDate(calDate);
		calendarsDateService.deleteTtMktCalendarDate(ttMktCalendarsDate);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改交易日历
	 * 
	 * @param ttMktCalendarDate
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTtMktCalendarDate")
	public RetMsg<Serializable> updateTtMktCalendarDate(@RequestBody TtMktCalendarsDate ttMktCalendarsDate) {
		calendarsDateService.updateTtMktCalendarDate(ttMktCalendarsDate);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/queryTtMktCalendarDate")
	public RetMsg<List<TtMktCalendarsDate>> queryTtMktCalendarDate() {
		List<TtMktCalendarsDate> list = calendarsDateService.queryTtMktCalendarDate();
		return RetMsgHelper.ok(list);
	}

	@ResponseBody
	@RequestMapping(value = "/queryListTtMktCalendarDate")
	public RetMsg<List<TtMktCalendarsDate>> queryListTtMktCalendarDate() {
		List<TtMktCalendarsDate> list = calendarsDateService.queryListTtMktCalendarDate();
		return RetMsgHelper.ok(list);
	}

	@ResponseBody
	@RequestMapping(value = "/getTtMktCalendarByMonth")
	public RetMsg<List<TtMktCalendarsDate>> getTtMktCalendarByMonth(@RequestBody Map<String, Object> map)
			throws Exception {
		List<TtMktCalendarsDate> list = calendarsDateService.getTtMktCalendarByMonth(map);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 新增交易日历
	 * 
	 * @param ttMktCalendarDate
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtMktCalendarDates")
	public RetMsg<Serializable> insertTtMktCalendarDates(@RequestBody String[] str) {
		String mgs = null;
		for (int i = 0; i < str.length; i++) 
		{
			TtMktCalendarsDate calendarsDate = new TtMktCalendarsDate();
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String calDate1 = str[i];
				Date date = format.parse(calDate1);
				String calDate = format.format(date.getTime());
				calendarsDate.setCalDate(calDate);
				calendarsDate.setCalCode("CHINA_IB");
				calendarsDate.setCalFlag("0");
				int count = calendarsDateService.queryTtMktCalendarDateById(calendarsDate);
				if(count == 0){
					mgs = calendarsDateService.insertTtMktCalendarDate(calendarsDate);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}// end for
		if (mgs == null || "000".equalsIgnoreCase(mgs)) {
			return RetMsgHelper.ok();
		} else {
			return RetMsgHelper.simple("001", mgs);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/deleteTtMktCalendarDates")
	public RetMsg<Serializable> deleteTtMktCalendarDates(@RequestBody String[] str) {
		for (int i = 0; i < str.length; i++) {
			try {
				TtMktCalendarsDate ttMktCalendarsDate = new TtMktCalendarsDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String calDate1 = str[i];
				Date date1 = format.parse(calDate1);
				String calDate = format.format(date1.getTime());
				ttMktCalendarsDate.setCalDate(calDate);
				ttMktCalendarsDate.setCalCode("CHINA_IB");
				int count = calendarsDateService.queryTtMktCalendarDateById(ttMktCalendarsDate);
				if(count != 0){
					calendarsDateService.deleteTtMktCalendarDate(ttMktCalendarsDate);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		return RetMsgHelper.ok();
	}

}
