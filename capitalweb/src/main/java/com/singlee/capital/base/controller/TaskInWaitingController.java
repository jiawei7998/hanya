package com.singlee.capital.base.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaskInMap;
import com.singlee.capital.base.model.TaskInWaiting;
import com.singlee.capital.base.service.TaskInWaitingService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/TaskInWaitingController")
public class TaskInWaitingController extends CommonController{
	
	@Resource
	TaskInWaitingService taskInWaitingService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTaskInWaitingPage")
	public RetMsg<PageInfo<TaskInWaiting>> searchTaskInWaitingPage(@RequestBody Map<String, Object> params){
		Page<TaskInWaiting> page =new Page<TaskInWaiting>();
		try {
			params.put("userId", SlSessionHelper.getUserId());
			page = taskInWaitingService.searchTaskInWaitingPage(params);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTaskInWaitingCount")
	public RetMsg<TaskInMap> getTaskInWaitingCount(@RequestBody Map<String, Object> params){
		TaskInMap map = new TaskInMap();
		try {
			params.put("userId", SlSessionHelper.getUserId());
			map = taskInWaitingService.getTaskInWaitingCount(params);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(map);
	}
	
	
}
