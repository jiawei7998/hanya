package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;

/**
 * @projectName 同业业务管理系统
 * @className 产品
 * @description TODO
 * @author 倪航
 * @createDate 2016-5-12 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_PRODUCT")
public class TcProduct implements Serializable ,TreeInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5572828216838303298L;
	/**
	 * 产品编号 序列自动生成
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_PRODUCT.NEXTVAL FROM DUAL")
	private String prdNo;
	/**
	 * 所属机构
	 */
	private String prdInst;
	/**
	 * 产品名称
	 */
	private String prdName;
	/**
	 * 产品期限
	 */
	private String prdTerm;
	/**
	 * 产品类型
	 */
	private String prdType;
	/**
	 * 产品性质 1-资产 2-负债
	 */
	private String prdProp;
	/**
	 * 是否模板 0-否 1-是
	 */
	private String isTemplate;
	/**
	 * 启用标志 0-禁用 1-启用
	 */
	private String useFlag;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	/**
	 * 模块ID
	 */
	private String moduleId;
	/**
	 * 模块父节点ID
	 */
	private String modulePid;
	/**
	 * 模块名称
	 */
	private String moduleName;
	/**
	 * 排序
	 */
	private Integer moduleSortno;
	/**
	 * 模块URL
	 */
	private String moduleUrl;
	/**
	 * 银行所属id
	 */
	private String branchId;
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	//0-不出账,1-出账务 
	private String isAcup;
	//出账币种
	private String acupCcys;
	
	public String getIsAcup() {
		return isAcup;
	}

	public void setIsAcup(String isAcup) {
		this.isAcup = isAcup;
	}

	public String getAcupCcys() {
		return acupCcys;
	}

	public void setAcupCcys(String acupCcys) {
		this.acupCcys = acupCcys;
	}

	/**
	 * 所属机构名称
	 */
	@Transient
	private String prdInstName;
	/**
	 * 产品类型名称
	 */
	@Transient
	private String prdTypeName;
	/**
	 * 产品大类编号
	 */
	@Transient
	private String prdRootTypeNo;
	/**
	 * 产品大类名称
	 */
	@Transient
	private String prdRootTypeName;
	/**
	 * 预定义字段集合
	 */
	@Transient
	private List<TcForm> forms;
	
	private String approveType;//1-审批+正式  2-正式
	
	

	public String getApproveType() {
		return approveType;
	}

	public void setApproveType(String approveType) {
		this.approveType = approveType;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPrdInst() {
		return prdInst;
	}

	public void setPrdInst(String prdInst) {
		this.prdInst = prdInst;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPrdTerm() {
		return prdTerm;
	}

	public void setPrdTerm(String prdTerm) {
		this.prdTerm = prdTerm;
	}

	public String getPrdType() {
		return prdType;
	}

	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}

	public String getPrdProp() {
		return prdProp;
	}

	public void setPrdProp(String prdProp) {
		this.prdProp = prdProp;
	}

	public String getIsTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(String isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	
	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getModulePid() {
		return modulePid;
	}

	public void setModulePid(String modulePid) {
		this.modulePid = modulePid;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getModuleSortno() {
		return moduleSortno;
	}

	public void setModuleSortno(Integer moduleSortno) {
		this.moduleSortno = moduleSortno;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getPrdInstName() {
		return prdInstName;
	}

	public void setPrdInstName(String prdInstName) {
		this.prdInstName = prdInstName;
	}

	public String getPrdTypeName() {
		return prdTypeName;
	}

	public void setPrdTypeName(String prdTypeName) {
		this.prdTypeName = prdTypeName;
	}

	public String getPrdRootTypeNo() {
		return prdRootTypeNo;
	}

	public void setPrdRootTypeNo(String prdRootTypeNo) {
		this.prdRootTypeNo = prdRootTypeNo;
	}

	public String getPrdRootTypeName() {
		return prdRootTypeName;
	}

	public void setPrdRootTypeName(String prdRootTypeName) {
		this.prdRootTypeName = prdRootTypeName;
	}

	public List<TcForm> getForms() {
		return forms;
	}

	public void setForms(List<TcForm> forms) {
		this.forms = forms;
	}
	@Override
	public String getText(){
		return this.prdName;
	}
	@Override
	public String getValue(){
		return this.prdNo;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return getPrdNo();
	}

	@Override
	public String getParentId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void children(List<? extends TreeInterface> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<? extends TreeInterface> children() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSort() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getIconCls() {
		// TODO Auto-generated method stub
		return null;
	}
}
