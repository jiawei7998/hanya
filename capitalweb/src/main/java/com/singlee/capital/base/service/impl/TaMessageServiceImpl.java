package com.singlee.capital.base.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TaMessageMapper;
import com.singlee.capital.base.model.TaMessage;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.fund.model.ProductApproveFund;
import com.singlee.capital.fund.model.TdDurationFundCashdiv;
import com.singlee.capital.fund.model.TdDurationFundRedeem;
import com.singlee.capital.fund.model.TdDurationFundReinvest;
import com.singlee.capital.fund.model.TdDurationFundTrans;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;
import com.singlee.capital.trade.mapper.TdProductApproveOverDraftMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.service.FlowQueryService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaMessageServiceImpl implements TaMessageService {

	@Resource
	TaMessageMapper taMessageMapper;
	/** Activiti任务接口 */
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	@Autowired
	HistoryService historyService;
	@Autowired
	FlowQueryService flowQueryService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TdProductApproveOverDraftMapper productApproveOverDraftMapper;

	@Override
	public int deleteTaMessage(Map<String, Object> params) {
		return taMessageMapper.deleteTaMessage(params);
	}

	@Override
	public Page<TaMessage> selectTaMessage(Map<String, Object> params) {
		// 获得机构向下的所有机构
		List<String> userInstIdList = new ArrayList<String>();
		TtInstitution ttInstitution = SlSessionHelper.getInstitution();
		HashMap<String, Object> map2 = new HashMap<String, Object>();
		map2.put("instId", ttInstitution.getInstId());
		List<TtInstitution> institutions = institutionService.searchChildrenInst(map2);
		userInstIdList.add(ttInstitution.getInstId());
		for (TtInstitution institution : institutions) {
			userInstIdList.add(institution.getInstId());
		}
		params.put("userInstIdList", userInstIdList);
		List<String> prdNos = Arrays.asList(ParameterUtil.getString(params, "prdNos", "").split(","));
		params.put("prdNoArrays", prdNos);
		return taMessageMapper.selectTaMessage(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public int updateTaMessage(Map<String, Object> params) {
		return taMessageMapper.updateTaMessage(params);
	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public int insertTaMessage(Map<String, Object> params) {
		String callBackService = ParameterUtil.getString(params, "callBackService", "");
		String dealno = ParameterUtil.getString(params, "serial_no", "");
		String flow_id = ParameterUtil.getString(params, "flow_id", "");
		String flow_type = ParameterUtil.getString(params, "flow_type", "");
		String task_id = ParameterUtil.getString(params, "task_id", "");
		String type = ParameterUtil.getString(params, "type", "");
		int flag = 0;
		String nextTask_id = "";
		TtFlowSerialMap flowSerMapHis = ttFlowSerialMapMapper.get(null, dealno, null, 0);
		String processInstanceId = String.valueOf(flowSerMapHis.getExecution());
		List<HistoricTaskInstance> lst_his = historyService.createHistoricTaskInstanceQuery()
				.processInstanceId(processInstanceId).list();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < lst_his.size(); i++) {
			if (lst_his.get(i).getEndTime() == null) {
				nextTask_id = lst_his.get(i).getId();
			}
		}
		// 提交审批
		if ("approveCommit".equals(type)) {
			SlbpmCallBackInteface callBackInteface = SpringContextHolder.getBean(callBackService);
			Object modelObject = callBackInteface.getBizObj(flow_type, flow_id, dealno);
			if (null != modelObject) {
				Map<String, Object> messageMap = BeanUtil.beanToHashMap(modelObject);
				messageMap.put("dealNo", dealno);
				messageMap.put("isProcessed", "0");
				messageMap.put("isRead", "0");
				messageMap.put("taskId", nextTask_id);
				messageMap.put("flowId", flow_id);
				messageMap.put("lstDate", df.format(new Date()));
				// 基金交换
				if (modelObject instanceof TdDurationFundTrans) {
					messageMap.put("amt", messageMap.get("turnInAmt"));
					messageMap.put("dealType", "2");
					messageMap.put("prdType", "5");// 基金交换
					messageMap.put("fundCode", messageMap.get("outFundCode"));// 基金代码
					messageMap.put("fundName", messageMap.get("inFundCode"));// 基金名称
					messageMap.put("qty", messageMap.get("turnInQty"));// 基金份额
					messageMap.put("cashAmt", 0.00);// 基金红利
				}
				// 红利再投
				if (modelObject instanceof TdDurationFundReinvest) {
					messageMap.put("amt", messageMap.get("confirmQty"));
					messageMap.put("dealType", "2");
					messageMap.put("prdType", "4");// 基金交换
					messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
					messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
					messageMap.put("qty", messageMap.get("utQty"));// 基金份额
					messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
				}
				// 基金申购
				if (modelObject instanceof ProductApproveFund) {
					messageMap.put("dealType", "2");
					messageMap.put("prdType", "3");// 基金申购
					messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
					messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
					messageMap.put("qty", messageMap.get("shareAmt"));// 基金份额
					messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
				}
				// 基金赎回
				if (modelObject instanceof TdDurationFundRedeem) {
					messageMap.put("amt", messageMap.get("redeemAmt"));
					messageMap.put("dealType", "2");
					messageMap.put("prdType", "2");// 基金赎回
					messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
					messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
					messageMap.put("qty", messageMap.get("redeemQty"));// 基金份额
					messageMap.put("cashAmt", messageMap.get("dividendAmt"));// 基金红利
				}
				if (modelObject instanceof TdProductApproveMain) {
					if (Integer.parseInt(messageMap.get("prdNo").toString()) == 906) {
						TdProductApproveOverDraft overDraft = new TdProductApproveOverDraft();
						overDraft.setDealNo(messageMap.get("dealNo").toString());
						overDraft.setVersion(Integer.parseInt(messageMap.get("version").toString()));
						overDraft = productApproveOverDraftMapper.selectByPrimaryKey(overDraft);
						messageMap.put("amt", overDraft.getCreditAmt());
					}

				}
				// 现金分红
				if (modelObject instanceof TdDurationFundCashdiv) {
					messageMap.put("amt", messageMap.get("cashdivAmt"));
					messageMap.put("dealType", "2");
					messageMap.put("prdType", "1");// 现金分红
					messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
					messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
					messageMap.put("qty", messageMap.get("utQty"));// 基金份额
					messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
				}
				Map<String, Object> result = flowQueryService.getNextStepCandidatesList(flow_id, nextTask_id, dealno);
				String userListJson = ParameterUtil.getString(result, "userList", null);
				List<ApproveUser> list = (List<ApproveUser>) result.get("userList");

				String nextUser = "";// 下一环节处理人
				if (null != list) {
					for (int i = 0; i < list.size(); i++) {
						if (i == (list.size() - 1)) {
							nextUser += list.get(i).getUserId();
						} else {
							nextUser += list.get(i).getUserId() + ",";
						}
					}
				}
				messageMap.put("approveUser", nextUser);
				TaMessage taMessage = new TaMessage();
				BeanUtil.populate(taMessage, messageMap);
				flag = taMessageMapper.insert(taMessage);
			}
		}
		// 审批操作
		if ("approve".equals(type)) {
			// 判断是否未最后一个节点
			if (!"".equals(nextTask_id)) {
				TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, dealno, null, 0);
				// 获得当前流程实例的回调接口
				String status_change_listener = flowSerMap.getStatus_change_listener();
				dealno = flowSerMap.getSerial_no();
				flow_id = flowSerMap.getFlow_id();
				flow_type = flowSerMap.getFlow_type();
				SlbpmCallBackInteface callBackInteface = SpringContextHolder.getBean(status_change_listener);
				Object modelObject = callBackInteface.getBizObj(flow_type, flow_id, dealno);
				if (null != modelObject) {
					Map<String, Object> messageMap = BeanUtil.beanToHashMap(modelObject);
					messageMap.put("dealNo", dealno);
					messageMap.put("opType", "1");
					messageMap.put("isProcessed", "0");
					messageMap.put("isRead", "0");
					messageMap.put("taskId", nextTask_id);
					messageMap.put("flowId", flow_id);
					messageMap.put("lstDate", df.format(new Date()));
					// 基金交换
					if (modelObject instanceof TdDurationFundTrans) {
						messageMap.put("amt", messageMap.get("turnInAmt"));
						messageMap.put("dealType", "2");
						messageMap.put("prdType", "5");// 基金交换
						messageMap.put("fundCode", messageMap.get("outFundCode"));// 基金代码
						messageMap.put("fundName", messageMap.get("inFundCode"));// 基金名称
						messageMap.put("qty", messageMap.get("turnInQty"));// 基金份额
						messageMap.put("cashAmt", 0.00);// 基金红利
					}
					// 红利再投
					if (modelObject instanceof TdDurationFundReinvest) {
						messageMap.put("amt", messageMap.get("confirmQty"));
						messageMap.put("dealType", "2");
						messageMap.put("prdType", "4");// 基金交换
						messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
						messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
						messageMap.put("qty", messageMap.get("utQty"));// 基金份额
						messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
					}
					// 基金申购
					if (modelObject instanceof ProductApproveFund) {
						messageMap.put("dealType", "2");
						messageMap.put("prdType", "3");// 基金申购
						messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
						messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
						messageMap.put("qty", messageMap.get("shareAmt"));// 基金份额
						messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
					}
					// 基金赎回
					if (modelObject instanceof TdDurationFundRedeem) {
						messageMap.put("amt", messageMap.get("redeemAmt"));
						messageMap.put("dealType", "2");
						messageMap.put("prdType", "2");// 基金赎回
						messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
						messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
						messageMap.put("qty", messageMap.get("redeemQty"));// 基金份额
						messageMap.put("cashAmt", messageMap.get("dividendAmt"));// 基金红利
					}
					if (modelObject instanceof TdProductApproveMain) {
						if (Integer.parseInt(messageMap.get("prdNo").toString()) == 906) {
							TdProductApproveOverDraft overDraft = new TdProductApproveOverDraft();
							overDraft.setDealNo(messageMap.get("dealNo").toString());
							overDraft.setVersion(Integer.parseInt(messageMap.get("version").toString()));
							overDraft = productApproveOverDraftMapper.selectByPrimaryKey(overDraft);
							messageMap.put("amt", overDraft.getCreditAmt());
						}

					}
					// 现金分红
					if (modelObject instanceof TdDurationFundCashdiv) {
						messageMap.put("amt", messageMap.get("cashdivAmt"));
						messageMap.put("dealType", "2");
						messageMap.put("prdType", "1");// 现金分红
						messageMap.put("fundCode", messageMap.get("fundCode"));// 基金代码
						messageMap.put("fundName", messageMap.get("fundName"));// 基金名称
						messageMap.put("qty", messageMap.get("utQty"));// 基金份额
						messageMap.put("cashAmt", messageMap.get("cashdivAmt"));// 基金红利
					}
					Map<String, Object> result = flowQueryService.getNextStepCandidatesList(flow_id, nextTask_id,
							dealno);
					String userListJson = ParameterUtil.getString(result, "userList", null);
					List<ApproveUser> list = (List<ApproveUser>) result.get("userList");

					String nextUser = "";// 下一环节处理人
					if (null != list) {
						for (int i = 0; i < list.size(); i++) {
							if (i == (list.size() - 1)) {
								nextUser += list.get(i).getUserId();
							} else {
								nextUser += list.get(i).getUserId() + ",";
							}
						}
					}
					messageMap.put("approveUser", nextUser);
					TaMessage taMessage = new TaMessage();
					BeanUtil.populate(taMessage, messageMap);
					if (null == taMessageMapper.selectTaMessageByDealNoTaskId(messageMap)) {
						flag = taMessageMapper.insert(taMessage);
					}
					if (flag > 0) {
						// 将之前的待办消息置为已处理
						Map<String, Object> updatemap = new HashMap<String, Object>();
						updatemap.put("dealNo", dealno);
						updatemap.put("isProcessed", "1");
						updatemap.put("isRead", "1");
						updatemap.put("flowId", flow_id);
						updatemap.put("taskId", task_id);
						updatemap.put("approveStatus", taMessage.getApproveStatus());
						updatemap.put("lstDate", df.format(new Date()));
						taMessageMapper.updateTaMessage(updatemap);
					}
				}
			} else {
				// 将之前的待办消息置为已处理
				Map<String, Object> updatemap = new HashMap<String, Object>();
				updatemap.put("dealNo", dealno);
				updatemap.put("isProcessed", "1");
				updatemap.put("isRead", "1");
				updatemap.put("taskId", task_id);
				updatemap.put("approveStatus", "6");
				updatemap.put("lstDate", df.format(new Date()));
				taMessageMapper.updateTaMessage(updatemap);
			}
		}
		// 加签操作
		if ("invitationWithTask".equals(type)) {
			TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, dealno, null, 0);
			// 获得当前流程实例的回调接口
			String status_change_listener = flowSerMap.getStatus_change_listener();
			dealno = flowSerMap.getSerial_no();
			flow_id = flowSerMap.getFlow_id();
			flow_type = flowSerMap.getFlow_type();
			SlbpmCallBackInteface callBackInteface = SpringContextHolder.getBean(status_change_listener);
			Object modelObject = callBackInteface.getBizObj(flow_type, flow_id, dealno);
			if (null != modelObject) {
				Map<String, Object> messageMap = BeanUtil.beanToHashMap(modelObject);
				messageMap.put("dealNo", dealno);
				messageMap.put("opType", "1");
				messageMap.put("isProcessed", "0");
				messageMap.put("isRead", "0");
				messageMap.put("taskId", nextTask_id);
				messageMap.put("flowId", flow_id);
				messageMap.put("lstDate", df.format(new Date()));
				TaMessage taMessage = new TaMessage();
				BeanUtil.populate(taMessage, messageMap);
				if (null == taMessageMapper.selectTaMessageByDealNoTaskId(messageMap)) {
					flag = taMessageMapper.insert(taMessage);
				}
				if (flag > 0) {
					// 将之前的待办消息置为已处理
					Map<String, Object> updatemap = new HashMap<String, Object>();
					updatemap.put("dealNo", dealno);
					updatemap.put("isProcessed", "1");
					updatemap.put("isRead", "1");
					updatemap.put("flowId", flow_id);
					updatemap.put("taskId", task_id);
					updatemap.put("approveStatus", taMessage.getApproveStatus());
					updatemap.put("lstDate", df.format(new Date()));
					taMessageMapper.updateTaMessage(updatemap);
				}
			}
		}
		return flag;
	}

	@Override
	public int selectTaMessageCount(Map<String, Object> params) {
		return taMessageMapper.selectTaMessageCount(params);
	}

}
