package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.hrbextra.credit.model.HrbCreditCustProd;
import com.singlee.hrbextra.credit.model.HrbCreditLimitOccupy;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-2 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface ProductService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param prdNo - 产品主键
	 * @return 产品对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public TcProduct getProductById(String prdNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品对象列表
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public Page<TcProduct> getProductPage (Map<String, Object> params);
	
	/**
	 * 条件查询产品表单
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-10-10
	 */
	public List<TcProduct> getProductList(Map<String, Object> params);
	
	/**
	 * 新增
	 * 
	 * @param product - 产品对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void createProduct(TcProduct product);
	
	/**
	 * 修改
	 * 
	 * @param product - 产品对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void updateProduct(TcProduct product);
	
	/**
	 * 删除
	 * 
	 * @param prdNos - 产品主键列表
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void deleteProduct(String[] prdNos);
	
	/**
	 * 条件查询产品表单
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-10-10
	 */
	public List<TcFieldset> getProductFormFieldsetField(Map<String,Object> params);
	/**
	 * 获得参与方配置
	 * @param params
	 * @return
	 */
	public List<TcFieldset> getParticipantFormFieldsetField(Map<String, Object> params);
	
	/**
	 * 根据产品查询对象
	 * 
	 * @param i_name - 产品名称
	 * @return String
	 * @author Wuyc
	 * @date 2016-12-1
	 */
	public List<TcProduct> getProductByName(String prdName);

	/**
	 * 查询还未被选中的产品
	 * @param map
	 * @return
	 */
	public Page<TcProduct> getPageProductNotChoose(Map<String, Object> map);
	
	/**
	 * 查询已经被选中的产品
	 * @param map
	 * @return
	 */
	public List<TcProduct> getPageProductChoose(Map<String, Object> map);
	
	/**
	 * 查询所有产品
	 * @return
	 */
	public List<TcProduct> searchProduct(Map<String, Object> params);
	
	/**
	 *查询所有产品及扩展产品 
	 *@author lijing
	 *@since 20180122
	 * @return
	 */
	List<TcProduct> getAllProductAndExt(Map<String, Object> params);
	
	/**
	 * 判定是否出账
	 * @param params
	 * @return
	 */
	public boolean checkIsAcctAcup(Map<String, Object> params);

    /**
     *获取该客户某个业务剩余的占用额度和已经释放的额度
     *
     * @return
     */
    List<HrbCreditCustProd> getBalance(Map<String, Object> params);
}
