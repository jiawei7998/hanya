package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TaskInWaitingMapper;
import com.singlee.capital.base.model.TaskInMap;
import com.singlee.capital.base.model.TaskInWaiting;
import com.singlee.capital.base.service.TaMessageService;
import com.singlee.capital.base.service.TaskInWaitingService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Service
public class TaskInWaitingServiceImpl implements TaskInWaitingService{
	
	@Resource	
	TaskInWaitingMapper taskInWaitingMapper;
	@Resource
	TaMessageService taMessageService;

	@Override
	public Page<TaskInWaiting> searchTaskInWaitingPage(Map<String, Object> map) {
		return taskInWaitingMapper.searchTaskInWaitingPage(map,ParameterUtil.getRowBounds(map));
	}

	@Override
	public TaskInMap getTaskInWaitingCount(Map<String, Object> map) {
		int countApp=0;//待审批
		int countVer=0;//已审批
		int countDown=0;//已完成
		int newApp=0;//已拒绝
		String approveStatusArr = "4,5,11,15";
		map.put("approveStatus", approveStatusArr.split(","));
		countApp = taMessageService.selectTaMessageCount(map);
		map = new HashMap<String, Object>();
		approveStatusArr = "4,5,11,15";
		map.put("userId", SlSessionHelper.getUserId());
		map.put("approveStatus", approveStatusArr.split(","));
		countVer = taMessageService.selectTaMessageCount(map);
		map = new HashMap<String, Object>();
		approveStatusArr = "6,10,12,14";
		map.put("approveStatus", approveStatusArr.split(","));
		map.put("userId", SlSessionHelper.getUserId());
		countDown = taMessageService.selectTaMessageCount(map);
		map = new HashMap<String, Object>();
		approveStatusArr = "7,16";
		map.put("approveStatus", approveStatusArr.split(","));
		map.put("userId", SlSessionHelper.getUserId());
		newApp = taMessageService.selectTaMessageCount(map);
		TaskInMap taskInMap = new TaskInMap();
		taskInMap.setApproveCount(countApp);
		taskInMap.setApprovedCount(countVer);
		taskInMap.setFinishCount(countDown);
		taskInMap.setMineCount(newApp);
		return taskInMap;
	}

}
