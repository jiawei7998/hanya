package com.singlee.capital.base.participant.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcProductParticipant;

public interface TcProductParticipantService {
	/**
	 * 查询获得产品与参与方的关联配置信息
	 * @param params
	 * @return
	 */
	public Page<TcProductParticipant> getProductParticipantList(Map<String, Object> params);
	
	/**
	 * 删除产品与参与方的关联
	 * @param map
	 */
	public void deleteProductParticipant(Map<String, Object> map);
	
	/**
	 * 新增产品与参与方的关联
	 * @param map
	 */
	public void addProductParticipant(TcProductParticipant tcProductParticipant);
	
	/**
	 * 更新产品与参与方的关联
	 * @param map
	 */
	public void updateProductParticipant(TcProductParticipant tcProductParticipant);
	
	/**
	 * 查询是否已绑定交易对手
	 * @param map
	 * @return
	 */
	public List<TcProductParticipant> getProductParticipantCounterparty(Map<String, Object> map);
}
