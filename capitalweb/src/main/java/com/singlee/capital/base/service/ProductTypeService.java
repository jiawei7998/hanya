package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcProductType;
import com.singlee.capital.base.model.TtProductFlowTypeMap;
import com.singlee.capital.flow.pojo.vo.ProductFlowTreeView;


/**
 * 产品类型 服务 
 * @author xuhui
 *
 */
public interface ProductTypeService {

	/**
	 * 新增产品类型
	 */
	void addProductType(TcProductType prdType);
	
	/**
	 * 更新产品类型
	 * @param prdType
	 */
	void updateProductType(TcProductType prdType);
	/**
	 * 根据编号获取产品类型
	 * @param prdTypeNo
	 * @return
	 */
	TcProductType getProductTypeByNo(String prdTypeNo);
		
	/**
	 * 查询产品类型
	 * @return
	 */
	List<TcProductType> search();

	/**
	 * 取父产品类型列表
	 * @param prd_type_no
	 * @return
	 */
	List<TcProductType> getParentProductType(String prd_type_no);

	List<ProductFlowTreeView> searchProductTypeFlowView();
	
	List<TtProductFlowTypeMap> getFlowTypeByProductType(Map<String, Object> map);
	
	void saveFlowTypeByProductType(Map<String, Object> map);
	
	List<TcProductType> getProductTypeList(Map<String, Object> map);
}
