package com.singlee.capital.base.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.pojo.DictForCombobox;
import com.singlee.capital.base.service.DictService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

 
/**
 * 字典控制器 请求前缀 /DictController/
 * 
 * 不需要任何拦截器，提高效率
 * 
 * @author cz
 * 
 */
@Controller
@RequestMapping(value = "/DictController")
public class DictController extends CommonController {
	
	/** 机构服务接口 spring 负责注入 */
	@Autowired
	DictService dictService;
	

	/**
	 * 获取客户大类
	 */
	@RequestMapping(value = "/selectBigKind")
	@ResponseBody
	public List<DictForCombobox> selectBigKind()  {
		return dictService.listBigKind();
	}		
	/**
	 * 获取客户小类
	 */
	@RequestMapping(value = "/selectSmallKind/{bigkind}")
	@ResponseBody
	public List<DictForCombobox> selectSmallKind(@PathVariable String  bigkind)  {
		return dictService.listSmallKind(bigkind);
	}
	
	/**
	 * 获取报表客户大类
	 */
	@RequestMapping(value = "/selectBigKindName")
	@ResponseBody
	public RetMsg<List<DictForCombobox>> selectBigKindName()  {
		List<DictForCombobox> retList = dictService.listBigKind();
		RetMsg<List<DictForCombobox>> ret = RetMsgHelper.ok(retList);
		if(RetMsgHelper.codeOk.equals(ret.getCode())){
			//ret
		}
		return ret;
	}
	
	/**
	 * 根据客户类别获取不同的客户大类
	 */
	@RequestMapping(value = "/selectBigKindByCode")
	@ResponseBody
	public List<DictForCombobox> selectBigKindByCode(@RequestBody Map<String, String> params){
		System.out.println(params+"************************************");
		String kind_code = ParameterUtil.getString(params, "kind_code", "");
		return dictService.listBigKindByCode(kind_code);
	}	
}