package com.singlee.capital.base.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 字段
 * @description TODO
 * @author 倪航
 * @createDate 2016-5-12 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_FIELD")
public class TcField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6742375494438586301L;
	/**
	 * 字段编号 序列自动生成
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_FIELD.NEXTVAL FROM DUAL")
	private String fldNo;
	/**
	 * 字段名称 提交到后台的请求参数名称
	 */
	private String fldName;
	/**
	 * 字段类型
	 */
	private String fldType;
	/**
	 * 表单名称  表单中展示的名称
	 */
	private String formName;
	/**
	 * 表单提示 表单中的提示信息
	 */
	private String formTips;
	/**
	 * 表单类型
	 */
	private String formType;
	/**
	 * 预定义组件
	 */
	private String componet;
	/**
	 * 表单占列
	 */
	private Integer formCols;
	/**
	 * 表单占行
	 */
	private Integer formRows;
	/**
	 * 表单长度
	 */
	private Integer formLength;
	/**
	 * 是否多选 树形框是否支持多选
	 */
	private String formMultiple;
	/**
	 * 最小值 数值框允许的最小值
	 */
	private String formMin;
	/**
	 * 最大值 数值框允许的最大值
	 */
	private String formMax;
	/**
	 * 显示精度 数值框小数点后的显示精度
	 */
	private String formPrecision;
	/**
	 * 数字分割符
	 */
	private String formSplit;
	/**
	 * 数字倍数
	 */
	private BigDecimal formTimes;
	/**
	 * 表单验证
	 */
	private String formValid;
	/**
	 * 验证参数
	 */
	private String validParam;
	/**
	 * 是否必填 表单中设置必填校验及星号标志
	 */
	private String isMandatory;
	/**
	 * 是否禁用
	 */
	private String isDisabled;
	/**
	 * 关键字 数据字典的字典类型编号
	 */
	private String defKey;
	/**
	 * 请求地址
	 */
	private String defUrl;
	/**
	 * 默认值
	 */
	private String defValue;
	/**
	 * 默认文本值
	 */
	private String defText;
	/**
	 * 启用标志 0-禁用 1-启用
	 */
	private String useFlag;
	/**
	 * 更新时间
	 */
	private String lastUpdate;
	@Transient
	private String formStatus;//表单栏位属性
	
	public String getFormStatus() {
		return formStatus;
	}
	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}
	/**
	 * 附件树模版
	 */
	@Transient
	private String attachTreeTemp;
	
	public String getFldNo() {
		return fldNo;
	}
	public void setFldNo(String fldNo) {
		this.fldNo = fldNo;
	}
	public String getFldName() {
		return fldName;
	}
	public void setFldName(String fldName) {
		this.fldName = fldName;
	}
	public String getFldType() {
		return fldType;
	}
	public void setFldType(String fldType) {
		this.fldType = fldType;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormTips() {
		return formTips;
	}
	public void setFormTips(String formTips) {
		this.formTips = formTips;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getComponet() {
		return componet;
	}
	public void setComponet(String componet) {
		this.componet = componet;
	}
	public Integer getFormCols() {
		return formCols;
	}
	public void setFormCols(Integer formCols) {
		this.formCols = formCols;
	}
	public Integer getFormRows() {
		return formRows;
	}
	public void setFormRows(Integer formRows) {
		this.formRows = formRows;
	}
	public Integer getFormLength() {
		return formLength;
	}
	public void setFormLength(Integer formLength) {
		this.formLength = formLength;
	}
	public String getFormMultiple() {
		return formMultiple;
	}
	public void setFormMultiple(String formMultiple) {
		this.formMultiple = formMultiple;
	}
	public String getFormMin() {
		return formMin;
	}
	public void setFormMin(String formMin) {
		this.formMin = formMin;
	}
	public String getFormMax() {
		return formMax;
	}
	public void setFormMax(String formMax) {
		this.formMax = formMax;
	}
	public String getFormPrecision() {
		return formPrecision;
	}
	public void setFormPrecision(String formPrecision) {
		this.formPrecision = formPrecision;
	}
	public String getFormSplit() {
		return formSplit;
	}
	public void setFormSplit(String formSplit) {
		this.formSplit = formSplit;
	}
	public String getFormValid() {
		return formValid;
	}
	public void setFormValid(String formValid) {
		this.formValid = formValid;
	}
	public String getValidParam() {
		return validParam;
	}
	public void setValidParam(String validParam) {
		this.validParam = validParam;
	}
	public String getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getIsDisabled() {
		return isDisabled;
	}
	public void setIsDisabled(String isDisabled) {
		this.isDisabled = isDisabled;
	}
	public String getDefKey() {
		return defKey;
	}
	public void setDefKey(String defKey) {
		this.defKey = defKey;
	}
	public String getDefUrl() {
		return defUrl;
	}
	public void setDefUrl(String defUrl) {
		this.defUrl = defUrl;
	}
	public String getDefValue() {
		return defValue;
	}
	public void setDefValue(String defValue) {
		this.defValue = defValue;
	}
	public String getDefText() {
		return defText;
	}
	public void setDefText(String defText) {
		this.defText = defText;
	}
	public String getUseFlag() {
		return useFlag;
	}
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public BigDecimal getFormTimes() {
		return formTimes;
	}
	public void setFormTimes(BigDecimal formTimes) {
		this.formTimes = formTimes;
	}
	public String getAttachTreeTemp() {
		return attachTreeTemp;
	}
	public void setAttachTreeTemp(String attachTreeTemp) {
		this.attachTreeTemp = attachTreeTemp;
	}
}