package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaskInWaiting;

public interface TaskInWaitingMapper extends Mapper<TaskInWaiting>{

	public Page<TaskInWaiting> searchTaskInWaitingPage(Map<String,Object> map,RowBounds rb);
	public List<TaskInWaiting> getTaskInWaitingCount(Map<String,Object> map);
	
}
