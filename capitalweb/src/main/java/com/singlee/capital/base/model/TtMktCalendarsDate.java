package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_MKT_CALENDAR_DATE")
public class TtMktCalendarsDate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String calCode;
	private String calDate;
	private String calFlag;
	public String getCalCode() {
		return calCode;
	}
	public void setCalCode(String calCode) {
		this.calCode = calCode;
	}
	public String getCalDate() {
		return calDate;
	}
	public void setCalDate(String calDate) {
		this.calDate = calDate;
	}
	public String getCalFlag() {
		return calFlag;
	}
	public void setCalFlag(String calFlag) {
		this.calFlag = calFlag;
	}
}
