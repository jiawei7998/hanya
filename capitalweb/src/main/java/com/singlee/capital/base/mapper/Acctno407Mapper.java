package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.Acctno407;

public interface Acctno407Mapper {
	
	/**
	 * 407帐号 -- 用于行内行外支付
	 * @param map
	 * @return
	 */
	public List<Acctno407> selectAcctno407(Map<String, Object> map);

}
