package com.singlee.capital.base.pojo;
/**
 * 用于绑定界面上字典项下拉框对象
 * @author gm
 *
 */
public class DictForCombobox {

	private String value;
	private String text;

	public DictForCombobox() {
	}
	public DictForCombobox(String value, String text) {
		this.value = value;
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
