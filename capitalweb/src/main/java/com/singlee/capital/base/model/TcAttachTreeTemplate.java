package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;

/**
 * 附件 组织机构 树形 表 
 * 
 * @author x230i
 *
 */
@Entity
@Table(name = "TC_ATTACH_TREE_TEMPLATE")
public class TcAttachTreeTemplate implements Serializable, TreeInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String refId;
	private String refType;//REF_TYPE	N	VARCHAR2(20)	Y			所属关联类型
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_ATTACH.NEXTVAL FROM DUAL")
	private String treeId ;//	N	VARCHAR2(32)	N			
	private String treePid ;//	N	VARCHAR2(32)	N
	private String treeName;//FIELD_NAME	N	VARCHAR2(32)	Y			关联字段名
	private Integer sort;//CONTENT_TYPE	N	VARCHAR2(32)	N			附件内容类型
	
	@Transient
	private List<TcAttachTreeTemplate> children;
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TcAttachTreeTemplate [refId=");
		builder.append(refId);
		builder.append(", refType=");
		builder.append(refType);
		builder.append(", treeId=");
		builder.append(treeId);
		builder.append(", treePid=");
		builder.append(treePid);
		builder.append(", treeName=");
		builder.append(treeName);
		builder.append(", sort=");
		builder.append(sort);
		builder.append("]");
		return builder.toString();
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getTreeId() {
		return treeId;
	}
	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}
	public String getTreePid() {
		return treePid;
	}
	public void setTreePid(String treePid) {
		this.treePid = treePid;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	@Override
	public int getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	@Override
	public String getId() {
		return getTreeId();
	}
	@Override
	public String getParentId() {
		return getTreePid();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void children( List<? extends TreeInterface> list) {
		setChildren((List<TcAttachTreeTemplate>) list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	
	public List<TcAttachTreeTemplate> getChildren() {
		return children;
	}
	public void setChildren(List<TcAttachTreeTemplate> children) {
		Collections.sort(children, new Comparator<TcAttachTreeTemplate>() {
			@Override
			public int compare(TcAttachTreeTemplate arg0, TcAttachTreeTemplate arg1) {
				return arg0.getSort() > arg1.getSort() ? 1 : 0;
			}
			
		});
		this.children = children;
	}
	
	@Override
	public String getText() {
		return getTreeName();
	}
	@Override
	public String getValue() {
		return getTreeId();
	}
	@Override
	public String getIconCls() {
		return "";
	}
}
