package com.singlee.capital.base.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcFormMapper;
import com.singlee.capital.base.mapper.TcProductFormMapper;
import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.model.TcProductForm;
import com.singlee.capital.base.service.ProductFormService;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品字段映射服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductFormServiceImpl implements ProductFormService {
	
	@Autowired
	private TcProductFormMapper productFormMapper;
	
	@Autowired
	private TcFormMapper formMapper;
	
	@Override
	public List<TcProductForm> getProductFormList(Map<String, Object> params) {
		return productFormMapper.getProductFormList(params);
	}

	@Override
	public List<TcForm> getFormList(Map<String, Object> params) {
		//根据产品编号取得已配置的表单集合
		List<TcProductForm> productForms = productFormMapper.getProductFormList(params);
		List<String> formNos = new ArrayList<String>();
		for (TcProductForm productForm : productForms) {
			formNos.add(productForm.getFormNo());
		}
		//如果产品未配置表单，则默认搜索表单编号为0的结果集
		if(formNos.size() == 0){
			formNos.add("0");
		}
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("formNos", formNos);
		//如果FormNo相同，把flowroleid赋值出来，提取出审批角色ID到前台 、待完善  wzf
		List<TcForm> lstForm = formMapper.getFormList(param);
		for(TcForm frm : lstForm) {
			for (TcProductForm productForm : productForms) {
				if (productForm.getFormNo().equals(frm.getFormNo()))
				{
					frm.setFlowRoleId(productForm.getFlowRoleId());
				}
			}
		}
		
		return lstForm;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
