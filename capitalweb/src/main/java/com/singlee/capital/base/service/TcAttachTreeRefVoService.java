package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcAttachTreeFile;


/**
 * @projectName 
 * @className 附件树服务
 * @description TODO
 * @author 刘小燚
 * @createDate 2017-6-3 下午3:25:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcAttachTreeRefVoService {

	public List<TcAttachTreeFile> getTcAttachTreeRefVo(Map<String, String> map);
	public List<TcAttachTreeFile> getTcAttachTreeRefVoByPrd(Map<String, String> map);
	
}
