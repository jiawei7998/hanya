package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;

/**
 * @projectName 同业业务管理系统
 * @className 产品种类
 * @description TODO
 * @author 倪航
 * @createDate 2016-5-12 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_PRODUCT_TYPE")
public class TcProductType implements Serializable,TreeInterface {

	private static final long serialVersionUID = 1L;
	/**
	 * 产品种类编号 序列自动生成
	 */
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_PRODUCT_TYPE.NEXTVAL FROM DUAL")
	private Integer prdTypeNo;
	/**
	 * 父产品种类编号
	 */
	private Integer supPrdTypeNo;
	/**
	 * 产品种类名称
	 */
	private String prdTypeName;
	/**
	 * 产品种类描述
	 */
	private String prdTypeDesc;
	
	private String approveType;//审批类型
	
	private String deskId;
	
	private String branchId;
	
	public String getDeskId() {
		return deskId;
	}
	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getApproveType() {
		return approveType;
	}
	public void setApproveType(String approveType) {
		this.approveType = approveType;
	}
	@Transient
	/** 子节点 */
	private List<TcProductType> children = new ArrayList<TcProductType>();

	/** 添加子节点 */
	public void addChild(TcProductType productType) {
		children.add(productType);
	}
	
	/** 按环节编号比较的节点比较器 */
	public class ProductTypeNoComparator implements Comparator<Object> {
		@Override
		public int compare(Object o1, Object o2) {
			int j1 = ((TcProductType) o1).prdTypeNo;
			int j2 = ((TcProductType) o2).prdTypeNo;
			return (j1 < j2 ? -1 : (j1 == j2 ? 0 : 1));
		}
	}
	
	/** 兄弟节点横向排序 */
	public void sortChildren(Comparator<Object> comparator) {
		if (children.size() != 0) {
			// 对本层节点进行排序（可根据不同的排序属性，传入不同的比较器，这里传入ORDER比较器）
			Collections.sort(children, comparator);
			// 对每个节点的下一层节点进行排序
			for (int i = 0; i < children.size(); i++) {
				((TcProductType) children.get(i)).sortChildren(comparator);
			}
		} else {
			// 无子节点的children设为null
			children = null;
		}
	}
	public Integer getPrdTypeNo() {
		return prdTypeNo;
	}
	public void setPrdTypeNo(Integer prdTypeNo) {
		this.prdTypeNo = prdTypeNo;
	}
	public Integer getSupPrdTypeNo() {
		return supPrdTypeNo;
	}
	public void setSupPrdTypeNo(Integer supPrdTypeNo) {
		this.supPrdTypeNo = supPrdTypeNo;
	}
	public String getPrdTypeName() {
		return prdTypeName;
	}
	public void setPrdTypeName(String prdTypeName) {
		this.prdTypeName = prdTypeName;
	}
	public String getPrdTypeDesc() {
		return prdTypeDesc;
	}
	public void setPrdTypeDesc(String prdTypeDesc) {
		this.prdTypeDesc = prdTypeDesc;
	}
	public List<TcProductType> getChildren() {
		return children;
	}
	public void setChildren(List<TcProductType> children) {
		this.children = children;
	}
	@Override
	public String getId() {
		return getPrdTypeNo().toString();
	}
	@Override
	public String getParentId() {
		return getSupPrdTypeNo().toString();
	}
	@Override
	public String getText() {
		return prdTypeName;
	}
	@Override
	public String getValue() {
		return prdTypeNo.toString();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TcProductType>) list);
		
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	@Override
	public int getSort() {
		return prdTypeNo;
	}
	@Override
	public String getIconCls() {
		return null;
	}
}