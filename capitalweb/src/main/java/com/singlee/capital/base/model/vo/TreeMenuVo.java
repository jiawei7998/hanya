package com.singlee.capital.base.model.vo;

import java.io.Serializable;

/**
 * 菜单模式
 * @author louhuanqing
 *
 */
public class TreeMenuVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//id: "addRight", pid: "right", text: "基金持仓", iconCls: "Reports", url: "", iconPosition: "top"
	private String id;
	private String pid;
	private String text;
	private String iconCls;
	private String url;
	private String iconPosition;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIconPosition() {
		return iconPosition;
	}
	public void setIconPosition(String iconPosition) {
		this.iconPosition = iconPosition;
	}
	

}
