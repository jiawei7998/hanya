package com.singlee.capital.base.participant.model;

import java.io.Serializable;
/**
 * 产品 参与方 字段配置表
 * @author SINGLEE
 *
 */
public class TcProductPartField implements Serializable {
	private static final long serialVersionUID = 1L;
	private String prdNo;//产品代码
	private String partyId;//参与方代码
	private String fldNo;//字段代码
	private String formName;
	private String prdName;
	private String partyName;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	private int sortNo;//排序
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getFldNo() {
		return fldNo;
	}
	public void setFldNo(String fldNo) {
		this.fldNo = fldNo;
	}
	public int getSortNo() {
		return sortNo;
	}
	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}
	@Override
	public String toString() {
		return "TcProductPartField [prdNo=" + prdNo + ", partyId=" + partyId
				+ ", fldNo=" + fldNo + ", sortNo=" + sortNo + "]";
	}
	
	
}
