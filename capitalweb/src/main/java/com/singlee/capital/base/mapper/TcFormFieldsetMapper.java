package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.model.TcFormFieldset;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单字段分组映射持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-30 下午3:31:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcFormFieldsetMapper extends Mapper<TcFormFieldset> {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 表单字段分组映射对象列表
	 * @author Hunter
	 * @date 2016-8-30
	 */
	public List<TcFormFieldset> getFormFieldsetList(Map<String, Object> params);
	/**
	 * 获得参与方配置
	 * @param params
	 * @return
	 */
	public List<TcFormFieldset> getFormPartyFieldsetList(Map<String, Object> params);
	
	/**
	 * 根据表单编号删除表单字段映射
	 * 
	 * @param formNo - 表单编号
	 * @author Hunter
	 * @date 2016-8-30
	 */
	public void deleteFormFieldsetByFormId(String formNo);
}