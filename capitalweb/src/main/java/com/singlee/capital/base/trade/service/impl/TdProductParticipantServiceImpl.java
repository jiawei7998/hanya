package com.singlee.capital.base.trade.service.impl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.participant.mapper.TcProductParticipantMapper;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.trade.mapper.TdProductParticipantMapper;
import com.singlee.capital.base.trade.model.TdProductParticipant;
import com.singlee.capital.base.trade.service.TdProductParticipantService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdProductCustCreditMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.service.ProductCustCreditService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdProductParticipantServiceImpl implements
		TdProductParticipantService {
	@Autowired
	BatchDao batchDao;
	@Autowired
	TcProductParticipantMapper tcProductParticipantMapper;//参与方配置
	@Autowired
	TdProductParticipantMapper tdProductParticipantMapper;//参与方交易记录
	@Autowired
	ProductCustCreditService productCustCreditService;//额度判定算法
	@Autowired
	TdProductCustCreditMapper productCustCreditMapper;
	@Override
	public void insertBatchForTdProductParticipant(List<TdProductParticipant> tdProductParticipants) {
		// TODO Auto-generated method stub
      if(tdProductParticipants!=null && tdProductParticipants.size()>0){
		   batchDao.batch("com.singlee.capital.base.trade.mapper.TdProductParticipantMapper.insert", tdProductParticipants);
	   }
	}
	@Override
	public void insertParticipantByDeal(TdProductApproveMain productApproveMain) {
		// TODO Auto-generated method stub
		//审批子表
		Object object = productApproveMain.getParticipantApprove();
		if (object != null) {
		/**
		 * 循环参与方
		 */
		List<TdProductApproveSub> tdSubs = FastJsonUtil.parseArrays(object.toString(), TdProductApproveSub.class);
		Map<String, String> tdSubsMap = new HashMap<String, String>();  
	    for (TdProductApproveSub subs : tdSubs) {  
	    	tdSubsMap.put(subs.getSubFldName(), subs.getSubFldValue());  
	    }
	    //定义参与方接收数组
	    List<TdProductParticipant> tdProductParticipants = new ArrayList<TdProductParticipant>();
	    //找出当前产品需要的参与方数量
	    Map<String, Object> params = new HashMap<String, Object>();
	    params.put("prdNo", productApproveMain.getPrdNo());
	    List<TcProductParticipant> tcProductParticipants = this.tcProductParticipantMapper.getProductParticipantList(params, ParameterUtil.getRowBounds(params)).getResult();
	    TdProductParticipant tdProductParticipant = null;
	    for(TcProductParticipant tcParticipant : tcProductParticipants){
	    	try {
	    		tdProductParticipant = new TdProductParticipant();
	    		tdProductParticipant.setDealNo(productApproveMain.getDealNo());
	    		tdProductParticipant.setAccessLevel(tdSubsMap.get(tcParticipant.getPartyId()+"_accessLevel"));
	    		tdProductParticipant.setGuaName(tdSubsMap.get(tcParticipant.getPartyId()+"_guaName"));
	    		tdProductParticipant.setIndustryType(tdSubsMap.get(tcParticipant.getPartyId()+"_industryType"));
	    		tdProductParticipant.setIsActive(productApproveMain.getIsActive());
	    		tdProductParticipant.setLocation(tdSubsMap.get(tcParticipant.getPartyId()+"_location"));
	    		tdProductParticipant.setPartyCode(tdSubsMap.get(tcParticipant.getPartyId()+"_partyCode"));
	    		tdProductParticipant.setPartyContacts1(tdSubsMap.get(tcParticipant.getPartyId()+"_partyContacts1"));
	    		tdProductParticipant.setPartyContacts2(tdSubsMap.get(tcParticipant.getPartyId()+"_partyContacts2"));
	    		tdProductParticipant.setPartyId(tcParticipant.getPartyId());
	    		tdProductParticipant.setPartyName(tdSubsMap.get(tcParticipant.getPartyId()+"_partyName"));
	    		tdProductParticipant.setTrusteeType(tdSubsMap.get(tcParticipant.getPartyId()+"_trusteeType"));
	    		tdProductParticipant.setVersion(String.valueOf(productApproveMain.getVersion()));
	    		if(null != tdProductParticipant.getPartyCode() && !"".equals(tdProductParticipant.getPartyCode()))
	    		{
	    			tdProductParticipants.add(tdProductParticipant);
	    			//判定当前的参与方是否已配置成为交易对手
	    			if(StringUtils.equals(tcParticipant.getJudgeCounterparty()==null?"x":tcParticipant.getJudgeCounterparty(), "1"))
	    			{
	    				productApproveMain.setcNo(tdProductParticipant.getPartyCode());//赋值交易对手
	    				productApproveMain.setContact(tdProductParticipant.getPartyContacts1());//赋值第一顺位联系人
	    			}
	    		}
	    		
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new RException(e);
			}
	    	
	    }
	    //没有确认交易对手的需要重新确认
	    if(null==productApproveMain.getcNo()||"".equals(productApproveMain.getcNo())){
			//同业保理资产受益权 特殊处理
			if(productApproveMain.getPrdNo()==293)
			{
				if(null != tdSubsMap.get("P032_partyCode")){
					productApproveMain.setcNo(tdSubsMap.get("P032_partyCode"));//赋值交易对手
    				productApproveMain.setContact(tdSubsMap.get("P032_partyContacts1"));//赋值第一顺位联系人
				}else {
					productApproveMain.setcNo(tdSubsMap.get("P003_partyCode"));//赋值交易对手
    				productApproveMain.setContact(tdSubsMap.get("P003_partyContacts1"));//赋值第一顺位联系人
				}
			}
		}
	    //先删除历史的数据
	    params.put("dealNo", productApproveMain.getDealNo());
	    this.tdProductParticipantMapper.deleteTdProductPartyByDealno(params);
	    productCustCreditMapper.deleteTdProductCustCreditByDealNo(productApproveMain.getDealNo());
	    //将参与方持久化
	    if(tdProductParticipants!=null && tdProductParticipants.size()>0){
			   batchDao.batch("com.singlee.capital.base.trade.mapper.TdProductParticipantMapper.insert", tdProductParticipants);
			   try {
				batchDao.batch("com.singlee.capital.trade.mapper.TdProductCustCreditMapper.insert",
				   productCustCreditService.getProductCustCreditForDeal(productApproveMain, tdSubsMap));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new RException(e);
			}
			   
		   }
		}
	}
	@Override
	public List<TdProductApproveSub> transProductParticipantAsApproveSubs(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		//获得交易向下的参与方数据 DEAL_NO VERSION
		List<TdProductParticipant> tdProductParticipants = this.tdProductParticipantMapper.getTdProductParticipantsByDealno(params);
		List<TdProductApproveSub> tdProductApproveSubs = new ArrayList<TdProductApproveSub>();//接收列表
		//循环参与方
		TdProductApproveSub tdProductApproveSub = null;
		for(TdProductParticipant tdProductParticipant : tdProductParticipants){
			try {
				//构建JAVABEAN 属性解析
				PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
				PropertyDescriptor[] descriptors = propertyUtilsBean.getPropertyDescriptors(tdProductParticipant);
				for (int i = 0; i < descriptors.length; i++) {
					String name = descriptors[i].getName();
					if (!"class".equals(name)) {//去掉CLASS 属性
						tdProductApproveSub = new TdProductApproveSub();
						tdProductApproveSub.setSubFldName(tdProductParticipant.getPartyId()+"_"+name);
						tdProductApproveSub.setSubFldValue(StringUtils.trimToEmpty(propertyUtilsBean.getNestedProperty(tdProductParticipant, name)==null?"":propertyUtilsBean.getNestedProperty(tdProductParticipant, name).toString()));
						tdProductApproveSubs.add(tdProductApproveSub);
					}
				}
			} catch (Exception e) {
				//e.printStackTrace();
				throw new RException(e);
			}
		}
		return tdProductApproveSubs;
	}
	
	
	public static Map<String, Object> beanToMap(Object obj) {
		Map<String, Object> params = new HashMap<String, Object>(0);
		try {
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			PropertyDescriptor[] descriptors = propertyUtilsBean
					.getPropertyDescriptors(obj);
			for (int i = 0; i < descriptors.length; i++) {
				String name = descriptors[i].getName();
				if (!"class".equals(name)) {
					params.put(name,
							propertyUtilsBean.getNestedProperty(obj, name));
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return params;
	}
	@Override
	public void deleteProductParticipantByDealNo(Map<String, Object> params) {
		// TODO Auto-generated method stub
		tdProductParticipantMapper.deleteTdProductPartyByDealno(params);
	}

}
