package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachNode;
import com.singlee.capital.base.model.TcAttachNodeRelation;

public interface AttachNodeServer {
	
	
	/**
	 * 查询产品类型
	 * @return
	 */
	Page<TcAttachNode> search(Map<String, Object> params);
	 
	/**
	 * 创建节点
	 * @param ta
	 */
	void createAttachNode(TcAttachNode ta);
	
	/**
	 * 修改节点
	 * 
	 */

	 void updateAttachNode(TcAttachNode ta);
	 
	 /**
	  * 删除
	  * @param ta
	  */
	 void deleteAttachNode(TcAttachNode ta);
	 
	 /**
	  * 查询附件数下节点
	  * @param params
	  */
	 List<TcAttachNodeRelation> searchAttachNodeRelationList(Map<String, Object> params);
	 
	 /** 
	  * 添加节点与附件树的关系
	  * @param params
	  */
	 void addAttachRelation (Map<String, Object> params);
	 
	 /**
	  * 删除节点与附件树的关系
	  */
	 void  delAttachRelation(Map<String, Object> params);
}
