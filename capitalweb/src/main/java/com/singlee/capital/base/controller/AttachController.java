package com.singlee.capital.base.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.singlee.capital.base.model.TcAttach;
import com.singlee.capital.base.service.AttachService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.JQueryFileUploadResult;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.webmvc.JqueryFileUploadHandlerMulipartFile;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.interfacex.qdb.service.QdAttachService;
import com.singlee.capital.interfacex.qdb.util.PathUtils;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/AttachController")
public class AttachController extends CommonController{
	public static Logger log = LoggerFactory.getLogger("com");
	@Autowired
	private AttachService asss;
	
	@Autowired
	private QdAttachService qdAttachService;
	
	@ResponseBody
	@RequestMapping(value = "/uploadAttachByJqueryFileUpload", method = RequestMethod.POST)
	public JQueryFileUploadResult uploadAttachByJqueryFileUpload(MultipartHttpServletRequest request,
			HttpServletResponse response) throws Exception {
		final String userId = SlSessionHelper.getUserId();
		final String userName = SlSessionHelper.getUser().getUserName();
		final String instId = SlSessionHelper.getInstitutionId();
		final String instName = SlSessionHelper.getInstitution().getInstName();
		final String refType = HttpUtil.getParameterString(request, "refType", null);
		final String refId = HttpUtil.getParameterString(request, "refId", null);
		final String fieldName = HttpUtil.getParameterString(request, "fieldName", null);
		
		
		return super.uploadFile(request, response, new JqueryFileUploadHandlerMulipartFile() {
			
			@Override
			protected void checkFileName(String fileName) {
				JY.info(fieldName);
			}
			
			@Override
			protected JQueryFileUploadResult attachment(MultipartFile mulfile) {
				JQueryFileUploadResult j = new JQueryFileUploadResult();
				byte[] content = null;
				String fileName = mulfile.getOriginalFilename();
				try {
					content = mulfile.getBytes();
				} catch (IOException e) {
					JY.raise("process file %s exception %s", fileName, e.getMessage());
					j.addError(e.getMessage());
				}				
				try {
					String serino = qdAttachService.getAttachSerino();
					TcAttach a = new TcAttach();
					a.setAttachName(fileName);
					a.setAttachStatus("");
					a.setAttachPath(serino);
					a.setAttachType("");
					a.setContentSize(content.length);
					a.setContentType(mulfile.getContentType());
					a.setFieldName(fieldName);
					a.setRefId(refId);
					a.setRefType(refType);
					a.setCreateUserId(userId);
					a.setCreateUserName(userName);
					a.setUpdateUserId(userId);
					a.setUpdateUserName(userName);
					a.setCreateInstId(instId);
					a.setCreateInstName(instName);
					a.setUpdateInstId(instId);
					a.setUpdateInstName(instName);					
					
					String path = getAttachFilesPath() + fileName;
					
					File file = new File(path);
					
					mulfile.transferTo(file);
					
					String flag = qdAttachService.attachUpload(serino, path);
					if(flag != null && flag.contains("SUCCESS")){
						file.delete();
					}else{
						file.delete();
						log.info("文件上传失败,保存到本地数据库");
						a.setAttachBlob(content);
					}
					log.info("file文件上传:路径是"+path);
					log.info("file文件上传:文件"+path);
					asss.createAttach(a);
					j.addFile(a.getAttachId(), null, "../sl/AttachController/downloadAttach/" + a.getAttachId(), a.getAttachName(), a.getContentSize(), "../sl/AttachController/removeAttach/" + a.getAttachId(), "POST", null, a.getCreateTime(), a.getCreateUserId(), a.getCreateUserName());
				} catch (Exception e) {
					e.printStackTrace();
				} 
				return j;
			}
		});
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchAttach")
	public RetMsg<List<JQueryFileUploadResult>> searchAttach(@RequestBody Map<String, String> params) throws RException {
		String refId = ParameterUtil.getString(params, "refId", null);
		String refType = ParameterUtil.getString(params, "refType", null);
		List<TcAttach> list = asss.searchAttach(refType, refId);
		List<JQueryFileUploadResult> result = new LinkedList<JQueryFileUploadResult>();
		for (TcAttach tcAttach : list) {
			JQueryFileUploadResult j = new JQueryFileUploadResult();
			j.addFile(tcAttach.getAttachId(), null, "../sl/AttachController/downloadAttach/" + tcAttach.getAttachId(), tcAttach.getAttachName(), tcAttach.getContentSize(), "../sl/AttachController/removeAttach/" + tcAttach.getAttachId(), "POST", null, tcAttach.getCreateTime(), tcAttach.getCreateUserId(), tcAttach.getCreateUserName());
			result.add(j);
		}
		return RetMsgHelper.ok(result);
	}
	
	@ResponseBody
	@RequestMapping(value = "/removeAttach/{attachId}")
	public RetMsg<Serializable> removeAttach(@PathVariable String attachId) throws RException {
		asss.deleteAttach(attachId);	
		return RetMsgHelper.ok();
	}
	
	private String getAttachFilesPath(){
		File dir = new File(PathUtils.getProjectDir("attachTmpFiles"));
		if(!dir.exists())
		{
			dir.mkdirs();
		}
		return dir.getAbsolutePath() + "/";
	}
	
	private String getDownLoadAttachFilesPath(){
		File dir = new File(PathUtils.getProjectDir("attachTmpFiles"));
		if(!dir.exists())
		{
			dir.mkdirs();
		}
		return dir.getAbsolutePath();
	}
	
	@RequestMapping(value = "/downloadAttach/{attachId}", method = RequestMethod.GET)
	public void downloadAttach(@PathVariable String attachId, HttpServletRequest request,
			HttpServletResponse response){
		FileInputStream in = null;
		try {
			TcAttach attach = asss.getAttach(attachId);
			JY.require(attach!=null, "附件 %s 没有找到！", attachId);
			
			byte[] total = null;
			if(attach.getAttachBlob() == null)
			{
				String path = getDownLoadAttachFilesPath();
				String flag = qdAttachService.attachDownload(attach.getAttachPath(), path);
				if(flag != null && flag.contains("SUCCESS")){
					log.info(attach.getAttachName()+"文件下载成功");
				}
				
				File file = new File(path + "/" + attach.getAttachName());
				byte[] bytes = new byte[1];
				in = new FileInputStream(file);
				int i = in.read(bytes);
				List<Byte> list = new ArrayList<Byte>();
				list.add(bytes[0]);
				while(i!=-1 && in.available() != 0)
				{
					i = in.read(bytes);
					list.add(bytes[0]);
				}
				total = new byte[list.size()];
				int count = 0;
				for(byte a : list){
					total[count++] = a;
				}
				
			}else{//从数据库获取
				total = attach.getAttachBlob();
			}
			
			String contentType = attach.getContentType();
			response.setContentType(contentType);
			String encode_filename = URLEncoder.encode(attach.getAttachName(), "utf-8");
			String ua = request.getHeader("User-Agent");
			if (ua != null && ua.indexOf("Firefox/") >= 0) {
				response.setHeader("Content-Disposition", "attachment; filename*="
						+ encode_filename);
			} else {
				response.setHeader("Content-Disposition", "attachment; filename="
						+ encode_filename);
			}
			// 区分 是 blob 数据，还是 http:// 引用数据 
			HttpUtil.flushHttpResponse(response, contentType, total);
		} catch (Exception e) {
			log.error("文件下载报错"+e.getStackTrace());
		} finally{
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
