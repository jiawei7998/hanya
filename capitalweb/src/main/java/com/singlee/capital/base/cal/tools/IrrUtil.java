package com.singlee.capital.base.cal.tools;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.calBean;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.system.dict.DictConstants;

/**
 * 
 * 例题·计算题】正保公司于2008年1月1日购入中原公司发行的2年期债券，支付价款41 486万元，债券面值40 000万元，每半年付息一次，
 * 到期还本。合同约定债券发行方中原公司在遇到特定情况下可以将债券赎回，且不需要为赎回支付额外款项。
 * 甲公司在购买时预计发行方不会提前赎回，并将其划分为持有至到期投资。该债券票面利率8％，实际利率6％，
 * 采用实际利率法摊销，则甲公司2009年6月30日持有至到期投资的摊余成本为（ ）万元。 A．40764.50 B．41130.58 C．40387.43
 * D．41600 【答案】C 【解析】 利息调整分摊表
 * 
 * （1）2008年1月1日： 借：持有至到期投资——成本 40 000 持有至到期投资——利息调整 1 486 贷：银行存款 41 486
 * 2008年6月30日： 借：应收利息 1600（40 000×8％/2） 贷：投资收益 1 244.58（41 1486×6％/2）
 * 持有至到期投资——利息调整 355.42 借：银行存款 1600 贷：应收利息 1600 此时持有至到期投资的摊余成本=41 486+1 244.58-1
 * 600=41130.58（万元） （2）2008年12月31日： 借：应收利息 1 600（40 000×8％/2） 贷：投资收益 1 233.92（4
 * 1130.58×6％/2） 持有至到期投资——利息调整 366.08 借：银行存款 1600 贷：应收利息 1600 此时持有至到期投资的摊余成本=41
 * 130.58+1 233.92-1600=40 764.50（万元） （3）2009年6月30日： 借：应收利息 1 600（40 000×8％/2）
 * 贷：投资收益 1 222.93（40 764.50×6％/2） 持有至到期投资——利息调整 377.07 借：银行存款 1600 贷：应收利息 1600
 * 此时持有至到期投资的摊余成本=40 764.50+1 222.93-1600=40 387.43（万元） （4）2009年12月31日： 借：应收利息 1
 * 600（40 000×8％/2） 贷：投资收益 1212.57（倒挤） 持有至到期投资——利息调整
 * 387.43（1486-355.42-366.08-377.07） 借：银行存款 1600 贷：应收利息 1600 借：银行存款 40000
 * 贷：持有至到期投资——成本 40000
 * 
 * 
 * 
 * 这个题目中“支付价款41486万元”这个金额是怎么确定下来的？它实际上就是未来现金流量折现计算的结果，
 * 本题的未来现金流量是每半年收到的利息40000*8%*1/2=1600万元，以及最后一期收到的本金40000万元，
 * 将这些现金流量折现：1600/(1+3%)+1600/(1+3%)^2+1600/(1+3%)^3+1600/(1+3%)^4+40000/(1+3%)^4=41486.84(万元)，
 * 即支付的价款41486元，需要注意的是由于是每半年付息一次，而题目中给出的利率在没有特别说明的情况下是年利率，
 * 所以折现的期数为4期，利率为半年的利率3%。说明这些目的是想告诉大家，实际利率法的摊销实际上是折现的反向处理。
 * 
 * @author SINGLEE
 *
 */
public class IrrUtil {
	/** 迭代次数 */
	public static int LOOPNUM = 1000;
	/** 最小差异 */
	public static final double MINDIF = 0.00000001;

	/**
	 * @desc 使用方法参考main方法
	 * @param cashFlow 资金流
	 * @return 收益率
	 */
	public static double getIrr(List<calBean> cashFlow) {
		double flowOut = cashFlow.get(0).getAmt();
		double minValue = 0d;
		double maxValue = 1d;
		double testValue = 0d;
		while (LOOPNUM > 0) {
			testValue = (minValue + maxValue) / 2;
			double npv = NPV(cashFlow, testValue);
			if (Math.abs(flowOut + npv) < MINDIF) {
				break;
			} else if (Math.abs(flowOut) > npv) {
				maxValue = testValue;
			} else {
				minValue = testValue;
			}
			LOOPNUM--;
		}
		return testValue;
	}

	public static double NPV(final List<calBean> flowInArr, final double rate) {
		double npv = 0;
		for (int i = 1; i < flowInArr.size(); i++) {
			npv += flowInArr.get(i).getAmt() / Math.pow(1 + rate / flowInArr.get(i).getQs(), flowInArr.get(i).getXs());
		}
		return npv;
	}

	public static void main(String[] args) throws Exception {

		/**
		 * 记录摊销明细
		 */
		/*
		 * List<AmorBean> amorBeans= new ArrayList<AmorBean>();
		 *//**
			 * 收息区间
			 */
		/*
		 * List<CashflowInterest> tmCashflowInterests = new
		 * ArrayList<CashflowInterest>(); CashflowInterest tmCashflowInterest = new
		 * CashflowInterest(); tmCashflowInterest.setRefBeginDate("2017-10-23");
		 * tmCashflowInterest.setRefEndDate("2017-10-25");
		 * tmCashflowInterest.setInterestAmt(0.07222);
		 * tmCashflowInterests.add(tmCashflowInterest);
		 *//**
			 * 封装利率区间
			 */
		/*
		 * List<InterestRange> interestRanges = new ArrayList<InterestRange>();
		 * InterestRange interestRange = new InterestRange();
		 * interestRange.setStartDate("2017-10-23");interestRange.setEndDate(
		 * "2017-10-25");interestRange.setRate(0.130);
		 * interestRanges.add(interestRange);
		 * 
		 *//**
			 * 起息日 清算日 到期日
			 *//*
				 * String vDate = "2017-10-23"; String mDate = "2017-10-25"; double
				 * cPrice=99.92778; String basic = "Actual/360";
				 * 
				 * amorBeans = calAmorBeans(amorBeans, tmCashflowInterests, interestRanges,
				 * vDate, mDate, cPrice, basic, 100.00); for(AmorBean amorBean2 :amorBeans){
				 * System.out.println(amorBean2.toString()); }
				 */
		System.out.println(new BigDecimal("10").doubleValue());
	}

	public synchronized static List<AmorBean> calAmorBeans(List<AmorBean> amorBeans,
			List<CashflowInterest> tmCashflowInterests, List<InterestRange> interestRanges, String vDate, String mDate,
			double cPrice, String basic, double facePrice) throws Exception {

		boolean zyjFlag = false;
		if (cPrice > facePrice) {
			zyjFlag = true;
		} // 溢价
		else {
			zyjFlag = false;
		}
		/**
		 * 封装现金流
		 */
		List<calBean> flowInList = new ArrayList<calBean>();

		Map<String, Double> amtParams = new HashMap<String, Double>();
		amtParams.put(mDate, facePrice);

		AmorBean amorBean = null;
		amorBean = new AmorBean();
		amorBean.setSettAmt(cPrice);
		amorBean.setUnAmorAmt(Math.abs(facePrice - cPrice));
		amorBeans.add(amorBean);

		for (int i = 0; i < PlaningTools.daysBetween(vDate, mDate); i++) {
			LOOPNUM = 1000;
			flowInList.clear();
			/**
			 * 上一次的攤余成本+需要支付的利息（清算日到上一个付息日之間应付利息）
			 */
			flowInList.add(new calBean(-amorBeans.get(i).getSettAmt() - PlaningTools.daysBetween(
					getNearIntBDate(tmCashflowInterests, PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i)),
					PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i))
					* PlaningTools.getInterestRangeByDate(interestRanges,
							PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i))
					* 100.00 /
					/**
					 * 判断 是否为 ACT/ACT类型 如果是，那么需要判定所在年份是否为闰年，如果是那么366 否则365 其他按照ACT360 365计算
					 */
					(DayCountBasis.equal(basic).getMolecule().equalsIgnoreCase(DictConstants.DayCounter.ActualActual)
							? getActual(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i), true)
							: DayCountBasis.equal(basic).getDenominator()),
					1, 1));
			/**
			 * 填充收息现金流表
			 */
			for (CashflowInterest temp : tmCashflowInterests) {
				if (PlaningTools.compareDate2(temp.getRefEndDate(),
						PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i), "yyyy-MM-dd")) {
					flowInList.add(new calBean(
							temp.getInterestAmt() + (amtParams.get(temp.getRefEndDate()) == null ? 0.00
									: amtParams.get(temp.getRefEndDate())),
							PlaningTools.div(
									PlaningTools.daysBetween(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i),
											temp.getRefEndDate()) * 1d,
									PlaningTools.daysBetween(temp.getRefBeginDate(), temp.getRefEndDate()) * 1d, 12),
							PlaningTools.div(
									(DayCountBasis.equal(basic).getMolecule()
											.equalsIgnoreCase(DictConstants.DayCounter.ActualActual)
													? (getActual(temp.getRefBeginDate(),
															true) >= getActual(temp.getRefEndDate(), false)
																	? getActual(temp.getRefBeginDate(), true)
																	: getActual(temp.getRefEndDate(), false))
													: DayCountBasis.equal(basic).getDenominator()),
									PlaningTools.daysBetween(temp.getRefBeginDate(), temp.getRefEndDate()) * 1d, 12)));
				}
			}
			amorBean = new AmorBean();
			amorBean.setYeild(
					new BigDecimal(IrrUtil.getIrr(flowInList)).setScale(12, BigDecimal.ROUND_HALF_UP).doubleValue());
			amorBean.setDp1(NPV(flowInList, amorBean.getYeild()));
			flowInList.clear();
			flowInList.add(new calBean(0, 0, 0));
			/**
			 * 相差1一天的情况进行填充
			 */
			for (CashflowInterest temp : tmCashflowInterests) {
				if (PlaningTools.compareDate(temp.getRefEndDate(),
						PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i + 1), "yyyy-MM-dd")) {
					flowInList.add(new calBean(
							temp.getInterestAmt() + (amtParams.get(temp.getRefEndDate()) == null ? 0.00
									: amtParams.get(temp.getRefEndDate())),
							PlaningTools.div(
									PlaningTools.daysBetween(
											PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i + 1),
											temp.getRefEndDate()) * 1d,
									PlaningTools.daysBetween(temp.getRefBeginDate(), temp.getRefEndDate()) * 1d, 12),
							PlaningTools.div(
									(DayCountBasis.equal(basic).getMolecule()
											.equalsIgnoreCase(DictConstants.DayCounter.ActualActual)
													? (getActual(temp.getRefBeginDate(),
															true) >= getActual(temp.getRefEndDate(), false)
																	? getActual(temp.getRefBeginDate(), true)
																	: getActual(temp.getRefEndDate(), false))
													: DayCountBasis.equal(basic).getDenominator()),
									PlaningTools.daysBetween(temp.getRefBeginDate(), temp.getRefEndDate()) * 1d, 12)));
				}
			}
			if (i != PlaningTools.daysBetween(vDate, mDate) - 1) {
				amorBean.setDp2(NPV(flowInList, amorBean.getYeild()));
				amorBean.setActualIntAmt(amorBean.getDp2() - amorBean.getDp1());
				amorBean.setInterestAmt(PlaningTools.getInterestRangeByDate(interestRanges,
						PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i)) * 100.00
						/ getActual(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i), true));
				amorBean.setAmorAmt(zyjFlag == true ? amorBean.getInterestAmt() - amorBean.getActualIntAmt()
						: amorBean.getActualIntAmt() - amorBean.getInterestAmt());
				amorBean.setUnAmorAmt(amorBeans.get(i).getUnAmorAmt() - amorBean.getAmorAmt());
				amorBean.setSettAmt(zyjFlag == true ? (amorBeans.get(i).getSettAmt() - amorBean.getAmorAmt())
						: amorBeans.get(i).getSettAmt() + amorBean.getAmorAmt());
				amorBean.setSheOrder(i + 1);
				amorBean.setPostdate(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i));
				amorBeans.add(amorBean);
			} else {// 最后一期 使用倒挤的方式
				amorBean.setDp2(amorBeans.get(i).getDp2() + amorBeans.get(i).getUnAmorAmt());
				amorBean.setActualIntAmt(PlaningTools.getInterestRangeByDate(interestRanges,
						PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i)) * 100.00
						/ getActual(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i), true)
						+ amorBeans.get(i).getUnAmorAmt());
				amorBean.setInterestAmt(PlaningTools.getInterestRangeByDate(interestRanges,
						PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i)) * 100.00
						/ getActual(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i), true));
				amorBean.setAmorAmt(amorBeans.get(i).getUnAmorAmt());
				amorBean.setUnAmorAmt(0);
				amorBean.setSettAmt(zyjFlag == true ? (amorBeans.get(i).getSettAmt() - amorBean.getAmorAmt())
						: amorBeans.get(i).getSettAmt() + amorBean.getAmorAmt());
				amorBean.setSheOrder(i + 1);
				amorBean.setPostdate(PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, i));
				amorBeans.add(amorBean);
			}

		}

		return amorBeans;
	}

	/**
	 * 查找 清算日所在最近的付息区间
	 */
	public static String getNearIntBDate(List<CashflowInterest> tmCashflowInterests, String vDate)
			throws ParseException {
		for (CashflowInterest temp : tmCashflowInterests) {
			if (PlaningTools.compareDate(vDate, temp.getRefBeginDate(), "yyyy-MM-dd")
					&& PlaningTools.compareDate2(temp.getRefEndDate(), vDate, "yyyy-MM-dd")) {
				return temp.getRefBeginDate();
			}
		}
		return null;

	}

	public static boolean leapYear(int year) {
		if (((year % 100 == 0) && (year % 400 == 0)) || ((year % 100 != 0) && (year % 4 == 0))) {
			return true;}
		else {
			return false;}
	}

	public static int getActual(String vDate, boolean flag) throws ParseException {
		// 判断 vDate所在年份是否为闰年
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c2 = Calendar.getInstance();
		c2.setTime(sdf.parse(vDate));
		int year = Integer.parseInt(simpleDateFormat.format(sdf.parse(vDate)));
		if (leapYear(year)) {

			Calendar c1 = Calendar.getInstance();

			c1.setTime(sdf.parse(year + "-02-29"));
			if (flag) {
				if (c1.compareTo(c2) >= 0) {
                    return 366;
                } else {
					return 365;
				}
			} else {
				if (c1.compareTo(c2) <= 0) {
                    return 366;
                } else {
					return 365;
				}
			}
		}

		return 365;
	}

}
