package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.FuzzyQueryMapper;
import com.singlee.capital.base.service.FuzzyQueryService;
import com.singlee.capital.counterparty.model.CounterPartyFuzzyVo;
import com.singlee.capital.counterparty.model.PayBank;
import com.singlee.capital.market.model.TtMktBondIssuer;

/**
 * 模糊查询业务层
 * @author gm
 *
 */
@Service("fuzzyQueryService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FuzzyQueryServiceImpl implements FuzzyQueryService {

	/**模糊查询dao层*/
	@Autowired
	public FuzzyQueryMapper fuzzyQueryMapper;

	/**
	 * 发行人 模糊查询
	 * @param map
	 * 	bk_name：
	 *  limit：显示数量
	 * @return
	 */
	@Override
	public List<TtMktBondIssuer> selectBondIssuer(String bk_name,int limit){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("issuer_name", bk_name);
		map.put("limit", limit);
		return null;//fuzzyQueryMapper.selectBondIssuer(map);
	}
	
	/**
	 * 支付行号 模糊查询
	 * @param map
	 * 	bk_name：
	 *  limit：显示数量
	 * @return
	 */
	@Override
	public List<PayBank> selectPayBank(Map<String,Object> map){
		return fuzzyQueryMapper.selectPayBank(map);
	}
	
	/**
	 * 交易对手模糊查询
	 * @param map
	 * 	party_name:交易对手名称
	 *  limit：显示记录数
	 *  hasCashAcc:是否关联资金账户
	 * @return
	 */
	@Override
	public List<CounterPartyFuzzyVo> selectCounterParty(String party_name,int limit,boolean hasCashAcc){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("party_name", party_name);
		map.put("limit", limit);
		map.put("hasCashAcc", hasCashAcc);
		return fuzzyQueryMapper.selectCounterParty(map);
	}

}