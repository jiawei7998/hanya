package com.singlee.capital.base.cal;

public class calBean {
	@Override
	public String toString() {
		return "calBean [amt=" + amt + ", xs=" + xs + ", qs=" + qs + "]";
	}
	private double amt;
	private double xs;
	private double qs;
	public calBean(double amt, double xs, double qs) {
		super();
		this.amt = amt;
		this.xs = xs;
		this.qs = qs;
	}

	public double getQs() {
		return qs;
	}

	public void setQs(double qs) {
		this.qs = qs;
	}

	public double getAmt() {
		return amt;
	}
	
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getXs() {
		return xs;
	}
	public void setXs(double xs) {
		this.xs = xs;
	}
	public calBean(double amt, double xs) {
		super();
		this.amt = amt;
		this.xs = xs;
	}
}
