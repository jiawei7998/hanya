package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcFieldset;

/**
 * @projectName 同业业务管理系统
 * @className 自定义字段分组映射服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-30 下午5:45:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface FieldsetService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param fdsNo - 字段分组主键
	 * @return 字段分组对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public TcFieldset getFieldsetById(String fdsNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段分组对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public Page<TcFieldset> getFieldsetPage (Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段分组对象列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public List<TcFieldset> getFieldsetList (Map<String, Object> params);
	
	
	public List<TcFieldset> getFormFieldsetList (Map<String, Object> params);
	/**
	 * 新增
	 * 
	 * @param fieldset - 字段分组对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void createFieldset(TcFieldset fieldset);
	
	/**
	 * 修改
	 * 
	 * @param fieldset - 字段分组对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void updateFieldset(TcFieldset fieldset);
	
	/**
	 * 删除
	 * 
	 * @param fieldsetNoArray - 字段分组主键列表
	 * @author Hunter
	 * @date 2016-7-29
	 */
	public void deleteFieldset(String[] fdsNos);
	
	/**
	 * 20170517 通过产品获得参与方的配置项
	 * @param params
	 * @return
	 */
	public List<TcFieldset> getFormPartyFieldsetList(Map<String, Object> params);
}
