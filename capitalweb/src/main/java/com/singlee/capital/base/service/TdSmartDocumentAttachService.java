package com.singlee.capital.base.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TdSmartDocumentAttach;

/**
 * 影像控件使用的SERVICE 服务
 * @author kf0737
 *
 */
public interface TdSmartDocumentAttachService {
	/**
	 * 上传单一文件到服务器
	 * @param tdSmartDocumentAttach
	 * @return
	 */
	public boolean uploadSmartDocumentToServer(TdSmartDocumentAttach tdSmartDocumentAttach);
	
	
	List<TdSmartDocumentAttach>  selectTdSmartDocumentAttach(Map<String, String> paramsMap);
	/**
	 * 查询唯一文件
	 * @param attachId
	 * @return
	 */
	public  TdSmartDocumentAttach getTdSmartDocumentAttach(String attachId);
	/**
	 * 删除唯一的文件
	 * @param attachId
	 */
	public void deleteTdSmartDocumentAttach(String attachId);
	
}
