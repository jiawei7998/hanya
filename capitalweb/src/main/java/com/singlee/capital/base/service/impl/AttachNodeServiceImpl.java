package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcAttachNodeMapper;
import com.singlee.capital.base.model.TcAttachNode;
import com.singlee.capital.base.model.TcAttachNodeRelation;
import com.singlee.capital.base.service.AttachNodeServer;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AttachNodeServiceImpl implements AttachNodeServer{
	@Autowired
	private TcAttachNodeMapper attachNodeMapper;
	
	@Override
	public Page<TcAttachNode> search(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return attachNodeMapper.search(params,ParameterUtil.getRowBounds(params)) ;
	}

	@Override
	public void createAttachNode(TcAttachNode ta) {
		if(attachNodeMapper.selectByPrimaryKey(ta) == null){
			attachNodeMapper.insert(ta);
		}else{
			JY.raiseRException(MsgUtils.getMessage("该节点已存在"));
		}
	}

	@Override
	public void updateAttachNode(TcAttachNode ta) {
		if(attachNodeMapper.selectByPrimaryKey(ta)==null){
			JY.raiseRException(MsgUtils.getMessage("该节点不存在"));
		}else{
			 attachNodeMapper.updateByPrimaryKey(ta);
		}
	}
	@Override
	public void deleteAttachNode(TcAttachNode ta){
		if(attachNodeMapper.selectByPrimaryKey(ta)==null){
			JY.raiseRException(MsgUtils.getMessage("该节点不存在"));
		}else{
			 attachNodeMapper.deleteByPrimaryKey(ta);
		}
	}

	@Override
	public List<TcAttachNodeRelation> searchAttachNodeRelationList(Map<String, Object> params) {
		return attachNodeMapper.searchAttachNodeRelationList(params);
	}

	@Override
	public void addAttachRelation(Map<String, Object> params) {
		 // 判断关系是否配置
		if(params != null ){
			String ids = params.get("filterList")+"";
			String attachTypes =  params.get("attachTypes")+"";
			if(ids != null && ids.length()>0){
				String [] idArray = ids.split(",");
				String [] attachTypeArray = attachTypes.split(",");
				for(int i = 0 ; i<idArray.length ;i++){
					Map<String, Object> insertMap = new HashMap<String, Object>();
					insertMap.put("parId",params.get("treeId"));
					insertMap.put("attachId",idArray[i]);
					insertMap.put("deleteFlag",0);
					insertMap.put("attachType",attachTypeArray[i]);
					attachNodeMapper.insertRealtion(insertMap);
				}
			}
		}
		
	}

	@Override
	public void delAttachRelation(Map<String, Object> params) {
		Map<String, Object> delMap = new HashMap<String, Object>();
		delMap.put("parId",params.get("treeId"));
		delMap.put("filterList",params.get("filterList"));
		attachNodeMapper.delAttachRelation(delMap);
	}
	
}
