package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TtMktCalendarsDate;

import tk.mybatis.mapper.common.Mapper;

public interface TtMktCalendarsDateMapper extends Mapper<TtMktCalendarsDate>{
	
	public Page<TtMktCalendarsDate> TtMktCalendarDatePage(Map<String, Object> param,RowBounds rowBounds);
	/**
	 * 添加交易日历
	 * @param ttMktCalendarDate
	 */
	public void insertTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	/**
	 * 删除交易日历
	 * @param ttMktCalendarDate
	 */
	public void deleteTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	/**
	 * 修改交易日历
	 * @param ttMktCalendarDate
	 */
	public void updateTtMktCalendarDate(TtMktCalendarsDate ttMktCalendarDate);
	
	/**
	 * 根据交易日历id查询有多少条数据
	 * @param ttMktCalendarDate
	 * @return
	 */
	public int queryTtMktCalendarDateById(TtMktCalendarsDate ttMktCalendarDate);
	
	public List<TtMktCalendarsDate> queryTtMktCalendarDate();
	
	public List<TtMktCalendarsDate> queryListTtMktCalendarDate();
	
	public List<TtMktCalendarsDate> getTtMktCalendarByMonth(Map<String, Object> param);
}
