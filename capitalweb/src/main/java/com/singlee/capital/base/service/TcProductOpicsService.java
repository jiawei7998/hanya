package com.singlee.capital.base.service;


import com.singlee.capital.base.model.TcProductOpics;

public interface TcProductOpicsService {

    TcProductOpics selectByPrdCode(String fprdCode);
}
