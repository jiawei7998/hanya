package com.singlee.capital.base.service;

import java.util.List;

import com.singlee.capital.base.model.TcAttach;
import com.singlee.capital.base.model.TdSmartDocumentAttach;

/**
 * @projectName 
 * @className 附件服务
 * @description TODO
 * @author Lyon
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AttachService {

	/**
	 * 获得 
	 * @param id
	 * @return
	 */
	TcAttach getAttach(String id);
	/**
	 * 获得 
	 * @param refType - 引用类型
	 * @param refId - 引用编号
	 * @return
	 */
	List<TcAttach> searchAttach(String refType, String refId);
	/**
	 * 新增
	 * 
	 * @param field - 字段对象
	 */
	public void createAttach(TdSmartDocumentAttach a);
	
	/**
	 * 新增
	 * 
	 * @param field - 字段对象
	 */
	public void createAttach(TcAttach a);
	
	/**
	 * 修改
	 * 
	 * @param field - 字段对象
	 */
	public void updateAttach(TcAttach a);
	
	/**
	 * 删除
	 * 
	 * @param attachId - 字段主键
	 */
	public void deleteAttach(String attachId);
	
	/**
	 * 清理未关联附件
	 * 
	 * @param attachId - 字段主键
	 */
	public void clearAttach();

}
