package com.singlee.capital.base.participant.model;

import java.io.Serializable;
/**
 * 参与方类型表
 * @author SINGLEE
 *
 */
public class TcParticipant implements Serializable {
	private static final long serialVersionUID = 1L;
	private String partyId;//参与方代码
	private String partyName;//参与方名称
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	@Override
	public String toString() {
		return "TcParticipant [partyId=" + partyId + ", partyName=" + partyName
				+ "]";
	}
}
	
