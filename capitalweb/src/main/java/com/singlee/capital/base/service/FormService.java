package com.singlee.capital.base.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcForm;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-2 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface FormService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param prdNo - 表单主键
	 * @return 表单对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public TcForm getFormById(String formNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public Page<TcForm> getFormPage (Map<String, Object> params);
	
	/**
	 * 新增
	 * 
	 * @param Form - 表单对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void createForm(TcForm form);
	
	/**
	 * 修改
	 * 
	 * @param form - 表单对象
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void updateForm(TcForm form);
	
	/**
	 * 删除
	 * 
	 * @param formNos - 表单主键列表
	 * @author Hunter
	 * @date 2016-8-2
	 */
	public void deleteForm(String[] formNos);
}
