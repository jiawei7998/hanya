package com.singlee.capital.base.participant.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.mapper.TcParticipantMapper;
import com.singlee.capital.base.participant.model.TcParticipant;
import com.singlee.capital.base.participant.service.TcParticipantService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
@Service
public class TcParticipantServiceImpl implements TcParticipantService {
	
	@Autowired
	TcParticipantMapper tcParticipantMapper;
	
	@Override
	public Page<TcParticipant> getParticipantList(Map<String, Object> params) {
		return tcParticipantMapper.getParticipantList(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addParticipant(TcParticipant tcParticipant) throws RException {
		List<TcParticipant> participantList = tcParticipantMapper.findParticipantByPartyId(tcParticipant.getPartyId());
		if(!participantList.isEmpty()) {
			JY.raiseRException("参与方代码已存在");
		}
		tcParticipantMapper.insert(tcParticipant);
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateParticipant(TcParticipant tcParticipant) {
		tcParticipantMapper.updateParticipant(tcParticipant);
	}
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteParticipant(Map<String, Object> map) {
		tcParticipantMapper.deleteParticipant(map);
	}
}
