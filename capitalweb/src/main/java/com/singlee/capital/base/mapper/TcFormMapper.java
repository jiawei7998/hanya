package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcForm;

/**
 * @projectName 同业业务管理系统
 * @className 自定义表单持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcFormMapper extends Mapper<TcForm> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param formNo - 表单主键
	 * @return 表单对象
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public TcForm getFormById(String formNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public Page<TcForm> getFormList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 表单对象列表
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public List<TcForm> getFormList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 记录数
	 * @author Hunter
	 * @date 2016-8-1
	 */
	public int judgeDuplicate(Map<String, Object> params);
}