package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.model.TcAttachTreeFile;
import com.singlee.capital.base.model.TcAttachTreeRef;
import com.singlee.capital.base.service.TcAttachTreeRefService;
import com.singlee.capital.base.service.TcAttachTreeRefVoService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;

/**
 * @projectName 同业业务管理系统
 * @className 附件树控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:39:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/TcAttachTreeRefController")
public class TcAttachTreeRefController extends CommonController {
	
	@Resource
	TcAttachTreeRefService tcAttachTreeRefService;
	@Resource
	TcAttachTreeRefVoService tcAttachTreeRefVoService;
	
	@ResponseBody
	@RequestMapping(value = "/searchTcAttachTreeRef")
	public RetMsg<List<TcAttachTreeFile>> searchTcAttachTreeRef(@RequestBody Map<String, String> params) throws RException {
		List<TcAttachTreeFile> list = tcAttachTreeRefVoService.getTcAttachTreeRefVo(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTcAttachTreeRefByPrd")
	public RetMsg<List<TcAttachTreeFile>> searchTcAttachTreeRefByPrd(@RequestBody Map<String, String> params) throws RException {
		List<TcAttachTreeFile> list = tcAttachTreeRefVoService.getTcAttachTreeRefVoByPrd(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/insertTcAttachTreeFef")
	public RetMsg<Serializable> insertTcAttachTreeFef(@RequestBody TcAttachTreeRef ref) throws RException {
		tcAttachTreeRefService.insertTcAttachTreeFef(ref);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteTcAttachTreeFef")
	public RetMsg<Serializable> deleteTcAttachTreeFef(@RequestBody TcAttachTreeRef ref) throws RException {
		tcAttachTreeRefService.deleteTcAttachTreeFef(ref);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTcAttachTreeRef")
	public RetMsg<TcAttachTreeRef> getTcAttachTreeRef(@RequestBody TcAttachTreeRef ref) throws RException {
		TcAttachTreeRef list = tcAttachTreeRefService.getTcAttachTreeRefById(ref);
		return RetMsgHelper.ok(list);
	}
	
}
