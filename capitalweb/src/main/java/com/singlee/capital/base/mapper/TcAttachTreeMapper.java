package com.singlee.capital.base.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachTree;
import com.singlee.capital.base.model.TcAttachTreeNodeVo;
import com.singlee.capital.common.spring.mybatis.MyMapper;

/**
 * 
 * @author x230i
 *
 */
public interface TcAttachTreeMapper extends MyMapper<TcAttachTree> {
	
	/**
	 * 根据请求条件 获得附件列表 
	 * @param params
	 * @return 
	 */
	Page<TcAttachTree> selectTcAttachTree(Map<String, String> params,RowBounds rb);
	
	
	int judgeDuplicate(Map<String, String> params);
	
	Page<TcAttachTreeNodeVo> getTcAttachTreeNodeVo(
			Map<String, String> params, RowBounds rb);
}