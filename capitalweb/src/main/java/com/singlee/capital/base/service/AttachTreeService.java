package com.singlee.capital.base.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcAttachTree;
import com.singlee.capital.base.model.TcAttachTreeNodeVo;

/**
 * @projectName 
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @createDate 2017-1-3 下午3:25:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AttachTreeService {

	/**
	 * 查询 
	 * @param params - 查询参数
	 * @return 附件树对象集合
	 */
	Page<TcAttachTree> searchAttachTree(Map<String, String> params,RowBounds rb);
	
	/**
	 * 新增
	 * 
	 * @param attachTree - 附件树对象
	 */
	public void createAttachTree(TcAttachTree attachTree);
	
	/**
	 * 修改
	 * 
	 * @param attachTree - 附件树对象
	 */
	public void updateAttachTree(Map<String, String> params);
	
	/**
	 * 删除
	 * 
	 * @param attachTreeIds - 附件树主键数组
	 */
	public void deleteAttachTree(String[] attachTreeIds);
	
	/**
	 * 查询 
	 * @param params - 查询参数
	 * @return 附件树对象集合
	 */
	Page<TcAttachTreeNodeVo> getTcAttachTreeNodeVo(Map<String, String> params, RowBounds rb);
	
}
