package com.singlee.capital.base.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TcFieldsetMapper;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.service.FieldsetService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品字段分组映射服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FieldsetServiceImpl implements FieldsetService {
	
	@Autowired
	private TcFieldsetMapper fieldsetMapper;
	
	@Override
	public TcFieldset getFieldsetById(String fdsNo) {
		return fieldsetMapper.getFieldsetById(fdsNo); 
	}

	@Override
	public Page<TcFieldset> getFieldsetPage(Map<String, Object> params) {
		return fieldsetMapper.getFieldsetList(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TcFieldset> getFieldsetList(Map<String, Object> params) {
		return fieldsetMapper.getFieldsetList(params);
	}
	
	@Override
	@AutoLogMethod(value="创建自定义字段分组")
	public void createFieldset(TcFieldset fieldset) {
		fieldset.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		fieldsetMapper.insert(fieldset);
	}

	@Override
	@AutoLogMethod(value="修改自定义字段分组")
	public void updateFieldset(TcFieldset fieldset) {
		fieldset.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		fieldsetMapper.updateByPrimaryKeySelective(fieldset);
	}

	@Override
	@AutoLogMethod(value="删除自定义字段分组")
	public void deleteFieldset(String[] fdsNos) {
		for (int i = 0; i < fdsNos.length; i++) {
			fieldsetMapper.deleteByPrimaryKey(fdsNos[i]);
		}
	}

	@Override
	public List<TcFieldset> getFormFieldsetList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		List<TcFieldset> resultFieldsets = fieldsetMapper.getFormFieldsetList(params);
		for(int i  = 0; i<resultFieldsets.size();i++){
			if(null != resultFieldsets.get(i).getAtachFormid() && !"".equalsIgnoreCase(resultFieldsets.get(i).getAtachFormid()))
			{
				params.put("formNo", resultFieldsets.get(i).getAtachFormid());
				resultFieldsets.get(i).setFieldSets(fieldsetMapper.getFormFieldsetList(params));
			}
		}
		return resultFieldsets;
	}

	/**
	 * 获得参与方配置
	 */
	@Override
	public List<TcFieldset> getFormPartyFieldsetList(Map<String, Object> params) {
		
		return fieldsetMapper.getFormPartyFieldsetList(params);
	}
	/***
	 * ----以上为对外服务-------------------------------
	 */
}
