package com.singlee.capital.base.participant.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.participant.model.TcParticipant;

public interface TcParticipantService {
	/**
	 * 获取全部参与方配置列表
	 * @param params
	 * @return
	 */
	public Page<TcParticipant> getParticipantList(Map<String, Object> params);
	
	/**
	 * 新增参与方类型
	 * @param tcParticipant
	 */
	public void addParticipant(TcParticipant tcParticipant);
	
	/**
	 * 更新参与方类型
	 * @param tcParticipant
	 */
	public void updateParticipant(TcParticipant tcParticipant);
	
	/**
	 * 删除参与方类型
	 * @param map
	 */
	public void deleteParticipant(Map<String, Object> map);
}
