package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcField;
import com.singlee.capital.base.service.FieldService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
/**
 * @projectName 同业业务管理系统
 * @className 自定义字段控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午11:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/FieldController")
public class FieldController extends CommonController {
	
	@Autowired
	private FieldService fieldService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFieldPage")
	public RetMsg<PageInfo<TcField>> searchFieldPage(@RequestBody Map<String,Object> params){
		Page<TcField> page = fieldService.getFieldPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFieldList")
	public RetMsg<List<TcField>> searchFieldList(@RequestBody Map<String,Object> params){
		List<TcField> page = fieldService.getFieldList(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 新增
	 * 
	 * @param field - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/addField")
	public RetMsg<Serializable> addField(@RequestBody TcField field) {
		fieldService.createField(field);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * 
	 * @param field - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/editField")
	public RetMsg<Serializable> editField(@RequestBody Map<String,Object> params){
		fieldService.updateField(params);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 * 
	 * @param field - 字段对象
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/removeField")
	public RetMsg<Serializable> removeField(@RequestBody String[] fldNos) {
		fieldService.deleteField(fldNos);
		return RetMsgHelper.ok();
	}

}
