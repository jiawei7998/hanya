package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcFieldset;

/**
 * @projectName 同业业务管理系统
 * @className 自定义字段分组分组映射持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-30 下午3:31:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcFieldsetMapper extends Mapper<TcFieldset> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param fdsNo - 字段分组主键
	 * @return 字段分组对象
	 * @author Hunter
	 * @date 2016-9-3
	 */
	public TcFieldset getFieldsetById(String fdsNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 字段分组对象列表
	 * @author Hunter
	 * @date 2016-9-3
	 */
	public Page<TcFieldset> getFieldsetList(Map<String, Object> params, RowBounds rb);
	
	public List<TcFieldset> getFormFieldsetList(Map<String, Object> params);
	/**
	 * 获得参与方配置 20170517
	 * @param params
	 * @return
	 */
	public List<TcFieldset> getFormPartyFieldsetList(Map<String, Object> params);
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 字段分组分组映射对象列表
	 * @author Hunter
	 * @date 2016-9-3
	 */
	public List<TcFieldset> getFieldsetList(Map<String, Object> params);
	
}