package com.singlee.capital.base.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TcFieldset;
import com.singlee.capital.base.service.FieldsetService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
/**
 * @projectName 同业业务管理系统
 * @className 自定义字段分组映射控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-31 下午7:30:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/FieldsetController")
public class FieldsetController extends CommonController {
	
	@Autowired
	private FieldsetService fieldsetService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-3
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFieldsetPage")
	public RetMsg<PageInfo<TcFieldset>> searchFieldsetPage(@RequestBody Map<String,Object> params){
		Page<TcFieldset> page = fieldsetService.getFieldsetPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-3
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFieldsetList")
	public RetMsg<List<TcFieldset>> searchProductFieldsetList(@RequestBody Map<String,Object> params){
		List<TcFieldset> list = fieldsetService.getFieldsetList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 新增
	 * 
	 * @param fieldset - 字段分组对象
	 * @author Hunter
	 * @date 2016-9-3
	 */
	@ResponseBody
	@RequestMapping(value = "/addFieldset")
	public RetMsg<Serializable> addField(@RequestBody TcFieldset fieldset) {
		fieldsetService.createFieldset(fieldset);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * 
	 * @param fieldset - 字段分组对象
	 * @author Hunter
	 * @date 2016-9-3
	 */
	@ResponseBody
	@RequestMapping(value = "/editFieldset")
	public RetMsg<Serializable> editFieldset(@RequestBody TcFieldset fieldset) {
		fieldsetService.updateFieldset(fieldset);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除
	 * 
	 * @param fieldset - 字段分组对象
	 * @author Hunter
	 * @date 2016-9-3
	 */
	@ResponseBody
	@RequestMapping(value = "/removeFieldset")
	public RetMsg<Serializable> removeFieldset(@RequestBody String[] fdsNos) {
		fieldsetService.deleteFieldset(fdsNos);
		return RetMsgHelper.ok();
	}
}
