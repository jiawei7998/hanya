package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.model.TcProductForm;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-1 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TcProductFormMapper extends Mapper<TcProductForm> {

	/**
	 * 根据请求参数查询列表
	 * 
	 * @param params - 请求参数
	 * @return 产品对象列表
	 * @author Hunter
	 * @date 2016-10-11
	 */
	public List<TcProductForm> getProductFormList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询列表
	 * 
	 * @param prdNo - 产品编号
	 * @author Hunter
	 * @date 2016-10-11
	 */
	public void deleteProductFormByProductId(String prdNo);
}