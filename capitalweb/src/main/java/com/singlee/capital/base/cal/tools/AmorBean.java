package com.singlee.capital.base.cal.tools;

import java.io.Serializable;
/**
 * 用于记录摊销数据
 * @author SINGLEE
 *
 */
public class AmorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6152324569813017536L;
	/**
	 * 期数
	 */
	private int sheOrder;
	
	private double dp1;
	
	private double dp2;
	
	private double yeild;
	
	public double getDp1() {
		return dp1;
	}
	public void setDp1(double dp1) {
		this.dp1 = dp1;
	}
	public double getDp2() {
		return dp2;
	}
	public void setDp2(double dp2) {
		this.dp2 = dp2;
	}
	public double getYeild() {
		return yeild;
	}
	public void setYeild(double yeild) {
		this.yeild = yeild;
	}
	private String postdate;
	/**
	 * 应付利息
	 * 面值*票面利率
	 */
	private double interestAmt;
	/**
	 * 实际利息
	 * 摊销金额*实际利率
	 */
	private double actualIntAmt;
	/**
	 * 摊销金额
	 * 应计利息-实际利息
	 */
	private double amorAmt;
	/**
	 * 未摊销金额
	 * 折溢价-已摊销金额
	 */
	private double unAmorAmt;
	/**
	 * 摊销金额（头寸）
	 */
	private double actAmorAmt;
	/**
	 * 摊销调整（用户还本的时候）
	 */
	private double adjAmorAmt;
	
	public double getActAmorAmt() {
		return actAmorAmt;
	}
	public void setActAmorAmt(double actAmorAmt) {
		this.actAmorAmt = actAmorAmt;
	}
	public double getAdjAmorAmt() {
		return adjAmorAmt;
	}
	public void setAdjAmorAmt(double adjAmorAmt) {
		this.adjAmorAmt = adjAmorAmt;
	}
	/**
	 * 摊余成本
	 * 与实际利率计算实际利息
	 */
	private double settAmt;
	public double getInterestAmt() {
		return interestAmt;
	}
	public void setInterestAmt(double interestAmt) {
		this.interestAmt = interestAmt;
	}
	public double getActualIntAmt() {
		return actualIntAmt;
	}
	public void setActualIntAmt(double actualIntAmt) {
		this.actualIntAmt = actualIntAmt;
	}
	public double getAmorAmt() {
		return amorAmt;
	}
	public void setAmorAmt(double amorAmt) {
		this.amorAmt = amorAmt;
	}
	public double getUnAmorAmt() {
		return unAmorAmt;
	}
	public void setUnAmorAmt(double unAmorAmt) {
		this.unAmorAmt = unAmorAmt;
	}
	public double getSettAmt() {
		return settAmt;
	}
	public void setSettAmt(double settAmt) {
		this.settAmt = settAmt;
	}
	public int getSheOrder() {
		return sheOrder;
	}
	public void setSheOrder(int sheOrder) {
		this.sheOrder = sheOrder;
	}
	
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	@Override
	public String toString() {
		return "AmorBean [sheOrder=" + sheOrder + ", dp1=" + dp1 + ", dp2="
				+ dp2 + ", yeild=" + yeild + ", postdate=" + postdate
				+ ", interestAmt=" + interestAmt + ", actualIntAmt="
				+ actualIntAmt + ", amorAmt=" + amorAmt + ", unAmorAmt="
				+ unAmorAmt + ", actAmorAmt=" + actAmorAmt + ", adjAmorAmt="
				+ adjAmorAmt + ", settAmt=" + settAmt + "]";
	}
	
		
}
