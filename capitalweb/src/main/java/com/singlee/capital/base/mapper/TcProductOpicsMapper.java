package com.singlee.capital.base.mapper;

import com.singlee.capital.base.model.TcProductOpics;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：meigy
 * @date ：Created in 2021/11/13 19:26
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface TcProductOpicsMapper extends Mapper<TcProductOpics> {

    TcProductOpics selectByPrdCode(@Param("fprdCode") String prdCode);

}