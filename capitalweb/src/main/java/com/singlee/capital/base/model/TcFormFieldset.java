package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @projectName 同业业务管理系统
 * @className 表单分组字段映射
 * @description TODO
 * @author 倪航
 * @createDate 2016-8-30 下午2:47:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TC_FORM_FIELDSET_MAP")
public class TcFormFieldset implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3525667688060268039L;
	/**
	 * 表单编号 序列自动生成
	 */
	@Id
	private String formNo;
	/**
	 * 字段分组编号 序列自动生成
	 */
	@Id
	private String fdsNo;
	/**
	 * 表单索引 生成表单时的顺序
	 */
	private Integer formIndex;
	/**
	 * 关联字段分组字段
	 */
	@Transient
	private List<TcFormFieldsetField> formFieldsetFields;
	
	/**
	 * 关联字段分组
	 */
	@Transient
	private TcFieldset fieldset;
	
	public String getFormNo() {
		return formNo;
	}
	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}
	public String getFdsNo() {
		return fdsNo;
	}
	public void setFdsNo(String fdsNo) {
		this.fdsNo = fdsNo;
	}
	public Integer getFormIndex() {
		return formIndex;
	}
	public void setFormIndex(Integer formIndex) {
		this.formIndex = formIndex;
	}
	public TcFieldset getFieldset() {
		return fieldset;
	}
	public void setFieldset(TcFieldset fieldset) {
		this.fieldset = fieldset;
	}
	public List<TcFormFieldsetField> getFormFieldsetFields() {
		return formFieldsetFields;
	}
	public void setFormFieldsetFields(
			List<TcFormFieldsetField> formFieldsetFields) {
		this.formFieldsetFields = formFieldsetFields;
	}
}
