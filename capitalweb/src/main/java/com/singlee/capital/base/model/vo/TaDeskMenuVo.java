package com.singlee.capital.base.model.vo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Transient;

public class TaDeskMenuVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//台子名称
	private String name;
	//台子ICON
	private String icon;
	//台子文本
	private String text;
	//台子URL
	private String url;
	
	private String uid;
	
	@Transient
	private List<TaDeskMenuVo> children;
	
	

	public List<TaDeskMenuVo> getChildren() {
		return children;
	}

	public void setChildren(List<TaDeskMenuVo> children) {
		this.children = children;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
