package com.singlee.capital.base.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_PRODUCT_FLOW_TYPE_MAP")
public class TtProductFlowTypeMap implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * 产品种类编号
	 */
	private String productTypeNo;
	/**
	 * 流程类型编号
	 */
	private String flowTypeNo;
	
	public String getFlowTypeNo() {
		return flowTypeNo;
	}
	public void setFlowTypeNo(String flowTypeNo) {
		this.flowTypeNo = flowTypeNo;
	}
	public String getProductTypeNo() {
		return productTypeNo;
	}
	public void setProductTypeNo(String productTypeNo) {
		this.productTypeNo = productTypeNo;
	}
}