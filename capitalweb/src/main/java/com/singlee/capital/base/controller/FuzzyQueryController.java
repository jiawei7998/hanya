package com.singlee.capital.base.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.base.service.FuzzyQueryService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.counterparty.model.CounterPartyFuzzyVo;
import com.singlee.capital.counterparty.model.PayBank;
import com.singlee.capital.market.model.TtMktBondIssuer;
import com.singlee.capital.market.service.MktBondIssuerService;
import com.singlee.capital.system.controller.CommonController;

/**
 * 模糊查询统一管理
 * 
 * @author gm
 * 
 */
@Controller
@RequestMapping(value = "/FuzzyQueryController")
public class FuzzyQueryController extends CommonController {

	/** 模糊查询业务层 */
	@Autowired
	public FuzzyQueryService fuzzyQueryService;
	
	/** 发行人服务层 */
	@Autowired
	private MktBondIssuerService bondIssuerService;

	/**
	 * 发行人分页
	 * 
	 * @param map
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageBondIssuer")
	public RetMsg<PageInfo<TtMktBondIssuer>> searchPageBondIssuer(@RequestBody Map<String,Object> params) throws RException {
		Page<TtMktBondIssuer> page = bondIssuerService.getBondIssuerPage(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 行号 行名 模糊查询
	 * 
	 * @param map
	 *            bk_name：行名或者行号 limit：显示数量
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/selectBank")
	public List<PayBank> selectBank(@RequestBody Map<String,Object> map) throws IOException {
		String bk_name = ParameterUtil.getString(map, "text", "");
		List<PayBank> list = new ArrayList<PayBank>();
		//if(!("".equals(bk_name))){
			map.put("bk_name", bk_name.replace(' ', '%'));
			map.put("limit", "20");
			list =  fuzzyQueryService.selectPayBank(map);
		//}
		return list;
	}

	/**
	 * 交易对手模糊查询
	 * 
	 * @param map
	 *            party_name:交易对手名称 limit:显示记录数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/selectCP")
	public List<CounterPartyFuzzyVo> selectCP(@RequestBody Map<String,Object> map) throws IOException {
		String cp = ParameterUtil.getString(map, "text", "");
		cp =cp.replace(' ', '%');
		return fuzzyQueryService.selectCounterParty(cp, 15, false);
	}
}
