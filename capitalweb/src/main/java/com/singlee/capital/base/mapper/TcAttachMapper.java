package com.singlee.capital.base.mapper;

import java.util.List;


import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.base.model.TcAttach;

/**
 * 
 * @author x230i
 *
 */
public interface TcAttachMapper extends Mapper<TcAttach> {
	
	/**
	 * 根据指定的关联类型 和关联 id 获得附件列表 
	 * @param refType
	 * @param refId
	 * @return
	 */
	List<TcAttach> selectTcAttach(@Param("refType") String refType, @Param("refId") String refId);
	
	/**
	 * 
	 */
	int deleteTcAttach(@Param("refType") String refType, @Param("refId") String refId);
	
	/**
	 * 
	 */
	int clearTcAttach();
}