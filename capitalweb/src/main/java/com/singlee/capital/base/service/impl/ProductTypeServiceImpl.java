package com.singlee.capital.base.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.base.mapper.TcProductTypeMapper;
import com.singlee.capital.base.mapper.TtProductFlowTypeMapMapper;
import com.singlee.capital.base.model.TcProductType;
import com.singlee.capital.base.model.TtProductFlowTypeMap;
import com.singlee.capital.base.service.ProductTypeService;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.flow.pojo.vo.ProductFlowTreeView;


/**
 * 产品类型 服务 
 * @author xuhui
 *
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService{

	@Autowired
	private TcProductTypeMapper mapper;
	
	@Autowired
	private TtProductFlowTypeMapMapper ttProductFlowTypeMapMapper;
	
	/** 批量Dao */
	@Autowired
	private BatchDao batchDao;

	
	/**
	 * 查询产品类型 
	 * @return
	 */
	@Override
	public List<TcProductType> search(){
		List<TcProductType> list = mapper.selectAll();
		return TreeUtil.mergeChildrenList(list, "2");
	}

	@Override
	public void addProductType(TcProductType prdtype) {
		mapper.insert(prdtype);
	}

	@Override
	public TcProductType getProductTypeByNo(String prdtypeNo) {
		return mapper.selectByPrimaryKey(prdtypeNo);
	}

	@Override
	public void updateProductType(TcProductType prdType) {
		
		mapper.updateByPrimaryKeySelective(prdType);
	}
	
	@Override
	public List<TcProductType> getParentProductType(String prd_type_no) {
		return mapper.getParentProductType(prd_type_no);
	}

	@Override
	public List<ProductFlowTreeView> searchProductTypeFlowView() {
		return TreeUtil.mergeChildrenList(mapper.searchProductTypeFlowView(), "2");
	}
	
	@Override
	public List<TtProductFlowTypeMap> getFlowTypeByProductType(Map<String, Object> map) {
		return ttProductFlowTypeMapMapper.getFlowTypeByProductType(map);
	}

	@Override
	public void saveFlowTypeByProductType(Map<String, Object> map) {
		String[] flowTypeNos = StringUtil.isNotEmpty(ParameterUtil.getString(map, "flowTypeNos", ""))?ParameterUtil.getString(map, "flowTypeNos", "").split(","):null;
		String prdTypeNo = ParameterUtil.getString(map, "prdTypeNo", "");
		List<TtProductFlowTypeMap> list = new ArrayList<TtProductFlowTypeMap> ();
		//先删除旧的再添加新的
		ttProductFlowTypeMapMapper.removeFlowTypeByProductType(map);
		if(flowTypeNos != null){
			for(int i = 0; i < flowTypeNos.length; i++){
				TtProductFlowTypeMap ttProductFlowTypeMap = new TtProductFlowTypeMap();
				ttProductFlowTypeMap.setProductTypeNo(prdTypeNo);
				ttProductFlowTypeMap.setFlowTypeNo(flowTypeNos[i]);
				list.add(ttProductFlowTypeMap);
			}
		}
		
		batchDao.batch("com.singlee.capital.base.mapper.TtProductFlowTypeMapMapper.insert", list);
	}

	@Override
	public List<TcProductType> getProductTypeList(Map<String, Object> map) {
		return mapper.listProductType(map);
	}
	
}
