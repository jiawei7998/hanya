package com.singlee.capital.base.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="TD_SMARTDOCUMENT_ATTACH")
public class TdSmartDocumentAttach implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_ATTACH.NEXTVAL FROM DUAL")
	private String attachId;//文档唯一编号
	private String url;//smartDocumentCache影像路径
	private String appCode;//业务系统
	private String archiveTypeCode;//业务类型
	private String archivePath;//档案路径
	private String userName;//账号
	private String refTreeNodeId;//树形结构ID（NODE）
	private String contentType;//附件内容类型
	private int contentSize;//附件尺寸
	private String attachName;//附件名称
	private String attachStatus;//附件状态
	private byte[] attachBlob;//本地文件备份			
	private String orderId;//审批流水号
	private String tradeId;//正式申请流水号
	private String indexFile;//影像索引
	private String createUserId;//创建用户ID 
	private String createUserName;//创建用户名称
	private String createInstId;//创建机构ID
	private String createInstName;//创建机构名称
	private String createTime;//创建时间
	private String updateUserId;//修改用户ID 
	private String updateUserName;//修改用户名称
	private String updateInstId;//修改机构ID
	private String updateInstName;//修改机构名称
	private String updateTime;//修改时间
	
	@Transient
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAttachId() {
		return attachId;
	}
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public String getArchiveTypeCode() {
		return archiveTypeCode;
	}
	public void setArchiveTypeCode(String archiveTypeCode) {
		this.archiveTypeCode = archiveTypeCode;
	}
	public String getArchivePath() {
		return archivePath;
	}
	public void setArchivePath(String archivePath) {
		this.archivePath = archivePath;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRefTreeNodeId() {
		return refTreeNodeId;
	}
	public void setRefTreeNodeId(String refTreeNodeId) {
		this.refTreeNodeId = refTreeNodeId;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getContentSize() {
		return contentSize;
	}
	public void setContentSize(int contentSize) {
		this.contentSize = contentSize;
	}
	public String getAttachName() {
		return attachName;
	}
	public void setAttachName(String attachName) {
		this.attachName = attachName;
	}
	public String getAttachStatus() {
		return attachStatus;
	}
	public void setAttachStatus(String attachStatus) {
		this.attachStatus = attachStatus;
	}
	public byte[] getAttachBlob() {
		return attachBlob;
	}
	public void setAttachBlob(byte[] attachBlob) {
		this.attachBlob = attachBlob;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public String getIndexFile() {
		return indexFile;
	}
	public void setIndexFile(String indexFile) {
		this.indexFile = indexFile;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateInstId() {
		return createInstId;
	}
	public void setCreateInstId(String createInstId) {
		this.createInstId = createInstId;
	}
	public String getCreateInstName() {
		return createInstName;
	}
	public void setCreateInstName(String createInstName) {
		this.createInstName = createInstName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	public String getUpdateInstId() {
		return updateInstId;
	}
	public void setUpdateInstId(String updateInstId) {
		this.updateInstId = updateInstId;
	}
	public String getUpdateInstName() {
		return updateInstName;
	}
	public void setUpdateInstName(String updateInstName) {
		this.updateInstName = updateInstName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "TdSmartDocumentAttach [attachId=" + attachId + ", url=" + url
				+ ", appCode=" + appCode + ", archiveTypeCode="
				+ archiveTypeCode + ", archivePath=" + archivePath
				+ ", userName=" + userName + ", refTreeNodeId=" + refTreeNodeId
				+ ", contentType=" + contentType + ", contentSize="
				+ contentSize + ", attachName=" + attachName
				+ ", attachStatus=" + attachStatus + ", attachBlob="
				+ Arrays.toString(attachBlob) + ", orderId=" + orderId
				+ ", tradeId=" + tradeId + ", indexFile=" + indexFile
				+ ", createUserId=" + createUserId + ", createUserName="
				+ createUserName + ", createInstId=" + createInstId
				+ ", createInstName=" + createInstName + ", createTime="
				+ createTime + ", updateUserId=" + updateUserId
				+ ", updateUserName=" + updateUserName + ", updateInstId="
				+ updateInstId + ", updateInstName=" + updateInstName
				+ ", updateTime=" + updateTime + "]";
	}
}
