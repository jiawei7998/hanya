package com.singlee.capital.base.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.base.model.TcAttachTreeFile;
import com.singlee.capital.base.model.TcAttachTreeRefVo;
import com.singlee.capital.common.spring.mybatis.MyMapper;

/**
 * 
 * @author liuxy
 *
 */
public interface TcAttachTreeRefVoMapper extends MyMapper<TcAttachTreeRefVo> {
	
	/**
	 * 根据请求条件 获得附件列表 
	 * @param params
	 * @return 
	 */
	List<TcAttachTreeFile> getTcAttachTreeRefVo(Map<String, String> params);
	
}