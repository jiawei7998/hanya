package com.singlee.capital.base.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.model.TcFormFieldset;
import com.singlee.capital.base.service.FormFieldsetService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
/**
 * @projectName 同业业务管理系统
 * @className 表单字段分组映射控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-9 下午8:10:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/FormFieldsetController")
public class FormFieldsetController extends CommonController {
	
	@Autowired
	private FormFieldsetService formFieldsetService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFormFieldsetList")
	public RetMsg<List<TcFormFieldset>> searchFormFieldsetList(@RequestBody Map<String,Object> params){
		List<TcFormFieldset> list = formFieldsetService.getFormFieldsetList(params);
		return RetMsgHelper.ok(list);
	}
}
