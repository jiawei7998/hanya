package com.singlee.capital.base.cal;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.stereotype.Component;

/**
 * 用于日志模块化
 * 
 * @author singlee
 * 
 */
@Component
public class LogManager {
	
	public static final String MODEL_BATCH= "BATCH";
	
	public static final String MODEL_BATCHSCAN= "BATCHSCAN";
	
	public static final String MODEL_BOOKKEEPING= "BOOKKEEPING";
	
	public static final String MODEL_INTERFACEX= "INTERFACEX";
	
	public static final String MODEL_MQ= "MQ";
	/**
	 * 用于临时保存各个模块的Logger(key:模块名，value:Logger)
	 */
	private static Hashtable<String, Logger> loggerHt = new Hashtable<String, Logger>();

	/**
	 * 初始化 PropertyConfigurator的配置文件
	 * 
	 * @param configFile
	 */
	public static void inintPropertyConfigurator() {
		PropertyConfigurator.configure("./config/log4jsinglee.properties");
	}

	/**
	 * 得到这个模块的日志记录Logger
	 * 
	 * @param modelName
	 *            模块名
	 * @return
	 */
	public static Logger getLogger(String modelName) {
		Logger logger = loggerHt.get(modelName);
		if (null == logger) {
			logger = Logger.getLogger(modelName);
			loggerHt.put(modelName, logger);
		}// end if
		return logger;
	}
}
