package com.singlee.capital.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcAttachTreeFileMapper;
import com.singlee.capital.base.model.TcAttachTreeFile;
import com.singlee.capital.base.service.TcAttachTreeFileService;
import com.singlee.capital.base.service.TcAttachTreeRefVoService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;

/**
 * @className 附件树服务
 * @description TODO
 * @author Lyon
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcAttachTreeFileServiceImpl implements TcAttachTreeFileService {
	
	@Resource
	TcAttachTreeFileMapper TcAttachTreeFileMapper;
	@Resource
	TcAttachTreeRefVoService tcAttachTreeRefVoService;

	@Override
	public int insertTcAttachTreeFef(List<TcAttachTreeFile> ref) {
		int flag = TcAttachTreeFileMapper.insertListWithId(ref);
		return flag;
	}

	@Override
	public int updateTcAttachTreeFef(TcAttachTreeFile ref) {
		return TcAttachTreeFileMapper.updateByPrimaryKey(ref);
	}

	@Override
	public int deleteTcAttachTreeFef(TcAttachTreeFile ref) {
		return TcAttachTreeFileMapper.delete(ref);
	}

	@Override
	public TcAttachTreeFile getTcAttachTreeFileById(TcAttachTreeFile ref) {
		return TcAttachTreeFileMapper.selectByPrimaryKey(ref);
	}

	@Override
	public List<TcAttachTreeFile> searchTcAttachTreeFile(Map<String, Object> ref) {
		List<TcAttachTreeFile> list = TcAttachTreeFileMapper.searchTcAttachTreeFile(ref);
		if(list.size() ==0){
			Map<String, String> param = new HashMap<String, String>();
			param.put("refNo", ParameterUtil.getString(ref, "refNo", ""));
			param.put("prdNo", ParameterUtil.getString(ref, "prdNo", ""));
			list = tcAttachTreeRefVoService.getTcAttachTreeRefVoByPrd(param);
		}
		list = TreeUtil.mergeChildrenList(list, "-");
		return list;
	}

	@Override
	public int insertTcAttachTreeFilebatch(List<TcAttachTreeFile> list) {
		return TcAttachTreeFileMapper.insertTcAttachTreeFilebatch(list);
	}

	@Override
	public int deleteTcAttachTreeFileByDealNo(Map<String, Object> ref) {
		return TcAttachTreeFileMapper.deleteTcAttachTreeFileByDealNo(ref);
	}
	
}
