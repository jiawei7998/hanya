package com.singlee.capital.base.cal.tools;

import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.cashflow.model.TdCashflowFee;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class PlaningTools {

	/**
	 (Frequency.ONCE.getValue()==4)//利随本清
	 (Frequency.MONTH.getValue()==2)//月
	 (Frequency.YEAR.getValue()==1)//年
	 (Frequency.HALFYEAR.getValue()==3)//半年
	 (Frequency.QUARTER.getValue()==6)//季度
	 (Frequency.DAY.getValue()==7)//天
	 * @param interestRanges 		利率区间(变化-调整)  A=<X<B
	 * @param principalIntervals    本金区间（变化-还本） A=<X<B
	 * @param detailSchedules       计算明细(计提每一天)
	 * @param CashflowInterests      收息计划（区间式）
	 * @param vDate  交易起息日(存在交易起息日非计提起息日时，需要传入真实的起息日期）
	 * @param mDate  交易到期日
	 * @param frequency   频率
	 * @param dayCountBasis  计息基础
	 * @throws ParseException
	 */
	public static void calPlanSchedule(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> CashflowDailyInterests,List<CashflowInterest> CashflowInterests,String vDate,String mDate,String actualVDate,double amt,double intRate,Frequency frequency,DayCountBasis dayCountBasis) throws ParseException {
		CashflowInterest  planSchedule = null;int monthSpace = 0;String endDateString = "";int count = 0;
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("年："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};

				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 4:
				System.out.println("一次性");
				planSchedule = new CashflowInterest();
				planSchedule.setSeqNumber(CashflowInterests.size()+1);
				planSchedule.setRefBeginDate(vDate);
				planSchedule.setRefEndDate(mDate);
				planSchedule.setTheoryPaymentDate(mDate);
				planSchedule.setDayCounter(dayCountBasis.getMolecule());

				planSchedule.setExecuteRate(div(intRate, 1,6));
				planSchedule.setInterestAmt(
						fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
				);
				CashflowInterests.add(planSchedule);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.DAY,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				break;
			default:
				break;
		}
	}
	/**
	 * @param args
	 * @throws ParseException
	 */
	/*public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		String vDate = "2016-12-21";
		String mDate = "2027-02-22";
		double  amt = 200000.00f;
		double  intRate = 0.06f;
		List<PlanSchedule> pList = new ArrayList<PlanSchedule>();//返回的收息计划
		List<DetailSchedule> detailSchedules = new ArrayList<DetailSchedule>();//返回的每日计提
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		PrincipalInterval principalInterval = new PrincipalInterval();
		principalInterval.setStartDate(vDate);principalInterval.setEndDate("2017-1-21");principalInterval.setResidual_Principal(200000.00f);
		principalIntervals.add(principalInterval);
		PrincipalInterval principalInterval2 = new PrincipalInterval();
		principalInterval2.setStartDate("2017-1-22");principalInterval2.setEndDate(mDate);principalInterval2.setResidual_Principal(100000.00f);
		principalIntervals.add(principalInterval2);
		
		*//**
	 *  [2016/12/21,2017/01/27]  6
	 [2017/01/28,2017/02/21]  5
	 *//*
		InterestRange interestRange = new InterestRange();
		interestRange.setStartDate(vDate);interestRange.setEndDate("2017-01-27");interestRange.setRate(6.00f/100);
		interestRanges.add(interestRange);
		InterestRange interestRange2 = new InterestRange();
		interestRange2.setStartDate("2017-01-28");interestRange2.setEndDate(mDate);interestRange2.setRate(5.00f/100);
		interestRanges.add(interestRange2);
		
		calPlanSchedule(interestRanges,CashflowCapitals,CashflowDailyInterests,pList,vDate,mDate,vDate,amt,intRate,Frequency.valueOf(0),DayCountBasis.valueOf(1));
		for(DetailSchedule detailSchedule : detailSchedules)
			
			System.out.println(detailSchedule.toString());
		
		for(PlanSchedule planSchedule : pList)
			
			System.out.println(planSchedule.toString());
	}*/
	/**
	 * 获得区间的剩余本金
	 * @param principalIntervals
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static double getPrincipalIntervalByDate(List<PrincipalInterval> principalIntervals,String date) throws ParseException{

		for(PrincipalInterval principalInterval:principalIntervals){

			if(compareDate(principalInterval.getEndDate(),date,"yyyy-MM-dd") && compareDate(date,principalInterval.getStartDate(),"yyyy-MM-dd")){
				return principalInterval.getResidual_Principal();
			}
		}
		return 0;
	}


	/**
	 * 获得区间的利率
	 * @param principalIntervals
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static double getInterestRangeByDate(List<InterestRange> interestRanges,String date) throws ParseException{

		for(InterestRange interestRange:interestRanges){

			if(compareDate(interestRange.getEndDate(),date,"yyyy-MM-dd") && compareDate(date,interestRange.getStartDate(),"yyyy-MM-dd")){
				return interestRange.getRate();
			}
		}
		return 0;
	}
	/**
	 * 区间本金 块 求利息总和
	 * @param principalIntervals
	 * @param date
	 * @param vDate
	 * @param dayCountBasis
	 * @param i_Rate
	 * @return
	 * @throws ParseException
	 *//*
	public static double sumPrincipalIntervalBetweenDate(List<PrincipalInterval> principalIntervals,String date,String vDate,DayCountBasis dayCountBasis,List<InterestRange> interestRanges) throws ParseException{
		double resultAmt = 0.00f;
		for(PrincipalInterval principalInterval:principalIntervals){
			*//**
	 * 2016-12-21~2017-01-19
	 * 2017-01-20~2017-02-22
	 * addOrSubMonth_Day(principalInterval.getEndDate(),Frequency.DAY,1) 不包含尾巴 让1-19时不落入第二个区间
	 *//*
			if(compareDate(date,addOrSubMonth_Day(principalInterval.getEndDate(),Frequency.DAY,1),"yyyy-MM-dd")){
				resultAmt =add(div(fun_Calculation_interval_Range(interestRanges,principalIntervals,principalInterval.getStartDate(),addOrSubMonth_Day(principalInterval.getEndDate(),Frequency.DAY,1),vDate,dayCountBasis),1,2)
						, resultAmt);
			}
			if(compareDate(date,principalInterval.getStartDate(),"yyyy-MM-dd")&&compareDate(principalInterval.getEndDate(),date,"yyyy-MM-dd")){
				resultAmt =add(div(fun_Calculation_interval_Range(interestRanges,principalIntervals,principalInterval.getStartDate(),date,vDate,dayCountBasis),1,2)
				, resultAmt);
				
			}
		}
		return resultAmt;
	}*/
	/**
	 *
	 v_Cal1   := To_Number(((i_Pdate + 1) - (v_Vdate)) * i_Amt * i_Rate / 100 /
	 i_Basis);
	 v_Cal2   := To_Number(((i_Pdate + 1 - 1) - (v_Vdate)) * i_Amt * i_Rate / 100 /
	 i_Basis);
	 v_Result := Round(v_Cal1, i_Round) - Round(v_Cal2, i_Round);
	 * @param v_Vdate  业务实际起息日  最小的日期
	 * @param i_BPdate  计算计提利息的截止日 最大日期
	 * @param i_EPdate计算计提利息的起始日 第二小的日期
	 * @param i_Amt  计算区间的本金（计提利息本金）
	 * @param i_Rate 计算区间的利率 非百分比
	 * flag = true 说明设置了第一次付息日  否则 没有
	 * @return
	 * @throws ParseException
	 */
	public static double fun_Calculation_interval(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> CashflowDailyInterests,String i_BPdate,String i_EPdate,String v_Vdate,DayCountBasis dayCountBasis) throws ParseException
	{
		int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
		int count = 0; double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
		CashflowDailyInterest detailSchedule = null;
		boolean prepaymentFlag = false;//区间中是否涉及本金为0的情况,如果取到本金为0 说明做过提前还本 后面的本金都为0
		while(count-step<-1){
			detailSchedule = new CashflowDailyInterest();
			detailSchedule.setSeqNumber(CashflowDailyInterests.size()+1);
			detailSchedule.setDayCounter(dayCountBasis.getMolecule());

			/**
			 * 每日计算(计提) 后一日-前一日  再四舍五入
			 */
			i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			if(i_Amt == 0 && prepaymentFlag == false){
				prepaymentFlag = true;
			}

			i_Rate = getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			double dayCalAmt = new BigDecimal(sub(div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+2))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()),div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()))).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue();

			detailSchedule.setInterest(dayCalAmt);
			detailSchedule.setPrincipal(i_Amt);
			detailSchedule.setActualRate(div(i_Rate,1,6));
			detailSchedule.setRefBeginDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			detailSchedule.setRefEndDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			CashflowDailyInterests.add(detailSchedule);
			/**
			 * 累加
			 */
			result = add(result, dayCalAmt);
			count ++;
		}
		/**
		 * 区间的最后一天计算规则为 =（区间总利息-之前的每日计提之和）
		 * 由于最后一期的最后一条收息交易需要总收息金额-已计提的数额
		 */
		double dayCalAmt = 0.00f;
		if(prepaymentFlag == false)
		{
			double lastCalAmt = 0.00;
			for(CashflowDailyInterest detailSchedule2: CashflowDailyInterests)
			{
				if(compareDate(detailSchedule2.getRefBeginDate(),v_Vdate, "yyyy-MM-dd")) {
					lastCalAmt = add(lastCalAmt, detailSchedule2.getInterest());}//累计之前的计提数额 因为每一段的区间收息存在（先四舍五入再加总和先加总再四舍五入的差别）
			}

			i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));

			dayCalAmt = sub(div(fun_Calculation_interval_Range(interestRanges,principalIntervals,v_Vdate,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1),v_Vdate,dayCountBasis),1,2), lastCalAmt);
		}

		detailSchedule = new CashflowDailyInterest();
		detailSchedule.setSeqNumber(CashflowDailyInterests.size()+1);
		detailSchedule.setDayCounter(dayCountBasis.getMolecule());
		detailSchedule.setInterest(dayCalAmt);
		detailSchedule.setPrincipal(i_Amt);
		detailSchedule.setActualRate(div(getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count)),1,6));
		detailSchedule.setRefBeginDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
		detailSchedule.setRefEndDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
		CashflowDailyInterests.add(detailSchedule);
		result = add(result, dayCalAmt);
		return result;
	}


	public static double fun_Calculation_interval_Range(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,String i_BPdate,String i_EPdate,String v_Vdate,DayCountBasis dayCountBasis) throws ParseException
	{
		int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
		int count = 0; double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
		while(count-step<0){
			/**
			 * 每日计算(计提) 后一日-前一日  再四舍五入
			 */
			i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			i_Rate = getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			double dayCalAmt = new BigDecimal(sub(div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+2))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()),div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()))).setScale(12,java.math.BigDecimal.ROUND_HALF_UP).doubleValue();
			/**
			 * 累加
			 */
			result = add(result, dayCalAmt);
			count ++;
		}
		return result;
	}


	public static double fun_Calculation_interval_Range2(List<InterestRange> interestRanges,String i_BPdate,String i_EPdate,String v_Vdate,DayCountBasis dayCountBasis,double faceAmt) throws ParseException
	{
		int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
		int count = 0; double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
		while(count-step<0){
			/**
			 * 每日计算(计提) 后一日-前一日  再四舍五入
			 */
			i_Amt = faceAmt;
			i_Rate = getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			double dayCalAmt = new BigDecimal(sub(div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+2))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()),div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()))).setScale(12,java.math.BigDecimal.ROUND_HALF_UP).doubleValue();
			/**
			 * 累加
			 */
			result = add(result, dayCalAmt);
			count ++;
		}
		return result;
	}

	private static boolean isSameDate(String date1, String date2,String pattern) throws ParseException {

		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar cal1=Calendar.getInstance();
		Calendar cal2=Calendar.getInstance();

		cal1.setTime(sdf.parse(date1));
		cal2.setTime(sdf.parse(date2));

		boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
				.get(Calendar.YEAR);
		boolean isSameMonth = isSameYear
				&& cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
		boolean isSameDate = isSameMonth
				&& cal1.get(Calendar.DAY_OF_MONTH) == cal2
				.get(Calendar.DAY_OF_MONTH);

		return isSameDate;
	}
	/*** 
	 * 日期月份减一个月
	 *
	 * @param datetime
	 *            日期(2014-11)
	 * @return 2014-10
	 */
	public static String addOrSubMonth_Day(String datetime,Frequency frequency,int count) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(datetime);
		} catch (ParseException e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(frequency.getCalendarType(),frequency.getPace()*count);
		date = cl.getTime();
		return sdf.format(date);
	}

	/**
	 *字符串的日期格式的计算
	 */
	public static int daysBetween(String smdate,String bdate) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(smdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(bdate));
		long time2 = cal.getTimeInMillis();
		long between_days=(time2-time1)/(1000*3600*24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 比较两个日期之间的大小
	 *
	 * @param d1
	 * @param d2
	 * @return 前者大于等于后者返回true 反之false
	 */
	public static boolean compareDate(String date1,String date2,String pattern)throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar c1=Calendar.getInstance();
		Calendar c2=Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int result = c1.compareTo(c2);
		if (result >= 0) {
			return true; }
		else {
			return false;}
	}

	/**
	 * 比较两个日期之间的大小
	 *
	 * @param d1
	 * @param d2
	 * @return 前者大于后者返回true 反之false
	 */
	public static boolean compareDate2(String date1,String date2,String pattern)throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar c1=Calendar.getInstance();
		Calendar c2=Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int result = c1.compareTo(c2);
		if (result > 0) {
			return true;}
		else {
			return false;}
	}

	public static boolean compareDate3(String date1,String date2,String pattern)throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar c1=Calendar.getInstance();
		Calendar c2=Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int result = c1.compareTo(c2);
		if (result == 0) {
			return true; }
		else {
			return false;}
	}

	public static boolean compareDateEqual(String date1,String date2,String pattern)throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar c1=Calendar.getInstance();
		Calendar c2=Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int result = c1.compareTo(c2);
		if (result == 0) {
			return true;}
		else {
			return false;}
	}

	/**
	 *
	 * 计算两个日期相差的月份数
	 *
	 * @param date1 日期1
	 * @param date2 日期2
	 * @param pattern  日期1和日期2的日期格式
	 * @return  相差的月份数
	 * @throws ParseException
	 */
	public static int countMonths(String date1,String date2,String pattern) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);

		Calendar c1=Calendar.getInstance();
		Calendar c2=Calendar.getInstance();

		c1.setTime(sdf.parse(date1));
		c2.setTime(sdf.parse(date2));

		int year =c2.get(Calendar.YEAR)-c1.get(Calendar.YEAR);

		//开始日期若小月结束日期
		if(year<=0){
			year=-year;
			return year*12+c1.get(Calendar.MONTH)-c2.get(Calendar.MONTH);
		}

		return year*12+c2.get(Calendar.MONTH)-c1.get(Calendar.MONTH);
	}

	/**
	 * 由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精
	 * 确的浮点数运算，包括加减乘除和四舍五入。
	 */
	//默认除法运算精度
	private static final int DEF_DIV_SCALE = 10;

	/**
	 * 提供精确的加法运算。
	 * @param v1 被加数
	 * @param v2 加数
	 * @return 两个参数的和
	 */
	public static double add(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.add(b2).doubleValue();
	}

	/**
	 * 提供精确的减法运算。
	 * @param v1 被减数
	 * @param v2 减数
	 * @return 两个参数的差
	 */
	public static double sub(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.subtract(b2).doubleValue();
	}

	/**
	 * 提供精确的乘法运算。
	 * @param v1 被乘数
	 * @param v2 乘数
	 * @return 两个参数的积
	 */
	public static double mul(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 提供（相对）精确的除法运算，当发生除不尽的情况时，精确到
	 * 小数点以后10位，以后的数字四舍五入。
	 * @param v1 被除数
	 * @param v2 除数
	 * @return 两个参数的商
	 */
	public static double div(double v1,double v2){
		return div(v1,v2,DEF_DIV_SCALE);
	}

	/**
	 * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
	 * 定精度，以后的数字四舍五入。
	 * @param v1 被除数
	 * @param v2 除数
	 * @param scale 表示表示需要精确到小数点以后几位。
	 * @return 两个参数的商
	 */
	public static double div(double v1,double v2,int scale){
		if(scale<0){
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.divide(b2,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 提供精确的小数位四舍五入处理。
	 * @param v 需要四舍五入的数字
	 * @param scale 小数点后保留几位
	 * @return 四舍五入后的结果
	 */
	public static double round(double v,int scale){
		if(scale<0){
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return b.divide(one,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
	}



	public static void calPrincipalSchedule(List<CashflowCapital> CashflowCapitals,String vDate,String mDate,double amt,Frequency frequency,DayCountBasis dayCountBasis) throws ParseException {
		CashflowCapital  CashflowCapital= null;int monthSpace = 0;int count = 0;String endDateString = "";
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("年："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};

				endDateString = (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(amt);
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 4:
				System.out.println("一次性");

				CashflowCapital = new CashflowCapital();
				CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
				CashflowCapital.setRepaymentSdate(mDate);
				CashflowCapital.setRepaymentSamt(amt);
				CashflowCapitals.add(CashflowCapital);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString = (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(amt);
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString =  (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(amt);
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString =  (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(amt);
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				break;
			default:
				break;
		}
	}

	public static void calPrincipalScheduleHrb(List<CashflowCapital> CashflowCapitals,String vDate,String mDate,double camt,Frequency frequency,DayCountBasis dayCountBasis) throws ParseException {
		CashflowCapital  CashflowCapital= null;int monthSpace = 0;int count = 0;String endDateString = "";
		BigDecimal invAmt=new BigDecimal(camt);
		BigDecimal amt=new BigDecimal("0.00");
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				amt=invAmt.divide(BigDecimal.valueOf(monthSpace+1),2, BigDecimal.ROUND_HALF_UP);//平均每次要归还的金额
				System.out.println("年："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSamt(amt.doubleValue());
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					CashflowCapital.setRepaymentSamt(0f);
					count++;
					CashflowCapitals.add(CashflowCapital);
				};

				endDateString = (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(invAmt.subtract(amt.multiply(BigDecimal.valueOf(count))).doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 4:
				System.out.println("一次性");
				CashflowCapital = new CashflowCapital();
				CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
				CashflowCapital.setRepaymentSdate(mDate);
				CashflowCapital.setRepaymentSamt(invAmt.doubleValue());
				CashflowCapitals.add(CashflowCapital);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				amt=invAmt.divide(BigDecimal.valueOf(monthSpace+1),2, BigDecimal.ROUND_HALF_UP);//平均每次要归还的金额
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					CashflowCapital.setRepaymentSamt(amt.doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString = (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(invAmt.subtract(amt.multiply(BigDecimal.valueOf(count))).doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				amt=invAmt.divide(BigDecimal.valueOf(monthSpace+1),2, BigDecimal.ROUND_HALF_UP);//平均每次要归还的金额
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					CashflowCapital.setRepaymentSamt(amt.doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString =  (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(invAmt.subtract(amt.multiply(BigDecimal.valueOf(count))).doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				amt=invAmt.divide(BigDecimal.valueOf(monthSpace+1),2, BigDecimal.ROUND_HALF_UP);//平均每次要归还的金额
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					CashflowCapital.setRepaymentSamt(amt.doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				endDateString =  (CashflowCapital==null||CashflowCapital.getRepaymentSdate()==null)?vDate:CashflowCapital.getRepaymentSdate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(mDate);
					CashflowCapital.setRepaymentSamt(invAmt.subtract(amt.multiply(BigDecimal.valueOf(count))).doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				amt=invAmt.divide(BigDecimal.valueOf(monthSpace),2, BigDecimal.ROUND_HALF_UP);//平均每次要归还的金额
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					CashflowCapital = new CashflowCapital();
					CashflowCapital.setSeqNumber(CashflowCapitals.size()+1);
					CashflowCapital.setRepaymentSdate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					CashflowCapital.setRepaymentSamt(amt.doubleValue());
					count++;
					CashflowCapitals.add(CashflowCapital);
				};
				break;
			default:
				break;
		}
	}


	/*****************青岛银行********************/

	/**
	 (Frequency.ONCE.getValue()==4)//利随本清
	 (Frequency.MONTH.getValue()==2)//月
	 (Frequency.YEAR.getValue()==1)//年
	 (Frequency.HALFYEAR.getValue()==3)//半年
	 (Frequency.QUARTER.getValue()==6)//季度
	 (Frequency.DAY.getValue()==7)//天
	 * @param interestRanges 		利率区间(变化-调整)  A=<X<B
	 * @param principalIntervals    本金区间（变化-还本） A=<X<B
	 * @param detailSchedules       计算明细(计提每一天)
	 * @param CashflowInterests      收息计划（区间式）
	 * @param vDate  交易起息日(存在交易起息日非计提起息日时，需要传入真实的起息日期）
	 * @param mDate  交易到期日
	 * @param frequency   频率
	 * @param dayCountBasis  计息基础
	 * @throws ParseException
	 */
	public static void calPlanSchedule(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> CashflowDailyInterests,List<CashflowInterest> CashflowInterests,String vDate,String mDate,String actualVDate,double amt,double intRate,Frequency frequency,DayCountBasis dayCountBasis,int nDays) throws ParseException {
		CashflowInterest  planSchedule = null;int monthSpace = 0;String endDateString = "";int count = 0;
		String tempEndDate = null;
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("年："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};

				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 4:
				System.out.println("一次性");
				planSchedule = new CashflowInterest();
				planSchedule.setSeqNumber(CashflowInterests.size()+1);
				planSchedule.setRefBeginDate(vDate);
				planSchedule.setRefEndDate(mDate);
				planSchedule.setTheoryPaymentDate(mDate);
				planSchedule.setDayCounter(dayCountBasis.getMolecule());

				planSchedule.setExecuteRate(div(intRate, 1,6));
				planSchedule.setInterestAmt(
						fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
				);
				CashflowInterests.add(planSchedule);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				break;
			default:
				break;
		}
	}

	/**
	 (Frequency.ONCE.getValue()==4)//利随本清
	 (Frequency.MONTH.getValue()==2)//月
	 (Frequency.YEAR.getValue()==1)//年
	 (Frequency.HALFYEAR.getValue()==3)//半年
	 (Frequency.QUARTER.getValue()==6)//季度
	 (Frequency.DAY.getValue()==7)//天
	 * @param interestRanges 		利率区间(变化-调整)  A=<X<B
	 * @param principalIntervals    本金区间（变化-还本） A=<X<B
	 * @param detailSchedules       计算明细(计提每一天)
	 * @param CashflowInterests      收息计划（区间式）
	 * @param vDate  交易起息日(存在交易起息日非计提起息日时，需要传入真实的起息日期）
	 * @param mDate  交易到期日
	 * @param frequency   频率
	 * @param dayCountBasis  计息基础
	 * @throws ParseException
	 */
	public static void calPlanScheduleExceptFess(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> CashflowDailyInterests,List<CashflowInterest> CashflowInterests,String vDate,String mDate,String actualVDate,double amt,double intRate,Frequency frequency,DayCountBasis dayCountBasis,int nDays,List<TdFeesPassAgewayDaily> fessList) throws ParseException {
		CashflowInterest  planSchedule = null;int monthSpace = 0;String endDateString = "";int count = 0;
		String tempEndDate = null;
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("年："+monthSpace);

				while(count-monthSpace<0){
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());
					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				}

				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());
					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 4:
				System.out.println("一次性");
				planSchedule = new CashflowInterest();
				planSchedule.setSeqNumber(CashflowInterests.size()+1);
				planSchedule.setRefBeginDate(vDate);
				planSchedule.setRefEndDate(mDate);
				planSchedule.setTheoryPaymentDate(mDate);
				planSchedule.setDayCounter(dayCountBasis.getMolecule());

				planSchedule.setExecuteRate(div(intRate, 1,6));
				planSchedule.setActuralRate(div(intRate, 1,6));

				fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

				CashflowInterests.add(planSchedule);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);

				while(count-monthSpace<0){
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}


					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);


					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setActuralRate(div(intRate, 1,6));

					fun_Calculation_interval_except_fess(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule,actualVDate,dayCountBasis,fessList);

					count++;
					CashflowInterests.add(planSchedule);
				};
				break;
			default:
				break;
		}
	}


	public static void calPlanScheduleForFee(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> CashflowDailyInterests,List<TdCashflowFee> CashflowInterests,String vDate,String mDate,String actualVDate,double amt,double intRate,Frequency frequency,DayCountBasis dayCountBasis) throws ParseException {
		TdCashflowFee  planSchedule = null;int monthSpace = 0;String endDateString = "";int count = 0;
		switch (frequency.getValue()) {
			case 1:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("年："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.YEAR,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};

				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 4:
				System.out.println("一次性");
				planSchedule = new TdCashflowFee();
				planSchedule.setSeqNumber(CashflowInterests.size()+1);
				planSchedule.setRefBeginDate(vDate);
				planSchedule.setRefEndDate(mDate);
				planSchedule.setTheoryPaymentDate(mDate);
				planSchedule.setDayCounter(dayCountBasis.getMolecule());

				planSchedule.setExecuteRate(div(intRate, 1,6));
				planSchedule.setInterestAmt(
						fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
				);
				CashflowInterests.add(planSchedule);
				break;
			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.MONTH,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				}
				break;
			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0){

					planSchedule = new TdCashflowFee();
					planSchedule.setSeqNumber(CashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day(vDate,Frequency.DAY,count));
					planSchedule.setRefEndDate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day(vDate,Frequency.DAY,count+1));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					planSchedule.setInterestAmt(
							fun_Calculation_interval(interestRanges,principalIntervals,CashflowDailyInterests,planSchedule.getRefBeginDate(),planSchedule.getRefEndDate(),actualVDate,dayCountBasis)
					);
					count++;
					CashflowInterests.add(planSchedule);
				};
				break;
			default:
				break;
		}
	}

	/**
	 *
	 v_Cal1   := To_Number(((i_Pdate + 1) - (v_Vdate)) * i_Amt * i_Rate / 100 /
	 i_Basis);
	 v_Cal2   := To_Number(((i_Pdate + 1 - 1) - (v_Vdate)) * i_Amt * i_Rate / 100 /
	 i_Basis);
	 v_Result := Round(v_Cal1, i_Round) - Round(v_Cal2, i_Round);
	 * @param v_Vdate  业务实际起息日  最小的日期
	 * @param i_BPdate  计算计提利息的截止日 最大日期
	 * @param i_EPdate计算计提利息的起始日 第二小的日期
	 * @param i_Amt  计算区间的本金（计提利息本金）
	 * @param i_Rate 计算区间的利率 非百分比
	 * flag = true 说明设置了第一次付息日  否则 没有
	 * @return
	 * @throws ParseException
	 */
	public static void fun_Calculation_interval_except_fess(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> cashflowDailyInterests,CashflowInterest cashflowInterest,String v_Vdate,DayCountBasis dayCountBasis,List<TdFeesPassAgewayDaily> fessList) throws ParseException
	{
		String i_BPdate = cashflowInterest.getRefBeginDate();
		String i_EPdate = cashflowInterest.getRefEndDate();
		int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
		int count = 0; double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
		CashflowDailyInterest detailSchedule = null;
		boolean prepaymentFlag = false;//区间中是否涉及本金为0的情况,如果取到本金为0 说明做过提前还本 后面的本金都为0
		while(count-step < -1)
		{
			detailSchedule = new CashflowDailyInterest();
			detailSchedule.setSeqNumber(cashflowDailyInterests.size()+1);
			detailSchedule.setDayCounter(dayCountBasis.getMolecule());

			/**
			 * 每日计算(计提) 后一日-前一日  再四舍五入
			 */
			i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			if(i_Amt == 0 && prepaymentFlag == false){
				prepaymentFlag = true;
			}

			i_Rate = getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			double dayCalAmt = new BigDecimal(sub(div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+2))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()),div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()))).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue();

			detailSchedule.setInterest(dayCalAmt);
			detailSchedule.setPrincipal(i_Amt);
			detailSchedule.setActualRate(div(i_Rate,1,6));
			detailSchedule.setRefBeginDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			detailSchedule.setRefEndDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
			detailSchedule.setDealNo(cashflowInterest.getDealNo());
			detailSchedule.setVersion(cashflowInterest.getVersion());
			cashflowDailyInterests.add(detailSchedule);
			/**
			 * 累加
			 */
			result = add(result, dayCalAmt);
			count ++;
		}
		/**
		 * 区间的最后一天计算规则为 =（区间总利息-之前的每日计提之和）
		 * 由于最后一期的最后一条收息交易需要总收息金额-已计提的数额
		 */
		double dayCalAmt = 0.00f;
		if(prepaymentFlag == false)
		{
			double lastCalAmt = 0.00;
			for(CashflowDailyInterest detailSchedule2: cashflowDailyInterests)
			{
				if(compareDate(detailSchedule2.getRefBeginDate(),v_Vdate, "yyyy-MM-dd")) {
					lastCalAmt = add(lastCalAmt, detailSchedule2.getInterest());}//累计之前的计提数额 因为每一段的区间收息存在（先四舍五入再加总和先加总再四舍五入的差别）
			}

			i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));

			dayCalAmt = sub(
					div(
							fun_Calculation_interval_Range(interestRanges,principalIntervals,v_Vdate,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1),v_Vdate,dayCountBasis)
							,1,2
					), lastCalAmt);
		}

		detailSchedule = new CashflowDailyInterest();
		detailSchedule.setSeqNumber(cashflowDailyInterests.size()+1);
		detailSchedule.setDayCounter(dayCountBasis.getMolecule());
		detailSchedule.setInterest(dayCalAmt);
		detailSchedule.setPrincipal(i_Amt);
		detailSchedule.setActualRate(div(getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count)),1,6));
		detailSchedule.setRefBeginDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
		detailSchedule.setRefEndDate(addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
		detailSchedule.setDealNo(cashflowInterest.getDealNo());
		detailSchedule.setVersion(cashflowInterest.getVersion());
		cashflowDailyInterests.add(detailSchedule);
		result = add(result, dayCalAmt);

		cashflowInterest.setExecuteRate(getInterestRangeByDate(interestRanges,addOrSubMonth_Day(cashflowInterest.getRefEndDate(),Frequency.DAY,-1)));
		//减去通道计提
		if(fessList.size() > 0 && step > 0)
		{
			BigDecimal temp = new BigDecimal(0);
			for (TdFeesPassAgewayDaily e : fessList)
			{
				if(e.getRefBeginDate().compareTo(i_BPdate) >= 0 && e.getRefBeginDate().compareTo(i_EPdate) < 0)
				{
					temp = temp.add(new BigDecimal(e.getInterest()));
				}
			}
			result = sub(result,temp.doubleValue());
			//计算参考利率 RATE = 利息 * basis / (本金*期限)
			i_Amt = getPrincipalIntervalByDate(principalIntervals,i_BPdate);
			if(i_Amt > 0 && step > 0)
			{
				i_Rate = div(mul(result, dayCountBasis.getDenominator()) , mul(i_Amt,step), 6);
				cashflowInterest.setActuralRate(i_Rate);
			}else{
				cashflowInterest.setActuralRate(cashflowInterest.getExecuteRate());
			}

		}else{
			cashflowInterest.setActuralRate(cashflowInterest.getExecuteRate());
		}
		cashflowInterest.setInterestAmt(result);
	}

	/****
	 * 根据已经计算好的每日计提利息 减去 通道计提利息之和 计算收息计划
	 * 用于通道变更后需要重新计算收息计划的情景
	 * @param interestRanges
	 * @param principalIntervals
	 * @param cashflowDailyInterests
	 * @param cashflowInterest
	 * @param v_Vdate
	 * @param dayCountBasis
	 * @param fessList
	 * @throws ParseException
	 */
	public static void fun_reset_intPlan_except_fess(List<PrincipalInterval> principalIntervals,List<CashflowDailyInterest> cashflowDailyInterests,CashflowInterest cashflowInterest,String v_Vdate,DayCountBasis dayCountBasis,List<TdFeesPassAgewayDaily> fessList) throws ParseException
	{
		String i_BPdate = cashflowInterest.getRefBeginDate();
		String i_EPdate = cashflowInterest.getRefEndDate();
		int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
		double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
		for (CashflowDailyInterest dailyInterest : cashflowDailyInterests)
		{
			if(dailyInterest.getRefBeginDate().compareTo(i_BPdate) >= 0 && dailyInterest.getRefBeginDate().compareTo(i_EPdate) < 0)
			{
				result = add(result, dailyInterest.getInterest());
			}
		}

		//减去通道计提
		if(fessList.size() > 0)
		{
			BigDecimal temp = new BigDecimal(0);
			for (TdFeesPassAgewayDaily e : fessList)
			{
				if(e.getRefBeginDate().compareTo(i_BPdate) >= 0 && e.getRefBeginDate().compareTo(i_EPdate) < 0)
				{
					temp = temp.add(new BigDecimal(e.getInterest()));
				}
			}
			result = sub(result,temp.doubleValue());
			//计算参考利率 RATE = 利息 * basis / (本金*期限)
			i_Amt = getPrincipalIntervalByDate(principalIntervals,i_BPdate);
			if(i_Amt > 0 && step > 0){
				i_Rate = div(mul(result, dayCountBasis.getDenominator()) , mul(i_Amt,step), 6);
				cashflowInterest.setActuralRate(i_Rate);
			}else{
				cashflowInterest.setActuralRate(cashflowInterest.getExecuteRate());
			}
		}else{
			cashflowInterest.setActuralRate(cashflowInterest.getExecuteRate());
		}
		cashflowInterest.setInterestAmt(result);
	}

	public static CashflowInterest fun_Calculation_interval_Range_except_fee(List<InterestRange> interestRanges,List<PrincipalInterval> principalIntervals,String i_BPdate,String i_EPdate,String v_Vdate,DayCountBasis dayCountBasis,List<TdFeesPassAgewayDaily> fessList) throws ParseException
	{
		CashflowInterest cash = new CashflowInterest();
		try {
			int step = daysBetween(i_BPdate,i_EPdate)/(Frequency.DAY.getPace());
			int count = 0; double result = 0.00;double i_Amt = 0.00f; double i_Rate = 0.00f;
			while(count-step<0){
				/**
				 * 每日计算(计提) 后一日-前一日  再四舍五入
				 */
				i_Amt = getPrincipalIntervalByDate(principalIntervals,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
				i_Rate = getInterestRangeByDate(interestRanges,addOrSubMonth_Day(i_BPdate,Frequency.DAY,count));
				double dayCalAmt = new BigDecimal(sub(div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+2))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()),div(mul(new BigDecimal(daysBetween(v_Vdate, addOrSubMonth_Day(i_BPdate,Frequency.DAY,count+1))).doubleValue(), mul(i_Amt,i_Rate)),dayCountBasis.getDenominator()))).setScale(12,java.math.BigDecimal.ROUND_HALF_UP).doubleValue();
				/**
				 * 累加
				 */
				result = add(result, dayCalAmt);
				count ++;
			}


			if(fessList.size() > 0)
			{
				BigDecimal temp = new BigDecimal(0);
				for (TdFeesPassAgewayDaily e : fessList)
				{
					if(e.getRefBeginDate().compareTo(i_BPdate) >= 0 && e.getRefBeginDate().compareTo(i_EPdate) < 0)
					{
						temp = temp.add(new BigDecimal(e.getInterest()));
					}
				}
				result = sub(result,temp.doubleValue());
				//计算参考利率 RATE = 利息 * basis / (本金*期限)
				i_Amt = getPrincipalIntervalByDate(principalIntervals,i_BPdate);
				if(i_Amt > 0 && step > 0)
				{
					i_Rate = div(mul(result, dayCountBasis.getDenominator()) , mul(i_Amt,step), 6);
				}
			}
			cash.setInterestAmt(result);
			cash.setActuralRate(i_Rate);

		} catch (Exception e) {
			cash.setInterestAmt(0);
			e.printStackTrace();
		}
		return cash;
	}

	/*** 
	 * 日期月份减一个月
	 *
	 * @param datetime
	 *            日期(2014-11)
	 * @return 2014-10
	 */
	public static String addOrSubMonth_Day_N(String datetime,Frequency frequency,int count,int nDays) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(datetime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		//频率+nDays作为收息频率
		for (int i = 0; i < count; i++)
		{
			cl.add(frequency.getCalendarType(),frequency.getPace());
			cl.add(GregorianCalendar.DATE,nDays);
		}
		date = cl.getTime();
		return sdf.format(date);
	}

	/**
	 * 计算收息区间
	 (Frequency.ONCE.getValue()==4)//利随本清
	 (Frequency.MONTH.getValue()==2)//月
	 (Frequency.YEAR.getValue()==1)//年
	 (Frequency.HALFYEAR.getValue()==3)//半年
	 (Frequency.QUARTER.getValue()==6)//季度
	 (Frequency.DAY.getValue()==7)//天
	 * @param interestRanges 		利率区间(变化-调整)  A=<X<B
	 * @param principalIntervals    本金区间（变化-还本） A=<X<B
	 * @param detailSchedules       计算明细(计提每一天)
	 * @param CashflowInterests      收息计划（区间式）
	 * @param vDate  交易起息日(存在交易起息日非计提起息日时，需要传入真实的起息日期）
	 * @param mDate  交易到期日
	 * @param frequency   频率
	 * @param dayCountBasis  计息基础
	 * @throws ParseException
	 */
	public static void calPlanScheduleRange(List<CashflowInterest> cashflowInterests,String vDate,String mDate,String actualVDate,double intRate,Frequency frequency,DayCountBasis dayCountBasis,int nDays) throws ParseException {
		CashflowInterest  planSchedule = null;
		int monthSpace = 0;
		String endDateString = "";
		int count = 0;
		String tempEndDate = null;
		switch (frequency.getValue())
		{
			case 1:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.YEAR.getPace()*12);
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.YEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}

				while(count-monthSpace<0)
				{
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.YEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());
					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}

				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				break;

			case 4:
				planSchedule = new CashflowInterest();
				planSchedule.setSeqNumber(cashflowInterests.size()+1);
				planSchedule.setRefBeginDate(vDate);
				planSchedule.setRefEndDate(mDate);
				planSchedule.setTheoryPaymentDate(mDate);
				planSchedule.setDayCounter(dayCountBasis.getMolecule());
				planSchedule.setExecuteRate(div(intRate, 1,6));
				cashflowInterests.add(planSchedule);
				break;

			case 2:
				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd");
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.MONTH,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("月："+monthSpace);
				while(count-monthSpace<0){
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.MONTH,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));

					count++;
					cashflowInterests.add(planSchedule);
				}
				endDateString = (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				break;
			case 3:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.HALFYEAR.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.HALFYEAR,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("半年："+monthSpace);
				while(count-monthSpace<0)
				{
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}


					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.HALFYEAR,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				break;

			case 6:

				monthSpace = countMonths(mDate,vDate,"yyyy-MM-dd")/(Frequency.QUARTER.getPace());
				if(compareDate(addOrSubMonth_Day(vDate,Frequency.QUARTER,monthSpace),mDate,"yyyy-MM-dd"))
				{
					monthSpace = monthSpace-1;
				}
				System.out.println("季度："+monthSpace);
				while(count-monthSpace<0){

					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.QUARTER,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				endDateString =  (planSchedule==null||planSchedule.getRefEndDate()==null)?vDate:planSchedule.getRefEndDate();
				if(!isSameDate(endDateString,mDate,"yyyy-MM-dd"))
				{
					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(endDateString);
					planSchedule.setRefEndDate(mDate);
					planSchedule.setTheoryPaymentDate(mDate);
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				break;

			case 7:

				monthSpace = daysBetween(vDate,mDate)/(Frequency.DAY.getPace());
				System.out.println("天DAY："+monthSpace);
				while(count-monthSpace<0)
				{
					tempEndDate = addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays);
					if(tempEndDate.compareTo(mDate) >= 0)
					{
						break;
					}

					planSchedule = new CashflowInterest();
					planSchedule.setSeqNumber(cashflowInterests.size()+1);
					planSchedule.setRefBeginDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count,nDays));
					planSchedule.setRefEndDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setTheoryPaymentDate(addOrSubMonth_Day_N(vDate,Frequency.DAY,count+1,nDays));
					planSchedule.setDayCounter(dayCountBasis.getMolecule());

					planSchedule.setExecuteRate(div(intRate, 1,6));
					count++;
					cashflowInterests.add(planSchedule);
				}
				break;
			default:
				break;
		}
	}
}
