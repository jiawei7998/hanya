package com.singlee.capital.base.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.base.model.TcForm;
import com.singlee.capital.base.service.ProductFormService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
/**
 * @projectName 同业业务管理系统
 * @className 自定义产品表单映射控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-12 下午5:30:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/ProductFormController")
public class ProductFormController extends CommonController {
	
	@Autowired
	private ProductFormService productFormService;
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-7-29
	 */
	@ResponseBody
	@RequestMapping(value = "/searchFormList")
	public RetMsg<List<TcForm>> searchFormList(@RequestBody Map<String,Object> params){
		List<TcForm> list = productFormService.getFormList(params);
		return RetMsgHelper.ok(list);
	}
}
