package com.singlee.capital.ordersorting.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 来账分拣查询实体类
 * @author lxy
 *
 */
@Entity
@Table(name="TD_ORDERSORTING_DETAIL")
public class OrderSortingDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String  dealNo;
	private BigDecimal revAmt;
	private String revType;
	private String  refNo;
	private int refType;
	private String revDate;
	private BigDecimal srevAmt;
	private String srevDate;
	private String ccy;
	
	public String getSrevDate() {
		return srevDate;
	}
	public void setSrevDate(String srevDate) {
		this.srevDate = srevDate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public BigDecimal getRevAmt() {
		return revAmt;
	}
	public void setRevAmt(BigDecimal revAmt) {
		this.revAmt = revAmt;
	}
	public String getRevType() {
		return revType;
	}
	public void setRevType(String revType) {
		this.revType = revType;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public int getRefType() {
		return refType;
	}
	public void setRefType(int refType) {
		this.refType = refType;
	}
	public String getRevDate() {
		return revDate;
	}
	public void setRevDate(String revDate) {
		this.revDate = revDate;
	}
	public BigDecimal getSrevAmt() {
		return srevAmt;
	}
	public void setSrevAmt(BigDecimal srevAmt) {
		this.srevAmt = srevAmt;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
}
