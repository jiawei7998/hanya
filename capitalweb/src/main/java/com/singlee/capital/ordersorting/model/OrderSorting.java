package com.singlee.capital.ordersorting.model;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * 来账分拣查询实体类
 * @author lxy
 *
 */
@Entity
public class OrderSorting implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	/** 现金流编号 */
	private String serNo; 
	/** 交易编号 */
	private String dealNo;
	/** 交易类型 */
	private String dealType;
	/** 业务名称 */
	private String dealName;
	/** 来账日期 */
	private String refDate;
	/** 来账金额 */
	private String amAmt;
	/**产品编号*/
	private String prdNo;
	/**产品名称*/
	private String prdName;
	/**客户编号 */
	private String cNo;
	/**客户名称 */
	private String partyName;
	/**交易日期 */
	private String dealDate;
	/**起息日 */
	private String vDate;
	/**到期日 */
	private String mDate;
	/**金额类型*/
	private String amtType;
	/** 存续期交易编号 */
	private String cDealNo;
	private String ccy;
	
	private String selfAccCode;
	
	
	public String getSelfAccCode() {
		return selfAccCode;
	}
	public void setSelfAccCode(String selfAccCode) {
		this.selfAccCode = selfAccCode;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getAmAmt() {
		return amAmt;
	}
	public void setAmAmt(String amAmt) {
		this.amAmt = amAmt;
	}
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public String getRefDate() {
		return refDate;
	}
	public void setRefDate(String refDate) {
		this.refDate = refDate;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getAmtType() {
		return amtType;
	}
	public void setAmtType(String amtType) {
		this.amtType = amtType;
	}
	public String getcDealNo() {
		return cDealNo;
	}
	public void setcDealNo(String cDealNo) {
		this.cDealNo = cDealNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
	
}
