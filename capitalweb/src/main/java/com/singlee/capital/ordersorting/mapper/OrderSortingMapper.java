package com.singlee.capital.ordersorting.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.ordersorting.model.OrderSorting;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;

public interface OrderSortingMapper extends Mapper<OrderSorting> {

	public Page<OrderSorting> searchOrderSorting(Map<String, Object> params, RowBounds rb);
	
	public Page<OrderSorting> searchFeeOrderSorting(Map<String, Object> params, RowBounds rb);
	
	public List<OrderSorting> getOrderSorting(Map<String, Object> params);
	
	public List<OrderSorting> getFeeOrderSorting(Map<String, Object> params);
	
	public String getSponInstByDealNo(String dealNo);

	public Page<OrderSorting> searchOrderSortingByType(Map<String, Object> params, RowBounds rb);

	public Page<OrderSorting> searchOrderSortingByBenjin(Map<String, Object> params, RowBounds rowBounds);

	public Page<OrderSorting> searchOrderSortingByZhongshou(Map<String, Object> params, RowBounds rowBounds);

	public Page<OrderSorting> searchOrderSortingByLixi(Map<String, Object> params, RowBounds rowBounds);

	public List<OrderSorting> getOrderSortingByBenjin(Map<String, Object> params);

	public List<OrderSorting> getOrderSortingByLixi(Map<String, Object> params);

	public List<OrderSorting> getOrderSortingByZhongshou(Map<String, Object> params);

	public List<OrderSorting> getOrderSortingByFee(Map<String, Object> params);
	
	public Page<OrderSorting> searchOrderSortingAmt(Map<String, Object> params, RowBounds rb);
	
	public Page<OrderSorting> searchOrderSortingInt(Map<String, Object> params, RowBounds rb);

	public List<TdOrdersortingApprove> selectExistOrderSorting(Map<String, Object> map);
}
