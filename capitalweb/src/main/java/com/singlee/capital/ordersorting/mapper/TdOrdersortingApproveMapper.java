package com.singlee.capital.ordersorting.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;

import tk.mybatis.mapper.common.Mapper;


public interface TdOrdersortingApproveMapper extends Mapper<TdOrdersortingApprove>{

	public Page<TdOrdersortingApprove> searchTdOrdersortingApprove(
			Map<String, Object> params, RowBounds rb);
	
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveMySelf(
			Map<String, Object> params, RowBounds rb);
	
	public int updateTdOrdersortingApproveStatus(Map<String, Object> params);
	
	public TdOrdersortingApprove getTdOrdersortingApproveById(Map<String, Object> params);
	
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveFinished(
			Map<String, Object> params, RowBounds rb);

	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveUnfinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveByType(
			Map<String, Object> params, RowBounds rowBounds);
	
}
