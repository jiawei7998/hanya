package com.singlee.capital.ordersorting.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

/**
 * 来帐分拣审批表
 * @author SL_LXY
 *
 */
@Entity
@Table(name="TD_ORDERSORTING_APPROVE")
public class TdOrdersortingApprove implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//审批单号
	private String prdNo;//产品编号 序列自动生成
	private String dealType;//审批单类型 1-审批 2-核实
	private String sponsor;//审批发起人
	private String sponInst;//审批发起机构
	private String aDate;//审批开始日期
	private int version;//版本
	private String approveStatus;//审批状态
	private String refNo;//原交易编号
	private String cno;//交易对手
	private String refPrd;//原交易类型
	private String refAmt;//分拣总金额
	private String cDealNo;//现金流交易编号
	private String lstDate;//最后修改时间
	private String ccy;//币种
	private String orderType;//币种

	@Transient
	private String flowType;
	@Transient
	private String taskId;
	@Transient
	private String taskName;
	@Transient
	private String prdName;
	@Transient
	private String cnName;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;

	
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getRefPrd() {
		return refPrd;
	}
	public void setRefPrd(String refPrd) {
		this.refPrd = refPrd;
	}
	public String getRefAmt() {
		return refAmt;
	}
	public void setRefAmt(String refAmt) {
		this.refAmt = refAmt;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getcDealNo() {
		return cDealNo;
	}
	public void setcDealNo(String cDealNo) {
		this.cDealNo = cDealNo;
	}
	public String getLstDate() {
		return lstDate;
	}
	public void setLstDate(String lstDate) {
		this.lstDate = lstDate;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
}
