package com.singlee.capital.ordersorting.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.model.TsaAccountView;
import com.singlee.capital.ordersorting.model.OrderSorting;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.system.model.TtInstitutionSettls;

public interface OrderSortingService {
	
	public Page<OrderSorting> searchOrderSorting(Map<String,Object> params);
	
	public Page<OrderSorting> searchOrderSortingForSelect(Map<String,Object> params);
	
	public List<OrderSorting> getOrderSorting(Map<String, Object> params);
	
	public List<OrderSorting> getFeeOrderSorting(Map<String, Object> params);
	
	public Page<Root> selectTiTsaAccountByTranDate(Map<String,Object> params);
	
	public Page<TsaAccountView> selectTsaAccountViewByTranDate(Map<String,Object> params);
	/**
	 * 根据登录机构 下的 所有98账号 查询 T24动账通知
	 * @param params
	 * @return
	 */
	public Page<TsaAccountView> t24TdpAcStmtInqRequestByAcctNo(Map<String,Object> params);
	
	public List<Root> searchTiTsaAccountByTno(Map<String, Object> map);
	
	public void updateTiTsaAccountStatusByTno (Map<String, Object> map);
	
	public List<Root> searchTiTsaAccountByTflow(Map<String, Object> map);
	
	public List<TtInstitutionSettls> getAcctNoListByInstId(Map<String, Object> map);
	
	/**
	 * 根据单个 98 账号查询 T24动账通知
	 * @param params
	 * @return
	 */
	public Page<TsaAccountView> t24TdpAcStmtInqRequestByOneAcctNo(Map<String,Object> params);
	
	public Page<OrderSorting> searchOrderSortingByType(Map<String, Object> map);

	public List<TdOrdersortingApprove> selectExistOrderSorting(Map<String, Object> map);
	
	public List<OrderSorting> getOrderSortingByType(Map<String, Object> params);
	
}
