package com.singlee.capital.ordersorting.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.model.TsaAccountView;
import com.singlee.capital.ordersorting.model.OrderSorting;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.ordersorting.service.OrderSortingService;
import com.singlee.capital.system.model.TtInstitutionSettls;

@Controller
@RequestMapping(value = "/OrderSortingController")
public class OrderSortingController {

	@Resource
	OrderSortingService orderSortingService;

	/**
	 * 查询来账待分拣列表
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchOrderSorting")
	public RetMsg<PageInfo<OrderSorting>> searchOrderSorting(@RequestBody Map<String, Object> map) throws Exception {
		Page<OrderSorting> page = orderSortingService.searchOrderSorting(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询来账待分拣列表
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchOrderSortingForSelect")
	public RetMsg<PageInfo<OrderSorting>> searchOrderSortingForSelect(@RequestBody Map<String, Object> map) throws Exception {
		Page<OrderSorting> page = orderSortingService.searchOrderSortingForSelect(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getOrderSorting")
	public RetMsg<List<OrderSorting>> getOrderSorting(@RequestBody Map<String, Object> map) throws Exception {
		List<OrderSorting> page = orderSortingService.getOrderSorting(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFeeOrderSorting")
	public RetMsg<List<OrderSorting>> getFeeOrderSorting(@RequestBody Map<String, Object> map) throws Exception {
		List<OrderSorting> page = orderSortingService.getFeeOrderSorting(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询T24来账列表
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTiTsaAccountByTranDate")
	public RetMsg<PageInfo<TsaAccountView>> selectTiTsaAccountByTranDate(@RequestBody Map<String, Object> map) throws Exception {
		Page<TsaAccountView> page = orderSortingService.selectTsaAccountViewByTranDate(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询T24来账明细
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTiTsaAccountByTno")
	public RetMsg<List<Root>> searchTiTsaAccountByTno(@RequestBody Map<String, Object> map) throws Exception {
		List<Root> page = orderSortingService.searchTiTsaAccountByTno(map);
		return RetMsgHelper.ok(page);
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/searchTiTsaAccountByTflow")
	public RetMsg<List<Root>> searchTiTsaAccountByTflow(@RequestBody Map<String, Object> map) throws Exception {
		List<Root> page = orderSortingService.searchTiTsaAccountByTflow(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询T24动账通知(机构下所有账号)
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/moveAccountNotice")
	public RetMsg<PageInfo<TsaAccountView>> moveAccountNotice(@RequestBody Map<String, Object> map) throws Exception {
		Page<TsaAccountView> page = orderSortingService.t24TdpAcStmtInqRequestByAcctNo(map);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 查询T24动账通知 (单个账号查询)
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/moveAccountNoticeByOneAcctNo")
	public RetMsg<PageInfo<TsaAccountView>> moveAccountNoticeByOneAcctNo(@RequestBody Map<String, Object> map) throws Exception {
		Page<TsaAccountView> page = orderSortingService.t24TdpAcStmtInqRequestByOneAcctNo(map);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAcctNoListByInstId")
	public RetMsg<List<TtInstitutionSettls>> getAcctNoListByInstId(@RequestBody Map<String, Object> map) throws Exception {
		List<TtInstitutionSettls> list = orderSortingService.getAcctNoListByInstId(map);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchOrderSortingByType")
	public RetMsg<PageInfo<OrderSorting>> searchOrderSortingByType(@RequestBody Map<String, Object> map,String type) throws Exception {
		map.put("type", type);
		Page<OrderSorting> page = orderSortingService.searchOrderSortingByType(map);		
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/ifExistOrderSorting")
	public boolean ifExistOrderSorting(@RequestBody Map<String, Object> map) throws Exception {
		List<TdOrdersortingApprove> orders = orderSortingService.selectExistOrderSorting(map);
		if(orders != null && orders.size() > 0){
			return false;
		}
		return true;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getOrderSortingByType")
	public RetMsg<List<OrderSorting>> getOrderSortingByType(@RequestBody Map<String, Object> map) throws Exception {
		List<OrderSorting> page = orderSortingService.getOrderSortingByType(map);
		return RetMsgHelper.ok(page);
	}
	
}
