package com.singlee.capital.ordersorting.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.ordersorting.service.OrderSortingDetailService;
import com.singlee.capital.ordersorting.service.TdOrdersortingApproveService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;

@Controller
@RequestMapping(value = "/TdOrdersortingApproveController")
public class TdOrdersortingApproveController {

	@Resource
	TdOrdersortingApproveService tdOrdersortingApproveService;
	@Resource
	OrderSortingDetailService orderSortingDetailService;
	@Resource
	private TrdOrderMapper approveManageDao;
	@Resource
	InstitutionService institutionService;
	
	/**
	 * 添加
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/saveTdOrdersortingApprove")
	public RetMsg<Serializable> insertTdOrdersortingApprove(@RequestBody Map<String,Object> params) throws Exception {
		//根据orderNo判断是否未修改
		String orderNo = ParameterUtil.getString(params, "orderSortNo", "");
		if(orderNo == null || "".equals(orderNo))
		{
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "RE");
			orderNo = approveManageDao.getOrderId(mapSel);
		}
		params.put("dealNo", orderNo);
		params.put("version", 1);
		tdOrdersortingApproveService.insertTdOrdersortingApprove(params);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTdOrdersortingApprove")
	public RetMsg<Serializable> updateTdOrdersortingApprove(@RequestBody TdOrdersortingApprove map) throws Exception {
		tdOrdersortingApproveService.updateTdOrdersortingApprove(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTdOrdersortingApprove")
	public RetMsg<Serializable> deleteTdOrdersortingApprove(@RequestBody TdOrdersortingApprove map) throws Exception {
		tdOrdersortingApproveService.deleteTdOrdersortingApprove(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdOrdersortingApproveById")
	public RetMsg<TdOrdersortingApprove> getTdOrdersortingApproveById(@RequestBody Map<String,Object> map) throws Exception {
		TdOrdersortingApprove mo = tdOrdersortingApproveService.getTdOrdersortingApproveById(map);
		return RetMsgHelper.ok(mo);
	}
	
	/**
	 * 查询
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/getOrderSortingDetail")
	public RetMsg<List<OrderSortingDetail>> getOrderSortingDetail(@RequestBody Map<String,Object> params) throws Exception {
		List<OrderSortingDetail> mo = orderSortingDetailService.getOrderSortingDetail(params);
		return RetMsgHelper.ok(mo);
	}
	
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveByMyself")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchPageProductApproveFundByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatusArr =  DictConstants.ApproveStatus.New;
		params.put("approveStatus", approveStatusArr.split(","));
		Page<TdOrdersortingApprove> page = tdOrdersortingApproveService.searchTdOrdersortingApproveMySelf(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	/**
	 * 18/01/03 下面加的 3个方法（ByType结尾的）原来上海的没有的   
	 * 
	*/
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveByMyselfByType")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchTdOrdersortingApproveByMyselfByType(@RequestBody Map<String,Object> params,String type){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		//String approveStatusArr =  DictConstants.ApproveStatus.New;
		//params.put("approveStatus", approveStatusArr.split(","));
		params.put("type", type);                                       
		Page<TdOrdersortingApprove> page = tdOrdersortingApproveService.searchTdOrdersortingApproveMySelfByType(params,ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveUnfinished")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchTdOrdersortingApproveUnfinished(@RequestBody Map<String,Object> params){
		Page<TdOrdersortingApprove> page  = new Page<TdOrdersortingApprove>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			String approveStatusArr =  DictConstants.ApproveStatus.WaitApprove+","+DictConstants.ApproveStatus.Approving;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId());
			HashMap<String, Object> map =  new HashMap<String, Object>();
			map.put("instId", SlSessionHelper.getInstitution().getInstId());
			List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
			String str = "";
			for(int i=0;i<tlist.size();i++){
				if(i <(tlist.size()-1)){
					str+=tlist.get(i).getInstId()+",";
				}else{
					str+=tlist.get(i).getInstId();
				}
			}
			params.put("institutionList", str.split(","));
			page = tdOrdersortingApproveService.searchTdOrdersortingApprove(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RException(e);
		}
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveUnfinishedByType")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchTdOrdersortingApproveUnfinishedByType(@RequestBody Map<String,Object> params,String type){
		Page<TdOrdersortingApprove> page  = new Page<TdOrdersortingApprove>();
		try {
			params.put("isActive", DictConstants.YesNo.YES);
			//String approveStatusArr =  DictConstants.ApproveStatus.Verifying+","+DictConstants.ApproveStatus.WaitVerify;
			String approveStatusArr = DictConstants.ApproveStatus.WaitApprove
									+","+DictConstants.ApproveStatus.Approving
									+","+DictConstants.ApproveStatus.Verifying;
			params.put("approveStatus", approveStatusArr.split(","));
			params.put("userId", SlSessionHelper.getUserId());
			params.put("insId", SlSessionHelper.getInstitutionId()); 
			params.put("type", type);
			page = tdOrdersortingApproveService.searchTdOrdersortingApproveUnfinishedByType(params,ParameterUtil.getRowBounds(params));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveFinished")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchTdOrdersortingApproveFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verifying + "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		HashMap<String, Object> map =  new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitution().getInstId());
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		String str = "";
		for(int i=0;i<tlist.size();i++){
			if(i <(tlist.size()-1)){
				str+=tlist.get(i).getInstId()+",";
			}else{
				str+=tlist.get(i).getInstId();
			}
		}
		params.put("institutionList", str.split(","));
		Page<TdOrdersortingApprove> page = tdOrdersortingApproveService.searchTdOrdersortingApproveFinished(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchTdOrdersortingApproveFinishedByType")
	public RetMsg<PageInfo<TdOrdersortingApprove>> searchTdOrdersortingApproveFinishedByType(@RequestBody Map<String,Object> params,String type){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			params.put("approveStatus", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		params.put("type", type);
		Page<TdOrdersortingApprove> page = tdOrdersortingApproveService.searchTdOrdersortingApproveFinishedByType(params, ParameterUtil.getRowBounds(params));
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 调用多贷多借接口
	 * @param 
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/t24TStBeaRepayAaaRequestOrder")
	public RetMsg<Serializable> t24TStBeaRepayAaaRequestOrder(@RequestBody Map<String,Object> params) {
		String retMsg = "";
		try {
			retMsg = tdOrdersortingApproveService.t24TStBeaRepayAaaRequestOrder(params);
		} catch (Exception e) {
			retMsg = e.getMessage().toString();
			JY.error(e.getMessage(), e);
		}
		return RetMsgHelper.ok(retMsg);
	}
	
}
