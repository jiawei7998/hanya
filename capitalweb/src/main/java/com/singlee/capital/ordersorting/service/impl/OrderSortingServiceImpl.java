package com.singlee.capital.ordersorting.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.model.TDpAcStmtInfoRec;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRq;
import com.singlee.capital.interfacex.model.TDpAcStmtInqRs;
import com.singlee.capital.interfacex.model.TsaAccountView;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.ordersorting.mapper.OrderSortingMapper;
import com.singlee.capital.ordersorting.model.OrderSorting;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.ordersorting.service.OrderSortingService;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TtInstitutionSettls;

@Service
public class OrderSortingServiceImpl implements OrderSortingService {
	
	@Resource
	OrderSortingMapper orderSortingMapper;
	@Resource
	DictionaryGetService dictionaryGetService;
	@Resource
	TiInterfaceInfoMapper tiInterfaceInfoMapper;
	@Resource
	private SocketClientService socketClientService;
	@Resource
	private TtInstitutionSettlsMapper institutionSettlsMapper;
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;

	@Override
	public Page<OrderSorting> searchOrderSorting(Map<String, Object> params) {
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		String opType = String.valueOf(params.get("opType"));
		//查询普通待分拣记录
		Page<OrderSorting> page = new Page<OrderSorting>(); 
		if("1".equals(opType)){
			page = orderSortingMapper.searchOrderSorting(params,ParameterUtil.getRowBounds(params));
		}else{//查询费用待分拣记录
			page = orderSortingMapper.searchFeeOrderSorting(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	@Override
	public Page<OrderSorting> searchOrderSortingForSelect(Map<String, Object> params) {
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		String type = String.valueOf(params.get("type"));
		if("1".equals(type))// 本金
		{
			return orderSortingMapper.searchOrderSortingAmt(params,ParameterUtil.getRowBounds(params));
			
		}else if("2".equals(type))// 利息
		{
			return orderSortingMapper.searchOrderSortingInt(params,ParameterUtil.getRowBounds(params));
			
		}else{
			return orderSortingMapper.searchOrderSorting(params,ParameterUtil.getRowBounds(params));
			
		}
	}

	@Override
	public List<OrderSorting> getOrderSorting(Map<String, Object> params){
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");//
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		if(params.containsKey("dealTypeArr")){
			String tnoStr = !"".equals(params.get("dealTypeArr")) ?params.get("dealTypeArr").toString() : "";
			String[] tnoArr = tnoStr.split(",");
			params.put("dealTypeArr", tnoArr);
		}
		if(params.containsKey("refDateArr")){
			String refDateStr = !"".equals(params.get("refDateArr")) ?params.get("refDateArr").toString() : "";
			String[] refDateArr = refDateStr.split(",");
			params.put("refDateArr", refDateArr);
		}
		
		return orderSortingMapper.getOrderSorting(params);
	}

	@Override
	public Page<Root> selectTiTsaAccountByTranDate(Map<String, Object> params) {
		return tiInterfaceInfoMapper.selectTiTsaAccountByTranDate(params,ParameterUtil.getRowBounds(params));
	}
	
	@Override
	public Page<TsaAccountView> selectTsaAccountViewByTranDate(Map<String, Object> params) {
		if(params.containsKey("beginDate") && params.get("beginDate").toString().trim().length()>0)	{
			params.put("beginDate", params.get("beginDate").toString().trim().replaceAll("-", ""));
		}
		if(params.containsKey("endDate") && params.get("endDate").toString().trim().length()>0)	{
			params.put("endDate", params.get("endDate").toString().trim().replaceAll("-", ""));
		}
		Page<TsaAccountView> list = tiInterfaceInfoMapper.selectTsaAccountViewByTran(params,ParameterUtil.getRowBounds(params));
		return list;
	}

	@Override
	public List<Root> searchTiTsaAccountByTno(Map<String, Object> map) {
		if(map.containsKey("tNo")){
			String tnoStr = !"".equals(map.get("tNo")) ?map.get("tNo").toString() : "";
			String[] tnoArr = tnoStr.split(",");
			map.put("tNo", tnoArr);
		}
		if(map.containsKey("tFlow")){
			String tFlowStr = !"".equals(map.get("tFlow")) ?map.get("tFlow").toString() : "";
			String[] tFlowArr = tFlowStr.split(",");
			map.put("tFlow", tFlowArr);
		}
		return tiInterfaceInfoMapper.searchTiTsaAccountByTno(map);
	}
	//searchTiTsaAccountByTflow
	
	@Override
    public List<Root> searchTiTsaAccountByTflow(Map<String, Object> map) {
		if(map.containsKey("tNo")){
			String tnoStr = !"".equals(map.get("tNo")) ?map.get("tNo").toString() : "";
			String[] tnoArr = tnoStr.split(",");
			map.put("tNo", tnoArr);
		}
		if(map.containsKey("tFlow")){
			String tFlowStr = !"".equals(map.get("tFlow")) ?map.get("tFlow").toString() : "";
			String[] tFlowArr = tFlowStr.split(",");
			map.put("tFlow", tFlowArr);
		}
		return tiInterfaceInfoMapper.searchTiTsaAccountByTflow(map);
	}

	@Override
	public List<OrderSorting> getFeeOrderSorting(Map<String, Object> params) {
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		if(params.containsKey("dealTypeArr")){
			String tnoStr = !"".equals(params.get("dealTypeArr")) ?params.get("dealTypeArr").toString() : "";
			String[] tnoArr = tnoStr.split(",");
			params.put("dealTypeArr", tnoArr);
		}
		if(params.containsKey("refDateArr")){
			String refDateStr = !"".equals(params.get("refDateArr")) ?params.get("refDateArr").toString() : "";
			String[] refDateArr = refDateStr.split(",");
			params.put("refDateArr", refDateArr);
		}
		return orderSortingMapper.getFeeOrderSorting(params);
	}

	@Override
	public void updateTiTsaAccountStatusByTno(Map<String, Object> map) {
		String tnoStr = map.get("tNo") != "" ?map.get("tNo").toString() : "";
		String[] tnoArr = tnoStr.split(",");
		map.put("tNo", tnoArr);
		tiInterfaceInfoMapper.updateTiTsaAccountStatusByTno(map);
	}

	@Override
	public Page<TsaAccountView> t24TdpAcStmtInqRequestByAcctNo(Map<String, Object> params) {
		//
		Page<TsaAccountView> page=new Page<TsaAccountView>();
		try {
			TDpAcStmtInqRq request=new TDpAcStmtInqRq();
			List<TtInstitutionSettls> settlsAcct= institutionSettlsMapper.getInstSettlsList(params);
			for (TtInstitutionSettls ttInstitutionSettls : settlsAcct) {
				request.setMediumType(ttInstitutionSettls.getMediumType());
				request.setAcctNo(ttInstitutionSettls.getInstAcctNo().trim());
				request.setCurrency(ttInstitutionSettls.getCcy());
				request.setBeginDt("".equals(params.get("beginDate").toString())?CalendarUtil.getDateStr(new Date(),"yyyyMMdd"):params.get("beginDate").toString().replace("-", "").replace("-", ""));
				request.setEndDt("".equals(params.get("endDate").toString())?CalendarUtil.getDateStr(new Date(),"yyyyMMdd"):params.get("endDate").toString().replace("-", "").replace("-", ""));
				request.setMode("I");
				CommonRqHdr hdr = new CommonRqHdr();
				hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
				hdr.setRqUID(UUID.randomUUID().toString());
				hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
				request.setCommonRqHdr(hdr);
				TDpAcStmtInqRs response= socketClientService.t24TdpAcStmtInqRequest(request);
				if("000000".equals(response.getCommonRsHdr().getStatusCode())){
					Map<String, Object> map = new HashMap<String, Object>();
					List<TDpAcStmtInfoRec> list=response.getTDpAcStmtInfoRec();
					for (TDpAcStmtInfoRec tDpAcStmtInfoRec : list) {
						map.put("payAcct", ttInstitutionSettls.getInstAcctNo()==null|| "".equals(ttInstitutionSettls.getInstAcctNo())?"":ttInstitutionSettls.getInstAcctNo().trim());
						map.put("payNme",ttInstitutionSettls.getInstAcctNm()==null|| "".equals(ttInstitutionSettls.getInstAcctNm())?"":ttInstitutionSettls.getInstAcctBknm().trim());
					    map.put("payBz", ttInstitutionSettls.getCcy()==null|| "".equals(ttInstitutionSettls.getCcy())?"":ttInstitutionSettls.getCcy().trim());
					    map.put("resBz", ttInstitutionSettls.getCcy()==null|| "".equals(ttInstitutionSettls.getCcy())?"":ttInstitutionSettls.getCcy().trim());
					    map.put("tranType", tDpAcStmtInfoRec.getVouType());
					    map.put("tranZy", tDpAcStmtInfoRec.getDesc());
					    map.put("tranAmt", tDpAcStmtInfoRec.getTxnAmt());
					    map.put("tranDate", tDpAcStmtInfoRec.getTxnDate());
					    map.put("tranTime", tDpAcStmtInfoRec.getTxnTime());
					    map.put("resAcct", tDpAcStmtInfoRec.getOppAc());
					    map.put("resNme", tDpAcStmtInfoRec.getOppacName());
					    map.put("tRmk", tDpAcStmtInfoRec.getRemark());
					    map.put("tFlow", tDpAcStmtInfoRec.getTxnRef());
					    map.put("tNo", tDpAcStmtInfoRec.getStmt());
					    map.put("virAcct", "");
					    map.put("status", "0");
						Root root=new Root();
						ParentChildUtil.HashMapToClass(map, root);
						Map<String, Object> newmap=new HashMap<String, Object>();
						newmap.put("txnRef", tDpAcStmtInfoRec.getTxnRef());
						List<Root> listRoot=tiInterfaceInfoMapper.searchTiTsaAccountByTflow(newmap);
						if(tDpAcStmtInfoRec.getTxnDrcr()!=null&& "CREDIT".equals(tDpAcStmtInfoRec.getTxnDrcr())){
							if(ti2ndPaymentMapper.querySrquidCount(tDpAcStmtInfoRec.getTxnRef())<=0){
								if(listRoot.size()==0){
									tiInterfaceInfoMapper.insertTiTsaAccount(root);
								}
							}
						}
					}
				}
			}
			page=selectTsaAccountViewByTranDate(params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new RException(e);
		}
		return page;
	}

	@Override
	public List<TtInstitutionSettls> getAcctNoListByInstId(Map<String, Object> map) {
		List<TtInstitutionSettls> settlsAcct= institutionSettlsMapper.getInstSettlsList(map);
		return settlsAcct;
	}
	@Override
	public Page<TsaAccountView> t24TdpAcStmtInqRequestByOneAcctNo(Map<String, Object> params) {
		//
		Page<TsaAccountView> page=new Page<TsaAccountView>();
		try {  
			TDpAcStmtInqRq request=new TDpAcStmtInqRq();
			List<TtInstitutionSettls> settlsAcct= institutionSettlsMapper.getInstSettlsListByInstAcctNo(params);
			for (TtInstitutionSettls ttInstitutionSettls : settlsAcct) {
				request.setMediumType(ttInstitutionSettls.getMediumType());
				request.setAcctNo(ttInstitutionSettls.getInstAcctNo()==null?"":ttInstitutionSettls.getInstAcctNo().trim());
				request.setCurrency(ttInstitutionSettls.getCcy());
				request.setBeginDt("".equals(params.get("beginDate").toString())?CalendarUtil.getDateStr(new Date(),"yyyyMMdd"):params.get("beginDate").toString().replace("-", ""));
				request.setEndDt("".equals(params.get("endDate").toString())?CalendarUtil.getDateStr(new Date(),"yyyyMMdd"):params.get("endDate").toString().replace("-", ""));
				request.setMode("I");
				CommonRqHdr hdr = new CommonRqHdr();
				hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
				hdr.setRqUID(UUID.randomUUID().toString());
				hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
				request.setCommonRqHdr(hdr);
				TDpAcStmtInqRs response= socketClientService.t24TdpAcStmtInqRequest(request);
				if("000000".equals(response.getCommonRsHdr().getStatusCode())){
					Map<String, Object> map = new HashMap<String, Object>();
					List<TDpAcStmtInfoRec> list=response.getTDpAcStmtInfoRec();
					for (TDpAcStmtInfoRec tDpAcStmtInfoRec : list) {
						map.put("payAcct", ttInstitutionSettls.getInstAcctNo()==null?"":ttInstitutionSettls.getInstAcctNo().trim());
						map.put("payNme",ttInstitutionSettls.getInstAcctNm()==null?"":ttInstitutionSettls.getInstAcctNm().trim());
					    map.put("payBz", ttInstitutionSettls.getCcy()==null?"":ttInstitutionSettls.getCcy().trim());
					    map.put("resBz", ttInstitutionSettls.getCcy()==null?"":ttInstitutionSettls.getCcy().trim());
					    map.put("tranType", tDpAcStmtInfoRec.getVouType());
					    map.put("tranZy", tDpAcStmtInfoRec.getDesc());
					    map.put("tranAmt", tDpAcStmtInfoRec.getTxnAmt());
					    map.put("tranDate", tDpAcStmtInfoRec.getTxnDate());
					    map.put("tranTime", tDpAcStmtInfoRec.getTxnTime());
					    map.put("resAcct", tDpAcStmtInfoRec.getOppAc());
					    map.put("resNme", tDpAcStmtInfoRec.getOppacName());
					    map.put("tRmk", tDpAcStmtInfoRec.getRemark());
					    map.put("tFlow", tDpAcStmtInfoRec.getTxnRef());
					    map.put("tNo", tDpAcStmtInfoRec.getStmt());
					    map.put("virAcct", "");
					    map.put("status", "0");
						Root root=new Root();
						ParentChildUtil.HashMapToClass(map, root);
						Map<String, Object> newmap=new HashMap<String, Object>();
						newmap.put("txnRef", tDpAcStmtInfoRec.getTxnRef());
						List<Root> listRoot=tiInterfaceInfoMapper.searchTiTsaAccountByTflow(newmap);
						if(tDpAcStmtInfoRec.getTxnDrcr()!=null && "CREDIT".equals(tDpAcStmtInfoRec.getTxnDrcr())){
							if(ti2ndPaymentMapper.querySrquidCount(tDpAcStmtInfoRec.getTxnRef())<=0){
								if(listRoot.size()==0){
									tiInterfaceInfoMapper.insertTiTsaAccount(root);
								}
							}
						}
					}
				}
			}
			page=selectTsaAccountViewByTranDate(params);
		} catch (Exception e) {
			throw new RException(e);
		}
		return page;
	}


	@Override
	public Page<OrderSorting> searchOrderSortingByType(Map<String, Object> params) {
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		if("1".equals(params.get("type"))){
			return orderSortingMapper.searchOrderSortingByBenjin(params,ParameterUtil.getRowBounds(params));
		}else if("2".equals(params.get("type"))){
			return orderSortingMapper.searchOrderSortingByLixi(params,ParameterUtil.getRowBounds(params));
		}else if("999".equals(params.get("type"))){
			return orderSortingMapper.searchOrderSortingByZhongshou(params,ParameterUtil.getRowBounds(params));
		}
		return orderSortingMapper.searchOrderSortingByType(params,ParameterUtil.getRowBounds(params));
	}

	@Override
	public List<TdOrdersortingApprove> selectExistOrderSorting(Map<String, Object> map) {
		return orderSortingMapper.selectExistOrderSorting(map);
	}

	@Override
	public List<OrderSorting> getOrderSortingByType(Map<String, Object> params) {
		List<TaDictVo> list = dictionaryGetService.getTaDictByCode("orderTerm");
		if(list.size() > 0){
			params.put("orderTerm", list.get(0).getDict_key());
		}else{
			params.put("orderTerm", "1");
		}
		if("1".equals(params.get("type"))){
			return orderSortingMapper.getOrderSortingByBenjin(params);
			
		}else if("2".equals(params.get("type"))){
			return orderSortingMapper.getOrderSortingByLixi(params);
			
		}else if("999".equals(params.get("type"))){
			return orderSortingMapper.getOrderSortingByZhongshou(params);
			
		}else if("5".equals(params.get("type"))){
			return orderSortingMapper.getOrderSortingByFee(params);
			
		}
		return orderSortingMapper.getOrderSorting(params);
	}

}
