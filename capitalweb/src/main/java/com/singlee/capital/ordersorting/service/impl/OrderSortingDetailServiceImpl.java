package com.singlee.capital.ordersorting.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.ordersorting.mapper.OrderSortingDetailMapper;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;
import com.singlee.capital.ordersorting.service.OrderSortingDetailService;

@Service
public class OrderSortingDetailServiceImpl implements OrderSortingDetailService{

	@Resource
	OrderSortingDetailMapper orderSortingDetailMapper;

	@Override
	public List<OrderSortingDetail> getOrderSortingDetail(
			Map<String, Object> params) {
		if(params.containsKey("refNoArr")){
			String tnoStr = !"".equals(params.get("refNoArr")) ?params.get("refNoArr").toString() : "";
			String[] tnoArr = tnoStr.split(",");
			params.put("refNoArr", tnoArr);
		}
		return orderSortingDetailMapper.getOrderSortingDetail(params);
	}

	@Override
	public int insertOrderSortingDetail(Map<String, Object> params) {
		int flag = 0;
		String jsonStr = params.get("jsonStr") != null ? params.get("jsonStr").toString():"";
		jsonStr = jsonStr.replaceAll("DD123", params.get("dealNo").toString()) ;
		if(!"".equals(jsonStr)){
			List<OrderSortingDetail> list = FastJsonUtil.parseArrays(jsonStr, OrderSortingDetail.class);
			if(list.size() > 0){
				flag = orderSortingDetailMapper.insertOrderSortingDetail(list);
			}
		}
		return flag;
	}

	@Override
	public int deleteOrderSortingDetailByDeal(Map<String, Object> params) {
		return orderSortingDetailMapper.deleteOrderSortingDetail(params);
	}

	@Override
	public List<OrderSortingDetail> getOrderSortingDetailByRef(
			Map<String, Object> params) {
		return orderSortingDetailMapper.getOrderSortingDetailByRef(params);
	}

	@Override
	public List<OrderSortingDetail> getOrderSortingDetailByDealNo(Map<String, Object> params) {
		return orderSortingDetailMapper.getOrderSortingDetailByDealNo(params);
	}

}
