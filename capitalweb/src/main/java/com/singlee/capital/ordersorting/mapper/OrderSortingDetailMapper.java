package com.singlee.capital.ordersorting.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;

import tk.mybatis.mapper.common.Mapper;

public interface OrderSortingDetailMapper extends Mapper<OrderSortingDetail>{
	
	public int insertOrderSortingDetail(List<OrderSortingDetail> list);
	public int deleteOrderSortingDetail(Map<String,Object> params);
	public List<OrderSortingDetail> getOrderSortingDetail(Map<String, Object> params);
	
	public List<OrderSortingDetail> getOrderSortingDetailByDealNo(Map<String, Object> params);
	public List<OrderSortingDetail> getOrderSortingDetailByRef(Map<String, Object> params);

}
