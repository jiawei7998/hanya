package com.singlee.capital.ordersorting.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;

public interface TdOrdersortingApproveService {
	
	public Page<TdOrdersortingApprove> searchTdOrdersortingApprove(Map<String,Object> params, RowBounds rb);
	
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveMySelf(Map<String,Object> params, RowBounds rb);
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveFinished(Map<String,Object> params, RowBounds rb);
	public List<TdOrdersortingApprove> getTdOrdersortingApprove(Map<String, String> params);
	
	public  int insertTdOrdersortingApprove(Map<String,Object> params);
	
	public  int deleteTdOrdersortingApprove(TdOrdersortingApprove params);
	
	public  int updateTdOrdersortingApprove(TdOrdersortingApprove params);
	
	public TdOrdersortingApprove getTdOrdersortingApproveById(Map<String, Object> params);
	
	public String t24TStBeaRepayAaaRequestOrder(Map<String,Object> params) throws Exception;

	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveMySelfByType(Map<String, Object> params, RowBounds rowBounds);

	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveUnfinishedByType(Map<String, Object> params, RowBounds rowBounds);

	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveFinishedByType(Map<String, Object> params, RowBounds rowBounds);
			
	public void approveTest(String serialNo);
}
