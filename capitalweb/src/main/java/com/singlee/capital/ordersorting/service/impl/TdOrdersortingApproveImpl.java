package com.singlee.capital.ordersorting.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.Acctno407Mapper;
import com.singlee.capital.base.model.Acctno407;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.fund.service.TdDurationFundCashdivService;
import com.singlee.capital.fund.service.TdDurationFundRedeemService;
import com.singlee.capital.fund.service.TdFundAcupService;
import com.singlee.capital.fund.service.TdProductFundTposService;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceInfoMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.CreditBeaRec;
import com.singlee.capital.interfacex.model.DebitBeaRec;
import com.singlee.capital.interfacex.model.Root;
import com.singlee.capital.interfacex.model.TExReconAllInqRec;
import com.singlee.capital.interfacex.model.TExReconAllInqRq;
import com.singlee.capital.interfacex.model.TExReconAllInqRs;
import com.singlee.capital.interfacex.model.TExReconT24lz;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.qdb.service.CreditService;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.ordersorting.mapper.OrderSortingMapper;
import com.singlee.capital.ordersorting.mapper.TdOrdersortingApproveMapper;
import com.singlee.capital.ordersorting.model.OrderSortingDetail;
import com.singlee.capital.ordersorting.model.TdOrdersortingApprove;
import com.singlee.capital.ordersorting.service.OrderSortingDetailService;
import com.singlee.capital.ordersorting.service.OrderSortingService;
import com.singlee.capital.ordersorting.service.TdOrdersortingApproveService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityCurrentMapper;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityMapper;
import com.singlee.capital.trade.mapper.TdFeeHandleCashflowMapper;
import com.singlee.capital.trade.mapper.TdInterestSettleMapper;
import com.singlee.capital.trade.mapper.TdOutrightSaleMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdInterestSettle;
import com.singlee.capital.trade.model.TdOutrightSale;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;

@Transactional(value="transactionManager",rollbackFor=Exception.class)
@Service("tdOrdersortingApproveService")
public class TdOrdersortingApproveImpl implements TdOrdersortingApproveService,SlbpmCallBackInteface,TaskEventInterface{

	@Resource
	TdOrdersortingApproveMapper tdOrdersortingApproveMapper;
	@Resource
	OrderSortingDetailService orderSortingDetailService;
	@Resource
	TdProductFundTposService tdProductFundTposService;
//	交易还本执行表
	@Resource
	TdAmtRangeMapper amtRangeMapper;
	@Autowired
	private TrdTradeMapper tradeMapper;
//	@Autowired
//	private AdvanceMaturityService advanceMaturityService;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private BookkeepingService bookkeepingService;
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;
	@Autowired
	private TrdTposMapper tposMapper;
	@Autowired
	private TdCashflowCapitalMapper tdCashflowCapitalMapper;
	@Autowired
	private TdCashflowInterestMapper tdCashflowInterestMapper;
	@Autowired
	private BatchDao batchDao;
//	@Autowired
//	private TdOverDueConfirmService overDueConfirmService;
//	@Autowired
//	private TdProductFeeDealMapper productFeeDealMapper;
//	@Autowired
//	private TdCashflowFeeMapper tdCashflowFeeMapper;
	@Resource
	TdDurationFundRedeemService tdDurationFundRedeemService;
	@Resource
	TdFundAcupService tdFundAcupService;
	@Resource
	TdDurationFundCashdivService tdDurationFundCashdivService;
	@Resource
	OrderSortingService orderSortingService;
	@Autowired
	TdInterestSettleMapper tdInterestSettleMapper;
	@Autowired
	private AdvanceMaturityCurrentService advanceMaturityCurrentService;
	@Autowired
	InterestSettleService interestSettleService;
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdCustBatchStatusMapper edCustBatchStatusMapper;
	@Autowired
	TdFeeHandleCashflowMapper feeHandleCashflowMapper;
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private  Acctno407Mapper acctno407Mapper;
	@Autowired
	TtInstitutionSettlsMapper ttInstitutionSettlsMapper;
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private TiInterfaceInfoMapper tiInterfaceInfoMapper;
	@Autowired
	private OrderSortingMapper orderSortingMapper;
	@Autowired 
	private CreditService creditService;
	@Autowired
	private TdOutrightSaleMapper outrightSaleMapper;
	@Autowired
	private TdAdvanceMaturityMapper tdAdvanceMaturityMapper;
	@Autowired
	private TdAdvanceMaturityCurrentMapper tdAdvanceMaturityCurrentMapper;
	
	@Override
	public List<TdOrdersortingApprove> getTdOrdersortingApprove(
			Map<String, String> params) {
		return null;
	}

	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApprove(
			Map<String, Object> params, RowBounds rb) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApprove(params, rb);
	}

	@Override
	public int deleteTdOrdersortingApprove(TdOrdersortingApprove params) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("dealNo",params.getDealNo());
		//先更新动账表status为 0
		List<OrderSortingDetail> list=orderSortingDetailService.getOrderSortingDetail(map);
		if(list.size()>0){
			String tnoStr="";
			for (OrderSortingDetail orderSortingDetail : list) {
				if(orderSortingDetail.getRefType()==1){ // 0 同业系统实收    1来账列表实收
					tnoStr+=orderSortingDetail.getRefNo()+",";
				}
			}
			if(tnoStr.length()>0){
				map.put("tNo", tnoStr);
				map.put("status", '0');
				orderSortingService.updateTiTsaAccountStatusByTno(map); //更新动账表ti_tsaAccount
			}
		}
		//先删除款项明细表
		orderSortingDetailService.deleteOrderSortingDetailByDeal(map);
		return tdOrdersortingApproveMapper.delete(params);
	}

	@Override
	public int insertTdOrdersortingApprove(Map<String,Object> params) {
		TdOrdersortingApprove tdOrdersortingApprove = new TdOrdersortingApprove();
		BeanUtil.populate(tdOrdersortingApprove, params);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		tdOrdersortingApprove.setLstDate(df.format(new Date()));
		//查询是否已经存在，如果存在：先删除，再添加
		TdOrdersortingApprove orderSorting = tdOrdersortingApproveMapper.getTdOrdersortingApproveById(params);
		if(null != orderSorting){
			tdOrdersortingApproveMapper.deleteByPrimaryKey(tdOrdersortingApprove);
			//先更新动账表status为 0
//			List<OrderSortingDetail> list = orderSortingDetailService.getOrderSortingDetail(params);
//			if(list.size() > 0){
//				String tnoStr="";
//				for (OrderSortingDetail orderSortingDetail : list) {
//					if(orderSortingDetail.getRefType()==1){
//						tnoStr+=orderSortingDetail.getRefNo()+",";
//					}
//				}
//				if(tnoStr.length()>0){
//					params.put("tNo", tnoStr);
//					params.put("status", '0');
//					orderSortingService.updateTiTsaAccountStatusByTno(params); //更新动账表
//				}
//			}
			orderSortingDetailService.deleteOrderSortingDetailByDeal(params);
		}
		int flag = tdOrdersortingApproveMapper.insert(tdOrdersortingApprove);
		if(flag > 0){
			flag = orderSortingDetailService.insertOrderSortingDetail(params);
//			String jsonStr = params.get("jsonStr") != null ? params.get("jsonStr").toString():"";
//			if(!"".equals(jsonStr)){
//				List<OrderSortingDetail> list = FastJsonUtil.parseArrays(jsonStr, OrderSortingDetail.class);
//				String tnoStr="";
//				for (OrderSortingDetail orderSortingDetail : list) {
//					if(orderSortingDetail.getRefType()==1){
//						tnoStr+=orderSortingDetail.getRefNo()+",";
//					}
//				}
//				if(tnoStr.length()>0){
//					params.put("tNo", tnoStr);
//					params.put("status", '2');
//					orderSortingService.updateTiTsaAccountStatusByTno(params); //更新动账表
//				}
//			}
		}
		return flag;
	}

	@Override
	public int updateTdOrdersortingApprove(TdOrdersortingApprove params) {
		return tdOrdersortingApproveMapper.updateByPrimaryKey(params);
	}

	@Override
	public Object getBizObj(String flowType, String flowId, String serialNo) {
		return null;
	}
	public void creditRelease(String dealNo,String approveStatus,String releaseAmt){
		try {
			creditService.creditRelease(dealNo, approveStatus,releaseAmt);
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("额度释放出错", e);
		}
	}

	
	/****
	@Override
	public void statusChange(String flowType, String flowId, String serialNo,
			String status, String flowCompleteType) {
		
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serialNo);
		map.put("approveStatus", status);
		//更新状态哦
		tdOrdersortingApproveMapper.updateTdOrdersortingApproveStatus(map);
		if(status.equals("6")){//审批通过
			try{
			//审批通过
			TdOrdersortingApprove tdOrdersortingApprove = tdOrdersortingApproveMapper.getTdOrdersortingApproveById(map);
			List<OrderSortingDetail> orDetails = orderSortingDetailService.getOrderSortingDetail(BeanUtil.beanToHashMap(tdOrdersortingApprove));
			Map<String,OrderSortingDetail> tmcMaps = Maps.uniqueIndex(orDetails, new Function <OrderSortingDetail,String> () {  
		          public String apply(OrderSortingDetail orderSortingDetail) {  
		            return orderSortingDetail.getRevType()+"-"+orderSortingDetail.getRefNo();   
		    }});  

			String postDate = dayendDateService.getDayendDate();
			//通过 来账分拣 获得原始交易单据的类型
			HashMap<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("trade_id",(tdOrdersortingApprove.getcDealNo()==null ||tdOrdersortingApprove.getcDealNo().equals("")||
					tdOrdersortingApprove.getcDealNo().equals("null"))
					?tdOrdersortingApprove.getRefNo():tdOrdersortingApprove.getcDealNo());
			
			//基金模块来帐分拣处理
			TtTrdTrade trade = tradeMapper.selectTradeForTradeId(paramsMap);//根据来账分拣匹配的refNo查询该笔收款对应的动作交易
			//判断是否查找投资类交易是否为空，为空的情况下去查找基金存续期操作（赎回/分红）
			if(trade == null){
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				//插入每日金额过渡表
				TdDurationFundRedeem tdDurationFundRedeem = new TdDurationFundRedeem();
				tdDurationFundRedeem.setDealNo(ParameterUtil.getString(paramsMap, "trade_id",""));
				TdDurationFundRedeem fund = null;Map<String,Object> amtMap = new HashMap<String, Object>();
				TdAccTrdDaily tdAccTrdDaily = new TdAccTrdDaily();//基金帐务都是逐条处理
				
				TdDurationFundCashdiv tdDuration = new TdDurationFundCashdiv();
				tdDuration.setDealNo(paramsMap.get("trade_id").toString());
				TdDurationFundCashdiv cashdiv = null;
				
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
					switch (keyNum) {
					case 6://基金赎回-本金
						//查询基金赎回原交易
						
						fund = tdDurationFundRedeemService.getTdDurationFundRedeem(BeanUtil.beanToMap(tdDurationFundRedeem));
						if(fund != null){
							//tdAccTrdDaily.setAccAmt(fund.getRedeemAmt().add(fund.getDividendAmt()).doubleValue());
							//按实际分拣金额处理
							tdAccTrdDaily.setAccAmt(tmcMaps.get(key)!=null?
									PlaningTools.add(tdAccTrdDaily.getAccAmt(), tmcMaps.get(key).getRevAmt().doubleValue())
									:PlaningTools.add(tdAccTrdDaily.getAccAmt(),fund.getRedeemAmt().doubleValue()));
							
							tdAccTrdDaily.setDealNo(fund.getDealNo());
							tdAccTrdDaily.setOpTime(fund.getvDate());
							tdAccTrdDaily.setPostDate(fund.getvDate());
							tdAccTrdDaily.setAccType(DurationConstants.AccType.REDEEM_FUND_AMT);
							amtMap.put(DurationConstants.AccType.REDEEM_FUND_AMT,tdAccTrdDaily.getAccAmt());//放款会计事件
							
						}
						break;
					case 8://基金赎回-利息
						//查询基金赎回原交易
						fund = tdDurationFundRedeemService.getTdDurationFundRedeem(BeanUtil.beanToMap(tdDurationFundRedeem));
						if(fund != null){
							//tdAccTrdDaily.setAccAmt(fund.getRedeemAmt().add(fund.getDividendAmt()).doubleValue());
							//按实际分拣金额处理
							tdAccTrdDaily.setAccAmt(tmcMaps.get(key)!=null?
									PlaningTools.add(tdAccTrdDaily.getAccAmt(), tmcMaps.get(key).getRevAmt().doubleValue())
									:PlaningTools.add(tdAccTrdDaily.getAccAmt(),fund.getDividendAmt().doubleValue()));
							tdAccTrdDaily.setDealNo(fund.getDealNo());
							tdAccTrdDaily.setOpTime(fund.getvDate());
							tdAccTrdDaily.setPostDate(fund.getvDate());
							tdAccTrdDaily.setAccType(DurationConstants.AccType.REDEEM_FUND_AMT);
							//amtMap.put(DurationConstants.AccType.REDEEM_FUND_AMT,tdAccTrdDaily.getAccAmt());//放款会计事件
							amtMap.put(DurationConstants.AccType.REDEEM_FUND_AMTDIV,tmcMaps.get(key)!=null?tmcMaps.get(key).getRevAmt().doubleValue():0.00);//放款会计事件
						}
					break;
					case 7://基金分红
						//查询基金分红原交易
						cashdiv=tdDurationFundCashdivService.getTdDurationFundCashdiv(tdDuration);
						if(null !=cashdiv){
							tdAccTrdDaily.setAccAmt(cashdiv.getCashdivAmt().doubleValue());
							tdAccTrdDaily.setDealNo(cashdiv.getDealNo());
							tdAccTrdDaily.setOpTime(cashdiv.getvDate());
							tdAccTrdDaily.setPostDate(cashdiv.getvDate());
							tdAccTrdDaily.setAccType(DurationConstants.AccType.CASHDIV_FUND_AMT);
							
							//调用账务处理服务
							InstructionPackage inst = new InstructionPackage();
							amtMap.put(DurationConstants.AccType.CASHDIV_FUND_AMT,
									tmcMaps.get(key)!=null?tmcMaps.get(key).getRevAmt().doubleValue():
									cashdiv.getCashdivAmt());//放款会计事件
							
							Map<String,Object> infoMap  = BeanUtil.beanToMap(cashdiv);						
							infoMap.put("invtype",(infoMap.get("invType") == null) ? infoMap.get("invtype") : infoMap.get("invType"));
							inst.setInfoMap(infoMap);//业务信息
							inst.setAmtMap(amtMap);//金额信息
							tdAccTrdDaily.setAcctNo(bookkeepingService.generateEntry4Instruction(inst));
							accTrdDailyMapper.insert(tdAccTrdDaily);
							//基金持仓操作
							Map<String,Object> param = new HashMap<String, Object>();
							param.put("shAmt", cashdiv.getCashdivAmt());
							param.put("fundCode", cashdiv.getFundCode());
							param.put("postdate", cashdiv.gettDate());
							param.put("prdNo", cashdiv.getPrdNo());
							param.put("invtype", cashdiv.getInvType());
							param.put("opType", "2");//操作类型为分红
							param.put("ccy", cashdiv.getCcy());
							tdProductFundTposService.fundTposManager(param);
						}
						break;
					}
				}
				if(null != tdAccTrdDaily && null != fund){//基金赎回
					//调用账务处理服务
					InstructionPackage inst = new InstructionPackage();
					Map<String,Object> infoMap  = BeanUtil.beanToMap(fund);						
					infoMap.put("invtype",(infoMap.get("invType") == null) ? infoMap.get("invtype") : infoMap.get("invType"));
					inst.setInfoMap(infoMap);//业务信息
					inst.setAmtMap(amtMap);//金额信息
					
					tdAccTrdDaily.setAcctNo(bookkeepingService.generateEntry4Instruction(inst));
					accTrdDailyMapper.insert(tdAccTrdDaily);
					//基金持仓操作
					HashMap<String, Object> mapTp = new HashMap<String, Object>();
					mapTp.put("utAmt",fund.getRedeemAmt()); //赎回金额
					mapTp.put("utQty", fund.getRedeemQty());//赎回份额
					mapTp.put("shAmt", fund.getDividendAmt());//分红金额
					mapTp.put("fundCode", fund.getFundCode());//基金代码
					mapTp.put("opType", "1");//操作类型为赎回
					mapTp.put("prdNo", fund.getPrdNo());
					mapTp.put("invtype", fund.getInvType());
					mapTp.put("ccy", fund.getCcy());
					tdProductFundTposService.fundTposManager(mapTp);
				}
				
				return;
			}
			if(DictConstants.TrdType.CustomTradeOverDue.equals(trade.getTrdtype())){//逾期到期
				//本金+利息+逾期利息
				//根据 来账分拣的流水关联到原始 逾期交易号
				TdOverDueConfirm overDueConfirm = overDueConfirmService.queryOverDueDealByRefNo(tdOrdersortingApprove.getRefNo());
				//根据 overDueConfirm 的 refNo 获得原始交易
				HashMap<String, Object> mainMap = new HashMap<String, Object>();
				mainMap.put("dealNo", overDueConfirm.getRefNo());
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveForDuration(mainMap);
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = new HashMap<String, Object>();
				TdTrdTpos tpos =null;
				InstructionPackage instructionPackage = null;
				List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
				boolean flag = true;
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
					switch (keyNum) {
					case 5://费用
						flag = false;
						
						//根据
						accTrdDailies.clear();
						Map<String,Object> feeMap = new HashMap<String, Object>();
						feeMap.put("feeDealNo", key.substring(key.indexOf("-")));
						TdCashflowFee tdCashFlowFee = new TdCashflowFee();
						tdCashFlowFee.setCashflowId(key.substring(key.indexOf("-")+1,key.length()));
						if(!tdCashFlowFee.getCashflowId().startsWith("CF")){
							if(tdCashFlowFee.getCashflowId().startsWith("TRD"))
							{//手工补录
								TdFeeHandleCashflow feeHandleCashflow = feeHandleCashflowMapper.selectByPrimaryKey(tdCashFlowFee.getCashflowId());
								feeMap.put("feeDealNo", feeHandleCashflow.getDealNo());
								
								//构建账务过渡表
								accTrdDaily = new TdAccTrdDaily();
								//产生账务数据
								amtMap = new HashMap<String, Object>();
								//实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
								//判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
								accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()));
								accTrdDaily.setPostDate(postDate);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());
								feeHandleCashflow.setTrueRevDate(accTrdDaily.getPostDate());
								feeHandleCashflow.setTrueRevAmt(accTrdDaily.getAccAmt());
								feeHandleCashflowMapper.updateByPrimaryKey(feeHandleCashflow);
								accTrdDailies.add(accTrdDaily);
								instructionPackage = new InstructionPackage();
								productApproveMain.setIntFre(productApproveMain.getIntFre());
								instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
								amtMap.put(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()), accTrdDaily.getAccAmt());
								instructionPackage.setAmtMap(amtMap);
								
								String acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
								if(null != acctno && !"".equals(acctno))
								{//跟新回执会计套号
									for(int i = 0 ;i <accTrdDailies.size();i++)
									{
										accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
									}
								}
								batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
								break;
							}else{
								tdCashFlowFee = tdCashflowFeeMapper.selectByPrimaryKey(tdCashFlowFee);
								feeMap.put("feeDealNo", tdCashFlowFee.getFeeDealNo());
								
							}
						}else{
							feeMap.put("feeDealNo", tdCashFlowFee.getCashflowId());
						}
						feeMap.put("dealNo", productApproveMain.getDealNo());
						TdProductFeeDeal productFeeDeal = productFeeDealMapper.getTdProductFeeDealByFeeDealNo(feeMap);
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						//产生账务数据
						amtMap = new HashMap<String, Object>();
						//实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
						//判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
						accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()));
						accTrdDaily.setPostDate(postDate);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						//如果头寸计提 已经是负数，那么可以推断 511提前收了；
						//再收息，直接511做调整+收息金额，计提直接清0
						//如果计提是正数，那么需要判断   计提-收息值 >=0那么说明计提的多，511多计提了，需要冲减差额 计提也需要冲减差额，并转计提-407
						//计提-收息值 <0  计提减去收息金额  511追加差额
						if(productFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeAfter)){
							if(productFeeDeal.getAccruedTint()<=0){
								accTrdDaily.setAccAmt(0.00);//
								amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
								productFeeDeal.setAccruedTint(0.00);
								productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));
								productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
								//构建账务过渡表
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//费用调整
								accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
								accTrdDaily.setPostDate(postDate);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
							}else{
								double trueFeeAccruedAmt = PlaningTools.sub(productFeeDeal.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
								if(trueFeeAccruedAmt>=0){
									accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
									amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
									
									productFeeDeal.setAccruedTint(0.00);
									productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
									productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
									//构建账务过渡表
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//费用调整
									accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
									accTrdDaily.setPostDate(postDate);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
								}else{
									accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
									amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
//									amtMap.put(DurationConstants.AccType.FEE_AMT_ADJ, trueFeeAccruedAmt);
									accTrdDailies.add(accTrdDaily);
									
									productFeeDeal.setAccruedTint(0.0);
									productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
									productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
									
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//利息调整 销记差额部分
									accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
								}
								//更新tdCashFlowFee实际收款实际
								if(DictConstants.intType.chargeAfter.equalsIgnoreCase(productFeeDeal.getFeeCtype())){
									tdCashFlowFee.setActualDate(dayendDateService.getDayendDate());
									tdCashFlowFee.setActualRamt(trueFeeAccruedAmt);
									tdCashflowFeeMapper.updateByPrimaryKey(tdCashFlowFee);
								}
								
							}
						}else{
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//
							amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}
						instructionPackage = new InstructionPackage();
						productApproveMain.setIntFre(productFeeDeal.getFeeCfre());//将原交易的频率改为费用的
						Map<String, Object> beanMap = BeanUtil.beanToMap(productApproveMain);
						beanMap.put("intType", productFeeDeal.getFeeCtype());
						beanMap.put("feeType", productFeeDeal.getFeeType());
						instructionPackage.setInfoMap(beanMap);
						instructionPackage.setAmtMap(amtMap);
						String acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							}
						}
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
						break;
					case 3:
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//逾期罚息
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_OD_LATECHARGE);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						amtMap.put(DurationConstants.AccType.AMT_INTEREST_OD_LATECHARGE, accTrdDaily.getAccAmt());//会计-逾期罚息
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						accTrdDailies.add(accTrdDaily);
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						tpos.setPenaltyAmt(PlaningTools.add(tpos.getPenaltyAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//逾期罚息增加
						this.tposMapper.updateByPrimaryKey(tpos);
						break;
					case 1://本金
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL_OD);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL_OD, accTrdDaily.getAccAmt());//会计-本金
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						accTrdDailies.add(accTrdDaily);
						
						
						//更新TD_CASHFLOW_CAPITAL的还本实际日期和金额  在发生逾期确认时点已完成了更新0
//						TdCashflowCapital capital = new TdCashflowCapital();
//						capital.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
//						capital = tdCashflowCapitalMapper.selectByPrimaryKey(capital);
//						capital.setRepaymentTamt(tmcMaps.get(key).getRevAmt().doubleValue());
//						capital.setRepaymentTdate(dayendDateService.getDayendDate());
//						tdCashflowCapitalMapper.updateCashflowCapitalById(capital);
						//更新实际现金流表
						TdAmtRange amtRange = new TdAmtRange();
						amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
						amtRange.setAmtType("2");//违约还本
						amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						amtRange.setExecDate(dayendDateService.getDayendDate());
						amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
						amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
						amtRange.setVersion(0);
						amtRangeMapper.insert(amtRange);
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//剩余清算本金减少
						tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
						this.tposMapper.updateByPrimaryKey(tpos);
						//释放额度
						//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
						Map<String,Object> queryMaps = new HashMap<String, Object>();
						queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
						List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
						for(int i = 0; i<creditList.size();i++){
							creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
							if(creditList.get(i).getAmt()<0){
								creditList.get(i).setAmt(0);
							}
						}
						//代入额度操作服务
//						 暴力输出：1、只要是涉及到的客户，全部回归初始化  
//						 2、按照VDATE的顺序进行额度的占用
//						 3、保留最后操作的日志
//						 这样处理的好处：
						List<String> dealNos =  new ArrayList<String>();
						dealNos.add(productApproveMain.getDealNo());
						if(creditList.size()>0)
							edCustManangeService.eduOccpFlowService(creditList,dealNos);
						break;
					case 4://逾期利息
						accTrdDaily = new TdAccTrdDaily();
//						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_OD);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
//						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
//						amtMap.put(DurationConstants.AccType.AMT_INTEREST_OD, accTrdDaily.getAccAmt());//会计-利息
						accTrdDailies.add(accTrdDaily);
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						
						if(tpos.getOverdueAccruedTint()<=0){
							accTrdDaily.setAccAmt(0.00);
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_OD, 0.00);
							accTrdDailies.add(accTrdDaily);
							
							tpos.setOverdueAccruedTint(0.00);//应计利息180
							tpos.setOverdueActualTint(PlaningTools.add(tpos.getOverdueAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ_OD);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ_OD, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
							
							//利息180冲减值0，因为没有可以冲减的，直接补计514
							//intAccTrdDaily.setAccAmt(0.00);
							//amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);//会计-利息
							//accTrdDailies.add(intAccTrdDaily);
						}else{
							double trueAmt = PlaningTools.sub(tpos.getOverdueAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
							if(trueAmt>=0){
								//利息180冲减全额，514冲减差额
								accTrdDaily.setAccAmt(tpos.getOverdueAccruedTint());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_OD, accTrdDaily.getAccAmt());//会计-利息
								accTrdDailies.add(accTrdDaily);
								
								tpos.setOverdueAccruedTint(0.00);//应计利息180
								tpos.setOverdueActualTint(PlaningTools.add(tpos.getOverdueAccruedTint(), -trueAmt));//利息收入514 调整多余部分
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 销记差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ_OD);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ_OD, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}else{
								
								//利息180冲减全额，514补计差额
								accTrdDaily.setAccAmt(tpos.getOverdueAccruedTint());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_OD, tmcMaps.get(key).getRevAmt().doubleValue());//会计-利息
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_INTADJ_OD, trueAmt);//会计-利息
								accTrdDailies.add(accTrdDaily);
								
								tpos.setOverdueAccruedTint(0.00);//应计利息180
								tpos.setOverdueActualTint(PlaningTools.add(tpos.getOverdueAccruedTint(), -trueAmt));//利息收入514补计少收部分
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ_OD, accTrdDaily.getAccAmt());
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								accTrdDailies.add(accTrdDaily);
								
							}
						}
						
						this.tposMapper.updateByPrimaryKey(tpos);
						break;
					case 2://利息
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());//会计-利息
						accTrdDailies.add(accTrdDaily);
						//更新TD_CASHFLOW_INTEREST的收息实际日期和金额  此处无法判别 收息现金流;因为逾期了，由逾期去结算
//						TdCashflowInterest cashflowInterest = new TdCashflowInterest();
//						cashflowInterest.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
//						cashflowInterest = tdCashflowInterestMapper.selectByPrimaryKey(cashflowInterest);
//						cashflowInterest.setActualRamt(tmcMaps.get(key).getRevAmt().doubleValue());
//						cashflowInterest.setActualDate(dayendDateService.getDayendDate());
//						tdCashflowInterestMapper.updateCashflowInterestById(cashflowInterest);
						
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
//						 * 如果180 已经是负数，那么可以推断 514提前收了；
//						 * 再收息，直接514做调整+收息金额，180直接清0
//						 * 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514多计提了，需要冲减差额 180也需要冲减差额，并转180-407
//						 *  180-收息值 <0  180减去收息金额  514追加差额
						if(tpos.getAccruedTint()<=0){
							accTrdDaily.setAccAmt(0.00);
							amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);
							accTrdDailies.add(accTrdDaily);
							
							tpos.setAccruedTint(0.00);//应计利息180
							tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
							
							//利息180冲减值0，因为没有可以冲减的，直接补计514
							//intAccTrdDaily.setAccAmt(0.00);
							//amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);//会计-利息
							//accTrdDailies.add(intAccTrdDaily);
						}else{
							double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
							if(trueAmt>=0){
								//利息180冲减全额，514冲减差额
								accTrdDaily.setAccAmt(tpos.getAccruedTint());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());//会计-利息
								accTrdDailies.add(accTrdDaily);
								
								tpos.setAccruedTint(0.00);//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514 调整多余部分
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 销记差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}else{
								
								//利息180冲减全额，514补计差额
								accTrdDaily.setAccAmt(tpos.getAccruedTint());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST, tmcMaps.get(key).getRevAmt().doubleValue());//会计-利息
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_INTADJ, trueAmt);//会计-利息
								accTrdDailies.add(accTrdDaily);
								
								tpos.setAccruedTint(0.00);//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514补计少收部分
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								accTrdDailies.add(accTrdDaily);
								
							}
						}
						this.tposMapper.updateByPrimaryKey(tpos);
						break;
					default:
						break;
					}
				}
				if(flag){
					Map<String, Object> infoMap = new HashMap<String, Object>();
					infoMap = BeanUtil.beanToMap(productApproveMain);
					infoMap.put("overdueDays", productApproveMain.getTpos().getOverdueDays());
					//产生账务数据
					instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(infoMap);
					instructionPackage.setAmtMap(amtMap);//本金+利息+调整
					String acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						for(int i = 0 ;i <accTrdDailies.size();i++)
						{
							accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							accTrdDailies.get(i).setPostDate(dayendDateService.getDayendDate());
						}
					}
					batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
				}
				
			}
			if(DictConstants.TrdType.Custom.equals(trade.getTrdtype())){//正常到期
				//正常收付息交易  本金或利息(单独收取的，要么还本，要么收息）非合并收取
				if(tmcMaps.size()==1){
					//TdAdvanceMaturity advanceMaturity = advanceMaturityService.getAdvanceMaturityById(tdOrdersortingApprove.getRefNo());
					TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(trade.getTrade_id());
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = null;String acctno = "";
					InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
					List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						String refType = key.substring(0, key.indexOf("-"));
						if(refType.equals("999")){
							break;
						}
						int optype = 0;
						if(StringUtils.isNumeric(refType)){
							optype = Integer.parseInt(refType);
						}else{
							optype = 5;
						}
						switch (optype) {
						case 5://费用
							//根据
							//根据
							accTrdDailies.clear();
							Map<String,Object> feeMap = new HashMap<String, Object>();
							feeMap.put("feeDealNo", key.substring(key.indexOf("-")));
							TdCashflowFee tdCashFlowFee = new TdCashflowFee();
							tdCashFlowFee.setCashflowId(key.substring(key.indexOf("-")+1,key.length()));
							if(!tdCashFlowFee.getCashflowId().startsWith("CF")){
								if(tdCashFlowFee.getCashflowId().startsWith("TRD"))
								{//手工补录
									TdFeeHandleCashflow feeHandleCashflow = feeHandleCashflowMapper.selectByPrimaryKey(tdCashFlowFee.getCashflowId());
									feeMap.put("feeDealNo", feeHandleCashflow.getDealNo());
									
									//构建账务过渡表
									accTrdDaily = new TdAccTrdDaily();
									//产生账务数据
									amtMap = new HashMap<String, Object>();
//									 * 实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
//									 * 判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
									accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()));
									accTrdDaily.setPostDate(postDate);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());
									feeHandleCashflow.setTrueRevDate(accTrdDaily.getPostDate());
									feeHandleCashflow.setTrueRevAmt(accTrdDaily.getAccAmt());
									feeHandleCashflowMapper.updateByPrimaryKey(feeHandleCashflow);
									accTrdDailies.add(accTrdDaily);
									instructionPackage = new InstructionPackage();
									productApproveMain.setIntFre(productApproveMain.getIntFre());
									instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
									amtMap.put(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()), accTrdDaily.getAccAmt());
									instructionPackage.setAmtMap(amtMap);
									
									acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
									if(null != acctno && !"".equals(acctno))
									{//跟新回执会计套号
										for(int i = 0 ;i <accTrdDailies.size();i++)
										{
											accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
										}
									}
									batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
									break;
								}else{
									tdCashFlowFee = tdCashflowFeeMapper.selectByPrimaryKey(tdCashFlowFee);
									feeMap.put("feeDealNo", tdCashFlowFee.getFeeDealNo());
									
								}
							}else{
								feeMap.put("feeDealNo", tdCashFlowFee.getCashflowId());
							}
							feeMap.put("dealNo", productApproveMain.getDealNo());
							TdProductFeeDeal productFeeDeal = productFeeDealMapper.getTdProductFeeDealByFeeDealNo(feeMap);
							//构建账务过渡表
							accTrdDaily = new TdAccTrdDaily();
							//产生账务数据
							amtMap = new HashMap<String, Object>();
//							 * 实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
//							 * 判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
							accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()));
							accTrdDaily.setPostDate(postDate);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
//							 * 如果头寸计提 已经是负数，那么可以推断 511提前收了；
//							 * 再收息，直接511做调整+收息金额，计提直接清0
//							 * 如果计提是正数，那么需要判断   计提-收息值 >=0那么说明计提的多，511多计提了，需要冲减差额 计提也需要冲减差额，并转计提-407
//							 *  计提-收息值 <0  计提减去收息金额  511追加差额
							if(productFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeAfter)){
								if(productFeeDeal.getAccruedTint()<=0){
									accTrdDaily.setAccAmt(0.00);//
									amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
									
									productFeeDeal.setAccruedTint(0.00);
									productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));
									productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
									//构建账务过渡表
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//费用调整
									accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
									accTrdDaily.setPostDate(postDate);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
									
								}else{
									double trueFeeAccruedAmt = PlaningTools.sub(productFeeDeal.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
									if(trueFeeAccruedAmt>=0){
										accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
										amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
										
										productFeeDeal.setAccruedTint(0.00);
										productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
										productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
										//构建账务过渡表
										accTrdDaily = new TdAccTrdDaily();
										accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//费用调整
										accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
										accTrdDaily.setPostDate(postDate);
										accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
										accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
										accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
										accTrdDaily.setPostDate(dayendDateService.getDayendDate());
										amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
									}else{
										accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
										amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
	//									amtMap.put(DurationConstants.AccType.FEE_AMT_ADJ, trueFeeAccruedAmt);
										accTrdDailies.add(accTrdDaily);
										
										productFeeDeal.setAccruedTint(0.0);
										productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
										productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
										
										accTrdDaily = new TdAccTrdDaily();
										accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//利息调整 销记差额部分
										accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
										accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
										accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
										accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
										accTrdDaily.setPostDate(dayendDateService.getDayendDate());
										amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
									}
									//更新tdCashFlowFee实际收款实际
									if(DictConstants.intType.chargeAfter.equalsIgnoreCase(productFeeDeal.getFeeCtype())){
										tdCashFlowFee.setActualDate(dayendDateService.getDayendDate());
										tdCashFlowFee.setActualRamt(trueFeeAccruedAmt);
										tdCashflowFeeMapper.updateByPrimaryKey(tdCashFlowFee);
									}
									
								}
							}else{
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//
								amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							instructionPackage = new InstructionPackage();
							productApproveMain.setIntFre(productFeeDeal.getFeeCfre());//将原交易的频率改为费用的
							Map<String, Object> beanMap = BeanUtil.beanToMap(productApproveMain);
							beanMap.put("intType", productFeeDeal.getFeeCtype());
							beanMap.put("feeType", productFeeDeal.getFeeType());
							instructionPackage.setInfoMap(beanMap);
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != flowId && !"".equals(flowId))
							{//跟新回执会计套号
								for(int i = 0 ;i <accTrdDailies.size();i++)
								{
									accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
								}
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
							break;
						case 1://本金
							//构建账务过渡表
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
							accTrdDaily.setPostDate(postDate);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							
							//产生账务数据
							amtMap = new HashMap<String, Object>();
							amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());
							
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != flowId && !"".equals(flowId))
							{//跟新回执会计套号
								accTrdDaily.setAcctNo(acctno);//关联账务数据
							}
							accTrdDailyMapper.insert(accTrdDaily);
							//更新TD_CASHFLOW_CAPITAL的还本实际日期和金额
							TdCashflowCapital capital = new TdCashflowCapital();
							capital.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
							capital = tdCashflowCapitalMapper.selectByPrimaryKey(capital);
							capital.setRepaymentTamt(tmcMaps.get(key).getRevAmt().doubleValue());
							capital.setRepaymentTdate(dayendDateService.getDayendDate());
							tdCashflowCapitalMapper.updateCashflowCapitalById(capital);
							//更新实际现金流表
							TdAmtRange amtRange = new TdAmtRange();
							amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
							amtRange.setAmtType("1");//正常还本
							amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
							amtRange.setExecDate(dayendDateService.getDayendDate());
							amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
							amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
							amtRange.setVersion(0);
							amtRangeMapper.insert(amtRange);
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
							tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//剩余清算本金减少
							tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
							this.tposMapper.updateByPrimaryKey(tpos);
							
							//释放额度
							//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
							Map<String,Object> queryMaps = new HashMap<String, Object>();
							queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
							List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
							for(int i = 0; i<creditList.size();i++){
								creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
								if(creditList.get(i).getAmt()<0){
									creditList.get(i).setAmt(0);
								}
							}
							//代入额度操作服务
//							 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
//							 * 3、保留最后操作的日志
//							 * 这样处理的好处：
							List<String> dealNos =  new ArrayList<String>();
							dealNos.add(productApproveMain.getDealNo());
							if(creditList.size()>0)
								edCustManangeService.eduOccpFlowService(creditList,dealNos);
							break;
						case 2://利息
							accTrdDailies.clear();
							amtMap = new HashMap<String, Object>();
							//构建账务过渡表
							TdAccTrdDaily intAccTrdDaily = new TdAccTrdDaily();//以实际收款-已计提累计作为基准
							intAccTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
							intAccTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							intAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							intAccTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							intAccTrdDaily.setPostDate(dayendDateService.getDayendDate());
							
							//更新TD_CASHFLOW_INTEREST的收息实际日期和金额
							//if(tmcMaps.get(key).getRefNo() != null){
								TdCashflowInterest cashflowInterest = new TdCashflowInterest();
								cashflowInterest.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
								cashflowInterest = tdCashflowInterestMapper.selectByPrimaryKey(cashflowInterest);
								cashflowInterest.setActualRamt(tmcMaps.get(key).getRevAmt().doubleValue());
								cashflowInterest.setActualDate(dayendDateService.getDayendDate());
								tdCashflowInterestMapper.updateCashflowInterestById(cashflowInterest);
							//}
							
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
//							 * 如果180 已经是负数，那么可以推断 514提前收了；
//							 * 再收息，直接514做调整+收息金额，180直接清0
//							 * 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514多计提了，需要冲减差额 180也需要冲减差额，并转180-407
//							 *  180-收息值 <0  180减去收息金额  514追加差额
							if(tpos.getAccruedTint()<=0){
								intAccTrdDaily.setAccAmt(0.00);
								amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);
								accTrdDailies.add(intAccTrdDaily);
								
								tpos.setAccruedTint(0.00);//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
								//利息180冲减值0，因为没有可以冲减的，直接补计514
								//intAccTrdDaily.setAccAmt(0.00);
								//amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);//会计-利息
								//accTrdDailies.add(intAccTrdDaily);
							}else{
								double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
								if(trueAmt>=0){
									//利息180冲减全额，514冲减差额
									intAccTrdDaily.setAccAmt(tpos.getAccruedTint());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST, intAccTrdDaily.getAccAmt());//会计-利息
									accTrdDailies.add(intAccTrdDaily);
									
									tpos.setAccruedTint(0.00);//应计利息180
									tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514 调整多余部分
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueAmt);//利息调整 销记差额部分
									accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
								}else{
									
									//利息180冲减全额，514补计差额
									intAccTrdDaily.setAccAmt(tpos.getAccruedTint());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST, tmcMaps.get(key).getRevAmt().doubleValue());//会计-利息
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_INTADJ, trueAmt);//会计-利息
									accTrdDailies.add(intAccTrdDaily);
									
									tpos.setAccruedTint(0.00);//应计利息180
									tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514补计少收部分
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
									accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									accTrdDailies.add(accTrdDaily);
									
								}
								
							}
							this.tposMapper.updateByPrimaryKey(tpos);
							//产生账务数据
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != flowId && !"".equals(flowId))
							{//跟新回执会计套号
								for(int i = 0 ;i <accTrdDailies.size();i++)
								{
									accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
								}
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
							break;//注意：这里的180可能无法清零，需要日终再次调整；  因为到期是可以分开收息和收本的
						default:
							break;
						}
					}
				}else {
					//到期收本金+利息部分
					TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(trade.getTrade_id());
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = new HashMap<String, Object>();;String acctno = "";
					InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
					List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
					boolean flag = true;
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						String refType = key.substring(0, key.indexOf("-"));
						if(refType.equals("999")){
							break;
						}
						int optype = 0;
						if(StringUtils.isNumeric(refType)){
							optype = Integer.parseInt(refType);
						}else{
							optype = 5;
						}
						switch (optype) {
						case 5://费用
							flag = false;
							
							//根据
							accTrdDailies.clear();
							Map<String,Object> feeMap = new HashMap<String, Object>();
							feeMap.put("feeDealNo", key.substring(key.indexOf("-")));
							TdCashflowFee tdCashFlowFee = new TdCashflowFee();
							tdCashFlowFee.setCashflowId(key.substring(key.indexOf("-")+1,key.length()));
							if(!tdCashFlowFee.getCashflowId().startsWith("CF")){
								if(tdCashFlowFee.getCashflowId().startsWith("TRD"))
								{//手工补录
									TdFeeHandleCashflow feeHandleCashflow = feeHandleCashflowMapper.selectByPrimaryKey(tdCashFlowFee.getCashflowId());
									feeMap.put("feeDealNo", feeHandleCashflow.getDealNo());
									
									//构建账务过渡表
									accTrdDaily = new TdAccTrdDaily();
									//产生账务数据
									amtMap = new HashMap<String, Object>();
//									 * 实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
//									 * 判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
									accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()));
									accTrdDaily.setPostDate(postDate);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());
									feeHandleCashflow.setTrueRevDate(accTrdDaily.getPostDate());
									feeHandleCashflow.setTrueRevAmt(accTrdDaily.getAccAmt());
									feeHandleCashflowMapper.updateByPrimaryKey(feeHandleCashflow);
									accTrdDailies.add(accTrdDaily);
									instructionPackage = new InstructionPackage();
									productApproveMain.setIntFre(productApproveMain.getIntFre());
									instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
									amtMap.put(DurationConstants.dealFeeTypeMap.get(feeHandleCashflow.getFeeType()), accTrdDaily.getAccAmt());
									instructionPackage.setAmtMap(amtMap);
									
									acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
									if(null != acctno && !"".equals(acctno))
									{//跟新回执会计套号
										for(int i = 0 ;i <accTrdDailies.size();i++)
										{
											accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
										}
									}
									batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
									break;
								}else{
									tdCashFlowFee = tdCashflowFeeMapper.selectByPrimaryKey(tdCashFlowFee);
									feeMap.put("feeDealNo", tdCashFlowFee.getFeeDealNo());
									
								}
							}else{
								feeMap.put("feeDealNo", tdCashFlowFee.getCashflowId());
							}
							feeMap.put("dealNo", productApproveMain.getDealNo());
							TdProductFeeDeal productFeeDeal = productFeeDealMapper.getTdProductFeeDealByFeeDealNo(feeMap);
							//构建账务过渡表
							accTrdDaily = new TdAccTrdDaily();
							//产生账务数据
							amtMap = new HashMap<String, Object>();
//							 * 实收实付制  判定交易的每日计提累计多 则冲销，少则以实收为准
//							 * 判断当前计提是否超过实收金额，如果是，那么需要冲销多余部分
							accTrdDaily.setAccType(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()));
							accTrdDaily.setPostDate(postDate);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
//							 * 如果头寸计提 已经是负数，那么可以推断 511提前收了；
//							 * 再收息，直接511做调整+收息金额，计提直接清0
//							 * 如果计提是正数，那么需要判断   计提-收息值 >=0那么说明计提的多，511多计提了，需要冲减差额 计提也需要冲减差额，并转计提-407
//							 *  计提-收息值 <0  计提减去收息金额  511追加差额
							if(productFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeAfter)){
								if(productFeeDeal.getAccruedTint()<=0){
									accTrdDaily.setAccAmt(0.00);//
									amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
									
									productFeeDeal.setAccruedTint(0.00);
									productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));
									productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
									//构建账务过渡表
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//费用调整
									accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
									accTrdDaily.setPostDate(postDate);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
									
								}else{
									double trueFeeAccruedAmt = PlaningTools.sub(productFeeDeal.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
									if(trueFeeAccruedAmt>=0){
										accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
										amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
										
										productFeeDeal.setAccruedTint(0.00);
										productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
										productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
										//构建账务过渡表
										accTrdDaily = new TdAccTrdDaily();
										accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//费用调整
										accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
										accTrdDaily.setPostDate(postDate);
										accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
										accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
										accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
										accTrdDaily.setPostDate(dayendDateService.getDayendDate());
										amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
									}else{
										accTrdDaily.setAccAmt(productFeeDeal.getAccruedTint());//
										amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
	//									amtMap.put(DurationConstants.AccType.FEE_AMT_ADJ, trueFeeAccruedAmt);
										accTrdDailies.add(accTrdDaily);
										
										productFeeDeal.setAccruedTint(0.0);
										productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), -trueFeeAccruedAmt));
										productFeeDealMapper.updateByPrimaryKey(productFeeDeal);
										
										accTrdDaily = new TdAccTrdDaily();
										accTrdDaily.setAccAmt(-trueFeeAccruedAmt);//利息调整 销记差额部分
										accTrdDaily.setAccType(DurationConstants.AccType.FEE_AMT_INTADJ);
										accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
										accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
										accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
										accTrdDaily.setPostDate(dayendDateService.getDayendDate());
										amtMap.put(DurationConstants.AccType.FEE_AMT_INTADJ, accTrdDaily.getAccAmt());
										accTrdDailies.add(accTrdDaily);
									}
									//更新tdCashFlowFee实际收款实际
									if(DictConstants.intType.chargeAfter.equalsIgnoreCase(productFeeDeal.getFeeCtype())){
										tdCashFlowFee.setActualDate(dayendDateService.getDayendDate());
										tdCashFlowFee.setActualRamt(trueFeeAccruedAmt);
										tdCashflowFeeMapper.updateByPrimaryKey(tdCashFlowFee);
									}
									
								}
							}else{
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//
								amtMap.put(DurationConstants.dealFeeTypeMap.get(productFeeDeal.getFeeType()), accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							instructionPackage = new InstructionPackage();
							productApproveMain.setIntFre(productFeeDeal.getFeeCfre());//将原交易的频率改为费用的
							Map<String, Object> beanMap = BeanUtil.beanToMap(productApproveMain);
							beanMap.put("intType", productFeeDeal.getFeeCtype());
							beanMap.put("feeType", productFeeDeal.getFeeType());
							instructionPackage.setInfoMap(beanMap);
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != flowId && !"".equals(flowId))
							{//跟新回执会计套号
								for(int i = 0 ;i <accTrdDailies.size();i++)
								{
									accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
								}
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
							break;
						case 1://本金
							//构建账务过渡表
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());//会计-本金
							accTrdDailies.add(accTrdDaily);
							
							
							//更新TD_CASHFLOW_CAPITAL的还本实际日期和金额
							//if(tmcMaps.get(key).getRefNo() != null){
								TdCashflowCapital capital = new TdCashflowCapital();
								capital.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
								capital = tdCashflowCapitalMapper.selectByPrimaryKey(capital);
								capital.setRepaymentTamt(tmcMaps.get(key).getRevAmt().doubleValue());
								capital.setRepaymentTdate(dayendDateService.getDayendDate());
								capital.setCfEvent("");
								tdCashflowCapitalMapper.updateCashflowCapitalById(capital);
							//}
							//更新实际现金流表
							TdAmtRange amtRange = new TdAmtRange();
							amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
							amtRange.setAmtType("1");//正常还本
							amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
							amtRange.setExecDate(dayendDateService.getDayendDate());
							amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
							amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
							amtRange.setVersion(0);
							amtRangeMapper.insert(amtRange);
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
							tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//剩余清算本金减少
							tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
							this.tposMapper.updateByPrimaryKey(tpos);
							//释放额度
							//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
							Map<String,Object> queryMaps = new HashMap<String, Object>();
							queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
							List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
							for(int i = 0; i<creditList.size();i++){
								creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
								if(creditList.get(i).getAmt()<0){
									creditList.get(i).setAmt(0);
								}
							}
							//代入额度操作服务
//							 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
//							 * 3、保留最后操作的日志
//							 * 这样处理的好处：
							List<String> dealNos =  new ArrayList<String>();
							dealNos.add(productApproveMain.getDealNo());
							if(creditList.size()>0)
								edCustManangeService.eduOccpFlowService(creditList,dealNos);
							
							break;
						case 2://利息
							//构建账务过渡表
							TdAccTrdDaily intAccTrdDaily = new TdAccTrdDaily();//以实际收款-已计提累计作为基准
							intAccTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
							intAccTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							intAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							intAccTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							intAccTrdDaily.setPostDate(dayendDateService.getDayendDate());
							
							//更新TD_CASHFLOW_INTEREST的收息实际日期和金额
							//if(tmcMaps.get(key).getRefNo() != null){
								TdCashflowInterest cashflowInterest = new TdCashflowInterest();
								cashflowInterest.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
								cashflowInterest = tdCashflowInterestMapper.selectByPrimaryKey(cashflowInterest);
								cashflowInterest.setActualRamt(tmcMaps.get(key).getRevAmt().doubleValue());
								cashflowInterest.setActualDate(dayendDateService.getDayendDate());
								tdCashflowInterestMapper.updateCashflowInterestById(cashflowInterest);
							//}
							
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
//							 * 如果180 已经是负数，那么可以推断 514提前收了；
//							 * 再收息，直接514做调整+收息金额，180直接清0
//							 * 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514多计提了，需要冲减差额 180也需要冲减差额，并转180-407
//							 *  180-收息值 <0  180减去收息金额  514追加差额
							if(tpos.getAccruedTint()<=0){
								intAccTrdDaily.setAccAmt(0.00);
								amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);
								accTrdDailies.add(intAccTrdDaily);
								
								tpos.setAccruedTint(0.00);//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
								//利息180冲减值0，因为没有可以冲减的，直接补计514
								//intAccTrdDaily.setAccAmt(0.00);
								//amtMap.put(DurationConstants.AccType.AMT_INTEREST, 0.00);//会计-利息
								//accTrdDailies.add(intAccTrdDaily);
							}else{
								double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
								if(trueAmt>=0){
									//利息180冲减全额，514冲减差额
									intAccTrdDaily.setAccAmt(tpos.getAccruedTint());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST, intAccTrdDaily.getAccAmt());//会计-利息
									accTrdDailies.add(intAccTrdDaily);
									
									tpos.setAccruedTint(0.00);//应计利息180
									tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514 调整多余部分
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueAmt);//利息调整 销记差额部分
									accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
									accTrdDailies.add(accTrdDaily);
								}else{
									
									//利息180冲减全额，514补计差额
									intAccTrdDaily.setAccAmt(tpos.getAccruedTint());
									amtMap.put(DurationConstants.AccType.AMT_INTEREST, tmcMaps.get(key).getRevAmt().doubleValue());//会计-利息
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_INTADJ, trueAmt);//会计-利息
									accTrdDailies.add(intAccTrdDaily);
									
									tpos.setAccruedTint(0.00);//应计利息180
									tpos.setActualTint(PlaningTools.add(tpos.getAccruedTint(), -trueAmt));//利息收入514补计少收部分
									accTrdDaily = new TdAccTrdDaily();
									accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
									accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
									accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
									accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
									accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
									amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
									accTrdDaily.setPostDate(dayendDateService.getDayendDate());
									accTrdDailies.add(accTrdDaily);
									
								}
								
							}
							this.tposMapper.updateByPrimaryKey(tpos);
							break;
						default:
							break;
						}
					}
					if(flag){
						//产生账务数据
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);//本金+利息+调整
						acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
								accTrdDailies.get(i).setPostDate(dayendDateService.getDayendDate());
							}
						}
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
					}
				}
			}
			if(DictConstants.TrdType.CustomInterestSettleConfirm.equals(trade.getTrdtype())){
				TdInterestSettle settl = interestSettleService.getInterestSettleById(tdOrdersortingApprove.getcDealNo());
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(settl.getRefNo());
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = new HashMap<String, Object>();;String acctno = "";
				InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
				List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					String refType = key.substring(0, key.indexOf("-"));
					int optype = 0;
					if(StringUtils.isNumeric(refType)){
						optype = Integer.parseInt(refType);
					}else{
						optype = 5;
					}
					switch (optype) {
						case 10://活期提回结息
							accTrdDailies.clear();
							amtMap = new HashMap<String, Object>();
							//构建账务过渡表
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INT);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							
							TdInterestSettle tdInterestSettle = tdInterestSettleMapper.getInterestSettleById(tmcMaps.get(key).getRefNo());
							double intadj = PlaningTools.sub(tdInterestSettle.getInterestSettlAmt(),tmcMaps.get(key).getRevAmt().doubleValue());
							if(intadj >= 0){
								accTrdDaily.setAccAmt(tdInterestSettle.getInterestSettlAmt());//结息金额
								amtMap.put(DurationConstants.AccType.INCOME_INT, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
								//结息金额与账务日期已计提总额的差额
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INTADJ);
								accTrdDaily.setAccAmt(-intadj);//活期收息结息调整
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.INCOME_INTADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}else{
								accTrdDaily.setAccAmt(tdInterestSettle.getInterestSettlAmt());//结息金额
								amtMap.put(DurationConstants.AccType.INCOME_INT, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
								
								//结息金额与账务日期已计提总额的差额
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INTADJ);
								accTrdDaily.setAccAmt(-intadj);//活期收息结息调整
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.INCOME_INTADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
							tpos.setInterestSettleAmt(0);//当期结息金额为0
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tdInterestSettle.getSettlIntadj()));//累计每日利息
							this.tposMapper.updateByPrimaryKey(tpos);
							//产生账务数据
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != flowId && !"".equals(flowId))
							{//跟新回执会计套号
								for(int i = 0 ;i <accTrdDailies.size();i++)
								{
									accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
								}
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
							break;
						default:
							break;
					}
				}
			}
			if(DictConstants.TrdType.CustomAdvanceMaturity.equals(trade.getTrdtype()))//提前到期
			{
				//根据提前到期交易号查询提前到期交易
				TdAdvanceMaturity advanceMaturity = advanceMaturityService.getAdvanceMaturityById(trade.getTrade_id());
				Map<String, Object> amtParams  =  new HashMap<String, Object>();
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(advanceMaturity.getRefNo());
				if(advanceMaturity.getIntType().equalsIgnoreCase(DictConstants.intType.chargeAfter))//后收息
				{
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
						switch (keyNum) {
						case 1://提前还款本金
							amtParams.put(DurationConstants.AccType.AM_AMT, tmcMaps.get(key).getRevAmt());
							//释放额度
							//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
							Map<String,Object> queryMaps = new HashMap<String, Object>();
							queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
							List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
							for(int i = 0; i<creditList.size();i++){
								creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
								if(creditList.get(i).getAmt()<0){
									creditList.get(i).setAmt(0);
								}
							}
							//代入额度操作服务
//							 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
//							 * 3、保留最后操作的日志
//							 * 这样处理的好处：
							List<String> dealNos =  new ArrayList<String>();
							dealNos.add(productApproveMain.getDealNo());
							if(creditList.size()>0)
								edCustManangeService.eduOccpFlowService(creditList,dealNos);
							break;
						case 2://提前还款利息
							amtParams.put(DurationConstants.AccType.AM_INT, tmcMaps.get(key).getRevAmt());
							break;
						case 9://提前还款违约金
							amtParams.put(DurationConstants.AccType.AM_PENALTY, tmcMaps.get(key).getRevAmt());
							break;
						default:
							break;
						}
					}
				}else
				{
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
						switch (keyNum) {
						case 1://提前还款本金
							amtParams.put(DurationConstants.AccType.AM_AMT, tmcMaps.get(key).getRevAmt());
							break;
						case 2://提前还款归还利息
							amtParams.put(DurationConstants.AccType.AM_RETURN_IAMT, tmcMaps.get(key).getRevAmt());
							break;
						default:
							break;
						}
					}
				}
				//账务处理  TPOS更新  
				accessService.getNeedForAccAdvanceMaturity(advanceMaturity.getDealNo(), advanceMaturity.getRefNo(), amtParams);
				//更新实际现金流表
				TdAmtRange amtRange = new TdAmtRange();
				amtRange.setDealNo(advanceMaturity.getRefNo());
				amtRange.setAmtType("2");//提前还本
				amtRange.setExecAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt()));
				amtRange.setExecDate(dayendDateService.getDayendDate());
				amtRange.setRefNo(advanceMaturity.getDealNo());
				amtRange.setRefTable("TD_ADVANCE_MATURITY");
				amtRange.setVersion(advanceMaturity.getVersion());
				amtRangeMapper.insert(amtRange);
				}
				if(DictConstants.TrdType.CustomAdvanceMaturityCurrent.equals(trade.getTrdtype())){

					//根据提前到期交易号查询提前到期活期交易
					TdAdvanceMaturityCurrent advanceMaturityCurrent = advanceMaturityCurrentService.getAdvanceMaturityCurrentById(trade.getTrade_id());
					Map<String, Object> amtParams  =  new HashMap<String, Object>();
					TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
						switch (keyNum) {
						case 1://提前还款本金
							amtParams.put(DurationConstants.AccType.AM_AMT, tmcMaps.get(key).getRevAmt());
							//释放额度
							//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
							Map<String,Object> queryMaps = new HashMap<String, Object>();
							queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
							List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
							for(int i = 0; i<creditList.size();i++){
								creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
								if(creditList.get(i).getAmt()<0){
									creditList.get(i).setAmt(0);
								}
							}
							//代入额度操作服务
//							 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
//							 * 3、保留最后操作的日志
//							 * 这样处理的好处：
							List<String> dealNos =  new ArrayList<String>();
							dealNos.add(productApproveMain.getDealNo());
							if(creditList.size()>0)
								edCustManangeService.eduOccpFlowService(creditList,dealNos);
							break;
						case 2://提前还款利息
							amtParams.put(DurationConstants.AccType.AM_INT, tmcMaps.get(key).getRevAmt());
							break;
						case 9://提前还款罚息
							amtParams.put(DurationConstants.AccType.AM_PENALTY, tmcMaps.get(key).getRevAmt());
							break;
						default:
							break;
						}
					}
					//账务处理  TPOS更新  
					accessService.getNeedForAccAdvanceMaturityCurrent(advanceMaturityCurrent.getDealNo(), advanceMaturityCurrent.getRefNo(), amtParams);
					//更新实际现金流表
					TdAmtRange amtRange = new TdAmtRange();
					amtRange.setDealNo(advanceMaturityCurrent.getRefNo());
					amtRange.setAmtType("2");//提前还本
					amtRange.setExecAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturityCurrent.getAmAmt()));
					amtRange.setExecDate(dayendDateService.getDayendDate());
					amtRange.setRefNo(advanceMaturityCurrent.getDealNo());
					amtRange.setRefTable("TD_ADVANCE_MATURITY_CURRENT");
					amtRange.setVersion(advanceMaturityCurrent.getVersion());
					amtRangeMapper.insert(amtRange);
					
				}if(DictConstants.TrdType.CustomAdvanceMaturityCurrent.equals(trade.getTrdtype())){

					//根据提前到期交易号查询提前到期活期交易
					TdAdvanceMaturityCurrent advanceMaturityCurrent = advanceMaturityCurrentService.getAdvanceMaturityCurrentById(trade.getTrade_id());
					Map<String, Object> amtParams  =  new HashMap<String, Object>();
					TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
					Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
					while(keyiIterable.hasNext()){
						String key = keyiIterable.next();
						int keyNum =Integer.parseInt(key.substring(0, key.indexOf("-")));
						switch (keyNum) {
						case 1://提前还款本金
							amtParams.put(DurationConstants.AccType.AM_AMT, tmcMaps.get(key).getRevAmt());
							//释放额度
							//查询所有需要进行额度占用的记录  目前释放一般是1笔一释放
							Map<String,Object> queryMaps = new HashMap<String, Object>();
							queryMaps.put("dealNo", productApproveMain.getDealNo());//原交易单
							List<EdInParams> creditList = edCustBatchStatusMapper.getTdEdInParamsByDealNo2(queryMaps);
							for(int i = 0; i<creditList.size();i++){
								creditList.get(i).setAmt(PlaningTools.sub(creditList.get(i).getAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));
								if(creditList.get(i).getAmt()<0){
									creditList.get(i).setAmt(0);
								}
							}
							//代入额度操作服务
//							 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
//							 * 3、保留最后操作的日志
//							 * 这样处理的好处：
							List<String> dealNos =  new ArrayList<String>();
							dealNos.add(productApproveMain.getDealNo());
							if(creditList.size()>0)
								edCustManangeService.eduOccpFlowService(creditList,dealNos);
							break;
						case 2://提前还款利息
							amtParams.put(DurationConstants.AccType.AM_INT, tmcMaps.get(key).getRevAmt());
							break;
						case 9://提前还款罚息
							amtParams.put(DurationConstants.AccType.AM_PENALTY, tmcMaps.get(key).getRevAmt());
							break;
						default:
							break;
						}
					}
					//账务处理  TPOS更新  
					accessService.getNeedForAccAdvanceMaturityCurrent(advanceMaturityCurrent.getDealNo(), advanceMaturityCurrent.getRefNo(), amtParams);
					//更新实际现金流表
					TdAmtRange amtRange = new TdAmtRange();
					amtRange.setDealNo(advanceMaturityCurrent.getRefNo());
					amtRange.setAmtType("2");//提前还本
					amtRange.setExecAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturityCurrent.getAmAmt()));
					amtRange.setExecDate(dayendDateService.getDayendDate());
					amtRange.setRefNo(advanceMaturityCurrent.getDealNo());
					amtRange.setRefTable("TD_ADVANCE_MATURITY_CURRENT");
					amtRange.setVersion(advanceMaturityCurrent.getVersion());
					amtRangeMapper.insert(amtRange);
					
				}
			} catch (Exception e) {
				JY.error(e.getMessage());
				throw new RException(e);
			}
		}
	}
	**/
	
	@Override
	public void statusChange(String flowType, String flowId, String serialNo,String approveStatus, String flowCompleteType)
	{
		String status = DictConstants.ApproveStatus.Verified;
		if(DictConstants.ApproveStatus.Approving.equals(approveStatus)){
			status = DictConstants.ApproveStatus.Verifying;
		}else if(DictConstants.ApproveStatus.ApprovedPass.equals(approveStatus)){
			status = DictConstants.ApproveStatus.Verified; 
		}else if(DictConstants.ApproveStatus.New.equals(approveStatus)){
			status = DictConstants.ApproveStatus.New;
		}
		Map<String,Object> map  = new HashMap<String, Object>();
		map.put("dealNo", serialNo);
		map.put("approveStatus", status);
		//更新状态哦
		tdOrdersortingApproveMapper.updateTdOrdersortingApproveStatus(map);
		//不等于审批通过 直接返回
		if(!DictConstants.ApproveStatus.ApprovedPass.equals(approveStatus))
		{
			return;
		}
		TdOrdersortingApprove tdOrdersortingApprove = tdOrdersortingApproveMapper.getTdOrdersortingApproveById(map);
		List<OrderSortingDetail> orDetails = orderSortingDetailService.getOrderSortingDetailByDealNo(BeanUtil.beanToHashMap(tdOrdersortingApprove));
		Map<String,OrderSortingDetail> tmcMaps = Maps.uniqueIndex(orDetails, new Function <OrderSortingDetail,String> () {  
	          @Override
              public String apply(OrderSortingDetail orderSortingDetail) {
	            return orderSortingDetail.getRevType()+"-"+orderSortingDetail.getRefNo();   
	    }});  

		String postDate = dayendDateService.getDayendDate();
		/**
		 通过 来账分拣 获得原始交易单据的类型
		*/
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
//		paramsMap.put("trade_id",(tdOrdersortingApprove.getcDealNo()==null ||tdOrdersortingApprove.getcDealNo().equals("")||
//				tdOrdersortingApprove.getcDealNo().equals("null"))
//				?tdOrdersortingApprove.getRefNo():tdOrdersortingApprove.getcDealNo());
		if(tdOrdersortingApprove.getcDealNo() != null && tdOrdersortingApprove.getcDealNo().length() > 11 && tdOrdersortingApprove.getcDealNo().contains("TRD")) {
			paramsMap.put("trade_id",tdOrdersortingApprove.getcDealNo());
		} else {
			paramsMap.put("trade_id",tdOrdersortingApprove.getRefNo());
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("refNo", tdOrdersortingApprove.getRefNo());
		params.put("osaleType", "T");
		TdOutrightSale tdOutrightSale = outrightSaleMapper.getTdOutrightSaleApproved(params);
		
		TtTrdTrade trade = tradeMapper.selectTradeForTradeId(paramsMap);
		trade.setRef_tradeid(tdOrdersortingApprove.getRefNo());
		if(DictConstants.TrdType.Custom.equals(trade.getTrdtype()))//正常到期
		{
			//正常收付息交易  本金或利息(单独收取的，要么还本，要么收息）非合并收取
			if(tmcMaps.size()==1){
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(trade.getTrade_id());
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = null;String acctno = "";
				InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
				List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					String refType = key.substring(0, key.indexOf("-"));
					int optype = 0;
					if(StringUtils.isNumeric(refType)){
						optype = Integer.parseInt(refType);
					}else{
						optype = 5;
					}
					switch (optype) {
					case 1://本金
						//构建账务过渡表
						if(tdOutrightSale == null)//如果交易未做过转让,则确认还本时产生账务数据
						{
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
							accTrdDaily.setPostDate(postDate);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							
							try {
								//产生账务数据
								amtMap = new HashMap<String, Object>();
								amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());
								instructionPackage = new InstructionPackage();
								instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
								instructionPackage.setAmtMap(amtMap);
								acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
								accTrdDaily.setAcctNo(acctno);//关联账务数据
								
								accTrdDailyMapper.insert(accTrdDaily);
							} catch (Exception e) {
								JY.error("生成账务出错:", e);
							}
						}
						try {
							if(tmcMaps.get(key).getRefNo() != null && tmcMaps.get(key).getRefNo().length() < 11) {
								//更新TD_CASHFLOW_CAPITAL的还本实际日期和金额
								TdCashflowCapital capital = new TdCashflowCapital();
								capital.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
								capital = tdCashflowCapitalMapper.selectByPrimaryKey(capital);
								capital.setRepaymentTamt(tmcMaps.get(key).getRevAmt().doubleValue());
								capital.setRepaymentTdate(dayendDateService.getDayendDate());
								tdCashflowCapitalMapper.updateCashflowCapitalById(capital);
							}
						} catch (Exception e) {
							JY.error("更新TD_CASHFLOW_CAPITAL出错:", e);
						}
						//更新实际现金流表
						TdAmtRange amtRange = new TdAmtRange();
						amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
						amtRange.setAmtType("1");//正常还本
						amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						amtRange.setExecDate(dayendDateService.getDayendDate());
						amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
						amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
						amtRange.setVersion(0);
						amtRangeMapper.insert(amtRange);
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						
						double banlance = PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue());
						tpos.setSettlAmt(banlance < 0 ? 0 : banlance);//剩余清算本金减少
						tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
						this.tposMapper.updateByPrimaryKey(tpos);
						
						creditRelease(productApproveMain.getDealNo(), approveStatus,String.valueOf(tmcMaps.get(key).getRevAmt()));
						
						break;
					case 2://利息
						accTrdDailies.clear();
						
						amtMap = new HashMap<String, Object>();
						
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						//判断实收-应收 大于 0 利息字典送应收 反的送实收
						if(tmcMaps.get(key).getRevAmt().doubleValue() > tmcMaps.get(key).getSrevAmt().doubleValue() ){
							accTrdDaily.setAccAmt(tmcMaps.get(key).getSrevAmt().doubleValue());//利息
						}else{
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
						}
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());
						accTrdDailies.add(accTrdDaily);
						try {
							if(tmcMaps.get(key).getRefNo() != null && tmcMaps.get(key).getRefNo().length() < 11) {
								//更新TD_CASHFLOW_INTEREST的收息实际日期和金额
								TdCashflowInterest cashflowInterest = new TdCashflowInterest();
								cashflowInterest.setCashflowId(tmcMaps.get(key).getRefNo());//cashflow_id
								cashflowInterest = tdCashflowInterestMapper.selectByPrimaryKey(cashflowInterest);
								cashflowInterest.setActualRamt(tmcMaps.get(key).getRevAmt().doubleValue());
								cashflowInterest.setActualDate(dayendDateService.getDayendDate());
								tdCashflowInterestMapper.updateCashflowInterestById(cashflowInterest);
							}
						} catch(Exception e) {
							JY.error("更新cashflowInterest出错:", e);
						}
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						
						/**
						 如果180 已经是负数，那么可以推断 514提前收了；
						 再收息，直接514做调整+收息金额，180也做调整-收息金额
						 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514无需调整
						 *180-收息值 <0  180减去收息金额  514追加差额
						 */
						if(tpos.getAccruedTint()<0){
							//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
							tpos.setAccruedTint(0);
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}else{
							double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
							if(trueAmt>=0){
								tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), trueAmt));//利息收入514
								//514无需调整
							}else{
								//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setAccruedTint(0);
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), -trueAmt));//利息收入514
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							
						}
						
						this.tposMapper.updateByPrimaryKey(tpos);
						
						try {
							//产生账务数据
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
						} catch (Exception e) {
							JY.error("收息计划确认,生成账务信息出错:", e);
						}
						
						break;//注意：这里的180可能无法清零，需要日终再次调整；  因为到期是可以分开收息和收本的
					default:
						break;
					}
				}
			}
		}//正常到期
		
		else if(DictConstants.TrdType.CustomAdvanceMaturity.equals(trade.getTrdtype()))//提前到期
		{
			//正常收付息交易  本金或利息(单独收取的，要么还本，要么收息）非合并收取
			if(tmcMaps.size()==1){
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(trade.getRef_tradeid());
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = null;String acctno = "";
				InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
				List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					String refType = key.substring(0, key.indexOf("-"));
					int optype = 0;
					if(StringUtils.isNumeric(refType)){
						optype = Integer.parseInt(refType);
					}else{
						optype = 5;
					}
					switch (optype) {
					case 1://本金
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
						accTrdDaily.setPostDate(postDate);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						try {
							//产生账务数据
							amtMap = new HashMap<String, Object>();
							amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							accTrdDaily.setAcctNo(acctno);//关联账务数据
							
							accTrdDailyMapper.insert(accTrdDaily);
						} catch (Exception e) {
							JY.error("生成账务出错:", e);
						}
						try {
							Map<String, Object> param = new HashMap<String, Object>();
							param.put("dealNo", tdOrdersortingApprove.getcDealNo());
							param.put("actAmt", tmcMaps.get(key).getRevAmt().doubleValue());
							param.put("actAmtDate", dayendDateService.getDayendDate());
							
							tdAdvanceMaturityMapper.updateTdAdvanceMaturityActAmt(param);
							
						} catch (Exception e) {
							JY.error("更新TD_ADVANCE_MATURITY出错:", e);
						}
						//更新实际现金流表
						TdAmtRange amtRange = new TdAmtRange();
						amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
						amtRange.setAmtType("1");//正常还本
						amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						amtRange.setExecDate(dayendDateService.getDayendDate());
						amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
						amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
						amtRange.setVersion(0);
						amtRangeMapper.insert(amtRange);
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						
						double banlance = PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue());
						tpos.setSettlAmt(banlance < 0 ? 0 : banlance);//剩余清算本金减少
						tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
						this.tposMapper.updateByPrimaryKey(tpos);
						
						creditRelease(productApproveMain.getDealNo(), approveStatus,String.valueOf(tmcMaps.get(key).getRevAmt()));
						
						break;
					case 2://利息
						accTrdDailies.clear();
						
						amtMap = new HashMap<String, Object>();
						
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						//判断实收-应收 大于 0 利息字典送应收 反的送实收
						if(tmcMaps.get(key).getRevAmt().doubleValue() > tmcMaps.get(key).getSrevAmt().doubleValue() ){
							accTrdDaily.setAccAmt(tmcMaps.get(key).getSrevAmt().doubleValue());//利息
						}else{
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
						}
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());
						accTrdDailies.add(accTrdDaily);
						try {
							Map<String, Object> param = new HashMap<String, Object>();
							param.put("dealNo", tdOrdersortingApprove.getcDealNo());
							param.put("actInt", tmcMaps.get(key).getRevAmt().doubleValue());
							param.put("actIntDate", dayendDateService.getDayendDate());
							
							tdAdvanceMaturityMapper.updateTdAdvanceMaturityActInt(param);
						} catch(Exception e) {
							JY.error("更新cashflowInterest出错:", e);
						}
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						/**
						 如果180 已经是负数，那么可以推断 514提前收了；
						 再收息，直接514做调整+收息金额，180也做调整-收息金额
						 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514无需调整
						 *180-收息值 <0  180减去收息金额  514追加差额
						 */
						if(tpos.getAccruedTint()<0){
							//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
							tpos.setAccruedTint(0);
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}else{
							double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
							if(trueAmt>=0){
								tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), trueAmt));//利息收入514
								//514无需调整
							}else{
								//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setAccruedTint(0);
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), -trueAmt));//利息收入514
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							
						}
						
						this.tposMapper.updateByPrimaryKey(tpos);
						
						try {
							//产生账务数据
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
						} catch (Exception e) {
							JY.error("收息计划确认,生成账务信息出错:", e);
						}
						
						break;//注意：这里的180可能无法清零，需要日终再次调整；  因为到期是可以分开收息和收本的
					default:
						break;
					}
				}
			}
		}
		
		else if(DictConstants.TrdType.CustomOutRightSale.equals(trade.getTrdtype()))//资产卖断 和 资产转让
		{
			//正常收付息交易  本金或利息(单独收取的，要么还本，要么收息）非合并收取
			if(tmcMaps.size()==1){
				TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(trade.getRef_tradeid());
				Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
				TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = null;String acctno = "";
				InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
				List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
				while(keyiIterable.hasNext()){
					String key = keyiIterable.next();
					String refType = key.substring(0, key.indexOf("-"));
					int optype = 0;
					if(StringUtils.isNumeric(refType)){
						optype = Integer.parseInt(refType);
					}else{
						optype = 5;
					}
					switch (optype) {
					case 1://本金
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
						accTrdDaily.setPostDate(postDate);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						try {
							//产生账务数据
							amtMap = new HashMap<String, Object>();
							amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							accTrdDaily.setAcctNo(acctno);//关联账务数据
							
							accTrdDailyMapper.insert(accTrdDaily);
						} catch (Exception e) {
							JY.error("生成账务出错:", e);
						}
						try {
							Map<String, Object> param = new HashMap<String, Object>();
							param.put("dealNo", tdOrdersortingApprove.getcDealNo());
							param.put("actAmt", tmcMaps.get(key).getRevAmt().doubleValue());
							param.put("actAmtDate", dayendDateService.getDayendDate());
							
							outrightSaleMapper.updateTdOutrightSaleActAmt(params);
						} catch (Exception e) {
							JY.error("更新TD_OUTRIGHT_SALE出错:", e);
						}
						//更新实际现金流表
						TdAmtRange amtRange = new TdAmtRange();
						amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
						amtRange.setAmtType("1");//正常还本
						amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
						amtRange.setExecDate(dayendDateService.getDayendDate());
						amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
						amtRange.setRefTable("TD_CASHFLOW_CAPITAL");
						amtRange.setVersion(0);
						amtRangeMapper.insert(amtRange);
						
						if(tdOutrightSale == null)//转让不释放额度
						{
							//更改头寸表
							tpos = new TdTrdTpos();
							tpos.setDealNo(productApproveMain.getDealNo());
							tpos.setVersion(productApproveMain.getVersion());
							tpos = tposMapper.selectByPrimaryKey(tpos);
							
							double banlance = PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue());
							tpos.setSettlAmt(banlance < 0 ? 0 : banlance);//剩余清算本金减少
							tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
							this.tposMapper.updateByPrimaryKey(tpos);
							
							creditRelease(productApproveMain.getDealNo(), approveStatus,String.valueOf(tmcMaps.get(key).getRevAmt()));
						}
						
						break;
					case 2://利息
						accTrdDailies.clear();
						
						amtMap = new HashMap<String, Object>();
						
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						//判断实收-应收 大于 0 利息字典送应收 反的送实收
						if(tmcMaps.get(key).getRevAmt().doubleValue() > tmcMaps.get(key).getSrevAmt().doubleValue() ){
							accTrdDaily.setAccAmt(tmcMaps.get(key).getSrevAmt().doubleValue());//利息
						}else{
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
						}
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());
						accTrdDailies.add(accTrdDaily);
						
						try {
							Map<String, Object> param = new HashMap<String, Object>();
							param.put("dealNo", tdOrdersortingApprove.getcDealNo());
							param.put("actInt", tmcMaps.get(key).getRevAmt().doubleValue());
							param.put("actIntDate", dayendDateService.getDayendDate());
							
							outrightSaleMapper.updateTdOutrightSaleActInt(params);
							
						} catch(Exception e) {
							JY.error("更新TD_ADVANCE_MATURITY出错:", e);
						}
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						/**
						 如果180 已经是负数，那么可以推断 514提前收了；
						 再收息，直接514做调整+收息金额，180也做调整-收息金额
						 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514无需调整
						 *180-收息值 <0  180减去收息金额  514追加差额
						 */
						if(tpos.getAccruedTint()<0){
							//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
							tpos.setAccruedTint(0);
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}else{
							double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
							if(trueAmt>=0){
								tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), trueAmt));//利息收入514
								//514无需调整
							}else{
								//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
								tpos.setAccruedTint(0);
								tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), -trueAmt));//利息收入514
								accTrdDaily = new TdAccTrdDaily();
								accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
								accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
								accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
								accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
								accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
								accTrdDaily.setPostDate(dayendDateService.getDayendDate());
								amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
								accTrdDailies.add(accTrdDaily);
							}
							
						}
						
						this.tposMapper.updateByPrimaryKey(tpos);
						
						try {
							//产生账务数据
							instructionPackage = new InstructionPackage();
							instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
							instructionPackage.setAmtMap(amtMap);
							acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							}
							batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
						} catch (Exception e) {
							JY.error("收息计划确认,生成账务信息出错:", e);
						}
						
						break;//注意：这里的180可能无法清零，需要日终再次调整；  因为到期是可以分开收息和收本的
					default:
						break;
					}
				}
			}
		}//end 资产卖断
		if(DictConstants.TrdType.CustomInterestSettleConfirm.equals(trade.getTrdtype())){
			TdInterestSettle settl = interestSettleService.getInterestSettleById(tdOrdersortingApprove.getcDealNo());
			TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(settl.getRefNo());
			Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
			TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = new HashMap<String, Object>();;String acctno = "";
			InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			while(keyiIterable.hasNext()){
				String key = keyiIterable.next();
				String refType = key.substring(0, key.indexOf("-"));
				int optype = 0;
				if(StringUtils.isNumeric(refType)){
					optype = Integer.parseInt(refType);
				}else{
					optype = 5;
				}
				switch (optype) {
					case 10://活期提回结息
						accTrdDailies.clear();
						amtMap = new HashMap<String, Object>();
						//构建账务过渡表
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INT);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						
						TdInterestSettle tdInterestSettle = tdInterestSettleMapper.getInterestSettleById(tmcMaps.get(key).getRefNo());
						double intadj = PlaningTools.sub(tdInterestSettle.getInterestSettlAmt(),tmcMaps.get(key).getRevAmt().doubleValue());
						if(intadj >= 0){
							accTrdDaily.setAccAmt(tdInterestSettle.getInterestSettlAmt());//结息金额
							amtMap.put(DurationConstants.AccType.INCOME_INT, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
							
							//结息金额与账务日期已计提总额的差额
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INTADJ);
							accTrdDaily.setAccAmt(-intadj);//活期收息结息调整
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.INCOME_INTADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}else{
							accTrdDaily.setAccAmt(tdInterestSettle.getInterestSettlAmt());//结息金额
							amtMap.put(DurationConstants.AccType.INCOME_INT, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
							
							//结息金额与账务日期已计提总额的差额
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccType(DurationConstants.AccType.INCOME_INTADJ);
							accTrdDaily.setAccAmt(-intadj);//活期收息结息调整
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.INCOME_INTADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}
						
						//更改头寸表
						tpos = new TdTrdTpos();
						tpos.setDealNo(productApproveMain.getDealNo());
						tpos.setVersion(productApproveMain.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						tpos.setInterestSettleAmt(0);//当期结息金额为0
						tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tdInterestSettle.getSettlIntadj()));//累计每日利息
						this.tposMapper.updateByPrimaryKey(tpos);
						//产生账务数据
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							for(int i = 0 ;i <accTrdDailies.size();i++)
							{
								accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
							}
						}
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
						break;
					default:
						break;
				}
			}
		}else if(DictConstants.TrdType.CustomAdvanceMaturityCurrent.equals(trade.getTrdtype())){

			//根据提前到期交易号查询提前到期活期交易
			TdAdvanceMaturityCurrent advanceMaturityCurrent = advanceMaturityCurrentService.getAdvanceMaturityCurrentById(trade.getTrade_id());
			TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
			
			Iterator<String> keyiIterable = tmcMaps.keySet().iterator();
			TdAccTrdDaily accTrdDaily = null;Map<String, Object> amtMap = null;String acctno = "";
			InstructionPackage instructionPackage = null;TdTrdTpos tpos =null;
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			while(keyiIterable.hasNext()){
				String key = keyiIterable.next();
				String refType = key.substring(0, key.indexOf("-"));
				int optype = 0;
				if(StringUtils.isNumeric(refType)){
					optype = Integer.parseInt(refType);
				}else{
					optype = 5;
				}
				switch (optype) {
				case 1://本金
					//构建账务过渡表
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
					accTrdDaily.setAccType(DurationConstants.AccType.AMT_PRINCIPAL);
					accTrdDaily.setPostDate(postDate);
					accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
					accTrdDaily.setPostDate(dayendDateService.getDayendDate());
					try {
						//产生账务数据
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.AMT_PRINCIPAL, accTrdDaily.getAccAmt());
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
						accTrdDaily.setAcctNo(acctno);//关联账务数据
						
						accTrdDailyMapper.insert(accTrdDaily);
					} catch (Exception e) {
						JY.error("生成账务出错:", e);
					}
					try {
						Map<String, Object> param = new HashMap<String, Object>();
						param.put("dealNo", tdOrdersortingApprove.getcDealNo());
						param.put("actAmt", tmcMaps.get(key).getRevAmt().doubleValue());
						param.put("actAmtDate", dayendDateService.getDayendDate());
						
						tdAdvanceMaturityCurrentMapper.updateTdAdvanceMaturityActAmt(param);
						
					} catch (Exception e) {
						JY.error("更新TD_ADVANCE_MATURITY出错:", e);
					}
					//更新实际现金流表
					TdAmtRange amtRange = new TdAmtRange();
					amtRange.setDealNo(productApproveMain.getDealNo());//原始交易单
					amtRange.setAmtType("1");//正常还本
					amtRange.setExecAmt(tmcMaps.get(key).getRevAmt().doubleValue());//本金
					amtRange.setExecDate(dayendDateService.getDayendDate());
					amtRange.setRefNo(tmcMaps.get(key).getRefNo());//CASHFLOW_ID
					amtRange.setRefTable("TD_ADVANCE_MATURITY_CURRENT");
					amtRange.setVersion(0);
					amtRangeMapper.insert(amtRange);
					//更改头寸表
					tpos = new TdTrdTpos();
					tpos.setDealNo(productApproveMain.getDealNo());
					tpos.setVersion(productApproveMain.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					
					double banlance = PlaningTools.sub(tpos.getSettlAmt(), tmcMaps.get(key).getRevAmt().doubleValue());
					tpos.setSettlAmt(banlance < 0 ? 0 : banlance);//剩余清算本金减少
					tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), tmcMaps.get(key).getRevAmt().doubleValue()));//归还本金增加
					this.tposMapper.updateByPrimaryKey(tpos);
					
					creditRelease(productApproveMain.getDealNo(), approveStatus,String.valueOf(tmcMaps.get(key).getRevAmt()));
					
					break;
				case 2://利息
					accTrdDailies.clear();
					
					amtMap = new HashMap<String, Object>();
					
					//构建账务过渡表
					accTrdDaily = new TdAccTrdDaily();
					//判断实收-应收 大于 0 利息字典送应收 反的送实收
					if(tmcMaps.get(key).getRevAmt().doubleValue() > tmcMaps.get(key).getSrevAmt().doubleValue() ){
						accTrdDaily.setAccAmt(tmcMaps.get(key).getSrevAmt().doubleValue());//利息
					}else{
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息
					}
					accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST);
					accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
					accTrdDaily.setPostDate(dayendDateService.getDayendDate());
					amtMap.put(DurationConstants.AccType.AMT_INTEREST, accTrdDaily.getAccAmt());
					accTrdDailies.add(accTrdDaily);
					try {
						Map<String, Object> param = new HashMap<String, Object>();
						param.put("dealNo", tdOrdersortingApprove.getcDealNo());
						param.put("actInt", tmcMaps.get(key).getRevAmt().doubleValue());
						param.put("actIntDate", dayendDateService.getDayendDate());
						
						tdAdvanceMaturityCurrentMapper.updateTdAdvanceMaturityActInt(param);
					} catch(Exception e) {
						JY.error("更新cashflowInterest出错:", e);
					}
					//更改头寸表
					tpos = new TdTrdTpos();
					tpos.setDealNo(productApproveMain.getDealNo());
					tpos.setVersion(productApproveMain.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					/**
					 如果180 已经是负数，那么可以推断 514提前收了；
					 再收息，直接514做调整+收息金额，180也做调整-收息金额
					 如果180是正数，那么需要判断   180-收息值 >=0那么说明计提的多，514无需调整
					 *180-收息值 <0  180减去收息金额  514追加差额
					 */
					if(tpos.getAccruedTint()<0){
						//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
						tpos.setAccruedTint(0);
						tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tmcMaps.get(key).getRevAmt().doubleValue()));//利息收入514
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setAccAmt(tmcMaps.get(key).getRevAmt().doubleValue());//利息调整 追加收息金额
						accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
						accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
						accTrdDaily.setPostDate(dayendDateService.getDayendDate());
						amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
						accTrdDailies.add(accTrdDaily);
					}else{
						double trueAmt = PlaningTools.sub(tpos.getAccruedTint(), tmcMaps.get(key).getRevAmt().doubleValue());
						if(trueAmt>=0){
							tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), trueAmt));//利息收入514
							//514无需调整
						}else{
							//tpos.setAccruedTint(PlaningTools.sub(tpos.getAccruedTint(),tmcMaps.get(key).getRevAmt().doubleValue()));//应计利息180
							tpos.setAccruedTint(0);
							tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), -trueAmt));//利息收入514
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setAccAmt(-trueAmt);//利息调整 追加差额部分
							accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
							accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setRefNo(tmcMaps.get(key).getDealNo());//来账分拣的DEALNO
							accTrdDaily.setPostDate(dayendDateService.getDayendDate());
							amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, accTrdDaily.getAccAmt());
							accTrdDailies.add(accTrdDaily);
						}
						
					}
					
					this.tposMapper.updateByPrimaryKey(tpos);
					
					try {
						//产生账务数据
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						acctno = bookkeepingService.generateEntry4Instruction(instructionPackage);
						for(int i = 0 ;i <accTrdDailies.size();i++)
						{
							accTrdDailies.get(i).setAcctNo(acctno);//关联账务数据
						}
						batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
					} catch (Exception e) {
						JY.error("收息计划确认,生成账务信息出错:", e);
					}
					
					break;//注意：这里的180可能无法清零，需要日终再次调整；  因为到期是可以分开收息和收本的
				default:
					break;
				}
			}
		}
		
	}

	@Override
	public TdOrdersortingApprove getTdOrdersortingApproveById(
			Map<String, Object> params) {
		return tdOrdersortingApproveMapper.getTdOrdersortingApproveById(params);
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no,
			String task_id, String task_def_key) {
		return null;
	}

	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveMySelf(
			Map<String, Object> params, RowBounds rb) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApproveMySelf(params, rb);
	}

	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveFinished(
			Map<String, Object> params, RowBounds rb) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApproveFinished(params, rb);
	}
	
	@Override
	public String t24TStBeaRepayAaaRequestOrder(Map<String,Object> params) throws Exception{
		TdOrdersortingApprove tdOrdersortingApprove = tdOrdersortingApproveMapper.getTdOrdersortingApproveById(params);
		List<OrderSortingDetail> orDetails = orderSortingDetailService.getOrderSortingDetail(BeanUtil.beanToHashMap(tdOrdersortingApprove));
		String rsmcode =  "000000";
		Map<String,Object> mapT24lz = new HashMap<String, Object>();
		mapT24lz.put("feeBaseLogid", tdOrdersortingApprove.getDealNo());
		TExReconT24lz texReconT24lz	= ti2ndPaymentMapper.searchTExReconT24lz(mapT24lz);
		boolean flag = true;//判断是否发送成功
		if(texReconT24lz == null){
			flag = false;//未发送
		}
		if(texReconT24lz != null &&!"000000".equals(texReconT24lz.getRETCODE())){
			TExReconAllInqRq texReconAllInqRq = new TExReconAllInqRq();
			CommonRqHdr hdr = new CommonRqHdr();
			hdr.setRqUID(UUID.randomUUID().toString());
			hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
			hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
			texReconAllInqRq.setCommonRqHdr(hdr);
			texReconAllInqRq.setChannelId(InterfaceCode.TI_CHANNELNO);
			texReconAllInqRq.setFBID(InterfaceCode.TI_FBID);
			texReconAllInqRq.setTxnType(InterfaceCode.TI_IFBM0002);
			texReconAllInqRq.setMode("I");
			texReconAllInqRq.setTransId(texReconT24lz.getSrquid());
			TExReconAllInqRs  texReconAllInqRs = socketClientService.t24TexReconAllInqRequest(texReconAllInqRq);
			if(texReconAllInqRs.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)|| 
					"10510206".equals(texReconAllInqRs.getCommonRsHdr().getStatusCode())){
				List<TExReconAllInqRec> texReconAllInqRecList = texReconAllInqRs.getTExReconAllInqRec();
				//判断是否已经发送
				if(null != texReconAllInqRecList && texReconAllInqRecList.size()>0){
					for (int i = 0; i < texReconAllInqRecList.size(); i++) {
						//检查发送状态是否已经成功，如果不成功，则重新进行发送
						TExReconAllInqRec texReconAllInqRec = texReconAllInqRecList.get(i);
						if(texReconAllInqRec.getTxnStatus() == null || !"0000".equals(texReconAllInqRec.getTxnStatus())){
							flag = false;//发送成功，查询对账未处理成功，需重新发送
						}
						if("0000".equals(texReconAllInqRec.getTxnStatus())){
							rsmcode = "000000";
						}
					}
				}
			}else{
				rsmcode = "999999";
				throw new RException("T24对账接口异常，请稍后重试！");
			}
		}
		
		//未发送，组装多借多贷报文进行发送
		if(!flag){
			TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
			Map<String, Object> mapt = new HashMap<String, Object>();
			request.setCurrencyDr(tdOrdersortingApprove.getCcy());
			request.setCurrencyCr(tdOrdersortingApprove.getCcy());
			//查询407账号
			mapt.put("ccy", tdOrdersortingApprove.getCcy().trim());
			String sponInst = orderSortingMapper.getSponInstByDealNo(tdOrdersortingApprove.getRefNo());
			if(null == StringUtils.trimToNull(sponInst))
			{
				throw new RException("不存在原交易对应的申请机构号！");
			}
			mapt.put("instId", sponInst);
			List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
			if(null == acct || acct.size()<=0){
				throw new RException("找不到："+sponInst+"机构"+tdOrdersortingApprove.getCcy()+"的407账号！");
			}
			//获取所有收款98账号
			Map<String, Object> mapT24 = new HashMap<String, Object>();
			String tNoArr = "";
			for (int i = 0; i < orDetails.size(); i++) {
				if("999".equals(orDetails.get(i).getRevType())){
					tNoArr += orDetails.get(i).getRefNo()+",";
				}
			}
			List<CreditBeaRec> crList = new ArrayList<CreditBeaRec>();
			List<DebitBeaRec> drList = new ArrayList<DebitBeaRec>();
			CreditBeaRec recc1;
			DebitBeaRec rec1;
			if(!"".equals(tNoArr)){
				tNoArr = tNoArr.substring(0, tNoArr.length()-1);
				mapT24.put("tFlow", tNoArr.split(","));
				//查询98账户
				List<Root> rootList = tiInterfaceInfoMapper.searchTiTsaAccountByTflow(mapT24);
				for (int i = 0; i < rootList.size(); i++) {
					//借方98
					rec1 = new DebitBeaRec();
					TtInstitutionSettls ttInstitutionSettls = new TtInstitutionSettls();
					Map<String,Object> settlsMap = new HashMap<String, Object>();
					settlsMap.put("inst_acct_no", rootList.get(i).getPayAcct());
					ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByLikeAcctNo(settlsMap);
					rec1.setDebitMedType(ttInstitutionSettls.getMediumType());	
					rec1.setAmtDr(String.valueOf(rootList.get(i).getTranAmt()));
					rec1.setAcctNoDr(ttInstitutionSettls.getInstAcctNo());
					drList.add(rec1);	
					//贷方407
					recc1 = new CreditBeaRec();
					recc1.setCreditMedType(acct.get(0).getMediumType());
					recc1.setAmtCr(String.valueOf(rootList.get(i).getTranAmt()));
					recc1.setAcctNoCr(acct.get(0).getAcctNo());
					crList.add(recc1);
				}
				request.setFbNo(InterfaceCode.TI_FBID);
				request.setCreditBeaRec(crList);
				request.setDebitBeaRec(drList);
				request.setTxnType(InterfaceCode.TI_IFBM0002);
				request.setFeeBaseLogId(tdOrdersortingApprove.getDealNo());
				CommonRqHdr hdr2 = new CommonRqHdr();
				hdr2.setRqUID(UUID.randomUUID().toString());
				hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
				hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
				hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
				request.setCommonRqHdr(hdr2);
				TStBeaRepayAaaRs response = new TStBeaRepayAaaRs();
				try {
					response = socketClientService.t24TStBeaRepayAaaRequest(request);
					if(response.getCommonRsHdr().getStatusCode()=="000000"
							|| "000000".equals(response.getCommonRsHdr().getStatusCode())||
						response.getCommonRsHdr().getStatusCode()=="105114"
							|| "105114".equals(response.getCommonRsHdr().getStatusCode())){
						JY.info("来账分拣调用多借多贷接口处理成功");
						rsmcode =  "000000";
					}else {
						throw new RException("来账分拣调用多借多贷接口处理失败,错误信息："+response.getCommonRsHdr().getServerStatusCode());
					}
				} catch (Exception e) {
					JY.error(e.getMessage());
					throw new RException("来账分拣调用多借多贷接口处理失败,错误信息："+response.getCommonRsHdr().getServerStatusCode());
				}
			}
		}
		return rsmcode;
		
	}
	/**
	 * 18/01/03 下面加的 3个方法（bytype）原来上海的没有的
	 * 
	*/
	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveMySelfByType(
			Map<String, Object> params, RowBounds rowBounds) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApproveMySelf(params, rowBounds);
	}

	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveUnfinishedByType(
			Map<String, Object> params, RowBounds rowBounds) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApproveUnfinished(params, rowBounds);
		
	}

	@Override
	public Page<TdOrdersortingApprove> searchTdOrdersortingApproveFinishedByType(
			Map<String, Object> params, RowBounds rowBounds) {
		return tdOrdersortingApproveMapper.searchTdOrdersortingApproveFinished(params, rowBounds);
	}

	@Override
	public void approveTest(String serialNo) {
		
		
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }
}
