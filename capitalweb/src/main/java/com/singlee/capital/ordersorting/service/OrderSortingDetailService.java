package com.singlee.capital.ordersorting.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.ordersorting.model.OrderSortingDetail;

public interface OrderSortingDetailService {
	
	public List<OrderSortingDetail> getOrderSortingDetail(Map<String, Object> params);
	public int insertOrderSortingDetail(Map<String, Object> params);
	public int deleteOrderSortingDetailByDeal(Map<String, Object> params);
	
	public List<OrderSortingDetail> getOrderSortingDetailByRef(Map<String, Object> params);
	
	public List<OrderSortingDetail> getOrderSortingDetailByDealNo(Map<String, Object> params);
	

}
