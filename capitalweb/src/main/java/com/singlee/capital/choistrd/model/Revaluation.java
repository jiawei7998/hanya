package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Revaluation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//SELECT DEALNO,SEQ,DEALDATE,PRODCODE,PRODTYPE,PS,CCY,CCYAMT,CCYRATE_8,CTRCCY,CTRAMT,CCYREVALAMT,CTRREVALAMT,CCYSETTDATE,CTRSETTDATE,SWAPDEAL,FARNEARIND FROM FXDH WHERE VERDATE IS NOT NULL AND REVDATE IS NULL;

	private String DEALNO;
	
	private String SEQ;
	
	private String DEALDATE;
	
	private String PRODCODE;
	
	private String PRODTYPE;
	
	private String PS;
	
	private String CCY;
	
	private BigDecimal CCYAMT;
	
	private BigDecimal CCYRATE_8;
	
	private String CTRCCY;
	
	private BigDecimal CTRAMT;
	
	private BigDecimal CCYREVALAMT;
	
	private BigDecimal CTRREVALAMT;
	
	private String CCYSETTDATE;
	
	private String CTRSETTDATE;
	
	private String SWAPDEAL;
	
	private String FARNEARIND;
	
	public Revaluation(){}

	/**
	 * @return the dEALNO
	 */
	public String getDEALNO() {
		return DEALNO;
	}

	/**
	 * @param dealno the dEALNO to set
	 */
	public void setDEALNO(String dealno) {
		DEALNO = dealno;
	}

	/**
	 * @return the sEQ
	 */
	public String getSEQ() {
		return SEQ;
	}

	/**
	 * @param seq the sEQ to set
	 */
	public void setSEQ(String seq) {
		SEQ = seq;
	}

	/**
	 * @return the dEALDATE
	 */
	public String getDEALDATE() {
		return DEALDATE;
	}

	/**
	 * @param dealdate the dEALDATE to set
	 */
	public void setDEALDATE(String dealdate) {
		DEALDATE = dealdate;
	}

	/**
	 * @return the pRODCODE
	 */
	public String getPRODCODE() {
		return PRODCODE;
	}

	/**
	 * @param prodcode the pRODCODE to set
	 */
	public void setPRODCODE(String prodcode) {
		PRODCODE = prodcode;
	}

	/**
	 * @return the pRODTYPE
	 */
	public String getPRODTYPE() {
		return PRODTYPE;
	}

	/**
	 * @param prodtype the pRODTYPE to set
	 */
	public void setPRODTYPE(String prodtype) {
		PRODTYPE = prodtype;
	}

	/**
	 * @return the pS
	 */
	public String getPS() {
		return PS;
	}

	/**
	 * @param ps the pS to set
	 */
	public void setPS(String ps) {
		PS = ps;
	}

	/**
	 * @return the cCY
	 */
	public String getCCY() {
		return CCY;
	}

	/**
	 * @param ccy the cCY to set
	 */
	public void setCCY(String ccy) {
		CCY = ccy;
	}

	/**
	 * @return the cCYAMT
	 */
	public BigDecimal getCCYAMT() {
		return CCYAMT;
	}

	/**
	 * @param ccyamt the cCYAMT to set
	 */
	public void setCCYAMT(BigDecimal ccyamt) {
		CCYAMT = ccyamt;
	}

	/**
	 * @return the cCYRATE_8
	 */
	public BigDecimal getCCYRATE_8() {
		return CCYRATE_8;
	}

	/**
	 * @param ccyrate_8 the cCYRATE_8 to set
	 */
	public void setCCYRATE_8(BigDecimal ccyrate_8) {
		CCYRATE_8 = ccyrate_8;
	}

	/**
	 * @return the cTRCCY
	 */
	public String getCTRCCY() {
		return CTRCCY;
	}

	/**
	 * @param ctrccy the cTRCCY to set
	 */
	public void setCTRCCY(String ctrccy) {
		CTRCCY = ctrccy;
	}

	/**
	 * @return the cTRAMT
	 */
	public BigDecimal getCTRAMT() {
		return CTRAMT;
	}

	/**
	 * @param ctramt the cTRAMT to set
	 */
	public void setCTRAMT(BigDecimal ctramt) {
		CTRAMT = ctramt;
	}

	/**
	 * @return the cCYREVALAMT
	 */
	public BigDecimal getCCYREVALAMT() {
		return CCYREVALAMT;
	}

	/**
	 * @param ccyrevalamt the cCYREVALAMT to set
	 */
	public void setCCYREVALAMT(BigDecimal ccyrevalamt) {
		CCYREVALAMT = ccyrevalamt;
	}

	/**
	 * @return the cTRREVALAMT
	 */
	public BigDecimal getCTRREVALAMT() {
		return CTRREVALAMT;
	}

	/**
	 * @param ctrrevalamt the cTRREVALAMT to set
	 */
	public void setCTRREVALAMT(BigDecimal ctrrevalamt) {
		CTRREVALAMT = ctrrevalamt;
	}

	/**
	 * @return the cCYSETTDATE
	 */
	public String getCCYSETTDATE() {
		return CCYSETTDATE;
	}

	/**
	 * @param ccysettdate the cCYSETTDATE to set
	 */
	public void setCCYSETTDATE(String ccysettdate) {
		CCYSETTDATE = ccysettdate;
	}

	/**
	 * @return the cTRSETTDATE
	 */
	public String getCTRSETTDATE() {
		return CTRSETTDATE;
	}

	/**
	 * @param ctrsettdate the cTRSETTDATE to set
	 */
	public void setCTRSETTDATE(String ctrsettdate) {
		CTRSETTDATE = ctrsettdate;
	}

	/**
	 * @return the sWAPDEAL
	 */
	public String getSWAPDEAL() {
		return SWAPDEAL;
	}

	/**
	 * @param swapdeal the sWAPDEAL to set
	 */
	public void setSWAPDEAL(String swapdeal) {
		SWAPDEAL = swapdeal;
	}

	/**
	 * @return the fARNEARIND
	 */
	public String getFARNEARIND() {
		return FARNEARIND;
	}

	/**
	 * @param farnearind the fARNEARIND to set
	 */
	public void setFARNEARIND(String farnearind) {
		FARNEARIND = farnearind;
	}
	
}
