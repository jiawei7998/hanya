package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.OpicsSecm;
import com.singlee.capital.choistrd.model.Secl;


import tk.mybatis.mapper.common.Mapper;

public interface GetSeclMapper  extends Mapper<OpicsSecm>{
	List<OpicsSecm> selectSeclKey (Map<String, Object> map);
}
