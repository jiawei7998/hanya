package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SL_PMTQ;
import com.singlee.capital.choistrd.model.SL_PMTQREVERSE;


import tk.mybatis.mapper.common.Mapper;



public interface PmtqMappr extends Mapper<SL_PMTQ>{

	/**
	 * ��ѯ�����е���Ҫ���͵�PMTQ����
	 * @param dealDate
	 * @param isSend
	 * @param isSuccess
	 * @return
	 * @throws Exception
	 */
	public List<SL_PMTQ> queryAllPmtqFromOpicsByCondition() throws Exception;
	
	//public List<SL_PMTQ> queryAllPmtqFromOpicsByPostdate(String postdate) throws Exception;
	/**
	 * ����PMTQ״̬
	 * @param sl_pmtq
	 * @return
	 * @throws Exception
	 */
	public int updatePmtqStatus(SL_PMTQ sl_pmtq) throws Exception;
	/**
	 * ����������ѯPMTQ
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	public SL_PMTQ queryPmtqByDealnoSeqCondition(Map<String, Object> map) throws Exception;
	/**
	 * �����Ҫ�����Ĵ�����Ϣ
	 * @param postdate
	 * @return
	 * @throws Exception
	 */
	//public List<SL_PMTQREVERSE> queryPmtqReverseFromOpicsByPostdate(String postdate) throws Exception;
	
	
	//public SL_PMTQ queryPmtqByChoisRefHisNo(SL_PMTQREVERSE sl_pmtqreverse) throws Exception;
	
}
