package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SWDS;

import tk.mybatis.mapper.common.Mapper;

public interface SwdsMapper extends Mapper<SWDS>{
	
	public List<SWDS> queryAllSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	public List<SWDS> queryAllRevSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
}
