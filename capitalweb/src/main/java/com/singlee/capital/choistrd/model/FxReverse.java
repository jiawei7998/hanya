package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class FxReverse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String server;
	
	private String inputCode;
	
	private String subjectCode;
	
	private String tradCode;
	
	private String traderCode;
	
	private String FXFIG_REF_NO;
	
	private String seq;
	
	private String dealno;
	
	public FxReverse(){}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}
	/**
	 * @return the inputCode
	 */
	public String getInputCode() {
		return inputCode;
	}

	/**
	 * @param inputCode the inputCode to set
	 */
	public void setInputCode(String inputCode) {
		this.inputCode = inputCode;
	}

	/**
	 * @return the subjectCode
	 */
	public String getSubjectCode() {
		return subjectCode;
	}

	/**
	 * @param subjectCode the subjectCode to set
	 */
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	/**
	 * @return the tradCode
	 */
	public String getTradCode() {
		return tradCode;
	}

	/**
	 * @param tradCode the tradCode to set
	 */
	public void setTradCode(String tradCode) {
		this.tradCode = tradCode;
	}

	/**
	 * @return the traderCode
	 */
	public String getTraderCode() {
		return traderCode;
	}

	/**
	 * @param traderCode the traderCode to set
	 */
	public void setTraderCode(String traderCode) {
		this.traderCode = traderCode;
	}

	/**
	 * @return the fXFIG_REF_NO
	 */
	public String getFXFIG_REF_NO() {
		return FXFIG_REF_NO;
	}

	/**
	 * @param fxfig_ref_no the fXFIG_REF_NO to set
	 */
	public void setFXFIG_REF_NO(String fxfig_ref_no) {
		FXFIG_REF_NO = fxfig_ref_no;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	
}
