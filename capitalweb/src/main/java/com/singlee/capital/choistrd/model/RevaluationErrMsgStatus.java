package com.singlee.capital.choistrd.model;

public class RevaluationErrMsgStatus {

	private int status;
	
	private RevaluationTmaxBean revaluationTmaxBean;
	
	public RevaluationErrMsgStatus(){}
	
	
	public RevaluationErrMsgStatus(int status, RevaluationTmaxBean revaluationTmaxBean) {
		super();
		this.status = status;
		this.revaluationTmaxBean = revaluationTmaxBean;
	}

	

	/**
	 * @return the revaluationTmaxBean
	 */
	public RevaluationTmaxBean getRevaluationTmaxBean() {
		return revaluationTmaxBean;
	}


	/**
	 * @param revaluationTmaxBean the revaluationTmaxBean to set
	 */
	public void setRevaluationTmaxBean(RevaluationTmaxBean revaluationTmaxBean) {
		this.revaluationTmaxBean = revaluationTmaxBean;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
