package com.singlee.capital.choistrd.service.impl;


import java.text.SimpleDateFormat;
import java.util.Date;


import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.choistrd.mapper.FxTradeMapper;
import com.singlee.capital.choistrd.model.FxReverse;
import com.singlee.capital.choistrd.model.FxStatus;

import com.singlee.capital.common.util.PropertiesUtil;
@Service
public class FxReverseTmaxCommand  {
	public static Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private FxTradeMapper fxTradeMapper ;

	
	public String FXFIG_SSCK;
	public String FXFIG_THID;
	
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	ChiosLogin chiosLogin = new ChiosLogin();

	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();
	

	public int executeHostVisitAction(FxReverse fxReverse) throws Exception {
		// TODO Auto-generated method stub
		
			
			FxStatus fxStatus = new FxStatus();
				fxStatus.setTablename("FXDH");	
				//fxStatus.setChoisHisNo(fxReverse.getFXFIG_REF_NO());
				
				fxStatus.setUpcount(0);
				fxStatus.setUpown("TMAX");
				fxStatus.setUptime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				fxStatus.setSendtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				
				long standardStampStart = System.currentTimeMillis();

				//拼接报文
				
			     //返回的柜员session
				String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
				StdTMsgGenerator generator = new StdTMsgGenerator();
				//报文头
				byte[] headerPacket=choisCommonMsg.makeHead();
				
				//报文体
				 String handleCode="FE1800";
				 String serveHandleCode="OPCFE18";
				PropertiesConfiguration stdtmsgSysValue;
				
				stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
				 
			
				FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
				
				String coMsg=choisCommonMsg.makeMsg();//公共部分
				String coMsg2=choisCommonMsg.makeMsg2();//公共部分
				String coMsg3=choisCommonMsg.makeMsg3();//公共部分
				//交易数据
				String dealMsg="FE11"+fxReverse.getFXFIG_REF_NO();
				byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
				byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
				String serviceUrl = "OPS";
				byte[] msgData =totalPacket;
				EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
				System.out.println("eaiDv:--"+eaiDv);
				byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
				System.out.println("FXReverse返回信息："+new String(returnData, "UTF-8"));
				//CHOIS参考号
				int len=(new String(returnData, "UTF-8")).length();
				
				
				logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
		
				logger.info("RECV MSG:"+new String(returnData, "UTF-8"));
				//CHOIS参考号
				String choisHisNoString=(new String(returnData, "UTF-8")).substring(len-220,len-218);
				String resmg=null;
				resmg=((new String(returnData, "UTF-8")).substring(0,292).substring((new String(returnData, "UTF-8")).substring(0,292).length()-2));
			
				
				fxStatus.setSendmsg(new String(msgData, "UTF-8"));
			try {
				if(resmg.equals("10")) {
					fxStatus.setReversal("Y");
					fxStatus.setSendresult("SUCCESS");
					fxStatus.setIsresend("N");
					fxStatus.setIssend("Y");
				}else {
					fxStatus.setUpown("TMAX");
					fxStatus.setIsresend("Y");
					fxStatus.setErrmsg(new String(returnData, "UTF-8"));
				}
				
				
				
			} catch (Exception sv) {
				
				
					logger.info("WebtServiceException msg:"+sv.toString());
					fxStatus.setErrmsg(sv.toString());
			}
				//fxStatus.setUpCount(oldFxStatus.getUpCount()+1);
				fxStatus.setDealno(fxReverse.getDealno().trim());
				fxStatus.setSeq(fxReverse.getSeq());
				if(this.fxTradeMapper.updateFxStatusByChoisRefHisNo(fxStatus)>0)
				{
					logger.info("CHOIS_REF_NO["+fxStatus.getChoisHisNo()+"]TABLENAME["+fxStatus.getTablename()+"]更新[SL_FXDH_STATUS]成功.");
				}else {
					logger.info("CHOIS_REF_NO["+fxStatus.getChoisHisNo()+"]TABLENAME["+fxStatus.getTablename()+"]更新账[SL_FXDH_STATUS]失败.");
				}
				
				
			
			
			
			long standardStampEnd = System.currentTimeMillis();
			logger.info("提交至主机返回耗时 ："+ (standardStampEnd - standardStampStart) + "毫秒");
			if ((standardStampEnd - standardStampStart) > 60000) {
				
				logger.info("提交主机超时:[ROLLBACK]");
				
			} else {
				if(this.fxTradeMapper.updateFxStatusByChoisRefHisNo(fxStatus)>0)
				{
					logger.info("CHOIS_REF_NO["+fxStatus.getChoisHisNo()+"]TABLENAME["+fxStatus.getTablename()+"]进行重发更新[SL_FXDH_STATUS]成功.");
				}else {
					logger.info("CHOIS_REF_NO["+fxStatus.getChoisHisNo()+"]TABLENAME["+fxStatus.getTablename()+"]进行重发更新[SL_FXDH_STATUS]失败.");
				}
				logger.info("提交主机成功:[COMMIT]");
				
			}
		
		

		return 0;
	}


	


}