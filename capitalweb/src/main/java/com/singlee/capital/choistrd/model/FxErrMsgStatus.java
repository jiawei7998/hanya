package com.singlee.capital.choistrd.model;

public class FxErrMsgStatus {

	private int status;
	
	private Fx fx;
	
	public FxErrMsgStatus(){}
	
	
	public FxErrMsgStatus(int status, Fx fx) {
		super();
		this.status = status;
		this.fx = fx;
	}
	/**
	 * @return the fx
	 */
	public Fx getFx() {
		return fx;
	}


	/**
	 * @param fx the fx to set
	 */
	public void setFx(Fx fx) {
		this.fx = fx;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
