package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class CustRecieve implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String FOIQ11_CIX_NO;
	private String FOIQ11_BIC_CD;
	private String FOIQ11_CTRY_CD;
	private String FOIQ11_CUST_SNM;
	private String FOIQ11_CUST_ENM;
	private String FOIQ11_ADDR_ENM;
	private String FOIQ11_POST_NO;
	private String FOIQ11_LST_IL;
	private String FOIQ11_HEAD_CTRY_CD;
	public String getFOIQ11_CIX_NO() {
		return FOIQ11_CIX_NO;
	}
	public void setFOIQ11_CIX_NO(String fOIQ11_CIX_NO) {
		FOIQ11_CIX_NO = fOIQ11_CIX_NO;
	}
	public String getFOIQ11_BIC_CD() {
		return FOIQ11_BIC_CD;
	}
	public void setFOIQ11_BIC_CD(String fOIQ11_BIC_CD) {
		FOIQ11_BIC_CD = fOIQ11_BIC_CD;
	}
	public String getFOIQ11_CTRY_CD() {
		return FOIQ11_CTRY_CD;
	}
	public void setFOIQ11_CTRY_CD(String fOIQ11_CTRY_CD) {
		FOIQ11_CTRY_CD = fOIQ11_CTRY_CD;
	}
	public String getFOIQ11_CUST_SNM() {
		return FOIQ11_CUST_SNM;
	}
	public void setFOIQ11_CUST_SNM(String fOIQ11_CUST_SNM) {
		FOIQ11_CUST_SNM = fOIQ11_CUST_SNM;
	}
	public String getFOIQ11_CUST_ENM() {
		return FOIQ11_CUST_ENM;
	}
	public void setFOIQ11_CUST_ENM(String fOIQ11_CUST_ENM) {
		FOIQ11_CUST_ENM = fOIQ11_CUST_ENM;
	}
	public String getFOIQ11_ADDR_ENM() {
		return FOIQ11_ADDR_ENM;
	}
	public void setFOIQ11_ADDR_ENM(String fOIQ11_ADDR_ENM) {
		FOIQ11_ADDR_ENM = fOIQ11_ADDR_ENM;
	}
	public String getFOIQ11_POST_NO() {
		return FOIQ11_POST_NO;
	}
	public void setFOIQ11_POST_NO(String fOIQ11_POST_NO) {
		FOIQ11_POST_NO = fOIQ11_POST_NO;
	}
	public String getFOIQ11_LST_IL() {
		return FOIQ11_LST_IL;
	}
	public void setFOIQ11_LST_IL(String fOIQ11_LST_IL) {
		FOIQ11_LST_IL = fOIQ11_LST_IL;
	}
	public String getFOIQ11_HEAD_CTRY_CD() {
		return FOIQ11_HEAD_CTRY_CD;
	}
	public void setFOIQ11_HEAD_CTRY_CD(String fOIQ11_HEAD_CTRY_CD) {
		FOIQ11_HEAD_CTRY_CD = fOIQ11_HEAD_CTRY_CD;
	}
}
