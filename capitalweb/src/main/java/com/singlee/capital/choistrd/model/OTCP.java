package com.singlee.capital.choistrd.model;


import java.io.Serializable;
import java.math.BigDecimal;
public class OTCP implements Serializable {
private static final long serialVersionUID = 1L;
private String br;

private String dealno;

private String seq;

private String product;

private String prodtype;

private String otctype;

private BigDecimal delta_8;

private BigDecimal theta_8;

private BigDecimal gamma_8;

private BigDecimal vega_8;

private BigDecimal mktpremamt;

private BigDecimal mktpremperc_8;

private BigDecimal volatility_8;

private BigDecimal uaprice_8;

private String interfacenum;

private BigDecimal rho_8;

private BigDecimal vanna_8;

private BigDecimal phi_8;

private BigDecimal hedge;

private BigDecimal forward_8;

private BigDecimal depo_8;

private BigDecimal ctrdepo_8;

private BigDecimal histspot_8;

private BigDecimal histforward_8;

private BigDecimal mktpremval;

private BigDecimal profit;

private String text1;

private String text2;

private String text3;

private BigDecimal rate1_8;

private BigDecimal rate2_8;

private BigDecimal rate3_8;

private BigDecimal amt1;

private BigDecimal amt2;

private String date1;

private String date2;

private BigDecimal impliedqty;

private BigDecimal marketpremamt;

private BigDecimal ystmktpremamt;

private BigDecimal tdyintrinsicval;

private BigDecimal ystintrinsicval;

private BigDecimal ccydeltaamt;

private BigDecimal ctrdeltaamt;

private BigDecimal basemktpremamt;

public String getBr() {
	return br;
}

public void setBr(String br) {
	this.br = br;
}

public String getDealno() {
	return dealno;
}

public void setDealno(String dealno) {
	this.dealno = dealno;
}

public String getSeq() {
	return seq;
}

public void setSeq(String seq) {
	this.seq = seq;
}

public String getProduct() {
	return product;
}

public void setProduct(String product) {
	this.product = product;
}

public String getProdtype() {
	return prodtype;
}

public void setProdtype(String prodtype) {
	this.prodtype = prodtype;
}

public String getOtctype() {
	return otctype;
}

public void setOtctype(String otctype) {
	this.otctype = otctype;
}

public BigDecimal getDelta_8() {
	return delta_8;
}

public void setDelta_8(BigDecimal delta_8) {
	this.delta_8 = delta_8;
}

public BigDecimal getTheta_8() {
	return theta_8;
}

public void setTheta_8(BigDecimal theta_8) {
	this.theta_8 = theta_8;
}

public BigDecimal getGamma_8() {
	return gamma_8;
}

public void setGamma_8(BigDecimal gamma_8) {
	this.gamma_8 = gamma_8;
}

public BigDecimal getVega_8() {
	return vega_8;
}

public void setVega_8(BigDecimal vega_8) {
	this.vega_8 = vega_8;
}

public BigDecimal getMktpremamt() {
	return mktpremamt;
}

public void setMktpremamt(BigDecimal mktpremamt) {
	this.mktpremamt = mktpremamt;
}

public BigDecimal getMktpremperc_8() {
	return mktpremperc_8;
}

public void setMktpremperc_8(BigDecimal mktpremperc_8) {
	this.mktpremperc_8 = mktpremperc_8;
}

public BigDecimal getVolatility_8() {
	return volatility_8;
}

public void setVolatility_8(BigDecimal volatility_8) {
	this.volatility_8 = volatility_8;
}

public BigDecimal getUaprice_8() {
	return uaprice_8;
}

public void setUaprice_8(BigDecimal uaprice_8) {
	this.uaprice_8 = uaprice_8;
}

public String getInterfacenum() {
	return interfacenum;
}

public void setInterfacenum(String interfacenum) {
	this.interfacenum = interfacenum;
}

public BigDecimal getRho_8() {
	return rho_8;
}

public void setRho_8(BigDecimal rho_8) {
	this.rho_8 = rho_8;
}

public BigDecimal getVanna_8() {
	return vanna_8;
}

public void setVanna_8(BigDecimal vanna_8) {
	this.vanna_8 = vanna_8;
}

public BigDecimal getPhi_8() {
	return phi_8;
}

public void setPhi_8(BigDecimal phi_8) {
	this.phi_8 = phi_8;
}

public BigDecimal getHedge() {
	return hedge;
}

public void setHedge(BigDecimal hedge) {
	this.hedge = hedge;
}

public BigDecimal getForward_8() {
	return forward_8;
}

public void setForward_8(BigDecimal forward_8) {
	this.forward_8 = forward_8;
}

public BigDecimal getDepo_8() {
	return depo_8;
}

public void setDepo_8(BigDecimal depo_8) {
	this.depo_8 = depo_8;
}

public BigDecimal getCtrdepo_8() {
	return ctrdepo_8;
}

public void setCtrdepo_8(BigDecimal ctrdepo_8) {
	this.ctrdepo_8 = ctrdepo_8;
}

public BigDecimal getHistspot_8() {
	return histspot_8;
}

public void setHistspot_8(BigDecimal histspot_8) {
	this.histspot_8 = histspot_8;
}

public BigDecimal getHistforward_8() {
	return histforward_8;
}

public void setHistforward_8(BigDecimal histforward_8) {
	this.histforward_8 = histforward_8;
}

public BigDecimal getMktpremval() {
	return mktpremval;
}

public void setMktpremval(BigDecimal mktpremval) {
	this.mktpremval = mktpremval;
}

public BigDecimal getProfit() {
	return profit;
}

public void setProfit(BigDecimal profit) {
	this.profit = profit;
}

public String getText1() {
	return text1;
}

public void setText1(String text1) {
	this.text1 = text1;
}

public String getText2() {
	return text2;
}

public void setText2(String text2) {
	this.text2 = text2;
}

public String getText3() {
	return text3;
}

public void setText3(String text3) {
	this.text3 = text3;
}

public BigDecimal getRate1_8() {
	return rate1_8;
}

public void setRate1_8(BigDecimal rate1_8) {
	this.rate1_8 = rate1_8;
}

public BigDecimal getRate2_8() {
	return rate2_8;
}

public void setRate2_8(BigDecimal rate2_8) {
	this.rate2_8 = rate2_8;
}

public BigDecimal getRate3_8() {
	return rate3_8;
}

public void setRate3_8(BigDecimal rate3_8) {
	this.rate3_8 = rate3_8;
}

public BigDecimal getAmt1() {
	return amt1;
}

public void setAmt1(BigDecimal amt1) {
	this.amt1 = amt1;
}

public BigDecimal getAmt2() {
	return amt2;
}

public void setAmt2(BigDecimal amt2) {
	this.amt2 = amt2;
}

public String getDate1() {
	return date1;
}

public void setDate1(String date1) {
	this.date1 = date1;
}

public String getDate2() {
	return date2;
}

public void setDate2(String date2) {
	this.date2 = date2;
}

public BigDecimal getImpliedqty() {
	return impliedqty;
}

public void setImpliedqty(BigDecimal impliedqty) {
	this.impliedqty = impliedqty;
}

public BigDecimal getMarketpremamt() {
	return marketpremamt;
}

public void setMarketpremamt(BigDecimal marketpremamt) {
	this.marketpremamt = marketpremamt;
}

public BigDecimal getYstmktpremamt() {
	return ystmktpremamt;
}

public void setYstmktpremamt(BigDecimal ystmktpremamt) {
	this.ystmktpremamt = ystmktpremamt;
}

public BigDecimal getTdyintrinsicval() {
	return tdyintrinsicval;
}

public void setTdyintrinsicval(BigDecimal tdyintrinsicval) {
	this.tdyintrinsicval = tdyintrinsicval;
}

public BigDecimal getYstintrinsicval() {
	return ystintrinsicval;
}

public void setYstintrinsicval(BigDecimal ystintrinsicval) {
	this.ystintrinsicval = ystintrinsicval;
}

public BigDecimal getCcydeltaamt() {
	return ccydeltaamt;
}

public void setCcydeltaamt(BigDecimal ccydeltaamt) {
	this.ccydeltaamt = ccydeltaamt;
}

public BigDecimal getCtrdeltaamt() {
	return ctrdeltaamt;
}

public void setCtrdeltaamt(BigDecimal ctrdeltaamt) {
	this.ctrdeltaamt = ctrdeltaamt;
}

public BigDecimal getBasemktpremamt() {
	return basemktpremamt;
}

public void setBasemktpremamt(BigDecimal basemktpremamt) {
	this.basemktpremamt = basemktpremamt;
}


}