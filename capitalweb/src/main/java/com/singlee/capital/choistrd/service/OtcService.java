package com.singlee.capital.choistrd.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.OTCP;
import com.singlee.capital.choistrd.model.OTDT;

public interface OtcService {
	/**
	 * 按照头信息的DEALNO号去发送它的明细交易信息
	 * @param dealno
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryAllOTCPListIntoWebtFieldSet(Map<String, Object> mapn) throws Exception;

	/**
	 * VERIFY
	 * @param dealno
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryVerifyOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * VERIFY
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryVerifyOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 将需要发送的OTDT数据打包成WebtFieldSet
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryAllOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * VERIFY
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryVerifyRevOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 将需要发送的OTDT数据打包成WebtFieldSet
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryAllOTDRevTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 按照头信息的DEALNO号去发送它的明细交易信息
	 * @param dealno
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryAllRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;

	/**
	 * VERIFY
	 * @param dealno
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryVerifyRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
}
