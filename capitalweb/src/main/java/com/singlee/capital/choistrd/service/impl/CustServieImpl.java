package com.singlee.capital.choistrd.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.SubStrChinese;
import com.singlee.capital.choistrd.model.CustRecieve;
import com.singlee.capital.choistrd.service.CustService;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.ifs.mapper.IfsBondPledgeMapper;

@Service
public class CustServieImpl implements CustService{
	
	@Autowired
	IfsBondPledgeMapper ifsBondPledgeMapper;
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	
	ChiosLogin chiosLogin = new ChiosLogin();
	
	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();
	
	public String FXFIG_SSCK;
	public String eaiDv = AdapterConstants.EAI_DOM_SVR;
	SubStrChinese subStrChinese=new SubStrChinese();
	public boolean executeHostVisitAction() throws Exception {
		// TODO Auto-generated method stub
		CustRecieve custRecieve = new CustRecieve();
		boolean retFlag = true;
		String returnFlag = "";
		try {
				long standardStampStart = System.currentTimeMillis();
				
				String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
				StdTMsgGenerator generator = new StdTMsgGenerator();
				byte[] headerPacket=choisCommonMsg.makeHead();
				
				
				String handleCode="IQ1100";
				String serveHandleCode="OPCIQ11";
				PropertiesConfiguration stdtmsgSysValue;
				
				stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
				 
			
				FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
				
				String coMsg=choisCommonMsg.makeMsg();
				String coMsg2=choisCommonMsg.makeMsg2();
				String coMsg3=choisCommonMsg.makeMsg3();
				
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				String dealMsg="3"+sdf.format(date);
//				String dealMsg="3"+"2022-03-11";
				
				byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
				byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
				String serviceUrl = "OPS";
				byte[] msgData =totalPacket;
				EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
				System.out.println(":--"+eaiDv);
				byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
				System.out.println("FX��ֵ������Ϣ��"+new String(returnData, "UTF-8"));
				int num = 0;
				for(int i = 0; i < (new String(returnData, "UTF-8")).length(); i++) {
					char c = new String(returnData, "UTF-8").charAt(i);
					if(subStrChinese.isChinese(c)) {
						num+=1;
					}
				}
				int len=(new String(returnData, "UTF-8")).length()+num*2;
				
				logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
			try {
				logger.info("RECV MSG:"+new String(returnData, "UTF-8"));
				String recMsg = new String(returnData,"utf-8");
				logger.info("==============:"+recMsg);
				
				returnFlag = new String(returnData, "UTF-8").substring(290,292);
				logger.info("==============:"+returnFlag);
				int endIndex = recMsg.lastIndexOf("OPCIQ11");
				String custInfo = recMsg.substring(endIndex+7, recMsg.indexOf("@"));
				
				if(returnFlag.equals("EM")) {
					logger.info("接收返回的错误报文:"+new String(returnData, "UTF-8"));
				}else { 
					int count = (len-2-385)/1689;
					for(int i = 0; i<count;i++) {
						custRecieve.setFOIQ11_CIX_NO(subStrChinese.subChString(custInfo,0+1689*i,8).trim());
						custRecieve.setFOIQ11_BIC_CD(subStrChinese.subChString(custInfo,8+1689*i,11).trim());
						custRecieve.setFOIQ11_CTRY_CD(subStrChinese.subChString(custInfo,19+1689*i,2).trim());
						custRecieve.setFOIQ11_CUST_SNM(subStrChinese.subChString(custInfo,21+1689*i,150).trim());
						custRecieve.setFOIQ11_CUST_ENM(subStrChinese.subChString(custInfo,171+1689*i,300).trim());
						custRecieve.setFOIQ11_ADDR_ENM(subStrChinese.subChString(custInfo,771+1689*i,300).trim());
						custRecieve.setFOIQ11_POST_NO(subStrChinese.subChString(custInfo,1671+1689*i,6).trim());
						custRecieve.setFOIQ11_LST_IL(subStrChinese.subChString(custInfo,1677+1689*i,10).trim());
						custRecieve.setFOIQ11_HEAD_CTRY_CD(subStrChinese.subChString(custInfo,1687+1689*i,2).trim());
						ifsBondPledgeMapper.insertCust(custRecieve);
					}
				}
				
				} catch (Exception e) {
				retFlag = false;
				
				e.printStackTrace();
				logger.info("Exception MSG:"+e.getMessage());
				
				chiosLogin.main();
				throw e;
				
			}
			long standardStampEnd = System.currentTimeMillis();
		} catch (Exception e) {
			retFlag = false;
			logger.info("ROLLBACK");
			logger.info("Exception:"+e.getMessage());
			e.printStackTrace();
			chiosLogin.main();
			throw e;
		} finally{
		}
		return retFlag;
	}
}
