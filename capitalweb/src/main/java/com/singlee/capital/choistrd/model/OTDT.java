package com.singlee.capital.choistrd.model;


import java.io.Serializable;
import java.math.BigDecimal;
public class OTDT implements Serializable {
private static final long serialVersionUID = 1L;
private String BR;
public String getBR() {
return BR;
}
public void setBR(String BR) {
this.BR = BR;
}
private String DEALNO;
public String getDEALNO() {
return DEALNO;
}
public void setDEALNO(String DEALNO) {
this.DEALNO = DEALNO;
}
private String SEQ;
public String getSEQ() {
return SEQ;
}
public void setSEQ(String SEQ) {
this.SEQ = SEQ;
}
private String PRODUCT;
public String getPRODUCT() {
return PRODUCT;
}
public void setPRODUCT(String PRODUCT) {
this.PRODUCT = PRODUCT;
}
private String PRODTYPE;
public String getPRODTYPE() {
return PRODTYPE;
}
public void setPRODTYPE(String PRODTYPE) {
this.PRODTYPE = PRODTYPE;
}
private BigDecimal AVGRATE_8;
public BigDecimal getAVGRATE_8() {
return AVGRATE_8;
}
public void setAVGRATE_8(BigDecimal AVGRATE_8) {
this.AVGRATE_8 = AVGRATE_8;
}
private BigDecimal AVGSTKE_8;
public BigDecimal getAVGSTKE_8() {
return AVGSTKE_8;
}
public void setAVGSTKE_8(BigDecimal AVGSTKE_8) {
this.AVGSTKE_8 = AVGSTKE_8;
}
private String AVGSTKEEDATE;
public String getAVGSTKEEDATE() {
return AVGSTKEEDATE;
}
public void setAVGSTKEEDATE(String AVGSTKEEDATE) {
this.AVGSTKEEDATE = AVGSTKEEDATE;
}
private String AVGSTKESDATE;
public String getAVGSTKESDATE() {
return AVGSTKESDATE;
}
public void setAVGSTKESDATE(String AVGSTKESDATE) {
this.AVGSTKESDATE = AVGSTKESDATE;
}
private BigDecimal CCYPREMBAMT;
public BigDecimal getCCYPREMBAMT() {
return CCYPREMBAMT;
}
public void setCCYPREMBAMT(BigDecimal CCYPREMBAMT) {
this.CCYPREMBAMT = CCYPREMBAMT;
}
private BigDecimal BINARY;
public BigDecimal getBINARY() {
return BINARY;
}
public void setBINARY(BigDecimal BINARY) {
this.BINARY = BINARY;
}
private String BROK;
public String getBROK() {
return BROK;
}
public void setBROK(String BROK) {
this.BROK = BROK;
}
private String CCY;
public String getCCY() {
return CCY;
}
public void setCCY(String CCY) {
this.CCY = CCY;
}
private String CCYCP;
public String getCCYCP() {
return CCYCP;
}
public void setCCYCP(String CCYCP) {
this.CCYCP = CCYCP;
}
private BigDecimal CCYPREM;
public BigDecimal getCCYPREM() {
return CCYPREM;
}
public void setCCYPREM(BigDecimal CCYPREM) {
this.CCYPREM = CCYPREM;
}
private BigDecimal CCYPREMAMT;
public BigDecimal getCCYPREMAMT() {
return CCYPREMAMT;
}
public void setCCYPREMAMT(BigDecimal CCYPREMAMT) {
this.CCYPREMAMT = CCYPREMAMT;
}
private BigDecimal CCYREVALAMT;
public BigDecimal getCCYREVALAMT() {
return CCYREVALAMT;
}
public void setCCYREVALAMT(BigDecimal CCYREVALAMT) {
this.CCYREVALAMT = CCYREVALAMT;
}
private BigDecimal CCYPREMRATE_8;
public BigDecimal getCCYPREMRATE_8() {
return CCYPREMRATE_8;
}
public void setCCYPREMRATE_8(BigDecimal CCYPREMRATE_8) {
this.CCYPREMRATE_8 = CCYPREMRATE_8;
}
private BigDecimal CCYPRIN;
public BigDecimal getCCYPRIN() {
return CCYPRIN;
}
public void setCCYPRIN(BigDecimal CCYPRIN) {
this.CCYPRIN = CCYPRIN;
}
private BigDecimal CONTSIZE;
public BigDecimal getCONTSIZE() {
return CONTSIZE;
}
public void setCONTSIZE(BigDecimal CONTSIZE) {
this.CONTSIZE = CONTSIZE;
}
private String COST;
public String getCOST() {
return COST;
}
public void setCOST(String COST) {
this.COST = COST;
}
private String CTRCCY;
public String getCTRCCY() {
return CTRCCY;
}
public void setCTRCCY(String CTRCCY) {
this.CTRCCY = CTRCCY;
}
private String CTRCP;
public String getCTRCP() {
return CTRCP;
}
public void setCTRCP(String CTRCP) {
this.CTRCP = CTRCP;
}
private BigDecimal CTRPREM;
public BigDecimal getCTRPREM() {
return CTRPREM;
}
public void setCTRPREM(BigDecimal CTRPREM) {
this.CTRPREM = CTRPREM;
}
private BigDecimal CTRPREMAMT;
public BigDecimal getCTRPREMAMT() {
return CTRPREMAMT;
}
public void setCTRPREMAMT(BigDecimal CTRPREMAMT) {
this.CTRPREMAMT = CTRPREMAMT;
}
private BigDecimal CTRREVALAMT;
public BigDecimal getCTRREVALAMT() {
return CTRREVALAMT;
}
public void setCTRREVALAMT(BigDecimal CTRREVALAMT) {
this.CTRREVALAMT = CTRREVALAMT;
}
private BigDecimal CTRPREMRATE_8;
public BigDecimal getCTRPREMRATE_8() {
return CTRPREMRATE_8;
}
public void setCTRPREMRATE_8(BigDecimal CTRPREMRATE_8) {
this.CTRPREMRATE_8 = CTRPREMRATE_8;
}
private BigDecimal CTRPRIN;
public BigDecimal getCTRPRIN() {
return CTRPRIN;
}
public void setCTRPRIN(BigDecimal CTRPRIN) {
this.CTRPRIN = CTRPRIN;
}
private String CNO;
public String getCNO() {
return CNO;
}
public void setCNO(String CNO) {
this.CNO = CNO;
}
private String CUTOFF;
public String getCUTOFF() {
return CUTOFF;
}
public void setCUTOFF(String CUTOFF) {
this.CUTOFF = CUTOFF;
}
private String DEALDATE;
public String getDEALDATE() {
return DEALDATE;
}
public void setDEALDATE(String DEALDATE) {
this.DEALDATE = DEALDATE;
}
private String DEALVDATE;
public String getDEALVDATE() {
return DEALVDATE;
}
public void setDEALVDATE(String DEALVDATE) {
this.DEALVDATE = DEALVDATE;
}
private String DEALTEXT;
public String getDEALTEXT() {
return DEALTEXT;
}
public void setDEALTEXT(String DEALTEXT) {
this.DEALTEXT = DEALTEXT;
}
private String DELIVERY;
public String getDELIVERY() {
return DELIVERY;
}
public void setDELIVERY(String DELIVERY) {
this.DELIVERY = DELIVERY;
}
private BigDecimal DELTA_8;
public BigDecimal getDELTA_8() {
return DELTA_8;
}
public void setDELTA_8(BigDecimal DELTA_8) {
this.DELTA_8 = DELTA_8;
}
private BigDecimal DKITRIG1_8;
public BigDecimal getDKITRIG1_8() {
return DKITRIG1_8;
}
public void setDKITRIG1_8(BigDecimal DKITRIG1_8) {
this.DKITRIG1_8 = DKITRIG1_8;
}
private BigDecimal DKITRIG2_8;
public BigDecimal getDKITRIG2_8() {
return DKITRIG2_8;
}
public void setDKITRIG2_8(BigDecimal DKITRIG2_8) {
this.DKITRIG2_8 = DKITRIG2_8;
}
private BigDecimal DKOTRIG1_8;
public BigDecimal getDKOTRIG1_8() {
return DKOTRIG1_8;
}
public void setDKOTRIG1_8(BigDecimal DKOTRIG1_8) {
this.DKOTRIG1_8 = DKOTRIG1_8;
}
private BigDecimal DKOTRIG2_8;
public BigDecimal getDKOTRIG2_8() {
return DKOTRIG2_8;
}
public void setDKOTRIG2_8(BigDecimal DKOTRIG2_8) {
this.DKOTRIG2_8 = DKOTRIG2_8;
}
private String EXERDATE;
public String getEXERDATE() {
return EXERDATE;
}
public void setEXERDATE(String EXERDATE) {
this.EXERDATE = EXERDATE;
}
private String EXERIND;
public String getEXERIND() {
return EXERIND;
}
public void setEXERIND(String EXERIND) {
this.EXERIND = EXERIND;
}
private String EXOTIC;
public String getEXOTIC() {
return EXOTIC;
}
public void setEXOTIC(String EXOTIC) {
this.EXOTIC = EXOTIC;
}
private String EXPDATE;
public String getEXPDATE() {
return EXPDATE;
}
public void setEXPDATE(String EXPDATE) {
this.EXPDATE = EXPDATE;
}
private BigDecimal FEEAMT;
public BigDecimal getFEEAMT() {
return FEEAMT;
}
public void setFEEAMT(BigDecimal FEEAMT) {
this.FEEAMT = FEEAMT;
}
private String FEECCY;
public String getFEECCY() {
return FEECCY;
}
public void setFEECCY(String FEECCY) {
this.FEECCY = FEECCY;
}
private String INPUTDATE;
public String getINPUTDATE() {
return INPUTDATE;
}
public void setINPUTDATE(String INPUTDATE) {
this.INPUTDATE = INPUTDATE;
}
private String INPUTTIME;
public String getINPUTTIME() {
return INPUTTIME;
}
public void setINPUTTIME(String INPUTTIME) {
this.INPUTTIME = INPUTTIME;
}
private String IOPER;
public String getIOPER() {
return IOPER;
}
public void setIOPER(String IOPER) {
this.IOPER = IOPER;
}
private BigDecimal KITRIG_8;
public BigDecimal getKITRIG_8() {
return KITRIG_8;
}
public void setKITRIG_8(BigDecimal KITRIG_8) {
this.KITRIG_8 = KITRIG_8;
}
private BigDecimal KOTRIG_8;
public BigDecimal getKOTRIG_8() {
return KOTRIG_8;
}
public void setKOTRIG_8(BigDecimal KOTRIG_8) {
this.KOTRIG_8 = KOTRIG_8;
}
private String LINKDEALNO;
public String getLINKDEALNO() {
return LINKDEALNO;
}
public void setLINKDEALNO(String LINKDEALNO) {
this.LINKDEALNO = LINKDEALNO;
}
private String LINKPRODUCT;
public String getLINKPRODUCT() {
return LINKPRODUCT;
}
public void setLINKPRODUCT(String LINKPRODUCT) {
this.LINKPRODUCT = LINKPRODUCT;
}
private String LINKPRODTYPE;
public String getLINKPRODTYPE() {
return LINKPRODTYPE;
}
public void setLINKPRODTYPE(String LINKPRODTYPE) {
this.LINKPRODTYPE = LINKPRODTYPE;
}
private String LSTMNTDATE;
public String getLSTMNTDATE() {
return LSTMNTDATE;
}
public void setLSTMNTDATE(String LSTMNTDATE) {
this.LSTMNTDATE = LSTMNTDATE;
}
private String OTCTYPE;
public String getOTCTYPE() {
return OTCTYPE;
}
public void setOTCTYPE(String OTCTYPE) {
this.OTCTYPE = OTCTYPE;
}
private String PREMAUTHDTE;
public String getPREMAUTHDTE() {
return PREMAUTHDTE;
}
public void setPREMAUTHDTE(String PREMAUTHDTE) {
this.PREMAUTHDTE = PREMAUTHDTE;
}
private String PREMAUTHIND;
public String getPREMAUTHIND() {
return PREMAUTHIND;
}
public void setPREMAUTHIND(String PREMAUTHIND) {
this.PREMAUTHIND = PREMAUTHIND;
}
private String PREMAUTHOPER;
public String getPREMAUTHOPER() {
return PREMAUTHOPER;
}
public void setPREMAUTHOPER(String PREMAUTHOPER) {
this.PREMAUTHOPER = PREMAUTHOPER;
}
private String PREMSMEANS;
public String getPREMSMEANS() {
return PREMSMEANS;
}
public void setPREMSMEANS(String PREMSMEANS) {
this.PREMSMEANS = PREMSMEANS;
}
private String PREMSACCT;
public String getPREMSACCT() {
return PREMSACCT;
}
public void setPREMSACCT(String PREMSACCT) {
this.PREMSACCT = PREMSACCT;
}
private String PREMSOPER;
public String getPREMSOPER() {
return PREMSOPER;
}
public void setPREMSOPER(String PREMSOPER) {
this.PREMSOPER = PREMSOPER;
}
private String PLMETHOD;
public String getPLMETHOD() {
return PLMETHOD;
}
public void setPLMETHOD(String PLMETHOD) {
this.PLMETHOD = PLMETHOD;
}
private String PORT;
public String getPORT() {
return PORT;
}
public void setPORT(String PORT) {
this.PORT = PORT;
}
private String PS;
public String getPS() {
return PS;
}
public void setPS(String PS) {
this.PS = PS;
}
private BigDecimal QTY;
public BigDecimal getQTY() {
return QTY;
}
public void setQTY(BigDecimal QTY) {
this.QTY = QTY;
}
private BigDecimal QUANTO_8;
public BigDecimal getQUANTO_8() {
return QUANTO_8;
}
public void setQUANTO_8(BigDecimal QUANTO_8) {
this.QUANTO_8 = QUANTO_8;
}
private String REVDATE;
public String getREVDATE() {
return REVDATE;
}
public void setREVDATE(String REVDATE) {
this.REVDATE = REVDATE;
}
private String REVREASON;
public String getREVREASON() {
return REVREASON;
}
public void setREVREASON(String REVREASON) {
this.REVREASON = REVREASON;
}
private String REVTEXT;
public String getREVTEXT() {
return REVTEXT;
}
public void setREVTEXT(String REVTEXT) {
this.REVTEXT = REVTEXT;
}
private String SECID;
public String getSECID() {
return SECID;
}
public void setSECID(String SECID) {
this.SECID = SECID;
}
private String SETTLEDTE;
public String getSETTLEDTE() {
return SETTLEDTE;
}
public void setSETTLEDTE(String SETTLEDTE) {
this.SETTLEDTE = SETTLEDTE;
}
private String SHPEROPT;
public String getSHPEROPT() {
return SHPEROPT;
}
public void setSHPEROPT(String SHPEROPT) {
this.SHPEROPT = SHPEROPT;
}
private String SIIND;
public String getSIIND() {
return SIIND;
}
public void setSIIND(String SIIND) {
this.SIIND = SIIND;
}
private BigDecimal STRIKE_8;
public BigDecimal getSTRIKE_8() {
return STRIKE_8;
}
public void setSTRIKE_8(BigDecimal STRIKE_8) {
this.STRIKE_8 = STRIKE_8;
}
private String STYLE;
public String getSTYLE() {
return STYLE;
}
public void setSTYLE(String STYLE) {
this.STYLE = STYLE;
}
private String SYMBOL;
public String getSYMBOL() {
return SYMBOL;
}
public void setSYMBOL(String SYMBOL) {
this.SYMBOL = SYMBOL;
}
private String TENOR;
public String getTENOR() {
return TENOR;
}
public void setTENOR(String TENOR) {
this.TENOR = TENOR;
}
private String TERMS;
public String getTERMS() {
return TERMS;
}
public void setTERMS(String TERMS) {
this.TERMS = TERMS;
}
private String TRAD;
public String getTRAD() {
return TRAD;
}
public void setTRAD(String TRAD) {
this.TRAD = TRAD;
}
private String UNIT;
public String getUNIT() {
return UNIT;
}
public void setUNIT(String UNIT) {
this.UNIT = UNIT;
}
private String VERDATE;
public String getVERDATE() {
return VERDATE;
}
public void setVERDATE(String VERDATE) {
this.VERDATE = VERDATE;
}
private String VERIND;
public String getVERIND() {
return VERIND;
}
public void setVERIND(String VERIND) {
this.VERIND = VERIND;
}
private BigDecimal VOLATILITY_8;
public BigDecimal getVOLATILITY_8() {
return VOLATILITY_8;
}
public void setVOLATILITY_8(BigDecimal VOLATILITY_8) {
this.VOLATILITY_8 = VOLATILITY_8;
}
private String VOPER;
public String getVOPER() {
return VOPER;
}
public void setVOPER(String VOPER) {
this.VOPER = VOPER;
}
private String DEALSRCE;
public String getDEALSRCE() {
return DEALSRCE;
}
public void setDEALSRCE(String DEALSRCE) {
this.DEALSRCE = DEALSRCE;
}
private String FINCNTR1;
public String getFINCNTR1() {
return FINCNTR1;
}
public void setFINCNTR1(String FINCNTR1) {
this.FINCNTR1 = FINCNTR1;
}
private String FINCNTR2;
public String getFINCNTR2() {
return FINCNTR2;
}
public void setFINCNTR2(String FINCNTR2) {
this.FINCNTR2 = FINCNTR2;
}
private String UNDTENOR;
public String getUNDTENOR() {
return UNDTENOR;
}
public void setUNDTENOR(String UNDTENOR) {
this.UNDTENOR = UNDTENOR;
}
private String UNDACCTNGTYPE;
public String getUNDACCTNGTYPE() {
return UNDACCTNGTYPE;
}
public void setUNDACCTNGTYPE(String UNDACCTNGTYPE) {
this.UNDACCTNGTYPE = UNDACCTNGTYPE;
}
private String STARTDATE;
public String getSTARTDATE() {
return STARTDATE;
}
public void setSTARTDATE(String STARTDATE) {
this.STARTDATE = STARTDATE;
}
private String ENDDATE;
public String getENDDATE() {
return ENDDATE;
}
public void setENDDATE(String ENDDATE) {
this.ENDDATE = ENDDATE;
}
private BigDecimal SPOTRATE_8;
public BigDecimal getSPOTRATE_8() {
return SPOTRATE_8;
}
public void setSPOTRATE_8(BigDecimal SPOTRATE_8) {
this.SPOTRATE_8 = SPOTRATE_8;
}
private BigDecimal CCYBRATE_8;
public BigDecimal getCCYBRATE_8() {
return CCYBRATE_8;
}
public void setCCYBRATE_8(BigDecimal CCYBRATE_8) {
this.CCYBRATE_8 = CCYBRATE_8;
}
private String CCYBTERMS;
public String getCCYBTERMS() {
return CCYBTERMS;
}
public void setCCYBTERMS(String CCYBTERMS) {
this.CCYBTERMS = CCYBTERMS;
}
private BigDecimal CTRBRATE_8;
public BigDecimal getCTRBRATE_8() {
return CTRBRATE_8;
}
public void setCTRBRATE_8(BigDecimal CTRBRATE_8) {
this.CTRBRATE_8 = CTRBRATE_8;
}
private String CTRBTERMS;
public String getCTRBTERMS() {
return CTRBTERMS;
}
public void setCTRBTERMS(String CTRBTERMS) {
this.CTRBTERMS = CTRBTERMS;
}
private BigDecimal CCYINTRATE_8;
public BigDecimal getCCYINTRATE_8() {
return CCYINTRATE_8;
}
public void setCCYINTRATE_8(BigDecimal CCYINTRATE_8) {
this.CCYINTRATE_8 = CCYINTRATE_8;
}
private BigDecimal CTRINTRATE_8;
public BigDecimal getCTRINTRATE_8() {
return CTRINTRATE_8;
}
public void setCTRINTRATE_8(BigDecimal CTRINTRATE_8) {
this.CTRINTRATE_8 = CTRINTRATE_8;
}
private String PREMCCY;
public String getPREMCCY() {
return PREMCCY;
}
public void setPREMCCY(String PREMCCY) {
this.PREMCCY = PREMCCY;
}
private BigDecimal PREMAMT;
public BigDecimal getPREMAMT() {
return PREMAMT;
}
public void setPREMAMT(BigDecimal PREMAMT) {
this.PREMAMT = PREMAMT;
}
private BigDecimal PREMRATE_8;
public BigDecimal getPREMRATE_8() {
return PREMRATE_8;
}
public void setPREMRATE_8(BigDecimal PREMRATE_8) {
this.PREMRATE_8 = PREMRATE_8;
}
private String REVIND;
public String getREVIND() {
return REVIND;
}
public void setREVIND(String REVIND) {
this.REVIND = REVIND;
}
private String BINARYCCY;
public String getBINARYCCY() {
return BINARYCCY;
}
public void setBINARYCCY(String BINARYCCY) {
this.BINARYCCY = BINARYCCY;
}
private String UNDSEQ;
public String getUNDSEQ() {
return UNDSEQ;
}
public void setUNDSEQ(String UNDSEQ) {
this.UNDSEQ = UNDSEQ;
}
private String INTERFACENUM;
public String getINTERFACENUM() {
return INTERFACENUM;
}
public void setINTERFACENUM(String INTERFACENUM) {
this.INTERFACENUM = INTERFACENUM;
}
private String CONTEXT;
public String getCONTEXT() {
return CONTEXT;
}
public void setCONTEXT(String CONTEXT) {
this.CONTEXT = CONTEXT;
}
private String CONTEXT1;
public String getCONTEXT1() {
return CONTEXT1;
}
public void setCONTEXT1(String CONTEXT1) {
this.CONTEXT1 = CONTEXT1;
}
private String CONTEXT2;
public String getCONTEXT2() {
return CONTEXT2;
}
public void setCONTEXT2(String CONTEXT2) {
this.CONTEXT2 = CONTEXT2;
}
private String LOCATION;
public String getLOCATION() {
return LOCATION;
}
public void setLOCATION(String LOCATION) {
this.LOCATION = LOCATION;
}
private String FREQUENCY;
public String getFREQUENCY() {
return FREQUENCY;
}
public void setFREQUENCY(String FREQUENCY) {
this.FREQUENCY = FREQUENCY;
}
private String SOURCE;
public String getSOURCE() {
return SOURCE;
}
public void setSOURCE(String SOURCE) {
this.SOURCE = SOURCE;
}
private String REVALSOURCE;
public String getREVALSOURCE() {
return REVALSOURCE;
}
public void setREVALSOURCE(String REVALSOURCE) {
this.REVALSOURCE = REVALSOURCE;
}
private String PHONCI;
public String getPHONCI() {
return PHONCI;
}
public void setPHONCI(String PHONCI) {
this.PHONCI = PHONCI;
}
private String PHONECDATE;
public String getPHONECDATE() {
return PHONECDATE;
}
public void setPHONECDATE(String PHONECDATE) {
this.PHONECDATE = PHONECDATE;
}
private String PHONETEXT;
public String getPHONETEXT() {
return PHONETEXT;
}
public void setPHONETEXT(String PHONETEXT) {
this.PHONETEXT = PHONETEXT;
}
private String TEXT1;
public String getTEXT1() {
return TEXT1;
}
public void setTEXT1(String TEXT1) {
this.TEXT1 = TEXT1;
}
private String MASTERAGREEMENT;
public String getMASTERAGREEMENT() {
return MASTERAGREEMENT;
}
public void setTEXT2(String MASTERAGREEMENT) {
this.MASTERAGREEMENT = MASTERAGREEMENT;
}
private String TEXT3;
public String getTEXT3() {
return TEXT3;
}
public void setTEXT3(String TEXT3) {
this.TEXT3 = TEXT3;
}
private BigDecimal RATE1_8;
public BigDecimal getRATE1_8() {
return RATE1_8;
}
public void setRATE1_8(BigDecimal RATE1_8) {
this.RATE1_8 = RATE1_8;
}
private BigDecimal RATE2_8;
public BigDecimal getRATE2_8() {
return RATE2_8;
}
public void setRATE2_8(BigDecimal RATE2_8) {
this.RATE2_8 = RATE2_8;
}
private BigDecimal RATE3_8;
public BigDecimal getRATE3_8() {
return RATE3_8;
}
public void setRATE3_8(BigDecimal RATE3_8) {
this.RATE3_8 = RATE3_8;
}
private BigDecimal AMT1;
public BigDecimal getAMT1() {
return AMT1;
}
public void setAMT1(BigDecimal AMT1) {
this.AMT1 = AMT1;
}
private BigDecimal AMT2;
public BigDecimal getAMT2() {
return AMT2;
}
public void setAMT2(BigDecimal AMT2) {
this.AMT2 = AMT2;
}
private String DATE1;
public String getDATE1() {
return DATE1;
}
public void setDATE1(String DATE1) {
this.DATE1 = DATE1;
}
private String DATE2;
public String getDATE2() {
return DATE2;
}
public void setDATE2(String DATE2) {
this.DATE2 = DATE2;
}
private BigDecimal SALESCREDAMT;
public BigDecimal getSALESCREDAMT() {
return SALESCREDAMT;
}
public void setSALESCREDAMT(BigDecimal SALESCREDAMT) {
this.SALESCREDAMT = SALESCREDAMT;
}
private String SALESCREDCCY;
public String getSALESCREDCCY() {
return SALESCREDCCY;
}
public void setSALESCREDCCY(String SALESCREDCCY) {
this.SALESCREDCCY = SALESCREDCCY;
}
private String TRIGIND;
public String getTRIGIND() {
return TRIGIND;
}
public void setTRIGIND(String TRIGIND) {
this.TRIGIND = TRIGIND;
}
private String TRIGDATE;
public String getTRIGDATE() {
return TRIGDATE;
}
public void setTRIGDATE(String TRIGDATE) {
this.TRIGDATE = TRIGDATE;
}
private String TRIGOPER;
public String getTRIGOPER() {
return TRIGOPER;
}
public void setTRIGOPER(String TRIGOPER) {
this.TRIGOPER = TRIGOPER;
}
private BigDecimal REBATEAMT;
public BigDecimal getREBATEAMT() {
return REBATEAMT;
}
public void setREBATEAMT(BigDecimal REBATEAMT) {
this.REBATEAMT = REBATEAMT;
}
private BigDecimal CLOSINGRATE_8;
public BigDecimal getCLOSINGRATE_8() {
return CLOSINGRATE_8;
}
public void setCLOSINGRATE_8(BigDecimal CLOSINGRATE_8) {
this.CLOSINGRATE_8 = CLOSINGRATE_8;
}
private String FEENO;
public String getFEENO() {
return FEENO;
}
public void setFEENO(String FEENO) {
this.FEENO = FEENO;
}
private String FEESEQ;
public String getFEESEQ() {
return FEESEQ;
}
public void setFEESEQ(String FEESEQ) {
this.FEESEQ = FEESEQ;
}
private String CASHSETTLEIND;
public String getCASHSETTLEIND() {
return CASHSETTLEIND;
}
public void setCASHSETTLEIND(String CASHSETTLEIND) {
this.CASHSETTLEIND = CASHSETTLEIND;
}
private String EXEROPER;
public String getEXEROPER() {
return EXEROPER;
}
public void setEXEROPER(String EXEROPER) {
this.EXEROPER = EXEROPER;
}
private String UNDERLYINGIND;
public String getUNDERLYINGIND() {
return UNDERLYINGIND;
}
public void setUNDERLYINGIND(String UNDERLYINGIND) {
this.UNDERLYINGIND = UNDERLYINGIND;
}
private String AMENDDATE;
public String getAMENDDATE() {
return AMENDDATE;
}
public void setAMENDDATE(String AMENDDATE) {
this.AMENDDATE = AMENDDATE;
}
private String PSDEALNO;
public String getPSDEALNO() {
return PSDEALNO;
}
public void setPSDEALNO(String PSDEALNO) {
this.PSDEALNO = PSDEALNO;
}
private String PSSEQ;
public String getPSSEQ() {
return PSSEQ;
}
public void setPSSEQ(String PSSEQ) {
this.PSSEQ = PSSEQ;
}
private String PSOPER;
public String getPSOPER() {
return PSOPER;
}
public void setPSOPER(String PSOPER) {
this.PSOPER = PSOPER;
}
private String PSDATE;
public String getPSDATE() {
return PSDATE;
}
public void setPSDATE(String PSDATE) {
this.PSDATE = PSDATE;
}
private String PSIND;
public String getPSIND() {
return PSIND;
}
public void setPSIND(String PSIND) {
this.PSIND = PSIND;
}
private String REVPSDATE;
public String getREVPSDATE() {
return REVPSDATE;
}
public void setREVPSDATE(String REVPSDATE) {
this.REVPSDATE = REVPSDATE;
}
private BigDecimal REVQTY;
public BigDecimal getREVQTY() {
return REVQTY;
}
public void setREVQTY(BigDecimal REVQTY) {
this.REVQTY = REVQTY;
}
private BigDecimal REVPREM;
public BigDecimal getREVPREM() {
return REVPREM;
}
public void setREVPREM(BigDecimal REVPREM) {
this.REVPREM = REVPREM;
}
private BigDecimal REVPRINCIPAL;
public BigDecimal getREVPRINCIPAL() {
return REVPRINCIPAL;
}
public void setREVPRINCIPAL(BigDecimal REVPRINCIPAL) {
this.REVPRINCIPAL = REVPRINCIPAL;
}
private String MARKETVALIND;
public String getMARKETVALIND() {
return MARKETVALIND;
}
public void setMARKETVALIND(String MARKETVALIND) {
this.MARKETVALIND = MARKETVALIND;
}
private BigDecimal UNDPRICE_8;
public BigDecimal getUNDPRICE_8() {
return UNDPRICE_8;
}
public void setUNDPRICE_8(BigDecimal UNDPRICE_8) {
this.UNDPRICE_8 = UNDPRICE_8;
}
private BigDecimal CAPPRICE_8;
public BigDecimal getCAPPRICE_8() {
return CAPPRICE_8;
}
public void setCAPPRICE_8(BigDecimal CAPPRICE_8) {
this.CAPPRICE_8 = CAPPRICE_8;
}
private BigDecimal DISCPRICE_8;
public BigDecimal getDISCPRICE_8() {
return DISCPRICE_8;
}
public void setDISCPRICE_8(BigDecimal DISCPRICE_8) {
this.DISCPRICE_8 = DISCPRICE_8;
}
private BigDecimal PARTICPER_8;
public BigDecimal getPARTICPER_8() {
return PARTICPER_8;
}
public void setPARTICPER_8(BigDecimal PARTICPER_8) {
this.PARTICPER_8 = PARTICPER_8;
}
private BigDecimal LINKPRINAMT;
public BigDecimal getLINKPRINAMT() {
return LINKPRINAMT;
}
public void setLINKPRINAMT(BigDecimal LINKPRINAMT) {
this.LINKPRINAMT = LINKPRINAMT;
}
private BigDecimal EXPONENT;
public BigDecimal getEXPONENT() {
return EXPONENT;
}
public void setEXPONENT(BigDecimal EXPONENT) {
this.EXPONENT = EXPONENT;
}
private BigDecimal DISCPER_8;
public BigDecimal getDISCPER_8() {
return DISCPER_8;
}
public void setDISCPER_8(BigDecimal DISCPER_8) {
this.DISCPER_8 = DISCPER_8;
}
private BigDecimal UPDATECOUNTER;
public BigDecimal getUPDATECOUNTER() {
return UPDATECOUNTER;
}
public void setUPDATECOUNTER(BigDecimal UPDATECOUNTER) {
this.UPDATECOUNTER = UPDATECOUNTER;
}
private String TRIGSTARTDATE;
public String getTRIGSTARTDATE() {
return TRIGSTARTDATE;
}
public void setTRIGSTARTDATE(String TRIGSTARTDATE) {
this.TRIGSTARTDATE = TRIGSTARTDATE;
}
private String TRIGENDDATE;
public String getTRIGENDDATE() {
return TRIGENDDATE;
}
public void setTRIGENDDATE(String TRIGENDDATE) {
this.TRIGENDDATE = TRIGENDDATE;
}
private BigDecimal ORIGPRINCIPAL;
public BigDecimal getORIGPRINCIPAL() {
return ORIGPRINCIPAL;
}
public void setORIGPRINCIPAL(BigDecimal ORIGPRINCIPAL) {
this.ORIGPRINCIPAL = ORIGPRINCIPAL;
}
private BigDecimal ORIGQTY;
public BigDecimal getORIGQTY() {
return ORIGQTY;
}
public void setORIGQTY(BigDecimal ORIGQTY) {
this.ORIGQTY = ORIGQTY;
}
private String REPORATECODE;
public String getREPORATECODE() {
return REPORATECODE;
}
public void setREPORATECODE(String REPORATECODE) {
this.REPORATECODE = REPORATECODE;
}
private String AVGMETHOD;
public String getAVGMETHOD() {
return AVGMETHOD;
}
public void setAVGMETHOD(String AVGMETHOD) {
this.AVGMETHOD = AVGMETHOD;
}
private String MATHSOURCE;
public String getMATHSOURCE() {
return MATHSOURCE;
}
public void setMATHSOURCE(String MATHSOURCE) {
this.MATHSOURCE = MATHSOURCE;
}
private String MODEL;
public String getMODEL() {
return MODEL;
}
public void setMODEL(String MODEL) {
this.MODEL = MODEL;
}
private String SWIFTMATCHIND;
public String getSWIFTMATCHIND() {
return SWIFTMATCHIND;
}
public void setSWIFTMATCHIND(String SWIFTMATCHIND) {
this.SWIFTMATCHIND = SWIFTMATCHIND;
}
private String CUSTCDATE;
public String getCUSTCDATE() {
return CUSTCDATE;
}
public void setCUSTCDATE(String CUSTCDATE) {
this.CUSTCDATE = CUSTCDATE;
}
private BigDecimal ORIGPREMAMT;
public BigDecimal getORIGPREMAMT() {
return ORIGPREMAMT;
}
public void setORIGPREMAMT(BigDecimal ORIGPREMAMT) {
this.ORIGPREMAMT = ORIGPREMAMT;
}
private BigDecimal ORIGPREMRATE_8;
public BigDecimal getORIGPREMRATE_8() {
return ORIGPREMRATE_8;
}
public void setORIGPREMRATE_8(BigDecimal ORIGPREMRATE_8) {
this.ORIGPREMRATE_8 = ORIGPREMRATE_8;
}
private String BRPRCINDTE;
public String getBRPRCINDTE() {
return BRPRCINDTE;
}
public void setBRPRCINDTE(String BRPRCINDTE) {
this.BRPRCINDTE = BRPRCINDTE;
}
private String SALESCREDITIND;
public String getSALESCREDITIND() {
return SALESCREDITIND;
}
public void setSALESCREDITIND(String SALESCREDITIND) {
this.SALESCREDITIND = SALESCREDITIND;
}
private String ALLOCATEIND;
public String getALLOCATEIND() {
return ALLOCATEIND;
}
public void setALLOCATEIND(String ALLOCATEIND) {
this.ALLOCATEIND = ALLOCATEIND;
}
private String ALLOCATEDATE;
public String getALLOCATEDATE() {
return ALLOCATEDATE;
}
public void setALLOCATEDATE(String ALLOCATEDATE) {
this.ALLOCATEDATE = ALLOCATEDATE;
}
private String ASSIGNIND;
public String getASSIGNIND() {
return ASSIGNIND;
}
public void setASSIGNIND(String ASSIGNIND) {
this.ASSIGNIND = ASSIGNIND;
}
private String ASSIGNCNO;
public String getASSIGNCNO() {
return ASSIGNCNO;
}
public void setASSIGNCNO(String ASSIGNCNO) {
this.ASSIGNCNO = ASSIGNCNO;
}
private String CUSTSTATUS;
public String getCUSTSTATUS() {
return CUSTSTATUS;
}
public void setCUSTSTATUS(String CUSTSTATUS) {
this.CUSTSTATUS = CUSTSTATUS;
}
private String ASSIGNDEALNO;
public String getASSIGNDEALNO() {
return ASSIGNDEALNO;
}
public void setASSIGNDEALNO(String ASSIGNDEALNO) {
this.ASSIGNDEALNO = ASSIGNDEALNO;
}
private String ASSIGNDATE;
public String getASSIGNDATE() {
return ASSIGNDATE;
}
public void setASSIGNDATE(String ASSIGNDATE) {
this.ASSIGNDATE = ASSIGNDATE;
}
private String ASSIGNDEALNOSEQ;
public String getASSIGNDEALNOSEQ() {
return ASSIGNDEALNOSEQ;
}
public void setASSIGNDEALNOSEQ(String ASSIGNDEALNOSEQ) {
this.ASSIGNDEALNOSEQ = ASSIGNDEALNOSEQ;
}
private String ASSIGNCUSTSTATUS;
public String getASSIGNCUSTSTATUS() {
return ASSIGNCUSTSTATUS;
}
public void setASSIGNCUSTSTATUS(String ASSIGNCUSTSTATUS) {
this.ASSIGNCUSTSTATUS = ASSIGNCUSTSTATUS;
}
private int SCHEDID;
public int getSCHEDID() {
return SCHEDID;
}
public void setSCHEDID(int SCHEDID) {
this.SCHEDID = SCHEDID;
}
private int BASKETID;
public int getBASKETID() {
return BASKETID;
}
public void setBASKETID(int BASKETID) {
this.BASKETID = BASKETID;
}
}