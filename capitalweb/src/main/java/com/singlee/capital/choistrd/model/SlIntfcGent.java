package com.singlee.capital.choistrd.model;

import java.util.Date;

public class SlIntfcGent {
	public String paramod;
	public String br;
	public String paraid;
	public String part1;
	public String part2;
	public String object;
	public String operid;
	public String note;
	public Date lstdate;
	public String getParamod() {
		return paramod;
	}
	public void setParamod(String paramod) {
		this.paramod = paramod;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getParaid() {
		return paraid;
	}
	public void setParaid(String paraid) {
		this.paraid = paraid;
	}
	public String getPart1() {
		return part1;
	}
	public void setPart1(String part1) {
		this.part1 = part1;
	}
	public String getPart2() {
		return part2;
	}
	public void setPart2(String part2) {
		this.part2 = part2;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getOperid() {
		return operid;
	}
	public void setOperid(String operid) {
		this.operid = operid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getLstdate() {
		return lstdate;
	}
	public void setLstdate(Date lstdate) {
		this.lstdate = lstdate;
	}
	
	
	
	
}
