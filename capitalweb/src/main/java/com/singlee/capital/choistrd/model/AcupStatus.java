package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class AcupStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String tableId;
	private String sysDate;
	private String branPrcDate;
	private String createTime;
	private int createCount;
	private String issuccess;
	private int acupCount;
	private int slAcupSourceCount;
	private int slAcupCount;
	private int slAcuperrCount;
	private String sendTime;
	private int sendCount;
	private int sendSuccessCount;
	private int sendFailCount;
	private String descr;
	
	public AcupStatus(){}
	/**
	 * @return the tableId
	 */
	public String getTableId() {
		return tableId;
	}
	/**
	 * @param tableId the tableId to set
	 */
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	/**
	 * @return the sysDate
	 */
	public String getSysDate() {
		return sysDate;
	}
	/**
	 * @param sysDate the sysDate to set
	 */
	public void setSysDate(String sysDate) {
		this.sysDate = sysDate;
	}
	/**
	 * @return the branPrcDate
	 */
	public String getBranPrcDate() {
		return branPrcDate;
	}
	/**
	 * @param branPrcDate the branPrcDate to set
	 */
	public void setBranPrcDate(String branPrcDate) {
		this.branPrcDate = branPrcDate;
	}
	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the createCount
	 */
	public int getCreateCount() {
		return createCount;
	}
	/**
	 * @param createCount the createCount to set
	 */
	public void setCreateCount(int createCount) {
		this.createCount = createCount;
	}
	/**
	 * @return the issuccess
	 */
	public String getIssuccess() {
		return issuccess;
	}
	/**
	 * @param issuccess the issuccess to set
	 */
	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
	}
	/**
	 * @return the acupCount
	 */
	public int getAcupCount() {
		return acupCount;
	}
	/**
	 * @param acupCount the acupCount to set
	 */
	public void setAcupCount(int acupCount) {
		this.acupCount = acupCount;
	}
	/**
	 * @return the slAcupSourceCount
	 */
	public int getSlAcupSourceCount() {
		return slAcupSourceCount;
	}
	/**
	 * @param slAcupSourceCount the slAcupSourceCount to set
	 */
	public void setSlAcupSourceCount(int slAcupSourceCount) {
		this.slAcupSourceCount = slAcupSourceCount;
	}
	/**
	 * @return the slAcupCount
	 */
	public int getSlAcupCount() {
		return slAcupCount;
	}
	/**
	 * @param slAcupCount the slAcupCount to set
	 */
	public void setSlAcupCount(int slAcupCount) {
		this.slAcupCount = slAcupCount;
	}
	/**
	 * @return the slAcuperrCount
	 */
	public int getSlAcuperrCount() {
		return slAcuperrCount;
	}
	/**
	 * @param slAcuperrCount the slAcuperrCount to set
	 */
	public void setSlAcuperrCount(int slAcuperrCount) {
		this.slAcuperrCount = slAcuperrCount;
	}
	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}
	/**
	 * @param sendTime the sendTime to set
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	/**
	 * @return the sendCount
	 */
	public int getSendCount() {
		return sendCount;
	}
	/**
	 * @param sendCount the sendCount to set
	 */
	public void setSendCount(int sendCount) {
		this.sendCount = sendCount;
	}
	/**
	 * @return the sendSuccessCount
	 */
	public int getSendSuccessCount() {
		return sendSuccessCount;
	}
	/**
	 * @param sendSuccessCount the sendSuccessCount to set
	 */
	public void setSendSuccessCount(int sendSuccessCount) {
		this.sendSuccessCount = sendSuccessCount;
	}
	/**
	 * @return the sendFailCount
	 */
	public int getSendFailCount() {
		return sendFailCount;
	}
	/**
	 * @param sendFailCount the sendFailCount to set
	 */
	public void setSendFailCount(int sendFailCount) {
		this.sendFailCount = sendFailCount;
	}
	/**
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}
	/**
	 * @param descr the descr to set
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

}
