package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class DerivativesStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String tableName;
	
	private String  dealno;
	
	private String seq;
	
	private String choisRefNo;
	
	private String choisHisNo;
	
	private String sendTime;
	
	private String isSend;
	
	private String isReSend;
	
	private String sendResult;
	
	private String errMsg;
	
	private String sendMsg;
	
	private String product;
	
	private String type;
	
	private Integer upCount;
	
	private String upOwn;
	
	private String upTime;
	
	private String inputTime;
	
	private String reversal;//REVERSAL
	
	private String schdSeq;//SCHDSEQ
	
	private String verautFlag;
	
	/**
	 * @return the verautFlag
	 */
	public String getVerautFlag() {
		return verautFlag;
	}

	/**
	 * @param verautFlag the verautFlag to set
	 */
	public void setVerautFlag(String verautFlag) {
		this.verautFlag = verautFlag;
	}

	/**
	 * @return the upCount
	 */
	public Integer getUpCount() {
		return upCount;
	}

	/**
	 * @param upCount the upCount to set
	 */
	public void setUpCount(Integer upCount) {
		this.upCount = upCount;
	}

	
	/**
	 * @return the upOwn
	 */
	public String getUpOwn() {
		return upOwn;
	}

	/**
	 * @param upOwn the upOwn to set
	 */
	public void setUpOwn(String upOwn) {
		this.upOwn = upOwn;
	}

	/**
	 * @return the upTime
	 */
	public String getUpTime() {
		return upTime;
	}

	/**
	 * @param upTime the upTime to set
	 */
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	/**
	 * @return the inputTime
	 */
	public String getInputTime() {
		return inputTime;
	}

	/**
	 * @param inputTime the inputTime to set
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public DerivativesStatus(){}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the dealno
	 */
	public String getDealno() {
		return dealno;
	}

	/**
	 * @param dealno the dealno to set
	 */
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	

	public String getChoisRefNo() {
		return choisRefNo;
	}

	public void setChoisRefNo(String choisRefNo) {
		this.choisRefNo = choisRefNo;
	}

	public String getChoisHisNo() {
		return choisHisNo;
	}

	public void setChoisHisNo(String choisHisNo) {
		this.choisHisNo = choisHisNo;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime the sendTime to set
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return the isSend
	 */
	public String getIsSend() {
		return isSend;
	}

	/**
	 * @param isSend the isSend to set
	 */
	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}

	/**
	 * @return the isReSend
	 */
	public String getIsReSend() {
		return isReSend;
	}

	/**
	 * @param isReSend the isReSend to set
	 */
	public void setIsReSend(String isReSend) {
		this.isReSend = isReSend;
	}

	/**
	 * @return the sendResult
	 */
	public String getSendResult() {
		return sendResult;
	}

	/**
	 * @param sendResult the sendResult to set
	 */
	public void setSendResult(String sendResult) {
		this.sendResult = sendResult;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * @return the sendMsg
	 */
	public String getSendMsg() {
		return sendMsg;
	}

	/**
	 * @param sendMsg the sendMsg to set
	 */
	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}

	/**
	 * @return the reversal
	 */
	public String getReversal() {
		return reversal;
	}

	/**
	 * @param reversal the reversal to set
	 */
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

	/**
	 * @return the schdSeq
	 */
	public String getSchdSeq() {
		return schdSeq;
	}

	/**
	 * @param schdSeq the schdSeq to set
	 */
	public void setSchdSeq(String schdSeq) {
		this.schdSeq = schdSeq;
	}

}
