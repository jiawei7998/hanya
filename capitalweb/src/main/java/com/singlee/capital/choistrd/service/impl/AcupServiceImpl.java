package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.choistrd.mapper.AcupMapper;
import com.singlee.capital.choistrd.mapper.FxTradeMapper;
import com.singlee.capital.choistrd.model.SL_ACUP;
import com.singlee.capital.choistrd.model.SL_ACUP_SNEDFLAG;
import com.singlee.capital.choistrd.service.AcupService;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.page.PageInfo;
@Service
public class AcupServiceImpl implements AcupService {
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private AcupMapper acupMapper ;

	@Override
	public String executeAcupProc(Map<String, String> map) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.executeAcupProc(map);
	}

	@Override
	public List<SL_ACUP> queryAllSLAcupFromOPicsByCondition(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SL_ACUP> querySLAcupBySetno(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.querySLAcupBySetno(map);
	}

	@Override
	public int updateSLAcupBySetno(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.updateSLAcupBySetno(map);
	}

	@Override
	public List<String> queryAllSetNoToSend(String postdate, String dealFlag) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryOpicsPostDate() throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.queryOpicsPostDate();
	}

	@Override
	public String queryOpicsBrprcDate() throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.queryOpicsBrprcDate()
				;
	}

	@Override
	public List<SL_ACUP> queryAllSLAcupFromOPics(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.queryAllSLAcupFromOPics(postdate);
	}

	@Override
	public List<SL_ACUP> querySLAcupFromOpicsByPostdate(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.querySLAcupFromOpicsByPostdate(postdate);
	}

	@Override
	public List<SL_ACUP> queryAllSLAcupErrFromOPics(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.queryAllSLAcupErrFromOPics(postdate);
	}

	@Override
	public List<SL_ACUP> queryAcupBySetnoAndSeqNo(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.queryAcupBySetnoAndSeqNo(map);
	}

	@Override
	public List<SL_ACUP> repeatQueryAllSetNoToSend(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.repeatQueryAllSetNoToSend(map);
	}

	@Override
	public int querySLAcupSendFlag(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.querySLAcupSendFlag(postdate);
	}

	@Override
	public int insertSLAcupSendFlag(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return acupMapper.insertSLAcupSendFlag(postdate);
	}

	@Override
	public PageInfo<SL_ACUP> queryAcup(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SL_ACUP> page = null;
		page = acupMapper.queryAcup(map, rb);
		return new PageInfo<SL_ACUP>(page);
	}

	
	@Override
	public void deleteAcupSendFlag(String postdate) {
		// TODO Auto-generated method stub
		acupMapper.deleteAcupSendFlag(postdate);
	}

	@Override
	public PageInfo<SL_ACUP_SNEDFLAG> queryAcupSendFlag(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SL_ACUP_SNEDFLAG> page = null;
		page = acupMapper.queryAcupSendFlag(map, rb);
		return new PageInfo<SL_ACUP_SNEDFLAG>(page);
	}

}
