package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SWDT;

import tk.mybatis.mapper.common.Mapper;

public interface SwdtMapper extends Mapper<SWDT>{
	
	public List<SWDT> queryAllSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	public List<SWDT> queryAllRevSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
}
