package com.singlee.capital.choistrd.service.impl;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.StringUtil;
import com.singlee.capital.chois.util.StringUtils;
import com.singlee.capital.chois.util.SubStrChinese;
import com.singlee.capital.choistrd.mapper.FxTradeMapper;
import com.singlee.capital.choistrd.mapper.PmtqMappr;
import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FxStatus;
import com.singlee.capital.choistrd.model.SL_PMTQ;
import com.singlee.capital.common.util.PropertiesUtil;
@Service
public class PmtqInputTmaxCommand  {
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private PmtqMappr pmtqMaper ;
	
	String resmg=null;
	public Date date =new Date();
	public SimpleDateFormat sdfdate = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
	public String msgTypeCode=null;//标准报文数据种类代码
	public String  msgTypeCodeLen;//标准报文数据长度
	public int  msgTypeCodeLen1;//标准报文数据长度
	public String sysName;//系统姓名
	public String handleCode;//处理代码
	public String channelType;//渠道类型
	public String channelId;//渠道ID
	public String gainInst;//获取机构
	public String cdate;//日期
	public String ctime;//时间
	public String keyTrade;//KEY1, 交易
	public String keyTradeNo;//KEY2 , 参考号
	public String serveHandleCode;//服务处理代码
	public String FXFIG_CJUM;
	public String FXFIG_AJUM;
	public String FXFIG_SJUM;
	public String FXFIG_OPNO;
	public String FXFIG_OPGB;
	public String FXFIG_IBIL;
	public String FXFIG_AMOD;
	public String FXFIG_TELL;
	public String FXFIG_SSID;
	public String FXFIG_SSIL;
	public String FXFIG_TELLNM;
	public String FXFIG_THGB;
	public String FXFIG_TYPE;
	public String FXFIG_JUM_CD;
	public String FXFIG_CCY;
	
	public String FXFIG_SSCK;
	public String FXFIG_THID;
	
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	ChiosLogin chiosLogin = new ChiosLogin();

	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();

	StringUtils stringUtils=new StringUtils();
	
	StringUtil stringUtil=new StringUtil();
	
	SubStrChinese subStrChinese=new SubStrChinese();
	public int executeHostVisitAction(SL_PMTQ pmtq,String sess) throws Exception {
		// TODO Auto-generated method stub
		
		try {
			SL_PMTQ sl_pmtq = new SL_PMTQ();
			  
			sl_pmtq.setISREVERSAL("N");
			sl_pmtq.setDEALNO(pmtq.getDEALNO().trim());
			sl_pmtq.setSEQ(pmtq.getSEQ().trim());
			sl_pmtq.setUPCOUNT(0);
			
		    sl_pmtq.setSENDTIME(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			sl_pmtq.setINPUTTIME(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
			
			long standardStampStart = System.currentTimeMillis();
			
			//super.setWebtFieldSetSession(sndset);
				//拼接报文
			//String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
		     //返回的柜员session
			
			StdTMsgGenerator generator = new StdTMsgGenerator();
			//报文头
			byte[] headerPacket=choisCommonMsg.makeHead();
			
			//报文体
			PropertiesConfiguration stdtmsgSysValue;
			
			stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
			 String handleCode="DE4100";
			 String serveHandleCode="OPCDE41";
			FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
			
			String coMsg=choisCommonMsg.makeMsg();//公共部分
			String coMsg2=choisCommonMsg.makeMsg2();//公共部分
			String coMsg3=choisCommonMsg.makeMsg3();//公共部分
			//交易数据
			String dealMsg=null;
		//String s=String.format("%1$-10s",fx.getFarVdate()==null?"":fx.getFarVdate().trim());
			dealMsg=String.format("%1$-8s",pmtq.getCIX_NO())
					+String.format("%1$-15s",pmtq.getOWN_REF_NO())
					+stringUtils.fillZero(String.valueOf(pmtq.getOWN_HIS_NO()),5,0,true,true)
					+String.format("%1$-100s",pmtq.getRCV_NAME())
					+String.format("%1$-40s",pmtq.getRCV_ACCT_NO())
					+String.format("%1$-20s",pmtq.getRCV_BKCD1())
					+String.format("%1$-80s",pmtq.getRCV_BKNM1())
					+String.format("%1$-100s",pmtq.getREMARK())
					+String.format("%1$-6s",pmtq.getMA_TP())
					+String.format("%1$-7s",pmtq.getTR_TP())
					+String.format("%1$-8s",pmtq.getDEALNO())
					+String.format("%1$-10s",pmtq.getCDATE())
					+String.format("%1$-10s",pmtq.getVDATE())
					+String.format("%1$-1s",pmtq.getPAYRECIND())
					+String.format("%1$-6s",pmtq.getPRODUCT());
					
			byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
			byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
			String serviceUrl = "OPS";
			byte[] msgData =totalPacket;
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			System.out.println("eaiDv:--"+eaiDv);
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
			System.out.println("PMTQ返回信息："+new String(returnData, "UTF-8"));
			
			int len=(new String(returnData, "UTF-8")).length();
			
			
			logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
	
			logger.info("RECV MSG:"+new String(returnData, "GBK"));
			String recMsg=new String(returnData, "UTF-8");
		
			
			resmg=((new String(returnData, "UTF-8")).substring(0,292).substring((new String(returnData, "UTF-8")).substring(0,292).length()-2));
			if(resmg.equals("10")) {
				//CHOIS参考号
				String nupdInfo = recMsg.substring(recMsg.indexOf("@")-15, recMsg.indexOf("@"));
				//String recRefNo = nupdInfo.substring(0,15);
				//String recHisNo = nupdInfo.substring(15, 20);
				
				
				sl_pmtq.setISSUCCESS("Y");
				//sl_pmtq.setIsresend("N");
				sl_pmtq.setISSEND("Y");
				sl_pmtq.setCHOIS_HISNO(0);
				sl_pmtq.setCHOIS_REFNO(nupdInfo);
				sl_pmtq.setCHOISSENDMSG(new String(msgData, "UTF-8"));
				
			}else {
				sl_pmtq.setISSUCCESS("N");
				//sl_pmtq.setIsresend("N");
				sl_pmtq.setISSEND("Y");
				sl_pmtq.setCHOIS_HISNO(0);
				sl_pmtq.setCHOIS_REFNO("");
				sl_pmtq.setCHOISSENDMSG(new String(msgData, "UTF-8"));
				sl_pmtq.setCHOISERRMSG((new String(returnData, "GBK")));
				//fxTradeMapper.insertFxStatusByNew(fxStatus);
			}

			long standardStampEnd = System.currentTimeMillis();
			
			if ((standardStampEnd - standardStampStart) > 60000) {
				chiosLogin.main();
				logger.info("[ROLLBACK]");
				return -1;
			} else {
				Map<String, Object> instMap =new HashMap<String, Object>();
				instMap.put("DEALNO", sl_pmtq.getDEALNO().trim());
				instMap.put("SEQ", sl_pmtq.getSEQ().trim());
				instMap.put("BR", sl_pmtq.getBR().trim());
				instMap.put("PRODUCT", sl_pmtq.getPRODUCT().trim());
				instMap.put("TYPE", sl_pmtq.getTYPE().trim());
				instMap.put("CDATE", sl_pmtq.getCDATE().trim());
				instMap.put("CTIME", sl_pmtq.getCTIME().trim());
				instMap.put("PAYRECIND", sl_pmtq.getPAYRECIND().trim());
				SL_PMTQ oldFxStatus = this.pmtqMaper.queryPmtqByDealnoSeqCondition(instMap);
				if(null != oldFxStatus)
				{
					sl_pmtq.setUPCOUNT(oldFxStatus.getUPCOUNT()+1);
					//sl_pmtq.setUP("TMAX");
					//sl_pmtq.setIsresend("Y");
					if(this.pmtqMaper.updatePmtqStatus(sl_pmtq)>0)
					{
						logger.info("DEALNO["+sl_pmtq.getDEALNO()+"]"+"更新[SL_FXDH_STATUS]成功");
					}else
					{
						logger.info("DEALNO["+sl_pmtq.getDEALNO()+"]TABLENAME["+"更新账[SL_FXDH_STATUS]失败.");
					}
				}
				logger.info("[COMMIT]");
				//trans.commit();
			}
		} catch (Exception e) {
			chiosLogin.main();
			
			e.printStackTrace();
			logger.info("[ROLLBACK] EXCEPTION:"+e.getMessage());
			throw e;
		} finally {
			
			
		}

		return 0;
	}


	


}
