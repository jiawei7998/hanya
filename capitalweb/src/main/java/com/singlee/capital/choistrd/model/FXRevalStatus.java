package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;


public class FXRevalStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private String dealno;
	
	private String postdate;
	
	private String revalamt;
	
	private String ccy;
	
	private String chois_ref_no;
	
	private String chois_his_no;
	
	private String trx_yn;
		
	private Date sendtime;
	
	private String issend;
	
	private String isresend;
	
	private String sendresult;
	
	private String sendmsg;
	
	private String tablename;
	
	private int upcount;
	
	private String upown;
	 
	private Date uptime;
	
	private Date inputtime;

	
	
	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getRevalamt() {
		return revalamt;
	}

	public void setRevalamt(String revalamt) {
		this.revalamt = revalamt;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	



	public String getChois_ref_no() {
		return chois_ref_no;
	}

	public void setChois_ref_no(String chois_ref_no) {
		this.chois_ref_no = chois_ref_no;
	}

	public String getChois_his_no() {
		return chois_his_no;
	}

	public void setChois_his_no(String chois_his_no) {
		this.chois_his_no = chois_his_no;
	}

	public String getTrx_yn() {
		return trx_yn;
	}

	public void setTrx_yn(String trx_yn) {
		this.trx_yn = trx_yn;
	}

	public Date getSendtime() {
		return sendtime;
	}

	public void setSendtime(Date sendtime) {
		this.sendtime = sendtime;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIsresend() {
		return isresend;
	}

	public void setIsresend(String isresend) {
		this.isresend = isresend;
	}

	public String getSendresult() {
		return sendresult;
	}

	public void setSendresult(String sendresult) {
		this.sendresult = sendresult;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public int getUpcount() {
		return upcount;
	}

	public void setUpcount(int upcount) {
		this.upcount = upcount;
	}

	public String getUpown() {
		return upown;
	}

	public void setUpown(String upown) {
		this.upown = upown;
	}

	public Date getUptime() {
		return uptime;
	}

	public void setUptime(Date uptime) {
		this.uptime = uptime;
	}

	public Date getInputtime() {
		return inputtime;
	}

	public void setInputtime(Date inputtime) {
		this.inputtime = inputtime;
	}

	

	
}
