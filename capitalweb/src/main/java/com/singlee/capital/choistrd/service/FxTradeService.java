package com.singlee.capital.choistrd.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FxReverse;
import com.singlee.capital.choistrd.model.FxStatus;

public interface FxTradeService {	
	
	/**
	 * 获取全部的FX交易
	 * @param dealDate
	 * @param trype SPOT FWD SWAP
	 * @return
	 * @throws Exception
	 */
	public List<Fx> queryAllFxListSpot(String postdate) throws Exception;
	
	public List<Fx> queryAllFxListFwd(String postdate) throws Exception;
	
	public List<Fx> queryAllFxListSwap( String postdate) throws Exception;
	
	public List<FxReverse> queryAllFxListRev(String postdate) throws Exception;
	
	/**
	 * 获取全部FX交易 按条件
	 * @param dealDate
	 * @param type SPOT FWD SWAP
	 * @param condition   F-失败  S-成功
	 * @return
	 * @throws Exception
	 */
	public List<Fx> queryAllConditionFxListProc(String dealDate, String type,
			String condition) throws Exception;
	
	public List<Fx> queryAllConditionFxListProcNew(Map<String,Object> map) throws Exception;
	
	/**
	 * 获取全部FX交易 按条件 冲销的交易
	 * @param dealDate
	 * @param type SPOT FWD SWAP
	 * @param condition   F-失败  S-成功
	 * @return
	 * @throws Exception
	 */
	public List<FxReverse> queryAllConditionFxReverseListProc(String dealDate, String type,
			String condition) throws Exception;
	
	/**
	 * 查询数据库中FX交易的状态
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	public FxStatus queryFxStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	/**
	 * 插入新的FX状态
	 * @param fxStatus
	 * @return
	 * @throws Exception
	 */
	public int insertFxStatusByNew(FxStatus fxStatus) throws Exception;
	/**
	 * 更新FX的状态
	 * @param fxStatus
	 * @return
	 * @throws Exception
	 */
	public int updateFxStatusByDealnoSeq(FxStatus fxStatus) throws Exception;
	/**
	 * 删除FX状态
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	public int deleteFxStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	
	public int updateFxStatusByChoisRefHisNo(FxStatus fxStatus) throws Exception;
	
	public String queryOpicsPostDate() throws Exception;
}
