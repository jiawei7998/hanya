package com.singlee.capital.choistrd.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SL_PMTQ;

public interface PmtqService {
	public List<SL_PMTQ> queryAllPmtqFromOpicsByCondition() throws Exception;
	
	public int updatePmtqStatus(SL_PMTQ sl_pmtq) throws Exception;
	
	public SL_PMTQ queryPmtqByDealnoSeqCondition(Map<String, Object> map) throws Exception;
	
}
