package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.CustCnoCcy;
import com.singlee.capital.choistrd.model.CustQuery;
import com.singlee.capital.choistrd.model.SL_CUST;

import tk.mybatis.mapper.common.Mapper;



public interface CustQueryChoisMapper  extends Mapper<CustCnoCcy>{

	
	public List<CustCnoCcy> queryAllShouldSendCust() throws Exception;
	
	public int insertCustNew(Map<String, Object> map) throws Exception ;
	
	public SL_CUST queryCustByCno(Map<String, Object> map) throws Exception;
	
	public int updateCustByCno(Map<String, Object> map) throws Exception ;
	
}
