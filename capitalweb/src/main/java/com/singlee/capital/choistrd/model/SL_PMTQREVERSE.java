package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class SL_PMTQREVERSE implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	private String SVCN;
	private String IBCD;
	private String GWAM;
	private String GEOR;
	private String OPNO;
	private String OWN_REF_NO;
	private String OWN_HIS_NO;
	
	public SL_PMTQREVERSE(){}
	/**
	 * @return the sVCN
	 */
	public String getSVCN() {
		return SVCN;
	}
	/**
	 * @param svcn the sVCN to set
	 */
	public void setSVCN(String svcn) {
		SVCN = svcn;
	}
	/**
	 * @return the iBCD
	 */
	public String getIBCD() {
		return IBCD;
	}
	/**
	 * @param ibcd the iBCD to set
	 */
	public void setIBCD(String ibcd) {
		IBCD = ibcd;
	}
	/**
	 * @return the gWAM
	 */
	public String getGWAM() {
		return GWAM;
	}
	/**
	 * @param gwam the gWAM to set
	 */
	public void setGWAM(String gwam) {
		GWAM = gwam;
	}
	/**
	 * @return the gEOR
	 */
	public String getGEOR() {
		return GEOR;
	}
	/**
	 * @param geor the gEOR to set
	 */
	public void setGEOR(String geor) {
		GEOR = geor;
	}
	/**
	 * @return the oPNO
	 */
	public String getOPNO() {
		return OPNO;
	}
	/**
	 * @param opno the oPNO to set
	 */
	public void setOPNO(String opno) {
		OPNO = opno;
	}
	/**
	 * @return the oWN_REF_NO
	 */
	public String getOWN_REF_NO() {
		return OWN_REF_NO;
	}
	/**
	 * @param own_ref_no the oWN_REF_NO to set
	 */
	public void setOWN_REF_NO(String own_ref_no) {
		OWN_REF_NO = own_ref_no;
	}
	/**
	 * @return the oWN_HIS_NO
	 */
	public String getOWN_HIS_NO() {
		return OWN_HIS_NO;
	}
	/**
	 * @param own_his_no the oWN_HIS_NO to set
	 */
	public void setOWN_HIS_NO(String own_his_no) {
		OWN_HIS_NO = own_his_no;
	}
	
	
	
}
