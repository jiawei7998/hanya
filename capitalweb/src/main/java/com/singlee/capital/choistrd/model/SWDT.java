package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SWDT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String br;

    private String dealno;

    private String seq;

    private String dealind;

	private String product;

    private String prodtype;

    private String basis;

    private String calcrule;

    private String compound;

    private BigDecimal finexchamt;

    private BigDecimal finexchbamt;

    private String finexchauthdte;

    private String finexchauthind;

    private String finexchccy;

    private String finexchdate;

    private String finexchfincent;

    private String finexchoper;

    private String finexchsacct;

    private String finexchsmeans;

    private String firstipaydate;

    private BigDecimal fixdintamt;

    private String fixfloatind;

    private BigDecimal initexchamt;

    private BigDecimal initexchbamt;

    private String initexchauthdte;

    private String initexchauthind;

    private String initexchccy;

    private String initexchdate;

    private String initexchfincent;

    private String initexchoper;

    private String initexchsmeans;

    private String initexchsacct;

    private String intauthdte;

    private String intauthind;

    private String intauthoper;

    private String intconvdte;

    private BigDecimal intconvrate_8;

    private String intccy;

    private BigDecimal intexchrate_8;

    private String intexchterms;

    private String intpayadjust;

    private String intpaycycle;

    private String intpayday;

    private String intpayfincent;

    private String intpaypayrule;

    private BigDecimal intrate_8;

    private BigDecimal intratecap_8;

    private BigDecimal intratefloor_8;

    private String intsacct;

    private String intsmeans;

    private String lastipaydate;

    private String matdate;

    private String notccy;

    private BigDecimal notccyamt;

    private BigDecimal npvamt;

    private String nxtraterev;

    private String payrecind;

    private String ratecode;

    private String raterevcycle;

    private String raterevday;

    private String raterevdte;

    private String raterevfincent;

    private String raterevfrstlst;

    private String raterevpayrule;

    private String schdflag;

    private BigDecimal spread_8;

    private String startdate;

    private String upfrontfeeno;

    private String upfrontfeenettype;

    private String yieldcurve;

    private String acrfrstlst;

    private BigDecimal npvamtinitexch;

    private BigDecimal npvamtfinexch;

    private String ps;

    private BigDecimal strike_8;

    private BigDecimal index_8;

    private BigDecimal particperc_8;

    private BigDecimal delta_8;

    private BigDecimal volatility_8;

    private String settfirst;

    private String collarind;

    private BigDecimal npvbamt;

    private BigDecimal tdyaccrintamt;

    private BigDecimal tdyaccrintbamt;

    private BigDecimal ystaccrintamt;

    private BigDecimal ystaccrintbamt;

    private BigDecimal npvbamtinitexch;

    private BigDecimal npvbamtfinexch;

    private String upfrontfeefincent;

    private String upfrontfeeind;

    private BigDecimal tdycomprate_8;

    private BigDecimal tdycompamt;

    private BigDecimal ystcomprate_8;

    private BigDecimal ystcompamt;

    private String exchrevdate;

    private String datedirection;

    private BigDecimal purchint;

    private String lastcoupondate;

    private BigDecimal premdisc;

    private String exchraterevdate;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal amount3;

    private String char1;

    private String char2;

    private String formulaind;

    private String date1;

    private String date2;

    private String flag1;

    private BigDecimal rate1_8;

    private BigDecimal rate2_8;

    private String usualid;

    private String btbusualid;

    private String mbsleg;

    private BigDecimal speed_12;

    private BigDecimal servicing_12;

    private String prepaytype;

    private String settnotional;

    private BigDecimal factor_12;

    private String factorrevdate;

    private BigDecimal origprinadjamt;

    private String intdeal;

    private BigDecimal internalrate_8;

    private BigDecimal corpspread_8;

    private BigDecimal bcreditamt;

    private BigDecimal bmtmamt;

    private BigDecimal bmktamt;

    private BigDecimal exchrate_8;

    private String exchterms;

    private BigDecimal initbaseamt;

    private BigDecimal initfinamt;

    private BigDecimal initfinbaseamt;

    private BigDecimal initfinexchrate_8;

    private BigDecimal initbaseexchrate_8;

    private String initfinexchterms;

    private String initbaseterms;

    private String flag2;

    private String flag3;

    private BigDecimal rate3_8;

    private BigDecimal rate4_8;

    private String date3;

    private String date4;

    private BigDecimal amount4;

    private BigDecimal amount5;

    private BigDecimal corpspreadamt;

    private String postnotional;

    private String basketdescr;

    private BigDecimal convprice_12;

    private String fwdfwdyieldcurve;

    private String equitytype;

    private String intnegauthdte;

    private String intnegauthind;

    private String intnegauthoper;

    private String intnegsacct;

    private String intnegsmeans;

    private BigDecimal price_12;

    private BigDecimal priceexchrate_8;

    private String priceexchterms;

    private String repriceind;

    private BigDecimal quantity;

    private String secid;

    private String settdays;

    private String secccy;

    private String repricedate;

    private BigDecimal rateofreturn_12;

    private BigDecimal divpercent_8;

    private String divpaydateind;

    private String divreinvestind;

    private String divpayccyind;

    private BigDecimal multiplier_8;

    private String returntype;

    private String valuationtime;

    private String nxtrepricedate;

    private String repricedays;

    private String externalvalind;

    private String notifdays;

    private String notiftime;

    private BigDecimal dealrate_8;

    private String dealterms;

    private String fxratecode;

    private String nextfxdate;

    private String exchmethod;

    private String multiccyacctind;

    private String legtype;

    private Short schedid;

    private Short prodid;

    private Short formulaid;

    private String accrualccy;

    private String mktvalsource;

    private String accrualmethod;

    private String grossufpayrecind;

    private BigDecimal grossufpayamt;

    private BigDecimal grossufpayrate_8;

    private String schdconv;

    private String redemptionccyind;

    private String nextfxprindate;

    private BigDecimal deffactor;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDealind() {
		return dealind;
	}

	public void setDealind(String dealind) {
		this.dealind = dealind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getCalcrule() {
		return calcrule;
	}

	public void setCalcrule(String calcrule) {
		this.calcrule = calcrule;
	}

	public String getCompound() {
		return compound;
	}

	public void setCompound(String compound) {
		this.compound = compound;
	}

	public BigDecimal getFinexchamt() {
		return finexchamt;
	}

	public void setFinexchamt(BigDecimal finexchamt) {
		this.finexchamt = finexchamt;
	}

	public BigDecimal getFinexchbamt() {
		return finexchbamt;
	}

	public void setFinexchbamt(BigDecimal finexchbamt) {
		this.finexchbamt = finexchbamt;
	}

	public String getFinexchauthdte() {
		return finexchauthdte;
	}

	public void setFinexchauthdte(String finexchauthdte) {
		this.finexchauthdte = finexchauthdte;
	}

	public String getFinexchauthind() {
		return finexchauthind;
	}

	public void setFinexchauthind(String finexchauthind) {
		this.finexchauthind = finexchauthind;
	}

	public String getFinexchccy() {
		return finexchccy;
	}

	public void setFinexchccy(String finexchccy) {
		this.finexchccy = finexchccy;
	}

	public String getFinexchdate() {
		return finexchdate;
	}

	public void setFinexchdate(String finexchdate) {
		this.finexchdate = finexchdate;
	}

	public String getFinexchfincent() {
		return finexchfincent;
	}

	public void setFinexchfincent(String finexchfincent) {
		this.finexchfincent = finexchfincent;
	}

	public String getFinexchoper() {
		return finexchoper;
	}

	public void setFinexchoper(String finexchoper) {
		this.finexchoper = finexchoper;
	}

	public String getFinexchsacct() {
		return finexchsacct;
	}

	public void setFinexchsacct(String finexchsacct) {
		this.finexchsacct = finexchsacct;
	}

	public String getFinexchsmeans() {
		return finexchsmeans;
	}

	public void setFinexchsmeans(String finexchsmeans) {
		this.finexchsmeans = finexchsmeans;
	}

	public String getFirstipaydate() {
		return firstipaydate;
	}

	public void setFirstipaydate(String firstipaydate) {
		this.firstipaydate = firstipaydate;
	}

	public BigDecimal getFixdintamt() {
		return fixdintamt;
	}

	public void setFixdintamt(BigDecimal fixdintamt) {
		this.fixdintamt = fixdintamt;
	}

	public String getFixfloatind() {
		return fixfloatind;
	}

	public void setFixfloatind(String fixfloatind) {
		this.fixfloatind = fixfloatind;
	}

	public BigDecimal getInitexchamt() {
		return initexchamt;
	}

	public void setInitexchamt(BigDecimal initexchamt) {
		this.initexchamt = initexchamt;
	}

	public BigDecimal getInitexchbamt() {
		return initexchbamt;
	}

	public void setInitexchbamt(BigDecimal initexchbamt) {
		this.initexchbamt = initexchbamt;
	}

	public String getInitexchauthdte() {
		return initexchauthdte;
	}

	public void setInitexchauthdte(String initexchauthdte) {
		this.initexchauthdte = initexchauthdte;
	}

	public String getInitexchauthind() {
		return initexchauthind;
	}

	public void setInitexchauthind(String initexchauthind) {
		this.initexchauthind = initexchauthind;
	}

	public String getInitexchccy() {
		return initexchccy;
	}

	public void setInitexchccy(String initexchccy) {
		this.initexchccy = initexchccy;
	}

	public String getInitexchdate() {
		return initexchdate;
	}

	public void setInitexchdate(String initexchdate) {
		this.initexchdate = initexchdate;
	}

	public String getInitexchfincent() {
		return initexchfincent;
	}

	public void setInitexchfincent(String initexchfincent) {
		this.initexchfincent = initexchfincent;
	}

	public String getInitexchoper() {
		return initexchoper;
	}

	public void setInitexchoper(String initexchoper) {
		this.initexchoper = initexchoper;
	}

	public String getInitexchsmeans() {
		return initexchsmeans;
	}

	public void setInitexchsmeans(String initexchsmeans) {
		this.initexchsmeans = initexchsmeans;
	}

	public String getInitexchsacct() {
		return initexchsacct;
	}

	public void setInitexchsacct(String initexchsacct) {
		this.initexchsacct = initexchsacct;
	}

	public String getIntauthdte() {
		return intauthdte;
	}

	public void setIntauthdte(String intauthdte) {
		this.intauthdte = intauthdte;
	}

	public String getIntauthind() {
		return intauthind;
	}

	public void setIntauthind(String intauthind) {
		this.intauthind = intauthind;
	}

	public String getIntauthoper() {
		return intauthoper;
	}

	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}

	public String getIntconvdte() {
		return intconvdte;
	}

	public void setIntconvdte(String intconvdte) {
		this.intconvdte = intconvdte;
	}

	public BigDecimal getIntconvrate_8() {
		return intconvrate_8;
	}

	public void setIntconvrate_8(BigDecimal intconvrate_8) {
		this.intconvrate_8 = intconvrate_8;
	}

	public String getIntccy() {
		return intccy;
	}

	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}

	public BigDecimal getIntexchrate_8() {
		return intexchrate_8;
	}

	public void setIntexchrate_8(BigDecimal intexchrate_8) {
		this.intexchrate_8 = intexchrate_8;
	}

	public String getIntexchterms() {
		return intexchterms;
	}

	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}

	public String getIntpayadjust() {
		return intpayadjust;
	}

	public void setIntpayadjust(String intpayadjust) {
		this.intpayadjust = intpayadjust;
	}

	public String getIntpaycycle() {
		return intpaycycle;
	}

	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}

	public String getIntpayday() {
		return intpayday;
	}

	public void setIntpayday(String intpayday) {
		this.intpayday = intpayday;
	}

	public String getIntpayfincent() {
		return intpayfincent;
	}

	public void setIntpayfincent(String intpayfincent) {
		this.intpayfincent = intpayfincent;
	}

	public String getIntpaypayrule() {
		return intpaypayrule;
	}

	public void setIntpaypayrule(String intpaypayrule) {
		this.intpaypayrule = intpaypayrule;
	}

	public BigDecimal getIntrate_8() {
		return intrate_8;
	}

	public void setIntrate_8(BigDecimal intrate_8) {
		this.intrate_8 = intrate_8;
	}

	public BigDecimal getIntratecap_8() {
		return intratecap_8;
	}

	public void setIntratecap_8(BigDecimal intratecap_8) {
		this.intratecap_8 = intratecap_8;
	}

	public BigDecimal getIntratefloor_8() {
		return intratefloor_8;
	}

	public void setIntratefloor_8(BigDecimal intratefloor_8) {
		this.intratefloor_8 = intratefloor_8;
	}

	public String getIntsacct() {
		return intsacct;
	}

	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}

	public String getIntsmeans() {
		return intsmeans;
	}

	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}

	public String getLastipaydate() {
		return lastipaydate;
	}

	public void setLastipaydate(String lastipaydate) {
		this.lastipaydate = lastipaydate;
	}

	public String getMatdate() {
		return matdate;
	}

	public void setMatdate(String matdate) {
		this.matdate = matdate;
	}

	public String getNotccy() {
		return notccy;
	}

	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}

	public BigDecimal getNotccyamt() {
		return notccyamt;
	}

	public void setNotccyamt(BigDecimal notccyamt) {
		this.notccyamt = notccyamt;
	}

	public BigDecimal getNpvamt() {
		return npvamt;
	}

	public void setNpvamt(BigDecimal npvamt) {
		this.npvamt = npvamt;
	}

	public String getNxtraterev() {
		return nxtraterev;
	}

	public void setNxtraterev(String nxtraterev) {
		this.nxtraterev = nxtraterev;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public String getRaterevcycle() {
		return raterevcycle;
	}

	public void setRaterevcycle(String raterevcycle) {
		this.raterevcycle = raterevcycle;
	}

	public String getRaterevday() {
		return raterevday;
	}

	public void setRaterevday(String raterevday) {
		this.raterevday = raterevday;
	}

	public String getRaterevdte() {
		return raterevdte;
	}

	public void setRaterevdte(String raterevdte) {
		this.raterevdte = raterevdte;
	}

	public String getRaterevfincent() {
		return raterevfincent;
	}

	public void setRaterevfincent(String raterevfincent) {
		this.raterevfincent = raterevfincent;
	}

	public String getRaterevfrstlst() {
		return raterevfrstlst;
	}

	public void setRaterevfrstlst(String raterevfrstlst) {
		this.raterevfrstlst = raterevfrstlst;
	}

	public String getRaterevpayrule() {
		return raterevpayrule;
	}

	public void setRaterevpayrule(String raterevpayrule) {
		this.raterevpayrule = raterevpayrule;
	}

	public String getSchdflag() {
		return schdflag;
	}

	public void setSchdflag(String schdflag) {
		this.schdflag = schdflag;
	}

	public BigDecimal getSpread_8() {
		return spread_8;
	}

	public void setSpread_8(BigDecimal spread_8) {
		this.spread_8 = spread_8;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getUpfrontfeeno() {
		return upfrontfeeno;
	}

	public void setUpfrontfeeno(String upfrontfeeno) {
		this.upfrontfeeno = upfrontfeeno;
	}

	public String getUpfrontfeenettype() {
		return upfrontfeenettype;
	}

	public void setUpfrontfeenettype(String upfrontfeenettype) {
		this.upfrontfeenettype = upfrontfeenettype;
	}

	public String getYieldcurve() {
		return yieldcurve;
	}

	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}

	public String getAcrfrstlst() {
		return acrfrstlst;
	}

	public void setAcrfrstlst(String acrfrstlst) {
		this.acrfrstlst = acrfrstlst;
	}

	public BigDecimal getNpvamtinitexch() {
		return npvamtinitexch;
	}

	public void setNpvamtinitexch(BigDecimal npvamtinitexch) {
		this.npvamtinitexch = npvamtinitexch;
	}

	public BigDecimal getNpvamtfinexch() {
		return npvamtfinexch;
	}

	public void setNpvamtfinexch(BigDecimal npvamtfinexch) {
		this.npvamtfinexch = npvamtfinexch;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public BigDecimal getStrike_8() {
		return strike_8;
	}

	public void setStrike_8(BigDecimal strike_8) {
		this.strike_8 = strike_8;
	}

	public BigDecimal getIndex_8() {
		return index_8;
	}

	public void setIndex_8(BigDecimal index_8) {
		this.index_8 = index_8;
	}

	public BigDecimal getParticperc_8() {
		return particperc_8;
	}

	public void setParticperc_8(BigDecimal particperc_8) {
		this.particperc_8 = particperc_8;
	}

	public BigDecimal getDelta_8() {
		return delta_8;
	}

	public void setDelta_8(BigDecimal delta_8) {
		this.delta_8 = delta_8;
	}

	public BigDecimal getVolatility_8() {
		return volatility_8;
	}

	public void setVolatility_8(BigDecimal volatility_8) {
		this.volatility_8 = volatility_8;
	}

	public String getSettfirst() {
		return settfirst;
	}

	public void setSettfirst(String settfirst) {
		this.settfirst = settfirst;
	}

	public String getCollarind() {
		return collarind;
	}

	public void setCollarind(String collarind) {
		this.collarind = collarind;
	}

	public BigDecimal getNpvbamt() {
		return npvbamt;
	}

	public void setNpvbamt(BigDecimal npvbamt) {
		this.npvbamt = npvbamt;
	}

	public BigDecimal getTdyaccrintamt() {
		return tdyaccrintamt;
	}

	public void setTdyaccrintamt(BigDecimal tdyaccrintamt) {
		this.tdyaccrintamt = tdyaccrintamt;
	}

	public BigDecimal getTdyaccrintbamt() {
		return tdyaccrintbamt;
	}

	public void setTdyaccrintbamt(BigDecimal tdyaccrintbamt) {
		this.tdyaccrintbamt = tdyaccrintbamt;
	}

	public BigDecimal getYstaccrintamt() {
		return ystaccrintamt;
	}

	public void setYstaccrintamt(BigDecimal ystaccrintamt) {
		this.ystaccrintamt = ystaccrintamt;
	}

	public BigDecimal getYstaccrintbamt() {
		return ystaccrintbamt;
	}

	public void setYstaccrintbamt(BigDecimal ystaccrintbamt) {
		this.ystaccrintbamt = ystaccrintbamt;
	}

	public BigDecimal getNpvbamtinitexch() {
		return npvbamtinitexch;
	}

	public void setNpvbamtinitexch(BigDecimal npvbamtinitexch) {
		this.npvbamtinitexch = npvbamtinitexch;
	}

	public BigDecimal getNpvbamtfinexch() {
		return npvbamtfinexch;
	}

	public void setNpvbamtfinexch(BigDecimal npvbamtfinexch) {
		this.npvbamtfinexch = npvbamtfinexch;
	}

	public String getUpfrontfeefincent() {
		return upfrontfeefincent;
	}

	public void setUpfrontfeefincent(String upfrontfeefincent) {
		this.upfrontfeefincent = upfrontfeefincent;
	}

	public String getUpfrontfeeind() {
		return upfrontfeeind;
	}

	public void setUpfrontfeeind(String upfrontfeeind) {
		this.upfrontfeeind = upfrontfeeind;
	}

	public BigDecimal getTdycomprate_8() {
		return tdycomprate_8;
	}

	public void setTdycomprate_8(BigDecimal tdycomprate_8) {
		this.tdycomprate_8 = tdycomprate_8;
	}

	public BigDecimal getTdycompamt() {
		return tdycompamt;
	}

	public void setTdycompamt(BigDecimal tdycompamt) {
		this.tdycompamt = tdycompamt;
	}

	public BigDecimal getYstcomprate_8() {
		return ystcomprate_8;
	}

	public void setYstcomprate_8(BigDecimal ystcomprate_8) {
		this.ystcomprate_8 = ystcomprate_8;
	}

	public BigDecimal getYstcompamt() {
		return ystcompamt;
	}

	public void setYstcompamt(BigDecimal ystcompamt) {
		this.ystcompamt = ystcompamt;
	}

	public String getExchrevdate() {
		return exchrevdate;
	}

	public void setExchrevdate(String exchrevdate) {
		this.exchrevdate = exchrevdate;
	}

	public String getDatedirection() {
		return datedirection;
	}

	public void setDatedirection(String datedirection) {
		this.datedirection = datedirection;
	}

	public BigDecimal getPurchint() {
		return purchint;
	}

	public void setPurchint(BigDecimal purchint) {
		this.purchint = purchint;
	}

	public String getLastcoupondate() {
		return lastcoupondate;
	}

	public void setLastcoupondate(String lastcoupondate) {
		this.lastcoupondate = lastcoupondate;
	}

	public BigDecimal getPremdisc() {
		return premdisc;
	}

	public void setPremdisc(BigDecimal premdisc) {
		this.premdisc = premdisc;
	}

	public String getExchraterevdate() {
		return exchraterevdate;
	}

	public void setExchraterevdate(String exchraterevdate) {
		this.exchraterevdate = exchraterevdate;
	}

	public BigDecimal getAmount1() {
		return amount1;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public BigDecimal getAmount3() {
		return amount3;
	}

	public void setAmount3(BigDecimal amount3) {
		this.amount3 = amount3;
	}

	public String getChar1() {
		return char1;
	}

	public void setChar1(String char1) {
		this.char1 = char1;
	}

	public String getChar2() {
		return char2;
	}

	public void setChar2(String char2) {
		this.char2 = char2;
	}

	public String getFormulaind() {
		return formulaind;
	}

	public void setFormulaind(String formulaind) {
		this.formulaind = formulaind;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getFlag1() {
		return flag1;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public BigDecimal getRate1_8() {
		return rate1_8;
	}

	public void setRate1_8(BigDecimal rate1_8) {
		this.rate1_8 = rate1_8;
	}

	public BigDecimal getRate2_8() {
		return rate2_8;
	}

	public void setRate2_8(BigDecimal rate2_8) {
		this.rate2_8 = rate2_8;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public String getBtbusualid() {
		return btbusualid;
	}

	public void setBtbusualid(String btbusualid) {
		this.btbusualid = btbusualid;
	}

	public String getMbsleg() {
		return mbsleg;
	}

	public void setMbsleg(String mbsleg) {
		this.mbsleg = mbsleg;
	}

	public BigDecimal getSpeed_12() {
		return speed_12;
	}

	public void setSpeed_12(BigDecimal speed_12) {
		this.speed_12 = speed_12;
	}

	public BigDecimal getServicing_12() {
		return servicing_12;
	}

	public void setServicing_12(BigDecimal servicing_12) {
		this.servicing_12 = servicing_12;
	}

	public String getPrepaytype() {
		return prepaytype;
	}

	public void setPrepaytype(String prepaytype) {
		this.prepaytype = prepaytype;
	}

	public String getSettnotional() {
		return settnotional;
	}

	public void setSettnotional(String settnotional) {
		this.settnotional = settnotional;
	}

	public BigDecimal getFactor_12() {
		return factor_12;
	}

	public void setFactor_12(BigDecimal factor_12) {
		this.factor_12 = factor_12;
	}

	public String getFactorrevdate() {
		return factorrevdate;
	}

	public void setFactorrevdate(String factorrevdate) {
		this.factorrevdate = factorrevdate;
	}

	public BigDecimal getOrigprinadjamt() {
		return origprinadjamt;
	}

	public void setOrigprinadjamt(BigDecimal origprinadjamt) {
		this.origprinadjamt = origprinadjamt;
	}

	public String getIntdeal() {
		return intdeal;
	}

	public void setIntdeal(String intdeal) {
		this.intdeal = intdeal;
	}

	public BigDecimal getInternalrate_8() {
		return internalrate_8;
	}

	public void setInternalrate_8(BigDecimal internalrate_8) {
		this.internalrate_8 = internalrate_8;
	}

	public BigDecimal getCorpspread_8() {
		return corpspread_8;
	}

	public void setCorpspread_8(BigDecimal corpspread_8) {
		this.corpspread_8 = corpspread_8;
	}

	public BigDecimal getBcreditamt() {
		return bcreditamt;
	}

	public void setBcreditamt(BigDecimal bcreditamt) {
		this.bcreditamt = bcreditamt;
	}

	public BigDecimal getBmtmamt() {
		return bmtmamt;
	}

	public void setBmtmamt(BigDecimal bmtmamt) {
		this.bmtmamt = bmtmamt;
	}

	public BigDecimal getBmktamt() {
		return bmktamt;
	}

	public void setBmktamt(BigDecimal bmktamt) {
		this.bmktamt = bmktamt;
	}

	public BigDecimal getExchrate_8() {
		return exchrate_8;
	}

	public void setExchrate_8(BigDecimal exchrate_8) {
		this.exchrate_8 = exchrate_8;
	}

	public String getExchterms() {
		return exchterms;
	}

	public void setExchterms(String exchterms) {
		this.exchterms = exchterms;
	}

	public BigDecimal getInitbaseamt() {
		return initbaseamt;
	}

	public void setInitbaseamt(BigDecimal initbaseamt) {
		this.initbaseamt = initbaseamt;
	}

	public BigDecimal getInitfinamt() {
		return initfinamt;
	}

	public void setInitfinamt(BigDecimal initfinamt) {
		this.initfinamt = initfinamt;
	}

	public BigDecimal getInitfinbaseamt() {
		return initfinbaseamt;
	}

	public void setInitfinbaseamt(BigDecimal initfinbaseamt) {
		this.initfinbaseamt = initfinbaseamt;
	}

	public BigDecimal getInitfinexchrate_8() {
		return initfinexchrate_8;
	}

	public void setInitfinexchrate_8(BigDecimal initfinexchrate_8) {
		this.initfinexchrate_8 = initfinexchrate_8;
	}

	public BigDecimal getInitbaseexchrate_8() {
		return initbaseexchrate_8;
	}

	public void setInitbaseexchrate_8(BigDecimal initbaseexchrate_8) {
		this.initbaseexchrate_8 = initbaseexchrate_8;
	}

	public String getInitfinexchterms() {
		return initfinexchterms;
	}

	public void setInitfinexchterms(String initfinexchterms) {
		this.initfinexchterms = initfinexchterms;
	}

	public String getInitbaseterms() {
		return initbaseterms;
	}

	public void setInitbaseterms(String initbaseterms) {
		this.initbaseterms = initbaseterms;
	}

	public String getFlag2() {
		return flag2;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public String getFlag3() {
		return flag3;
	}

	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}

	public BigDecimal getRate3_8() {
		return rate3_8;
	}

	public void setRate3_8(BigDecimal rate3_8) {
		this.rate3_8 = rate3_8;
	}

	public BigDecimal getRate4_8() {
		return rate4_8;
	}

	public void setRate4_8(BigDecimal rate4_8) {
		this.rate4_8 = rate4_8;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public String getDate4() {
		return date4;
	}

	public void setDate4(String date4) {
		this.date4 = date4;
	}

	public BigDecimal getAmount4() {
		return amount4;
	}

	public void setAmount4(BigDecimal amount4) {
		this.amount4 = amount4;
	}

	public BigDecimal getAmount5() {
		return amount5;
	}

	public void setAmount5(BigDecimal amount5) {
		this.amount5 = amount5;
	}

	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}

	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}

	public String getPostnotional() {
		return postnotional;
	}

	public void setPostnotional(String postnotional) {
		this.postnotional = postnotional;
	}

	public String getBasketdescr() {
		return basketdescr;
	}

	public void setBasketdescr(String basketdescr) {
		this.basketdescr = basketdescr;
	}

	public BigDecimal getConvprice_12() {
		return convprice_12;
	}

	public void setConvprice_12(BigDecimal convprice_12) {
		this.convprice_12 = convprice_12;
	}

	public String getFwdfwdyieldcurve() {
		return fwdfwdyieldcurve;
	}

	public void setFwdfwdyieldcurve(String fwdfwdyieldcurve) {
		this.fwdfwdyieldcurve = fwdfwdyieldcurve;
	}

	public String getEquitytype() {
		return equitytype;
	}

	public void setEquitytype(String equitytype) {
		this.equitytype = equitytype;
	}

	public String getIntnegauthdte() {
		return intnegauthdte;
	}

	public void setIntnegauthdte(String intnegauthdte) {
		this.intnegauthdte = intnegauthdte;
	}

	public String getIntnegauthind() {
		return intnegauthind;
	}

	public void setIntnegauthind(String intnegauthind) {
		this.intnegauthind = intnegauthind;
	}

	public String getIntnegauthoper() {
		return intnegauthoper;
	}

	public void setIntnegauthoper(String intnegauthoper) {
		this.intnegauthoper = intnegauthoper;
	}

	public String getIntnegsacct() {
		return intnegsacct;
	}

	public void setIntnegsacct(String intnegsacct) {
		this.intnegsacct = intnegsacct;
	}

	public String getIntnegsmeans() {
		return intnegsmeans;
	}

	public void setIntnegsmeans(String intnegsmeans) {
		this.intnegsmeans = intnegsmeans;
	}

	public BigDecimal getPrice_12() {
		return price_12;
	}

	public void setPrice_12(BigDecimal price_12) {
		this.price_12 = price_12;
	}

	public BigDecimal getPriceexchrate_8() {
		return priceexchrate_8;
	}

	public void setPriceexchrate_8(BigDecimal priceexchrate_8) {
		this.priceexchrate_8 = priceexchrate_8;
	}

	public String getPriceexchterms() {
		return priceexchterms;
	}

	public void setPriceexchterms(String priceexchterms) {
		this.priceexchterms = priceexchterms;
	}

	public String getRepriceind() {
		return repriceind;
	}

	public void setRepriceind(String repriceind) {
		this.repriceind = repriceind;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getSettdays() {
		return settdays;
	}

	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}

	public String getSecccy() {
		return secccy;
	}

	public void setSecccy(String secccy) {
		this.secccy = secccy;
	}

	public String getRepricedate() {
		return repricedate;
	}

	public void setRepricedate(String repricedate) {
		this.repricedate = repricedate;
	}

	public BigDecimal getRateofreturn_12() {
		return rateofreturn_12;
	}

	public void setRateofreturn_12(BigDecimal rateofreturn_12) {
		this.rateofreturn_12 = rateofreturn_12;
	}

	public BigDecimal getDivpercent_8() {
		return divpercent_8;
	}

	public void setDivpercent_8(BigDecimal divpercent_8) {
		this.divpercent_8 = divpercent_8;
	}

	public String getDivpaydateind() {
		return divpaydateind;
	}

	public void setDivpaydateind(String divpaydateind) {
		this.divpaydateind = divpaydateind;
	}

	public String getDivreinvestind() {
		return divreinvestind;
	}

	public void setDivreinvestind(String divreinvestind) {
		this.divreinvestind = divreinvestind;
	}

	public String getDivpayccyind() {
		return divpayccyind;
	}

	public void setDivpayccyind(String divpayccyind) {
		this.divpayccyind = divpayccyind;
	}

	public BigDecimal getMultiplier_8() {
		return multiplier_8;
	}

	public void setMultiplier_8(BigDecimal multiplier_8) {
		this.multiplier_8 = multiplier_8;
	}

	public String getReturntype() {
		return returntype;
	}

	public void setReturntype(String returntype) {
		this.returntype = returntype;
	}

	public String getValuationtime() {
		return valuationtime;
	}

	public void setValuationtime(String valuationtime) {
		this.valuationtime = valuationtime;
	}

	public String getNxtrepricedate() {
		return nxtrepricedate;
	}

	public void setNxtrepricedate(String nxtrepricedate) {
		this.nxtrepricedate = nxtrepricedate;
	}

	public String getRepricedays() {
		return repricedays;
	}

	public void setRepricedays(String repricedays) {
		this.repricedays = repricedays;
	}

	public String getExternalvalind() {
		return externalvalind;
	}

	public void setExternalvalind(String externalvalind) {
		this.externalvalind = externalvalind;
	}

	public String getNotifdays() {
		return notifdays;
	}

	public void setNotifdays(String notifdays) {
		this.notifdays = notifdays;
	}

	public String getNotiftime() {
		return notiftime;
	}

	public void setNotiftime(String notiftime) {
		this.notiftime = notiftime;
	}

	public BigDecimal getDealrate_8() {
		return dealrate_8;
	}

	public void setDealrate_8(BigDecimal dealrate_8) {
		this.dealrate_8 = dealrate_8;
	}

	public String getDealterms() {
		return dealterms;
	}

	public void setDealterms(String dealterms) {
		this.dealterms = dealterms;
	}

	public String getFxratecode() {
		return fxratecode;
	}

	public void setFxratecode(String fxratecode) {
		this.fxratecode = fxratecode;
	}

	public String getNextfxdate() {
		return nextfxdate;
	}

	public void setNextfxdate(String nextfxdate) {
		this.nextfxdate = nextfxdate;
	}

	public String getExchmethod() {
		return exchmethod;
	}

	public void setExchmethod(String exchmethod) {
		this.exchmethod = exchmethod;
	}

	public String getMulticcyacctind() {
		return multiccyacctind;
	}

	public void setMulticcyacctind(String multiccyacctind) {
		this.multiccyacctind = multiccyacctind;
	}

	public String getLegtype() {
		return legtype;
	}

	public void setLegtype(String legtype) {
		this.legtype = legtype;
	}

	public Short getSchedid() {
		return schedid;
	}

	public void setSchedid(Short schedid) {
		this.schedid = schedid;
	}

	public Short getProdid() {
		return prodid;
	}

	public void setProdid(Short prodid) {
		this.prodid = prodid;
	}

	public Short getFormulaid() {
		return formulaid;
	}

	public void setFormulaid(Short formulaid) {
		this.formulaid = formulaid;
	}

	public String getAccrualccy() {
		return accrualccy;
	}

	public void setAccrualccy(String accrualccy) {
		this.accrualccy = accrualccy;
	}

	public String getMktvalsource() {
		return mktvalsource;
	}

	public void setMktvalsource(String mktvalsource) {
		this.mktvalsource = mktvalsource;
	}

	public String getAccrualmethod() {
		return accrualmethod;
	}

	public void setAccrualmethod(String accrualmethod) {
		this.accrualmethod = accrualmethod;
	}

	public String getGrossufpayrecind() {
		return grossufpayrecind;
	}

	public void setGrossufpayrecind(String grossufpayrecind) {
		this.grossufpayrecind = grossufpayrecind;
	}

	public BigDecimal getGrossufpayamt() {
		return grossufpayamt;
	}

	public void setGrossufpayamt(BigDecimal grossufpayamt) {
		this.grossufpayamt = grossufpayamt;
	}

	public BigDecimal getGrossufpayrate_8() {
		return grossufpayrate_8;
	}

	public void setGrossufpayrate_8(BigDecimal grossufpayrate_8) {
		this.grossufpayrate_8 = grossufpayrate_8;
	}

	public String getSchdconv() {
		return schdconv;
	}

	public void setSchdconv(String schdconv) {
		this.schdconv = schdconv;
	}

	public String getRedemptionccyind() {
		return redemptionccyind;
	}

	public void setRedemptionccyind(String redemptionccyind) {
		this.redemptionccyind = redemptionccyind;
	}

	public String getNextfxprindate() {
		return nextfxprindate;
	}

	public void setNextfxprindate(String nextfxprindate) {
		this.nextfxprindate = nextfxprindate;
	}

	public BigDecimal getDeffactor() {
		return deffactor;
	}

	public void setDeffactor(BigDecimal deffactor) {
		this.deffactor = deffactor;
	}
}
