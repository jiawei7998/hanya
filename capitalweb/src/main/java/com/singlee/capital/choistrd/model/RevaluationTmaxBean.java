package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class RevaluationTmaxBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *  
FXFIG_SVCN
FXFIG_IBCD
FXFIG_GWAM
FXFIG_GEOR
FXFIG_OPNO
FIFE14_OPICS_REF_NO
FIFE14_DEAL_IL
FIFE14_CCY
FIFE14_REVALU_AMT

	 */
	private String FXFIG_SVCN;
	private String FXFIG_IBCD;
	private String FXFIG_GWAM;
	private String FXFIG_GEOR;
	private String FXFIG_OPNO;
	private String FIFE14_OPICS_REF_NO;
	private String FIFE14_DEAL_IL;
	private String FIFE14_CCY;
	private BigDecimal FIFE14_REVALU_AMT;
	
	public RevaluationTmaxBean(){}
	/**
	 * @return the fXFIG_SVCN
	 */
	public String getFXFIG_SVCN() {
		return FXFIG_SVCN;
	}
	/**
	 * @param fxfig_svcn the fXFIG_SVCN to set
	 */
	public void setFXFIG_SVCN(String fxfig_svcn) {
		FXFIG_SVCN = fxfig_svcn;
	}
	/**
	 * @return the fXFIG_IBCD
	 */
	public String getFXFIG_IBCD() {
		return FXFIG_IBCD;
	}
	/**
	 * @param fxfig_ibcd the fXFIG_IBCD to set
	 */
	public void setFXFIG_IBCD(String fxfig_ibcd) {
		FXFIG_IBCD = fxfig_ibcd;
	}
	/**
	 * @return the fXFIG_GWAM
	 */
	public String getFXFIG_GWAM() {
		return FXFIG_GWAM;
	}
	/**
	 * @param fxfig_gwam the fXFIG_GWAM to set
	 */
	public void setFXFIG_GWAM(String fxfig_gwam) {
		FXFIG_GWAM = fxfig_gwam;
	}
	/**
	 * @return the fXFIG_GEOR
	 */
	public String getFXFIG_GEOR() {
		return FXFIG_GEOR;
	}
	/**
	 * @param fxfig_geor the fXFIG_GEOR to set
	 */
	public void setFXFIG_GEOR(String fxfig_geor) {
		FXFIG_GEOR = fxfig_geor;
	}
	/**
	 * @return the fXFIG_OPNO
	 */
	public String getFXFIG_OPNO() {
		return FXFIG_OPNO;
	}
	/**
	 * @param fxfig_opno the fXFIG_OPNO to set
	 */
	public void setFXFIG_OPNO(String fxfig_opno) {
		FXFIG_OPNO = fxfig_opno;
	}
	/**
	 * @return the fIFE14_OPICS_REF_NO
	 */
	public String getFIFE14_OPICS_REF_NO() {
		return FIFE14_OPICS_REF_NO;
	}
	/**
	 * @param fife14_opics_ref_no the fIFE14_OPICS_REF_NO to set
	 */
	public void setFIFE14_OPICS_REF_NO(String fife14_opics_ref_no) {
		FIFE14_OPICS_REF_NO = fife14_opics_ref_no;
	}
	/**
	 * @return the fIFE14_DEAL_IL
	 */
	public String getFIFE14_DEAL_IL() {
		return FIFE14_DEAL_IL;
	}
	/**
	 * @param fife14_deal_il the fIFE14_DEAL_IL to set
	 */
	public void setFIFE14_DEAL_IL(String fife14_deal_il) {
		FIFE14_DEAL_IL = fife14_deal_il;
	}
	/**
	 * @return the fIFE14_CCY
	 */
	public String getFIFE14_CCY() {
		return FIFE14_CCY;
	}
	/**
	 * @param fife14_ccy the fIFE14_CCY to set
	 */
	public void setFIFE14_CCY(String fife14_ccy) {
		FIFE14_CCY = fife14_ccy;
	}
	/**
	 * @return the fIFE14_REVALU_AMT
	 */
	public BigDecimal getFIFE14_REVALU_AMT() {
		return FIFE14_REVALU_AMT;
	}
	/**
	 * @param fife14_revalu_amt the fIFE14_REVALU_AMT to set
	 */
	public void setFIFE14_REVALU_AMT(BigDecimal fife14_revalu_amt) {
		FIFE14_REVALU_AMT = fife14_revalu_amt;
	}
}
