package com.singlee.capital.choistrd.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.StringUtils;
import com.singlee.capital.chois.util.SubStrChinese;
import com.singlee.capital.choistrd.mapper.CustQueryChoisMapper;
import com.singlee.capital.choistrd.mapper.SlIntfcGentMapper;
import com.singlee.capital.choistrd.model.CustCnoCcy;
import com.singlee.capital.choistrd.model.SL_CUST;

import com.singlee.capital.choistrd.model.SlMktStatus;
import com.singlee.capital.common.util.PropertiesUtil;
@Service
public class MktTmaxCommand {
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	@Autowired
	private SlIntfcGentMapper intfcGentMapper;
	public String FXFIG_SSCK;
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	ChiosLogin chiosLogin = new ChiosLogin();

	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();
	StringUtils stringUtils=new StringUtils();

	SubStrChinese subStrChinese=new SubStrChinese();
	public int executeHostVisitAction() throws Exception {
		Map<String, Object> instMap =new HashMap<String, Object>();
		Map<String, Object> insMap =new HashMap<String, Object>();
		Map<String, Object> insMap1 =new HashMap<String, Object>();
		Map<String, Object> insMap2 =new HashMap<String, Object>();
		Map<String, Object> insMap3 =new HashMap<String, Object>();		
		Map<String, Object> insMap4 =new HashMap<String, Object>();
		Map<String, Object> insMap5 =new HashMap<String, Object>();
		Map<String, Object> insMap6 =new HashMap<String, Object>();
		Map<String, Object> insMap7 =new HashMap<String, Object>();
		Map<String, Object> insMap8 =new HashMap<String, Object>();
		Map<String, Object> insMap9 =new HashMap<String, Object>();
		Map<String, Object> insMap10 =new HashMap<String, Object>();
		Map<String, Object> insMap11 =new HashMap<String, Object>();
		Map<String, Object> insMap12 =new HashMap<String, Object>();
		Map<String, Object> insMap13 =new HashMap<String, Object>();
		Map<String, Object> insMap14 =new HashMap<String, Object>();
		Map<String, Object> insMap15 =new HashMap<String, Object>();
		Map<String, Object> map =new HashMap<String, Object>();
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		
		
		try {
			 //返回的柜员session
			String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");

				//拼接报文
				
				StdTMsgGenerator generator = new StdTMsgGenerator();
				//报文头
				byte[] headerPacket=choisCommonMsg.makeHead();
				
				//报文体
				PropertiesConfiguration stdtmsgSysValue;
				
				stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
				 String handleCode="IQ6000";
				 String serveHandleCode="OPCIQ60";
				FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
				
				String coMsg=choisCommonMsg.makeMsg();//公共部分
				String coMsg2=choisCommonMsg.makeMsg2();//公共部分
				String coMsg3=choisCommonMsg.makeMsg3();//公共部分
				//交易数据
				String dealMsg=null;
			
				dealMsg=format.format(date)+"          "+"          "+"   ";
				//dealMsg="20230808"+"          "+"          "+"   ";
				byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
				byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
				String serviceUrl = "OPS";
				byte[] msgData =totalPacket;
				EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
				System.out.println("eaiDv:--"+eaiDv);
				byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
				System.out.println("市场数据返回信息："+new String(returnData, "UTF-8"));
				
				logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
		
				logger.info("RECV MSG:"+new String(returnData, "UTF-8"));
				//CHOIS参考号
				String recMsg=new String(returnData, "UTF-8");
				String resmg=null;
				resmg=((new String(returnData, "UTF-8")).substring(0,292).substring((new String(returnData, "UTF-8")).substring(0,292).length()-2));
				logger.info("resmg:"+resmg);
				int num = 0;
				for(int i = 0; i < (new String(returnData, "UTF-8")).length(); i++) {
					char c = new String(returnData, "UTF-8").charAt(i);
					if(subStrChinese.isChinese(c)) {
						num+=1;
					}
				}
				int len=(new String(returnData, "UTF-8")).length()+num*2;
				int endIndex = recMsg.lastIndexOf("OPCIQ60");
				String mkt = recMsg.substring(endIndex+7, recMsg.indexOf("@"));

				if(resmg.equals("10")) {
					int count = (len-2-385)/215;
					for(int i = 0; i<count;i++) {
						 Map<String, Object> kmap =new HashMap<String, Object>();
					     kmap.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						
						/*SlMktStatus slMkt=new SlMktStatus();
						slMkt.setMakeIl(subStrChinese.subChString(mkt,0+215*i,10).trim());						
						slMkt.setCcy(subStrChinese.subChString(mkt,20+215*i,3).trim());
						*/
						//SN
						insMap.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap.put("mtyend", "SN");
						insMap.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,23+215*i,12).trim()));
													
						List<SlMktStatus> slMkts=intfcGentMapper.queryMkt(insMap);
						//slMkt.setMtyend("SN");
						if(slMkts.size()>0) {
							intfcGentMapper.deleteMkt(insMap);
							
						}
						intfcGentMapper.insertMkt(insMap);
						//1D
						insMap1.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap1.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap1.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap1.put("mtyend", "1D");
						insMap1.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,35+215*i,12).trim()));
													
						List<SlMktStatus> slMkts1=intfcGentMapper.queryMkt(insMap1);
						//slMkt.setMtyend("1D");
						if(slMkts1.size()>0) {
							intfcGentMapper.deleteMkt(insMap1);
							
						}
						intfcGentMapper.insertMkt(insMap1);
						
						//1W
						insMap2.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap2.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap2.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap2.put("mtyend", "1W");
						insMap2.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,47+215*i,12).trim()));
													
						List<SlMktStatus> slMkts2=intfcGentMapper.queryMkt(insMap2);
						//slMkt.setMtyend("1W");
						if(slMkts2.size()>0) {
							intfcGentMapper.deleteMkt(insMap2);
							
						}
						intfcGentMapper.insertMkt(insMap2);
						//2W
						insMap3.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap3.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap3.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap3.put("mtyend", "2W");
						insMap3.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,59+215*i,12).trim()));
													
						List<SlMktStatus> slMkts3=intfcGentMapper.queryMkt(insMap3);
						//slMkt.setMtyend("2W");
						if(slMkts3.size()>0) {
							intfcGentMapper.deleteMkt(insMap3);
							
						}
						intfcGentMapper.insertMkt(insMap3);
						//1M
						insMap4.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap4.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap4.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap4.put("mtyend", "1M");
						insMap4.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,71+215*i,12).trim()));
													
						List<SlMktStatus> slMkts4=intfcGentMapper.queryMkt(insMap4);
						//slMkt.setMtyend("1M");
						if(slMkts4.size()>0) {
							intfcGentMapper.deleteMkt(insMap4);
							
						}
						intfcGentMapper.insertMkt(insMap4);
						//2M
						insMap5.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap5.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap5.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap5.put("mtyend", "2M");
						insMap5.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,83+215*i,12).trim()));
													
						List<SlMktStatus> slMkts5=intfcGentMapper.queryMkt(insMap5);
						//slMkt.setMtyend("2M");
						if(slMkts5.size()>0) {
							intfcGentMapper.deleteMkt(insMap5);
							
						}
						intfcGentMapper.insertMkt(insMap5);
						//3M
						insMap6.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap6.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap6.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap6.put("mtyend", "3M");
						insMap6.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,95+215*i,12).trim()));
													
						List<SlMktStatus> slMkts6=intfcGentMapper.queryMkt(insMap6);
						//slMkt.setMtyend("3M");
						if(slMkts6.size()>0) {
							intfcGentMapper.deleteMkt(insMap6);
							
						}
						intfcGentMapper.insertMkt(insMap6);
						//4M
						insMap7.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap7.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap7.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap7.put("mtyend", "4M");
						insMap7.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,107+215*i,12).trim()));
													
						List<SlMktStatus> slMkts7=intfcGentMapper.queryMkt(insMap7);
						//slMkt.setMtyend("4M");
						if(slMkts7.size()>0) {
							intfcGentMapper.deleteMkt(insMap7);
							
						}
						intfcGentMapper.insertMkt(insMap7);
						//5M
						insMap8.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap8.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap8.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap8.put("mtyend", "5M");
						insMap8.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,119+215*i,12).trim()));
													
						List<SlMktStatus> slMkts8=intfcGentMapper.queryMkt(insMap8);
						//slMkt.setMtyend("5M");
						if(slMkts8.size()>0) {
							intfcGentMapper.deleteMkt(insMap8);
							
						}
						intfcGentMapper.insertMkt(insMap8);
						//6M
						insMap9.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap9.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap9.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap9.put("mtyend", "6M");
						insMap9.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,131+215*i,12).trim()));
													
						List<SlMktStatus> slMkts9=intfcGentMapper.queryMkt(insMap9);
						//slMkt.setMtyend("6M");
						if(slMkts9.size()>0) {
							intfcGentMapper.deleteMkt(insMap9);
							
						}
						intfcGentMapper.insertMkt(insMap9);
						//7M
						insMap10.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap10.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap10.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap10.put("mtyend", "7M");
						insMap10.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,143+215*i,12).trim()));
													
						List<SlMktStatus> slMkts10=intfcGentMapper.queryMkt(insMap10);
						//slMkt.setMtyend("7M");
						if(slMkts10.size()>0) {
							intfcGentMapper.deleteMkt(insMap10);
							
						}
						intfcGentMapper.insertMkt(insMap10);
						//8M
						insMap11.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap11.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap11.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap11.put("mtyend", "8M");
						insMap11.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,155+215*i,12).trim()));
													
						List<SlMktStatus> slMkts11=intfcGentMapper.queryMkt(insMap11);
						//slMkt.setMtyend("8M");
						if(slMkts11.size()>0) {
							intfcGentMapper.deleteMkt(insMap11);
							
						}
						intfcGentMapper.insertMkt(insMap11);
						//9M
						insMap12.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap12.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap12.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap12.put("mtyend", "9M");
						insMap12.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,167+215*i,12).trim()));
													
						List<SlMktStatus> slMkts12=intfcGentMapper.queryMkt(insMap12);
						//slMkt.setMtyend("9M");
						if(slMkts12.size()>0) {
							intfcGentMapper.deleteMkt(insMap12);
							
						}
						intfcGentMapper.insertMkt(insMap12);
						//10M
						insMap13.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap13.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap13.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap13.put("mtyend", "10M");
						insMap13.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,179+215*i,12).trim()));
													
						List<SlMktStatus> slMkts13=intfcGentMapper.queryMkt(insMap13);
						//slMkt.setMtyend("10M");
						if(slMkts13.size()>0) {
							intfcGentMapper.deleteMkt(insMap13);
							
						}
						intfcGentMapper.insertMkt(insMap13);
						//11M
						insMap14.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap14.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap14.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap14.put("mtyend", "11M");
						insMap14.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,191+215*i,12).trim()));
													
						List<SlMktStatus> slMkts14=intfcGentMapper.queryMkt(insMap14);
						//slMkt.setMtyend("11M");
						if(slMkts14.size()>0) {
							intfcGentMapper.deleteMkt(insMap14);
							
						}
						intfcGentMapper.insertMkt(insMap14);
						//12M
						insMap15.put("makeIl", subStrChinese.subChString(mkt,0+215*i,10).trim());
						insMap15.put("rtType", subStrChinese.subChString(mkt,10+215*i,10).trim());
						insMap15.put("ccy", subStrChinese.subChString(mkt,20+215*i,3).trim());
						insMap15.put("mtyend", "1Y");
						insMap15.put("price", stringUtils.setNum(subStrChinese.subChString(mkt,203+215*i,12).trim()));
													
						List<SlMktStatus> slMkts15=intfcGentMapper.queryMkt(insMap15);
						//slMkt.setMtyend("12M");
						if(slMkts15.size()>0) {
							intfcGentMapper.deleteMkt(insMap15);
							
						}
						intfcGentMapper.insertMkt(insMap15);
					}
				}else {
					logger.info("接收返回的错误报文:"+new String(returnData, "UTF-8"));
				}				
			
		}catch(Exception e) {
			logger.info("Exception MSG:"+e.getMessage());
		}
		   return 0;
		}
	
}
