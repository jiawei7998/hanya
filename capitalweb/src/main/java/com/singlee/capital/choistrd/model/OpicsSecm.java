
package com.singlee.capital.choistrd.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



public class OpicsSecm implements Serializable {
    private String secid;

    private String acctngtype;

    private Date calldate;

    private BigDecimal callprice8;

    private BigDecimal clsgindxrate8;

    private BigDecimal convcouprate8;

    private Date convdate;

    private String couponno;

    private BigDecimal couprate8;

    private Date coupenddate;

    private String ccy;

    private String denom;

    private String descr;

    private String endtime;

    private Date firstipaydate;

    private String fixccy;

    private String guarantor;

    private String infratecode;

    private BigDecimal initindexrate8;

    private String basis;

    private String intcalcrule;

    private String intpaycycle;

    private String intpayrule;

    private String ratecode;

    private Date issdate;

    private String issuer;

    private Date lastipaydate;

    private Date lstmntdate;

    private String markmakind;

    private Date mdate;

    private String mutfundind;

    private String normcoupperiod;

    private BigDecimal paramt;

    private String pricedecs;

    private String pricerndrule;

    private String product;

    private String prodtype;

    private Date putdate;

    private BigDecimal putprice8;

    private String ratedecs;

    private String raterndrule;

    private String ratetol;

    private BigDecimal redempamt;

    private String repocollpct;

    private String series;

    private String settdays;

    private BigDecimal spreadrate8;

    private String starttime;

    private String symbolid;

    private String taxind;

    private String tenor;

    private String tradstat;

    private String secunit;

    private Date vdate;

    private String exchcno;

    private String whaccno;

    private String settccy;

    private String pricetol;

    private Date calltopar;

    private Date puttopar;

    private String mindenom;

    private String minincrement;

    private String linksecid;

    private String intccy;

    private BigDecimal intexchrate8;

    private String intexchterms;

    private String secmsic;

    private String repoflag;

    private String exdivdays;

    private String dcpriceind;

    private String fincent;

    private String exercisedays;

    private String intenddaterule;

    private String shortcoupind;

    private String emtaind;

    private String publishdays;

    private String annpers;

    private String fixpers;

    private BigDecimal fixrate8;

    private String cashdecs;

    private String aidecs;

    private String pmtfreq;

    private String prinpmtfreq;

    private String fixfreq;

    private BigDecimal maxmfix8;

    private BigDecimal maxmlife8;

    private String intrndrule;

    private String minincrmt;

    private String noamorts;

    private BigDecimal lastamort8;

    private String pitype;

    private Long updatecounter;

    private String physdelind;

    private String dvpind;

    private String recorddateind;

    private BigDecimal perratefloor12;

    private BigDecimal liferatefloor12;

    private String delaydays;

    private String pmtday;

    private String settleclass;

    private BigDecimal pmtcap12;

    private String poolno;

    private String cashflowind;

    private String prepmtmodel;

    private BigDecimal withtaxrate12;

    private String factsecind;

    private BigDecimal amt1;

    private BigDecimal amt2;

    private BigDecimal rate112;

    private BigDecimal rate212;

    private Date date1;

    private Date date2;

    private String indexratecode;

    private String ilind;

    private String deflationind;

    private String ildecs;

    private String ngpriceind;

    private String cpidaycycle;

    private BigDecimal schedid;

    private BigDecimal prodid;

    private String structid;

    private String mktvalsource;

    private BigDecimal formulaid;

    private String intsettccy;

    private String ratrevfirstlast;

    private String ratefixdays;

    private String dtcind;

    private String fedind;

    private String euroclearind;

    private String clearstreamind;

    private String localmarket;

    private String repotrackingind;

    private String templateind;

    private String endofmonthind;

    private String regenind;

    private Date regendate;

    private String allownegativeint;

    
    
    public String getSecid() {
		return secid;
	}



	public void setSecid(String secid) {
		this.secid = secid;
	}



	public String getAcctngtype() {
		return acctngtype;
	}



	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}



	public Date getCalldate() {
		return calldate;
	}



	public void setCalldate(Date calldate) {
		this.calldate = calldate;
	}



	public BigDecimal getCallprice8() {
		return callprice8;
	}



	public void setCallprice8(BigDecimal callprice8) {
		this.callprice8 = callprice8;
	}



	public BigDecimal getClsgindxrate8() {
		return clsgindxrate8;
	}



	public void setClsgindxrate8(BigDecimal clsgindxrate8) {
		this.clsgindxrate8 = clsgindxrate8;
	}



	public BigDecimal getConvcouprate8() {
		return convcouprate8;
	}



	public void setConvcouprate8(BigDecimal convcouprate8) {
		this.convcouprate8 = convcouprate8;
	}



	public Date getConvdate() {
		return convdate;
	}



	public void setConvdate(Date convdate) {
		this.convdate = convdate;
	}



	public String getCouponno() {
		return couponno;
	}



	public void setCouponno(String couponno) {
		this.couponno = couponno;
	}



	public BigDecimal getCouprate8() {
		return couprate8;
	}



	public void setCouprate8(BigDecimal couprate8) {
		this.couprate8 = couprate8;
	}



	public Date getCoupenddate() {
		return coupenddate;
	}



	public void setCoupenddate(Date coupenddate) {
		this.coupenddate = coupenddate;
	}



	public String getCcy() {
		return ccy;
	}



	public void setCcy(String ccy) {
		this.ccy = ccy;
	}



	public String getDenom() {
		return denom;
	}



	public void setDenom(String denom) {
		this.denom = denom;
	}



	public String getDescr() {
		return descr;
	}



	public void setDescr(String descr) {
		this.descr = descr;
	}



	public String getEndtime() {
		return endtime;
	}



	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}



	public Date getFirstipaydate() {
		return firstipaydate;
	}



	public void setFirstipaydate(Date firstipaydate) {
		this.firstipaydate = firstipaydate;
	}



	public String getFixccy() {
		return fixccy;
	}



	public void setFixccy(String fixccy) {
		this.fixccy = fixccy;
	}



	public String getGuarantor() {
		return guarantor;
	}



	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}



	public String getInfratecode() {
		return infratecode;
	}



	public void setInfratecode(String infratecode) {
		this.infratecode = infratecode;
	}



	public BigDecimal getInitindexrate8() {
		return initindexrate8;
	}



	public void setInitindexrate8(BigDecimal initindexrate8) {
		this.initindexrate8 = initindexrate8;
	}



	public String getBasis() {
		return basis;
	}



	public void setBasis(String basis) {
		this.basis = basis;
	}



	public String getIntcalcrule() {
		return intcalcrule;
	}



	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}



	public String getIntpaycycle() {
		return intpaycycle;
	}



	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}



	public String getIntpayrule() {
		return intpayrule;
	}



	public void setIntpayrule(String intpayrule) {
		this.intpayrule = intpayrule;
	}



	public String getRatecode() {
		return ratecode;
	}



	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}



	public Date getIssdate() {
		return issdate;
	}



	public void setIssdate(Date issdate) {
		this.issdate = issdate;
	}



	public String getIssuer() {
		return issuer;
	}



	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}



	public Date getLastipaydate() {
		return lastipaydate;
	}



	public void setLastipaydate(Date lastipaydate) {
		this.lastipaydate = lastipaydate;
	}



	public Date getLstmntdate() {
		return lstmntdate;
	}



	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}



	public String getMarkmakind() {
		return markmakind;
	}



	public void setMarkmakind(String markmakind) {
		this.markmakind = markmakind;
	}



	public Date getMdate() {
		return mdate;
	}



	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}



	public String getMutfundind() {
		return mutfundind;
	}



	public void setMutfundind(String mutfundind) {
		this.mutfundind = mutfundind;
	}



	public String getNormcoupperiod() {
		return normcoupperiod;
	}



	public void setNormcoupperiod(String normcoupperiod) {
		this.normcoupperiod = normcoupperiod;
	}



	public BigDecimal getParamt() {
		return paramt;
	}



	public void setParamt(BigDecimal paramt) {
		this.paramt = paramt;
	}



	public String getPricedecs() {
		return pricedecs;
	}



	public void setPricedecs(String pricedecs) {
		this.pricedecs = pricedecs;
	}



	public String getPricerndrule() {
		return pricerndrule;
	}



	public void setPricerndrule(String pricerndrule) {
		this.pricerndrule = pricerndrule;
	}



	public String getProduct() {
		return product;
	}



	public void setProduct(String product) {
		this.product = product;
	}



	public String getProdtype() {
		return prodtype;
	}



	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}



	public Date getPutdate() {
		return putdate;
	}



	public void setPutdate(Date putdate) {
		this.putdate = putdate;
	}



	public BigDecimal getPutprice8() {
		return putprice8;
	}



	public void setPutprice8(BigDecimal putprice8) {
		this.putprice8 = putprice8;
	}



	public String getRatedecs() {
		return ratedecs;
	}



	public void setRatedecs(String ratedecs) {
		this.ratedecs = ratedecs;
	}



	public String getRaterndrule() {
		return raterndrule;
	}



	public void setRaterndrule(String raterndrule) {
		this.raterndrule = raterndrule;
	}



	public String getRatetol() {
		return ratetol;
	}



	public void setRatetol(String ratetol) {
		this.ratetol = ratetol;
	}



	public BigDecimal getRedempamt() {
		return redempamt;
	}



	public void setRedempamt(BigDecimal redempamt) {
		this.redempamt = redempamt;
	}



	public String getRepocollpct() {
		return repocollpct;
	}



	public void setRepocollpct(String repocollpct) {
		this.repocollpct = repocollpct;
	}



	public String getSeries() {
		return series;
	}



	public void setSeries(String series) {
		this.series = series;
	}



	public String getSettdays() {
		return settdays;
	}



	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}



	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}



	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}



	public String getStarttime() {
		return starttime;
	}



	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}



	public String getSymbolid() {
		return symbolid;
	}



	public void setSymbolid(String symbolid) {
		this.symbolid = symbolid;
	}



	public String getTaxind() {
		return taxind;
	}



	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}



	public String getTenor() {
		return tenor;
	}



	public void setTenor(String tenor) {
		this.tenor = tenor;
	}



	public String getTradstat() {
		return tradstat;
	}



	public void setTradstat(String tradstat) {
		this.tradstat = tradstat;
	}



	public String getSecunit() {
		return secunit;
	}



	public void setSecunit(String secunit) {
		this.secunit = secunit;
	}



	public Date getVdate() {
		return vdate;
	}



	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}



	public String getExchcno() {
		return exchcno;
	}



	public void setExchcno(String exchcno) {
		this.exchcno = exchcno;
	}



	public String getWhaccno() {
		return whaccno;
	}



	public void setWhaccno(String whaccno) {
		this.whaccno = whaccno;
	}



	public String getSettccy() {
		return settccy;
	}



	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}



	public String getPricetol() {
		return pricetol;
	}



	public void setPricetol(String pricetol) {
		this.pricetol = pricetol;
	}



	public Date getCalltopar() {
		return calltopar;
	}



	public void setCalltopar(Date calltopar) {
		this.calltopar = calltopar;
	}



	public Date getPuttopar() {
		return puttopar;
	}



	public void setPuttopar(Date puttopar) {
		this.puttopar = puttopar;
	}



	public String getMindenom() {
		return mindenom;
	}



	public void setMindenom(String mindenom) {
		this.mindenom = mindenom;
	}



	public String getMinincrement() {
		return minincrement;
	}



	public void setMinincrement(String minincrement) {
		this.minincrement = minincrement;
	}



	public String getLinksecid() {
		return linksecid;
	}



	public void setLinksecid(String linksecid) {
		this.linksecid = linksecid;
	}



	public String getIntccy() {
		return intccy;
	}



	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}



	public BigDecimal getIntexchrate8() {
		return intexchrate8;
	}



	public void setIntexchrate8(BigDecimal intexchrate8) {
		this.intexchrate8 = intexchrate8;
	}



	public String getIntexchterms() {
		return intexchterms;
	}



	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}



	public String getSecmsic() {
		return secmsic;
	}



	public void setSecmsic(String secmsic) {
		this.secmsic = secmsic;
	}



	public String getRepoflag() {
		return repoflag;
	}



	public void setRepoflag(String repoflag) {
		this.repoflag = repoflag;
	}



	public String getExdivdays() {
		return exdivdays;
	}



	public void setExdivdays(String exdivdays) {
		this.exdivdays = exdivdays;
	}



	public String getDcpriceind() {
		return dcpriceind;
	}



	public void setDcpriceind(String dcpriceind) {
		this.dcpriceind = dcpriceind;
	}



	public String getFincent() {
		return fincent;
	}



	public void setFincent(String fincent) {
		this.fincent = fincent;
	}



	public String getExercisedays() {
		return exercisedays;
	}



	public void setExercisedays(String exercisedays) {
		this.exercisedays = exercisedays;
	}



	public String getIntenddaterule() {
		return intenddaterule;
	}



	public void setIntenddaterule(String intenddaterule) {
		this.intenddaterule = intenddaterule;
	}



	public String getShortcoupind() {
		return shortcoupind;
	}



	public void setShortcoupind(String shortcoupind) {
		this.shortcoupind = shortcoupind;
	}



	public String getEmtaind() {
		return emtaind;
	}



	public void setEmtaind(String emtaind) {
		this.emtaind = emtaind;
	}



	public String getPublishdays() {
		return publishdays;
	}



	public void setPublishdays(String publishdays) {
		this.publishdays = publishdays;
	}



	public String getAnnpers() {
		return annpers;
	}



	public void setAnnpers(String annpers) {
		this.annpers = annpers;
	}



	public String getFixpers() {
		return fixpers;
	}



	public void setFixpers(String fixpers) {
		this.fixpers = fixpers;
	}



	public BigDecimal getFixrate8() {
		return fixrate8;
	}



	public void setFixrate8(BigDecimal fixrate8) {
		this.fixrate8 = fixrate8;
	}



	public String getCashdecs() {
		return cashdecs;
	}



	public void setCashdecs(String cashdecs) {
		this.cashdecs = cashdecs;
	}



	public String getAidecs() {
		return aidecs;
	}



	public void setAidecs(String aidecs) {
		this.aidecs = aidecs;
	}



	public String getPmtfreq() {
		return pmtfreq;
	}



	public void setPmtfreq(String pmtfreq) {
		this.pmtfreq = pmtfreq;
	}



	public String getPrinpmtfreq() {
		return prinpmtfreq;
	}



	public void setPrinpmtfreq(String prinpmtfreq) {
		this.prinpmtfreq = prinpmtfreq;
	}



	public String getFixfreq() {
		return fixfreq;
	}



	public void setFixfreq(String fixfreq) {
		this.fixfreq = fixfreq;
	}



	public BigDecimal getMaxmfix8() {
		return maxmfix8;
	}



	public void setMaxmfix8(BigDecimal maxmfix8) {
		this.maxmfix8 = maxmfix8;
	}



	public BigDecimal getMaxmlife8() {
		return maxmlife8;
	}



	public void setMaxmlife8(BigDecimal maxmlife8) {
		this.maxmlife8 = maxmlife8;
	}



	public String getIntrndrule() {
		return intrndrule;
	}



	public void setIntrndrule(String intrndrule) {
		this.intrndrule = intrndrule;
	}



	public String getMinincrmt() {
		return minincrmt;
	}



	public void setMinincrmt(String minincrmt) {
		this.minincrmt = minincrmt;
	}



	public String getNoamorts() {
		return noamorts;
	}



	public void setNoamorts(String noamorts) {
		this.noamorts = noamorts;
	}



	public BigDecimal getLastamort8() {
		return lastamort8;
	}



	public void setLastamort8(BigDecimal lastamort8) {
		this.lastamort8 = lastamort8;
	}



	public String getPitype() {
		return pitype;
	}



	public void setPitype(String pitype) {
		this.pitype = pitype;
	}



	public Long getUpdatecounter() {
		return updatecounter;
	}



	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}



	public String getPhysdelind() {
		return physdelind;
	}



	public void setPhysdelind(String physdelind) {
		this.physdelind = physdelind;
	}



	public String getDvpind() {
		return dvpind;
	}



	public void setDvpind(String dvpind) {
		this.dvpind = dvpind;
	}



	public String getRecorddateind() {
		return recorddateind;
	}



	public void setRecorddateind(String recorddateind) {
		this.recorddateind = recorddateind;
	}



	public BigDecimal getPerratefloor12() {
		return perratefloor12;
	}



	public void setPerratefloor12(BigDecimal perratefloor12) {
		this.perratefloor12 = perratefloor12;
	}



	public BigDecimal getLiferatefloor12() {
		return liferatefloor12;
	}



	public void setLiferatefloor12(BigDecimal liferatefloor12) {
		this.liferatefloor12 = liferatefloor12;
	}



	public String getDelaydays() {
		return delaydays;
	}



	public void setDelaydays(String delaydays) {
		this.delaydays = delaydays;
	}



	public String getPmtday() {
		return pmtday;
	}



	public void setPmtday(String pmtday) {
		this.pmtday = pmtday;
	}



	public String getSettleclass() {
		return settleclass;
	}



	public void setSettleclass(String settleclass) {
		this.settleclass = settleclass;
	}



	public BigDecimal getPmtcap12() {
		return pmtcap12;
	}



	public void setPmtcap12(BigDecimal pmtcap12) {
		this.pmtcap12 = pmtcap12;
	}



	public String getPoolno() {
		return poolno;
	}



	public void setPoolno(String poolno) {
		this.poolno = poolno;
	}



	public String getCashflowind() {
		return cashflowind;
	}



	public void setCashflowind(String cashflowind) {
		this.cashflowind = cashflowind;
	}



	public String getPrepmtmodel() {
		return prepmtmodel;
	}



	public void setPrepmtmodel(String prepmtmodel) {
		this.prepmtmodel = prepmtmodel;
	}



	public BigDecimal getWithtaxrate12() {
		return withtaxrate12;
	}



	public void setWithtaxrate12(BigDecimal withtaxrate12) {
		this.withtaxrate12 = withtaxrate12;
	}



	public String getFactsecind() {
		return factsecind;
	}



	public void setFactsecind(String factsecind) {
		this.factsecind = factsecind;
	}



	public BigDecimal getAmt1() {
		return amt1;
	}



	public void setAmt1(BigDecimal amt1) {
		this.amt1 = amt1;
	}



	public BigDecimal getAmt2() {
		return amt2;
	}



	public void setAmt2(BigDecimal amt2) {
		this.amt2 = amt2;
	}



	public BigDecimal getRate112() {
		return rate112;
	}



	public void setRate112(BigDecimal rate112) {
		this.rate112 = rate112;
	}



	public BigDecimal getRate212() {
		return rate212;
	}



	public void setRate212(BigDecimal rate212) {
		this.rate212 = rate212;
	}



	public Date getDate1() {
		return date1;
	}



	public void setDate1(Date date1) {
		this.date1 = date1;
	}



	public Date getDate2() {
		return date2;
	}



	public void setDate2(Date date2) {
		this.date2 = date2;
	}



	public String getIndexratecode() {
		return indexratecode;
	}



	public void setIndexratecode(String indexratecode) {
		this.indexratecode = indexratecode;
	}



	public String getIlind() {
		return ilind;
	}



	public void setIlind(String ilind) {
		this.ilind = ilind;
	}



	public String getDeflationind() {
		return deflationind;
	}



	public void setDeflationind(String deflationind) {
		this.deflationind = deflationind;
	}



	public String getIldecs() {
		return ildecs;
	}



	public void setIldecs(String ildecs) {
		this.ildecs = ildecs;
	}



	public String getNgpriceind() {
		return ngpriceind;
	}



	public void setNgpriceind(String ngpriceind) {
		this.ngpriceind = ngpriceind;
	}



	public String getCpidaycycle() {
		return cpidaycycle;
	}



	public void setCpidaycycle(String cpidaycycle) {
		this.cpidaycycle = cpidaycycle;
	}



	public BigDecimal getSchedid() {
		return schedid;
	}



	public void setSchedid(BigDecimal schedid) {
		this.schedid = schedid;
	}



	public BigDecimal getProdid() {
		return prodid;
	}



	public void setProdid(BigDecimal prodid) {
		this.prodid = prodid;
	}



	public String getStructid() {
		return structid;
	}



	public void setStructid(String structid) {
		this.structid = structid;
	}



	public String getMktvalsource() {
		return mktvalsource;
	}



	public void setMktvalsource(String mktvalsource) {
		this.mktvalsource = mktvalsource;
	}



	public BigDecimal getFormulaid() {
		return formulaid;
	}



	public void setFormulaid(BigDecimal formulaid) {
		this.formulaid = formulaid;
	}



	public String getIntsettccy() {
		return intsettccy;
	}



	public void setIntsettccy(String intsettccy) {
		this.intsettccy = intsettccy;
	}



	public String getRatrevfirstlast() {
		return ratrevfirstlast;
	}



	public void setRatrevfirstlast(String ratrevfirstlast) {
		this.ratrevfirstlast = ratrevfirstlast;
	}



	public String getRatefixdays() {
		return ratefixdays;
	}



	public void setRatefixdays(String ratefixdays) {
		this.ratefixdays = ratefixdays;
	}



	public String getDtcind() {
		return dtcind;
	}



	public void setDtcind(String dtcind) {
		this.dtcind = dtcind;
	}



	public String getFedind() {
		return fedind;
	}



	public void setFedind(String fedind) {
		this.fedind = fedind;
	}



	public String getEuroclearind() {
		return euroclearind;
	}



	public void setEuroclearind(String euroclearind) {
		this.euroclearind = euroclearind;
	}



	public String getClearstreamind() {
		return clearstreamind;
	}



	public void setClearstreamind(String clearstreamind) {
		this.clearstreamind = clearstreamind;
	}



	public String getLocalmarket() {
		return localmarket;
	}



	public void setLocalmarket(String localmarket) {
		this.localmarket = localmarket;
	}



	public String getRepotrackingind() {
		return repotrackingind;
	}



	public void setRepotrackingind(String repotrackingind) {
		this.repotrackingind = repotrackingind;
	}



	public String getTemplateind() {
		return templateind;
	}



	public void setTemplateind(String templateind) {
		this.templateind = templateind;
	}



	public String getEndofmonthind() {
		return endofmonthind;
	}



	public void setEndofmonthind(String endofmonthind) {
		this.endofmonthind = endofmonthind;
	}



	public String getRegenind() {
		return regenind;
	}



	public void setRegenind(String regenind) {
		this.regenind = regenind;
	}



	public Date getRegendate() {
		return regendate;
	}



	public void setRegendate(Date regendate) {
		this.regendate = regendate;
	}



	public String getAllownegativeint() {
		return allownegativeint;
	}



	public void setAllownegativeint(String allownegativeint) {
		this.allownegativeint = allownegativeint;
	}



	private static final long serialVersionUID = 1L;
}