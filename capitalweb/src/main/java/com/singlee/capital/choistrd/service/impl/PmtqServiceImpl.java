package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.PmtqMappr;
import com.singlee.capital.choistrd.model.SL_PMTQ;
import com.singlee.capital.choistrd.service.PmtqService;
@Service
public class PmtqServiceImpl implements PmtqService {
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	@Autowired
	private PmtqMappr pmtqMaper;
	@Override
	public List<SL_PMTQ> queryAllPmtqFromOpicsByCondition() throws Exception {
		// TODO Auto-generated method stub
		return pmtqMaper.queryAllPmtqFromOpicsByCondition();
	}

	@Override
	public int updatePmtqStatus(SL_PMTQ sl_pmtq) throws Exception {
		// TODO Auto-generated method stub
		return pmtqMaper.updatePmtqStatus(sl_pmtq);
	}

	@Override
	public SL_PMTQ queryPmtqByDealnoSeqCondition(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return pmtqMaper.queryPmtqByDealnoSeqCondition(map);
	}

}
