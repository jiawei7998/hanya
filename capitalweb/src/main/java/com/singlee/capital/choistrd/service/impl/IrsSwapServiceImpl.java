package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.SwdhMapper;
import com.singlee.capital.choistrd.mapper.SwdsMapper;
import com.singlee.capital.choistrd.mapper.SwdtMapper;
import com.singlee.capital.choistrd.model.SWDH;
import com.singlee.capital.choistrd.model.SWDS;
import com.singlee.capital.choistrd.model.SWDT;
import com.singlee.capital.choistrd.service.IrsSwapService;

@Service
public class IrsSwapServiceImpl implements IrsSwapService{
	
	@Autowired
	private SwdhMapper swdhMapper;
	
	@Autowired
	private SwdtMapper swdtMapper;
	
	@Autowired
	private SwdsMapper swdsMapper;
	
	@Override
	public List<SWDH> queryAllSWDHListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdhMapper.queryAllSWDHListIntoWebtFieldSet(map);
	}

	@Override
	public List<SWDT> queryAllSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdtMapper.queryAllSWDTListIntoWebtFieldSet(map);
	}

	@Override
	public List<SWDS> queryAllSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdsMapper.queryAllSWDSListIntoWebtFieldSet(map);
	}

	@Override
	public List<SWDH> queryAllRevSWDHListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdhMapper.queryAllRevSWDHListIntoWebtFieldSet(map);
	}

	@Override
	public List<SWDT> queryAllRevSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdtMapper.queryAllRevSWDTListIntoWebtFieldSet(map);
	}

	@Override
	public List<SWDS> queryAllRevSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return swdsMapper.queryAllRevSWDSListIntoWebtFieldSet(map);
	}

}
