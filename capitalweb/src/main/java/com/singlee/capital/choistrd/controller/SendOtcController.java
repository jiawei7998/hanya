package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.OTCP;
import com.singlee.capital.choistrd.model.OTDT;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.choistrd.service.OtcService;
import com.singlee.capital.choistrd.service.impl.OtcInputTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value="/SendOtcController")
public class SendOtcController extends CommonController{
	public static Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private OtcService otcService;
	@Autowired
	private OtcInputTmaxCommand otcInputTmaxCommand;
	
	@Autowired
	private FxTradeService fxTradeService;
	ChiosLogin chiosLogin = new ChiosLogin();
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	 
	@ResponseBody
	@RequestMapping(value="/sendOtcChois")
	public RetMsg<Serializable> sendOtcChois(@RequestBody Map<String, Object> params) throws Exception{
		int ret=0;
		logger.info("*********SEND OTC INITAIL MSG*****************");
		//返回的柜员sessio
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
		String postdate = fxTradeService.queryOpicsPostDate();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("postdate",postdate);
		//map.put("postdate", "2022/09/21");
		List<OTDT> webtList31 = new ArrayList<OTDT>();
		webtList31 = otcService.queryVerifyOTDTListIntoWebtFieldSet(map);
		for(OTDT otdt:webtList31) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otdt,"OTDT",sess,"N");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<OTDT> webtList3 = new ArrayList<OTDT>();
		webtList3 = otcService.queryAllOTDTListIntoWebtFieldSet(map);
		for(OTDT otdt:webtList3) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otdt,"OTDT",sess,"N");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<OTCP> webtList4 = new ArrayList<OTCP>();
		webtList4 = otcService.queryAllOTCPListIntoWebtFieldSet(map);
		for(OTCP otcp:webtList4) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otcp,"OTCP",sess,"N");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		
		logger.info("**************************END OTC INITAIL MSG***************************");
		logger.info("**************************SEND OTC REVERSE MSG***************************");
		try
		{
		List<OTDT> webtList31Reverse = new ArrayList<OTDT>();
		webtList31Reverse = otcService.queryVerifyRevOTDTListIntoWebtFieldSet(map);
		for(OTDT otdt:webtList31Reverse) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otdt,"OTDT",sess,"Y");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<OTDT> webtList3Reverse = new ArrayList<OTDT>();
		webtList3Reverse = otcService.queryAllOTDRevTListIntoWebtFieldSet(map);
		for(OTDT otdt:webtList3Reverse) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otdt,"OTDT",sess,"Y");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<OTCP> webtList4Reverse = new ArrayList<OTCP>();
		webtList4Reverse = otcService.queryAllRevOTCPListIntoWebtFieldSet(map);
		for(OTCP otcp:webtList4Reverse) {
			try{
				
				ret=otcInputTmaxCommand.executeHostVisitAction(otcp,"OTCP",sess,"Y");
			}catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryOtcListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END OTC REVERSE MSG***************************");
		return RetMsgHelper.ok(ret);
	}
}
