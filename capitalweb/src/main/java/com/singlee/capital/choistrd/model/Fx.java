package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Fx implements Serializable {

	private static final long serialVersionUID = 1L;

	private String server;
	
	private String inputCode;
	
	private String subjectCode;
	
	private String tradCode;
	
	private String traderCode;
	
	private String prodCode;
	
	private String prodCodeName;
	
	private String custCode;
	
	private String custCodeName;
	
	private String dealDate;
	
	private String vdate;
	
	private String settType;
	
	private String ccy;
	
	private BigDecimal ccyAmt;
	
	private BigDecimal ccyRate_8; 
	
	private String ctrCcy;
	
	private BigDecimal ctrAmt;
	
	private String dealno;
	
	private String seq;
	
	private String dealerId;
	
	private String baseCcy;
	
	private String cptyCcy;
	
	private String custType;
	
	private String dealGb;
	
	private String dealMatchType;
	
	private String outOfRiskGb;
	
	private String inOutGb;
	
	private String farVdate;
	
	private String farType;
	
	private String farCcy;
	
	private BigDecimal farAmt;
	
	private BigDecimal farRate;
	
	private String farCtrCcy;
	
	private BigDecimal farCtrAmt;
	
	private String outRiskGb;//OUTRISKGB
	
	private String nearRefNo;
	
	private String farRefNo;
	
	private String DEALTYPE;
	
	/**
	 * ����������Ϣ
	 */
	private String NEAR_CONFIRM_YN;
	private String NEAR_CONFIRM_IL;
	private String NEAR_PO_YN;
	private String NEAR_PO_IL;
	private String NEAR_OUR_RCV_DEPO_CD;
	private String NEAR_OUR_RCV_DEPO_NM;
	private String NEAR_OUR_PAY_DEPO_CD;
	private String NEAR_OUR_PAY_DEPO_NM;
	private String NEAR_THR_RCV_DEPO_BIC;
	private String NEAR_THR_RCV_DEPO_NM;
	private String CNAPS_YN;
	/**
	 * Զ��������Ϣ
	 */
	private String FAR_CONFIRM_YN;
	private String FAR_CONFIRM_IL;
	private String FAR_PO_YN;
	private String FAR_PO_IL;
	private String FAR_OUR_RCV_DEPO_CD;
	private String FAR_OUR_RCV_DEPO_NM;
	private String FAR_OUR_PAY_DEPO_CD;
	private String FAR_OUR_PAY_DEPO_NM;
	private String FAR_THR_RCV_DEPO_BIC;
	private String FAR_THR_RCV_DEPO_NM;
	private String FAR_CNAPS_YN;
	/**
	 * �����Ϣ
	 */
	private String CNAPS_RCV_ACCT_NO;
	private String CNAPS_CD;
	private String CNAPS_NM;
	
	private String FAR_CNAPS_RCV_ACCT_NO;
	private String FAR_CNAPS_CD;
	private String FAR_CNAPS_NM;
	
	/**
	 * ��ע(�������ֱ�ʾ�������)
	 */
	private String REMARK;
	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String remark) {
		REMARK = remark;
	}

	/**
	 * @return the nEAR_CONFIRM_YN
	 */
	public String getNEAR_CONFIRM_YN() {
		return NEAR_CONFIRM_YN;
	}

	/**
	 * @param near_confirm_yn the nEAR_CONFIRM_YN to set
	 */
	public void setNEAR_CONFIRM_YN(String near_confirm_yn) {
		NEAR_CONFIRM_YN = near_confirm_yn;
	}

	/**
	 * @return the nEAR_CONFIRM_IL
	 */
	public String getNEAR_CONFIRM_IL() {
		return NEAR_CONFIRM_IL;
	}

	/**
	 * @param near_confirm_il the nEAR_CONFIRM_IL to set
	 */
	public void setNEAR_CONFIRM_IL(String near_confirm_il) {
		NEAR_CONFIRM_IL = near_confirm_il;
	}

	/**
	 * @return the nEAR_PO_YN
	 */
	public String getNEAR_PO_YN() {
		return NEAR_PO_YN;
	}

	/**
	 * @param near_po_yn the nEAR_PO_YN to set
	 */
	public void setNEAR_PO_YN(String near_po_yn) {
		NEAR_PO_YN = near_po_yn;
	}

	/**
	 * @return the nEAR_PO_IL
	 */
	public String getNEAR_PO_IL() {
		return NEAR_PO_IL;
	}

	/**
	 * @param near_po_il the nEAR_PO_IL to set
	 */
	public void setNEAR_PO_IL(String near_po_il) {
		NEAR_PO_IL = near_po_il;
	}

	/**
	 * @return the nEAR_OUR_RCV_DEPO_CD
	 */
	public String getNEAR_OUR_RCV_DEPO_CD() {
		return NEAR_OUR_RCV_DEPO_CD;
	}

	/**
	 * @param near_our_rcv_depo_cd the nEAR_OUR_RCV_DEPO_CD to set
	 */
	public void setNEAR_OUR_RCV_DEPO_CD(String near_our_rcv_depo_cd) {
		NEAR_OUR_RCV_DEPO_CD = near_our_rcv_depo_cd;
	}

	/**
	 * @return the nEAR_OUR_RCV_DEPO_NM
	 */
	public String getNEAR_OUR_RCV_DEPO_NM() {
		return NEAR_OUR_RCV_DEPO_NM;
	}

	/**
	 * @param near_our_rcv_depo_nm the nEAR_OUR_RCV_DEPO_NM to set
	 */
	public void setNEAR_OUR_RCV_DEPO_NM(String near_our_rcv_depo_nm) {
		NEAR_OUR_RCV_DEPO_NM = near_our_rcv_depo_nm;
	}

	/**
	 * @return the nEAR_OUR_PAY_DEPO_CD
	 */
	public String getNEAR_OUR_PAY_DEPO_CD() {
		return NEAR_OUR_PAY_DEPO_CD;
	}

	/**
	 * @param near_our_pay_depo_cd the nEAR_OUR_PAY_DEPO_CD to set
	 */
	public void setNEAR_OUR_PAY_DEPO_CD(String near_our_pay_depo_cd) {
		NEAR_OUR_PAY_DEPO_CD = near_our_pay_depo_cd;
	}

	/**
	 * @return the nEAR_OUR_PAY_DEPO_NM
	 */
	public String getNEAR_OUR_PAY_DEPO_NM() {
		return NEAR_OUR_PAY_DEPO_NM;
	}

	/**
	 * @param near_our_pay_depo_nm the nEAR_OUR_PAY_DEPO_NM to set
	 */
	public void setNEAR_OUR_PAY_DEPO_NM(String near_our_pay_depo_nm) {
		NEAR_OUR_PAY_DEPO_NM = near_our_pay_depo_nm;
	}

	/**
	 * @return the nEAR_THR_RCV_DEPO_BIC
	 */
	public String getNEAR_THR_RCV_DEPO_BIC() {
		return NEAR_THR_RCV_DEPO_BIC;
	}

	/**
	 * @param near_thr_rcv_depo_bic the nEAR_THR_RCV_DEPO_BIC to set
	 */
	public void setNEAR_THR_RCV_DEPO_BIC(String near_thr_rcv_depo_bic) {
		NEAR_THR_RCV_DEPO_BIC = near_thr_rcv_depo_bic;
	}

	/**
	 * @return the nEAR_THR_RCV_DEPO_NM
	 */
	public String getNEAR_THR_RCV_DEPO_NM() {
		return NEAR_THR_RCV_DEPO_NM;
	}

	/**
	 * @param near_thr_rcv_depo_nm the nEAR_THR_RCV_DEPO_NM to set
	 */
	public void setNEAR_THR_RCV_DEPO_NM(String near_thr_rcv_depo_nm) {
		NEAR_THR_RCV_DEPO_NM = near_thr_rcv_depo_nm;
	}

	/**
	 * @return the cNAPS_YN
	 */
	public String getCNAPS_YN() {
		return CNAPS_YN;
	}

	/**
	 * @param cnaps_yn the cNAPS_YN to set
	 */
	public void setCNAPS_YN(String cnaps_yn) {
		CNAPS_YN = cnaps_yn;
	}

	/**
	 * @return the fAR_CONFIRM_YN
	 */
	public String getFAR_CONFIRM_YN() {
		return FAR_CONFIRM_YN;
	}

	/**
	 * @param far_confirm_yn the fAR_CONFIRM_YN to set
	 */
	public void setFAR_CONFIRM_YN(String far_confirm_yn) {
		FAR_CONFIRM_YN = far_confirm_yn;
	}

	/**
	 * @return the fAR_CONFIRM_IL
	 */
	public String getFAR_CONFIRM_IL() {
		return FAR_CONFIRM_IL;
	}

	/**
	 * @param far_confirm_il the fAR_CONFIRM_IL to set
	 */
	public void setFAR_CONFIRM_IL(String far_confirm_il) {
		FAR_CONFIRM_IL = far_confirm_il;
	}

	/**
	 * @return the fAR_PO_YN
	 */
	public String getFAR_PO_YN() {
		return FAR_PO_YN;
	}

	/**
	 * @param far_po_yn the fAR_PO_YN to set
	 */
	public void setFAR_PO_YN(String far_po_yn) {
		FAR_PO_YN = far_po_yn;
	}

	/**
	 * @return the fAR_PO_IL
	 */
	public String getFAR_PO_IL() {
		return FAR_PO_IL;
	}

	/**
	 * @param far_po_il the fAR_PO_IL to set
	 */
	public void setFAR_PO_IL(String far_po_il) {
		FAR_PO_IL = far_po_il;
	}

	/**
	 * @return the fAR_OUR_RCV_DEPO_CD
	 */
	public String getFAR_OUR_RCV_DEPO_CD() {
		return FAR_OUR_RCV_DEPO_CD;
	}

	/**
	 * @param far_our_rcv_depo_cd the fAR_OUR_RCV_DEPO_CD to set
	 */
	public void setFAR_OUR_RCV_DEPO_CD(String far_our_rcv_depo_cd) {
		FAR_OUR_RCV_DEPO_CD = far_our_rcv_depo_cd;
	}

	/**
	 * @return the fAR_OUR_RCV_DEPO_NM
	 */
	public String getFAR_OUR_RCV_DEPO_NM() {
		return FAR_OUR_RCV_DEPO_NM;
	}

	/**
	 * @param far_our_rcv_depo_nm the fAR_OUR_RCV_DEPO_NM to set
	 */
	public void setFAR_OUR_RCV_DEPO_NM(String far_our_rcv_depo_nm) {
		FAR_OUR_RCV_DEPO_NM = far_our_rcv_depo_nm;
	}

	/**
	 * @return the fAR_OUR_PAY_DEPO_CD
	 */
	public String getFAR_OUR_PAY_DEPO_CD() {
		return FAR_OUR_PAY_DEPO_CD;
	}

	/**
	 * @param far_our_pay_depo_cd the fAR_OUR_PAY_DEPO_CD to set
	 */
	public void setFAR_OUR_PAY_DEPO_CD(String far_our_pay_depo_cd) {
		FAR_OUR_PAY_DEPO_CD = far_our_pay_depo_cd;
	}

	/**
	 * @return the fAR_OUR_PAY_DEPO_NM
	 */
	public String getFAR_OUR_PAY_DEPO_NM() {
		return FAR_OUR_PAY_DEPO_NM;
	}

	/**
	 * @param far_our_pay_depo_nm the fAR_OUR_PAY_DEPO_NM to set
	 */
	public void setFAR_OUR_PAY_DEPO_NM(String far_our_pay_depo_nm) {
		FAR_OUR_PAY_DEPO_NM = far_our_pay_depo_nm;
	}

	/**
	 * @return the fAR_THR_RCV_DEPO_BIC
	 */
	public String getFAR_THR_RCV_DEPO_BIC() {
		return FAR_THR_RCV_DEPO_BIC;
	}

	/**
	 * @param far_thr_rcv_depo_bic the fAR_THR_RCV_DEPO_BIC to set
	 */
	public void setFAR_THR_RCV_DEPO_BIC(String far_thr_rcv_depo_bic) {
		FAR_THR_RCV_DEPO_BIC = far_thr_rcv_depo_bic;
	}

	/**
	 * @return the fAR_THR_RCV_DEPO_NM
	 */
	public String getFAR_THR_RCV_DEPO_NM() {
		return FAR_THR_RCV_DEPO_NM;
	}

	/**
	 * @param far_thr_rcv_depo_nm the fAR_THR_RCV_DEPO_NM to set
	 */
	public void setFAR_THR_RCV_DEPO_NM(String far_thr_rcv_depo_nm) {
		FAR_THR_RCV_DEPO_NM = far_thr_rcv_depo_nm;
	}

	/**
	 * @return the fAR_CNAPS_YN
	 */
	public String getFAR_CNAPS_YN() {
		return FAR_CNAPS_YN;
	}

	/**
	 * @param far_cnaps_yn the fAR_CNAPS_YN to set
	 */
	public void setFAR_CNAPS_YN(String far_cnaps_yn) {
		FAR_CNAPS_YN = far_cnaps_yn;
	}

	/**
	 * @return the cNAPS_RCV_ACCT_NO
	 */
	public String getCNAPS_RCV_ACCT_NO() {
		return CNAPS_RCV_ACCT_NO;
	}

	/**
	 * @param cnaps_rcv_acct_no the cNAPS_RCV_ACCT_NO to set
	 */
	public void setCNAPS_RCV_ACCT_NO(String cnaps_rcv_acct_no) {
		CNAPS_RCV_ACCT_NO = cnaps_rcv_acct_no;
	}

	/**
	 * @return the cNAPS_CD
	 */
	public String getCNAPS_CD() {
		return CNAPS_CD;
	}

	/**
	 * @param cnaps_cd the cNAPS_CD to set
	 */
	public void setCNAPS_CD(String cnaps_cd) {
		CNAPS_CD = cnaps_cd;
	}

	/**
	 * @return the cNAPS_NM
	 */
	public String getCNAPS_NM() {
		return CNAPS_NM;
	}

	/**
	 * @param cnaps_nm the cNAPS_NM to set
	 */
	public void setCNAPS_NM(String cnaps_nm) {
		CNAPS_NM = cnaps_nm;
	}

	/**
	 * @return the nearRefNo
	 */
	public String getNearRefNo() {
		return nearRefNo;
	}

	/**
	 * @param nearRefNo the nearRefNo to set
	 */
	public void setNearRefNo(String nearRefNo) {
		this.nearRefNo = nearRefNo;
	}

	/**
	 * @return the farRefNo
	 */
	public String getFarRefNo() {
		return farRefNo;
	}

	/**
	 * @param farRefNo the farRefNo to set
	 */
	public void setFarRefNo(String farRefNo) {
		this.farRefNo = farRefNo;
	}

	/**
	 * @return the custType
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * @param custType the custType to set
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}

	/**
	 * @return the dealGb
	 */
	public String getDealGb() {
		return dealGb;
	}

	/**
	 * @param dealGb the dealGb to set
	 */
	public void setDealGb(String dealGb) {
		this.dealGb = dealGb;
	}

	/**
	 * @return the dealMatchType
	 */
	public String getDealMatchType() {
		return dealMatchType;
	}

	/**
	 * @param dealMatchType the dealMatchType to set
	 */
	public void setDealMatchType(String dealMatchType) {
		this.dealMatchType = dealMatchType;
	}

	/**
	 * @return the outOfRiskGb
	 */
	public String getOutOfRiskGb() {
		return outOfRiskGb;
	}

	/**
	 * @param outOfRiskGb the outOfRiskGb to set
	 */
	public void setOutOfRiskGb(String outOfRiskGb) {
		this.outOfRiskGb = outOfRiskGb;
	}

	/**
	 * @return the inOutGb
	 */
	public String getInOutGb() {
		return inOutGb;
	}

	/**
	 * @param inOutGb the inOutGb to set
	 */
	public void setInOutGb(String inOutGb) {
		this.inOutGb = inOutGb;
	}

	/**
	 * @return the baseCcy
	 */
	public String getBaseCcy() {
		return baseCcy;
	}

	/**
	 * @param baseCcy the baseCcy to set
	 */
	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}

	/**
	 * @return the cptyCcy
	 */
	public String getCptyCcy() {
		return cptyCcy;
	}

	/**
	 * @param cptyCcy the cptyCcy to set
	 */
	public void setCptyCcy(String cptyCcy) {
		this.cptyCcy = cptyCcy;
	}

	/**
	 * @return the dealerId
	 */
	public String getDealerId() {
		return dealerId;
	}

	/**
	 * @param dealerId the dealerId to set
	 */
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public Fx(){}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @return the inputCode
	 */
	public String getInputCode() {
		return inputCode;
	}

	/**
	 * @param inputCode the inputCode to set
	 */
	public void setInputCode(String inputCode) {
		this.inputCode = inputCode;
	}

	/**
	 * @return the subjectCode
	 */
	public String getSubjectCode() {
		return subjectCode;
	}

	/**
	 * @param subjectCode the subjectCode to set
	 */
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	/**
	 * @return the tradCode
	 */
	public String getTradCode() {
		return tradCode;
	}

	/**
	 * @param tradCode the tradCode to set
	 */
	public void setTradCode(String tradCode) {
		this.tradCode = tradCode;
	}

	/**
	 * @return the traderCode
	 */
	public String getTraderCode() {
		return traderCode;
	}

	/**
	 * @param traderCode the traderCode to set
	 */
	public void setTraderCode(String traderCode) {
		this.traderCode = traderCode;
	}

	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}

	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	/**
	 * @return the custCode
	 */
	public String getCustCode() {
		return custCode;
	}

	/**
	 * @param custCode the custCode to set
	 */
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	/**
	 * @return the dealDate
	 */
	public String getDealDate() {
		return dealDate;
	}

	/**
	 * @param dealDate the dealDate to set
	 */
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	/**
	 * @return the vdate
	 */
	public String getVdate() {
		return vdate;
	}

	/**
	 * @param vdate the vdate to set
	 */
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	/**
	 * @return the settType
	 */
	public String getSettType() {
		return settType;
	}

	/**
	 * @param settType the settType to set
	 */
	public void setSettType(String settType) {
		this.settType = settType;
	}

	/**
	 * @return the ccy
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * @param ccy the ccy to set
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	/**
	 * @return the ccyAmt
	 */
	public BigDecimal getCcyAmt() {
		return ccyAmt;
	}

	/**
	 * @param ccyAmt the ccyAmt to set
	 */
	public void setCcyAmt(BigDecimal ccyAmt) {
		this.ccyAmt = ccyAmt;
	}

	/**
	 * @return the ccyRate_8
	 */
	public BigDecimal getCcyRate_8() {
		return ccyRate_8;
	}

	/**
	 * @param ccyRate_8 the ccyRate_8 to set
	 */
	public void setCcyRate_8(BigDecimal ccyRate_8) {
		this.ccyRate_8 = ccyRate_8;
	}

	/**
	 * @return the ctrCcy
	 */
	public String getCtrCcy() {
		return ctrCcy;
	}

	/**
	 * @param ctrCcy the ctrCcy to set
	 */
	public void setCtrCcy(String ctrCcy) {
		this.ctrCcy = ctrCcy;
	}

	/**
	 * @return the ctrAmt
	 */
	public BigDecimal getCtrAmt() {
		return ctrAmt;
	}

	/**
	 * @param ctrAmt the ctrAmt to set
	 */
	public void setCtrAmt(BigDecimal ctrAmt) {
		this.ctrAmt = ctrAmt;
	}

	/**
	 * @return the dealno
	 */
	public String getDealno() {
		return dealno;
	}

	/**
	 * @param dealno the dealno to set
	 */
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * @return the prodCodeName
	 */
	public String getProdCodeName() {
		return prodCodeName;
	}

	/**
	 * @param prodCodeName the prodCodeName to set
	 */
	public void setProdCodeName(String prodCodeName) {
		this.prodCodeName = prodCodeName;
	}

	/**
	 * @return the custCodeName
	 */
	public String getCustCodeName() {
		return custCodeName;
	}

	/**
	 * @param custCodeName the custCodeName to set
	 */
	public void setCustCodeName(String custCodeName) {
		this.custCodeName = custCodeName;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	/**
	 * @return the farVdate
	 */
	public String getFarVdate() {
		return farVdate;
	}

	/**
	 * @param farVdate the farVdate to set
	 */
	public void setFarVdate(String farVdate) {
		this.farVdate = farVdate;
	}

	/**
	 * @return the farType
	 */
	public String getFarType() {
		return farType;
	}

	/**
	 * @param farType the farType to set
	 */
	public void setFarType(String farType) {
		this.farType = farType;
	}

	/**
	 * @return the farCcy
	 */
	public String getFarCcy() {
		return farCcy;
	}

	/**
	 * @param farCcy the farCcy to set
	 */
	public void setFarCcy(String farCcy) {
		this.farCcy = farCcy;
	}

	/**
	 * @return the farAmt
	 */
	public BigDecimal getFarAmt() {
		return farAmt;
	}

	/**
	 * @param farAmt the farAmt to set
	 */
	public void setFarAmt(BigDecimal farAmt) {
		this.farAmt = farAmt;
	}

	/**
	 * @return the farRate
	 */
	public BigDecimal getFarRate() {
		return farRate;
	}

	/**
	 * @param farRate the farRate to set
	 */
	public void setFarRate(BigDecimal farRate) {
		this.farRate = farRate;
	}

	/**
	 * @return the farCtrCcy
	 */
	public String getFarCtrCcy() {
		return farCtrCcy;
	}

	/**
	 * @param farCtrCcy the farCtrCcy to set
	 */
	public void setFarCtrCcy(String farCtrCcy) {
		this.farCtrCcy = farCtrCcy;
	}

	/**
	 * @return the farCtrAmt
	 */
	public BigDecimal getFarCtrAmt() {
		return farCtrAmt;
	}

	/**
	 * @param farCtrAmt the farCtrAmt to set
	 */
	public void setFarCtrAmt(BigDecimal farCtrAmt) {
		this.farCtrAmt = farCtrAmt;
	}

	/**
	 * @return the outRiskGb
	 */
	public String getOutRiskGb() {
		return outRiskGb;
	}

	/**
	 * @param outRiskGb the outRiskGb to set
	 */
	public void setOutRiskGb(String outRiskGb) {
		this.outRiskGb = outRiskGb;
	}

	/**
	 * @return the fAR_CNAPS_RCV_ACCT_NO
	 */
	public String getFAR_CNAPS_RCV_ACCT_NO() {
		return FAR_CNAPS_RCV_ACCT_NO;
	}

	/**
	 * @param far_cnaps_rcv_acct_no the fAR_CNAPS_RCV_ACCT_NO to set
	 */
	public void setFAR_CNAPS_RCV_ACCT_NO(String far_cnaps_rcv_acct_no) {
		FAR_CNAPS_RCV_ACCT_NO = far_cnaps_rcv_acct_no;
	}

	/**
	 * @return the fAR_CNAPS_CD
	 */
	public String getFAR_CNAPS_CD() {
		return FAR_CNAPS_CD;
	}

	/**
	 * @param far_cnaps_cd the fAR_CNAPS_CD to set
	 */
	public void setFAR_CNAPS_CD(String far_cnaps_cd) {
		FAR_CNAPS_CD = far_cnaps_cd;
	}

	/**
	 * @return the fAR_CNAPS_NM
	 */
	public String getFAR_CNAPS_NM() {
		return FAR_CNAPS_NM;
	}

	/**
	 * @param far_cnaps_nm the fAR_CNAPS_NM to set
	 */
	public void setFAR_CNAPS_NM(String far_cnaps_nm) {
		FAR_CNAPS_NM = far_cnaps_nm;
	}

	public String getDEALTYPE() {
		return DEALTYPE;
	}

	public void setDEALTYPE(String dealtype) {
		DEALTYPE = dealtype;
	}
}
