package com.singlee.capital.choistrd.model;

import java.math.BigDecimal;

public class SlMktStatus {
	public String makeIl;
	public String rtType;
	public String ccy;
	public String mtyend;
	public String price;
	public String getMakeIl() {
		return makeIl;
	}
	public void setMakeIl(String makeIl) {
		this.makeIl = makeIl;
	}
	public String getRtType() {
		return rtType;
	}
	public void setRtType(String rtType) {
		this.rtType = rtType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getMtyend() {
		return mtyend;
	}
	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	
}
