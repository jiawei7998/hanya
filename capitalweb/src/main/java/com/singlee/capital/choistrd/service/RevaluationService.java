package com.singlee.capital.choistrd.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.RevalStatus;
import com.singlee.capital.choistrd.model.FXRevalStatus;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;

public interface RevaluationService {
	public List<RevaluationTmaxBean> queryAllRevaluationProc(String postDate) throws Exception;
	
	public List<RevaluationTmaxBean> queryAllShouldSendRevaluations(String postDate) throws Exception;
	
	
	public List<FXRevalStatus> queryRevaluationStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	
	public int insertRevaluationStatusByNew(FXRevalStatus revaluationStatus) throws Exception;
	
	public int updateRevaluationStatusByDealnoSeq(FXRevalStatus rRevaluationStatus) throws Exception;
	
	public int deleteRevaluationStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	public RevalStatus queryRevaStatus(Map<String,Object> map) throws Exception; 
}
