package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.FxTradeMapper;
import com.singlee.capital.choistrd.mapper.RevaluationMapper;
import com.singlee.capital.choistrd.mapper.FXRevalStatusMapper;
import com.singlee.capital.choistrd.model.RevalStatus;
import com.singlee.capital.choistrd.model.FXRevalStatus;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.capital.choistrd.service.RevaluationService;
@Service
public class RevaluationServiceImpl implements RevaluationService {
	@Autowired
	private RevaluationMapper revaluationMapper ;
	
	@Autowired
	private FXRevalStatusMapper revaluationStatusMapper ;
	
	@Override
	public List<RevaluationTmaxBean> queryAllRevaluationProc(String postDate) throws Exception {
		// TODO Auto-generated method stub
		return revaluationMapper.queryAllRevaluationProc(postDate);
	}

	@Override
	public List<RevaluationTmaxBean> queryAllShouldSendRevaluations(String postDate) throws Exception {
		// TODO Auto-generated method stub
		return revaluationMapper.queryAllShouldSendRevaluations(postDate);
	}

	@Override
	public  List<FXRevalStatus> queryRevaluationStatusByDealnoSeq(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return revaluationStatusMapper.queryRevaluationStatusByDealnoSeq(map);
	}

	@Override
	public int insertRevaluationStatusByNew(FXRevalStatus RevaluationStatus) throws Exception {
		// TODO Auto-generated method stub
		return revaluationStatusMapper.insertRevaluationStatusByNew(RevaluationStatus);
	}

	@Override
	public int updateRevaluationStatusByDealnoSeq(FXRevalStatus revaluationStatus) throws Exception {
		// TODO Auto-generated method stub
		return revaluationStatusMapper.updateRevaluationStatusByDealnoSeq(revaluationStatus);
	}

	@Override
	public int deleteRevaluationStatusByDealnoSeq(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return revaluationStatusMapper.deleteRevaluationStatusByDealnoSeq(map);
	}

	@Override
	public RevalStatus queryRevaStatus(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return revaluationStatusMapper.queryRevaStatus(map);
	}

}
