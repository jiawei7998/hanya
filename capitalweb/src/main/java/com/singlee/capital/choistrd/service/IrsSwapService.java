package com.singlee.capital.choistrd.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SWDH;
import com.singlee.capital.choistrd.model.SWDS;
import com.singlee.capital.choistrd.model.SWDT;

public interface IrsSwapService {
	
	public List<SWDH> queryAllSWDHListIntoWebtFieldSet(Map<String, Object> map) throws Exception;

	public List<SWDT> queryAllSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	public List<SWDS> queryAllSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	public List<SWDH> queryAllRevSWDHListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	public List<SWDT> queryAllRevSWDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;

	public List<SWDS> queryAllRevSWDSListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
}
