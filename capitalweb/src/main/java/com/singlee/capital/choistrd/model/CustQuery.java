package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class CustQuery implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String FXFIG_SVCN;
	private String FXFIG_IBCD;
	private String FXFIG_GWAM;
	private String FXFIG_GEOR;
	private String FXFIG_OPNO;
	private String FXFIG_CIF_NO;
	private String FIIQ13_CCY;
	
	public CustQuery(){}
	/**
	 * @return the fXFIG_SVCN
	 */
	public String getFXFIG_SVCN() {
		return FXFIG_SVCN;
	}
	/**
	 * @param fxfig_svcn the fXFIG_SVCN to set
	 */
	public void setFXFIG_SVCN(String fxfig_svcn) {
		FXFIG_SVCN = fxfig_svcn;
	}
	/**
	 * @return the fXFIG_IBCD
	 */
	public String getFXFIG_IBCD() {
		return FXFIG_IBCD;
	}
	/**
	 * @param fxfig_ibcd the fXFIG_IBCD to set
	 */
	public void setFXFIG_IBCD(String fxfig_ibcd) {
		FXFIG_IBCD = fxfig_ibcd;
	}
	/**
	 * @return the fXFIG_GWAM
	 */
	public String getFXFIG_GWAM() {
		return FXFIG_GWAM;
	}
	/**
	 * @param fxfig_gwam the fXFIG_GWAM to set
	 */
	public void setFXFIG_GWAM(String fxfig_gwam) {
		FXFIG_GWAM = fxfig_gwam;
	}
	/**
	 * @return the fXFIG_GEOR
	 */
	public String getFXFIG_GEOR() {
		return FXFIG_GEOR;
	}
	/**
	 * @param fxfig_geor the fXFIG_GEOR to set
	 */
	public void setFXFIG_GEOR(String fxfig_geor) {
		FXFIG_GEOR = fxfig_geor;
	}
	/**
	 * @return the fXFIG_OPNO
	 */
	public String getFXFIG_OPNO() {
		return FXFIG_OPNO;
	}
	/**
	 * @param fxfig_opno the fXFIG_OPNO to set
	 */
	public void setFXFIG_OPNO(String fxfig_opno) {
		FXFIG_OPNO = fxfig_opno;
	}
	/**
	 * @return the fXFIG_CIF_NO
	 */
	public String getFXFIG_CIF_NO() {
		return FXFIG_CIF_NO;
	}
	/**
	 * @param fxfig_cif_no the fXFIG_CIF_NO to set
	 */
	public void setFXFIG_CIF_NO(String fxfig_cif_no) {
		FXFIG_CIF_NO = fxfig_cif_no;
	}
	/**
	 * @return the fIIQ13_CCY
	 */
	public String getFIIQ13_CCY() {
		return FIIQ13_CCY;
	}
	/**
	 * @param fiiq13_ccy the fIIQ13_CCY to set
	 */
	public void setFIIQ13_CCY(String fiiq13_ccy) {
		FIIQ13_CCY = fiiq13_ccy;
	}


}
