package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class CustCnoCcy implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cno;
	
	private String ccy;
	
	public CustCnoCcy(){}

	/**
	 * @return the cno
	 */
	public String getCno() {
		return cno;
	}

	/**
	 * @param cno the cno to set
	 */
	public void setCno(String cno) {
		this.cno = cno;
	}

	/**
	 * @return the ccy
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * @param ccy the ccy to set
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
	
	
}
