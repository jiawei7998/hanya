package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.OTCP;
import com.singlee.capital.choistrd.model.OTDT;
import com.singlee.capital.choistrd.model.SL_ACUP;


import tk.mybatis.mapper.common.Mapper;



public interface OtcpMapper  extends Mapper<OTCP>{

	/**
	 * 按照头信息的DEALNO号去发送它的明细交易信息
	 * @param dealno
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryAllOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;

	/**
	 * VERIFY
	 * @param dealno
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryVerifyOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 按照头信息的DEALNO号去发送它的明细交易信息
	 * @param dealno
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryAllRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;

	/**
	 * VERIFY
	 * @param dealno
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTCP> queryVerifyRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	
}
