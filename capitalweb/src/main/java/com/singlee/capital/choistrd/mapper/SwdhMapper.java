package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SWDH;

import tk.mybatis.mapper.common.Mapper;

public interface SwdhMapper extends Mapper<SWDH>{
	
	public List<SWDH> queryAllSWDHListIntoWebtFieldSet(Map<String, Object> mapn) throws Exception;
	
	public List<SWDH> queryAllRevSWDHListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
}
