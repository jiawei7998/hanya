package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.service.AmtRangeService;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FxReverse;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.choistrd.service.impl.FxInputTmaxCommand;
import com.singlee.capital.choistrd.service.impl.FxReverseTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value="/SendFxChoisController")
public class SendFxChoisController extends CommonController{
	public static Logger logger = LoggerFactory.getLogger("com");
	@Autowired
	private FxTradeService fxTradeService;
	
	@Autowired
	private FxInputTmaxCommand fxInputTmaxCommand;
	
	@Autowired
	private FxReverseTmaxCommand fxReverseTmaxCommand;
	
	private String beginTime;
	
	private String endTime;
	@ResponseBody
	@RequestMapping(value="/sendFxChois")
	public RetMsg<Serializable> sendFxChois(@RequestBody Map<String, Object> params) throws Exception{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String today = simpleDateFormat.format(new Date());
		
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		String postdate = fxTradeService.queryOpicsPostDate();
		ChiosLogin chiosLogin = new ChiosLogin();
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		 //返回的柜员sessio
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
	   
		logger.info("**************************SEND SPOT INITAIL MSG***************************");
		try
		{
			List<Fx> fxList1 = fxTradeService.queryAllFxListSpot(postdate);
			if(fxList1.size()>0){
				
				for(Fx fx1:fxList1)
				{
					try{
					//logger.info("SEND BEAN:"+BeanUtils.describe(fx1));
						fxInputTmaxCommand.executeHostVisitAction(fx1,"SPOT",sess);
					}catch (Exception e) {
						// TODO: handle exception
						logger.info("executeHostVisitAction Exception:"+e.getMessage());
					}
				}
			}

		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryAllConditionFxListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END SPOT INITAIL MSG***************************");
		logger.info("**************************SEND FWD INITAIL MSG***************************");
		try
		{
			List<Fx> fxList2 = fxTradeService.queryAllFxListFwd("2023-12-20");
			for(Fx fx2:fxList2)
			{
				try{
					//logger.info("SEND BEAN:"+BeanUtils.describe(fx2));
					fxInputTmaxCommand.executeHostVisitAction(fx2,"FWD",sess);
				}catch (Exception e) {
					// TODO: handle exception
					logger.info("executeHostVisitAction Exception:"+e.getMessage());
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryAllConditionFxListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END FWD INITAIL MSG***************************");
		logger.info("**************************SEND SWAP INITAIL MSG***************************");
		try
		{
			List<Fx> fxList3 = fxTradeService.queryAllFxListSwap("2023-12-20");
			for(Fx fx3:fxList3)
			{
				try{
					//logger.info("SEND BEAN:"+BeanUtils.describe(fx3));
					fxInputTmaxCommand.executeHostVisitAction(fx3,"SWAP",sess);
				}catch (Exception e) {
					// TODO: handle exception
					logger.info("executeHostVisitAction Exception:"+e.getMessage());
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryAllConditionFxListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END SWAP INITAIL MSG***************************");
		
		logger.info("**************************SEND FX REVERSE MSG***************************");
		try
		{
			List<FxReverse> fxReverseList = fxTradeService.queryAllFxListRev(postdate);
			for(FxReverse fxReverse:fxReverseList)
			{
				try{
					//logger.info("SEND BEAN:"+BeanUtils.describe(fxReverse));
					fxReverseTmaxCommand.executeHostVisitAction(fxReverse);
				}catch (Exception e) {
					// TODO: handle exception
					logger.info("executeHostVisitAction Exception:"+e.getMessage());
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryAllConditionFxReverseListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END FX REVERSE MSG***************************");
	
		
		return RetMsgHelper.ok("OK");
	}
	
	public static int compare_date(String DATE1, String DATE2) {


		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
		Date dt1 = df.parse(DATE1);
		Date dt2 = df.parse(DATE2);
		if (dt1.getTime() > dt2.getTime()) {
		System.out.println("dt1 在dt2前");
		return 1;
		} else if (dt1.getTime() < dt2.getTime()) {
		System.out.println("dt1在dt2后");
		return -1;
		} else {
		return 0;
		}
		} catch (Exception exception) {
		exception.printStackTrace();
		}
		return 0;
		}
}
