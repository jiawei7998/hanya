package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.AcupMapper;
import com.singlee.capital.choistrd.mapper.OtcpMapper;
import com.singlee.capital.choistrd.mapper.OtdtChoisMapper;
import com.singlee.capital.choistrd.model.OTCP;
import com.singlee.capital.choistrd.model.OTDT;
import com.singlee.capital.choistrd.service.OtcService;

@Service
public class OtcServiceImpl implements OtcService {
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private OtcpMapper otcpMapper ;
	
	@Autowired
	private OtdtChoisMapper otdtMapper ;

	@Override
	public List<OTCP> queryAllOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otcpMapper.queryAllOTCPListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTCP> queryVerifyOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otcpMapper.queryVerifyOTCPListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTDT> queryVerifyOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otdtMapper.queryVerifyOTDTListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTDT> queryAllOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otdtMapper.queryAllOTDTListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTDT> queryVerifyRevOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otdtMapper.queryVerifyRevOTDTListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTDT> queryAllOTDRevTListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otdtMapper.queryAllOTDRevTListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTCP> queryAllRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otcpMapper.queryAllRevOTCPListIntoWebtFieldSet(map);
	}

	@Override
	public List<OTCP> queryVerifyRevOTCPListIntoWebtFieldSet(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return otcpMapper.queryVerifyRevOTCPListIntoWebtFieldSet(map);
	}
	
	
}
