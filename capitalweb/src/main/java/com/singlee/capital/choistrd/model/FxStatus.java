package com.singlee.capital.choistrd.model;

import java.io.Serializable;
/**
 * 
 * @author Win7
 *
 */
public class FxStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  dealno;
	
	private String seq;
	
	private String choisRefNo;
	
	private String choisHisNo;
	
	private String sendtime;
	
	private String issend;
	
	private String isresend;
	
	private String sendresult;
	
	private String errmsg;
	
	private String sendmsg;
	
	/**
	 * 
	TABLENAME VARCHAR(4),
    UPCOUNT INT,
    UPOWN   VARCHAR(10),
    UPTIME DATETIME,
    INPUTTIME DATETIME,
    REVERSAL  CHAR(1)
	 */
	
	private String tablename;
	
	private int upcount;
	
	private String upown;
	
	private String uptime;
	
	private String inputtime;
	
	private String reversal;

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public int getUpcount() {
		return upcount;
	}

	public void setUpcount(int upcount) {
		this.upcount = upcount;
	}

	public String getUpown() {
		return upown;
	}

	public void setUpown(String upown) {
		this.upown = upown;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	/**
	 * @return the reversal
	 */
	public String getReversal() {
		return reversal;
	}

	/**
	 * @param reversal the reversal to set
	 */
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

	public FxStatus(){}
	
	/**
	 * @return the dealno
	 */
	public String getDealno() {
		return dealno;
	}

	/**
	 * @param dealno the dealno to set
	 */
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	
	public String getChoisRefNo() {
		return choisRefNo;
	}

	public void setChoisRefNo(String choisRefNo) {
		this.choisRefNo = choisRefNo;
	}

	public String getChoisHisNo() {
		return choisHisNo;
	}

	public void setChoisHisNo(String choisHisNo) {
		this.choisHisNo = choisHisNo;
	}

	public String getSendtime() {
		return sendtime;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIsresend() {
		return isresend;
	}

	public void setIsresend(String isresend) {
		this.isresend = isresend;
	}

	public String getSendresult() {
		return sendresult;
	}

	public void setSendresult(String sendresult) {
		this.sendresult = sendresult;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	
	
}
