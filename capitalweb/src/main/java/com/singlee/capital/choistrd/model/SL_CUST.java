package com.singlee.capital.choistrd.model;

import java.io.Serializable;

public class SL_CUST implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String CFETSCNO;
	private String OPICSCNO;
	private String CMNE;
	private String CCY;
	private String DEPO_BIC;
	private String BIC_NM;
	private String DEPO_ACCT;
	private String ACCT_NAME;
	private String CNAPS_CD;
	private String CNAPS_NM;
	private String INPUTTIME;
	private int UPDATEFLAG;
	private String SENDMSG;
	private String ERRMSG;
	private String ISSUCCESS;
	
	public SL_CUST(){}
	/**
	 * @return the cFETSCNO
	 */
	public String getCFETSCNO() {
		return CFETSCNO;
	}
	/**
	 * @param cfetscno the cFETSCNO to set
	 */
	public void setCFETSCNO(String cfetscno) {
		CFETSCNO = cfetscno;
	}
	/**
	 * @return the oPICSCNO
	 */
	public String getOPICSCNO() {
		return OPICSCNO;
	}
	/**
	 * @param opicscno the oPICSCNO to set
	 */
	public void setOPICSCNO(String opicscno) {
		OPICSCNO = opicscno;
	}
	/**
	 * @return the cMNE
	 */
	public String getCMNE() {
		return CMNE;
	}
	/**
	 * @param cmne the cMNE to set
	 */
	public void setCMNE(String cmne) {
		CMNE = cmne;
	}
	/**
	 * @return the cCY
	 */
	public String getCCY() {
		return CCY;
	}
	/**
	 * @param ccy the cCY to set
	 */
	public void setCCY(String ccy) {
		CCY = ccy;
	}
	/**
	 * @return the dEPO_BIC
	 */
	public String getDEPO_BIC() {
		return DEPO_BIC;
	}
	/**
	 * @param depo_bic the dEPO_BIC to set
	 */
	public void setDEPO_BIC(String depo_bic) {
		DEPO_BIC = depo_bic;
	}
	/**
	 * @return the dEPO_ACCT
	 */
	public String getDEPO_ACCT() {
		return DEPO_ACCT;
	}
	/**
	 * @param depo_acct the dEPO_ACCT to set
	 */
	public void setDEPO_ACCT(String depo_acct) {
		DEPO_ACCT = depo_acct;
	}
	/**
	 * @return the aCCT_NAME
	 */
	public String getACCT_NAME() {
		return ACCT_NAME;
	}
	/**
	 * @param acct_name the aCCT_NAME to set
	 */
	public void setACCT_NAME(String acct_name) {
		ACCT_NAME = acct_name;
	}
	/**
	 * @return the cNAPS_CD
	 */
	public String getCNAPS_CD() {
		return CNAPS_CD;
	}
	/**
	 * @param cnaps_cd the cNAPS_CD to set
	 */
	public void setCNAPS_CD(String cnaps_cd) {
		CNAPS_CD = cnaps_cd;
	}
	/**
	 * @return the cNAPS_NM
	 */
	public String getCNAPS_NM() {
		return CNAPS_NM;
	}
	/**
	 * @param cnaps_nm the cNAPS_NM to set
	 */
	public void setCNAPS_NM(String cnaps_nm) {
		CNAPS_NM = cnaps_nm;
	}
	/**
	 * @return the iNPUTTIME
	 */
	public String getINPUTTIME() {
		return INPUTTIME;
	}
	/**
	 * @param inputtime the iNPUTTIME to set
	 */
	public void setINPUTTIME(String inputtime) {
		INPUTTIME = inputtime;
	}
	/**
	 * @return the uPDATEFLAG
	 */
	public int getUPDATEFLAG() {
		return UPDATEFLAG;
	}
	/**
	 * @param updateflag the uPDATEFLAG to set
	 */
	public void setUPDATEFLAG(int updateflag) {
		UPDATEFLAG = updateflag;
	}
	/**
	 * @return the sENDMSG
	 */
	public String getSENDMSG() {
		return SENDMSG;
	}
	/**
	 * @param sendmsg the sENDMSG to set
	 */
	public void setSENDMSG(String sendmsg) {
		SENDMSG = sendmsg;
	}
	/**
	 * @return the eRRMSG
	 */
	public String getERRMSG() {
		return ERRMSG;
	}
	/**
	 * @param errmsg the eRRMSG to set
	 */
	public void setERRMSG(String errmsg) {
		ERRMSG = errmsg;
	}
	/**
	 * @return the iSSUCCESS
	 */
	public String getISSUCCESS() {
		return ISSUCCESS;
	}
	/**
	 * @param issuccess the iSSUCCESS to set
	 */
	public void setISSUCCESS(String issuccess) {
		ISSUCCESS = issuccess;
	}
	/**
	 * @return the bIC_NM
	 */
	public String getBIC_NM() {
		return BIC_NM;
	}
	/**
	 * @param bic_nm the bIC_NM to set
	 */
	public void setBIC_NM(String bic_nm) {
		BIC_NM = bic_nm;
	}


}
