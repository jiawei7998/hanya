package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.SWDH;
import com.singlee.capital.choistrd.model.SWDS;
import com.singlee.capital.choistrd.model.SWDT;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.choistrd.service.IrsSwapService;
import com.singlee.capital.choistrd.service.impl.SwapInputTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value = "/SendSwapController")
public class SendSwapController extends CommonController {
	public static Logger logger = LoggerFactory.getLogger("com");

	@Autowired
	private IrsSwapService swapService;
	@Autowired
	private SwapInputTmaxCommand swapInputTmaxCommand;

	@Autowired
	private FxTradeService fxTradeService;
	ChiosLogin chiosLogin = new ChiosLogin();
	String eaiDv = AdapterConstants.EAI_DOM_SVR;

	@ResponseBody
	@RequestMapping(value = "/sendSwapChois")
	public RetMsg<Serializable> sendSwapChois(@RequestBody Map<String, Object> params) throws Exception {

		int ret = 0;
		logger.info("*********SEND SWAP INITAIL MSG*****************");
		// 返回的柜员sessio
		String sess = chiosLogin.sendLogin(eaiDv, "OPS_CHS_DSS00001");
		String postdate = fxTradeService.queryOpicsPostDate();
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("postdate", postdate);
		 //map.put("postdate", "2023-08-09");
		List<SWDH> webtList3 = new ArrayList<SWDH>();
		webtList3 = swapService.queryAllSWDHListIntoWebtFieldSet(map);
		for (SWDH swdh : webtList3) {
			try {
				ret = swapInputTmaxCommand.executeHostVisitAction(swdh, "SWDH", sess);
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<SWDT> webtList4 = new ArrayList<SWDT>();
		webtList4 = swapService.queryAllSWDTListIntoWebtFieldSet(map);
		for (SWDT swdt : webtList4) {
			try {

				ret = swapInputTmaxCommand.executeHostVisitAction(swdt, "SWDT", sess);
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}
		List<SWDS> webtList5 = new ArrayList<SWDS>();
		webtList5 = swapService.queryAllSWDSListIntoWebtFieldSet(map);
		for (SWDS swds : webtList5) {
			try {

				ret = swapInputTmaxCommand.executeHostVisitAction(swds, "SWDS", sess);
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
		}

		logger.info("**************************END SWAP INITAIL MSG***************************");

		logger.info("**************************SEND SWAP REVERSE MSG***************************");
		try {
			List<SWDH> webtList3Reverse = new ArrayList<SWDH>();
			webtList3Reverse = swapService.queryAllRevSWDHListIntoWebtFieldSet(map);
			for (SWDH swdh : webtList3Reverse) {
				try {

					ret = swapInputTmaxCommand.executeHostVisitAction(swdh, "SWDH", sess);
				} catch (Exception e) {
					// TODO: handle exception
					logger.info(e.getMessage());
				}
			}

			List<SWDT> webtList4Reverse = new ArrayList<SWDT>();
			webtList4Reverse = swapService.queryAllRevSWDTListIntoWebtFieldSet(map);
			for (SWDT swdt : webtList4Reverse) {
				try {

					ret = swapInputTmaxCommand.executeHostVisitAction(swdt, "SWDT", sess);
				} catch (Exception e) {
					// TODO: handle exception
					logger.info(e.getMessage());
				}
			}

			List<SWDS> webtList5Reverse = new ArrayList<SWDS>();
			webtList5Reverse = swapService.queryAllRevSWDSListIntoWebtFieldSet(map);
			for (SWDS swds : webtList5Reverse) {
				try {

					ret = swapInputTmaxCommand.executeHostVisitAction(swds, "SWDS", sess);
				} catch (Exception e) {
					// TODO: handle exception
					logger.info(e.getMessage());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("querySwapListProc Exception:" + e.getMessage());
		}
		logger.info("**************************END SWAP REVERSE MSG***************************");
		return RetMsgHelper.ok(ret);

	}

}
