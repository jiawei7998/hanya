package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.SL_ACUP;
import com.singlee.capital.choistrd.model.SL_ACUP_SNEDFLAG;
import com.singlee.capital.choistrd.service.AcupService;
import com.singlee.capital.choistrd.service.impl.AcupTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.page.PageInfo;


@Controller
@RequestMapping(value="/SendAcupController")
public class SendAcupController {
	public static Logger logger = LoggerFactory.getLogger("com");
	
	@Autowired
	private AcupService acupService;
	@Autowired
	private AcupTmaxCommand acupTmaxCommand;
	
	@ResponseBody
	@RequestMapping(value="/acupCreate")
	public RetMsg<Serializable> acupCreate(@RequestBody Map<String, String> params) throws Exception{
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("br", "01");
		map.put("postdate", params.get("postdate"));
		acupService.executeAcupProc(map);
		String code=(String) map.get("RETCODE");
				
		return RetMsgHelper.ok(code);
	}
	
	@ResponseBody
	@RequestMapping(value="/sendAcupChois")
	public RetMsg<Serializable> sendAcupChois(@RequestBody Map<String, Object> params) throws Exception{
		logger.info("开始发送总账--------------");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealflag", "X");
		map.put("postdate", params.get("postdate"));
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		
		ChiosLogin chiosLogin = new ChiosLogin();
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
		int ret=0;
		List<SL_ACUP> setNoList = this.acupService.repeatQueryAllSetNoToSend(map);
		int count=acupService.querySLAcupSendFlag(params.get("postdate").toString());
		if(count==0) {
			if(setNoList.size()>0) {
				for(SL_ACUP acup : setNoList)
				{
					Map<String, Object> map1 = new HashMap<String, Object>();
					map1.put("setno", acup.getSetno());
					//map1.put("postdate", params.get("postdate"));
					
					List<SL_ACUP> acupList = this.acupService.querySLAcupBySetno(map1);
					//ret = this.acupTmaxCommand.executeHostVisitAction(acupList,sess);
					this.acupTmaxCommand.executeHostVisitAction(acupList,sess);
				}
				//2023-10-13
				Map<String, Object> map2 = new HashMap<String, Object>();
				
				//map2.put("pageNumber", "10");
				//map2.put("pageSize","0");
				map2.put("dealflag", "F");
				
			
				PageInfo<SL_ACUP> page = null;
				page = acupService.queryAcup(map2);
				if(page.getList().size()==0) {
					ret = 1;
					acupService.insertSLAcupSendFlag(params.get("postdate").toString());
					
				}else {
					ret = 2;
				}
				/*if(ret==1) {
					acupService.insertSLAcupSendFlag(params.get("postdate").toString());
				}*/
			}
		}else {
			logger.info("已经发送过总账--------------");
			ret=3;
		}
		
		
		logger.info("结束发送总账--------------");
				
		return RetMsgHelper.ok(ret);
	}
	
	@ResponseBody
	@RequestMapping(value="/queryAcup")
	public  RetMsg<PageInfo<SL_ACUP>> queryAcup(@RequestBody Map<String, Object> params) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("pageNumber", params.get("pageNumber"));
		map.put("pageSize", params.get("pageSize"));
		map.put("dealflag", params.get("dealflag"));
		map.put("dealno", params.get("dealno"));
		map.put("postdate", params.get("postdate"));
		PageInfo<SL_ACUP> page=acupService.queryAcup(map);
		return RetMsgHelper.ok(page);
		
	}
	
	@ResponseBody
	@RequestMapping(value="/queryAcupSendFlag")
	public  RetMsg<PageInfo<SL_ACUP_SNEDFLAG>> queryAcupSendFlag(@RequestBody Map<String, Object> params) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageNumber", params.get("pageNumber"));
		map.put("pageSize", params.get("pageSize"));
		map.put("postdate", params.get("postdate"));
		PageInfo<SL_ACUP_SNEDFLAG> page=acupService.queryAcupSendFlag(map);
		return RetMsgHelper.ok(page);
		
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAcupSendFlag")
	public   RetMsg<Serializable> deleteAcupSendFlag(@RequestBody Map<String, String> params) throws Exception{
		
		String postdate=params.get("postdate");
		acupService.deleteAcupSendFlag(postdate);
		return RetMsgHelper.ok();
		
	}
}
