package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.choistrd.service.RevaluationService;
import com.singlee.capital.choistrd.service.impl.FxInputTmaxCommand;
import com.singlee.capital.choistrd.service.impl.RevaluationTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;


@Controller
@RequestMapping(value="/SendRevaluationChoisController")
public class SendRevaluationChoisController extends CommonController{
	public static Logger logger = LoggerFactory.getLogger("com");
	@Autowired
	private RevaluationService revaluationService;
	
	@Autowired
	private RevaluationTmaxCommand revaluationTmaxCommand;
	
	
	
	@ResponseBody
	@RequestMapping(value="/sendChois")
	public RetMsg<Serializable> sendChois(@RequestBody Map<String, Object> params) throws Exception{
		logger.info("**************************开始发送外汇估值***************************");
		ChiosLogin chiosLogin = new ChiosLogin();
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		 //返回的柜员sessio
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
		String postdate =params.get("postdate").toString();
		String FIFE14_OPICS_REF_NO=null;
		int ret=0;
		List<RevaluationTmaxBean> revaluationTmaxBeanList = this.revaluationService.queryAllRevaluationProc(postdate);
		try{
			for(RevaluationTmaxBean revaluationTmaxBean:revaluationTmaxBeanList) {
				 ret = this.revaluationTmaxCommand.executeHostVisitAction(revaluationTmaxBean,postdate,sess);
				FIFE14_OPICS_REF_NO=revaluationTmaxBean.getFIFE14_OPICS_REF_NO().trim();
			}
		}catch (Exception e) {
			// TODO: handle exception
			logger.info("MSG:发送Revaluation估值"+FIFE14_OPICS_REF_NO+"数据异常:"+e.getMessage());
		}
		logger.info("**************************结束外汇发送估值***************************");
		return RetMsgHelper.ok(ret);
	}
}
