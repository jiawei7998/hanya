package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.service.AmtRangeService;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FxReverse;
import com.singlee.capital.choistrd.model.SL_PMTQ;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.choistrd.service.PmtqService;
import com.singlee.capital.choistrd.service.impl.FxInputTmaxCommand;
import com.singlee.capital.choistrd.service.impl.FxReverseTmaxCommand;
import com.singlee.capital.choistrd.service.impl.PmtqInputTmaxCommand;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value="/SendFxChoisController")
public class SendPmtqChoisController extends CommonController{
	public static Logger logger = LoggerFactory.getLogger("com");
	@Autowired
	private PmtqService pmtqService;
	
	@Autowired
	private FxTradeService fxTradeService;
	
	@Autowired
	private PmtqInputTmaxCommand pmtqInputTmaxCommand;
	
	
	
	private String beginTime;
	
	private String endTime;
	@ResponseBody
	@RequestMapping(value="/sendPmtqChois")
	public RetMsg<Serializable> sendPmtqChois(@RequestBody Map<String, Object> params) throws Exception{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String today = simpleDateFormat.format(new Date());
		
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		String postdate = fxTradeService.queryOpicsPostDate();
		ChiosLogin chiosLogin = new ChiosLogin();
		String eaiDv = AdapterConstants.EAI_DOM_SVR;
		 //返回的柜员sessio
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
	   
		logger.info("**************************SEND PMTQ INITAIL MSG***************************");
		try
		{
			List<SL_PMTQ> pmtqList1 = pmtqService.queryAllPmtqFromOpicsByCondition();
			if(pmtqList1.size()>0){
				
				for(SL_PMTQ pmtq1:pmtqList1)
				{
					try{
					//logger.info("SEND BEAN:"+BeanUtils.describe(fx1));
						pmtqInputTmaxCommand.executeHostVisitAction(pmtq1,sess);
					}catch (Exception e) {
						// TODO: handle exception
						logger.info("executeHostVisitAction Exception:"+e.getMessage());
					}
				}
			}

		}catch (Exception e) {
			// TODO: handle exception
			logger.info("queryAllConditionFxListProc Exception:"+e.getMessage());
		}
		logger.info("**************************END PMTQ INITAIL MSG***************************");
		
		return RetMsgHelper.ok("OK");
	}
	
	public static int compare_date(String DATE1, String DATE2) {


		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
		Date dt1 = df.parse(DATE1);
		Date dt2 = df.parse(DATE2);
		if (dt1.getTime() > dt2.getTime()) {
		System.out.println("dt1 在dt2前");
		return 1;
		} else if (dt1.getTime() < dt2.getTime()) {
		System.out.println("dt1在dt2后");
		return -1;
		} else {
		return 0;
		}
		} catch (Exception exception) {
		exception.printStackTrace();
		}
		return 0;
		}
}
