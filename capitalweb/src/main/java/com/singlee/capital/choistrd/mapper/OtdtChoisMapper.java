package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.OTDT;
import com.singlee.capital.choistrd.model.SL_ACUP;

import tk.mybatis.mapper.common.Mapper;



public interface OtdtChoisMapper  extends Mapper<OTDT>{

	/**
	 * VERIFY
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryVerifyRevOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 将需要发送的OTDT数据打包成WebtFieldSet
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryAllOTDRevTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * VERIFY
	 * @param dealDate
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryVerifyOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
	/**
	 * 将需要发送的OTDT数据打包成WebtFieldSet
	 * @return
	 * @throws Exception
	 */
	public List<OTDT> queryAllOTDTListIntoWebtFieldSet(Map<String, Object> map) throws Exception;
	
}
