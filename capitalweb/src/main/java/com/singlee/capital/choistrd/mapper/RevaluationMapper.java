package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FXRevalStatus;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;

import tk.mybatis.mapper.common.Mapper;




public interface RevaluationMapper extends Mapper<RevaluationTmaxBean>{

	/**
	 * @param postDate
	 * @return
	 * @throws Exception
	 */
	public List<RevaluationTmaxBean> queryAllRevaluationProc(String postDate) throws Exception;
	
	public List<RevaluationTmaxBean> queryAllShouldSendRevaluations(String postDate) throws Exception;
	
	
}
