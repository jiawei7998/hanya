package com.singlee.capital.choistrd.model;


import java.io.Serializable;
import java.math.BigDecimal;
public class OTSH implements Serializable {
private static final long serialVersionUID = 1L;
private String BR;
public String getBR() {
return BR;
}
public void setBR(String BR) {
this.BR = BR;
}
private String DEALNO;
public String getDEALNO() {
return DEALNO;
}
public void setDEALNO(String DEALNO) {
this.DEALNO = DEALNO;
}
private String SEQ;
public String getSEQ() {
return SEQ;
}
public void setSEQ(String SEQ) {
this.SEQ = SEQ;
}
private String REFIXDATE;
public String getREFIXDATE() {
return REFIXDATE;
}
public void setREFIXDATE(String REFIXDATE) {
this.REFIXDATE = REFIXDATE;
}
private String REFIXTIME;
public String getREFIXTIME() {
return REFIXTIME;
}
public void setREFIXTIME(String REFIXTIME) {
this.REFIXTIME = REFIXTIME;
}
private String VDATE;
public String getVDATE() {
return VDATE;
}
public void setVDATE(String VDATE) {
this.VDATE = VDATE;
}
private BigDecimal WEIGHT;
public BigDecimal getWEIGHT() {
return WEIGHT;
}
public void setWEIGHT(BigDecimal WEIGHT) {
this.WEIGHT = WEIGHT;
}
private BigDecimal REFIXRATE_8;
public BigDecimal getREFIXRATE_8() {
return REFIXRATE_8;
}
public void setREFIXRATE_8(BigDecimal REFIXRATE_8) {
this.REFIXRATE_8 = REFIXRATE_8;
}
private BigDecimal WEIGHTTODATE;
public BigDecimal getWEIGHTTODATE() {
return WEIGHTTODATE;
}
public void setWEIGHTTODATE(BigDecimal WEIGHTTODATE) {
this.WEIGHTTODATE = WEIGHTTODATE;
}
private BigDecimal SPOTTODATE_8;
public BigDecimal getSPOTTODATE_8() {
return SPOTTODATE_8;
}
public void setSPOTTODATE_8(BigDecimal SPOTTODATE_8) {
this.SPOTTODATE_8 = SPOTTODATE_8;
}
private String REVIND;
public String getREVIND() {
return REVIND;
}
public void setREVIND(String REVIND) {
this.REVIND = REVIND;
}
private BigDecimal AMT1;
public BigDecimal getAMT1() {
return AMT1;
}
public void setAMT1(BigDecimal AMT1) {
this.AMT1 = AMT1;
}
private String CHAR1;
public String getCHAR1() {
return CHAR1;
}
public void setCHAR1(String CHAR1) {
this.CHAR1 = CHAR1;
}
private String DATE1;
public String getDATE1() {
return DATE1;
}
public void setDATE1(String DATE1) {
this.DATE1 = DATE1;
}
private BigDecimal RATE1_8;
public BigDecimal getRATE1_8() {
return RATE1_8;
}
public void setRATE1_8(BigDecimal RATE1_8) {
this.RATE1_8 = RATE1_8;
}
private String LSTMNTDATE;
public String getLSTMNTDATE() {
return LSTMNTDATE;
}
public void setLSTMNTDATE(String LSTMNTDATE) {
this.LSTMNTDATE = LSTMNTDATE;
}
}