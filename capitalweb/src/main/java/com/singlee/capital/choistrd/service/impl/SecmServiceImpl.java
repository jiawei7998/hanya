package com.singlee.capital.choistrd.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.StringUtils;
import com.singlee.capital.choistrd.model.Secm;
import com.singlee.capital.choistrd.service.SecmService;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.ifs.mapper.IfsBondPledgeMapper;
import com.singlee.ifs.model.SecmStatus;

@Service
public class SecmServiceImpl implements SecmService{
	

	
	@Autowired
	IfsBondPledgeMapper ifsBondPledgeMapper;
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	
	ChiosLogin chiosLogin = new ChiosLogin();
	
	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();
	
	public String FXFIG_SSCK;
	public String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	StringUtils stringUtils = new StringUtils();
	
	public boolean executeHostVisitAction() throws Exception {
		// TODO Auto-generated method stub
		boolean retFlag = true;
		String returnFlag = "";
		try {
				long standardStampStart = System.currentTimeMillis();
				
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String branprcdate = null;
				branprcdate = ifsBondPledgeMapper.queryBrps();
				if(branprcdate == null || "".equals(branprcdate) ) {
					branprcdate = sdf.format(date);
				}
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap.put("branprcdate",branprcdate);
				List<Secm> secmList = ifsBondPledgeMapper.queryAllSecm(hashMap);
				SecmStatus secmStatus = new SecmStatus();
				String FXFIG_SVCN = null;
				String FXFIG_IBCD = null;
				String FXFIG_GWAM = null;
				String FXFIG_GEOR = null;
				String FXFIG_OPNO = null;
				String FIFB10_SECURITY_ID = null;
				String FIFB10_COUPON_STYLE = null;
				String FIFB10_BOND_TYPE = null;
				String FIFB10_ISSUE_IL = null;
				String FIFB10_EXP_IL = null;
				String FIFB10_ISSUE_MTD = null;
				String FIFB10_ISSUE_MARKET_NM = null;
				String FIFB10_ISSUE_MKT_NATION_CD = null;
				String FIFB10_REPAY_PRIORITY = null;
				String FIFB10_CREDIT_AGENCY = null;
				BigDecimal FIFB10_TOTAL_ISSUE_AMT = null;
				String FIFB10_TOTAL_ISSUE_CCY = null;
				String FIFB10_REPAY_MTD = null;
				String FIFB10_FST_REPAY_IL = null;
				BigDecimal FIFB10_REPAY_PRICE = null;
				BigDecimal FIFB10_REPAY_AMT = null;
				String FIFB10_FIX_FLT_GB = null;
				BigDecimal FIFB10_COUPON_SPREAD_RT = null;
				String FIFB10_CAP_RT_YN = null;
				BigDecimal FIFB10_CAP_RT = null;
				String FIFB10_FLOOR_RT_YN = null;
				BigDecimal FIFB10_FLOOR_RT = null;
				String FIFB10_ADV_ARR_GB = null;
				String FIFB10_INT_FREQ = null;
				String FIFB10_COUPON_ADJ_YN = null;
				String FIFB10_BSNS_DAY_RULE = null;
				String FIFB10_ACCR_TYPE = null;
				String FIFB10_SINGLE_BOTH_TYPE = null;
				String FIFB10_BASE_IL = null;
				String FIFB10_ISSUE_ID = null;
				String FIFB10_RATE_CCY  = null;
				String FIFB10_RATE_FREQ = null;
				String dealMsg = null;
				for(int i = 0;i<secmList.size();i++) {
					String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
					StdTMsgGenerator generator = new StdTMsgGenerator();
					byte[] headerPacket=choisCommonMsg.makeHead();
					String handleCode="FB1000";
					String serveHandleCode="OPCFB10";
					PropertiesConfiguration stdtmsgSysValue;
					stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
					FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
					String coMsg=choisCommonMsg.makeMsg();
					String coMsg2=choisCommonMsg.makeMsg2();
					String coMsg3=choisCommonMsg.makeMsg3();
					FXFIG_SVCN = secmList.get(i).getFXFIG_SVCN();
					FXFIG_IBCD = secmList.get(i).getFXFIG_IBCD();
					FXFIG_GWAM = secmList.get(i).getFXFIG_GWAM();
					FXFIG_GEOR = secmList.get(i).getFXFIG_GEOR();
					FXFIG_OPNO = secmList.get(i).getFXFIG_OPNO();
					FIFB10_SECURITY_ID = secmList.get(i).getFIFB10_SECURITY_ID();
					FIFB10_COUPON_STYLE = secmList.get(i).getFIFB10_COUPON_STYLE();
					FIFB10_BOND_TYPE = secmList.get(i).getFIFB10_BOND_TYPE();
					FIFB10_ISSUE_IL = secmList.get(i).getFIFB10_ISSUE_IL();
					FIFB10_EXP_IL = secmList.get(i).getFIFB10_EXP_IL();
					FIFB10_ISSUE_MTD = secmList.get(i).getFIFB10_ISSUE_MTD();
					FIFB10_ISSUE_MARKET_NM = secmList.get(i).getFIFB10_ISSUE_MARKET_NM();
					FIFB10_ISSUE_MKT_NATION_CD = secmList.get(i).getFIFB10_ISSUE_MKT_NATION_CD();
					FIFB10_REPAY_PRIORITY = secmList.get(i).getFIFB10_REPAY_PRIORITY();
					FIFB10_CREDIT_AGENCY = secmList.get(i).getFIFB10_CREDIT_AGENCY();
					FIFB10_TOTAL_ISSUE_AMT = secmList.get(i).getFIFB10_TOTAL_ISSUE_AMT();
					FIFB10_TOTAL_ISSUE_CCY = secmList.get(i).getFIFB10_TOTAL_ISSUE_CCY();
					FIFB10_REPAY_MTD = secmList.get(i).getFIFB10_REPAY_MTD();
					FIFB10_FST_REPAY_IL = secmList.get(i).getFIFB10_FST_REPAY_IL();
					FIFB10_REPAY_PRICE = secmList.get(i).getFIFB10_REPAY_PRICE();
					FIFB10_REPAY_AMT = secmList.get(i).getFIFB10_REPAY_AMT();
					FIFB10_FIX_FLT_GB = secmList.get(i).getFIFB10_FIX_FLT_GB();
					FIFB10_COUPON_SPREAD_RT = secmList.get(i).getFIFB10_COUPON_SPREAD_RT();
					FIFB10_CAP_RT_YN = secmList.get(i).getFIFB10_CAP_RT_YN();
					FIFB10_CAP_RT = secmList.get(i).getFIFB10_CAP_RT();
					FIFB10_FLOOR_RT_YN = secmList.get(i).getFIFB10_FLOOR_RT_YN();
					FIFB10_FLOOR_RT = secmList.get(i).getFIFB10_FLOOR_RT();
					FIFB10_ADV_ARR_GB = secmList.get(i).getFIFB10_ADV_ARR_GB();
					FIFB10_INT_FREQ = secmList.get(i).getFIFB10_INT_FREQ();
					FIFB10_COUPON_ADJ_YN = secmList.get(i).getFIFB10_COUPON_ADJ_YN();
					FIFB10_BSNS_DAY_RULE = secmList.get(i).getFIFB10_BSNS_DAY_RULE();
					FIFB10_ACCR_TYPE = secmList.get(i).getFIFB10_ACCR_TYPE();
					FIFB10_SINGLE_BOTH_TYPE = secmList.get(i).getFIFB10_SINGLE_BOTH_TYPE();
					FIFB10_BASE_IL = secmList.get(i).getFIFB10_BASE_IL();
					FIFB10_ISSUE_ID = secmList.get(i).getFIFB10_ISSUE_ID();
					FIFB10_RATE_CCY = secmList.get(i).getFIFB10_RATE_CCY();
					FIFB10_RATE_FREQ = secmList.get(i).getFIFB10_RATE_FREQ();
					dealMsg = String.format("%1$-15s",FIFB10_SECURITY_ID==null?"":FIFB10_SECURITY_ID)
							+String.format("%1$-1s",FIFB10_COUPON_STYLE==null?"":FIFB10_COUPON_STYLE)
							+String.format("%1$-2s",FIFB10_BOND_TYPE==null?"":FIFB10_BOND_TYPE)
							+String.format("%1$-10s",FIFB10_ISSUE_IL==null?"":FIFB10_ISSUE_IL)
							+String.format("%1$-10s",FIFB10_EXP_IL==null?"":FIFB10_EXP_IL)
							+String.format("%1$-1s",FIFB10_ISSUE_MTD==null?"":FIFB10_ISSUE_MTD)
							+String.format("%1$-30s",FIFB10_ISSUE_MARKET_NM==null?"":FIFB10_ISSUE_MARKET_NM)
							+String.format("%1$-2s",FIFB10_ISSUE_MKT_NATION_CD==null?"":FIFB10_ISSUE_MKT_NATION_CD)
							+String.format("%1$-1s",FIFB10_REPAY_PRIORITY==null?"":FIFB10_REPAY_PRIORITY)
							+String.format("%1$-2s",FIFB10_CREDIT_AGENCY==null?"":FIFB10_CREDIT_AGENCY)
							+stringUtils.fillZero((FIFB10_TOTAL_ISSUE_AMT==null?new BigDecimal("0"):FIFB10_TOTAL_ISSUE_AMT).toString(),16,2,true,true)
							+String.format("%1$-3s",FIFB10_TOTAL_ISSUE_CCY==null?"":FIFB10_TOTAL_ISSUE_CCY)
							+String.format("%1$-1s",FIFB10_REPAY_MTD==null?"":FIFB10_REPAY_MTD)
							+String.format("%1$-10s",FIFB10_FST_REPAY_IL==null?"":FIFB10_FST_REPAY_IL)
							+stringUtils.fillZero((FIFB10_REPAY_PRICE==null?new BigDecimal("0"):FIFB10_REPAY_PRICE).toString(),16,10,true,true)
							+stringUtils.fillZero((FIFB10_REPAY_AMT==null?new BigDecimal("0"):FIFB10_REPAY_AMT).toString(),16,2,true,true)
							+String.format("%1$-1s",FIFB10_FIX_FLT_GB==null?"":FIFB10_FIX_FLT_GB)
							+stringUtils.fillZero((FIFB10_COUPON_SPREAD_RT==null?new BigDecimal("0"):FIFB10_COUPON_SPREAD_RT).toString(),16,10,true,true)
							+String.format("%1$-1s",FIFB10_CAP_RT_YN==null?"":FIFB10_CAP_RT_YN)
							+stringUtils.fillZero((FIFB10_CAP_RT==null?new BigDecimal("0"):FIFB10_CAP_RT).toString(),16,10,true,true)
							+String.format("%1$-1s",FIFB10_FLOOR_RT_YN==null?"":FIFB10_FLOOR_RT_YN)
							+stringUtils.fillZero((FIFB10_FLOOR_RT==null?new BigDecimal("0"):FIFB10_FLOOR_RT).toString(),16,10,true,true)
							+String.format("%1$-1s",FIFB10_SINGLE_BOTH_TYPE==null?"":FIFB10_SINGLE_BOTH_TYPE)
							+String.format("%1$-10s",FIFB10_BASE_IL==null?"":FIFB10_BASE_IL)
							+String.format("%1$-10s",FIFB10_ISSUE_ID==null?"":FIFB10_ISSUE_ID)
							+String.format("%1$-3s",FIFB10_RATE_CCY==null?"":FIFB10_RATE_CCY)
							+String.format("%1$-1s",FIFB10_RATE_FREQ==null?"":FIFB10_RATE_FREQ)
							+String.format("%1$-1s",FIFB10_ADV_ARR_GB==null?"":FIFB10_ADV_ARR_GB)
							+String.format("%1$-1s",FIFB10_INT_FREQ==null?"":FIFB10_INT_FREQ)
							+String.format("%1$-1s",FIFB10_COUPON_ADJ_YN==null?"":FIFB10_COUPON_ADJ_YN)
							+String.format("%1$-1s",FIFB10_BSNS_DAY_RULE==null?"":FIFB10_BSNS_DAY_RULE)
							+String.format("%1$-1s",FIFB10_ACCR_TYPE==null?"":FIFB10_ACCR_TYPE);
					byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
					byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
					String serviceUrl = "OPS";
					byte[] msgData =totalPacket;
					EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
					System.out.println(":--"+eaiDv);
//					ifsBondPledgeMapper.insertSecmStatus(secmStatus);
					
					byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
//					System.out.println("FX��ֵ������Ϣ��"+new String(returnData, "UTF-8"));
					int len=(new String(returnData, "UTF-8")).length();
					String choisHisNoString=(new String(returnData, "UTF-8")).substring(len-220,len-218);
					
					logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
					try {
						logger.info("RECV MSG:" + new String(returnData, "UTF-8"));
						String recMsg = new String(returnData, "utf-8");
						logger.info("==============:" + recMsg);
	
						 returnFlag = new String(returnData, "UTF-8").substring(290,292);
						 SecmStatus oldSecmStatus = ifsBondPledgeMapper.querySecmStatusByDealnoSeq(secmList.get(i).getFIFB10_SECURITY_ID());
						 secmStatus.setSecid(secmList.get(i).getFIFB10_SECURITY_ID());
						 secmStatus.setChoisRefNo("");
						 secmStatus.setChoisHisNo("");
						 secmStatus.setSendtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						 secmStatus.setSendmsg(new String(msgData, "UTF-8"));
						 secmStatus.setRecvmsg(new String(returnData, "UTF-8"));
						 secmStatus.setTablename("SECM");
						 secmStatus.setUpown("TMAX");
						 secmStatus.setUptime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						 secmStatus.setInputtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						 if(null != oldSecmStatus){
							 if(returnFlag.equals("EM")) {
								 secmStatus.setIssend("N");
								 secmStatus.setIsresend("Y");
								 secmStatus.setSendresult("FAILURE");
								 secmStatus.setUpcount(oldSecmStatus.getUpcount()+1);
								 ifsBondPledgeMapper.updateSecmStatusByDealnoSeq(secmStatus);
							 }
						 }else {
							 if (returnFlag.equals("EM")) {
								 secmStatus.setIssend("N");
								 secmStatus.setIsresend("N");
								 secmStatus.setSendresult("FAILURE");
								 secmStatus.setUpcount(0);
								 logger.info("接收返回的错误报文:" + new String(returnData, "UTF-8"));
							 } else {
								 secmStatus.setIssend("Y");
								 secmStatus.setIsresend("N");
								 secmStatus.setSendresult("SUCCESS");
								 secmStatus.setUpcount(0);
							 }
							 ifsBondPledgeMapper.insertSecmStatus(secmStatus);
						 }
						 
	
					} catch (Exception e) {
						retFlag = false;
	
						e.printStackTrace();
						logger.info("Exception MSG:" + e.getMessage());
	
						chiosLogin.main();
						throw e;
	
					}
				}
				
			long standardStampEnd = System.currentTimeMillis();
		} catch (Exception e) {
			retFlag = false;
			
			logger.info("ROLLBACK");
			logger.info("Exception:"+e.getMessage());
			
			e.printStackTrace();
			chiosLogin.main();
			throw e;
			
		} finally{
			
		}
		return retFlag;
	}
}
