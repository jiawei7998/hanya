package com.singlee.capital.choistrd.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.FxTradeMapper;
import com.singlee.capital.choistrd.model.Fx;
import com.singlee.capital.choistrd.model.FxReverse;
import com.singlee.capital.choistrd.model.FxStatus;
import com.singlee.capital.choistrd.service.FxTradeService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
@Service
public class FxTradeServiceImpl implements FxTradeService {

	@Autowired
	private FxTradeMapper fxTradeMapper ;
	
	

	@Override
	public List<Fx> queryAllConditionFxListProc(String dealDate, String type,
			String condition) throws Exception {
		// TODO Auto-generated method stub
		List<Fx> fxList = new ArrayList<Fx>();
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("dealDate", dealDate);
		instMap.put("type", type);
		instMap.put("condition", condition);
		
		fxList=fxTradeMapper.queryAllConditionFxListProc(instMap);
		Fx fx = null;
		for(Fx newfx :fxList) {
			fx = new Fx();
			fx.setCcy(newfx.getCcy());
			fx.setCcyAmt(newfx.getCcyAmt());
			fx.setCcyRate_8(newfx.getCcyRate_8());
			fx.setCtrAmt(newfx.getCtrAmt());
			fx.setCtrCcy(newfx.getCtrCcy());
			fx.setCustCode(newfx.getCustCode());
			fx.setDealDate(newfx.getDealDate());
			fx.setDealno(newfx.getDealno());
			fx.setInputCode(newfx.getInputCode());
			fx.setProdCode(newfx.getProdCode());
			fx.setServer(newfx.getServer());
			fx.setSettType(newfx.getSettType());
			fx.setSubjectCode(newfx.getSubjectCode());
			fx.setTradCode(newfx.getTradCode());
			fx.setTraderCode(newfx.getTraderCode());
			fx.setVdate(newfx.getVdate());
			fx.setCustCodeName(newfx.getCustCodeName());
			fx.setProdCodeName(newfx.getProdCodeName());
			fx.setDealerId(newfx.getDealerId());
			fx.setBaseCcy(newfx.getBaseCcy());
			fx.setCptyCcy(newfx.getCptyCcy());
			fx.setCustType(newfx.getCustType());
			fx.setDealGb(newfx.getDealGb());
			fx.setDealMatchType(newfx.getDealMatchType());
			fx.setOutOfRiskGb(newfx.getOutOfRiskGb());
			fx.setInOutGb(newfx.getInOutGb());
			fx.setSeq(newfx.getSeq());
			fx.setFarAmt(newfx.getFarAmt());
			fx.setFarCcy(newfx.getFarCcy());
			fx.setFarCtrAmt(newfx.getFarCtrAmt());
			fx.setFarCtrCcy(newfx.getFarCtrCcy());
			fx.setFarRate(newfx.getFarRate());
			fx.setFarType(newfx.getFarType());
			fx.setFarVdate(newfx.getFarType());
			fx.setOutOfRiskGb(newfx.getOutOfRiskGb());
			fx.setNearRefNo(newfx.getNearRefNo());
			fx.setFarRefNo(newfx.getFarRefNo());
			
			/**
			 * 近端清算信息
			 */
			fx.setNEAR_CONFIRM_IL(newfx.getNEAR_CONFIRM_IL());
			fx.setNEAR_CONFIRM_YN(newfx.getNEAR_CONFIRM_YN());
			fx.setNEAR_OUR_PAY_DEPO_CD(newfx.getNEAR_OUR_PAY_DEPO_CD());
			fx.setNEAR_OUR_PAY_DEPO_NM(newfx.getNEAR_OUR_PAY_DEPO_NM());
			fx.setNEAR_OUR_RCV_DEPO_CD(newfx.getNEAR_OUR_RCV_DEPO_CD());
			fx.setNEAR_OUR_RCV_DEPO_NM(newfx.getNEAR_OUR_RCV_DEPO_NM());
			fx.setNEAR_PO_IL(newfx.getNEAR_PO_IL());
			fx.setNEAR_PO_YN(newfx.getNEAR_PO_YN());
			fx.setNEAR_THR_RCV_DEPO_BIC(newfx.getNEAR_THR_RCV_DEPO_BIC());
			fx.setNEAR_THR_RCV_DEPO_NM(newfx.getNEAR_THR_RCV_DEPO_NM());
			fx.setCNAPS_YN(newfx.getCNAPS_YN());
			/**
			 * 远端清算信息
			 */
			fx.setFAR_CONFIRM_IL(newfx.getFAR_CONFIRM_IL());
			fx.setFAR_CONFIRM_YN(newfx.getFAR_CONFIRM_YN());
			fx.setFAR_OUR_PAY_DEPO_CD(newfx.getFAR_OUR_PAY_DEPO_CD());
			fx.setFAR_OUR_PAY_DEPO_NM(newfx.getFAR_OUR_PAY_DEPO_NM());
			fx.setFAR_OUR_RCV_DEPO_CD(newfx.getFAR_OUR_RCV_DEPO_CD());
			fx.setFAR_OUR_RCV_DEPO_NM(newfx.getFAR_OUR_RCV_DEPO_NM());
			fx.setFAR_PO_IL(newfx.getFAR_PO_IL());
			fx.setFAR_PO_YN(newfx.getFAR_PO_YN());
			fx.setFAR_THR_RCV_DEPO_BIC(newfx.getFAR_THR_RCV_DEPO_BIC());
			fx.setFAR_THR_RCV_DEPO_NM(newfx.getFAR_THR_RCV_DEPO_NM());
			fx.setFAR_CNAPS_YN(newfx.getFAR_CNAPS_YN());
			/**
			 * 大额支付信息
			 */
			fx.setCNAPS_RCV_ACCT_NO(newfx.getCNAPS_RCV_ACCT_NO());
			fx.setCNAPS_CD(newfx.getCNAPS_CD());
			fx.setCNAPS_NM(newfx.getCNAPS_NM());
			fx.setFAR_CNAPS_RCV_ACCT_NO(newfx.getFAR_CNAPS_RCV_ACCT_NO());
			fx.setFAR_CNAPS_CD(newfx.getFAR_CNAPS_CD());
			fx.setFAR_CNAPS_NM(newfx.getFAR_CNAPS_NM());			
			
			fx.setDEALTYPE(newfx.getDEALTYPE());
			
			//REMARK20150505bsn
			fx.setREMARK(newfx.getREMARK());
		}
		return fxList;
	}

	@Override
	public List<Fx> queryAllConditionFxListProcNew(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FxReverse> queryAllConditionFxReverseListProc(String dealDate, String type,
			String condition) throws Exception {
		// TODO Auto-generated method stub
		List<FxReverse> fxList = new ArrayList<FxReverse>();
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("dealDate", dealDate);
		instMap.put("type", type);
		instMap.put("condition", condition);
		
		fxList=fxTradeMapper.queryAllConditionFxReverseListProc(instMap);
		FxReverse fxReverse = null;
		for(FxReverse reverse : fxList) {
			fxReverse = new FxReverse();
			fxReverse.setFXFIG_REF_NO(reverse.getFXFIG_REF_NO());
			fxReverse.setInputCode(reverse.getInputCode());
			fxReverse.setServer(reverse.getServer());
			fxReverse.setSubjectCode(reverse.getSubjectCode());
			fxReverse.setTradCode(reverse.getTradCode());
			fxReverse.setTraderCode(reverse.getTraderCode());
			
		}
		return fxList;
	}

	@Override
	public FxStatus queryFxStatusByDealnoSeq(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insertFxStatusByNew(FxStatus fxStatus) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.insertFxStatusByNew(fxStatus);
	}

	@Override
	public int updateFxStatusByDealnoSeq(FxStatus fxStatus) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.updateFxStatusByDealnoSeq(fxStatus);
	}

	@Override
	public int deleteFxStatusByDealnoSeq(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateFxStatusByChoisRefHisNo(FxStatus fxStatus) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.updateFxStatusByChoisRefHisNo(fxStatus);
	}

	@Override
	public String queryOpicsPostDate() throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.queryOpicsPostDate();
	}

	@Override
	public List<Fx> queryAllFxListSpot(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.queryAllFxListSpot(postdate);
	}

	@Override
	public List<Fx> queryAllFxListFwd(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.queryAllFxListFwd(postdate);
	}

	@Override
	public List<Fx> queryAllFxListSwap(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.queryAllFxListSwap(postdate);
	}

	@Override
	public List<FxReverse> queryAllFxListRev(String postdate) throws Exception {
		// TODO Auto-generated method stub
		return fxTradeMapper.queryAllFxListRev(postdate);
	}

}
