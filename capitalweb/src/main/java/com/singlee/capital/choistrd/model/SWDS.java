package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SWDS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String product;

    private String prodtype;

    private String intenddte;

    private String schdseq;

    private String schdtype;
    
    private String basis;

    private String brprcindte;

    private String calcrule;

    private String compound;

    private BigDecimal fixedintamt;

    private String fixfloatind;

    private BigDecimal implintamt;

    private BigDecimal implintrate_8;

    private BigDecimal intamt;

    private String intauthdate;

    private String intauthind;

    private String intauthoper;

    private String intccy;

    private BigDecimal intexchrate_8;

    private String intexchterms;

    private BigDecimal intrate_8;

    private BigDecimal intratecap_8;

    private BigDecimal intratefloor_8;

    private String intsacct;

    private String intsmeans;

    private String intstrtdte;

    private String ipaydate;

    private String lstmntdte;

    private BigDecimal netipay;

    private String payrecind;

    private BigDecimal posnintamt;

    private BigDecimal ppayamt;

    private String ppayauthdte;

    private String ppayauthind;

    private String ppayauthoper;

    private String ppayccy;

    private String ppaydate;

    private BigDecimal prinadjamt;

    private BigDecimal prinamt;

    private String princcy;

    private BigDecimal princcyintamt;

    private String prinratecode;

    private String ratecode;

    private String ratefixdte;

    private String raterevdte;

    private String revdate;

    private String raterevfrstlst;

    private BigDecimal spread_8;

    private BigDecimal steprate_8;

    private String verdate;

    private BigDecimal pvamt;

    private BigDecimal spreadamt;

    private BigDecimal pvbamt;

    private String ps;

    private BigDecimal strike_8;

    private BigDecimal index_8;

    private BigDecimal particperc_8;

    private String collarind;

    private BigDecimal exchrate_8;

    private BigDecimal exchamt;

    private String prinsettmeans;

    private String prinsettacct;

    private String lastcoupondate;

    private String exchraterevdate;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal amount3;

    private String char1;

    private String char2;

    private String char3;

    private String date1;

    private String date2;

    private String formulaind;

    private BigDecimal rate1_8;

    private BigDecimal rate2_8;

    private BigDecimal factor_12;

    private String factordate;

    private String factorfixdate;

    private BigDecimal flatintcompamt;

    private BigDecimal sumcompoundamt;

    private BigDecimal remainingpay;

    private String settle;

    private String factorrevdate;

    private String mbspayday;

    private String intdeal;

    private String taxddealno;

    private String taxdproduct;

    private String taxdprodtype;

    private String taxdseq;

    private BigDecimal taxamt;

    private BigDecimal internalrate_8;

    private BigDecimal corpspread_8;

    private BigDecimal corpspreadamt;

    private String exchterms;

    private String flag2;

    private String flag3;

    private BigDecimal rate3_8;

    private BigDecimal rate4_8;

    private String date3;

    private String date4;

    private BigDecimal amount4;

    private BigDecimal amount5;

    private BigDecimal accrint;

    private String ccytax;

    private BigDecimal convprice_12;

    private BigDecimal price_12;

    private BigDecimal priceexchrate_8;

    private String priceexchterms;

    private String intnegauthdte;

    private String intnegauthind;

    private String intnegauthoper;

    private String intnegsacct;

    private String intnegsmeans;

    private String repricedate;

    private BigDecimal rateofreturn_12;

    private BigDecimal divmktmktamt;

    private BigDecimal divpayamt;

    private BigDecimal divpercent_8;

    private String divpaydateind;

    private String equitytype;

    private String breakdate;

    private BigDecimal undintpayamt;

    private String unadjintenddte;

    private String unadjintstrtdte;

    private Short groupid;

    private Short formulaid;

    private Short fixingoverride;

    private String shareadjaccroptind;

    private BigDecimal shareadjrate_8;

    private String ppayverdate;

    private BigDecimal ppayexchrate_8;

    private String ppayexchterms;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDealind() {
		return dealind;
	}

	public void setDealind(String dealind) {
		this.dealind = dealind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getIntenddte() {
		return intenddte;
	}

	public void setIntenddte(String intenddte) {
		this.intenddte = intenddte;
	}

	public String getSchdseq() {
		return schdseq;
	}

	public void setSchdseq(String schdseq) {
		this.schdseq = schdseq;
	}

	public String getSchdtype() {
		return schdtype;
	}

	public void setSchdtype(String schdtype) {
		this.schdtype = schdtype;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(String brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getCalcrule() {
		return calcrule;
	}

	public void setCalcrule(String calcrule) {
		this.calcrule = calcrule;
	}

	public String getCompound() {
		return compound;
	}

	public void setCompound(String compound) {
		this.compound = compound;
	}

	public BigDecimal getFixedintamt() {
		return fixedintamt;
	}

	public void setFixedintamt(BigDecimal fixedintamt) {
		this.fixedintamt = fixedintamt;
	}

	public String getFixfloatind() {
		return fixfloatind;
	}

	public void setFixfloatind(String fixfloatind) {
		this.fixfloatind = fixfloatind;
	}

	public BigDecimal getImplintamt() {
		return implintamt;
	}

	public void setImplintamt(BigDecimal implintamt) {
		this.implintamt = implintamt;
	}

	public BigDecimal getImplintrate_8() {
		return implintrate_8;
	}

	public void setImplintrate_8(BigDecimal implintrate_8) {
		this.implintrate_8 = implintrate_8;
	}

	public BigDecimal getIntamt() {
		return intamt;
	}

	public void setIntamt(BigDecimal intamt) {
		this.intamt = intamt;
	}

	public String getIntauthdate() {
		return intauthdate;
	}

	public void setIntauthdate(String intauthdate) {
		this.intauthdate = intauthdate;
	}

	public String getIntauthind() {
		return intauthind;
	}

	public void setIntauthind(String intauthind) {
		this.intauthind = intauthind;
	}

	public String getIntauthoper() {
		return intauthoper;
	}

	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}

	public String getIntccy() {
		return intccy;
	}

	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}

	public BigDecimal getIntexchrate_8() {
		return intexchrate_8;
	}

	public void setIntexchrate_8(BigDecimal intexchrate_8) {
		this.intexchrate_8 = intexchrate_8;
	}

	public String getIntexchterms() {
		return intexchterms;
	}

	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}

	public BigDecimal getIntrate_8() {
		return intrate_8;
	}

	public void setIntrate_8(BigDecimal intrate_8) {
		this.intrate_8 = intrate_8;
	}

	public BigDecimal getIntratecap_8() {
		return intratecap_8;
	}

	public void setIntratecap_8(BigDecimal intratecap_8) {
		this.intratecap_8 = intratecap_8;
	}

	public BigDecimal getIntratefloor_8() {
		return intratefloor_8;
	}

	public void setIntratefloor_8(BigDecimal intratefloor_8) {
		this.intratefloor_8 = intratefloor_8;
	}

	public String getIntsacct() {
		return intsacct;
	}

	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}

	public String getIntsmeans() {
		return intsmeans;
	}

	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}

	public String getIntstrtdte() {
		return intstrtdte;
	}

	public void setIntstrtdte(String intstrtdte) {
		this.intstrtdte = intstrtdte;
	}

	public String getIpaydate() {
		return ipaydate;
	}

	public void setIpaydate(String ipaydate) {
		this.ipaydate = ipaydate;
	}

	public String getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public BigDecimal getNetipay() {
		return netipay;
	}

	public void setNetipay(BigDecimal netipay) {
		this.netipay = netipay;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public BigDecimal getPosnintamt() {
		return posnintamt;
	}

	public void setPosnintamt(BigDecimal posnintamt) {
		this.posnintamt = posnintamt;
	}

	public BigDecimal getPpayamt() {
		return ppayamt;
	}

	public void setPpayamt(BigDecimal ppayamt) {
		this.ppayamt = ppayamt;
	}

	public String getPpayauthdte() {
		return ppayauthdte;
	}

	public void setPpayauthdte(String ppayauthdte) {
		this.ppayauthdte = ppayauthdte;
	}

	public String getPpayauthind() {
		return ppayauthind;
	}

	public void setPpayauthind(String ppayauthind) {
		this.ppayauthind = ppayauthind;
	}

	public String getPpayauthoper() {
		return ppayauthoper;
	}

	public void setPpayauthoper(String ppayauthoper) {
		this.ppayauthoper = ppayauthoper;
	}

	public String getPpayccy() {
		return ppayccy;
	}

	public void setPpayccy(String ppayccy) {
		this.ppayccy = ppayccy;
	}

	public String getPpaydate() {
		return ppaydate;
	}

	public void setPpaydate(String ppaydate) {
		this.ppaydate = ppaydate;
	}

	public BigDecimal getPrinadjamt() {
		return prinadjamt;
	}

	public void setPrinadjamt(BigDecimal prinadjamt) {
		this.prinadjamt = prinadjamt;
	}

	public BigDecimal getPrinamt() {
		return prinamt;
	}

	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}

	public String getPrinccy() {
		return princcy;
	}

	public void setPrinccy(String princcy) {
		this.princcy = princcy;
	}

	public BigDecimal getPrinccyintamt() {
		return princcyintamt;
	}

	public void setPrinccyintamt(BigDecimal princcyintamt) {
		this.princcyintamt = princcyintamt;
	}

	public String getPrinratecode() {
		return prinratecode;
	}

	public void setPrinratecode(String prinratecode) {
		this.prinratecode = prinratecode;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public String getRatefixdte() {
		return ratefixdte;
	}

	public void setRatefixdte(String ratefixdte) {
		this.ratefixdte = ratefixdte;
	}

	public String getRaterevdte() {
		return raterevdte;
	}

	public void setRaterevdte(String raterevdte) {
		this.raterevdte = raterevdte;
	}

	public String getRevdate() {
		return revdate;
	}

	public void setRevdate(String revdate) {
		this.revdate = revdate;
	}

	public String getRaterevfrstlst() {
		return raterevfrstlst;
	}

	public void setRaterevfrstlst(String raterevfrstlst) {
		this.raterevfrstlst = raterevfrstlst;
	}

	public BigDecimal getSpread_8() {
		return spread_8;
	}

	public void setSpread_8(BigDecimal spread_8) {
		this.spread_8 = spread_8;
	}

	public BigDecimal getSteprate_8() {
		return steprate_8;
	}

	public void setSteprate_8(BigDecimal steprate_8) {
		this.steprate_8 = steprate_8;
	}

	public String getVerdate() {
		return verdate;
	}

	public void setVerdate(String verdate) {
		this.verdate = verdate;
	}

	public BigDecimal getPvamt() {
		return pvamt;
	}

	public void setPvamt(BigDecimal pvamt) {
		this.pvamt = pvamt;
	}

	public BigDecimal getSpreadamt() {
		return spreadamt;
	}

	public void setSpreadamt(BigDecimal spreadamt) {
		this.spreadamt = spreadamt;
	}

	public BigDecimal getPvbamt() {
		return pvbamt;
	}

	public void setPvbamt(BigDecimal pvbamt) {
		this.pvbamt = pvbamt;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public BigDecimal getStrike_8() {
		return strike_8;
	}

	public void setStrike_8(BigDecimal strike_8) {
		this.strike_8 = strike_8;
	}

	public BigDecimal getIndex_8() {
		return index_8;
	}

	public void setIndex_8(BigDecimal index_8) {
		this.index_8 = index_8;
	}

	public BigDecimal getParticperc_8() {
		return particperc_8;
	}

	public void setParticperc_8(BigDecimal particperc_8) {
		this.particperc_8 = particperc_8;
	}

	public String getCollarind() {
		return collarind;
	}

	public void setCollarind(String collarind) {
		this.collarind = collarind;
	}

	public BigDecimal getExchrate_8() {
		return exchrate_8;
	}

	public void setExchrate_8(BigDecimal exchrate_8) {
		this.exchrate_8 = exchrate_8;
	}

	public BigDecimal getExchamt() {
		return exchamt;
	}

	public void setExchamt(BigDecimal exchamt) {
		this.exchamt = exchamt;
	}

	public String getPrinsettmeans() {
		return prinsettmeans;
	}

	public void setPrinsettmeans(String prinsettmeans) {
		this.prinsettmeans = prinsettmeans;
	}

	public String getPrinsettacct() {
		return prinsettacct;
	}

	public void setPrinsettacct(String prinsettacct) {
		this.prinsettacct = prinsettacct;
	}

	public String getLastcoupondate() {
		return lastcoupondate;
	}

	public void setLastcoupondate(String lastcoupondate) {
		this.lastcoupondate = lastcoupondate;
	}

	public String getExchraterevdate() {
		return exchraterevdate;
	}

	public void setExchraterevdate(String exchraterevdate) {
		this.exchraterevdate = exchraterevdate;
	}

	public BigDecimal getAmount1() {
		return amount1;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public BigDecimal getAmount3() {
		return amount3;
	}

	public void setAmount3(BigDecimal amount3) {
		this.amount3 = amount3;
	}

	public String getChar1() {
		return char1;
	}

	public void setChar1(String char1) {
		this.char1 = char1;
	}

	public String getChar2() {
		return char2;
	}

	public void setChar2(String char2) {
		this.char2 = char2;
	}

	public String getChar3() {
		return char3;
	}

	public void setChar3(String char3) {
		this.char3 = char3;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getFormulaind() {
		return formulaind;
	}

	public void setFormulaind(String formulaind) {
		this.formulaind = formulaind;
	}

	public BigDecimal getRate1_8() {
		return rate1_8;
	}

	public void setRate1_8(BigDecimal rate1_8) {
		this.rate1_8 = rate1_8;
	}

	public BigDecimal getRate2_8() {
		return rate2_8;
	}

	public void setRate2_8(BigDecimal rate2_8) {
		this.rate2_8 = rate2_8;
	}

	public BigDecimal getFactor_12() {
		return factor_12;
	}

	public void setFactor_12(BigDecimal factor_12) {
		this.factor_12 = factor_12;
	}

	public String getFactordate() {
		return factordate;
	}

	public void setFactordate(String factordate) {
		this.factordate = factordate;
	}

	public String getFactorfixdate() {
		return factorfixdate;
	}

	public void setFactorfixdate(String factorfixdate) {
		this.factorfixdate = factorfixdate;
	}

	public BigDecimal getFlatintcompamt() {
		return flatintcompamt;
	}

	public void setFlatintcompamt(BigDecimal flatintcompamt) {
		this.flatintcompamt = flatintcompamt;
	}

	public BigDecimal getSumcompoundamt() {
		return sumcompoundamt;
	}

	public void setSumcompoundamt(BigDecimal sumcompoundamt) {
		this.sumcompoundamt = sumcompoundamt;
	}

	public BigDecimal getRemainingpay() {
		return remainingpay;
	}

	public void setRemainingpay(BigDecimal remainingpay) {
		this.remainingpay = remainingpay;
	}

	public String getSettle() {
		return settle;
	}

	public void setSettle(String settle) {
		this.settle = settle;
	}

	public String getFactorrevdate() {
		return factorrevdate;
	}

	public void setFactorrevdate(String factorrevdate) {
		this.factorrevdate = factorrevdate;
	}

	public String getMbspayday() {
		return mbspayday;
	}

	public void setMbspayday(String mbspayday) {
		this.mbspayday = mbspayday;
	}

	public String getIntdeal() {
		return intdeal;
	}

	public void setIntdeal(String intdeal) {
		this.intdeal = intdeal;
	}

	public String getTaxddealno() {
		return taxddealno;
	}

	public void setTaxddealno(String taxddealno) {
		this.taxddealno = taxddealno;
	}

	public String getTaxdproduct() {
		return taxdproduct;
	}

	public void setTaxdproduct(String taxdproduct) {
		this.taxdproduct = taxdproduct;
	}

	public String getTaxdprodtype() {
		return taxdprodtype;
	}

	public void setTaxdprodtype(String taxdprodtype) {
		this.taxdprodtype = taxdprodtype;
	}

	public String getTaxdseq() {
		return taxdseq;
	}

	public void setTaxdseq(String taxdseq) {
		this.taxdseq = taxdseq;
	}

	public BigDecimal getTaxamt() {
		return taxamt;
	}

	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}

	public BigDecimal getInternalrate_8() {
		return internalrate_8;
	}

	public void setInternalrate_8(BigDecimal internalrate_8) {
		this.internalrate_8 = internalrate_8;
	}

	public BigDecimal getCorpspread_8() {
		return corpspread_8;
	}

	public void setCorpspread_8(BigDecimal corpspread_8) {
		this.corpspread_8 = corpspread_8;
	}

	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}

	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}

	public String getExchterms() {
		return exchterms;
	}

	public void setExchterms(String exchterms) {
		this.exchterms = exchterms;
	}

	public String getFlag2() {
		return flag2;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public String getFlag3() {
		return flag3;
	}

	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}

	public BigDecimal getRate3_8() {
		return rate3_8;
	}

	public void setRate3_8(BigDecimal rate3_8) {
		this.rate3_8 = rate3_8;
	}

	public BigDecimal getRate4_8() {
		return rate4_8;
	}

	public void setRate4_8(BigDecimal rate4_8) {
		this.rate4_8 = rate4_8;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public String getDate4() {
		return date4;
	}

	public void setDate4(String date4) {
		this.date4 = date4;
	}

	public BigDecimal getAmount4() {
		return amount4;
	}

	public void setAmount4(BigDecimal amount4) {
		this.amount4 = amount4;
	}

	public BigDecimal getAmount5() {
		return amount5;
	}

	public void setAmount5(BigDecimal amount5) {
		this.amount5 = amount5;
	}

	public BigDecimal getAccrint() {
		return accrint;
	}

	public void setAccrint(BigDecimal accrint) {
		this.accrint = accrint;
	}

	public String getCcytax() {
		return ccytax;
	}

	public void setCcytax(String ccytax) {
		this.ccytax = ccytax;
	}

	public BigDecimal getConvprice_12() {
		return convprice_12;
	}

	public void setConvprice_12(BigDecimal convprice_12) {
		this.convprice_12 = convprice_12;
	}

	public BigDecimal getPrice_12() {
		return price_12;
	}

	public void setPrice_12(BigDecimal price_12) {
		this.price_12 = price_12;
	}

	public BigDecimal getPriceexchrate_8() {
		return priceexchrate_8;
	}

	public void setPriceexchrate_8(BigDecimal priceexchrate_8) {
		this.priceexchrate_8 = priceexchrate_8;
	}

	public String getPriceexchterms() {
		return priceexchterms;
	}

	public void setPriceexchterms(String priceexchterms) {
		this.priceexchterms = priceexchterms;
	}

	public String getIntnegauthdte() {
		return intnegauthdte;
	}

	public void setIntnegauthdte(String intnegauthdte) {
		this.intnegauthdte = intnegauthdte;
	}

	public String getIntnegauthind() {
		return intnegauthind;
	}

	public void setIntnegauthind(String intnegauthind) {
		this.intnegauthind = intnegauthind;
	}

	public String getIntnegauthoper() {
		return intnegauthoper;
	}

	public void setIntnegauthoper(String intnegauthoper) {
		this.intnegauthoper = intnegauthoper;
	}

	public String getIntnegsacct() {
		return intnegsacct;
	}

	public void setIntnegsacct(String intnegsacct) {
		this.intnegsacct = intnegsacct;
	}

	public String getIntnegsmeans() {
		return intnegsmeans;
	}

	public void setIntnegsmeans(String intnegsmeans) {
		this.intnegsmeans = intnegsmeans;
	}

	public String getRepricedate() {
		return repricedate;
	}

	public void setRepricedate(String repricedate) {
		this.repricedate = repricedate;
	}

	public BigDecimal getRateofreturn_12() {
		return rateofreturn_12;
	}

	public void setRateofreturn_12(BigDecimal rateofreturn_12) {
		this.rateofreturn_12 = rateofreturn_12;
	}

	public BigDecimal getDivmktmktamt() {
		return divmktmktamt;
	}

	public void setDivmktmktamt(BigDecimal divmktmktamt) {
		this.divmktmktamt = divmktmktamt;
	}

	public BigDecimal getDivpayamt() {
		return divpayamt;
	}

	public void setDivpayamt(BigDecimal divpayamt) {
		this.divpayamt = divpayamt;
	}

	public BigDecimal getDivpercent_8() {
		return divpercent_8;
	}

	public void setDivpercent_8(BigDecimal divpercent_8) {
		this.divpercent_8 = divpercent_8;
	}

	public String getDivpaydateind() {
		return divpaydateind;
	}

	public void setDivpaydateind(String divpaydateind) {
		this.divpaydateind = divpaydateind;
	}

	public String getEquitytype() {
		return equitytype;
	}

	public void setEquitytype(String equitytype) {
		this.equitytype = equitytype;
	}

	public String getBreakdate() {
		return breakdate;
	}

	public void setBreakdate(String breakdate) {
		this.breakdate = breakdate;
	}

	public BigDecimal getUndintpayamt() {
		return undintpayamt;
	}

	public void setUndintpayamt(BigDecimal undintpayamt) {
		this.undintpayamt = undintpayamt;
	}

	public String getUnadjintenddte() {
		return unadjintenddte;
	}

	public void setUnadjintenddte(String unadjintenddte) {
		this.unadjintenddte = unadjintenddte;
	}

	public String getUnadjintstrtdte() {
		return unadjintstrtdte;
	}

	public void setUnadjintstrtdte(String unadjintstrtdte) {
		this.unadjintstrtdte = unadjintstrtdte;
	}

	public Short getGroupid() {
		return groupid;
	}

	public void setGroupid(Short groupid) {
		this.groupid = groupid;
	}

	public Short getFormulaid() {
		return formulaid;
	}

	public void setFormulaid(Short formulaid) {
		this.formulaid = formulaid;
	}

	public Short getFixingoverride() {
		return fixingoverride;
	}

	public void setFixingoverride(Short fixingoverride) {
		this.fixingoverride = fixingoverride;
	}

	public String getShareadjaccroptind() {
		return shareadjaccroptind;
	}

	public void setShareadjaccroptind(String shareadjaccroptind) {
		this.shareadjaccroptind = shareadjaccroptind;
	}

	public BigDecimal getShareadjrate_8() {
		return shareadjrate_8;
	}

	public void setShareadjrate_8(BigDecimal shareadjrate_8) {
		this.shareadjrate_8 = shareadjrate_8;
	}

	public String getPpayverdate() {
		return ppayverdate;
	}

	public void setPpayverdate(String ppayverdate) {
		this.ppayverdate = ppayverdate;
	}

	public BigDecimal getPpayexchrate_8() {
		return ppayexchrate_8;
	}

	public void setPpayexchrate_8(BigDecimal ppayexchrate_8) {
		this.ppayexchrate_8 = ppayexchrate_8;
	}

	public String getPpayexchterms() {
		return ppayexchterms;
	}

	public void setPpayexchterms(String ppayexchterms) {
		this.ppayexchterms = ppayexchterms;
	}

	
}
