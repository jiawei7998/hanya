package com.singlee.capital.choistrd.mapper;


import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.RevalStatus;
import com.singlee.capital.choistrd.model.FXRevalStatus;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;

import tk.mybatis.mapper.common.Mapper;

public interface FXRevalStatusMapper extends Mapper<FXRevalStatus>{

	/**
	 * ״̬
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	//public  RevaluationStatus queryRevaluationStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	public   List<FXRevalStatus> queryRevaluationStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	public RevalStatus queryRevaStatus(Map<String,Object> map) throws Exception; 
	/**
	 *״̬
	 * @param RevaluationStatus
	 * @return
	 * @throws Exception
	 */
	public int insertRevaluationStatusByNew(FXRevalStatus RevaluationStatus) throws Exception;
	/**
	 * ״̬
	 * @param FXRevalStatus
	 * @return
	 * @throws Exception
	 */
	public int updateRevaluationStatusByDealnoSeq(FXRevalStatus revaluationStatus) throws Exception;
	/**
	 *״̬
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	public int deleteRevaluationStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
	
}
