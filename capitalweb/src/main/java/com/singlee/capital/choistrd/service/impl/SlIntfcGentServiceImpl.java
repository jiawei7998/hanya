package com.singlee.capital.choistrd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.choistrd.mapper.SlIntfcGentMapper;
import com.singlee.capital.choistrd.model.SlIntfcGent;
import com.singlee.capital.choistrd.model.SlIycrGent;
import com.singlee.capital.choistrd.model.SlMktStatus;
import com.singlee.capital.choistrd.service.SlIntfcGentService;
@Service
public class SlIntfcGentServiceImpl implements SlIntfcGentService {

	@Autowired
	private SlIntfcGentMapper slIntfcGentMapper ;
	@Override
	public List<SlIntfcGent> queryGent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.queryGent(map);
	}
	@Override
	public List<SlIycrGent> queryIycrGent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.queryIycrGent(map);
	}
	@Override
	public List<SlMktStatus> queryMkt(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.queryMkt(map);
	}
	@Override
	public SlMktStatus queryMktByType(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.queryMktByType(map);
	}
	@Override
	public int insertMkt(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.insertMkt(map);
	}
	@Override
	public int deleteMkt(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return slIntfcGentMapper.deleteMkt(map);
	}

}
