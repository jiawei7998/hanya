package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.DerivativesStatus;

public interface DerivativesStatusMapper {

	/**
	 * ��ѯ���ݿ���Derivatives���׵�״̬
	 * @param dealno
	 * @param seq
	 * @return
	 * @throws Exception
	 */
	public DerivativesStatus queryDerivativesStatusByDealnoSeq(Map<String,Object> ma) throws Exception;
	
	/**
	 * �����µ�Derivatives״̬
	 * @param DerivativesStatus
	 * @return
	 * @throws Exception
	 */
	public int insertDerivativesStatusByNew(DerivativesStatus derivativesStatus) throws Exception;
	/**
	 * ����Derivatives��״̬
	 * @param DerivativesStatus
	 * @return
	 * @throws Exception
	 */
	public int updateDerivativesStatusByDealnoSeq(DerivativesStatus derivativesStatus) throws Exception;
	
	public List<DerivativesStatus> querySwapDerivativesStatusByDealnoSeq(Map<String,Object> map) throws Exception;
	
}
