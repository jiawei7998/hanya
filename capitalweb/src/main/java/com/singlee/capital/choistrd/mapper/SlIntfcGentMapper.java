package com.singlee.capital.choistrd.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.SL_CUST;
import com.singlee.capital.choistrd.model.SlIntfcGent;
import com.singlee.capital.choistrd.model.SlIycrGent;

import com.singlee.capital.choistrd.model.SlMktStatus;
import com.singlee.ifs.model.IfsIntfcIycrBean;

import tk.mybatis.mapper.common.Mapper;

public interface SlIntfcGentMapper extends Mapper<SlIntfcGent>{
	public List<SlIntfcGent> queryGent(Map<String, Object>  map);
	
	public List<SlIycrGent> queryIycrGent(Map<String, Object>  map);
	
	public List<SlMktStatus> queryMkt(Map<String, Object>  map);
	
	public SlMktStatus queryMktByType(Map<String, Object> map) throws Exception;
	
	public int insertMkt(Map<String, Object> map) throws Exception ;
	
	public int deleteMkt(Map<String, Object> map);
}
