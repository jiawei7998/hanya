package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SL_PMTQ implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*
	 * ��ΰ��FEESģ������ֶ�
	 * 
	 * */
	private String PMTQ_FEE_DEALNO;
	
	private String PMTQ_FEE_PRODUCT;
	
	private String ISREVERSAL;
	private int UPCOUNT;
	private String BR;
	private String CDATE;
	private String CTIME;
	private String CCY;
	
	private String CHOISSENDMSG;
	
	private String CHOISERRMSG;

	/**
	 * @return the cHOISSENDMSG
	 */
	public String getCHOISSENDMSG() {
		return CHOISSENDMSG;
	}

	/**
	 * @param choissendmsg the cHOISSENDMSG to set
	 */
	public void setCHOISSENDMSG(String choissendmsg) {
		CHOISSENDMSG = choissendmsg;
	}

	/**
	 * @return the cHOISERRMSG
	 */
	public String getCHOISERRMSG() {
		return CHOISERRMSG;
	}

	/**
	 * @param choiserrmsg the cHOISERRMSG to set
	 */
	public void setCHOISERRMSG(String choiserrmsg) {
		CHOISERRMSG = choiserrmsg;
	}

	/**
	 * @return the cCY
	 */
	public String getCCY() {
		return CCY;
	}

	/**
	 * @param ccy the cCY to set
	 */
	public void setCCY(String ccy) {
		CCY = ccy;
	}

	public String getBR() {
		return BR;
	}

	public void setBR(String BR) {
		this.BR = BR;
	}

	private String DEALNO;

	public String getDEALNO() {
		return DEALNO;
	}

	public void setDEALNO(String DEALNO) {
		this.DEALNO = DEALNO;
	}

	private String SEQ;

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String SEQ) {
		this.SEQ = SEQ;
	}

	private String PRODUCT;

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String PRODUCT) {
		this.PRODUCT = PRODUCT;
	}

	private String TYPE;

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String TYPE) {
		this.TYPE = TYPE;
	}

	private String VDATE;

	public String getVDATE() {
		return VDATE;
	}

	public void setVDATE(String VDATE) {
		this.VDATE = VDATE;
	}

	private String PAYRECIND;

	public String getPAYRECIND() {
		return PAYRECIND;
	}

	public void setPAYRECIND(String PAYRECIND) {
		this.PAYRECIND = PAYRECIND;
	}

	private String CNO;

	public String getCNO() {
		return CNO;
	}

	public void setCNO(String CNO) {
		this.CNO = CNO;
	}

	private String SETMEANS;

	public String getSETMEANS() {
		return SETMEANS;
	}

	public void setSETMEANS(String SETMEANS) {
		this.SETMEANS = SETMEANS;
	}

	private String SETACCT;

	public String getSETACCT() {
		return SETACCT;
	}

	public void setSETACCT(String SETACCT) {
		this.SETACCT = SETACCT;
	}

	private String COMREFNO;

	public String getCOMREFNO() {
		return COMREFNO;
	}

	public void setCOMREFNO(String COMREFNO) {
		this.COMREFNO = COMREFNO;
	}

	private String SVCN;

	public String getSVCN() {
		return SVCN;
	}

	public void setSVCN(String SVCN) {
		this.SVCN = SVCN;
	}

	private String IBCD;

	public String getIBCD() {
		return IBCD;
	}

	public void setIBCD(String IBCD) {
		this.IBCD = IBCD;
	}

	private String GWAM;

	public String getGWAM() {
		return GWAM;
	}

	public void setGWAM(String GWAM) {
		this.GWAM = GWAM;
	}

	private String GEOR;

	public String getGEOR() {
		return GEOR;
	}

	public void setGEOR(String GEOR) {
		this.GEOR = GEOR;
	}

	private String OPNO;

	public String getOPNO() {
		return OPNO;
	}

	public void setOPNO(String OPNO) {
		this.OPNO = OPNO;
	}

	private BigDecimal FAMT;

	public BigDecimal getFAMT() {
		return FAMT;
	}

	public void setFAMT(BigDecimal FAMT) {
		this.FAMT = FAMT;
	}

	private String CIX_NO;

	public String getCIX_NO() {
		return CIX_NO;
	}

	public void setCIX_NO(String CIX_NO) {
		this.CIX_NO = CIX_NO;
	}

	private String OWN_REF_NO;

	public String getOWN_REF_NO() {
		return OWN_REF_NO;
	}

	public void setOWN_REF_NO(String OWN_REF_NO) {
		this.OWN_REF_NO = OWN_REF_NO;
	}

	private int OWN_HIS_NO;

	public int getOWN_HIS_NO() {
		return OWN_HIS_NO;
	}

	public void setOWN_HIS_NO(int OWN_HIS_NO) {
		this.OWN_HIS_NO = OWN_HIS_NO;
	}

	private String RCV_NAME;

	public String getRCV_NAME() {
		return RCV_NAME;
	}

	public void setRCV_NAME(String RCV_NAME) {
		this.RCV_NAME = RCV_NAME;
	}

	private String RCV_ACCT_NO;

	public String getRCV_ACCT_NO() {
		return RCV_ACCT_NO;
	}

	public void setRCV_ACCT_NO(String RCV_ACCT_NO) {
		this.RCV_ACCT_NO = RCV_ACCT_NO;
	}

	private String RCV_BKCD1;

	public String getRCV_BKCD1() {
		return RCV_BKCD1;
	}

	public void setRCV_BKCD1(String RCV_BKCD1) {
		this.RCV_BKCD1 = RCV_BKCD1;
	}

	private String RCV_BKNM1;

	public String getRCV_BKNM1() {
		return RCV_BKNM1;
	}

	public void setRCV_BKNM1(String RCV_BKNM1) {
		this.RCV_BKNM1 = RCV_BKNM1;
	}

	private String REMARK;

	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String REMARK) {
		this.REMARK = REMARK;
	}

	private String MA_TP;

	public String getMA_TP() {
		return MA_TP;
	}

	public void setMA_TP(String MA_TP) {
		this.MA_TP = MA_TP;
	}

	private String TR_TP;

	public String getTR_TP() {
		return TR_TP;
	}

	public void setTR_TP(String TR_TP) {
		this.TR_TP = TR_TP;
	}

	private String ISSEND;

	public String getISSEND() {
		return ISSEND;
	}

	public void setISSEND(String ISSEND) {
		this.ISSEND = ISSEND;
	}

	private String ISSUCCESS;

	public String getISSUCCESS() {
		return ISSUCCESS;
	}

	public void setISSUCCESS(String ISSUCCESS) {
		this.ISSUCCESS = ISSUCCESS;
	}

	private String SENDTIME;

	public String getSENDTIME() {
		return SENDTIME;
	}

	public void setSENDTIME(String SENDTIME) {
		this.SENDTIME = SENDTIME;
	}

	private String INPUTTIME;

	public String getINPUTTIME() {
		return INPUTTIME;
	}

	public void setINPUTTIME(String INPUTTIME) {
		this.INPUTTIME = INPUTTIME;
	}

	private String CHOIS_REFNO;

	public String getCHOIS_REFNO() {
		return CHOIS_REFNO;
	}

	public void setCHOIS_REFNO(String CHOIS_REFNO) {
		this.CHOIS_REFNO = CHOIS_REFNO;
	}

	private int CHOIS_HISNO;

	public int getCHOIS_HISNO() {
		return CHOIS_HISNO;
	}

	public void setCHOIS_HISNO(int CHOIS_HISNO) {
		this.CHOIS_HISNO = CHOIS_HISNO;
	}

	/**
	 * @return the cDATE
	 */
	public String getCDATE() {
		return CDATE;
	}

	/**
	 * @param cdate the cDATE to set
	 */
	public void setCDATE(String cdate) {
		CDATE = cdate;
	}

	/**
	 * @return the cTIME
	 */
	public String getCTIME() {
		return CTIME;
	}

	/**
	 * @param ctime the cTIME to set
	 */
	public void setCTIME(String ctime) {
		CTIME = ctime;
	}

	/**
	 * @return the iSREVERSAL
	 */
	public String getISREVERSAL() {
		return ISREVERSAL;
	}

	/**
	 * @param isreversal the iSREVERSAL to set
	 */
	public void setISREVERSAL(String isreversal) {
		ISREVERSAL = isreversal;
	}

	/**
	 * @return the uPCOUNT
	 */
	public int getUPCOUNT() {
		return UPCOUNT;
	}

	/**
	 * @param upcount the uPCOUNT to set
	 */
	public void setUPCOUNT(int upcount) {
		UPCOUNT = upcount;
	}

	public String getPMTQ_FEE_DEALNO() {
		return PMTQ_FEE_DEALNO;
	}

	public void setPMTQ_FEE_DEALNO(String pmtq_fee_dealno) {
		PMTQ_FEE_DEALNO = pmtq_fee_dealno;
	}

	public String getPMTQ_FEE_PRODUCT() {
		return PMTQ_FEE_PRODUCT;
	}

	public void setPMTQ_FEE_PRODUCT(String pmtq_fee_product) {
		PMTQ_FEE_PRODUCT = pmtq_fee_product;
	}
}