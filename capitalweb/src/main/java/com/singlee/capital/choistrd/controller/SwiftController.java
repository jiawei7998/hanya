package com.singlee.capital.choistrd.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.chois.util.SwiftProcessor;
import com.singlee.capital.choistrd.service.impl.FxReverseTmaxCommand;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.system.controller.CommonController;

@Controller
@RequestMapping(value="/SwiftController")
public class SwiftController extends CommonController{
	
	@Autowired
	private SwiftProcessor swiftProcessor;
	
	@ResponseBody
	@RequestMapping(value="/sendSwift")
	public RetMsg<Serializable> sendSwift(@RequestBody Map<String, Object> params) throws Exception{
		
		swiftProcessor.initProcessRunner();
		return null;
		
	}
}
