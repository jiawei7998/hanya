package com.singlee.capital.choistrd.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SlNupdStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String br;
	private String nos;
	private String dealno;
	private String seq;
	private String product;
	private String type;
	private Date vdate;
	private Date postdate;
	private String cmne;
	private BigDecimal amtupd;
	private String own_ref_no;
	private String own_his_no;
	private String cancel;
	private String issend;
	private String issuccess;
	private String reversal;
	private String errmsg;
	private String sendmsg;
	private String isresend;
	private String sendtime;
	private String inputtime;
	private Integer upcount;
	private String upown;
	private String uptime;
	private String ref_no;
	private BigDecimal his_no;
	private String upflag;
	private String adflag;
	private String dlflag;
	private String okflag;
	private String fee_dealno;
	private String fee_product;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getNos() {
		return nos;
	}

	public void setNos(String nos) {
		this.nos = nos;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public Date getPostdate() {
		return postdate;
	}

	public void setPostdate(Date postdate) {
		this.postdate = postdate;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public BigDecimal getAmtupd() {
		return amtupd;
	}

	public void setAmtupd(BigDecimal amtupd) {
		this.amtupd = amtupd;
	}

	public String getOwn_ref_no() {
		return own_ref_no;
	}

	public void setOwn_ref_no(String own_ref_no) {
		this.own_ref_no = own_ref_no;
	}

	public String getOwn_his_no() {
		return own_his_no;
	}

	public void setOwn_his_no(String own_his_no) {
		this.own_his_no = own_his_no;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIssuccess() {
		return issuccess;
	}

	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
	}

	public String getReversal() {
		return reversal;
	}

	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	public String getIsresend() {
		return isresend;
	}

	public void setIsresend(String isresend) {
		this.isresend = isresend;
	}

	public String getSendtime() {
		return sendtime;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public Integer getUpcount() {
		return upcount;
	}

	public void setUpcount(Integer upcount) {
		this.upcount = upcount;
	}

	public String getUpown() {
		return upown;
	}

	public void setUpown(String upown) {
		this.upown = upown;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public String getRef_no() {
		return ref_no;
	}

	public void setRef_no(String ref_no) {
		this.ref_no = ref_no;
	}

	public BigDecimal getHis_no() {
		return his_no;
	}

	public void setHis_no(BigDecimal his_no) {
		this.his_no = his_no;
	}

	public String getUpflag() {
		return upflag;
	}

	public void setUpflag(String upflag) {
		this.upflag = upflag;
	}

	public String getAdflag() {
		return adflag;
	}

	public void setAdflag(String adflag) {
		this.adflag = adflag;
	}

	public String getDlflag() {
		return dlflag;
	}

	public void setDlflag(String dlflag) {
		this.dlflag = dlflag;
	}

	public String getOkflag() {
		return okflag;
	}

	public void setOkflag(String okflag) {
		this.okflag = okflag;
	}

	public String getFee_dealno() {
		return fee_dealno;
	}

	public void setFee_dealno(String fee_dealno) {
		this.fee_dealno = fee_dealno;
	}

	public String getFee_product() {
		return fee_product;
	}

	public void setFee_product(String fee_product) {
		this.fee_product = fee_product;
	}
}
