package com.singlee.capital.trade.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.trade.mapper.TdProductFeeCalMapper;
import com.singlee.capital.trade.model.TdProductFeeCal;
import com.singlee.capital.trade.service.TdProductFeeCalService;
@Service
public class TdProductFeeCalServiceImpl implements TdProductFeeCalService {
	@Autowired
	private TdProductFeeCalMapper productFeeCalMapper;
	@Override
	public List<?> getCalFeeForProductDeal(
			Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		Map<String,Object>  feeMap = new HashMap<String, Object>();
		List<FeeCal> feeCals = new ArrayList<TdProductFeeCalServiceImpl.FeeCal>();
		Iterator<String> keyIt =  paramMap.keySet().iterator();
		while(keyIt.hasNext())//翻译计息
		{
			String basis = keyIt.next();
			if(basis.indexOf("asis")>0)
			{
				paramMap.put(basis, DayCountBasis.equal(paramMap.get(basis).toString()).getDenominator());
			}
		}
		feeMap.put("feeMap", paramMap);
		paramMap.put("cfBase", "0");
		List<TdProductFeeCal> productFeeCals = productFeeCalMapper.getProductFeeCalsByPrdNo(paramMap);
		FeeCal feeCal = null;
		for(TdProductFeeCal productFeeCal : productFeeCals){
			feeCal = new FeeCal();
			feeCal.setFeeKey(productFeeCal.getFeeType());
			feeCal.setFeeValue(calcBkIndex(feeMap,productFeeCal.getFeeCexp()));
			paramMap.put(feeCal.getFeeKey(), feeCal.getFeeValue());//回填表单中未计算的数据，以便后续计算能直接引用
			feeCals.add(feeCal);
		}
		return feeCals;
	}

	class FeeCal implements Serializable{
		private static final long serialVersionUID = 1L;
		private String feeKey;
		private double feeValue;
		public String getFeeKey() {
			return feeKey;
		}
		public void setFeeKey(String feeKey) {
			this.feeKey = feeKey;
		}
		public double getFeeValue() {
			return feeValue;
		}
		public void setFeeValue(double feeValue) {
			this.feeValue = feeValue;
		}
		
	}
	
	private static ScriptEngineManager manager = new ScriptEngineManager(); 
	private static ScriptEngine engine;
	/**
	 * 计算记账指标
	 * @param map
	 * @param expression
	 * @return
	 */
	public static double calcBkIndex(Map<String, Object> map, String expression){
		if(engine == null) {
            engine =  manager.getEngineByName("js");
        }
		
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			return Double.parseDouble(val.toString());
		} catch (Exception e) {
			throw new RException(e);
		}
	}
	@Override
	public List<TdProductFeeCal> getProductFeeForPrdNo(
			Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		paramMap.put("cfBase", "0");
		return productFeeCalMapper.getProductFeeCalsByPrdNo(paramMap);
	}
}
