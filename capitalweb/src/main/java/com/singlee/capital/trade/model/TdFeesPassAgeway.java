package com.singlee.capital.trade.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "TD_FEES_PASSAGEWAY")
public class TdFeesPassAgeway {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_PASSAGEWAY.NEXTVAL FROM DUAL")
	private String  dealNo               ;   
	private String  refNo                ;   
	private String  feeType              ;   
	private String  feeSituation         ;   	
	private String  intFre               ; 
	private String  cashtDate            ; 
	private double  feeRate              ;   
	private String  remark                ;  
	private String  feeCcy               ;   
	private String  feeBasis             ;   
	private String  passagewayCno        ;   
	private String  passagewayName       ;   
	private String  passagewayBankacccode;   
	private String  passagewayBankaccname;   
	private String  passagewayBankcode   ;   
	private String  passagewayBankname   ;  
	private String  contact        		;   
	private String  contactPhone       	;   
	private String  contactAddr			;   
	private String  bigKind				;  
	private String  midKind				; 
	private String  smallKind   		;   
	private String  custType   		    ; 
	private String  version             ;
	private String  isPass				;
	

	@Transient
	private List<TdFeesPassAgeway> tdFeesPassAgeways;
	
	
	public List<TdFeesPassAgeway> getTdFeesPassAgeways() {
		return tdFeesPassAgeways;
	}

	public void setTdFeesPassAgeways(List<TdFeesPassAgeway> tdFeesPassAgeways) {
		this.tdFeesPassAgeways = tdFeesPassAgeways;
	}

	public String getIntFre() {
		return intFre;
	}

	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}

	public String getCashtDate() {
		return cashtDate;
	}

	public void setCashtDate(String cashtDate) {
		this.cashtDate = cashtDate;
	}

	public String getIsPass() {
		return isPass;
	}

	public void setIsPass(String isPass) {
		this.isPass = isPass;
	}
	
	public String getMidKind() {
		return midKind;
	}

	public void setMidKind(String midKind) {
		this.midKind = midKind;
	}

	public TdFeesPassAgeway(String dealNo, String refNo, String feeType,
			String feeSituation, double feeRate, String remark, String feeCcy,
			String feeBasis, String passagewayCno, String passagewayName,
			String passagewayBankacccode, String passagewayBankaccname,
			String passagewayBankcode, String passagewayBankname, String version,
			String  contact, String  contactPhone, String  contactAddr, 
			String  bigKind, String  midKind, String  smallKind, String  custType) {
		super();
		this.dealNo = dealNo;
		this.refNo = refNo;
		this.feeType = feeType;
		this.feeSituation = feeSituation;
		this.feeRate = feeRate;
		this.remark = remark;
		this.feeCcy = feeCcy;
		this.feeBasis = feeBasis;
		this.passagewayCno = passagewayCno;
		this.passagewayName = passagewayName;
		this.passagewayBankacccode = passagewayBankacccode;
		this.passagewayBankaccname = passagewayBankaccname;
		this.passagewayBankcode = passagewayBankcode;
		this.passagewayBankname = passagewayBankname;
		this.contact = contact;
		this.contactPhone = contactPhone;
		this.contactAddr = contactAddr;
		this.bigKind = bigKind;
		this.smallKind = smallKind;
		this.custType = custType;
		this.version = version;
	}

	public TdFeesPassAgeway() {
		super();
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getFeeSituation() {
		return feeSituation;
	}

	public void setFeeSituation(String feeSituation) {
		this.feeSituation = feeSituation;
	}

	public double getFeeRate() {
		return feeRate;
	}

	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFeeCcy() {
		return feeCcy;
	}

	public void setFeeCcy(String feeCcy) {
		this.feeCcy = feeCcy;
	}

	public String getFeeBasis() {
		return feeBasis;
	}

	public void setFeeBasis(String feeBasis) {
		this.feeBasis = feeBasis;
	}

	public String getPassagewayCno() {
		return passagewayCno;
	}

	public void setPassagewayCno(String passagewayCno) {
		this.passagewayCno = passagewayCno;
	}

	public String getPassagewayName() {
		return passagewayName;
	}

	public void setPassagewayName(String passagewayName) {
		this.passagewayName = passagewayName;
	}

	public String getPassagewayBankacccode() {
		return passagewayBankacccode;
	}

	public void setPassagewayBankacccode(String passagewayBankacccode) {
		this.passagewayBankacccode = passagewayBankacccode;
	}

	public String getPassagewayBankaccname() {
		return passagewayBankaccname;
	}

	public void setPassagewayBankaccname(String passagewayBankaccname) {
		this.passagewayBankaccname = passagewayBankaccname;
	}

	public String getPassagewayBankcode() {
		return passagewayBankcode;
	}

	public void setPassagewayBankcode(String passagewayBankcode) {
		this.passagewayBankcode = passagewayBankcode;
	}

	public String getPassagewayBankname() {
		return passagewayBankname;
	}

	public void setPassagewayBankname(String passagewayBankname) {
		this.passagewayBankname = passagewayBankname;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactAddr() {
		return contactAddr;
	}

	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}

	public String getBigKind() {
		return bigKind;
	}

	public void setBigKind(String bigKind) {
		this.bigKind = bigKind;
	}

	public String getSmallKind() {
		return smallKind;
	}

	public void setSmallKind(String smallKind) {
		this.smallKind = smallKind;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

}
