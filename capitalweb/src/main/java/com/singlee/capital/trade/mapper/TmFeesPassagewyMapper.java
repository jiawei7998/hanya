package com.singlee.capital.trade.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TmFeesPassagewy;

public interface TmFeesPassagewyMapper extends Mapper<TmFeesPassagewy>{

	public List<TmFeesPassagewy> getTmFeesPassagewys(Map<String, Object> params);

	public void deleteByDealNo(List<String> list);

	public BigDecimal selectInterestByNo(String orderId);

	public List<TmFeesPassagewy> getTmFeesPassagewysByChangeNo(
			Map<String, Object> map);

	public List<TmFeesPassagewy> getByPrimaryKey(Map<String, Object> maps);

}
