package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBalance;
import com.singlee.capital.trade.model.TdProductApproveMain;

public interface TdBalanceService {
	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdBalance getAdvanceMaturityById(String dealNo);
	
	
	/**
	 * 根据主键查询交易结清
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 交易结清对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdBalance getAdvanceMaturityByMap(Map<String,Object> map);
	
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdBalance> getBalancePage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void insertBalance(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateBalance(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteAdvanceMaturity(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	public TdBalance ifExistDuration(Map<String,Object> map);
	
	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 提前到期交易单号
	 */
	public void dealVersionAndCashFlow(String trade_id);
	
}
