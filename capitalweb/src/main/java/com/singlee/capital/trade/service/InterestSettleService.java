package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdInterestSettle;

/**
 * @projectName 同业业务管理系统
 * @className 利率变更服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface InterestSettleService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 利率变更主键
	 * @return 利率变更对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdInterestSettle getInterestSettleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdInterestSettle> getInterestSettlePage (Map<String, Object> params, int isFinish);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createInterestSettle(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateInterestSettle(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteInterestSettle(String[] dealNos);
	/**
	 * 
	 * @param dealNo
	 */
	public void removeInterestSettle(String dealNo);
	/**
	 * 查询出交易所带的所有利率变更交易
	 * @param params
	 * @return
	 */
	public Page<TdInterestSettle> getInterestSettleForDealNoPage (Map<String, Object> params);
	
}
