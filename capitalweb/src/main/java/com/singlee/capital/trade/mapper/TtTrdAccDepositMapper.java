package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdAccDeposit;

/**
 * 同业存放
 * @author 
 *
 */
public interface TtTrdAccDepositMapper extends Mapper<TtTrdAccDeposit>{

	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccDeposit> getDepositList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccDeposit> getDepositListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 */
	public Page<TtTrdAccDeposit> getDepositListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 获取序列
	 * 
	 * @return int
	 */
	public int getSeq();
}