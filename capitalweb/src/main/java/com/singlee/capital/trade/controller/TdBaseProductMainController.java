package com.singlee.capital.trade.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.service.SlSessionHelperExt;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TdBaseproductApproveMain;
import com.singlee.capital.trade.service.TdBaseproductApproveMainService;
@Controller
@RequestMapping("/TdBaseProductMainController")
public class TdBaseProductMainController extends CommonController {
	@Autowired
	TdBaseproductApproveMainService tdBaseproductApproveMainService;
	/**
	 * 保存基础资产
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/saveApprove")
	public TtTrdOrder saveApprove(@RequestBody HashMap<String, Object> params) throws IOException{
		params.put("opUserId", SlSessionHelper.getUserId());
		params.put("accInCash", SlSessionHelperExt.getInCashAccId());
		return tdBaseproductApproveMainService.createBaseProductApprove(params);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageBaseProducts")
	public RetMsg<PageInfo<TdBaseproductApproveMain>> searchPageBaseProducts(@RequestBody Map<String,Object> params) throws RException {
		Page<TdBaseproductApproveMain> page = tdBaseproductApproveMainService.getBaseproductApproveMains(params);
		
		return RetMsgHelper.ok(page);
	}
}
