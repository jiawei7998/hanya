package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdTradeOffsetMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTradeOffset;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TradeOffsetService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 冲销服务
 * @author xuhui
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeOffsetServiceImpl extends AbstractDurationService implements TradeOffsetService {

	@Autowired
	private TdTradeOffsetMapper tdTradeOffsetMapper;
	
	/** 交易单dao **/
//	@Autowired
//	private TrdTradeMapper tradeManageMapper;

	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	DayendDateService dateService;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TrdOrderMapper trdOrderMapper;
	@Autowired
	private TrdTradeMapper trdTradeMapper;
	@Autowired
	private FlowOpService flowOpService;
	
	
	@Override
	public TdTradeOffset getTradeOffsetById(String dealNo) {
		return tdTradeOffsetMapper.getTradeOffsetById(dealNo);
	}

	@Override
	public Page<TdTradeOffset> getTradeOffsetPage(
			Map<String, Object> params, int isFinished) {
		Page<TdTradeOffset> page = new Page<TdTradeOffset>();
		if(isFinished == 2){
			page = tdTradeOffsetMapper.getTradeOffsetList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page = tdTradeOffsetMapper.getTradeOffsetListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page = tdTradeOffsetMapper.getTradeOffsetListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTradeOffset> list =page.getResult();
		for(TdTradeOffset to :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(to.getRefNo());
			if(main!=null){
				to.setProduct(main.getProduct());
				to.setParty(main.getCounterParty());
				to.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建审批单冲销金融工具表")
	public void createTradeOffset(Map<String, Object> params) {
		//tdTradeOffsetMapper.insert(tradeOffset(params));
		TdTradeOffset tradeOffset = new TdTradeOffset();
		try {
			BeanUtil.populate(tradeOffset, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		tradeOffset.setaDate(dateService.getDayendDate());//正式环境上  账务日期和 当前日期一样
		tradeOffset.setSponsor(SlSessionHelper.getUserId());
		tradeOffset.setSponInst(SlSessionHelper.getInstitutionId());
		tradeOffset.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tdTradeOffsetMapper.insert(tradeOffset);
	}

	@Override
	@AutoLogMethod(value = "修改审批单冲销金融工具表")
	public void updateTradeOffset(Map<String, Object> params) {
		//tdTradeOffsetMapper.updateByPrimaryKey(tradeOffset(params));
		TdTradeOffset tradeOffset = new TdTradeOffset();
		try {
			BeanUtil.populate(tradeOffset, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		tradeOffset.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tdTradeOffsetMapper.updateByPrimaryKey(tradeOffset);
	}

	
	public TdTradeOffset tradeOffset(Map<String, Object> params){
		// map转实体类
		TdTradeOffset tradeOffset = new TdTradeOffset();
		try {
			BeanUtil.populate(tradeOffset, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTradeOffset trdOffset = new TdTradeOffset();
        trdOffset.setDealNo(String.valueOf(params.get("dealNo")));
        trdOffset.setDealType(tradeOffset.getDealType());
        trdOffset = tdTradeOffsetMapper.searchTradeOffset(trdOffset);

        if(tradeOffset.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeOffset.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    	
    		tradeOffset.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
        if(trdOffset ==null && DictConstants.DealType.Verify.equals(tradeOffset.getDealType())) {
        	TdTradeOffset tOffset = new TdTradeOffset();
        	tOffset.setDealNo(String.valueOf(params.get("order_id")));
        	tOffset.setDealType(DictConstants.DealType.Approve);
            tOffset = tdTradeOffsetMapper.searchTradeOffset(tOffset);

        	
            tradeOffset.setiCode(tOffset.getiCode());
            tradeOffset.setaType(tOffset.getaType());
            tradeOffset.setmType(tOffset.getmType());
    		tradeOffset.setVersion(tOffset.getVersion());
    		tradeOffset.setRefNo(tOffset.getRefNo());
    		tradeOffset.setAccountingDate(tOffset.getAccountingDate());
    		tradeOffset.setOffsetReason(tOffset.getOffsetReason());
    		tradeOffset.setRemark(tOffset.getRemark());
        	tradeOffset.setSponsor(tOffset.getSponsor());
        	tradeOffset.setSponInst(tOffset.getSponInst());
        	tradeOffset.setaDate(tOffset.getaDate());
        }
        else{
        	if(!DictConstants.DealType.Verify.equals(tradeOffset.getDealType())){
        		tradeOffset.setaDate(DateUtil.getCurrentDateAsString());
        	}
        	else if(trdOffset!=null){
        		tradeOffset.setaDate(trdOffset.getaDate());
        	}
        	tradeOffset.setSponsor(SlSessionHelper.getUserId());
        	tradeOffset.setSponInst(SlSessionHelper.getInstitutionId());
        }

		tradeOffset.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return tradeOffset;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTradeOffset(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdTradeOffsetMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}
	/***
	 * ----以上为对外服务-------------------------------
	 */

	/**
	 * 原交易状态修改为冲销 并释放额度
	 */
	@Override
	public void updateTradeOffsetStatus(String dealNo) {
		TdTradeOffset offset =getTradeOffsetById(dealNo);
		HashMap<String, Object> cmap = new HashMap<String, Object>();
		cmap.put("trade_id", offset.getRefNo());
		cmap.put("version_number", offset.getVersion());
		TtTrdTrade trade = trdTradeMapper.selectTradeForTradeId(cmap);
		if(trade!=null){
			trade.setTrade_status(DictConstants.ApproveStatus.TradeOffset);
			HashMap<String, Object> orderMap = new HashMap<String, Object>();
			orderMap.put("order_id", trade.getOrder_id());
			TtTrdOrder ttTrdOrder = trdOrderMapper.selectOrderForOrderId(orderMap);
			ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.TradeOffset);
			trdOrderMapper.updateApprove(ttTrdOrder);
			trdTradeMapper.updateTrade(trade);
			//删除现金流
			//productApproveService.getProductApproveActivated(offset.getRefNo());
			//cashflowService.deleteCashFlow(approveMain.getiCode(), approveMain.getaType(), approveMain.getmType());
			/**额度释放 start**/
			/*Map<String, Object> map = quotaService.initTrade(offset.getRefNo(), null);
			trdQuotaService.orderReleaseQuota(ParameterUtil.getString(map, "orderId", ""));*/
			/**额度释放 end **/
		}
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTradeOffset tradeOffset=new TdTradeOffset();
		BeanUtil.populate(tradeOffset, params);
		tradeOffset = tdTradeOffsetMapper.searchTradeOffset(tradeOffset);
		return productApproveService.getProductApproveActivated(tradeOffset.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void addEntryOffset(Map<String, Object> mapTemp) {
		tdTradeOffsetMapper.addEntryOffset(mapTemp);
	}

	@Override
	public List<TbkEntry> searchTbkEntryByDealNo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return tdTradeOffsetMapper.searchTbkEntryByDealNo(map);
	}
}
