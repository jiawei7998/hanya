package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.capital.trade.model.TdTradeOffset;

/**
 * @projectName 同业业务管理系统
 * @className 出账交易冲销服务接口
 * @author xuhui
 * @createDate 2016-9-25 下午3:00:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TradeOffsetService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 出账交易冲销主键
	 * @return 出账交易冲销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTradeOffset getTradeOffsetById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 出账交易冲销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeOffset> getTradeOffsetPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 出账交易冲销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createTradeOffset(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateTradeOffset(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 出账交易冲销主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteTradeOffset(String[] dealNos);
	
	/**
	 * 结算完成之后的冲销处理  原交易状态修改为冲销  并释放额度
	 * @param dealNo   冲销单号
	 */
	public void updateTradeOffsetStatus(String dealNo);
	public List<TbkEntry> searchTbkEntryByDealNo(Map<String,Object> map);
	public void addEntryOffset(Map<String, Object> mapTemp);
	
}
