package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TdCrossMarketingMapper;
import com.singlee.capital.trade.model.TdCrossMarketing;
import com.singlee.capital.trade.service.CrossMarketingService;

/**
 * @projectName 同业业务管理系统
 * @className 交叉营销服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CrossMarketingServiceImpl implements CrossMarketingService {
	
	@Autowired
	public TdCrossMarketingMapper tdCrossSellingMapper;
	
	@Override
	public List<TdCrossMarketing> getCrossMarketingList(Map<String, Object> params) {
		return tdCrossSellingMapper.getCrossMarketingList(params);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
