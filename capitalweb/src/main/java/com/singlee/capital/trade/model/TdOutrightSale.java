package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 单笔资产卖断
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_OUTRIGHT_SALE")
public class TdOutrightSale implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	
	private String resReason;
	
	private String osaleTitle;//合同编号
	/**
	 * 交易日期
	 */
	private String dealDate;
	
	private String orgiMdate;//原业务到期日期
	
	private double orgiRate;//原交易利率（当前）%：
	
	private double osaleRate;// 卖断利率(%)：（*）
	
	private double osaleSrate;//卖断利差(%)：（*）
	
	private double osaleAmt;//卖断金额（元）：（*）
	
	private double osaleIamt;//卖断应收利息（元）：
	
	private double osaleAamt;//卖断实际利息（元）：
	
	private double osaleSamt;//卖断利差部分（元）：
    
	private double remainAmt;//卖断时剩余本金
	
	private String osaleVdate;//卖断起息日期：（*）
	
	private String osaleCno;
	
	private String osalePartyName;
	
	private String osaleType;
	
	public String getOsaleType() {
		return osaleType;
	}
	public void setOsaleType(String osaleType) {
		this.osaleType = osaleType;
	}
	public String getOsalePartyName() {
		return osalePartyName;
	}
	public void setOsalePartyName(String osalePartyName) {
		this.osalePartyName = osalePartyName;
	}
	public String getOsaleCno() {
		return osaleCno;
	}
	public void setOsaleCno(String osaleCno) {
		this.osaleCno = osaleCno;
	}
	/**
	 * 影像资料
	 */
	private String image;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	/**
	 * 版本号
	 */
	private int version;
	
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	//现金流类型
	private String cfType;
	
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	
	public String getOrgiMdate() {
		return orgiMdate;
	}
	public void setOrgiMdate(String orgiMdate) {
		this.orgiMdate = orgiMdate;
	}
	
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	
	
	public String getResReason() {
		return resReason;
	}
	public void setResReason(String resReason) {
		this.resReason = resReason;
	}
	public String getOsaleTitle() {
		return osaleTitle;
	}
	public void setOsaleTitle(String osaleTitle) {
		this.osaleTitle = osaleTitle;
	}
	public double getOrgiRate() {
		return orgiRate;
	}
	public void setOrgiRate(double orgiRate) {
		this.orgiRate = orgiRate;
	}
	public double getOsaleRate() {
		return osaleRate;
	}
	public void setOsaleRate(double osaleRate) {
		this.osaleRate = osaleRate;
	}
	public double getOsaleSrate() {
		return osaleSrate;
	}
	public void setOsaleSrate(double osaleSrate) {
		this.osaleSrate = osaleSrate;
	}
	public double getOsaleAmt() {
		return osaleAmt;
	}
	public void setOsaleAmt(double osaleAmt) {
		this.osaleAmt = osaleAmt;
	}
	public double getOsaleIamt() {
		return osaleIamt;
	}
	public void setOsaleIamt(double osaleIamt) {
		this.osaleIamt = osaleIamt;
	}
	public double getOsaleAamt() {
		return osaleAamt;
	}
	public void setOsaleAamt(double osaleAamt) {
		this.osaleAamt = osaleAamt;
	}
	public double getOsaleSamt() {
		return osaleSamt;
	}
	public void setOsaleSamt(double osaleSamt) {
		this.osaleSamt = osaleSamt;
	}
	public String getOsaleVdate() {
		return osaleVdate;
	}
	public void setOsaleVdate(String osaleVdate) {
		this.osaleVdate = osaleVdate;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
}
