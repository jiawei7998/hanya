package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdDurationToDo;

import tk.mybatis.mapper.common.Mapper;

/**
 * @projectName 同业业务管理系统
 * @className 还未到生效日的存续期业务记录
 * @description TODO
 * @author dzy
 * @createDate 2016-10-14 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdDurationToDoMapper extends Mapper<TdDurationToDo> {

	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdDurationToDo> searchDurationToDo(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查询对象
	 * @param change
	 * @return
	 */
	public TdDurationToDo searchDurationToDo(Map<String, Object> params);
}