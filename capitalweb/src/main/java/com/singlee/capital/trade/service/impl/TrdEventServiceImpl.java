package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.TrdEventService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TrdEventServiceImpl implements TrdEventService {

	@Autowired
	private TdTrdEventMapper trdEventMapper;
	
	@Override
	public List<TdTrdEvent> searchTrdEventByKey(Map<String, Object> params) {
		return trdEventMapper.searchTrdEventByKey(params);
	}

	@Override
	public List<TdTrdEvent> getTrdEventForDealNo(Map<String, Object> params) {
		// TODO Auto-generated method stub
		//获得distinct 的交易事件   params  dealNo
		List<TdTrdEvent> resultTrdEvents = new ArrayList<TdTrdEvent>();
		List<TdTrdEvent> trdEvents = this.trdEventMapper.getTrdEventDistinctForDealNo(params);
		for(TdTrdEvent trdEvent : trdEvents){
			if(StringUtils.isNotEmpty(DurationConstants.durationJSPMap.get(trdEvent.getEventTable())))
			{
				trdEvent.setJspUrl(DurationConstants.durationJSPMap.get(trdEvent.getEventTable()));
				resultTrdEvents.add(trdEvent);
			}
		}
		return resultTrdEvents;
	}

}
