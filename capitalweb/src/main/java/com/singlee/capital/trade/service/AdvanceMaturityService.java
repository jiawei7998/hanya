package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统
 * @className 提前到期服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AdvanceMaturityService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdAdvanceMaturity getAdvanceMaturityById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturity> getAdvanceMaturityPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createAdvanceMaturity(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateAdvanceMaturity(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteAdvanceMaturity(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	public TdAdvanceMaturity ifExistDuration(Map<String,Object> map);
	
	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 提前到期交易单号
	 */
	public void dealVersionAndCashFlow(String trade_id);
	
	/**
	 * 计算获取应计利息
	 * @param map
	 * @return
	 */
	public double getAmInt(Map<String,Object> map);
	
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public Page<TdAdvanceMaturity> getAdvanceMaturityForDealNoPage (Map<String, Object> params);
	
	public Integer getDurationUnfinishCount(Map<String,Object> map);
}
