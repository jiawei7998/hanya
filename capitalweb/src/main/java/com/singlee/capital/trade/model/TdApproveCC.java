package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @projectName 同业业务管理系统
 * @className 审批抄送
 * @description TODO
 * @createDate 2017-5-11 15:19
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_APPROVE_CC")
public class TdApproveCC implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6070433770089098601L;
	/**
	 * 业务编号
	 */
	private String serialNo;
	/**
	 * 业务类型
	 */
	private String serialType;
	/**
	 * 抄送人ID
	 */
	private String ccFromUserId;
	/**
	 * 抄送目标人ID
	 */
	private String ccToUserId;
	/**
	 * 抄送时间
	 */
	private String ccTime;
	/**
	 * 是否已读
	 */
	private String isRead;
	/**
	 * 抄送人姓名
	 */
	@Transient
	private String ccFromUserName;
	/**
	 * 抄送目标人姓名
	 */
	@Transient
	private String ccToUserName;
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getSerialType() {
		return serialType;
	}
	public void setSerialType(String serialType) {
		this.serialType = serialType;
	}
	public String getCcFromUserId() {
		return ccFromUserId;
	}
	public void setCcFromUserId(String ccFromUserId) {
		this.ccFromUserId = ccFromUserId;
	}
	public String getCcToUserId() {
		return ccToUserId;
	}
	public void setCcToUserId(String ccToUserId) {
		this.ccToUserId = ccToUserId;
	}
	public String getCcTime() {
		return ccTime;
	}
	public void setCcTime(String ccTime) {
		this.ccTime = ccTime;
	}
	public String getIsRead() {
		return isRead;
	}
	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}
	public String getCcFromUserName() {
		return ccFromUserName;
	}
	public void setCcFromUserName(String ccFromUserName) {
		this.ccFromUserName = ccFromUserName;
	}
	public String getCcToUserName() {
		return ccToUserName;
	}
	public void setCcToUserName(String ccToUserName) {
		this.ccToUserName = ccToUserName;
	}
}
