package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TdBaseAssetGuartyMapper;
import com.singlee.capital.trade.model.TdBaseAssetGuarty;
import com.singlee.capital.trade.service.BaseAssetGuartyService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseAssetGuartyServiceImpl implements BaseAssetGuartyService {
	@Autowired
	TdBaseAssetGuartyMapper assetGuartyMapper;
	@Override
	public List<TdBaseAssetGuarty> getBaseAssetGuartyList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return assetGuartyMapper.getTdBaseAssetGuartyList(params);
	}

}
