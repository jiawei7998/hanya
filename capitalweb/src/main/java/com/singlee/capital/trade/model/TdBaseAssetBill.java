package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产票据
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASE_ASSET_BILL")
public class TdBaseAssetBill implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1146739332851746203L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 票号
	 */
	private String billNo;
	/**
	 * 票据类型
	 */
	private String billType;
	/**
	 * 票据种类
	 */
	private String billKind;
	/**
	 * 出票日
	 */
	private String dDate;
	/**
	 * 到期日
	 */
	private String mDate;
	/**
	 * 票面金额
	 */
	private String billAmt;
	/**
	 * 出票人
	 */
	private String drawer;
	/**
	 * 出票人开户行
	 */
	private String drawerBank;
	/**
	 * 出票人开户行行号 
	 */
	private String drawerBankNo;
	
	private String discountApper;//贴现申请人名称
	private String payeer;//收款人名称
	private String platEarmark;//平台投向
	private String investFinal;//最终投向
	private String indusEarmark;//行业投向
	private double calSamt;//服务费计息金额
	private int calSterm;//服务费计息天数
	private double billSpvrate;//票据转让利息（SPV）
	private double billProcamt;//受益权转让价款
	private double billIntamt;//实际利息
	private int billTerm;//票据计息天数
	
	private String baseCity;
	private String baseProv;
	
	
	public String getBaseCity() {
		return baseCity;
	}
	public void setBaseCity(String baseCity) {
		this.baseCity = baseCity;
	}
	public String getBaseProv() {
		return baseProv;
	}
	public void setBaseProv(String baseProv) {
		this.baseProv = baseProv;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	public String getBillKind() {
		return billKind;
	}
	public void setBillKind(String billKind) {
		this.billKind = billKind;
	}
	public String getdDate() {
		return dDate;
	}
	public void setdDate(String dDate) {
		this.dDate = dDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getBillAmt() {
		return billAmt;
	}
	public void setBillAmt(String billAmt) {
		this.billAmt = billAmt;
	}
	public String getDrawer() {
		return drawer;
	}
	public void setDrawer(String drawer) {
		this.drawer = drawer;
	}
	public String getDrawerBank() {
		return drawerBank;
	}
	public void setDrawerBank(String drawerBank) {
		this.drawerBank = drawerBank;
	}
	public String getDrawerBankNo() {
		return drawerBankNo;
	}
	public void setDrawerBankNo(String drawerBankNo) {
		this.drawerBankNo = drawerBankNo;
	}
	public String getDiscountApper() {
		return discountApper;
	}
	public void setDiscountApper(String discountApper) {
		this.discountApper = discountApper;
	}
	public String getPayeer() {
		return payeer;
	}
	public void setPayeer(String payeer) {
		this.payeer = payeer;
	}
	public String getPlatEarmark() {
		return platEarmark;
	}
	public void setPlatEarmark(String platEarmark) {
		this.platEarmark = platEarmark;
	}
	public String getInvestFinal() {
		return investFinal;
	}
	public void setInvestFinal(String investFinal) {
		this.investFinal = investFinal;
	}
	public String getIndusEarmark() {
		return indusEarmark;
	}
	public void setIndusEarmark(String indusEarmark) {
		this.indusEarmark = indusEarmark;
	}
	public double getCalSamt() {
		return calSamt;
	}
	public void setCalSamt(double calSamt) {
		this.calSamt = calSamt;
	}
	public int getCalSterm() {
		return calSterm;
	}
	public void setCalSterm(int calSterm) {
		this.calSterm = calSterm;
	}
	public double getBillSpvrate() {
		return billSpvrate;
	}
	public void setBillSpvrate(double billSpvrate) {
		this.billSpvrate = billSpvrate;
	}
	public double getBillProcamt() {
		return billProcamt;
	}
	public void setBillProcamt(double billProcamt) {
		this.billProcamt = billProcamt;
	}
	public double getBillIntamt() {
		return billIntamt;
	}
	public void setBillIntamt(double billIntamt) {
		this.billIntamt = billIntamt;
	}
	public int getBillTerm() {
		return billTerm;
	}
	public void setBillTerm(int billTerm) {
		this.billTerm = billTerm;
	}
	
	
}
