package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdTransforScheduleFee;

/**
 * @projectName 同业业务管理系统
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdTransforScheduleFeeMapper extends Mapper<TdTransforScheduleFee> {

	public TdTransforScheduleFee getTransforScheduleFeeById(String dealNo);
	
	public Page<TdTransforScheduleFee> getTransforScheduleFeeList(Map<String, Object> params, RowBounds rb);

	public Page<TdTransforScheduleFee> getTransforScheduleFeeListFinished(Map<String, Object> params, RowBounds rb);

	public Page<TdTransforScheduleFee> getTransforScheduleFeeListMine(Map<String, Object> params, RowBounds rb);
	
	public TdTransforScheduleFee searchTransforScheduleFee(TdTransforScheduleFee tdTransforScheduleFee);
}