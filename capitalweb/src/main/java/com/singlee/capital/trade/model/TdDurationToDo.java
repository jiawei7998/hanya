package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 还未到生效日的存续期业务记录
 * @description TODO
 * @author dzy
 * @createDate 2016-10-14 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_DURATION_TODO")
public class TdDurationToDo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5268483065376041580L;
	
	@Id
	private String dealNo;
	private String refNo;
	private String iCode;
	private String aType;
	private String mType;
	private String eDate;
	private String flag;
	private String createDate;
	private String updateDate;
	private String remark;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String geteDate() {
		return eDate;
	}
	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "TdDurationToDo [dealNo=" + dealNo + ", refNo=" + refNo + ", iCode=" + iCode + ", aType=" + aType
				+ ", mType=" + mType + ", eDate=" + eDate + ", flag=" + flag + ", createDate=" + createDate
				+ ", updateDate=" + updateDate + ", remark=" + remark + "]";
	}
	
}
