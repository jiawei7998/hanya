package com.singlee.capital.trade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TdDurationToDoMapper;
import com.singlee.capital.trade.model.TdDurationToDo;
import com.singlee.capital.trade.service.DurationToDoService;

/**
 * @className 附件树服务
 * @description TODO
 * @author dzy
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DurationToDoServiceImpl implements DurationToDoService {
	
	@Autowired
	private TdDurationToDoMapper tdDurationToDoMapper;

	/**
	 * 查询存续期未到到期日的业务记录
	 */
	@Override
	public Page<TdDurationToDo> getDurationToDo(Map<String, Object> params) {
		return tdDurationToDoMapper.searchDurationToDo(params, ParameterUtil.getRowBounds(params));
	}

	/**
	 * 添加
	 */
	@Override
	public void createDurationToDo(Map<String, Object> params) {
		tdDurationToDoMapper.insert(durationToDo(params));
		
	}

	/**
	 * 修改
	 */
	@Override
	public void updateDurationToDo(Map<String, Object> params) {
		tdDurationToDoMapper.updateByPrimaryKey(durationToDo(params));
		
	}
	
	/**
	 * Map转换成TdDurationToDo对象
	 * @param map
	 * @return
	 */
	public TdDurationToDo durationToDo(Map<String,Object> map){
		TdDurationToDo durationToDo = new TdDurationToDo();
		
		durationToDo.setDealNo(String.valueOf(map.get("dealNo")));
		durationToDo.setRefNo(String.valueOf(map.get("refNo")));
		durationToDo.setiCode(String.valueOf(map.get("iCode")));
		durationToDo.setaType(String.valueOf(map.get("aType")));
		durationToDo.setmType(String.valueOf(map.get("mType")));
		durationToDo.seteDate(String.valueOf(map.get("eDate")));
		durationToDo.setFlag(DictConstants.IBOpStatus.New);
		durationToDo.setCreateDate(DateUtil.getCurrentDateAsString());
		durationToDo.setUpdateDate(DateUtil.getCurrentDateAsString());
		durationToDo.setRemark(String.valueOf(map.get("remark")));
		
		return durationToDo;
	}

	/**
	 * 删除
	 */
	@Override
	public void deleteDurationToDo(String[] dealNos) {
		for(int i=0;i<dealNos.length;i++){
			tdDurationToDoMapper.deleteByPrimaryKey(dealNos[i]);
		}
		
	}
	
}
