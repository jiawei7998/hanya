package com.singlee.capital.trade.acc.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyVo;

public interface TdAccTrdDailyVoMapper{
	/**
	 * 默认获取当前批量日期的数据
	 * @return
	 */
	public List<TdAccTrdDailyVo> getTdAccTrdDailyVos(Map<String,Object> params);
	
	public List<TdAccTrdDailyVo> getTdAccTrdDailyVosCurrent(Map<String,Object> params);

	public Page<TdAccTrdDailyVo> getTdAccTrdDailyVoss(Map<String, Object> params, RowBounds rowBounds);
	
	public List<TdAccTrdDailyVo> getTdAccTrdDailyVosForOverdue(Map<String,Object> params);
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public List<TdAccTrdDailyVo> getTdAccTrdDailyVosForFeeDaily(Map<String,Object> params);
	
	public List<TdAccTrdDailyVo> getFeeWriteOffDatesForOverdue(Map<String,Object> params);
}
