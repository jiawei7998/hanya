package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdMulResaleDetailMapper;
import com.singlee.capital.trade.mapper.TdMulResaleMapper;
import com.singlee.capital.trade.model.TdMultiResale;
import com.singlee.capital.trade.model.TdMultiResaleDetail;
import com.singlee.capital.trade.service.MulResaleService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MulResaleServiceImpl implements MulResaleService {

	@Autowired
	private TdMulResaleMapper tdMulResaleMapper;
	@Autowired
	private TdMulResaleDetailMapper tdMulResaleDetailMapper;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private FlowOpService flowOpService;
	
	@Override
	public TdMultiResale getMulResaleById(String dealNo) {
		return tdMulResaleMapper.getMulResaleById(dealNo);
	}

	@Override
	public Page<TdMultiResale> getMulResalePage(
			Map<String, Object> params, int isFinished) {
		if(isFinished == 2){
			return tdMulResaleMapper.getMulResaleList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			return tdMulResaleMapper.getMulResaleListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			return tdMulResaleMapper.getMulResaleListMine(params, ParameterUtil.getRowBounds(params));
		}
	}

	@Override
	@AutoLogMethod(value = "创建资产卖断金融工具表")
	public void createMulResale(Map<String, Object> params) {
		TdMultiResale resale =tradeExtend(params);
		tdMulResaleMapper.insert(resale);
       //转让资产列表
		String detailList = ParameterUtil.getString(params, "detailList","");
		if(StringUtil.isNotEmpty(detailList)){
			List<TdMultiResaleDetail> list=FastJsonUtil.parseArrays(detailList, TdMultiResaleDetail.class);
			for(TdMultiResaleDetail detail :list){
				detail.setRefNo(resale.getDealNo());
			}
			if(list!=null && list.size()>0){
				batchDao.batch("com.singlee.capital.trade.mapper.TdMulResaleDetailMapper.insert", list);
			}
		}
		
	}

	@Override
	@AutoLogMethod(value = "修改资产卖断金融工具表")
	public void updateMulResale(Map<String, Object> params) {
		TdMultiResale resale =tradeExtend(params);
		tdMulResaleMapper.updateByPrimaryKey(resale);
       //转让资产列表
		String detailList = ParameterUtil.getString(params, "detailList","");
		if(StringUtil.isNotEmpty(detailList)){
			List<TdMultiResaleDetail> list=FastJsonUtil.parseArrays(detailList, TdMultiResaleDetail.class);
			for(TdMultiResaleDetail detail :list){
				detail.setRefNo(resale.getDealNo());
			}
			if(list!=null && list.size()>0){
				tdMulResaleDetailMapper.deleteResaleDetailByRefNo(resale.getDealNo());
				batchDao.batch("com.singlee.capital.trade.mapper.TdMulResaleDetailMapper.insert", list);
			}
		}
		
	}

	
	private TdMultiResale tradeExtend(Map<String, Object> params){
		// map转实体类
		TdMultiResale tdMultiResale = new TdMultiResale();
		try {
			BeanUtil.populate(tdMultiResale, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}

		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdMultiResale extend = new TdMultiResale();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(tdMultiResale.getDealType());
        extend = tdMulResaleMapper.searchMulResale(extend);
       
		//审批完成，为交易单添加一条数据
        if(extend ==null && DictConstants.DealType.Verify.equals(tdMultiResale.getDealType())) {
        	TdMultiResale multiResale = new TdMultiResale();
        	multiResale.setDealNo(ParameterUtil.getString(params, "i_code", ""));
        	multiResale.setDealType(DictConstants.DealType.Approve);
        	multiResale = tdMulResaleMapper.searchMulResale(multiResale);
        	List<TdMultiResaleDetail>  detailList =tdMulResaleDetailMapper.getResaleDetailListByRefNo(multiResale.getDealNo());
        	
        	tdMultiResale.setaDate(multiResale.getaDate());
        	tdMultiResale.setResTitle(multiResale.getResTitle());
        	tdMultiResale.setResDate(multiResale.getResDate());
        	tdMultiResale.setResAmt(multiResale.getResAmt());
        	tdMultiResale.setResInterest(multiResale.getResInterest());
        	tdMultiResale.setTransferIncome(multiResale.getTransferIncome());
        	tdMultiResale.setResReason(multiResale.getResReason());
        	tdMultiResale.setcNo(multiResale.getcNo());
        	tdMultiResale.setVersion(multiResale.getVersion());
        	tdMultiResale.setRemark(multiResale.getRemark());
        	tdMultiResale.setSponsor(multiResale.getSponsor());
        	tdMultiResale.setSponInst(multiResale.getSponInst());
        	
        	if(detailList!=null && detailList.size()>0){
            	for(TdMultiResaleDetail detail : detailList){
            		detail.setRefNo(tdMultiResale.getDealNo());
            	}
            	batchDao.batch("com.singlee.capital.trade.mapper.TdMulResaleDetailMapper.insert", detailList);
        	}
        	
        }
        else{
        	if(!DictConstants.DealType.Verify.equals(tdMultiResale.getDealType())){
        		tdMultiResale.setaDate(DateUtil.getCurrentDateAsString());
        	}
        	else if(extend!=null){
        		tdMultiResale.setaDate(extend.getaDate());
        	}
        	tdMultiResale.setSponsor(SlSessionHelper.getUserId());
        	tdMultiResale.setSponInst(SlSessionHelper.getInstitutionId());
        }

        tdMultiResale.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return tdMultiResale;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteMulResale(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdMulResaleMapper.deleteByPrimaryKey(dealNos[i]);
			tdMulResaleDetailMapper.deleteResaleDetailByRefNo(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	@Override
	public void callBackProcess(String  dealNo) {
		TdMultiResale mulResale =getMulResaleById(dealNo);
		JY.require(mulResale!=null, "卖断信息为空!");
		for(TdMultiResaleDetail detail :mulResale.getDetailList()){
			HashMap<String,Object> dmap =new HashMap<String,Object>();
			dmap.put("dealNo", detail.getDealNo());
			//DurationCustomVo vo = null;//capitalSettleMapper.getDurationCustomVoByDealno(dmap);
			/*if(vo!=null){
				TdProductApproveMain main = productApproveService.getProductApproveActivated(detail.getDealNo());
				MmibFpMLVo mv =cashFlowService.multiResaleFpMLVo(main.getFpml(), mulResale.getResDate(), detail.getMlAmt());
				MmibFactory.createAMmibFpML(mv);
				main.setFpml(mv.getFpml());
				
				List<TtCashflowInterest> intList = new ArrayList<TtCashflowInterest>();
				List<TtCashflowCapital> capList = new ArrayList<TtCashflowCapital>();
				
				intList =calCashFlowService.toTtCashflowInterest(mv.getInterestCashFlowList(), main.getiCode(), main.getaType(), main.getmType(), main.getBasis());
				
				capList =calCashFlowService.toTtCashflowCapital(mv.getCapitalCashFlowList(), main.getiCode(), main.getaType(), main.getmType());

				productApproveMainMapper.updateByPrimaryKey(main);
				
				Map<String,Object> cmap=new HashMap<String,Object>();
				cmap.put("iCode", main.getiCode());
				cmap.put("aType", main.getaType());
				cmap.put("mType", main.getmType());
				ttCashflowInterestMapper.deleteCfInterst(cmap);
				if(intList!=null && intList.size()>0){
					batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowInterestMapper.insert", intList);
				}
				ttCashflowCapitalMapper.deleteCfCapital(cmap);
				if(capList!=null && capList.size()>0){
					batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowCapitalMapper.insert", capList);
				}
				
				HashMap<String,Object> map =new HashMap<String,Object>();
				map.put("trade_id", detail.getiCode());
				TtTrdTrade trade = trdTradeMapper.selectTradeForTradeId(map);
				trdQuotaService.partReleaseQuota(trade.getOrder_id(), detail.getOrignAmt());*/
			}
		}
		
	}