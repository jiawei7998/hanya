package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 模块 功能按钮映射对象
 * @author lyonchen
 *
 */
@Entity
@Table(name = "TA_MODULE_FUNCTION_MAP")
public class ModuleFunctionMap implements Serializable {
	
	private static final long serialVersionUID = -990570599511625157L;
	@Id
	private String moduleId;//模块ID
	@Id
	private String functionId;//功能

	public ModuleFunctionMap() {
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	
}
