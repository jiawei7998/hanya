package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.trade.model.TdBaseproductApproveMain;

public interface TdBaseproductApproveMainService {

	public TtTrdOrder createBaseProductApprove(Map<String, Object> params) ;
	/**
	 * 获取交易对应的基础资产列表
	 * @param params
	 * @return
	 */
	public Page<TdBaseproductApproveMain> getBaseproductApproveMains(Map<String, Object> params);
}
