package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdTradeExtend;
import com.singlee.capital.trade.service.TradeExtendService;

@Controller
@RequestMapping(value="/TdTradeExtendController")
public class TdTradeExtendController extends CommonController{
	@Autowired
	TradeExtendService tradeExtendService;
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExtendForDealNo")
	public RetMsg<PageInfo<TdTradeExtend>> searchPageTradeExtendForDealNo(@RequestBody Map<String,Object> params){
		Page<TdTradeExtend> page = tradeExtendService.getTradeExtendForDealNoPage(params);
		return RetMsgHelper.ok(page);
	}
}
