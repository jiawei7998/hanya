package com.singlee.capital.trade.acc.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyAmorVo;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyVo;
import com.singlee.capital.trade.acc.service.AccForTradeAccessService;
import com.singlee.capital.trade.model.TdAdvanceMaturity;

@Controller
@RequestMapping(value = "/AccForTradeAccessController")
public class AccForTradeAccessController extends CommonController {

	@Autowired
	AccForTradeAccessService accForTradeAccessService;
	
	/**
	 * 查询每日计提通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdAccTrdDailyVos")
	public RetMsg<PageInfo<TdAccTrdDailyVo>> getTdAccTrdDailyVos(@RequestBody Map<String,Object> params) {
		String dealNo = ParameterUtil.getString(params, "dealNo",null);
		accForTradeAccessService.getNeedForAccDrawingDatasStepOne(dealNo);
		Page<TdAccTrdDailyVo> page = accForTradeAccessService.getTdAccTrdDailyVos(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询提前还款通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdAdvanceMaturity")
	public RetMsg<PageInfo<TdAdvanceMaturity>> getTdAdvanceMaturity(@RequestBody Map<String,Object> params) {
		String dealNo = ParameterUtil.getString(params, "dealNo",null);
		String refNo = ParameterUtil.getString(params, "refNo",null);
		accForTradeAccessService.getNeedForAccAdvanceMaturity(dealNo, refNo, null);
		Page<TdAdvanceMaturity> page = accForTradeAccessService.getTdAdvanceMaturity(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询逾期计提通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdAccTrdDailyVoss")
	public RetMsg<PageInfo<TdAccTrdDailyVo>> getTdAccTrdDailyVoss(@RequestBody Map<String,Object> params) {
		String dealNo = ParameterUtil.getString(params, "dealNo",null);
		accForTradeAccessService.getNeedForAccDrawingDatesForOverDueStepTow(dealNo);
		Page<TdAccTrdDailyVo> page = accForTradeAccessService.getTdAccTrdDailyVos(params);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 查询每日摊销通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAccTrdDailyAmor")
	public RetMsg<PageInfo<TdAccTrdDailyAmorVo>> getAccTrdDailyAmor(@RequestBody Map<String,Object> params) {
		String dealNo = ParameterUtil.getString(params, "dealNo",null);
		accForTradeAccessService.getNeedForAccDrawingDatesForAmorStepThree(dealNo);
		Page<TdAccTrdDailyAmorVo> page = accForTradeAccessService.getAccTrdDailyAmor(params);
		return RetMsgHelper.ok(page);
	}
	

	/**
	 * 查询交易詳情通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdAccTrdDaily")
	public RetMsg<List<TdAccTrdDaily>> getTdAccTrdDaily(@RequestBody Map<String,Object> params) {
		List<TdAccTrdDaily> page = accForTradeAccessService.getTdAccTrdDaily(params);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 查询自动结息通过单据号
	 * @param params	dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdAccTrdDailyVoInterestSettless")
	public RetMsg<PageInfo<TdAccTrdDailyVo>> getTdAccTrdDailyVoInterestSettless(@RequestBody Map<String,Object> params) {
		String dealNo = ParameterUtil.getString(params, "dealNo",null);
		accForTradeAccessService.getNeedForSettleDatasStepOne(dealNo);
		Page<TdAccTrdDailyVo> page = accForTradeAccessService.getTdAccTrdDailyVos(params);
		return RetMsgHelper.ok(page);
	}
	
}
