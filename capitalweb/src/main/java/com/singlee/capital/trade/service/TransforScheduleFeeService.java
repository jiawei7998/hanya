package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforScheduleFee;

/**
 * @projectName 同业业务管理系统

 * @version 1.0
 */
public interface TransforScheduleFeeService {


	
	public TdTransforScheduleFee getTransforScheduleFeeById(String dealNo);
	
	
	public Page<TdTransforScheduleFee> getTransforScheduleFeePage (Map<String, Object> params, int isFinished);
	
	public void createTransforScheduleFee(Map<String, Object> params);
	
	
	public void updateTransforScheduleFee(Map<String, Object> params);
	
	public void deleteTransforScheduleFee(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	
	public double getAmIntCalForTrasforInterest(Map<String, Object> map);
	
}
