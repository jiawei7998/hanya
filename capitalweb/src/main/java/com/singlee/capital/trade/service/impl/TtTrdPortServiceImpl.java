package com.singlee.capital.trade.service.impl;

import java.util.Map;



import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.trade.mapper.TtTrdPortMapper;
import com.singlee.capital.trade.model.TtTrdPort;
import com.singlee.capital.trade.service.TtTrdPortService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TtTrdPortServiceImpl implements TtTrdPortService{
	@Autowired
	private TtTrdPortMapper trdPortMapper;
	private static Logger log = LoggerFactory.getLogger("TRADE");
	/**
	 * 查询投资组合列表
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	@Override
	public Page<TtTrdPort> searchTtTrdPort(Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		Page<TtTrdPort> params = trdPortMapper.searchTtTrdPort(param, rowBounds);
		return params;
	}

	/**
	 * 新增投资组合
	 * @param ttTrdPort
	 */
	@Override
	public String insertTtTrdPort(TtTrdPort ttTrdPort) {
		int t = trdPortMapper.selectTtTrdPortById(ttTrdPort.getPortfolio());
		if(t != 0){
			log.info("投资组合代码("+ttTrdPort.getPortfolio()+")已经存在");
			return "投资组合代码("+ttTrdPort.getPortfolio()+")已经存在";
		}else{
			String nowDate=DayendDateServiceProxy.getInstance().getSettlementDateTime();
			ttTrdPort.setLstmntdte(nowDate);
			trdPortMapper.insertTtTrdPort(ttTrdPort);
			return "000";
		}
		
	}

	/**
	 * 修改投资组合
	 * 
	 * 修改的保存后的时间为系统时间
	 */
	@Override
	public void updateTtTrdPort(TtTrdPort ttTrdPort) {
		String nowDate=DayendDateServiceProxy.getInstance().getSettlementDateTime();
		ttTrdPort.setLstmntdte(nowDate);
		trdPortMapper.updateTtTrdPort(ttTrdPort);
	}

	/**
	 * 删除投资组合
	 */
	@Override
	public void deleteTtTrdPortById(String[] portfolio) {
		for (int i = 0; i < portfolio.length; i++) {
			trdPortMapper.deleteTtTrdPortById(portfolio[i]);
		}
	}

}
