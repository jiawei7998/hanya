package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdAssetLcMapper;
import com.singlee.capital.trade.model.TdAssetLc;
import com.singlee.capital.trade.service.TdAssetLCService;

/**
 * @projectName 同业业务管理系统
 * @className 信用证资产服务实现
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdAssetLCServiceImpl implements TdAssetLCService {
	
	@Autowired
	public TdAssetLcMapper tdAssetLcMapper;

	@Override
	public List<TdAssetLc> getAssetLcList(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.tdAssetLcMapper.getTdAssetLcList(params);
	}

	@Override
	public Page<TdAssetLc> getAssetLcLists(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.tdAssetLcMapper.getTdAssetLcLists(params, ParameterUtil.getRowBounds(params));
	}
	
}
