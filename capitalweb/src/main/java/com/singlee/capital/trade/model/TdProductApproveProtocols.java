package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="TD_PRODUCT_APPROVE_PROTOCOLS")
public class TdProductApproveProtocols implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;
	@Id
	private int version;
	@Id
	private String  protocolId;
	private String  protocolRv;
	private int  protocolNum;
	private String  officeSeal;
	private String  personalSeal;
	private String  pagingSeal;
	private String  oterSeal;
	private int  seqNumber;
	private String protocolType;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getProtocolId() {
		return protocolId;
	}
	public void setProtocolId(String protocolId) {
		this.protocolId = protocolId;
	}
	public String getProtocolRv() {
		return protocolRv;
	}
	public void setProtocolRv(String protocolRv) {
		this.protocolRv = protocolRv;
	}
	public int getProtocolNum() {
		return protocolNum;
	}
	public void setProtocolNum(int protocolNum) {
		this.protocolNum = protocolNum;
	}
	public String getOfficeSeal() {
		return officeSeal;
	}
	public void setOfficeSeal(String officeSeal) {
		this.officeSeal = officeSeal;
	}
	public String getPersonalSeal() {
		return personalSeal;
	}
	public void setPersonalSeal(String personalSeal) {
		this.personalSeal = personalSeal;
	}
	public String getPagingSeal() {
		return pagingSeal;
	}
	public void setPagingSeal(String pagingSeal) {
		this.pagingSeal = pagingSeal;
	}
	public String getOterSeal() {
		return oterSeal;
	}
	public void setOterSeal(String oterSeal) {
		this.oterSeal = oterSeal;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getProtocolType() {
		return protocolType;
	}
	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}
	@Override
	public String toString() {
		return "TdProductApproveProtocols [dealNo=" + dealNo + ", version="
				+ version + ", protocolId=" + protocolId + ", protocolRv="
				+ protocolRv + ", protocolNum=" + protocolNum + ", officeSeal="
				+ officeSeal + ", personalSeal=" + personalSeal
				+ ", pagingSeal=" + pagingSeal + ", oterSeal=" + oterSeal
				+ ", seqNumber=" + seqNumber + ", protocolType=" + protocolType
				+ "]";
	}
	
}
