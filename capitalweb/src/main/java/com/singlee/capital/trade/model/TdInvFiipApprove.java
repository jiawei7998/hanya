package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * 投资理财 投资占比审批维护表
 * @version 1.0
 */
@Entity
@Table(name = "TD_INV_FIIP_APPROVE")
public class TdInvFiipApprove implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	private int version;
	
	private double plChange;
	private double cashFloor;
	private double cashUp;
	private double cgFloor;
	private double cgUp;
	private double peoplebocFloor;
	private double peoplebocUp;
	private double policybocFloor;
	private double policybocUp;
	private double lgocFloor;
	private double lgocUp;
	private double psecFloor;
	private double psecUp;
	private double cbicFloor;
	private double cbicUp;
	private String isSd;
	private double accgFloor;
	private double accgUp;
	private String isBondst;
	private double otherfiFloor;
	private double otherfiUp;
	private double geFloor;
	private double geUp;
	private double other;
	private String postdate;
	private int quoarter;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public double getPlChange() {
		return plChange;
	}
	public void setPlChange(double plChange) {
		this.plChange = plChange;
	}
	
	public double getCashFloor() {
		return cashFloor;
	}
	public void setCashFloor(double cashFloor) {
		this.cashFloor = cashFloor;
	}
	public double getCashUp() {
		return cashUp;
	}
	public void setCashUp(double cashUp) {
		this.cashUp = cashUp;
	}
	public double getCgFloor() {
		return cgFloor;
	}
	public void setCgFloor(double cgFloor) {
		this.cgFloor = cgFloor;
	}
	public double getCgUp() {
		return cgUp;
	}
	public void setCgUp(double cgUp) {
		this.cgUp = cgUp;
	}
	public double getPeoplebocFloor() {
		return peoplebocFloor;
	}
	public void setPeoplebocFloor(double peoplebocFloor) {
		this.peoplebocFloor = peoplebocFloor;
	}
	public double getPeoplebocUp() {
		return peoplebocUp;
	}
	public void setPeoplebocUp(double peoplebocUp) {
		this.peoplebocUp = peoplebocUp;
	}
	public double getPolicybocFloor() {
		return policybocFloor;
	}
	public void setPolicybocFloor(double policybocFloor) {
		this.policybocFloor = policybocFloor;
	}
	public double getPolicybocUp() {
		return policybocUp;
	}
	public void setPolicybocUp(double policybocUp) {
		this.policybocUp = policybocUp;
	}
	public double getLgocFloor() {
		return lgocFloor;
	}
	public void setLgocFloor(double lgocFloor) {
		this.lgocFloor = lgocFloor;
	}
	public double getLgocUp() {
		return lgocUp;
	}
	public void setLgocUp(double lgocUp) {
		this.lgocUp = lgocUp;
	}
	public double getPsecFloor() {
		return psecFloor;
	}
	public void setPsecFloor(double psecFloor) {
		this.psecFloor = psecFloor;
	}
	public double getPsecUp() {
		return psecUp;
	}
	public void setPsecUp(double psecUp) {
		this.psecUp = psecUp;
	}
	public double getCbicFloor() {
		return cbicFloor;
	}
	public void setCbicFloor(double cbicFloor) {
		this.cbicFloor = cbicFloor;
	}
	public double getCbicUp() {
		return cbicUp;
	}
	public void setCbicUp(double cbicUp) {
		this.cbicUp = cbicUp;
	}
	public String getIsSd() {
		return isSd;
	}
	public void setIsSd(String isSd) {
		this.isSd = isSd;
	}
	public double getAccgFloor() {
		return accgFloor;
	}
	public void setAccgFloor(double accgFloor) {
		this.accgFloor = accgFloor;
	}
	public double getAccgUp() {
		return accgUp;
	}
	public void setAccgUp(double accgUp) {
		this.accgUp = accgUp;
	}
	public String getIsBondst() {
		return isBondst;
	}
	public void setIsBondst(String isBondst) {
		this.isBondst = isBondst;
	}
	public double getOtherfiFloor() {
		return otherfiFloor;
	}
	public void setOtherfiFloor(double otherfiFloor) {
		this.otherfiFloor = otherfiFloor;
	}
	public double getOtherfiUp() {
		return otherfiUp;
	}
	public void setOtherfiUp(double otherfiUp) {
		this.otherfiUp = otherfiUp;
	}
	public double getGeFloor() {
		return geFloor;
	}
	public void setGeFloor(double geFloor) {
		this.geFloor = geFloor;
	}
	public double getGeUp() {
		return geUp;
	}
	public void setGeUp(double geUp) {
		this.geUp = geUp;
	}
	public double getOther() {
		return other;
	}
	public void setOther(double other) {
		this.other = other;
	}
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public int getQuoarter() {
		return quoarter;
	}
	public void setQuoarter(int quoarter) {
		this.quoarter = quoarter;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
	
	
	
}
