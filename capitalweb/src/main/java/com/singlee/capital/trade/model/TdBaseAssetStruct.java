package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TD_BASE_ASSET_STRUCT")
public class TdBaseAssetStruct implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2166681784656456509L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	private String seqNo;
	
	/*****************基础资产列表********************/
	private String aDate;//股东性质
	private String ccy;//币种
	private double amt;//金额
	private double banlance;//余额
	private String rateType;//利率类型
	private String rateCode;//利率代码
	private double rateDiff;//利差
	private String acturalRate;//利率
	private String vDate;//起息日
	private String tDate;//划款日
	private String mDate;//到期日
	private String meDate;//到账日
	private String sDate;//首次付息日
	private String eDate;//最后到期日
	private int term;//期限
	private String basis;//基准利率
	private double mInt;//到期利率
	private double mAmt;//到期金额
	private String intFre;//付息频率
	private String intType;//首席类型
	private double intAmt;//到期金额
	private String cifNo;//客户号
	private String assetCname;//客户名称
	private String assetSno;//
	private String assetCustLocate;//所属地区
	private String assetInnerCust;//是否行内客户
	private String indusCat;//行业类别
	private String indusCatBig;//行业大类
	private String indusCatMid;//行业中类
	private String indusCatSmall;//行业小类
	private String holdingType;//持股类型
	private String companySize;//公司规模
	private double regCapital;//注册资本
	private String innerLevel;//内部评价
	private String outerLevel;//公开评级
	private String financingUse;//投资用途
	private String isPlat;//是否平台
	private String assetType;//基础资产类型
	private String assetTarget;//标的物
	private String secId    ;                                 // 债券编号 
	private String secName; // 债券名称
	private String stockId    ;                               // 股票代码 
	private double warnLinePrice;//预警线
	private double coverLinePrice ;                           // 补仓线                      
	private double openLinePrice  ;                           //平仓线 
	private String lmtNo;//额度编号
	private String remark;//基础资产增信描述
	
	public String getSecName() {
		return secName;
	}
	public void setSecName(String secName) {
		this.secName = secName;
	}
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public double getWarnLinePrice() {
		return warnLinePrice;
	}
	public void setWarnLinePrice(double warnLinePrice) {
		this.warnLinePrice = warnLinePrice;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getBanlance() {
		return banlance;
	}
	public void setBanlance(double banlance) {
		this.banlance = banlance;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public double getRateDiff() {
		return rateDiff;
	}
	public void setRateDiff(double rateDiff) {
		this.rateDiff = rateDiff;
	}
	public String getActuralRate() {
		return acturalRate;
	}
	public void setActuralRate(String acturalRate) {
		this.acturalRate = acturalRate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String gettDate() {
		return tDate;
	}
	public void settDate(String tDate) {
		this.tDate = tDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getMeDate() {
		return meDate;
	}
	public void setMeDate(String meDate) {
		this.meDate = meDate;
	}
	public String getsDate() {
		return sDate;
	}
	public void setsDate(String sDate) {
		this.sDate = sDate;
	}
	public String geteDate() {
		return eDate;
	}
	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	public int getTerm() {
		return term;
	}
	public void setTerm(int term) {
		this.term = term;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public double getmInt() {
		return mInt;
	}
	public void setmInt(double mInt) {
		this.mInt = mInt;
	}
	public double getmAmt() {
		return mAmt;
	}
	public void setmAmt(double mAmt) {
		this.mAmt = mAmt;
	}
	public String getIntFre() {
		return intFre;
	}
	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}
	public String getIntType() {
		return intType;
	}
	public void setIntType(String intType) {
		this.intType = intType;
	}
	public double getIntAmt() {
		return intAmt;
	}
	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getAssetTarget() {
		return assetTarget;
	}
	public void setAssetTarget(String assetTarget) {
		this.assetTarget = assetTarget;
	}
	
	public String getAssetCname() {
		return assetCname;
	}
	public void setAssetCname(String assetCname) {
		this.assetCname = assetCname;
	}
	public String getAssetSno() {
		return assetSno;
	}
	public void setAssetSno(String assetSno) {
		this.assetSno = assetSno;
	}
	public String getAssetCustLocate() {
		return assetCustLocate;
	}
	public void setAssetCustLocate(String assetCustLocate) {
		this.assetCustLocate = assetCustLocate;
	}
	public String getAssetInnerCust() {
		return assetInnerCust;
	}
	public void setAssetInnerCust(String assetInnerCust) {
		this.assetInnerCust = assetInnerCust;
	}
	public String getIndusCat() {
		return indusCat;
	}
	public void setIndusCat(String indusCat) {
		this.indusCat = indusCat;
	}
	public String getIndusCatBig() {
		return indusCatBig;
	}
	public void setIndusCatBig(String indusCatBig) {
		this.indusCatBig = indusCatBig;
	}
	public String getIndusCatMid() {
		return indusCatMid;
	}
	public void setIndusCatMid(String indusCatMid) {
		this.indusCatMid = indusCatMid;
	}
	public String getIndusCatSmall() {
		return indusCatSmall;
	}
	public void setIndusCatSmall(String indusCatSmall) {
		this.indusCatSmall = indusCatSmall;
	}
	public String getHoldingType() {
		return holdingType;
	}
	public void setHoldingType(String holdingType) {
		this.holdingType = holdingType;
	}
	public String getCompanySize() {
		return companySize;
	}
	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}
	public double getRegCapital() {
		return regCapital;
	}
	public void setRegCapital(double regCapital) {
		this.regCapital = regCapital;
	}
	public String getInnerLevel() {
		return innerLevel;
	}
	public void setInnerLevel(String innerLevel) {
		this.innerLevel = innerLevel;
	}
	public String getOuterLevel() {
		return outerLevel;
	}
	public void setOuterLevel(String outerLevel) {
		this.outerLevel = outerLevel;
	}
	public String getFinancingUse() {
		return financingUse;
	}
	public void setFinancingUse(String financingUse) {
		this.financingUse = financingUse;
	}
	public String getIsPlat() {
		return isPlat;
	}
	public void setIsPlat(String isPlat) {
		this.isPlat = isPlat;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCifNo() {
		return cifNo;
	}
	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}
	public String getLmtNo() {
		return lmtNo;
	}
	public void setLmtNo(String lmtNo) {
		this.lmtNo = lmtNo;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getStockId() {
		return stockId;
	}
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}
	public double getCoverLinePrice() {
		return coverLinePrice;
	}
	public void setCoverLinePrice(double coverLinePrice) {
		this.coverLinePrice = coverLinePrice;
	}
	public double getOpenLinePrice() {
		return openLinePrice;
	}
	public void setOpenLinePrice(double openLinePrice) {
		this.openLinePrice = openLinePrice;
	}
	
	
	
}
