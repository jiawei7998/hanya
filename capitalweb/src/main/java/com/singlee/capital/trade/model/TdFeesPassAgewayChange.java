package com.singlee.capital.trade.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


@Entity
@Table(name = "TD_FEES_PASSAGEWAY_CHANGE")
public class TdFeesPassAgewayChange {
	
	@Id
	private String  dealNo               ;   
	private String  refNo                ;
	private String cfType;
	private String dealType;
	private String sponsor;
	private String sponInst;
	private String changedate;
	private String changereason;
	private String effectDate;
	private String lastUpdate;
	private String remainAmt;	
	private String  version               ;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private String approveStatus;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	private String aDate;
	

	@Transient
	private List<TdFeesPassAgewayChange> tdFeesPassAgeways;
	
	
	public List<TdFeesPassAgewayChange> getTdFeesPassAgeways() {
		return tdFeesPassAgeways;
	}

	public void setTdFeesPassAgeways(List<TdFeesPassAgewayChange> tdFeesPassAgeways) {
		this.tdFeesPassAgeways = tdFeesPassAgeways;
	}

	public TcProduct getProduct() {
		return product;
	}

	public void setProduct(TcProduct product) {
		this.product = product;
	}

	public TtCounterParty getParty() {
		return party;
	}

	public void setParty(TtCounterParty party) {
		this.party = party;
	}


	public TaUser getUser() {
		return user;
	}

	public void setUser(TaUser user) {
		this.user = user;
	}

	public TtInstitution getInstitution() {
		return institution;
	}

	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}


	public TdFeesPassAgewayChange() {
		super();
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	
	public String getCfType() {
		return cfType;
	}

	public void setCfType(String cfType) {
		this.cfType = cfType;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}


	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getRemainAmt() {
		return remainAmt;
	}

	public void setRemainAmt(String remainAmt) {
		this.remainAmt = remainAmt;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getEffectDate() {
		return effectDate;
	}

	public void setEffectDate(String effectDate) {
		this.effectDate = effectDate;
	}

	public String getChangereason() {
		return changereason;
	}

	public void setChangereason(String changereason) {
		this.changereason = changereason;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public String getChangedate() {
		return changedate;
	}

	public void setChangedate(String changedate) {
		this.changedate = changedate;
	}

}
