package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdRateChange;

/**
 * @projectName 同业业务管理系统
 * @className 利率变更服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface RateChangeService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 利率变更主键
	 * @return 利率变更对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdRateChange getRateChangeById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdRateChange> getRateChangePage (Map<String, Object> params, int isFinish);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createRateChange(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateRateChange(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteRateChange(String[] dealNos);
	
	/**
	 * 查询出交易所带的所有利率变更交易
	 * @param params
	 * @return
	 */
	public Page<TdRateChange> getRateChangeForDealNoPage (Map<String, Object> params);
	
}
