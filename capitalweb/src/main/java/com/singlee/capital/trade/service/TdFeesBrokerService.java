package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;
import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesBroker;

public interface TdFeesBrokerService {
	
	/**
	 * 查询中介费信息
	 */
	public Page<TdFeesBroker> pageFeesBrokerList(Map<String ,Object> map);
	
	/**
	 * 导出
	 */
	public byte[] exportRep(Map<String, Object> map) throws Exception;

	public Page<TdFeesBroker> getFeesBrokerByDealNo(Map<String, Object> params);
	
	public List<TdFeesBroker> getFeesBrokerListByDealNo(Map<String, Object> params);
	
	public void updateFees(Map<String, Object> map);

}
