package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdOutrightSale;

/**
 * @projectName 同业业务管理系统
 * @className 交易单笔卖断持久层
 * @version 1.0
 */
public interface TdOutrightSaleMapper extends Mapper<TdOutrightSale> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 交易单笔卖断对象
	 */
	public TdOutrightSale getTdOutrightSaleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易单笔卖断对象列表
	 */
	public Page<TdOutrightSale> getTdOutrightSaleList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易单笔卖断对象列表
	 */
	public Page<TdOutrightSale> getTdOutrightSaleListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易单笔卖断对象列表
	 */
	public Page<TdOutrightSale> getTdOutrightSaleListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param tradeExtend
	 * @return
	 */
	public TdOutrightSale searchTdOutrightSale(TdOutrightSale outrightSale);

	public TdOutrightSale getTdOutrightSaleApproved(Map<String, Object> params);
	
	public void updateTdOutrightSaleActAmt(Map<String, Object> params);
	
	public void updateTdOutrightSaleActInt(Map<String, Object> params);
}