package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;

/**
 * 成本中心
 * @author Administrator
 *
 */
@Entity
@Table(name = "TT_TRD_COST")
public class TtTrdCost implements Serializable , TreeInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//成本中心子节点
	@Id
	private String costcent;
	//描述字段名称
	private String costdesc;
	//成本中心父节点
	private String parentcostcent;
	//时间日期
	private String lstmntdte;
	//预留字段1
	private String remark1;
	//预留字段2
	private String remark2;
	//预留字段3
	private String remark3;
	/**
	 * 这里定义的List<TaModule> children，需与TreeUtil工具类中的TreeModuleBo对象中children属性
	 */
	@Transient
	private List<TtTrdCost> children;
	
	/**
	 * @param tm
	 */
	public void addChildrenList(TtTrdCost ttTrdCost){
		if(children==null){
			children = new ArrayList<TtTrdCost>();
		}
		children.add(ttTrdCost);
	}
	public List<TtTrdCost> getChildren() {
		return children;
	}
	public void setChildren(List<TtTrdCost> l) {
		children = l;
	}
	
	public TtTrdCost(){
		
	}
	
	public String getCostcent() {
		return costcent;
	}
	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}
	public String getCostdesc() {
		return costdesc;
	}
	public void setCostdesc(String costdesc) {
		this.costdesc = costdesc;
	}
	public String getParentcostcent() {
		return parentcostcent;
	}
	public void setParentcostcent(String parentcostcent) {
		this.parentcostcent = parentcostcent;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getRemark3() {
		return remark3;
	}
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	@Override
	public String getId() {
		return costcent;
	}
	@Override
	public String getParentId() {
		return parentcostcent;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TtTrdCost>) list);
	}
	@Override
	public List<? extends TreeInterface> children() {
		return (List<? extends TreeInterface>) getChildren();
	}
	@Override
	public String getText() {
		return costdesc;
	}
	@Override
	public String getValue() {
		return costcent;
	}
	@Override
	public int getSort() {
		return 0;
	}
	@Override
	public String getIconCls() {
		return null;
	}
	
	@Override
	public String toString() {
		return String
				.format("TaUser [costcent=%s, costdesc=%s, parentcostcent=%s, lstmntdte=%s, remark1=%s, remark2=%s, remark3=%s]",
						costcent, costdesc, parentcostcent, lstmntdte, remark1,
						remark2, remark3);
	}
}
