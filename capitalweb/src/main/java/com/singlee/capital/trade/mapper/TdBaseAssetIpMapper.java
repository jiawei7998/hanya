package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAssetIp;

public interface TdBaseAssetIpMapper extends Mapper<TdBaseAssetIp>{

	public Page<TdBaseAssetIp> getTdBaseAssetIps(Map<String, Object> params, RowBounds rb);
	
}
