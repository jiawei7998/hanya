package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdTrdEvent;

public interface TrdEventService {

	List<TdTrdEvent> searchTrdEventByKey(Map<String, Object> params);
	/**
	 * 根据交易流水获得改交易项下的存续期活动
	 * @param params
	 * @return
	 */
	List<TdTrdEvent> getTrdEventForDealNo(Map<String, Object> params);
}
