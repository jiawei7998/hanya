package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TD_PRODUCT_APPROVE_AMTLINE")
public class TdProductApproveAmtLine implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6667696630412505459L;
	
	@Id
	private int  prd_no;//产品编号
	private String ccy;
	private double amt_line;
	
	
	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public int getPrdNo() {
		return prd_no;
	}

	public void setPrdNo(int prdNo) {
		this.prd_no = prdNo;
	}

	public double getAmtLine() {
		return amt_line;
	}

	public void setAmtLine(double amtLine) {
		this.amt_line = amtLine;
	}
	
}	 