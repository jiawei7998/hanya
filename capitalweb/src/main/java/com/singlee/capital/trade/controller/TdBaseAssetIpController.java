package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdBaseAssetIp;
import com.singlee.capital.trade.service.TdBaseAssetIpService;
/**
 * 投资占比
 * @author SINGLEE
 *
 */
@Controller
@RequestMapping("/TdBaseAssetIpController")
public class TdBaseAssetIpController extends CommonController{
	@Autowired
	private TdBaseAssetIpService baseAssetIpService;
	@ResponseBody
	@RequestMapping(value = "/getTdBaseAssetIpByDealno")
	public RetMsg<PageInfo<TdBaseAssetIp>> getTdBaseAssetIpByDealno(@RequestBody Map<String,Object> params){
		
		Page<TdBaseAssetIp> baseAssets = this.baseAssetIpService.getBaseAssetIps(params);
		
		return RetMsgHelper.ok(baseAssets);
	}
}
