package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdBaseRiskMapper;
import com.singlee.capital.trade.model.TdBaseRisk;
import com.singlee.capital.trade.service.BaseRiskService;

/**
 * @projectName 同业业务管理系统
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseRiskServiceImpl implements BaseRiskService {
	
	@Autowired
	public TdBaseRiskMapper tdBaseRiskMapper;
	
	@Override
	public List<TdBaseRisk> getBaseRiskList(Map<String, Object> params) {
		return tdBaseRiskMapper.getBaseRiskList(params);
	}

	@Override
	public Page<TdBaseRisk> getBaseRiskLists(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdBaseRisk> param = tdBaseRiskMapper.getBaseRiskLists(params, rowBounds);
		return param;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
