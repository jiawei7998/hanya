package com.singlee.capital.trade.mapper;

import java.util.Map;




import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAssetGuarty;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:52:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdBaseAssetGuartyMapper extends Mapper<TdBaseAssetGuarty> {
	
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 * 
	 * @param dealNo - 交易单号
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public void deleteBaseAssetGuartyByDealNo(String dealNo);
	/**
	 * 插入产品审批子表记录
	 * 
	 * @param params
	 */
	public void insertBaseAsseetGuarty(TdBaseAssetGuarty assetGuarty);
	/**
	 * 分页查询产品审批子表记录
	 * 
	 * @param params
	 */
	public Page<TdBaseAssetGuarty> getTdBaseAssetGuartyList(Map<String,Object> map);
}