package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdCost;

public interface TtTrdCostService {
	/**
	 * 查询成本中心列表
	 * @return
	 */
	public List<TtTrdCost> selectTtTrdCost(Map <String, Object> param);
	
	/**
	 * 查询成本中心列表分页
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public List<TtTrdCost> searchTtTrdCost(Map<String, Object> param);
	/**
	 * 根据Id 查询成本中心数据
	 * @param param
	 * @return
	 */
	public TtTrdCost selectTtTrdCostById(Map<String, Object> param);
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 */
	//public List<TtTrdCost> childrenInstitutionId(String costcent);
	
	/**
	 * 修改成本中心列表
	 * @param ttTrdCost
	 */
	public void updateTtTrdCost(TtTrdCost ttTrdCost);
	
	/**
	 * 根据costcent子节点成本中心列表
	 * @param costcent
	 */
	public void deleteTtTrdCost(String[] costcent);
	
	/**
	 * 添加成本中心列表
	 * @param ttTrdCost
	 */
	public String insertTtTrdCost(TtTrdCost ttTrdCost);
	/**
	 * 从投资组合查询成本中心
	 * @param ttTrdCost
	 */
	public Page<TtTrdCost> searchCost(Map<String, Object> param);
	
	/**
	 * 获取全部成本中心
	 */
	public Page<TtTrdCost> searchAllCost(Map<String, Object> param);
	

	/**
	 * 获取全部成本中心，除去-1
	 */
	public Page<TtTrdCost> searchAllCostExcept(Map<String, Object> param);
}
