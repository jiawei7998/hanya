package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdProductFeeCal;

public interface TdProductFeeCalMapper extends Mapper<TdProductFeeCal>{
	
	public List<TdProductFeeCal> getProductFeeCalsByPrdNo(Map<String, Object> paramsMap);
}
