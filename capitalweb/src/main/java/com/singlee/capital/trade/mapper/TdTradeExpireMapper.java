package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdTradeExpire;

/**
 * @projectName 同业业务管理系统
 * @className 资产到期持久层
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdTradeExpireMapper extends Mapper<TdTradeExpire> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 资产到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTradeExpire getTradeExpireById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeExpire> getTradeExpireList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeExpire> getTradeExpireListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产到期对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdTradeExpire> getTradeExpireListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param tradeExpire
	 * @return
	 */
	public TdTradeExpire searchTradeExpire(TdTradeExpire tradeExpire);
	/**
	 * 更新主表数据
	 * @param params
	 */
	public void updateProductApproveMain(Map<String, Object> params);
	
}