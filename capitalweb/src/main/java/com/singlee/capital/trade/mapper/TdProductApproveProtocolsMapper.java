package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdProductApproveProtocols;

public interface TdProductApproveProtocolsMapper extends Mapper<TdProductApproveProtocols>{
	/**
	 * 审批结束后进行审批单与正式申请单拷贝
	 * @param params
	 */
	public void insertProductApproveProtocols(Map<String, Object> params);
	public void deleteProductApproveProtocolsByDealNo(String dealNo);
	public List<TdProductApproveProtocols> getProductApproveProtocolsByDealNo(String dealNo);
}
