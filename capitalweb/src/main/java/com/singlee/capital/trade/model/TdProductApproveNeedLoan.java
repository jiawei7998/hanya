package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TD_PRODUCT_APPROVE_NEEDLOAN")
public class TdProductApproveNeedLoan implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7464825601678792451L;
	
	@Id
	private Integer prd_no;//产品编号

	public Integer getPrdNo() {
		return prd_no;
	}

	public void setPrdNo(Integer prdNo) {
		this.prd_no = prdNo;
	}
	
	
}	 