package com.singlee.capital.trade.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TcGuidePriceVo;
import com.singlee.capital.trade.service.TcGuidePriceService;

@Controller
@RequestMapping(value = "/GuidePriceController")
public class TcGuidePriceController extends CommonController{
	@Autowired
	private TcGuidePriceService guidePriceService;
	@ResponseBody
	@RequestMapping(value = "/getGuidePriceVos")
	public RetMsg<List<TcGuidePriceVo>> getGuidePriceVos(@RequestBody Map<String, Object> params) throws RException{
		List<TcGuidePriceVo> guidePriceVos = guidePriceService.getTcGuidePrices();
		return RetMsgHelper.ok(guidePriceVos);
	}
}
