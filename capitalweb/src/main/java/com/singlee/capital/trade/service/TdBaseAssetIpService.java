package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAssetIp;

public interface TdBaseAssetIpService {
	
	public Page<TdBaseAssetIp> getBaseAssetIps(Map<String, Object> params);
}
