package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

@Entity
@Table(name = "TD_TRADE_EXPIRE")
public class TdTradeExpire implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3720610986116412801L;
	
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	
	private double remainAmt;//剩余本金
	
	private String expMDate;//到期日期
	
	private String expMeDate;//到账日期
	
	private int expTerm;//计息天数
	
	private int expOccupTerm;//占款期限

	private double expMInt;//到期利息(元)
	
	private double expMAmt;//到期金额(元)
	
	private double expRate;//持有期利率
	
	/**
	 * 备注
	 */
	private String reason;
	
	/**
	 * 版本号
	 */
	private int version;
	
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	
	public String getExpMDate() {
		return expMDate;
	}
	public void setExpMDate(String expMDate) {
		this.expMDate = expMDate;
	}
	public String getExpMeDate() {
		return expMeDate;
	}
	public void setExpMeDate(String expMeDate) {
		this.expMeDate = expMeDate;
	}
	public int getExpTerm() {
		return expTerm;
	}
	public void setExpTerm(int expTerm) {
		this.expTerm = expTerm;
	}
	public int getExpOccupTerm() {
		return expOccupTerm;
	}
	public void setExpOccupTerm(int expOccupTerm) {
		this.expOccupTerm = expOccupTerm;
	}
	public double getExpMInt() {
		return expMInt;
	}
	public void setExpMInt(double expMInt) {
		this.expMInt = expMInt;
	}
	public double getExpMAmt() {
		return expMAmt;
	}
	public void setExpMAmt(double expMAmt) {
		this.expMAmt = expMAmt;
	}
	
	public double getExpRate() {
		return expRate;
	}
	public void setExpRate(double expRate) {
		this.expRate = expRate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
	
	
}
