package com.singlee.capital.trade.mapper;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdMultiResaleDetail;

/**
 * @projectName 同业业务管理系统
 * @className 资产卖断持久层
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdMulResaleDetailMapper extends Mapper<TdMultiResaleDetail> {
	/**
	 * 根据原流水号删除
	 * @param refNo
	 */
      public void deleteResaleDetailByRefNo(String refNo);
      
      /**
       * 根据原流水号查询
       * @param redNo
       * @param rb
       * @return
       */
      public Page<TdMultiResaleDetail> getResaleDetailList(String refNo, RowBounds rb);
      
      /**
       * 根据原流水号查询列表
       * @param refNo
       * @return
       */
      public List<TdMultiResaleDetail> getResaleDetailListByRefNo(String refNo);
}