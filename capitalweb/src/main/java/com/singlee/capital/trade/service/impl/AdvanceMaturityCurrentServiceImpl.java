package com.singlee.capital.trade.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityCurrentMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.ProductApproveService;

/**
 * @className 提前到期
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AdvanceMaturityCurrentServiceImpl extends AbstractDurationService implements AdvanceMaturityCurrentService {

	@Autowired
	private TdAdvanceMaturityCurrentMapper advanceMaturityCurrentMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	
	@Autowired 
	private TdRateRangeMapper rateRangeMapper;
	@Autowired
	private DayendDateService dateService;
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;
	@Override
	public TdAdvanceMaturityCurrent getAdvanceMaturityCurrentById(String dealNo) {
		return advanceMaturityCurrentMapper.getAdvanceMaturityCurrentById(dealNo);
	}

	@Override
	public Page<TdAdvanceMaturityCurrent> getAdvanceMaturityCurrentPage(
			Map<String, Object> params, int isFinished) {
		Page<TdAdvanceMaturityCurrent> page= new Page<TdAdvanceMaturityCurrent>();
		if(isFinished == 1){
			page=advanceMaturityCurrentMapper.getAdvanceMaturityCurrentList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=advanceMaturityCurrentMapper.getAdvanceMaturityCurrentListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {
			page=advanceMaturityCurrentMapper.getAdvanceMaturityCurrentListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		List<TdAdvanceMaturityCurrent> list =page.getResult();
		for(TdAdvanceMaturityCurrent tm :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(tm.getRefNo());
			if(main!=null){
				tm.setProduct(main.getProduct());
				tm.setParty(main.getCounterParty());
				tm.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建提前到期金融工具表")
	public void createAdvanceMaturityCurrent(Map<String, Object> params) {
		advanceMaturityCurrentMapper.insert(advanceMaturityCurrent(params));
	}

	@Override
	@AutoLogMethod(value = "修改提前到期金融工具表")
	public void updateAdvanceMaturityCurrent(Map<String, Object> params) {
		advanceMaturityCurrentMapper.updateByPrimaryKey(advanceMaturityCurrent(params));
	}

	
	private TdAdvanceMaturityCurrent advanceMaturityCurrent(Map<String, Object> params){
		// map转实体类
		TdAdvanceMaturityCurrent advanceMaturityCurrent = new TdAdvanceMaturityCurrent();
		try {
			BeanUtil.populate(advanceMaturityCurrent, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
        if(advanceMaturityCurrent.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		advanceMaturityCurrent.setVersion(productApproveMain.getVersion());
        }
    	advanceMaturityCurrent.setSponsor(SlSessionHelper.getUserId());
    	advanceMaturityCurrent.setSponInst(SlSessionHelper.getInstitutionId());
		advanceMaturityCurrent.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		advanceMaturityCurrent.setCfType(DictConstants.TrdType.CustomAdvanceMaturityCurrent);
		return advanceMaturityCurrent;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteAdvanceMaturityCurrent(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			advanceMaturityCurrentMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	@Override
	public TdAdvanceMaturityCurrent ifExistDuration(Map<String, Object> map) {
		return advanceMaturityCurrentMapper.ifExistDuration(map);
	}

	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 提起到期核实单号
	 */
	@Override
	public void dealVersionAndCashFlow(String trade_id) {}

	/**
	 * 计算获取应计利息
	 */
	@Override
	public double getAmInt(Map<String, Object> map) {
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(ParameterUtil.getString(map, "refNo", ""))){
        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(map, "refNo", "")));
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double amInt = 0.00;
		String postdate = dateService.getDayendDate();
		/**
		 *	如果是先收息，则查找摊销计划表
		 *  后收息则计算计提差额
		 */
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeBefore))
		{
			map.put("startDate", postdate);
			map.put("endDate", ParameterUtil.getString(map, "amDate", postdate));
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("version", productApproveMain.getVersion());
			
			//替換為后收息的模式20171029
			//List<CashflowAmor> cashflowAmors = cashflowAmorMapper.pageDailyCashflowAmorList(map, new RowBounds(0,99999999)).getResult();
			List<CashflowDailyInterest> cashflowAmors= cashflowDailyInterestMapper.pageDailyInterestList(map, new RowBounds(0,99999999)).getResult();
			for(CashflowDailyInterest cashflowAmor:cashflowAmors) {
				amInt = PlaningTools.add(amInt, cashflowAmor.getInterest());
			}
		}else{
			TdAdvanceMaturityCurrent advanceMaturityCurrent = new TdAdvanceMaturityCurrent();
			BeanUtil.populate(advanceMaturityCurrent, map);
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();
			List<TdRateRange> rateRanges = rateRangeMapper.getRateRanges(map);
			InterestRange interestRange = null;
			for(TdRateRange rateRange : rateRanges){
				interestRange = new InterestRange();
				interestRange.setStartDate(rateRange.getBeginDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(rateRange.getExecRate());
				interestRanges.add(interestRange);
			}
			
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
			PrincipalInterval principalInterval = new PrincipalInterval();
			
			principalInterval.setStartDate(postdate);
			principalInterval.setEndDate(ParameterUtil.getString(map, "mDate", advanceMaturityCurrent.getAmDate()));
			principalInterval.setResidual_Principal(ParameterUtil.getDouble(map, "remainAmt", advanceMaturityCurrent.getAmAmt()));
			principalIntervals.add(principalInterval);
		
		try {
			amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, postdate, advanceMaturityCurrent.getAmDate(), postdate, 
					DayCountBasis.equal(productApproveMain.getBasis()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			JY.require(true,"计算区间差额计提出错"+e.getMessage());
		}   
		}
		return amInt;
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdAdvanceMaturityCurrent advanceMaturityCurrent = new TdAdvanceMaturityCurrent();
		BeanUtil.populate(advanceMaturityCurrent, params);
		advanceMaturityCurrent = advanceMaturityCurrentMapper.searchAdvanceMaturityCurrent(advanceMaturityCurrent);
		return productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdAdvanceMaturityCurrent advanceMaturityCurrent = new TdAdvanceMaturityCurrent();
		BeanUtil.populate(advanceMaturityCurrent, params);
		advanceMaturityCurrent = advanceMaturityCurrentMapper.searchAdvanceMaturityCurrent(advanceMaturityCurrent);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(advanceMaturityCurrent.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(advanceMaturityCurrent.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(advanceMaturityCurrent.getDealNo());//将活期提前支取的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomAdvanceMaturityCurrent);//活期提前支取
		tdTrdEvent.setEventTable("TD_ADVANCE_MaturityCurrent");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdAdvanceMaturityCurrent advanceMaturityCurrent = new TdAdvanceMaturityCurrent();
		BeanUtil.populate(advanceMaturityCurrent, params);
		/**
		 * 根据提前到期的交易数据进行数据清分
		 */
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(advanceMaturityCurrent.getRefNo());
		JY.require(productApproveMain!=null, "原交易流水不存在！异常！");
		
		
	}
	
	@Override
	public Page<TdAdvanceMaturityCurrent> getAdvanceMaturityCurrentForDealNoPage(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.advanceMaturityCurrentMapper.getAllAdvanceMaturityCurrentsByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}
}
