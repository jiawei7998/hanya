package com.singlee.capital.trade.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TM_FEES_PASSAGEWY")
public class TmFeesPassagewy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_PASSAGEWAY.NEXTVAL FROM DUAL")
	private String  passId;
	private String  dealNo               ;   
	private String  refNo                ; 
	private String  feeType              ;//
	private String  feeSituation         ;  
	private String  intFre               ; //
	private String  cashtDate            ;//
	private double  feeRate              ; 
	private String  remark                ; //
	private String  feeCcy               ;   
	private String  feeBasis             ;   
	private String  passagewayCno        ;   
	private String  passagewayName       ; 
	private String  passagewayBankacccode;//   
	private String  passagewayBankaccname; //  
	private String  passagewayBankcode   ;  // 
	private String  passagewayBankname   ;//
	private String  contact        		;  // 
	private String  contactPhone       	;  // 
	private String  contactAddr			;  // 
	private String  bigKind				;  //
	private String  midKind				; //
	private String  smallKind   		;  // 
	private String  custType   		;//
	private String  version              ;
	private String isPass				 ;
	@Transient
	private String bigKindName;
	@Transient
	private String midKindName;
	@Transient
	private String smallKindName;
	
	
	public String getBigKindName() {
		return bigKindName;
	}
	public void setBigKindName(String bigKindName) {
		this.bigKindName = bigKindName;
	}
	public String getMidKindName() {
		return midKindName;
	}
	public void setMidKindName(String midKindName) {
		this.midKindName = midKindName;
	}
	public String getSmallKindName() {
		return smallKindName;
	}
	public void setSmallKindName(String smallKindName) {
		this.smallKindName = smallKindName;
	}
	public String getPassId() {
		return passId;
	}
	public void setPassId(String passId) {
		this.passId = passId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getFeeSituation() {
		return feeSituation;
	}
	public void setFeeSituation(String feeSituation) {
		this.feeSituation = feeSituation;
	}

	public double getFeeRate() {
		return feeRate;
	}

	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}
	public String getFeeCcy() {
		return feeCcy;
	}
	public void setFeeCcy(String feeCcy) {
		this.feeCcy = feeCcy;
	}
	public String getFeeBasis() {
		return feeBasis;
	}
	public void setFeeBasis(String feeBasis) {
		this.feeBasis = feeBasis;
	}
	public String getPassagewayCno() {
		return passagewayCno;
	}
	public void setPassagewayCno(String passagewayCno) {
		this.passagewayCno = passagewayCno;
	}
	public String getPassagewayName() {
		return passagewayName;
	}
	public void setPassagewayName(String passagewayName) {
		this.passagewayName = passagewayName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getIsPass() {
		return isPass;
	}
	public void setIsPass(String isPass) {
		this.isPass = isPass;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public String getIntFre() {
		return intFre;
	}
	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}
	public String getCashtDate() {
		return cashtDate;
	}
	public void setCashtDate(String cashtDate) {
		this.cashtDate = cashtDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPassagewayBankacccode() {
		return passagewayBankacccode;
	}
	public void setPassagewayBankacccode(String passagewayBankacccode) {
		this.passagewayBankacccode = passagewayBankacccode;
	}
	public String getPassagewayBankaccname() {
		return passagewayBankaccname;
	}
	public void setPassagewayBankaccname(String passagewayBankaccname) {
		this.passagewayBankaccname = passagewayBankaccname;
	}
	public String getPassagewayBankcode() {
		return passagewayBankcode;
	}
	public void setPassagewayBankcode(String passagewayBankcode) {
		this.passagewayBankcode = passagewayBankcode;
	}
	public String getPassagewayBankname() {
		return passagewayBankname;
	}
	public void setPassagewayBankname(String passagewayBankname) {
		this.passagewayBankname = passagewayBankname;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactAddr() {
		return contactAddr;
	}
	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}
	public String getBigKind() {
		return bigKind;
	}
	public void setBigKind(String bigKind) {
		this.bigKind = bigKind;
	}
	public String getMidKind() {
		return midKind;
	}
	public void setMidKind(String midKind) {
		this.midKind = midKind;
	}
	public String getSmallKind() {
		return smallKind;
	}
	public void setSmallKind(String smallKind) {
		this.smallKind = smallKind;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
	
}
