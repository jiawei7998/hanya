package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.capital.trade.model.TdTradeOffset;

/**
 * @projectName 同业业务管理系统
 * @className 出账交易冲销持久层
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdTradeOffsetMapper extends Mapper<TdTradeOffset> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 出账交易冲销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTradeOffset getTradeOffsetById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 出账交易冲销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeOffset> getTradeOffsetList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 出账交易冲销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeOffset> getTradeOffsetListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 出账交易冲销对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdTradeOffset> getTradeOffsetListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param tradeExtend
	 * @return
	 */
	public TdTradeOffset searchTradeOffset(TdTradeOffset tradeOffset);
	public List<TbkEntry> searchTbkEntryByDealNo(Map<String, Object> params);
	/**
  	 * 添加交易的冲正账务
  	 * @param map
  	 */
  	public void addEntryOffset(Map<String, Object> map);
}