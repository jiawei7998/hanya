package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产-风险缓释
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASE_RISK")
public class TdBaseRisk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_BASE_RISK.NEXTVAL FROM DUAL")
	private String riskNo;
	
	private String dealNo;
	private Integer version;
	
	private String riskType;//缓释类型 1-保证担保 2-保单 3-保函保证
	private String ecifNo;//担保人编号（ECIF客户号）/保单开出人编号/保函开出行编号
	private String ecifName;//担保人名称/保单开出人名称/保函开出行名称
	private String rType;//担保人客户类型/保单类型
	private String vDate;//担保起始日/保单生效日/保证起始日
	private String mDate;//担保到期日/保单到期日/保证到期日
	private String ccy;//担保币种/保单币种/保证币种
	private double amt;//担保金额/保单担保金额/保证金额
	public String getRiskNo() {
		return riskNo;
	}
	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getRiskType() {
		return riskType;
	}
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	public String getEcifNo() {
		return ecifNo;
	}
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	public String getEcifName() {
		return ecifName;
	}
	public void setEcifName(String ecifName) {
		this.ecifName = ecifName;
	}
	public String getrType() {
		return rType;
	}
	public void setrType(String rType) {
		this.rType = rType;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
}
