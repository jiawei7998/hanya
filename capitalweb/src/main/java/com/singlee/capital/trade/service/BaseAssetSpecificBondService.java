package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdAssetSpecificBond;

/**
 * @projectName 同业业务管理系统
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface BaseAssetSpecificBondService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 */
	public List<TdAssetSpecificBond> getTdAssetSpecificBondList(Map<String, Object> params);
	
}
