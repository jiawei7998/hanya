package com.singlee.capital.trade.acc.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.accounting.model.TacTask;
import com.singlee.cap.accounting.service.AccountingTaskService;
import com.singlee.cap.base.mapper.TbkEventMapper;
import com.singlee.cap.base.model.TbkEvent;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.cap.bookkeeping.mapper.TbkManualEntryMapper;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.cap.bookkeeping.service.TbkEntryService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.TtSettlDateMapper;
import com.singlee.capital.base.model.TtSettlDate;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.interfacex.mapper.TtT24FloatRateMapper;
import com.singlee.capital.interfacex.model.TtT24FloatRate;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdMtmMapper;
import com.singlee.capital.interfacex.qdb.mapper.TdTrdValueAssessmentMapper;
import com.singlee.capital.interfacex.qdb.model.TdTrdMtm;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyAmorVoMapper;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyVoMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyAmorVo;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyVo;
import com.singlee.capital.trade.acc.service.AccForTradeAccessService;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityCurrentMapper;
import com.singlee.capital.trade.mapper.TdAdvanceMaturityMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccForTradeAccessServiceImpl implements AccForTradeAccessService {
	@Autowired
	TdAccTrdDailyVoMapper accTrdDailyVoMapper;
	@Autowired
	TdAccTrdDailyAmorVoMapper accTrdDailyAmorVoMapper;
	@Autowired
	TdAccTrdDailyMapper accTrdDailyMapper;
	@Autowired
	TdAdvanceMaturityMapper advanceMaturityMapper;
	@Autowired
	TrdTposMapper tposMapper;
	@Autowired
	BookkeepingService bookkeepingService;
	@Autowired
	TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	TdProductFeeDealMapper productFeeDealMapper;
	@Autowired
	BatchDao batchDao;
	@Autowired
	DayendDateService dateService;
	@Autowired
	TtSettlDateMapper ttSettlDateMapper;
	@Autowired
	TdAmtRangeMapper tdAmtRangeMapper;
	@Autowired
	TtT24FloatRateMapper t24FloatRateMapper;
	@Autowired
	TdAdvanceMaturityCurrentMapper advanceMaturityCurrentMapper;
	@Autowired
	TdCashflowDailyInterestMapper cashflowDailyInterestMapper;
	@Autowired
	TdFeesPassAgewayService tdFeesPassAgewayService;
	@Autowired
	private TbkManualEntryMapper tbkYearEntryMapper;
	@Autowired
	private TbkEventMapper tbkEventMapper;
	@Autowired
	private AccountingTaskService accountingTaskService;
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	@Autowired
	private TrdTposService trdTposService;
	@Autowired
	private TbkEntryService tbkEntryService;
	@Autowired
	private TdTrdMtmMapper tdTrdMtmMapper;
	@Autowired
	private TdTrdValueAssessmentMapper tdTrdValueAssessmentMapper;
	
	
	
	@Override
	public void getNeedForAccDrawingDatasStepOne(String dealNo) {
		// TODO Auto-generated method stub
//		try{
			//String currWorkDate = dateService.getDayendDate();//业务日
			String nextWorkDate = dateService.getNextDayendDate();//下一个自然日
			
			Map<String, Object> params =  new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getTdAccTrdDailyVos(params);
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdTrdTpos tpos=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"正常需计提数据条数为:" + accTrdDailyVos.size());
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"开始计提");
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_NORMAL);
					double dayCalAmt = 0.00;
					double dayInterest = 0.00;
					//处于期限之中的每日计提算法=（T+1)-(T)
					//判断  当前账务日期与下一个账务日期之间的差值  
					int days = 0;
					//nextWorkDate yu REF_END_DATE 比较大小  如果大于  则用 REF_END_DATE 小于等于用nextWorkDate
					if(PlaningTools.compareDate2(nextWorkDate, accTrdDailyVo.getRefEndDate(), "yyyy-MM-dd")) {
						days = PlaningTools.daysBetween(accTrdDailyVo.getCurDate(), accTrdDailyVo.getRefEndDate());
					}else{
						days = PlaningTools.daysBetween(accTrdDailyVo.getCurDate(), nextWorkDate);
					}
					if(days<=0) {continue;}//表示日期带入不正确，直接跳过该笔业务
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					//判断原交易 是否为浮动
					if(DictConstants.rateType.floatingRate.equalsIgnoreCase(productApproveMain.getRateType())){
						TtT24FloatRate t24FloatRate = new TtT24FloatRate();
						t24FloatRate.setRateCode(productApproveMain.getRateCode());
						t24FloatRate = t24FloatRateMapper.getMaxFloatRateByRateCode(t24FloatRate);
						if(null != t24FloatRate){//存在
							accTrdDailyVo.setExecRate(PlaningTools.add(
									PlaningTools.add(t24FloatRate.getCurrentRate(), productApproveMain.getRateDiff()/10000),
									PlaningTools.mul(t24FloatRate.getCurrentRate(), productApproveMain.getRateScale()/100)));
						}
					}
					
					params.put("dealNo", accTrdDailyVo.getDealNo());
					params.put("feeSituation", "Pay");
					List<TdFeesPassAgeway> fees = tdFeesPassAgewayService.getPassageWayByEndDate(params);
					
					for(int i = 0 ; i < days ; i++)//循环累加利息金额-跨越天数
					{
						dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(), PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())
												),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
							//刚好是期限的最后一天，那么算法需要改为[VDATE-MDATE]每日计提，筛选出需要计提的天数相加
						
						for (TdFeesPassAgeway fee : fees) {
							dayInterest = PlaningTools.add(dayInterest, 
									new BigDecimal(PlaningTools.sub(
									PlaningTools.div(
											PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(),
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),fee.getFeeRate())), DayCountBasis.equal(fee.getFeeBasis()).getDenominator()), 
										PlaningTools.div(
												PlaningTools.mul(
													new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(),
													PlaningTools.mul(accTrdDailyVo.getSettlAmt(),fee.getFeeRate())
													),DayCountBasis.equal(fee.getFeeBasis()).getDenominator())
													)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue());
						}
					
					}// end for
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提金额为："+dayCalAmt+"通道计提金额为：" + dayInterest);
					
					dayCalAmt = PlaningTools.sub(dayCalAmt,dayInterest);
					accTrdDaily.setAccAmt(dayCalAmt);
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
	//				accTrdDailyMapper.insert(accTrdDaily);
					/*****************TPOS*************************/
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计每日计提
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), dayCalAmt));//累计每日利息
					tpos.setPostdate(accTrdDailyVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
					if(tpos.getAccruedTint()<=0) {continue;}//负数的时候不再出计提
					
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_NORMAL, accTrdDaily.getAccAmt());
					
					InstructionPackage instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提开始调用核算接口");
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提分录id为："+flowId);
						accTrdDaily.setAcctNo(flowId);
					}
					accTrdDailies.add(accTrdDaily);
					accTrdDailyMapper.insert(accTrdDaily);
					
					
					if("1".equals(productApproveMain.getNostroFlag()) && productApproveMain.getNostroRate() > 0)
					{
						createNostroInteret(productApproveMain,days,accTrdDailyVo);
					}
				}catch (Exception e) {
					//e.printStackTrace();
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
//		}catch (Exception e) {
//			e.printStackTrace();
//			LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
//			JY.debug(e.getMessage());
//		}
	}
	
	//生产存放同业活期账务
	private void createNostroInteret(TdProductApproveMain productApproveMain,int days,TdAccTrdDailyVo accTrdDailyVo) 
	{
		try {
			double dayCalAmt = 0.00;
			String date = null;
			InstructionPackage instructionPackage = null;
			Map<String, Object> amtMap= null;
			TdTrdTpos tpos = null;
			TdAccTrdDaily tdAccTrdDaily = null;
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			for(int i=0 ; i <days ;i++)//循环累加利息金额-跨越天数
			{
				date = PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i);
				
				if(PlaningTools.compareDate2(date, productApproveMain.gettDate(), "yyyy-MM-dd")
						|| PlaningTools.compareDate2(productApproveMain.getNostroVdate(),date,"yyyy-MM-dd"))//日期小于起息日或者大于划款日
				{
					continue;
					
				}else if(PlaningTools.compareDateEqual(productApproveMain.getNostroVdate(), productApproveMain.gettDate(), "yyyy-MM-dd"))//日期等于起息日
				{
					//生成存放同业活期科目
					makeBookkeep(productApproveMain, date, productApproveMain.getNostroAcct(), "40700030039");
					makeBookkeep(productApproveMain, date,  "40700030039",productApproveMain.getNostroAcct());
					
				}else if(PlaningTools.compareDateEqual(date, productApproveMain.getNostroVdate(), "yyyy-MM-dd"))//日期等于起息日
				{
					//生成存放同业活期科目
					makeBookkeep(productApproveMain, date, productApproveMain.getNostroAcct(), "40700030039");
					
					//生成计提利息
					dayCalAmt = PlaningTools.add(dayCalAmt, 
						new BigDecimal(PlaningTools.sub(
						PlaningTools.div(
								PlaningTools.mul(
										new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(), PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(), 
										PlaningTools.mul(accTrdDailyVo.getSettlAmt(),productApproveMain.getNostroRate())),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())
						,PlaningTools.div(
								PlaningTools.mul(
										new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(), 
										PlaningTools.mul(accTrdDailyVo.getSettlAmt(),productApproveMain.getNostroRate())
										),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())				
						)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
					);
					//生成计提账务
					tdAccTrdDaily = new TdAccTrdDaily();
					tdAccTrdDaily.setAccAmt(dayCalAmt);
					tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					tdAccTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
					tdAccTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					tdAccTrdDaily.setAccType(DurationConstants.AccType.INTEREST_NORMAL);
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计每日计提
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), dayCalAmt));//累计每日利息
					tpos.setPostdate(accTrdDailyVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
					if(tpos.getAccruedTint()<=0) {continue;}//负数的时候不再出计提
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_NORMAL, tdAccTrdDaily.getAccAmt());
					
					instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						tdAccTrdDaily.setAcctNo(flowId);
					}				
					accTrdDailies.add(tdAccTrdDaily);
					
				}else if(PlaningTools.compareDate2(date, productApproveMain.getNostroVdate(), "yyyy-MM-dd")
						&& PlaningTools.compareDate2(productApproveMain.gettDate(),date, "yyyy-MM-dd"))//日期大于起息日小于划款日
				{
					//生成计提利息
					dayCalAmt = PlaningTools.add(dayCalAmt, 
						new BigDecimal(PlaningTools.sub(
						PlaningTools.div(
								PlaningTools.mul(
										new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(), PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(), 
										PlaningTools.mul(accTrdDailyVo.getSettlAmt(),productApproveMain.getNostroRate())),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())
						,PlaningTools.div(
								PlaningTools.mul(
										new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(), 
										PlaningTools.mul(accTrdDailyVo.getSettlAmt(),productApproveMain.getNostroRate())
										),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())				
						)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
					);
					//生成计提账务
					tdAccTrdDaily = new TdAccTrdDaily();
					tdAccTrdDaily.setAccAmt(dayCalAmt);
					tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					tdAccTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
					tdAccTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					tdAccTrdDaily.setAccType(DurationConstants.AccType.INTEREST_NORMAL);
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计每日计提
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), dayCalAmt));//累计每日利息
					tpos.setPostdate(accTrdDailyVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
					if(tpos.getAccruedTint()<=0) {continue;}//负数的时候不再出计提
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_NORMAL, tdAccTrdDaily.getAccAmt());
					
					instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						tdAccTrdDaily.setAcctNo(flowId);
					}				
					accTrdDailies.add(tdAccTrdDaily);
					
				}else if(PlaningTools.compareDateEqual(date, productApproveMain.gettDate(), "yyyy-MM-dd"))//日期等于划款日
				{
					//生成存放同业活期科目
					makeBookkeep(productApproveMain, date,  "40700030039",productApproveMain.getNostroAcct());
				}
				
			}// end for
			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		} catch (Exception e) {
			LogManager.getLogger(LogManager.MODEL_BATCH).error("生成活期账务出错", e);
		}
	}
	public void makeBookkeep(TdProductApproveMain productApproveMain,String date,String dAcct,String cAcct){
		String eventId=null;
		String flowNo=tbkYearEntryMapper.getNewFlowNo();
		TbkEntry tbkEntry=new TbkEntry();
		TbkEntry tbkEntry1=new TbkEntry();
		List<TbkEvent> eventList = null;
		List<TbkEntry> entryList=new ArrayList<TbkEntry>();
		tbkEntry.setValue(productApproveMain.getAmt());
		tbkEntry1.setValue(productApproveMain.getAmt());
		tbkEntry.setFlowId(flowNo);
		tbkEntry1.setFlowId(flowNo);
		tbkEntry.setSubjCode(dAcct);
		tbkEntry1.setSubjCode(cAcct);
		tbkEntry.setDebitCreditFlag("D");
		tbkEntry1.setDebitCreditFlag("C");
		entryList.add(tbkEntry);
		entryList.add(tbkEntry1);
		eventList = tbkEventMapper.getTbkEventPage(BeanUtil.beanToMap("manual"));//会计事件
		if(eventList.size() > 0){
			eventId = eventList.get(0).getEventId();
		}else{
			throw new RException("未找到对应的会计事件代码"); 
		}
		List<TacTask> taskList = accountingTaskService.selectEntryTaskList(null);
		TacTask task = taskList.get(0);
		for (int j = 0; j < entryList.size(); j++) {
			entryList.get(j).setFlowSeq(String.valueOf(j+1));
			entryList.get(j).setTaskId(task.getTaskId());
			entryList.get(j).setEventId(eventId);
			entryList.get(j).setCcy(productApproveMain.getCcy());
			entryList.get(j).setBkkpgOrgId(productApproveMain.getSponInst());
			entryList.get(j).setPostDate(date);
			entryList.get(j).setInnerAcctSn(entryList.get(j).getSubjCode());
			entryList.get(j).setCoreAcctCode(entryList.get(j).getSubjCode());
			entryList.get(j).setState(DictConstants.ApproveStatus.New);
			entryList.get(j).setUpdateTime(DateUtil.getCurrentDateTimeAsString());
			entryList.get(j).setDealNo(productApproveMain.getDealNo());
			entryList.get(j).setSponInst(productApproveMain.getSponInst());	
			entryList.get(j).setState("0");
			entryList.get(j).setSendFlag("0");
			//更新头寸
			String subjCode = entryList.get(j).getSubjCode();
			String dealNo = entryList.get(j).getDealNo();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("subjCode", subjCode);
			List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefList(map);
			if(list.size() == 0){
				throw new RException("未找到对应的科目=>"+subjCode); 
			}
			
			entryList.get(j).setSubjName(list.get(0).getSubjName());
			
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("dealNo", dealNo);
			TdTrdTpos ttts = trdTposService.getTdTrdTposByKey(map1);
			if(ttts != null){
				/**科目性质不为空*/
				if(list.get(0).getSubjNature() != null && !"".equals(list.get(0).getSubjNature())){
					if("1".equals(list.get(0).getSubjNature())){
						if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
							ttts.setSettlAmt(PlaningTools.sub(ttts.getSettlAmt(), entryList.get(j).getValue()));
						}else if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
							ttts.setSettlAmt(PlaningTools.add(entryList.get(j).getValue(), ttts.getSettlAmt()));
						}
						tposMapper.updateByPrimaryKey(ttts);
					}
					/**是否逾期   否*/
					if("5".equals(list.get(0).getSubjNature())|| "2".equals(list.get(0).getSubjNature())){
						if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
							ttts.setAccruedTint(PlaningTools.sub(ttts.getAccruedTint(),entryList.get(j).getValue()));
						}else if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
							ttts.setAccruedTint(PlaningTools.add(entryList.get(j).getValue(), ttts.getAccruedTint()));
						}
						tposMapper.updateByPrimaryKey(ttts);
					}else if("3".equals(list.get(0).getSubjNature())){
						if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Credit)){
							ttts.setActualTint(PlaningTools.sub(ttts.getActualTint(),entryList.get(j).getValue() ));
						}else if(entryList.get(j).getDebitCreditFlag().equals(DictConstants.DebitCreditFlag.Debit)){
							ttts.setActualTint(PlaningTools.add(entryList.get(j).getValue(), ttts.getActualTint()));
						}
						tposMapper.updateByPrimaryKey(ttts);
					}
				}
			}		
		}
		tbkEntryService.save(entryList);
		bookkeepingService.updateBookkeepingBalance(entryList);
	}
	
	@Override
	public void getNeedForCurrentAccDrawingDatasStepOne(String dealNo) {
		// TODO Auto-generated method stub
//			String nextWorkDate = dateService.getNextBizDate();//下一个业务日
			//String currWorkDate = dateService.getDayendDate();//业务日
			String nextWorkDate = dateService.getNextDayendDate();//dateService.getNextBizDate();//下一个业务日
			
			Map<String, Object> params =  new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getTdAccTrdDailyVosCurrent(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"活期交易需计提数据条数为:" + accTrdDailyVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdTrdTpos tpos=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_NORMAL);
					double dayCalAmt = 0.00;
					//处于期限之中的每日计提算法=（T+1)-(T)
					//判断  当前账务日期与下一个账务日期之间的差值  
					int days = 0;
					days = PlaningTools.daysBetween(accTrdDailyVo.getCurDate(), nextWorkDate);
					if(days<=0) {continue;}//表示日期带入不正确，直接跳过该笔业务
					for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
					dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(), PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())
												),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
					}
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提金额为："+dayCalAmt);
					accTrdDaily.setAccAmt(dayCalAmt);
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
					/*****************TPOS*************************/
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计每日计提
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), dayCalAmt));//累计每日利息
					tpos.setPostdate(accTrdDailyVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
					if(tpos.getAccruedTint()<=0) {continue;}//负数的时候不再出计提
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_NORMAL, accTrdDaily.getAccAmt());
					
					InstructionPackage instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
					instructionPackage.setAmtMap(amtMap);
					try{
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提开始调用核算接口");
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"计提分录id为："+flowId);
							accTrdDaily.setAcctNo(flowId);
							//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
						}
					}catch (Exception e) {
						LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
						//e.printStackTrace();
						JY.debug(e.getMessage());
					}
					accTrdDailies.add(accTrdDaily);
					accTrdDailyMapper.insert(accTrdDaily);
				}catch (Exception e) {
					//e.printStackTrace();
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		
	}
	
	
	@Override
	public void getNeedForAccAdvanceMaturity(String dealNo, String refNo,Map<String, Object> amtParams) {
		// TODO Auto-generated method stub
		Map<String, Object> tposParams = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dealNo", dealNo);
		params.put("refNo", refNo);//获取当前账务日期需要做处理的提前还本交易
		List<TdAdvanceMaturity> advanceMaturities = advanceMaturityMapper.getAdvanceMaturitiesForAcc(params);
		List<TdAccTrdDaily> accTrdDailies = null;//记录还本引起的金额类出账中间数据
		List<TdTrdTpos> trdTpos = new ArrayList<TdTrdTpos>();//记录头寸变动数据
		TdAccTrdDaily tdAccTrdDaily = null;TdProductApproveMain productApproveMain = null;Map<String, Object> amtMap = null;
		String postDate = dateService.getDayendDate();
		for(TdAdvanceMaturity advanceMaturity : advanceMaturities)
		{
			accTrdDailies = new ArrayList<TdAccTrdDaily>();
			amtMap = new HashMap<String, Object>();//
			tposParams.clear();//清空记录  用于查询每笔提前还本对应原交易持仓信息
			tposParams.put("dealNo", advanceMaturity.getRefNo());
			tposParams.put("version", advanceMaturity.getVersion());
			TdTrdTpos tpos = tposMapper.getTdTrdTposByKey(tposParams);
			JY.require(tpos!=null, advanceMaturity.getRefNo()+"持仓TPOS丢失！");
			//查询原交易数据
			productApproveMain = productApproveMainMapper.getProductApproveMainActivated(advanceMaturity.getRefNo());
			JY.require(productApproveMain!=null, productApproveMain.getDealNo()+"持仓交易丢失！");
			/**
			 * 将提前到期交易中的金额进行拆解
			 * 1、后收息
			 * 提前还本本金
			 * 提前还本利息
			 * 提前还本罚息
			 * 2、前收息
			 * 提前还本本金
			 * 提前还本归还利息
			 * 
			 */
			tdAccTrdDaily = new TdAccTrdDaily();//提前还款本金
			tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt()));//提前归还本金
			tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_AMT);
			tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
			tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
			tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
			tdAccTrdDaily.setPostDate(postDate);
			accTrdDailies.add(tdAccTrdDaily);
			//会计  提前还款-本金金额
			amtMap.put(DurationConstants.AccType.AM_AMT, tdAccTrdDaily.getAccAmt());
			
			if(advanceMaturity.getIntType().equalsIgnoreCase(DictConstants.intType.chargeBefore))//先收息
			{
				/**
				 * 如果是部分还款，归还利息，那么需要重新拟合摊销数据；
				 * 
				 * 
				 * 如果是全额还款，归还利息，那么只需要调整摊销余额与归还利息之间的差额
				 * 归还利息多于未摊销余额，那么需要冲减已摊
				 * 如果归还利息少于未摊销余额，那么将未摊-归还利息部分作为利息调整
				 * 
				 * 如果是全部还款，无归还利息，那么只需要将剩余未摊转为利息收入；
				 * 如果是部分还款，无归还利息，那么只需要将部分还款的对应未摊转为利息收入；
				 */
				tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_RETURN_IAMT,advanceMaturity.getReturnIamt()));//归还利息
				tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_RETURN_IAMT);
				tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
				tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
				tdAccTrdDaily.setPostDate(postDate);
				accTrdDailies.add(tdAccTrdDaily);
				//会计   提前还款（先收息） 归还利息金额
				amtMap.put(DurationConstants.AccType.AM_RETURN_IAMT,tdAccTrdDaily.getAccAmt());

				//DR 279的金额=归还的利息   1W
				double dr279amt = -ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_RETURN_IAMT,advanceMaturity.getReturnIamt());
				//DR 407来账的金额  40W
				double dr407amt = ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt());//提前归还本金
				//CR 181投资本金 = DR 407 + DR 279 = 41W
				//折溢价利息部分变成原利息-归还利息
				double disAmt = PlaningTools.sub(tpos.getDisAmt(), -ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_RETURN_IAMT,advanceMaturity.getReturnIamt()));
				int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
				//由于归还了利息，那么需要同步调整实际本金=原本金-归还利息
				double faceAmt = PlaningTools.sub(tpos.getSettlAmt(), -ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_RETURN_IAMT,advanceMaturity.getReturnIamt()));
				//新的投资收益率=disAmt*basis/期限/faceAmt
				double rateNew = PlaningTools.div(
						PlaningTools.mul(disAmt, DayCountBasis.valueOf(basicType).getDenominator()),
						PlaningTools.mul(faceAmt, productApproveMain.getTerm())
						);
				//重新封装归还利息后的本金区间（1）
				List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
				PrincipalInterval principalInterval= new PrincipalInterval();
				principalInterval.setStartDate(productApproveMain.getvDate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(faceAmt);
				principalIntervals.add(principalInterval);
				//重新封装归还利息后的利率区间（2）
				List<InterestRange> interestRanges = new ArrayList<InterestRange>();
				InterestRange interestRange = new InterestRange();
				interestRange.setStartDate(productApproveMain.getvDate());
				interestRange.setEndDate(productApproveMain.getmDate());
				interestRange.setRate(rateNew);
				interestRanges.add(interestRange);
				
				List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();//返回的收息计划
				List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
				try {
					CashflowDailyInterest amorBean = null;
					/*//重算每日摊销（归还利息后）
					PlaningTools.calPlanSchedule(interestRanges,principalIntervals,tmCashflowDailyInterests,tmCashflowInterests,
					productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
					faceAmt,rateNew,com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType));
					//计算归还利息后新的投资利率 摊销到当前账务日期的摊销总额
					
					double sumAmorAdj501 = 0.00;
					for(int j = 1 ;j <tmCashflowDailyInterests.size();j++)
					{
						amorBean = tmCashflowDailyInterests.get(j);
						if(PlaningTools.compareDate2(postDate, amorBean.getRefBeginDate(), "yyyy-MM-dd")){
							sumAmorAdj501 = PlaningTools.add(sumAmorAdj501, amorBean.getInterest());
						}
					}
					//得到当前已摊销的501
					double old501amt = tpos.getAmorTint();
					//拟合  需要调整的  +为以前多了，需要DR方向  -以前少了 CR方向； 正常情况下肯定是DR方向  5字头之前多的，因为折溢价多，后面的因为归还了利息，自然少；
					double adj501amtDR = PlaningTools.sub(old501amt, sumAmorAdj501);
					//计算提前还本带来的后续未摊部分需要提前实现，也就是CR 501  提前还款金额对应的本金在（提前还款日-到期日）区间的后续摊销金额合计
					principalIntervals.clear();*/
					// 计算扣除提前还本后的本金整个摊销计划，投资收益=rateNew
					faceAmt = PlaningTools.add(PlaningTools.sub(faceAmt,dr407amt),dr279amt);
					principalInterval.setStartDate(productApproveMain.getvDate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(faceAmt);
					principalIntervals.add(principalInterval);
					//重算剩余本金的每日摊销
					PlaningTools.calPlanSchedule(interestRanges,principalIntervals,tmCashflowDailyInterests,tmCashflowInterests,
							productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
							faceAmt,rateNew,com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())),DayCountBasis.valueOf(basicType));
					//加总剩余本金截止到期日的摊销之和
					double sumAmorAdj501 = 0.00;
					for(int j = 1 ;j <tmCashflowDailyInterests.size();j++)
					{
						amorBean = tmCashflowDailyInterests.get(j);
						if(PlaningTools.compareDate(amorBean.getRefBeginDate(), postDate, "yyyy-MM-dd")){
							sumAmorAdj501 = PlaningTools.add(sumAmorAdj501, amorBean.getInterest());
						}
					}
					//需要最终调整的金额  + 补计 501  -冲减501
					double  adj501amtDR = disAmt - sumAmorAdj501-tpos.getAmorTint();
					
					//dr407amt  adj501amtDR  dr279amt
					tpos.setSettlAmt(faceAmt);
					tpos.setDisAmt(faceAmt+sumAmorAdj501);
					tpos.setProcAmt(sumAmorAdj501);
					
					tdAccTrdDaily = new TdAccTrdDaily();
					tdAccTrdDaily.setAccAmt(adj501amtDR);//摊销利息调整
					tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_AMOR_ADJ);
					tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
					tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
					tdAccTrdDaily.setPostDate(postDate);
					accTrdDailies.add(tdAccTrdDaily);
					//会计  未摊部分需要直接转利息收入；
					amtMap.put(DurationConstants.AccType.AM_AMOR_ADJ, tdAccTrdDaily.getAccAmt());
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), tdAccTrdDaily.getAccAmt()));
					tpos.setAmorTint(PlaningTools.add(tpos.getAmorTint(), tdAccTrdDaily.getAccAmt()));
					tpos.setUnamorInt(PlaningTools.sub(tpos.getUnamorInt(),tdAccTrdDaily.getAccAmt()));
					
					//重新持久化
					//將之前的利息進行版本更替+1
					cashflowDailyInterestMapper.updateDailyInterestVersionAddOne(BeanUtil.beanToMap(productApproveMain));
					/**
					 * 重新安排換本計劃
					 */
					//利息计划  关联交易表单及状态数据设置
					String dealNO = productApproveMain.getDealNo();
					for(int j = 0;j<tmCashflowInterests.size();j++)
					{
						tmCashflowInterests.get(j).setDealNo(dealNO);
						tmCashflowInterests.get(j).setCfType("Interest");
						tmCashflowInterests.get(j).setPayDirection("Recevie");
						tmCashflowInterests.get(j).setVersion(productApproveMain.getVersion());
					}
					//利息明细  设置交易关联项
					for(int i=0;i<tmCashflowDailyInterests.size();i++)
					{
						tmCashflowDailyInterests.get(i).setDealNo(dealNO);
						tmCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
					}
					//批量插入调整后的利息计划
					batchDao.batch(getSqlForCashFlow(BeanUtil.beanToMap(productApproveMain))+"CashflowInterestMapper.insert", tmCashflowInterests);
					//批量插入调整后的每日计提
					batchDao.batch(getSqlForCashFlow(BeanUtil.beanToMap(productApproveMain))+"CashflowDailyInterestMapper.insert", tmCashflowDailyInterests);
					
				}catch (Exception e) {
					// TODO: handle exception
					throw new RException(e);
				}
				
			}else {
				//记录昨日计提和应计				
				tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_PENALTY,advanceMaturity.getAmPenalty()));//提前罚息
				tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_PENALTY);
				tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
				tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
				tdAccTrdDaily.setPostDate(postDate);
				accTrdDailies.add(tdAccTrdDaily);
				//会计 -提前还款（后收息） 罚息金额
				amtMap.put(DurationConstants.AccType.AM_PENALTY,tdAccTrdDaily.getAccAmt());
				
				tdAccTrdDaily = new TdAccTrdDaily();
//				tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_INT,advanceMaturity.getAmInt()));//提前利息
				tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_INT);
				tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
				tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
				tdAccTrdDaily.setPostDate(postDate);
//				accTrdDailies.add(tdAccTrdDaily);
				//会计  -提前还款（后收息） 收息金额
//				amtMap.put(DurationConstants.AccType.AM_INT,tdAccTrdDaily.getAccAmt());
								
				//应收利息调整 首先比对持仓的应计利息剩余总额与还本利息之差
				double adjIntAmt = PlaningTools.sub(tpos.getAccruedTint(),ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_INT,advanceMaturity.getAmInt()));
				if(PlaningTools.sub(tpos.getSettlAmt(), ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt()))<=0)//全部归还(剩余清算本金-提前到期本金)
				{	
					//全额还款，利息送应计利息
					tdAccTrdDaily.setAccAmt(tpos.getAccruedTint());//提前利息
					accTrdDailies.add(tdAccTrdDaily);
					amtMap.put(DurationConstants.AccType.AM_INT,tdAccTrdDaily.getAccAmt());
					//>0 全额还款说明计提多了相应的514也多   需要调整掉；否则说明计提少了，需要补514
					double trueAdjAmt = -adjIntAmt;
					tdAccTrdDaily = new TdAccTrdDaily();
					tdAccTrdDaily.setAccAmt(trueAdjAmt);//提前利息调整
					tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_INT_ADJ);
					tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
					tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
					tdAccTrdDaily.setPostDate(postDate);
					accTrdDailies.add(tdAccTrdDaily);
					//会计  -提前还款（后收息） 全部还款利息调整-冲减应计计提，补利息收入
					amtMap.put(DurationConstants.AccType.AM_INT_ADJ,tdAccTrdDaily.getAccAmt());
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(),trueAdjAmt));//截止当日利息累计  增加还息部分
					tpos.setAccruedTint(0);//截止当日应计累计  清0-全额还本
					tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(),ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
					tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
					
				}else {
					if(adjIntAmt >0){
						tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_INT,advanceMaturity.getAmInt()));//提前利息
					}else{
						tdAccTrdDaily.setAccAmt(tpos.getAccruedTint());//提前利息
					}
					accTrdDailies.add(tdAccTrdDaily);
					amtMap.put(DurationConstants.AccType.AM_INT,tdAccTrdDaily.getAccAmt());
					//部分还款时   计提多的，无需管理，计提少了需要追加514
					double trueAdjAmt = adjIntAmt>0?0:-adjIntAmt;
					tdAccTrdDaily = new TdAccTrdDaily();
					tdAccTrdDaily.setAccAmt(trueAdjAmt);//提前利息调整
					tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_INT_ADJ);
					tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
					tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
					tdAccTrdDaily.setPostDate(postDate);
					accTrdDailies.add(tdAccTrdDaily);
					//会计  -提前还款（后收息） 全部还款利息调整-冲减应计计提，补利息收入
					amtMap.put(DurationConstants.AccType.AM_INT_ADJ,tdAccTrdDaily.getAccAmt());
					tpos.setActualTint(PlaningTools.add(tpos.getActualTint(),trueAdjAmt));//截止当日利息累计  增加还息部分
					tpos.setAccruedTint(adjIntAmt);//截止当日应计累计  减去转换为利息收入的金额
					tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(),ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
					tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
					
				}
			}
			trdTpos.add(tpos);
			
			//调用账务接口
			InstructionPackage instructionPackage = new InstructionPackage();
			instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
			instructionPackage.setAmtMap(amtMap);
			String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
			if(null != flowId && !"".equals(flowId))
			{//更新回执会计套号  每笔交易
				for(int i = 0 ; i <accTrdDailies.size();i++)
				{
					accTrdDailies.get(i).setAcctNo(flowId);
					//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
				}
			}
			//每笔交易插入数据
			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		}
		batchDao.batch("com.singlee.capital.tpos.mapper.TrdTposMapper.updateByPrimaryKey", trdTpos);
	}
	
	
	@Override
	public void getNeedForAccAdvanceMaturityCurrent(String dealNo, String refNo,Map<String, Object> amtParams) {
		// TODO Auto-generated method stub
		Map<String, Object> tposParams = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dealNo", dealNo);
		params.put("refNo", refNo);//获取当前账务日期需要做处理的活期提前还本交易
		List<TdAdvanceMaturityCurrent> advanceMaturities = advanceMaturityCurrentMapper.getAdvanceMaturitiesForAcc(params);
		List<TdAccTrdDaily> accTrdDailies = null;//记录还本引起的金额类出账中间数据
		List<TdTrdTpos> trdTpos = new ArrayList<TdTrdTpos>();//记录头寸变动数据
		TdAccTrdDaily tdAccTrdDaily = null;TdProductApproveMain productApproveMain = null;Map<String, Object> amtMap = null;
		String postDate = dateService.getDayendDate();
		for(TdAdvanceMaturityCurrent advanceMaturity : advanceMaturities)
		{
			accTrdDailies = new ArrayList<TdAccTrdDaily>();
			amtMap = new HashMap<String, Object>();//
			tposParams.clear();//清空记录  用于查询每笔提前还本对应原交易持仓信息
			tposParams.put("dealNo", advanceMaturity.getRefNo());
			tposParams.put("version", advanceMaturity.getVersion());
			TdTrdTpos tpos = tposMapper.getTdTrdTposByKey(tposParams);
			JY.require(tpos!=null, advanceMaturity.getRefNo()+"持仓TPOS丢失！");
			//查询原交易数据
			productApproveMain = productApproveMainMapper.getProductApproveMainActivated(advanceMaturity.getRefNo());
			JY.require(productApproveMain!=null, productApproveMain.getDealNo()+"持仓交易丢失！");
			/**
			 * 将活期提前到期交易中的金额进行拆解
			 * 提前还本本金
			 * 提前还本利息
			 */
			tdAccTrdDaily = new TdAccTrdDaily();//提前还款本金
			tdAccTrdDaily.setAccAmt(ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt()));//提前归还本金
			tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_AMT);
			tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
			tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
			tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
			tdAccTrdDaily.setPostDate(postDate);
			accTrdDailies.add(tdAccTrdDaily);
			//会计  提前还款-本金金额
			amtMap.put(DurationConstants.AccType.AM_AMT, tdAccTrdDaily.getAccAmt());
			
			tdAccTrdDaily = new TdAccTrdDaily();
			tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_INT);
			tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
			tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
			tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
			tdAccTrdDaily.setPostDate(postDate);
//			accTrdDailies.add(tdAccTrdDaily);
			//会计  -提前还款（后收息） 收息金额
//			amtMap.put(DurationConstants.AccType.AM_INT,tdAccTrdDaily.getAccAmt());
							
			//应收利息调整 首先比对持仓的应计利息剩余总额与还本利息之差
			double adjIntAmt = PlaningTools.sub(tpos.getAccruedTint(),ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_INT,advanceMaturity.getAmInt()));
			//活期交易全部还款时，归还利息
			//活期交易部分还款时 ，不归还利息，利息到结息日再归还
			if(PlaningTools.sub(tpos.getSettlAmt(), ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT, advanceMaturity.getAmAmt()))<=0)//全部归还(剩余清算本金-提前到期本金)
			{	
				tdAccTrdDaily.setAccAmt(tpos.getAccruedTint());//提前利息
				accTrdDailies.add(tdAccTrdDaily);
				amtMap.put(DurationConstants.AccType.AM_INT,tdAccTrdDaily.getAccAmt());
				//>0 全额还款说明计提多了相应的514也多   需要调整掉；否则说明计提少了，需要补514
				double trueAdjAmt = -adjIntAmt;
				tdAccTrdDaily = new TdAccTrdDaily();
				tdAccTrdDaily.setAccAmt(trueAdjAmt);//提前利息调整
				tdAccTrdDaily.setAccType(DurationConstants.AccType.AM_INT_ADJ);
				tdAccTrdDaily.setDealNo(advanceMaturity.getRefNo());
				tdAccTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				tdAccTrdDaily.setRefNo(advanceMaturity.getDealNo());
				tdAccTrdDaily.setPostDate(postDate);
				accTrdDailies.add(tdAccTrdDaily);
				//会计  -提前还款（后收息） 全部还款利息调整-冲减应计计提，补利息收入
				amtMap.put(DurationConstants.AccType.AM_INT_ADJ,tdAccTrdDaily.getAccAmt());
				tpos.setActualTint(PlaningTools.add(tpos.getActualTint(),trueAdjAmt));//截止当日利息累计  增加还息部分
				tpos.setAccruedTint(0);//截止当日应计累计  清0-全额还本
			}
			/**
			 * tpos头寸更改
			 * 提前还款 扣减剩余持仓清算金额
			 * 增加还本金额
			 * 更改账务日期
			 * 计提利息冲减
			 */
			tpos.setSettlAmt(PlaningTools.sub(tpos.getSettlAmt(),ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
			tpos.setRetrnAmt(PlaningTools.add(tpos.getRetrnAmt(), ParameterUtil.getDouble(amtParams, DurationConstants.AccType.AM_AMT,advanceMaturity.getAmAmt())));
			trdTpos.add(tpos);
			
			//调用账务接口
			InstructionPackage instructionPackage = new InstructionPackage();
			instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
			instructionPackage.setAmtMap(amtMap);
			String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
			if(null != flowId && !"".equals(flowId))
			{//更新回执会计套号  每笔交易
				for(int i = 0 ; i <accTrdDailies.size();i++)
				{
					accTrdDailies.get(i).setAcctNo(flowId);
					//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
				}
			}
			//每笔交易插入数据
			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		}
		batchDao.batch("com.singlee.capital.tpos.mapper.TrdTposMapper.updateByPrimaryKey", trdTpos);
	}
	
	
	@Override
	public Page<TdAccTrdDailyVo> getTdAccTrdDailyVos(Map<String, Object> params) {
		return accTrdDailyVoMapper.getTdAccTrdDailyVoss(params, ParameterUtil.getRowBounds(params));
	}
	@Override
	public List<TdAccTrdDaily> getTdAccTrdDaily(Map<String, Object> params) {
		
		return accTrdDailyMapper.getTdAccTrdDailyByDealNo(params);
		
	}

	@Override
	public Page<TdAdvanceMaturity> getTdAdvanceMaturity(Map<String, Object> params) {
		return advanceMaturityMapper.getTdAdvanceMaturity(params, ParameterUtil.getRowBounds(params));
	}
	

	@Override
	public Page<TdAccTrdDailyAmorVo> getAccTrdDailyAmor(Map<String, Object> params) {
		return accTrdDailyAmorVoMapper.getTdAccTrdDailyAmorVoss(params, ParameterUtil.getRowBounds(params));
	}

	//逾期计提
	@Override
	public void getNeedForAccDrawingDatesForOverDueStepTow(String dealNo) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
//			String nextWorkDate = dateService.getNextBizDate();//下一个业务日
			//String currWorkDate = dateService.getDayendDate();//业务日
			String nextWorkDate = dateService.getNextDayendDate();//dateService.getNextBizDate();//下一个业务日
			
			Map<String, Object> params =  new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getTdAccTrdDailyVosForOverdue(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"逾期交易需计提数据条数为:" + accTrdDailyVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdTrdTpos tpos=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					
					if(tpos.getOverdueDays() < 89){//交易逾期从第0天开始计算
						double dayCalAmt = 0.00;
						//逾期直接采用1天算法
						//判断  当前账务日期与下一个账务日期之间的差值  
						int days = PlaningTools.daysBetween(accTrdDailyVo.getCurDate(), nextWorkDate);
						if(days<=0) {continue;}//表示日期带入不正确，直接跳过该笔业务
						for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
							dayCalAmt = PlaningTools.add(dayCalAmt,
								new BigDecimal(PlaningTools.div(PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate()),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator())).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
								);
						}
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期"+tpos.getOverdueDays()+"天计提金额为："+dayCalAmt);
						if(dayCalAmt<=0) {continue;}//负数的时候不再出计提
						/*****************TPOS*************************/
						tpos.setOverdueDays(tpos.getOverdueDays()+1);
						tpos.setOverdueAccruedTint(PlaningTools.add(tpos.getOverdueAccruedTint(), dayCalAmt));//累计每日逾期计提
						tpos.setOverdueActualTint(PlaningTools.add(tpos.getOverdueActualTint(), dayCalAmt));//累计每日逾期利息
						tpos.setPostdate(accTrdDailyVo.getCurDate());
						this.tposMapper.updateByPrimaryKey(tpos);
	
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_OVERDUE);
						accTrdDaily.setAccAmt(dayCalAmt);
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
						
						
						//查询原交易数据
						productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
						//调用账务接口
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.INTEREST_OVERDUE, accTrdDaily.getAccAmt());
						InstructionPackage instructionPackage = new InstructionPackage();
						Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
						tradeMap.put("overdueDays", tpos.getOverdueDays());
						instructionPackage.setInfoMap(tradeMap);
						instructionPackage.setAmtMap(amtMap);
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期"+tpos.getOverdueDays()+"天计提开始调用核算接口");
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期"+tpos.getOverdueDays()+"天计提分录id为："+flowId);
							accTrdDaily.setAcctNo(flowId);
							//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
						}
						accTrdDailies.add(accTrdDaily);
						accTrdDailyMapper.insert(accTrdDaily);
					}else if(tpos.getOverdueDays() == 89){
						double dayCalAmt = 0.00;
						dayCalAmt = tpos.getOverdueAccruedTint();
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期"+tpos.getOverdueDays()+"天计提金额为："+dayCalAmt);
						if(dayCalAmt<=0) {continue;}//负数的时候不再出计提
						/*****************TPOS*************************/
						/**
						 * 判定overdueDays=90天时需要 将 逾期180  514凊0
						 */
						tpos.setOverdueDays(tpos.getOverdueDays()+1);
						tpos.setOverdueAccruedTint(0.00);//累计每日逾期计提
						tpos.setOverdueActualTint(0.00);//累计每日逾期利息
						tpos.setPostdate(accTrdDailyVo.getCurDate());
						this.tposMapper.updateByPrimaryKey(tpos);
	
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_OVERDUE);
						accTrdDaily.setAccAmt(dayCalAmt);
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
						
						//查询原交易数据
						productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
						//调用账务接口
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.INTEREST_OVERDUE, accTrdDaily.getAccAmt());
						InstructionPackage instructionPackage = new InstructionPackage();
						Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
						tradeMap.put("overdueDays", tpos.getOverdueDays());
						instructionPackage.setInfoMap(tradeMap);
						instructionPackage.setAmtMap(amtMap);
						String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期计提开始调用核算接口");
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"逾期计提分录id为："+flowId);
							accTrdDaily.setAcctNo(flowId);
							//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
						}
						accTrdDailies.add(accTrdDaily);
						accTrdDailyMapper.insert(accTrdDaily);
					}else{
						tpos.setOverdueDays(tpos.getOverdueDays()+1);
						this.tposMapper.updateByPrimaryKey(tpos);
					}
				}catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
	}

	//每日摊销
	@Override
	public void getNeedForAccDrawingDatesForAmorStepThree(String dealNo) {
		// TODO Auto-generated method stub
			String curWorkDate = dateService.getDayendDate();//当前工作日
			String nextWorkDate = dateService.getNextDayendDate();//下一个业务日
//			String nextWorkDate = CalendarService.getInstance().nextIBTradeDateString(curWorkDate);//dateService.getNextBizDate();//下一个业务日
			
			Map<String, Object> params =new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			params.put("curWorkDate", curWorkDate);//可以单笔跑，也可以批量
			params.put("nextWorkDate", nextWorkDate);//必须带入;
			List<TdAccTrdDailyAmorVo> accTrdDailyAmorVos = accTrdDailyAmorVoMapper.getTdAccTrdDailyAmorVos(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"需摊销交易条数为:" + accTrdDailyAmorVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdTrdTpos tpos=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyAmorVo accTrdDailyAmorVo : accTrdDailyAmorVos)
			{
				try{
					if(accTrdDailyAmorVo.getActAmorAmt()==0) {continue;}
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyAmorVo.getDealNo());
					accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_AMORDAILY);
					accTrdDaily.setAccAmt(accTrdDailyAmorVo.getActAmorAmt());//摊销金额
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyAmorVo.getCurDate());
					accTrdDailies.add(accTrdDaily);
					
					/*****************TPOS*************************/
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyAmorVo.getDealNo());
					tpos.setVersion(accTrdDailyAmorVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					tpos.setAmorTint(PlaningTools.add(tpos.getAmorTint(), accTrdDaily.getAccAmt()));//累计每日逾期计提
					tpos.setUnamorInt(PlaningTools.sub(tpos.getUnamorInt(), accTrdDaily.getAccAmt()));//累计每日逾期利息
					tpos.setPostdate(accTrdDailyAmorVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
	
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyAmorVo.getDealNo()+"摊销金额为："+accTrdDaily.getAccAmt());
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_AMORDAILY, accTrdDaily.getAccAmt());
					InstructionPackage instructionPackage = new InstructionPackage();
					Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
					instructionPackage.setInfoMap(tradeMap);
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyAmorVo.getDealNo()+"摊销开始调用核算接口");
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyAmorVo.getDealNo()+"摊销分录id为："+flowId);
						accTrdDaily.setAcctNo(flowId);
						//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
					}
					accTrdDailyMapper.insert(accTrdDaily);
				}catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		
	}

	@Override
	public void getNeedForAccDrawingDataFees() {
		// TODO Auto-generated method stub
//		try{
//			String nextWorkDate = dateService.getNextBizDate();//下一个业务日
			//String currWorkDate = dateService.getDayendDate();//业务日
			String nextWorkDate = dateService.getNextDayendDate();//dateService.getNextBizDate();//下一个业务日
	
			Map<String, Object> params =  new HashMap<String, Object>();
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getTdAccTrdDailyVosForFeeDaily(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"需费用计提交易条数为:" + accTrdDailyVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdProductFeeDeal productFeeDeal=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					accTrdDaily.setAccType(accTrdDailyVo.getAcctype());
					double dayCalAmt = 0.00;
					//处于期限之中的每日计提算法=（T+1)-(T)
					//判断  当前账务日期与下一个账务日期之间的差值  
					int days = PlaningTools.daysBetween(accTrdDailyVo.getCurDate(), nextWorkDate);
					if(days<=0) {continue;}//表示日期带入不正确，直接跳过该笔业务
					for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
					dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(), PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator(),2)
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(accTrdDailyVo.getRefBeginDate(),PlaningTools.addOrSubMonth_Day(accTrdDailyVo.getCurDate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(accTrdDailyVo.getSettlAmt(),accTrdDailyVo.getExecRate())
												),DayCountBasis.equal(accTrdDailyVo.getBasis()).getDenominator(),2)				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
					
					}
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"费用计提为："+dayCalAmt);
					accTrdDaily.setAccAmt(dayCalAmt);
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
					/*****************FEE DEAL*************************/
					productFeeDeal = new TdProductFeeDeal();
					productFeeDeal.setFeeDealNo(accTrdDailyVo.getFeeDealNo());
					productFeeDeal.setDealNo(accTrdDailyVo.getDealNo());
					productFeeDeal=productFeeDealMapper.getTdProductFeeDealByFeeDealNo(BeanUtil.beanToMap(productFeeDeal));
					productFeeDeal.setAccruedTint(PlaningTools.add(productFeeDeal.getAccruedTint(), dayCalAmt));//累计每日计提
					productFeeDeal.setActualTint(PlaningTools.add(productFeeDeal.getActualTint(), dayCalAmt));//累计每日利息
					this.productFeeDealMapper.updateTdProductFeeDealByFeeDealNo(BeanUtil.beanToMap(productFeeDeal));
					if(productFeeDeal.getAccruedTint()<=0) {continue;}//负数的时候不再出计提
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					if(productFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeBefore)){
						amtMap.put(DurationConstants.AccType.INTEREST_FEE_AMORDAILY, accTrdDaily.getAccAmt());
					}else{
						amtMap.put(DurationConstants.AccType.INTEREST_FEE_NORMAL, accTrdDaily.getAccAmt());
					}
					
					InstructionPackage instructionPackage = new InstructionPackage();
					Map<String, Object> beanMap = BeanUtil.beanToMap(productApproveMain);
					beanMap.put("intType", productFeeDeal.getFeeCtype());
					beanMap.put("feeType", productFeeDeal.getFeeType());
					instructionPackage.setInfoMap(beanMap);
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"费用计提开始调用核算接口");
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"费用计提分录id为："+flowId);
						accTrdDaily.setAcctNo(flowId);
					}
					accTrdDailies.add(accTrdDaily);
					accTrdDailyMapper.insert(accTrdDaily);
				}catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					//e.printStackTrace();
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
//		}catch (Exception e) {
//			e.printStackTrace();
//			LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
//			JY.debug(e.getMessage());
//		}
	}


	@Override
	public void getNeedForSettleDatasStepOne(String dealNo) {
		String WorkDate = dateService.getDayendDate();//当前业务日
		Map<String, Object> map =  new HashMap<String, Object>();
		map.put("settlDate", WorkDate);
		List<TtSettlDate> settlList = ttSettlDateMapper.TtSettlDatePageFromDict(map);
		if(settlList != null && settlList.size() > 0){//判断当前日期是否为结息日
			Map<String, Object> params =  new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getTdAccTrdDailyVosCurrent(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"需自动结息交易条数为:" + accTrdDailyVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdAccTrdDaily accTrdDaily  = null;TdTrdTpos tpos=null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					/*****************TPOS*************************/
					tpos = new TdTrdTpos();
					tpos.setDealNo(accTrdDailyVo.getDealNo());
					tpos.setVersion(accTrdDailyVo.getVersion());
					tpos = tposMapper.selectByPrimaryKey(tpos);
					
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					accTrdDaily.setAccType(DurationConstants.AccType.SETTL_INT);
					accTrdDaily.setAccAmt(tpos.getAccruedTint());
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());

					tpos.setInterestSettleAmt(PlaningTools.add(tpos.getInterestSettleAmt(), tpos.getAccruedTint()));//累计每日计提转为结息金额
					tpos.setAccruedTint(0);//累计每日计提清空为0
					tpos.setPostdate(accTrdDailyVo.getCurDate());
					this.tposMapper.updateByPrimaryKey(tpos);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"自动结息金额为："+accTrdDaily.getAccAmt());
					
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.SETTL_INT, accTrdDaily.getAccAmt());
					
					InstructionPackage instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"自动结息开始调用核算接口");
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+"自动结息分录id为："+flowId);
						accTrdDaily.setAcctNo(flowId);
					}
					accTrdDailies.add(accTrdDaily);
					accTrdDailyMapper.insert(accTrdDaily);
				}catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					//e.printStackTrace();
					JY.debug(e.getMessage());
				}
			}
		}
	}

	@Override
	public void getNeedFeeWriteOffDatesForOverdueStepOne(String dealNo) {
		// TODO Auto-generated method stub
//		try{
			Map<String, Object> params =  new HashMap<String, Object>();
			params.put("dealNo", dealNo);//可以单笔跑，也可以批量
			List<TdAccTrdDailyVo> accTrdDailyVos = accTrdDailyVoMapper.getFeeWriteOffDatesForOverdue(params);
			LogManager.getLogger(LogManager.MODEL_BATCH).info(dateService.getDayendDate()+"需逾期中收冲销的条数为:" + accTrdDailyVos.size());
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			TdProductFeeDeal tdProductFeeDeal = null;
			TdAccTrdDaily accTrdDaily  = null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			for(TdAccTrdDailyVo accTrdDailyVo : accTrdDailyVos)
			{
				try{
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(accTrdDailyVo.getDealNo());
					accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_FEE_OVERDUE);
					accTrdDaily.setAccAmt(accTrdDailyVo.getSettlAmt());
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(accTrdDailyVo.getCurDate());
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+",费用类型为："+accTrdDailyVo.getPrdNo()+"的待冲销金额为："+accTrdDaily.getAccAmt());
					if(accTrdDaily.getAccAmt()<=0) {continue;}//计提金额小于等于0 ，则不需要冲销
					
					Map<String, Object> map1 =  new HashMap<String, Object>();
					map1.put("feeDealNo", accTrdDailyVo.getFeeDealNo());
					tdProductFeeDeal = productFeeDealMapper.getTdProductFeeDealByFeeDealNo(map1);
					tdProductFeeDeal.setAccruedTint(0.00);//累计每日计提
					tdProductFeeDeal.setActualTint(0.00);//累计每日利息
					this.productFeeDealMapper.updateTdProductFeeDealByFeeDealNo(BeanUtil.beanToMap(tdProductFeeDeal));
					
					//查询原交易数据
					productApproveMain = productApproveMainMapper.getProductApproveMainActivated(accTrdDaily.getDealNo());
					Map<String, Object> feeInfoMap = new HashMap<String, Object>();
					feeInfoMap = BeanUtil.beanToMap(productApproveMain);
					feeInfoMap.put("overdueDays", "1");// 逾期第一天
					feeInfoMap.put("intType", tdProductFeeDeal.getFeeCtype());
					feeInfoMap.put("feeType", tdProductFeeDeal.getFeeType());
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					amtMap.put(DurationConstants.AccType.INTEREST_FEE_OVERDUE, accTrdDaily.getAccAmt());
					
					InstructionPackage instructionPackage = new InstructionPackage();
					instructionPackage.setInfoMap(feeInfoMap);
					instructionPackage.setAmtMap(amtMap);
					String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+",费用类型为："+accTrdDailyVo.getPrdNo()+"计提冲销开始调用核算接口");
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+accTrdDailyVo.getDealNo()+",费用类型为："+accTrdDailyVo.getPrdNo()+"计提冲销分录id为："+flowId);
						accTrdDaily.setAcctNo(flowId);
					}
					accTrdDailies.add(accTrdDaily);
					accTrdDailyMapper.insert(accTrdDaily);
				}catch (Exception e) {
					LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
					//e.printStackTrace();
					JY.debug(e.getMessage());
				}
			}
//			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
//		}catch (Exception e) {
//			LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
//			e.printStackTrace();
//			JY.debug(e.getMessage());
//		}
	}

	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))){
			return "td";
			}
		else {
			return "tm";
		}
	}
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))){
			return "com.singlee.capital.cashflow.mapper.Td";}
		else {
			return "com.singlee.capital.cashflow.mapper.Tm";}
	}
	
	@Override
	public void getNeedForMtmDatasStepOne(String dealNo) {
		try{
			String currentWorkDate = dateService.getDayendDate();//工作日
			String nextWorkDate = dateService.getNextDayendDate();//下一个自然日
			int days = PlaningTools.daysBetween(currentWorkDate, nextWorkDate);
			Map<String, Object> map_1 = new HashMap<String, Object>();
			List<TdTrdMtm> tdTrdMtms = null;
			
			TdAccTrdDaily accTrdDaily  = null;
			TdProductApproveMain productApproveMain= null;
			Map<String, Object> amtMap = null;
			String valDate = null;
			String flowId = null;
			InstructionPackage instructionPackage = null;
			for(int i = 0 ; i < days ; i++)
			{
				valDate = PlaningTools.addOrSubMonth_Day(currentWorkDate, Frequency.DAY, i);
				map_1.put("valDate", valDate);
				map_1.put("dealNo", dealNo);
				tdTrdValueAssessmentMapper.createValueAssessmentByDate(map_1);
				
				tdTrdMtmMapper.deleteMtmByDate(map_1);
				tdTrdMtmMapper.createMtmByDate(map_1);
				
				//当日公允价值
				tdTrdMtms = tdTrdMtmMapper.selectTdTradMtm(map_1);
				for (TdTrdMtm tdTrdMtm : tdTrdMtms) 
				{
					if(tdTrdMtm.getValDiff() == 0){
						continue;
					}
					try{
						//查询原交易数据
						productApproveMain = productApproveMainMapper.getProductApproveMainActivated(tdTrdMtm.getDealNo());
						
						//当日公允价值
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(tdTrdMtm.getDealNo());
						accTrdDaily.setAccType(DurationConstants.AccType.MTM_VAL);
						accTrdDaily.setAccAmt(tdTrdMtm.getValDiff());
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(valDate);
						
						//调用账务接口
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.MTM_VAL, accTrdDaily.getAccAmt());
						
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo()+"公允价值开始调用核算接口");
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo()+"公允价值分录id为："+flowId);
							accTrdDaily.setAcctNo(flowId);
						}
						accTrdDailyMapper.insert(accTrdDaily);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo());
					} catch (Exception e) {
						e.printStackTrace();
						LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
						JY.debug(e.getMessage());
					}
					
				}// end for
				
				//冲销前一日估值
				tdTrdMtms = tdTrdMtmMapper.selectTdTradRevMtm(map_1);
				for (TdTrdMtm tdTrdMtm : tdTrdMtms) 
				{
					if(tdTrdMtm.getValDiffYstd() == 0){
						continue;
					}
					try{
						//查询原交易数据
						productApproveMain = productApproveMainMapper.getProductApproveMainActivated(tdTrdMtm.getDealNo());
						
						//冲销前一日估值
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(tdTrdMtm.getDealNo());
						accTrdDaily.setAccType(DurationConstants.AccType.REVMTM_VAL);
						accTrdDaily.setAccAmt(tdTrdMtm.getValDiffYstd());
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(valDate);
						
						//调用账务接口
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.REVMTM_VAL, accTrdDaily.getAccAmt());
						
						instructionPackage = new InstructionPackage();
						instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
						instructionPackage.setAmtMap(amtMap);
						flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo()+"冲销前一日估值开始调用核算接口");
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo()+"冲销前一日估值分录id为："+flowId);
							accTrdDaily.setAcctNo(flowId);
						}
						accTrdDailyMapper.insert(accTrdDaily);
						
						LogManager.getLogger(LogManager.MODEL_BATCH).info("交易编号为："+tdTrdMtm.getDealNo());
					} catch (Exception e) {
						e.printStackTrace();
						LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
						JY.debug(e.getMessage());
					}
					
				}// end for
			}// end for days
			
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.getLogger(LogManager.MODEL_BATCH).info(e.getMessage());
			JY.debug(e.getMessage());
		}
	}
	
}
