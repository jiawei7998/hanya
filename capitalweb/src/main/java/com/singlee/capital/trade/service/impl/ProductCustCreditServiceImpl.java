package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.participant.mapper.TcProductParticipantMapper;
import com.singlee.capital.base.participant.model.TcProductParticipant;
import com.singlee.capital.base.trade.mapper.TdProductParticipantMapper;
import com.singlee.capital.base.trade.model.TdProductParticipant;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;
import com.singlee.capital.trade.mapper.TdProductApproveOverDraftMapper;
import com.singlee.capital.trade.mapper.TdProductCustCreditMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductCustCredit;
import com.singlee.capital.trade.service.ProductCustCreditService;
import com.singlee.capital.users.mapper.TdCustIbMapper;
import com.singlee.capital.users.model.TdCustIb;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductCustCreditServiceImpl implements ProductCustCreditService {
	@Autowired
	private TdProductCustCreditMapper productCustCreditMapper;
	@Autowired
	private TcProductParticipantMapper participantMapper;
	@Autowired
	private TdProductParticipantMapper tdProductParticipantMapper;//查询交易所带的参与方信息列表
	@Autowired
	private TdCustIbMapper custIbMapper;
	@Autowired
	private TdProductApproveOverDraftMapper productApproveOverDraftMapper;
	@Override
	public List<TdProductCustCredit> getTdProductCustCreditList(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return productCustCreditMapper.getTdProductCustCreditList(params);
	}
	@Override
	public List<TdProductCustCredit> getProductCustCreditForDeal(TdProductApproveMain productApproveMain,Map<String, String> tdSubsMap) throws Exception {
		// TODO Auto-generated method stub
		//首先取产品对应的参与方列表
		Map<String,Object>  quotaMap = new HashMap<String, Object>();
		List<TcProductParticipant>  tcProductParticipants = participantMapper.getProductParticipantListByPrdNo(BeanUtil.beanToMap(productApproveMain));
		tdSubsMap.put("lcType", productApproveMain.getLcType());
		quotaMap.put("quotaMap", tdSubsMap);
		List<TdProductCustCredit> productCustCredits = new ArrayList<TdProductCustCredit>();
		TdProductCustCredit productCustCredit = null;
		for(TcProductParticipant participant : tcProductParticipants){
			if(null!=participant.getJudgeQexp() && !"".equals(participant.getJudgeQexp())){//需要扣减额度
				if(calcBkIndex(quotaMap,participant.getJudgeQexp())){
					productCustCredit = new TdProductCustCredit();
					productCustCredit.setDealNo(productApproveMain.getDealNo());
					productCustCredit.setCreditFlag("0");
					productCustCredit.setCreditType("0");
					productCustCredit.setPartyId(tdSubsMap.get(participant.getPartyId()));
					productCustCredit.setOrgCcy(productApproveMain.getCcy());
					productCustCredit.setCcy(DictConstants.Currency.CNY);
					if(productApproveMain.getDealType().equalsIgnoreCase(DictConstants.DealType.Approve)){
						if(productApproveMain.getCcy().equalsIgnoreCase(DictConstants.Currency.CNY)){
							productCustCredit.setRate(1);
						}else {
							productCustCredit.setRate(productApproveMain.getApproveRate8());
						}
					}else {
						if(productApproveMain.getCcy().equalsIgnoreCase(DictConstants.Currency.CNY)){
							productCustCredit.setRate(1);
						}else {
							productCustCredit.setRate(productApproveMain.getVerifyRate8());
						}
					}
					//判定法人关系;子机构额度占用需要寻址到所属一级法人
					TdCustIb custIb = custIbMapper.getCustIbFcciCodeByEcifNo(StringUtils.trimToEmpty(tdSubsMap.get(participant.getPartyId()+"_partyCode")));
					if(null != custIb && null != StringUtils.trimToNull(custIb.getFcciCode()))
					{
						productCustCredit.setCustmerCode(custIb.getFcciCode());
						productCustCredit.setCnName(custIb.getCnName());
					}else{
						productCustCredit.setCustmerCode(tdSubsMap.get(participant.getPartyId()+"_partyCode"));
						productCustCredit.setCnName(tdSubsMap.get(participant.getPartyId()+"_partyName"));
					}
					//同业投资处理
					if(productApproveMain.getPrdNo()==906){
						TdProductApproveOverDraft productApproveOverDraft = new TdProductApproveOverDraft();
						productApproveOverDraft.setDealNo(productApproveMain.getDealNo());
						productApproveOverDraft.setVersion(productApproveMain.getVersion());
						productApproveOverDraft = productApproveOverDraftMapper.selectByPrimaryKey(productApproveOverDraft);
						if(null != productApproveOverDraft) {
                            productCustCredit.setCreditAmt(productApproveOverDraft.getEnableAmt());//启用额度
                        } else {
							productCustCredit.setCreditAmt(0.0);
						}
					}else{
						//存在汇率折算的情况
						productCustCredit.setCreditAmt(PlaningTools.mul(productApproveMain.getAmt(),productCustCredit.getRate()));
					}
					productCustCredits.add(productCustCredit);
				}
			}
			
		}
		
		return productCustCredits;
	}

	
	private static ScriptEngineManager manager = new ScriptEngineManager(); 
	private static ScriptEngine engine;
	/**
	 * 计算记账指标
	 * @param map
	 * @param expression
	 * @return
	 * @throws Exception 
	 */
	public static boolean calcBkIndex(Map<String, Object> map, String expression) throws Exception{
		if(engine == null) {
            engine =  manager.getEngineByName("js");
        }
		
		for(String key:map.keySet()){
			engine.put(key, map.get(key));
		}
		try {
			Object val = engine.eval(expression);
			return Boolean.parseBoolean(val.toString());
		} catch (Exception e) {
			throw e;
		}
	}
	@Override
	public boolean getProductJudgeAccessLevel(Map<String, Object> params) {
		// TODO Auto-generated method stub
		//params  必须含有  dealNo  prdNo 
		List<TdProductParticipant> participants = tdProductParticipantMapper.getTdProductParticipantsByDealno(params);//根据交易号获得
		boolean returnFlag = true;
		//循环参与方，查看有accessLevel的记录
		for(TdProductParticipant participant:participants){
			if(StringUtils.isNotEmpty(participant.getAccessLevel()) && StringUtils.isNotBlank(participant.getAccessLevel()))
			{
				//当准入不为空的时候  需要获得该参与方配置所需要的准入级别
				//SELECT * FROM TC_PRODUCT_PARTICIPANT WHERE PRD_NO = #{prdNo} AND PARTY_ID = #{partyId} ORDER BY PRD_NO,SORT_NO
				params.put("partyId", participant.getPartyId());
				List<TcProductParticipant> productParticipantList = participantMapper.getProductParticipantByPrdNoAndPartyId(params);
				if(productParticipantList == null || productParticipantList.isEmpty()) {
					JY.raiseRException(participant.getPartyId()+"关系不存在");
				}
				TcProductParticipant tcProductParticipant = productParticipantList.get(0);//有肯定唯一
				//1	A级 2	B级 3	C级 4	临时
				if(Integer.parseInt(participant.getAccessLevel())>tcProductParticipant.getJudgeAlkey())//说明准入级别不够
				{
					returnFlag = false;
					break;
				}
			}
		}
		//默认直通
		return returnFlag;
	}
	@Override
	public List<EdInParams> getEdInParamsForProductCustCredit(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		
		return productCustCreditMapper.getEdInParamsForProductCustCredit(params);
	}
}
