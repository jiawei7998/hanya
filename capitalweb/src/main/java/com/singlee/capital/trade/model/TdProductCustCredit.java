package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * 额度审批过程中的迁移
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_PRODUCT_CUST_CREDIT")
public class TdProductCustCredit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dealNo;//交易编号
	private String custmerCode;//客户ECIF号
	private String cnName;//客户名称
	private double creditAmt;//额度金额
	private String creditFlag;//是否处理 1-已发生  2-未发生
	private String creditType;//额度类型 0-初始 1-预占用  2-实际占用
	
	private String partyId;//参与方类型
	
	private String ccy;//币种
	
	private double rate;//利率
	
	private String orgCcy;//原币
	
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getOrgCcy() {
		return orgCcy;
	}
	public void setOrgCcy(String orgCcy) {
		this.orgCcy = orgCcy;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getCustmerCode() {
		return custmerCode;
	}
	public void setCustmerCode(String custmerCode) {
		this.custmerCode = custmerCode;
	}
	public String getCnName() {
		return cnName;
	}
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
	public double getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(double creditAmt) {
		this.creditAmt = creditAmt;
	}
	public String getCreditFlag() {
		return creditFlag;
	}
	public void setCreditFlag(String creditFlag) {
		this.creditFlag = creditFlag;
	}
	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	
	
}
