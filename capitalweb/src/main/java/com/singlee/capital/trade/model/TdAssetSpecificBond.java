package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="TD_ASSET_SPECIFIC_BOND")
public class TdAssetSpecificBond implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_SPECIFIC_BOND.NEXTVAL FROM DUAL")
	private String assetId;
	private String dealNo;
	private int version;
	private double faceAmt;
	private String baseAssetname;
	private double custodFeerate;
	private String isMecustod;
	private double manageFeerate;
	private double faceRate;
	private double aloneFeerate;
	private String faceVdate;
	private String faceMdate;
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public double getFaceAmt() {
		return faceAmt;
	}
	public void setFaceAmt(double faceAmt) {
		this.faceAmt = faceAmt;
	}
	public String getBaseAssetname() {
		return baseAssetname;
	}
	public void setBaseAssetname(String baseAssetname) {
		this.baseAssetname = baseAssetname;
	}
	public double getCustodFeerate() {
		return custodFeerate;
	}
	public void setCustodFeerate(double custodFeerate) {
		this.custodFeerate = custodFeerate;
	}
	public String getIsMecustod() {
		return isMecustod;
	}
	public void setIsMecustod(String isMecustod) {
		this.isMecustod = isMecustod;
	}
	public double getManageFeerate() {
		return manageFeerate;
	}
	public void setManageFeerate(double manageFeerate) {
		this.manageFeerate = manageFeerate;
	}
	public double getFaceRate() {
		return faceRate;
	}
	public void setFaceRate(double faceRate) {
		this.faceRate = faceRate;
	}
	public double getAloneFeerate() {
		return aloneFeerate;
	}
	public void setAloneFeerate(double aloneFeerate) {
		this.aloneFeerate = aloneFeerate;
	}
	public String getFaceVdate() {
		return faceVdate;
	}
	public void setFaceVdate(String faceVdate) {
		this.faceVdate = faceVdate;
	}
	public String getFaceMdate() {
		return faceMdate;
	}
	public void setFaceMdate(String faceMdate) {
		this.faceMdate = faceMdate;
	}
	@Override
	public String toString() {
		return "TdAssetSpecificBond [dealNo=" + dealNo + ", version=" + version
				+ ", faceAmt=" + faceAmt + ", baseAssetname=" + baseAssetname
				+ ", custodFeerate=" + custodFeerate + ", isMecustod="
				+ isMecustod + ", manageFeerate=" + manageFeerate
				+ ", faceRate=" + faceRate + ", aloneFeerate=" + aloneFeerate
				+ ", faceVdate=" + faceVdate + ", faceMdate=" + faceMdate + "]";
	}
	

}
