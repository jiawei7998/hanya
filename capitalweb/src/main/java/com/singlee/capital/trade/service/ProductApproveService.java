package com.singlee.capital.trade.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.FinEnterpriseTrade;
import com.singlee.capital.trade.model.TdDurationCommon;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveProtocols;
import com.singlee.capital.trade.model.TdProductApproveSub;

/**
 * @projectName 同业业务管理系统
 * @className 产品审批服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-10 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface ProductApproveService {

	/**
	 * 根据交易单号取当前生效的交易单
	 * 
	 * @param dealNo - 产品审批主表主键
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public TdProductApproveMain getProductApproveForDuration(Map<String, Object> params);
	
	/**
	 * 审批单撤销
	 * @param params
	 * @return
	 */
	public TdProductApproveMain getProductApproveForCancel(Map<String, Object> params);
	
	/**
	 * 根据交易单号取当前生效的交易单
	 * 
	 * @param dealNo - 产品审批主表主键
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public TdProductApproveMain getProductApproveActivated(String dealNo);
	
	/**
	 * 根据条件查询审批单
	 * 
	 * @params params - 请求参数 
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public TdProductApproveMain getProductApproveByCondition(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public Page<TdProductApproveMain> getProductApprovePage( Map<String, Object> params, int isFinished);
	//public Page<TdProductApproveMain> getProductAppPage(Map<String, Object> params);
	
	/**
	 * 查询审批通过 状态为6的单据 用于撤销筛选
	 * @param params
	 * @return
	 */
	public Page<TdProductApproveMain> getApproveOrderVoForCancelPage(Map<String, Object> params);	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public void createProductApprove(Map<String, Object> params);
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public void updateProductApprove(Map<String, Object> params);
	
	/**
	 * 更新子表
	 * @param productApproveMain
	 */
	public void updateProductApproveSub(TdProductApproveMain productApproveMain);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 产品审批主表主键列表
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public void deleteProductApprove(String[] dealNos);
	
	/**
	 * 导入
	 * 
	 * @param dealNos - 产品审批主表主键列表
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public void importProductApprove();
	
	public void updateProductApproveMain(TdProductApproveMain main);
	/**
	 * 更新金融工具表版本更新
	 */
	public void updateProductApproveMainVersion(TdProductApproveMain main);
	
	/**
	 * 根据交易单号h和版本号查询对象数据
	 * 
	 * @param params - dealNo sort order
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-9-26
	 */
	public TdProductApproveMain getProductApproveMainByCondition(Map<String, Object> params);
	
	/**
	 * 单纯的修改main数据
	 * @param approveMain
	 * @return
	 */
	public int modifyProductApproveMain(TdProductApproveMain approveMain);
	
	/**
	 * 获取应收利息
	 * @param params
	 * @return
	 */
	public double getNeedInterest(Map<String, Object> params);
	
	/**
	 * 计算投资收益率或者转让金额
	 * @param params
	 * @return
	 */
	public double computeYiedOrFullPrice(Map<String,Object> params);
	

	/**
	 * 计算到期利息金额
	 * @param main
	 * @return
	 */
	public double computeMInterest(TdProductApproveMain main);
	
	/**
	 * 计算期限天数
	 * @param main
	 * @return
	 */
	public int computeTimeLimit(TdProductApproveMain main) throws ParseException;
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 存续期公共对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public List<TdDurationCommon> getDurationCommonList(Map<String, Object> params);
	
	/**
	 * 更新金融工具表引用编号
	 */
	public int updateMainRefNo(TdProductApproveMain main);
	
	/**
	 * CRMS资产端的产品
	 * @param params
	 */
	public void createCrmsProductApprove(Map<String, Object> params);
	public void updateCrmsProductApprove(Map<String, Object> params);
	
	/**
	 * 创建负债端的产品
	 * @param params
	 */
	public void createLiabilitiesProductApprove(Map<String, Object> params);
	public void updateLiabilitiesProductApprove(Map<String, Object> params);
	
	/**
	 * CRMS资产端的产品
	 * @param params
	 */
	public void createOverDraftProductApprove(Map<String, Object> params);
	public void updateOverDraftProductApprove(Map<String, Object> params);
	
	/**
	 * 根据DEALNO查询
	 * @param dealNo
	 * @return
	 */
	public List<TdProductApproveProtocols> getTdProductApproveProtocolsForDealNo(String dealNo);
	/**
	 * 保存额度占用信息
	 * @param dealNo
	 * @return
	 */
	public int saveTdProductCustCredit(Map<String,Object> map);	
	/**
	 * 查询CRMS企信融贷通审批交易发送数据
	 * @return
	 * @throws Exception
	 */
	public FinEnterpriseTrade selectFinEnterpriseApproveTrade(Map<String, Object> map) throws Exception;
	
	/**
	 * Ld号修改
	 * @param params
	 */
	public void updateDurationTradeLdno(Map<String, Object> params);
	
	public List<TdProductApproveSub> getProductApproveSubByDealno(Map<String, Object> params) throws Exception;
	
	/**
	 * 查询本机构、下属机构、业务辖区机构中的业务
	 */
	public Page<TdProductApproveMain> getProductApprovePageByInst(Map<String, Object> params);
	
	/**
	 * 查询产品是否需要放款
	 */
	public boolean getProductIsNeedLoan(String prd_no);
	
	/**
	 * 查询产品送交行领导金额线
	 */
	public double getProductAmtLine(String prd_no, String ccy);
	/**
	 * 委外业业务查询
	 */
	public Page<TdProductApproveMain> getProductAppPage( Map<String, Object> params);
	
	/**
	 * 检查产品编号唯一性
	 */
	public boolean checkProductCode(Map<String, Object> map);
	
	public Map<String, String> queryUserParam(Map<String, Object> map);
	
	public int getProductApproveMainListCount(Map<String, Object> params);
	
}
