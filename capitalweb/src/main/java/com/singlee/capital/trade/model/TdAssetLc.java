package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产信用证
 * @description TODO
 * @author 
 * @createDate
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_ASSET_LC")
public class TdAssetLc implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1146739332851746203L;
	@Id
	private String dealNo;//交易单号 自动生成
	@Id
	private String lcId;//信用证编号-
	private double lcAmt;//信用证金额-
	private String lcApprover;//信用证申请人-
	private String appliBank;//开证行全称
	private String appliContdate;//开证行确认付款日期
	private double appliContamt;//开证行确认付款金额
	private double lcSellIamt;//信用证转让利息-
	private String lcBenefier;//信用证受益人-
	private String lcIssueDate;//信用证开证日-
	private String lcSellBank;//信用证出让行-
	private String lcMdate;//信用证到期日-
	private int lcCalIdays;//信用证计息天数-
	private int lcGraceDays;//信用证宽限期-
	private double lcSellProcamt;//受益权转让价款-
	private String platEarmark;//平台投向
	private String investFinal;//最终投向
	private String indusEarmark;//行业投向
	private String lcCalMdate;//信用证计息到期日-
	private String lcCalVdate;//信用证计息起息日-
	private double lcSellRate;//信用证出让利率_%
	
	public double getLcSellRate() {
		return lcSellRate;
	}
	public void setLcSellRate(double lcSellRate) {
		this.lcSellRate = lcSellRate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	
	public String getLcId() {
		return lcId;
	}
	public void setLcId(String lcId) {
		this.lcId = lcId;
	}
	public double getLcAmt() {
		return lcAmt;
	}
	public void setLcAmt(double lcAmt) {
		this.lcAmt = lcAmt;
	}
	public String getLcApprover() {
		return lcApprover;
	}
	public void setLcApprover(String lcApprover) {
		this.lcApprover = lcApprover;
	}
	public String getAppliBank() {
		return appliBank;
	}
	public void setAppliBank(String appliBank) {
		this.appliBank = appliBank;
	}
	public String getAppliContdate() {
		return appliContdate;
	}
	public void setAppliContdate(String appliContdate) {
		this.appliContdate = appliContdate;
	}
	public double getAppliContamt() {
		return appliContamt;
	}
	public void setAppliContamt(double appliContamt) {
		this.appliContamt = appliContamt;
	}
	public double getLcSellIamt() {
		return lcSellIamt;
	}
	public void setLcSellIamt(double lcSellIamt) {
		this.lcSellIamt = lcSellIamt;
	}
	public String getLcBenefier() {
		return lcBenefier;
	}
	public void setLcBenefier(String lcBenefier) {
		this.lcBenefier = lcBenefier;
	}
	public String getLcIssueDate() {
		return lcIssueDate;
	}
	public void setLcIssueDate(String lcIssueDate) {
		this.lcIssueDate = lcIssueDate;
	}
	public String getLcSellBank() {
		return lcSellBank;
	}
	public void setLcSellBank(String lcSellBank) {
		this.lcSellBank = lcSellBank;
	}
	public String getLcMdate() {
		return lcMdate;
	}
	public void setLcMdate(String lcMdate) {
		this.lcMdate = lcMdate;
	}
	public int getLcCalIdays() {
		return lcCalIdays;
	}
	public void setLcCalIdays(int lcCalIdays) {
		this.lcCalIdays = lcCalIdays;
	}
	
	public double getLcSellProcamt() {
		return lcSellProcamt;
	}
	public void setLcSellProcamt(double lcSellProcamt) {
		this.lcSellProcamt = lcSellProcamt;
	}
	public String getPlatEarmark() {
		return platEarmark;
	}
	public void setPlatEarmark(String platEarmark) {
		this.platEarmark = platEarmark;
	}
	public String getInvestFinal() {
		return investFinal;
	}
	public void setInvestFinal(String investFinal) {
		this.investFinal = investFinal;
	}
	public String getIndusEarmark() {
		return indusEarmark;
	}
	public void setIndusEarmark(String indusEarmark) {
		this.indusEarmark = indusEarmark;
	}
	
	public int getLcGraceDays() {
		return lcGraceDays;
	}
	public void setLcGraceDays(int lcGraceDays) {
		this.lcGraceDays = lcGraceDays;
	}
	public String getLcCalMdate() {
		return lcCalMdate;
	}
	public void setLcCalMdate(String lcCalMdate) {
		this.lcCalMdate = lcCalMdate;
	}
	public String getLcCalVdate() {
		return lcCalVdate;
	}
	public void setLcCalVdate(String lcCalVdate) {
		this.lcCalVdate = lcCalVdate;
	}
	@Override
	public String toString() {
		return "TdAssetLc [dealNo=" + dealNo + ", lcId=" + lcId + ", lcAmt="
				+ lcAmt + ", lcApprover=" + lcApprover + ", appliBank="
				+ appliBank + ", appliContdate=" + appliContdate
				+ ", appliContamt=" + appliContamt + ", lcSellIamt="
				+ lcSellIamt + ", lcBenefier=" + lcBenefier + ", lcIssueDate="
				+ lcIssueDate + ", lcSellBank=" + lcSellBank + ", lcMdate="
				+ lcMdate + ", lcCalVdate=" + lcCalVdate + ", lcCalIdays="
				+ lcCalIdays + ", lcGraceDays=" + lcGraceDays
				+ ", lcSellProcamt=" + lcSellProcamt + ", platEarmark="
				+ platEarmark + ", investFinal=" + investFinal
				+ ", indusEarmark=" + indusEarmark + "]";
	}
	
	
	
}
