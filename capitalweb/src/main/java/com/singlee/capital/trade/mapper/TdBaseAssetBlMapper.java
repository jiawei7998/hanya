package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAssetBl;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产持久层-保理收款权
 * 

 * @version 1.0
 */
public interface TdBaseAssetBlMapper extends Mapper<TdBaseAssetBl> {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 */
	public Page<TdBaseAssetBl> getTdBaseAssetBlLists(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 */
	public List<TdBaseAssetBl> getTdBaseAssetBlList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 */
	public void deleteTdBaseAssetBlByDealNo(String dealNo);
	
	public void insertTdBaseAssetBlForCopy(Map<String, Object> params);
}