package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdApproveCancel;
import com.singlee.capital.trade.model.TdCashFlowChange;
import com.singlee.capital.trade.model.TdFeeHandleCashflow;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayChange;
import com.singlee.capital.trade.model.TdInterestSettle;
import com.singlee.capital.trade.model.TdInvFiipApprove;
import com.singlee.capital.trade.model.TdLiabilitiesDuration;
import com.singlee.capital.trade.model.TdMultiResale;
import com.singlee.capital.trade.model.TdOutrightSale;
import com.singlee.capital.trade.model.TdOverDueConfirm;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdRateChange;
import com.singlee.capital.trade.model.TdRateReset;
import com.singlee.capital.trade.model.TdTradeExpire;
import com.singlee.capital.trade.model.TdTradeExtend;
import com.singlee.capital.trade.model.TdTradeOffset;
import com.singlee.capital.trade.model.TdTransforSchedule;
import com.singlee.capital.trade.model.TdTransforScheduleFee;
import com.singlee.capital.trade.model.TdTransforScheduleInt;
import com.singlee.capital.trade.model.TmFeesPassagewy;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.AdvanceMaturityService;
import com.singlee.capital.trade.service.ApproveCancelService;
import com.singlee.capital.trade.service.CashFlowChangeService;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.LiabilitiesDurationService;
import com.singlee.capital.trade.service.MulResaleService;
import com.singlee.capital.trade.service.RateChangeService;
import com.singlee.capital.trade.service.RateResetService;
import com.singlee.capital.trade.service.TdFeeHandleCashflowService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TdInvFiipApproveService;
import com.singlee.capital.trade.service.TdOverDueConfirmService;
import com.singlee.capital.trade.service.TradeExpireService;
import com.singlee.capital.trade.service.TradeExtendService;
import com.singlee.capital.trade.service.TradeOffsetService;
import com.singlee.capital.trade.service.TradeOutrightSaleService;
import com.singlee.capital.trade.service.TransforScheduleFeeService;
import com.singlee.capital.trade.service.TransforScheduleIntService;
import com.singlee.capital.trade.service.TransforScheduleService;
/**
 * @projectName 同业业务管理系统
 * @className 存续期控制层
 * @description TODO
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/DurationController")
public class DurationController extends CommonController {
	
	@Autowired
	private AdvanceMaturityService advanceMaturityService;
	@Autowired
	private RateChangeService rateChangeService;
	@Autowired
	private CashFlowChangeService cashFlowChangeService;
	@Autowired
	private TradeExtendService tradeExtendService;
	@Autowired
	private TradeOutrightSaleService tradeOutrightSaleService;
	@Autowired
	private MulResaleService mulResaleService;
	@Autowired
	private ApproveCancelService approveCancelService;
	@Autowired
	private TradeOffsetService tradeOffsetService;
	@Autowired
	private LiabilitiesDurationService liabilitiesDurationService;
	
	@Autowired
	private TdOverDueConfirmService tdOverDueConfirmService;
	@Autowired
	private TransforScheduleService transforScheduleService;
	@Autowired
	private TransforScheduleIntService transforScheduleIntService;
	@Autowired
	private TransforScheduleFeeService transforScheduleFeeService;
	@Autowired
	private TdInvFiipApproveService invFiipApproveService;
	@Autowired
	private InterestSettleService interestSettleService;
	@Autowired
	private AdvanceMaturityCurrentService advanceMaturityCurrentService;
	@Autowired
	private TdFeeHandleCashflowService feeHandleCashflowService;
	@Autowired
	private TradeExpireService tradeExpireService;
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	@Autowired
	private RateResetService rateResetService;
	/**
	 * 条件查询列表(提前终止)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityFinished")
	public RetMsg<PageInfo<TdAdvanceMaturity>> searchPageAdvanceMaturityFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturity> page = advanceMaturityService.getAdvanceMaturityPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(提前终止)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityUnfinished")
	public RetMsg<PageInfo<TdAdvanceMaturity>> searchPageAdvanceMaturityNoFinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturity> page = advanceMaturityService.getAdvanceMaturityPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(提前终止)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityMine")
	public RetMsg<PageInfo<TdAdvanceMaturity>> searchPageAdvanceMaturityMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturity> page = advanceMaturityService.getAdvanceMaturityPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 条件查询列表(交易展期)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExtendFinished")
	public RetMsg<PageInfo<TdTradeExtend>> searchPageTradeExtendFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeExtend> page = tradeExtendService.getTradeExtendPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(交易展期)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExtendUnfinished")
	public RetMsg<PageInfo<TdTradeExtend>> searchPageTradeExtendNoFinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeExtend> page = tradeExtendService.getTradeExtendPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExtendByMyself")
	public RetMsg<PageInfo<TdTradeExtend>> searchPageTradeExtendByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeExtend> page = tradeExtendService.getTradeExtendPage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeExtend")
	public RetMsg<Serializable> removeTradeExtend(@RequestBody String[] dealNos) {
		tradeExtendService.deleteTradeExtend(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeAdvanceMaturity")
	public RetMsg<Serializable> removeProductApprove(@RequestBody String[] dealNos) {
		advanceMaturityService.deleteAdvanceMaturity(dealNos);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除提前支取
	 * @param dealNos
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/removeAdvanceMaturityCurrent")
	public RetMsg<Serializable>  removeAdvanceMaturityCurrent(@RequestBody String[] dealNos) {
		advanceMaturityCurrentService.deleteAdvanceMaturityCurrent(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(现金流变更未审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCashFlowChangeUnfinished")
	public RetMsg<PageInfo<TdCashFlowChange>> searchPageCashFlowChangeUnfinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdCashFlowChange> page = cashFlowChangeService.getCashFlowChangePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(现金流变更已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCashFlowChangeFinished")
	public RetMsg<PageInfo<TdCashFlowChange>> searchPageCashFlowChangeFinished(@RequestBody Map<String,Object> params){

		params.put("userId", SlSessionHelper.getUserId());
		Page<TdCashFlowChange> page = cashFlowChangeService.getCashFlowChangePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageCashFlowChangeByMyself")
	public RetMsg<PageInfo<TdCashFlowChange>> searchPageCashFlowChangeByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdCashFlowChange> page = cashFlowChangeService.getCashFlowChangePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除现金流变更
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeCashFlowChange")
	public RetMsg<Serializable> removeCashFlowChange(@RequestBody String[] dealNos) {
		cashFlowChangeService.deleteCashFlowChange(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdProductApproveMain")
	public TdProductApproveMain getTdProductApproveMain(@RequestBody Map<String,Object> params){
		TdProductApproveMain approveMain = advanceMaturityService.getProductApproveMainOfBefore(params);
		return approveMain;
	}
	
	/**
	 * 修改
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateAdvanceMaturity")
	public RetMsg<Serializable> updateAdvanceMaturity(@RequestBody Map<String,Object> map) {
		advanceMaturityService.updateAdvanceMaturity(map);
		return RetMsgHelper.ok();
	} 
	
	
	/**
	 * 条件查询列表(利率变更未审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRateChangeUnfinished")
	public RetMsg<PageInfo<TdRateChange>> searchPageRateChangeUnfinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdRateChange> page = rateChangeService.getRateChangePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(利率变更已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRateChangeFinished")
	public RetMsg<PageInfo<TdRateChange>> searchPageRateChangeFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdRateChange> page = rateChangeService.getRateChangePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(利率变我发起)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageRateChangeMySelf")
	public RetMsg<PageInfo<TdRateChange>> searchPageRateChangeMySelf(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdRateChange> page = rateChangeService.getRateChangePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除(利率变更)
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeRateChange")
	public RetMsg<Serializable> removeRateChange(@RequestBody String[] dealNos) {
		rateChangeService.deleteRateChange(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(资产卖断)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMultiResaleFinished")
	public RetMsg<PageInfo<TdMultiResale>> searchPageMultiResaleFinished(@RequestBody Map<String,Object> params){

		params.put("userId", SlSessionHelper.getUserId());
		Page<TdMultiResale> page = mulResaleService.getMulResalePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(资产卖断)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMultiResaleNoFinished")
	public RetMsg<PageInfo<TdMultiResale>> searchPageMultiResaleNoFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdMultiResale> page = mulResaleService.getMulResalePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)资产卖断
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageMultiResaleByMyself")
	public RetMsg<PageInfo<TdMultiResale>> searchPageMultiResaleByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdMultiResale> page = mulResaleService.getMulResalePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除现金流变更
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeMultiResale")
	public RetMsg<Serializable> removeMultiResale(@RequestBody String[] dealNos) {
		mulResaleService.deleteMulResale(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(交易展期)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageApproveCancelFinished")
	public RetMsg<PageInfo<TdApproveCancel>> searchPageApproveCancelFinished(@RequestBody Map<String,Object> params){

		params.put("userId", SlSessionHelper.getUserId());
		Page<TdApproveCancel> page = approveCancelService.getApproveCancelPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(审批单撤销)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageApproveCancelUnfinished")
	public RetMsg<PageInfo<TdApproveCancel>> searchPageApproveCancelNoFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdApproveCancel> page = approveCancelService.getApproveCancelPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageApproveCancelByMyself")
	public RetMsg<PageInfo<TdApproveCancel>> searchPageApproveCancelByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdApproveCancel> page = approveCancelService.getApproveCancelPage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeApproveCancel")
	public RetMsg<Serializable> removeApproveCancel(@RequestBody String[] dealNos) {
		approveCancelService.deleteApproveCancel(dealNos);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 条件查询列表(出账交易冲销)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOffsetFinished")
	public RetMsg<PageInfo<TdTradeOffset>> searchPageTradeOffsetFinished(@RequestBody Map<String,Object> params){

		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeOffset> page = tradeOffsetService.getTradeOffsetPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(出账交易冲销)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOffsetUnfinished")
	public RetMsg<PageInfo<TdTradeOffset>> searchPageTradeOffsetNoFinished(@RequestBody Map<String,Object> params){
		
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		Page<TdTradeOffset> page = tradeOffsetService.getTradeOffsetPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOffsetByMyself")
	public RetMsg<PageInfo<TdTradeOffset>> searchPageTradeOffsetByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeOffset> page = tradeOffsetService.getTradeOffsetPage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeOffset")
	public RetMsg<Serializable> removeTradeOffset(@RequestBody String[] dealNos) {
		tradeOffsetService.deleteTradeOffset(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 存续期原交易选择列表（有余额）
	 *//*
	@ResponseBody
	@RequestMapping(value = "/searchDurationCustomVoPage")
	public RetMsg<PageInfo<DurationCustomVo>> searchDurationCustomVoPage(@RequestBody Map<String,Object> params){
        String tradeId = ParameterUtil.getString(params, "trade_id", "");
        params.put("i_code", tradeId);
        boolean isHeaderInst =ParameterUtil.getBoolean(params, "isHeaderInst", false);
        if(!isHeaderInst){
        	params.put("spon_inst", SlSessionHelper.getInstitutionId());
        }
		Page<DurationCustomVo> page = capitalSettleService.pageDurationCustoms(params);
		return RetMsgHelper.ok(page);
	}*/
	
	/**
	 * 根据交易单号获取信息
	 * @param params
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(value = "/getDurationCustomVo")
	public RetMsg<DurationCustomVo> getDurationCustomVo(@RequestBody Map<String,Object> params){
		return RetMsgHelper.ok(capitalSettleService.getDurationCustomVoByDealno(params));
	}*/
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/ifExistDuration")
	public RetMsg<TdAdvanceMaturity> ifExistDuration(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(advanceMaturityService.ifExistDuration(map));
	}
	
	/**
	 * 查询是否已经存在未完成审批单撤销
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/ifExistApproveCancel")
	public RetMsg<TdApproveCancel> ifExistApproveCancel(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(approveCancelService.ifExistApproveCancel(map));
	}
	
	/**
	 * 条件查询列表(负债存续期)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageLiabilitiesDurationFinished")
	public RetMsg<PageInfo<TdLiabilitiesDuration>> searchPageLiabilitiesDurationFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdLiabilitiesDuration> page = liabilitiesDurationService.getLiabilitiesDurationPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(负债存续期)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageLiabilitiesDurationUnfinished")
	public RetMsg<PageInfo<TdLiabilitiesDuration>> searchPageLiabilitiesDurationNoFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdLiabilitiesDuration> page = liabilitiesDurationService.getLiabilitiesDurationPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(负债存续期)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageLiabilitiesDurationMine")
	public RetMsg<PageInfo<TdLiabilitiesDuration>> searchPageLiabilitiesDurationMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdLiabilitiesDuration> page = liabilitiesDurationService.getLiabilitiesDurationPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeLiabilitiesDuration")
	public RetMsg<Serializable> removeLiabilitiesDuration(@RequestBody String[] dealNos) {
		liabilitiesDurationService.deleteLiabilitiesDuration(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 计算返回应计利息
	 * 
	 * @param Map
	 * @author x220
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/getAmInt")
	public RetMsg<Double> getAmInt(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(advanceMaturityService.getAmInt(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAmIntCalForTrasforInterest")
	public RetMsg<Double> getAmIntCalForTrasforInterest(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(transforScheduleIntService.getAmIntCalForTrasforInterest(map));
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getAmIntCalForInterest")
	public RetMsg<Double> getAmIntCalForInterest(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(transforScheduleIntService.getAmIntCalForInterest(map));
	}
	/**
	 * 交易单号查询存续期交易
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-12-8
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDurationById")
	public RetMsg<Object> searchDurationById(@RequestBody Map<String,Object> params){
		String buziType = ParameterUtil.getString(params, "buziType", "");
		String dealNo = ParameterUtil.getString(params, "dealNo", "");
		Object obj = null;
		if ("利率变更".equals(buziType)) {
			obj = rateChangeService.getRateChangeById(dealNo);
		} else if ("现金流变更".equals(buziType)) {
			obj = cashFlowChangeService.getCashFlowChangeById(dealNo);
		} else if ("交易展期".equals(buziType)) {
			obj = tradeExtendService.getTradeExtendById(dealNo);
		} else if ("资产卖断".equals(buziType)) {
			obj = mulResaleService.getMulResaleById(dealNo);
		} else if ("提前到期".equals(buziType)) {
			obj = advanceMaturityService.getAdvanceMaturityById(dealNo);
		} else if ("出账交易冲销".equals(buziType)) {
			obj = tradeOffsetService.getTradeOffsetById(dealNo);
		}
		return RetMsgHelper.ok(obj);
	}
	
	/**
	 * 预生成中收业务信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createFeePlan")
	public RetMsg<Serializable> createFeePlan(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表(交易展期)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOverDueFinished")
	public RetMsg<PageInfo<TdOverDueConfirm>> searchPageTradeOverDueFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOverDueConfirm> page = tdOverDueConfirmService.getTradeOverDuePage(params,1);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 条件查询列表(交易展期)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOverDueUnfinished")
	public RetMsg<PageInfo<TdOverDueConfirm>> searchPageTradeOverDueUnfinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOverDueConfirm> page = tdOverDueConfirmService.getTradeOverDuePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOverDueByMyself")
	public RetMsg<PageInfo<TdOverDueConfirm>> searchPageTradeOverDueByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOverDueConfirm> page = tdOverDueConfirmService.getTradeOverDuePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeOverDue")
	public RetMsg<Serializable> removeTradeOverDue(@RequestBody String[] dealNos) {
		tdOverDueConfirmService.deleteTradeOverDue(dealNos);
		return RetMsgHelper.ok();
	}
	
	
	/**
	 * 条件查询列表(交易卖断)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightSaleFinished")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightSaleFinished(@RequestBody Map<String,Object> params){

		params.put("userId", SlSessionHelper.getUserId());
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(交易卖断)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightSaleUnfinished")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightSaleNoFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightSaleByMyself")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightSaleByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeOutRightSale")
	public RetMsg<Serializable> removeTradeOutRightSale(@RequestBody String[] dealNos) {
		tradeOutrightSaleService.deleteTdOutrightSale(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 计算逾期补记利息
	 * 
	 * @param Map
	 * @author x220
	 * @date 
	 */
	@ResponseBody
	@RequestMapping(value = "/getOverDueAmInt")
	public RetMsg<Double> getOverDueAmInt(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(tdOverDueConfirmService.calOverDueIntAmt(map));
	}
	
	
	/**************************************************************************/
	/**
	 * 条件查询列表(本金计划调整)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleFinished")
	public RetMsg<PageInfo<TdTransforSchedule>> searchPageTrasForProductCapitalScheduleFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforSchedule> page = transforScheduleService.getTransforSchedulePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(本金计划调整)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleUnfinished")
	public RetMsg<PageInfo<TdTransforSchedule>> searchPageTrasForProductCapitalScheduleNoFinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforSchedule> page = transforScheduleService.getTransforSchedulePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleByMyself")
	public RetMsg<PageInfo<TdTransforSchedule>> searchPageTrasForProductCapitalScheduleByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforSchedule> page = transforScheduleService.getTransforSchedulePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTrasForProductCapitalSchedule")
	public RetMsg<Serializable> removeTrasForProductCapitalSchedule(@RequestBody String[] dealNos) {
		transforScheduleService.deleteTransforSchedule(dealNos);
		return RetMsgHelper.ok();
	}
	/***************************************************************************/

	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleIntFinished")
	public RetMsg<PageInfo<TdTransforScheduleInt>> searchPageTrasForProductCapitalScheduleIntFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleInt> page = transforScheduleIntService.getTransforScheduleIntPage(params,1);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleIntUnfinished")
	public RetMsg<PageInfo<TdTransforScheduleInt>> searchPageTrasForProductCapitalScheduleIntNoFinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleInt> page = transforScheduleIntService.getTransforScheduleIntPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleIntByMyself")
	public RetMsg<PageInfo<TdTransforScheduleInt>> searchPageTrasForProductCapitalScheduleIntByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleInt> page = transforScheduleIntService.getTransforScheduleIntPage(params,3);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/removeTrasForProductCapitalScheduleInt")
	public RetMsg<Serializable> removeTrasForProductCapitalScheduleInt(@RequestBody String[] dealNos) {
		transforScheduleIntService.deleteTransforScheduleInt(dealNos);
		return RetMsgHelper.ok();
	}
	/***********************************************************************/
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTdInvFiipApproveFinished")
	public RetMsg<PageInfo<TdInvFiipApprove>> searchPageTdInvFiipApproveFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInvFiipApprove> page = invFiipApproveService.getTdInvFiipApprovePage(params,1);
		return RetMsgHelper.ok(page);
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageTdInvFiipApproveUnfinished")
	public RetMsg<PageInfo<TdInvFiipApprove>> searchPageTdInvFiipApproveUnfinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInvFiipApprove> page = invFiipApproveService.getTdInvFiipApprovePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTdInvFiipApproveByMyself")
	public RetMsg<PageInfo<TdInvFiipApprove>> searchPageTdInvFiipApproveByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInvFiipApprove> page = invFiipApproveService.getTdInvFiipApprovePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/removeTdInvFiipApprove")
	public RetMsg<Serializable> removeTdInvFiipApprove(@RequestBody String[] dealNos) {
		invFiipApproveService.deleteTdInvFiipApprove(dealNos);
		return RetMsgHelper.ok();
	}
	
	
	/***************************************************************************/

	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleFeeFinished")
	public RetMsg<PageInfo<TdTransforScheduleFee>> searchPageTrasForProductCapitalScheduleFeeFinished(@RequestBody Map<String,Object> params){

		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleFee> page = transforScheduleFeeService.getTransforScheduleFeePage(params,1);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleFeeUnfinished")
	public RetMsg<PageInfo<TdTransforScheduleFee>> searchPageTrasForProductCapitalScheduleFeeNoFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleFee> page = transforScheduleFeeService.getTransforScheduleFeePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTrasForProductCapitalScheduleFeeByMyself")
	public RetMsg<PageInfo<TdTransforScheduleFee>> searchPageTrasForProductCapitalScheduleFeeByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTransforScheduleFee> page = transforScheduleFeeService.getTransforScheduleFeePage(params,3);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/removeTrasForProductCapitalScheduleFee")
	public RetMsg<Serializable> removeTrasForProductCapitalScheduleFee(@RequestBody String[] dealNos) {
		transforScheduleFeeService.deleteTransforScheduleFee(dealNos);
		return RetMsgHelper.ok();
	}
	/***********************************************************************/
	

	/**
	 * 条件查询列表(结息确认未审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageInterestSettleUnfinished")
	public RetMsg<PageInfo<TdInterestSettle>> searchPageInterestSettleUnfinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInterestSettle> page = interestSettleService.getInterestSettlePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(结息确认已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageInterestSettleFinished")
	public RetMsg<PageInfo<TdInterestSettle>> searchPageInterestSettleFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInterestSettle> page = interestSettleService.getInterestSettlePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(结息确认我发起)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageInterestSettleMySelf")
	public RetMsg<PageInfo<TdInterestSettle>> searchPageInterestSettleMySelf(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdInterestSettle> page = interestSettleService.getInterestSettlePage(params,3);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/removeInterestSettle")
	public RetMsg<Serializable> removeInterestSettle(@RequestBody Map<String,Object> params) {
		interestSettleService.removeInterestSettle((String)params.get("dealNo"));
		return RetMsgHelper.ok();
	}
	/**
	 * 条件查询列表(活期提前支取)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityCurrentFinished")
	public RetMsg<PageInfo<TdAdvanceMaturityCurrent>> searchPageAdvanceMaturityCurrentFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturityCurrent> page = advanceMaturityCurrentService.getAdvanceMaturityCurrentPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(活期提前支取)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityCurrentUnfinished")
	public RetMsg<PageInfo<TdAdvanceMaturityCurrent>> searchPageAdvanceMaturityCurrentNoFinished(@RequestBody Map<String,Object> params){
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
		 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
				String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturityCurrent> page = advanceMaturityCurrentService.getAdvanceMaturityCurrentPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(活期提前支取)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityCurrentMine")
	public RetMsg<PageInfo<TdAdvanceMaturityCurrent>> searchPageAdvanceMaturityCurrentMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdAdvanceMaturityCurrent> page = advanceMaturityCurrentService.getAdvanceMaturityCurrentPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	
	
	/**
	 * 条件查询列表(活期提前支取)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFeeHandleCashflowFinished")
	public RetMsg<PageInfo<TdFeeHandleCashflow>> searchPageFeeHandleCashflowFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdFeeHandleCashflow> page = feeHandleCashflowService.getFeeHandleCashflowPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(活期提前支取)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFeeHandleCashflowNoFinished")
	public RetMsg<PageInfo<TdFeeHandleCashflow>> searchPageFeeHandleCashflowNoFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdFeeHandleCashflow> page = feeHandleCashflowService.getFeeHandleCashflowPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(活期提前支取)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageFeeHandleCashflowMine")
	public RetMsg<PageInfo<TdFeeHandleCashflow>> searchPageFeeHandleCashflowMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdFeeHandleCashflow> page = feeHandleCashflowService.getFeeHandleCashflowPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 条件查询列表(交易展期)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExpireFinished")
	public RetMsg<PageInfo<TdTradeExpire>> searchPageTradeExpireFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeExpire> page = tradeExpireService.getTradeExpirePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(交易展期)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExpireUnfinished")
	public RetMsg<PageInfo<TdTradeExpire>> searchPageTradeExpireNoFinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		
		//过滤掉的审批状态
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
					 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatusNo", orderStatus);
		
		Page<TdTradeExpire> page = tradeExpireService.getTradeExpirePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeExpireByMyself")
	public RetMsg<PageInfo<TdTradeExpire>> searchPageTradeExpireByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdTradeExpire> page = tradeExpireService.getTradeExpirePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeExpire")
	public RetMsg<Serializable> removeTradeExpire(@RequestBody String[] dealNos) {
		tradeExpireService.deleteTradeExpire(dealNos);
		return RetMsgHelper.ok();
	}
	
	/******************************************************************************************/
	//通道计划

	/**
	 * 条件查询列表(通道计划)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPagePassageWayFinished")
	public RetMsg<PageInfo<TdFeesPassAgewayChange>> searchPagePassageWayFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdFeesPassAgewayChange> page = tdFeesPassAgewayService.getPassageWayPage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(通道计划)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPagePassageWayNoFinished")
	public RetMsg<PageInfo<TdFeesPassAgewayChange>> searchPagePassageWayNoFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		
		//过滤掉的审批状态
				String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
							 		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
					String[] orderStatus = approveStatus.split(",");
					params.put("approveStatusNo", orderStatus);
		
		Page<TdFeesPassAgewayChange> page = tdFeesPassAgewayService.getPassageWayPage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(通道计划)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPagePassageWayMine")
	public RetMsg<PageInfo<TdFeesPassAgewayChange>> searchPagePassageWayMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdFeesPassAgewayChange> page = tdFeesPassAgewayService.getPassageWayPage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除(通道计划)
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removePassageWay")
	public RetMsg<Serializable> removePassageWay(@RequestBody String[] dealNos) {
		tdFeesPassAgewayService.deletePassageWay(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询通道
	 * @param dealNos - ID集合
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/getPassageWayByDealNo")
	public RetMsg<List<TmFeesPassagewy>> getPassageWayByDealNo(@RequestBody Map<String,Object> params) {		
		List<TmFeesPassagewy> list = tdFeesPassAgewayService.getPassageWayByDealNo(params);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/getTdPassageWayByDealNo")
	public RetMsg<List<TdFeesPassAgeway>> getTdPassageWayByDealNo(@RequestBody Map<String,Object> params) {		
		List<TdFeesPassAgeway> list = tdFeesPassAgewayService.getTdPassageWayByDealNo(params);
		return RetMsgHelper.ok(list);
	}
	/******************************************************************************************/
	
	/******************************************************************************************/
	//资产转让
	/**
	 * 条件查询列表(交易卖断)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightBuyFinished")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightBuyFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(交易卖断)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightBuyUnfinished")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightBuyUnfinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		//过滤掉的审批状态
		String approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatusNo", orderStatus);
				
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTradeOutRightBuyByMyself")
	public RetMsg<PageInfo<TdOutrightSale>> searchPageTradeOutRightBuyByMyself(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdOutrightSale> page = tradeOutrightSaleService.getTdOutrightSalePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTradeOutRightBuy")
	public RetMsg<Serializable> removeTradeOutRightBuy(@RequestBody String[] dealNos) {
		tradeOutrightSaleService.deleteTdOutrightSale(dealNos);
		return RetMsgHelper.ok();
	}
	
	/******************************************************************************************/
	
	/*************************************利率重订*****************************************************/
	
	@ResponseBody
	@RequestMapping(value = "/searchPageRateResetUnfinished")
	public RetMsg<PageInfo<TdRateReset>> searchPageRateResetUnfinished(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		//过滤掉的审批状态
		String approveStatusNo = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse
		+ "," + DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;
		String[] orderStatus = approveStatusNo.split(",");
		params.put("approveStatusNo", orderStatus);
		
		String approveStatus = ParameterUtil.getString(params, "approveStatus", null);
		if(!StringUtil.isNullOrEmpty(approveStatus)){
			orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
				
		Page<TdRateReset> page = rateResetService.getRateChangePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageRateResetFinished")
	public RetMsg<PageInfo<TdRateReset>> searchPageRateResetFinished(@RequestBody Map<String,Object> params){
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.Approving + "," + DictConstants.ApproveStatus.ApprovedPass;
			approveStatus += "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle;
			approveStatus += "," + DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed;
			approveStatus += "," + DictConstants.ApproveStatus.Verified;
			approveStatus += "," + DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted;
		}
		String[] orderStatus = approveStatus.split(",");
		params.put("approveStatus", orderStatus);
		
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdRateReset> page = rateResetService.getRateChangePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageRateResetMySelf")
	public RetMsg<PageInfo<TdRateReset>> searchPageRateResetMySelf(@RequestBody Map<String,Object> params){
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatus = ParameterUtil.getString(params, "approveStatus", null);
		if(!StringUtil.isNullOrEmpty(approveStatus)){
			String[] orderStatus = approveStatus.split(",");
			params.put("approveStatus", orderStatus);
		}
		Page<TdRateReset> page = rateResetService.getRateChangePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/removeRateReset")
	public RetMsg<Serializable> removeRateReset(@RequestBody String[] dealNos) {
		rateResetService.deleteRateChange(dealNos);
		return RetMsgHelper.ok();
	}
	
	/*************************************利率重订*****************************************************/
	
	
	/**
	 * 该笔交易是在该用户下是否完成审批
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/getDurationUnfinishCount")
	public RetMsg<Serializable> getDurationUnfinishCount(@RequestBody Map<String,Object> params)
	{
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse;
			params.put("approveStatusNo", approveStatus.split(","));
		}
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		if(advanceMaturityService.getDurationUnfinishCount(params) > 0){
			ret.setDetail("SUCC");
		}
		return ret;
	}
	
	/****
	 * 收息计划重新计算利息
	 * @param params refNo tmCashflowInterest tmCashflowInterestList
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/reSetInterestPlanForTemp")
	public RetMsg<Map<String, Object>> reSetInterestPlanForTemp(@RequestBody Map<String,Object> params)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		RetMsg<Map<String, Object>> ret = RetMsgHelper.ok(map);
		try {
			//refNo tmCashflowInterest tmCashflowInterestList
			Map<String, Object> map_1 = transforScheduleIntService.reSetInterestPlanForTemp(params);
			map.put("code", map_1.get("code"));
			map.put("msg",  map_1.get("msg"));
			map.put("data", map_1.get("data"));
			
		} catch (Exception e) {
			map.put("code", "120");
			map.put("msg",  "计算收息计划异常:"+e.getMessage());
			e.printStackTrace();
			
		}
		return ret;
	}
	
	/****
	 * 收息计划重新计算利息
	 * @param params  dealNo refNo tmCashflowInterest tmCashflowInterestList
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/calExtendInterestPlanForTemp")
	public RetMsg<Map<String, Object>> calExtendInterestPlanForTemp(@RequestBody Map<String,Object> params)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		RetMsg<Map<String, Object>> ret = RetMsgHelper.ok(map);
		try {
			//refNo tmCashflowInterest tmCashflowInterestList
			Map<String, Object> map_1 = tradeExtendService.calExtendInterestPlanForTemp(params);
			map.put("code", map_1.get("code"));
			map.put("msg",  map_1.get("msg"));
			map.put("data", map_1.get("data"));
			
		} catch (Exception e) {
			map.put("code", "120");
			map.put("msg",  "计算收息计划异常:"+e.getMessage());
			e.printStackTrace();
			
		}
		return ret;
	}
	
	/****
	 * 保存展期利息和本金现金流
	 * @param params  dealNo approveCashflowInterest approveCashflowCapitals
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveExtendCashFlowForApprove")
	public RetMsg<Serializable> saveExtendCashFlowForApprove(@RequestBody Map<String,Object> params)
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		try {
			//dealNo approveCashflowInterest approveCashflowCapitals
			tradeExtendService.saveExtendCashFlowForApprove(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	
}
