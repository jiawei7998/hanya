package com.singlee.capital.trade.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.mapper.TmApproveInterestMapper;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.TmApproveCapital;
import com.singlee.capital.cashflow.model.TmApproveInterest;
import com.singlee.capital.cashflow.model.TmCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants.RateType;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper;
import com.singlee.capital.trade.mapper.TdTransforScheduleIntMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforScheduleInt;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TransforScheduleIntService;
import com.singlee.slbpm.service.FlowOpService;

@Service
public class TransforScheduleIntServiceImpl extends AbstractDurationService implements TransforScheduleIntService {

	@Autowired
	private TdTransforScheduleIntMapper tdTransforScheduleIntMapper;

	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;//计划表
	@Autowired
	private TdCashflowCapitalMapper cashflowCapitalMapper;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private TmApproveInterestMapper approveInterestMapper;
	@Autowired
	private DayendDateService dateService;
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;
	@Autowired
	private TdRateRangeMapper  rateRangeMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	private TdFeesPassAgewayDailyMapper tdFeesPassAgewayDailyMapper;

	private boolean allowChangeRate = false;
	
	@Override
	public TdTransforScheduleInt getTransforScheduleIntById(String dealNo) {
		return tdTransforScheduleIntMapper.getTransforScheduleIntById(dealNo);
	}

	@Override
	public Page<TdTransforScheduleInt> getTransforScheduleIntPage(
			Map<String, Object> params, int isFinished) {
		Page<TdTransforScheduleInt> page= new Page<TdTransforScheduleInt>();
		if(isFinished == 2){
			page= tdTransforScheduleIntMapper.getTransforScheduleIntList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= tdTransforScheduleIntMapper.getTransforScheduleIntListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= tdTransforScheduleIntMapper.getTransforScheduleIntListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTransforScheduleInt> list =page.getResult();
		for(TdTransforScheduleInt te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易展期金融工具表")
	public void createTransforScheduleInt(Map<String, Object> params) {
		tdTransforScheduleIntMapper.insert(TransforScheduleInt(params));
	}

	@Override
	@AutoLogMethod(value = "修改交易展期金融工具表")
	public void updateTransforScheduleInt(Map<String, Object> params) {
		tdTransforScheduleIntMapper.updateByPrimaryKey(TransforScheduleInt(params));
	}

	
	private TdTransforScheduleInt TransforScheduleInt(Map<String, Object> params){
		// map转实体类
		TdTransforScheduleInt transforScheduleInt = new TdTransforScheduleInt();
		try {
			BeanUtil.populate(transforScheduleInt, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTransforScheduleInt extend = new TdTransforScheduleInt();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(transforScheduleInt.getDealType());
        extend = tdTransforScheduleIntMapper.searchTransforScheduleInt(extend);
        int basicType = 0;
        if(transforScheduleInt.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(transforScheduleInt.getRefNo());
        	basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			
        	JY.require(productApproveMain!=null, "原交易信息不存在");
            transforScheduleInt.setVersion(productApproveMain.getVersion());
            if(DictConstants.intType.chargeBefore.equalsIgnoreCase(productApproveMain.getIntType()))
            {
            	throw new RException(productApproveMain.getDealNo()+"为前收息业务,不允许做利息计划变更!");
            }
        }else{
        	JY.raise(transforScheduleInt.getRefNo()+"原交易信息不存在");
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(transforScheduleInt.getDealType())){
    		transforScheduleInt.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		transforScheduleInt.setaDate(extend.getaDate());
    	}
    	transforScheduleInt.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		//还本计划副本
		Object obj = transforScheduleInt.getApproveInterests();
		if (obj != null) {
			approveInterestMapper.deleteTmApproveInterestList(params);
			List<TmApproveInterest> approveInterests = FastJsonUtil.parseArrays(obj.toString(), TmApproveInterest.class);
			int count = 1;
			Collections.sort(approveInterests);
			for (TmApproveInterest approveInterest : approveInterests) {
				approveInterest.setSeqNumber(count);
				approveInterest.setDealNo(transforScheduleInt.getDealNo());
				approveInterest.setVersion(transforScheduleInt.getVersion());
				approveInterest.setRefNo(transforScheduleInt.getRefNo());
				approveInterest.setCfType(DictConstants.CashFlowType.Interest);
				approveInterest.setPayDirection("Recevie");
				approveInterest.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
				if(approveInterest.getTheoryPaymentDate() != null && approveInterest.getTheoryPaymentDate().length() > 10)
				{
					approveInterest.setTheoryPaymentDate(approveInterest.getTheoryPaymentDate().substring(0, 10));
				}
				if(approveInterest.getRefBeginDate() != null && approveInterest.getRefBeginDate().length() > 10)
				{
					approveInterest.setRefBeginDate(approveInterest.getRefBeginDate().substring(0, 10));
				}
				if(approveInterest.getRefEndDate() != null && approveInterest.getRefEndDate().length() > 10)
				{
					approveInterest.setRefEndDate(approveInterest.getRefEndDate().substring(0, 10));
				}
				approveInterestMapper.insert(approveInterest);
				count++;
			}
		}
		transforScheduleInt.setSponsor(SlSessionHelper.getUserId());
		transforScheduleInt.setSponInst(SlSessionHelper.getInstitutionId());
		return transforScheduleInt;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTransforScheduleInt(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdTransforScheduleIntMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTransforScheduleInt TransforScheduleInt = new TdTransforScheduleInt();
		BeanUtil.populate(TransforScheduleInt, params);
		TransforScheduleInt = tdTransforScheduleIntMapper.searchTransforScheduleInt(TransforScheduleInt);
		return productApproveService.getProductApproveActivated(TransforScheduleInt.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdTransforScheduleInt TransforScheduleInt = new TdTransforScheduleInt();
		BeanUtil.populate(TransforScheduleInt, params);
		if(TransforScheduleInt.getDealNo() == null){
			TransforScheduleInt.setDealNo(ParameterUtil.getString(params, "trade_id", ""));
		}
		
		TransforScheduleInt = tdTransforScheduleIntMapper.searchTransforScheduleInt(TransforScheduleInt);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(TransforScheduleInt.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(TransforScheduleInt.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(TransforScheduleInt.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomCashFlowChangeInt);//
		tdTrdEvent.setEventTable("TD_TRASFOR_SCHEDULE_INT");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		
		/**
		 * 完成后需要将新的收息计划更新到数据库  TD_CASHFLOW_INTEREST
		 * 重算未收息的计划表
		 */
		TdTransforScheduleInt cfChange = new TdTransforScheduleInt();//本金计划审批数据
		BeanUtil.populate(cfChange, params);
		//删除 TD_CASHFLOW_INTEREST 中未真实收息的计划
		cashflowInterestMapper.deleteForApproveNotActual(params);
		//插入审批通过的调整计划TM_APPROVE_INTEREST
		approveInterestMapper.insertApproveCapitalToTd(params);
		/**
		 * 更新利率调整TT_RATE_RANGE
		 */
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(cfChange.getRefNo());
		
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeBefore)){
			/**
			 * 先收息是没有收息计划表的，需要自动生成一次还本收息 利率区间
			 */
			TdRateRange rateRange = new TdRateRange();
			rateRange.setDealNo(productApproveMain.getDealNo());
			rateRange.setVersion(productApproveMain.getVersion());
			rateRange.setBeginDate(productApproveMain.getvDate());
			rateRange.setEndDate(productApproveMain.getmDate());
			rateRange.setExecRate(productApproveMain.getContractRate());
			rateRange.setAccrulType(RateType.ACCRU_TYPE);
			rateRangeMapper.insert(rateRange);
		}else{
			//收息计划是否允许修改利率,如果不允许则不需要修改利率区间
			if(allowChangeRate){
				List<TdRateRange> rateRanges = new ArrayList<TdRateRange>();
				TdRateRange rateRange = null;
				List<CashflowInterest> cashflowInterests = cashflowInterestMapper.getInterestList(BeanUtil.beanToMap(productApproveMain));
				for(CashflowInterest cashflowInterest : cashflowInterests)
				{
					rateRange = new TdRateRange();
					rateRange.setDealNo(cashflowInterest.getDealNo());
					rateRange.setVersion(cashflowInterest.getVersion());
					rateRange.setBeginDate(cashflowInterest.getRefBeginDate());
					rateRange.setEndDate(cashflowInterest.getRefEndDate());
					rateRange.setExecRate(cashflowInterest.getExecuteRate());
					rateRange.setAccrulType(RateType.ACCRU_TYPE);
					rateRanges.add(rateRange);
				}
				rateRangeMapper.deleteRateRanges(BeanUtil.beanToMap(productApproveMain));
				batchDao.batch("com.singlee.capital.cashflow.mapper.TdRateRangeMapper.insert", rateRanges);
			}
			
			try {
				//重算收息计划
				reSetInterestPlan(params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "com.singlee.capital.cashflow.mapper.Td";
        } else {
            return "com.singlee.capital.cashflow.mapper.Tm";
        }
	}
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "td";
        } else {
            return "tm";
        }
	}

	
	
	
	
	/**
	 * 计算获取应计利息 收息计划调整的时候预计算 
	 */
	@Override
	public double getAmIntCalForTrasforInterest(Map<String, Object> map) {
		String postdate = dateService.getDayendDate();
		String refBeginDate =  ParameterUtil.getString(map, "refBeginDate", postdate);
		String refEndDate =  ParameterUtil.getString(map, "refEndDate", postdate);
		String dealNo = ParameterUtil.getString(map, "dealNo", "");
		double executeRate = ParameterUtil.getDouble(map, "executeRate", 0.00);
		double remainAmt = ParameterUtil.getDouble(map, "remainAmt", 0.00);
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(dealNo)){
        	productApproveMain = productApproveService.getProductApproveActivated(dealNo);
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double amInt = 0.00;
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();
		InterestRange interestRange =  new InterestRange();
		interestRange.setStartDate(refBeginDate);
		interestRange.setEndDate(refEndDate);
		interestRange.setRate(executeRate);
		interestRanges.add(interestRange);
		//获取当前时点的剩余本金 ，获取后续 时间段的还本计划，计算出区间利息 
		
		List<CashflowCapital> tmCashflowCapitals = new ArrayList<CashflowCapital>();
		tmCashflowCapitals = ((CashFlowMapper)SpringContextHolder.getBean("tdCashflowCapitalMapper")).getCapitalList(map);//获得本金现金流
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		/**
		 * 将本金现金流 翻译为 本金变化区间
		 */
		try {
			int count  = 0;
			if(tmCashflowCapitals.size() > 0)
			{
				for(CashflowCapital tmCashflowCapital : tmCashflowCapitals){
					if(PlaningTools.compareDate(tmCashflowCapital.getRepaymentSdate(), tmCashflowCapitals.get(0).getRepaymentSdate(), "yyyy-MM-dd"))
					{
						principalInterval = new PrincipalInterval();
						principalInterval.setStartDate(count==0?refBeginDate:tmCashflowCapitals.get(count-1).getRepaymentSdate());
						principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
						principalInterval.setResidual_Principal(PlaningTools.sub(remainAmt, tempAmt));
						principalIntervals.add(principalInterval);
						tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
						count++;
					}
				}
				
			}else{
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(productApproveMain.getvDate());
				principalInterval.setEndDate(productApproveMain.getiDate() == null ? productApproveMain.getmDate() 
						: PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(), Frequency.YEAR, 20));
				principalInterval.setResidual_Principal(remainAmt);
				
				principalIntervals.add(principalInterval);
				
			}
		
			amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, refBeginDate, refEndDate, refBeginDate, 
					DayCountBasis.equal(productApproveMain.getBasis()));
		} catch (Exception e) {
			e.printStackTrace();
			//JY.require(true,"计算区间差额计提出错"+e.getMessage());
			JY.error("计算区间差额计提出错"+e.getMessage(),e);
		}   
		return amInt;
	}
	
	
	
	/**
	 * 计算获取应计利息 收息计划调整的时候预计算 
	 */
	@Override
	public double getAmIntCalForInterest(Map<String, Object> map) {
		String postdate = dateService.getDayendDate();
		String refBeginDate =  ParameterUtil.getString(map, "refBeginDate", postdate);
		String refEndDate =  ParameterUtil.getString(map, "refEndDate", postdate);
		String dealNo = ParameterUtil.getString(map, "dealNo", "");
		double executeRate = ParameterUtil.getDouble(map, "executeRate", 0.00);
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(dealNo)){
        	productApproveMain = productApproveService.getProductApproveActivated(dealNo);
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double remainAmt = productApproveMain.getAmt();
		double amInt = 0.00;
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();
		InterestRange interestRange =  new InterestRange();
		interestRange.setStartDate(refBeginDate);
		interestRange.setEndDate(refEndDate);
		interestRange.setRate(executeRate);
		interestRanges.add(interestRange);
		//获取当前时点的剩余本金 ，获取后续 时间段的还本计划，计算出区间利息 
		
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		/**
		 * 将本金现金流 翻译为 本金变化区间
		 */
		try {
			String capcashtable = ParameterUtil.getString(map, "approveCashflowCapitals","");
			List<TmApproveCapital> approveCapitals = FastJsonUtil.parseArrays(capcashtable, TmApproveCapital.class);//获得本金现金流
			int count = 0;
			Collections.sort(approveCapitals);
			for (TmApproveCapital approveCapital : approveCapitals) {
				
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(count==0?refBeginDate:approveCapitals.get(count-1).getRepaymentSdate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(approveCapital.getRepaymentSdate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(PlaningTools.sub(remainAmt, tempAmt));
				principalIntervals.add(principalInterval);
				tempAmt = PlaningTools.add(tempAmt, approveCapital.getRepaymentSamt());
				count++;
			}
		
			amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, refBeginDate, refEndDate, refBeginDate, 
					DayCountBasis.equal(productApproveMain.getBasis()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			JY.require(true,"计算区间差额计提出错"+e.getMessage());
		}   
		return amInt;
	}
	
	/**
	 * 计算获取应计利息 收息计划调整的时候预计算 扣除通道
	 */
	@Override
	public CashflowInterest getAmIntCalForTrasforInterestExceptFee(Map<String, Object> map) {
		CashflowInterest cash = new CashflowInterest();
		
		String postdate = dateService.getDayendDate();
		String refBeginDate =  ParameterUtil.getString(map, "refBeginDate", postdate);
		String refEndDate =  ParameterUtil.getString(map, "refEndDate", postdate);
		String dealNo = ParameterUtil.getString(map, "dealNo", "");
		double executeRate = ParameterUtil.getDouble(map, "executeRate", 0.00);
		
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(dealNo)){
        	productApproveMain = productApproveService.getProductApproveActivated(dealNo);
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double remainAmt = productApproveMain.getAmt();
		
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();
		InterestRange interestRange =  new InterestRange();
		interestRange.setStartDate(refBeginDate);
		interestRange.setEndDate(refEndDate);
		interestRange.setRate(executeRate);
		interestRanges.add(interestRange);
		//获取当前时点的剩余本金 ，获取后续 时间段的还本计划，计算出区间利息 
		
		List<CashflowCapital> tmCashflowCapitals = new ArrayList<CashflowCapital>();
		tmCashflowCapitals = ((CashFlowMapper)SpringContextHolder.getBean("tdCashflowCapitalMapper")).getCapitalList(map);//获得本金现金流
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		/**
		 * 将本金现金流 翻译为 本金变化区间
		 */
		try {
			int count  = 0;
			if(tmCashflowCapitals.size() > 0)
			{
				Collections.sort(tmCashflowCapitals);
				for(CashflowCapital tmCashflowCapital : tmCashflowCapitals)
				{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?refBeginDate:tmCashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(remainAmt, tempAmt));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
					count++;
				}

			}else{
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(productApproveMain.getvDate());
				principalInterval.setEndDate(productApproveMain.getiDate() == null ? productApproveMain.getmDate() 
						: PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(), Frequency.YEAR, 20));
				principalInterval.setResidual_Principal(remainAmt);
				
				principalIntervals.add(principalInterval);
				
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("refNo", productApproveMain.getDealNo());
			params.put("refBeginDate", refBeginDate);
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
					
			cash = PlaningTools.fun_Calculation_interval_Range_except_fee(interestRanges, principalIntervals, refBeginDate, refEndDate, refBeginDate, 
					DayCountBasis.equal(productApproveMain.getBasis()),fAgewayDailies);
		} catch (Exception e) {
			e.printStackTrace();
			//JY.require(true,"计算区间差额计提出错"+e.getMessage());
			JY.error("计算区间差额计提出错"+e.getMessage(),e);
		}   
		return cash;
	}
	
	/****
	 * 自动重新计算利息
	 * @param params
	 */
	public void reSetInterestPlan(Map<String, Object> params)throws Exception
	{	
		TdProductApproveMain productApproveMain = null;//重新查询原交易数据
		if(!"".equals(ParameterUtil.getString(params, "refNo", ""))){
        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeAfter))//后收息 才需要调整利息
		{
			//获取利率区间
			List<TdRateRange> rateRanges = this.rateRangeMapper.getRateRanges(params);
			
			//封装利率区间 带入计算公式
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();
			InterestRange interestRange = null;
			for(TdRateRange rateRange : rateRanges)
			{
				interestRange = new InterestRange();
				interestRange.setStartDate(rateRange.getBeginDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(rateRange.getExecRate());
				interestRanges.add(interestRange);
			}
			//封装本金区间  带入计算公式
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
			//从实际本金现金流表获取本金 递减区间
			//List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(params);
			//获取还本计划
			List<CashflowCapital> cashflowCapitals=cashflowCapitalMapper.getCapitalListByRef(params);
			PrincipalInterval principalInterval=null;
			double tempAmt =0f;
			int count =0 ; //将本金计划翻译成本金剩余区间
			Collections.sort(cashflowCapitals);
			HashMap<String, PrincipalInterval> prHashMap = new HashMap<String, PrincipalInterval>();
			
			String endDate = null;
			for(CashflowCapital tmCashflowCapital : cashflowCapitals)
			{
				endDate = PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1);
				if(prHashMap.get(endDate) != null)
				{
					prHashMap.get(endDate).setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					
				}else{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?productApproveMain.getvDate():cashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);

					prHashMap.put(principalInterval.getEndDate(), principalInterval);
				}
				
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			//查询得到在利率变更期影响到的收息计划数据
			Map<String, Object> map_1 = new HashMap<String, Object>();
			map_1.put("refNo", ParameterUtil.getString(params, "refNo", ""));
			List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntByDate(map_1);
			
			/*
			 * 循环重算收息计划之间的利息
			 */
			Collections.sort(cashflowInterests);
			
			List<CashflowDailyInterest> cashflowDailyInterests = new ArrayList<CashflowDailyInterest>();
			
			//查询通道利息
			params.put("refNo",productApproveMain.getDealNo());
			params.put("refBeginDate", cashflowInterests.get(0).getRefBeginDate());
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
			
			//重新计算计提利息
			//根据每日计提 和通道计提 重新计算收息计划的金额
			List<TdCashflowInterest> cashflowInterestsForUpdate = new ArrayList<TdCashflowInterest>();
			for(int i = 0;i < cashflowInterests.size();i++)
			{
				if(cashflowInterests.get(i).getActualDate() != null){
					continue;
				}
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges,principalIntervals, cashflowDailyInterests, 
						cashflowInterests.get(i), cashflowInterests.get(0).getRefBeginDate(), DayCountBasis.equal(productApproveMain.getBasis()), 
						fAgewayDailies);
				
				cashflowInterestsForUpdate.add(cashflowInterests.get(i));
			}
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper.updateByPrimaryKey", cashflowInterestsForUpdate);
			
			//删除原始计提
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("startDate", cashflowInterestsForUpdate.get(0).getRefBeginDate());
			cashflowDailyInterestMapper.deleteCashflowDailyInterest(map);
			
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", cashflowDailyInterests);		
		}
	}
	
	
	/****
	 * 临时重算收息计划,不保存数据库
	 * @param params  refNo tmCashflowInterest tmCashflowInterestList
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> reSetInterestPlanForTemp(Map<String, Object> params)throws Exception
	{
		Map<String, Object> retMap = new HashMap<String, Object>();
		/**
		 * 得到界面的serch_form数据  收息计划修改;转换为JAVABEAN
		 */
		String tmCashflowInterest = ParameterUtil.getString(params, "tmCashflowInterest","");
		TmCashflowInterest pageBean = FastJsonUtil.parseObject(tmCashflowInterest, TmCashflowInterest.class);
		params.put("changeDate", pageBean.getRefBeginDate());
		List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntByDate(params);
		if(cashflowInterests != null && cashflowInterests.size() > 0){
			retMap.put("code", "100");
			retMap.put("msg", "调整的计划起息日期必须大于或等于已收息计划的结束日期!");
			return retMap;
		}
		
		
		//利息明细
		List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();
		List<CashflowInterest> resultList = new ArrayList<CashflowInterest>();//最终的收息计划列表，需要最后用于持久化
		HashMap<String, CashflowInterest> dateSplitHashMap = new HashMap<String, CashflowInterest>();
		List<CashflowCapital> tmCashflowCapitals = new ArrayList<CashflowCapital>();
		List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间列表
		try{
			TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));			
			
			String tmCashflowInterestList = ParameterUtil.getString(params, "tmCashflowInterestList","");
			tmCashflowInterests = FastJsonUtil.parseArrays(tmCashflowInterestList, CashflowInterest.class);
			
			params.put("dealNo", productApproveMain.getDealNo());
			tmCashflowCapitals = ((CashFlowMapper)SpringContextHolder.getBean(getTitleCashFlow(params)+"CashflowCapitalMapper")).getCapitalList(params);//获得本金现金流
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			PrincipalInterval principalInterval=null;double tempAmt =0f;
			/**
			 * 将本金现金流 翻译为 本金变化区间
			 */
			int count  = 0;
			for(CashflowCapital tmCashflowCapital : tmCashflowCapitals){
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(count==0?productApproveMain.getvDate():tmCashflowCapitals.get(count-1).getRepaymentSdate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
				principalIntervals.add(principalInterval);
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			dateSplitHashMap.put(pageBean.getRefBeginDate(), pageBean);
			for(CashflowInterest temp : tmCashflowInterests)
			{
				/**
				 * 拆分计划为 起息日 到期日
				 * 保留 起息日 到期日 分别小于/大于  更改过的 起息日和到期日
				 */
				if(PlaningTools.compareDate2(pageBean.getRefBeginDate(), temp.getRefBeginDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate2(temp.getRefBeginDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate(pageBean.getRefEndDate(), temp.getRefBeginDate(), "yyyy-MM-dd")
						&& PlaningTools.compareDate(temp.getRefEndDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(pageBean.getRefEndDate(), temp);
				}
				
			}
			dateSplitHashMap.put(productApproveMain.getmDate(), tmCashflowInterests.get(tmCashflowInterests.size()-1));
			/**
			 * 进行排序
			 */
			
			List<Map.Entry<String, CashflowInterest>> arrayList = new ArrayList<Entry<String, CashflowInterest>>(dateSplitHashMap.entrySet());
			Collections.sort(arrayList, new Comparator<Object>() {
				   @Override
				   public int compare(Object o1, Object o2) {
					    @SuppressWarnings("unchecked")
						Map.Entry<String, CashflowInterest> obj1 = (Map.Entry<String, CashflowInterest>) o1;
					    @SuppressWarnings("unchecked")
					    Map.Entry<String, CashflowInterest> obj2 = (Map.Entry<String, CashflowInterest>) o2;
					    try {
							return PlaningTools.compareDate2(obj1.toString(), obj2.toString(), "yyyy-MM-dd") == true?1:0;
						} catch (ParseException e) {
							return 0;
						}
				   }
			 });
			/**
			 * 创建利率区间
			 */
			params.put("refNo", productApproveMain.getDealNo());
			List<TdRateRange> ranges = rateRangeMapper.getRateRanges(params);
			InterestRange interestRange = null;
			for(TdRateRange tdRateRange:ranges){
				interestRange = new InterestRange();
				interestRange.setStartDate(tdRateRange.getBeginDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(tdRateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(tdRateRange.getExecRate());
				interestRanges.add(interestRange);
			}
			
			params.put("refNo", productApproveMain.getDealNo());
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fessList = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
			
			CashflowInterest tmCashflowInterest2 = null;
			for(int i=0 ; i<arrayList.size()-1;i++){
				tmCashflowInterest2 = new TmCashflowInterest();
				tmCashflowInterest2.setDayCounter(pageBean.getDayCounter());
				tmCashflowInterest2.setDealNo(productApproveMain.getDealNo());
				tmCashflowInterest2.setExecuteRate(arrayList.get(i).getValue().getExecuteRate());
				tmCashflowInterest2.setRefBeginDate(arrayList.get(i).getKey());
				tmCashflowInterest2.setRefEndDate(arrayList.get(i+1).getKey());
				tmCashflowInterest2.setTheoryPaymentDate(PlaningTools.compareDateEqual(arrayList.get(i).getValue().getRefEndDate(), arrayList.get(i+1).getKey(), "yyyy-MM-dd")?arrayList.get(i).getValue().getTheoryPaymentDate():arrayList.get(i+1).getKey());
				tmCashflowInterest2.setCfType("Interest");
				tmCashflowInterest2.setPayDirection("Recevie");
				
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges, principalIntervals, tmCashflowDailyInterests, tmCashflowInterest2, productApproveMain.getvDate(), DayCountBasis.equal(pageBean.getDayCounter()), fessList);
				
				tmCashflowInterest2.setVersion(productApproveMain.getVersion());
				tmCashflowInterest2.setSeqNumber(i+1);
				resultList.add(tmCashflowInterest2);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		retMap.put("code", "000");
		retMap.put("data", resultList);
		return retMap;
	}
	
}
