package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdAssetSpecificBond;

/**
 * @projectName 同业业务管理系统
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdAssetSpecificBondMapper extends Mapper<TdAssetSpecificBond> {

	
	public Page<TdAssetSpecificBond> getTdAssetSpecificBondList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 */
	public List<TdAssetSpecificBond> getTdAssetSpecificBondList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 */
	public void deleteTdAssetSpecificBondByDealNo(String dealNo);
	/**
	 * 审批转交易拷贝
	 * @param params
	 */
	public void insertTdAssetSpecificBondForCopy(Map<String, Object> params);
}