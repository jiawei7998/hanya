package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdCashFlowChange;

/**
 * @projectName 同业业务管理系统
 * @className 现金流持久层
 * @description TODO
 * @createDate 2016-9-25 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdCashFlowChangeMapper extends Mapper<TdCashFlowChange> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 现金流对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdCashFlowChange getCashFlowChangeById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 现金流对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdCashFlowChange> getCashFlowChangeList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 现金流对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdCashFlowChange> getCashFlowChangeListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 现金流对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdCashFlowChange> getCashFlowChangeListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据dealno，dealtype获取
	 * @param map
	 * @return
	 */
	public TdCashFlowChange getCashFlowChange(TdCashFlowChange cf);
}