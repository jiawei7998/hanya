package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.Function;

/**
 * 功能点mapper
 * @author Administrator
 *
 */
public interface FunctionMapper extends Mapper<Function> {
	
	/**
	 * 根据module_id读取该模块相关功能点信息
	 * @param module_id :菜单id(不允许为空)
	 * @return
	 */
	public Page<Function> searchFunctionByModuleId(Map<String, Object> param,RowBounds rowBounds);
	
	/**
	 * 添加功能点信息
	 * @param function
	 */
	public void insertFunction(Function function);
	
	/**
	 * 修改功能点信息
	 * @param function
	 */
	public void updateFunction(Function function);
	
	/**
	 * 根据功能点对象删除功能点信息
	 * @param function
	 */
	public void deleteFunctionById(Function function);
	
	/**
	 * 根据菜单子节点删除相对应的功能点信息
	 * @param moduleIdss
	 */
	public void deleteTaModuleById(String moduleIdss);
	
	/**
	 * 查询功能点列表
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public Page<Function> pageFunction(Map<String, Object> param,RowBounds rowBounds);
	
	/**
	 * 根据功能点Id查询存在多少条数据
	 * @param functionId
	 * @return
	 */
	public int queryFunctionById(@Param("functionId")String functionId);
}