package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.joyard.jc.fpml.service.FpMLFactory;
import com.joyard.jc.instrument.mmib.MmibFpMLVo;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.cashflow.model.TtCashflowCapital;
import com.singlee.capital.cashflow.model.TtCashflowInterest;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdCashFlowChangeMapper;
import com.singlee.capital.trade.model.TdCashFlowChange;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.CashFlowChangeService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 现金流变更
 * @description TODO
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CashFlowChangeServiceImpl implements CashFlowChangeService {

	@Autowired
	private TdCashFlowChangeMapper cashFlowChangeMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private FlowOpService flowOpService;
	
	@Override
	public TdCashFlowChange getCashFlowChangeById(String dealNo) {
		return cashFlowChangeMapper.getCashFlowChangeById(dealNo);
	}

	@Override
	public Page<TdCashFlowChange> getCashFlowChangePage(
			Map<String, Object> params, int isFinish) {
		Page<TdCashFlowChange> page= new Page<TdCashFlowChange>();
		if(isFinish == 1){
			page = cashFlowChangeMapper.getCashFlowChangeListFinished(params, ParameterUtil.getRowBounds(params));
		}else if(isFinish == 2){
			page = cashFlowChangeMapper.getCashFlowChangeList(params, ParameterUtil.getRowBounds(params));
		}
		else{
			page = cashFlowChangeMapper.getCashFlowChangeListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdCashFlowChange> list =page.getResult();
		for(TdCashFlowChange cfc :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(cfc.getRefNo());
			if(main!=null){
				cfc.setProduct(main.getProduct());
				cfc.setParty(main.getCounterParty());
				cfc.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建现金流变更金融工具表")
	public void createCashFlowChange(Map<String, Object> params) {
		cashFlowChangeMapper.insert(CashFlowChange(params,true));
	}

	@Override
	@AutoLogMethod(value = "修改现金流变更金融工具表")
	public void updateCashFlowChange(Map<String, Object> params) {
		cashFlowChangeMapper.updateByPrimaryKey(CashFlowChange(params,false));
	}

	
	private TdCashFlowChange CashFlowChange(Map<String, Object> params,boolean isAdd){
		// map转实体类
		TdCashFlowChange cashFlowChange = new TdCashFlowChange();
		try {
			BeanUtil.populate(cashFlowChange, params);
			
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}


		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdCashFlowChange cfChange = new TdCashFlowChange();
        cfChange.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
        cfChange.setDealType(DictConstants.DealType.Verify);
        cfChange = cashFlowChangeMapper.getCashFlowChange(cfChange);
        if(cfChange==null && StringUtil.isNotEmpty(cashFlowChange.getRefNo())){
        	TdCashFlowChange cfChangeVo = cashFlowChangeMapper.getCashFlowChangeById(ParameterUtil.getString(params, "dealNo", ""));
        	if(cfChangeVo==null){
	        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(cashFlowChange.getRefNo());
	        	JY.require(productApproveMain!=null, "金融工具表中未获取到成交编号为%s的有效数据!",cashFlowChange.getRefNo());
				JY.require(StringUtils.isNotEmpty(productApproveMain.getFpml()), "对应的金融工具表数据中没有条款，因此无法更新!");
	        	// 生成条款
		       /* MmibFpMLVo vo =updateCashFlowChangeFpml(params,productApproveMain.getFpml(),productApproveMain.getiCode(),productApproveMain.getaType(),productApproveMain.getmType());
				
				cashFlowChange.setFpml(vo.getFpml());
				cashFlowChange.setiCode(productApproveMain.getiCode());
				cashFlowChange.setaType(productApproveMain.getaType());
				cashFlowChange.setmType(productApproveMain.getmType());*/
				cashFlowChange.setVersion(productApproveMain.getVersion());
        	}
        	else{
        		  MmibFpMLVo vo =updateCashFlowChangeFpml(params,cfChangeVo.getFpml(),cfChangeVo.getiCode(),cfChangeVo.getaType(),cfChangeVo.getmType());
  				  cashFlowChange.setFpml(vo.getFpml());
        	}
        }
    		
        if(cfChange!=null && DictConstants.DealType.Verify.equals(cashFlowChange.getDealType())) {
        	if(!isAdd){
	        	MmibFpMLVo mmibVo =updateCashFlowChangeFpml(params,cfChange.getFpml(),cfChange.getiCode(),cfChange.getaType(),cfChange.getmType());
	    		cashFlowChange.setFpml(mmibVo.getFpml());
        	}
        	else{
        		cashFlowChange.setFpml(cfChange.getFpml());
        	}
    		cashFlowChange.setiCode(cfChange.getiCode());
    		cashFlowChange.setaType(cfChange.getaType());
    		cashFlowChange.setmType(cfChange.getmType());
    		cashFlowChange.setVersion(cfChange.getVersion());
    		cashFlowChange.setRefNo(cfChange.getRefNo());
        }
        if(cfChange==null && DictConstants.DealType.Verify.equals(cashFlowChange.getDealType())) {
        	TdCashFlowChange cf = new TdCashFlowChange();
        	cf.setDealNo(ParameterUtil.getString(params, "order_id", ""));
        	cf.setDealType(DictConstants.DealType.Approve);
        	cf = cashFlowChangeMapper.getCashFlowChange(cf);
        	cashFlowChange.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
    		cashFlowChange.setFpml(cf.getFpml());
			cashFlowChange.setiCode(cf.getiCode());
			cashFlowChange.setaType(cf.getaType());
			cashFlowChange.setmType(cf.getmType());
			cashFlowChange.setVersion(cf.getVersion());
			cashFlowChange.setRefNo(cf.getRefNo());
			cashFlowChange.setEffectDate(cf.getEffectDate());
			cashFlowChange.setRemark(cf.getRemark());
			cashFlowChange.setSponsor(cf.getSponsor());
			cashFlowChange.setSponInst(cf.getSponInst());
			cashFlowChange.setaDate(cf.getaDate());
	    }else{
        	if(!DictConstants.DealType.Verify.equals(cashFlowChange.getDealType())){
        		cashFlowChange.setaDate(DateUtil.getCurrentDateAsString());
        	}
        	else if(cfChange!=null){
        		cashFlowChange.setaDate(cfChange.getaDate());
        	}
	    	cashFlowChange.setSponsor(SlSessionHelper.getUserId());
	    	cashFlowChange.setSponInst(SlSessionHelper.getInstitutionId());
	    }
		cashFlowChange.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return cashFlowChange;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteCashFlowChange(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			cashFlowChangeMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getTdProductApproveMain(String dealNo) {
		return productApproveService.getProductApproveActivated(dealNo);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	/**
	 * 现金流变更生成新的条款
	 * @param map
	 * @param fpml 老的fpml
	 * @param iCode
	 * @param aType
	 * @param mType
	 * @return
	 */
	private MmibFpMLVo updateCashFlowChangeFpml(Map<String,Object> map, String fpml,String iCode,String aType,String mType){
			List<TtCashflowInterest> intList= new ArrayList<TtCashflowInterest>();
			List<TtCashflowCapital> capList = new ArrayList<TtCashflowCapital>();
			MmibFpMLVo mv = new MmibFpMLVo();
			mv.setFpML(fpml);
			FpMLFactory.getInstance().parseFpML(mv);
			//利息现金流编辑面的表单内容 需定义为参数名字为interestCashFlowList
			String interest = ParameterUtil.getString(map, "interestCashFlowList","");
			//编辑保存时利息现金流不为空
			if(StringUtils.isNotEmpty(interest)){
				List<TtCashflowInterest> list=FastJsonUtil.parseArrays(interest, TtCashflowInterest.class);
				intList.addAll(list);
				Collections.sort(intList);
				String theoryPayDate = "";
				int i=0;
				for(TtCashflowInterest in:intList){
					in.setiCode(iCode);
					in.setaType(aType);
					in.setmType(mType);
					in.setExecuteRate(in.getExecuteRate());
					
					if(!theoryPayDate.equals(in.getTheoryPaymentDate())){
						i++;
					}
					in.setSeqNumber(String.valueOf(i));
					theoryPayDate = in.getTheoryPaymentDate();
				}
			}		
			//本金现金流编辑面的表单内容 需定义为参数名字为capitalCashFlowList
			String capital =ParameterUtil.getString(map, "capitalCashFlowList","");
			//编辑保存时本金现金流不为空
			if(StringUtils.isNotEmpty(capital)){
				List<TtCashflowCapital> list=FastJsonUtil.parseArrays(capital, TtCashflowCapital.class);
				capList.addAll(list);
				Collections.sort(capList);
				String theoryPayDate = "";
				int i=0;
				for(TtCashflowCapital cap :capList){
					cap.setiCode(iCode);
					cap.setaType(aType);
					cap.setmType(mType);
					
					if(!theoryPayDate.equals(cap.getTheoryPaymentDate())){
						i++;
					}
					cap.setSeqNumber(String.valueOf(i));
					theoryPayDate = cap.getTheoryPaymentDate();
				}
			}
			/*if((intList!=null && intList.size()>0) || (capList!=null && capList.size()>0)){
				if(intList!=null && intList.size()>0){
					mv.setInterestCashFlowList(calCashFlowService.toInterestCashFlow(intList));
				}
				if(capList!=null && capList.size()>0){
					mv.setCapitalCashFlowList(calCashFlowService.toCapitalCashFlow(capList));
				}
			}*/
    		JY.require(mv!=null, "条款转对象失败!");
        	FpMLFactory.getInstance().generateFpML(mv);
			return mv;
	}
}
