package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TcGuidePriceMapper;
import com.singlee.capital.trade.model.TcGuidePrice;
import com.singlee.capital.trade.model.TcGuidePriceVo;
import com.singlee.capital.trade.service.TcGuidePriceService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TcGuidePriceServiceImpl implements TcGuidePriceService {
	@Autowired
	private TcGuidePriceMapper guidePriceMapper;
	@Override
	public List<TcGuidePriceVo> getTcGuidePrices() {
		// TODO Auto-generated method stub
		List<TcGuidePriceVo> guidePriceVos = new ArrayList<TcGuidePriceVo>();
		List<String> prdNos = this.guidePriceMapper.getDistinctPrdNo();
		Map<String,Object> paramsMap = new HashMap<String, Object>();
		TcGuidePriceVo guidePriceVo = null;
		for(String prdNo:prdNos)
		{
			paramsMap.put("prdNo", prdNo);
			List<TcGuidePrice> tcGuidePrices = guidePriceMapper.getGuidePricesByPrdNo(paramsMap);
			guidePriceVo = new TcGuidePriceVo();
			int count = 0;
			for(TcGuidePrice guidePrice : tcGuidePrices)
			{
				if(count==0){
					guidePriceVo.setPrdNo(guidePrice.getPrdNo());
					guidePriceVo.setPrdName(guidePrice.getPrdName());
					guidePriceVo.setPrdInst(guidePrice.getPrdInst());
					guidePriceVo.setPrdType(guidePrice.getPrdType());
				}
				if("1M".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_1M(guidePrice.getbPrice2());
                }
				if("2W".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_2W(guidePrice.getbPrice2());
                }
				if("3M".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_3M(guidePrice.getbPrice2());
                }
				if("6M".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_6M(guidePrice.getbPrice2());
                }
				if("1Y".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_1Y(guidePrice.getbPrice2());
                }
				if("2-3Y".equalsIgnoreCase(guidePrice.getTermCode())) {
                    guidePriceVo.setRate_2Y(guidePrice.getbPrice2());
                }
			}
			
			guidePriceVos.add(guidePriceVo);
		}
		return guidePriceVos;
	}

}
