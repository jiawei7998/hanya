package com.singlee.capital.trade.service.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.joyard.jc.fpml.service.FpMLFactory;
import com.joyard.jc.instrument.mmib.AMmib;
import com.joyard.jc.instrument.mmib.MmibFactory;
import com.joyard.jc.instrument.mmib.MmibFpMLVo;
import com.joyard.jc.time.Date;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.base.service.ProductService;
import com.singlee.capital.base.trade.mapper.TdProductParticipantMapper;
import com.singlee.capital.base.trade.service.TdProductParticipantService;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.counterparty.service.CounterPartyService;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.qdb.pojo.FBZ003;
import com.singlee.capital.interfacex.qdb.service.FBZService;
import com.singlee.capital.loan.mapper.TcLoanCheckboxDealMapper;
import com.singlee.capital.loan.model.TcLoanCheckboxDeal;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaUserParamMapper;
import com.singlee.capital.system.mapper.TcInstitutionProductMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUserParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;
import com.singlee.capital.trade.mapper.TdAssetLcMapper;
import com.singlee.capital.trade.mapper.TdAssetSpecificBondMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetBillMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetBlMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetGuartyMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetStructMapper;
import com.singlee.capital.trade.mapper.TdBaseRiskMapper;
import com.singlee.capital.trade.mapper.TdFeesBrokerMapper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayMapper;
import com.singlee.capital.trade.mapper.TdProductApproveAmtLineMapper;
import com.singlee.capital.trade.mapper.TdProductApproveCrmsMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductApproveNeedLoanMapper;
import com.singlee.capital.trade.mapper.TdProductApproveOverDraftMapper;
import com.singlee.capital.trade.mapper.TdProductApproveProtocolsMapper;
import com.singlee.capital.trade.mapper.TdProductApproveSubMapper;
import com.singlee.capital.trade.mapper.TdProductCustCreditMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.mapper.TdRiskAssetMapper;
import com.singlee.capital.trade.mapper.TdRiskSlowReleaseMapper;
import com.singlee.capital.trade.model.FinEnterpriseTrade;
import com.singlee.capital.trade.model.TdAssetLc;
import com.singlee.capital.trade.model.TdAssetSpecificBond;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdBaseAssetBill;
import com.singlee.capital.trade.model.TdBaseAssetBl;
import com.singlee.capital.trade.model.TdBaseAssetGuarty;
import com.singlee.capital.trade.model.TdBaseAssetStruct;
import com.singlee.capital.trade.model.TdBaseRisk;
import com.singlee.capital.trade.model.TdDurationCommon;
import com.singlee.capital.trade.model.TdFeesBroker;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveAmtLine;
import com.singlee.capital.trade.model.TdProductApproveCrms;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveProtocols;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.model.TdProductCustCredit;
import com.singlee.capital.trade.model.TdRiskAsset;
import com.singlee.capital.trade.model.TdRiskSlowRelease;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.slbpm.mapper.TtFlowRoleUserMapMapper;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapVo;
import com.singlee.slbpm.service.FlowOpService;
import com.singlee.slbpm.service.FlowQueryService;

/**
 * @projectName 同业业务管理系统
 * @className 自定义产品服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 201705
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductApproveServiceImpl implements ProductApproveService {
	@Autowired
	private TdBaseAssetGuartyMapper assetGuartyMapper;
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	private TdProductApproveSubMapper productApproveSubMapper;
	@Autowired
	private TdAssetLcMapper tdAssetLcMapper;
	@Autowired
	private TdBaseAssetMapper baseAssetMapper;
	@Autowired
	private TdBaseAssetBillMapper baseAssetBillMapper;
	@Autowired
	private TdRiskAssetMapper riskAssetMapper;
	@Autowired
	private TdRiskSlowReleaseMapper riskSlowReleaseMapper;
	@Autowired
	private TcProductMapper productMapper;
	@Autowired
	private DayendDateService dayendDateService; 
	@Autowired
	private CalCashFlowService calCashFlowService;
	@Autowired
	private TdProductParticipantMapper productParticipantMapper;
	@Autowired
	private TdProductParticipantService tdProductParticipantService;
	@Autowired
	private TdRateRangeMapper rateRangeMapper;
	@Autowired
	private TrdTposMapper tposMapper;
	@Autowired
	private TdAssetLcMapper assetLcMapper;
	@Autowired
	private TdProductApproveCrmsMapper productApproveCrmsMapper;
	@Autowired
	private TdProductApproveOverDraftMapper productApproveOverDraftMapper;
	@Autowired
	private TdProductApproveProtocolsMapper productApproveProtocolsMapper;
	@Autowired
	private TdAssetSpecificBondMapper assetSpecificBondMapper;
	@Autowired
	private TdProductCustCreditMapper productCustCreditMapper;
	@Autowired
	private CashflowFeeService cashflowFeeService;
	@Autowired
	private TcLoanCheckboxDealMapper tcLoanCheckboxDealMapper;
	@Autowired
	TdProductFeeDealMapper productFeeDealMapper;
	@Autowired
	FlowQueryService flowQueryService;
	@Autowired
	private TdBaseAssetBlMapper baseAssetBlMapper;
	@Autowired
	private TdBaseRiskMapper baseRiskMapper;
	@Autowired
	private TtFlowRoleUserMapMapper ttFlowRoleUserMapMapper;
	@Autowired
	private TtInstitutionMapper institutionMapper;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private FlowOpService flowOpService;
	DecimalFormat doubleFormat = new DecimalFormat("###,###,###,###.00");
	@Autowired
	TcInstitutionProductMapper tcInstitutionProductMapper;
	@Autowired
	BookkeepingService bookkeepingService;
	@Autowired
	ProductService productService;
	@Autowired
	private TdProductApproveNeedLoanMapper tdProductApproveNeedLoanMapper;
	@Autowired
	private TdProductApproveAmtLineMapper tdProductApproveAmtLineMapper;
	@Autowired
	private TdBaseAssetStructMapper assetStructMapper;
	@Autowired
	TdFeesPassAgewayMapper feesPassagewyMapper;
	@Autowired
	private TdFeesPassAgewayService feesPassAgewayService;
	
	@Autowired
	private CounterPartyService counterPartyService;
	
	@Autowired
	private TdCashflowCapitalMapper  cashflowCapitalMapper;
	
	@Autowired
	private TdFeesBrokerMapper brokerMapper; 
	
	@Autowired
	private TaUserParamMapper userParamMapper;
	
	@Autowired
	private FBZService fbzService;
	
	public boolean checkProdAccess(String prdNo,String instId){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("instId", instId);
		List<String> prodList = tcInstitutionProductMapper.getPrdNos(map);
		for(String prodNo : prodList){
			if(StringUtils.equalsIgnoreCase(prdNo, prodNo)){
				return true;
			}
		}
		return false;
	}
	@Override
	public TdProductApproveMain getProductApproveActivated(String dealNo) {
		
		TdProductApproveMain productApproveMain = productApproveMainMapper.getProductApproveMainActivated(dealNo);
		if(productApproveMain != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("dealNo", dealNo);
			params.put("genMainNo", dealNo);
			List<TdProductApproveSub> productApproveSubs = productApproveSubMapper.getProductApproveSubList(params);
			if(null!=productApproveSubs&&productApproveSubs.size()>0) {
                productApproveMain.setProductApproveSubs(productApproveSubs);
            }
			params.put("version", productApproveMain.getVersion());
			productApproveMain.setParticipantApprove(tdProductParticipantService.transProductParticipantAsApproveSubs(params));
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("party_id", productApproveMain.getcNo());
			productApproveMain.setCounterParty(counterPartyService.selectCPByMap(map));
		}
		return productApproveMain;
	}
	@Override
	public TdProductApproveMain getProductApproveForDuration(Map<String, Object> params) {
		//取当前激活的交易单
		String dealNo = ParameterUtil.getString(params, "dealNo", "");
		TdProductApproveMain activeDeal = productApproveMainMapper.getProductApproveMainActivated(dealNo);
		/**
		 * 筛选所属交易的收息计划表
		 * 根据当前账务日期
		 */
		String postdate = dayendDateService.getDayendDate();
		params.put("refNo", dealNo);
		params.put("version", activeDeal.getVersion());
		List<TdRateRange> rateChanges = rateRangeMapper.getRateRanges(params);
		for(TdRateRange rateRange:rateChanges){
			try {
				if(PlaningTools.compareDate2(rateRange.getEndDate(),postdate,"yyyy-MM-dd") && PlaningTools.compareDate(postdate,rateRange.getBeginDate(),"yyyy-MM-dd")){
					activeDeal.setPreDate(rateRange.getBeginDate());
					break;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				JY.error(e.getMessage());
			}
		}
		/**
		 * 获取持仓信息
		 */
		if(null != activeDeal){
			activeDeal.setTpos(tposMapper.getTdTrdTposByKey(params));
		}
		return activeDeal;
	}
	
	@Override
	public TdProductApproveMain getProductApproveByCondition(Map<String, Object> params) {
		return productApproveMainMapper.getProductApproveMainByCondition(params);
	}
	
	@Override
	public Page<TdProductApproveMain> getProductApprovePage(Map<String, Object> params, int isFinished) {
		if(params.get("approveStatus") != null && !"".equals(params.get("approveStatus"))){
			String[] orderStatus = params.get("approveStatus").toString().split(",");
			params.put("approveStatus", orderStatus);
		}
		Page<TdProductApproveMain> TdProductApproveMainList = null;
		if(isFinished == 2) {
            TdProductApproveMainList = productApproveMainMapper.getProductApproveMainFinishList(params, ParameterUtil.getRowBounds(params));
        } else if(isFinished == 1){
			// + wzf - 2017.5.10 - 机构隔离
			// 登录人机构ID
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("instId", SlSessionHelper.getInstitutionId());
			TtInstitution ttinstitu =new TtInstitution();
			ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
			// 根据机构主键获取机构信息
			ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);
			
			// 根据登录人机构ID查询他的子机构
			List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
			List<String> ttlist=new ArrayList<String>();
			for (TtInstitution ttInstitution : tlist) {
				ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
			}
			params.put("tttlist", ttlist);
			
			// - wzf - 2017.5.10
			
			// + Yang Yang - 2017.5.17 - 获取当前登录用户的审批角色
			if(params.get("userId") != null && !"".equals(params.get("userId"))){
				String userId = params.get("userId").toString();
				HashMap<String, Object> param_map = new HashMap<String, Object>();
				param_map.put("user_id", userId);
				List<FlowRoleUserMapVo> list = ttFlowRoleUserMapMapper.listFlowRoleUserMap(param_map);
				if(list.size() == 1) {
					params.put("flow_role_id", list.get(0).getRole_id());					
				}
			}
			// - Yang Yang - 2017.5.17 - 获取当前登录用户的审批角色
			
			TdProductApproveMainList = productApproveMainMapper.getProductApproveMainList(params, ParameterUtil.getRowBounds(params));
		}else if(isFinished == 99){
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("instId", SlSessionHelper.getInstitutionId());
			TtInstitution ttinstitu =new TtInstitution();
			ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
			// 根据机构主键获取机构信息
			ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);
			
			// 根据登录人机构ID查询他的子机构
			List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
			List<String> ttlist=new ArrayList<String>();
			for (TtInstitution ttInstitution : tlist) {
				ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
			}
			params.put("tttlist", ttlist);
			TdProductApproveMainList = productApproveMainMapper.getProductApproveMainListMine(params, ParameterUtil.getRowBounds(params));
		}
		else {
            TdProductApproveMainList = productApproveMainMapper.getProductApproveMainListMine(params, ParameterUtil.getRowBounds(params));
        }
		for (TdProductApproveMain tdProductApproveMain : TdProductApproveMainList) {
			//查询 KEY-VALUE 自由项
			params.put("genMainNo", tdProductApproveMain.getDealNo());
			tdProductApproveMain.setProductApproveSubs(productApproveSubMapper.getProductApproveSubList(params));
			//查询参与方配置
			params.put("dealNo", tdProductApproveMain.getDealNo());
			params.put("version", tdProductApproveMain.getVersion());
			tdProductApproveMain.setParticipantApprove(tdProductParticipantService.transProductParticipantAsApproveSubs(params));
			if(DictConstants.TrdType.AbssetForEnterpriceCredit.equals(tdProductApproveMain.getTrdtype()))
			{
				TdProductApproveCrms productApproveCrms = new TdProductApproveCrms();
				productApproveCrms.setDealNo(tdProductApproveMain.getDealNo());
				productApproveCrms.setVersion(tdProductApproveMain.getVersion());
				productApproveCrms = productApproveCrmsMapper.selectByPrimaryKey(productApproveCrms);
				tdProductApproveMain.setProductApproveCrms(productApproveCrms);
			}
			if(DictConstants.TrdType.ApproveOverDraft.equals(tdProductApproveMain.getTrdtype()))
			{
				TdProductApproveOverDraft productApproveOverDraft = new TdProductApproveOverDraft();
				productApproveOverDraft.setDealNo(tdProductApproveMain.getDealNo());
				productApproveOverDraft.setVersion(tdProductApproveMain.getVersion());
				productApproveOverDraft = productApproveOverDraftMapper.selectByPrimaryKey(productApproveOverDraft);
				tdProductApproveMain.setProductApproveOverDraft(productApproveOverDraft);
			}	
			List<TdProductApproveProtocols> productApproveProtocols = productApproveProtocolsMapper.getProductApproveProtocolsByDealNo(tdProductApproveMain.getDealNo());
			if(null != productApproveProtocols) {
                tdProductApproveMain.setProductProcotols(productApproveProtocols);
            }
		}
		return TdProductApproveMainList;
	}
	
	
	@Override
	@AutoLogMethod(value="创建负债业务金融工具表")
	public void createLiabilitiesProductApprove(Map<String, Object> params) {
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}	
		//初始化表数据
		productApproveMain.setDealType(ParameterUtil.getString(params, "dealType", ""));
		productApproveMain.setDealDate(DateUtil.getCurrentDateAsString());
		productApproveMain.setIsActive(DictConstants.YesNo.YES);//有效
		productApproveMain.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		productApproveMain.setSponsor(productApproveMain.getSponsor() == null ? SlSessionHelper.getUserId() : productApproveMain.getSponsor());
		productApproveMain.setSponInst(productApproveMain.getSponInst() == null ? SlSessionHelper.getInstitutionId() : productApproveMain.getSponInst());
		String settlementDate = dayendDateService.getSettlementDate();
		productApproveMain.setaDate(settlementDate);	
		//拷贝数据  审批转核实
		if(DictConstants.YesNo.YES.equalsIgnoreCase(ParameterUtil.getString(params, "copyApprove", "")))
		{
			params.put("version",productApproveMain.getVersion());//设置初始
			productParticipantMapper.insertProductPartyCopy(params);//参与者拷贝
			//1.将实体交易持久化
			productApproveMainMapper.insert(productApproveMain);//主体交易存储
		    
		}else{
			//1.将参与方持久化
		    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
			//2.向main表中插入数据
			productApproveMainMapper.insert(productApproveMain);
		}
	}
	@Override
	@AutoLogMethod(value="修改自定义业务金融工具表")
	public void updateLiabilitiesProductApprove(Map<String, Object> params) {
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(ParameterUtil.getString(params, "dealNo", ""));
		try{
			BeanUtil.populate(productApproveMain, params);
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
		if(main!=null){
			int version =main.getVersion();
			BeanUtil.copyNotEmptyProperties(main, productApproveMain);
			main.setVersion(version);
		}
		//初始化表数据
		main.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		//1.将参与方持久化
	    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
	    
	    BeanUtil.copyNotEmptyProperties(main, productApproveMain);
		//2.向main表中更新数据
		productApproveMainMapper.updateByPrimaryKey(main);
	}
	
	@Override
	@AutoLogMethod(value="创建自定义业务金融工具表")
	public void createProductApprove(Map<String, Object> params) {
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		//检查机构产品准入
//		if(!checkProdAccess(String.valueOf(productApproveMain.getPrdNo()),productApproveMain.getSponInst()))
//		{
//			throw new RException(productApproveMain.getProduct().getPrdName()+"["+productApproveMain.getPrdNo()+"]没有准入权限,业务不能开展!");
//		}
		HashMap<String, Object> amtMap = new HashMap<String, Object>();
		amtMap.put(DurationConstants.AccType.LOAN_AMT, productApproveMain.getAmt());
		InstructionPackage instructionPackage = new InstructionPackage();
		instructionPackage.setInfoMap(params);
		instructionPackage.setAmtMap(amtMap);
//		bookkeepingService.checkInfoSceneEntry(instructionPackage);
		//初始化表数据
		productApproveMain.setDealType(ParameterUtil.getString(params, "dealType", ""));
		productApproveMain.setDealDate(DateUtil.getCurrentDateAsString());
		productApproveMain.setIsActive(DictConstants.YesNo.YES);//有效
		productApproveMain.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		productApproveMain.setSponsor(productApproveMain.getSponsor() == null ? SlSessionHelper.getUserId() : productApproveMain.getSponsor());
		productApproveMain.setSponInst(productApproveMain.getSponInst() == null ? SlSessionHelper.getInstitutionId() : productApproveMain.getSponInst());
		String settlementDate = dayendDateService.getSettlementDate();
		productApproveMain.setaDate(settlementDate);	
		//拷贝数据  审批转核实
		if(DictConstants.YesNo.YES.equalsIgnoreCase(ParameterUtil.getString(params, "copyApprove", "")))
		{
			params.put("version",productApproveMain.getVersion());//设置初始
			baseAssetMapper.insertBaseAssetForCopy(params);
			baseAssetBlMapper.insertTdBaseAssetBlForCopy(params);//保理收款权
			calCashFlowService.createAllCashFlowsForTradCopy(params);//所以现金流拷贝
			riskAssetMapper.insertRiskAssetForCopy(params);//风险RWA拷贝
			baseRiskMapper.insertBaseRiskForCopy(params);//风险缓释
			baseAssetBillMapper.insertBaseAssetBillForCopy(params);//票据拷贝
			tdAssetLcMapper.insertAssetLcForCopy(params);//信用证拷贝
			productParticipantMapper.insertProductPartyCopy(params);//参与者拷贝
			productApproveSubMapper.insertProductApproveSubsCopy(params);//自由项拷贝
			//1.将实体交易持久化
			//拷贝 审批汇率到 核实汇率上面 暂时随便处理的,测试提的需求~我也不是很想改的
			//流水的测试,铁打的IT
			productApproveMain.setVerifyRate8(productApproveMain.getApproveRate8());
			productApproveMainMapper.insert(productApproveMain);//主体交易存储
			//合同信息拷贝
			productApproveProtocolsMapper.insertProductApproveProtocols(params);
			//特定债券
			assetSpecificBondMapper.insertTdAssetSpecificBondForCopy(params);
			//额度占用试运算
			productCustCreditMapper.insertTdProductCustCreditForCopy(params);
			//拷贝FEE交易
			productFeeDealMapper.insetProductFeeDealByDealNoCopy(params);
		    
		}else{
			//保存 SAVE 直接生成现金流计划
			if(!StringUtils.equalsIgnoreCase(String.valueOf(productApproveMain.getProduct().getPrdTerm()), "2")){//1定期 2活期  活期业务无预置现金流(目前只有存放同业活期)
				List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
				List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
				PrincipalInterval principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(productApproveMain.getvDate());principalInterval.setEndDate(productApproveMain.getmDate());principalInterval.setResidual_Principal(productApproveMain.getAmt());
				principalIntervals.add(principalInterval);
				InterestRange interestRange = new InterestRange();
				interestRange.setStartDate(productApproveMain.getvDate());interestRange.setEndDate(productApproveMain.getmDate());interestRange.setRate(productApproveMain.getContractRate());
				interestRanges.add(interestRange);
				
				List<TdFeesPassAgeway> feesPassagewyList = feesPassagewyMapper.getTdPassageWayByRefNo(params);
				HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
				
				try {
					feesPassAgewayService.createPassWayFeesPlan(productApproveMain, params, passAwayMap, principalIntervals, feesPassagewyList);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				calCashFlowService.createAllCashFlowsExpFees(principalIntervals, interestRanges, params,productApproveMain,passAwayMap);
			}
			//1.将参与方持久化
		    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
			//2.向main表中插入数据
			productApproveMainMapper.insert(productApproveMain);
			//3.向sub表插入数据
			updateProductApproveSub(productApproveMain);
			//4.费用依赖基础资产
			if(!StringUtils.equalsIgnoreCase(String.valueOf(productApproveMain.getPrdNo()), "907")) {
                cashflowFeeService.createFeeForProductDeals(params);//费用现金流
            }
			
			try {
				if("1".equals(productApproveMain.getNostroFlag()))
				{
					cashflowFeeService.createFeeForNostro(params,productApproveMain);//同业存放利息
				}
			} catch (Exception e) {
				JY.error("生成费用计划出错", e);
			}
			
			//保存客户行业类型
			counterPartyService.updateCounterKind(params);
			//联系人不能为空
			if(ParameterUtil.getString(params, "contact", null) != null && !"".equals(ParameterUtil.getString(params, "contact", null))) {
				Map<String, Object> paramsCounter = new HashMap<String,Object>();
				paramsCounter.put("partyId", ParameterUtil.getString(params, "cNo", ""));
				paramsCounter.put("name", ParameterUtil.getString(params, "contact", ""));
				paramsCounter.put("callPhone", ParameterUtil.getString(params, "contactPhone", ""));
				paramsCounter.put("address", ParameterUtil.getString(params, "contactAddr", ""));
				counterPartyService.updatePartyConByKey(paramsCounter);
			}
		}
		
		
	}

	@Override
	@AutoLogMethod(value="修改自定义业务金融工具表")
	public void updateProductApprove(Map<String, Object> params) {
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(ParameterUtil.getString(params, "dealNo", ""));
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(String.valueOf(productApproveMain.getPrdNo())));
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
		if(main!=null){
			int version =main.getVersion();
			BeanUtil.copyNotEmptyProperties(main, productApproveMain);
			main.setVersion(version);
		}
		//比较修改的金额是否小于等于预申请单
		if(main != null && main.getRefNo()!=null && !"".equalsIgnoreCase(main.getRefNo()))
		{
			TdProductApproveMain oldApproveMain =  productApproveMainMapper.getProductApproveMainActivated(main.getRefNo());
			if(PlaningTools.sub(oldApproveMain.getAmt(), main.getAmt())<0)
			{
				throw new RException("修改后的合同本金金额必须小于等于预申请单合同金额:<br/><font color='red'>"+doubleFormat.format(oldApproveMain.getAmt())+"</font>");
			}
		}
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		PrincipalInterval principalInterval = new PrincipalInterval();
		principalInterval.setStartDate(productApproveMain.getvDate());principalInterval.setEndDate(productApproveMain.getmDate());principalInterval.setResidual_Principal(productApproveMain.getAmt());
		principalIntervals.add(principalInterval);
		InterestRange interestRange = new InterestRange();
		interestRange.setStartDate(productApproveMain.getvDate());interestRange.setEndDate(productApproveMain.getmDate());interestRange.setRate(productApproveMain.getContractRate());
		interestRanges.add(interestRange);
		if(!StringUtils.equalsIgnoreCase(String.valueOf(productApproveMain.getProduct().getPrdTerm()), "2"))//1定期 2活期
		{
			List<TdFeesPassAgeway> feesPassagewyList = feesPassagewyMapper.getTdPassageWayByRefNo(params);
			HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
			
			try {
				feesPassAgewayService.createPassWayFeesPlan(productApproveMain, params, passAwayMap, principalIntervals, feesPassagewyList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			calCashFlowService.createAllCashFlowsExpFees(principalIntervals, interestRanges, params,productApproveMain,passAwayMap);
		}
		//1.将参与方持久化
	    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
	    BeanUtil.copyNotEmptyProperties(main, productApproveMain);
	    //初始化表数据
	  	main.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		//2.向main表中更新数据
		productApproveMainMapper.updateByPrimaryKey(main);
		//3.向sub表更新数据
		updateProductApproveSub(main);
		//4.费用依赖基础资产
		if(!StringUtils.equalsIgnoreCase(String.valueOf(productApproveMain.getPrdNo()), "907")) {
            cashflowFeeService.createFeeForProductDeals(params);//费用现金流
        }
		
		try {
			if("1".equals(productApproveMain.getNostroFlag()))
			{
				cashflowFeeService.createFeeForNostro(params,productApproveMain);//同业存放利息
			}
		} catch (Exception e) {
			JY.error("生成费用计划出错", e);
		}
		
		//保存客户行业类型
		counterPartyService.updateCounterKind(params);
		//联系人不能为空
		if(ParameterUtil.getString(params, "contact", null) != null && !"".equals(ParameterUtil.getString(params, "contact", null))) {
			Map<String, Object> paramsCounter = new HashMap<String,Object>();
			paramsCounter.put("partyId", ParameterUtil.getString(params, "cNo", ""));
			paramsCounter.put("name", ParameterUtil.getString(params, "contact", ""));
			paramsCounter.put("callPhone", ParameterUtil.getString(params, "contactPhone", ""));
			paramsCounter.put("address", ParameterUtil.getString(params, "contactAddr", ""));
			counterPartyService.updatePartyConByKey(paramsCounter);
		}
	}

	@Override
	@AutoLogMethod(value="删除产品审批")
	public void deleteProductApprove(String[] dealNos) {
		for (int i = 0; i < dealNos.length; i++) {
			TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(dealNos[i]);
			if (main.getApproveStatus() == null || !main.getApproveStatus().equals(DictConstants.ApproveStatus.New)) {
				JY.raiseRException("单号：" + main.getDealNo() + "，交易已发起不允许删除！");
			}
			
			try {
				//删除额度状态
				FBZ003 fbz=new FBZ003();
				fbz.setDealNo(dealNos[i]);
				fbzService.deleteFbz(fbz);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			
			Map<String, Object> m = new HashMap<String,Object>();
			m.put("dealNo",dealNos[i]);
			m.put("version", main.getVersion());
			productApproveMainMapper.deleteByPrimaryKey(m);
			removeProductApproveSub(dealNos);
			//删除参与方……
			tdProductParticipantService.deleteProductParticipantByDealNo(m);
			//删除现金流……
			calCashFlowService.deleteAllCashFlowsForDeleteDeal(m);
			//删除CRMS资产类交易关联表
			if(DictConstants.TrdType.AbssetForEnterpriceCredit.equals(main.getTrdtype()))
			{
				TdProductApproveCrms productApproveCrms = new TdProductApproveCrms();
				productApproveCrms.setDealNo(main.getDealNo());
				productApproveCrms.setVersion(main.getVersion());
				productApproveCrmsMapper.deleteByPrimaryKey(productApproveCrms);
			}
			//删除关联表
			baseAssetMapper.deleteBaseAssetByDealNo(main.getDealNo());
			baseAssetBlMapper.deleteTdBaseAssetBlByDealNo(main.getDealNo());//保理收款权
			riskAssetMapper.deleteRiskAssetByDealNo(main.getDealNo());//风险RWA拷贝
			baseRiskMapper.deleteBaseRiskByDealNo(main.getDealNo());//风险缓释
			baseAssetBillMapper.deleteBaseAssetBillByDealNo(main.getDealNo());//票据拷贝
			tdAssetLcMapper.deleteTdAssetLcByDealNo(main.getDealNo());//信用证拷贝
			//合同信息拷贝
			productApproveProtocolsMapper.deleteProductApproveProtocolsByDealNo(main.getDealNo());
			//特定债券
			assetSpecificBondMapper.deleteTdAssetSpecificBondByDealNo(main.getDealNo());
			//额度占用试运算
			productCustCreditMapper.deleteTdProductCustCreditByDealNo(main.getDealNo());
			//拷贝FEE交易
			productFeeDealMapper.deleteProductFeeDeals(BeanUtil.beanToHashMap(main));
			//查看是否占用了相应的额度、目前额度需检查2种；预占用、正式占用
			/**
			 * 额度释放
			 * 20170920
			 */
//			if(main.getPrdNo()!=298){//298 企信不占额度
//				/**
//				 * 额度释放，花落知多少，暴力啊
//				 * 20170920
//				 */
//				if(null != main.getRefNo())
//					if(!edCustManangeService.eduReleaseFlowService(main.getRefNo())){
//						throw new RException(main.getRefNo()+"额度释放失败!"+main.getRefNo());
//					}
//				if(!edCustManangeService.eduReleaseFlowService(main.getDealNo())){
//					throw new RException(main.getDealNo()+"额度释放失败!");
//				}
//			}
			
		}
	}

	@Override
	@AutoLogMethod(value="导入产品审批")
	public void importProductApprove() {
		
	}
	
	@Override
	public List<TdDurationCommon> getDurationCommonList(Map<String, Object> params) {
		return productApproveMainMapper.getDurationCommonList(params);
	}
	
	/**
	 * 查询产品是否需要放款
	 */
	@Override
	public boolean getProductIsNeedLoan(String prd_no) {
		int num = tdProductApproveNeedLoanMapper.isNeedLoan(prd_no);
		return num > 0;
	}
	
	/**
	 * 查询产品送交领导金额线
	 */
	@Override
	public double getProductAmtLine(String prd_no, String ccy) {
		if(StringUtil.isEmpty(ccy)){
			ccy = "CNY";
		}
		TdProductApproveAmtLine amtLine = tdProductApproveAmtLineMapper.searchAmtLine(prd_no, ccy);
		if(amtLine == null){
			return 0.0;
		}
		return amtLine.getAmtLine();
	}
	
	/**
	 * 查询本机构、下属机构、业务辖区机构中的业务
	 */
	@Override
	public Page<TdProductApproveMain> getProductApprovePageByInst(Map<String, Object> params) {
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitutionId());
		// 根据登录人机构ID查询他的子机构
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> inst = new ArrayList<String>();
		for(TtInstitution item : tlist){
			inst.add(item.getInstId());
		}
		params.put("isActive", 1);
		params.put("dealType", DictConstants.DealType.Verify);
		String[] approveStatusNo = new String[1];
		approveStatusNo[0]=DictConstants.ApproveStatus.Verified;
		params.put("approveStatusNo", approveStatusNo);
		Page<TdProductApproveMain> tdProductApproveMainList = productApproveMainMapper.getProductApproveMainListMine(params, ParameterUtil.getRowBounds(params));
		return tdProductApproveMainList;
	}
	
	/***
	 * ----以上为对外服务-------------------------------
	 */
	@Override
	public void updateProductApproveSub(TdProductApproveMain productApproveMain) {
		if("A".equals(productApproveMain.getAmtManage())){
			try {
				//删除额度状态
				FBZ003 fbz=new FBZ003();
				fbz.setDealNo(productApproveMain.getDealNo());
				fbzService.deleteFbz(fbz);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//审批子表
		Object obj = productApproveMain.getProductApproveSubs();
		if (obj != null) {
			productApproveSubMapper.deleteProductApproveSubByDealNo(productApproveMain.getDealNo());
			List<TdProductApproveSub> productApproveSubs = FastJsonUtil.parseArrays(obj.toString(), TdProductApproveSub.class);
			for (TdProductApproveSub productApproveSub : productApproveSubs) {
				productApproveSub.setGenMainNo(productApproveMain.getDealNo());
				productApproveSubMapper.insert(productApproveSub);
			}
		}
		//基础资产
		obj = productApproveMain.getBaseAssets();
		if (obj != null) {
			int seq = 1;
			baseAssetMapper.deleteBaseAssetByDealNo(productApproveMain.getDealNo());
			List<TdBaseAsset> baseAssets = FastJsonUtil.parseArrays(obj.toString(), TdBaseAsset.class);
			for (TdBaseAsset baseAsset : baseAssets) {
				baseAsset.setSeq(seq++);
				baseAsset.setDealNo(productApproveMain.getDealNo());
				baseAssetMapper.insert(baseAsset);
			}
		}
		obj=productApproveMain.getTdBaseAssetGuartys();
		if(obj!=null){
			int seq=1;
			assetGuartyMapper.deleteBaseAssetGuartyByDealNo(productApproveMain.getDealNo());
			List<TdBaseAssetGuarty> assetGuarties=FastJsonUtil.parseArrays(obj.toString(), TdBaseAssetGuarty.class);
			for(TdBaseAssetGuarty assetGuarty:assetGuarties){
				assetGuarty.setSeq(seq);
				assetGuarty.setDealNo(productApproveMain.getDealNo());
				assetGuartyMapper.insertBaseAsseetGuarty(assetGuarty);
			}
		}
		//结构化基础资产
		obj = productApproveMain.getTdBaseAssetStructs();
		if (obj != null) {
			assetStructMapper.deleteBaseAssetStructByDealNo(productApproveMain.getDealNo());
			List<TdBaseAssetStruct> baseAssets = FastJsonUtil.parseArrays(obj.toString(), TdBaseAssetStruct.class);
			int i = 1;
			for (TdBaseAssetStruct baseAsset : baseAssets) {
				baseAsset.setDealNo(productApproveMain.getDealNo());
				baseAsset.setSeqNo(String.valueOf(i++));
				try {
					assetStructMapper.insert(baseAsset);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		//保理收款权基础资产
		obj = productApproveMain.getBaseAssetBlInfos();
		if (obj != null) {
			baseAssetBlMapper.deleteTdBaseAssetBlByDealNo(productApproveMain.getDealNo());
			List<TdBaseAssetBl> baseAssetBls = FastJsonUtil.parseArrays(obj.toString(), TdBaseAssetBl.class);
			for (TdBaseAssetBl baseAssetBl : baseAssetBls) {
				baseAssetBl.setDealNo(productApproveMain.getDealNo());
				baseAssetBl.setVersion(productApproveMain.getVersion());
				baseAssetBlMapper.insert(baseAssetBl);
			}
		}
		//票据基础资产
		obj = productApproveMain.getBaseAssetBills();
		if (obj != null) {
			baseAssetBillMapper.deleteBaseAssetBillByDealNo(productApproveMain.getDealNo());
			List<TdBaseAssetBill> baseAssetBills = FastJsonUtil.parseArrays(obj.toString(), TdBaseAssetBill.class);
			for (TdBaseAssetBill baseAssetBill : baseAssetBills) {
				baseAssetBill.setDealNo(productApproveMain.getDealNo());
				baseAssetBillMapper.insert(baseAssetBill);
			}
		}
		
		//信用证基础资产
		obj = productApproveMain.getBaseAssetLCs();
		if (obj != null) {
			assetLcMapper.deleteTdAssetLcByDealNo(productApproveMain.getDealNo());
			List<TdAssetLc> tAssetLcs = FastJsonUtil.parseArrays(obj.toString(), TdAssetLc.class);
			for (TdAssetLc assetLc : tAssetLcs) {
				assetLc.setDealNo(productApproveMain.getDealNo());
				assetLcMapper.insert(assetLc);
			}
		}
		//风险资产
		obj = productApproveMain.getRiskAssets();
		if (obj != null) {
			riskAssetMapper.deleteRiskAssetByDealNo(productApproveMain.getDealNo());
			List<TdRiskAsset> riskAssets = FastJsonUtil.parseArrays(obj.toString(), TdRiskAsset.class);
			for (TdRiskAsset riskAsset : riskAssets) {
				riskAsset.setDealNo(productApproveMain.getDealNo());
				riskAssetMapper.insert(riskAsset);
			}
		}
		//缓释信息
		obj = productApproveMain.getBaseRisks();
		if (obj != null) {
			baseRiskMapper.deleteBaseRiskByDealNo(productApproveMain.getDealNo());
			List<TdBaseRisk> baseRisks = FastJsonUtil.parseArrays(obj.toString(), TdBaseRisk.class);
			for (TdBaseRisk baseRisk : baseRisks) {
				baseRisk.setDealNo(productApproveMain.getDealNo());
				baseRiskMapper.insert(baseRisk);
			}
		}
		//风险缓释
		obj = productApproveMain.getRiskSlowReleases();
		if (obj != null) {
			riskSlowReleaseMapper.deleteRiskSlowReleaseByDealNo(productApproveMain.getDealNo());
			List<TdRiskSlowRelease> riskSlowReleases = FastJsonUtil.parseArrays(obj.toString(), TdRiskSlowRelease.class);
			for (TdRiskSlowRelease riskSlowRelease : riskSlowReleases) {
				riskSlowRelease.setDealNo(productApproveMain.getDealNo());
				riskSlowReleaseMapper.insert(riskSlowRelease);
			}
		}
		//合同协议
		obj = productApproveMain.getProductProcotols();
		if (obj != null) {
			productApproveProtocolsMapper.deleteProductApproveProtocolsByDealNo(productApproveMain.getDealNo());
			List<TdProductApproveProtocols> productApproveProtocols = FastJsonUtil.parseArrays(obj.toString(), TdProductApproveProtocols.class);
			for (TdProductApproveProtocols productApproveProtocols2 : productApproveProtocols) {
				productApproveProtocols2.setDealNo(productApproveMain.getDealNo());
				productApproveProtocols2.setVersion(productApproveMain.getVersion());
				if(null != StringUtils.trimToNull(productApproveProtocols2.getProtocolId())) {
                    productApproveProtocolsMapper.insert(productApproveProtocols2);
                }
			}
		}
		
		//特定债券
		obj = productApproveMain.getBaseAssetSpecificBonds();
		if (obj != null) {
			assetSpecificBondMapper.deleteTdAssetSpecificBondByDealNo(productApproveMain.getDealNo());
			List<TdAssetSpecificBond> assetSpecificBonds = FastJsonUtil.parseArrays(obj.toString(), TdAssetSpecificBond.class);
			for (TdAssetSpecificBond assetSpecificBond : assetSpecificBonds) {
				assetSpecificBond.setDealNo(productApproveMain.getDealNo());
				assetSpecificBond.setVersion(productApproveMain.getVersion());
				assetSpecificBondMapper.insert(assetSpecificBond);
			}
		}
		
		//CheckList审查信息
		obj = productApproveMain.getTcLoanCheckboxDeal();
		if (obj != null) {
			List<TcLoanCheckboxDeal> list = FastJsonUtil.parseArrays(obj.toString(), TcLoanCheckboxDeal.class);
			if(list != null && list.size() >0){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ckPrd", list.get(0).getCkPrd());
				map.put("dealNo", productApproveMain.getDealNo());
				map.put("userId", list.get(0).getUserId());
				try {
					//查询当前环节、当前用户所属审批角色
					Map<String,Object> flowMap = flowQueryService.getFirstUserTaskSetting(list.get(0).getCkPrd(),productApproveMain.getDealType(),SlSessionHelper.getInstitution().getInstId());
					@SuppressWarnings("unchecked")
					List<String> lst_flow_role = (List<String>) flowMap.get("lst_flow_role");
					if(lst_flow_role!=null){
						if(lst_flow_role.size() > 0){
							map.put("roleNo", lst_flow_role.get(0));
						}
					}
					//先删除
					tcLoanCheckboxDealMapper.deleteTcLoanCheckboxDeal(map);
					map.put("loan_list", list);
					//查询该用户的审批角色，一个用户可以多个审批角色，查看历史时便于区分
					tcLoanCheckboxDealMapper.insertTcLoanCheckboxDeal(map);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		
		// 中介费
		obj = productApproveMain.getTdFeesBrokers();
		if (obj != null) {
			brokerMapper.deleteTdFeesByDealNo(productApproveMain.getDealNo());
			List<TdFeesBroker> tdFeesBrokers = FastJsonUtil.parseArrays(obj.toString(), TdFeesBroker.class);
			for (TdFeesBroker tdFeesBroker : tdFeesBrokers) {
				tdFeesBroker.setRefNo(productApproveMain.getDealNo());
				try {
					brokerMapper.insert(tdFeesBroker);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		//通道计划
		obj = productApproveMain.getFeesPassagewyList();
		if (obj != null) 
		{
			try {
				productApproveMain = getProductApproveActivated(productApproveMain.getDealNo());
				productApproveMain.setProduct(productMapper.getProductById(String.valueOf(productApproveMain.getPrdNo())));
				Map<String, Object> params = null;
				try{
					params = BeanUtil.beanToMap(productApproveMain);
				} catch (Exception e) {
					JY.raise(e.getMessage());
				}
				params.put("refNo", productApproveMain.getDealNo());
				
				List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
				PrincipalInterval principalInterval = null;
				double tempAmt = 0f;
				int count = 0 ; //将本金计划翻译成本金剩余区间
				//获得界面还本计划 TABLE列表数据
				List<CashflowCapital> cashflowCapitals = cashflowCapitalMapper.getCapitalListByRef(params);
				Collections.sort(cashflowCapitals);
				for(CashflowCapital tmCashflowCapital : cashflowCapitals)
				{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count == 0 ? productApproveMain.getvDate() : cashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
					count++;
				}
				
				List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
				InterestRange interestRange = new InterestRange();
				interestRange.setStartDate(productApproveMain.getvDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(productApproveMain.getmDate(), Frequency.DAY, -1));
				interestRange.setRate(productApproveMain.getContractRate());
				interestRanges.add(interestRange);
				
				
				List<TdFeesPassAgeway> feesPassagewyList = FastJsonUtil.parseArrays(obj.toString(), TdFeesPassAgeway.class);
				HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
				
				feesPassAgewayService.createPassWayFeesPlan(productApproveMain, params, passAwayMap, principalIntervals, feesPassagewyList);
				
				//重新生成收息、摊销计划
				calCashFlowService.createIntCashFlowsExpFees(cashflowCapitals,principalIntervals, interestRanges, params,productApproveMain,passAwayMap);
			} catch (Exception e) {
				e.printStackTrace();
				JY.error("生成通道计划出错:", e);
			}
		}
	}
	
	
	public void removeProductApproveSub(String[] dealNos) {
		for (int i = 0; i < dealNos.length; i++) {
			//审批子表
			productApproveSubMapper.deleteProductApproveSubByDealNo(dealNos[i]);
			//信用证
			tdAssetLcMapper.deleteTdAssetLcByDealNo(dealNos[i]);
			//基础资产
			baseAssetMapper.deleteBaseAssetByDealNo(dealNos[i]);
			//票据基础资产
			baseAssetBillMapper.deleteBaseAssetBillByDealNo(dealNos[i]);
			//风险资产
			riskAssetMapper.deleteRiskAssetByDealNo(dealNos[i]);
			//风险缓释
			riskSlowReleaseMapper.deleteRiskSlowReleaseByDealNo(dealNos[i]);
		}
	}
	
	@Override
	public void updateProductApproveMain(TdProductApproveMain main) {
		/*MmibFpMLVo mv = new MmibFpMLVo();
		mv.setFpML(main.getFpml());
		FpMLFactory.getInstance().parseFpML(mv);
		List<TtCashflowInterest> intList = new ArrayList<TtCashflowInterest>();
		List<TtCashflowCapital> capList = new ArrayList<TtCashflowCapital>();
		
		List<InterestCashFlow> intCfList = mv.getInterestCashFlowList();
		List<CapitalCashFlow> capCfList = mv.getCapitalCashFlowList();
		
		intList =calCashFlowService.toTtCashflowInterest(intCfList, main.getiCode(), main.getaType(), main.getmType(), main.getBasis());
		
		capList =calCashFlowService.toTtCashflowCapital(capCfList, main.getiCode(), main.getaType(), main.getmType());

		updateProductApproveMainVersion(main);
		
		updateProductApproveSub(main);
		
		Map<String,Object> cmap=new HashMap<String,Object>();
		cmap.put("iCode", main.getiCode());
		cmap.put("aType", main.getaType());
		cmap.put("mType", main.getmType());
		ttCashflowInterestMapper.deleteCfInterst(cmap);
		if(intList!=null && intList.size()>0){
			batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowInterestMapper.insert", intList);
		}
		ttCashflowCapitalMapper.deleteCfCapital(cmap);
		if(capList!=null && capList.size()>0){
			batchDao.batch("com.singlee.capital.cashflow.mapper.TtCashflowCapitalMapper.insert", capList);
		}*/
	}
	
	/**
	 * 更新金融工具表版本更新
	 */
	@Override
	public void updateProductApproveMainVersion(TdProductApproveMain main){
		//更新老版本数据状态
		int i = productApproveMainMapper.updateMainStatus(main);
		JY.require(i==1, "更新金融工具表状态失败.");
		int oldMaxVersion = productApproveMainMapper.selectMaxVersion(main);
		main.setVersion(oldMaxVersion+1);
		main.setIsActive(DictConstants.YesNo.YES);//有效
		main.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		productApproveMainMapper.insert(main);
	}

	/**
	 * 根据流水号和version版本号查询数据对象
	 */
	@Override
	public TdProductApproveMain getProductApproveMainByCondition(Map<String, Object> params) {
		return productApproveMainMapper.getProductApproveMainByCondition(params);
	}

	/**
	 * 修改数据
	 * @param approveMain
	 * @return
	 */
	@Override
	public int modifyProductApproveMain(TdProductApproveMain approveMain) {
		int i = productApproveMainMapper.updateByPrimaryKey(approveMain);
		return i;
	}

	@Override
	public double getNeedInterest(Map<String, Object> params) {
		//取当前激活的交易单
		String dealNo = ParameterUtil.getString(params, "dealNo", "");
		String tansferDate = ParameterUtil.getString(params, "transferDate", "");
		JY.require(StringUtil.isNotEmpty(tansferDate), "转让日期不能为空!");
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(dealNo);
		if(main!=null){
			MmibFpMLVo mv = new MmibFpMLVo();
			mv.setFpML(main.getFpml());
			FpMLFactory.getInstance().parseFpML(mv);
			AMmib am = MmibFactory.createMmib(mv);
			Date date =new Date(tansferDate);
			double interest = am.accruedInterest(date, false);
			return interest;

			
		}
		return 0.0;
	}

	@Override
	public double computeYiedOrFullPrice(Map<String, Object> params) {
		String tansferDate = ParameterUtil.getString(params, "transferDate", "");
		double tansferAmt = ParameterUtil.getDouble(params, "transferAmt", Double.NaN);
		double yieldRate = ParameterUtil.getDouble(params, "yieldRate", Double.NaN);
		//取当前激活的交易单
		String dealNo = ParameterUtil.getString(params, "dealNo", "");
		JY.require(StringUtil.isNotEmpty(tansferDate), "转让日期不能为空!");
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(dealNo);
		if(main!=null){
			MmibFpMLVo mv = new MmibFpMLVo();
			mv.setFpML(main.getFpml());
			FpMLFactory.getInstance().parseFpML(mv);
			AMmib am = MmibFactory.createMmib(mv);
			Date date =new Date(tansferDate);
			if(!Double.isNaN(tansferAmt)){
				double yield =0.0;
				try{
					yield =am.yieldToMaturity(date, tansferAmt);
					JY.require(!Double.isNaN(yield),"还没有起息或者剩余期限小于1天!");
				}
				catch(Exception e){
					JY.raise("计算资产转让收益率失败,可以关闭自动计算! %s", e.getMessage());
				}
				return yield;
			}
			if(!Double.isNaN(yieldRate)){
				double fullPrice =0.0;
				try{
					fullPrice =am.fullPrice(date, yieldRate);
					JY.require(!Double.isNaN(fullPrice),"还没有起息或者剩余期限小于1天!");
				}
				catch(Exception e){
					JY.raise("计算资产转让金额失败,可以关闭自动计算! %s", e.getMessage());
				}
				return fullPrice;
			}
			
		}
		return Double.NaN;

	}

	@Override
	public double computeMInterest(TdProductApproveMain main) {
		double m_int = main.getAmt()*main.getActuralRate()*main.getTerm()/getBasis(main.getBasis());
		return MathUtil.round(m_int, 2);
	}

	@Override
	public int computeTimeLimit(TdProductApproveMain main) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(sdf.parse(main.getvDate()));    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(sdf.parse(main.getmDate()));    
        long time2 = cal.getTimeInMillis();  
        if((time2-time1)<0){
        	return 0;
        }
        else{
	        long between_days=(time2-time1)/(1000*3600*24);  
	        return Integer.parseInt(String.valueOf(between_days)); 
        }
	}
	
	/**
	 * 计息基准转化
	 * @param basisStr
	 * @return
	 */
	public double getBasis(String basisStr){
		if ("Actual/365".equals(basisStr)) {
			return 365;
		} else if ("Actual/Actual(ISMA)".equals(basisStr)) {
			return 365;
		} else if ("Actual/365(Fixed)".equals(basisStr)) {
			return 365;
		} else if ("Actual/360".equals(basisStr)) {
			return 360;
		} else if ("30/360".equals(basisStr)) {
			return 360;
		}
		else{
			return 365;
		}
	}

	@Override
	public int updateMainRefNo(TdProductApproveMain main) {
		return productApproveMainMapper.updateMainRefNo(main);
	}

	@Override
	@AutoLogMethod(value="创建CRMS业务金融工具表")
	public void createCrmsProductApprove(Map<String, Object> params) {
		// TODO Auto-generated method stub
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}	
		//初始化表数据
		productApproveMain.setDealType(ParameterUtil.getString(params, "dealType", ""));
		productApproveMain.setDealDate(DateUtil.getCurrentDateAsString());
		productApproveMain.setIsActive(DictConstants.YesNo.YES);//有效
		productApproveMain.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		productApproveMain.setSponsor(productApproveMain.getSponsor() == null ? SlSessionHelper.getUserId() : productApproveMain.getSponsor());
		productApproveMain.setSponInst(productApproveMain.getSponInst() == null ? SlSessionHelper.getInstitutionId() : productApproveMain.getSponInst());
		String settlementDate = dayendDateService.getSettlementDate();
		productApproveMain.setaDate(settlementDate);	
		//拷贝数据  审批转核实
		if(DictConstants.YesNo.YES.equalsIgnoreCase(ParameterUtil.getString(params, "copyApprove", "")))
		{
			params.put("version",productApproveMain.getVersion());//设置初始
			productParticipantMapper.insertProductPartyCopy(params);//参与者拷贝
			//1.将实体交易持久化
			productApproveMainMapper.insert(productApproveMain);//主体交易存储
			//拷贝CRMS延伸段
			productApproveCrmsMapper.insertProductApproveCrmsCopy(params);
		    
		}else{
			//1.将参与方持久化
		    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
			//2.向main表中插入数据
			productApproveMainMapper.insert(productApproveMain);
			//3.向sub表插入数据
			updateProductApproveSub(productApproveMain);
			//3.持久化CRMS延伸表
			TdProductApproveCrms productApproveCrms = new TdProductApproveCrms();
			try{
				BeanUtil.populate(productApproveCrms, params);//再次赋值
			} catch (Exception e) {
				JY.raise(e.getMessage());
			}	
			productApproveCrmsMapper.insert(productApproveCrms);
		}
	}

	@Override
	@AutoLogMethod(value="更新CRMS业务金融工具表")
	public void updateCrmsProductApprove(Map<String, Object> params) {
		// TODO Auto-generated method stub
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(ParameterUtil.getString(params, "dealNo", ""));
		try{
			BeanUtil.populate(productApproveMain, params);
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
		if(main!=null){
			int version =main.getVersion();
			//BeanUtil.copyNotEmptyProperties(main, productApproveMain);
			BeanUtil.copyNotNullProperties(main, productApproveMain);
			main.setVersion(version);
		}
		//初始化表数据
		main.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		//1.将参与方持久化
	    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
		//2.向main表中更新数据
		productApproveMainMapper.updateByPrimaryKey(main);
		//3.向sub表插入数据
		updateProductApproveSub(productApproveMain);
		
		TdProductApproveCrms productApproveCrms = new TdProductApproveCrms();
		try{
			BeanUtil.populate(productApproveCrms, params);//再次赋值
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}	
		productApproveCrmsMapper.updateByPrimaryKey(productApproveCrms);
	}

	@Override
	public List<TdProductApproveProtocols> getTdProductApproveProtocolsForDealNo(
			String dealNo) {
		// TODO Auto-generated method stub
		return productApproveProtocolsMapper.getProductApproveProtocolsByDealNo(dealNo);
	}
	
	@Override
	public int saveTdProductCustCredit(Map<String,Object> map){
		String dealNo = ParameterUtil.getString(map, "dealNo","");
		String jsonStr =  ParameterUtil.getString(map, "jsonStr","");
		int num = 0;
		//额度信息
		if(dealNo != null && !"".equals(dealNo)){
			List<TdProductCustCredit> list = FastJsonUtil.parseArrays(jsonStr, TdProductCustCredit.class);
			if(list != null && list.size() >0){
				productCustCreditMapper.deleteTdProductCustCreditByDealNo(dealNo);
				for(TdProductCustCredit tdProductCustCredit : list){
					productCustCreditMapper.insert(tdProductCustCredit);
					num++;
				}
			}
		}
		return num;
	}
	@Override
	public FinEnterpriseTrade selectFinEnterpriseApproveTrade(
			Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return productApproveMainMapper.selectFinEnterpriseApproveTrade(map);
	}
	@Override
	public Page<TdProductApproveMain> getApproveOrderVoForCancelPage(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.productApproveMainMapper.getOrderVoPageForCancel(params, ParameterUtil.getRowBounds(params));
	}
	@Override
	public TdProductApproveMain getProductApproveForCancel(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		//取当前激活的审批单
		String dealNo = ParameterUtil.getString(params, "dealNo", "");
		TdProductApproveMain activeDeal = productApproveMainMapper.getProductApproveMainActivated(dealNo);
		return activeDeal;
	}
	
	
	@Override
	@AutoLogMethod(value="创建OVERDRAFT业务金融工具表")
	public void createOverDraftProductApprove(Map<String, Object> params) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}	
		//初始化表数据
		productApproveMain.setDealType(ParameterUtil.getString(params, "dealType", ""));
		productApproveMain.setDealDate(DateUtil.getCurrentDateAsString());
		productApproveMain.setIsActive(DictConstants.YesNo.YES);//有效
		productApproveMain.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		productApproveMain.setSponsor(productApproveMain.getSponsor() == null ? SlSessionHelper.getUserId() : productApproveMain.getSponsor());
		productApproveMain.setSponInst(productApproveMain.getSponInst() == null ? SlSessionHelper.getInstitutionId() : productApproveMain.getSponInst());
		String settlementDate = dayendDateService.getSettlementDate();
		productApproveMain.setaDate(settlementDate);	
		//拷贝数据  审批转核实
		if(DictConstants.YesNo.YES.equalsIgnoreCase(ParameterUtil.getString(params, "copyApprove", "")))
		{
			params.put("version",productApproveMain.getVersion());//设置初始
			
			productApproveOverDraftMapper.insertProductApproveOverDraftCopy(params);
			
			productParticipantMapper.insertProductPartyCopy(params);//参与者拷贝
			//1.将实体交易持久化
			productApproveMainMapper.insert(productApproveMain);//主体交易存储
		    
		}else{
			
			//X.持久化OVERDRAFT延伸表
			TdProductApproveOverDraft productApproveOverDraft = new TdProductApproveOverDraft();
			try{
				BeanUtil.populate(productApproveOverDraft, params);//再次赋值
			} catch (Exception e) {
				JY.raise(e.getMessage());
			}	
			productApproveOverDraftMapper.insert(productApproveOverDraft);
			//1.将参与方持久化
		    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
			//2.向main表中插入数据
			productApproveMainMapper.insert(productApproveMain);
			//3.向sub表插入数据
			updateProductApproveSub(productApproveMain);
			
		}
	}
	@Override
	public void updateOverDraftProductApprove(Map<String, Object> params) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(ParameterUtil.getString(params, "dealNo", ""));
		try{
			BeanUtil.populate(productApproveMain, params);
		}
		catch (Exception e) {
			JY.raise(e.getMessage());
		}
		if(main!=null){
			int version =main.getVersion();
			BeanUtil.copyNotEmptyProperties(main, productApproveMain);
			main.setVersion(version);
		}
		//初始化表数据
		main.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		TdProductApproveOverDraft productApproveOverDraft = new TdProductApproveOverDraft();
		try{
			BeanUtil.populate(productApproveOverDraft, params);//再次赋值
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}	
		productApproveOverDraftMapper.updateByPrimaryKey(productApproveOverDraft);
		//1.将参与方持久化
	    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
		//2.向main表中更新数据
		productApproveMainMapper.updateByPrimaryKey(main);
		//3.向sub表插入数据
		updateProductApproveSub(productApproveMain);
		
		
	}
	
	@Override
	public void updateDurationTradeLdno(Map<String, Object> params) {
		TdProductApproveMain approveMain=productApproveMainMapper.queryMainListforLdNo(params.get("ldNo").toString());
		if(approveMain == null){
			TdProductApproveMain main=new TdProductApproveMain();
			main.setDealNo(params.get("dealNo").toString());
			main.setLdNo(params.get("ldNo").toString());
			productApproveMainMapper.updateMainLdnoByDealno(main);
			
			TdProductApproveMain approveMainTemp=productApproveMainMapper.getProductApproveMainActivated(ParameterUtil.getString(params, "dealNo", ""));
			//1、判定如果不在IFBM出账那么直接情况还本还息等计划数据，采用后续T24数据填补
			if(!productService.checkIsAcctAcup(BeanUtil.beanToMap(approveMainTemp)))
			{
				calCashFlowService.deleteAllCashFlowsForDeleteDeal(BeanUtil.beanToMap(approveMainTemp));
			}
		}else{
			if(approveMain.getDealNo().equals(params.get("dealNo"))){//同一笔交易视为修改
				TdProductApproveMain main=new TdProductApproveMain();
				main.setDealNo(params.get("dealNo").toString());
				main.setLdNo(params.get("ldNo").toString());
				productApproveMainMapper.updateMainLdnoByDealno(main);
			}else{
				JY.raiseRException("LD号已存在！");
			}
		}
		
	}
	
	@Override
	public List<TdProductApproveSub> getProductApproveSubByDealno(Map<String, Object> params) throws Exception{
		TdProductApproveSub approveSub=new TdProductApproveSub();
		approveSub.setGenMainNo(params.get("genMainNo").toString());
		approveSub.setSubFldName("isSettlSuccess");
		List<TdProductApproveSub> list=productApproveSubMapper.getProductApproveSubList(params);
		if(ti2ndPaymentMapper.queryTi2ndPaymentCount(params.get("genMainNo").toString())>0){
			approveSub.setSubFldValue("1");
		}else{
			approveSub.setSubFldValue("0");
		}
		list.add(approveSub);
		return list;
	}
	
	//委外业务查询
	@Override
	public Page<TdProductApproveMain> getProductAppPage(Map<String, Object> params) {
		Page<TdProductApproveMain> page = productApproveMainMapper.selectGetOutProFinishList(params, ParameterUtil.getRowBounds(params));
		return page;
	}
	
	@Override
	public boolean checkProductCode(Map<String, Object> map) {
		return productApproveMainMapper.checkProductCode(map) == 0;
	}
	
	@Override
	public Map<String, String> queryUserParam(Map<String, Object> param) {
		Map<String, String> map = new HashMap<String, String>();
		List<TaUserParam> params = userParamMapper.searchTaUserParam(param);
		if(params != null && params.size() > 0){
			map.put("value", params.get(0).getP_value());
		}
		return map;
	}
	
	@Override
	public int getProductApproveMainListCount(Map<String, Object> params){
		// 机构隔离
		// 登录人机构ID
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitutionId());
		TtInstitution ttinstitu =new TtInstitution();
		ttinstitu.setInstId(SlSessionHelper.getInstitutionId());
		// 根据机构主键获取机构信息
		ttinstitu = institutionMapper.selectByPrimaryKey(ttinstitu);
		
		// 根据登录人机构ID查询他的子机构
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> ttlist=new ArrayList<String>();
		for (TtInstitution ttInstitution : tlist) {
			ttlist.add(ttInstitution.getInstId());   //登录人子机构ID
		}
		params.put("tttlist", ttlist);
		
		//获取当前登录用户的审批角色
		if(params.get("userId") != null && !"".equals(params.get("userId"))){
			String userId = params.get("userId").toString();
			HashMap<String, Object> param_map = new HashMap<String, Object>();
			param_map.put("user_id", userId);
			List<FlowRoleUserMapVo> list = ttFlowRoleUserMapMapper.listFlowRoleUserMap(param_map);
			if(list.size() == 1) {
				params.put("flow_role_id", list.get(0).getRole_id());					
			}
		}
		return productApproveMainMapper.getProductApproveMainListCount(params);
	}

}
