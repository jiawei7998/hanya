package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseRisk;

/**
 * @projectName 同业业务管理系统
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface BaseRiskService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 风险缓释对象集合
	 * @date 2016-10-14
	 */
	public List<TdBaseRisk> getBaseRiskList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 风险缓释对象集合
	 * @date 2016-10-14
	 */
	public Page<TdBaseRisk> getBaseRiskLists(Map<String, Object> params);
	
}
