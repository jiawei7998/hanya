package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

/***
 * 逾期交易确认表
 * @author kf0738
 *
 */
@Entity
@Table(name = "TD_OVERDUE_CONFIRM")
public class TdOverDueConfirm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String     dealNo    ; //审批单编号
	private String     dealType  ; //审批类型
	private String     sponsor    ; //审批发起人
	private String     sponInst  ; //审批发起机构
	private String     aDate     ; //审批开始日期
	private String     image      ; //影像资料
	private String     lastUpdate; //最后更新时间
	private Integer    version    ; //版本
	private String     refNo     ; //交易流水号
	private String     odVdate   ; //逾期起息日期
	private String     orgiMdate ; //原业务到期日期
	private int        orgiGdays ; //原业务到期宽限日
	private double     odRate    ; //逾期罚息利率_%
	private double     odAmt     ; //逾期补计利息
	private String     odRemark  ; //逾期备注
	private String     dealDate   ; //交易日期
	private double     remainAmt;
	
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	
	//现金流类型
	private String cfType;
	
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getOdVdate() {
		return odVdate;
	}
	public void setOdVdate(String odVdate) {
		this.odVdate = odVdate;
	}
	public String getOrgiMdate() {
		return orgiMdate;
	}
	public void setOrgiMdate(String orgiMdate) {
		this.orgiMdate = orgiMdate;
	}
	
	public int getOrgiGdays() {
		return orgiGdays;
	}
	public void setOrgiGdays(int orgiGdays) {
		this.orgiGdays = orgiGdays;
	}
	public double getOdRate() {
		return odRate;
	}
	public void setOdRate(double odRate) {
		this.odRate = odRate;
	}
	public double getOdAmt() {
		return odAmt;
	}
	public void setOdAmt(double odAmt) {
		this.odAmt = odAmt;
	}
	public String getOdRemark() {
		return odRemark;
	}
	public void setOdRemark(String odRemark) {
		this.odRemark = odRemark;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	@Override
	public String toString() {
		return "TdOverDueConfirm [dealNo=" + dealNo + ", dealType=" + dealType
				+ ", sponsor=" + sponsor + ", sponInst=" + sponInst
				+ ", aDate=" + aDate + ", image=" + image + ", lastUpdate="
				+ lastUpdate + ", version=" + version + ", refNo=" + refNo
				+ ", odVdate=" + odVdate + ", orgiMdate=" + orgiMdate
				+ ", orgiGdays=" + orgiGdays + ", odRate=" + odRate
				+ ", odAmt=" + odAmt + ", odRemark=" + odRemark + ", dealDate="
				+ dealDate + ", approveStatus=" + approveStatus + ", user="
				+ user + ", institution=" + institution + ", taskId=" + taskId
				+ ", product=" + product + ", party=" + party + ", prdName="
				+ prdName + "]";
	}
	


}
