package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdPort;

public interface TtTrdPortService {
	/**
	 * 查询投资组合列表
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public Page<TtTrdPort> searchTtTrdPort(Map<String, Object> param);
	
	/**
	 * 新增投资组合
	 * @param ttTrdPort
	 */
	public String insertTtTrdPort(TtTrdPort ttTrdPort);
	
	/**
	 * 修改投资组合
	 * @param ttTrdPort
	 */
	public void updateTtTrdPort(TtTrdPort ttTrdPort);
	
	/**
	 * 根据portfolio删除投资组合
	 * @param portfolio
	 */
	public void deleteTtTrdPortById(String[] portfolio);
}
