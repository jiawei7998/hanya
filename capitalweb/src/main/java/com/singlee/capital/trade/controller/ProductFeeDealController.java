package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.service.ProductFeeDealService;
@Controller
@RequestMapping(value = "/ProductFeeDealController")
public class ProductFeeDealController extends CommonController{
	
	@Autowired
	ProductFeeDealService productFeeDealService;
	
	
	@ResponseBody
	@RequestMapping(value = "/searchPageProductFeeDeals")
	public RetMsg<PageInfo<TdProductFeeDeal>> searchPageProductFeeDeals(@RequestBody Map<String,Object> params){
		Page<TdProductFeeDeal> page = productFeeDealService.getFeeDealsByOrgDealNo(params);
		return RetMsgHelper.ok(page);
	}

}
