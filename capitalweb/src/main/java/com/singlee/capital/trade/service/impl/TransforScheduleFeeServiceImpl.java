package com.singlee.capital.trade.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowFeeMapper;
import com.singlee.capital.cashflow.mapper.TmApproveFeeMapper;
import com.singlee.capital.cashflow.mapper.parent.CashFlowMapper;
import com.singlee.capital.cashflow.model.TmApproveFee;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdTransforScheduleFeeMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforScheduleFee;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TransforScheduleFeeService;
import com.singlee.slbpm.service.FlowOpService;

@Service
public class TransforScheduleFeeServiceImpl extends AbstractDurationService implements TransforScheduleFeeService {

	@Autowired
	private TdTransforScheduleFeeMapper TdTransforScheduleFeeMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private TmApproveFeeMapper approveFeeMapper;
	@Autowired
	private DayendDateService dateService;
	@Autowired
	private TdCashflowFeeMapper cashflowFeeMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Override
	public TdTransforScheduleFee getTransforScheduleFeeById(String dealNo) {
		return TdTransforScheduleFeeMapper.getTransforScheduleFeeById(dealNo);
	}

	@Override
	public Page<TdTransforScheduleFee> getTransforScheduleFeePage(
			Map<String, Object> params, int isFinished) {
		Page<TdTransforScheduleFee> page= new Page<TdTransforScheduleFee>();
		if(isFinished == 2){
			page= TdTransforScheduleFeeMapper.getTransforScheduleFeeList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= TdTransforScheduleFeeMapper.getTransforScheduleFeeListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= TdTransforScheduleFeeMapper.getTransforScheduleFeeListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTransforScheduleFee> list =page.getResult();
		for(TdTransforScheduleFee te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易展期金融工具表")
	public void createTransforScheduleFee(Map<String, Object> params) {
		TdTransforScheduleFeeMapper.insert(TransforScheduleFee(params));
	}

	@Override
	@AutoLogMethod(value = "修改交易展期金融工具表")
	public void updateTransforScheduleFee(Map<String, Object> params) {
		TdTransforScheduleFeeMapper.updateByPrimaryKey(TransforScheduleFee(params));
	}

	
	private TdTransforScheduleFee TransforScheduleFee(Map<String, Object> params){
		// map转实体类
		TdTransforScheduleFee TransforScheduleFee = new TdTransforScheduleFee();
		try {
			BeanUtil.populate(TransforScheduleFee, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTransforScheduleFee extend = new TdTransforScheduleFee();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(TransforScheduleFee.getDealType());
        extend = TdTransforScheduleFeeMapper.searchTransforScheduleFee(extend);

        if(TransforScheduleFee.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(TransforScheduleFee.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
            TransforScheduleFee.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(TransforScheduleFee.getDealType())){
    		TransforScheduleFee.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		TransforScheduleFee.setaDate(extend.getaDate());
    	}
    	TransforScheduleFee.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		//还本计划副本
		Object obj = TransforScheduleFee.getApproveFees();
		if (obj != null) {
			approveFeeMapper.deleteTmApproveFeeList(params);
			List<TmApproveFee> approveFees = FastJsonUtil.parseArrays(obj.toString(), TmApproveFee.class);
			int count = 1;
			Collections.sort(approveFees);
			for (TmApproveFee approveFee : approveFees) {
				approveFee.setSeqNumber(count);
				approveFee.setDealNo(TransforScheduleFee.getDealNo());
				approveFee.setVersion(TransforScheduleFee.getVersion());
				approveFee.setRefNo(TransforScheduleFee.getRefNo());
				approveFee.setPayDirection("Recevie");
				approveFeeMapper.insert(approveFee);
				count++;
			}
		}
		TransforScheduleFee.setaDate(DateUtil.getCurrentDateAsString());
		TransforScheduleFee.setSponsor(SlSessionHelper.getUserId());
		TransforScheduleFee.setSponInst(SlSessionHelper.getInstitutionId());
		return TransforScheduleFee;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTransforScheduleFee(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			TdTransforScheduleFeeMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTransforScheduleFee TransforScheduleFee = new TdTransforScheduleFee();
		BeanUtil.populate(TransforScheduleFee, params);
		TransforScheduleFee = TdTransforScheduleFeeMapper.searchTransforScheduleFee(TransforScheduleFee);
		return productApproveService.getProductApproveActivated(TransforScheduleFee.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTransforScheduleFee TransforScheduleFee = new TdTransforScheduleFee();
		BeanUtil.populate(TransforScheduleFee, params);
		TransforScheduleFee = TdTransforScheduleFeeMapper.searchTransforScheduleFee(TransforScheduleFee);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(TransforScheduleFee.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(TransforScheduleFee.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(TransforScheduleFee.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomCashFlowChangeInt);//
		tdTrdEvent.setEventTable("TD_TRASFOR_SCHEDULE_FEE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		
		/**
		 * 完成后需要将新的收息计划更新到数据库  TD_CASHFLOW_FEE
		 * 重算未收息的计划表
		 */
		TdTransforScheduleFee cfChange = new TdTransforScheduleFee();//本金计划审批数据
		BeanUtil.populate(cfChange, params);
		//删除FEDEALNO+DEALNO项下的未发生费用分拣的计划
		//将TM_APPROVE_FEE项下未嘎圣费用分拣的计划更新过来
		cashflowFeeMapper.deleteForApproveNotActual(params);
		approveFeeMapper.insertApproveCapitalToTd(params);
	}
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "com.singlee.capital.cashflow.mapper.Td";
        } else {
            return "com.singlee.capital.cashflow.mapper.Tm";
        }
	}
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "td";
        } else {
            return "tm";
        }
	}

	
	
	
	
	/**
	 * 计算获取应计利息 收息计划调整的时候预计算 
	 */
	@Override
	public double getAmIntCalForTrasforInterest(Map<String, Object> map) {
		String postdate = dateService.getDayendDate();
		String refBeginDate =  ParameterUtil.getString(map, "refBeginDate", postdate);
		String refEndDate =  ParameterUtil.getString(map, "refEndDate", postdate);
		String dealNo = ParameterUtil.getString(map, "dealNo", "");
		double executeRate = ParameterUtil.getDouble(map, "executeRate", 0.00);
		double remainAmt = ParameterUtil.getDouble(map, "remainAmt", 0.00);
		//提前支取日
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(dealNo)){
        	productApproveMain = productApproveService.getProductApproveActivated(dealNo);
            JY.require(productApproveMain!=null, "原交易信息不存在");
        }else{
        	throw new RException("原交易信息不存在！");
        }
		double amInt = 0.00;
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();
		InterestRange interestRange =  new InterestRange();
		interestRange.setStartDate(refBeginDate);
		interestRange.setEndDate(refEndDate);
		interestRange.setRate(executeRate);
		interestRanges.add(interestRange);
		//获取当前时点的剩余本金 ，获取后续 时间段的还本计划，计算出区间利息 
		
		List<CashflowCapital> tmCashflowCapitals = new ArrayList<CashflowCapital>();
		tmCashflowCapitals = ((CashFlowMapper)SpringContextHolder.getBean("tdCashflowCapitalMapper")).getCapitalList(map);//获得本金现金流
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		/**
		 * 将本金现金流 翻译为 本金变化区间
		 */
		try {
			int count  = 0;
			for(CashflowCapital tmCashflowCapital : tmCashflowCapitals){
				if(PlaningTools.compareDate(tmCashflowCapital.getRepaymentSdate(), refBeginDate, "yyyy-MM-dd"))
				{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?refBeginDate:tmCashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(remainAmt, tempAmt));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
					count++;
				}
			}
		
			amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, refBeginDate, refEndDate, refBeginDate, 
					DayCountBasis.equal(productApproveMain.getBasis()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			JY.require(true,"计算区间差额计提出错"+e.getMessage());
		}   
		return amInt;
	}
}
