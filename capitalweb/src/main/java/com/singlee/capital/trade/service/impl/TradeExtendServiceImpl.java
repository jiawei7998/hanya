package com.singlee.capital.trade.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TmApproveCapitalMapper;
import com.singlee.capital.cashflow.mapper.TmApproveInterestMapper;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.TmApproveCapital;
import com.singlee.capital.cashflow.model.TmApproveInterest;
import com.singlee.capital.cashflow.model.TmCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.duration.service.DurationConstants.RateType;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdTradeExtendMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTradeExtend;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TradeExtendService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * 到期展期交易的服务处理
 * 到期展期的起息日期必须大于等于原交易到期日
 * 计划的起始日必须全部大于原交易到期日
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeExtendServiceImpl extends AbstractDurationService implements TradeExtendService {

	@Autowired
	private TdTradeExtendMapper tdTradeExtendMapper;

	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TmApproveInterestMapper approveInterestMapper;
	@Autowired
	private TmApproveCapitalMapper approveCapitalMapper;
	@Autowired
	private TdCashflowCapitalMapper cashflowCapitalMapper;
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;
	@Autowired
	private TrdTposMapper tposMapper;
	@Autowired
	private BookkeepingService bookkeepingService;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	
	
	@Override
	public TdTradeExtend getTradeExtendById(String dealNo) {
		return tdTradeExtendMapper.getTradeExtendById(dealNo);
	}

	@Override
	public Page<TdTradeExtend> getTradeExtendPage(
			Map<String, Object> params, int isFinished) {
		Page<TdTradeExtend> page= new Page<TdTradeExtend>();
		if(isFinished == 2){
			page= tdTradeExtendMapper.getTradeExtendList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= tdTradeExtendMapper.getTradeExtendListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= tdTradeExtendMapper.getTradeExtendListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTradeExtend> list =page.getResult();
		for(TdTradeExtend te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易展期金融工具表")
	public void createTradeExtend(Map<String, Object> params) {
		try {
			tdTradeExtendMapper.insert(createTradeExtendCashFlow(params));
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise("创建交易展期出错", e);
		}
	}

	@Override
	@AutoLogMethod(value = "修改交易展期金融工具表")
	public void updateTradeExtend(Map<String, Object> params) {
		try {
			tdTradeExtendMapper.updateByPrimaryKey(createTradeExtendCashFlow(params));
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise("创建交易展期出错", e);
		}
	}

	
	public TdTradeExtend tradeExtend(Map<String, Object> params){
		// map转实体类
		TdTradeExtend tradeExtend = new TdTradeExtend();
		try {
			BeanUtil.populate(tradeExtend, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		//交易展期
		
		//收息计划副本
		approveInterestMapper.deleteTmApproveInterestList(params);
		String intcashtable = ParameterUtil.getString(params, "approveCashflowInterest","");
		List<TmApproveInterest> tmCashflowCapitals=FastJsonUtil.parseArrays(intcashtable, TmApproveInterest.class);
		int count = 1;
		Collections.sort(tmCashflowCapitals);
		int basicType = 0;
        if(tradeExtend.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
        	basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			
        	JY.require(productApproveMain!=null, "原交易信息不存在");
            if(DictConstants.intType.chargeBefore.equalsIgnoreCase(productApproveMain.getIntType()))
            {
            	throw new RException(productApproveMain.getDealNo()+"为前收息业务,不允许做展期!");
            }
        }else{
        	JY.raise(tradeExtend.getRefNo()+"原交易信息不存在");
        }
		for (TmApproveInterest approveInterest : tmCashflowCapitals) {
			approveInterest.setSeqNumber(count);
			approveInterest.setDealNo(tradeExtend.getDealNo());
			approveInterest.setVersion(tradeExtend.getVersion());
			approveInterest.setRefNo(tradeExtend.getRefNo());
			approveInterest.setCfType("Interest");
			approveInterest.setPayDirection("Recevie");
			approveInterest.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
			if(approveInterest.getTheoryPaymentDate() != null && approveInterest.getTheoryPaymentDate().length() > 10)
			{
				approveInterest.setTheoryPaymentDate(approveInterest.getTheoryPaymentDate().substring(0, 10));
			}
			if(approveInterest.getRefBeginDate() != null && approveInterest.getRefBeginDate().length() > 10)
			{
				approveInterest.setRefBeginDate(approveInterest.getRefBeginDate().substring(0, 10));
			}
			if(approveInterest.getRefEndDate() != null && approveInterest.getRefEndDate().length() > 10)
			{
				approveInterest.setRefEndDate(approveInterest.getRefEndDate().substring(0, 10));
			}
			approveInterestMapper.insert(approveInterest);
			count++;
		}
		
		//还本计划副本
		approveCapitalMapper.deleteApproveCapitalList(params);
		String capcashtable = ParameterUtil.getString(params, "approveCashflowCapitals","");
		List<TmApproveCapital> approveCapitals = FastJsonUtil.parseArrays(capcashtable, TmApproveCapital.class);
		count = 1;
		Collections.sort(approveCapitals);
		for (TmApproveCapital approveCapital : approveCapitals) {
			approveCapital.setSeqNumber(count);
			approveCapital.setDealNo(tradeExtend.getDealNo());
			approveCapital.setVersion(tradeExtend.getVersion());
			approveCapital.setRefNo(tradeExtend.getRefNo());
			approveCapital.setCfType(DictConstants.CashFlowType.Principal);
			approveCapital.setPayDirection("Recevie");
			approveCapital.setCfEvent(DictConstants.TrdType.CustomTradeExtend);
			if(approveCapital.getRepaymentSdate() != null && approveCapital.getRepaymentSdate().length() > 10)
			{
				approveCapital.setRepaymentSdate(approveCapital.getRepaymentSdate().substring(0, 10));
			}
			approveCapitalMapper.insert(approveCapital);
			count++;
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTradeExtend extend = new TdTradeExtend();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(tradeExtend.getDealType());
        extend = tdTradeExtendMapper.searchTradeExtend(extend);

        if(tradeExtend.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		tradeExtend.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(tradeExtend.getDealType())){
    		tradeExtend.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		tradeExtend.setaDate(extend.getaDate());
    	}
    	tradeExtend.setSponsor(SlSessionHelper.getUserId());
    	tradeExtend.setSponInst(SlSessionHelper.getInstitutionId());
		tradeExtend.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tradeExtend.setCfType(DictConstants.TrdType.CustomTradeExtend);
		return tradeExtend;
	}
	
	public TdTradeExtend createTradeExtendCashFlow(Map<String, Object> params) throws Exception{
		// map转实体类
		TdTradeExtend tradeExtend = new TdTradeExtend();
		try {
			BeanUtil.populate(tradeExtend, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		TdProductApproveMain productApproveMain = null;
		if(tradeExtend.getRefNo() != null){
			productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
			JY.require(productApproveMain!=null, "原交易信息不存在");
		}
		
		//生成交易展期收息计划
		int basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis()) 
				? 1 : DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis()) ? 2 : 3;
		
		Frequency frequency = com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre()));
		
		List<CashflowInterest> cashflowInterests = new ArrayList<CashflowInterest>();
		TmCashflowInterest fistPlanSchedule = null;
		try {
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(productApproveMain.getIntFre())) != com.singlee.capital.base.cal.Frequency.ONCE)
			{	
				fistPlanSchedule = new TmCashflowInterest();
				PlaningTools.calPlanScheduleRange(cashflowInterests, tradeExtend.getExpVdate(), tradeExtend.getExpMdate(),
						tradeExtend.getExpVdate(), tradeExtend.getExpRate(), frequency, DayCountBasis.valueOf(basicType), productApproveMain.getnDays());
				
			}else{
				fistPlanSchedule = new TmCashflowInterest();
				fistPlanSchedule.setSeqNumber(1);
				fistPlanSchedule.setRefBeginDate(tradeExtend.getExpVdate());
				fistPlanSchedule.setRefEndDate(tradeExtend.getExpMdate());
				fistPlanSchedule.setTheoryPaymentDate(tradeExtend.getExpMdate());
				fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
				fistPlanSchedule.setExecuteRate(tradeExtend.getExpRate());
				cashflowInterests.add(fistPlanSchedule);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		InterestRange interestRange = new InterestRange();
		interestRange.setStartDate(tradeExtend.getExpVdate());
		interestRange.setEndDate(tradeExtend.getExpMdate());
		interestRange.setRate(tradeExtend.getExpRate());
		interestRanges.add(interestRange);
		
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
		PrincipalInterval principalInterval = new PrincipalInterval();
		principalInterval.setStartDate(tradeExtend.getExpVdate());
		principalInterval.setEndDate(tradeExtend.getExpMdate());
		principalInterval.setResidual_Principal(tradeExtend.getExpAmt());
		principalIntervals.add(principalInterval);
		
		//临时重新计算通道计划 每日通道计提利息
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
		try {
			productApproveMain.setmDate(tradeExtend.getExpMdate());
			passAwayMap = tdFeesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals,
					productApproveMain,tradeExtend.getExpVdate(),"temp");
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("重新计算通道计划错误", e);
		}
		List<TdFeesPassAgewayDaily> fAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
		if(passAwayMap != null){
			fAgewayDailies.addAll(passAwayMap.values());
		}
		
		//生成交易展期收息计划
		List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		approveInterestMapper.deleteTmApproveInterestList(params);
		TmApproveInterest approveInterest = null;
		int count = 1;
		for (CashflowInterest cashflowInterest : cashflowInterests) {
			approveInterest = new TmApproveInterest();
			approveInterest.setSeqNumber(count);
			approveInterest.setDealNo(tradeExtend.getDealNo());
			approveInterest.setVersion(tradeExtend.getVersion());
			approveInterest.setRefNo(tradeExtend.getRefNo());
			approveInterest.setCfType("Interest");
			approveInterest.setPayDirection("Recevie");
			approveInterest.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
			
			approveInterest.setTheoryPaymentDate(cashflowInterest.getTheoryPaymentDate());
			approveInterest.setRefBeginDate(cashflowInterest.getRefBeginDate());
			approveInterest.setRefEndDate(cashflowInterest.getRefEndDate());
			approveInterest.setExecuteRate(tradeExtend.getExpRate());
			
			PlaningTools.fun_Calculation_interval_except_fess(interestRanges, 
					principalIntervals, 
					detailSchedules, 
					cashflowInterest, tradeExtend.getExpVdate(), 
					DayCountBasis.equal(productApproveMain.getBasis()), fAgewayDailies);
			
			approveInterest.setInterestAmt(cashflowInterest.getInterestAmt());
			
			approveInterestMapper.insert(approveInterest);
			count++;
		}
		
		//生成展期还本计划
		approveCapitalMapper.deleteApproveCapitalList(params);
		TmApproveCapital approveCapital = new TmApproveCapital();
		approveCapital.setSeqNumber(1);
		approveCapital.setDealNo(tradeExtend.getDealNo());
		approveCapital.setVersion(tradeExtend.getVersion());
		approveCapital.setRefNo(tradeExtend.getRefNo());
		approveCapital.setCfType(DictConstants.CashFlowType.Principal);
		approveCapital.setPayDirection("Recevie");
		approveCapital.setCfEvent(DictConstants.TrdType.CustomTradeExtend);
		approveCapital.setRepaymentSdate(tradeExtend.getExpMdate());
		approveCapital.setRepaymentSamt(tradeExtend.getExpAmt());
		approveCapitalMapper.insert(approveCapital);
		
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTradeExtend extend = new TdTradeExtend();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(tradeExtend.getDealType());
        extend = tdTradeExtendMapper.searchTradeExtend(extend);       
        tradeExtend.setVersion(productApproveMain.getVersion());
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(tradeExtend.getDealType())){
    		tradeExtend.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		tradeExtend.setaDate(extend.getaDate());
    	}
    	tradeExtend.setSponsor(SlSessionHelper.getUserId());
    	tradeExtend.setSponInst(SlSessionHelper.getInstitutionId());
		tradeExtend.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tradeExtend.setCfType(DictConstants.TrdType.CustomTradeExtend);
		return tradeExtend;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTradeExtend(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdTradeExtendMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}
	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		TdTradeExtend tradeExtend = new TdTradeExtend();
		BeanUtil.populate(tradeExtend, params);
		tradeExtend = tdTradeExtendMapper.searchTradeExtend(tradeExtend);
		return productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdTradeExtend tradeExtend = new TdTradeExtend();
		BeanUtil.populate(tradeExtend, params);
		if(tradeExtend.getDealNo() == null){
			tradeExtend.setDealNo(ParameterUtil.getString(params, "trade_id", null));
		}
		tradeExtend = tdTradeExtendMapper.searchTradeExtend(tradeExtend);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(tradeExtend.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(tradeExtend.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(tradeExtend.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomTradeExtend);//利率变更
		tdTrdEvent.setEventTable("TD_TRADE_EXTEND");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		TdTradeExtend tradeExtend = new TdTradeExtend();
		BeanUtil.populate(tradeExtend, params);
		/**
		 * 得到展期交易的起息日，获得满足的还本计划   TD_CASHFLOW_CAPITAL.REPAYMENT_SDATE<=EXP.VDATE AND REPAYMENT_TDATE IS NULL
		 * 找到，那么判断该条计划是否未还本金，是，那么更新期预期换本金额=该条计划原换本金额-展期金额；
		 */
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("refNo", tradeExtend.getRefNo());
		paramsMap.put("repaymentSdate", tradeExtend.getExpVdate());
		List<TdCashflowCapital> capitals = cashflowCapitalMapper.getApproveTradeExtendNearCaplital(paramsMap);
		if(capitals.size()>0){
			Collections.sort(capitals);//升序
			TdCashflowCapital tempCashflowCapital = null;
			double remainAmt = tradeExtend.getExpAmt();
			for(int i=(capitals.size()-1);i>=0;i--)//按最大计划的金额递减
			{
				tempCashflowCapital = capitals.get(i);
				remainAmt = PlaningTools.sub(remainAmt, tempCashflowCapital.getRepaymentSamt());
				if(remainAmt>=0){
					tempCashflowCapital.setRepaymentSamt(0);
					cashflowCapitalMapper.updateByPrimaryKey(tempCashflowCapital);
				}else
				{
					tempCashflowCapital.setRepaymentSamt((Math.abs(remainAmt)));
					cashflowCapitalMapper.updateByPrimaryKey(tempCashflowCapital);
					break;
				}				
			}
		}
		/**
		 * 展期交易完成后，需要在原交易中添加新的收息计划  还本计划(交易里面的计划起始日必然大于之前的交易到期日)
		 * 只要将剩余的计划直接追加到后续计划即可
		 * 1、将还本计划直接追加到原交易的还本计划之后
		 * 2、将收息计划直接追加到原计划的收息计划之后
		 * 3、将利息变化添加到TT_RATE_RANGE后面
		 */
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
		JY.require(productApproveMain!=null, "原交易流水不存在！异常！");
		/*****************************************************************************************************************/
		//1、将还本计划直接追加到原交易的还本计划之后
		List<TmApproveCapital> approveCapitals = this.approveCapitalMapper.getApproveCapitalList(params);//按展期交易获得伴随的还本计划
		Collections.sort(approveCapitals);
		List<TdCashflowCapital> cashflowCapitals = new ArrayList<TdCashflowCapital>();
		TdCashflowCapital cashflowCapital = null;
		for(TmApproveCapital approveCapital : approveCapitals)
		{
			cashflowCapital = new TdCashflowCapital();
			BeanUtil.copyNotEmptyProperties(cashflowCapital, approveCapital);
			cashflowCapital.setCfEvent(DictConstants.TrdType.CustomTradeExtend);//展期
			cashflowCapital.setDealNo(approveCapital.getRefNo());//关联号=原交易号
			cashflowCapitals.add(cashflowCapital);
		}
		batchDao.batch(getSqlForCashFlow(params)+"CashflowCapitalMapper.insert", cashflowCapitals);
		//2、将收息计划直接追加到原交易的收息计划之后
		List<TmApproveInterest> approveInterests = this.approveInterestMapper.getTmApproveInterestList(params);//按展期交易获得伴随的收息计划
		Collections.sort(approveInterests);
		List<TdCashflowInterest> cashflowInterests = new ArrayList<TdCashflowInterest>();
		TdCashflowInterest cashflowInterest = null;
		for(TmApproveInterest approveInterest : approveInterests)
		{
			cashflowInterest = new TdCashflowInterest();
			BeanUtil.copyNotEmptyProperties(cashflowInterest, approveInterest);
			//cashflowInterest.setCfType(DictConstants.TrdType.CustomTradeExtend);//展期
			cashflowInterest.setDealNo(approveInterest.getRefNo());//关联号=原交易号
			cashflowInterest.setActuralRate(approveInterest.getExecuteRate());
			cashflowInterests.add(cashflowInterest);
		}
		batchDao.batch(getSqlForCashFlow(params)+"CashflowInterestMapper.insert", cashflowInterests);
		//3、将利率计划装载到 TT_RATE_RANGE
		List<TdRateRange> rateRanges = new ArrayList<TdRateRange>();
		TdRateRange rateRange = null;
		for(TmApproveInterest approveInterest : approveInterests)
		{
			rateRange = new TdRateRange();
			rateRange.setDealNo(approveInterest.getRefNo());//关联号=原交易号
			rateRange.setVersion(approveInterest.getVersion());
			rateRange.setBeginDate(approveInterest.getRefBeginDate());
			rateRange.setEndDate(approveInterest.getRefEndDate());
			rateRange.setExecRate(approveInterest.getExecuteRate());
			rateRange.setAccrulType(RateType.ACCRU_TYPE);
			rateRanges.add(rateRange);
		}
		batchDao.batch("com.singlee.capital.cashflow.mapper.TdRateRangeMapper.insert", rateRanges);
		
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
		//重新计算通道计划 每日通道计提利息
		try {
			paramsMap.put("dealNo", productApproveMain.getDealNo());
			paramsMap.put("mDate", tradeExtend.getExpMdate());
			productApproveMainMapper.updateProductApproveMain(paramsMap);
			
			//翻译本金区间
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			Collections.sort(cashflowCapitals);
			String endDate = null;
			PrincipalInterval principalInterval=null;
			double tempAmt = 0f;
			int count = 0 ; //将本金计划翻译成本金剩余区间
			HashMap<String, PrincipalInterval> prHashMap = new HashMap<String, PrincipalInterval>();
			for(CashflowCapital tmCashflowCapital : cashflowCapitals)
			{
				endDate = PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1);
				if(prHashMap.get(endDate) != null)
				{
					prHashMap.get(endDate).setResidual_Principal(PlaningTools.sub(tradeExtend.getExpAmt(), tempAmt));
				
				}else{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?tradeExtend.getExpVdate():cashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(tradeExtend.getExpAmt(), tempAmt));
					principalIntervals.add(principalInterval);

					prHashMap.put(principalInterval.getEndDate(), principalInterval);
				}
			
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			productApproveMain.setmDate(tradeExtend.getExpMdate());
			passAwayMap = tdFeesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals,
					productApproveMain,tradeExtend.getExpVdate(),"act");
			
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间列表
			InterestRange interestRange = null;
			interestRange = new InterestRange();
			interestRange.setStartDate(tradeExtend.getExpVdate());
			interestRange.setRate(tradeExtend.getExpRate());
			interestRange.setEndDate(tradeExtend.getExpMdate());
			interestRanges.add(interestRange);			
			
			List<TdFeesPassAgewayDaily> fAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
			if(passAwayMap != null){
				fAgewayDailies.addAll(passAwayMap.values());
			}
			
			List<CashflowDailyInterest> tdCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			for(TdCashflowInterest cfInterest:cashflowInterests){
				try {
					PlaningTools.fun_Calculation_interval_except_fess(interestRanges, 
							principalIntervals, 
							tdCashflowDailyInterests, 
							cfInterest, tradeExtend.getExpVdate(), 
							DayCountBasis.equal(cfInterest.getDayCounter()), fAgewayDailies);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("重新计算应计利息异常"+e.getMessage());
				}
			}
			for(int i =0;i<tdCashflowDailyInterests.size();i++)
			{
				tdCashflowDailyInterests.get(i).setDealNo(productApproveMain.getDealNo());
				tdCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
			}
			if(tdCashflowDailyInterests.size() > 0)
			{
				batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", tdCashflowDailyInterests);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("重新计算利息错误", e);
		}
		
		//判断是否需要调整利息
		try {
			//调用账务接口
			String curDate= dayendDateService.getDayendDate();
			int days = PlaningTools.daysBetween(tradeExtend.getExpVdate(),curDate);
			if(days<=0) {
                return;//结束
            }
			if(productApproveMain.getIntType().equalsIgnoreCase(DictConstants.intType.chargeAfter))// 后收息
			{
				double dayCalAmt = 0.00;//调整金额
				for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
					dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(tradeExtend.getExpVdate(), PlaningTools.addOrSubMonth_Day(tradeExtend.getExpVdate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(tradeExtend.getExpAmt(),tradeExtend.getExpRate())),DayCountBasis.equal(productApproveMain.getBasis()).getDenominator())
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(tradeExtend.getExpVdate(),PlaningTools.addOrSubMonth_Day(tradeExtend.getExpVdate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(tradeExtend.getExpAmt(),tradeExtend.getExpRate())
												),DayCountBasis.equal(productApproveMain.getBasis()).getDenominator())				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
					
					}
				 Map<String, Object> amtMap = new HashMap<String, Object>();
				 amtMap.put(DurationConstants.AccType.AMT_INTEREST_ADJ, dayCalAmt);
				 TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
				 accTrdDaily.setAccAmt(dayCalAmt);
				 accTrdDaily.setAccType(DurationConstants.AccType.AMT_INTEREST_ADJ);
				 accTrdDaily.setDealNo(productApproveMain.getDealNo());
				 accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				 accTrdDaily.setPostDate(curDate);
				 accTrdDaily.setRefNo(tradeExtend.getDealNo());
				 InstructionPackage instructionPackage = new InstructionPackage();
				 Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
				 instructionPackage.setInfoMap(tradeMap);
				 instructionPackage.setAmtMap(amtMap);
				 String acctNo = bookkeepingService.generateEntry4Instruction(instructionPackage);
				 if(null != acctNo && !"".equals(acctNo))
				 {//跟新回执会计套号
					accTrdDaily.setAcctNo(acctNo);
				 }
				 accTrdDailyMapper.insert(accTrdDaily);
				 TdTrdTpos tpos = new TdTrdTpos();
				 tpos.setDealNo(productApproveMain.getDealNo());
				 tpos.setVersion(productApproveMain.getVersion());
				 tpos = tposMapper.selectByPrimaryKey(tpos);
				 tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计180应计利息
				 tpos.setActualTint(PlaningTools.sub(tpos.getActualTint(), dayCalAmt));//累计514利息收入
				 tpos.setPostdate(curDate);
				 this.tposMapper.updateByPrimaryKey(tpos);
			}else
			{
				throw new RException("业务处理失败，当前只能处理前收息情况");
			}
			
		} catch (Exception e) {
			//JY.raise(e.getMessage());
			JY.error("生成展期利息出错", e);
		}
		
	}
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "com.singlee.capital.cashflow.mapper.Td";
        } else {
            return "com.singlee.capital.cashflow.mapper.Tm";
        }
	}
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "td";
        } else {
            return "tm";
        }
	}

	@Override
	public Page<TdTradeExtend> getTradeExtendForDealNoPage(Map<String, Object> params) {
		return this.tdTradeExtendMapper.getAllTradeExtendsByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}

	@Override
	public Map<String, Object> calExtendInterestPlanForTemp(Map<String, Object> params) throws Exception {
		Map<String, Object> retMap = new HashMap<String, Object>();
		/**
		 * 得到界面的serch_form数据  收息计划修改;转换为JAVABEAN
		 */
		String tmCashflowInterest = ParameterUtil.getString(params, "tmCashflowInterest","");
		TmCashflowInterest pageBean = FastJsonUtil.parseObject(tmCashflowInterest, TmCashflowInterest.class);
		params.put("changeDate", pageBean.getRefBeginDate());
		List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntByDate(params);
		if(cashflowInterests != null && cashflowInterests.size() > 0){
			retMap.put("code", "100");
			retMap.put("msg", "调整的计划起息日期必须大于或等于已收息计划的结束日期!");
			return retMap;
		}
		
		TdTradeExtend tdTradeExtend = tdTradeExtendMapper.getTradeExtendById(String.valueOf(params.get("dealNo")));
		
		//利息明细
		List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();
		List<TmApproveInterest> resultList = new ArrayList<TmApproveInterest>();//最终的收息计划列表，需要最后用于持久化
		HashMap<String, CashflowInterest> dateSplitHashMap = new HashMap<String, CashflowInterest>();
		List<TmApproveCapital> tmCashflowCapitals = new ArrayList<TmApproveCapital>();
		List<CashflowInterest> tmCashflowInterests = new ArrayList<CashflowInterest>();
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间列表
		try{
			String tmCashflowInterestList = ParameterUtil.getString(params, "tmCashflowInterestList","");
			tmCashflowInterests = FastJsonUtil.parseArrays(tmCashflowInterestList, CashflowInterest.class);
			
			params.put("dealNo", tdTradeExtend.getDealNo());
			tmCashflowCapitals = approveCapitalMapper.getApproveCapitalList(params);//获得本金现金流 临时表
			
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			PrincipalInterval principalInterval=null;double tempAmt =0f;
			/**
			 * 将本金现金流 翻译为 本金变化区间
			 */
			int count  = 0;
			for(TmApproveCapital tmCashflowCapital : tmCashflowCapitals){
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(count==0 ? tdTradeExtend.getExpVdate():tmCashflowCapitals.get(count-1).getRepaymentSdate());
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
				principalInterval.setResidual_Principal(PlaningTools.sub(tdTradeExtend.getExpAmt(), tempAmt));
				principalIntervals.add(principalInterval);
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			dateSplitHashMap.put(pageBean.getRefBeginDate(), pageBean);
			for(CashflowInterest temp : tmCashflowInterests)
			{
				/**
				 * 拆分计划为 起息日 到期日
				 * 保留 起息日 到期日 分别小于/大于  更改过的 起息日和到期日
				 */
				if(PlaningTools.compareDate2(pageBean.getRefBeginDate(), temp.getRefBeginDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate2(temp.getRefBeginDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(temp.getRefBeginDate(), temp);
				}
				if(PlaningTools.compareDate(pageBean.getRefEndDate(), temp.getRefBeginDate(), "yyyy-MM-dd")
						&& PlaningTools.compareDate(temp.getRefEndDate(), pageBean.getRefEndDate(), "yyyy-MM-dd"))
				{
					dateSplitHashMap.put(pageBean.getRefEndDate(), temp);
				}
				
			}
			dateSplitHashMap.put(tdTradeExtend.getExpMdate(), tmCashflowInterests.get(tmCashflowInterests.size()-1));
			/**
			 * 进行排序
			 */
			
			List<Map.Entry<String, CashflowInterest>> arrayList = new ArrayList<Entry<String, CashflowInterest>>(dateSplitHashMap.entrySet());
			Collections.sort(arrayList, new Comparator<Object>() {
				   @Override
				   public int compare(Object o1, Object o2) {
					    @SuppressWarnings("unchecked")
						Map.Entry<String, CashflowInterest> obj1 = (Map.Entry<String, CashflowInterest>) o1;
					    @SuppressWarnings("unchecked")
					    Map.Entry<String, CashflowInterest> obj2 = (Map.Entry<String, CashflowInterest>) o2;
					    try {
							return PlaningTools.compareDate2(obj1.toString(), obj2.toString(), "yyyy-MM-dd") == true?1:0;
						} catch (ParseException e) {
							return 0;
						}
				   }
			 });
			/**
			 * 创建利率区间
			 */
			
			InterestRange interestRange = null;
			
			for(int i=0 ; i < arrayList.size()-1;i++){
				interestRange = new InterestRange();
				interestRange.setStartDate(arrayList.get(i).getKey());
				interestRange.setRate(arrayList.get(i).getValue().getExecuteRate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(arrayList.get(i+1).getKey(), Frequency.DAY, -1));
				interestRanges.add(interestRange);			
			}
			
			//临时重新计算通道计划 每日通道计提利息
			HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
			try {
				TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
				productApproveMain.setmDate(tdTradeExtend.getExpMdate());
				
				passAwayMap = tdFeesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals,
						productApproveMain,tdTradeExtend.getExpVdate(),"temp");
			} catch (Exception e) {
				e.printStackTrace();
				JY.error("重新计算通道计划错误", e);
			}
			List<TdFeesPassAgewayDaily> fessList = new ArrayList<TdFeesPassAgewayDaily>();
			if(passAwayMap != null){
				fessList.addAll(passAwayMap.values());
			}
			
			CashflowInterest temp = null;
			TmApproveInterest tmCashflowInterest2 = null;
			for(int i = 0 ; i < arrayList.size()-1 ; i++){
				tmCashflowInterest2 = new TmApproveInterest();
				tmCashflowInterest2.setDayCounter(pageBean.getDayCounter());
				tmCashflowInterest2.setDealNo(tdTradeExtend.getDealNo());
				tmCashflowInterest2.setRefNo(tdTradeExtend.getRefNo());
				tmCashflowInterest2.setExecuteRate(arrayList.get(i).getValue().getExecuteRate());
				tmCashflowInterest2.setRefBeginDate(arrayList.get(i).getKey());
				tmCashflowInterest2.setRefEndDate(arrayList.get(i+1).getKey());
				tmCashflowInterest2.setTheoryPaymentDate(PlaningTools.compareDateEqual(arrayList.get(i).getValue().getRefEndDate(), arrayList.get(i+1).getKey(), "yyyy-MM-dd")?arrayList.get(i).getValue().getTheoryPaymentDate():arrayList.get(i+1).getKey());
				tmCashflowInterest2.setCfType("Interest");
				tmCashflowInterest2.setPayDirection("Recevie");
				
				temp = new CashflowInterest();
				temp.setRefBeginDate(tmCashflowInterest2.getRefBeginDate());
				temp.setRefEndDate(tmCashflowInterest2.getRefEndDate());
				temp.setExecuteRate(tmCashflowInterest2.getExecuteRate());
				temp.setDealNo(tmCashflowInterest2.getDealNo());
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges, principalIntervals, 
						tmCashflowDailyInterests, temp, tdTradeExtend.getExpVdate(), 
						DayCountBasis.equal(pageBean.getDayCounter()), fessList);
				
				tmCashflowInterest2.setExecuteRate(temp.getExecuteRate());
				tmCashflowInterest2.setInterestAmt(temp.getInterestAmt());
				
				tmCashflowInterest2.setVersion(tdTradeExtend.getVersion());
				tmCashflowInterest2.setSeqNumber(i+1);
				resultList.add(tmCashflowInterest2);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		retMap.put("code", "000");
		retMap.put("data", resultList);
		return retMap;
	}
	
	
	private void saveExtendCapitalCashFlow(Map<String, Object> params) throws Exception{
		// map转实体类
		TdTradeExtend tradeExtend = tdTradeExtendMapper.getTradeExtendById(String.valueOf(params.get("dealNo")));
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
        //还本计划副本
  		approveCapitalMapper.deleteApproveCapitalList(params);
  		String capcashtable = ParameterUtil.getString(params, "approveCashflowCapitals","");
  		List<TmApproveCapital> approveCapitals = FastJsonUtil.parseArrays(capcashtable, TmApproveCapital.class);
  		int count = 1;
  		Collections.sort(approveCapitals);
  		for (TmApproveCapital approveCapital : approveCapitals) {
  			approveCapital.setSeqNumber(count);
  			approveCapital.setDealNo(tradeExtend.getDealNo());
  			approveCapital.setVersion(tradeExtend.getVersion());
  			approveCapital.setRefNo(tradeExtend.getRefNo());
  			approveCapital.setCfType(DictConstants.CashFlowType.Principal);
  			approveCapital.setPayDirection("Recevie");
  			approveCapital.setCfEvent(DictConstants.TrdType.CustomTradeExtend);
  			if(approveCapital.getRepaymentSdate() != null && approveCapital.getRepaymentSdate().length() > 10)
  			{
  				approveCapital.setRepaymentSdate(approveCapital.getRepaymentSdate().substring(0, 10));
  			}
  			approveCapitalMapper.insert(approveCapital);
  			count++;
  		}
  		
  		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		count =0 ; //将本金计划翻译成本金剩余区间
		for(TmApproveCapital tmCashflowCapital : approveCapitals)
		{
			principalInterval = new PrincipalInterval();
			principalInterval.setStartDate(count==0 ? tradeExtend.getExpVdate() : approveCapitals.get(count-1).getRepaymentSdate());
			principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
			principalInterval.setResidual_Principal(PlaningTools.sub(tradeExtend.getExpAmt(), tempAmt));
			principalIntervals.add(principalInterval);
			tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
			count++;
		}
  		
  		//利率区间
  		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		InterestRange interestRange = new InterestRange();
		interestRange.setStartDate(tradeExtend.getExpVdate());
		interestRange.setEndDate(tradeExtend.getExpMdate());
		interestRange.setRate(tradeExtend.getExpRate());
		interestRanges.add(interestRange);
		
		//临时重新计算通道计划 每日通道计提利息
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
		try {
			productApproveMain.setmDate(tradeExtend.getExpMdate());
			passAwayMap = tdFeesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals,
					productApproveMain,tradeExtend.getExpVdate(),"temp");
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("重新计算通道计划错误", e);
		}
		List<TdFeesPassAgewayDaily> fAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
		if(passAwayMap != null){
			fAgewayDailies.addAll(passAwayMap.values());
		}
		
		//生成交易展期收息计划
		List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		
		List<TmApproveInterest> cashflowInterests = approveInterestMapper.getTmApproveInterestList(params);
		CashflowInterest temp = null;
		for (TmApproveInterest approveInterest : cashflowInterests) {
			temp = new CashflowInterest();
			temp.setRefBeginDate(approveInterest.getRefBeginDate());
			temp.setRefEndDate(approveInterest.getRefEndDate());
			temp.setExecuteRate(approveInterest.getExecuteRate());
			temp.setDealNo(approveInterest.getDealNo());
			PlaningTools.fun_Calculation_interval_except_fess(interestRanges, 
					principalIntervals, 
					detailSchedules, 
					temp, tradeExtend.getExpVdate(), 
					DayCountBasis.equal(productApproveMain.getBasis()), fAgewayDailies);
			
			approveInterest.setInterestAmt(temp.getInterestAmt());
			approveInterestMapper.updateByPrimaryKey(approveInterest);
		}
	}
	
	private void saveExtendIntCashFlow(Map<String, Object> params) throws Exception{
		// map转实体类
		TdTradeExtend tradeExtend = tdTradeExtendMapper.getTradeExtendById(String.valueOf(params.get("dealNo")));
		//收息计划副本
		approveInterestMapper.deleteTmApproveInterestList(params);
		String intcashtable = ParameterUtil.getString(params, "approveCashflowInterest","");
		List<TmApproveInterest> tmCashflowInterests = FastJsonUtil.parseArrays(intcashtable, TmApproveInterest.class);
		int count = 1;
		Collections.sort(tmCashflowInterests);
		int basicType = 0;
        if(tradeExtend.getRefNo() != null)
        {
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExtend.getRefNo());
        	basicType = DictConstants.DayCounter.Actual360.equals(productApproveMain.getBasis())?1:DictConstants.DayCounter.Actual365.equals(productApproveMain.getBasis())?2:3;
			
        	JY.require(productApproveMain!=null, "原交易信息不存在");
            if(DictConstants.intType.chargeBefore.equalsIgnoreCase(productApproveMain.getIntType()))
            {
            	throw new RException(productApproveMain.getDealNo()+"为前收息业务,不允许做展期!");
            }
        }else{
        	JY.raise(tradeExtend.getRefNo()+"原交易信息不存在");
        }
		for (TmApproveInterest approveInterest : tmCashflowInterests) 
		{
			approveInterest.setSeqNumber(count);
			approveInterest.setDealNo(tradeExtend.getDealNo());
			approveInterest.setVersion(tradeExtend.getVersion());
			approveInterest.setRefNo(tradeExtend.getRefNo());
			approveInterest.setCfType("Interest");
			approveInterest.setPayDirection("Recevie");
			approveInterest.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
			if(approveInterest.getTheoryPaymentDate() != null && approveInterest.getTheoryPaymentDate().length() > 10)
			{
				approveInterest.setTheoryPaymentDate(approveInterest.getTheoryPaymentDate().substring(0, 10));
			}
			if(approveInterest.getRefBeginDate() != null && approveInterest.getRefBeginDate().length() > 10)
			{
				approveInterest.setRefBeginDate(approveInterest.getRefBeginDate().substring(0, 10));
			}
			if(approveInterest.getRefEndDate() != null && approveInterest.getRefEndDate().length() > 10)
			{
				approveInterest.setRefEndDate(approveInterest.getRefEndDate().substring(0, 10));
			}
			approveInterestMapper.insert(approveInterest);
			count++;
		}
	}
	
	@Override
	public void saveExtendCashFlowForApprove(Map<String, Object> params) throws Exception{
		if(params.get("approveCashflowInterest") != null){ //利息现金流
			saveExtendIntCashFlow(params);
			
		}else if(params.get("approveCashflowCapitals") != null){ //本金现金流
			saveExtendCapitalCashFlow(params);
			
		}
	}
}
