package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TT_TRD_PORT")
public class TtTrdPort implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//投资组合
	@Id
	private String portfolio;
	//投资组合备注信息
	private String portdesc;
	//成本中心号
	private String cost;
	//修改时间
	private String lstmntdte;
	//预留字段
	private String text1;
	//预留字段
	private String text2;
	//预留字段
	private String date1;
	//预留字段
	private String date2;
	//预留字段
	private String amount1;
	//预留字段
	private String amount2;
	public String getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}
	public String getPortdesc() {
		return portdesc;
	}
	public void setPortdesc(String portdesc) {
		this.portdesc = portdesc;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getAmount1() {
		return amount1;
	}
	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}
	public String getAmount2() {
		return amount2;
	}
	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
