package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TcGuidePrice;

public interface TcGuidePriceMapper extends Mapper<TcGuidePrice>{

	public List<String> getDistinctPrdNo();
	public List<TcGuidePrice> getGuidePricesByPrdNo(Map<String, Object> paramsMap);
}
