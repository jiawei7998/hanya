package com.singlee.capital.trade.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import org.apache.poi.ss.usermodel.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.counterparty.mapper.CounterPartyDao;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
import com.singlee.capital.trade.mapper.TdBaseAssetMapper;
import com.singlee.capital.trade.mapper.TdBaseAssetStructMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdBaseAssetStruct;
import com.singlee.capital.trade.model.TdBaseAssetStructVo;
import com.singlee.capital.trade.model.TdBaseAssetVo;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.BaseAssetService;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseAssetServiceImpl implements BaseAssetService {
	
	@Autowired
	public TdBaseAssetMapper tdBaseAssetMapper;
	
	@Autowired
	private TdBaseAssetStructMapper assetStructMapper;
	
	@Autowired
	private TdProductApproveMainMapper tdProductApproveMainMapper;
	
	@Resource
	private DictionaryGetService dictionaryGetService;
	
	@Autowired
	private CounterPartyDao counterPartyDao;
	
	public static Logger log = LoggerFactory.getLogger("FIBS");
	
	@Override
	public List<TdBaseAsset> getBaseAssetList(Map<String, Object> params) {
		return tdBaseAssetMapper.getBaseAssetList(params);
	}

	@Override
	public Page<TdBaseAsset> getBaseAssetLists(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdBaseAsset> param = tdBaseAssetMapper.getBaseAssetLists(params, rowBounds);
		return param;
	}

	@Override
	public List<TdBaseAssetStruct> getBaseAssetStructList(Map<String, Object> params) {
		return assetStructMapper.getBaseAssetStructList(params);
	}

	@Override
	public List<TdBaseAssetVo> searchBaseAssetListVo(Map<String, Object> params) {
		return tdBaseAssetMapper.searchBaseAssetListVo(params);
	}

	@Override
	public List<TdBaseAssetStructVo> getBaseAssetStructListVo(Map<String, Object> params) {
		return assetStructMapper.getBaseAssetStructListVo(params);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	
	@Override
	public TdBaseAsset queryTdBaseAssetById(String dealNo) {
		TdBaseAsset baseAsset = new TdBaseAsset();
		baseAsset = tdBaseAssetMapper.queryTdBaseAssetById(dealNo);
		return baseAsset;
	}

	@Override
	public boolean setTdBaseAssetPriceByExcel(String file) {
		try {
			ExcelUtil excelUtil = new ExcelUtil(file);
			int rowNum = excelUtil.getWb().getSheetAt(0).getLastRowNum();
			Sheet sheet = excelUtil.getSheetAt(0);
			Row row;
			List<TdBaseAsset> list  = new ArrayList<TdBaseAsset>();
			for (int i = 1; i <= rowNum; i++) {
				TdBaseAsset tdBaseAsset = new TdBaseAsset();
				row = sheet.getRow(i);
				tdBaseAsset.setDealNo(row.getCell(0).getStringCellValue());
//				tdBaseAsset.setSeq(String.valueOf(row.getCell(1).getNumericCellValue()));
//				tdBaseAsset.setCapitalPurpose(row.getCell(2).getStringCellValue());
//				tdBaseAsset.setIsDirectToEstate(row.getCell(3).getStringCellValue());
//				tdBaseAsset.setIsDirectToPlat(row.getCell(4).getStringCellValue());
//				tdBaseAsset.setIsSelfManagement(row.getCell(5).getStringCellValue());
//				tdBaseAsset.setAssetType(row.getCell(6).getStringCellValue());
//				tdBaseAsset.setBaseAssetAmt(row.getCell(7).getNumericCellValue());
				list.add(tdBaseAsset);
				if (i%1000 == 0) {
					tdBaseAssetMapper.insertTdBaseAssetExcel(list);
					list = new ArrayList<TdBaseAsset>();
				}
			}
			tdBaseAssetMapper.insertTdBaseAssetExcel(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 手工导入
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String setBaseAssetByExcel(String type, InputStream is) {
		StringBuffer sb = new StringBuffer();
		Sheet sheet = null;
		TdBaseAsset tdBaseAsset =null;
		List<TdBaseAsset> exitlist =null;
		TdProductApproveMain tdProductApproveMain =null;
		List<TdBaseAsset> list  = null;
		String dt = null;
		HashMap<String, List<TdBaseAsset>> maps = new HashMap<String, List<TdBaseAsset>>();
		try {
			ExcelUtil excelUtil = new ExcelUtil(type,is);
			sheet = excelUtil.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			System.out.println("rowNum"+rowNum);
			Row row;
			String deal_no = null;
			TaDictVo dict = null;
			String key = null;
			Map<String, Object> map = new HashMap<String, Object>();
			List<CounterPartyKindVo> kinds = counterPartyDao.getAllPartyKind(map);
			HashMap<String, String> firstKindMap = new HashMap<String, String>();
			HashMap<String, String> secondKindMap = new HashMap<String, String>();
			HashMap<String, String> thirdKindMap = new HashMap<String, String>(); 
			for (CounterPartyKindVo counterPartyKindVo : kinds) 
			{
				if("0".equals(counterPartyKindVo.getKind_grade())){
					firstKindMap.put(counterPartyKindVo.getKind_name(), counterPartyKindVo.getKind_code());
					
				}else if("1".equals(counterPartyKindVo.getKind_grade())){
					secondKindMap.put(counterPartyKindVo.getKind_name(), counterPartyKindVo.getKind_code());
					
				}else if("2".equals(counterPartyKindVo.getKind_grade())){
					thirdKindMap.put(counterPartyKindVo.getKind_name(), counterPartyKindVo.getKind_code());
					
				}
			}
			
			HashMap<String, Object> param = new HashMap<String, Object>();
			if(rowNum >0) {
			for (int i = 1; i <= rowNum; i++) 
			{
				tdBaseAsset = new TdBaseAsset();
				tdProductApproveMain = new TdProductApproveMain();
				row = sheet.getRow(i);
				if(row == null) {
                    continue;
                }
				
				try {
					deal_no = StringUtils.trimToEmpty(row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
				} catch (Exception e) {
					throw new RuntimeException("交易单号不存在");
				}
				
				
//				先判断交易是否已存在
				if(deal_no != null && !"".equals(deal_no))
				{
					tdProductApproveMain = tdProductApproveMainMapper.getProductApproveMainActivated(row.getCell(0).getStringCellValue());
					if(tdProductApproveMain != null &&!"".equals(tdProductApproveMain)){
						double def =0;
						tdBaseAsset.setSeq(i);
						tdBaseAsset.setDealNo(getCellData(row.getCell(0), 32,Cell.CELL_TYPE_STRING));//交易单编号
						tdBaseAsset.setCnm(getCellData(row.getCell(1), 30,Cell.CELL_TYPE_STRING));//基础资产客户
						tdBaseAsset.setCno(getCellData(row.getCell(2), 30,Cell.CELL_TYPE_STRING));//基础资产客户号
						tdBaseAsset.setBaseAssetType(getCellData(row.getCell(3), 15,Cell.CELL_TYPE_STRING));//基础资产类别
						tdBaseAsset.setInBalance(getCellData(row.getCell(4), 15,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(4), 15,Cell.CELL_TYPE_STRING)):def);//对应投资金额
						tdBaseAsset.setIsCust(getCellData(row.getCell(5), 30,Cell.CELL_TYPE_STRING));//是否本行客户
						tdBaseAsset.setMoneyMarketType(getCellData(row.getCell(6), 30,Cell.CELL_TYPE_STRING));//货币市场类型
						tdBaseAsset.setBigCat(getCellData(row.getCell(7), 30,Cell.CELL_TYPE_STRING));//行业大类
						tdBaseAsset.setMiddleCat(getCellData(row.getCell(8), 30,Cell.CELL_TYPE_STRING));//行业中类
						tdBaseAsset.setSmallCat(getCellData(row.getCell(9),30,Cell.CELL_TYPE_STRING));//行业小类
						tdBaseAsset.setInnerLevel(getCellData(row.getCell(10),30,Cell.CELL_TYPE_STRING));//行内评级
						tdBaseAsset.setOuterLevel(getCellData(row.getCell(11),30,Cell.CELL_TYPE_STRING));//公开评级
						tdBaseAsset.setPledgeType(getCellData(row.getCell(12),30,Cell.CELL_TYPE_STRING));//质押物类型
						tdBaseAsset.setPledgeAmt(getCellData(row.getCell(13),30,Cell.CELL_TYPE_NUMERIC) !=null ? Double.parseDouble(getCellData(row.getCell(13),30,Cell.CELL_TYPE_NUMERIC)) :def);//质押金额
						tdBaseAsset.setPledgeVdate(getCellData(row.getCell(14),30,CELL_TYPE_DATE));//质押起始日
						tdBaseAsset.setPledgeMdate(getCellData(row.getCell(15),30,CELL_TYPE_DATE));//质押到期日
						tdBaseAsset.setPledgeRate(getCellData(row.getCell(16),30,Cell.CELL_TYPE_NUMERIC)!=null ? Double.parseDouble(getCellData(row.getCell(16),30,Cell.CELL_TYPE_NUMERIC)) :def);//质押利率
						
						tdBaseAsset.setPledgeSpread(getCellData(row.getCell(17),30,Cell.CELL_TYPE_NUMERIC)!=null ? Double.parseDouble(getCellData(row.getCell(17),30,Cell.CELL_TYPE_NUMERIC)) :def);//质押利差
						tdBaseAsset.setMortgageType(getCellData(row.getCell(18),30,Cell.CELL_TYPE_STRING));//基础资产增信方式
						tdBaseAsset.setMortgageRemark(getCellData(row.getCell(19),30,Cell.CELL_TYPE_STRING));//基础资产描述
						tdBaseAsset.setShareType(getCellData(row.getCell(20),30,Cell.CELL_TYPE_STRING));//股东性质
						tdBaseAsset.setGuarantor(getCellData(row.getCell(21),30,Cell.CELL_TYPE_STRING));//保证人名称
						tdBaseAsset.setGuarantorIndus(getCellData(row.getCell(22),30,Cell.CELL_TYPE_STRING));//保证人所属行业
						tdBaseAsset.setSecId(getCellData(row.getCell(23),30,Cell.CELL_TYPE_STRING));//债券编码
						tdBaseAsset.setOptExerDate(getCellData(row.getCell(24),30,CELL_TYPE_DATE));//行权日期
						
						tdBaseAsset.setStockId(getCellData(row.getCell(25),30,Cell.CELL_TYPE_STRING));//股票代码
						tdBaseAsset.setWarnLinePrice(getCellData(row.getCell(26),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(26),30,Cell.CELL_TYPE_STRING)) :def);//预警线
						tdBaseAsset.setCoverLinePrice(getCellData(row.getCell(27),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(27),30,Cell.CELL_TYPE_STRING)) :def);//补仓线
						tdBaseAsset.setOpenLinePrice(getCellData(row.getCell(28),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(28),30,Cell.CELL_TYPE_STRING)) :def);//平仓线
						tdBaseAsset.setAssetType(getCellData(row.getCell(29),30,Cell.CELL_TYPE_STRING));//风险基础资产类型
						tdBaseAsset.setCoefficient(getCellData(row.getCell(30),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(30),30,Cell.CELL_TYPE_STRING)):def );//风险基础资产系数
						tdBaseAsset.setInBalance(getCellData(row.getCell(31),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(31),30,Cell.CELL_TYPE_STRING)):def );//对应投资金额
						tdBaseAsset.setAssetBalanceAuto(getCellData(row.getCell(32),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(32),30,Cell.CELL_TYPE_STRING)):def );//风险资产占用
						tdBaseAsset.setRsrAmt(getCellData(row.getCell(33),30,Cell.CELL_TYPE_STRING) !=null ? Double.parseDouble(getCellData(row.getCell(33),30,Cell.CELL_TYPE_STRING)):def);//风险缓释额
						tdBaseAsset.setRemark(getCellData(row.getCell(34),30,Cell.CELL_TYPE_STRING));//备注
					
						//数据字典 将中文转换为数字
						try {
							//基础资产类别
							if(tdBaseAsset.getBaseAssetType() !=null && !"".equals(tdBaseAsset.getBaseAssetType())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("baseAssetType", tdBaseAsset.getBaseAssetType());
								if(dict != null){
									tdBaseAsset.setBaseAssetType(dict.getDict_key());
								}
							}
							
							//是否行内客户
							if(tdBaseAsset.getIsCust() !=null && !"".equals(tdBaseAsset.getIsCust())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("YesNo", tdBaseAsset.getIsCust());
								if(dict != null){
									tdBaseAsset.setIsCust(dict.getDict_key());
								}	
							}
							
							//基础资产增信方式
							if(tdBaseAsset.getMortgageType() !=null && !"".equals(tdBaseAsset.getMortgageType())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("icWay", tdBaseAsset.getMortgageType());
								if(dict != null){
									tdBaseAsset.setMortgageType(dict.getDict_key());
								}
							}
							
							//股东性质
							if(tdBaseAsset.getShareType() !=null && !"".equals(tdBaseAsset.getShareType())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("shoreholderType", tdBaseAsset.getShareType());
								if(dict != null){
									tdBaseAsset.setShareType(dict.getDict_key());
								}	
							}
							
							//保证人所属
							if(tdBaseAsset.getGuarantorIndus() !=null && !"".equals(tdBaseAsset.getGuarantorIndus())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("GBT4754_2011", tdBaseAsset.getGuarantorIndus());
								if(dict != null){
									tdBaseAsset.setGuarantorIndus(dict.getDict_key());
								}
							}
							
							//货币市场类型
							if(tdBaseAsset.getMoneyMarketType() !=null && !"".equals(tdBaseAsset.getMoneyMarketType())) {
								dict = dictionaryGetService.getTaDictByCodeAndValue("moneyMarketType", tdBaseAsset.getMoneyMarketType());
								if(dict != null){
									tdBaseAsset.setMoneyMarketType(dict.getDict_key());
								}
							}
							
							//行业大类
							if(tdBaseAsset.getBigCat()!=null && !"".equals(tdBaseAsset.getBigCat())) {
								key = firstKindMap.get(tdBaseAsset.getBigCat().replace("_", "、"));
								if(key != null){
									tdBaseAsset.setBigCat(key);
								}
							}
							
							
							//行业中类
							if(tdBaseAsset.getMiddleCat() !=null && !"".equals(tdBaseAsset.getMiddleCat())) {
								key = secondKindMap.get(tdBaseAsset.getMiddleCat().replace("_", "、"));
								if(key != null){
									tdBaseAsset.setMiddleCat(key);
								}	
							}
							
							
							//行业小类
							if(tdBaseAsset.getSmallCat() !=null && !"".equals(tdBaseAsset.getSmallCat())) {
								key = thirdKindMap.get(tdBaseAsset.getSmallCat().replace("_", "、"));
								if(key != null){
									tdBaseAsset.setSmallCat(key);
								}
							}
							
							//质押物类型
							if(tdBaseAsset.getPledgeType()!=null && !"".equals(tdBaseAsset.getPledgeType())) {
								dict=dictionaryGetService.getTaDictByCodeAndValue("pledgeType",tdBaseAsset.getPledgeType());
								if(dict!=null){
									tdBaseAsset.setPledgeType(dict.getDict_key());
								}	
							}
							
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						list = maps.get(deal_no);
						if(list == null)
						{
							list = new ArrayList<TdBaseAsset>();
							maps.put(deal_no, list);
						}
						list.add(tdBaseAsset);
						
					}else {
						throw new RuntimeException("交易单号不存在");
					}
					
				}
			}
			}else {
				throw new RuntimeException("模板不能为空");
			}
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");  
			for (Entry<String, List<TdBaseAsset>> e : maps.entrySet()) 
			{
				deal_no = e.getKey();
				//查看此交易在基础资产表是否存在
				param.put("dealNo", deal_no);
				exitlist = tdBaseAssetMapper.getBaseAssetList(param);
				if(exitlist != null && exitlist.size() > 0)
				{
					dt = sdf.format(new Date());
					for (TdBaseAsset tdAsset : exitlist) 
					{
						tdAsset.setVersion(dt);
						tdBaseAssetMapper.insertTdBaseAssetHis(tdAsset);
					}
					tdBaseAssetMapper.deleteBaseAssetByDealNo(deal_no);
				}
				
				//插入已更新的基础资产交易
				tdBaseAssetMapper.insertTdBaseAssetExcel(e.getValue());
			}// end for
			
		} catch (Exception e) {
			if(sheet!=null){
				e.printStackTrace();
				log.error("BaseAssetServiceImpl--setStockPriceByExcel"+e);
				sb.append("基础资产信息上传失败："+e.getMessage());
			}else{
				log.error("BaseAssetServiceImpl--setStockPriceByExcel"+e);
				sb.append("无法识别Excel后缀名");
			}
		}
		return sb.toString();
	}

	@Override
	public Page<TdBaseAsset> getBaseAssetHisLists(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdBaseAsset> param = tdBaseAssetMapper.getBaseAssetHisLists(params, rowBounds);
		return param;
	}
	
	public  static final int CELL_TYPE_DATE = 999;
	
	@SuppressWarnings({ "deprecation" })
	private String getCellData(Cell cell,int lenth,int type)throws Exception{
		if(cell == null) {
            return null;
        }
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String value = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			value = null;
			break;
			
		case Cell.CELL_TYPE_STRING:
			if(Cell.CELL_TYPE_STRING != type){
				throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是字符类型");
			}
			value = cell.getRichStringCellValue().getString();
			break;
			
		case Cell.CELL_TYPE_NUMERIC:
			if(CELL_TYPE_DATE == type){
				if(DateUtil.isCellDateFormatted(cell)){
					value = sdf.format(cell.getDateCellValue());
				}else{
					throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是日期类型");
					
				}
			}else if(Cell.CELL_TYPE_NUMERIC == type || Cell.CELL_TYPE_STRING == type){
				if(Cell.CELL_TYPE_NUMERIC == type) {
					value = String.valueOf(cell.getNumericCellValue());
				}
				if(Cell.CELL_TYPE_STRING == type) {
					value = String.valueOf(new BigDecimal(cell.getNumericCellValue()));
				}
				
			}else{
				throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容不是数字类型");
				
			}
			
			break;
			
		case Cell.CELL_TYPE_BOOLEAN:
			value = String.valueOf(cell.getBooleanCellValue());
			break;
			
		case Cell.CELL_TYPE_FORMULA:
			try {
				value = String.valueOf(cell.getNumericCellValue());
			} catch (Exception e) {
				try {
					value = cell.getRichStringCellValue().toString();
				} catch (Exception e2) {
					value = "";
				}
			}
			break;

		default:
			break;
		}
		if(value != null && lenth > 0 && value.length() > lenth){
			throw new RuntimeException("Excel第"+cell.getRowIndex()+"行第" + ExcelUtil.indexToColumn(cell.getColumnIndex()+1)+"列单元格内容超过最大长度："+lenth);
		}
		return value;
	}
}
