package com.singlee.capital.trade.acc.model;

import java.io.Serializable;

public class TdAccTrdDailyAmorVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String acctype;//每日类型
	private String dealNo;//交易流水
	private int version;//版本
	private String prdNo;//产品代码
	private String invtype;//投资类型
	private double settlAmt;//合同本金
	private String basis;//计息基础
	private double contractRate;//合同利率
	private String curDate;//当前账务日期
	private double procAmt;//结算金额
	private double disAmt;//折价金额
	private double actAmorAmt;//实际摊销金额
	private String cashflowId;//关联摊销流水
	public String getAcctype() {
		return acctype;
	}
	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public double getSettlAmt() {
		return settlAmt;
	}
	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getCurDate() {
		return curDate;
	}
	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}
	public double getContractRate() {
		return contractRate;
	}
	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}
	public double getProcAmt() {
		return procAmt;
	}
	public void setProcAmt(double procAmt) {
		this.procAmt = procAmt;
	}
	public double getDisAmt() {
		return disAmt;
	}
	public void setDisAmt(double disAmt) {
		this.disAmt = disAmt;
	}
	public double getActAmorAmt() {
		return actAmorAmt;
	}
	public void setActAmorAmt(double actAmorAmt) {
		this.actAmorAmt = actAmorAmt;
	}
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	@Override
	public String toString() {
		return "TdAccTrdDailyAmorVo [acctype=" + acctype + ", dealNo=" + dealNo
				+ ", version=" + version + ", prdNo=" + prdNo + ", invtype="
				+ invtype + ", settlAmt=" + settlAmt + ", basis=" + basis
				+ ", contractRate=" + contractRate + ", curDate=" + curDate
				+ ", procAmt=" + procAmt + ", disAmt=" + disAmt
				+ ", actAmorAmt=" + actAmorAmt + ", cashflowId=" + cashflowId
				+ "]";
	}
	
}
