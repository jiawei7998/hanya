package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;

/**
 * @projectName 同业业务管理系统
 * @className 活期提前支取持久层
 * @description TODO
 * @author shiting
 * @createDate 2017-09-01
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdAdvanceMaturityCurrentMapper extends Mapper<TdAdvanceMaturityCurrent> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdAdvanceMaturityCurrent getAdvanceMaturityCurrentById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturityCurrent> getAdvanceMaturityCurrentList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturityCurrent> getAdvanceMaturityCurrentListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturityCurrent> getAdvanceMaturityCurrentListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param advanceMaturityCurrent
	 * @return
	 */
	public TdAdvanceMaturityCurrent searchAdvanceMaturityCurrent(TdAdvanceMaturityCurrent advanceMaturityCurrent);
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	public TdAdvanceMaturityCurrent ifExistDuration(Map<String,Object> map);
	/**
	 * 获取提前到期数据 供账务使用
	 * @param map
	 * @return
	 */
	public List<TdAdvanceMaturityCurrent> getAdvanceMaturitiesForAcc(Map<String,Object> map);

	public Page<TdAdvanceMaturityCurrent> getTdAdvanceMaturityCurrent(
			Map<String, Object> params, RowBounds rowBounds);
	
	
	/**
	 * 根据 RefNo version 查询所有历史生效的 提前到期
	 * @param params
	 * @return
	 */
	public Page<TdAdvanceMaturityCurrent>  getAllAdvanceMaturityCurrentsByDealnoVersion(Map<String, Object> params,RowBounds rb);
	
	public void updateTdAdvanceMaturityActAmt(Map<String,Object> map);
	
	public void updateTdAdvanceMaturityActInt(Map<String,Object> map);
}