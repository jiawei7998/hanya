package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TdRiskSlowReleaseMapper;
import com.singlee.capital.trade.model.TdRiskSlowRelease;
import com.singlee.capital.trade.service.RiskSlowReleaseService;

/**
 * @projectName 同业业务管理系统
 * @className 风险缓释服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RiskSlowReleaseServiceImpl implements RiskSlowReleaseService {
	
	@Autowired
	public TdRiskSlowReleaseMapper tdRiskSlowReleaseMapper;
	
	@Override
	public List<TdRiskSlowRelease> getRiskSlowReleaseList(Map<String, Object> params) {
		return tdRiskSlowReleaseMapper.getRiskSlowReleaseList(params);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
