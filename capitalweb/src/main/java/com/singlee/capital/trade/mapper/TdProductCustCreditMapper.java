package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.trade.model.TdProductCustCredit;

public interface TdProductCustCreditMapper extends Mapper<TdProductCustCredit>{
	
	public Page<TdProductCustCredit> getTdProductCustCreditList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 */
	public List<TdProductCustCredit> getTdProductCustCreditList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 */
	public void deleteTdProductCustCreditByDealNo(String dealNo);
	/**
	 * 审批转交易拷贝
	 * @param params
	 */
	public void insertTdProductCustCreditForCopy(Map<String, Object> params);
	
	/**
	 * 根据交易额度占用表获取额度记录信息，并形成带入参数
	 * @param params
	 * @return
	 */
	public List<EdInParams> getEdInParamsForProductCustCredit(Map<String, Object> params);
	
	/**
	 * 批量JOB额度释放重新占用
	 * @param params
	 * @return
	 */
	public List<EdInParams> getEdInParamsForProductCustCreditByBatchJob(Map<String, Object> params);
	
	/**
	 * 获取所有需要占用额度的数据
	 * @return
	 */
	public List<EdInParams> getEdInParamsForProductCustCreditForTransfor();
	
	public void deleteAllTdProductCustCredits();
	
	void deleteTdProductCustCreditByDealNoAndCustNo(Map<String, Object> params);
}
