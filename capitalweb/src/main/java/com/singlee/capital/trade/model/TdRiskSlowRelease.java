package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 风险资产
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_RISK_SLOW_RELEASE")
public class TdRiskSlowRelease implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -629234994788831077L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 风险缓释类型
	 */
	private String rsrType;
	/**
	 * 风险缓释金额
	 */
	private Double rsrAmt;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getRsrType() {
		return rsrType;
	}
	public void setRsrType(String rsrType) {
		this.rsrType = rsrType;
	}
	public Double getRsrAmt() {
		return rsrAmt;
	}
	public void setRsrAmt(Double rsrAmt) {
		this.rsrAmt = rsrAmt;
	}
}
