package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdRateReset;

import tk.mybatis.mapper.common.Mapper;

/**
 * @projectName 同业业务管理系统
 * @className 提前到期持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-11 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdRateResetMapper extends Mapper<TdRateReset> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 利率变更
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdRateReset getRateChangeById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 利率变更对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdRateReset> getRateChangeList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 利率变更对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdRateReset> getRateChangeListFinish(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param change
	 * @return
	 */
	public TdRateReset searchRateChange(TdRateReset change);
	
	/**
	 * 待审批
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdRateReset> getRateChangeApproveList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 我发起列表
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdRateReset> getRateChangeMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据 RefNo version 查询所有历史生效的 利率变更
	 * @param params
	 * @return
	 */
	public List<TdRateReset>  getAllRateChangesByDealnoVersion(Map<String, Object> params);
	
}