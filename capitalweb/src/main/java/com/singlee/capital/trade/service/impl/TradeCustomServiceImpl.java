package com.singlee.capital.trade.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.mapper.TbkEntryMapper;
import com.singlee.cap.bookkeeping.model.TbkEntry;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.cap.bookkeeping.service.impl.BookkeepingServiceImpl;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.service.RateRangeService;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.interfacex.qdb.service.CnapsService;
import com.singlee.capital.interfacex.qdb.service.CreditService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.SysParamService;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyAmorVoMapper;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyAmorVo;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.model.TdAdvanceMaturityCurrent;
import com.singlee.capital.trade.model.TdApproveCancel;
import com.singlee.capital.trade.model.TdFeeHandleCashflow;
import com.singlee.capital.trade.model.TdFeesPassAgewayChange;
import com.singlee.capital.trade.model.TdInterestSettle;
import com.singlee.capital.trade.model.TdInvFiipApprove;
import com.singlee.capital.trade.model.TdMultiResale;
import com.singlee.capital.trade.model.TdOutrightSale;
import com.singlee.capital.trade.model.TdOverDueConfirm;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.model.TdRateChange;
import com.singlee.capital.trade.model.TdRateReset;
import com.singlee.capital.trade.model.TdTradeExpire;
import com.singlee.capital.trade.model.TdTradeExtend;
import com.singlee.capital.trade.model.TdTradeOffset;
import com.singlee.capital.trade.model.TdTransforSchedule;
import com.singlee.capital.trade.model.TdTransforScheduleFee;
import com.singlee.capital.trade.model.TdTransforScheduleInt;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.AdvanceMaturityService;
import com.singlee.capital.trade.service.ApproveCancelService;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.MulResaleService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.RateChangeService;
import com.singlee.capital.trade.service.RateResetService;
import com.singlee.capital.trade.service.TdBalanceService;
import com.singlee.capital.trade.service.TdFeeHandleCashflowService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TdInvFiipApproveService;
import com.singlee.capital.trade.service.TdOverDueConfirmService;
import com.singlee.capital.trade.service.TradeExpireService;
import com.singlee.capital.trade.service.TradeExtendService;
import com.singlee.capital.trade.service.TradeManageService;
import com.singlee.capital.trade.service.TradeOffsetService;
import com.singlee.capital.trade.service.TradeOutrightSaleService;
import com.singlee.capital.trade.service.TradeServiceListener;
import com.singlee.capital.trade.service.TransforScheduleFeeService;
import com.singlee.capital.trade.service.TransforScheduleIntService;
import com.singlee.capital.trade.service.TransforScheduleService;

@Service("tradeCustomServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeCustomServiceImpl extends AbstractDurationService implements TradeServiceListener {
	@Autowired
	TdCashflowInterestMapper cashflowInterestMapper;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private AdvanceMaturityService advanceMaturityService;
	@Autowired
	private TrdTradeMapper trdTradeMapper;
	@Autowired
	private TdProductApproveMainMapper mainMapper;
	@Autowired
	private RateChangeService rateChangeService;
	@Autowired
	private TradeExtendService tradeExtendService;
	@Autowired
	private TradeOutrightSaleService tradeOutrightSaleService;
	@Autowired
	private RateRangeService rateRangeService;
	@Autowired
	private MulResaleService mulResaleService;
	@Autowired
	private ApproveCancelService approveCancelService;
	@Autowired
	private TradeOffsetService tradeOffsetService;
	/** 审批单dao **/
	@Autowired
	private TrdOrderMapper trdOrderMapper;
	@Autowired
	private SysParamService sysParamService;
	@Autowired
	private TdInvFiipApproveService invFiipApproveService;
	@Autowired
	private TradeManageService tradeManageService;
	@Autowired
	private TrdTposService tposService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	/** 业务日 */
	@Autowired
	private DayendDateService dayendDateService;
	@Autowired
	private TdOverDueConfirmService tdOverDueConfirmService;
	@Autowired
	private TransforScheduleService transforScheduleService;//本金计划调整
	@Autowired
	private TransforScheduleIntService transforScheduleIntService;//利息计划调整
	@Autowired
	private TransforScheduleFeeService transforScheduleFeeService;//费用计划调整
	@Autowired
	private BookkeepingService bookkeepingService;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;
	@Autowired
	private TrdTposMapper tposMapper;
	@Autowired
	private TdAccTrdDailyAmorVoMapper accTrdDailyAmorVoMapper;
	@Autowired
	private InterestSettleService interestSettleService;
	@Autowired
	private AdvanceMaturityCurrentService advanceMaturityCurrentService;
	@Autowired
	private TdFeeHandleCashflowService feeHandleCashflowService;
	@Autowired
	TdProductFeeDealMapper tdProductFeeDealMapper;
	@Autowired
	private TradeExpireService tradeExpireService;
	@Autowired
	private TdFeesPassAgewayService feesPassAgewayService;
	@Autowired
	private RateResetService rateResetService;
	@Autowired
	private TdBalanceService balanceService;
	@Autowired
	private CnapsService cnapsService;
	@Autowired
	private CreditService creditService;
	@Autowired
	private BookkeepingServiceImpl bookkeepingServiceImpl;
	
	@Override
	public Object searchTradeList(HashMap<String, Object> map, int pageNum, int pageSize) {
		map.put("pageNum", pageNum);
		map.put("pageSize", pageSize);
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<TtTrdTrade> resultset = trdTradeMapper.selectTtTrdTrade(map, rb);
		return resultset;
	}

	@Override
	public void selectTradeInfo(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelTrade(TtTrdTrade ttTrdTrade) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dealConfirmTrade(TtTrdTrade ttTrdTrade) {
		//存续期（提前终止）核实完成 productApproveMain金融工具表数据version+1
		//String businessDate = dayendDateService.getSettlementDate();  //当前账务日期
		
		String beanName = DurationConstants.getBeanFromTrdType(ttTrdTrade.getTrdtype());
		AbstractDurationService abstractDurationService = SpringContextHolder.getBean(beanName);
		if(DictConstants.TrdType.FeeCashFlowHandle_SINGLE.equals(ttTrdTrade.getTrdtype())){
			TdFeeHandleCashflow feeHandleCashflow = feeHandleCashflowService.getFeeHandleCashflowById(ttTrdTrade.getTrade_id());
			JY.require(feeHandleCashflow!=null, "交易编号%s对应的费用补录信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(feeHandleCashflow));
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(feeHandleCashflow));
			
		}else if(DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdTrade.getTrdtype())){
			TdAdvanceMaturity advanceMaturity = advanceMaturityService.getAdvanceMaturityById(ttTrdTrade.getTrade_id());
			JY.require(advanceMaturity!=null, "交易编号%s对应的提前支取信息不存在!");
			/**
			 * 将交易数据传输给提前还款的处理类处理后续任务分发
			 */
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(advanceMaturity));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(advanceMaturity));
		
		}else if(DictConstants.TrdType.CustomAdvanceMaturityCurrent.equals(ttTrdTrade.getTrdtype())){
			TdAdvanceMaturityCurrent advanceMaturityCurrent = advanceMaturityCurrentService.getAdvanceMaturityCurrentById(ttTrdTrade.getTrade_id());
			JY.require(advanceMaturityCurrent!=null, "交易编号%s对应的提前支取信息不存在!");
			/**
			 * 将交易数据传输给提前还款的处理类处理后续任务分发
			 */
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(advanceMaturityCurrent));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(advanceMaturityCurrent));
		
		}
		else if (DictConstants.TrdType.Custom.equals(ttTrdTrade.getTrdtype())){
			/**
			 * 完成交易-放款完成
			 */
			Map<String,Object> map =new HashMap<String,Object>();
			map.put("dealNo", ttTrdTrade.getI_code());
			TdProductApproveMain main = mainMapper.getProductApproveMain(map);
			/**
			 * 交易达成后产生利率区间/记录持仓数据
			 */
			rateRangeService.createRateRangeByCompleteTrade(BeanUtil.beanToMap(main));
			tposService.insertTposByTrade(BeanUtil.beanToMap(main));
			/**
			 * 完成后默认放款 
			 */
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(main));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(main));
			 
			 /****
			 * 额度操作
			 */
			 ttTrdTrade.setSettleamount(main.getAmt());
			 creditOperation(ttTrdTrade, main, DictConstants.ApproveStatus.ApprovedPass);
			
		} else if (DictConstants.TrdType.CustomRateChange.equals(ttTrdTrade.getTrdtype())){
			/**
			 * 检查利率变更交易是否存在，避免被人为删除；利率变更完成后，更新利率区间表以便每日计提正确性；
			 * 不更新原交易；
			 */
			TdRateChange rateChange = rateChangeService.getRateChangeById(ttTrdTrade.getTrade_id());
			JY.require(rateChange!=null, "交易编号%s对应的利率变更信息不存在!");
			//1、修改利率区间表
			rateRangeService.updateRateRangeByRateChange(BeanUtil.beanToMap(rateChange));
			 /**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(rateChange));
		}
		 else if (DictConstants.TrdType.CustomInterestSettleConfirm.equals(ttTrdTrade.getTrdtype())){
				/**
				 * 检查结息确认交易是否存在，避免被人为删除；结息确认完成后 ，出结息确认分录；
				 * 不更新原交易；
				 */
				TdInterestSettle settle = interestSettleService.getInterestSettleById(ttTrdTrade.getTrade_id());
				JY.require(settle!=null, "交易编号%s对应的结息确认信息不存在!");
				 /**
				 * 统一记录事件 TD_TRD_EVENT
				 */
				 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(settle));
				 abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(settle));
			}
		else if (DictConstants.TrdType.CustomTradeExtend.equals(ttTrdTrade.getTrdtype())){
			 TdTradeExtend tradeExtend = tradeExtendService.getTradeExtendById(ttTrdTrade.getTrade_id());
				JY.require(tradeExtend!=null, "交易编号%s对应的交易展期信息不存在!");
				//1.升版本，生成现金流
				//updateApproveMainVersion(tradeExtend.getFpml(), tradeExtend.getiCode(), tradeExtend.getVersion(),null,tradeExtend.getExpDate());
			 abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(tradeExtend));
			 /**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			 abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(tradeExtend));
			}
		else if (DictConstants.TrdType.CustomOutRightSale.equals(ttTrdTrade.getTrdtype())) {
			TdOutrightSale outrightSale = tradeOutrightSaleService.getTdOutrightSaleById(ttTrdTrade.getTrade_id());
			JY.require(outrightSale!=null, "交易编号%s对应的卖断交易信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(outrightSale));	
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(outrightSale));
		 }
		else if(DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdTrade.getTrdtype())){
			TdTransforSchedule cfChange = transforScheduleService.getTransforScheduleById(ttTrdTrade.getTrade_id());
			JY.require(cfChange!=null, "交易编号%s对应的现金流变更信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(cfChange));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(cfChange));
		}
		else if(DictConstants.TrdType.CustomCashFlowChangeInt.equals(ttTrdTrade.getTrdtype())){
			TdTransforScheduleInt cfChange = transforScheduleIntService.getTransforScheduleIntById(ttTrdTrade.getTrade_id());
			JY.require(cfChange!=null, "交易编号%s对应的现金流变更信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(cfChange));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(cfChange));
		}
		else if(DictConstants.TrdType.CustomCashFlowChangeFee.equals(ttTrdTrade.getTrdtype())){
			TdTransforScheduleFee transforScheduleFee = transforScheduleFeeService.getTransforScheduleFeeById(ttTrdTrade.getTrade_id());
			JY.require(transforScheduleFee!=null, "交易编号%s对应的现金流变更信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(transforScheduleFee));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(transforScheduleFee));
		}
		else if(DictConstants.TrdType.CustomInvFiIpTransFor.equals(ttTrdTrade.getTrdtype())){
			TdInvFiipApprove invFiipApprove = invFiipApproveService.getTdInvFiipApproveById(ttTrdTrade.getTrade_id());
			JY.require(invFiipApprove!=null, "交易编号%s对应的现金流变更信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(invFiipApprove));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(invFiipApprove));
		}
		else if(DictConstants.TrdType.ApproveCancel.equals(ttTrdTrade.getTrdtype())){
			TdApproveCancel approveCancel = approveCancelService.getApproveCancelById(ttTrdTrade.getTrade_id());
			JY.require(approveCancel!=null, "撤销单审批编号%s信息不存在!");
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(approveCancel));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(approveCancel));
		}
		
		else if (DictConstants.TrdType.CustomTradeOverDue.equals(ttTrdTrade.getTrdtype())){
			TdOverDueConfirm tdOverDueConfirm = tdOverDueConfirmService.queryOverDueDealByDealno(ttTrdTrade.getTrade_id());
			JY.require(tdOverDueConfirm != null, "交易编号%s对应的交易逾期信息不存在!");
			//1.升版本，生成现金流
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(tdOverDueConfirm));
			/**
			 * 统一记录事件 TD_TRD_EVENT
			 */
			abstractDurationService.saveTrdEvent(BeanUtil.beanToMap(tdOverDueConfirm));
		}
		
		else if (DictConstants.TrdType.CustomRateReset.equals(ttTrdTrade.getTrdtype())){
			/**
			 * 检查利率变更交易是否存在，避免被人为删除；利率变更完成后，更新利率区间表以便每日计提正确性；
			 * 不更新原交易；
			 */
			TdRateReset rateChange = rateResetService.getRateChangeById(ttTrdTrade.getTrade_id());
			JY.require(rateChange!=null, "交易编号%s对应的利率变更信息不存在!");
			//1、修改利率区间表
			rateRangeService.updateRateRangeByRateChange(BeanUtil.beanToMap(rateChange));
		}
		else if (DictConstants.TrdType.CustomTradeExpire.equals(ttTrdTrade.getTrdtype())){
			TdTradeExpire tradeExpire = tradeExpireService.getTradeExpireById(ttTrdTrade.getTrade_id());
			JY.require(tradeExpire!=null, "交易编号%s对应的交易展期信息不存在!");
			//1.升版本，生成现金流
			 abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(tradeExpire));
		}
		else if (DictConstants.TrdType.CustomPassageWay.equals(ttTrdTrade.getTrdtype())){
			TdFeesPassAgewayChange change = feesPassAgewayService.queryTdFeesPassAgewayById(ttTrdTrade.getTrade_id());
			JY.require(change != null, "交易编号%s对应的交易逾期信息不存在!");
			//1.升版本，生成现金流
			abstractDurationService.saveCompleteDuration(BeanUtil.beanToMap(change));
			
		}else if(DictConstants.TrdType.AccountTradeOffset.equals(ttTrdTrade.getTrdtype())){
			HashMap<String, Object> cmap = new HashMap<String, Object>();
			cmap.put("trade_id", ttTrdTrade.getTrade_id());
			cmap.put("version_number", ttTrdTrade.getVersion_number());
			TtTrdTrade trade = trdTradeMapper.selectTradeForTradeId(cmap);
			try {
				TdTradeOffset tradeOffset = tradeOffsetService.getTradeOffsetById(ttTrdTrade.getTrade_id());
				if(trade != null && tradeOffset != null)
				{
					tradeManageService.cancelTrade(tradeOffset.getRefNo(), trade.getVersion_number(), "", "注销", "admin");
					HashMap<String, Object> mapTemp = new HashMap<String, Object>();
					mapTemp.put("dealNo", tradeOffset.getRefNo());
					List<TbkEntry> list = tradeOffsetService.searchTbkEntryByDealNo(mapTemp);
					if(list !=null && list.size()>0) {
						bookkeepingServiceImpl.updateBookkeepingBalance(list);
					}
					tradeOffsetService.addEntryOffset(mapTemp);
					
				}
				try {
					TdProductApproveMain main = productApproveService.getProductApproveActivated(tradeOffset.getRefNo());
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("dealNo", main.getDealNo());
					map.put("version", main.getVersion());
					TdTrdTpos tpos = tposMapper.getTdTrdTposByKey(map);
			
					ttTrdTrade.setSettleamount(tpos == null ? main.getAmt() : tpos.getSettlAmt());
					ttTrdTrade.setRef_tradeid(main.getDealNo());
					
					creditOperation(ttTrdTrade, trade, DictConstants.ApproveStatus.ApprovedPass);
				} catch (Exception e) {
					JY.error("交易撤销释放额度出错",e);
				}
			} catch (Exception e) {
				JY.error("交易撤销出错",e);
			}
		}
		
	}

	@Override
	public void withdrawTrade(TrdTradeVo trdTradeVo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveTrade(HashMap<String, Object> map, TtTrdTrade ttTrdTrade, boolean isAdd) {
		map.put("dealNo", ttTrdTrade.getTrade_id());
		map.put("dealType", DictConstants.DealType.Verify);

		String businessDate = dayendDateService.getSettlementDate();  //当前账务日期
		if(!isAdd){
			TtTrdTrade tempTrdTrade = trdTradeMapper.selectTradeForTradeId(BeanUtil.beanToHashMap(ttTrdTrade));
			if(null != tempTrdTrade && !StringUtils.equalsIgnoreCase(tempTrdTrade.getTrade_status(), DictConstants.ApproveStatus.New)){
				JY.raise("当前业务非新建状态，不允许修改!");
			}
		}
		//给交易单赋值I_CODE
		if (DictConstants.TrdType.Custom.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				ttTrdTrade.setI_code(ttTrdTrade.getTrade_id());
				ttTrdTrade.setSelf_outsecuaccid("-1"); // 外部证券账户无所谓
				if(null!=ttTrdTrade.getOrder_id() && !"".equalsIgnoreCase(ttTrdTrade.getOrder_id())){
					TdProductApproveMain main = productApproveService.getProductApproveActivated(ttTrdTrade.getOrder_id());
					main.setDealNo(ttTrdTrade.getTrade_id());
					main.setRefNo(ttTrdTrade.getOrder_id());//必须将关联的审批单号进行赋值；以便后续的拷贝现金流交易关联数据使用
					//结算日期如果有划款日，则为划款日，如果没有，则为起息日,如果还没有，则审批开始日期
					if(!StringUtil.isNullOrEmpty(main.gettDate())) {
                        ttTrdTrade.setFst_settle_date(main.gettDate());
                    } else if(!StringUtil.isNullOrEmpty(main.getvDate())) {
                        ttTrdTrade.setFst_settle_date(main.getvDate());
                    } else {
                        ttTrdTrade.setFst_settle_date(main.getaDate());
                    }
					main.setDealType(DictConstants.DealType.Verify);
					HashMap<String, Object> mapMain = ParentChildUtil.ClassToHashMap(main);
					map.put("copyApprove", DictConstants.YesNo.YES);//驱动核实单据从审批单据获取
					map.putAll(mapMain);
				}
			}else{
				if(!StringUtil.isNullOrEmpty(ParameterUtil.getString(map, "tDate", ""))){
					ttTrdTrade.setFst_settle_date(ParameterUtil.getString(map, "tDate", ""));
				}else if(!StringUtil.isNullOrEmpty(ParameterUtil.getString(map, "vDate", ""))){
					ttTrdTrade.setFst_settle_date(ParameterUtil.getString(map, "vDate", ""));
				}
				if(!StringUtil.isNullOrEmpty(ParameterUtil.getString(map, "meDate", ""))){
					ttTrdTrade.setEnd_settle_date(ParameterUtil.getString(map, "meDate", ""));
				}else if(!StringUtil.isNullOrEmpty(ParameterUtil.getString(map, "mDate", ""))){
					ttTrdTrade.setEnd_settle_date(ParameterUtil.getString(map, "mDate", ""));
				}
			}
			if(isAdd){
				productApproveService.createProductApprove(map);
				//feesPlanService.createTradeFeePlan(map);
			}else{
				productApproveService.updateProductApprove(map);
			}
		} else if (DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				advanceMaturityService.createAdvanceMaturity(map);
			}else{
				advanceMaturityService.updateAdvanceMaturity(map);
			}
		}
		else if (DictConstants.TrdType.CustomAdvanceMaturityCurrent.equals(ttTrdTrade.getTrdtype())) { // 活期提前支取
			if(isAdd){
				advanceMaturityCurrentService.createAdvanceMaturityCurrent(map);
			}else{
				advanceMaturityCurrentService.updateAdvanceMaturityCurrent(map);
			}
		}
		else if (DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdTrade.getTrdtype())) {//本金现金流调整
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				transforScheduleService.createTransforSchedule(map);
			}else{
				transforScheduleService.updateTransforSchedule(map);
			}
		}
		else if (DictConstants.TrdType.CustomCashFlowChangeInt.equals(ttTrdTrade.getTrdtype())) {//利息现金流调整
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				transforScheduleIntService.createTransforScheduleInt(map);
			}else{
				transforScheduleIntService.updateTransforScheduleInt(map);
			}
		}
		else if (DictConstants.TrdType.CustomCashFlowChangeFee.equals(ttTrdTrade.getTrdtype())) {//费用现金流调整
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				transforScheduleFeeService.createTransforScheduleFee(map);
			}else{
				transforScheduleFeeService.updateTransforScheduleFee(map);
			}
		}
		else if (DictConstants.TrdType.CustomInvFiIpTransFor.equals(ttTrdTrade.getTrdtype())) {//利息现金流调整
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				invFiipApproveService.createTdInvFiipApprove(map);
			}else{
				invFiipApproveService.updateTdInvFiipApprove(map);
			}
		}
		else if (DictConstants.TrdType.CustomRateChange.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				rateChangeService.createRateChange(map);
			}else{
				ttTrdTrade.setTrade_date(ParameterUtil.getString(map, "effectDate", businessDate));
				rateChangeService.updateRateChange(map);
			}
		}
		else if (DictConstants.TrdType.CustomInterestSettleConfirm.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				interestSettleService.createInterestSettle(map);
			}else{
				interestSettleService.updateInterestSettle(map);
			}
		}
		else if (DictConstants.TrdType.CustomTradeExtend.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				tradeExtendService.createTradeExtend(map);
			}else{
				tradeExtendService.updateTradeExtend(map);
			}
		}
		/**
		 * 单笔卖断
		 */
		else if (DictConstants.TrdType.CustomOutRightSale.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				tradeOutrightSaleService.createTdOutrightSale(map);
			}else{
				tradeOutrightSaleService.updateTdOutrightSale(map);
			}
		}
		else if (DictConstants.TrdType.CustomMultiResale.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				TdMultiResale multiResale = mulResaleService.getMulResaleById(ttTrdTrade.getOrder_id());
				map.put("resAmt", multiResale.getResAmt());
				map.put("resDate", multiResale.getResDate());
				map.put("resInterest", multiResale.getResInterest());
				map.put("cNo", multiResale.getcNo());
				map.put("resReason", multiResale.getResReason());
				mulResaleService.createMulResale(map);
			}else{
				ttTrdTrade.setTrade_date(ParameterUtil.getString(map, "resDate", businessDate));
				mulResaleService.updateMulResale(map);
			}
		}

		else if (DictConstants.TrdType.ApproveCancel.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				approveCancelService.createApproveCancel(map);
			}else{
				approveCancelService.updateApproveCancel(map);
			}
		}

		else if (DictConstants.TrdType.AccountTradeOffset.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				ttTrdTrade.setSelf_outsecuaccid("-1");
				/*TdTradeOffset tradeOffset = tradeOffsetService.getTradeOffsetById(ttTrdTrade.getOrder_id());
				map.put("accountingDate", tradeOffset.getAccountingDate());
				map.put("offsetReason", tradeOffset.getOffsetReason());*/
				tradeOffsetService.createTradeOffset(map);
			}else{
				tradeOffsetService.updateTradeOffset(map);
			}
		}
		
		else if (DictConstants.TrdType.CustomTradeOverDue.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				tdOverDueConfirmService.insertOverDueDeal(map);
			}else{
				tdOverDueConfirmService.updateOverDueDeal(map);
			}
		}
		else if (DictConstants.TrdType.AbssetForEnterpriceCredit.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				productApproveService.createCrmsProductApprove(map);
			}else{
				productApproveService.updateCrmsProductApprove(map);
			}
		}
		else if (DictConstants.TrdType.LiabilitiesDepositRegular.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				productApproveService.createLiabilitiesProductApprove(map);
			}else{
				productApproveService.updateLiabilitiesProductApprove(map);
			}
		}
		else if (DictConstants.TrdType.ApproveOverDraft.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){
				productApproveService.createOverDraftProductApprove(map);
			}else{
				productApproveService.updateOverDraftProductApprove(map);
			}
		}
		else if (DictConstants.TrdType.FeeCashFlowHandle_SINGLE.equals(ttTrdTrade.getTrdtype())) {
			if(isAdd){//费用 单例
				feeHandleCashflowService.createFeeHandleCashflow(map);
			}else{
				feeHandleCashflowService.updateFeeHandleCashflow(map);
			}
		}
		else if (DictConstants.TrdType.CustomTradeExpire.equals(ttTrdTrade.getTrdtype())) {//资产到期
			if(isAdd){
				tradeExpireService.createTradeExpire(map);
			}else{
				tradeExpireService.updateTradeExpire(map);
			}
		}
		else if (DictConstants.TrdType.CustomPassageWay.equals(ttTrdTrade.getTrdtype())) {//通道计划
			if(isAdd){
				feesPassAgewayService.createTempPassageWay(map);
			}else{
				feesPassAgewayService.updateTempPassageWay(map);
			}
		}
		else if(DictConstants.TrdType.CustomRateReset.equals(ttTrdTrade.getTrdtype())){ //利率重订
			if(isAdd){
				rateResetService.createRateChange(map);
			}else{
				ttTrdTrade.setTrade_date(ParameterUtil.getString(map, "effectDate", businessDate));
				rateResetService.updateRateChange(map);
			}
		}
		else if (DictConstants.TrdType.CustomDealEnd.equals(ttTrdTrade.getTrdtype())){
			if(isAdd){
				balanceService.insertBalance(map);
			}else{
				balanceService.updateBalance(map);
			}
		}
	}

	@Override
	public void noPassApprove(TtTrdTrade ttTrdTrade) {
		creditOperation(ttTrdTrade, ttTrdTrade, DictConstants.ApproveStatus.ApprovedNoPass);
	}
	
	@Override
	public void beforeSave(Map<String, Object> map) {
		
	}

	@Override
	public void afterSave(Map<String, Object> map, TtTrdTrade ttTrdTrade) {
		/*boolean isAdd = ParameterUtil.getBoolean(map, "isAdd", true);
		String prdProp  =ParameterUtil.getString(map, "prdProp", "");
		//存续期直接核实通过
		if (DictConstants.TrdType.CustomAdvanceMaturity.equals(ttTrdTrade.getTrdtype()) || DictConstants.TrdType.CustomCashFlowChange.equals(ttTrdTrade.getTrdtype()) || DictConstants.TrdType.CustomRateChange.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.CustomTradeExtend.equals(ttTrdTrade.getTrdtype()) || DictConstants.TrdType.CustomMultiResale.equals(ttTrdTrade.getTrdtype()) || DictConstants.TrdType.ApproveCancel.equals(ttTrdTrade.getTrdtype())
				|| DictConstants.TrdType.AccountTradeOffset.equals(ttTrdTrade.getTrdtype()) || DictConstants.prdProp.intermediateIncome.equals(prdProp)) {
			 if(isAdd){
				 ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Verifying);
				 trdTradeMapper.updateImportTrade(ttTrdTrade);
				 updateApproveStatus(ttTrdTrade,DictConstants.ApproveStatus.Verified);
			 }
		}*/
	}
	public void updateApproveStatus(TtTrdTrade ttTrdTrade,String tradeStatus){
		if(DictConstants.ApproveStatus.Verified.equals(tradeStatus)){
			String folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
			tradeManageService.confirmTrade(ttTrdTrade.getTrade_id(), ttTrdTrade.getVersion_number(), folder, ttTrdTrade.getOperator_id());
		}
		//更新业务审批单状态
		HashMap<String, Object> orderMap = new HashMap<String, Object>();
		orderMap.put("order_id", ttTrdTrade.getOrder_id());
		orderMap.put("is_locked", true);
		TtTrdOrder ttTrdOrder = trdOrderMapper.selectOrderForOrderId(orderMap);
		if(null != ttTrdOrder){//存续期没有事前审批
			ttTrdOrder.setOrder_status(tradeStatus);
			trdOrderMapper.updateApprove(ttTrdOrder);
		}
		ttTrdTrade.setTrade_status(tradeStatus);
		trdTradeMapper.updateTrade(ttTrdTrade);
	}
	
	
	public void confirmAdvanceMaturity(TdAdvanceMaturity advanceMaturity){
		//1.升版本，生成现金流
		//updateApproveMainVersion(advanceMaturity.getFpml(), advanceMaturity.getiCode(), advanceMaturity.getVersion(),advanceMaturity.getAmDate(),null);
		//2.修改新版本金额
		/*TdProductApproveMain approveMain = productApproveService.getProductApproveActivated(advanceMaturity.getiCode());
		double newAmt = approveMain.getAmt() - Double.valueOf(advanceMaturity.getAmAmt());
		approveMain.setAmt(newAmt);
		int i = productApproveService.modifyProductApproveMain(approveMain);	*///修改数据
		//JY.require(i==1, "更新原交易金额有误！");
		//3.释放掉提前到期的金额
		Map<String,Object> m = new HashMap<String, Object>();
		m.put("trade_id", advanceMaturity.getRefNo());
		m.put("order_id", null);
		//TtTrdTrade trade = trdTradeMapper.selectOneTradeIdorOrderId(m);
		//trdQuotaService.partReleaseQuota(trade.getOrder_id(), Double.valueOf(advanceMaturity.getAmAmt()));
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return productApproveService.getProductApproveActivated(ParameterUtil.getString(params, "dealNo", ""));
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		productApproveMain = productApproveService.getProductApproveActivated(ParameterUtil.getString(params, "trade_id", ""));
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		if(productApproveMain != null){
			tdTrdEvent.setDealNo(productApproveMain.getDealNo());//原交易流水号
			tdTrdEvent.setVersion(productApproveMain.getVersion());//原交易版本
			tdTrdEvent.setEventRefno(productApproveMain.getRefNo());//将利率变更的交易流水进行匹配赋值
		}else{
			tdTrdEvent.setDealNo(ParameterUtil.getString(params, "dealNo", ""));//原交易流水号
			tdTrdEvent.setVersion(0);//原交易版本
			tdTrdEvent.setEventRefno(ParameterUtil.getString(params, "dealNo", ""));//将利率变更的交易流水进行匹配赋值
		}
		
		tdTrdEvent.setEventType(DictConstants.TrdType.Custom);//利率变更
		tdTrdEvent.setEventTable("TD_PRODUCT_APPROVE_MAIN");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		//调用放款的会计分录
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		productApproveMain = productApproveService.getProductApproveActivated(ParameterUtil.getString(params, "dealNo", ""));
		
		try {
			//生成大额支付结算指令
			createSettleInfo(productApproveMain);
		} catch (Exception e) {
			JY.raise("生成大额支付指令出错",e);
		}
		
		String postdate = dayendDateService.getDayendDate();
		//调用账务接口
		Map<String, Object> amtMap = new HashMap<String, Object>();
		List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
		TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
		accTrdDaily.setAccAmt(productApproveMain.getAmt());
		accTrdDaily.setAccType(DurationConstants.AccType.LOAN_AMT);
		accTrdDaily.setDealNo(productApproveMain.getDealNo());//业务正式申请交易单
		accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
		accTrdDaily.setPostDate(postdate);
		accTrdDaily.setRefNo(productApproveMain.getRefNo());
		accTrdDailies.add(accTrdDaily);
		amtMap.put(DurationConstants.AccType.LOAN_AMT, productApproveMain.getAmt());
		accTrdDaily = new TdAccTrdDaily();
		accTrdDaily.setAccAmt(productApproveMain.getProcAmt());
		accTrdDaily.setAccType(DurationConstants.AccType.LOAN_PROCAMT);
		accTrdDaily.setDealNo(productApproveMain.getDealNo());//业务正式申请交易单
		accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
		accTrdDaily.setPostDate(postdate);
		accTrdDaily.setRefNo(productApproveMain.getRefNo());
		accTrdDailies.add(accTrdDaily);
		amtMap.put(DurationConstants.AccType.LOAN_PROCAMT, productApproveMain.getProcAmt());
		accTrdDaily = new TdAccTrdDaily();
		accTrdDaily.setAccAmt(productApproveMain.getIntAmt());
		accTrdDaily.setAccType(DurationConstants.AccType.LOAN_DISAMT);
		accTrdDaily.setDealNo(productApproveMain.getDealNo());//业务正式申请交易单
		accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
		accTrdDaily.setPostDate(postdate);
		accTrdDaily.setRefNo(productApproveMain.getRefNo());
		accTrdDailies.add(accTrdDaily);
		amtMap.put(DurationConstants.AccType.LOAN_DISAMT, productApproveMain.getDisAmt());
		
		InstructionPackage instructionPackage = new InstructionPackage();
		instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
		instructionPackage.setAmtMap(amtMap);
		String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
		if(null != flowId && !"".equals(flowId))
		{//更新回执会计套号  每笔交易
			for(int i = 0 ; i <accTrdDailies.size();i++)
			{
				accTrdDailies.get(i).setAcctNo(flowId);
				//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(acctDaily));
			}
		}
		
		//持久化数据
		batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		
		//判断是否需要调整利息
		try {
			String curDate= dayendDateService.getDayendDate();
			int mdates = productApproveMain.getmDate()==null ? 1 : PlaningTools.daysBetween(curDate,productApproveMain.getmDate());
			int days = 0;
			if(mdates <= 0){
				days = PlaningTools.daysBetween(productApproveMain.getvDate(),productApproveMain.getmDate());
			}else{
				days = PlaningTools.daysBetween(productApproveMain.getvDate(),curDate);
			}
			if(days<=0) {
                return;//结束
            }
			if(productApproveMain.getIntType().equalsIgnoreCase(DictConstants.intType.chargeAfter))// 后收息
			{
				double dayCalAmt = 0.00;//调整金额
				for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
					dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(productApproveMain.getvDate(), PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(productApproveMain.getAmt(),productApproveMain.getContractRate())),DayCountBasis.equal(productApproveMain.getBasis()).getDenominator(),2)
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(productApproveMain.getvDate(),PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(productApproveMain.getAmt(),productApproveMain.getContractRate())
												),DayCountBasis.equal(productApproveMain.getBasis()).getDenominator(),2)				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
					
					}
				 amtMap = new HashMap<String, Object>();
				 amtMap.put(DurationConstants.AccType.INTEREST_NORMAL, dayCalAmt);
				 accTrdDaily = new TdAccTrdDaily();
				 accTrdDaily.setAccAmt(dayCalAmt);
				 accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_NORMAL_ADJ);
				 accTrdDaily.setDealNo(productApproveMain.getDealNo());
				 accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				 accTrdDaily.setPostDate(curDate);
				 accTrdDaily.setRefNo(productApproveMain.getRefNo());
				 instructionPackage = new InstructionPackage();
				 Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
				 instructionPackage.setInfoMap(tradeMap);
				 instructionPackage.setAmtMap(amtMap);
				 String acctNo = bookkeepingService.generateEntry4Instruction(instructionPackage);
				 if(null != acctNo && !"".equals(acctNo))
				 {//跟新回执会计套号
					accTrdDaily.setAcctNo(acctNo);
					//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
				 }
				 accTrdDailyMapper.insert(accTrdDaily);
				 TdTrdTpos tpos = new TdTrdTpos();
				 tpos.setDealNo(productApproveMain.getDealNo());
				 tpos.setVersion(productApproveMain.getVersion());
				 tpos = tposMapper.selectByPrimaryKey(tpos);
				 tpos.setAccruedTint(PlaningTools.add(tpos.getAccruedTint(), dayCalAmt));//累计180应计利息
				 tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), dayCalAmt));//累计514利息收入
				 tpos.setPostdate(curDate);
				 this.tposMapper.updateByPrimaryKey(tpos);
			}else
			{
				//摊销调整
				try{
					params =new HashMap<String, Object>();
					params.put("dealNo", productApproveMain.getDealNo());//可以单笔跑，也可以批量
					params.put("curWorkDate", productApproveMain.getvDate());//可以单笔跑，也可以批量
					params.put("nextWorkDate", curDate);//必须带入;
					List<TdAccTrdDailyAmorVo> accTrdDailyAmorVos = accTrdDailyAmorVoMapper.getTdAccTrdDailyAmorVos(params);
					accTrdDailies = new ArrayList<TdAccTrdDaily>();
					TdTrdTpos tpos=null;
					amtMap = new HashMap<String, Object>();
					for(TdAccTrdDailyAmorVo accTrdDailyAmorVo : accTrdDailyAmorVos)
					{
						accTrdDaily = new TdAccTrdDaily();
						accTrdDaily.setDealNo(accTrdDailyAmorVo.getDealNo());
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_AMORDAILY_ADJ);						
						accTrdDaily.setAccAmt(accTrdDailyAmorVo.getActAmorAmt());//摊销金额
						accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
						accTrdDaily.setPostDate(accTrdDailyAmorVo.getCurDate());
						accTrdDailies.add(accTrdDaily);
						
						/*****************TPOS*************************/
						tpos = new TdTrdTpos();
						tpos.setDealNo(accTrdDailyAmorVo.getDealNo());
						tpos.setVersion(accTrdDailyAmorVo.getVersion());
						tpos = tposMapper.selectByPrimaryKey(tpos);
						tpos.setAmorTint(PlaningTools.add(tpos.getAmorTint(), accTrdDaily.getAccAmt()));//累计每日摊销计提
						tpos.setUnamorInt(PlaningTools.sub(tpos.getUnamorInt(), accTrdDaily.getAccAmt()));//未摊销金额
						tpos.setPostdate(accTrdDailyAmorVo.getCurDate());
						this.tposMapper.updateByPrimaryKey(tpos);
						//调用账务接口
						amtMap = new HashMap<String, Object>();
						amtMap.put(DurationConstants.AccType.INTEREST_AMORDAILY, accTrdDaily.getAccAmt());
						instructionPackage = new InstructionPackage();
						Map<String, Object> tradeMap = BeanUtil.beanToMap(productApproveMain);
						instructionPackage.setInfoMap(tradeMap);
						instructionPackage.setAmtMap(amtMap);
						String acctNo = bookkeepingService.generateEntry4Instruction(instructionPackage);
						if(null != flowId && !"".equals(flowId))
						{//跟新回执会计套号
							accTrdDaily.setAcctNo(acctNo);
							//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
						}
					}
					batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
				}catch (Exception e) {
					JY.debug(e.getMessage());
				}
			}
			List<TdProductFeeDeal> dealList = tdProductFeeDealMapper.getAllProductFeeDeals(params);
			if(dealList.size() > 0){
				for (TdProductFeeDeal tdProductFeeDeal : dealList) {
					double dayCalAmt = 0.00;//调整金额
					for(int i=0 ; i <days ;i++){//循环累加利息金额-跨越天数
						dayCalAmt = PlaningTools.add(dayCalAmt, 
								new BigDecimal(PlaningTools.sub(
								PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(productApproveMain.getvDate(), PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(),Frequency.DAY,i+1))).doubleValue(), 
												PlaningTools.mul(tdProductFeeDeal.getFeeCamt(),tdProductFeeDeal.getFeeRate())),DayCountBasis.equal(tdProductFeeDeal.getFeeCbasis()).getDenominator(),2)
								,PlaningTools.div(
										PlaningTools.mul(
												new BigDecimal(PlaningTools.daysBetween(productApproveMain.getvDate(),PlaningTools.addOrSubMonth_Day(productApproveMain.getvDate(),Frequency.DAY,i))).doubleValue(), 
												PlaningTools.mul(tdProductFeeDeal.getFeeCamt(),tdProductFeeDeal.getFeeRate())
												),DayCountBasis.equal(tdProductFeeDeal.getFeeCbasis()).getDenominator(),2)				
								)).setScale(2,java.math.BigDecimal.ROUND_HALF_UP).doubleValue()
							);
					
					}
					accTrdDaily = new TdAccTrdDaily();
					accTrdDaily.setDealNo(productApproveMain.getDealNo());
					accTrdDaily.setAccAmt(dayCalAmt);
					accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
					accTrdDaily.setPostDate(curDate);
					/*****************FEE DEAL*************************/
					tdProductFeeDeal.setAccruedTint(PlaningTools.add(tdProductFeeDeal.getAccruedTint(), dayCalAmt));//累计每日计提
					tdProductFeeDeal.setActualTint(PlaningTools.add(tdProductFeeDeal.getActualTint(), dayCalAmt));//累计每日利息
					tdProductFeeDealMapper.updateTdProductFeeDealByFeeDealNo(BeanUtil.beanToMap(tdProductFeeDeal));
					if(tdProductFeeDeal.getAccruedTint()<=0) {
                        continue;//负数的时候不再出计提
                    }
					//调用账务接口
					amtMap = new HashMap<String, Object>();
					if(tdProductFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeBefore)){
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_FEE_NORMAL_ADJ);
						amtMap.put(DurationConstants.AccType.INTEREST_FEE_AMORDAILY, accTrdDaily.getAccAmt());
					}else{
						accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_FEE_AMORDAILY_ADJ);
						amtMap.put(DurationConstants.AccType.INTEREST_FEE_NORMAL, accTrdDaily.getAccAmt());
					}
					
					instructionPackage = new InstructionPackage();
					Map<String, Object> beanMap = BeanUtil.beanToMap(productApproveMain);
					beanMap.put("intType", tdProductFeeDeal.getFeeCtype());
					beanMap.put("feeType", tdProductFeeDeal.getFeeType());
					instructionPackage.setInfoMap(beanMap);
					instructionPackage.setAmtMap(amtMap);
					flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
					if(null != flowId && !"".equals(flowId))
					{//跟新回执会计套号
						accTrdDaily.setAcctNo(flowId);
					}
					accTrdDailies.add(accTrdDaily);
				}
				batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JY.raise(e.getMessage());
		}
		
	}
	
	private void createSettleInfo(TdProductApproveMain approveMain) throws Exception{
		cnapsService.insertTdTrdSettleByProduct(approveMain);
	}
	
	/*****
	 * 额度操作
	 * @param ttTrdTrade
	 * @param o
	 */
	public void creditOperation(TtTrdTrade ttTrdTrade,Object o,String approveStatus)
	{
		try {
			creditService.creditOperation(ttTrdTrade, o, approveStatus);
		} catch (Exception e) {
			JY.error("额度操作失败", e);
		}
	}

}
