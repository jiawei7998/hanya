package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.service.TrdTposService;
import com.singlee.capital.trade.mapper.TdBalanceMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdBalance;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdBalanceService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;

/**
 * @className 结清
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service("TdBalanceServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdBalanceServiceImpl extends AbstractDurationService implements TdBalanceService,SlbpmCallBackInteface {

	@Autowired
	private TdBalanceMapper balanceMapper;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	TdBalanceMapper tdBalanceMapper;
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	@Autowired
	private TrdTposService tposService;
	
	@Override
	public TdBalance getAdvanceMaturityById(String dealNo) {
		return balanceMapper.getAdvanceMaturityById(dealNo);
	}
	@Override
	public TdBalance getAdvanceMaturityByMap(Map<String, Object> params) {
		return balanceMapper.getAdvanceMaturityByMap(params);
	}

	@Override
	public Page<TdBalance> getBalancePage(Map<String, Object> params, int isFinished) {
		String approveStatus = ParameterUtil.getString(params, "approveStatus", null);
		Page<TdBalance> page= new Page<TdBalance>();
		if(approveStatus != null && !"".equals(approveStatus)){
			String [] AMStatus = approveStatus.split(",");
			params.put("approveStatus", AMStatus);
		}
		if(isFinished == 1){
			page=balanceMapper.getBalanceList(params,ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=balanceMapper.getBalanceListFinished(params,ParameterUtil.getRowBounds(params));
		}else {
			page=balanceMapper.getBalanceListMine(params,ParameterUtil.getRowBounds(params));
		}
		List<TdBalance> list =page.getResult();
		for(TdBalance tm :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(tm.getRefNo());
			if(main!=null){
				tm.setProduct(main.getProduct());
				tm.setParty(main.getCounterParty());
				tm.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建提前到期金融工具表")
	public void insertBalance(Map<String, Object> params) {
		balanceMapper.insert(balance(params));
	}

	@Override
	@AutoLogMethod(value = "修改提前到期金融工具表")
	public void updateBalance(Map<String, Object> params) {
		balanceMapper.updateByPrimaryKey(balance(params));
	}

	
	private TdBalance balance(Map<String, Object> params){
		// map转实体类
		TdBalance balance = new TdBalance();
		try {
			BeanUtil.populate(balance, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
        if(balance.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(balance.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		balance.setVersion(productApproveMain.getVersion());
        }
    	balance.setSponsor(SlSessionHelper.getUserId());
    	balance.setSponInst(SlSessionHelper.getInstitutionId());
		balance.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return balance;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteAdvanceMaturity(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			balanceMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	@Override
	public TdBalance ifExistDuration(Map<String, Object> map) {
		return balanceMapper.ifExistDuration(map);
	}

	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 提起到期核实单号
	 */
	@Override
	public void dealVersionAndCashFlow(String trade_id) {}


	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdBalance balance = new TdBalance();
		BeanUtil.populate(balance, params);
		balance = balanceMapper.searchAdvanceMaturity(balance);
		return productApproveService.getProductApproveActivated(balance.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdBalance balance = new TdBalance();
		//BeanUtil.populate(balance, params);
		balance.setDealNo((String)params.get("trade_id"));
		balance = balanceMapper.searchAdvanceMaturity(balance);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(balance.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(balance.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(balance.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomAdvanceMaturity);//利率变更
		tdTrdEvent.setEventTable("TD_ADVANCE_MATURITY");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
		//tdBalanceMapper.insert(balance);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdBalance balance = new TdBalance();
		BeanUtil.populate(balance, params);
		/**
		 * 根据提前到期的交易数据进行数据清分
		 */
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(balance.getRefNo());
		JY.require(productApproveMain!=null, "原交易流水不存在！异常！");
		
		
	}

	@Override
	public Object getBizObj(String flowType, String flowId, String serialNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void statusChange(String flowType, String flowId, String serialNo,
			String status, String flowCompleteType) {
		try {
			HashMap<String, Object> tradeMap = new HashMap<String, Object>();
			if(DictConstants.ApproveStatus.Approving.equals(status)){
				status = DictConstants.ApproveStatus.Verifying;
			}else if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
				status = DictConstants.ApproveStatus.Verified;
			}			
			tradeMap.put("trade_id", serialNo);
			tradeMap.put("is_locked", true);
			TdBalance balance = tdBalanceMapper.selectByPrimaryKey(serialNo);
			TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(tradeMap);
			if("12".equals(status)){
				tposService.updateClear(balance.getRefNo());
			}
			ttTrdTrade.setTrade_status(status);
			tradeManageMapper.updateTrade(ttTrdTrade);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
