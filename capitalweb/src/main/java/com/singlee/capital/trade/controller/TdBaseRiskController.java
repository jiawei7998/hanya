package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdBaseRisk;
import com.singlee.capital.trade.service.BaseRiskService;

@Controller
@RequestMapping(value = "/TdBaseRiskController")
public class TdBaseRiskController extends CommonController{
	@Autowired
	private BaseRiskService baseRiskService;
	
	@ResponseBody
	@RequestMapping(value = "/getBaseRiskList")
	public RetMsg<PageInfo<TdBaseRisk>> getBaseRiskList(@RequestBody Map<String, Object> params) throws RException{
		Page<TdBaseRisk> param = baseRiskService.getBaseRiskLists(params);
		return RetMsgHelper.ok(param);
	}
}
