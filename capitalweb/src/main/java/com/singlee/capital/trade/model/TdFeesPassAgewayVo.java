package com.singlee.capital.trade.model;


public class TdFeesPassAgewayVo extends TdFeesPassAgeway{
	private String  bigKindName;  
	private String  midKindName; 
	private String  smallKindName;
	public String getBigKindName() {
		return bigKindName;
	}
	public void setBigKindName(String bigKindName) {
		this.bigKindName = bigKindName;
	}
	public String getMidKindName() {
		return midKindName;
	}
	public void setMidKindName(String midKindName) {
		this.midKindName = midKindName;
	}
	public String getSmallKindName() {
		return smallKindName;
	}
	public void setSmallKindName(String smallKindName) {
		this.smallKindName = smallKindName;
	}   
	

	
	
}
