package com.singlee.capital.trade.service.impl;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.interfacex.qdb.ESB.util.ExcelUtil;
import com.singlee.capital.interfacex.qdb.util.Reports;
import com.singlee.capital.trade.mapper.TdFeesBrokerMapper;
import com.singlee.capital.trade.model.TdFeesBroker;
import com.singlee.capital.trade.service.TdFeesBrokerService;

@Service("tdFeesBrokerServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdFeesBrokerServiceImpl implements TdFeesBrokerService{
	@Autowired
	private TdFeesBrokerMapper tdFeesBrokerMapper;
	@Resource
	DictionaryGetService dictionaryGetService;
	
   // 查询中介费
	@Override
	public Page<TdFeesBroker> pageFeesBrokerList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TdFeesBroker> result = tdFeesBrokerMapper.getFeesBrokerList(map, rowBounds);
		return result;
				
	}

	@Override
	public Page<TdFeesBroker> getFeesBrokerByDealNo(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdFeesBroker> result = tdFeesBrokerMapper.getFeesBrokerByDealNo(params, rowBounds);
		return result;
	}

	@Override
	public void updateFees(Map<String, Object> map) {
		tdFeesBrokerMapper.updateFees(map);
	}

	@Override
	public byte[] exportRep(Map<String, Object> map) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			DecimalFormat df=new DecimalFormat("0.00");
			NumberFormat nf=NumberFormat.getPercentInstance();
			nf.setMinimumFractionDigits(4);
			nf.setMaximumFractionDigits(4);
			ExcelUtil eu = new ExcelUtil(Reports.getReportModel((Reports.FeesBroker)));
			List<TdFeesBroker> list  = tdFeesBrokerMapper.getFeesBrokerList(map);
			List<TaDictVo> feeBasis = dictionaryGetService.getTaDictByCode("DayCounter");
			List<TaDictVo> feeType = dictionaryGetService.getTaDictByCode("feePayType");
			List<TaDictVo> feeSituation = dictionaryGetService.getTaDictByCode("feeSituation");
			for(int i=0;i<list.size();i++){
				eu.writeStr(0, i+2, 0, eu.getCenterStrWrapCellStyle(), (i+1)+"");
				//eu.writeStr(0, i+2, 1, eu.getCenterStrWrapCellStyle(), list.get(i).getDealNo());
				eu.writeStr(0, i+2, 1, eu.getCenterStrWrapCellStyle(), list.get(i).getRefNo());
				eu.writeStr(0, i+2, 2, eu.getCenterStrWrapCellStyle(), list.get(i).getPrdName());
				eu.writeStr(0, i+2, 3, eu.getCenterStrWrapCellStyle(), list.get(i).getDealDate());
				
				//新增2个属性    成本中心：cost    投资组合
				eu.writeStr(0, i+2, 4, eu.getCenterStrWrapCellStyle(),list.get(i).getCost());
				eu.writeStr(0, i+2, 5, eu.getCenterStrWrapCellStyle(),list.get(i).getPort());
				eu.writeStr(0, i+2, 9, eu.getCenterStrWrapCellStyle(),list.get(i).getOrganization());
				eu.writeStr(0, i+2, 10, eu.getCenterStrWrapCellStyle(),nf.format(list.get(i).getFeeRate()));
				eu.writeStr(0, i+2, 11, eu.getCenterStrWrapCellStyle(),list.get(i).getPartyName());
				for(TaDictVo ta : feeBasis){
					if(ta.getDict_key().equals(list.get(i).getFeeBasis())){
						list.get(i).setFeeBasis(ta.getDict_value());
					}
				}
				eu.writeStr(0, i+2, 6, eu.getCenterStrWrapCellStyle(), list.get(i).getFeeBasis());
				for(TaDictVo ta : feeType){
					if(ta.getDict_key().equals(list.get(i).getFeeType())){
						list.get(i).setFeeType(ta.getDict_value());
					}
				}
				eu.writeStr(0, i+2, 7, eu.getCenterStrWrapCellStyle(), list.get(i).getFeeType());
				for(TaDictVo ta : feeSituation){
					if(ta.getDict_key().equals(list.get(i).getFeeSituation())){
						list.get(i).setFeeSituation(ta.getDict_value());
					}
				}
				eu.writeStr(0, i+2, 8, eu.getCenterStrWrapCellStyle(), df.format(list.get(i).getFeesAmt())+"");
			}
			//to_do
			eu.setForceRecalculation();
			eu.getWb().write(os);
			byte[] content = os.toByteArray();
			return content;
		} catch (Exception e) {
			throw e;
		} finally {
			if(os != null){
				try {
					os.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<TdFeesBroker> getFeesBrokerListByDealNo(Map<String, Object> params) {
		return tdFeesBrokerMapper.getFeesBrokerByDealNo(params);
	}


}
