package com.singlee.capital.trade.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TrdTradeVo;

/**
 * 交易单管理业务层
 * @author thinkpad
 *
 */
public interface TradeManageService {
	/**
	 * 查询交易单对象，用于工作台显示
	 * @param map
	 * @return
	 */
	List<TtTrdTrade> searchTradeForDeskPage(HashMap<String,Object> map, int pageNum, int pageSize);
	
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	Object searchTradeList(HashMap<String,Object> map);
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	Object searchTradeListForCharges(HashMap<String,Object> map);
	/**
	 * 查询交易单明细信息
	 * @param map
	 * @return
	 */
	HashMap<String,Object> selectTradeInfo(HashMap<String,Object> map);

	/**
	 * 注销交易单信息
	 * @param trade_id
	 */
	void cancelTrade(String trade_id,int version_number,String folder,String memo,String opUserId);
	
	/**
	 * 确认交易单信息
	 * @param trade_id
	 */
	void dealConfirmTrade(String trade_id,int version_number,String folder,String opUserId);
	/**
	 * 确认交易单信息
	 * @param trade_id
	 */
	void confirmTrade(String trade_id,int version_number,String folder,String opUserId);
	
	/**
	 * 取消确认交易单信息
	 * @param trade_id
	 */
	void withdrawTrade(String trade_id,int version_number,String folder,String opUserId);
	
//	void  getPublicInfoImportTrade(TradeEditVo trdTradeVo,ExecutionReportCSTP executionReportCSTP);
	
	/**
	 * 
	 * @param order
	 * @return
	 */
	boolean needTrade(TtTrdOrder order,String folder);
	
	/**
	 * 保存审批单信息
	 * @param map
	 */
	TtTrdTrade saveTradeInfo(HashMap<String,Object> map);
	/**
	 * 获取trade信息
	 * @param map
	 * @return
	 */
	 TtTrdTrade searchTradeInfo(HashMap<String,Object> map);
	
	/**
	 * 查询导入的excel 同业拆借
	 * @param 
	 */
	HashMap<String,Object> selectImportTrade(Workbook wb,String folder);
	
	/**
	 * 不区分业务查询交易对手分页
	 * @param map
	 * @return TtTrdTrade
	 */
	Page<TtTrdTrade> searchTradeListWithoutFolder(HashMap<String,Object> map);
	/**
	 * 不区分业务查询交易对手分页
	 * @param map
	 * @return TrdTradeVo
	 */
	Page<TrdTradeVo> getTradeVoPage(HashMap<String,Object> map);
	
	/**
	 * 查询trade结果集
	 * @param map
	 * @return
	 */
	public List<TtTrdTrade> selectTtTrdTrade(HashMap<String,Object> map);
	
	/**
	 * 根据trade_id或order_id查询激活状态交易单
	 * @return
	 */
	public TtTrdTrade selectOneTradeIdorOrderId(Map<String,Object> map);
	
	/**
	 * 不需要生成核实单的处理逻辑
	 * @param order
	 * @param folder
	 */
	public void notNeedTradeProcess(TtTrdOrder order,String folder);
	
	/**
	 * 删除交易单
	 * @param map
	 * @return
	 */
	public int delTrade(Map<String,Object> map);
	
}
