package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.base.trade.model.TdProductParticipant;
import com.singlee.capital.common.pojo.BaseDomain;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.loan.model.TcLoanCheckboxDeal;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;

/**
 * @projectName 同业业务管理系统
 * @className 产品审批主表
 * @description TODO

 * @version 1.0
 */
@Entity
@Table(name = "TD_PRODUCT_APPROVE_MAIN")
public class TdProductApproveMain extends BaseDomain implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5132725957608613575L;
	/**
	 * 审批单编号 由函数生成
	 */
	@Id
	private String dealNo;
	/***********************审批块**************************/
	private Integer prdNo;//产品编号
	private String dealType;//审批单类型 1-审批 2-核实
	private String cNo;//交易对手编号 交易对手表的ID
	private String contact;//联系人
	private String sponsor;//审批发起人
	private String sponInst;//审批发起机构
	private String sponsorPhone;//审批发起人手机
	private String aDate;//审批开始日期
	private String conTitle;//合同标题 产品标题
	/***********************业务主要要素块**************************/
	private String ccy;//币种
	private double amt;//本金
	private String rateType;//利率类型
	private String rateCode;//基准利率代码
	private double rateDiff;//浮息点差(BP)
	private double rateScale;//浮动比例_%
	private double rateCap;//利率上限_%
	private double rateFloor;//利率下限_%
	private double enterpCostRate;//企业融资成本_%
	private double contractRate;//投资收益率_% 合同利率_%
	private double acturalRate;//实际利率_% 参与计提的利率
	private double compSumRate;//综合收益率_%
	private String inteffRule;//利率生效规则
	private double lateChargeRate;//逾期罚息率_%
	private String vDate;//起息日期
	private String tDate;//划款日期
	private String mDate;//到期日期
	private String meDate;//到账日期
	private String graceType;//宽限期类型 1-工作日 2-自然天
	private int gracePeriod;//宽限期天数
	private Integer term;//计息天数
	private Integer occupTerm;//资金占用天数
	private String basis;//计息基础
	private String amtFre;//还本频率
	private String intFre;//收息频率
	private String feeType;//收费方式
	private double mInt;//到期利息(元)
	private double mAmt;//到期金额（元）
	private String sDate;//首次收息日
	private String intType;//收息方式
	private String intMDate;//前收息截止日期
	private Integer intTerm;//先收息天数
	private double intAmt;//前收息金额
	private String remarkNote;//补充说明
	private String invtype;//会计四分类
	/*********************交易配合项**********************/
	private String dealDate;//交易日期
	/*********************清算项************************/
	private String isSettl;//是否清算
	private String acctType;//清算方式
	private String selfAccCode;//本方账号
	private String selfAccName;//本方账户名称
	private String selfBankCode;//本方开户行行号
	private String selfBankName;//本方开户行名称
	private String partyAccCode;//对手方账号
	private String partyAccName;//对手方账户名称
	private String partyBankCode;//对手方开户行行号
	private String partyBankName;//对手方开户行名称
	private String settlRemark;//缴款信息备注
	/******************回填项**********************/
	private double guidePrice;//指导价_%
	private double ftpCostPrice;//FTP成本价_%
	/*********************收益率**********************/
	private double chargeRate;//中收收益率_%
	private double chanRate;//通道费率_%
	private double earlyRate;//提前支取利率_%
	private double finAdvFeerate;//财务顾问费率_%
	private double otherFeerate;//其他费率_%
	private double transferRate;//转让利率_%
	private double transferFeerate;//转让费率_%
	private double custodFeerate;//第一托管费率_%
	private double isMecustod;//第一是否托管在我行
	private double manageFeerate;//第一管理费率_%
	private double custodFeerate2;//第二托管费率_%
	private double isMecustod2;//第二是否托管在我行
	private double manageFeerate2;//第二管理费率_%
	
	private double proxyRate;//代理咨询费率_%
	private double approveRate;//审批收益率_%
	private double supervisionRate;//监督费率_%
	private double serviceRate;//服务费率_%
	private String serviceFeeamtBasis;//服务费利率类型-国内信用证项下资产收益权
	private double serviceFeeamtAmt;//服务费计息金额-国内信用证项下资产收益权
	private int serviceFeeamtTerm;//服务费计息天数-国内信用证项下资产收益权
	private double chargeFeeamt;//中收金额
	private double serviceFeeamt;//服务费合计
	private double custodFeeamt;//托管费合计
	private double manageFeeamt;//管理费合计
	/*************************************************/
	private String category;//T24开户使用
	private String acctNo;
	private String acctName;
	private double acctAmt; 
	/*******************企业信用类金融资产受益权*****************************/
	private double invFundFeerate;//投资基金托管费率_%
	private String invFundFeefre;//投资基金托管费频率
	private double entrustServiceFeerate;//委贷服务费率_%-
	private String entrustServiceFeefre;//委贷服务费频率
	private double fundSvFeerate;//资金监管费率_%
	private String fundSvFeefre;//资金监管费频率
	private String financeFeetype;//财务费收取方式
	private String financeFeefre;//财务费收取频率
	private double financeFeerate;//财务顾问费率_%
	private String custodFeefre;//第二托管费频率
	private String custodFeefre2;//第一托管费频率
	/*******************融贷通项下流动资金接力贷款************************/
	private String loanFtp;//贷款FTP
	private String ifbmFtp;//同业FTP
	private String adjFtp;//FTP调整
	
	private String custodFeeBasis;//托管利率类型
	private double aloneFeerate;//独立监督费率_%
	private double feeRate;//费率
	
	private double acctRecvAmt;//应收账款金额
	private double acctGuraAmt;//担保账款金额
	
	
	private String factoringType;//再保理类型
	private String acctSettlType;//账户结算类型
	private String custAcctngtype;//客户类型ACCT
	
	private double expectRate;//EXPECT_RATE
	private String serviceFeefre;//SERVICE_FEEFRE
	private String lcType;//LC_TYPE
	private String riskLevel;
	private String ldNo;
	private String crmsLoadId;
	private String crmsApproveId;
	private double repoRate;//远期回购方费率_%
	private double finaFeeamt;//财务顾问费
	
	//add by lxy 20170829
	private double approveRate8;//审批汇率
	private double verifyRate8; //正式汇率
	
	private String houseName;//户名 --同业存放(定期)
	/**
	 * -- Add/modify columns 
		alter table TD_PRODUCT_APPROVE_MAIN add party_ecif_code VARCHAR2(32);
		-- Add comments to the columns 
		comment on column TD_PRODUCT_APPROVE_MAIN.party_ecif_code
		  is '参与方客户号';
	 */
	private String partyEcifCode;//参与方清算客户号
	
	
	/***********青岛添加字段 *********/
	private String cost;//成本中心
	private String port;//投资组合
	
	private String productName;	 //资产名称
	private String productCode;	//资产业务代码（业务人员自行制定）
	private String nostroFlag;	//是否涉及存放同业活期账户
	private String nostroAcct;	//存放同业活期账户
	private String nostroVdate;	//存放同业活期起息日期 
	private double nostroRate;	//存放同业活期交易的利率
	
	private String invesstmentScope;	//理财产品投资范围
	private String accountManager;      //客户经理
	private String riskIns ;            //风险参与机构
	private String riskParty;          //实际风险承担方名称
	private String bookkeepingCoding;   //记账科目及编码
	
	//后期添加字段
	private int nDays;					//n个工作日后收息
	private String amtManage;			//额度管理
	private String baseProductRemark;	//业务结构-备注信息
	private String fundSource;			//资金来源
	private String invWay;				//投资方式
	private String fundInvestment;		//资金投向
	private String marketingOrg;		//营销机构
	private Double costPrice;			//成本价格
	private String transType;			//交易类型
	private String matchLiabType;		//匹配负债种类
	private Double afterTheAmount;		//劣后金额（亿）
	private String isNonStandard;		//是否行内非标
	private Double targetsQty;			//标的物总数量
	private String targetPricingBenchmark;//标的物定价基准
	private String incomeType;			//收益类型
	private String prodProperty;		//产品性质
	private String riskWay; //产品性质
	private String creditCust; //授信客户号
	private String quotaCust; //额度客户号
	private String accThreeSubject;		//会计三分类
	private String doubleSubject;		//二分类
	private String singleOrGather;		//单一或集合
	private String listingLocation;		//上市地点
	private String saleType;		//业务类型
	private String contactPhone;//联系人电话
	private String contactAddr;//联系人地址
	private String dealTitle;//审批单标题
	private String dealNoticeNo;//同业金融审批通知书编号
	private String iDate;//结息日期
	
	@Transient
	private List<TdBaseAssetGuarty> tdBaseAssetGuartys;//基础资产
	@Transient
	private List<TdBaseAssetStruct> tdBaseAssetStructs;
	
	/**
	 * 通道计划
	 */
	@Transient
	private List<TmFeesPassagewy> feesPassagewyList;
	
	/**
	 * 中介费
	 * */
	@Transient
	private List<TdFeesBroker> tdFeesBrokers;
	/***********青岛添加字段 *********/
	
	
	public String getPartyEcifCode() {
		return partyEcifCode;
	}

	public List<TdBaseAssetGuarty> getTdBaseAssetGuartys() {
		return tdBaseAssetGuartys;
	}

	public void setTdBaseAssetGuartys(List<TdBaseAssetGuarty> tdBaseAssetGuartys) {
		this.tdBaseAssetGuartys = tdBaseAssetGuartys;
	}

	public List<TdFeesBroker> getTdFeesBrokers() {
		return tdFeesBrokers;
	}

	public void setTdFeesBrokers(List<TdFeesBroker> tdFeesBrokers) {
		this.tdFeesBrokers = tdFeesBrokers;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactAddr() {
		return contactAddr;
	}

	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}

	public List<TmFeesPassagewy> getFeesPassagewyList() {
		return feesPassagewyList;
	}

	public void setFeesPassagewyList(List<TmFeesPassagewy> feesPassagewyList) {
		this.feesPassagewyList = feesPassagewyList;
	}

	public List<TdBaseAssetStruct> getTdBaseAssetStructs() {
		return tdBaseAssetStructs;
	}

	public void setTdBaseAssetStructs(List<TdBaseAssetStruct> tdBaseAssetStructs) {
		this.tdBaseAssetStructs = tdBaseAssetStructs;
	}

	public String getDealTitle() {
		return dealTitle;
	}

	public void setDealTitle(String dealTitle) {
		this.dealTitle = dealTitle;
	}

	public String getDealNoticeNo() {
		return dealNoticeNo;
	}

	public void setDealNoticeNo(String dealNoticeNo) {
		this.dealNoticeNo = dealNoticeNo;
	}

	public String getiDate() {
		return iDate;
	}

	public void setiDate(String iDate) {
		this.iDate = iDate;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getNostroFlag() {
		return nostroFlag;
	}

	public void setNostroFlag(String nostroFlag) {
		this.nostroFlag = nostroFlag;
	}

	public String getNostroAcct() {
		return nostroAcct;
	}

	public void setNostroAcct(String nostroAcct) {
		this.nostroAcct = nostroAcct;
	}

	public String getNostroVdate() {
		return nostroVdate;
	}

	public void setNostroVdate(String nostroVdate) {
		this.nostroVdate = nostroVdate;
	}

	public double getNostroRate() {
		return nostroRate;
	}

	public void setNostroRate(double nostroRate) {
		this.nostroRate = nostroRate;
	}

	public String getInvesstmentScope() {
		return invesstmentScope;
	}

	public void setInvesstmentScope(String invesstmentScope) {
		this.invesstmentScope = invesstmentScope;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public String getRiskIns() {
		return riskIns;
	}

	public void setRiskIns(String riskIns) {
		this.riskIns = riskIns;
	}

	public String getRiskParty() {
		return riskParty;
	}

	public void setRiskParty(String riskParty) {
		this.riskParty = riskParty;
	}

	public String getBookkeepingCoding() {
		return bookkeepingCoding;
	}

	public void setBookkeepingCoding(String bookkeepingCoding) {
		this.bookkeepingCoding = bookkeepingCoding;
	}

	public int getnDays() {
		return nDays;
	}

	public void setnDays(int nDays) {
		this.nDays = nDays;
	}

	public String getAmtManage() {
		return amtManage;
	}

	public void setAmtManage(String amtManage) {
		this.amtManage = amtManage;
	}

	public String getBaseProductRemark() {
		return baseProductRemark;
	}

	public void setBaseProductRemark(String baseProductRemark) {
		this.baseProductRemark = baseProductRemark;
	}

	public String getFundSource() {
		return fundSource;
	}

	public void setFundSource(String fundSource) {
		this.fundSource = fundSource;
	}

	public String getInvWay() {
		return invWay;
	}

	public void setInvWay(String invWay) {
		this.invWay = invWay;
	}

	public String getFundInvestment() {
		return fundInvestment;
	}

	public void setFundInvestment(String fundInvestment) {
		this.fundInvestment = fundInvestment;
	}

	public String getMarketingOrg() {
		return marketingOrg;
	}

	public void setMarketingOrg(String marketingOrg) {
		this.marketingOrg = marketingOrg;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getMatchLiabType() {
		return matchLiabType;
	}

	public void setMatchLiabType(String matchLiabType) {
		this.matchLiabType = matchLiabType;
	}

	public Double getAfterTheAmount() {
		return afterTheAmount;
	}

	public void setAfterTheAmount(Double afterTheAmount) {
		this.afterTheAmount = afterTheAmount;
	}

	public String getIsNonStandard() {
		return isNonStandard;
	}

	public void setIsNonStandard(String isNonStandard) {
		this.isNonStandard = isNonStandard;
	}

	public Double getTargetsQty() {
		return targetsQty;
	}

	public void setTargetsQty(Double targetsQty) {
		this.targetsQty = targetsQty;
	}

	public String getTargetPricingBenchmark() {
		return targetPricingBenchmark;
	}

	public void setTargetPricingBenchmark(String targetPricingBenchmark) {
		this.targetPricingBenchmark = targetPricingBenchmark;
	}

	public String getIncomeType() {
		return incomeType;
	}

	public void setIncomeType(String incomeType) {
		this.incomeType = incomeType;
	}

	public String getProdProperty() {
		return prodProperty;
	}

	public void setProdProperty(String prodProperty) {
		this.prodProperty = prodProperty;
	}

	public String getRiskWay() {
		return riskWay;
	}

	public void setRiskWay(String riskWay) {
		this.riskWay = riskWay;
	}

	public String getCreditCust() {
		return creditCust;
	}

	public void setCreditCust(String creditCust) {
		this.creditCust = creditCust;
	}

	public String getQuotaCust() {
		return quotaCust;
	}

	public void setQuotaCust(String quotaCust) {
		this.quotaCust = quotaCust;
	}

	public String getAccThreeSubject() {
		return accThreeSubject;
	}

	public void setAccThreeSubject(String accThreeSubject) {
		this.accThreeSubject = accThreeSubject;
	}

	public String getDoubleSubject() {
		return doubleSubject;
	}

	public void setDoubleSubject(String doubleSubject) {
		this.doubleSubject = doubleSubject;
	}

	public String getSingleOrGather() {
		return singleOrGather;
	}

	public void setSingleOrGather(String singleOrGather) {
		this.singleOrGather = singleOrGather;
	}

	public String getListingLocation() {
		return listingLocation;
	}

	public void setListingLocation(String listingLocation) {
		this.listingLocation = listingLocation;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public void setPartyEcifCode(String partyEcifCode) {
		this.partyEcifCode = partyEcifCode;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public double getApproveRate8() {
		return approveRate8;
	}

	public void setApproveRate8(double approveRate8) {
		this.approveRate8 = approveRate8;
	}

	public double getVerifyRate8() {
		return verifyRate8;
	}
	
	public void setVerifyRate8(double verifyRate8) {
		this.verifyRate8 = verifyRate8;
	}
	
	public double getRepoRate() {
		return repoRate;
	}

	public void setRepoRate(double repoRate) {
		this.repoRate = repoRate;
	}

	public double getFinaFeeamt() {
		return finaFeeamt;
	}

	public void setFinaFeeamt(double finaFeeamt) {
		this.finaFeeamt = finaFeeamt;
	}

	@Transient
	private String suspendStatus;//挂起状态
	@Transient
	private String instanceId;//流程实例
	
	public String getLdNo() {
		return ldNo;
	}

	public void setLdNo(String ldNo) {
		this.ldNo = ldNo;
	}

	public String getCrmsLoadId() {
		return crmsLoadId;
	}

	public void setCrmsLoadId(String crmsLoadId) {
		this.crmsLoadId = crmsLoadId;
	}

	public String getCrmsApproveId() {
		return crmsApproveId;
	}

	public void setCrmsApproveId(String crmsApproveId) {
		this.crmsApproveId = crmsApproveId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getSuspendStatus() {
		return suspendStatus;
	}

	public void setSuspendStatus(String suspendStatus) {
		this.suspendStatus = suspendStatus;
	}

	public String getLcType() {
		return lcType;
	}

	public void setLcType(String lcType) {
		this.lcType = lcType;
	}

	public String getServiceFeefre() {
		return serviceFeefre;
	}

	public void setServiceFeefre(String serviceFeefre) {
		this.serviceFeefre = serviceFeefre;
	}

	public double getExpectRate() {
		return expectRate;
	}

	public void setExpectRate(double expectRate) {
		this.expectRate = expectRate;
	}

	public String getFactoringType() {
		return factoringType;
	}

	public void setFactoringType(String factoringType) {
		this.factoringType = factoringType;
	}

	public String getAcctSettlType() {
		return acctSettlType;
	}

	public void setAcctSettlType(String acctSettlType) {
		this.acctSettlType = acctSettlType;
	}

	public String getCustAcctngtype() {
		return custAcctngtype;
	}

	public void setCustAcctngtype(String custAcctngtype) {
		this.custAcctngtype = custAcctngtype;
	}

	public double getAcctRecvAmt() {
		return acctRecvAmt;
	}

	public void setAcctRecvAmt(double acctRecvAmt) {
		this.acctRecvAmt = acctRecvAmt;
	}

	public double getAcctGuraAmt() {
		return acctGuraAmt;
	}

	public void setAcctGuraAmt(double acctGuraAmt) {
		this.acctGuraAmt = acctGuraAmt;
	}

	public double getAloneFeerate() {
		return aloneFeerate;
	}

	public void setAloneFeerate(double aloneFeerate) {
		this.aloneFeerate = aloneFeerate;
	}

	public double getFeeRate() {
		return feeRate;
	}

	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}

	public String getCustodFeeBasis() {
		return custodFeeBasis;
	}

	public void setCustodFeeBasis(String custodFeeBasis) {
		this.custodFeeBasis = custodFeeBasis;
	}

	public String getServiceFeeamtBasis() {
		return serviceFeeamtBasis;
	}

	public void setServiceFeeamtBasis(String serviceFeeamtBasis) {
		this.serviceFeeamtBasis = serviceFeeamtBasis;
	}

	public double getServiceFeeamtAmt() {
		return serviceFeeamtAmt;
	}

	public void setServiceFeeamtAmt(double serviceFeeamtAmt) {
		this.serviceFeeamtAmt = serviceFeeamtAmt;
	}

	public int getServiceFeeamtTerm() {
		return serviceFeeamtTerm;
	}

	public void setServiceFeeamtTerm(int serviceFeeamtTerm) {
		this.serviceFeeamtTerm = serviceFeeamtTerm;
	}

	public String getLoanFtp() {
		return loanFtp;
	}

	public void setLoanFtp(String loanFtp) {
		this.loanFtp = loanFtp;
	}

	public String getIfbmFtp() {
		return ifbmFtp;
	}

	public void setIfbmFtp(String ifbmFtp) {
		this.ifbmFtp = ifbmFtp;
	}

	public String getAdjFtp() {
		return adjFtp;
	}

	public void setAdjFtp(String adjFtp) {
		this.adjFtp = adjFtp;
	}

	@Transient
	private String trdtype;
	@Transient
	private TdProductApproveCrms productApproveCrms;
	@Transient
	private TdProductApproveOverDraft productApproveOverDraft;
	
	
	public TdProductApproveOverDraft getProductApproveOverDraft() {
		return productApproveOverDraft;
	}

	public void setProductApproveOverDraft(
			TdProductApproveOverDraft productApproveOverDraft) {
		this.productApproveOverDraft = productApproveOverDraft;
	}

	public TdProductApproveCrms getProductApproveCrms() {
		return productApproveCrms;
	}

	public void setProductApproveCrms(TdProductApproveCrms productApproveCrms) {
		this.productApproveCrms = productApproveCrms;
	}

	public String getTrdtype() {
		return trdtype;
	}

	public void setTrdtype(String trdtype) {
		this.trdtype = trdtype;
	}

	public double getInvFundFeerate() {
		return invFundFeerate;
	}

	public void setInvFundFeerate(double invFundFeerate) {
		this.invFundFeerate = invFundFeerate;
	}

	public String getInvFundFeefre() {
		return invFundFeefre;
	}

	public void setInvFundFeefre(String invFundFeefre) {
		this.invFundFeefre = invFundFeefre;
	}

	public double getEntrustServiceFeerate() {
		return entrustServiceFeerate;
	}

	public void setEntrustServiceFeerate(double entrustServiceFeerate) {
		this.entrustServiceFeerate = entrustServiceFeerate;
	}

	public String getEntrustServiceFeefre() {
		return entrustServiceFeefre;
	}

	public void setEntrustServiceFeefre(String entrustServiceFeefre) {
		this.entrustServiceFeefre = entrustServiceFeefre;
	}

	public double getFundSvFeerate() {
		return fundSvFeerate;
	}

	public void setFundSvFeerate(double fundSvFeerate) {
		this.fundSvFeerate = fundSvFeerate;
	}

	public String getFundSvFeefre() {
		return fundSvFeefre;
	}

	public void setFundSvFeefre(String fundSvFeefre) {
		this.fundSvFeefre = fundSvFeefre;
	}

	public String getFinanceFeetype() {
		return financeFeetype;
	}

	public void setFinanceFeetype(String financeFeetype) {
		this.financeFeetype = financeFeetype;
	}

	public String getFinanceFeefre() {
		return financeFeefre;
	}

	public void setFinanceFeefre(String financeFeefre) {
		this.financeFeefre = financeFeefre;
	}

	public double getFinanceFeerate() {
		return financeFeerate;
	}

	public void setFinanceFeerate(double financeFeerate) {
		this.financeFeerate = financeFeerate;
	}

	public String getCustodFeefre() {
		return custodFeefre;
	}

	public void setCustodFeefre(String custodFeefre) {
		this.custodFeefre = custodFeefre;
	}

	public String getCustodFeefre2() {
		return custodFeefre2;
	}

	public void setCustodFeefre2(String custodFeefre2) {
		this.custodFeefre2 = custodFeefre2;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public double getAcctAmt() {
		return acctAmt;
	}

	public void setAcctAmt(double acctAmt) {
		this.acctAmt = acctAmt;
	}

	@Transient
	private String preDate;//利率变更使用  本区间的起息日期
	
	public String getPreDate() {
		return preDate;
	}

	public void setPreDate(String preDate) {
		this.preDate = preDate;
	}

	/**
	 * 计息频率
	 */
	private String intCalcFre;
	/**
	 * 资料影像
	 */
	private String image;
	/**
	 * 更新时间
	 */
	private String lastUpdate;
	/**
	 * 版本
	 */
	@Id
	private int version;
	/**
	 * 是否有效
	 */
	private String isActive;
	/**
	 * 现金流条款
	 */
	private String fpml;

	/**
	 * 外部流水号
	 */
	private String refNo;
	/**
	 * 融资人
	 */
	@Transient
	private String financier;
	/**
	 * 项目名称
	 */
	@Transient
	private String projectName;
	/**
	 * 产品信息
	 */
	@Transient
	private TcProduct product;
	/**
	 * 发起人
	 */
	@Transient
	private TaUser user;
	/**
	 * 发起机构
	 */
	@Transient
	private TtInstitution institution;
	/**
	 * 交易对手表
	 */
	@Transient
	private TtCounterParty counterParty;
	/**
	 * 产品审批子表列表
	 */
	@Transient
	private List<TdProductApproveSub> productApproveSubs;
	
	@Transient
	private List<TdProductApproveProtocols> productProcotols;
	/**
	 * 参与方信息-转成可配置MAP KEY-VALUE
	 */
	@Transient
	private List<TdProductApproveSub> participantApprove;
	/**
	 * 中间业务收入
	 */
	@Transient
	private List<TdFeesPlan> feesPlanList;
	/**
	 * 增信措施
	 */
	@Transient
	private List<TdIncreaseCredit> increaseCredits;
	/**
	 * 交叉营销
	 */
	@Transient
	private List<TdAssetLc> baseAssetLCs;
	/**
	 * 基础资产
	 */
	@Transient
	private List<TdBaseAsset> baseAssets;
	
	/**
	 * 基础资产-保理收款权
	 */
	@Transient
	private List<TdBaseAssetBl> baseAssetBlInfos;
	
	/***
	 * 成本中心
	 */
	@Transient
	private TtTrdCost trdCost;
	
	/****
	 * 投资组合
	 */
	@Transient
	private TtTrdPort trdPort;
	
	/****
	 * 营销机构
	 */
	@Transient
	private TtInstitution marketIns;
	

	public TtTrdCost getTrdCost() {
		return trdCost;
	}

	public void setTrdCost(TtTrdCost trdCost) {
		this.trdCost = trdCost;
	}

	public TtTrdPort getTrdPort() {
		return trdPort;
	}

	public void setTrdPort(TtTrdPort trdPort) {
		this.trdPort = trdPort;
	}

	public TtInstitution getMarketIns() {
		return marketIns;
	}

	public void setMarketIns(TtInstitution marketIns) {
		this.marketIns = marketIns;
	}

	public List<TdBaseAssetBl> getBaseAssetBlInfos() {
		return baseAssetBlInfos;
	}

	public void setBaseAssetBlInfos(List<TdBaseAssetBl> baseAssetBlInfos) {
		this.baseAssetBlInfos = baseAssetBlInfos;
	}

	/**
	 * 票据基础资产
	 */
	@Transient
	private List<TdBaseAssetBill> baseAssetBills;
	@Transient
	private List<TdAssetSpecificBond> baseAssetSpecificBonds;
	
	public List<TdAssetSpecificBond> getBaseAssetSpecificBonds() {
		return baseAssetSpecificBonds;
	}

	public void setBaseAssetSpecificBonds(
			List<TdAssetSpecificBond> baseAssetSpecificBonds) {
		this.baseAssetSpecificBonds = baseAssetSpecificBonds;
	}

	/**
	 * 股票信息
	 */
	@Transient
	private List<TdStockInfo> stockInfos;
	/**
	 * 风险资产
	 */
	@Transient
	private List<TdRiskAsset> riskAssets;

	/**
	 * 风险缓释
	 */
	@Transient
	private List<TdRiskSlowRelease> riskSlowReleases;
	
	/**
	 * CheckList审查信息
	 */
	@Transient
	private List<TcLoanCheckboxDeal> tcLoanCheckboxDeal;
	
	/**
	 * 额度占用信息
	 */
	@Transient
	private List<TdProductCustCredit> tdProductCustCredit;
	
	/**
	 * 原本金
	 */
	@Transient
	private double originalAmt;
	/**
	 * 原利率
	 */
	@Transient
	private double originalRate;
	
	/**
	 * task_id
	 */
	@Transient
	private String taskId;
	
	/**
	 * task_name
	 */
	@Transient
	private String taskName;
	
	/**
	 * 数据来源
	 */
	@Transient
	private String tradeSource;
	/**
	 * 实际成交金额
	 */
	private double procAmt;
	/**
	 * 折溢价金额
	 */
	private double disAmt;

	@Transient
	private TdTrdTpos tpos;//TPOS 持仓信息
	
	
	public TdTrdTpos getTpos() {
		return tpos;
	}

	public void setTpos(TdTrdTpos tpos) {
		this.tpos = tpos;
	}

	public double getProcAmt() {
		return procAmt;
	}

	public void setProcAmt(double procAmt) {
		this.procAmt = procAmt;
	}

	public double getDisAmt() {
		return disAmt;
	}

	public void setDisAmt(double disAmt) {
		this.disAmt = disAmt;
	}

	/**
	 * 最后一个付息日
	 */
	private String eDate;

	public String geteDate() {
		return eDate;
	}

	public void seteDate(String eDate) {
		this.eDate = eDate;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public Integer getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(Integer prdNo) {
		this.prdNo = prdNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getConTitle() {
		return conTitle;
	}

	public void setConTitle(String conTitle) {
		this.conTitle = conTitle;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public double getRateDiff() {
		return rateDiff;
	}

	public void setRateDiff(double rateDiff) {
		this.rateDiff = rateDiff;
	}

	public double getContractRate() {
		return contractRate;
	}

	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}

	public double getActuralRate() {
		return acturalRate;
	}

	public void setActuralRate(double acturalRate) {
		this.acturalRate = acturalRate;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String gettDate() {
		return tDate;
	}

	public void settDate(String tDate) {
		this.tDate = tDate;
	}

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getMeDate() {
		return meDate;
	}

	public void setMeDate(String meDate) {
		this.meDate = meDate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public double getmInt() {
		return mInt;
	}

	public void setmInt(double mInt) {
		this.mInt = mInt;
	}

	public double getmAmt() {
		return mAmt;
	}

	public void setmAmt(double mAmt) {
		this.mAmt = mAmt;
	}

	public String getAmtFre() {
		return amtFre;
	}

	public void setAmtFre(String amtFre) {
		this.amtFre = amtFre;
	}

	public String getIntCalcFre() {
		return intCalcFre;
	}

	public void setIntCalcFre(String intCalcFre) {
		this.intCalcFre = intCalcFre;
	}

	public String getIntFre() {
		return intFre;
	}

	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}

	public String getIntType() {
		return intType;
	}

	public void setIntType(String intType) {
		this.intType = intType;
	}

	public String getIntMDate() {
		return intMDate;
	}

	public void setIntMDate(String intMDate) {
		this.intMDate = intMDate;
	}

	public Integer getIntTerm() {
		return intTerm;
	}

	public void setIntTerm(Integer intTerm) {
		this.intTerm = intTerm;
	}

	public double getIntAmt() {
		return intAmt;
	}

	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getFpml() {
		return fpml;
	}

	public void setFpml(String fpml) {
		this.fpml = fpml;
	}

	public String getSelfAccCode() {
		return selfAccCode;
	}

	public void setSelfAccCode(String selfAccCode) {
		this.selfAccCode = selfAccCode;
	}

	public String getSelfAccName() {
		return selfAccName;
	}

	public void setSelfAccName(String selfAccName) {
		this.selfAccName = selfAccName;
	}

	public String getSelfBankCode() {
		return selfBankCode;
	}

	public void setSelfBankCode(String selfBankCode) {
		this.selfBankCode = selfBankCode;
	}

	public String getSelfBankName() {
		return selfBankName;
	}

	public void setSelfBankName(String selfBankName) {
		this.selfBankName = selfBankName;
	}

	public String getPartyAccCode() {
		return partyAccCode;
	}

	public void setPartyAccCode(String partyAccCode) {
		this.partyAccCode = partyAccCode;
	}

	public String getPartyAccName() {
		return partyAccName;
	}

	public void setPartyAccName(String partyAccName) {
		this.partyAccName = partyAccName;
	}

	public String getPartyBankCode() {
		return partyBankCode;
	}

	public void setPartyBankCode(String partyBankCode) {
		this.partyBankCode = partyBankCode;
	}

	public String getPartyBankName() {
		return partyBankName;
	}

	public void setPartyBankName(String partyBankName) {
		this.partyBankName = partyBankName;
	}

	public TcProduct getProduct() {
		return product;
	}

	public void setProduct(TcProduct product) {
		this.product = product;
	}

	public TaUser getUser() {
		return user;
	}

	public void setUser(TaUser user) {
		this.user = user;
	}

	public TtInstitution getInstitution() {
		return institution;
	}

	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}

	public TtCounterParty getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(TtCounterParty counterParty) {
		this.counterParty = counterParty;
	}

	public List<TdProductApproveSub> getProductApproveSubs() {
		return productApproveSubs;
	}

	public void setProductApproveSubs(List<TdProductApproveSub> productApproveSubs) {
		this.productApproveSubs = productApproveSubs;
	}

	
	public List<TdProductApproveSub> getParticipantApprove() {
		return participantApprove;
	}

	public void setParticipantApprove(List<TdProductApproveSub> participantApprove) {
		this.participantApprove = participantApprove;
	}

	public List<TdFeesPlan> getFeesPlanList() {
		return feesPlanList;
	}

	public void setFeesPlanList(List<TdFeesPlan> feesPlanList) {
		this.feesPlanList = feesPlanList;
	}

	public List<TdIncreaseCredit> getIncreaseCredits() {
		return increaseCredits;
	}

	public void setIncreaseCredits(List<TdIncreaseCredit> increaseCredits) {
		this.increaseCredits = increaseCredits;
	}

	public List<TdAssetLc> getBaseAssetLCs() {
		return baseAssetLCs;
	}

	public void setBaseAssetLCs(List<TdAssetLc> baseAssetLCs) {
		this.baseAssetLCs = baseAssetLCs;
	}

	public List<TdBaseAsset> getBaseAssets() {
		return baseAssets;
	}

	public void setBaseAssets(List<TdBaseAsset> baseAssets) {
		this.baseAssets = baseAssets;
	}

	public List<TdRiskAsset> getRiskAssets() {
		return riskAssets;
	}

	public void setRiskAssets(List<TdRiskAsset> riskAssets) {
		this.riskAssets = riskAssets;
	}

	public List<TdRiskSlowRelease> getRiskSlowReleases() {
		return riskSlowReleases;
	}

	public void setRiskSlowReleases(List<TdRiskSlowRelease> riskSlowReleases) {
		this.riskSlowReleases = riskSlowReleases;
	}

	public double getOriginalAmt() {
		return originalAmt;
	}

	public void setOriginalAmt(double originalAmt) {
		this.originalAmt = originalAmt;
	}

	public double getOriginalRate() {
		return originalRate;
	}

	public void setOriginalRate(double originalRate) {
		this.originalRate = originalRate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public List<TdBaseAssetBill> getBaseAssetBills() {
		return baseAssetBills;
	}

	public void setBaseAssetBills(List<TdBaseAssetBill> baseAssetBills) {
		this.baseAssetBills = baseAssetBills;
	}

	public List<TdStockInfo> getStockInfos() {
		return stockInfos;
	}

	public void setStockInfos(List<TdStockInfo> stockInfos) {
		this.stockInfos = stockInfos;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getFinancier() {
		return financier;
	}

	public void setFinancier(String financier) {
		this.financier = financier;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTradeSource() {
		return tradeSource;
	}

	public void setTradeSource(String tradeSource) {
		this.tradeSource = tradeSource;
	}

	public double getRateScale() {
		return rateScale;
	}

	public void setRateScale(double rateScale) {
		this.rateScale = rateScale;
	}

	public double getRateCap() {
		return rateCap;
	}

	public void setRateCap(double rateCap) {
		this.rateCap = rateCap;
	}

	public double getRateFloor() {
		return rateFloor;
	}

	public void setRateFloor(double rateFloor) {
		this.rateFloor = rateFloor;
	}

	public double getEnterpCostRate() {
		return enterpCostRate;
	}

	public void setEnterpCostRate(double enterpCostRate) {
		this.enterpCostRate = enterpCostRate;
	}

	public double getCompSumRate() {
		return compSumRate;
	}

	public void setCompSumRate(double compSumRate) {
		this.compSumRate = compSumRate;
	}

	public String getInteffRule() {
		return inteffRule;
	}

	public void setInteffRule(String inteffRule) {
		this.inteffRule = inteffRule;
	}

	public double getLateChargeRate() {
		return lateChargeRate;
	}

	public void setLateChargeRate(double lateChargeRate) {
		this.lateChargeRate = lateChargeRate;
	}

	public double getGuidePrice() {
		return guidePrice;
	}

	public void setGuidePrice(double guidePrice) {
		this.guidePrice = guidePrice;
	}

	public double getFtpCostPrice() {
		return ftpCostPrice;
	}

	public void setFtpCostPrice(double ftpCostPrice) {
		this.ftpCostPrice = ftpCostPrice;
	}

	public String getGraceType() {
		return graceType;
	}

	public void setGraceType(String graceType) {
		this.graceType = graceType;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public Integer getOccupTerm() {
		return occupTerm;
	}

	public void setOccupTerm(Integer occupTerm) {
		this.occupTerm = occupTerm;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getRemarkNote() {
		return remarkNote;
	}

	public void setRemarkNote(String remarkNote) {
		this.remarkNote = remarkNote;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getIsSettl() {
		return isSettl;
	}

	public void setIsSettl(String isSettl) {
		this.isSettl = isSettl;
	}

	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public String getSettlRemark() {
		return settlRemark;
	}

	public void setSettlRemark(String settlRemark) {
		this.settlRemark = settlRemark;
	}

	public String getSponsorPhone() {
		return sponsorPhone;
	}

	public void setSponsorPhone(String sponsorPhone) {
		this.sponsorPhone = sponsorPhone;
	}

	public double getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(double chargeRate) {
		this.chargeRate = chargeRate;
	}

	public double getChanRate() {
		return chanRate;
	}

	public void setChanRate(double chanRate) {
		this.chanRate = chanRate;
	}

	public double getEarlyRate() {
		return earlyRate;
	}

	public void setEarlyRate(double earlyRate) {
		this.earlyRate = earlyRate;
	}

	public double getFinAdvFeerate() {
		return finAdvFeerate;
	}

	public void setFinAdvFeerate(double finAdvFeerate) {
		this.finAdvFeerate = finAdvFeerate;
	}

	public double getOtherFeerate() {
		return otherFeerate;
	}

	public void setOtherFeerate(double otherFeerate) {
		this.otherFeerate = otherFeerate;
	}

	public double getTransferRate() {
		return transferRate;
	}

	public void setTransferRate(double transferRate) {
		this.transferRate = transferRate;
	}

	public double getCustodFeerate() {
		return custodFeerate;
	}

	public void setCustodFeerate(double custodFeerate) {
		this.custodFeerate = custodFeerate;
	}

	public double getIsMecustod() {
		return isMecustod;
	}

	public void setIsMecustod(double isMecustod) {
		this.isMecustod = isMecustod;
	}

	public double getManageFeerate() {
		return manageFeerate;
	}

	public void setManageFeerate(double manageFeerate) {
		this.manageFeerate = manageFeerate;
	}

	public double getCustodFeerate2() {
		return custodFeerate2;
	}

	public void setCustodFeerate2(double custodFeerate2) {
		this.custodFeerate2 = custodFeerate2;
	}

	public double getIsMecustod2() {
		return isMecustod2;
	}

	public void setIsMecustod2(double isMecustod2) {
		this.isMecustod2 = isMecustod2;
	}

	public double getManageFeerate2() {
		return manageFeerate2;
	}

	public void setManageFeerate2(double manageFeerate2) {
		this.manageFeerate2 = manageFeerate2;
	}

	public double getProxyRate() {
		return proxyRate;
	}

	public void setProxyRate(double proxyRate) {
		this.proxyRate = proxyRate;
	}

	public double getApproveRate() {
		return approveRate;
	}

	public void setApproveRate(double approveRate) {
		this.approveRate = approveRate;
	}

	public double getSupervisionRate() {
		return supervisionRate;
	}

	public void setSupervisionRate(double supervisionRate) {
		this.supervisionRate = supervisionRate;
	}

	public double getServiceRate() {
		return serviceRate;
	}

	public void setServiceRate(double serviceRate) {
		this.serviceRate = serviceRate;
	}

	public double getChargeFeeamt() {
		return chargeFeeamt;
	}

	public void setChargeFeeamt(double chargeFeeamt) {
		this.chargeFeeamt = chargeFeeamt;
	}

	public double getServiceFeeamt() {
		return serviceFeeamt;
	}

	public void setServiceFeeamt(double serviceFeeamt) {
		this.serviceFeeamt = serviceFeeamt;
	}

	public double getCustodFeeamt() {
		return custodFeeamt;
	}

	public void setCustodFeeamt(double custodFeeamt) {
		this.custodFeeamt = custodFeeamt;
	}

	public double getManageFeeamt() {
		return manageFeeamt;
	}

	public void setManageFeeamt(double manageFeeamt) {
		this.manageFeeamt = manageFeeamt;
	}

	public double getTransferFeerate() {
		return transferFeerate;
	}

	public void setTransferFeerate(double transferFeerate) {
		this.transferFeerate = transferFeerate;
	}

	public List<TdProductApproveProtocols> getProductProcotols() {
		return productProcotols;
	}

	public void setProductProcotols(List<TdProductApproveProtocols> productProcotols) {
		this.productProcotols = productProcotols;
	}

	public List<TcLoanCheckboxDeal> getTcLoanCheckboxDeal() {
		return tcLoanCheckboxDeal;
	}

	public void setTcLoanCheckboxDeal(List<TcLoanCheckboxDeal> tcLoanCheckboxDeal) {
		this.tcLoanCheckboxDeal = tcLoanCheckboxDeal;
	}

	public List<TdProductCustCredit> getTdProductCustCredit() {
		return tdProductCustCredit;
	}

	public void setTdProductCustCredit(List<TdProductCustCredit> tdProductCustCredit) {
		this.tdProductCustCredit = tdProductCustCredit;
	}

	public String getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}	
	
	//授信审批编号-企信
	private String enterpriseInq;
	//授信用途-融贷通
	private String fincreditInq;

	public String getEnterpriseInq() {
		return enterpriseInq;
	}

	public void setEnterpriseInq(String enterpriseInq) {
		this.enterpriseInq = enterpriseInq;
	}

	public String getFincreditInq() {
		return fincreditInq;
	}

	public void setFincreditInq(String fincreditInq) {
		this.fincreditInq = fincreditInq;
	}
	
	@Transient
	private List<TdBaseRisk>  baseRisks;

	public List<TdBaseRisk> getBaseRisks() {
		return baseRisks;
	}

	public void setBaseRisks(List<TdBaseRisk> baseRisks) {
		this.baseRisks = baseRisks;
	}
	
	
	private int expiryDay;//结息日-活期
	private String invStyle;//投资方式-证券收益凭证   1-直投  2-其他

	public int getExpiryDay() {
		return expiryDay;
	}

	public void setExpiryDay(int expiryDay) {
		this.expiryDay = expiryDay;
	}

	public String getInvStyle() {
		return invStyle;
	}

	public void setInvStyle(String invStyle) {
		this.invStyle = invStyle;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	@Transient
	List<TdProductParticipant> tdProductParticipants;

	public List<TdProductParticipant> getTdProductParticipants() {
		return tdProductParticipants;
	}

	public void setTdProductParticipants(
			List<TdProductParticipant> tdProductParticipants) {
		this.tdProductParticipants = tdProductParticipants;
	}

	@Override
	public String toString() {
		return "TdProductApproveMain [dealNo=" + dealNo + ", prdNo=" + prdNo
				+ ", dealType=" + dealType + ", cNo=" + cNo + ", contact="
				+ contact + ", sponsor=" + sponsor + ", sponInst=" + sponInst
				+ ", sponsorPhone=" + sponsorPhone + ", aDate=" + aDate
				+ ", conTitle=" + conTitle + ", ccy=" + ccy + ", amt=" + amt
				+ ", rateType=" + rateType + ", rateCode=" + rateCode
				+ ", rateDiff=" + rateDiff + ", rateScale=" + rateScale
				+ ", rateCap=" + rateCap + ", rateFloor=" + rateFloor
				+ ", enterpCostRate=" + enterpCostRate + ", contractRate="
				+ contractRate + ", acturalRate=" + acturalRate
				+ ", compSumRate=" + compSumRate + ", inteffRule=" + inteffRule
				+ ", lateChargeRate=" + lateChargeRate + ", vDate=" + vDate
				+ ", tDate=" + tDate + ", mDate=" + mDate + ", meDate="
				+ meDate + ", graceType=" + graceType + ", gracePeriod="
				+ gracePeriod + ", term=" + term + ", occupTerm=" + occupTerm
				+ ", basis=" + basis + ", amtFre=" + amtFre + ", intFre="
				+ intFre + ", feeType=" + feeType + ", mInt=" + mInt
				+ ", mAmt=" + mAmt + ", sDate=" + sDate + ", intType="
				+ intType + ", intMDate=" + intMDate + ", intTerm=" + intTerm
				+ ", intAmt=" + intAmt + ", remarkNote=" + remarkNote
				+ ", invtype=" + invtype + ", dealDate=" + dealDate
				+ ", isSettl=" + isSettl + ", acctType=" + acctType
				+ ", selfAccCode=" + selfAccCode + ", selfAccName="
				+ selfAccName + ", selfBankCode=" + selfBankCode
				+ ", selfBankName=" + selfBankName + ", partyAccCode="
				+ partyAccCode + ", partyAccName=" + partyAccName
				+ ", partyBankCode=" + partyBankCode + ", partyBankName="
				+ partyBankName + ", settlRemark=" + settlRemark
				+ ", guidePrice=" + guidePrice + ", ftpCostPrice="
				+ ftpCostPrice + ", chargeRate=" + chargeRate + ", chanRate="
				+ chanRate + ", earlyRate=" + earlyRate + ", finAdvFeerate="
				+ finAdvFeerate + ", otherFeerate=" + otherFeerate
				+ ", transferRate=" + transferRate + ", transferFeerate="
				+ transferFeerate + ", custodFeerate=" + custodFeerate
				+ ", isMecustod=" + isMecustod + ", manageFeerate="
				+ manageFeerate + ", custodFeerate2=" + custodFeerate2
				+ ", isMecustod2=" + isMecustod2 + ", manageFeerate2="
				+ manageFeerate2 + ", proxyRate=" + proxyRate
				+ ", approveRate=" + approveRate + ", supervisionRate="
				+ supervisionRate + ", serviceRate=" + serviceRate
				+ ", serviceFeeamtBasis=" + serviceFeeamtBasis
				+ ", serviceFeeamtAmt=" + serviceFeeamtAmt
				+ ", serviceFeeamtTerm=" + serviceFeeamtTerm
				+ ", chargeFeeamt=" + chargeFeeamt + ", serviceFeeamt="
				+ serviceFeeamt + ", custodFeeamt=" + custodFeeamt
				+ ", manageFeeamt=" + manageFeeamt + ", category=" + category
				+ ", acctNo=" + acctNo + ", acctName=" + acctName
				+ ", acctAmt=" + acctAmt + ", invFundFeerate=" + invFundFeerate
				+ ", invFundFeefre=" + invFundFeefre
				+ ", entrustServiceFeerate=" + entrustServiceFeerate
				+ ", entrustServiceFeefre=" + entrustServiceFeefre
				+ ", fundSvFeerate=" + fundSvFeerate + ", fundSvFeefre="
				+ fundSvFeefre + ", financeFeetype=" + financeFeetype
				+ ", financeFeefre=" + financeFeefre + ", financeFeerate="
				+ financeFeerate + ", custodFeefre=" + custodFeefre
				+ ", custodFeefre2=" + custodFeefre2 + ", loanFtp=" + loanFtp
				+ ", ifbmFtp=" + ifbmFtp + ", adjFtp=" + adjFtp
				+ ", custodFeeBasis=" + custodFeeBasis + ", aloneFeerate="
				+ aloneFeerate + ", feeRate=" + feeRate + ", acctRecvAmt="
				+ acctRecvAmt + ", acctGuraAmt=" + acctGuraAmt
				+ ", factoringType=" + factoringType + ", acctSettlType="
				+ acctSettlType + ", custAcctngtype=" + custAcctngtype
				+ ", expectRate=" + expectRate + ", serviceFeefre="
				+ serviceFeefre + ", lcType=" + lcType + ", riskLevel="
				+ riskLevel + ", ldNo=" + ldNo + ", crmsLoadId=" + crmsLoadId
				+ ", crmsApproveId=" + crmsApproveId + ", repoRate=" + repoRate
				+ ", finaFeeamt=" + finaFeeamt + ", approveRate8="
				+ approveRate8 + ", verifyRate8=" + verifyRate8
				+ ", suspendStatus=" + suspendStatus + ", instanceId="
				+ instanceId + ", trdtype=" + trdtype + ", productApproveCrms="
				+ productApproveCrms + ", productApproveOverDraft="
				+ productApproveOverDraft + ", preDate=" + preDate
				+ ", intCalcFre=" + intCalcFre + ", image=" + image
				+ ", lastUpdate=" + lastUpdate + ", version=" + version
				+ ", isActive=" + isActive + ", fpml=" + fpml + ", refNo="
				+ refNo + ", financier=" + financier + ", projectName="
				+ projectName + ", product=" + product + ", user=" + user
				+ ", institution=" + institution + ", counterParty="
				+ counterParty + ", productApproveSubs=" + productApproveSubs
				+ ", productProcotols=" + productProcotols
				+ ", participantApprove=" + participantApprove
				+ ", feesPlanList=" + feesPlanList + ", increaseCredits="
				+ increaseCredits + ", baseAssetLCs=" + baseAssetLCs
				+ ", baseAssets=" + baseAssets + ", baseAssetBlInfos="
				+ baseAssetBlInfos + ", baseAssetBills=" + baseAssetBills
				+ ", baseAssetSpecificBonds=" + baseAssetSpecificBonds
				+ ", stockInfos=" + stockInfos + ", riskAssets=" + riskAssets
				+ ", riskSlowReleases=" + riskSlowReleases
				+ ", tcLoanCheckboxDeal=" + tcLoanCheckboxDeal
				+ ", tdProductCustCredit=" + tdProductCustCredit
				+ ", originalAmt=" + originalAmt + ", originalRate="
				+ originalRate + ", taskId=" + taskId + ", taskName="
				+ taskName + ", tradeSource=" + tradeSource + ", procAmt="
				+ procAmt + ", disAmt=" + disAmt + ", tpos=" + tpos
				+ ", eDate=" + eDate + ", enterpriseInq=" + enterpriseInq
				+ ", fincreditInq=" + fincreditInq + ", baseRisks=" + baseRisks
				+ ", expiryDay=" + expiryDay + ", invStyle=" + invStyle
				+ ", tdProductParticipants=" + tdProductParticipants + "]";
	}
	@Transient
	private String retcode;
	@Transient
	private String retmsg;

	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	
}	 