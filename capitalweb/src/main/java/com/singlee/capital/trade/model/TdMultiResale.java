package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 资产卖断
 * @author xuhui
 * @createDate 2016-9-25 上午12:00:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_MULTIRESALE")
public class TdMultiResale implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3720610986116412801L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;

	/**
	 * 合同号
	 */
	private String resTitle;
	/**
	 * 转让总金额
	 */
	private double resAmt;
	/**
	 * 转让日收息金额
	 */
	private double resInterest;
	/**
	 * 转让投资收益
	 */
	private double transferIncome;
	
	/**
	 * 转让日期
	 */
	private String resDate;
	
	/**
	 * 交易对手编号
	 */
	private String cNo;
	
	/**
	 * 卖断原因
	 */
	private String resReason;
	/**
	 * 影像资料
	 */
	private String image;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	/**
	 * 版本号
	 */
	private int version;
	/**
	 * 卖断备注
	 */
	private String remark;
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private List<TdMultiResaleDetail> detailList;
	@Transient
	private String partyName;
	@Transient
	private String partyCode;// 交易对手客户编号
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getResTitle() {
		return resTitle;
	}
	public void setResTitle(String resTitle) {
		this.resTitle = resTitle;
	}
	public double getResAmt() {
		return resAmt;
	}
	public void setResAmt(double resAmt) {
		this.resAmt = resAmt;
	}
	public double getResInterest() {
		return resInterest;
	}
	public void setResInterest(double resInterest) {
		this.resInterest = resInterest;
	}
	public String getResDate() {
		return resDate;
	}
	public void setResDate(String resDate) {
		this.resDate = resDate;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getResReason() {
		return resReason;
	}
	public void setResReason(String resReason) {
		this.resReason = resReason;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public List<TdMultiResaleDetail> getDetailList() {
		return detailList;
	}
	public void setDetailList(List<TdMultiResaleDetail> detailList) {
		this.detailList = detailList;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public double getTransferIncome() {
		return transferIncome;
	}
	public void setTransferIncome(double transferIncome) {
		this.transferIncome = transferIncome;
	}
	public String getPartyCode() {
		return partyCode;
	}
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
}
