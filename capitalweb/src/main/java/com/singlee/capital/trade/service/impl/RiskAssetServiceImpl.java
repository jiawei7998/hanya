package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TdRiskAssetMapper;
import com.singlee.capital.trade.model.TdRiskAsset;
import com.singlee.capital.trade.service.RiskAssetService;

/**
 * @projectName 同业业务管理系统
 * @className 风险资产服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RiskAssetServiceImpl implements RiskAssetService {
	
	@Autowired
	public TdRiskAssetMapper tdRiskAssetMapper;
	
	@Override
	public List<TdRiskAsset> getRiskAssetList(Map<String, Object> params) {
		return tdRiskAssetMapper.getRiskAssetList(params);
	}

	@Override
	public String getRiskLevel(String params) {
		return tdRiskAssetMapper.getRiskLevel(params);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
