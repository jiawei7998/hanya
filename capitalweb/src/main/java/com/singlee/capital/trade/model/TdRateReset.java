package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 提前到期
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_RATE_RESET")
public class TdRateReset implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	/**
	 * 交易日期
	 */
	private String dealDate;
	/**
	 * 原利率
	 */
	private double rate;
	/**
	 * 变更利率
	 */
	@Column(name="CHANGERATE")
	private double changeRate;
	/**
	 * 变更日期
	 */
	@Column(name="CHANGEDATE")
	private String changeDate;
	/**
	 * 变更原因
	 */
	@Column(name="CHANGEREASON")
	private String changeReason;
	/**
	 * 生效日期
	 */
	@Column(name="EFFECTDATE")
	private String effectDate;
	
	@Column(name="ENDDATE")
	private String endDate;
	
	private double adjintamt;
	
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	
	private int version;
	private String rateCode;
	private double rateCodeValue;
	private double rateDiff;
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	
	public double getAdjintamt() {
		return adjintamt;
	}
	public void setAdjintamt(double adjintamt) {
		this.adjintamt = adjintamt;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getChangeRate() {
		return changeRate;
	}
	public void setChangeRate(double changeRate) {
		this.changeRate = changeRate;
	}
	public String getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
	public String getChangeReason() {
		return changeReason;
	}
	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}
	public String getEffectDate() {
		return effectDate;
	}
	public void setEffectDate(String effectDate) {
		this.effectDate = effectDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public double getRateDiff() {
		return rateDiff;
	}
	public void setRateDiff(double rateDiff) {
		this.rateDiff = rateDiff;
	}
	public double getRateCodeValue() {
		return rateCodeValue;
	}
	public void setRateCodeValue(double rateCodeValue) {
		this.rateCodeValue = rateCodeValue;
	}
	
}
