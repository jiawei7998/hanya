package com.singlee.capital.trade.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TtTrdAccDepositMapper;
import com.singlee.capital.trade.model.TtTrdAccDeposit;
import com.singlee.capital.trade.service.AccDepositService;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccDepositServiceImpl implements AccDepositService {

	@Autowired
	private TtTrdAccDepositMapper depositMapper;
	
	@Override
	public TtTrdAccDeposit getDepositById(String adjId) {
		return depositMapper.selectByPrimaryKey(adjId);
	}

	@Override
	public Page<TtTrdAccDeposit> getDepositPage(Map<String, Object> params, int isFinished) {
		//状态
		String approveStatus = ParameterUtil.getString(params, "approveStatus", null);
		if(approveStatus != null && !"".equals(approveStatus)){
			String [] status = approveStatus.split(",");
			params.put("approveStatus", Arrays.asList(status));
		}else{
			params.put("approveStatus", null);
		}
		//金融机构类型
		String instType = ParameterUtil.getString(params, "instType", null);
		if(instType != null && !"".equals(instType)){
			String [] instTypeses = instType.split(",");
			params.put("instType", Arrays.asList(instTypeses));
		}else{
			params.put("instType", null);
		}
		//账户类型
		String accType = ParameterUtil.getString(params, "accType", null);
		if(accType != null && !"".equals(accType)){
			String [] accTypes = accType.split(",");
			params.put("accType", Arrays.asList(accTypes));
		}else{
			params.put("accType", null);
		}
		if(isFinished == 1){
			return depositMapper.getDepositList(params, ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			return depositMapper.getDepositListFinished(params, ParameterUtil.getRowBounds(params));
		}else {
			return depositMapper.getDepositListMine(params, ParameterUtil.getRowBounds(params));
		}
	}

	@Override
	@AutoLogMethod(value = "创建同业存放表")
	public void createDeposit(Map<String, Object> params) {
		depositMapper.insert(deposit(params));
	}
	
	@Override
	@AutoLogMethod(value = "修改同业存放表")
	public void updateDeposit(Map<String, Object> params) {
		depositMapper.updateByPrimaryKey(deposit(params));
	}
	
	private TtTrdAccDeposit deposit(Map<String, Object> params){
		// map转实体类
		TtTrdAccDeposit deposit = new TtTrdAccDeposit();
		try {
			BeanUtil.populate(deposit, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		String type = ParameterUtil.getString(params, "type", "");

		// 初始化表数据
        deposit.setApproveStatus(DictConstants.ApproveStatus.New);
        deposit.setIsActive(DictConstants.YesNo.YES);
       
		//审批完成，为交易单添加一条数据
		if("add".equals(type)){
			deposit.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			deposit.setCreateUser(SlSessionHelper.getUserId());
		}
		deposit.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		deposit.setUpdateUser(SlSessionHelper.getUserId());
		return deposit;
	}
	
	/**
	 * 删除
	 */
	@Override
	public void deleteDeposit(String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			depositMapper.deleteByPrimaryKey(ids[i]);
		}
	}
	
	/**
	 * 获取序列
	 */
	@Override
	public String getSeq() {
		int seq = depositMapper.getSeq();
		return "D" + DateUtil.getCurrentCompactDateTimeAsString() + seq;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
