package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesPassAgewayChange;

public interface TdFeesPassAgewayChangeMapper extends Mapper<TdFeesPassAgewayChange>{

	public Page<TdFeesPassAgewayChange> getTdFeesPassAgeways(Map<String, Object> params, RowBounds rb);

	public Page<TdFeesPassAgewayChange> getPassageWayList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<TdFeesPassAgewayChange> getPassageWayListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<TdFeesPassAgewayChange> getPassageWayListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public void deleteByDealNo(List<String> list);

	public TdFeesPassAgewayChange selectTrtrdById(String dealNo);

	public List<TdFeesPassAgewayChange> getTdFeesPassAgewaysByDeal(
			Map<String, Object> params);

	public void addFeeChange(TdFeesPassAgewayChange change);

	public TdFeesPassAgewayChange searchPassageWayChange(TdFeesPassAgewayChange tdFeesPassAgewayChange);

	
}
