package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.TdBalance;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.service.TdBalanceService;
@Controller
@RequestMapping(value = "/TdBalanceController")
public class TdBalanceController extends CommonController{
	@Autowired
	private TdBalanceService tdBalanceService;
	
	
	/**
	 * 条件查询列表(结清)待复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageBalanceUnfinished")
	public RetMsg<PageInfo<TdBalance>> searchPageBalanceNoFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdBalance> page = tdBalanceService.getBalancePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(结清)已复核
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageBalanceFinished")
	public RetMsg<PageInfo<TdBalance>> searchPageBalanceFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdBalance> page = tdBalanceService.getBalancePage(params,2);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(结清)我发起的
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageBalanceMine")
	public RetMsg<PageInfo<TdBalance>> searchPageBalanceMine(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdBalance> page = tdBalanceService.getBalancePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/ifExistBalance")
	public RetMsg<TdBalance> ifExistDuration(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(tdBalanceService.ifExistDuration(map));
	}
	/**
	 * 查询原交易是否已存在交易结清。
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/ifTradeEnd")
	public RetMsg<TdBalance> ifTradeEnd(@RequestBody Map<String,Object> map) {
		return RetMsgHelper.ok(tdBalanceService.getAdvanceMaturityByMap(map));
	}
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getTdProductAppMain")
	public TdProductApproveMain getTdProductAppMain(@RequestBody Map<String,Object> params){
		TdProductApproveMain approveMain = tdBalanceService.getProductApproveMainOfBefore(params);
		return approveMain;
	}
	/**
	 * 新增
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/insertBalance")
	public RetMsg<Serializable> insertBalance(@RequestBody Map<String,Object> params) {
		tdBalanceService.insertBalance(params);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/updateBalance")
	public RetMsg<Serializable> updateBalance(@RequestBody Map<String,Object> params) {
		tdBalanceService.updateBalance(params);
		return RetMsgHelper.ok();
	}
	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 * @author Hunter
	 * @date 2016-9-25
	 */
	@ResponseBody
	@RequestMapping(value = "/removeBalance")
	public RetMsg<Serializable> removeProductApprove(@RequestBody String[] dealNos) {
		tdBalanceService.deleteAdvanceMaturity(dealNos);
		return RetMsgHelper.ok();
	}
}
