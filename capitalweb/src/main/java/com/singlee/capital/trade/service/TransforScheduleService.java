package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforSchedule;

/**
 * @projectName 同业业务管理系统

 * @version 1.0
 */
public interface TransforScheduleService {


	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易展期主键
	 * @return 交易展期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTransforSchedule getTransforScheduleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTransforSchedule> getTransforSchedulePage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 交易展期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createTransforSchedule(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateTransforSchedule(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 交易展期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteTransforSchedule(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
}
