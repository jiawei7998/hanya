package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.trade.model.Function;
import com.singlee.capital.trade.model.TaModule;
import com.singlee.capital.trade.service.TaModuleService;

@Controller
@RequestMapping(value="/TaModuleController")
public class TaModuleController extends CommonController{
	@Autowired
	private TaModuleService moduleService;
    /**
     * 获取菜单列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/modules")
	public RetMsg<List<TaModule>> modules(){
		TaUser user = getThreadLocalUser();
		List<TaModule> page = moduleService.recursionModule(user==null?null:user.getRoleIdList());
		return RetMsgHelper.ok(page); 
	}
	
	  /**
     * 获取菜单列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/module")
	public RetMsg<Serializable> module(){
		return RetMsgHelper.simple("error.base.0001"); 
//		TaUser user = getThreadLocalUser();
//		LinkedList<TreeModule> page = moduleService.recursionModule(user.getRoleIdList());
		
	}
	/*
	 * 查询菜单列表
	 */
	@ResponseBody
	@RequestMapping(value="/menusList")
	public RetMsg<List<TaModule>> menus(){
		List<TaModule> list = moduleService.ModulesList();
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 新增菜单
	 */
	@ResponseBody
	@RequestMapping(value="/addTaModule")
	public RetMsg<Serializable> addTaModule(@RequestBody TaModule taModule) throws RException{
		moduleService.insertTaModules(taModule);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改菜单
	 */
	@ResponseBody
	@RequestMapping(value="/updateTaModule")
	public RetMsg<Serializable> updateTaModule(@RequestBody TaModule taModule)throws RException{
		moduleService.updateTaModules(taModule);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除菜单
	 */
	@ResponseBody
	@RequestMapping(value="/deleteTaModule")
	public RetMsg<Serializable> deleteTaModule(@RequestBody String[] moduleIds){
		System.out.println("deleteTaModule");
		moduleService.deleteTaModules(moduleIds);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 根据moduleId查询功能点
	 */
	@ResponseBody
	@RequestMapping(value="/selectFunctionModuleById")
	public RetMsg<PageInfo<Function>> selectFunctionModuleById(@RequestBody Map<String, Object> param) throws RException{
		System.out.println("selectFunctionModuleById");
		Page<Function> list = moduleService.selectFunctionAllById(param);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 新增功能点
	 */
	@ResponseBody
	@RequestMapping(value="/insertFunction")
	public RetMsg<Serializable> insertFunction(@RequestBody Function function) throws RException{
		System.out.println("insertFunction");
		String msg = moduleService.insertFunction(function);
		if("0000".equalsIgnoreCase(msg)){
			return RetMsgHelper.ok();
		}else{
			return RetMsgHelper.simple("001", msg);
		}
	}
	
	/**
	 * 修改功能点
	 */
	@ResponseBody
	@RequestMapping(value="/updateFunction")
	public RetMsg<Serializable> updateFunction(@RequestBody Function function){
		System.out.println("insertFunction");
		moduleService.updateFunction(function);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除功能点
	 */
	@ResponseBody
	@RequestMapping(value="/deleteFunction")
	public RetMsg<Serializable> deleteFunction(@RequestBody Map<String, Object> param){
		String functionId = ParameterUtil.getString(param, "functionIds", "");
		String moduleId = ParameterUtil.getString(param, "moduleId", "");
		Function function = new Function();
		function.setFunctionId(functionId);
		function.setModuleId(moduleId);
		moduleService.deleteFunctionById(function);
		return RetMsgHelper.ok();
	}
}
