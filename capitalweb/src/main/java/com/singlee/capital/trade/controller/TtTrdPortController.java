package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TtTrdPort;
import com.singlee.capital.trade.service.TtTrdPortService;

@RequestMapping(value = "/TtTrdPortController")
@Controller
public class TtTrdPortController extends CommonController{
	@Autowired
	private TtTrdPortService ttTrdPortService;
	
	/**
	 * 查询投资组合列表分页
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTtTrdPorts")
	public RetMsg<PageInfo<TtTrdPort>> searchTtTrdPort(@RequestBody Map<String, Object> param) throws RException{
		Page<TtTrdPort> params = ttTrdPortService.searchTtTrdPort(param);
		return RetMsgHelper.ok(params);
	}
	
	/**
	 * 新增
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtTrdPorts")
	public RetMsg<Serializable> insertTtTrdPort(@RequestBody TtTrdPort ttTrdPort) throws RException{
		String mgr = ttTrdPortService.insertTtTrdPort(ttTrdPort);
		if( "000" .equalsIgnoreCase(mgr))
		{
			return RetMsgHelper.ok();
		}else{
			return RetMsgHelper.simple("001", mgr);
		}
	}
	
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTtTrdPort")
	public RetMsg<Serializable> updateTtTrdPort(@RequestBody TtTrdPort ttTrdPort) throws RException{
		ttTrdPortService.updateTtTrdPort(ttTrdPort);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTtTrdPortById")
	public RetMsg<Serializable> deleteTtTrdPortById(@RequestBody String[] portfolio) throws RException{
		ttTrdPortService.deleteTtTrdPortById(portfolio);
		return RetMsgHelper.ok();
	}
}
