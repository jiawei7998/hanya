package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeeHandleCashflow;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统
 * @className 
 * @description TODO
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdFeeHandleCashflowService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdFeeHandleCashflow getFeeHandleCashflowById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdFeeHandleCashflow> getFeeHandleCashflowPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createFeeHandleCashflow(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateFeeHandleCashflow(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteFeeHandleCashflow(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
}
