package com.singlee.capital.trade.mapper;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdProductApproveAmtLine;

public interface TdProductApproveAmtLineMapper extends Mapper<TdProductApproveAmtLine> {
	
	public TdProductApproveAmtLine searchAmtLine(@Param(value = "prd_no")String prd_no, @Param(value = "ccy")String ccy);
	
	public void deleteAllAmtLine (@Param(value = "prd_no")String prd_no);
}
