package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdAttachTreeMap;


/**
 * 
 * @author x230i
 *
 */
public interface TdAttachTreeMapMapper extends Mapper<TdAttachTreeMap> {
	
	/**
	 * 根据指定的关联类型 和关联 id 获得附件列表 
	 * @param params
	 * @return
	 */
	List<TdAttachTreeMap> selectTdAttachTreeMap(Map<String, Object> params);
	
	/**
	 * 根据ID进行树的级联删除
	 * @param treeId
	 */
	int deleteTdAttachTreeMapByIdCascade(@Param("treeId") String treeId);
	
	/**
	 * 
	 */
	int deleteTdAttachTreeMap(@Param("refType") String refType, @Param("refId") String refId);
	
	
	/**
	 * 
	 */
	int clearTdAttachTreeMap();
	
	String idGeneriate();
	
	void insertIncludeId(TdAttachTreeMap attachTreeMap);
}