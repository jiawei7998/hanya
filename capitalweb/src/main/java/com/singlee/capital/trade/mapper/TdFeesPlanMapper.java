package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesPlan;

import tk.mybatis.mapper.common.Mapper;

/**
 * @projectName 同业业务管理系统
 * @className 中间费用持久层
 * @description TODO
 * @author dzy
 * @createDate 2016-10-11 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdFeesPlanMapper extends Mapper<TdFeesPlan> {

	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 中间费用对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdFeesPlan> searchFeesPlan(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public List<TdFeesPlan> searchFeesPlan(Map<String, Object> params);
	
	/**
	 * 删除
	 * @param refNo
	 * @return
	 */
	public void delFeesPlanByRefNo(String refNo);
	
	/**
	 * 删除
	 * @param params
	 */
	public void deleteFeesPlanByDealNo(Map<String, Object> params);
	
	/**
	 * 
	 * @param feePlan
	 */
	public void updateFeePlan(TdFeesPlan feePlan);
	
}