package com.singlee.capital.trade.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdRateChangeMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdRateChange;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.RateChangeService;
import com.singlee.slbpm.service.FlowOpService;

@Service
public class RateChangeServiceImpl extends AbstractDurationService implements RateChangeService {

	@Autowired
	private TdRateChangeMapper tdRateChangeMapper;
	
	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;

	@Autowired
	private FlowOpService flowOpService;
	
	@Autowired
	TdCashflowInterestMapper cashflowInterestMapper;
	
	@Autowired
	TdAmtRangeMapper amtRangeMapper;//实际现金流
	@Autowired
	TdCashflowCapitalMapper capitalMapper;//计划现金流
	@Autowired
	TdRateRangeMapper rangeMapper;//利率区间
	@Autowired
	TdCashflowDailyInterestMapper cashflowDailyInterestMapper;//每日计提
	@Autowired
	BatchDao batchDao;
	@Autowired
	DayendDateService dateService;

	@Override
	public TdRateChange getRateChangeById(String dealNo) {
		return tdRateChangeMapper.getRateChangeById(dealNo);
	}

	@Override
	public Page<TdRateChange> getRateChangePage(Map<String, Object> params, int isFinish) {
		Page<TdRateChange> page= new Page<TdRateChange>();
		if(isFinish == 1){
			page= tdRateChangeMapper.getRateChangeApproveList(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 2){
			page= tdRateChangeMapper.getRateChangeListFinish(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 3){
			page= tdRateChangeMapper.getRateChangeMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdRateChange> list =page.getResult();
		for(TdRateChange rc :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(rc.getRefNo());
			if(main!=null){
				rc.setProduct(main.getProduct());
				rc.setParty(main.getCounterParty());
				rc.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	/**
	 * 创建利率变更数据（order or trade+ratechang）
	 */
	@Override
	@AutoLogMethod(value = "创建利率变更数据")
	public void createRateChange(Map<String, Object> params) {
		tdRateChangeMapper.insert(rateChange(params));
	}

	/**
	 * 修改利率变更数据（order or trade+ratechange）
	 */
	@Override
	@AutoLogMethod(value = "修改利率变更数据")
	public void updateRateChange(Map<String, Object> params) {
		tdRateChangeMapper.updateByPrimaryKey(rateChange(params));
	}
	
	/**
	 * map转换成需要的rateChange对象
	 * @param map
	 * @return
	 */
	public TdRateChange rateChange(Map<String,Object> map){
		TdRateChange rateChange = new TdRateChange();
		try {
			BeanUtil.populate(rateChange, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
    /* // 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdRateChange change = new TdRateChange();
        change.setDealNo(String.valueOf(map.get("dealNo")));
        change.setDealType(rateChange.getDealType());
        change = tdRateChangeMapper.searchRateChange(change);
        
      //审批完成，为交易单添加一条数据
        if(change ==null && DictConstants.DealType.Verify.equals(rateChange.getDealType())) {
        	rateChange.setRefNo(rateChange.getRefNo());
        	rateChange.setVersion(rateChange.getVersion());
        	rateChange.setRate(rateChange.getRate());
        	rateChange.setChangeRate(rateChange.getChangeRate());
        	rateChange.setChangeDate(rateChange.getChangeDate());
        	rateChange.setChangeReason(rateChange.getChangeReason());
        	rateChange.setEffectDate(rateChange.getEffectDate());
            rateChange.setSponsor(rateChange.getSponsor());
            rateChange.setSponInst(rateChange.getSponInst());
        }else{
            rateChange.setSponsor(SlSessionHelper.getUserId());
            rateChange.setSponInst(SlSessionHelper.getInstitutionId());
        }*/
        
        rateChange.setSponsor(SlSessionHelper.getUserId());
        rateChange.setSponInst(SlSessionHelper.getInstitutionId());
        rateChange.setChangeDate(dateService.getSettlementDate());
        rateChange.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		return rateChange;
	}
	

	/**
	 * 删除利率变更数据（order or trade+ratechange）
	 */
	@Override
	public void deleteRateChange(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for(int i=0;i<dealNos.length;i++){
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdRateChangeMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}

	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdRateChange rateChange = new TdRateChange();
		BeanUtil.populate(rateChange, params);
		rateChange = tdRateChangeMapper.searchRateChange(rateChange);
		return productApproveService.getProductApproveActivated(rateChange.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdRateChange rateChange = new TdRateChange();
		String dealNo = ParameterUtil.getString(params, "dealNo", null);
		if(dealNo == null) {
			dealNo = ParameterUtil.getString(params, "trade_id", null);
		}
		rateChange.setDealNo(dealNo);
		rateChange = tdRateChangeMapper.searchRateChange(rateChange);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(rateChange.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(rateChange.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(rateChange.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomRateChange);//利率变更
		tdTrdEvent.setEventTable("TD_RATE_CHANGE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		/**
		 * 获得该交易未发生过收息的计划
		 */
		TdRateChange rateChange = new TdRateChange();
		BeanUtil.populate(rateChange, params);
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("refNo", rateChange.getRefNo());
		paramsMap.put("effectdate", rateChange.getEffectDate());
		TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(rateChange.getRefNo());
		List<TdCashflowInterest> cashflowInterests = this.cashflowInterestMapper.getCashflowInterestsIsNotTrueRecive(paramsMap);
		if(cashflowInterests.size()>0){
			Collections.sort(cashflowInterests);
			
			List<CashflowDailyInterest> tmCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			
			
			//获得利率区间
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
			List<TdRateRange> rateRanges = rangeMapper.getRateRanges(paramsMap);
			InterestRange interestRange = null;
			for(TdRateRange rateRange:rateRanges)
			{
				interestRange = new InterestRange();
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(rateRange.getExecRate());
				interestRange.setStartDate(rateRange.getBeginDate());
				interestRanges.add(interestRange);
			}
			//获得最新的真实还本计划
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(paramsMap);
			List<TdCashflowCapital> capitals = capitalMapper.getCaplitalsBigThanDate(paramsMap);
			int count =0 ; //将本金计划翻译成本金剩余区间
			PrincipalInterval principalInterval = null;double tempAmt =0f;
			if(amtRanges.size()>0){
				Collections.sort(amtRanges);
				for(TdAmtRange amtRange : amtRanges){//先载入已发生的现金流计划
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?productApproveMain.getvDate():amtRanges.get(count-1).getExecDate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(amtRange.getExecDate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.add(tempAmt, amtRange.getExecAmt());
					count++;
				}
				//比对 capitals 是否存在比 amtRanges最大还本日还大的数据
				String maxTempAmtRangeDate = amtRanges.get(amtRanges.size()-1).getExecDate();
				for(TdCashflowCapital capital:capitals)
				{
					try {
						if(PlaningTools.compareDate2(capital.getRepaymentSdate(), maxTempAmtRangeDate, "yyyy-MM-dd"))//前者大于后者
						{
							principalInterval = new PrincipalInterval();
							principalInterval.setStartDate(principalIntervals.get(count-1).getEndDate());
							principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(capital.getRepaymentSdate(), Frequency.DAY, -1));
							principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
							principalIntervals.add(principalInterval);
							tempAmt = PlaningTools.add(tempAmt, capital.getRepaymentSamt());
							count++;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						throw new RException(e);
					}
				}
			}else
			{//无实际现金流还本的直接用计划本金
				Collections.sort(capitals);
				for(CashflowCapital tmCashflowCapital : capitals){
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?productApproveMain.getvDate():capitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);
					tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
					count++;
				}
			}
			for(TdCashflowInterest cashflowInterest:cashflowInterests)//循环未收息的计划，重新计算应收计提金额+每日计提
			{
				try {
					cashflowInterest.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,tmCashflowDailyInterests,
							cashflowInterest.getRefBeginDate(),
							cashflowInterest.getRefEndDate(),
							cashflowInterest.getRefBeginDate(),
							DayCountBasis.equal(cashflowInterest.getDayCounter()))
							);
					cashflowInterest.setExecuteRate(rateChange.getChangeRate());
					//删除区间的每日计提。跟新为最新的每日计提
					paramsMap.put("dealNo", rateChange.getRefNo());
					paramsMap.put("startDate", cashflowInterest.getRefBeginDate());
				    paramsMap.put("endDate", cashflowInterest.getRefEndDate());
				    cashflowDailyInterestMapper.deleteCashflowDailyInterest(paramsMap);
				    //批量插入最新的
				  //利息明细  设置交易关联项
					for(int i=0;i<tmCashflowDailyInterests.size();i++)
					{
						tmCashflowDailyInterests.get(i).setDealNo(productApproveMain.getDealNo());
						tmCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
					}
				    batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", tmCashflowDailyInterests);
					cashflowInterestMapper.updateByPrimaryKey(cashflowInterest);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					throw new RException(e);
				}
			}
			/*//back value
			String postdate=this.dateService.getDayendDate();
			String rateChageVdate = rateChange.getEffectDate();
			//判断是否存在倒起息情况
			try {
				if(PlaningTools.compareDate2(postdate, rateChageVdate, "yyyy-MM-dd")){
					
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RException(e);
			}*/
		}
	}

	@Override
	public Page<TdRateChange> getRateChangeForDealNoPage(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.tdRateChangeMapper.getAllRateChangesByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}

}
