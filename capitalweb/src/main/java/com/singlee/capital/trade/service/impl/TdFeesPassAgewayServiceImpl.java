package com.singlee.capital.trade.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowFeeInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.model.TdCashflowFeeInterest;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TmCashflowInterest;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.mapper.TtDayendDateMapper;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayChangeMapper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper;
import com.singlee.capital.trade.mapper.TdFeesPassAgewayMapper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.mapper.TmFeesPassagewyMapper;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayChange;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdFeesPassAgewayVo;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.model.TmFeesPassagewy;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TradeManageService;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:56:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
public class TdFeesPassAgewayServiceImpl extends AbstractDurationService implements TdFeesPassAgewayService{

	@Autowired
	private TdFeesPassAgewayMapper tdFeesPassAgewayMapper;
	@Autowired
	TcProductMapper productMapper;
	@Autowired
	private TdFeesPassAgewayDailyMapper tdFeesPassAgewayDailyMapper;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;
	
	@Autowired
	TradeManageService tradeManageService;
	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private TmFeesPassagewyMapper tmFeesPassagewyMapper;

	@Autowired
	private BatchDao batchDao;
	
	@Autowired
	private TdCashflowFeeInterestMapper cashflowFeeInterestMapper;

	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;
	@Autowired
	private TtDayendDateMapper dateMapper; 
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	@Autowired
	private TdFeesPassAgewayChangeMapper tdFeesPassAgewayChangeMapper;
	
	@Autowired
	private TdCashflowCapitalMapper cashflowCapitalMapper;
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@Override
    public List<TdFeesPassAgeway> getTdFeesPassAgewayList(Map<String, Object> params)
	{
		return null;
	}
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@Override
    public Page<TdFeesPassAgeway> getTdFeesPassAgewayLists(Map<String, Object> params)
	{
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		return tdFeesPassAgewayMapper.getTdFeesPassAgeways(params, rowBounds);
	}
	
	/**
	 * 根据交易单号查询一条数据
	 * @param dealNo
	 * @return
	 */
	@Override
    public TdFeesPassAgewayChange queryTdFeesPassAgewayById(String dealNo)
	{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("dealNo", dealNo);
		List<TdFeesPassAgewayChange> list=tdFeesPassAgewayChangeMapper.getTdFeesPassAgewaysByDeal(map);
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
		//return tdFeesPassAgewayChangeMapper.selectByPrimaryKey(dealNo);
	}

	@Override
	public void createTempPassageWay(HashMap<String, Object> map) {
		// map转实体类
		TdFeesPassAgewayChange change = new TdFeesPassAgewayChange();
		try {
			BeanUtil.populate(change, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		Object obj = change.getTdFeesPassAgeways();
		List<TmFeesPassagewy> tmFeesPassagewys = FastJsonUtil.parseArrays(obj.toString(), TmFeesPassagewy.class);
		TdProductApproveMain productApproveMain = productApproveMainMapper.getProductApproveMainActivated(change.getRefNo());
		for (TmFeesPassagewy tmFeesPassagewy : tmFeesPassagewys) 
		{
			tmFeesPassagewy.setDealNo(change.getDealNo());
			tmFeesPassagewy.setRefNo(change.getRefNo());
			tmFeesPassagewy.setVersion(String.valueOf(productApproveMain.getVersion()));
			tmFeesPassagewyMapper.insert(tmFeesPassagewy);
		}		
		change.setSponsor(SlSessionHelper.getUser().getUserId());
		change.setSponInst(SlSessionHelper.getInstitution().getInstId());
		change.setCfType("2");
		change.setVersion(String.valueOf(productApproveMain.getVersion()));
		change.setDealType("2");
		change.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tdFeesPassAgewayChangeMapper.addFeeChange(change);		
	}

	@Override
	public void updateTempPassageWay(HashMap<String, Object> map) {
		// map转实体类
		TdFeesPassAgewayChange change = new TdFeesPassAgewayChange();
		try {
			BeanUtil.populate(change, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		List<String> list = new ArrayList<String>();
		list.add(change.getDealNo());
		tmFeesPassagewyMapper.deleteByDealNo(list);
		
		Object obj = change.getTdFeesPassAgeways();
		List<TmFeesPassagewy> tmFeesPassagewys = FastJsonUtil.parseArrays(obj.toString(), TmFeesPassagewy.class);
		TdProductApproveMain productApproveMain = productApproveMainMapper.getProductApproveMainActivated(change.getRefNo());
		for (TmFeesPassagewy tmFeesPassagewy : tmFeesPassagewys) {
			tmFeesPassagewy.setDealNo(change.getDealNo());
			tmFeesPassagewy.setRefNo(change.getRefNo());
			tmFeesPassagewy.setVersion(String.valueOf(productApproveMain.getVersion()));
			tmFeesPassagewyMapper.insert(tmFeesPassagewy);
		}		
		change.setSponsor(SlSessionHelper.getUser().getUserId());
		change.setSponInst(SlSessionHelper.getInstitution().getInstId());
		change.setCfType("2");
		change.setVersion(String.valueOf(productApproveMain.getVersion()));
		change.setDealType("2");
		change.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tdFeesPassAgewayChangeMapper.updateByPrimaryKey(change);	
	}

	@Override
	public Page<TdFeesPassAgewayChange> getPassageWayPage(Map<String, Object> params, int isFinished) {
		Page<TdFeesPassAgewayChange> page= new Page<TdFeesPassAgewayChange>();
		if(isFinished == 1){
			page=tdFeesPassAgewayChangeMapper.getPassageWayList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=tdFeesPassAgewayChangeMapper.getPassageWayListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {
			page=tdFeesPassAgewayChangeMapper.getPassageWayListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		List<TdFeesPassAgewayChange> list =page.getResult();
		for(TdFeesPassAgewayChange tm :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(tm.getRefNo());
			if(main!=null){
				tm.setPrdName(main.getProductName());
				tm.setProduct(main.getProduct());
				tm.setParty(main.getCounterParty());
				tm.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	public void deletePassageWay(String[] dealNos) {
		Map<String,Object> maps = new HashMap<String, Object>();
		maps.put("dealNos", dealNos);
		List<TmFeesPassagewy> tms = tmFeesPassagewyMapper.getByPrimaryKey(maps);
		List<String> list=new ArrayList<String>();
		if(tms!=null&&tms.size()>0){
			for (TmFeesPassagewy tm : tms) {
				list.add(tm.getDealNo());
			}
		}
		List<String> list2 = Arrays.asList(dealNos);
		tdFeesPassAgewayChangeMapper.deleteByDealNo(list2);
		tmFeesPassagewyMapper.deleteByDealNo(list);
		/*TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			tdFeesPassAgewayMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}*/
	}

	@Override
	public List<TmFeesPassagewy> getPassageWayByDealNo(
			Map<String, Object> params) {
		//return tdFeesPassAgewayMapper.getTdFeesPassAgewaysByDeal(params);
		List<TmFeesPassagewy> list = tmFeesPassagewyMapper.getTmFeesPassagewys(params);
		return list;
		//return tmFeesPassagewyMapper.getTmFeesPassagewysByChangeNo(params);
	}
	@Override
	public List<TdFeesPassAgeway> getTdPassageWayByDealNo(
			Map<String, Object> params) {
		return tdFeesPassAgewayMapper.getTdFeesPassAgewaysByDeal(params);
		//return tmFeesPassagewyMapper.getTmFeesPassagewysByChangeNo(params);
	}
	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		TdFeesPassAgewayChange feesPassAgewayChange = new TdFeesPassAgewayChange();
		BeanUtil.populate(feesPassAgewayChange, params);
		feesPassAgewayChange = tdFeesPassAgewayChangeMapper.searchPassageWayChange(feesPassAgewayChange);
		return productApproveService.getProductApproveActivated(feesPassAgewayChange.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		params.put("dealNo", params.get("trade_id"));
		List<TmFeesPassagewy> tms = tmFeesPassagewyMapper.getTmFeesPassagewysByChangeNo(params);
		if(tms!=null&&tms.size()>0){
			//TdFeesPassAgeway feesPassAgeway = new TdFeesPassAgeway();
			//feesPassAgeway.setDealNo(ParameterUtil.getString(params, "trade_id", ""));
			//feesPassAgeway = tdFeesPassAgewayMapper.selectOne(feesPassAgeway);
			
			TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
			tdTrdEvent.setDealNo(tms.get(0).getRefNo());//原交易流水号
			tdTrdEvent.setVersion(Integer.parseInt(tms.get(0).getVersion()) );//原交易版本
			tdTrdEvent.setEventRefno(tms.get(0).getDealNo());//将利率变更的交易流水进行匹配赋值
			tdTrdEvent.setEventType(DictConstants.TrdType.CustomPassageWay);//利率变更
			tdTrdEvent.setEventTable("td_fees_passageway");
			tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
			tdTrdEventMapper.insert(tdTrdEvent);
		}		
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		try {
			// map转实体类
			TdFeesPassAgewayChange change = new TdFeesPassAgewayChange();
			try {
				BeanUtil.populate(change, params);
			} catch (Exception e) {
				JY.raise(e.getMessage());
			}
			
			//流程通过的实现
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("refNo", change.getRefNo());
			List<TdFeesPassAgeway> orgFeesList = tdFeesPassAgewayMapper.getTdFeesListByRef(map);
			
			HashMap<String, TdFeesPassAgeway> orgPassDelMap = new HashMap<String, TdFeesPassAgeway>();
			for (TdFeesPassAgeway tdFeesPassAgeway : orgFeesList) 
			{
				orgPassDelMap.put(tdFeesPassAgeway.getPassagewayCno(), tdFeesPassAgeway);
			}
			
			map.put("dealNo", change.getDealNo());
			List<TmFeesPassagewy> list = tmFeesPassagewyMapper.getTmFeesPassagewysByChangeNo(map);
			//审批列表中有说明通道没被删除 map中剩下为本次删除的通道
			for (TmFeesPassagewy tmFeesPassagewy : list) {
				orgPassDelMap.remove(tmFeesPassagewy.getPassagewayCno());
			}
			
			//生效日期
			String effdate = change.getEffectDate();
			if(effdate == null){
				effdate = dateMapper.selecteTtDayendDate().get(0).getCurDate();
			}
			
			TdProductApproveMain activeDeal = productApproveMainMapper.getProductApproveMainActivated(change.getRefNo());
			
			Map<String, Object> params_1 = new HashMap<String, Object>();
			params_1.put("refNo", activeDeal.getDealNo());
			List<CashflowCapital> cashflowCapitals = cashflowCapitalMapper.getCapitalListByRef(params_1);
			Collections.sort(cashflowCapitals);
			String endDate = null;
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
			PrincipalInterval principalInterval=null;
			double tempAmt = 0f;
			int count = 0 ; //将本金计划翻译成本金剩余区间
			HashMap<String, PrincipalInterval> prHashMap = new HashMap<String, PrincipalInterval>();
			for(CashflowCapital tmCashflowCapital : cashflowCapitals)
			{
				endDate = PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1);
				if(prHashMap.get(endDate) != null)
				{
					prHashMap.get(endDate).setResidual_Principal(PlaningTools.sub(activeDeal.getAmt(), tempAmt));
					
				}else{
					principalInterval = new PrincipalInterval();
					principalInterval.setStartDate(count==0?activeDeal.getvDate():cashflowCapitals.get(count-1).getRepaymentSdate());
					principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
					principalInterval.setResidual_Principal(PlaningTools.sub(activeDeal.getAmt(), tempAmt));
					principalIntervals.add(principalInterval);

					prHashMap.put(principalInterval.getEndDate(), principalInterval);
				}
				
				tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
				count++;
			}
			
			HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
			//重新计算通道计划 每日通道计提利息
			try {
				TdFeesPassAgeway fee = null;
				String addFlag = null;
				for(TmFeesPassagewy tmFeesPassagewy : list) 
				{
					fee = new TdFeesPassAgeway();
					fee.setRefNo(tmFeesPassagewy.getRefNo());
					fee.setFeeBasis(tmFeesPassagewy.getFeeBasis());
					fee.setIsPass(tmFeesPassagewy.getIsPass());
					fee.setFeeCcy(tmFeesPassagewy.getFeeCcy());
					fee.setFeeType(tmFeesPassagewy.getFeeType());
					fee.setFeeSituation(tmFeesPassagewy.getFeeSituation());
					fee.setVersion(tmFeesPassagewy.getVersion());
					fee.setFeeRate(tmFeesPassagewy.getFeeRate());
					fee.setCustType(tmFeesPassagewy.getCustType());
					fee.setPassagewayCno(tmFeesPassagewy.getPassagewayCno());
					fee.setPassagewayName(tmFeesPassagewy.getPassagewayName());
					fee.setBigKind(tmFeesPassagewy.getBigKind());
					fee.setMidKind(tmFeesPassagewy.getMidKind());
					fee.setSmallKind(tmFeesPassagewy.getSmallKind());
					fee.setCashtDate(tmFeesPassagewy.getCashtDate());
					fee.setContact(tmFeesPassagewy.getContact());
					fee.setContactAddr(tmFeesPassagewy.getContactAddr());
					fee.setContactPhone(tmFeesPassagewy.getContactPhone());
					fee.setIntFre(tmFeesPassagewy.getIntFre());
					fee.setPassagewayBankacccode(tmFeesPassagewy.getPassagewayBankacccode());
					fee.setPassagewayBankaccname(tmFeesPassagewy.getPassagewayBankaccname());
					fee.setPassagewayBankcode(tmFeesPassagewy.getPassagewayBankcode());
					fee.setPassagewayBankname(tmFeesPassagewy.getPassagewayBankname());
					fee.setRemark(tmFeesPassagewy.getRemark());
					fee.setVersion(String.valueOf(activeDeal.getVersion()));
					
					addFlag = orgPassDelMap.get(tmFeesPassagewy.getPassagewayCno()) != null ? "del" : "add";
					
					reCalPassCashFlow(principalIntervals, fee, activeDeal, passAwayMap, effdate, addFlag);
				}
				
				//处理删除的通道
				for (TdFeesPassAgeway tdFeesPassAgeway : orgPassDelMap.values()) 
				{
					reCalPassCashFlow(principalIntervals, tdFeesPassAgeway, activeDeal, passAwayMap, effdate, "del");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				JY.error("重新计算通道计划错误", e);
			}
			
			params_1.put("startDate", effdate);
			List<TdCashflowInterest> cashflowInterests = cashflowInterestMapper.geTdCashFlowIntByDate(params_1);
			if(cashflowInterests == null || cashflowInterests.size() <= 0){
				return;
			}
			/*
			 * 循环重算收息计划之间的利息
			 */
			Collections.sort(cashflowInterests);
			
			params.put("refNo",change.getRefNo());
			params.put("refBeginDate", cashflowInterests.get(0).getRefBeginDate());
			params.put("intCal", "1");
			List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
			
			//重新计算收息计划
			try {
				//dealNo  startDate
				params.put("dealNo", activeDeal.getDealNo());
				params.put("startDate", cashflowInterests.get(0).getRefBeginDate());
				List<CashflowDailyInterest> cashflowDailyInterests = cashflowDailyInterestMapper.selectByDealNoAndDate(params);
				
				for(int i = 0;i < cashflowInterests.size();i++)
				{
					PlaningTools.fun_reset_intPlan_except_fess(principalIntervals, cashflowDailyInterests, 
							cashflowInterests.get(i), cashflowInterests.get(0).getRefBeginDate(), DayCountBasis.equal(activeDeal.getBasis()), 
							fAgewayDailies);
				}
				batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper.updateByPrimaryKey", cashflowInterests);
			} catch (Exception e) {
				//重新计算收息计划
				JY.error("重新计算收息计划错误", e);
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("保存通道计划错误", e);
			
		}
	}

	@Override
	public BigDecimal createAllCashFlowsForSaveApprove(List<PrincipalInterval> principalIntervals,List<InterestRange> interestRanges, Map<String, Object> params, TdFeesPassAgeway passagewy,HashMap<String, TdFeesPassAgewayDaily> passAwayMap) {
		BigDecimal interest = new BigDecimal(0);
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		List<CashflowInterest> pList = new ArrayList<CashflowInterest>();//返回的收息计划
		List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		TdFeesPassAgewayDaily fees = null;
		try{
			productApproveMain = productApproveMainMapper.getProductApproveMainActivated(String.valueOf(params.get("dealNo")));
			int basicType = DictConstants.DayCounter.Actual360.equals(passagewy.getFeeBasis()) 
					? 1 : DictConstants.DayCounter.Actual365.equals(passagewy.getFeeBasis()) ? 2 : 3;
			
			/**
			 * 判定利随本清模式，如果利随本清，那么只需要统一计算即可；
			 */
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(passagewy.getIntFre()))==com.singlee.capital.base.cal.Frequency.ONCE)
			{
				PlaningTools.calPlanSchedule(interestRanges,principalIntervals,detailSchedules,pList,
					productApproveMain.getvDate(),productApproveMain.getmDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
					productApproveMain.getAmt(),passagewy.getFeeRate(),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(passagewy.getIntFre())),DayCountBasis.valueOf(basicType),0);
			}else
			{
				//如果费用存在 扣划日日期，则 扣划日期 为第一个付息日期
				if(passagewy.getCashtDate() != null && !"".equals(passagewy.getCashtDate()))
				{
					//如果小于起息日
					if(PlaningTools.compareDate2(productApproveMain.getvDate(), passagewy.getCashtDate(), "yyyy-MM-dd"))
					{
						productApproveMain.setsDate(productApproveMain.getvDate());
					
					//如果大于到期日
					}else if(PlaningTools.compareDate2(passagewy.getCashtDate(),productApproveMain.getmDate(),  "yyyy-MM-dd")){
						
						productApproveMain.setsDate(productApproveMain.getmDate());
					}else{
						
						productApproveMain.setsDate(passagewy.getCashtDate());
					}
				}
				
				TmCashflowInterest fistPlanSchedule  = new TmCashflowInterest();//假设存在第一次付息日 与 最后一次付息日的设定，那么需要分段进行计息及周期判定
				//判断第一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.getvDate(), productApproveMain.getsDate(), "yyyy-MM-dd")){
					fistPlanSchedule.setSeqNumber(1);
					fistPlanSchedule.setRefBeginDate(productApproveMain.getvDate());
					fistPlanSchedule.setRefEndDate(productApproveMain.getsDate());
					fistPlanSchedule.setTheoryPaymentDate(productApproveMain.getsDate());
					fistPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,detailSchedules,fistPlanSchedule.getRefBeginDate(),fistPlanSchedule.getRefEndDate(),productApproveMain.getvDate(),DayCountBasis.valueOf(basicType)));
					fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					fistPlanSchedule.setExecuteRate(passagewy.getFeeRate());
					pList.add(fistPlanSchedule);
				}
					
				PlaningTools.calPlanSchedule(interestRanges,principalIntervals,detailSchedules,pList,
					productApproveMain.getsDate(),productApproveMain.geteDate(),productApproveMain.getvDate(),//计算第一次与最后一次付息日之间的设定
					productApproveMain.getAmt(),Double.valueOf(passagewy.getFeeRate()),com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(passagewy.getIntFre())),DayCountBasis.valueOf(basicType),productApproveMain.getnDays());
				
				//最后一次付息日-到期日  判断最后一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(productApproveMain.geteDate(), productApproveMain.getmDate(), "yyyy-MM-dd")){
					TmCashflowInterest lastPlanSchedule  = new TmCashflowInterest();
					lastPlanSchedule.setSeqNumber(pList.size()+1);
					lastPlanSchedule.setRefBeginDate(productApproveMain.geteDate());
					lastPlanSchedule.setRefEndDate(productApproveMain.getmDate());
					lastPlanSchedule.setTheoryPaymentDate(productApproveMain.getmDate());
					lastPlanSchedule.setInterestAmt(PlaningTools.fun_Calculation_interval(interestRanges,principalIntervals,detailSchedules,lastPlanSchedule.getRefBeginDate(),lastPlanSchedule.getRefEndDate(),productApproveMain.getvDate(),DayCountBasis.valueOf(basicType)));
					lastPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					lastPlanSchedule.setExecuteRate(passagewy.getFeeRate());
					pList.add(lastPlanSchedule);
				}
			}
			
			TdCashflowFeeInterest tcfi = null;
			for(int i = 0; i < pList.size(); i++) 
			{	
				tcfi = new TdCashflowFeeInterest();
				tcfi.setFeeCustName(passagewy.getPassagewayName());
				tcfi.setFeeCustNo(passagewy.getPassagewayCno());
				tcfi.setDealNo(productApproveMain.getDealNo());
				tcfi.setCfType(passagewy.getFeeType());
				tcfi.setDayCounter(pList.get(i).getDayCounter());
				tcfi.setActuralRate(pList.get(i).getActuralRate());
				tcfi.setRefBeginDate(pList.get(i).getRefBeginDate());
				tcfi.setRefEndDate(pList.get(i).getRefEndDate());
				tcfi.setInterestAmt(pList.get(i).getInterestAmt());
				tcfi.setExecuteRate(pList.get(i).getExecuteRate());
				tcfi.setSeqNumber(pList.get(i).getSeqNumber());
				tcfi.setTheoryPaymentDate(pList.get(i).getTheoryPaymentDate());
				tcfi.setPayDirection(passagewy.getFeeSituation());
				cashflowFeeInterestMapper.insert(tcfi);
			}
			
			List<TdFeesPassAgewayDaily> list = new ArrayList<TdFeesPassAgewayDaily>();
			TdFeesPassAgewayDaily agewayDaily = null;
			for(int i = 0 ; i< detailSchedules.size();i++)
			{
				fees = new TdFeesPassAgewayDaily();
				fees.setRefNo(passagewy.getRefNo());
				fees.setCno(passagewy.getPassagewayCno());
				fees.setVersion(productApproveMain.getVersion());
				fees.setActualRate(passagewy.getFeeRate());
				fees.setDayCounter(passagewy.getFeeBasis());
				fees.setInterest(detailSchedules.get(i).getInterest());
				fees.setPrincipal(detailSchedules.get(i).getPrincipal());
				fees.setRefBeginDate(detailSchedules.get(i).getRefBeginDate());
				fees.setRefEndDate(detailSchedules.get(i).getRefEndDate());
				fees.setIntCal(passagewy.getIsPass());
				list.add(fees);
				
				//参与计算的付费计划需要加入map中，用于冲减计提
				if("1".equals(passagewy.getIsPass()) && "Pay".equals(passagewy.getFeeSituation())){
					interest = interest.add(new BigDecimal(detailSchedules.get(i).getInterest()));
					agewayDaily = passAwayMap.get(detailSchedules.get(i).getRefBeginDate());
					if(agewayDaily == null)
					{
						passAwayMap.put(detailSchedules.get(i).getRefBeginDate(), fees);
					}else{
						agewayDaily.setInterest(PlaningTools.add(agewayDaily.getInterest(), fees.getInterest()));
					}
				}
				
			}
			batchDao.batch("com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper.insert", list);
		} catch (Exception e) {
			e.printStackTrace();
			JY.error(productApproveMain.getDealNo(), e);
			JY.raise(e.getMessage());
		}
		return  interest;	
	}
	

	@Override
	public List<TdFeesPassAgewayDaily> getFeesDailyInterestList(
			Map<String, Object> params) {
		List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayDailyMapper.getFeesDailyInterestList(params);
		return fAgewayDailies;
	}

	@Override
	public List<TdFeesPassAgeway> getPassageWayByEndDate(Map<String, Object> params) {
		return tdFeesPassAgewayMapper.getPassageWayByEndDate(params);
	}

	@Override
	public List<TdFeesPassAgewayVo> getTdFeesPassAgewayListVo(Map<String, Object> params) {
		return tdFeesPassAgewayMapper.getTdFeesPassAgewayListVo(params);
	}

	@Override
	public BigDecimal createPassWayFeesPlan(TdProductApproveMain productApproveMain,Map<String, Object> params,
			HashMap<String, TdFeesPassAgewayDaily> passAwayMap,List<PrincipalInterval> principalIntervals,
			List<TdFeesPassAgeway> feesPassagewyList) throws Exception {
		BigDecimal cash = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);
		tdFeesPassAgewayMapper.deleteByDealNoAndCust(params);
		cashflowFeeInterestMapper.deleteInterestList(params);
		tdFeesPassAgewayDailyMapper.deleteByDealNoAndCust(params);
		
		List<InterestRange> interestRangesFees = new ArrayList<InterestRange>();
		InterestRange temp = new InterestRange();
		temp.setStartDate(productApproveMain.getvDate());
		temp.setEndDate(productApproveMain.getmDate());
		
		interestRangesFees.add(temp);
		
		for (TdFeesPassAgeway feesPassagewy : feesPassagewyList) 
		{
			feesPassagewy.setRefNo(productApproveMain.getDealNo());
			tdFeesPassAgewayMapper.insert(feesPassagewy);//保存通道计划的计提利息
			//1定期 2活期  活期业务无预置现金流(目前只有存放同业活期)
			if(!StringUtils.equalsIgnoreCase(String.valueOf(productApproveMain.getProduct().getPrdTerm()), "2")){
				try {
					temp.setRate(feesPassagewy.getFeeRate());
					
					cash = createAllCashFlowsForSaveApprove(principalIntervals, interestRangesFees,params,feesPassagewy,passAwayMap);					
					total = total.add(cash);
				} catch (Exception e) {
					throw e;
				}
			}
		}
		return total;
	}
	
	//根据通道频率生成计划
	private List<CashflowInterest>  createPassCashFlowPlan(TdFeesPassAgeway passagewy,String vdate,String mdate,int nDays)throws Exception{
		int basicType = DictConstants.DayCounter.Actual360.equals(passagewy.getFeeBasis()) 
				? 1 : DictConstants.DayCounter.Actual365.equals(passagewy.getFeeBasis()) ? 2 : 3;
		
		Frequency frequency = com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(passagewy.getIntFre()));
		
		List<CashflowInterest> cashflowInterests = new ArrayList<CashflowInterest>();
		TmCashflowInterest fistPlanSchedule = null;
		try {
			if(com.singlee.capital.base.cal.Frequency.valueOf(Integer.parseInt(passagewy.getIntFre())) != com.singlee.capital.base.cal.Frequency.ONCE)
			{
				if(passagewy.getCashtDate() == null){
					passagewy.setCashtDate(vdate);
				}else{
					if(PlaningTools.compareDate2(vdate,passagewy.getCashtDate(),"yyyy-MM-dd"))
					{
						String tempDate = passagewy.getCashtDate();
						while(true){
							tempDate = PlaningTools.addOrSubMonth_Day(tempDate, frequency, 1);
							if(PlaningTools.compareDate(tempDate, vdate, "yyyy-MM-dd"))
							{
								break;
							}
						}// end while
						if(tempDate != null && tempDate.compareTo(mdate) <= 0){
							passagewy.setCashtDate(tempDate);
						}else{
							passagewy.setCashtDate(vdate);
						}
						
					}
				}
				
				fistPlanSchedule = new TmCashflowInterest();
				//判断第一次付息日与起息日如果相等，那么需要排除
				if(!PlaningTools.compareDate3(passagewy.getCashtDate(), vdate, "yyyy-MM-dd"))
				{
					fistPlanSchedule.setSeqNumber(1);
					fistPlanSchedule.setRefBeginDate(vdate);
					fistPlanSchedule.setRefEndDate(passagewy.getCashtDate());
					fistPlanSchedule.setTheoryPaymentDate(passagewy.getCashtDate());
					
					fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
					fistPlanSchedule.setExecuteRate(passagewy.getFeeRate());
					cashflowInterests.add(fistPlanSchedule);
				}
				
				PlaningTools.calPlanScheduleRange(cashflowInterests, passagewy.getCashtDate(), mdate,
						passagewy.getCashtDate(), passagewy.getFeeRate(), frequency, DayCountBasis.valueOf(basicType), nDays);
				
			}else{
				fistPlanSchedule = new TmCashflowInterest();
				fistPlanSchedule.setSeqNumber(1);
				fistPlanSchedule.setRefBeginDate(vdate);
				fistPlanSchedule.setRefEndDate(mdate);
				fistPlanSchedule.setTheoryPaymentDate(mdate);
				fistPlanSchedule.setDayCounter(DayCountBasis.valueOf(basicType).getMolecule());
				fistPlanSchedule.setExecuteRate(passagewy.getFeeRate());
				cashflowInterests.add(fistPlanSchedule);
			}
			return cashflowInterests;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Override
	public BigDecimal reCalPassCashFlow(List<PrincipalInterval> principalIntervals,TdFeesPassAgeway tdFeesPassAgeway,TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap,String effdate,String addFlag) throws Exception{
		BigDecimal interest = new BigDecimal(0);
		try{
			//获取区间包含 effdate 费用计划
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("feeCustNo", tdFeesPassAgeway.getPassagewayCno());
			List<TdCashflowFeeInterest> cashflowInterestsOld = cashflowFeeInterestMapper.getFeeInterestByGlEDate(map);
			
			String vdate1 = null;
			if(cashflowInterestsOld != null && cashflowInterestsOld.size() > 0){
				for (TdCashflowFeeInterest td : cashflowInterestsOld) 
				{
					if(td.getRefBeginDate().compareTo(effdate) <= 0 && td.getRefEndDate().compareTo(effdate) > 0){
						vdate1 = td.getRefBeginDate();
					}
				}
				if(vdate1 == null){
					//如果小于通道计划最小日期 起始日等于最小日期
					if(effdate.compareTo(cashflowInterestsOld.get(0).getRefBeginDate()) < 0){
						vdate1 = cashflowInterestsOld.get(0).getRefBeginDate();
						
					}else if(effdate.compareTo(cashflowInterestsOld.get(cashflowInterestsOld.size() - 1).getRefEndDate()) >= 0){
						vdate1 = cashflowInterestsOld.get(cashflowInterestsOld.size() - 1).getRefEndDate();
					}
				}
				
			}else{
				vdate1 = effdate;
			}
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			//dealNo cno
			params.put("dealNo", productApproveMain.getDealNo());
			params.put("cno", tdFeesPassAgeway.getPassagewayCno());
			List<TdFeesPassAgeway> orgTdFeesPassAgeways = tdFeesPassAgewayMapper.getTdPassageWayByRefNo(params);
			TdFeesPassAgeway orgFeePass = null;
			if(orgTdFeesPassAgeways != null && orgTdFeesPassAgeways.size() > 0){
				orgFeePass = orgTdFeesPassAgeways.get(0);
				
				tdFeesPassAgewayMapper.deleteByDealNoAndCust(params);
			}
			
			//dealNo endDate feeCustNo
			params.put("dealNo", productApproveMain.getDealNo());
			params.put("endDate", vdate1);
			params.put("feeCustNo", tdFeesPassAgeway.getPassagewayCno());
			cashflowFeeInterestMapper.deleteInterestList(params);
			
			//refNo effdate
			params.put("refNo", productApproveMain.getDealNo());
			params.put("effdate", vdate1);
			tdFeesPassAgewayDailyMapper.deleteByMap(params);
			
			//是否是删除的通道 如果是删除的通道,则重新生成通道计划 区间为本区间起始日期 到删除生效日
			if("del".equals(addFlag)){
				productApproveMain.setmDate(effdate);
			}else{
				tdFeesPassAgewayMapper.insert(tdFeesPassAgeway);
			}
			
			if(vdate1 != null && vdate1.compareTo(productApproveMain.getmDate()) >= 0){
				return interest;
			}
			List<CashflowInterest> cashflowInterests = createPassCashFlowPlan(tdFeesPassAgeway, vdate1, productApproveMain.getmDate(), productApproveMain.getnDays());
			
			InterestRange interestRange = null;
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
			//生效日期大于 本区间起息日则需要 添加此区间利率
			if(PlaningTools.compareDate2(effdate, vdate1, "yyyy-MM-dd")){
				interestRange = new InterestRange();
				interestRange.setStartDate(vdate1);
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(effdate, Frequency.DAY, -1));
				interestRange.setRate(cashflowInterestsOld.get(0).getExecuteRate());
				interestRanges.add(interestRange);
			}
			
			interestRange = new InterestRange();
			interestRange.setStartDate(effdate);
			interestRange.setEndDate(productApproveMain.getmDate());
			interestRange.setRate(tdFeesPassAgeway.getFeeRate());
			interestRanges.add(interestRange);
			
			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			for(int i = 0;i < cashflowInterests.size();i++)
			{	
				cashflowInterests.get(i).setInterestAmt(
						PlaningTools.fun_Calculation_interval(interestRanges, 
							principalIntervals, 
							detailSchedules, 
							cashflowInterests.get(i).getRefBeginDate(), 
							cashflowInterests.get(i).getRefEndDate(),
							vdate1,
							DayCountBasis.equal(tdFeesPassAgeway.getFeeBasis())));
			}
			
			TdCashflowFeeInterest tcfi = null;
			for(int i = 0; i < cashflowInterests.size(); i++) 
			{	
				tcfi = new TdCashflowFeeInterest();
				tcfi.setFeeCustName(tdFeesPassAgeway.getPassagewayName());
				tcfi.setFeeCustNo(tdFeesPassAgeway.getPassagewayCno());
				tcfi.setDealNo(productApproveMain.getDealNo());
				tcfi.setCfType(tdFeesPassAgeway.getFeeType());
				tcfi.setDayCounter(cashflowInterests.get(i).getDayCounter());
				tcfi.setActuralRate(cashflowInterests.get(i).getActuralRate());
				tcfi.setRefBeginDate(cashflowInterests.get(i).getRefBeginDate());
				tcfi.setRefEndDate(cashflowInterests.get(i).getRefEndDate());
				tcfi.setInterestAmt(cashflowInterests.get(i).getInterestAmt());
				tcfi.setExecuteRate(cashflowInterests.get(i).getExecuteRate());
				tcfi.setSeqNumber(cashflowInterests.get(i).getSeqNumber());
				tcfi.setTheoryPaymentDate(cashflowInterests.get(i).getTheoryPaymentDate());
				tcfi.setPayDirection(tdFeesPassAgeway.getFeeSituation());
				cashflowFeeInterestMapper.insert(tcfi);
			}
			
			List<TdFeesPassAgewayDaily> passAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
			TdFeesPassAgewayDaily agewayDaily = null;
			TdFeesPassAgewayDaily fees = null;
			for(int i = 0 ; i< detailSchedules.size();i++)
			{
				fees = new TdFeesPassAgewayDaily();
				fees.setRefNo(tdFeesPassAgeway.getRefNo());
				fees.setCno(tdFeesPassAgeway.getPassagewayCno());
				fees.setVersion(productApproveMain.getVersion());
				fees.setActualRate(tdFeesPassAgeway.getFeeRate());
				fees.setDayCounter(tdFeesPassAgeway.getFeeBasis());
				fees.setInterest(detailSchedules.get(i).getInterest());
				fees.setPrincipal(detailSchedules.get(i).getPrincipal());
				fees.setRefBeginDate(detailSchedules.get(i).getRefBeginDate());
				fees.setRefEndDate(detailSchedules.get(i).getRefEndDate());
				//生效日之前用原来的参与计算方式 之后用新的参与计算方式
				if(PlaningTools.compareDate2(effdate, detailSchedules.get(i).getRefBeginDate(), "yyyy-MM-dd") && orgFeePass != null){
					fees.setIntCal(orgFeePass.getIsPass());
				}else{
					fees.setIntCal(tdFeesPassAgeway.getIsPass());
				}
				
				passAgewayDailies.add(fees);
				
				//参与计算的付费计划需要加入map中，用于冲减计提
				if("1".equals(fees.getIntCal()) && "Pay".equals(tdFeesPassAgeway.getFeeSituation()))
				{
					interest = interest.add(new BigDecimal(detailSchedules.get(i).getInterest()));
					agewayDaily = passAwayMap.get(detailSchedules.get(i).getRefBeginDate());
					if(agewayDaily == null)
					{
						passAwayMap.put(detailSchedules.get(i).getRefBeginDate(), fees);
					}else{
						agewayDaily.setInterest(PlaningTools.add(agewayDaily.getInterest(), fees.getInterest()));
					}
				}
			}
			batchDao.batch("com.singlee.capital.trade.mapper.TdFeesPassAgewayDailyMapper.insert", passAgewayDailies);
		} catch (Exception e) {
			e.printStackTrace();
			JY.error(productApproveMain.getDealNo(), e);
			JY.raise(e.getMessage());
		}
		return  interest;	
	}

	@Override
	public BigDecimal calPassDailyCashFlowForTemp(List<PrincipalInterval> principalIntervals,TdFeesPassAgeway tdFeesPassAgeway,
			TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap, String effdate)throws Exception {
		BigDecimal interest = new BigDecimal(0);
		try{
			//获取区间包含 effdate 费用计划
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("feeCustNo", tdFeesPassAgeway.getPassagewayCno());
			map.put("effdate", effdate);
			List<TdCashflowFeeInterest> cashflowInterestsOld = cashflowFeeInterestMapper.getFeeInterestByGlEDate(map);
			
			String vdate1 = null;
			if(cashflowInterestsOld != null && cashflowInterestsOld.size() > 0){
				vdate1 = cashflowInterestsOld.get(0).getRefBeginDate();
				
			}else{
				vdate1 = effdate;
			}
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			//dealNo cno
			params.put("dealNo", productApproveMain.getDealNo());
			params.put("cno", tdFeesPassAgeway.getPassagewayCno());
			List<TdFeesPassAgeway> orgTdFeesPassAgeways = tdFeesPassAgewayMapper.getTdPassageWayByRefNo(map);
			TdFeesPassAgeway orgFeePass = null;
			if(orgTdFeesPassAgeways != null && orgTdFeesPassAgeways.size() > 0){
				orgFeePass = orgTdFeesPassAgeways.get(0);
			}
			
			List<CashflowInterest> cashflowInterests = createPassCashFlowPlan(tdFeesPassAgeway, vdate1, productApproveMain.getmDate(), productApproveMain.getnDays());
			
			InterestRange interestRange = null;
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
			//生效日期大于 本区间起息日则需要 添加此区间利率
			if(PlaningTools.compareDate2(effdate, vdate1, "yyyy-MM-dd")){
				interestRange = new InterestRange();
				interestRange.setStartDate(vdate1);
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(effdate, Frequency.DAY, -1));
				interestRange.setRate(cashflowInterestsOld.get(0).getExecuteRate());
				interestRanges.add(interestRange);
			}
			
			interestRange = new InterestRange();
			interestRange.setStartDate(effdate);
			interestRange.setEndDate(productApproveMain.getmDate());
			interestRange.setRate(tdFeesPassAgeway.getFeeRate());
			interestRanges.add(interestRange);
			
			List<CashflowDailyInterest> detailSchedules = new ArrayList<CashflowDailyInterest>();//返回的每日计提
			for(int i = 0;i < cashflowInterests.size();i++)
			{	
				cashflowInterests.get(i).setInterestAmt(
						PlaningTools.fun_Calculation_interval(interestRanges, 
							principalIntervals, 
							detailSchedules, 
							cashflowInterests.get(i).getRefBeginDate(), 
							cashflowInterests.get(i).getRefEndDate(),
							vdate1,
							DayCountBasis.equal(tdFeesPassAgeway.getFeeBasis())));
			}
			
			TdFeesPassAgewayDaily agewayDaily = null;
			TdFeesPassAgewayDaily fees = null;
			for(int i = 0 ; i< detailSchedules.size();i++)
			{
				fees = new TdFeesPassAgewayDaily();
				fees.setRefNo(tdFeesPassAgeway.getRefNo());
				fees.setCno(tdFeesPassAgeway.getPassagewayCno());
				fees.setVersion(productApproveMain.getVersion());
				fees.setActualRate(tdFeesPassAgeway.getFeeRate());
				fees.setDayCounter(tdFeesPassAgeway.getFeeBasis());
				fees.setInterest(detailSchedules.get(i).getInterest());
				fees.setPrincipal(detailSchedules.get(i).getPrincipal());
				fees.setRefBeginDate(detailSchedules.get(i).getRefBeginDate());
				fees.setRefEndDate(detailSchedules.get(i).getRefEndDate());
				//生效日之前用原来的参与计算方式 之后用新的参与计算方式
				if(PlaningTools.compareDate2(effdate, detailSchedules.get(i).getRefBeginDate(), "yyyy-MM-dd") && orgFeePass != null){
					fees.setIntCal(orgFeePass.getIsPass());
				}else{
					fees.setIntCal(tdFeesPassAgeway.getIsPass());
				}
				
				//参与计算的付费计划需要加入map中，用于冲减计提
				if("1".equals(fees.getIntCal()) && "Pay".equals(tdFeesPassAgeway.getFeeSituation()))
				{
					interest = interest.add(new BigDecimal(detailSchedules.get(i).getInterest()));
					agewayDaily = passAwayMap.get(detailSchedules.get(i).getRefBeginDate());
					if(agewayDaily == null)
					{
						passAwayMap.put(detailSchedules.get(i).getRefBeginDate(), fees);
					}else{
						agewayDaily.setInterest(PlaningTools.add(agewayDaily.getInterest(), fees.getInterest()));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.error(productApproveMain.getDealNo(), e);
			JY.raise(e.getMessage());
		}
		return  interest;	
	}
	
	@Override
    public HashMap<String, TdFeesPassAgewayDaily> recalAllPassFeePlanByCapitalByType(List<PrincipalInterval> principalIntervals, TdProductApproveMain productApproveMain,
                                                                                     String effdate, String type)throws Exception {
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = new HashMap<String, TdFeesPassAgewayDaily>();
		try {
			Map<String,Object> map =new HashMap<String,Object>();
			map.put("dealNo", productApproveMain.getDealNo());
			List<TdFeesPassAgeway> list =  tdFeesPassAgewayMapper.getTdPassageWayByRefNo(map);
			if(list.size() == 0) 
			{
				return passAwayMap;
			}
			for(TdFeesPassAgeway tdFeesPassAgeway : list) 
			{
				if("temp".equals(type)){//临时计算,不保存数据库
					calPassDailyCashFlowForTemp(principalIntervals, tdFeesPassAgeway, productApproveMain, passAwayMap, effdate);
				
				}else{//重新计算,保存数据库
					reCalPassCashFlow(principalIntervals, tdFeesPassAgeway, productApproveMain, passAwayMap, effdate,"add");
				
				}
				
			}
			return passAwayMap;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Override
    public double getSumFeeRateByRefNo(Map<String, Object> params){
		return tdFeesPassAgewayMapper.getSumFeeRateByRefNo(params);
	}
	
	@Override
	public BigDecimal getSumInterest(Map<String, Object> params) {
		double sumDailies = tdFeesPassAgewayDailyMapper.getSumInterest(params);
		return BigDecimal.valueOf(sumDailies);
	}
}
