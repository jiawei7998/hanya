package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.Function;
import com.singlee.capital.trade.model.TaModule;

public interface TaModuleService {
	/** 
	 * 递归获得所有 roldeIds 指定的 模块
	 *  
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModule(List<String> list);
	
	
	/**
	 * 查询所有菜单
	 */
	//List<TaModule> ModulesssList();
	List<TaModule> ModulesList();
	
	/**
	 * 某个菜单moduleId下的所有子子菜单的pid ，递归排序
	 * @param instId
	 * @return
	 */
	public List<TaModule> childrenInstitutionIds(String modulePid);
	
	/**
	 * 新增菜单
	 * @param taModule
	 */
	void insertTaModules(TaModule taModule);
	
	/**
	 * 修改菜单
	 * @param taModule
	 */
	void updateTaModules(TaModule taModule);
	
	/**
	 * 删除菜单
	 * @param moduleId
	 */
	void deleteTaModules(String[] moduleIdss);
	
	/**
	 * 根据moduleId查询功能点
	 */
	Page<Function> selectFunctionAllById(Map<String, Object> param);
	
	/**
	 * 新增功能点
	 */
	public String insertFunction(Function function);
	
	/**
	 * 修改功能点
	 */
	public void updateFunction(Function function);
	
	/**
	 * 　　删除功能点
	 */
	public void deleteFunctionById(Function function);
	
	
}
