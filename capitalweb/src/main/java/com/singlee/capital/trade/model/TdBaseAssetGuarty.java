package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASE_ASSET_GUARTY")
public class TdBaseAssetGuarty implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2166681784656456509L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	
	/*****************基础资产列表********************/
	private String  guartyMode    ;
	private String  pledgeAmt     ;
	private String  guarType      ;
	private String  pledgeRate     ;
	private String  colRightNo    ;
	private String  guarantor      ;
	private String  guarantorIndus ;
	private String  remark1         ;
	private String  remark2         ;
	private String  remark3         ;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getGuartyMode() {
		return guartyMode;
	}
	public void setGuartyMode(String guartyMode) {
		this.guartyMode = guartyMode;
	}
	public String getPledgeAmt() {
		return pledgeAmt;
	}
	public void setPledgeAmt(String pledgeAmt) {
		this.pledgeAmt = pledgeAmt;
	}
	public String getGuarType() {
		return guarType;
	}
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	public String getPledgeRate() {
		return pledgeRate;
	}
	public void setPledgeRate(String pledgeRate) {
		this.pledgeRate = pledgeRate;
	}
	public String getColRightNo() {
		return colRightNo;
	}
	public void setColRightNo(String colRightNo) {
		this.colRightNo = colRightNo;
	}
	public String getGuarantor() {
		return guarantor;
	}
	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}
	public String getGuarantorIndus() {
		return guarantorIndus;
	}
	public void setGuarantorIndus(String guarantorIndus) {
		this.guarantorIndus = guarantorIndus;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getRemark3() {
		return remark3;
	}
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	
	
	
	
	
}
