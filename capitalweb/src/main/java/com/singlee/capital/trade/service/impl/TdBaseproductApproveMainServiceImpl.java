package com.singlee.capital.trade.service.impl;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.base.trade.service.TdProductParticipantService;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdBaseproductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdProductApproveSubMapper;
import com.singlee.capital.trade.model.TdBaseproductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.service.TdBaseproductApproveMainService;
/**
 * 查询/新增 底层基础资产的实现类
 * @author SINGLEE
 *
 */
@Service
public class TdBaseproductApproveMainServiceImpl implements TdBaseproductApproveMainService {
	@Autowired
	TcProductMapper productMapper;
	@Autowired
	TdProductParticipantService tdProductParticipantService;
	@Autowired
	TrdOrderMapper approveManageDao;
	@Autowired
	TdBaseproductApproveMainMapper tdBaseproductApproveMainMapper;
	@Autowired
	TdProductApproveSubMapper tdProductApproveSubMapper;
	@Override
	public TtTrdOrder createBaseProductApprove(Map<String, Object> params) {
		// TODO Auto-generated method stub
		//map转实体类
		TdProductApproveMain productApproveMain = new TdProductApproveMain();
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		try{
			BeanUtil.populate(productApproveMain, params);
			productApproveMain.setProduct(productMapper.getProductById(prdNo));
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		if(null == productApproveMain.getDealNo() && "".equals(productApproveMain.getDealNo())){
			HashMap<String, Object> mapSel = new HashMap<String, Object>();
			mapSel.put("trdtype", "");
			String order_id = approveManageDao.getOrderId(mapSel);
			productApproveMain.setDealNo(order_id);
			productApproveMain.setVersion(0);
		}
		//1.将参与方持久化
	    tdProductParticipantService.insertParticipantByDeal(productApproveMain);
	    TdBaseproductApproveMain productBaseApproveMain = new TdBaseproductApproveMain();
	    BeanUtil.copyNotEmptyProperties(productBaseApproveMain, productApproveMain);
		//2.向main表中插入数据
	    if(null == productApproveMain.getDealNo() && "".equals(productApproveMain.getDealNo())){
	    	tdBaseproductApproveMainMapper.insert(productBaseApproveMain);
	    }else{
	    	tdBaseproductApproveMainMapper.updateByPrimaryKey(productBaseApproveMain);
	    }
		//3.向sub表插入数据
	    //审批子表
  		Object obj = productApproveMain.getProductApproveSubs();
  		if (obj != null) {
  			tdProductApproveSubMapper.deleteProductApproveSubByDealNo(productApproveMain.getDealNo());
  			List<TdProductApproveSub> productApproveSubs = FastJsonUtil.parseArrays(obj.toString(), TdProductApproveSub.class);
  			for (TdProductApproveSub productApproveSub : productApproveSubs) {
  				productApproveSub.setGenMainNo(productApproveMain.getDealNo());
  				tdProductApproveSubMapper.insert(productApproveSub);
  			}
  		}
  		TtTrdOrder ttTrdOrder = new TtTrdOrder();
  		ttTrdOrder.setOrder_id(productApproveMain.getDealNo());
  		return ttTrdOrder;
	}
	@Override
	public Page<TdBaseproductApproveMain> getBaseproductApproveMains(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		/**
		 * 查询出所有的底层资产数据列表
		 */
		Page<TdBaseproductApproveMain> tBaseproductApproveMains = this.tdBaseproductApproveMainMapper.getTdBaseproductApproveMainList(params, ParameterUtil.getRowBounds(params));
		/**
		 * 填充底层基础资产相关的参与方及自由项
		 */
		for (TdBaseproductApproveMain tdProductApproveMain : tBaseproductApproveMains) {
			//查询 KEY-VALUE 自由项
			params.put("genMainNo", "'"+tdProductApproveMain.getDealNo()+"'");
			tdProductApproveMain.setProductApproveSubs(tdProductApproveSubMapper.getProductApproveSubList(params));
			//查询参与方配置
			params.put("dealNo", tdProductApproveMain.getDealNo());
			params.put("version", tdProductApproveMain.getVersion());
			tdProductApproveMain.setParticipantApprove(tdProductParticipantService.transProductParticipantAsApproveSubs(params));
		}
		return tBaseproductApproveMains;
	}

}
