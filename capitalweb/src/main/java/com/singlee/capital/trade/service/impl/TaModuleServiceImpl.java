package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.trade.mapper.FunctionMapper;
import com.singlee.capital.trade.mapper.ModuleFunctionMapMapper;
import com.singlee.capital.trade.mapper.ModuleMapper;
import com.singlee.capital.trade.model.Function;
import com.singlee.capital.trade.model.ModuleFunctionMap;
import com.singlee.capital.trade.model.TaModule;
import com.singlee.capital.trade.service.TaModuleService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaModuleServiceImpl implements TaModuleService{

	//private static final String cacheName = "com.singlee.capital.system.institutions";
	@Autowired
	private ModuleMapper taModuleMapper;
	@Autowired
	private ModuleFunctionMapMapper moduleFunctionMapMapper;
	@Autowired
	private FunctionMapper taFunctionMapper;


	@Override
	//@Cacheable(value = cacheName, key="#list.toString()")
	public List<TaModule> recursionModule(List<String> list) {
		List<TaModule> orignial = taModuleMapper.recursionModule(list);
		return TreeUtil.mergeChildrenList(orignial, "0");
	}	
	
	

	
//	@Override
//	@Cacheable(value = cacheName, key="#role")
//	public LinkedList<TreeModule> recursionModule(String role){
//		List<String> roleList = new LinkedList<String>();
//		roleList.add(role);
//		LinkedList<TaModule> orignial = taModuleMapper.recursionModule(roleList);
//		return createModleList(orignial);
//	}
	
	/***
	 * ----以上为对外服务-------------------------------
	 */

	private TaModuleServiceImpl myself() {
		return SpringContextHolder.getBean(TaModuleServiceImpl.class);
	}

	/**
	 * 查询菜单列表
	 */
	@Override
	public List<TaModule> ModulesList() {
		List<TaModule> list = taModuleMapper.ModulesList();
		return TreeUtil.mergeChildrenList(list, "0");
	}
	
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * @param hashList
	 * @param parentTreeModule
	 * @return
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<TaModule> childrenInstitutionIds(String moduleId) {
		return (List<TaModule>)TreeUtil.getChildrenList(myself().ModulesList(), moduleId);
	}
	
	/**
	 * 新增菜单
	 */
	@Override
	public void insertTaModules(TaModule taModule) {
		
		TaModule taModule2 = taModuleMapper.selectByPrimaryKey(taModule);
		if(taModule2!=null){
			JY.raiseRException(MsgUtils.getMessage("error.system.0008"));
		}
		System.out.println("aaaaaa:"+taModule.getModulePid());
		List<String> taModuleList = taModuleMapper.queryTaModuleById(taModule.getModulePid());
		if(taModuleList.size() != 0){
			String TaModuleId = taModuleList.get(0);
			String[] EndIds = TaModuleId.split("-");//解析跟据父节点PID查询出子节点ID中‘-’
			System.out.println(EndIds[EndIds.length-1]);
			int endOldId = Integer.parseInt(EndIds[EndIds.length-1]);//取得子节点最后一位数
			int endNewId = endOldId + 1;//在子节点最后一位数上进行加1
			taModule.setModuleId(taModule.getModulePid()+"-"+endNewId);
			moduleFunctionMapMapper.deleteModuleFunctionMapById(taModule.getModuleId());
			ModuleFunctionMap mFunctionMap = new ModuleFunctionMap();
			mFunctionMap.setFunctionId("add");
			mFunctionMap.setModuleId(taModule.getModuleId());
			moduleFunctionMapMapper.insertModuleFunctionMap(mFunctionMap);
			
		}else{
			List<String> taModuleLists = taModuleMapper.queryTaModuleByIdS(taModule.getModulePid());
			if(taModuleLists!=null){
				String taModuleIds = taModuleLists.get(0);
				System.out.println("taModuleIds:"+taModuleIds);
				taModule.setModulePid(taModuleIds);
				System.out.println(taModule.getModulePid());
				int endNewIds = 1;
				taModule.setModuleId(taModuleIds+"-"+endNewIds);
				moduleFunctionMapMapper.deleteModuleFunctionMapById(taModule.getModuleId());
				ModuleFunctionMap mFunctionMap = new ModuleFunctionMap();
				mFunctionMap.setFunctionId("add");
				mFunctionMap.setModuleId(taModule.getModuleId());
				moduleFunctionMapMapper.insertModuleFunctionMap(mFunctionMap);
			}
		}
		taModuleMapper.insertTaModule(taModule);
	}

	/**
	 * 修改菜单
	 */
	@Override
	public void updateTaModules(TaModule taModule) {
		TaModule taModule2 = taModuleMapper.selectByPrimaryKey(taModule);
		if(taModule2==null){
			JY.raiseRException(MsgUtils.getMessage("error.system.0008"));
		}
		taModuleMapper.updateTaModule(taModule);
	}

	/**
	 * 删除菜单,及相对应的功能点和菜单功能点
	 */
	@Override
	public void deleteTaModules(String[] moduleIdss) {
		for (int i = 0; i < moduleIdss.length; i++) {
			taModuleMapper.deleteTaModule(moduleIdss[i]);
			moduleFunctionMapMapper.deleteModuleFunctionMapById(moduleIdss[i]);
			taFunctionMapper.deleteTaModuleById(moduleIdss[i]);
		}
	}

	/**
	 * 查询功能点列表，（根据菜单子节点查询相对应的功能点)
	 */
	@Override
	public Page<Function> selectFunctionAllById(Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		Page<Function> list = taFunctionMapper.searchFunctionByModuleId(param,rowBounds);
		return list;
	}

	/**
	 * 添加功能点信息
	 */
	@Override
	public String insertFunction(Function function) throws RException{
		int t = taFunctionMapper.queryFunctionById(function.getFunctionId());
		if(t == 0){
			function.setIsActive("1");
			taFunctionMapper.insertFunction(function);
			ModuleFunctionMap moduleFunctionMap = new ModuleFunctionMap();
			moduleFunctionMap.setModuleId(function.getModuleId());
			moduleFunctionMap.setFunctionId(function.getFunctionId());
			moduleFunctionMapMapper.insertModuleFunctionMap(moduleFunctionMap);
			return "0000";
		}else{
			JY.info(MsgUtils.getMessage("error.system.0008"));
			return "功能点代码("+function.getFunctionId()+")已经存在";
		}
	}

	/**
	 * 修改功能点信息
	 */
	@Override
	public void updateFunction(Function function) throws RException{
		if(function.getFunctionId() != null){
			taFunctionMapper.updateFunction(function);
		}
	}

	/**
	 * 根据功能点对象删除功能点信息和菜单功能点信息
	 */
	@Override
	public void deleteFunctionById(Function function) {
		if(function != null){
				taFunctionMapper.deleteFunctionById(function);
				moduleFunctionMapMapper.deleteModuleFunctionById(function);
		}
	}
}
