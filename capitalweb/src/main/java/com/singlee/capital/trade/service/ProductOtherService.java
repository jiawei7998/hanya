package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.ProductApproveParamVo;
import com.singlee.capital.trade.model.TdProductApproveAmtLine;


public interface ProductOtherService {

	
	public List<ProductApproveParamVo> getLoanParam();

	public void openLoan(String prdNo) throws Exception;
	
	public void closeLoan(String prdNo) throws Exception;
	
	public void saveAllProductParams(Map<String, Object> map) throws Exception;

	public List<TdProductApproveAmtLine> getAmtLineParam(String prdNo);

	
}
