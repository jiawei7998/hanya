package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.service.AdvanceMaturityService;

@Controller
@RequestMapping(value="/TdAdvanceMaturityController")
public class TdAdvanceMaturityController extends CommonController{
	@Autowired
	AdvanceMaturityService advanceMaturityService;
	
	@ResponseBody
	@RequestMapping(value = "/searchPageAdvanceMaturityForDealNo")
	public RetMsg<PageInfo<TdAdvanceMaturity>> searchPageAdvanceMaturityForDealNo(@RequestBody Map<String,Object> params){
		Page<TdAdvanceMaturity> page = advanceMaturityService.getAdvanceMaturityForDealNoPage(params);
		return RetMsgHelper.ok(page);
	}
}