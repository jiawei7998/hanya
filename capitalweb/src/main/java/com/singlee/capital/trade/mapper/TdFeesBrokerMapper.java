package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesBroker;

public interface TdFeesBrokerMapper extends Mapper<TdFeesBroker> {
	
	/**
	 * 查询中介费信息
	 */
	Page<TdFeesBroker> getFeesBrokerList(Map<String ,Object> map, RowBounds rb);
	
	List<TdFeesBroker> getFeesBrokerList(Map<String ,Object> map);


	void deleteTdFeesByDealNo(String dealNo);

	Page<TdFeesBroker> getFeesBrokerByDealNo(Map<String, Object> params,
			RowBounds rowBounds);
	
	List<TdFeesBroker> getFeesBrokerByDealNo(Map<String, Object> params);
	
	void updateFees(Map<String, Object> map);

}
