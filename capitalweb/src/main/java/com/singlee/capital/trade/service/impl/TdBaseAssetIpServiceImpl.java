package com.singlee.capital.trade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdBaseAssetIpMapper;
import com.singlee.capital.trade.model.TdBaseAssetIp;
import com.singlee.capital.trade.service.TdBaseAssetIpService;
@Service
public class TdBaseAssetIpServiceImpl implements TdBaseAssetIpService {
	@Autowired
	private TdBaseAssetIpMapper baseAssetIpMapper;
	@Override
	public Page<TdBaseAssetIp> getBaseAssetIps(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return baseAssetIpMapper.getTdBaseAssetIps(params,ParameterUtil.getRowBounds(params));
	}

}
