package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdLiabilitiesDuration;

import tk.mybatis.mapper.common.Mapper;

/**
 * @projectName 同业业务管理系统
 * @className 负债存续期持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdLiabilitiesDuartionMapper extends Mapper<TdLiabilitiesDuration> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdLiabilitiesDuration getLiabilitiesDurationById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdLiabilitiesDuration> getLiabilitiesDurationList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdLiabilitiesDuration> getLiabilitiesDurationListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdLiabilitiesDuration> getLiabilitiesDurationListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param LiabilitiesDuration
	 * @return
	 */
	public TdLiabilitiesDuration searchLiabilitiesDuration(TdLiabilitiesDuration LiabilitiesDuration);
}