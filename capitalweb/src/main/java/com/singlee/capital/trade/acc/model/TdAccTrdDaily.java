package com.singlee.capital.trade.acc.model;

import java.io.Serializable;
/**
 * 每日账务数据记录表（交易层）
 * @author SINGLEE
 *
 */
public class TdAccTrdDaily implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dealNo;//交易编号
	private String postDate;//账务日期
	private String accType;//计提类型-INTEREST 利息计提 
	private double accAmt;//计提所使用的本金
	private String opTime;//操作时间
	private String refNo;
	private String acctNo;//会计套号
	
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public double getAccAmt() {
		return accAmt;
	}
	public void setAccAmt(double accAmt) {
		this.accAmt = accAmt;
	}
	public String getOpTime() {
		return opTime;
	}
	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	@Override
	public String toString() {
		return "TdAccTrdDaily [dealNo=" + dealNo + ", postDate=" + postDate
				+ ", accType=" + accType + ", accAmt=" + accAmt + ", opTime="
				+ opTime + ", refNo=" + refNo + "]";
	}
	

}
