package com.singlee.capital.trade.mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.acc.model.TdProductApproveOverDraft;

public interface TdProductApproveOverDraftMapper extends Mapper<TdProductApproveOverDraft>{
	/*
	 * 拷贝表TD_PRODUCT_APPROVE_OVERDRAFT
	 */
	public void insertProductApproveOverDraftCopy(Map<String, Object> params);
}
