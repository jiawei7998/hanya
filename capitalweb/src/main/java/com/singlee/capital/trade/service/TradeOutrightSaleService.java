package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdOutrightSale;

/**
 * @projectName 同业业务管理系统
 * @className 交易卖断服务接口
 * @version 1.0
 */
public interface TradeOutrightSaleService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单笔卖断主键
	 * @return 交易单笔卖断对象
	 */
	public TdOutrightSale getTdOutrightSaleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 交易单笔卖断对象列表
	 */
	public Page<TdOutrightSale> getTdOutrightSalePage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 交易单笔卖断对象
	 */
	public void createTdOutrightSale(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 */
	public void updateTdOutrightSale(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 交易单笔卖断主键列表
	 */
	public void deleteTdOutrightSale(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	public void statusChanges(TdOutrightSale outrightSale);
	
	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单笔卖断主键
	 * @return 交易单笔卖断对象
	 */
	public TdOutrightSale getTdOutrightSaleApproved(Map<String,Object> map);
	
	/**
	 * 计算获取卖断应计利息
	 * @return
	 */
	public double getOutrightSaleInt(Map<String,Object> map);
}
