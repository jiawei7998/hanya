package com.singlee.capital.trade.acc.model;

import java.io.Serializable;

public class TdAccTrdDailyVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String acctype;//每日类型
	private String dealNo;//交易流水
	private int version;//版本
	private String prdNo;//产品代码
	private String invtype;//投资类型
	private double settlAmt;//计提本金
	private String basis;//计提基础
	private double execRate;//计提利率
	private String curDate;//当前账务日期
	private String refBeginDate;//区间起息日
	private String refEndDate;//区间截止日
	private String feeDealNo;//
	
	public String getFeeDealNo() {
		return feeDealNo;
	}
	public void setFeeDealNo(String feeDealNo) {
		this.feeDealNo = feeDealNo;
	}
	public String getAcctype() {
		return acctype;
	}
	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public double getSettlAmt() {
		return settlAmt;
	}
	public void setSettlAmt(double settlAmt) {
		this.settlAmt = settlAmt;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public double getExecRate() {
		return execRate;
	}
	public void setExecRate(double execRate) {
		this.execRate = execRate;
	}
	public String getCurDate() {
		return curDate;
	}
	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	@Override
	public String toString() {
		return "TdAccTrdDailyVo [acctype=" + acctype + ", dealNo=" + dealNo
				+ ", version=" + version + ", prdNo=" + prdNo + ", invtype="
				+ invtype + ", settlAmt=" + settlAmt + ", basis=" + basis
				+ ", execRate=" + execRate + ", curDate=" + curDate
				+ ", refBeginDate=" + refBeginDate + ", refEndDate="
				+ refEndDate + "]";
	}
	
}
