package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdCost;

public interface TtTrdCostMapper extends Mapper<TtTrdCost>{
	/**
	 * 查询成本中心列表(父id)
	 * @return
	 */
	public List<TtTrdCost> selectTtTrdCost(Map <String, Object> param);
	 
	TtTrdCost selectTtTrdCostBycostent(Map <String, Object> param);
	
	/**
	 * 查询成本中心列表(子id)
	 * @return
	 */
	public List<TtTrdCost> selectTtTrdCostByIdsSS(@Param("costcent")String costcent);
	
	/**
	 * 查询成本中心列表分页
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public List<TtTrdCost> searchTtTrdCost(Map<String, Object> param,RowBounds rowBounds);
	
	/**
	 * 查询成本中心列表分页
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public List<TtTrdCost> searchTtTrdCost(Map<String, Object> param);
	
	/**
	 * 修改成本中心列表
	 * @param ttTrdCost
	 */
	public void updateTtTrdCost(TtTrdCost ttTrdCost);
	
	/**
	 * 根据costcent子节点成本中心列表
	 * @param costcent
	 */
	public void deleteTtTrdCost(String costcent);
	
	/**
	 * 添加成本中心列表
	 * @param ttTrdCost
	 */
	public void insertTtTrdCost(TtTrdCost ttTrdCost);
	
	/**
	 * 根据PARENTCOSTCENT父节点查询相应的costcent子节点
	 * @param parentcostcent
	 * @return
	 */
	public List<String> selectTtTrdCostById(@Param("parentcostcent")String parentcostcent);
	
	/**
	 * 根据costcent查询是否有值
	 * @param costcent
	 * @return
	 */
	public int selectTtTrdCostByIds(TtTrdCost ttTrdCost);
	
	/**
	 * 查询数据，仅供c投资组合用
	 */
	public Page<TtTrdCost> searchCost(Map<String, Object> param,RowBounds rowBounds);
	
	public Page<TtTrdCost> selectAllCost(Map<String, Object> param,RowBounds rowBounds);
	
	public Page<TtTrdCost> selectAllCostExcept(Map<String, Object> param,RowBounds rowBounds);
}
