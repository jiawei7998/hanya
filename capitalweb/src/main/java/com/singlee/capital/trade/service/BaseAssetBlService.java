package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAssetBl;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务接口
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface BaseAssetBlService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 */
	public List<TdBaseAssetBl> getBaseAssetBlList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 基础资产对象集合
	 */
	public Page<TdBaseAssetBl> getBaseAssetBlLists(Map<String, Object> params);
	
}
