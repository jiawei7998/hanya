package com.singlee.capital.trade.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdOverDueConfirm;


public interface TdOverDueConfirmService {
	
	/****
	 * 判断交易是否逾期
	 * @param dealno 交易号
	 * @return
	 */
	public boolean validOverDueDealByDealno(String dealno);

	/****
	 * 查询逾期交易
	 * @param dealno 交易号
	 * @return
	 */
	public TdOverDueConfirm queryOverDueDealByDealno(String dealno);
	/**
	 * 查询唯一的逾期交易
	 * @param dealno
	 */
	public TdOverDueConfirm getOverDueConfirmById(String dealno);
	
	/****
	 * 更新逾期交易信息
	 * @param overDueConfirm
	 */
	public void updateOverDueDeal(TdOverDueConfirm overDueConfirm);
	
	/****
	 * 从交易金融工具主表中查询逾期交易
	 * @param params
	 * @return
	 */
	public List<TdOverDueConfirm> queryOverDueDeals(Map<String, Object> params);
	
	/*****
	 * 根据原交易编号关联查询对应的逾期交易
	 * @param refNo
	 * @return
	 */
	public TdOverDueConfirm queryOverDueDealByRefNo(String refNo);
	
	
	/*****
	 * 保存逾期交易
	 * @param overDueConfirm
	 */
	public void insertOverDueDeal(Map<String, Object> params);
	
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 交易逾期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdOverDueConfirm> getTradeOverDuePage (Map<String, Object> params, int isFinished);

	/**
	 * 删除
	 * 
	 * @param dealNos - ID集合
	 */
	public void deleteTradeOverDue(String[] dealNos);

	/*****
	 * 更新
	 * @param map
	 */
	public void updateOverDueDeal(HashMap<String, Object> map);
	
	/****
	 * 计算逾期利息
	 * @param params
	 * @return
	 */
	public double calOverDueIntAmt(Map<String, Object> params);
	
	
	public Page<TdOverDueConfirm> getOverdueConfirmForDealNoPage (Map<String, Object> params);
	
}
