package com.singlee.capital.trade.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowAmorMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowAmor;
import com.singlee.capital.cashflow.service.RateRangeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdOutrightSaleMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdOutrightSale;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TradeOutrightSaleService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 单笔卖断
 * @description TODO
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeOutrightSaleServiceImpl extends AbstractDurationService implements TradeOutrightSaleService {

	@Autowired
	private TdOutrightSaleMapper outrightSaleMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private TdCashflowCapitalMapper CashflowCapitalMapper;
	
	@Autowired
	private TdCashflowDailyInterestMapper CashflowDailyInterestMapper;
	
	@Autowired
	private TdCashflowInterestMapper CashflowInterestMapper;
	
	@Autowired
	private RateRangeService rateRangeService;
	
	@Autowired 
	private TdRateRangeMapper rateRangeMapper;
	@Autowired
	private TdCashflowAmorMapper cashflowAmorMapper;
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	
//	@Autowired
//	private TrdTposMapper tposMapper;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Override
	public TdOutrightSale getTdOutrightSaleById(String dealNo) {
		return outrightSaleMapper.getTdOutrightSaleById(dealNo);
	}

	@Override
	public Page<TdOutrightSale> getTdOutrightSalePage(
			Map<String, Object> params, int isFinished) {
		Page<TdOutrightSale> page= new Page<TdOutrightSale>();
		if(isFinished == 2){
			page= outrightSaleMapper.getTdOutrightSaleList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= outrightSaleMapper.getTdOutrightSaleListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= outrightSaleMapper.getTdOutrightSaleListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdOutrightSale> list =page.getResult();
		for(TdOutrightSale te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易单笔卖断金融工具表")
	public void createTdOutrightSale(Map<String, Object> params) {
		outrightSaleMapper.insert(TdOutrightSale(params));
	}

	@Override
	@AutoLogMethod(value = "修改交易单笔卖断金融工具表")
	public void updateTdOutrightSale(Map<String, Object> params) {
		outrightSaleMapper.updateByPrimaryKey(TdOutrightSale(params));
	}

	
	private TdOutrightSale TdOutrightSale(Map<String, Object> params){
		// map转实体类
		TdOutrightSale TdOutrightSale = new TdOutrightSale();
		try {
			BeanUtil.populate(TdOutrightSale, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdOutrightSale extend = new TdOutrightSale();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(TdOutrightSale.getDealType());
        extend = outrightSaleMapper.searchTdOutrightSale(extend);

        if(TdOutrightSale.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(TdOutrightSale.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		TdOutrightSale.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(TdOutrightSale.getDealType())){
    		TdOutrightSale.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		TdOutrightSale.setaDate(extend.getaDate());
    	}
    	TdOutrightSale.setSponsor(SlSessionHelper.getUserId());
    	TdOutrightSale.setSponInst(SlSessionHelper.getInstitutionId());
		TdOutrightSale.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		TdOutrightSale.setCfType(DictConstants.TrdType.CustomOutRightSale);
		return TdOutrightSale;
	}
	
	/**
	 * 流程通过的实现
	 * @param params
	 */
	@Override
	public void statusChanges(TdOutrightSale outrightSale) {
		
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTdOutrightSale(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			outrightSaleMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdOutrightSale TdOutrightSale = new TdOutrightSale();
		BeanUtil.populate(TdOutrightSale, params);
		TdOutrightSale = outrightSaleMapper.searchTdOutrightSale(TdOutrightSale);
		return productApproveService.getProductApproveActivated(TdOutrightSale.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdOutrightSale outrightSale = new TdOutrightSale();
		BeanUtil.populate(outrightSale, params);
		if(params.get("trade_id") != null){
			outrightSale.setDealNo(ParameterUtil.getString(params, "trade_id", ""));
		}
		outrightSale = outrightSaleMapper.searchTdOutrightSale(outrightSale);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(outrightSale.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(outrightSale.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(outrightSale.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomOutRightSale);//单笔卖断
		tdTrdEvent.setEventTable("TD_OUTRIGHT_SALE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		TdOutrightSale outrightSale = new TdOutrightSale();
		BeanUtil.populate(outrightSale, params);
		if(outrightSale.getOsaleType() != null && "S".equals(outrightSale.getOsaleType().trim())) {//卖断
			/**
			 * 删除之前数据存在的数据(还本计划，利息计提、收息计划）
			 */
			
			String postdate = "";
			Map<String, Object> tempMap = new HashMap<String, Object>();
			tempMap.put("dealNo", outrightSale.getRefNo());
			tempMap.put("refNo", outrightSale.getRefNo());
			tempMap.put("startDate", outrightSale.getOsaleVdate());
			List<TdCashflowInterest> tdCashflowInterest = CashflowInterestMapper.getInterestListUnconfirmed(tempMap);
			if(tdCashflowInterest.size() == 0) {
				postdate = outrightSale.getOsaleVdate();
			} else {
				postdate = tdCashflowInterest.get(0).getRefBeginDate();
			}
			
			//删除每日计提>卖断起息日
			params.put("dealNo", outrightSale.getRefNo());
			params.put("startDate", postdate);
			CashflowDailyInterestMapper.deleteCashflowDailyInterest(params);
			//删除还本日期>卖断起息日，收息日期>卖断起息日
			//params.put("endDate", outrightSale.getOsaleVdate());
			//CashflowCapitalMapper.deleteCapitalList(params);
			//删除未收还本计划
			CashflowCapitalMapper.deleteForApproveNotActual(tempMap);
			//删除未确认的利息
			Map<String,Object> map  = new HashMap<String, Object>();
			if(outrightSale.getRefNo() !=null && !"".equals(outrightSale.getRefNo())){
				map.put("refNo", outrightSale.getRefNo());
				CashflowInterestMapper.deleteForApproveNotActual(map);
			}
			
		} else if(outrightSale.getOsaleType() != null && "T".equals(outrightSale.getOsaleType().trim())) {//转让
			//修改收息
			params.put("effectDate", outrightSale.getOsaleVdate());
			params.put("refNo", outrightSale.getRefNo());
			params.put("changeRate", outrightSale.getOsaleSrate());
			params.put("notChangeTradeRate", "true");
			params.put("version", 0);
			//利率变更
			rateRangeService.updateRateRangeByRateChange(params);
		}
	}

	@Override
	public TdOutrightSale getTdOutrightSaleApproved(Map<String,Object> map) {
		return outrightSaleMapper.getTdOutrightSaleApproved(map);
	}

	//计算未收息至卖断日利息
	@Override
	public double getOutrightSaleInt(Map<String, Object> map) {
		TdProductApproveMain productApproveMain = null;
		if(!"".equals(ParameterUtil.getString(map, "refNo", ""))){
	    	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(map, "refNo", "")));
	        JY.require(productApproveMain!=null, "原交易信息不存在");
	    }else{
	    	throw new RException("原交易信息不存在！");
	    }
		double amInt = 0.00;
		String postdate = "";
		
		Map<String, Object> tempMap = new HashMap<String, Object>();
		tempMap.put("dealNo", ParameterUtil.getString(map, "refNo", ""));
		tempMap.put("refNo", ParameterUtil.getString(map, "refNo", ""));
		tempMap.put("startDate", ParameterUtil.getString(map, "osaleVdate", ""));
		List<TdCashflowInterest> tdCashflowInterest = CashflowInterestMapper.getInterestListUnconfirmed(tempMap);
		if(tdCashflowInterest.size() == 0) {
			return amInt;
		} else {
			postdate = tdCashflowInterest.get(0).getRefBeginDate();
		}
		
		/**
		 *	如果是先收息，则查找摊销计划表
		 *  后收息则计算计提差额
		 */
		if(productApproveMain.getIntType().equals(DictConstants.intType.chargeBefore))
		{
			map.put("startDate", postdate);
			map.put("endDate", ParameterUtil.getString(map, "amDate", postdate));
			map.put("dealNo", productApproveMain.getDealNo());
			map.put("version", productApproveMain.getVersion());
			
			List<CashflowAmor> cashflowAmors = cashflowAmorMapper.pageDailyCashflowAmorList(map, new RowBounds(0,99999999)).getResult();
			for(CashflowAmor cashflowAmor:cashflowAmors) {
                amInt = PlaningTools.add(amInt, cashflowAmor.getActAmorAmt());
            }
		}else{
			TdOutrightSale outrightSale = new TdOutrightSale();
			BeanUtil.populate(outrightSale, map);
			List<InterestRange> interestRanges = new ArrayList<InterestRange>();
			tempMap.put("startDate", "");
			List<TdRateRange> rateRanges = rateRangeMapper.getRateRanges(tempMap);
			InterestRange interestRange = null;
			for(TdRateRange rateRange : rateRanges){
				interestRange = new InterestRange();
				interestRange.setStartDate(rateRange.getBeginDate());
				interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(rateRange.getEndDate(), Frequency.DAY, -1));
				interestRange.setRate(rateRange.getExecRate());
				interestRanges.add(interestRange);
			}
		
			List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
			PrincipalInterval principalInterval = new PrincipalInterval();
			
			principalInterval.setStartDate(postdate);
			principalInterval.setEndDate(ParameterUtil.getString(map, "osaleVdate", outrightSale.getOsaleVdate()));
			principalInterval.setResidual_Principal(ParameterUtil.getDouble(map, "remainAmt", outrightSale.getRemainAmt()));
			principalIntervals.add(principalInterval);
			
			try {
				amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, postdate, outrightSale.getOsaleVdate(), postdate, 
						DayCountBasis.equal(productApproveMain.getBasis()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				JY.require(true,"计算区间差额计提出错"+e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		
		//是否减去通道计提
		if("true".equals(ParameterUtil.getString(map, "isFees", "").trim())) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("refNo", ParameterUtil.getString(map, "refNo", ""));
			params.put("refBeginDate", postdate);
			params.put("refEndDate", ParameterUtil.getString(map, "osaleVdate", postdate));
			params.put("intCal", "1");
			BigDecimal totalPassAgeway = new BigDecimal(0);
			List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayService.getFeesDailyInterestList(params);
			for(TdFeesPassAgewayDaily fp: fAgewayDailies) {
				totalPassAgeway = totalPassAgeway.add(BigDecimal.valueOf(fp.getInterest()));
			}
			amInt = amInt - totalPassAgeway.doubleValue();
		}
		
		return amInt;
	}

}
