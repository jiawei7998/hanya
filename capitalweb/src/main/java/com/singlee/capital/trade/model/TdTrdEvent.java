package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * 记录交易所发生事件表
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_TRD_EVENT")
public class TdTrdEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//交易流水
	@Id
	private int version;//版本号
	@Id
	private String eventType;//事件类别
	@Id
	private String eventRefno;//事件对应的流水
	@Id
	private String eventTable;//事件对应的表
	
	private String opTime;//操作时间
	@Transient
	private String jspUrl;
	@Transient
	private String jspName;

	public String getJspName() {
		return jspName;
	}

	public void setJspName(String jspName) {
		this.jspName = jspName;
	}

	public String getJspUrl() {
		return jspUrl;
	}

	public void setJspUrl(String jspUrl) {
		this.jspUrl = jspUrl;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventRefno() {
		return eventRefno;
	}

	public void setEventRefno(String eventRefno) {
		this.eventRefno = eventRefno;
	}

	public String getEventTable() {
		return eventTable;
	}

	public void setEventTable(String eventTable) {
		this.eventTable = eventTable;
	}

	public String getOpTime() {
		return opTime;
	}

	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}

	@Override
	public String toString() {
		return "TdTrdEvent [dealNo=" + dealNo + ", version=" + version
				+ ", eventType=" + eventType + ", eventRefno=" + eventRefno
				+ ", eventTable=" + eventTable + ", opTime=" + opTime + "]";
	}
	

}
