package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdAdvanceMaturity;

/**
 * @projectName 同业业务管理系统
 * @className 提前到期持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdAdvanceMaturityMapper extends Mapper<TdAdvanceMaturity> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdAdvanceMaturity getAdvanceMaturityById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturity> getAdvanceMaturityList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturity> getAdvanceMaturityListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdAdvanceMaturity> getAdvanceMaturityListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param advanceMaturity
	 * @return
	 */
	public TdAdvanceMaturity searchAdvanceMaturity(TdAdvanceMaturity advanceMaturity);
	
	/**
	 * 查询原交易是否已存在存续期业务交易
	 * @param map
	 * @return
	 */
	public TdAdvanceMaturity ifExistDuration(Map<String,Object> map);
	/**
	 * 获取提前到期数据 供账务使用
	 * @param map
	 * @return
	 */
	public List<TdAdvanceMaturity> getAdvanceMaturitiesForAcc(Map<String,Object> map);

	public Page<TdAdvanceMaturity> getTdAdvanceMaturity(Map<String, Object> params, RowBounds rowBounds);
	
	
	/**
	 * 根据 RefNo version 查询所有历史生效的 提前到期
	 * @param params
	 * @return
	 */
	public Page<TdAdvanceMaturity>  getAllAdvanceMaturitysByDealnoVersion(Map<String, Object> params,RowBounds rb);
	
	public void updateTdAdvanceMaturityActAmt(Map<String,Object> map);
	
	public void updateTdAdvanceMaturityActInt(Map<String,Object> map);
	
	public Integer getDurationUnfinishCount(Map<String,Object> map);
	
	
}