package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdApproveCancel;

/**
 * @projectName 同业业务管理系统
 * @className 审批单撤销服务接口
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface ApproveCancelService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 审批单撤销主键
	 * @return 审批单撤销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdApproveCancel getApproveCancelById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 审批单撤销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdApproveCancel> getApproveCancelPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 审批单撤销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createApproveCancel(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateApproveCancel(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 审批单撤销主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteApproveCancel(String[] dealNos);
	
	/**
	 * 查询是否已经存在审批单撤销
	 * 
	 * @param approveCancel
	 * @author firedog
	 * @date 2001-9-25
	 */
	public TdApproveCancel ifExistApproveCancel(Map<String,Object> map);
	
}
