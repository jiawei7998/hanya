package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 股票信息
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_STOCK_INFO")
public class TdStockInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7017723444287298578L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 股票名称
	 */
	private String stockName;
	/**
	 * 股票代码
	 */
	private String stockCode;
	/**
	 * 上市交易所
	 */
	private String stockExchanges;
	/**
	 * 交易金额(元)
	 */
	private Double stockAmt;
	/**
	 * 执行股价(元)
	 */
	private Double pledgePrice;
	/**
	 * 质押股数
	 */
	private Double pledgeNumber;
	/**
	 * 质押金额(元)
	 */
	private Double pledgeAmout;
	/**
	 * 质押率 
	 */
	private Double pledgeRatio;
	/**
	 * 预警线 
	 */
	private Double warningLine;
	/**
	 * 平仓线 
	 */
	private Double closingLine;
	/**
	 * 是否限售 
	 */
	private String isRestricted;
	/**
	 * 优先劣后比例
	 */
	private String priorityInferiorRatio;
	/**
	 * 其他比例
	 */
	private String otherRatio;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getStockExchanges() {
		return stockExchanges;
	}
	public void setStockExchanges(String stockExchanges) {
		this.stockExchanges = stockExchanges;
	}
	public Double getStockAmt() {
		return stockAmt;
	}
	public void setStockAmt(Double stockAmt) {
		this.stockAmt = stockAmt;
	}
	public Double getPledgePrice() {
		return pledgePrice;
	}
	public void setPledgePrice(Double pledgePrice) {
		this.pledgePrice = pledgePrice;
	}
	public Double getPledgeNumber() {
		return pledgeNumber;
	}
	public void setPledgeNumber(Double pledgeNumber) {
		this.pledgeNumber = pledgeNumber;
	}
	public Double getPledgeAmout() {
		return pledgeAmout;
	}
	public void setPledgeAmout(Double pledgeAmout) {
		this.pledgeAmout = pledgeAmout;
	}
	public Double getPledgeRatio() {
		return pledgeRatio;
	}
	public void setPledgeRatio(Double pledgeRatio) {
		this.pledgeRatio = pledgeRatio;
	}
	public Double getWarningLine() {
		return warningLine;
	}
	public void setWarningLine(Double warningLine) {
		this.warningLine = warningLine;
	}
	public Double getClosingLine() {
		return closingLine;
	}
	public void setClosingLine(Double closingLine) {
		this.closingLine = closingLine;
	}
	public String getIsRestricted() {
		return isRestricted;
	}
	public void setIsRestricted(String isRestricted) {
		this.isRestricted = isRestricted;
	}
	public String getPriorityInferiorRatio() {
		return priorityInferiorRatio;
	}
	public void setPriorityInferiorRatio(String priorityInferiorRatio) {
		this.priorityInferiorRatio = priorityInferiorRatio;
	}
	public String getOtherRatio() {
		return otherRatio;
	}
	public void setOtherRatio(String otherRatio) {
		this.otherRatio = otherRatio;
	}
}
