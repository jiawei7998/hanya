package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductApproveSub;

/**
 * @projectName 同业业务管理系统
 * @className 产品审批子表持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-10 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdProductApproveSubMapper extends Mapper<TdProductApproveSub> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param genMainNo - 产品审批子表主键
	 * @return 产品审批子表对象
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public TdProductApproveSub getProductApproveSubById(String genMainNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批子表对象列表
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public Page<TdProductApproveSub> getProductApproveSubList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 产品审批子表对象列表
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public List<TdProductApproveSub> getProductApproveSubList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 * 
	 * @param dealNo - 交易单号
	 * @author Hunter
	 * @date 2016-8-10
	 */
	public void deleteProductApproveSubByDealNo(String dealNo);
	/**
	 * 审批转换核实的时候直接采用拷贝方式
	 * @param map
	 */
	public void insertProductApproveSubsCopy(Map<String, Object> map);
}