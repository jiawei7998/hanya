package com.singlee.capital.trade.model;

import java.io.Serializable;

public class FinEnterpriseTrade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String partyname	;
	private String contractnum         ;
	private String custbrno            ;
	private String industype           ;
	private String region              ;
	private String currency            ;
	private String contractamt         ;
	private String rateType            ;
	private String baseratekey         ;
	private String cintetpe            ;
	private String forcatsprofitrate   ;
	private String penspread           ;
	private String incomedate          ;
	private String transferdate        ;
	private String duedate             ;
	private String recvdate            ;
	private String basis               ;
	private String syinterefre         ;
	private String interestfrequency   ;
	private String intpymtmethod       ;
	private String unitcost            ;
	private String setype              ;
	private String endstatus           ;
	public String getPartyname() {
		return partyname;
	}
	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}
	public String getContractnum() {
		return contractnum;
	}
	public void setContractnum(String contractnum) {
		this.contractnum = contractnum;
	}
	public String getCustbrno() {
		return custbrno;
	}
	public void setCustbrno(String custbrno) {
		this.custbrno = custbrno;
	}
	public String getIndustype() {
		return industype;
	}
	public void setIndustype(String industype) {
		this.industype = industype;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getContractamt() {
		return contractamt;
	}
	public void setContractamt(String contractamt) {
		this.contractamt = contractamt;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getBaseratekey() {
		return baseratekey;
	}
	public void setBaseratekey(String baseratekey) {
		this.baseratekey = baseratekey;
	}
	public String getCintetpe() {
		return cintetpe;
	}
	public void setCintetpe(String cintetpe) {
		this.cintetpe = cintetpe;
	}
	public String getForcatsprofitrate() {
		return forcatsprofitrate;
	}
	public void setForcatsprofitrate(String forcatsprofitrate) {
		this.forcatsprofitrate = forcatsprofitrate;
	}
	public String getPenspread() {
		return penspread;
	}
	public void setPenspread(String penspread) {
		this.penspread = penspread;
	}
	public String getIncomedate() {
		return incomedate;
	}
	public void setIncomedate(String incomedate) {
		this.incomedate = incomedate;
	}
	public String getTransferdate() {
		return transferdate;
	}
	public void setTransferdate(String transferdate) {
		this.transferdate = transferdate;
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}
	public String getRecvdate() {
		return recvdate;
	}
	public void setRecvdate(String recvdate) {
		this.recvdate = recvdate;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getSyinterefre() {
		return syinterefre;
	}
	public void setSyinterefre(String syinterefre) {
		this.syinterefre = syinterefre;
	}
	public String getInterestfrequency() {
		return interestfrequency;
	}
	public void setInterestfrequency(String interestfrequency) {
		this.interestfrequency = interestfrequency;
	}
	public String getIntpymtmethod() {
		return intpymtmethod;
	}
	public void setIntpymtmethod(String intpymtmethod) {
		this.intpymtmethod = intpymtmethod;
	}
	public String getUnitcost() {
		return unitcost;
	}
	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}
	public String getSetype() {
		return setype;
	}
	public void setSetype(String setype) {
		this.setype = setype;
	}
	public String getEndstatus() {
		return endstatus;
	}
	public void setEndstatus(String endstatus) {
		this.endstatus = endstatus;
	}
	
}
