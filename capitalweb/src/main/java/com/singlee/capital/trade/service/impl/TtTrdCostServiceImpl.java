package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.trade.mapper.TtTrdCostMapper;
import com.singlee.capital.trade.model.TtTrdCost;
import com.singlee.capital.trade.service.TtTrdCostService;

@Service
public class TtTrdCostServiceImpl implements TtTrdCostService {
	@Autowired
	private TtTrdCostMapper ttTrdCostMapper;

	private static Logger log = LoggerFactory.getLogger("TRADE");
	
	/**
	 * 查询成本中心列表
	 */
	@Override
    public List<TtTrdCost> selectTtTrdCost(Map <String, Object> param) {
		List<TtTrdCost> list = ttTrdCostMapper.selectTtTrdCost(param);
		return TreeUtil.mergeChildrenList(list, "0");
	}

	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * 
	 * @param hashList
	 * @param parentTreeModule
	 * @return
	 */

	/*@SuppressWarnings("unchecked")
	@Override
	public List<TtTrdCost> childrenInstitutionId(String costcent) {
		return (List<TtTrdCost>) TreeUtil.getChildrenList(myself().selectTtTrdCost(Map<String, Object> param), costcent);
	}*/

	/**
	 * 修改成本中心列表
	 * 
	 * 修改保存后最后修改时间改为系统时间
	 * @param ttTrdCost
	 */
	@Override
	public void updateTtTrdCost(TtTrdCost ttTrdCost) {
		String nowDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		ttTrdCost.setLstmntdte(nowDate);
		//ttTrdCost.setLstmntdte(DateUtil.getCurrentDateAsString().trim());
		ttTrdCostMapper.updateTtTrdCost(ttTrdCost);
	}

	/**
	 * 根据costcent子节点成本中心列表
	 * 
	 * @param costcent
	 */
	@Override
	public void deleteTtTrdCost(String[] costcent) {
		for (int i = 0; i < costcent.length; i++) {
			ttTrdCostMapper.deleteTtTrdCost(costcent[i]);
		}
	}

	/**
	 * 添加成本中心列表
	 * 
	 * @param ttTrdCost
	 */
	@Override
	public String insertTtTrdCost(TtTrdCost ttTrdCost) throws RException{
		int t =ttTrdCostMapper.selectTtTrdCostByIds(ttTrdCost);
		if(t == 0){
			ttTrdCost.setLstmntdte(DayendDateServiceProxy.getInstance().getSettlementDate());
			if(ttTrdCost.getParentId()==null || "".equals(ttTrdCost.getParentId())){
				ttTrdCost.setParentcostcent("-1");
			}
			ttTrdCostMapper.insertTtTrdCost(ttTrdCost);
			return "000";
		}else{
			log.info("成本中心代码("+ttTrdCost.getCostcent()+")已经存在");
			return "成本中心代码("+ttTrdCost.getCostcent()+")已经存在";
		}
		
	}

	@Override
	public List<TtTrdCost> searchTtTrdCost(Map<String, Object> param) {
		List<TtTrdCost> lists = ttTrdCostMapper.searchTtTrdCost(param);
		List<TtTrdCost> list = TreeUtil.mergeChildrenList(lists, "0");
		return list;
	}

	@Override
	public Page<TtTrdCost> searchCost(Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		Page<TtTrdCost> params = ttTrdCostMapper.searchCost(param, rowBounds);
		return params;
	}
	@Override
    public Page<TtTrdCost> searchAllCost(Map<String, Object> param){
		
		return ttTrdCostMapper.selectAllCost( param,ParameterUtil.getRowBounds(param));
	}
	
	@Override
    public Page<TtTrdCost> searchAllCostExcept(Map<String, Object> param){
		
		return ttTrdCostMapper.selectAllCostExcept( param,ParameterUtil.getRowBounds(param));
	}

	@Override
	public TtTrdCost selectTtTrdCostById(Map<String, Object> param) {
		return ttTrdCostMapper.selectTtTrdCostBycostent(param);
	}

}
