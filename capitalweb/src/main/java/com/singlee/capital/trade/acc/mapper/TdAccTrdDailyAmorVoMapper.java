package com.singlee.capital.trade.acc.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.acc.model.TdAccTrdDailyAmorVo;

public interface TdAccTrdDailyAmorVoMapper extends Mapper<TdAccTrdDailyAmorVo>{
	
	public List<TdAccTrdDailyAmorVo> getTdAccTrdDailyAmorVos(Map<String,Object> params);

	public Page<TdAccTrdDailyAmorVo> getTdAccTrdDailyAmorVoss(Map<String, Object> params, RowBounds rowBounds);
	
}
