package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdTransforScheduleInt;

/**
 * @projectName 同业业务管理系统
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdTransforScheduleIntMapper extends Mapper<TdTransforScheduleInt> {

	public TdTransforScheduleInt getTransforScheduleIntById(String dealNo);
	
	public Page<TdTransforScheduleInt> getTransforScheduleIntList(Map<String, Object> params, RowBounds rb);

	public Page<TdTransforScheduleInt> getTransforScheduleIntListFinished(Map<String, Object> params, RowBounds rb);

	public Page<TdTransforScheduleInt> getTransforScheduleIntListMine(Map<String, Object> params, RowBounds rb);
	
	public TdTransforScheduleInt searchTransforScheduleInt(TdTransforScheduleInt tdTransforScheduleInt);
}