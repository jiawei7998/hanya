package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="TC_PRODUCT_FEE_CAL")
public class TdProductFeeCal implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String prdNo;//产品类型
	@Id
	private String feeType;//费用类型
	private String feeName;//费用名称
	private String feeGlno;//费用GLNO
	private String feeGlnm;//费用GL名称
	private String feeCexp;//费用表达式
	private String isFeeDaily;//是否计提
	private String feeDesc;//描述
	
	private String isDisabled;//是否手工输入框 
	private int sortOrder;//排序
	
	private String feeCbeforeAmt;//先收息时的总费用字段
	private String feeCamt;//用于费用计提的主交易金额字段
	private String feeCbasis;//用于费用计提的计息基础字段
	private String feeCfre;//用于费用拆分的收费频率字段
	private String feeCtype;//用于费用计算的收费模式 前收息 后收息
	
	private String cfBase;//是否是基础资产
	
	public String getCfBase() {
		return cfBase;
	}
	public void setCfBase(String cfBase) {
		this.cfBase = cfBase;
	}
	public String getFeeCbeforeAmt() {
		return feeCbeforeAmt;
	}
	public void setFeeCbeforeAmt(String feeCbeforeAmt) {
		this.feeCbeforeAmt = feeCbeforeAmt;
	}
	public String getFeeCamt() {
		return feeCamt;
	}
	public void setFeeCamt(String feeCamt) {
		this.feeCamt = feeCamt;
	}
	public String getFeeCbasis() {
		return feeCbasis;
	}
	public void setFeeCbasis(String feeCbasis) {
		this.feeCbasis = feeCbasis;
	}
	public String getFeeCfre() {
		return feeCfre;
	}
	public void setFeeCfre(String feeCfre) {
		this.feeCfre = feeCfre;
	}
	public String getFeeCtype() {
		return feeCtype;
	}
	public void setFeeCtype(String feeCtype) {
		this.feeCtype = feeCtype;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getIsDisabled() {
		return isDisabled;
	}
	public void setIsDisabled(String isDisabled) {
		this.isDisabled = isDisabled;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public String getFeeGlno() {
		return feeGlno;
	}
	public void setFeeGlno(String feeGlno) {
		this.feeGlno = feeGlno;
	}
	public String getFeeGlnm() {
		return feeGlnm;
	}
	public void setFeeGlnm(String feeGlnm) {
		this.feeGlnm = feeGlnm;
	}
	public String getFeeCexp() {
		return feeCexp;
	}
	public void setFeeCexp(String feeCexp) {
		this.feeCexp = feeCexp;
	}
	public String getIsFeeDaily() {
		return isFeeDaily;
	}
	public void setIsFeeDaily(String isFeeDaily) {
		this.isFeeDaily = isFeeDaily;
	}
	public String getFeeDesc() {
		return feeDesc;
	}
	public void setFeeDesc(String feeDesc) {
		this.feeDesc = feeDesc;
	}
	
}
