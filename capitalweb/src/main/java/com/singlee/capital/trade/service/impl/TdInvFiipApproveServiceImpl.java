package com.singlee.capital.trade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.trade.mapper.TdBaseAssetIpMapper;
import com.singlee.capital.trade.mapper.TdInvFiipApproveMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdBaseAssetIp;
import com.singlee.capital.trade.model.TdInvFiipApprove;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdInvFiipApproveService;
import com.singlee.slbpm.service.FlowOpService;

@Service
public class TdInvFiipApproveServiceImpl extends AbstractDurationService implements TdInvFiipApproveService {

	@Autowired
	private TdInvFiipApproveMapper tdInvFiipApproveMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private DayendDateService dateService;
	@Autowired
	private TdBaseAssetIpMapper baseAssetIpMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Override
	public TdInvFiipApprove getTdInvFiipApproveById(String dealNo) {
		return tdInvFiipApproveMapper.getTdInvFiipApproveById(dealNo);
	}

	@Override
	public Page<TdInvFiipApprove> getTdInvFiipApprovePage(
			Map<String, Object> params, int isFinished) {
		Page<TdInvFiipApprove> page= new Page<TdInvFiipApprove>();
		if(isFinished == 2){
			page= tdInvFiipApproveMapper.getTdInvFiipApproveList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= tdInvFiipApproveMapper.getTdInvFiipApproveListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= tdInvFiipApproveMapper.getTdInvFiipApproveListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易展期金融工具表")
	public void createTdInvFiipApprove(Map<String, Object> params) {
		tdInvFiipApproveMapper.insert(TdInvFiipApprove(params));
	}

	@Override
	@AutoLogMethod(value = "修改交易展期金融工具表")
	public void updateTdInvFiipApprove(Map<String, Object> params) {
		tdInvFiipApproveMapper.updateByPrimaryKey(TdInvFiipApprove(params));
	}

	
	private TdInvFiipApprove TdInvFiipApprove(Map<String, Object> params){
		// map转实体类
		TdInvFiipApprove invFiipApprove = new TdInvFiipApprove();
		try {
			BeanUtil.populate(invFiipApprove, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdInvFiipApprove extend = new TdInvFiipApprove();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(invFiipApprove.getDealType());
        extend = tdInvFiipApproveMapper.searchTdInvFiipApprove(extend);

        if(invFiipApprove.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(invFiipApprove.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
            invFiipApprove.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(invFiipApprove.getDealType())){
    		invFiipApprove.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		invFiipApprove.setaDate(extend.getaDate());
    	}
    	invFiipApprove.setaDate(dateService.getDayendDate());
    	invFiipApprove.setPostdate(dateService.getDayendDate());
    	invFiipApprove.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		return invFiipApprove;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTdInvFiipApprove(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdInvFiipApproveMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdInvFiipApprove TdInvFiipApprove = new TdInvFiipApprove();
		BeanUtil.populate(TdInvFiipApprove, params);
		TdInvFiipApprove = tdInvFiipApproveMapper.searchTdInvFiipApprove(TdInvFiipApprove);
		return productApproveService.getProductApproveActivated(TdInvFiipApprove.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdInvFiipApprove TdInvFiipApprove = new TdInvFiipApprove();
		BeanUtil.populate(TdInvFiipApprove, params);
		TdInvFiipApprove = tdInvFiipApproveMapper.searchTdInvFiipApprove(TdInvFiipApprove);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(TdInvFiipApprove.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(TdInvFiipApprove.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(TdInvFiipApprove.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomInvFiIpTransFor);//
		tdTrdEvent.setEventTable("TD_INV_FIIP_APPROVE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		/**
		 * 审批完成后 将审批过程中保留的投资占比 过渡到投资占比列表中
		 * 可以关联到原交易中
		 */
		TdInvFiipApprove invFiipApprove = new TdInvFiipApprove();
		BeanUtil.populate(invFiipApprove, params);//map 转为 bean
		TdBaseAssetIp  baseAssetIp = new TdBaseAssetIp();
		BeanUtil.copyNotEmptyProperties(baseAssetIp, invFiipApprove);//拷贝到TdBaseAssetIp
		baseAssetIp.setDealNo(invFiipApprove.getRefNo());//转换dealNo为原交易
		baseAssetIp.setIsActive("1");//设置可用
		baseAssetIp.setPostdate(dateService.getDayendDate());//系统日期赋值
		baseAssetIpMapper.insert(baseAssetIp);//插入数据
	}
	
}
