package com.singlee.capital.trade.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.trade.model.TdFeesBroker;
import com.singlee.capital.trade.service.TdFeesBrokerService;

@Controller
@RequestMapping(value = "/TdFeesBrokerController")
public class TdFeesBrokerController {
	@Autowired
	private TdFeesBrokerService tdFeesBrokerService;
	/**
	 * 查询中介费
	 * 
	 * @param map
	 * @return 分页列表
	 * @date 2016-9-8
	 */
	@ResponseBody
	@RequestMapping(value = "/pageFeesBrokerList")
	public RetMsg<PageInfo<TdFeesBroker>> pageFeesBrokerList(@RequestBody Map<String,Object> params) throws RException {
		Page<TdFeesBroker> page = tdFeesBrokerService.pageFeesBrokerList(params);
		return RetMsgHelper.ok(page);
	}
	@ResponseBody
	@RequestMapping(value = "/getFeesBrokerByDealNo")
	public RetMsg<PageInfo<TdFeesBroker>> getFeesBrokerByDealNo(@RequestBody Map<String,Object> params) throws RException {
		Page<TdFeesBroker> page = tdFeesBrokerService.getFeesBrokerByDealNo(params);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFeesBrokerListByDealNo")
	public RetMsg<List<TdFeesBroker>> getFeesBrokerListByDealNo(@RequestBody Map<String,Object> params) throws RException {
		List<TdFeesBroker> list = tdFeesBrokerService.getFeesBrokerListByDealNo(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateFees")
	public RetMsg<Boolean> updateFees(@RequestBody Map<String,Object> params) throws RException {
        boolean f= false;
        try{
        	tdFeesBrokerService.updateFees(params);
        	f=true;
        }catch(Exception e){
        	e.printStackTrace();
        }
		return RetMsgHelper.ok(f);
	}
}
