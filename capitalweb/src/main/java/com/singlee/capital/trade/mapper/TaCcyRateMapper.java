package com.singlee.capital.trade.mapper;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;
import com.singlee.capital.trade.model.TaCcyRate;

/**
 * 汇率暂存mapper
 * @author LouHQ wangchen
 */
public interface TaCcyRateMapper extends Mapper<TaCcyRate> {
	
	public TaCcyRate getRate8(@Param("ccy")String ccy);
	
}