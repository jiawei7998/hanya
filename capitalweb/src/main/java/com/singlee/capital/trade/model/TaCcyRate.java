package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 币种汇率
 * @author LouHQ   wangchen
 *
 */
@Entity
@Table(name = "TA_CCY_RATE")
public class TaCcyRate implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5784706780744791327L;
	
	private String ccy;
	private double rate8;
	
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public double getRate8() {
		return rate8;
	}
	public void setRate8(double rate8) {
		this.rate8 = rate8;
	}

}
