package com.singlee.capital.trade.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
//收息计划确认
@Service("TradeOutRightBuyServiceImpl")
public class TradeOutRightBuyServiceImpl implements SlbpmCallBackInteface{

	
	/** 交易单dao **/
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	
	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("trade_id", serial_no);
		tradeMap.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(tradeMap);
		ttTrdTrade.setTrade_status(status);
		tradeManageMapper.updateTrade(ttTrdTrade);
	}

}
