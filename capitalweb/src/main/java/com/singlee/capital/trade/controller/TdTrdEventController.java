package com.singlee.capital.trade.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.TrdEventService;
@Controller
@RequestMapping(value="/TdTrdEventController")
public class TdTrdEventController extends CommonController{

	@Autowired
	TrdEventService eventService;
	
	@ResponseBody
	@RequestMapping(value = "/getTrdEventByDealNo")
	public RetMsg<List<TdTrdEvent>> getTrdEventByDealNo(@RequestBody Map<String,Object> params) throws RException{
		
		List<TdTrdEvent> trdEvents = this.eventService.getTrdEventForDealNo(params);
		
		return RetMsgHelper.ok(trdEvents);
	}
}
