package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASE_ASSET")
public class TdBaseAsset implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2166681784656456509L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	
	/*****************基础资产列表********************/
	private String shareType;//股东性质
	private String pledgeType;//质押物类型
	private double pledgeAmt;//质押物金额
	private String pledgeVdate;//质押起始日
	private String pledgeMdate;//质押到期日
	private double pledgeRate;//质押利率_%
	private double pledgeSpread;//质押利差_%
	private String mortgageType;//基础资产增信方式
	private String mortgageRemark;//基础资产增信描述
	
	/********************/
	private String baseAssetType ;                            //基础资产类别
	private double baseAmt  ;                                 //金额
	private String cno ;                                      //基础资产客户号
	private String cnm ;                                      //基础资产客户                     
	private String guarantor ;                                //保证人名称                       
	private String guarantorIndus ;                           // 保证人所属行业              
	private String secId    ;                                 // 债券编号                             
	private String secNm  ;                                   // 债券名称                             
	private String optExerDate  ;                             // 行权日期                       
	private String stockId    ;                               // 股票代码                           
	private String stockNm    ;                                 //股票名称                             
	private double warnLinePrice  ;                           //预警线                       
	private double coverLinePrice ;                           // 补仓线                      
	private double openLinePrice  ;                           //平仓线   
	
	private String isCust         ;//是否本行客户
	private String bigCat         ;//行业大类
	private String middleCat       ;//行业中类
	private String smallCat       ;//行业小类
	
	private String lmtId ;   //额度编号
	private String colRightNo; //抵押物权证号
	private String remark;//备注
	private String guarType;
	
	private String repoType;
	private String pledgeBankname;
	private String pledgeBankno;
	private String pledgeNo;

	private String  innerLevel;//行内评级
	private String  outerLevel;//公开评级

	/**
	 * 风险基础资产类型
	 */
	private String assetType;
	/**
	 * 系数
	 */
	private Double coefficient;
	/**
	 * 表内余额
	 */
	private Double inBalance;
	/**
	 * 基础资产余额（系统计算）
	 */
	private Double assetBalanceAuto;
	/**
	 * 基础资产余额（手工填写）
	 */
	private Double assetBalanceHand;
	/**
	 * 风险缓释额
	 */
	private Double rsrAmt;
	/**
	 * 基础资产类型
	 */
	@Transient
	private String assetTypeName;
	@Transient
	private String version;//版本号
	/********************/
	
	private String moneyMarketType;//货币市场类型
	@Transient
	private String bigCatName;//行业大类名称
	@Transient
	private String middleCatName;//行业中类名称
	@Transient
	private String smallCatName;//行业小类名称
	public String getMoneyMarketType() {
		return moneyMarketType;
	}
	public void setMoneyMarketType(String moneyMarketType) {
		this.moneyMarketType = moneyMarketType;
	}
	public String getDealNo() {
		return dealNo;
	}
	public String getGuarType() {
		return guarType;
	}
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	public String getLmtId() {
		return lmtId;
	}
	public void setLmtId(String lmtId) {
		this.lmtId = lmtId;
	}
	public String getColRightNo() {
		return colRightNo;
	}
	public void setColRightNo(String colRightNo) {
		this.colRightNo = colRightNo;
	}
	public String getIsCust() {
		return isCust;
	}
	public void setIsCust(String isCust) {
		this.isCust = isCust;
	}
	public String getBigCat() {
		return bigCat;
	}
	public void setBigCat(String bigCat) {
		this.bigCat = bigCat;
	}
	public String getMiddleCat() {
		return middleCat;
	}
	public void setMiddleCat(String middleCat) {
		this.middleCat = middleCat;
	}
	public String getSmallCat() {
		return smallCat;
	}
	public void setSmallCat(String smallCat) {
		this.smallCat = smallCat;
	}
	public String getBaseAssetType() {
		return baseAssetType;
	}
	public void setBaseAssetType(String baseAssetType) {
		this.baseAssetType = baseAssetType;
	}

	public String getCnm() {
		return cnm;
	}
	public void setCnm(String cnm) {
		this.cnm = cnm;
	}
	public String getGuarantor() {
		return guarantor;
	}
	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}
	public String getGuarantorIndus() {
		return guarantorIndus;
	}
	public void setGuarantorIndus(String guarantorIndus) {
		this.guarantorIndus = guarantorIndus;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getSecNm() {
		return secNm;
	}
	public void setSecNm(String secNm) {
		this.secNm = secNm;
	}
	public String getOptExerDate() {
		return optExerDate;
	}
	public void setOptExerDate(String optExerDate) {
		this.optExerDate = optExerDate;
	}
	public String getStockId() {
		return stockId;
	}
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}
	public String getStockNm() {
		return stockNm;
	}
	public void setStockNm(String stockNm) {
		this.stockNm = stockNm;
	}
	public double getWarnLinePrice() {
		return warnLinePrice;
	}
	public void setWarnLinePrice(double warnLinePrice) {
		this.warnLinePrice = warnLinePrice;
	}
	public double getCoverLinePrice() {
		return coverLinePrice;
	}
	public void setCoverLinePrice(double coverLinePrice) {
		this.coverLinePrice = coverLinePrice;
	}
	public double getOpenLinePrice() {
		return openLinePrice;
	}
	public void setOpenLinePrice(double openLinePrice) {
		this.openLinePrice = openLinePrice;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getShareType() {
		return shareType;
	}
	public void setShareType(String shareType) {
		this.shareType = shareType;
	}
	public String getPledgeType() {
		return pledgeType;
	}
	public void setPledgeType(String pledgeType) {
		this.pledgeType = pledgeType;
	}
	public double getPledgeAmt() {
		return pledgeAmt;
	}
	public void setPledgeAmt(double pledgeAmt) {
		this.pledgeAmt = pledgeAmt;
	}
	public String getPledgeVdate() {
		return pledgeVdate;
	}
	public void setPledgeVdate(String pledgeVdate) {
		this.pledgeVdate = pledgeVdate;
	}
	public String getPledgeMdate() {
		return pledgeMdate;
	}
	public void setPledgeMdate(String pledgeMdate) {
		this.pledgeMdate = pledgeMdate;
	}
	public double getPledgeRate() {
		return pledgeRate;
	}
	public void setPledgeRate(double pledgeRate) {
		this.pledgeRate = pledgeRate;
	}
	public double getPledgeSpread() {
		return pledgeSpread;
	}
	public void setPledgeSpread(double pledgeSpread) {
		this.pledgeSpread = pledgeSpread;
	}
	public String getMortgageType() {
		return mortgageType;
	}
	public void setMortgageType(String mortgageType) {
		this.mortgageType = mortgageType;
	}
	public String getMortgageRemark() {
		return mortgageRemark;
	}
	public void setMortgageRemark(String mortgageRemark) {
		this.mortgageRemark = mortgageRemark;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public Double getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}
	public Double getInBalance() {
		return inBalance;
	}
	public void setInBalance(Double inBalance) {
		this.inBalance = inBalance;
	}
	public Double getAssetBalanceAuto() {
		return assetBalanceAuto;
	}
	public void setAssetBalanceAuto(Double assetBalanceAuto) {
		this.assetBalanceAuto = assetBalanceAuto;
	}
	public Double getAssetBalanceHand() {
		return assetBalanceHand;
	}
	public void setAssetBalanceHand(Double assetBalanceHand) {
		this.assetBalanceHand = assetBalanceHand;
	}
	public Double getRsrAmt() {
		return rsrAmt;
	}
	public void setRsrAmt(Double rsrAmt) {
		this.rsrAmt = rsrAmt;
	}
	public String getAssetTypeName() {
		return assetTypeName;
	}
	public void setAssetTypeName(String assetTypeName) {
		this.assetTypeName = assetTypeName;
	}
	public double getBaseAmt() {
		return baseAmt;
	}
	public void setBaseAmt(double baseAmt) {
		this.baseAmt = baseAmt;
	}
	public String getRepoType() {
		return repoType;
	}
	public void setRepoType(String repoType) {
		this.repoType = repoType;
	}
	public String getPledgeBankname() {
		return pledgeBankname;
	}
	public void setPledgeBankname(String pledgeBankname) {
		this.pledgeBankname = pledgeBankname;
	}
	public String getPledgeBankno() {
		return pledgeBankno;
	}
	public void setPledgeBankno(String pledgeBankno) {
		this.pledgeBankno = pledgeBankno;
	}
	public String getPledgeNo() {
		return pledgeNo;
	}
	public void setPledgeNo(String pledgeNo) {
		this.pledgeNo = pledgeNo;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getInnerLevel() {
		return innerLevel;
	}
	public void setInnerLevel(String innerLevel) {
		this.innerLevel = innerLevel;
	}
	public String getOuterLevel() {
		return outerLevel;
	}
	public void setOuterLevel(String outerLevel) {
		this.outerLevel = outerLevel;
	}
	public String getBigCatName() {
		return bigCatName;
	}
	public void setBigCatName(String bigCatName) {
		this.bigCatName = bigCatName;
	}
	public String getMiddleCatName() {
		return middleCatName;
	}
	public void setMiddleCatName(String middleCatName) {
		this.middleCatName = middleCatName;
	}
	public String getSmallCatName() {
		return smallCatName;
	}
	public void setSmallCatName(String smallCatName) {
		this.smallCatName = smallCatName;
	}
	
	
}
