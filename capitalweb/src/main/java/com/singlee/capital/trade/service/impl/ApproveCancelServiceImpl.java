package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdApproveCancelMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdApproveCancel;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ApproveCancelService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 审批单撤销
 * @author xuhui
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ApproveCancelServiceImpl extends AbstractDurationService implements ApproveCancelService{

	@Autowired
	private TdApproveCancelMapper tdApproveCancelMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private FlowOpService flowOpService;
	
	@Override
	public TdApproveCancel getApproveCancelById(String dealNo) {
		return tdApproveCancelMapper.getApproveCancelById(dealNo);
	}

	@Override
	public Page<TdApproveCancel> getApproveCancelPage(
			Map<String, Object> params, int isFinished) {
		Page<TdApproveCancel> page= new Page<TdApproveCancel>();
		if(isFinished == 2){
			page = tdApproveCancelMapper.getApproveCancelList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page = tdApproveCancelMapper.getApproveCancelListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page = tdApproveCancelMapper.getApproveCancelListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdApproveCancel> list =page.getResult();
		for(TdApproveCancel ap :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(ap.getRefNo());
			if(main!=null){
				ap.setProduct(main.getProduct());
				ap.setParty(main.getCounterParty());
				ap.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建审批单撤销金融工具表")
	public void createApproveCancel(Map<String, Object> params) {
		tdApproveCancelMapper.insert(approveCancel(params));
	}

	@Override
	@AutoLogMethod(value = "修改审批单撤销金融工具表")
	public void updateApproveCancel(Map<String, Object> params) {
		tdApproveCancelMapper.updateByPrimaryKey(approveCancel(params));
	}

	
	private TdApproveCancel approveCancel(Map<String, Object> params){
		// map转实体类
		TdApproveCancel approveCancel = new TdApproveCancel();
		try {
			BeanUtil.populate(approveCancel, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		approveCancel.setSponsor(SlSessionHelper.getUserId());
		approveCancel.setSponInst(SlSessionHelper.getInstitutionId());
		approveCancel.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		return approveCancel;
	}
	
	/**
	 * 删除（审批单撤销+order）
	 */
	@Override
	public void deleteApproveCancel(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdApproveCancelMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 查询是否已存在未完成审批单撤销
	 */
	@Override
	public TdApproveCancel ifExistApproveCancel(Map<String,Object> map) {
		return tdApproveCancelMapper.ifExistApproveCancel(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdApproveCancel approveCancel = new TdApproveCancel();
		BeanUtil.populate(approveCancel, params);
		approveCancel = tdApproveCancelMapper.searchApproveCancel(approveCancel);
		return productApproveService.getProductApproveActivated(approveCancel.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdApproveCancel approveCancel = new TdApproveCancel();
		approveCancel.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
		approveCancel =  tdApproveCancelMapper.searchApproveCancel(approveCancel);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(approveCancel.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(approveCancel.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(approveCancel.getDealNo());//
		tdTrdEvent.setEventType(DictConstants.TrdType.ApproveCancel);//审批单撤销
		tdTrdEvent.setEventTable("TD_APPROVE_CANCEL");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		/*// TODO Auto-generated method stub
		//通过 撤销单的 refNo获得原预审批单号
		TdApproveCancel approveCancel = new TdApproveCancel();
		
		BeanUtil.populate(approveCancel, params);
		//更新审批单状态
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("order_id", approveCancel.getRefNo());//作废的原审批单据
		map.put("version_number", approveCancel.getVersion());
		map.put("is_locked", true);
		TtTrdOrder ttTrdOrder = tdOrderMapper.selectOrderForOrderId(map);
		ttTrdOrder.setIs_active(DictConstants.YesNo.NO);
		ttTrdOrder.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdOrder.setOperator_id(SlSessionHelper.getUserId());
		ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.Cancle);
		tdOrderMapper.updateApprove(ttTrdOrder);
		
		
		//删除核实单据
		map.put("isActive", "1");
		map.put("refNo", approveCancel.getRefNo());
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveByCondition(map);
		
		map.put("trade_id", productApproveMain.getDealNo());
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		ttTrdTrade.setIs_active(DictConstants.YesNo.NO);
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setOperator_id(SlSessionHelper.getUserId());
		ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Cancle);
		tradeManageMapper.updateTrade(ttTrdTrade);
		
		
		String[] dealNos = new String[1];
		dealNos[0]=productApproveMain.getDealNo();
		productApproveService.deleteProductApprove(dealNos);*/
	}
}
