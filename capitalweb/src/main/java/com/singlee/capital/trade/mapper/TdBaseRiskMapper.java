package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseRisk;

/**
 * @projectName 同业业务管理系统
 * 风险缓释
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdBaseRiskMapper extends Mapper<TdBaseRisk> {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @date 2016-10-14
	 */
	public Page<TdBaseRisk> getBaseRiskLists(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @date 2016-10-14
	 */
	public List<TdBaseRisk> getBaseRiskList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 * 
	 * @param dealNo - 交易单号
	 * @date 2016-10-14
	 */
	public void deleteBaseRiskByDealNo(String dealNo);
	
	public void insertBaseRiskForCopy(Map<String, Object> params);
}