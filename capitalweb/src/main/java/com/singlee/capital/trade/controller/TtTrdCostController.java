package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TtTrdCost;
import com.singlee.capital.trade.service.TtTrdCostService;

@Controller
@RequestMapping(value = "/TtTrdCostController")
public class TtTrdCostController extends CommonController{
	@Autowired 
	private TtTrdCostService ttTrdCostService;
	
	/**
	 * 查询成本中心列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtTrdCosts")
	public RetMsg<List<TtTrdCost>> selectTtTrdCost(@RequestBody Map<String, Object> map) throws RException{
		List<TtTrdCost> list = ttTrdCostService.selectTtTrdCost(map);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 根据ID查询成本中心数据
	 * @param map
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/selectTtTrdCostById")
	public RetMsg<TtTrdCost> selectTtTrdCostById(@RequestBody Map<String, Object> map) throws RException{
		TtTrdCost list = ttTrdCostService.selectTtTrdCostById(map);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 查询成本中心列表分页
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTtTrdCost")
	public RetMsg<List<TtTrdCost>> searchTtTrdCost(@RequestBody Map<String, Object> param) throws RException{
		return RetMsgHelper.ok(ttTrdCostService.searchTtTrdCost(param));
	}
	
	/**
	 * 修改成本中心列表
	 * @param ttTrdCost
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTtTrdCost")
	public RetMsg<Serializable> updateTtTrdCost(@RequestBody TtTrdCost ttTrdCost) throws RException{
		ttTrdCostService.updateTtTrdCost(ttTrdCost);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 根据costcent子节点成本中心列表
	 * @param ttTrdCost
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteTtTrdCost")
	public RetMsg<Serializable> deleteTtTrdCost(@RequestBody String[] costcent) throws RException{
		ttTrdCostService.deleteTtTrdCost(costcent);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 添加成本中心列表
	 * @param ttTrdCost
	 */
	@ResponseBody
	@RequestMapping(value = "/insertTtTrdCost")
	public RetMsg<Serializable> insertTtTrdCost(@RequestBody TtTrdCost ttTrdCost) throws RException{
		String msg = ttTrdCostService.insertTtTrdCost(ttTrdCost);
		if( "000" .equalsIgnoreCase(msg))
		{
			return RetMsgHelper.ok();
		}else{
			return RetMsgHelper.simple("001", msg);
		}
		
	}
	
	/**
	 * 查询陈本成本中心列表分页
	 */
	@ResponseBody
	@RequestMapping(value = "/selectCost")
	public RetMsg<PageInfo<TtTrdCost>> searchCost(@RequestBody Map<String, Object> param) throws RException{
		Page<TtTrdCost> params = ttTrdCostService.searchCost(param);
		return RetMsgHelper.ok(params);
	}
	/**
	 * 查询全部陈本成本中心列表
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllCosts")
	public RetMsg<PageInfo<TtTrdCost>> selectAllCosts(@RequestBody Map<String, Object> param)throws RException{
		return RetMsgHelper.ok(ttTrdCostService.searchAllCost(param));
	}
	
	/**
	 * 查询全部陈本成本中心列表，除去成本中心代码为-1的
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllCostsExcept")
	public RetMsg<PageInfo<TtTrdCost>> selectAllCostsExcept(@RequestBody Map<String, Object> param)throws RException{
		return RetMsgHelper.ok(ttTrdCostService.searchAllCostExcept(param));
	}
}
