package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductFeeDeal;

public interface ProductFeeDealService {
	/**
	 * 根据原单据流水查询获得所属费用交易
	 * @param paramsMap
	 * @return
	 */
	public Page<TdProductFeeDeal> getFeeDealsByOrgDealNo(Map<String, Object> paramsMap);
	
}
