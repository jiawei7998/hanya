package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdPort;

public interface TtTrdPortMapper extends Mapper<TtTrdPort>{
	/**
	 * 查询投资组合列表
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	public Page<TtTrdPort> searchTtTrdPort(Map<String, Object> param,RowBounds rowBounds);
	
	/**
	 * 新增投资组合
	 * @param ttTrdPort
	 */
	public void insertTtTrdPort(TtTrdPort ttTrdPort);
	
	/**
	 * 修改投资组合
	 * @param ttTrdPort
	 */
	public void updateTtTrdPort(TtTrdPort ttTrdPort);
	
	/**
	 * 根据portfolio删除投资组合
	 * @param portfolio
	 */
	public void deleteTtTrdPortById(@Param(value = "portfolio")String portfolio);
	
	/**
	 * 根据portfolio查询投资组合
	 * @param portfolio
	 */
	public int selectTtTrdPortById(@Param(value = "portfolio")String portfolio);
}
