package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 增信措施
 * @description TODO
 * @author 倪航
 * @icateDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Entity
@Table(name = "TD_INCREASE_CREDIT")
public class TdIncreaseCredit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2524648237754618733L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 增信方式
	 */
	private String icWay;
	/**
	 * 增信描述
	 */
	private String icDesc;
	/**
	 * 增信人编号
	 */
	private String icPartyId;
	/**
	 * 增信人客户号
	 */
	private String icPartyCode;
	/**
	 * 增信人名称
	 */
	private String icPartyName;
	/**
	 * 劣后金额
	 */
	private String inferiorAmt;
	/**
	 * 额度批复号
	 */
	private String icNoticeNo;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getIcWay() {
		return icWay;
	}
	public void setIcWay(String icWay) {
		this.icWay = icWay;
	}
	public String getIcDesc() {
		return icDesc;
	}
	public void setIcDesc(String icDesc) {
		this.icDesc = icDesc;
	}
	public String getIcPartyId() {
		return icPartyId;
	}
	public void setIcPartyId(String icPartyId) {
		this.icPartyId = icPartyId;
	}
	public String getIcPartyCode() {
		return icPartyCode;
	}
	public void setIcPartyCode(String icPartyCode) {
		this.icPartyCode = icPartyCode;
	}
	public String getIcPartyName() {
		return icPartyName;
	}
	public void setIcPartyName(String icPartyName) {
		this.icPartyName = icPartyName;
	}
	public String getInferiorAmt() {
		return inferiorAmt;
	}
	public void setInferiorAmt(String inferiorAmt) {
		this.inferiorAmt = inferiorAmt;
	}
	public String getIcNoticeNo() {
		return icNoticeNo;
	}
	public void setIcNoticeNo(String icNoticeNo) {
		this.icNoticeNo = icNoticeNo;
	}
}
