package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdAttachTreeMap;

/**
 * @projectName 
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:41:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AttachTreeMapService {

	/**
	 * 查询 
	 * @param params - 查询参数
	 * @return 附件树对象集合
	 */
	List<TdAttachTreeMap> searchAttachTreeMap(Map<String, Object> params);
	/**
	 * 创建
	 * 
	 * @param parentId - 父节点ID
	 * @return 附件树对象
	 */
	public TdAttachTreeMap createAttachTreeMap(String parentId);
	
	/**
	 * 修改
	 * 
	 * @param id - 节点ID
	 * @param text - 节点内容
	 * @return 附件树对象
	 */
	public TdAttachTreeMap updateAttachTreeMap(String id, String text);
	
	/**
	 * 删除
	 * 
	 * @param id - 节点ID
	 */
	public void deleteAttachTreeMap(String id);
	
	/**
	 * 拖拽
	 * 
	 * @param id - 拖拽的节点ID。
	 * @param targetId - 拖拽到的节点ID。
	 * @param point - 指明释放操作，可用值有：'append','top' 或 'bottom'。
	 */
	public void dndAttachTreeMap(String id, String targetId, String append);
	
	/**
	 * 清理未关联的附件树
	 * 
	 */
	public void clearAttachTreeMap();

}
