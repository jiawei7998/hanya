package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdCashFlowChange;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统
 * @className 现金流变更服务接口
 * @description TODO
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface CashFlowChangeService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 现金流变更对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdCashFlowChange getCashFlowChangeById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 现金流变更对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdCashFlowChange> getCashFlowChangePage (Map<String, Object> params, int isFinish);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 现金流变更对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createCashFlowChange(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateCashFlowChange(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 现金流变更主键列表
	 * @date 2016-9-25
	 */
	public void deleteCashFlowChange(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getTdProductApproveMain(String dealNo);
}
