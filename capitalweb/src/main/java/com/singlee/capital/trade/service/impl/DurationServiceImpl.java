package com.singlee.capital.trade.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.trade.mapper.TdAdvanceMaturityMapper;
import com.singlee.capital.trade.mapper.TdMulResaleMapper;
import com.singlee.capital.trade.model.TdAdvanceMaturity;
import com.singlee.capital.trade.service.DurationService;

/**
 * baseweb存续期接口实现类
 * @author Administrator
 *
 */
@Service(value="durationService")
public class DurationServiceImpl implements DurationService {
	
	@Autowired
	private TdAdvanceMaturityMapper tdAdvanceMaturityMapper;
	
	@Autowired
	private TdMulResaleMapper tdMulResaleMapper;

	/**
	 * 删除提前到期核实单
	 */
	@Override
	public int delAdvanceMaturity(String dealNo) {
		TdAdvanceMaturity advanceMaturity = new TdAdvanceMaturity();
		advanceMaturity.setDealNo(dealNo);
		return tdAdvanceMaturityMapper.deleteByPrimaryKey(dealNo);
	}

	/**
	 * 删除资产卖断
	 */
	@Override
	public int delMultiResale(String dealNo) {
		
		return tdMulResaleMapper.deleteByPrimaryKey(dealNo);
	}

}
