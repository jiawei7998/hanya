package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.base.mapper.TcAttachMapper;
import com.singlee.capital.base.mapper.TcAttachTreeTemplateMapper;
import com.singlee.capital.base.model.TcAttach;
import com.singlee.capital.base.model.TcAttachTreeTemplate;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.common.util.TreeIdGenerater;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdAttachTreeMapMapper;
import com.singlee.capital.trade.model.TdAttachTreeMap;
import com.singlee.capital.trade.service.AttachTreeMapService;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AttachTreeMapServiceImpl implements AttachTreeMapService, TreeIdGenerater{

	@Autowired
	private TcAttachMapper attachMapper;
	@Autowired
	private TdAttachTreeMapMapper attachTreeMapMapper;
	@Autowired
	private TcAttachTreeTemplateMapper attachTreeTemplateMapper;
	@Autowired
	private BatchDao batchDao;

	@Override
	public List<TdAttachTreeMap> searchAttachTreeMap(Map<String, Object> params) {
		List<TdAttachTreeMap> attachTreeMapList = attachTreeMapMapper.selectTdAttachTreeMap(params);
		String refType = ParameterUtil.getString(params, "refType", null);
		String refId = ParameterUtil.getString(params, "refId", null);
		String prdName = ParameterUtil.getString(params, "prdName", null);
		String dealNo = ParameterUtil.getString(params, "dealNo", null);
		String attachTreeTemp = ParameterUtil.getString(params, "attachTreeTemp", null);
		if (DictConstants.refType.form.equals(refType)) {
			if (attachTreeMapList.size() == 0) {
				if (attachTreeTemp != null) {
					Map<String, String> param = new HashMap<String, String>();
					param.put("refId", attachTreeTemp);
					param.put("sort", "tree_pid, sort");
					List<TcAttachTreeTemplate> attachTreeTemplateList = TreeUtil.mergeChildrenList(attachTreeTemplateMapper.selectTcAttachTreeTemplate(param), "0");
					
					attachTreeTemplateList = copy(attachTreeTemplateList, "0", this, dealNo);
					batchDao.batch("com.singlee.capital.trade.mapper.TdAttachTreeMapMapper.insertIncludeId", attachTreeTemplateList);
					/*TdAttachTreeMap attachTreeMap = new TdAttachTreeMap();
					for (TcAttachTreeTemplate attachTreeTemplate : attachTreeTemplateList) {
						//attachTreeMap.setTreeId(treeId);
						attachTreeMap.setTreePid("0");
						attachTreeMap.setRefId(refId);
						attachTreeMap.setRefType(refType);
						if (attachTreeTemplate.getTreeId().equals(attachTreeTemplate.getRefId())) {
							attachTreeTemplate.setTreeName(prdName + "[" + dealNo + "]");
						}
						copyAttachTree(attachTreeTemplate, attachTreeMap);
					}*/
				} else {
					TdAttachTreeMap tdAttachTreeMap = new TdAttachTreeMap();
					tdAttachTreeMap.setRefId(refId);
					tdAttachTreeMap.setRefType(refType);
					tdAttachTreeMap.setTreePid("0");
					tdAttachTreeMap.setTreeName(prdName + "[" + dealNo + "]");
					tdAttachTreeMap.setSort(1);
					attachTreeMapMapper.insert(tdAttachTreeMap);
				}
			}
		} else {
			if (attachTreeMapList.size() == 0) {
				Map<String, String> param = new HashMap<String, String>();
				param.put("refType", refType);
				param.put("sort", "tree_pid, sort");
				List<TcAttachTreeTemplate> attachTreeTemplateList = TreeUtil.mergeChildrenList(attachTreeTemplateMapper.selectTcAttachTreeTemplate(param), "0");
				if (attachTreeTemplateList == null || attachTreeTemplateList.size() == 0) {
					TdAttachTreeMap tdAttachTreeMap = new TdAttachTreeMap();
					tdAttachTreeMap.setRefId(refId);
					tdAttachTreeMap.setRefType(refType);
					tdAttachTreeMap.setTreePid("0");
					tdAttachTreeMap.setTreeName(prdName + "[" + dealNo + "]");
					tdAttachTreeMap.setSort(1);
					attachTreeMapMapper.insert(tdAttachTreeMap);
				} else {
					TdAttachTreeMap attachTreeMap = new TdAttachTreeMap();
					for (TcAttachTreeTemplate attachTreeTemplate : attachTreeTemplateList) {
						attachTreeMap.setTreePid("0");
						attachTreeMap.setRefId(refId);
						attachTreeMap.setRefType(refType);
						if (attachTreeTemplate.getTreeId().equals(attachTreeTemplate.getRefId())) {
							attachTreeTemplate.setTreeName(prdName + "[" + dealNo + "]");
						}
						copyAttachTree(attachTreeTemplate, attachTreeMap);
					}
				}
			}

		}
			
		List<TdAttachTreeMap> attachTreeMap = attachTreeMapMapper.selectTdAttachTreeMap(params);
		return TreeUtil.mergeChildrenList(attachTreeMap, "0");
	}
	
	@Override
	@AutoLogMethod(value = "创建附件树")
	public TdAttachTreeMap createAttachTreeMap(String parentId) {
		TdAttachTreeMap parentNode = new TdAttachTreeMap();
		parentNode.setTreeId(parentId);
		parentNode = attachTreeMapMapper.selectByPrimaryKey(parentNode);
		TdAttachTreeMap thisNode = new TdAttachTreeMap();
		thisNode.setRefId(parentNode.getRefId());
		thisNode.setRefType(parentNode.getRefType());
		thisNode.setTreePid(parentId);
		thisNode.setTreeName("untitled");
		int i = 1;
		i = dealSort(i, parentId);
		thisNode.setSort(i);
		TaUser user = SlSessionHelper.getUser();
		thisNode.setCreateUserId(user.getUserId());
		thisNode.setCreateUserName(user.getUserName());
		thisNode.setCreateInstId(user.getInstId());
		thisNode.setCreateInstName(user.getInstName());
		thisNode.setCreateTime(DateUtil.getCurrentTimeAsString());
		attachTreeMapMapper.insert(thisNode);
		return thisNode;
	}

	@Override
	@AutoLogMethod(value = "修改附件树")
	public TdAttachTreeMap updateAttachTreeMap(String id, String text) {
		TdAttachTreeMap tdAttachTreeMap = new TdAttachTreeMap();
		tdAttachTreeMap.setTreeId(id);
		tdAttachTreeMap = attachTreeMapMapper.selectByPrimaryKey(tdAttachTreeMap);
		tdAttachTreeMap.setTreeName(StringUtil.isNotEmpty(text)?text:"untitled");
		TaUser user = SlSessionHelper.getUser();
		tdAttachTreeMap.setUpdateUserId(user.getUserId());
		tdAttachTreeMap.setUpdateUserName(user.getUserName());
		tdAttachTreeMap.setUpdateInstId(user.getInstId());
		tdAttachTreeMap.setUpdateInstName(user.getInstName());
		tdAttachTreeMap.setUpdateTime(DateUtil.getCurrentTimeAsString());
		attachTreeMapMapper.updateByPrimaryKeySelective(tdAttachTreeMap);
		return tdAttachTreeMap;
	}

	@Override
	@AutoLogMethod(value = "删除附件树")
	public void deleteAttachTreeMap(String id) {
		String userId = SlSessionHelper.getUserId();
		TdAttachTreeMap tdAttachTreeMap = new TdAttachTreeMap();
		tdAttachTreeMap.setTreeId(id);
		tdAttachTreeMap = attachTreeMapMapper.selectByPrimaryKey(tdAttachTreeMap);
		if (!userId.equals(tdAttachTreeMap.getCreateUserId())) {
			JY.raise("非本人创建节点不允许删除。");
		}
		List<TcAttach> attachList =  attachMapper.selectTcAttach(tdAttachTreeMap.getRefType(), id);
		if (attachList != null && attachList.size() > 0) {
			JY.raise("不允许删除已上传附件的节点。");
		}
		attachTreeMapMapper.deleteTdAttachTreeMapByIdCascade(id);
	}

	@Override
	@AutoLogMethod(value = "拖拽附件树")
	public void dndAttachTreeMap(String id, String targetId, String point) {
		//根据节点取当前节点信息
		TdAttachTreeMap thisNode = new TdAttachTreeMap();
		thisNode.setTreeId(id);
		thisNode = attachTreeMapMapper.selectByPrimaryKey(thisNode);
		//根据目标节点编号取出目标节点信息
		TdAttachTreeMap targetNode = new TdAttachTreeMap();
		targetNode.setTreeId(targetId);
		targetNode = attachTreeMapMapper.selectByPrimaryKey(targetNode);
		if ("append".equals(point)) {
			//修改当前节点的父节点编号为目标节点的节点编号
			thisNode.setTreePid(targetId);
			int i = 1;
			i = dealSort(i, targetId);
			thisNode.setSort(i);
			attachTreeMapMapper.updateByPrimaryKey(thisNode);
		} else if ("top".equals(point)) {
			//修改排序
			int i = 1;
			thisNode.setTreePid(targetNode.getTreePid());
			thisNode.setSort(i++);
			i = dealSort(i, targetNode.getTreePid());
			attachTreeMapMapper.updateByPrimaryKey(thisNode);
		} else if ("bottom".equals(point)) {
			//修改排序
			int i = 1;
			i = dealSort(i, targetNode.getTreePid());
			thisNode.setTreePid(targetNode.getTreePid());
			thisNode.setSort(i);
			attachTreeMapMapper.updateByPrimaryKey(thisNode);
		}
	}

	@Override
	public void clearAttachTreeMap() {
		attachTreeMapMapper.clearTdAttachTreeMap();
	}
	/***
	 * ----以上为对外服务-------------------------------
	 */
	private int dealSort (int i, String parentId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("treePid", parentId);
		List<TdAttachTreeMap> attachTreeMapList = attachTreeMapMapper.selectTdAttachTreeMap(params);
		for (TdAttachTreeMap atm : attachTreeMapList) {
			atm.setSort(i++);
			attachTreeMapMapper.updateByPrimaryKey(atm);
		}
		return i;
	}
	
	private void copyAttachTree (TcAttachTreeTemplate attachTreeTemplate, TdAttachTreeMap attachTreeMap) {
		attachTreeMap.setTreeName(attachTreeTemplate.getTreeName());
		attachTreeMap.setSort(attachTreeTemplate.getSort());
		//attachTreeMap.setTreePid(attachTreeMap.getTreeId());
		attachTreeMapMapper.insert(attachTreeMap);
		List<TcAttachTreeTemplate> childrens = attachTreeTemplate.getChildren();
		if (childrens != null && childrens.size() > 0) {
			for (TcAttachTreeTemplate children : childrens) {
				copyAttachTree(children, attachTreeMap);
			}
		}
	}

	@Override
	public String getNewId() {
		return attachTreeMapMapper.idGeneriate();
	}
	
	/**
	 * 
	 * @param hashList
	 * @param rootId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static final List<TcAttachTreeTemplate> copy(List<TcAttachTreeTemplate> hashList, String rootId, TreeIdGenerater idGenerate, String refId) {
		List<TcAttachTreeTemplate> platList = new LinkedList<TcAttachTreeTemplate>(); 
		for (TcAttachTreeTemplate attachTreeMap : hashList) {
			attachTreeMap.setRefId(refId);
			attachTreeMap.setTreeId(idGenerate.getNewId());
			attachTreeMap.setTreePid(rootId);
			platList.add(attachTreeMap);
			if(attachTreeMap.children()!=null && attachTreeMap.children().size()>0){
				iterator(attachTreeMap.getId(), (List<TcAttachTreeTemplate>) attachTreeMap.children(), idGenerate, refId, platList);
			}
		}
		return platList;
	}

	@SuppressWarnings("unchecked")
	private static final void iterator(String id, List<TcAttachTreeTemplate> children, TreeIdGenerater idGenerate, String refId, List<TcAttachTreeTemplate> platList) {
		for (TcAttachTreeTemplate attachTreeMap : children) {
			attachTreeMap.setRefId(refId);
			attachTreeMap.setTreeId(idGenerate.getNewId());
			attachTreeMap.setTreePid(id);
			platList.add(attachTreeMap);
			if(attachTreeMap.children()!=null && attachTreeMap.children().size()>0){
				iterator(attachTreeMap.getId(), (List<TcAttachTreeTemplate>) attachTreeMap.children(), idGenerate, refId, platList);
			}
		}
			
	}
}
