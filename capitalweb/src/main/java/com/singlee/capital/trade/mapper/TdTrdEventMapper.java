package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdTrdEvent;

import tk.mybatis.mapper.common.Mapper;

public interface TdTrdEventMapper extends Mapper<TdTrdEvent>{

	List<TdTrdEvent> searchTrdEventByKey(Map<String, Object> params);
	/**
	 * 根据交易流水获得改交易项下的存续期活动
	 * @param params
	 * @return
	 */
	List<TdTrdEvent> getTrdEventDistinctForDealNo(Map<String, Object> params);
}
