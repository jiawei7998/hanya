package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductCustCredit;

/**
 * @projectName 同业业务管理系统
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface ProductCustCreditService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 */
	public List<TdProductCustCredit> getTdProductCustCreditList(Map<String, Object> params);
	/**
	 * 通过参与方配置的额度判断表达式判断哪些客户需要进入额度明细待占用表
	 * @param productApproveMain 交易单
	 * @param tdSubsMap 交易对应参与方，KEY VALUE值
	 * @return
	 */
	public List<TdProductCustCredit> getProductCustCreditForDeal(TdProductApproveMain productApproveMain,Map<String, String> tdSubsMap) throws Exception;
	/**
	 * 判断产品对应的准入情况
	 * @param params
	 * @return
	 */
	public boolean getProductJudgeAccessLevel(Map<String, Object> params);
	/**
	 * 额度输入参数
	 * @param params
	 * @return
	 */
	public List<EdInParams> getEdInParamsForProductCustCredit(Map<String, Object> params);
}
