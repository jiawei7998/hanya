package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.FinEnterpriseTrade;
import com.singlee.capital.trade.model.TdDurationCommon;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统
 * @className 产品审批主表持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-9 上午10:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdProductApproveMainMapper extends Mapper<TdProductApproveMain> {

	
	/**
	 * 根据交易单号查询当前激活的交易单
	 * 
	 * @param dealNo - 产品审批主表主键
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public TdProductApproveMain getProductApproveMainActivated(String dealNo);
	
	/**
	 * 根据交易单号查询最新版本的交易单
	 * 
	 * @param params - dealNo sort order
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-9-26
	 */
	public TdProductApproveMain getProductApproveMainByCondition(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdProductApproveMain> getProductApproveMainList(Map<String, Object> params, RowBounds rb);

	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdProductApproveMain> getProductApproveMainListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表 (已经审批)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdProductApproveMain> getProductApproveMainFinishList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public List<TdProductApproveMain> getProductApproveMainList(Map<String, Object> params);
	
	/**
	 * 根据icodes获取列表
	 * @param params
	 * @return
	 */
	public List<TdProductApproveMain> getProductApproveMainByIcodes(Map<String, Object> params);
	/**
	 * 更新状态
	 * @param main
	 */
	public int updateMainStatus(TdProductApproveMain main);
	
	/**
	 * 获取最大版本号 version
	 * @param main
	 * @return
	 */
	public int selectMaxVersion(TdProductApproveMain main);
	
	/**
	 * iCode aType mType version
	 * @param params
	 * @return
	 */
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params);
	
	
	/**
	 * 根据请求参数查询存续期列表
	 * 
	 * @param params - 请求参数
	 * @return 存续期公共对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public List<TdDurationCommon> getDurationCommonList(Map<String, Object> params);
	
	
	/**
	 * 更新状态
	 * @param main
	 */
	public int updateMainRefNo(TdProductApproveMain main);
	
	public FinEnterpriseTrade selectFinEnterpriseApproveTrade(Map<String, Object> map) throws Exception;
	
	
	/**
	 * 查询审批单据用于撤销操作
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<TdProductApproveMain> getOrderVoPageForCancel(Map<String, Object> params, RowBounds rb);
	/**
	 * 数据移型使用
	 * @param params
	 * @return
	 */
	public List<TdProductApproveMain> getAllProductApproveMainForActive(Map<String, Object> params);
	
	
	/**
	 * 根据LD号来查询TD_PRODUCT_APPROVE_MAIN表
	 * @param params
	 * @return
	 */
	public TdProductApproveMain queryMainListforLdNo(String ldNo);
	
	/**
	 * LD号补录
	 * @param params
	 * @param rb
	 * @return
	 */
	public void updateMainLdnoByDealno(TdProductApproveMain main);
	
	/**
	 * 根据LD号查询当前激活的交易单
	 * 
	 * @param ldno - LD号
	 * @return 产品审批主表对象
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public List<TdProductApproveMain> getProductApproveMainForLdNo();
	
	
	public TdProductApproveMain getProductApproveMainByLdNoActive(String ldNo);
	/**
	 * 找出全部LD还本计划
	 * @param postdate
	 * @return
	 */
	public List<TdProductApproveMain> getProductApproveMainListForLds(String postdate);
	/**
	 * 查詢獲得所有transfor的交易數據
	 * @return
	 */
	public List<TdProductApproveMain> getAllTransforDeals();
	
	public Page<TdProductApproveMain> selectGetOutProFinishList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 检查产品编号唯一性
	 */
	public Integer checkProductCode(Map<String, Object> map);
	
	public void updateContractRate(Map<String, Object> params);
	
	/**
	 * 检查用户是否有审批权限
	 */
	public Integer getProductApproveMainListCount(Map<String, Object> params);
	
	public int updateProductApproveMain(Map<String, Object> params);
	
}