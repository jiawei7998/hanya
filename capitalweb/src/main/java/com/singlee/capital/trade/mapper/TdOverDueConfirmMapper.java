package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdOverDueConfirm;

/***
 * 
 * 交易逾期
 * @author kf0738
 *
 */
public interface TdOverDueConfirmMapper extends Mapper<TdOverDueConfirm> {

	
	/**
	 * 根据交易号判断交易是
	 * 否逾期
	 * 
	 * @param dealNo - 交易单号
	 * @author 
	 */
	public TdOverDueConfirm queryOverDueDealByDealno(String dealNo);
	
	/**
	 * 根据交易号判断交易是
	 * 否逾期
	 * 
	 * @param dealNo - 交易单号
	 * @author 
	 */
	public Integer queryOverDueDealCountByDealno(String dealNo);
	
	/*****
	 * 根据原交易编号关联查询对应的逾期交易
	 * @param refNo
	 * @return
	 */
	public TdOverDueConfirm queryOverDueDealByRefNo(String refNo);
	
	/****
	 * 查询需要收款和已经逾期的交易数量
	 * @param params
	 * @return
	 */
	public List<TdOverDueConfirm> queryOverDueDealNums(Map<String, Object> params);
	
	/****
	 * 从交易金融工具主表中查询逾期交易
	 * @param params
	 * @return
	 */
	public List<TdOverDueConfirm> queryOverDueDeals(Map<String, Object> params);
	
	/****
	 * 更新逾期交易信息
	 * @param overDueConfirm
	 */
	public void updateOverDueDeal(TdOverDueConfirm overDueConfirm);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdOverDueConfirm> getTradeOverDueList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdOverDueConfirm> getTradeOverDueListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdOverDueConfirm> getTradeOverDueListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param tdOverDueConfirm
	 * @return
	 */
	public TdOverDueConfirm searchTradeOverDue(TdOverDueConfirm tdOverDueConfirm);
	
	public TdOverDueConfirm getOverDueConfirmById(String dealNo);

	/****
	 * 保存逾期交易
	 * @param params
	 */
	public void insertOverDueDealMap(Map<String, Object> params);
	
	
	/**
	 * 根据 RefNo version 查询所有历史生效的 逾期交易
	 * @param params
	 * @return
	 */
	public Page<TdOverDueConfirm>  getAllOverdueConfirmsByDealnoVersion(Map<String, Object> params,RowBounds rb);
}