package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductFeeDeal;

public interface TdProductFeeDealMapper extends Mapper<TdProductFeeDeal>{
	/**
	 * 删除所有交易关联的费用交易
	 * @param paramsMap
	 */
	public void deleteProductFeeDeals(Map<String, Object> paramsMap);
	
	public List<TdProductFeeDeal> getAllProductFeeDeals(Map<String, Object> paramsMap) ;
	
	public Page<TdProductFeeDeal> getPageProductFeeDeals(Map<String, Object> params, RowBounds rb);
	/**
	 * 插入FEE交易返回FEE交易号
	 * @param tdProductFeeDeal
	 */
	public void insetProductFeeDeal(TdProductFeeDeal tdProductFeeDeal);
	
	/**
	 * 拷贝
	 * @param paramsMap
	 */
	public void insetProductFeeDealByDealNoCopy(Map<String, Object> paramsMap);
	
	/**
	 * 根据FEE DEALNO 查询
	 * @param paramsMap
	 * @return
	 */
	public TdProductFeeDeal getTdProductFeeDealByFeeDealNo(Map<String, Object> paramsMap);

	/**
	 * 根据FEE DEALNO 修改
	 * @param paramsMap
	 * @return
	 */
	public void updateTdProductFeeDealByFeeDealNo(Map<String, Object> paramsMap);
}
