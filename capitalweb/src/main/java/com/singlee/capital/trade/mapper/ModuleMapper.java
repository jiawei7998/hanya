package com.singlee.capital.trade.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.Function;
import com.singlee.capital.trade.model.TaModule;


public interface ModuleMapper extends Mapper<TaModule>{
	/** 
	 * 递归获得所有 roldeIds 指定的 模块 
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModule(List<String> list);
	
	/** 
	 * 递归获得所有 roldeIds 指定的 模块 
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModuleAll();
	
	/**
	 * 根据id查询下列的子节点
	 */
	public List<TaModule> TaModuleById(String modulePid);
	
	/**
	 * 查询父菜单信息
	 * @return
	 */
	List<TaModule> searchParModule();
	
	/**
	 * 查询所有菜单
	 * @return
	 */
	//public List<TaModule> ModulesssList();
	List<TaModule> ModulesList();
	
	/**
	 * 新增菜单
	 * @param taModule
	 */
	public void insertTaModule(TaModule taModule);
	
	/**
	 * 修改菜单
	 * @param taModule
	 */
	public void updateTaModule(TaModule taModule);
	
	/**
	 * 删除菜单
	 * @param moduleIdss
	 */
	public void deleteTaModule(String moduleIdss);
	
	/**
	 * 根据父节点ModulePid查询下面的字节点ModuleId
	 */
	public List<String> queryTaModuleById(@Param("modulePid")String modulePid);
	
	
	/**
	 * 查询字节点ModuleId
	 * @param moduleId
	 * @return
	 */
	public List<String> queryTaModuleByIdS(@Param("moduleId") String moduleId);
	
	/**
	 * 根据id查询多少数据
	 */
	public Page<TaModule> queryAllById(Function function);
	
	
}
