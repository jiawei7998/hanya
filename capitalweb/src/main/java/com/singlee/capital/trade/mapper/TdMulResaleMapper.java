package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdMultiResale;

/**
 * @projectName 同业业务管理系统
 * @className 资产卖断持久层
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdMulResaleMapper extends Mapper<TdMultiResale> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 资产卖断对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdMultiResale getMulResaleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产卖断对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdMultiResale> getMulResaleList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产卖断对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdMultiResale> getMulResaleListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 资产卖断对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdMultiResale> getMulResaleListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param MulResale
	 * @return
	 */
	public TdMultiResale searchMulResale(TdMultiResale mulResale);
}