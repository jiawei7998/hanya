package com.singlee.capital.trade.model;

import java.io.Serializable;

public class ProductApproveParamVo implements Serializable {


	private static final long serialVersionUID = 1L;
	
	private String prdNo;
	private String prdName;
	private int    isNeedLoan;
	private double amtLine;
	
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public int getIsNeedLoan() {
		return isNeedLoan;
	}
	public void setIsNeedLoan(int isNeedLoan) {
		this.isNeedLoan = isNeedLoan;
	}
	public double getAmtLine() {
		return amtLine;
	}
	public void setAmtLine(double amtLine) {
		this.amtLine = amtLine;
	}

}
