package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdDurationToDo;

/**
 * @projectName 同业业务管理系统
 * @className 未到生效日的存续期业务记录
 * @description TODO
 * @author dzy
 * @createDate 2016-10-14 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface DurationToDoService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author dzy
	 * @date 2016-10-14
	 */
	public Page<TdDurationToDo> getDurationToDo(Map<String, Object> params);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author dzy
	 * @date 2016-10-14
	 */
	public void createDurationToDo(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author dzy
	 * @date 2016-10-14
	 */
	public void updateDurationToDo(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 主键列表
	 * @author dzy
	 * @date 2016-10-14
	 */
	public void deleteDurationToDo(String[] dealNos);
	
}
