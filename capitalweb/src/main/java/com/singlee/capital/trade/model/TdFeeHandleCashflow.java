package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 手工费用现金流
 * @description TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_FEE_HANDLE_CASHFLOW")
public class TdFeeHandleCashflow implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3720610986116412801L;
	/**
	 * 费用流水号
	 */
	@Id
	private String dealNo;
	/**
	 * 
	 */
	private String feeType;
	/**
	 * 
	 */
	private double feeAmt;
	/**
	 * 
	 */
	private String feeVdate;
	/**
	 * 
	 */
	private String feeDealDate;
	/**
	 * 
	 */
	private String feeRefNo;
	/**
	 * 
	 */
	private String feePrdNo;
	/**
	 * 
	 */
	private String feeEcifNo;
	
	@Transient
	private String feeEcifName;
	
	public String getFeeEcifName() {
		return feeEcifName;
	}
	public void setFeeEcifName(String feeEcifName) {
		this.feeEcifName = feeEcifName;
	}
	/**
	 * 
	 */
	private String dealType;
	/**
	 * 
	 */
	private String sponsor;
	/**
	 * 
	 */
	private String sponInst;
	/**
	 * 
	 */
	private String aDate;
	/**
	 * 
	 */
	private String lastUpdate;
	/**
	 * 
	 */
	private String trueRevDate;
	/**
	 * 
	 */
	private double trueRevAmt;
	/**
	 * 
	 */
	private String ownInst;
	/**
	 * 业务所属机构
	 */
	@Transient
	private String ownInstName;
	/**
	 * 
	 */
	private String remark;
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	//现金流类型
	private String cfType;
	
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public double getFeeAmt() {
		return feeAmt;
	}
	public void setFeeAmt(double feeAmt) {
		this.feeAmt = feeAmt;
	}
	public String getFeeVdate() {
		return feeVdate;
	}
	public void setFeeVdate(String feeVdate) {
		this.feeVdate = feeVdate;
	}
	public String getFeeDealDate() {
		return feeDealDate;
	}
	public void setFeeDealDate(String feeDealDate) {
		this.feeDealDate = feeDealDate;
	}
	public String getFeeRefNo() {
		return feeRefNo;
	}
	public void setFeeRefNo(String feeRefNo) {
		this.feeRefNo = feeRefNo;
	}
	public String getFeePrdNo() {
		return feePrdNo;
	}
	
	public String getFeeEcifNo() {
		return feeEcifNo;
	}
	public void setFeeEcifNo(String feeEcifNo) {
		this.feeEcifNo = feeEcifNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public void setFeePrdNo(String feePrdNo) {
		this.feePrdNo = feePrdNo;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getTrueRevDate() {
		return trueRevDate;
	}
	public void setTrueRevDate(String trueRevDate) {
		this.trueRevDate = trueRevDate;
	}
	public double getTrueRevAmt() {
		return trueRevAmt;
	}
	public void setTrueRevAmt(double trueRevAmt) {
		this.trueRevAmt = trueRevAmt;
	}
	public String getOwnInst() {
		return ownInst;
	}
	public void setOwnInst(String ownInst) {
		this.ownInst = ownInst;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOwnInstName() {
		return ownInstName;
	}
	public void setOwnInstName(String ownInstName) {
		this.ownInstName = ownInstName;
	}
	
}
