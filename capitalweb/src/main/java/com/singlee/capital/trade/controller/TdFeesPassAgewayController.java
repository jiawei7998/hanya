package com.singlee.capital.trade.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdFeesPassAgewayVo;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;


@Controller
@RequestMapping(value = "/TdFeesPassAgewayController")
public class TdFeesPassAgewayController  extends CommonController{

	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	public final static Logger log = LoggerFactory.getLogger("FIBS");
	@ResponseBody
	@RequestMapping(value = "/getTdFeesPassAgeways")
	public RetMsg<PageInfo<TdFeesPassAgeway>> getBaseAssetList(@RequestBody Map<String, Object> params) throws RException{
		Page<TdFeesPassAgeway> param = tdFeesPassAgewayService.getTdFeesPassAgewayLists(params);
		return RetMsgHelper.ok(param);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFeesDailyInterestList")
	public RetMsg<BigDecimal> getFeesDailyInterestList(@RequestBody Map<String, Object> params) throws RException{
		BigDecimal totalPassAgeway = new BigDecimal(0);
		params.put("intCal", "1");
		List<TdFeesPassAgewayDaily> fAgewayDailies = tdFeesPassAgewayService.getFeesDailyInterestList(params);
		for(TdFeesPassAgewayDaily fp: fAgewayDailies) {
			totalPassAgeway = totalPassAgeway.add(BigDecimal.valueOf(fp.getInterest()));
		}
		return RetMsgHelper.ok(totalPassAgeway);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getBaseAssetListVo")
	public RetMsg<List<TdFeesPassAgewayVo>> getBaseAssetListVo(@RequestBody Map<String, Object> params) throws RException{
		List<TdFeesPassAgewayVo> param = tdFeesPassAgewayService.getTdFeesPassAgewayListVo(params);
		return RetMsgHelper.ok(param);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getSumFeeRateByRefNo")
	public RetMsg<BigDecimal> getSumFeeRateByRefNo(@RequestBody Map<String, Object> params) throws RException{
		BigDecimal sumFeeRate = new BigDecimal(0);
		double feeRate = tdFeesPassAgewayService.getSumFeeRateByRefNo(params);
		sumFeeRate = BigDecimal.valueOf(feeRate).setScale(6,RoundingMode.HALF_UP);
		return RetMsgHelper.ok(sumFeeRate);
	}
	/**
	 * 获取两段日期之间通道费用
	 * @param params
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/getSumFeeRateByRefNoAndDay")
	public RetMsg<BigDecimal> getSumFeeRateByRefNoAndDay(@RequestBody Map<String, Object> params) throws RException{
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("refNo", ParameterUtil.getString(params, "refNo", ""));
		map.put("refBeginDate", ParameterUtil.getString(params, "refBeginDate", ""));
		map.put("refEndDate",ParameterUtil.getString(params, "refEndDate", ""));
		map.put("intCal", "1");
		BigDecimal sumFeeRate =tdFeesPassAgewayService.getSumInterest(map);
		return RetMsgHelper.ok(sumFeeRate);
	}
	
}
