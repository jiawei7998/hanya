package com.singlee.capital.trade.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TtTrdAccCurrentMapper;
import com.singlee.capital.trade.model.TtTrdAccCurrent;
import com.singlee.capital.trade.service.AccCurrentService;

/**
 * @className 附件树服务
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AccCurrentServiceImpl implements AccCurrentService {

	@Autowired
	private TtTrdAccCurrentMapper dueMapper;
	
	@Override
	public TtTrdAccCurrent getAccCurrentById(String adjId) {
		return dueMapper.selectByPrimaryKey(adjId);
	}

	@Override
	public Page<TtTrdAccCurrent> getAccCurrentPage(Map<String, Object> params, int isFinished) {
		//状态
		String approveStatus = ParameterUtil.getString(params, "approveStatus", null);
		if(approveStatus != null && !"".equals(approveStatus)){
			String [] status = approveStatus.split(",");
			params.put("approveStatus", Arrays.asList(status));
		}else{
			params.put("approveStatus", null);
		}
		//审批类型
		String approveType = ParameterUtil.getString(params, "approveType", null);
		if(approveType != null && !"".equals(approveType)){
			String [] approveTypes = approveType.split(",");
			params.put("approveType", Arrays.asList(approveTypes));
		}else{
			params.put("approveType", null);
		}
		//账户类型
		String accType = ParameterUtil.getString(params, "accType", null);
		if(accType != null && !"".equals(accType)){
			String [] accTypes = accType.split(",");
			params.put("accType", Arrays.asList(accTypes));
		}else{
			params.put("accType", null);
		}
		if(isFinished == 1){
			return dueMapper.getAccCurrentList(params, ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			return dueMapper.getAccCurrentListFinished(params, ParameterUtil.getRowBounds(params));
		}else {
			return dueMapper.getAccCurrentListMine(params, ParameterUtil.getRowBounds(params));
		}
	}

	@Override
	@AutoLogMethod(value = "创建存放同业表")
	public void createAccCurrent(Map<String, Object> params) {
		dueMapper.insert(accCurrent(params));
	}
	
	@Override
	@AutoLogMethod(value = "修改存放同业表")
	public void updateAccCurrent(Map<String, Object> params) {
		dueMapper.updateByPrimaryKey(accCurrent(params));
	}
	
	private TtTrdAccCurrent accCurrent(Map<String, Object> params){
		// map转实体类
		TtTrdAccCurrent accCurrent = new TtTrdAccCurrent();
		try {
			BeanUtil.populate(accCurrent, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		String type = ParameterUtil.getString(params, "type", "");

		// 初始化表数据
		accCurrent.setApproveStatus(DictConstants.ApproveStatus.New);
		accCurrent.setIsActive(DictConstants.YesNo.YES);
       
		//审批完成，为交易单添加一条数据
		if("add".equals(type)){
			accCurrent.setCreateTime(DateUtil.getCurrentDateTimeAsString());
			accCurrent.setCreateUser(SlSessionHelper.getUserId());
		}
		accCurrent.setUpdateTime(DateUtil.getCurrentDateTimeAsString());
		accCurrent.setUpdateUser(SlSessionHelper.getUserId());
		return accCurrent;
	}
	
	/**
	 * 删除
	 */
	@Override
	public void deleteAccCurrent(String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			dueMapper.deleteByPrimaryKey(ids[i]);
		}
	}

	/**
	 * 获取序列
	 * 
	 * @return java.lang.String
	 */
	@Override
	public String getSeq() {
		int seq = dueMapper.getSeq();
		return "C" + DateUtil.getCurrentCompactDateTimeAsString() + seq;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
