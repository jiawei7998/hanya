package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 基础资产
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASE_ASSET_BL")
public class TdBaseAssetBl implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer version;
	
	/*****************基础资产列表********************/
	private String shoreHolderType;//基础资产类型
	private String baseProductType;//股东性质
	private String trustType;//基础资产增信方式
	private String trustRemark;//基础资产增信描述
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getShoreHolderType() {
		return shoreHolderType;
	}
	public void setShoreHolderType(String shoreHolderType) {
		this.shoreHolderType = shoreHolderType;
	}
	public String getBaseProductType() {
		return baseProductType;
	}
	public void setBaseProductType(String baseProductType) {
		this.baseProductType = baseProductType;
	}
	public String getTrustType() {
		return trustType;
	}
	public void setTrustType(String trustType) {
		this.trustType = trustType;
	}
	public String getTrustRemark() {
		return trustRemark;
	}
	public void setTrustRemark(String trustRemark) {
		this.trustRemark = trustRemark;
	}
	
}
