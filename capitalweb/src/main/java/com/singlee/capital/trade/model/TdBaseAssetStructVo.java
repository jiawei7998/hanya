package com.singlee.capital.trade.model;

public class TdBaseAssetStructVo extends TdBaseAssetStruct{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  bigKindName;  
	private String  midKindName; 
	private String  smallKindName;
	public String getBigKindName() {
		return bigKindName;
	}
	public void setBigKindName(String bigKindName) {
		this.bigKindName = bigKindName;
	}
	public String getMidKindName() {
		return midKindName;
	}
	public void setMidKindName(String midKindName) {
		this.midKindName = midKindName;
	}
	public String getSmallKindName() {
		return smallKindName;
	}
	public void setSmallKindName(String smallKindName) {
		this.smallKindName = smallKindName;
	}   

	
	
	
}
