package com.singlee.capital.trade.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.Function;
import com.singlee.capital.trade.model.ModuleFunctionMap;

/**
 * 菜单功能点mapper
 * @author Administrator
 *
 */
public interface ModuleFunctionMapMapper extends Mapper<ModuleFunctionMap>{
	/**
	 * 添加菜单功能点信息
	 * @param moduleFunctionMap
	 */
	public void insertModuleFunctionMap(ModuleFunctionMap moduleFunctionMap);
	
	/**
	 * 根据菜单子节点删除菜单功能点信息
	 * @param moduleId
	 */
	public void deleteModuleFunctionMapById(String moduleId);
	
	/**
	 * 根据功能点对象删除菜单功能点信息
	 * @param function
	 */
	public void deleteModuleFunctionById(Function function);
}
