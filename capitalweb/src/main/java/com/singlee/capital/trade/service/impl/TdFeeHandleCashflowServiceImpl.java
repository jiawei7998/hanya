package com.singlee.capital.trade.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdFeeHandleCashflowMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdFeeHandleCashflow;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeeHandleCashflowService;
import com.singlee.slbpm.service.FlowOpService;

/**
 * @className 提前到期
 * @description TODO
 * @author Hunter
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdFeeHandleCashflowServiceImpl extends AbstractDurationService implements TdFeeHandleCashflowService {

	@Autowired
	private TdFeeHandleCashflowMapper feeHandleCashflowMapper;

	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	BatchDao batchDao;
	@Autowired
	CashflowFeeService cashflowFeeService;
	@Override
	public TdFeeHandleCashflow getFeeHandleCashflowById(String dealNo) {
		return feeHandleCashflowMapper.getFeeHandleCashflowById(dealNo);
	}

	@Override
	public Page<TdFeeHandleCashflow> getFeeHandleCashflowPage(
			Map<String, Object> params, int isFinished) {
		String feeStatus = ParameterUtil.getString(params, "feeStatus", null);
		Page<TdFeeHandleCashflow> page= new Page<TdFeeHandleCashflow>();
		if(feeStatus != null && !"".equals(feeStatus)){
			String [] feeStatuss = feeStatus.split(",");
			params.put("feeStatus", Arrays.asList(feeStatuss));
		}else
		{
			params.put("feeStatus", null);
		}
		if(isFinished == 1){
			page=feeHandleCashflowMapper.getFeeHandleCashflowList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){
			page=feeHandleCashflowMapper.getFeeHandleCashflowListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {
			page=feeHandleCashflowMapper.getFeeHandleCashflowListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建提前到期金融工具表")
	public void createFeeHandleCashflow(Map<String, Object> params) {
		feeHandleCashflowMapper.insert(feeHandleCashflow(params));
	}

	@Override
	@AutoLogMethod(value = "修改提前到期金融工具表")
	public void updateFeeHandleCashflow(Map<String, Object> params) {
		feeHandleCashflowMapper.updateByPrimaryKey(feeHandleCashflow(params));
	}

	
	private TdFeeHandleCashflow feeHandleCashflow(Map<String, Object> params){
		// map转实体类
		TdFeeHandleCashflow feeHandleCashflow = new TdFeeHandleCashflow();
		try {
			BeanUtil.populate(feeHandleCashflow, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		feeHandleCashflow.setSponsor(SlSessionHelper.getUserId());
		feeHandleCashflow.setSponInst(SlSessionHelper.getInstitutionId());
		feeHandleCashflow.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		feeHandleCashflow.setCfType(DictConstants.TrdType.FeeCashFlowHandle_SINGLE);
		return feeHandleCashflow;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteFeeHandleCashflow(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	
	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdFeeHandleCashflow feeCashflow = new TdFeeHandleCashflow();
		BeanUtil.populate(feeCashflow, params);
		feeCashflow = feeHandleCashflowMapper.searchFeeHandleCashflow(feeCashflow);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(feeCashflow.getFeeRefNo());//原交易流水号
		tdTrdEvent.setVersion(0);//原交易版本
		tdTrdEvent.setEventRefno(feeCashflow.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.FeeCashFlowHandle_SINGLE);//利率变更
		tdTrdEvent.setEventTable("TD_FEE_HANDLE_CASHFLOW");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}
}
