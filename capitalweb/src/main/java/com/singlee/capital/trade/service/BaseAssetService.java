package com.singlee.capital.trade.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdBaseAssetStruct;
import com.singlee.capital.trade.model.TdBaseAssetStructVo;
import com.singlee.capital.trade.model.TdBaseAssetVo;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:56:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface BaseAssetService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdBaseAsset> getBaseAssetList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public Page<TdBaseAsset> getBaseAssetLists(Map<String, Object> params);

	public List<TdBaseAssetStruct> getBaseAssetStructList(Map<String, Object> params);

	public List<TdBaseAssetVo> searchBaseAssetListVo(Map<String, Object> params);

	public List<TdBaseAssetStructVo> getBaseAssetStructListVo(Map<String, Object> params);
	
	/**
	 * 根据交易单号查询一条数据
	 * @param dealNo
	 * @return
	 */
	public TdBaseAsset queryTdBaseAssetById(String dealNo);
	
	
	public boolean setTdBaseAssetPriceByExcel(String file);
	
	public String setBaseAssetByExcel(String type, InputStream is);
	
	public Page<TdBaseAsset> getBaseAssetHisLists(Map<String, Object> params);
	
}
