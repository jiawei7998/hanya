package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 交叉营销
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_CROSS_MARKETING")
public class TdCrossMarketing implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1009663233143672085L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 客户经理（营销人）
	 */
	private String manager;
	/**
	 * 客户经理姓名
	 */
	@Transient
	private String managerName;
	/**
	 * 营销机构
	 */
	private String marketingInst;
	/**
	 * 营销机构名称
	 */
	@Transient
	private String marketingInstName;
	/**
	 * 分成比例
	 */
	private Double splitRatio;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getMarketingInst() {
		return marketingInst;
	}
	public void setMarketingInst(String marketingInst) {
		this.marketingInst = marketingInst;
	}
	public String getMarketingInstName() {
		return marketingInstName;
	}
	public void setMarketingInstName(String marketingInstName) {
		this.marketingInstName = marketingInstName;
	}
	public Double getSplitRatio() {
		return splitRatio;
	}
	public void setSplitRatio(Double splitRatio) {
		this.splitRatio = splitRatio;
	}
}