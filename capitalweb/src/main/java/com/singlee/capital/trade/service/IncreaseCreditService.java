package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdIncreaseCredit;

/**
 * @projectName 同业业务管理系统
 * @className 增信措施服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:56:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface IncreaseCreditService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 增信措施对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdIncreaseCredit> getIncreaseCreditList(Map<String, Object> params);
	
}
