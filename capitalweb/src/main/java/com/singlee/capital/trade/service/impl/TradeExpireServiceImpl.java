package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TdTradeExpireMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTradeExpire;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TradeExpireService;

/**
 * 到期展期交易的服务处理
 * 到期展期的起息日期必须大于等于原交易到期日
 * 计划的起始日必须全部大于原交易到期日
 * @company 杭州新利科技有限公司
 * 
 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeExpireServiceImpl extends AbstractDurationService implements TradeExpireService {

	@Autowired
	private TdTradeExpireMapper tdTradeExpireMapper;

	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;

	
	
	@Override
	public TdTradeExpire getTradeExpireById(String dealNo) {
		return tdTradeExpireMapper.getTradeExpireById(dealNo);
	}

	@Override
	public Page<TdTradeExpire> getTradeExpirePage(
			Map<String, Object> params, int isFinished) {
		Page<TdTradeExpire> page= new Page<TdTradeExpire>();
		if(isFinished == 2){
			page= tdTradeExpireMapper.getTradeExpireList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= tdTradeExpireMapper.getTradeExpireListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= tdTradeExpireMapper.getTradeExpireListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTradeExpire> list =page.getResult();
		for(TdTradeExpire te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建资产到期金融工具表")
	public void createTradeExpire(Map<String, Object> params) {
		tdTradeExpireMapper.insert(tradeExpire(params));
	}

	@Override
	@AutoLogMethod(value = "修改资产到期金融工具表")
	public void updateTradeExpire(Map<String, Object> params) {
		tdTradeExpireMapper.updateByPrimaryKey(tradeExpire(params));
	}

	
	private TdTradeExpire tradeExpire(Map<String, Object> params){
		// map转实体类
		TdTradeExpire tradeExpire = new TdTradeExpire();
		try {
			BeanUtil.populate(tradeExpire, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTradeExpire Expire = new TdTradeExpire();
        Expire.setDealNo(String.valueOf(params.get("dealNo")));
        Expire.setDealType(tradeExpire.getDealType());
        Expire = tdTradeExpireMapper.searchTradeExpire(Expire);

        if(tradeExpire.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(tradeExpire.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
    		tradeExpire.setVersion(productApproveMain.getVersion());
        }
       
    	else if(Expire!=null){
    		tradeExpire.setaDate(Expire.getaDate());
    	}
    	tradeExpire.setSponsor(SlSessionHelper.getUserId());
    	tradeExpire.setSponInst(SlSessionHelper.getInstitutionId());
		return tradeExpire;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTradeExpire(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			tdTradeExpireMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}
	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTradeExpire tradeExpire = new TdTradeExpire();
		BeanUtil.populate(tradeExpire, params);
		tradeExpire = tdTradeExpireMapper.searchTradeExpire(tradeExpire);
		return productApproveService.getProductApproveActivated(tradeExpire.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTradeExpire tradeExpire = new TdTradeExpire();
		BeanUtil.populate(tradeExpire, params);
		tradeExpire.setDealNo(ParameterUtil.getString(params, "trade_id", null));
		tradeExpire = tdTradeExpireMapper.searchTradeExpire(tradeExpire);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(tradeExpire.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(tradeExpire.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(tradeExpire.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType("3001_TDEXP");//利率变更
		tdTrdEvent.setEventTable("TD_TRADE_Expire");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		TdTradeExpire tradeExpire = new TdTradeExpire();
		BeanUtil.populate(tradeExpire, params);
		
		TdProductApproveMain approveMain = productApproveMainMapper.getProductApproveMainActivated(tradeExpire.getRefNo());
		//更新原交易一些字段
		Map<String,Object> map  =new HashMap<String, Object>();
		if(approveMain != null){
			map.put("dealNo", approveMain.getDealNo());//
			map.put("occupTerm", tradeExpire.getExpOccupTerm());//占款期限
			map.put("term", tradeExpire.getExpTerm());//计息天数
			map.put("mInt", tradeExpire.getExpMInt());//到期利息
			map.put("mAmt", tradeExpire.getExpMAmt());//到期金额
			map.put("mDate", tradeExpire.getExpMDate());//到期日期
			map.put("acturalRate", tradeExpire.getExpRate());//实际利率
			map.put("contractRate", tradeExpire.getExpRate());//投资收益率_%
			tdTradeExpireMapper.updateProductApproveMain(map);
			
		}
	}
	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "com.singlee.capital.cashflow.mapper.Td";
        } else {
            return "com.singlee.capital.cashflow.mapper.Tm";
        }
	}
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "td";
        } else {
            return "tm";
        }
	}
}
