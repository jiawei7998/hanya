package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdRiskAsset;

/**
 * @projectName 同业业务管理系统
 * @className 风险资产服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:56:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface RiskAssetService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 风险资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdRiskAsset> getRiskAssetList(Map<String, Object> params);
	
	/**
	 * 查出风险等级代码
	 * 
	 * @param params - 请求参数
	 * @date 2016-12-05
	 */
	public String getRiskLevel(String params);
	
}
