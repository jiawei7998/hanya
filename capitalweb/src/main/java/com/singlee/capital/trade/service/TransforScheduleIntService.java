package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTransforScheduleInt;

/**
 * @projectName 同业业务管理系统

 * @version 1.0
 */
public interface TransforScheduleIntService {


	
	public TdTransforScheduleInt getTransforScheduleIntById(String dealNo);
	
	
	public Page<TdTransforScheduleInt> getTransforScheduleIntPage (Map<String, Object> params, int isFinished);
	
	public void createTransforScheduleInt(Map<String, Object> params);
	
	
	public void updateTransforScheduleInt(Map<String, Object> params);
	
	public void deleteTransforScheduleInt(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	/**
	 * 单独收息计划变更时使用的自动计算利息的方法
	 * @param map
	 * @return
	 */
	public double getAmIntCalForTrasforInterest(Map<String, Object> map);
	
	/**
	 * 展期交易时所需要自动计算利息的方法
	 * @param map
	 * @return
	 */
	public double getAmIntCalForInterest(Map<String, Object> map);
	
	/**
	 * 单独收息计划变更时使用的自动计算利息的方法
	 * @param map
	 * @return
	 */
	public CashflowInterest getAmIntCalForTrasforInterestExceptFee(Map<String, Object> map);
	
	public Map<String, Object> reSetInterestPlanForTemp(Map<String, Object> params)throws Exception;
	
}
