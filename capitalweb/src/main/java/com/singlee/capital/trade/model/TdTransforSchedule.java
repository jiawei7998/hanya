package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.cashflow.model.TmApproveCapital;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * 计划变更总表
 * @version 1.0
 */
@Entity
@Table(name = "TD_TRASFOR_SCHEDULE")
public class TdTransforSchedule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	/**
	 * 变更日期
	 */
	@Column(name="CHANGEDATE")
	private String changeDate;
	/**
	 * 变更原因
	 */
	@Column(name="CHANGEREASON")
	private String changeReason;
	/**
	 * 生效日期
	 */
	@Column(name="EFFECTDATE")
	private String effectDate;
	
	private String feeDealno;//费用流水
	
	public String getFeeDealno() {
		return feeDealno;
	}
	public void setFeeDealno(String feeDealno) {
		this.feeDealno = feeDealno;
	}
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	
	private int version;
	
	private double remainAmt;
	
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	
	@Transient
	private List<TmApproveCapital> approveCapitals;
	
	
	
	public List<TmApproveCapital> getApproveCapitals() {
		return approveCapitals;
	}
	public void setApproveCapitals(List<TmApproveCapital> approveCapitals) {
		this.approveCapitals = approveCapitals;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	
	public String getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
	public String getChangeReason() {
		return changeReason;
	}
	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}
	public String getEffectDate() {
		return effectDate;
	}
	public void setEffectDate(String effectDate) {
		this.effectDate = effectDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	@Override
	public String toString() {
		return "TdTransforSchedule [dealNo=" + dealNo + ", refNo=" + refNo
				+ ", dealType=" + dealType + ", sponsor=" + sponsor
				+ ", sponInst=" + sponInst + ", aDate=" + aDate
				+ ", changeDate=" + changeDate + ", changeReason="
				+ changeReason + ", effectDate=" + effectDate + ", feeDealno="
				+ feeDealno + ", lastUpdate=" + lastUpdate + ", version="
				+ version + ", approveStatus=" + approveStatus + ", user="
				+ user + ", institution=" + institution + ", taskId=" + taskId
				+ ", product=" + product + ", party=" + party + ", prdName="
				+ prdName + "]";
	}
	
}
