package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.mapper.TmApproveCapitalMapper;
import com.singlee.capital.cashflow.model.TdCashflowInterest;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.TmApproveCapital;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowDailyInterest;
import com.singlee.capital.cashflow.service.CashflowFeeService;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.mapper.TdTransforScheduleMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.model.TdTransforSchedule;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TransforScheduleService;
import com.singlee.slbpm.service.FlowOpService;

@Service
public class TransforScheduleServiceImpl extends AbstractDurationService implements TransforScheduleService {

	@Autowired
	private TdTransforScheduleMapper tdTransforScheduleMapper;
	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private TmApproveCapitalMapper approveCapitalMapper;
	@Autowired
	private TdCashflowCapitalMapper  cashflowCapitalMapper;
	@Autowired
	private TdCashflowInterestMapper cashflowInterestMapper;
	@Autowired
	private TdCashflowDailyInterestMapper cashflowDailyInterestMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TdRateRangeMapper rateRangeMapper;
	@Autowired
	CashflowFeeService cashflowFeeService;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	private TdProductFeeDealMapper productFeeDealMapper;
	
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	
	
	@Override
	public TdTransforSchedule getTransforScheduleById(String dealNo) {
		return tdTransforScheduleMapper.getTransforScheduleById(dealNo);
	}

	@Override
	public Page<TdTransforSchedule> getTransforSchedulePage(
			Map<String, Object> params, int isFinished) {
		Page<TdTransforSchedule> page= new Page<TdTransforSchedule>();
		if(isFinished == 2){
			page= tdTransforScheduleMapper.getTransforScheduleList(params,
					ParameterUtil.getRowBounds(params));
		}else if(isFinished == 1){
			page= tdTransforScheduleMapper.getTransforScheduleListFinished(params,
					ParameterUtil.getRowBounds(params));
		}
		else{
			page= tdTransforScheduleMapper.getTransforScheduleListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdTransforSchedule> list =page.getResult();
		for(TdTransforSchedule te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	@AutoLogMethod(value = "创建交易展期金融工具表")
	public void createTransforSchedule(Map<String, Object> params) {
		tdTransforScheduleMapper.insert(TransforSchedule(params));
	}

	@Override
	@AutoLogMethod(value = "修改交易展期金融工具表")
	public void updateTransforSchedule(Map<String, Object> params) {
		tdTransforScheduleMapper.updateByPrimaryKey(TransforSchedule(params));
	}

	
	private TdTransforSchedule TransforSchedule(Map<String, Object> params){
		// map转实体类
		TdTransforSchedule transforSchedule = new TdTransforSchedule();
		try {
			BeanUtil.populate(transforSchedule, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		// 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdTransforSchedule extend = new TdTransforSchedule();
        extend.setDealNo(String.valueOf(params.get("dealNo")));
        extend.setDealType(transforSchedule.getDealType());
        extend = tdTransforScheduleMapper.searchTransforSchedule(extend);

        if(transforSchedule.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(transforSchedule.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
            transforSchedule.setVersion(productApproveMain.getVersion());
            if(DictConstants.intType.chargeBefore.equalsIgnoreCase(productApproveMain.getIntType()))
            {
            	throw new RException(productApproveMain.getDealNo()+"为前收息业务,不允许做还本计划变更!");
            }
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(transforSchedule.getDealType())){
    		transforSchedule.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	else if(extend!=null){
    		transforSchedule.setaDate(extend.getaDate());
    	}
    	transforSchedule.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		//还本计划副本
		Object obj = transforSchedule.getApproveCapitals();
		if (obj != null) {
			approveCapitalMapper.deleteApproveCapitalList(params);
			List<TmApproveCapital> approveCapitals = FastJsonUtil.parseArrays(obj.toString(), TmApproveCapital.class);
			int count = 1;
			Collections.sort(approveCapitals);
			for (TmApproveCapital approveCapital : approveCapitals) {
				approveCapital.setSeqNumber(count);
				approveCapital.setDealNo(transforSchedule.getDealNo());
				approveCapital.setVersion(transforSchedule.getVersion());
				approveCapital.setRefNo(transforSchedule.getRefNo());
				approveCapital.setCfType(DictConstants.CashFlowType.Principal);
				approveCapital.setPayDirection("Recevie");
				approveCapital.setCfEvent("Normal");
				if(approveCapital.getRepaymentSdate() != null && approveCapital.getRepaymentSdate().length() > 10)
				{
					approveCapital.setRepaymentSdate(approveCapital.getRepaymentSdate().substring(0, 10));
				}
				approveCapitalMapper.insert(approveCapital);
				count++;
			}
		}
		transforSchedule.setSponsor(SlSessionHelper.getUserId());
		transforSchedule.setSponInst(SlSessionHelper.getInstitutionId());
		
		return transforSchedule;
	}
	
	/**
	 * 删除（提前支取+order）
	 */
	@Override
	public void deleteTransforSchedule(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdTransforScheduleMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	/**
	 * 获取原交易信息
	 */
	@Override
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map) {
		return productApproveService.getProductApproveMainByCondition(map);
	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdTransforSchedule TransforSchedule = new TdTransforSchedule();
		BeanUtil.populate(TransforSchedule, params);
		TransforSchedule = tdTransforScheduleMapper.searchTransforSchedule(TransforSchedule);
		return productApproveService.getProductApproveActivated(TransforSchedule.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdTransforSchedule TransforSchedule = new TdTransforSchedule();
		BeanUtil.populate(TransforSchedule, params);
		TransforSchedule = tdTransforScheduleMapper.searchTransforSchedule(TransforSchedule);
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(TransforSchedule.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(TransforSchedule.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(TransforSchedule.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomCashFlowChange);//
		tdTrdEvent.setEventTable("TD_TRASFOR_SCHEDULE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		/**
		 * 完成后需要将新的本金计划更新到数据库  TD_CASHFLOW_CAPITAL
		 * 重算未收息的计划表
		 */
		TdTransforSchedule cfChange = new TdTransforSchedule();//本金计划审批数据
		BeanUtil.populate(cfChange, params);
		//删除 TD_CASHFLOW_CAPITAL 中未真实收款的计划
		cashflowCapitalMapper.deleteForApproveNotActual(params);
		//插入审批通过的调整计划TM_APPROVE_CAPITAL
		approveCapitalMapper.insertApproveCapitalToTd(params);
		//调整利息计划
		// 实体化界面字段为TdProductApproveMain
		TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(cfChange.getRefNo());
		//获得界面还本计划 TABLE列表数据
		List<CashflowCapital> cashflowCapitals=cashflowCapitalMapper.getCapitalList(BeanUtil.beanToMap(productApproveMain));
		
		List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
		/**
		 * 筛选出所有的 利率区间
		 */
		List<TdRateRange> ranges = rateRangeMapper.getRateRanges(params);
		InterestRange interestRange = null;
		for(TdRateRange tdRateRange:ranges){
			interestRange = new InterestRange();
			interestRange.setStartDate(tdRateRange.getBeginDate());
			interestRange.setEndDate(PlaningTools.addOrSubMonth_Day(tdRateRange.getEndDate(), Frequency.DAY, -1));
			interestRange.setRate(tdRateRange.getExecRate());
			interestRanges.add(interestRange);
		}
		List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
		PrincipalInterval principalInterval=null;double tempAmt =0f;
		int count =0 ; //将本金计划翻译成本金剩余区间
		Collections.sort(cashflowCapitals);
		for(CashflowCapital tmCashflowCapital : cashflowCapitals){
			principalInterval = new PrincipalInterval();
			principalInterval.setStartDate(count==0?productApproveMain.getvDate():cashflowCapitals.get(count-1).getRepaymentSdate());
			principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(tmCashflowCapital.getRepaymentSdate(), Frequency.DAY, -1));
			principalInterval.setResidual_Principal(PlaningTools.sub(productApproveMain.getAmt(), tempAmt));
			principalIntervals.add(principalInterval);
			tempAmt = PlaningTools.add(tempAmt, tmCashflowCapital.getRepaymentSamt());
			count++;
		}
		//利息明细
		List<CashflowDailyInterest> tdCashflowDailyInterests = new ArrayList<CashflowDailyInterest>();//返回的每日计提
		//获取利息计划(没有做过收息的交易)
		List<TdCashflowInterest>  cashflowInterests = cashflowInterestMapper.getInterestListForApprove(BeanUtil.beanToMap(productApproveMain));
		if(cashflowInterests == null || cashflowInterests.size() == 0)
		{
			return;
		}
		Collections.sort(cashflowInterests);
		
		HashMap<String, TdFeesPassAgewayDaily> passAwayMap = null;
		//重新计算通道计划 每日通道计提利息
		try {
			passAwayMap = tdFeesPassAgewayService.recalAllPassFeePlanByCapitalByType(principalIntervals,
					productApproveMain,cashflowInterests.get(0).getRefBeginDate(),"act");
		} catch (Exception e) {
			e.printStackTrace();
			JY.error("重新计算通道计划错误", e);
		}
		List<TdFeesPassAgewayDaily> fAgewayDailies = new ArrayList<TdFeesPassAgewayDaily>();
		if(passAwayMap != null){
			fAgewayDailies.addAll(passAwayMap.values());
		}
		
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("dealNo", productApproveMain.getDealNo());
		paramsMap.put("version", productApproveMain.getVersion());
		
		for(TdCashflowInterest cashflowInterest:cashflowInterests){
			try {
				PlaningTools.fun_Calculation_interval_except_fess(interestRanges, 
						principalIntervals, 
						tdCashflowDailyInterests, 
						cashflowInterest,cashflowInterests.get(0).getRefBeginDate(), 
						DayCountBasis.equal(cashflowInterest.getDayCounter()), fAgewayDailies);
				
				//更新该条收息计划数据
				cashflowInterestMapper.updateByPrimaryKey(cashflowInterest);
				/**
				 * <delete id="deleteCashflowDailyInterest" parameterType="map">
						delete from TD_CASHFLOW_DAILY_INTEREST WHERE 
						DEAL_NO=#{dealNo} AND VERSION=#{version}
						<if test="startDate != null and startDate != ''">
					    	<![CDATA[ and T.REF_BEGIN_DATE>=#{startDate}]]>
					    </if>
					    <if test="endDate != null and endDate != ''">
					    	<![CDATA[ and T.REF_END_DATE<#{endDate}]]>
					    </if>
					</delete>
				 */
				paramsMap.put("startDate", cashflowInterest.getRefBeginDate());
				paramsMap.put("endDate", cashflowInterest.getRefEndDate());
				//删除该区间的每日预计提数据
				cashflowDailyInterestMapper.deleteCashflowDailyInterest(paramsMap);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("重新计算应计利息异常"+e.getMessage());
			}
		}
		for(int i =0;i<tdCashflowDailyInterests.size();i++)
		{
			tdCashflowDailyInterests.get(i).setDealNo(productApproveMain.getDealNo());
			tdCashflowDailyInterests.get(i).setVersion(productApproveMain.getVersion());
		}
		if(tdCashflowDailyInterests.size() > 0)
		{
			batchDao.batch("com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper.insert", tdCashflowDailyInterests);
		}
		//费用计划修改
		/**
		 * 剔除移型帶來的費用計劃爲空的情況
		 */
		Page<TdProductFeeDeal> productFeeDeals = productFeeDealMapper.getPageProductFeeDeals(paramsMap, ParameterUtil.getRowBounds(paramsMap));
		if(null != productFeeDeals && null != productFeeDeals.getResult() && productFeeDeals.getResult().size()>0) {
            cashflowFeeService.updateByHandleCaptitalAmt(BeanUtil.beanToMap(productApproveMain), principalIntervals);
        }
	}

	public String getSqlForCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "com.singlee.capital.cashflow.mapper.Td";
        } else {
            return "com.singlee.capital.cashflow.mapper.Tm";
        }
	}
	public String getTitleCashFlow(Map<String, Object> params)
	{
		if(DictConstants.DealType.Verify.equals(ParameterUtil.getString(params, "dealType", ""))) {
            return "td";
        } else {
            return "tm";
        }
	}

}
