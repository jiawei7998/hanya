package com.singlee.capital.trade.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdAttachTreeMap;
import com.singlee.capital.trade.service.AttachTreeMapService;

/**
 * @projectName 同业业务管理系统
 * @className 附件树控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-16 上午11:39:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/AttachTreeMapController")
public class AttachTreeMapController extends CommonController {
	
	@Autowired
	private AttachTreeMapService attachTreeMapService;
	
	@ResponseBody
	@RequestMapping(value = "/searchAttachTreeMap")
	public RetMsg<List<TdAttachTreeMap>> searchAttachTreeMap(@RequestBody Map<String, Object> params) throws RException {
		List<TdAttachTreeMap> list = attachTreeMapService.searchAttachTreeMap(params);	
		return RetMsgHelper.ok(list);
	}

	@ResponseBody
	@RequestMapping(value = "/createAttachTreeMap")
	public RetMsg<TdAttachTreeMap> addAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String parentId = ParameterUtil.getString(request.getParameterMap(), "parentId", null);
		TdAttachTreeMap tdAttachTreeMap = attachTreeMapService.createAttachTreeMap(parentId);
		return RetMsgHelper.ok(tdAttachTreeMap);
	}

	@ResponseBody
	@RequestMapping(value = "/editAttachTreeMap")
	public RetMsg<TdAttachTreeMap> editAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		String text = ParameterUtil.getString(request.getParameterMap(), "text", null);
		TdAttachTreeMap tdAttachTreeMap = attachTreeMapService.updateAttachTreeMap(id, text);
		return RetMsgHelper.ok(tdAttachTreeMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "/destoryAttachTreeMap")
	public RetMsg<Serializable> removeAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		attachTreeMapService.deleteAttachTreeMap(id);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/dndAttachTreeMap")
	public RetMsg<Serializable> dndAttachTreeMap(HttpServletRequest request, HttpServletResponse response) throws RException {
		String id = ParameterUtil.getString(request.getParameterMap(), "id", null);
		String targetId = ParameterUtil.getString(request.getParameterMap(), "targetId", null);
		String point = ParameterUtil.getString(request.getParameterMap(), "point", null);
		attachTreeMapService.dndAttachTreeMap(id, targetId, point);
		return RetMsgHelper.ok();
	}
}
