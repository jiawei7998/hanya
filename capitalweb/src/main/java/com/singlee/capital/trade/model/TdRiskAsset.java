package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 风险资产
 * @description TODO
 * @author 倪航
 * @createDate 2016-10-11 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_RISK_ASSET")
public class TdRiskAsset implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9090679694147997187L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 序号
	 */
	@Id
	private Integer seq;
	/**
	 * 基础资产类型
	 */
	private String assetType;
	/**
	 * 系数
	 */
	private Double coefficient;
	/**
	 * 表内余额
	 */
	private Double inBalance;
	/**
	 * 基础资产余额（系统计算）
	 */
	private Double assetBalanceAuto;
	/**
	 * 基础资产余额（手工填写）
	 */
	private Double assetBalanceHand;
	/**
	 * 风险缓释额
	 */
	private Double rsrAmt;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 基础资产类型
	 */
	@Transient
	private String assetTypeName;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public Double getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}
	public Double getInBalance() {
		return inBalance;
	}
	public void setInBalance(Double inBalance) {
		this.inBalance = inBalance;
	}
	public Double getAssetBalanceAuto() {
		return assetBalanceAuto;
	}
	public void setAssetBalanceAuto(Double assetBalanceAuto) {
		this.assetBalanceAuto = assetBalanceAuto;
	}
	public Double getAssetBalanceHand() {
		return assetBalanceHand;
	}
	public void setAssetBalanceHand(Double assetBalanceHand) {
		if(assetBalanceHand == null){
			this.assetBalanceHand = Double.NaN;
		}else{
			this.assetBalanceHand = assetBalanceHand;
		}
	}
	public Double getRsrAmt() {
		return rsrAmt;
	}
	public void setRsrAmt(Double rsrAmt) {
		this.rsrAmt = rsrAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAssetTypeName() {
		return assetTypeName;
	}
	public void setAssetTypeName(String assetTypeName) {
		this.assetTypeName = assetTypeName;
	}
}
