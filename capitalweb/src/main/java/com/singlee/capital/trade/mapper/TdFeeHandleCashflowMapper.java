package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeeHandleCashflow;

/**
 * @projectName 同业业务管理系统
 * @className 手工费用
 * @description TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdFeeHandleCashflowMapper extends Mapper<TdFeeHandleCashflow> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdFeeHandleCashflow getFeeHandleCashflowById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdFeeHandleCashflow> getFeeHandleCashflowList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdFeeHandleCashflow> getFeeHandleCashflowListFinished(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表我的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdFeeHandleCashflow> getFeeHandleCashflowListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param advanceMaturity
	 * @return
	 */
	public TdFeeHandleCashflow searchFeeHandleCashflow(TdFeeHandleCashflow advanceMaturity);
}