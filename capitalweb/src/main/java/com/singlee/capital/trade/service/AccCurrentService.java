package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TtTrdAccCurrent;

/**
 * @projectName 同业业务管理系统
 * @className 提前到期服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface AccCurrentService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 提前到期主键
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TtTrdAccCurrent getAccCurrentById(String adjNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TtTrdAccCurrent> getAccCurrentPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 提前到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createAccCurrent(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateAccCurrent(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 提前到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteAccCurrent(String[] currentIds);

	/**
	 * 获取序列
	 * 
	 * @return java.lang.String
	 */
	public String getSeq();
	
}
