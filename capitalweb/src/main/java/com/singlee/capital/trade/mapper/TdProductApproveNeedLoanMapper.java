package com.singlee.capital.trade.mapper;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdProductApproveNeedLoan;

public interface TdProductApproveNeedLoanMapper extends Mapper<TdProductApproveNeedLoan> {
	
	public int isNeedLoan(@Param(value = "prd_no")String prd_no);
	
	public void deleteAllNeedLoan();
}
