package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdInterestSettle;

/**
 * @projectName 同业业务管理系统
 * @className 活期结息确认持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-11 下午2:04:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdInterestSettleMapper extends Mapper<TdInterestSettle> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 活期结息确认
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdInterestSettle getInterestSettleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 活期结息确认对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdInterestSettle> getInterestSettleList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 活期结息确认对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdInterestSettle> getInterestSettleListFinish(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param change
	 * @return
	 */
	public TdInterestSettle searchInterestSettle(TdInterestSettle change);
	
	/**
	 * 待审批
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdInterestSettle> getInterestSettleApproveList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 我发起列表
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TdInterestSettle> getInterestSettleMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据 RefNo version 查询所有历史生效的 活期结息确认
	 * @param params
	 * @return
	 */
	public Page<TdInterestSettle>  getAllInterestSettlesByDealnoVersion(Map<String, Object> params,RowBounds rb);
	/**
	 * 单个删除
	 * @param dealNo
	 */
	public void removeInterestSettle(@Param("dealNo")String dealNo);
}