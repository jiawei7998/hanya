package com.singlee.capital.trade.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.joyard.jc.service.ComputeService;
import com.joyard.jc.util.DateUtils;
import com.singlee.cap.base.model.TbkSubjectDef;
import com.singlee.cap.base.service.TbkSubjectDefService;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRec;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRq;
import com.singlee.capital.interfacex.model.TCoLoansIntInqRs;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRq;
import com.singlee.capital.interfacex.model.TExCurrencyAllInqRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.model.ProductApproveParamVo;
import com.singlee.capital.trade.model.TdAssetLc;
import com.singlee.capital.trade.model.TdAssetSpecificBond;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.model.TdBaseAssetBill;
import com.singlee.capital.trade.model.TdBaseAssetBl;
import com.singlee.capital.trade.model.TdBaseAssetGuarty;
import com.singlee.capital.trade.model.TdBaseAssetStruct;
import com.singlee.capital.trade.model.TdBaseAssetStructVo;
import com.singlee.capital.trade.model.TdBaseAssetVo;
import com.singlee.capital.trade.model.TdBaseRisk;
import com.singlee.capital.trade.model.TdCrossMarketing;
import com.singlee.capital.trade.model.TdDurationCommon;
import com.singlee.capital.trade.model.TdIncreaseCredit;
import com.singlee.capital.trade.model.TdProductApproveAmtLine;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductApproveProtocols;
import com.singlee.capital.trade.model.TdProductApproveSub;
import com.singlee.capital.trade.model.TdProductCustCredit;
import com.singlee.capital.trade.model.TdRiskAsset;
import com.singlee.capital.trade.model.TdRiskSlowRelease;
import com.singlee.capital.trade.model.TdStockInfo;
import com.singlee.capital.trade.service.AdvanceMaturityCurrentService;
import com.singlee.capital.trade.service.AdvanceMaturityService;
import com.singlee.capital.trade.service.BaseAssetBillService;
import com.singlee.capital.trade.service.BaseAssetBlService;
import com.singlee.capital.trade.service.BaseAssetGuartyService;
import com.singlee.capital.trade.service.BaseAssetService;
import com.singlee.capital.trade.service.BaseAssetSpecificBondService;
import com.singlee.capital.trade.service.BaseRiskService;
import com.singlee.capital.trade.service.CrossMarketingService;
import com.singlee.capital.trade.service.IncreaseCreditService;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.ProductCustCreditService;
import com.singlee.capital.trade.service.ProductOtherService;
import com.singlee.capital.trade.service.RateChangeService;
import com.singlee.capital.trade.service.RateResetService;
import com.singlee.capital.trade.service.RiskAssetService;
import com.singlee.capital.trade.service.RiskSlowReleaseService;
import com.singlee.capital.trade.service.StockInfoService;
import com.singlee.capital.trade.service.TdAssetLCService;
import com.singlee.capital.trade.service.TdFeesPassAgewayService;
import com.singlee.capital.trade.service.TdOverDueConfirmService;
import com.singlee.capital.trade.service.TdProductFeeCalService;
import com.singlee.capital.trade.service.TradeExpireService;
import com.singlee.capital.trade.service.TradeExtendService;
import com.singlee.capital.trade.service.TradeOffsetService;
import com.singlee.capital.trade.service.TradeOutrightSaleService;
import com.singlee.capital.trade.service.TransforScheduleFeeService;
import com.singlee.capital.trade.service.TransforScheduleIntService;
import com.singlee.capital.trade.service.TransforScheduleService;
import com.singlee.slbpm.pojo.bo.TdFlowTaskEvent;
import com.singlee.slbpm.service.FlowQueryService;
/**
 * @projectName 同业业务管理系统
 * @className 产品审批主表控制层
 * @description TODO
 * @author Hunter
 * @createDate 2016-8-10 上午11:18:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Controller
@RequestMapping(value = "/ProductApproveController")
public class ProductApproveController extends CommonController {
	@Autowired
	private BaseAssetGuartyService assetGuartyService;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired 
	private BaseAssetService baseAssetService;
	@Autowired 
	private BaseAssetBlService baseAssetBlService;
	@Autowired 
	private BaseAssetBillService baseAssetBillService;
	@Autowired 
	private StockInfoService stockInfoService;
	@Autowired 
	private RiskAssetService riskAssetService;
	@Autowired 
	private RiskSlowReleaseService riskSlowReleaseService;
	@Autowired 
	private CrossMarketingService crossSellingService;
	@Autowired 
	private IncreaseCreditService increaseCreditService;
	@Autowired
	private TdAssetLCService tdAssetLCService;
	@Autowired
	private BaseAssetSpecificBondService baseAssetSpecificBondService;
	@Resource
	private ComputeService computeService;
	@Autowired
	private TdProductFeeCalService productFeeCalService;
	@Autowired
	private ProductCustCreditService productCustCreditService;
	@Autowired
	private FlowQueryService flowQueryService;
	//基准利率查询
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private BaseRiskService baseRiskService;
	@Autowired
	private RateChangeService rateChangeService;
	@Autowired
	private TradeExtendService tradeExtendService;
	@Autowired
	private AdvanceMaturityService advanceMaturityService;
	@Autowired
	private TransforScheduleService transforScheduleService;
	@Autowired
	private TransforScheduleIntService transforScheduleIntService;
	@Autowired
	private TransforScheduleFeeService transforScheduleFeeService;
	@Autowired
	private TdOverDueConfirmService overDueConfirmService;
	@Autowired
	private AdvanceMaturityCurrentService advanceMaturityCurrentService;
	@Autowired
	private InterestSettleService interestSettleService;
	@Autowired
	private ProductOtherService productOtherService;
	
	@Autowired
	private TradeExpireService tradeExpireService;
	@Autowired
	private TradeOffsetService tradeOffsetService;
	@Autowired
	private RateResetService rateResetService;
	@Autowired
	private TdFeesPassAgewayService tdFeesPassAgewayService;
	@Autowired
	private TradeOutrightSaleService tradeOutrightSaleService;
	@Autowired
	private TbkSubjectDefService tbkSubjectDefService;
	/**
	 * dealNo查询当前激活的交易单
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveActivated")
	public RetMsg<TdProductApproveMain> getProductApproveActivated(@RequestBody TdProductApproveMain productApproveMain){
		return RetMsgHelper.ok(productApproveService.getProductApproveActivated(productApproveMain.getDealNo()));
	}
	
	/**
	 * dealNo查询带原交易单信息的交易单
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveForDuration")
	public RetMsg<TdProductApproveMain> searchProductApproveForDuration(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productApproveService.getProductApproveForDuration(params));
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveForCancel")
	public RetMsg<TdProductApproveMain> searchProductApproveForCancel(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productApproveService.getProductApproveForCancel(params));
	}
	/**
	 * 根据查询条件取得交易单信息
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveByCondition")
	public RetMsg<TdProductApproveMain> searchProductApproveByCondition(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productApproveService.getProductApproveByCondition(params));
	}
	
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApprove")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageProduct(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,3);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 返回可以审批撤销的单据列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getOrderVoForCancelPage")
	public RetMsg<PageInfo<TdProductApproveMain>> getOrderVoForCancelPage(@RequestBody Map<String,Object> params){
		String productType = ParameterUtil.getString(params, "custom_product_search", "");
		if(StringUtil.isNotEmpty(productType)){
			String[] PrdTypeArr =productType.split(",");
			params.put("PrdTypeList", Arrays.asList(PrdTypeArr));
		}
		Page<TdProductApproveMain> page = productApproveService.getApproveOrderVoForCancelPage(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTotalApprove")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageTotalApprove(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("dealType", DictConstants.DealType.Approve);
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageTotalVerify")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageTotalVerify(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("dealType", ParameterUtil.getString(params, "dealType", "2"));//DictConstants.DealType.Verify);
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchPageTotalVerify2")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageTotalVerify2(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("dealType", ParameterUtil.getString(params, "dealType", "2"));//DictConstants.DealType.Verify);
		if(params.get("verifyVDate") != null){
			String verifyVDate = String.valueOf(params.get("verifyVDate"));
			if(verifyVDate.length() > 10){
				params.put("verifyVDate", verifyVDate.substring(0, 10));
			}
		}
		
		if(params.get("verifyVDate2") != null){
			String verifyVDate2 = String.valueOf(params.get("verifyVDate2"));
			if(verifyVDate2.length() > 10){
				params.put("verifyVDate2", verifyVDate2.substring(0, 10));
			}
		}
		
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,99);
		return RetMsgHelper.ok(page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchDurationForDealNo")
	public RetMsg<?> searchDurationForDealNo(@RequestBody Map<String,Object> params){
		int prdType = ParameterUtil.getInt(params, "prdType", 0);
		String dealNo = ParameterUtil.getString(params, "dealNo", "0");
		
		switch (prdType) {
		case 7:
			return RetMsgHelper.ok(advanceMaturityService.getAdvanceMaturityById(dealNo));
		case 8:
			return RetMsgHelper.ok(tradeExtendService.getTradeExtendById(dealNo));
		case 9:
			return RetMsgHelper.ok(rateChangeService.getRateChangeById(dealNo));
		case 10://还本计划调整
			return RetMsgHelper.ok(transforScheduleService.getTransforScheduleById(dealNo));
		case 11:
			return RetMsgHelper.ok(transforScheduleIntService.getTransforScheduleIntById(dealNo));
		case 12:
			return RetMsgHelper.ok(transforScheduleFeeService.getTransforScheduleFeeById(dealNo));
		case 14:
			return RetMsgHelper.ok(overDueConfirmService.getOverDueConfirmById(dealNo));
		case 15:
			return RetMsgHelper.ok(advanceMaturityCurrentService.getAdvanceMaturityCurrentById(dealNo));
		case 16:
			return RetMsgHelper.ok(interestSettleService.getInterestSettleById(dealNo));
		
		case 17://资产到期 
			return RetMsgHelper.ok(tradeExpireService.getTradeExpireById(dealNo));
			
		case 18://交易冲正
			return RetMsgHelper.ok(tradeOffsetService.getTradeOffsetById(dealNo));
			
		case 19://利率重订
			return RetMsgHelper.ok(rateResetService.getRateChangeById(dealNo));
			
		case 20://通道计划
			return RetMsgHelper.ok(tdFeesPassAgewayService.queryTdFeesPassAgewayById(dealNo));
			
		case 21://资产转让
			return RetMsgHelper.ok(tradeOutrightSaleService.getTdOutrightSaleById(dealNo));
		
		case 22://资产卖断
			return RetMsgHelper.ok(tradeOutrightSaleService.getTdOutrightSaleById(dealNo));
			
		default:
			break;
		}
		return null;
	}
	/**
	 * 条件查询列表(我发起的)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveByMyself")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageProductApproveByMyself(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,3);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(未完成)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveUnfinished")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageProductApproveUnfinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse;
			params.put("approveStatusNo", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params,1);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 条件查询列表(已审批)
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageProductApproveFinished")
	public RetMsg<PageInfo<TdProductApproveMain>> searchPageProductApproveFinished(@RequestBody Map<String,Object> params){
		params.put("isActive", DictConstants.YesNo.YES);
		params.put("userId", SlSessionHelper.getUserId());
		
		String approveStatusNo = DictConstants.ApproveStatus.New;
		params.put("approveStatusNo", approveStatusNo.split(","));
		Page<TdProductApproveMain> page = productApproveService.getProductApprovePage(params, 2);
		return RetMsgHelper.ok(page);
	}
	
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 产品审批主表对象集合
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/removeProductApprove")
	public RetMsg<Serializable> removeProductApprove(@RequestBody String[] dealNos) {
		productApproveService.deleteProductApprove(dealNos);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 期限天数
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/getTimeLimit")
	@ResponseBody
	public long TimeLimit(@RequestBody Map<String,String> map){
		String startDate = map.get("startDate");
		String endDate = map.get("endDate");
		long time = DateUtils.days(startDate, endDate);
		return time;
	}
	
	/**
	 * 实际利率
	 * @return
	 */
	@RequestMapping(value="/getActuralRate")
	@ResponseBody
	public double actualRate(@RequestBody Map<String,String> map){
		String meDate = map.get("meDate"); 	//到期宽限日期
		String tDate = map.get("tDate");	//划款日期
		String vDate = map.get("vDate");	//起息日期
		String mDate = map.get("mDate");	//到期日期
		String basis = map.get("basis");	//计息基础
		double contractRate = ParameterUtil.getDouble(map, "contractRate" , 0); //合同利率
		double year1 = computeService.computeYear(basis, meDate, tDate);
		double year2 = computeService.computeYear(basis, mDate, vDate);
		if(year1==0 || year2 ==0){
			return 0;
		}else{
			double acturalRate = (contractRate*year2)/year1;
			return MathUtil.round(acturalRate, 4);
		}
	}
	
	/**
	 * 到期利息金额
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getM_int")
	@ResponseBody
	public double getM_int(@RequestBody Map<String,Double> map){
		double amt = map.get("amt");
		double rate = map.get("rate");
		double basis = map.get("basis");
		double term = map.get("term");
		double m_int = amt*rate*term/basis;
		return MathUtil.round(m_int, 2);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetList")
	public RetMsg<List<TdBaseAsset>> searchBaseAssetList(@RequestBody Map<String,Object> params){
		List<TdBaseAsset> list = baseAssetService.getBaseAssetList(params);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetGuartyList")
	public RetMsg<List<TdBaseAssetGuarty>> searchBaseAssetGuartyList(@RequestBody Map<String,Object> params){
		List<TdBaseAssetGuarty> list = assetGuartyService.getBaseAssetGuartyList(params);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 风险缓释表-保证担保/保单/保函保证
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseRiskList")
	public RetMsg<List<TdBaseRisk>> searchBaseRiskList(@RequestBody Map<String,Object> params){
		List<TdBaseRisk> list = baseRiskService.getBaseRiskList(params);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/searchbaseAssetBlInfoList")
	public RetMsg<List<TdBaseAssetBl>> searchbaseAssetBlInfoList(@RequestBody Map<String,Object> params){
		List<TdBaseAssetBl> list = baseAssetBlService.getBaseAssetBlList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetBillList")
	public RetMsg<List<TdBaseAssetBill>> searchStockInfoList(@RequestBody Map<String,Object> params){
		List<TdBaseAssetBill> list = baseAssetBillService.getBaseAssetBillList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchStockInfoList")
	public RetMsg<List<TdStockInfo>> searchBaseAssetBillList(@RequestBody Map<String,Object> params){
		List<TdStockInfo> list = stockInfoService.getStockInfoList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRiskAssetList")
	public RetMsg<List<TdRiskAsset>> searchRiskAssetList(@RequestBody Map<String,Object> params){
		List<TdRiskAsset> list = riskAssetService.getRiskAssetList(params);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 返回信用证列表
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetLCList")
	public RetMsg<List<TdAssetLc>> searchBaseAssetLCList(@RequestBody Map<String,Object> params){
		List<TdAssetLc> list = tdAssetLCService.getAssetLcList(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetSpecificBondList")
	public RetMsg<List<TdAssetSpecificBond>> searchBaseAssetSpecificBondList(@RequestBody Map<String,Object> params){
		List<TdAssetSpecificBond> list = baseAssetSpecificBondService.getTdAssetSpecificBondList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchRiskSlowReleaseList")
	public RetMsg<List<TdRiskSlowRelease>> searchRiskSlowReleaseList(@RequestBody Map<String,Object> params){
		List<TdRiskSlowRelease> list = riskSlowReleaseService.getRiskSlowReleaseList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCrossMarketingList")
	public RetMsg<List<TdCrossMarketing>> searchCrossSellingList(@RequestBody Map<String,Object> params){
		List<TdCrossMarketing> list = crossSellingService.getCrossMarketingList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchIncreaseCreditList")
	public RetMsg<List<TdIncreaseCredit>> searchIncreaseCreditList(@RequestBody Map<String,Object> params){
		List<TdIncreaseCredit> list = increaseCreditService.getIncreaseCreditList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获取应收利息
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/getNeedInterest")
	public double getNeedInterest(@RequestBody Map<String, Object> params){
		return productApproveService.getNeedInterest(params);
	}
	
	/**
	 *计算投资收益率或者转让金额
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/computeYiedOrFullPrice")
	public double computeYiedOrFullPrice(@RequestBody Map<String, Object> params) {
	  return productApproveService.computeYiedOrFullPrice(params);
	}

	/**
	 * 根据条件查询所有存续期交易
	 * 
	 * @param dealNo - 交易单号
	 * @return 
	 * @author Hunter
	 * @date 2016-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDurationCommonList")
	public RetMsg<List<TdDurationCommon>> searchDurationCommonList(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productApproveService.getDurationCommonList(params));
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/calForFees")
	public RetMsg<?> calForFees(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productFeeCalService.getCalFeeForProductDeal(params));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getProductFeeCalForPrdNo")
	public RetMsg<?> getProductFeeCalForPrdNo(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productFeeCalService.getProductFeeForPrdNo(params));
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getProductCustCreditService")
	public RetMsg<List<TdProductCustCredit>> getProductCustCreditService(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productCustCreditService.getTdProductCustCreditList(params));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getProductProtocolsFordealNo")
	public RetMsg<List<TdProductApproveProtocols>> getProductProtocolsFordealNo(@RequestBody Map<String, Object> params){
		return RetMsgHelper.ok(productApproveService.getTdProductApproveProtocolsForDealNo(ParameterUtil.getString(params,"dealNo","X")));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFlowEventByTaskId")
	public RetMsg<List<TdFlowTaskEvent>> getFlowEventByTaskId(@RequestBody Map<String, Object> params){
		List<TdFlowTaskEvent> taskEvent = flowQueryService.getFlowTaskEventSelectByTask(params);
		return RetMsgHelper.ok(taskEvent);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveTdProductCustCredit")
	public RetMsg<Serializable> saveTdProductCustCredit(@RequestBody Map<String, Object> params){
		productApproveService.saveTdProductCustCredit(params);
		return RetMsgHelper.ok();
	}
	/**
	 * 查询基准利率
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/t24TCoLoansIntInqRequest")
	public RetMsg<TCoLoansIntInqRec> t24TCoLoansIntInqRequest(@RequestBody Map<String, Object> params){
		TCoLoansIntInqRq request = new TCoLoansIntInqRq();
		request.setInterestKey(ParameterUtil.getString(params, "rateCode", ""));
		request.setCurrency("CNY");
		CommonRqHdr hdr = new CommonRqHdr();
		hdr.setRqUID(UUID.randomUUID().toString());
		hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
		hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
		request.setCommonRqHdr(hdr);
		TCoLoansIntInqRs model = null;
		try {
			model = socketClientService.t24TCoLoansIntInqRequest(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new RException(ParameterUtil.getString(params, "rateCode", "")+"利率异常，查无信息");
		}
		if(model!= null && model.getTCoLoansIntInqRec().size() >0){
			return RetMsgHelper.ok(model.getTCoLoansIntInqRec().get(0));
		}
		return RetMsgHelper.ok(new TCoLoansIntInqRec());
	}
	
	
	
	@RequestMapping(value = "/readBillExcel")
	@ResponseBody
	public RetMsg<List<TdBaseAssetBill>> readExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		List<TdBaseAssetBill> list = new ArrayList<TdBaseAssetBill>();Workbook wb =null;
		try {
			// 获取excel的工作簿
			wb = parseWorkBook(request);
			//读取数据，并转化为List保存
			Sheet billBase = wb.getSheet(0);
			TdBaseAssetBill baseAssetBill = null;
			for(int r = 1; r < billBase.getRows(); r++){//第一行开始读取
				baseAssetBill = new TdBaseAssetBill();
				if(StringUtils.isEmpty(getCellContent(billBase, r, 0))) {
                    continue;
                }
				for (int c = 0; c < billBase.getColumns(); c++) {
					String value = getCellContent(billBase, r, c);
					if(StringUtils.isEmpty(value)) {
                        continue;
                    }
					switch (c) {
					case 0:
						baseAssetBill.setSeq(Integer.parseInt(value));
						break;
					case 1:
						baseAssetBill.setBillNo(value);
						break;
					case 2:
						baseAssetBill.setBillType(value);
						break;
					case 3:
						baseAssetBill.setBillKind(value);
						break;
					case 4:
						baseAssetBill.setDrawerBank(value);
						break;
					case 5:
						baseAssetBill.setDrawerBankNo(value);
						break;
					case 6:
						baseAssetBill.setdDate(value.replace("/", "-"));
						break;
					case 7:
						baseAssetBill.setmDate(value.replace("/", "-"));
						break;
					case 8:
						baseAssetBill.setBillAmt(value);
						break;
					case 9:
						baseAssetBill.setDrawer(value);
						break;
					case 10:
						baseAssetBill.setDiscountApper(value);
						break;
					case 11:
						baseAssetBill.setPayeer(value);
						break;
					case 12:
						baseAssetBill.setPlatEarmark("0"+value);
						break;
					case 13:
						baseAssetBill.setInvestFinal("0"+value);
						break;
					case 14:
						baseAssetBill.setIndusEarmark(value);
						break;
					case 15:
						baseAssetBill.setBaseCity(value);
						break;
					case 16:
						baseAssetBill.setBaseProv(value);
						break;
					default:
						break;
					}
				}
				
				list.add(baseAssetBill);
			}
		} catch (Exception e) {
			JY.error(e);
			throw new RException(e);
		}finally{
			if(null != wb) {
                wb.close();
            }
		}
		return RetMsgHelper.ok(list);
	}
	/**
     * 获取sheet单元格内容
	 * 取sheet中r行c列的内容
	 * @param sheet
	 * @param r
	 * @param c
	 * @return r行c列的内容
	 * @author wzf
	 */
	private String getCellContent(Sheet sheet, int r, int c) {
		String a = "";
		int rowLength = sheet.getRows();
		// 传入的r数大于总行数
		if (r >= rowLength) {
			return "";
		}
		Cell[] row = sheet.getRow(r);
		try {
			if (row != null) {
				if (row.length <= c) {
					// a = null;
				} else {
					if(row[c].getType() == CellType.DATE){
					   DateCell dc = (DateCell)row[c];
					   Date date = dc.getDate();
					   SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd");
					    a = ds.format(date);
					}else if(row[c].getType() == CellType.NUMBER){
						NumberCell ncCell = (NumberCell)row[c];
						a = ncCell.getContents();
					}else{
						a = row[c].getContents();
					}
					
				}
			} else {
				// a = null;
			}
		} catch (Exception e) {
			JY.error(e);
			throw new RException(e);
		}
		return a;
	}
	/**
     * 解析xls
     */
	public Workbook parseWorkBook(MultipartHttpServletRequest request) throws Exception {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
		// 2.交验并处理成excel
		List<Workbook> excelList = new LinkedList<Workbook>();
		for (UploadedFile uploadedFile : uploadedFileList) {
			JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
			Workbook book = Workbook.getWorkbook(new ByteArrayInputStream(uploadedFile.getBytes()));
			excelList.add(book);
		}
		JY.require(excelList.size() == 1, "只能处理一份excel");
		return excelList.get(0);

	}
	
	
	@RequestMapping(value = "/readLcExcel")
	@ResponseBody
	public RetMsg<List<TdAssetLc>> readLcExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		List<TdAssetLc> list = new ArrayList<TdAssetLc>();Workbook wb = null;
		try {
			// 获取excel的工作簿
			wb = parseWorkBook(request);
			//读取数据，并转化为List保存
			Sheet lcBase = wb.getSheet(0);
			TdAssetLc assetLc = null;
			for(int r = 1; r < lcBase.getRows(); r++){//第一行开始读取
				assetLc = new TdAssetLc();
				if(StringUtils.isEmpty(getCellContent(lcBase, r, 0))) {
                    continue;
                }
				for (int c = 0; c < lcBase.getColumns(); c++) {
					String value = getCellContent(lcBase, r, c);
					if(StringUtils.isEmpty(value)) {
                        continue;
                    }
					switch (c) {
					/**
					 * {field:'lcId',title:'信用证编号'},
					{field:'lcAmt',title:'信用证金额',align:'right',formatter:function(value){return CommonUtil.moneyFormat(value,2);}},
					{field:'appliBank',title:'开证行全称'},
					{field:'lcIssueDate',title:'信用证开证日'},
					{field:'lcMdate',title:'信用证到期日'},
					{field:'appliContdate',title:'开证行确认付款日期'},
					{field:'appliContamt',title:'开证行确认付款金额',align:'right',formatter:function(value){return CommonUtil.moneyFormat(value,2);}},
					{field:'lcSellIamt',title:'信用证利息',align:'right',formatter:function(value){return CommonUtil.moneyFormat(value,2);}},
					{field:'lcGraceDays',title:'信用证宽限期'}
					 */
					case 0:
						assetLc.setLcId(value);
						break;
					case 1:
						assetLc.setLcAmt(new BigDecimal(value).doubleValue());
						break;
					case 2:
						assetLc.setAppliBank(value);
						break;
					case 3:
						assetLc.setLcIssueDate(value);
						break;
					case 4:
						assetLc.setLcBenefier(value);
						break;
					case 5:
						assetLc.setAppliContdate(value);
						break;
					case 6:
						if(null != value && StringUtils.isNotEmpty(value) && StringUtils.isNotBlank(value)) {
                            assetLc.setAppliContamt(new BigDecimal(value.trim().replace(",", "")).doubleValue());
                        }
						break;
					/*case 7:
						if(null != value && StringUtils.isNotEmpty(value) && StringUtils.isNotBlank(value))
							assetLc.setLcSellIamt(new BigDecimal(value.trim().replace(",", "")).doubleValue());
						break;*/
					case 7:
						if(null != value && StringUtils.isNotEmpty(value) && StringUtils.isNotBlank(value)) {
                            assetLc.setLcGraceDays(Integer.parseInt(value.trim().replace(",", "")));
                        }
						break;
					case 8:
						assetLc.setLcApprover(value);
						break;
					default:
						break;
					}
				}
				
				list.add(assetLc);
			}
		} catch (Exception e) {
			JY.error(e);
			throw new RException(e.getMessage());
		}finally{
			if(null != wb) {
                wb.close();
            }
		}
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 查询基准利率
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value = "/t24TexCurrencyAllInqRequest")
	public RetMsg<TExCurrencyAllInqRs> t24TexCurrencyAllInqRequest(@RequestBody Map<String, Object> params){
		TExCurrencyAllInqRq tExCurrencyAllInqRq = new TExCurrencyAllInqRq();
		tExCurrencyAllInqRq.setCurrency(ParameterUtil.getString(params, "CCY", ""));
		tExCurrencyAllInqRq.setFlag("Y");
		TExCurrencyAllInqRs tExCurrencyAllInqRs = new TExCurrencyAllInqRs();
		try {
			tExCurrencyAllInqRs = socketClientService.t24TexCurrencyAllInqRequest(tExCurrencyAllInqRq);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return RetMsgHelper.ok(tExCurrencyAllInqRs);
	}
	
	/***LD号补录*/
	@ResponseBody
	@RequestMapping(value = "/updateDurationTradeLdno")
	public RetMsg<Serializable> updateDurationTradeLdno(@RequestBody Map<String,Object> params) {
		productApproveService.updateDurationTradeLdno(params);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/getProductApproveSubByDealno")
	public RetMsg<List<TdProductApproveSub>> getProductApproveSubByDealno(@RequestBody Map<String,Object> params) throws Exception {
		List<TdProductApproveSub> list=productApproveService.getProductApproveSubByDealno(params);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getLoanParam")
	public RetMsg<List<ProductApproveParamVo>> getLoanParam(@RequestBody Map<String,Object> params) throws Exception {
		List<ProductApproveParamVo> list = productOtherService.getLoanParam();
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAmtLineParam")
	public RetMsg<List<TdProductApproveAmtLine>> getAmtLineParam(@RequestBody Map<String,Object> params) throws Exception {
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		List<TdProductApproveAmtLine> list = productOtherService.getAmtLineParam(prdNo);
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveAllProductParams")
	public RetMsg<Serializable> saveAllProductParams(@RequestBody Map<String,Object> params) throws Exception {
		productOtherService.saveAllProductParams(params);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/openLoan")
	public RetMsg<Serializable> openLoan(@RequestBody Map<String,Object> params) throws Exception {
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		productOtherService.openLoan(prdNo);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/closeLoan")
	public RetMsg<Serializable> closeLoan(@RequestBody Map<String,Object> params) throws Exception {
		String prdNo = ParameterUtil.getString(params, "prdNo", "");
		productOtherService.closeLoan(prdNo);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateProductApproveSub")
	public RetMsg<Serializable> updateProductApproveSub(@RequestBody Map<String,Object> params) throws Exception {
		try{
			TdProductApproveMain productApproveMain = new TdProductApproveMain();
			BeanUtil.populate(productApproveMain, params);
			productApproveService.updateProductApproveSub(productApproveMain);
		}catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		return RetMsgHelper.ok();
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetStructList")
	public RetMsg<List<TdBaseAssetStruct>> searchBaseAssetStructList(@RequestBody Map<String,Object> params){
		List<TdBaseAssetStruct> list = baseAssetService.getBaseAssetStructList(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetListVo")
	public RetMsg<List<TdBaseAssetVo>> searchBaseAssetListVo(@RequestBody Map<String,Object> params){
		List<TdBaseAssetVo> list = baseAssetService.searchBaseAssetListVo(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 条件查询列表
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-10-14
	 */
	@ResponseBody
	@RequestMapping(value = "/searchBaseAssetStructListVo")
	public RetMsg<List<TdBaseAssetStructVo>> searchBaseAssetStructListVo(@RequestBody Map<String,Object> params){
		List<TdBaseAssetStructVo> list = baseAssetService.getBaseAssetStructListVo(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 检查产品编号唯一性
	 */
	@ResponseBody
	@RequestMapping(value = "/checkProductCode")
	public RetMsg<Map<String, String>> checkProductCode(@RequestBody HashMap<String, Object> map) {
		Map<String, String> retMap = new HashMap<String, String>();
		retMap.put("code", "000");
		if(!productApproveService.checkProductCode(map)){
			retMap.put("code", "100");
			retMap.put("msg", "资产编码已经存在,请确认!");
			
		}else{
			//根据cost获取二分类
			map.put("p_type", "1");
			map.put("p_sub_type", "12");//12productCode分类
			String productCode = String.valueOf(map.get("productCode"));
			if(productCode != null && productCode.length() > 2){
				productCode = productCode.substring(0, 2);
			}
			map.put("p_name", productCode);
			Map<String, String> tempMap = productApproveService.queryUserParam(map);
			if(tempMap.get("value") == null){
				retMap.put("code", "200");
				retMap.put("msg", "资产编码不符合规范,请确认!");
			}
		}
		return RetMsgHelper.ok(retMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "/calIntPaydate")
	public RetMsg<Map<String, String>> calIntPaydate(@RequestBody Map<String,Object> params) throws Exception {
		Map<String, String> maps = new HashMap<String, String>();
		RetMsg<Map<String, String>> ret = RetMsgHelper.ok(maps);
		try{
			String intFre = String.valueOf(params.get("intFre"));
			String vDate = String.valueOf(params.get("vDate"));
			String mDate = String.valueOf(params.get("mDate"));
			String sDate_1 = String.valueOf(params.get("sDate"));
			if("1".equals(intFre)){//年
				calIntDate(vDate, mDate, sDate_1, maps, Frequency.YEAR);
				
			}else if("2".equals(intFre)){//半年
				calIntDate(vDate, mDate, sDate_1, maps, Frequency.HALFYEAR);
				
			}else if("4".equals(intFre)){//季
				calIntDate(vDate, mDate, sDate_1, maps, Frequency.QUARTER);
				
			}else if("12".equals(intFre)){//月
				calIntDate(vDate, mDate, sDate_1, maps, Frequency.MONTH);
				
			}
			ret.setDesc("999");
			ret.setDetail("成功");
		}catch (Exception e) {
			e.printStackTrace();
			JY.raise(e.getMessage());
		}
		return ret;
	}
	
	private void calIntDate(String vDate,String mDate,String sDate_1,Map<String, String> maps,Frequency frequency)throws Exception
	{
		//如果传递了首次收息日sDate_1则用sDate_1
		String sDate = null;
		if(StringUtils.isNotBlank(sDate_1)){
			sDate = sDate_1;
		}else{
			sDate = PlaningTools.addOrSubMonth_Day(vDate, frequency, 1);
		}
		
		String eDate = sDate;
		String tempDate = sDate;
		while(true){
			tempDate = PlaningTools.addOrSubMonth_Day(tempDate, frequency, 1);
			if(PlaningTools.compareDate(tempDate, mDate, "yyyy-MM-dd"))
			{
				break;
			}
			eDate = tempDate;
		}// end while
		
		if(PlaningTools.compareDate(sDate, mDate, "yyyy-MM-dd")){
			sDate = mDate;
		}
		if(PlaningTools.compareDate(eDate, mDate, "yyyy-MM-dd")){
			eDate = mDate;
		}
		maps.put("sDate", sDate);
		maps.put("eDate", eDate);
	}
	
	/****
	 * 
	 * @param params costcent
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/queryDoubleSubjectByCost")
	public RetMsg<Map<String, String>> queryDoubleSubjectByCost(@RequestBody Map<String,Object> params) throws Exception {
		//用户参数 p_type 1	 分组设置 p_sub_type 10
		params.put("p_type", "1");
		params.put("p_sub_type", "11");//11二分类
		params.put("p_name", params.get("costcent"));
		//根据cost获取二分类
		return RetMsgHelper.ok(productApproveService.queryUserParam(params));
	}
	
	/****
	 * 
	 * @param params costcent
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/queryProductCodeDesc")
	public RetMsg<Map<String, String>> queryProductCodeDesc(@RequestBody Map<String,Object> params) throws Exception {
		//用户参数 p_type 1	 分组设置 p_sub_type 10
		params.put("p_type", "1");
		params.put("p_sub_type", "12");//12productCode分类
		String productCode = String.valueOf(params.get("productCode"));
		if(productCode != null && productCode.length() > 2){
			productCode = productCode.substring(0, 2);
		}
		params.put("p_name", productCode);
		//根据cost获取二分类
		return RetMsgHelper.ok(productApproveService.queryUserParam(params));
	}
	
	/**
	 * 该笔交易是在该用户下是否完成审批
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/searchProductApproveUnfinishedCount")
	public RetMsg<Serializable> searchProductApproveUnfinishedCount(@RequestBody Map<String,Object> params)
	{
		params.put("isActive", DictConstants.YesNo.YES);
		String approveStatus = ParameterUtil.getString(params, "approveStatus", "");
		if(StringUtil.isNullOrEmpty(approveStatus)){
			approveStatus = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse;
			params.put("approveStatusNo", approveStatus.split(","));
		}
		params.put("userId", SlSessionHelper.getUserId());
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		if(productApproveService.getProductApproveMainListCount(params) > 0){
			ret.setDetail("SUCC");
		}
		return ret;
	}
	
	/**
	 * 检查活期账户是否存在
	 * 
	 * @param params - 请求参数
	 * @return 
	 * @author Hunter
	 * @date 2016-8-10
	 */
	@ResponseBody
	@RequestMapping(value = "/checkNostAccNo")
	public RetMsg<Serializable> checkNostAccNo(@RequestBody Map<String,Object> params)
	{
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		List<TbkSubjectDef> list = tbkSubjectDefService.getTbkSubjectDefSameService(params);
		if(list.size() > 0){
			ret.setDetail("SUCC");
		}
		return ret;
	}
	
}
