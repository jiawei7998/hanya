package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdBaseproductApproveMain;

/**

 * @version 1.0
 */
public interface TdBaseproductApproveMainMapper extends Mapper<TdBaseproductApproveMain> {

	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 产品审批主表对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdBaseproductApproveMain> getTdBaseproductApproveMainList(Map<String, Object> params, RowBounds rb);
	
	
	public TdBaseproductApproveMain getTdBaseproductApproveMainByDealno(Map<String, Object> params);
}