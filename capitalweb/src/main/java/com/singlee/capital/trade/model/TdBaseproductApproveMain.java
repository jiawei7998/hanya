package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.common.pojo.BaseDomain;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;

/**
 * @projectName 同业业务管理系统
 * @className 产品审批主表
 * @description TODO
 * @author 倪航
 * @createDate 2016-8-9 下午4:46:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_BASEPRODUCT_APPROVE_MAIN")
public class TdBaseproductApproveMain extends BaseDomain implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5132725957608613575L;
	/**
	 * 审批单编号 由函数生成
	 */
	@Id
	private String dealNo;
	/***********************审批块**************************/
	private Integer prdNo;//产品编号
	private String dealType;//审批单类型 1-审批 2-核实
	private String cNo;//交易对手编号 交易对手表的ID
	private String contact;//联系人
	private String sponsor;//审批发起人
	private String sponInst;//审批发起机构
	private String sponsorPhone;//审批发起人手机
	private String aDate;//审批开始日期
	private String conTitle;//合同标题 产品标题
	/***********************业务主要要素块**************************/
	private String ccy;//币种
	private double amt;//本金
	private String rateType;//利率类型
	private String rateCode;//基准利率代码
	private double rateDiff;//浮息点差(BP)
	private double rateScale;//浮动比例_%
	private double rateCap;//利率上限_%
	private double rateFloor;//利率下限_%
	private double enterpCostRate;//企业融资成本_%
	private double contractRate;//投资收益率_% 合同利率_%
	private double acturalRate;//实际利率_% 参与计提的利率
	private double compSumRate;//综合收益率_%
	private String inteffRule;//利率生效规则
	private double lateChargeRate;//逾期罚息率_%
	private String vDate;//起息日期
	private String tDate;//划款日期
	private String mDate;//到期日期
	private String meDate;//到账日期
	private String graceType;//宽限期类型 1-工作日 2-自然天
	private int gracePeriod;//宽限期天数
	private Integer term;//计息天数
	private Integer occupTerm;//资金占用天数
	private String basis;//计息基础
	private String amtFre;//还本频率
	private String intFre;//收息频率
	private String feeType;//收费方式
	private double mInt;//到期利息(元)
	private double mAmt;//到期金额（元）
	private String sDate;//首次收息日
	private String intType;//收息方式
	private String intMDate;//前收息截止日期
	private Integer intTerm;//先收息天数
	private double intAmt;//前收息金额
	private String remarkNote;//补充说明
	private String invtype;//会计四分类
	/*********************交易配合项**********************/
	private String dealDate;//交易日期
	/*********************清算项************************/
	private String isSettl;//是否清算
	private String acctType;//清算方式
	private String selfAccCode;//本方账号
	private String selfAccName;//本方账户名称
	private String selfBankCode;//本方开户行行号
	private String selfBankName;//本方开户行名称
	private String partyAccCode;//对手方账号
	private String partyAccName;//对手方账户名称
	private String partyBankCode;//对手方开户行行号
	private String partyBankName;//对手方开户行名称
	private String settlRemark;//缴款信息备注
	/******************回填项**********************/
	private double guidePrice;//指导价_%
	private double ftpCostPrice;//FTP成本价_%
	/*********************收益率**********************/
	private double chargeRate;//中收收益率_%
	private double chanRate;//通道费率_%
	private double earlyRate;//提前支取利率_%
	private double supervisionRate;//监督费率_%
	private double custodFeerate;//第一托管费率_%
	/*************************************************/
	/**
	 * 计息频率
	 */
	private String intCalcFre;
	/**
	 * 资料影像
	 */
	private String image;
	/**
	 * 更新时间
	 */
	private String lastUpdate;
	/**
	 * 版本
	 */
	@Id
	private int version;
	/**
	 * 是否有效
	 */
	private String isActive;
	/**
	 * 现金流条款
	 */
	private String fpml;

	/**
	 * 外部流水号
	 */
	private String refNo;
	@Transient
	private String prdName;//产品名称
	@Transient
	private String partyName;//客户名称
	/**
	 * 融资人
	 */
	@Transient
	private String financier;
	/**
	 * 项目名称
	 */
	@Transient
	private String projectName;
	/**
	 * 产品信息
	 */
	@Transient
	private TcProduct product;
	/**
	 * 发起人
	 */
	@Transient
	private TaUser user;
	/**
	 * 发起机构
	 */
	@Transient
	private TtInstitution institution;
	/**
	 * 交易对手表
	 */
	@Transient
	private TtCounterParty counterParty;
	/**
	 * 产品审批子表列表
	 */
	@Transient
	private List<TdProductApproveSub> productApproveSubs;
	/**
	 * 参与方信息-转成可配置MAP KEY-VALUE
	 */
	@Transient
	private List<TdProductApproveSub> participantApprove;
	/**
	 * 中间业务收入
	 */
	@Transient
	private List<TdFeesPlan> feesPlanList;
	/**
	 * 增信措施
	 */
	@Transient
	private List<TdIncreaseCredit> increaseCredits;
	/**
	 * 交叉营销
	 */
	@Transient
	private List<TdCrossMarketing> crossMarketings;
	/**
	 * 基础资产
	 */
	@Transient
	private List<TdBaseAsset> baseAssets;
	/**
	 * 票据基础资产
	 */
	@Transient
	private List<TdBaseAssetBill> baseAssetBills;
	/**
	 * 股票信息
	 */
	@Transient
	private List<TdStockInfo> stockInfos;
	/**
	 * 风险资产
	 */
	@Transient
	private List<TdRiskAsset> riskAssets;

	/**
	 * 风险缓释
	 */
	@Transient
	private List<TdRiskSlowRelease> riskSlowReleases;
	
	/**
	 * 原本金
	 */
	@Transient
	private double originalAmt;
	/**
	 * 原利率
	 */
	@Transient
	private double originalRate;
	
	/**
	 * task_id
	 */
	@Transient
	private String taskId;
	
	/**
	 * 数据来源
	 */
	@Transient
	private String tradeSource;
	/**
	 * 实际成交金额
	 */
	private double procAmt;
	/**
	 * 折溢价金额
	 */
	private double disAmt;

	
	public double getProcAmt() {
		return procAmt;
	}

	public void setProcAmt(double procAmt) {
		this.procAmt = procAmt;
	}

	public double getDisAmt() {
		return disAmt;
	}

	public void setDisAmt(double disAmt) {
		this.disAmt = disAmt;
	}

	/**
	 * 最后一个付息日
	 */
	private String eDate;

	public String geteDate() {
		return eDate;
	}

	public void seteDate(String eDate) {
		this.eDate = eDate;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public Integer getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(Integer prdNo) {
		this.prdNo = prdNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getConTitle() {
		return conTitle;
	}

	public void setConTitle(String conTitle) {
		this.conTitle = conTitle;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponInst() {
		return sponInst;
	}

	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}

	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public double getRateDiff() {
		return rateDiff;
	}

	public void setRateDiff(double rateDiff) {
		this.rateDiff = rateDiff;
	}

	public double getContractRate() {
		return contractRate;
	}

	public void setContractRate(double contractRate) {
		this.contractRate = contractRate;
	}

	public double getActuralRate() {
		return acturalRate;
	}

	public void setActuralRate(double acturalRate) {
		this.acturalRate = acturalRate;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getvDate() {
		return vDate;
	}

	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	public String gettDate() {
		return tDate;
	}

	public void settDate(String tDate) {
		this.tDate = tDate;
	}

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getMeDate() {
		return meDate;
	}

	public void setMeDate(String meDate) {
		this.meDate = meDate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public double getmInt() {
		return mInt;
	}

	public void setmInt(double mInt) {
		this.mInt = mInt;
	}

	public double getmAmt() {
		return mAmt;
	}

	public void setmAmt(double mAmt) {
		this.mAmt = mAmt;
	}

	public String getAmtFre() {
		return amtFre;
	}

	public void setAmtFre(String amtFre) {
		this.amtFre = amtFre;
	}

	public String getIntCalcFre() {
		return intCalcFre;
	}

	public void setIntCalcFre(String intCalcFre) {
		this.intCalcFre = intCalcFre;
	}

	public String getIntFre() {
		return intFre;
	}

	public void setIntFre(String intFre) {
		this.intFre = intFre;
	}

	public String getIntType() {
		return intType;
	}

	public void setIntType(String intType) {
		this.intType = intType;
	}

	public String getIntMDate() {
		return intMDate;
	}

	public void setIntMDate(String intMDate) {
		this.intMDate = intMDate;
	}

	public Integer getIntTerm() {
		return intTerm;
	}

	public void setIntTerm(Integer intTerm) {
		this.intTerm = intTerm;
	}

	public double getIntAmt() {
		return intAmt;
	}

	public void setIntAmt(double intAmt) {
		this.intAmt = intAmt;
	}
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getFpml() {
		return fpml;
	}

	public void setFpml(String fpml) {
		this.fpml = fpml;
	}

	public String getSelfAccCode() {
		return selfAccCode;
	}

	public void setSelfAccCode(String selfAccCode) {
		this.selfAccCode = selfAccCode;
	}

	public String getSelfAccName() {
		return selfAccName;
	}

	public void setSelfAccName(String selfAccName) {
		this.selfAccName = selfAccName;
	}

	public String getSelfBankCode() {
		return selfBankCode;
	}

	public void setSelfBankCode(String selfBankCode) {
		this.selfBankCode = selfBankCode;
	}

	public String getSelfBankName() {
		return selfBankName;
	}

	public void setSelfBankName(String selfBankName) {
		this.selfBankName = selfBankName;
	}

	public String getPartyAccCode() {
		return partyAccCode;
	}

	public void setPartyAccCode(String partyAccCode) {
		this.partyAccCode = partyAccCode;
	}

	public String getPartyAccName() {
		return partyAccName;
	}

	public void setPartyAccName(String partyAccName) {
		this.partyAccName = partyAccName;
	}

	public String getPartyBankCode() {
		return partyBankCode;
	}

	public void setPartyBankCode(String partyBankCode) {
		this.partyBankCode = partyBankCode;
	}

	public String getPartyBankName() {
		return partyBankName;
	}

	public void setPartyBankName(String partyBankName) {
		this.partyBankName = partyBankName;
	}

	public TcProduct getProduct() {
		return product;
	}

	public void setProduct(TcProduct product) {
		this.product = product;
	}

	public TaUser getUser() {
		return user;
	}

	public void setUser(TaUser user) {
		this.user = user;
	}

	public TtInstitution getInstitution() {
		return institution;
	}

	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}

	public TtCounterParty getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(TtCounterParty counterParty) {
		this.counterParty = counterParty;
	}

	public List<TdProductApproveSub> getProductApproveSubs() {
		return productApproveSubs;
	}

	public void setProductApproveSubs(List<TdProductApproveSub> productApproveSubs) {
		this.productApproveSubs = productApproveSubs;
	}

	
	public List<TdProductApproveSub> getParticipantApprove() {
		return participantApprove;
	}

	public void setParticipantApprove(List<TdProductApproveSub> participantApprove) {
		this.participantApprove = participantApprove;
	}

	public List<TdFeesPlan> getFeesPlanList() {
		return feesPlanList;
	}

	public void setFeesPlanList(List<TdFeesPlan> feesPlanList) {
		this.feesPlanList = feesPlanList;
	}

	public List<TdIncreaseCredit> getIncreaseCredits() {
		return increaseCredits;
	}

	public void setIncreaseCredits(List<TdIncreaseCredit> increaseCredits) {
		this.increaseCredits = increaseCredits;
	}

	public List<TdCrossMarketing> getCrossMarketings() {
		return crossMarketings;
	}

	public void setCrossMarketings(List<TdCrossMarketing> crossMarketings) {
		this.crossMarketings = crossMarketings;
	}

	public List<TdBaseAsset> getBaseAssets() {
		return baseAssets;
	}

	public void setBaseAssets(List<TdBaseAsset> baseAssets) {
		this.baseAssets = baseAssets;
	}

	public List<TdRiskAsset> getRiskAssets() {
		return riskAssets;
	}

	public void setRiskAssets(List<TdRiskAsset> riskAssets) {
		this.riskAssets = riskAssets;
	}

	public List<TdRiskSlowRelease> getRiskSlowReleases() {
		return riskSlowReleases;
	}

	public void setRiskSlowReleases(List<TdRiskSlowRelease> riskSlowReleases) {
		this.riskSlowReleases = riskSlowReleases;
	}

	public double getOriginalAmt() {
		return originalAmt;
	}

	public void setOriginalAmt(double originalAmt) {
		this.originalAmt = originalAmt;
	}

	public double getOriginalRate() {
		return originalRate;
	}

	public void setOriginalRate(double originalRate) {
		this.originalRate = originalRate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public List<TdBaseAssetBill> getBaseAssetBills() {
		return baseAssetBills;
	}

	public void setBaseAssetBills(List<TdBaseAssetBill> baseAssetBills) {
		this.baseAssetBills = baseAssetBills;
	}

	public List<TdStockInfo> getStockInfos() {
		return stockInfos;
	}

	public void setStockInfos(List<TdStockInfo> stockInfos) {
		this.stockInfos = stockInfos;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getFinancier() {
		return financier;
	}

	public void setFinancier(String financier) {
		this.financier = financier;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTradeSource() {
		return tradeSource;
	}

	public void setTradeSource(String tradeSource) {
		this.tradeSource = tradeSource;
	}

	public double getRateScale() {
		return rateScale;
	}

	public void setRateScale(double rateScale) {
		this.rateScale = rateScale;
	}

	public double getRateCap() {
		return rateCap;
	}

	public void setRateCap(double rateCap) {
		this.rateCap = rateCap;
	}

	public double getRateFloor() {
		return rateFloor;
	}

	public void setRateFloor(double rateFloor) {
		this.rateFloor = rateFloor;
	}

	public double getEnterpCostRate() {
		return enterpCostRate;
	}

	public void setEnterpCostRate(double enterpCostRate) {
		this.enterpCostRate = enterpCostRate;
	}

	public double getCompSumRate() {
		return compSumRate;
	}

	public void setCompSumRate(double compSumRate) {
		this.compSumRate = compSumRate;
	}

	public String getInteffRule() {
		return inteffRule;
	}

	public void setInteffRule(String inteffRule) {
		this.inteffRule = inteffRule;
	}

	public double getLateChargeRate() {
		return lateChargeRate;
	}

	public void setLateChargeRate(double lateChargeRate) {
		this.lateChargeRate = lateChargeRate;
	}

	public double getGuidePrice() {
		return guidePrice;
	}

	public void setGuidePrice(double guidePrice) {
		this.guidePrice = guidePrice;
	}

	public double getFtpCostPrice() {
		return ftpCostPrice;
	}

	public void setFtpCostPrice(double ftpCostPrice) {
		this.ftpCostPrice = ftpCostPrice;
	}

	public String getGraceType() {
		return graceType;
	}

	public void setGraceType(String graceType) {
		this.graceType = graceType;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public Integer getOccupTerm() {
		return occupTerm;
	}

	public void setOccupTerm(Integer occupTerm) {
		this.occupTerm = occupTerm;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getRemarkNote() {
		return remarkNote;
	}

	public void setRemarkNote(String remarkNote) {
		this.remarkNote = remarkNote;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getIsSettl() {
		return isSettl;
	}

	public void setIsSettl(String isSettl) {
		this.isSettl = isSettl;
	}

	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public String getSettlRemark() {
		return settlRemark;
	}

	public void setSettlRemark(String settlRemark) {
		this.settlRemark = settlRemark;
	}

	public String getSponsorPhone() {
		return sponsorPhone;
	}

	public void setSponsorPhone(String sponsorPhone) {
		this.sponsorPhone = sponsorPhone;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public double getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(double chargeRate) {
		this.chargeRate = chargeRate;
	}

	public double getChanRate() {
		return chanRate;
	}

	public void setChanRate(double chanRate) {
		this.chanRate = chanRate;
	}

	public double getEarlyRate() {
		return earlyRate;
	}

	public void setEarlyRate(double earlyRate) {
		this.earlyRate = earlyRate;
	}

	public double getSupervisionRate() {
		return supervisionRate;
	}

	public void setSupervisionRate(double supervisionRate) {
		this.supervisionRate = supervisionRate;
	}

	public double getCustodFeerate() {
		return custodFeerate;
	}

	public void setCustodFeerate(double custodFeerate) {
		this.custodFeerate = custodFeerate;
	}

	
}