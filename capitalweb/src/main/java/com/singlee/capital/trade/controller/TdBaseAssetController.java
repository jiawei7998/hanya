package com.singlee.capital.trade.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.interfacex.qdb.util.StringUtils;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdBaseAsset;
import com.singlee.capital.trade.service.BaseAssetService;

@Controller
@RequestMapping(value = "/TdBaseAssetController")
public class TdBaseAssetController extends CommonController{
	@Autowired
	private BaseAssetService baseAssetService;
	
	public static Logger log = LoggerFactory.getLogger("FIBS");
	
	@ResponseBody
	@RequestMapping(value = "/getBaseAssetList")
	public RetMsg<PageInfo<TdBaseAsset>> getBaseAssetList(@RequestBody Map<String, Object> params) throws RException{
		Page<TdBaseAsset> param = baseAssetService.getBaseAssetLists(params);
		return RetMsgHelper.ok(param);
	}
	
	@RequestMapping(value = "/queryTdBaseAssetByIdss/{dealNo}", method = RequestMethod.GET)
	public void uploadingTdBaseAssets(@PathVariable String dealNo,HttpServletRequest request,HttpServletResponse response) throws IOException{
		TdBaseAsset tdBaseAsset = baseAssetService.queryTdBaseAssetById(dealNo);
		JY.require(dealNo != null, "模板%s 没有找到！", dealNo);
		String encode_filename = URLEncoder.encode(tdBaseAsset.getDealNo()
				+ ".xls", "utf-8");
		String ua = request.getHeader("User-Agent");
		if (ua != null && ua.indexOf("Firefox/") >= 0) {
			response.setHeader("Content-Disposition", "attachment; filename*="
					+ encode_filename);
		} else {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ encode_filename);
		}

		HttpUtil.flushHttpResponse(response, "application/msexcel",
				tdBaseAsset.getDealNo());
	}
	
	//上传Excel
	@ResponseBody
	@RequestMapping(value = "/uploadTdBaseAssetExcel", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String uploadTdBaseAssetExcel(MultipartHttpServletRequest request, HttpServletResponse response) {
		StringBuffer sb = new StringBuffer();
		try {
			// 1.文件获取
			List<UploadedFile> uploadedFileList = null;
			uploadedFileList = FileManage.requestExtractor(request);
			// 2.交验并处理成excel
			for (UploadedFile uploadedFile : uploadedFileList) {
				String name = uploadedFile.getFullname();
				InputStream is = new ByteArrayInputStream(uploadedFile.getBytes());
				sb.append(baseAssetService.setBaseAssetByExcel(name, is));
			}
		} catch (IOException e) {
			log.error("TdBaseAssetController--uploadTdBaseAssetExcel"+e);
			sb.append("基础资产信息上传失败");
		}
		return StringUtils.isEmpty(sb.toString())?"基础资产信息上传成功":sb.toString();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getBaseAssetHisLists")
	public RetMsg<PageInfo<TdBaseAsset>> getBaseAssetHisLists(@RequestBody Map<String, Object> params) throws RException{
        
		Page<TdBaseAsset> param = baseAssetService.getBaseAssetHisLists(params);
		return RetMsgHelper.ok(param);
	}
}
