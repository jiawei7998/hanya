package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdTransforSchedule;

/**
 * @projectName 同业业务管理系统
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdTransforScheduleMapper extends Mapper<TdTransforSchedule> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 交易展期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTransforSchedule getTransforScheduleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTransforSchedule> getTransforScheduleList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTransforSchedule> getTransforScheduleListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 交易展期对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdTransforSchedule> getTransforScheduleListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param TransforSchedule
	 * @return
	 */
	public TdTransforSchedule searchTransforSchedule(TdTransforSchedule TransforSchedule);
}