package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdMultiResale;

/**
 * @projectName 同业业务管理系统
 * @className 资产卖断服务接口
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface MulResaleService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 资产卖断主键
	 * @return 资产卖断对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdMultiResale getMulResaleById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 资产卖断对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdMultiResale> getMulResalePage(Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 资产卖断对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createMulResale(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateMulResale(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 资产卖断主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteMulResale(String[] dealNos);
	
	/**
	 * 结算完成之后的回调处理，对每笔资产的额度释放
	 * @param dealNo  资产卖断单号
	 */
	public void callBackProcess(String  dealNo);
}
