package com.singlee.capital.trade.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayChange;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdFeesPassAgewayVo;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TmFeesPassagewy;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:56:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdFeesPassAgewayService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdFeesPassAgeway> getTdFeesPassAgewayList(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public Page<TdFeesPassAgeway> getTdFeesPassAgewayLists(Map<String, Object> params);
	
	/**
	 * 根据交易单号查询一条数据
	 * @param dealNo
	 * @return
	 */
	public TdFeesPassAgewayChange queryTdFeesPassAgewayById(String dealNo);

	public void createTempPassageWay(HashMap<String, Object> map);

	public void updateTempPassageWay(HashMap<String, Object> map);

	public Page<TdFeesPassAgewayChange> getPassageWayPage(
			Map<String, Object> params, int i);

	public void deletePassageWay(String[] dealNos);

	public List<TmFeesPassagewy> getPassageWayByDealNo(
			Map<String, Object> params);

	public BigDecimal createAllCashFlowsForSaveApprove(
			List<PrincipalInterval> principalIntervals,
			List<InterestRange> interestRanges, Map<String, Object> params,TdFeesPassAgeway feesPassagewy,HashMap<String, TdFeesPassAgewayDaily> passAwayMap);

	public List<TdFeesPassAgeway> getTdPassageWayByDealNo(
			Map<String, Object> params);

	public List<TdFeesPassAgewayDaily> getFeesDailyInterestList(
			Map<String, Object> params);

	public List<TdFeesPassAgeway> getPassageWayByEndDate(Map<String, Object> params);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdFeesPassAgewayVo> getTdFeesPassAgewayListVo(Map<String, Object> params);
	
	public BigDecimal createPassWayFeesPlan(TdProductApproveMain productApproveMain,Map<String, Object> params, HashMap<String, TdFeesPassAgewayDaily> passAwayMap,List<PrincipalInterval> principalIntervals, List<TdFeesPassAgeway> feesPassagewyList)throws Exception;
	
	public BigDecimal reCalPassCashFlow(List<PrincipalInterval> principalIntervals,TdFeesPassAgeway tdFeesPassAgeway,TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap,String effdate,String addFlag) throws Exception;
		
	public BigDecimal calPassDailyCashFlowForTemp(List<PrincipalInterval> principalIntervals,TdFeesPassAgeway tdFeesPassAgeway,TdProductApproveMain productApproveMain,HashMap<String, TdFeesPassAgewayDaily> passAwayMap,String effdate) throws Exception;
	
	public HashMap<String, TdFeesPassAgewayDaily> recalAllPassFeePlanByCapitalByType(List<PrincipalInterval> principalIntervals,TdProductApproveMain productApproveMain,String effdate,String type)throws Exception;
		
	public double getSumFeeRateByRefNo(Map<String, Object> params);
	
	public BigDecimal getSumInterest(Map<String, Object> params);
}
