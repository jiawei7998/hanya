package com.singlee.capital.trade.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.DayCountBasis;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdAmtRange;
import com.singlee.capital.cashflow.model.TdCashflowCapital;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdOverDueConfirmMapper;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdOverDueConfirm;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.TdOverDueConfirmService;
import com.singlee.slbpm.service.FlowOpService;

@Service("tdOverDueConfirmServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TdOverDueConfirmServiceImpl extends AbstractDurationService implements TdOverDueConfirmService 
{
	@Autowired
	public TdOverDueConfirmMapper tdOverDueConfirmMapper;
	@Autowired
	private ProductApproveService productApproveService;
	@Autowired
	private ApproveManageService approveManageService;
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	@Autowired
	private DayendDateService dayendDateService;//系统日期表
	@Autowired
	private TdAmtRangeMapper amtRangeMapper;//实际还本
	@Autowired
	private TdAccTrdDailyMapper accTrdDailyMapper;//每日计提中间表
	@Autowired
	private TrdTposMapper tposMapper;//持仓表
	@Autowired
	TdRateRangeMapper rateRangeMapper;//实际利率区间
	@Autowired
	BookkeepingService bookkeepingService;
	@Autowired
	TdCashflowCapitalMapper capitalMapper;
	@Autowired
	private FlowOpService flowOpService;
	@Autowired
	private TrdTradeMapper tradeMapper;
	@Autowired
	private TdProductFeeDealMapper productFeeDealMapper;
	@Override
	public TdOverDueConfirm queryOverDueDealByDealno(String dealNo) {
		return tdOverDueConfirmMapper.queryOverDueDealByDealno(dealNo);
	}
	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		TdOverDueConfirm tdOverDueConfirm = new TdOverDueConfirm();
		BeanUtil.populate(tdOverDueConfirm, params);
		tdOverDueConfirm = tdOverDueConfirmMapper.searchTradeOverDue(tdOverDueConfirm);
		return productApproveService.getProductApproveActivated(tdOverDueConfirm.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdOverDueConfirm tdOverDueConfirm = new TdOverDueConfirm();
		tdOverDueConfirm.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
		tdOverDueConfirm = tdOverDueConfirmMapper.searchTradeOverDue(tdOverDueConfirm);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(tdOverDueConfirm.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(tdOverDueConfirm.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(tdOverDueConfirm.getDealNo());//将逾期的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomTradeOverDue);//交易逾期
		tdTrdEvent.setEventTable("TD_OVERDUE_CONFIRM");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}
	
	/**
	 * map转换成需要的TdOverDueConfirm对象
	 * @param map
	 * @return
	 */
	public TdOverDueConfirm overDue(Map<String,Object> map){
		TdOverDueConfirm tdOverDueConfirm = new TdOverDueConfirm();
		try {
			BeanUtil.populate(tdOverDueConfirm, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		tdOverDueConfirm.setSponsor(SlSessionHelper.getUserId());
		tdOverDueConfirm.setSponInst(SlSessionHelper.getInstitutionId());
		tdOverDueConfirm.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		tdOverDueConfirm.setCfType(DictConstants.TrdType.CustomTradeOverDue);
		return tdOverDueConfirm;
	}

	@Override
	public void updateOverDueDeal(TdOverDueConfirm overDueConfirm) {
		tdOverDueConfirmMapper.updateOverDueDeal(overDueConfirm);
	}

	@Override
	public List<TdOverDueConfirm> queryOverDueDeals(Map<String, Object> params) {
		return tdOverDueConfirmMapper.queryOverDueDeals(params);
	}

	@Override
	public TdOverDueConfirm queryOverDueDealByRefNo(String refNo) {
		return tdOverDueConfirmMapper.queryOverDueDealByRefNo(refNo);
	}
	

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		try {
			String odVdate = ParameterUtil.getString(params, "odVdate", dayendDateService.getDayendDate());
			TdProductApproveMain productApproveMain = null;//重新查询原交易数据
			if(!"".equals(ParameterUtil.getString(params, "refNo", ""))){
	        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
	            JY.require(productApproveMain != null, "原交易信息不存在");
	        }else{
	        	throw new RException("原交易信息不存在！");
	        }
			/**
			 * 逾期确认完，需要将未发生还本计划 还本金额及实际日期 全部置为逾期日  金额全部置为0
			 */
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("refNo", ParameterUtil.getString(params, "refNo", ""));
			List<TdCashflowCapital> capitals = capitalMapper.getCaplitalsIsNotRecive(paramsMap);
			for(TdCashflowCapital capital : capitals)
			{
				capital.setRepaymentTdate(odVdate);
				capital.setRepaymentTamt(0.00);
				this.capitalMapper.updateByPrimaryKey(capital);
			}
			/***************************************************************************
			 * 判定交易逾期起息日与当前账务日期大小  小于账务日期，表明是倒起息交易，需要出利息调整
			 */
			if(PlaningTools.compareDate2(dayendDateService.getDayendDate(),odVdate,"yyyy-MM-dd"))//倒起息
			{
				
				String postdate = dayendDateService.getDayendDate();
				//TD_ACC_TRD_DAILY 增加一条调整的计提
				
				//封装利率区间 带入计算公式
				List<InterestRange> interestRanges = new ArrayList<InterestRange>();
				InterestRange interestRange = new InterestRange();
				interestRange.setStartDate(odVdate);
				interestRange.setEndDate(dayendDateService.getDayendDate());
				interestRange.setRate(ParameterUtil.getDouble(params, "odRate", 0.00));
				interestRanges.add(interestRange);
				//封装本金区间  带入计算公式
				List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
				List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(params);
				PrincipalInterval principalInterval = null;
				double tempAmt = productApproveMain.getAmt();//本金
				if(amtRanges.size() > 0){//发生过还本 那么从实际TT_AMT_RANGE还本区间表获取还本区间
					int i = 0;
					for(i =0 ;i <amtRanges.size();i++)
					{
						tempAmt = PlaningTools.sub(tempAmt, amtRanges.get(i).getExecAmt());//本金递减	
					}
				}
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(odVdate);
				principalInterval.setResidual_Principal(tempAmt);
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(odVdate, Frequency.YEAR, 10));//无限逾期10年(逾期情况)
				principalIntervals.add(principalInterval);
				double amInt = 0.00;
				try {//倒起息差额计算
					amInt = PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, odVdate, dayendDateService.getDayendDate(), odVdate, 
							DayCountBasis.equal(productApproveMain.getBasis()));
				} catch (ParseException e) {
					JY.require(true,"计算区间差额计提出错"+e.getMessage());
				}   
				//从数据库获得利率变更交易
				TdOverDueConfirm tdOverDueConfirm = tdOverDueConfirmMapper.queryOverDueDealByDealno(ParameterUtil.getString(params, "dealNo", ""));//逾期交易自身的交易流水
				tdOverDueConfirm.setOdAmt(amInt);
				tdOverDueConfirmMapper.updateByPrimaryKey(tdOverDueConfirm);
				/**
				 * 更改头寸表  1、判定交易是否逾期  如果是：那么就需要冲减逾期利息
				 * 2、如果非逾期交易，那么需要冲减应计利息
				 */
				//逾期情况处理
				Map<String, Object> tposMap = new HashMap<String, Object>();
				tposMap.put("dealNo", productApproveMain.getDealNo());
				tposMap.put("version", tdOverDueConfirm.getVersion());
				TdTrdTpos tpos = tposMapper.getTdTrdTposByKey(tposMap);
				tpos.setOverdueDays(PlaningTools.daysBetween(odVdate,dayendDateService.getDayendDate()));
				tpos.setOverdueAccruedTint(PlaningTools.add(tpos.getOverdueAccruedTint(), amInt));
				tpos.setOverdueActualTint(PlaningTools.add(tpos.getOverdueActualTint(), amInt));
				tposMapper.updateByPrimaryKey(tpos);
				
				/**
				 * 生成利率调整分录中间表TD_ACC_TRD_DAILY
				 */
				TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
				accTrdDaily.setDealNo(tdOverDueConfirm.getRefNo());
				accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_ADJBYOVERDUE);//逾期引起的利率变更
				accTrdDaily.setAccAmt(amInt);
				accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				accTrdDaily.setPostDate(postdate);
				accTrdDaily.setRefNo(tdOverDueConfirm.getDealNo());
				accTrdDailyMapper.insert(accTrdDaily);
				
				//调用账务接口
				Map<String, Object> amtMap = new HashMap<String, Object>();
				amtMap.put(DurationConstants.AccType.INTEREST_OVERDUE, accTrdDaily.getAccAmt());
				
				Map<String, Object> infoMap = new HashMap<String, Object>();
				infoMap = BeanUtil.beanToMap(productApproveMain);
				infoMap.put("overdueDays", PlaningTools.daysBetween(odVdate,dayendDateService.getDayendDate()));
				
				InstructionPackage instructionPackage = new InstructionPackage();
				instructionPackage.setInfoMap(infoMap);
				instructionPackage.setAmtMap(amtMap);
				String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
				if(null != flowId && !"".equals(flowId))
				{//跟新回执会计套号
					accTrdDaily.setAcctNo(flowId);
					accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
				}
				
				//逾期确认倒起息需求进行中收计提冲销
				Map<String,Object> feeMap = new HashMap<String, Object>();
				feeMap.put("dealNo", productApproveMain.getDealNo());
				List<TdProductFeeDeal> feeDealList = productFeeDealMapper.getAllProductFeeDeals(feeMap);
				if(feeDealList.size() >0){
					for (TdProductFeeDeal tdProductFeeDeal : feeDealList) {
						if(tdProductFeeDeal.getFeeCtype().equals(DictConstants.intType.chargeAfter)){ //后收息逾期计提进行冲销
							accTrdDaily = new TdAccTrdDaily();
							accTrdDaily.setDealNo(tdProductFeeDeal.getDealNo());
							accTrdDaily.setRefNo(tdOverDueConfirm.getDealNo());
							accTrdDaily.setAccType(DurationConstants.AccType.INTEREST_FEE_OVERDUE);
							accTrdDaily.setAccAmt(tdProductFeeDeal.getAccruedTint());
							accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
							accTrdDaily.setPostDate(postdate);
							accTrdDailyMapper.insert(accTrdDaily);
							
							tdProductFeeDeal.setAccruedTint(0.00);//累计每日计提
							tdProductFeeDeal.setActualTint(0.00);//累计每日利息
							this.productFeeDealMapper.updateTdProductFeeDealByFeeDealNo(BeanUtil.beanToMap(tdProductFeeDeal));
							//调用账务接口
							amtMap = new HashMap<String, Object>();
							amtMap.put(DurationConstants.AccType.INTEREST_FEE_OVERDUE, accTrdDaily.getAccAmt());
							
							instructionPackage = new InstructionPackage();
							Map<String, Object> feeInfoMap = new HashMap<String, Object>();
							feeInfoMap = BeanUtil.beanToMap(productApproveMain);
							feeInfoMap.put("overdueDays", PlaningTools.daysBetween(odVdate,postdate));
							feeInfoMap.put("intType", tdProductFeeDeal.getFeeCtype());
							instructionPackage.setInfoMap(feeInfoMap);
							instructionPackage.setAmtMap(amtMap);
							String feeFlowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
							if(null != feeFlowId && !"".equals(feeFlowId))
							{//跟新回执会计套号
								accTrdDaily.setAcctNo(feeFlowId);
								accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
							}
						}
					}
				}
				
			}//倒起息结束
			
			//插入新增利率
			TdRateRange addRange = new TdRateRange();
			addRange.setBeginDate(odVdate);
			addRange.setExecRate(ParameterUtil.getDouble(params, "odRate", 0.00));
			addRange.setEndDate(PlaningTools.addOrSubMonth_Day(odVdate, Frequency.YEAR, 10));
			addRange.setDealNo(productApproveMain.getDealNo());
			addRange.setVersion(productApproveMain.getVersion());
			addRange.setAccrulType(DurationConstants.RateType.ACCRU_TYPE);
			rateRangeMapper.insert(addRange);
		}catch(Exception e)
		{
			JY.debug(e.getMessage());
		}
	}
	
	@Override
	public Page<TdOverDueConfirm> getTradeOverDuePage(Map<String, Object> params, int isFinished) {
		Page<TdOverDueConfirm> page= new Page<TdOverDueConfirm>();
		if(isFinished == 2){
			page= tdOverDueConfirmMapper.getTradeOverDueList(params,ParameterUtil.getRowBounds(params));
			
		}else if(isFinished == 1){
			page= tdOverDueConfirmMapper.getTradeOverDueListFinished(params,ParameterUtil.getRowBounds(params));
			
		}else{
			page= tdOverDueConfirmMapper.getTradeOverDueListMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdOverDueConfirm> list =page.getResult();
		if(list == null) {
			return page;
		}
		for(TdOverDueConfirm te :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(te.getRefNo());
			if(main!=null){
				te.setProduct(main.getProduct());
				te.setParty(main.getCounterParty());
				te.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	@Override
	public void deleteTradeOverDue(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for (int i = 0; i < dealNos.length; i++) {
			//删除前先检查是否存在流程,若存在则撤消
			flowOpService.ApproveCancel(dealNos[i]);
			tdOverDueConfirmMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}
	}

	@Override
	public void insertOverDueDeal(Map<String, Object> params) {
		tdOverDueConfirmMapper.insert(overDueDeal(params));
		
	}

	private TdOverDueConfirm overDueDeal(Map<String, Object> params){
		// map转实体类
		TdOverDueConfirm overDueConfirm = new TdOverDueConfirm();
		try {
			BeanUtil.populate(overDueConfirm, params);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
        if(overDueConfirm.getRefNo() != null){
        	TdProductApproveMain productApproveMain = productApproveService.getProductApproveActivated(overDueConfirm.getRefNo());
            JY.require(productApproveMain!=null, "原交易信息不存在");
            overDueConfirm.setVersion(productApproveMain.getVersion());
        }
       
		//审批完成，为交易单添加一条数据
    	if(!DictConstants.DealType.Verify.equals(overDueConfirm.getDealType())){
    		overDueConfirm.setaDate(DateUtil.getCurrentDateAsString());
    	}
    	overDueConfirm.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
    	overDueConfirm.setCfType(DictConstants.TrdType.CustomTradeOverDue);
    	
    	
    	overDueConfirm.setSponsor(SlSessionHelper.getUser().getUserId());
    	overDueConfirm.setSponInst(SlSessionHelper.getInstitution().getInstId());
    	overDueConfirm.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
        
    	TdOverDueConfirm othersConfirm = tdOverDueConfirmMapper.queryOverDueDealByRefNo(overDueConfirm.getRefNo());
    	if(null != othersConfirm)
    	{
    		HashMap<String,Object> map = new HashMap<String, Object>();map.put("trade_id", othersConfirm.getDealNo());
    		TtTrdTrade trade = tradeMapper.selectTradeForTradeId(map);
    		if(!"8".equals(trade.getTrade_status()) && !"17".equals(trade.getTrade_status()))
    		{
    			throw new RException(overDueConfirm.getRefNo()+"已存在逾期交易:"+othersConfirm.getDealNo()+"<br>状态:"+trade.getTrade_status());
    		}
    	}
    	
		return overDueConfirm;
	}
	
	@Override
	public void updateOverDueDeal(HashMap<String, Object> map) {
		tdOverDueConfirmMapper.updateByPrimaryKey(overDue(map));
		
	}

	@Override
	public boolean validOverDueDealByDealno(String dealNo) {
		return tdOverDueConfirmMapper.queryOverDueDealCountByDealno(dealNo) > 0;
	}

	@Override
	public double calOverDueIntAmt(Map<String, Object> params) {
		try {
			String odVdate = ParameterUtil.getString(params, "odVdate", dayendDateService.getDayendDate());
			TdProductApproveMain productApproveMain = null;//重新查询原交易数据
			if(!"".equals(ParameterUtil.getString(params, "refNo", ""))){
	        	productApproveMain = productApproveService.getProductApproveActivated((ParameterUtil.getString(params, "refNo", "")));
	            JY.require(productApproveMain != null, "原交易信息不存在");
	        }else{
	        	throw new RException("原交易信息不存在！");
	        }
			/***************************************************************************
			 * 判定利率变更生效日与当前账务日期大小  小于账务日期，表明是倒起息交易，需要出利息调整
			 */
			if(PlaningTools.compareDate2(dayendDateService.getDayendDate(),odVdate,"yyyy-MM-dd"))//倒起息
			{
				//封装利率区间 带入计算公式
				List<InterestRange> interestRanges = new ArrayList<InterestRange>();
				InterestRange interestRange = new InterestRange();
				interestRange.setStartDate(odVdate);
				interestRange.setEndDate(dayendDateService.getDayendDate());
				interestRange.setRate(ParameterUtil.getDouble(params, "odRate", 0.00));
				interestRanges.add(interestRange);
				//封装本金区间  带入计算公式
				List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();
				List<TdAmtRange> amtRanges = amtRangeMapper.getAmtRanges(params);
				PrincipalInterval principalInterval = null;
				double tempAmt = productApproveMain.getAmt();//本金
				if(amtRanges.size() > 0){//发生过还本 那么从实际TT_AMT_RANGE还本区间表获取还本区间
					int i = 0;
					for(i =0 ;i <amtRanges.size();i++)
					{
						tempAmt = PlaningTools.sub(tempAmt, amtRanges.get(i).getExecAmt());//本金递减	
					}
				}
				principalInterval = new PrincipalInterval();
				principalInterval.setStartDate(odVdate);
				principalInterval.setResidual_Principal(tempAmt);
				principalInterval.setEndDate(PlaningTools.addOrSubMonth_Day(odVdate, Frequency.YEAR, 10));//无限逾期10年(逾期情况)
				principalIntervals.add(principalInterval);
				try {//倒起息差额计算
					return PlaningTools.fun_Calculation_interval_Range(interestRanges, principalIntervals, odVdate, dayendDateService.getDayendDate(), odVdate, 
							DayCountBasis.equal(productApproveMain.getBasis()));
				} catch (ParseException e) {
					JY.require(true,"计算区间差额计提出错"+e.getMessage());
				} 
			}  
		}catch(Exception e)
		{
			JY.debug(e.getMessage());
		}
		return 0;
	}
	
	
	
	@Override
	public Page<TdOverDueConfirm> getOverdueConfirmForDealNoPage(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.tdOverDueConfirmMapper.getAllOverdueConfirmsByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}
	@Override
	public TdOverDueConfirm getOverDueConfirmById(String dealno) {
		// TODO Auto-generated method stub
		return tdOverDueConfirmMapper.getOverDueConfirmById(dealno);
	}

}
