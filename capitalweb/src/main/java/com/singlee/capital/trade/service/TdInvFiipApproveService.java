package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdInvFiipApprove;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统

 * @version 1.0
 */
public interface TdInvFiipApproveService {


	/**
	 * 根据主键查询对象
	 */
	public TdInvFiipApprove getTdInvFiipApproveById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 */
	public Page<TdInvFiipApprove> getTdInvFiipApprovePage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 */
	public void createTdInvFiipApprove(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 */
	public void updateTdInvFiipApprove(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 */
	public void deleteTdInvFiipApprove(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
}
