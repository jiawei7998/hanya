package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTradeExpire;

/**
 * @projectName 同业业务管理系统
 * @className 资产到期服务接口
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TradeExpireService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 资产到期主键
	 * @return 资产到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdTradeExpire getTradeExpireById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 资产到期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdTradeExpire> getTradeExpirePage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 资产到期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createTradeExpire(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateTradeExpire(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 资产到期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteTradeExpire(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
}
