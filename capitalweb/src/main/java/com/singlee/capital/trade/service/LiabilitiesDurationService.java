package com.singlee.capital.trade.service;

import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdLiabilitiesDuration;
import com.singlee.capital.trade.model.TdProductApproveMain;

/**
 * @projectName 同业业务管理系统
 * @className 负债存续期服务接口
 * @description TODO
 * @author Hunter
 * @createDate 2016-9-25 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface LiabilitiesDurationService {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 负债存续期主键
	 * @return 负债存续期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdLiabilitiesDuration getLiabilitiesDurationById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 负债存续期对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdLiabilitiesDuration> getLiabilitiesDurationPage (Map<String, Object> params, int isFinished);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 负债存续期对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void createLiabilitiesDuration(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void updateLiabilitiesDuration(Map<String, Object> params);
	
	/**
	 * 删除
	 * 
	 * @param dealNos - 负债存续期主键列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public void deleteLiabilitiesDuration(String[] dealNos);
	
	/**
	 * 获取原交易信息
	 * @param dealNo
	 * @return
	 */
	public TdProductApproveMain getProductApproveMainOfBefore(Map<String,Object> map);
	
	
	/**
	 * 升版本 + 更改现金流
	 * @param trade_id 负债存续期交易单号
	 */
	public void dealVersionAndCashFlow(String trade_id);
}
