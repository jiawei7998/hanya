package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TD_FEES_PASSAGEWAY_DAILY")
public class TdFeesPassAgewayDaily implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String dayCounter; //计息基准
	private String refBeginDate;// 开始日
	private String refEndDate;// 结束日
	private double principal;// 本金
	private double interest;// 应计利息
	private int seqNumber; //期号
	private String refNo;
	private int version;
	private double actualRate;
	private String cno;
	private String intCal;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_PASSAGEWAY_DAILY.NEXTVAL FROM DUAL")
	private String cashflowId;// 现金流序列号 PK
	
	
	public String getIntCal() {
		return intCal;
	}
	public void setIntCal(String intCal) {
		this.intCal = intCal;
	}
	public String getDayCounter() {
		return dayCounter;
	}
	public void setDayCounter(String dayCounter) {
		this.dayCounter = dayCounter;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public double getActualRate() {
		return actualRate;
	}
	public void setActualRate(double actualRate) {
		this.actualRate = actualRate;
	}
	public String getCashflowId() {
		return cashflowId;
	}
	public void setCashflowId(String cashflowId) {
		this.cashflowId = cashflowId;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	
	

}
