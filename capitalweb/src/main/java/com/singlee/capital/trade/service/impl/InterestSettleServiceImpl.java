package com.singlee.capital.trade.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.cap.bookkeeping.service.BookkeepingService;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.cashflow.mapper.TdAmtRangeMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowDailyInterestMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.tpos.mapper.TrdTposMapper;
import com.singlee.capital.tpos.model.TdTrdTpos;
import com.singlee.capital.trade.acc.model.TdAccTrdDaily;
import com.singlee.capital.trade.mapper.TdInterestSettleMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.model.TdInterestSettle;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.service.InterestSettleService;
import com.singlee.capital.trade.service.ProductApproveService;

@Service
public class InterestSettleServiceImpl extends AbstractDurationService implements InterestSettleService {

	@Autowired
	private TdInterestSettleMapper tdInterestSettleMapper;
	
	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	
	@Autowired
	TdCashflowInterestMapper cashflowInterestMapper;
	
	@Autowired
	TdAmtRangeMapper amtRangeMapper;//实际现金流
	@Autowired
	TdCashflowCapitalMapper capitalMapper;//计划现金流
	@Autowired
	TdRateRangeMapper rangeMapper;//利率区间
	@Autowired
	TdCashflowDailyInterestMapper cashflowDailyInterestMapper;//每日计提
	@Autowired
	BatchDao batchDao;
	@Autowired
	DayendDateService dateService;
	@Autowired
	TrdTposMapper tposMapper;
	@Autowired
	BookkeepingService bookkeepingService;
	@Override
	public TdInterestSettle getInterestSettleById(String dealNo) {
		return tdInterestSettleMapper.getInterestSettleById(dealNo);
	}

	@Override
	public Page<TdInterestSettle> getInterestSettlePage(Map<String, Object> params, int isFinish) {
		Page<TdInterestSettle> page= new Page<TdInterestSettle>();
		if(isFinish == 1){
			page= tdInterestSettleMapper.getInterestSettleApproveList(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 2){
			page= tdInterestSettleMapper.getInterestSettleListFinish(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 3){
			page= tdInterestSettleMapper.getInterestSettleMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdInterestSettle> list =page.getResult();
		for(TdInterestSettle rc :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(rc.getRefNo());
			if(main!=null){
				rc.setProduct(main.getProduct());
				rc.setParty(main.getCounterParty());
				rc.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	/**
	 * 创建结息确认数据（order or trade+ratechang）
	 */
	@Override
	@AutoLogMethod(value = "创建结息确认数据")
	public void createInterestSettle(Map<String, Object> params) {
		
		tdInterestSettleMapper.insert(InterestSettle(params));
	}

	/**
	 * 修改结息确认数据（order or trade+InterestSettle）
	 */
	@Override
	@AutoLogMethod(value = "修改结息确认数据")
	public void updateInterestSettle(Map<String, Object> params) {
		tdInterestSettleMapper.updateByPrimaryKey(InterestSettle(params));
	}
	
	/**
	 * map转换成需要的InterestSettle对象
	 * @param map
	 * @return
	 */
	public TdInterestSettle InterestSettle(Map<String,Object> map){
		TdInterestSettle InterestSettle = new TdInterestSettle();
		try {
			BeanUtil.populate(InterestSettle, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		InterestSettle.setSettlIntadj(PlaningTools.sub(InterestSettle.getInterestSettlAmt(),InterestSettle.getSettlInt()));
        InterestSettle.setSponsor(SlSessionHelper.getUserId());
        InterestSettle.setSponInst(SlSessionHelper.getInstitutionId());
        InterestSettle.setaDate(dateService.getDayendDate());
        InterestSettle.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
        InterestSettle.setCfType(DictConstants.TrdType.CustomInterestSettleConfirm);
		return InterestSettle;
	}
	

	/**
	 * 删除结息确认数据（order or trade+InterestSettle）
	 */
	@Override
	public void deleteInterestSettle(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for(int i=0;i<dealNos.length;i++){
			tdInterestSettleMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}

	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdInterestSettle InterestSettle = new TdInterestSettle();
		BeanUtil.populate(InterestSettle, params);
		InterestSettle = tdInterestSettleMapper.searchInterestSettle(InterestSettle);
		return productApproveService.getProductApproveActivated(InterestSettle.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdInterestSettle InterestSettle = new TdInterestSettle();
		InterestSettle.setDealNo(ParameterUtil.getString(params, "dealNo", ""));
		InterestSettle = tdInterestSettleMapper.searchInterestSettle(InterestSettle);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(InterestSettle.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(InterestSettle.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(InterestSettle.getDealNo());//将结息确认的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomInterestSettleConfirm);//结息确认
		tdTrdEvent.setEventTable("TD_RATE_CHANGE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		/**
		 * 结息确认后出结息确认分录
		 */
		TdInterestSettle interestSettle = new TdInterestSettle();
		BeanUtil.populate(interestSettle, params);
		if(interestSettle.getSettleFlag().equals(DictConstants.YesNo.NO)){//不清算,结息金额转入头寸
			String WorkDate = dateService.getDayendDate();//当前业务日
			List<TdAccTrdDaily> accTrdDailies = new ArrayList<TdAccTrdDaily>();
			Map<String, Object> amtMap = null;
			amtMap = new HashMap<String, Object>();
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("refNo", interestSettle.getRefNo());
			TdProductApproveMain productApproveMain = this.productApproveService.getProductApproveActivated(interestSettle.getRefNo());
			TdAccTrdDaily accTrdDaily = new TdAccTrdDaily();
			accTrdDaily.setDealNo(interestSettle.getDealNo());
			accTrdDaily.setAccType(DurationConstants.AccType.CONFIRM_SETTL_INT);
			accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
			accTrdDaily.setRefNo(interestSettle.getDealNo());//结息确认的DEALNO
			accTrdDaily.setPostDate(WorkDate);
			if(interestSettle.getSettlIntadj() >= 0){
				accTrdDaily.setAccAmt(interestSettle.getInterestSettlAmt());//结息金额
				amtMap.put(DurationConstants.AccType.CONFIRM_SETTL_INT, accTrdDaily.getAccAmt());
				accTrdDailies.add(accTrdDaily);
				
				//结息金额与账务日期已计提总额的差额
				accTrdDaily = new TdAccTrdDaily();
				accTrdDaily.setAccType(DurationConstants.AccType.CONFIRM_SETTL_INTADJ);
				accTrdDaily.setAccAmt(-interestSettle.getSettlIntadj());//活期收息结息调整
				accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
				accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				accTrdDaily.setRefNo(interestSettle.getDealNo());//结息确认的DEALNO
				accTrdDaily.setPostDate(WorkDate);
				amtMap.put(DurationConstants.AccType.CONFIRM_SETTL_INTADJ, accTrdDaily.getAccAmt());
				accTrdDailies.add(accTrdDaily);
			}else{
				accTrdDaily.setAccAmt(interestSettle.getInterestSettlAmt());//结息金额
				amtMap.put(DurationConstants.AccType.CONFIRM_SETTL_INT, accTrdDaily.getAccAmt());
				accTrdDailies.add(accTrdDaily);
				
				//结息金额与账务日期已计提总额的差额
				accTrdDaily = new TdAccTrdDaily();
				accTrdDaily.setAccType(DurationConstants.AccType.CONFIRM_SETTL_INTADJ);
				accTrdDaily.setAccAmt(-interestSettle.getSettlIntadj());//活期收息结息调整
				accTrdDaily.setDealNo(productApproveMain.getDealNo());//原始交易的DEALNO
				accTrdDaily.setOpTime(DateUtil.getCurrentCompactDateTimeAsString());
				accTrdDaily.setRefNo(interestSettle.getDealNo());//结息确认的DEALNO
				accTrdDaily.setPostDate(WorkDate);
				amtMap.put(DurationConstants.AccType.CONFIRM_SETTL_INTADJ, accTrdDaily.getAccAmt());
				accTrdDailies.add(accTrdDaily);
			}
			TdTrdTpos tpos=null;
			/*****************TPOS*************************/
			tpos = new TdTrdTpos();
			tpos.setDealNo(interestSettle.getRefNo());
			tpos.setVersion(interestSettle.getVersion());
			tpos = tposMapper.selectByPrimaryKey(tpos);
			tpos.setSettlAmt(PlaningTools.add(tpos.getSettlAmt(), interestSettle.getSettlInt()));//结息金额累计进头寸
			tpos.setInterestSettleAmt(0.00);//当期结息金额为0
			tpos.setActualTint(PlaningTools.add(tpos.getActualTint(), interestSettle.getSettlIntadj()));//累计每日利息
			tpos.setPostdate(WorkDate);
			this.tposMapper.updateByPrimaryKey(tpos);
			//调用账务接口
			InstructionPackage instructionPackage = new InstructionPackage();
			instructionPackage.setInfoMap(BeanUtil.beanToMap(productApproveMain));
			instructionPackage.setAmtMap(amtMap);
			try{
				String flowId = bookkeepingService.generateEntry4Instruction(instructionPackage);
				if(null != flowId && !"".equals(flowId))
				{//跟新回执会计套号
					accTrdDaily.setAcctNo(flowId);
					//accTrdDailyMapper.updateAccTrdDailyForAcctNo(BeanUtil.beanToMap(accTrdDaily));
				}
			}catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				JY.debug(e.getMessage());
			}
			batchDao.batch("com.singlee.capital.trade.acc.mapper.TdAccTrdDailyMapper.insert", accTrdDailies);
		}
	}

	@Override
	public Page<TdInterestSettle> getInterestSettleForDealNoPage(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return this.tdInterestSettleMapper.getAllInterestSettlesByDealnoVersion(params,ParameterUtil.getRowBounds(params));
	}
	/**
	 * 单个删除
	 */
	@Override
	public void removeInterestSettle(String dealNo) {
		tdInterestSettleMapper.removeInterestSettle(dealNo);
		
	}

}
