package com.singlee.capital.trade.service.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jxl.Workbook;

import org.activiti.engine.RuntimeService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.mapper.TrdOrderMapper;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.approve.service.ApproveServiceListener;
import com.singlee.capital.base.cal.Frequency;
import com.singlee.capital.base.cal.LogManager;
import com.singlee.capital.base.cal.tools.PlaningTools;
import com.singlee.capital.base.mapper.Acctno407Mapper;
import com.singlee.capital.base.model.Acctno407;
import com.singlee.capital.base.util.SettleConstants;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.eu.util.EdExceptionCode;
import com.singlee.capital.interfacex.mapper.Ti2ndPaymentMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceDependScopeMapper;
import com.singlee.capital.interfacex.mapper.TiInterfaceLogMapper;
import com.singlee.capital.interfacex.model.CommonRqHdr;
import com.singlee.capital.interfacex.model.CreditBeaRec;
import com.singlee.capital.interfacex.model.DebitBeaRec;
import com.singlee.capital.interfacex.model.EnterpriseApproveRq;
import com.singlee.capital.interfacex.model.EnterpriseApproveRs;
import com.singlee.capital.interfacex.model.FinCreditApproveRq;
import com.singlee.capital.interfacex.model.FinCreditApproveRs;
import com.singlee.capital.interfacex.model.TDpAcAciModRq;
import com.singlee.capital.interfacex.model.TDpAcAciModRs;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRq;
import com.singlee.capital.interfacex.model.TDpAcctNewtdaAaaRs;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRq;
import com.singlee.capital.interfacex.model.TStBeaRepayAaaRs;
import com.singlee.capital.interfacex.model.TiInterfaceDependScope;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRq;
import com.singlee.capital.interfacex.model.UPPSCdtTrfRs;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRec;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRq;
import com.singlee.capital.interfacex.model.UPPSigBusiQueryRs;
import com.singlee.capital.interfacex.service.SocketClientService;
import com.singlee.capital.interfacex.socketservice.InterfaceCode;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.system.service.SysParamService;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdProductApproveMainMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.FinEnterpriseTrade;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.pojo.TradeEditVo;
import com.singlee.capital.trade.pojo.TrdTradeVo;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.ProductCustCreditService;
import com.singlee.capital.trade.service.TradeManageService;
import com.singlee.capital.trade.service.TradeServiceListener;
import com.singlee.capital.trust.mapper.TrdTrustMapper;
import com.singlee.capital.trust.model.TtTrdTrust;
import com.singlee.capital.trust.service.TrustManageService;
import com.singlee.capital.users.mapper.TdCustAccMapper;
import com.singlee.capital.users.model.TdCustAcc;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import com.singlee.slbpm.listener.CreditListener;
import com.singlee.slbpm.mapper.ProcTaskEventMapper;
import com.singlee.slbpm.service.FlowQueryService;

/**
 * 交易单管理业务层
 * @author thinkpad
 *
 */
@Service("tradeManageService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TradeManageServiceImpl implements TradeManageService,SlbpmCallBackInteface,TaskEventInterface{
	@Autowired
	TtInstitutionSettlsMapper ttInstitutionSettlsMapper;
	@Autowired
	private  Acctno407Mapper acctno407Mapper;
	@Autowired
	private  TdCustAccMapper tdCustAccMapper;
	@Autowired
	private Ti2ndPaymentMapper ti2ndPaymentMapper;
	@Autowired
	private TiInterfaceLogMapper tiInterfacleLogMapper;
	@Autowired
    private	DayendDateService dayendDateService;
	/** 交易单dao **/
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	
	/** 审批单dao **/
	@Autowired
	private TrdOrderMapper trdOrderMapper;
	
	/** 委托单dao **/
	@Autowired
	private TrdTrustMapper trustManageDao;
	
	/** 审批单业务层 **/
	@Autowired
	private ApproveManageService approveManageService;

	/** 委托单业务层 **/
	@Autowired
	private TrustManageService trustManageService;

	/** 系统参数表 **/
	@Autowired
	private SysParamService sysParamService;
	
	@Autowired
	private UserParamService userParamService;
	/** 流程环节事件dao*/
	@Autowired
	private ProcTaskEventMapper procTaskEventMapper;
	/** 额度处理业务层*/
	@Autowired
	private ProductCustCreditService productCustCreditService;
	@Autowired
	private ProductApproveService productApproveService;

	@Autowired
	private TiInterfaceDependScopeMapper tiInterfaceDependScopeMapper;
	@Autowired
	private SocketClientService socketClientService;
	@Autowired
	private FlowQueryService flowQueryService;
	/*@Autowired
	private TctermService tctermService;*/
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private TdProductApproveMainMapper productApproveMainMapper;
	/** 下行数据对象dao **/
//	@Autowired
//	private ExecutionReportCSTPDao executionReportCSTPDao;
//	@Autowired
//	private MsgService msgService;
	
//	@Autowired
//	private ExecutionReportService executionReportService;
	@Autowired
	EdCustManangeService edCustManangeService;
	/**模糊查询dao层*/
	
	@Autowired
	private CreditListener creditListener;
	
	DecimalFormat format = new DecimalFormat("########.00");
	/**
	 * 查询交易单对象，用于工作台显示
	 * @param map
	 * @return
	 */
	@Override
	public List<TtTrdTrade> searchTradeForDeskPage(HashMap<String,Object> map, int pageNum, int pageSize){
		map.put("trade_user",ParameterUtil.getString(map,"opUserId",""));
		return tradeManageMapper.selectTtTrdTrade(map);
		
	}
	
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	@Override
	public Object searchTradeList(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		
		int pageNumber =ParameterUtil.getInt(map,"pageNumber",1);
		int pageSize = ParameterUtil.getInt(map,"pageSize",12);
		String trdtype_search = ParameterUtil.getString(map,"trdtype_search","");
		String trade_status_search = ParameterUtil.getString(map,"trade_status_search","");
		map.put("trdtype_search", StringUtil.checkEmptyNull(trdtype_search)?null:trdtype_search.split(","));
		map.put("trade_status_search", StringUtil.checkEmptyNull(trade_status_search)?null:trade_status_search.split(","));
		map.put("user_id", ParameterUtil.getString(map,"opUserId",""));
		return tradeServiceListener.searchTradeList(map, pageNumber, pageSize);
	}
	/**
	 * 根据不同的业务类型,调用不同的bean执行查询动作
	 * @param map
	 * @return
	 */
	@Override
	@SuppressWarnings("unused")
	public Object searchTradeListForCharges(HashMap<String,Object> map){
		map.put("user_id", ParameterUtil.getString(map,"opUserId",""));
		map.put("inst_id", ParameterUtil.getString(map,"opInstId",""));// 增加机构隔离
		/*String trdtype_search = ParameterUtil.getString(map,"trdtype","");
		map.put("trdtype", StringUtil.checkEmptyNull(trdtype_search)?null:trdtype_search.split(","));*/
		// 1.数组查询
		Page<TradeEditVo> Page = new Page<TradeEditVo>();
		List<TradeEditVo> list = tradeManageMapper.searchTradeListForCharges(map);
		return Page;
	}
	/**
	 * 查询交易单明细信息
	 * @param map
	 * @return
	 */
	@Override
	public HashMap<String,Object> selectTradeInfo(HashMap<String,Object> map){
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder"))), "缺少必须的参数folder.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("trade_id"))), "缺少必须的参数trade_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(map.get("version_number"))), "缺少必须的参数version_number.");

		//1.查询交易单明细信息
		TradeEditVo tradeEditVo = tradeManageMapper.selectTradeEditForTradeId(map);
		HashMap<String,Object> objMap = ParentChildUtil.ClassToHashMap(tradeEditVo);

		//2.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(StringUtil.toString(map.get("folder")),DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.selectTradeInfo(objMap);
		
		return objMap;
	}
	
	/**
	 * 保存交易单信息
	 * @param map
	 */
	@Override
	@AutoLogMethod(value = "保存交易")
	public TtTrdTrade saveTradeInfo(HashMap<String,Object> map){
		//1.增加逻辑，当为非测试环境下，当业务日期 不等于 自然日期时不允许保存审批单
		if(userParamService.getBooleanSysParamByName("system.isJudgeSysDateAndBusDate", false)){
			JY.require(DateUtil.getCurrentDateAsString().equals(dayendDateService.getSettlementDate()), "业务日历不等于当前日期,无法执行保存操作.");
		}
		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
		//approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		boolean isAdd=true;
		TtTrdTrade ttTrdTrade = new TtTrdTrade();
		//1.将hashmap转换为TtTrdTrade对象
		ParentChildUtil.HashMapToClass(map, ttTrdTrade);
		//添加导入方式为excel
//		ttTrdTrade.setImp_source(DictConstants.TradeImportSource.Excel);
		//增加内部资金账户判断
//		TtAccInSecu ttAccInSecu = accInSecuService.getAcc(ttTrdTrade.getSelf_insecuaccid());
//		JY.require(ttAccInSecu!=null && DictConstants.AccStatus.Enabled.equals(ttAccInSecu.getStatus()), "保存审批单时,请确认本方内部证券账户为可用状态.");
		//JY.require(!StringUtil.checkEmptyNull(ttTrdTrade.getSelf_incashaccid()), "保存交易单时,内部资金账号不能为空.");
		//增加交易日判断
		//approveInfoService.processToJudgeTradeDate(ttTrdTrade.getM_type(), ttTrdTrade.getTrade_date());
		if(StringUtil.isNullOrEmpty(ttTrdTrade.getTrade_id())){
        	String trade_id = tradeManageMapper.getTradeId(map);
    		ttTrdTrade.setTrade_id(trade_id);
        }else{
        	isAdd=false;
        }
        String opUserId = ParameterUtil.getString(map, "opUserId", "");
		ttTrdTrade.setTrade_time(DateUtil.getCurrentDateTimeAsString());
		
		//3.默认属性赋值
		ttTrdTrade.setIs_active(DictConstants.YesNo.YES);
		ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.New);
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setOperator_id(opUserId);
		ttTrdTrade.setTrade_user(ParameterUtil.getString(map, "trade_user", opUserId));

		//edit by wangchen on 2017/8/17
		//校验是否存在于流程中（退回发起）,若存在则不修改状态
		Map<String,Object> prdMap = new HashMap<String,Object>();
		prdMap.put("pageNumber", "1");
		prdMap.put("userId", SlSessionHelper.getUserId());
		prdMap.put("pageSize", "99999999");
		List<TdProductApproveMain> unfinishApproveList = productApproveService.getProductApprovePage(prdMap, 1);
		for(TdProductApproveMain item : unfinishApproveList){
			if(ttTrdTrade.getTrade_id().equals(item.getDealNo())){
				ttTrdTrade.setTrade_status(item.getApproveStatus());
				break;
			}
		}
		
		//成交单是否重复校验：相同编号的成交单不可重复保存
		if(isAdd && !StringUtil.isNullOrEmpty(ttTrdTrade.getExecid())){
			HashMap<String, Object> execidCheckMap = new HashMap<String, Object>();
			execidCheckMap.put("execid", ttTrdTrade.getExecid());
			execidCheckMap.put("is_active", DictConstants.YesNo.YES);
			List<TtTrdTrade> execCheckList = tradeManageMapper.selectTtTrdTrade(execidCheckMap);
			JY.require(execCheckList.size() == 0, "数据库内已有成交单编号相同的交易单，不可重复添加。");
		}
		
		//4.调用其他业务接口
		String folder = ParameterUtil.getString(map, "folder", "");
		if(StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))){
			folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(folder)), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.saveTrade(map, ttTrdTrade,isAdd);
		//5.根据order_id判断执行新增或修改操作 
		if(isAdd){
			tradeManageMapper.insertTrade(ttTrdTrade);
		}else{
			tradeManageMapper.updateImportTrade(ttTrdTrade);
		}
		tradeServiceListener.afterSave(map, ttTrdTrade);
		return ttTrdTrade;
	}
	
	
	/**
	 * 注销交易单信息
	 * @param trade_id
	 */
	@Override
	@AutoLogMethod(value = "取消交易")
	public void cancelTrade(String trade_id,int version_number,String folder,String memo,String opUserId){
		JY.require(!StringUtil.checkEmptyNull(trade_id), "缺少必须的参数trade_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(version_number)), "缺少必须的参数version_number.");

		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
		//approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId()==null?"999999" : SlSessionHelper.getInstitutionId());
		JY.info("撤销交易开始,交易单号" + trade_id);
		//1.锁定交易单
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("version_number", version_number);
		map.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		if(StringUtil.isNullOrEmpty(folder)){
			folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
		}
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		opUserId = StringUtil.isNullOrEmpty(opUserId)?"admin":opUserId;

		//2.交易单验证
		JY.require(ttTrdTrade != null, "选中交易单对象为空或不存在.");
		//JY.require(DictConstants.ApproveStatus.New.equals(ttTrdTrade.getTrade_status()), "删除的交易单状态必须为[待确认]状态.");

		//3.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.cancelTrade(ttTrdTrade);
		
		//4.锁定委托单(当委托单不存在时，说明该单据由交易单直接生成，则锁定交易单)
		if(!StringUtil.checkEmptyNull(ttTrdTrade.getTrust_id())){
			//3.1 锁定审批单
			map = new HashMap<String, Object>();
			map.put("trust_id",ttTrdTrade.getTrust_id());
			map.put("version_number",ttTrdTrade.getTrust_version());
			map.put("is_locked", true);
			TtTrdTrust ttTrdTrust = trustManageDao.selectTrustForTrustId(map);

			//3.2 更新委托单余额
			String execid = ttTrdTrust.getExecid();
			trustManageService.updateTrustRemainAmount(ttTrdTrust, ttTrdTrade.getTotalamount(), 0,opUserId);
			//拒绝交易的时候，更新备注信息 add by lihb 20140120
			if(!StringUtil.checkEmptyNull(execid)){
				HashMap<String, Object> cstpMap = new HashMap<String, Object>();
				cstpMap.put("dealNumber", execid);
				if(!StringUtil.checkEmptyNull(memo)){
					cstpMap.put("remark", memo);
				}
//				executionReportService.updateByExecId(cstpMap);
//				//消息发送
//				String tipMsg = "成交编号为[" + execid + "]的下行数据被" + SessionManager.getCurrentUserVo().getUser_name()+ "拒绝，备注："+memo+"。请重新处理!";
//				ExecutionReportCSTP cstp = executionReportService.selectByExecId(execid);
//				if(cstp!=null){
//					msgService.publishCstpRejectBySettle(ttTrdTrust.getExecid(),tipMsg, cstp.getUpdateUser());
//				}
			}
		}else if(!StringUtil.checkEmptyNull(ttTrdTrade.getOrder_id())){
			//3.1 锁定审批单
			map = new HashMap<String, Object>();
			map.put("order_id",ttTrdTrade.getOrder_id());
			map.put("is_locked", true);
			TtTrdOrder ttTrdOrder = trdOrderMapper.selectOrderForOrderId(map);

			//3.2 更新审批单余额
			approveManageService.updateApproveRemainAmount(ttTrdOrder, ttTrdTrade.getTotalamount(), 0, false,opUserId);
			
//			//消息发送
//			String tipMsg = "审批编号为[" + ttTrdOrder.getOrder_id() + "]的交易被" + 
//					SessionManager.getCurrentUserVo().getUser_name()+ "拒绝，备注："+memo+"。请重新处理!";
//			msgService.publishCstpRejectBySettle(ttTrdOrder.getOrder_id(),tipMsg, ttTrdOrder.getSelf_traderid());
		}else{
			//存在一种交易单单没有审批单与委托单。如：债券行权时生成都交易单数据
		}
				
		//5.更新交易单信息
		ttTrdTrade.setIs_active(DictConstants.YesNo.NO);
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setOperator_id(opUserId);
		ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.Cancle);
		
		tradeManageMapper.updateTrade(ttTrdTrade);
		JY.info("撤销交易完成,交易单号" + trade_id);
	}
	
	/**
	 * 确认交易单信息
	 * @param trade_id
	 * 没有进行机构轧帐检查
	 */
	@Override
	@AutoLogMethod(value = "成交确认交易")
	public void confirmTrade(String trade_id,int version_number,String folder,String opUserId){
		JY.require(!StringUtil.checkEmptyNull(trade_id), "缺少必须的参数trade_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(version_number)), "缺少必须的参数version_number.");
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		
		//1.锁定交易单
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("version_number", version_number);
		map.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);
		
		//2.交易单验证
		JY.require(ttTrdTrade != null, "选中交易单对象为空或不存在.");
		JY.require(DictConstants.ApproveStatus.Verifying.equals(ttTrdTrade.getTrade_status()), "交易单必须为核实中状态");
		
		//3.生成交易指令
		//SettleInstPackage pack = null;
		/*if(settleExternalService.isSettle(ttTrdTrade)){
			TrdTradeVo trdTradeVo = new TrdTradeVo();
			ParentChildUtil.fatherToChildWithoutException(ttTrdTrade, trdTradeVo);
			pack = settleInstService.buildSettleInst(trdTradeVo);
		}*/
		//4.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
		if(!StringUtil.isNullOrEmpty(beanName)){
			TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
			tradeServiceListener.dealConfirmTrade(ttTrdTrade);
		}
	
		//5.更改交易单状态
		ttTrdTrade.setConfirm_date(dayendDateService.getSettlementDate());
		ttTrdTrade.setConfirm_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setConfirm_user(opUserId);
		//ttTrdTrade.setTrade_status(DictConstants.TradeStatus.FINISH);
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setOperator_id(opUserId);
		tradeManageMapper.updateTrade(ttTrdTrade);
	}
	
	@Override
	public void dealConfirmTrade(String trade_id, int version_number, String folder,String opUserId) {
		// TODO Auto-generated method stub
		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
		//approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		confirmTrade(trade_id, version_number, folder,opUserId);
	}
	/**
	 * 取消确认交易单信息
	 * @param trade_id
	 */
	@Override
	@AutoLogMethod(value = "撤销成交确认交易")
	public void withdrawTrade(String trade_id,int version_number,String folder,String opUserId){
		JY.require(!StringUtil.checkEmptyNull(trade_id), "缺少必须的参数trade_id.");
		JY.require(!StringUtil.checkEmptyNull(StringUtil.toString(version_number)), "缺少必须的参数version_number.");
		//JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		JY.info("撤销成交确认交易开始,交易单号" + trade_id);
		//2.增加逻辑：机构轧帐完成后，判断是否允许保存审批单
		//approveInterfaceService.rollingAccountCheck(SlSessionHelper.getInstitutionId());
		
		//1.锁定交易单
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", trade_id);
		map.put("version_number", version_number);
		map.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(map);

		//5.调用其他业务接口
		if(StringUtil.checkEmptyNull(StringUtil.toString(map.get("folder")))){
			folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
		}
		
		//2.交易单验证
		JY.require(ttTrdTrade != null, "选中交易单对象为空或不存在.");
		JY.require(DictConstants.ApproveStatus.Verified.equals(ttTrdTrade.getTrade_status()), "撤销的交易单状态必须为[出账中]状态.");

		//3.生成交易指令
		TrdTradeVo trdTradeVo = new TrdTradeVo();
		ParentChildUtil.fatherToChildWithoutException(ttTrdTrade, trdTradeVo);
		//settleInstService.withdrawInstruction(trdTradeVo);
		
		//3.调用其他业务接口
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.withdrawTrade(trdTradeVo);
		
		//5.更改交易单状态
		ttTrdTrade.setTrade_status(DictConstants.ApproveStatus.New);
		ttTrdTrade.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttTrdTrade.setOperator_id(opUserId);
		tradeManageMapper.updateTrade(ttTrdTrade);
		
		//6.更改审批单状态
		HashMap<String, Object> orderMap = new HashMap<String, Object>();
		orderMap.put("order_id", ttTrdTrade.getOrder_id());
		List<TtTrdOrder> orders = trdOrderMapper.selectTtTrdOrder(orderMap);
		JY.require(orders != null&& orders.size() == 1, "找不到对应的业务审批单或业务审批单重复,请确认！业务审批单号："+ttTrdTrade.getOrder_id());
		TtTrdOrder order = orders.get(0);
		order.setOrder_status(DictConstants.ApproveStatus.ApprovedPass);
		order.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		order.setOperator_id(opUserId);
		trdOrderMapper.updateApprove(order);
		JY.info("撤销成交确认交易完成,交易单号" + trade_id);
	}

	@Override
	public HashMap<String, Object> selectImportTrade(Workbook wb, String folder) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 流程状态变更(用于审批流程函数回调)
	 * @param flow_id		流程id
	 * @param serial_no		序号
	 * @param status		最新审批状态
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("trade_id", serial_no);
		tradeMap.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(tradeMap);
		//保存原来的审批状态
//		String oldTradeStatus = ttTrdTrade.getTrade_status();
		ttTrdTrade.setStrike_yield(0);
		
		//Map<String,Object> prdPropMap = productAndApproveMainService.getProductMain(serial_no); //获取交易单产品性质(1.资产，2负债，3投资额度)	
		
		//限额接口调用：更改后交易单状态为【待审批】则需要增加限额，交易单状态为【审批拒绝】、【新建】或【注销】时限额为减
//		boolean isCancel = false;
		if(DictConstants.ApproveStatus.New.equals(status)){
//			isCancel = true;
			//6.消息发送
//			List<ApproveLogVo> list = flowService.searchFlowLog("", "", serial_no, "", "", "",false);
//			for (ApproveLogVo approveLogVo : list) {
//				//最后审批通过时user_id的编号为空格，需过滤该数据
//				if(StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim()) || ttTrdOrder.getUser_id().equals(approveLogVo.getUser_id())){
//					continue;
//				}
////				msgService.puslishApprovingCancel(serial_no, approveLogVo.getUser_id().trim());
//			}
			//如果是退回或驳回到发起岗则解冻额度
			if(DictConstants.FlowCompleteType.Redo.equals(flowCompleteType)){
				creditListener.reback(serial_no);
			}
			
		}else if(DictConstants.ApproveStatus.Cancle.equals(status)){
			//add by wangchen   2017/8/18 16:20  start
			TdProductApproveMain main = productApproveMainMapper.getProductApproveMainActivated(serial_no);
			if(main != null && main.getPrdNo()!=298){//298 企信不占额度
				/**
				 * 额度释放，花落知多少，暴力啊
				 * 20170920
				 */
				if(null != main.getRefNo()) {
                    if(!edCustManangeService.eduReleaseFlowService(main.getRefNo())){
                        throw new RException(serial_no+"额度释放失败!"+main.getRefNo());
                    }
                }
				if(!edCustManangeService.eduReleaseFlowService(main.getDealNo())){
					throw new RException(serial_no+"额度释放失败!");
				}
			}
			//end
//			isCancel = true;
			//6.消息发送
//			List<ApproveLogVo> list = flowService.searchFlowLog("", "", serial_no, "", "", "",false);
//			for (ApproveLogVo approveLogVo : list) {
//				//最后审批通过时user_id的编号为空格，需过滤该数据
//				if(StringUtil.checkEmptyNull(approveLogVo.getUser_id().trim()) || ttTrdOrder.getUser_id().equals(approveLogVo.getUser_id())){
//					continue;
//				}
////				msgService.puslishApproveCancel(serial_no, approveLogVo.getUser_id().trim());
//			}
		}else if(DictConstants.ApproveStatus.WaitApprove.equals(status)){
//			isCancel = false;
			//1.释放审批时交易对手占用额度
//			trdQuotaService.orderReleaseQuota(ttTrdTrade.getOrder_id());
			//2.占用核实时交易对手额度
//			trdQuotaService.tradeOccupyQuota(serial_no, ttTrdTrade.getVersion_number());
			// 待审批 ---》 待核实
			status = DictConstants.ApproveStatus.WaitVerify;
		}else if(DictConstants.ApproveStatus.Approving.equals(status)){
//			isCancel = false;
			//1.释放审批时交易对手占用额度
//			trdQuotaService.orderReleaseQuota(ttTrdTrade.getOrder_id());
			//2.占用核实时交易对手额度
//			trdQuotaService.tradeOccupyQuota(serial_no, ttTrdTrade.getVersion_number());
			// 审批中 ---》 核实中
			status = DictConstants.ApproveStatus.Verifying;
			
			if("3001".equals(ttTrdTrade.getTrdtype())){
				creditListener.commitLimit(ttTrdTrade.getTrade_id(), ttTrdTrade, status);
			}
		}else if(DictConstants.ApproveStatus.ApprovedNoPass.equals(status)){
			
			/*//释放额度 （拒绝动作会更改审批单拒绝，释放额度）
			if(DictConstants.prdProp.assets.equals(prdPropMap.get("prdProp"))){  //资产业务
				if(DictConstants.FlowCompleteType.Refuse.equals(flowCompleteType)){
					trdQuotaService.orderReleaseQuota(ttTrdTrade.getOrder_id());
					//删除现金流
					cashflowService.deleteCashFlow(ttTrdTrade.getI_code(), ttTrdTrade.getA_type(), ttTrdTrade.getM_type());
				}
			}*/
			
			//消息发送功能
//			msgService.puslishApprovedNoPass(ttTrdOrder.getOrder_id(), ttTrdOrder.getSelf_traderid());
//			isCancel = true;
		}else if(DictConstants.ApproveStatus.ApprovedPass.equals(status)){
			/**
			 * 判定划款日是否与<账务日期  如果是 那么需要等待截止到账务日期 进行划款，也就是划款日期<=账务日期不能进行划款操作；
			 */
			String folder = sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1();
			confirmTrade(ttTrdTrade.getTrade_id(), ttTrdTrade.getVersion_number(), folder, ttTrdTrade.getOperator_id());
			status = DictConstants.ApproveStatus.Verified;
		}else if(DictConstants.ApproveStatus.ApprovedNoPass.equals(status)){
			//审批拒绝时使用其他业务接口
				this.noPassApprove(ttTrdTrade, sysParamService.selectSysParam("000002", ttTrdTrade.getTrdtype()).getP_prop1());
		}
		//更新业务审批单状态
		if(DictConstants.TrdType.Custom.equalsIgnoreCase(ttTrdTrade.getTrdtype())){//存续期没有事前审批
			HashMap<String, Object> orderMap = new HashMap<String, Object>();
			if(ttTrdTrade.getOrder_id() != null){
				orderMap.put("order_id", (ttTrdTrade.getOrder_id() == null || "".equals(ttTrdTrade.getOrder_id()))
						? ttTrdTrade.getTrade_id() : ttTrdTrade.getOrder_id());
				orderMap.put("is_locked", true);
				TtTrdOrder ttTrdOrder = trdOrderMapper.selectOrderForOrderId(orderMap);
				//核实单退回发起、驳回到新建，审批单应该是审批通过
				if(DictConstants.ApproveStatus.New.equals(status)){
					ttTrdOrder.setOrder_status(DictConstants.ApproveStatus.ApprovedPass);
				}else{
					ttTrdOrder.setOrder_status(status);
				}
				trdOrderMapper.updateApprove(ttTrdOrder);
			}
		}
		ttTrdTrade.setTrade_status(status);
		tradeManageMapper.updateTrade(ttTrdTrade);
	}

	/**
	 * 审批拒绝
	 * @param order_id
	 */
	public void noPassApprove(TtTrdTrade ttTrdTrade,String folder) {
		JY.require(!StringUtil.checkEmptyNull(folder), "缺少必须的参数folder.");
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.noPassApprove(ttTrdTrade);
		
	}

	/**
	 * 不区分业务查询交易对手分页
	 * @param map
	 * @return TtTrdTrade
	 */
	@Override
	public Page<TtTrdTrade> searchTradeListWithoutFolder(HashMap<String,Object> map){
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		String tradeStatus = ParameterUtil.getString(map, "tradeStatus", "");
		if(StringUtil.isNotEmpty(tradeStatus)){
			String[] statusArr =tradeStatus.split(",");
			map.put("tradeStatusList", statusArr);
		}
		return tradeManageMapper.selectTtTrdTrade(map, rowBounds);
	}
	
	/**
	 * 不区分业务查询交易对手分页
	 * @param map
	 * @return TtTrdTrade
	 */
	@Override
	public Page<TrdTradeVo> getTradeVoPage(HashMap<String,Object> map){
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		String productType = ParameterUtil.getString(map, "custom_product_search", "");
		if(StringUtil.isNotEmpty(productType)){
			String[] PrdTypeArr =productType.split(",");
			map.put("PrdTypeList", Arrays.asList(PrdTypeArr));
		}
		return tradeManageMapper.getTradeVoPage(map, rowBounds);
	}
	
	@Override
	public TtTrdTrade searchTradeInfo(HashMap<String,Object> map){
		return tradeManageMapper.selectTradeForTradeId(map);
	}

	/**
	 * 查询trade结果集
	 */
	@Override
	public List<TtTrdTrade> selectTtTrdTrade(HashMap<String, Object> map) {
		return tradeManageMapper.selectTtTrdTrade(map);
	}

	@Override
	public boolean needTrade(TtTrdOrder order,String folder) {
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Approve);
		ApproveServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		return tradeServiceListener.need2Trade(order);
		
	}

	/**
	 * 根据trade_id或order_id查询激活状态交易单
	 * @return
	 */
	@Override
	public TtTrdTrade selectOneTradeIdorOrderId(Map<String, Object> map) {
		return tradeManageMapper.selectOneTradeIdorOrderId(map);
	}

	@Override
	public void notNeedTradeProcess(TtTrdOrder order, String folder) {
		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Approve);
		ApproveServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
		tradeServiceListener.notNeedTradeProcess(order);
		
	}

	/**
	 * 删除交易单
	 */
	@Override
	public int delTrade(Map<String, Object> map) {
		return tradeManageMapper.delTrade(map);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		/*
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("trade_id", serial_no);
		TtTrdTrade ttTrdTrade = new TtTrdTrade();
		try {
			ttTrdTrade = tradeManageMapper.selectTradeForTradeId(tradeMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ttTrdTrade;
		*/
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("dealNo", serial_no);
		TdProductApproveMain  main  = productApproveService.getProductApproveByCondition(map);
		return main;
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id,
			String task_def_key) throws Exception {
		//查询当前节点下配置的事件 #{flow_id} AND TASK_DEF_KEY = #{task_def_key}
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("flow_id", flow_id);
		map.put("task_def_key", task_def_key);
		map.put("dealNo", serial_no);
		map.put("trade_id", serial_no);
		HashMap<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("trade_id", serial_no);
		TtTrdTrade ttTrdTrade  = this.tradeManageMapper.selectTradeForTradeId(paramMap);
		boolean flag = false;
		String beanName = DurationConstants.getBeanFromTrdType(ttTrdTrade.getTrdtype());
		//有些产品不在main表，也可能没有配置关系
		if(null != StringUtil.trimToNull(beanName)){
			AbstractDurationService abstractDurationService = SpringContextHolder.getBean(beanName);
			TdProductApproveMain  main  = abstractDurationService.getProductApproveMain(map);
			map.put("prd_code", main==null?99999999:main.getPrdNo());
			List<String> eventList = procTaskEventMapper.getProcTaskEvents(map);
			if(null != eventList){
				for (int i = 0; i < eventList.size(); i++) {
					//quotaAutoOccupy 	额度实际占用（自动）
					//quotaManualOccupy 	额度实际占用（人工额度岗）
					//quotaAutoRelease	额度释放（自动）
					//quotaManualRelease	额度释放（人工额度岗）
					//进行额度实际占用操作
					if("quotaAutoOccupy".equals(eventList.get(i)) || "quotaManualOccupy".equals(eventList.get(i))){
						//金融机构理财产品 保本才需要占用额度且非quotaManualOccupy
						//产品是290金融机构理财 且 非保本/保本浮动 时 遇到quotaAutoOccupy事件 不进行额度占用
						if(main.getPrdNo()==290 && "quotaAutoOccupy".equals(eventList.get(i)) && !"0".equals(main.getLcType()))
						{
							continue;
						}
						if(main.getPrdNo()!=298){//企业信用类金融资产受益权
							//查询所有需要进行额度占用的记录
							List<EdInParams> creditList = productCustCreditService.getEdInParamsForProductCustCredit(map);
							if(creditList==null || creditList.size()<=0){
								if(main.getPrdNo()==294){
									flag = false;
								}else
								{
									flag = true;
								}
							}else{
								//代入额度操作服务
								/**
								 * 暴力输出：1、只要是涉及到的客户，全部回归初始化  2、按照VDATE的顺序进行额度的占用
								 * 3、保留最后操作的日志
								 * 这样处理的好处：
								 */
								List<String> dealNos =  new ArrayList<String>();
								if(null != main.getRefNo() && !"".equalsIgnoreCase(main.getRefNo())) {
                                    dealNos.add(main.getRefNo());
                                }
								dealNos.add(main.getDealNo());
								if(main.getPrdNo()==299)//融贷通项下流动资金接力贷款
                                {
                                    for(int j = 0 ;j<creditList.size();j++){//
                                        creditList.get(j).setDealType(DictConstants.DealType.Approve);
                                    }
                                }
								try{
									Pair<Boolean, List<TdEdDealLog>> pair = edCustManangeService.eduOccpFlowService(creditList,dealNos);
									if(pair.getLeft())//占用成功后再判别是否有3年期以上的额度
									{
										flag = true;
										//判定如果是三年期审查，那么说明已经占用过额度了，不需要再进行占用额度了
										//获取流程实例Id
										Map<String, String> process = flowQueryService.getFlowIdAndInstanceIdBySerialNo(serial_no);
										String processInstanceId = ParameterUtil.getString(process, "instance_id", "");
										Object term3YdayFlagObject = runtimeService.getVariable(processInstanceId, "term3YFlag");
										if(null != term3YdayFlagObject && Boolean.parseBoolean(term3YdayFlagObject.toString())) {
                                            continue;
                                        }
										//add by wangchen   2017/8/18 16:20  start
										TdProductApproveMain tdProductApproveMain = productApproveService.getProductApproveActivated(serial_no);
										String vDate = tdProductApproveMain.getvDate();//起息日
										String vDate3Y = DateUtil.dateAdjust(vDate, 3, Calendar.YEAR);//3年后的起息日
										boolean term3YFlag = false;
										//循环检测是否存在大于3年期限的额度
										for(TdEdDealLog item : pair.getRight()){
											String creditDay = "";
											//赋值 对年对月对日的额度确切期限截止日 edMdate
											if(DictConstants.TermType.Day.equalsIgnoreCase(item.getTermType()))//天
                                            {
                                                creditDay = PlaningTools.addOrSubMonth_Day(vDate, Frequency.DAY, Integer.parseInt(item.getTermId()));
                                            } else if(DictConstants.TermType.Year.equalsIgnoreCase(item.getTermType()))//天
                                            {
                                                creditDay = PlaningTools.addOrSubMonth_Day(vDate, Frequency.YEAR, Integer.parseInt(item.getTermId()));
                                            } else if(DictConstants.TermType.Month.equalsIgnoreCase(item.getTermType()))//天
                                            {
                                                creditDay = PlaningTools.addOrSubMonth_Day(vDate, Frequency.MONTH, Integer.parseInt(item.getTermId()));
                                            } else if(DictConstants.TermType.Quarter.equalsIgnoreCase(item.getTermType()))//天
                                            {
                                                creditDay = PlaningTools.addOrSubMonth_Day(vDate, Frequency.QUARTER, Integer.parseInt(item.getTermId()));
                                            } else {
												throw new RException(EdExceptionCode.NotAccessTermTypeError);
											}
											if(DateUtil.daysBetween(vDate3Y, creditDay) > 1){//超过三年
												term3YFlag = true;
												break;
											}
										}
										//将是否大于3年期限记入流程变量备用
										runtimeService.setVariable(processInstanceId, "term3YFlag", term3YFlag);
									}else{
										flag = false;//进入额度岗位
										 if("quotaManualOccupy".equals(eventList.get(i)))
										 {
											 throw new RException("额度占用失败，请调整！");
										 }
									}
								}catch (Exception e) {
									// TODO: handle exception
									//捕获异常 进入额度岗位
									 flag = false;//进入额度岗位
									// e.printStackTrace();
									 if("quotaManualOccupy".equals(eventList.get(i)))
									 {
										 throw new RException("额度占用失败，请调整！");
									 }
								}
							}
						}
						//end
					}
					//交易活期转定期交易,同业存放(活期)交易绑定此事件
					if("depositCurrent".equals(eventList.get(i)) ){
						Map<String, Object> xmap = new HashMap<String, Object>();
						xmap.put("scope", main.getTerm());
						TiInterfaceDependScope scope = tiInterfaceDependScopeMapper.queryDependScopeByScopes(xmap);
						TDpAcctNewtdaAaaRq request = new TDpAcctNewtdaAaaRq();
						request.setDebitMedType("A");
						request.setAcctNoDr(main.getAcctNo());//23001688294-CN0010303
						request.setCurrencyDr(main.getCcy());
						request.setAmt(format.format(main.getAmt()).toString());
						request.setTerm(scope.getTenor()); //3M 6M 12M 24M 36M 60M
						request.setCateGory("4003");
						request.setFbNo(InterfaceCode.TI_FBID);
						request.setTxnType(InterfaceCode.TI_IFBM0006);
						request.setFeeBaseLogId(main.getDealNo());
						
						CommonRqHdr hdr = new CommonRqHdr();
						hdr.setRqUID(UUID.randomUUID().toString());
						hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
						hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
						hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
						request.setCommonRqHdr(hdr);
						try {
							TDpAcctNewtdaAaaRs response = socketClientService.t24TDpAcctNewtdaAaa(request);
							if(!response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
								throw new Exception("活转定交易错误："+response.getCommonRsHdr().getServerStatusCode());
							}
							
							//利率变更 
							TDpAcAciModRq request2 = new TDpAcAciModRq();
							/**
							 * "1-新增  2-修改  3-删除"
							 * */
							request2.setPlanMode("2");
							request2.setApproveType(InterfaceCode.TI_FBID);
							request2.setFtTxnType(InterfaceCode.TI_IFBM0007);
							request2.setSerialNum(response.getAcctNo()+"-"+new SimpleDateFormat("HHmmss").format(new Date()));
							request2.setTreasuryRate(String.valueOf(main.getContractRate()));
							
							CommonRqHdr hdr2 = new CommonRqHdr();
							hdr2.setRqUID(UUID.randomUUID().toString());
							hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
							hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
							hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
							hdr2.setCompanyCode(response.getCompanyCode());
							request2.setCommonRqHdr(hdr2);
							
							TDpAcAciModRs response2 = socketClientService.t24TDpAcActModRequest(request2);
							if(!response2.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
								throw new Exception("利率变更错误："+response2.getCommonRsHdr().getServerStatusCode());
							}
							
						} catch (Exception e) {
							if ("depositCurrent".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
						
					}
					if("tdpAcAciModAdd".equals(eventList.get(i)) ){
						try {
							//利率新增 
							TDpAcAciModRq request2 = new TDpAcAciModRq();
								/** 
								 * "1-新增  2-修改  3-删除"
								 * */
								request2.setPlanMode("1");
								request2.setApproveType(InterfaceCode.TI_FBID);
								request2.setFtTxnType(InterfaceCode.TI_IFBM0007);
								request2.setSerialNum(main.getAcctNo()+"-"+new SimpleDateFormat("HHmmss").format(new Date()));
								request2.setTreasuryRate(String.valueOf(main.getContractRate()));
								
								CommonRqHdr hdr2 = new CommonRqHdr();
								hdr2.setRqUID(UUID.randomUUID().toString());
								hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
								hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								hdr2.setCompanyCode(main.getSponInst());
								request2.setCommonRqHdr(hdr2);
								
								TDpAcAciModRs response2 = socketClientService.t24TDpAcActModRequest(request2);
								if(!response2.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
									throw new Exception("利率变更错误："+response2.getCommonRsHdr().getServerStatusCode());
								}
						} catch (Exception e) {
							if ("tdpAcAciModAdd".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
					}
					if("tdpAcAciModUpdate".equals(eventList.get(i)) ){
	
						try {
							//利率修改
							TDpAcAciModRq request2 = new TDpAcAciModRq();
								/** 
								 * "1-新增  2-修改  3-删除"
								 * */
								request2.setPlanMode("2");
								request2.setApproveType(InterfaceCode.TI_FBID);
								request2.setFtTxnType(InterfaceCode.TI_IFBM0007);
								request2.setSerialNum(main.getAcctNo()+"-"+new SimpleDateFormat("HHmmss").format(new Date()));
								request2.setTreasuryRate(String.valueOf(main.getContractRate()));
								
								CommonRqHdr hdr2 = new CommonRqHdr();
								hdr2.setRqUID(UUID.randomUUID().toString());
								hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
								hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								hdr2.setCompanyCode(main.getSponInst());
								request2.setCommonRqHdr(hdr2);
								
								TDpAcAciModRs response2 = socketClientService.t24TDpAcActModRequest(request2);
								if(!response2.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
									throw new Exception("利率变更错误："+response2.getCommonRsHdr().getServerStatusCode());
								}
						} catch (Exception e) {
							if ("tdpAcAciModUpdate".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
					
					}
					if("tdpAcAciModDelete".equals(eventList.get(i)) ){
	
						try {
							//利率删除 
							TDpAcAciModRq request2 = new TDpAcAciModRq();
								/** 
								 * "1-新增  2-修改  3-删除"
								 * */
								request2.setPlanMode("3");
								request2.setApproveType(InterfaceCode.TI_FBID);
								request2.setFtTxnType(InterfaceCode.TI_IFBM0007);
								request2.setSerialNum(main.getAcctNo()+"-"+new SimpleDateFormat("HHmmss").format(new Date()));
								request2.setTreasuryRate(String.valueOf(main.getContractRate()));
								
								CommonRqHdr hdr2 = new CommonRqHdr();
								hdr2.setRqUID(UUID.randomUUID().toString());
								hdr2.setChannelId(InterfaceCode.TI_CHANNELNO);
								hdr2.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								hdr2.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								hdr2.setCompanyCode(main.getSponInst());
								request2.setCommonRqHdr(hdr2);
								
								TDpAcAciModRs response2 = socketClientService.t24TDpAcActModRequest(request2);
								if(!response2.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
									throw new Exception("利率变更错误："+response2.getCommonRsHdr().getServerStatusCode());
								}
						} catch (Exception e) {
							if ("tdpAcAciModDelete".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
					
					}
					if("crmsEnterpriseApprove".equals(eventList.get(i)) ){//企信业务放款
						Map<String, Object> mapt = new HashMap<String, Object>();
						mapt.put("dealno", main.getDealNo());
						FinEnterpriseTrade trade = productApproveService.selectFinEnterpriseApproveTrade(mapt);
						EnterpriseApproveRq request = new EnterpriseApproveRq();
	//					request.setPartyName(trade.getPartyname());
						request.setContractNumber(trade.getContractnum());
						request.setCustBrNo(trade.getCustbrno());
						request.setIndusType(trade.getIndustype());
						request.setRegion(trade.getRegion());
						request.setCurrency(trade.getCurrency());
						request.setContractAmt(trade.getContractamt());
						request.setRateType(trade.getRateType());
						request.setBaseRateKey(trade.getBaseratekey());
						request.setCInteType(trade.getCintetpe());
						request.setUnitCost(trade.getUnitcost());
						request.setPenSpread(trade.getPenspread());
						request.setDueDate(trade.getDuedate());
						request.setBasis(trade.getBasis());
						request.setSyInteRefre(trade.getSyinterefre());
						request.setInterestFrequency(trade.getInterestfrequency());
						request.setIntPymtMethod(trade.getIntpymtmethod());
						request.setSeType(trade.getSetype());
						request.setEndStatus(trade.getEndstatus());
						
						try {
							EnterpriseApproveRs response = socketClientService.CrmsEnterpriseApproveRequest(request);
							//判断交易发送是否成功
							if(!response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
								throw new Exception("发送企信业务审批交易失败！错误："+response.getCommonRsHdr().getServerStatusCode());
							}else {
								//不占用额度  企业信用
							}
							flag = true;
						} catch (Exception e) {
							if ("crmsEnterpriseApprove".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
					}
					if("crmsFinCreditApprove".equals(eventList.get(i))){//融贷通业务放款
						Map<String, Object> mapt = new HashMap<String, Object>();
						mapt.put("dealno", main.getDealNo());
						FinEnterpriseTrade trade = productApproveService.selectFinEnterpriseApproveTrade(mapt);
						FinCreditApproveRq request = new FinCreditApproveRq();
	//					request.setPartyName(trade.getPartyname());
						request.setSubContractNum(trade.getContractnum());
						request.setCustBrNo(trade.getCustbrno());
						request.setIndusType(trade.getIndustype());
						request.setRegion(trade.getRegion());
						request.setCurrency(trade.getCurrency());
						request.setContractAmt(trade.getContractamt());
						request.setRateType(trade.getRateType());
						//非固定利率 需要传输 基准代码及利率点差  针对406  QC改的
						if(!DictConstants.rateType.fixedRate.equalsIgnoreCase(trade.getRateType())){
						request.setBaseRateKey(trade.getBaseratekey());
						request.setCInteType(trade.getCintetpe());
						}
						request.setForcatsProfitRate(trade.getForcatsprofitrate());
						request.setPenSpread(trade.getPenspread());
						request.setIncomeDate(trade.getIncomedate());
						request.setTransferDate(trade.getTransferdate());
						request.setDueDate(trade.getDuedate());
						request.setRecvDate(trade.getRecvdate());
						request.setBasis(trade.getBasis());
						request.setSyInteRefre(trade.getSyinterefre());
						request.setInterestFrequency(trade.getInterestfrequency());
						request.setIntPymtMethod(trade.getIntpymtmethod());
						try {
							FinCreditApproveRs response = socketClientService.CrmsFinCreditApproveRequest(request);
							//判断交易发送是否成功
							if(!response.getCommonRsHdr().getStatusCode().equals(InterfaceCode.TI_SUCCESS)){
								throw new Exception("发送融贷通业务审批交易失败！错误："+response.getCommonRsHdr().getServerStatusCode());
							}
							flag = true;
						} catch (Exception e) {
							if ("crmsEnterpriseApprove".equals(eventList.get(i))) {
								throw new Exception(e.getMessage());									
							}
						}
						
					}
					if("CNY".equals(main.getCcy())){
						Map<String, Object> mapt = new HashMap<String, Object>();
						if("transfer".equals(eventList.get(i))&& StringUtil.isNotEmpty(main.getIsSettl()) && main.getIsSettl().endsWith("1")){//放款 -IsSettl 1清算 0不清算
							//AcctType：清算方式  行内清算 1    行外清算 2
							TtInstitutionSettls ttInstitutionSettls = new TtInstitutionSettls();
							if(main.getAcctType().endsWith("1")){
								TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
								request.setCurrencyDr(main.getCcy());
								
								List<DebitBeaRec> recList = new ArrayList<DebitBeaRec>();
								//借方-新加的
								DebitBeaRec rec1 = new DebitBeaRec();
								mapt.put("ccy", main.getCcy().trim());
								mapt.put("instId", main.getSponInst());
								List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
								if(null == acct || acct.size()<=0){
									throw new RException("找不到："+main.getSponInst()+"机构"+main.getCcy()+"的407账号！");
								}
								rec1.setAcctNoDr(acct.get(0).getAcctNo());     
								rec1.setDebitMedType(acct.get(0).getMediumType());
	//							rec1.setAcctNoDr(dictionaryGetService.getTaDictByCodeAndKey("TransferAcct", "01").getDict_value());//407								
	//							rec1.setDebitMedType(dictionaryGetService.getTaDictByCodeAndKey("TransferAcct", "02").getDict_value());
								rec1.setAmtDr(format.format(main.getProcAmt()).toString() );
								recList.add(rec1);
										
								//借方
								DebitBeaRec rec = new DebitBeaRec();
								rec.setAcctNoDr(main.getSelfAccCode());//261=98						
								//查询98账户类型
								ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
								rec.setDebitMedType(ttInstitutionSettls.getMediumType());							
								rec.setAmtDr(format.format(main.getProcAmt()).toString() );							
								recList.add(rec);							
								request.setDebitBeaRec(recList);
								request.setCurrencyCr(main.getCcy());
								
								
								List<CreditBeaRec> reccList = new ArrayList<CreditBeaRec>();
								//贷方-新加的
								CreditBeaRec recc1 = new CreditBeaRec();
								recc1.setAcctNoCr(main.getSelfAccCode());							
								//查询98账户类型
								ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
								recc1.setCreditMedType(ttInstitutionSettls.getMediumType());							
								recc1.setAmtCr(format.format(main.getProcAmt()).toString() );
								reccList.add(recc1);
								//贷方
								CreditBeaRec recc = new CreditBeaRec();
								recc.setAcctNoCr(main.getPartyAccCode());						
								//查询客户账户类型
								Map<String, Object> newmap=new HashMap<String, Object>();
								newmap.put("bankaccid", main.getPartyAccCode());
								newmap.put("custmerCode", main.getPartyEcifCode());
								newmap.put("cny", main.getCcy());
								TdCustAcc tdCustAcc = tdCustAccMapper.showTdCustAccByColumns(newmap);
								
								JY.require(tdCustAcc != null, "查无此客户划款路径，请确认");
								recc.setCreditMedType(tdCustAcc.getMediumType());													
								recc.setAmtCr(format.format(main.getProcAmt()).toString() );						
								reccList.add(recc);							
														
								request.setCreditBeaRec(reccList);
	
								
								request.setFbNo(InterfaceCode.TI_FBID);
								request.setTxnType(InterfaceCode.TI_IFBM0001);
								request.setFeeBaseLogId(main.getDealNo());
								CommonRqHdr hdr = new CommonRqHdr();
								hdr.setRqUID(UUID.randomUUID().toString());
								hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
								hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								request.setCommonRqHdr(hdr);
								try {
									TStBeaRepayAaaRs response = socketClientService.t24TStBeaRepayAaaRequest(request);
									//行内清算
									map.put("transfer", response.getCommonRsHdr().getStatusCode()+"|"+response.getCommonRsHdr().getServerStatusCode());
									
								} catch (Exception e) {
									if ("transfer".equals(eventList.get(i))) {
										throw new Exception("error行内清算："+e.getMessage());									
									}
								}
								
							}
							
							if(main.getAcctType().endsWith("2")){
								StringBuffer logbuffer = new  StringBuffer();
								logbuffer.append("No.1**************************************");
								List<DebitBeaRec> recList = new ArrayList<DebitBeaRec>();
								List<CreditBeaRec> reccList = new ArrayList<CreditBeaRec>();
								TStBeaRepayAaaRq request = new TStBeaRepayAaaRq();
								request.setCurrencyDr(main.getCcy());		
								logbuffer.append(String.format("%20s", "CurrencyDr:")).append(main.getCcy());
								DebitBeaRec rec1 = new DebitBeaRec();
								mapt.put("ccy", main.getCcy());
								mapt.put("instId", main.getSponInst());
								List<Acctno407> acct = acctno407Mapper.selectAcctno407(mapt);
								if(null == acct || acct.size()<=0){
									throw new RException("找不到："+main.getSponInst()+"机构"+main.getCcy()+"的407账号！");
								}
								rec1.setAcctNoDr(acct.get(0).getAcctNo());
								logbuffer.append(String.format("%20s", "AcctNoDr:")).append(acct.get(0).getAcctNo());
								rec1.setDebitMedType(acct.get(0).getMediumType());
								logbuffer.append(String.format("%20s", "DebitMedType:")).append(acct.get(0).getMediumType());
								rec1.setAmtDr(format.format(main.getProcAmt()).toString() );
								logbuffer.append(String.format("%20s", "AmtDr:")).append(format.format(main.getProcAmt()).toString() );
								recList.add(rec1);
								request.setDebitBeaRec(recList);
								request.setCurrencyCr(main.getCcy());
								logbuffer.append(String.format("%20s", "CurrencyCr:")).append(main.getCcy());
								CreditBeaRec recc1 = new CreditBeaRec();
								recc1.setAcctNoCr(main.getSelfAccCode());
								logbuffer.append(String.format("%20s", "AcctNoCr:")).append(main.getSelfAccCode());
								recc1.setAmtCr(format.format(main.getProcAmt()).toString());	
								logbuffer.append(String.format("%20s", "AmtCr:")).append(format.format(main.getProcAmt()).toString());
								ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNo(main.getSelfAccCode());
								recc1.setCreditMedType(ttInstitutionSettls.getMediumType());	
								logbuffer.append(String.format("%20s", "CreditMedType:")).append(ttInstitutionSettls.getMediumType());
								reccList.add(recc1);						
								request.setCreditBeaRec(reccList);
								request.setFbNo(InterfaceCode.TI_FBID);
								request.setTxnType(InterfaceCode.TI_IFBM0001);
								request.setFeeBaseLogId(main.getDealNo());
								CommonRqHdr hdr = new CommonRqHdr();
								hdr.setRqUID(UUID.randomUUID().toString());
								hdr.setChannelId(InterfaceCode.TI_CHANNELNO);
								hdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
								hdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
								request.setCommonRqHdr(hdr);
								boolean runflag = true;
								try {
									TStBeaRepayAaaRs response = socketClientService.t24TStBeaRepayAaaRequest(request);
									//行内清算
									map.put("transfer", response.getCommonRsHdr().getStatusCode()+"|"+response.getCommonRsHdr().getServerStatusCode());
									if(response.getCommonRsHdr().getStatusCode()=="000000"|| "000000".equals(response.getCommonRsHdr().getStatusCode())){
										runflag = true;
									}else {
										runflag = false;
									}
									logbuffer.append(String.format("%20s", "StatusCode:")).append(response.getCommonRsHdr().getStatusCode());
								} catch (Exception e) {
									if ("transfer".equals(eventList.get(i))) {
										throw new Exception("error行内清算："+e.getMessage());									
									}
								}finally{
									logbuffer.append("**************************************No.1");
									LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(logbuffer.toString());
								}
								if(runflag){
									logbuffer = new StringBuffer();
									logbuffer.append("No.2**************************************");
									CommonRqHdr commonRqHdr = new CommonRqHdr();
									commonRqHdr.setRqUID(UUID.randomUUID().toString());
									commonRqHdr.setChannelId(InterfaceCode.TI_CHANNELNO);
									commonRqHdr.setTranDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
									commonRqHdr.setTranTime(new SimpleDateFormat("HHmmss").format(new Date()));
									commonRqHdr.setSPName("T24");
									UPPSCdtTrfRq request1 = new UPPSCdtTrfRq();
									request1.setCommonRqHdr(commonRqHdr);
									request1.setUserDefineTranCode(InterfaceCode.TI2ND_UPPSCdtTrf);
									request1.setPayPathCode(InterfaceCode.TI2ND_PayPathCode1001);
									request1.setPosEntryMode(InterfaceCode.TI2ND_PosEntryMode0);
									request1.setBusiType(InterfaceCode.TI2ND_BusiTypeA100);
									request1.setBusiKind(InterfaceCode.TI2ND_BusiKind02102);
									Map<String, Object> mapc = new HashMap<String, Object>();
									mapc.put("instId", main.getSponInst());
									mapc.put("instAcctNo",main.getSelfAccCode());
									mapc.put("ccy", main.getCcy());
									ttInstitutionSettls = ttInstitutionSettlsMapper.getInstSettlsByInstAcctNoAndDealno(mapc);	
									if(null == ttInstitutionSettls) {
                                        throw new RException(main.getSponInst()+":"+main.getSelfAccCode()+":"+main.getCcy()+"不存在机构清算账号！");
                                    }
									request1.setPayerAccType(ttInstitutionSettls.getPayerAccType());
									logbuffer.append(String.format("%20s", "PayerAccType:")).append(ttInstitutionSettls.getPayerAccType());
									request1.setPayerAcc(main.getSelfAccCode());
									logbuffer.append(String.format("%20s", "PayerAcc:")).append(main.getSelfAccCode());
									request1.setPayerName(main.getSelfAccName());
									logbuffer.append(String.format("%20s", "PayerName:")).append(main.getSelfAccName());
									request1.setPayerAccBank("");
									request1.setRealPayerAcc("");
									request1.setRealPayerName("");
									request1.setRealPayerAccType("");
									request1.setPayeeAccBank(main.getPartyBankCode());
									logbuffer.append(String.format("%20s", "PayeeAccBank:")).append(main.getPartyBankCode());
									request1.setPayeeName(main.getPartyAccName());
									logbuffer.append(String.format("%20s", "PayeeName:")).append(main.getPartyAccName());
									request1.setPayeeAccType("1");
									request1.setPayeeAcc(main.getPartyAccCode());
									logbuffer.append(String.format("%20s", "PayeeAcc:")).append(main.getPartyAccCode());
									request1.setCurrency(main.getCcy());
									logbuffer.append(String.format("%20s", "Currency:")).append(main.getCcy());
									request1.setAmount(format.format(main.getProcAmt()).toString() );
									logbuffer.append(String.format("%20s", "Amount:")).append(format.format(main.getProcAmt()).toString());
									request1.setPostScript("");
									request1.setCommissionNo("0");
									request1.setChargeNo("0");
									request1.setRemark1("X");
									request1.setFeeBaseLogId(main.getDealNo());
									try {
										UPPSCdtTrfRs response1 = socketClientService.T2ndPaymentUPPSCdtTrfRequest(request1);
										//二代支付调用
										map.put("transfer", response1.getCommonRsHdr().getStatusCode()+"|"+response1.getCommonRsHdr().getServerStatusCode());
										if("000000".equals(response1.getCommonRsHdr().getStatusCode())){
											//查询一次，若返回处理不成功，则后台JOB根据数据库中的状态轮询
											try {
													UPPSigBusiQueryRq request2 = new UPPSigBusiQueryRq();
													request2.setUserDefineTranCode(InterfaceCode.TI2ND_QueryUserDefineTranCode);
													request2.setOrigChannelCode(InterfaceCode.TI_CHANNELNO);
													request2.setOrigChannelDate(response1.getWorkDate());
													request2.setOrigFeeBaseLogId(main.getDealNo());
													UPPSigBusiQueryRs response2 = socketClientService.T2ndPaymentUPPSigBusiQueryRequest(request2);
													List<UPPSigBusiQueryRec> queryList = response2.getUPPSigBusiQueryRec();
													
													for(int k=0;k<queryList.size();k++){
														Map<String, Object> mapp = new HashMap<String, Object>();
														mapp.put("retcode", queryList.get(k).getCenterDealCode());
														mapp.put("retmsg",queryList.get(k).getCenterDealMsg());
														mapp.put("rquid",queryList.get(k).getChannelSerNo());
														mapp.put("agentserialno",queryList.get(k).getAgentSerialNo());
														mapp.put("centerdealcode", queryList.get(k).getCenterDealCode());
														mapp.put("centerdealmsg", queryList.get(k).getCenterDealMsg());
														mapp.put("payresult", queryList.get(k).getPayResult());
														mapp.put("workdate", queryList.get(k).getWorkDate());
														mapp.put("bankdate", queryList.get(k).getBankDate());
														mapp.put("bankid", queryList.get(k).getBankId());
														mapp.put("feeBaseLogid", queryList.get(k).getFeeBaseLogId());
														String srquid = ti2ndPaymentMapper.queryTi2ndPayment2(mapp).getSrquid();
														mapp.put("srquid", srquid);
														ti2ndPaymentMapper.update2ndPaymentRetMsg(mapp);
														tiInterfacleLogMapper.updateTiInterfaceRetcode(mapp);
														
														System.out.println("自助查询结果为：" + response2.getCommonRsHdr().getStatusCode()+"|"+response2.getCommonRsHdr().getServerStatusCode());
														LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("自助查询结果为：" + response2.getCommonRsHdr().getStatusCode()+"|"+response2.getCommonRsHdr().getServerStatusCode());
														LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("支付平台处理为: "+ queryList.get(k).getDealCode()+"|"+queryList.get(k).getDealMsg());
														LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("核心处理为: "+queryList.get(k).getCoreResult()+"|"+queryList.get(k).getBankDealCode()+"|"+queryList.get(k).getBankDealMsg());
														LogManager.getLogger(LogManager.MODEL_INTERFACEX).info("人行处理为: "+queryList.get(k).getCenterDealCode()+"|"+queryList.get(k).getCenterDealMsg());
														
														if("2".equals(queryList.get(k).getPayResult())||"0".equals(queryList.get(k).getPayResult())){
															break;
														}
													}
											} catch (Exception e) {
												// TODO: handle exception
												System.out.println("123:::"+e.getMessage());
												LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(e.getMessage());
											}
											
										}
										logbuffer.append(String.format("%20s", "StatusCode:")).append(response1.getCommonRsHdr().getStatusCode());
									} catch (Exception e) {
										if ("transfer".equals(eventList.get(i))) {
											throw new Exception("error行外清算："+e.getMessage());									
										}
									}finally{
										logbuffer.append("**************************************No.2");
										LogManager.getLogger(LogManager.MODEL_INTERFACEX).info(logbuffer.toString());
									}
								}
							}
						}
					
					}
					
				}
			}
		}
		map.put("quotaAutoOccupy", flag);
		return map;
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
        
    }
	

//	@Override
//	public HashMap<String, Object> selectImportTrade(Workbook wb,String folder) {
//		// TODO Auto-generated method stub
//		HashMap<String, Object> map=new HashMap<String, Object>();
//		String beanName = SettleConstants.getBeanFromTrdType(folder,DictConstants.BillsType.Trade);
//		TradeServiceListener tradeServiceListener = SpringContextHolder.getBean(beanName);
//		tradeServiceListener.parseTradeImport(wb, map);
//		return map;
//	}

//	@Override
//	public void getPublicInfoImportTrade(TradeEditVo trdTradeVo, ExecutionReportCSTP executionReportCSTP) {
//		// TODO Auto-generated method stub
//		trdTradeVo.setParty_bankacccode(executionReportCSTP.getCpCashAcctNumber());
//		trdTradeVo.setParty_bankaccname(executionReportCSTP.getCpCashAcctName());
//		trdTradeVo.setParty_bankname(executionReportCSTP.getCpSettleBankName());
//		trdTradeVo.setParty_bankcode(executionReportCSTP.getCpSettleBankSortCode());
//		trdTradeVo.setParty_tradername(executionReportCSTP.getCpTradeUser());
//		String userId = userService.selectUserByName(executionReportCSTP.getMgTradeUser()).getUser_id();
//		JY.require(!(StringUtil.isNullOrEmpty(userId)), "成交单上的本方用户名为空或无法与数据库匹配，请确认。");
//		trdTradeVo.setSelf_traderid(userId);
//		trdTradeVo.setSelf_tradername(executionReportCSTP.getMgTradeUser());
//		trdTradeVo.setTrdtype(executionReportCSTP.getDirection());
//		trdTradeVo.setParty_type("1");
//		trdTradeVo.setTrade_date(executionReportCSTP.getDealDate());
//		trdTradeVo.setExecid(executionReportCSTP.getDealNumber());
//		Map<String,String> map=new HashMap<String,String>();
//		if("1701".equals(executionReportCSTP.getDirection()) || "1702".equals(executionReportCSTP.getDirection())){
//			//通过资金账号确定对手方   暂支持拆借
//			map.put("bankaccid", executionReportCSTP.getCpCashAcctNumber());
//			CounterPartyVo tcp=counterPartyService.selectCPByMap(map);
//			trdTradeVo.setOutsider_party_id_name(tcp.getParty_shortname());
//			trdTradeVo.setOutsider_user_code_name(tcp.getParty_code());
//			trdTradeVo.setParty_id(tcp.getParty_id());
//		}else{
//			//通过中债登托管账户确定对手方  适用于大多数业务
//			map.put("zzd_acccode", executionReportCSTP.getCpSecuritiesAccNumber());
//			CounterPartyVo tcp=counterPartyService.selectCPByMap(map);
//			trdTradeVo.setOutsider_party_id_name(tcp.getParty_shortname());
//			trdTradeVo.setOutsider_user_code_name(tcp.getParty_code());
//			trdTradeVo.setParty_id(tcp.getParty_id());
//			trdTradeVo.setParty_zzdaccid(tcp.getZzd_acccode());
//		}
//		//trdTradeVo.setParty_largepaymentcode(tcp.get);
//	}
}
