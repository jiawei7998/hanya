package com.singlee.capital.trade.acc.model;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TD_PRODUCT_APPROVE_OVERDRAFT")
public class TdProductApproveOverDraft implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;//交易流水
	@Id
	private int version;//版本号
	
	private String acctNo;//透支账户名称
	private String acctNm;//透支账户号
	private String acctBkno;//透支账户开户行
	private String ccy;//币种
	private String vDate;//透支额度起始日
	private String mDate;//透支额度到期日
	private double creditAmt;//透支额度
	private double enableAmt;//启用额度
	private double manageFeerate;//额度管理费_%
	private int granceDays;//宽限期
	private String lateExpiryDate;//最晚结息日
	private String continuePeried;//持续透支期
	private double odRate;//透支利率_%
	private double chargeRate;//罚息利率_%
	private String platEarmark;//平台投向
	private String indusEarmark;//行业投向
	private String investFinal;//最终投向
	private String tzDate;//申请日
	private double remainAmt;//剩余额度
	private String oMdate;//原到期日
	private String tzInvtype;//资产会计四分类
	private String tzRemark;//备注
	private double nyRate;//挪用罚息_%
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getAcctNm() {
		return acctNm;
	}
	public void setAcctNm(String acctNm) {
		this.acctNm = acctNm;
	}
	public String getAcctBkno() {
		return acctBkno;
	}
	public void setAcctBkno(String acctBkno) {
		this.acctBkno = acctBkno;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public double getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(double creditAmt) {
		this.creditAmt = creditAmt;
	}
	public double getEnableAmt() {
		return enableAmt;
	}
	public void setEnableAmt(double enableAmt) {
		this.enableAmt = enableAmt;
	}
	public double getManageFeerate() {
		return manageFeerate;
	}
	public void setManageFeerate(double manageFeerate) {
		this.manageFeerate = manageFeerate;
	}
	public int getGranceDays() {
		return granceDays;
	}
	public void setGranceDays(int granceDays) {
		this.granceDays = granceDays;
	}
	public String getLateExpiryDate() {
		return lateExpiryDate;
	}
	public void setLateExpiryDate(String lateExpiryDate) {
		this.lateExpiryDate = lateExpiryDate;
	}
	public String getContinuePeried() {
		return continuePeried;
	}
	public void setContinuePeried(String continuePeried) {
		this.continuePeried = continuePeried;
	}
	public double getOdRate() {
		return odRate;
	}
	public void setOdRate(double odRate) {
		this.odRate = odRate;
	}
	public double getChargeRate() {
		return chargeRate;
	}
	public void setChargeRate(double chargeRate) {
		this.chargeRate = chargeRate;
	}
	public String getPlatEarmark() {
		return platEarmark;
	}
	public void setPlatEarmark(String platEarmark) {
		this.platEarmark = platEarmark;
	}
	public String getIndusEarmark() {
		return indusEarmark;
	}
	public void setIndusEarmark(String indusEarmark) {
		this.indusEarmark = indusEarmark;
	}
	public String getInvestFinal() {
		return investFinal;
	}
	public void setInvestFinal(String investFinal) {
		this.investFinal = investFinal;
	}
	public String getTzDate() {
		return tzDate;
	}
	public void setTzDate(String tzDate) {
		this.tzDate = tzDate;
	}
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	public String getoMdate() {
		return oMdate;
	}
	public void setoMdate(String oMdate) {
		this.oMdate = oMdate;
	}
	public String getTzInvtype() {
		return tzInvtype;
	}
	public void setTzInvtype(String tzInvtype) {
		this.tzInvtype = tzInvtype;
	}
	public String getTzRemark() {
		return tzRemark;
	}
	public void setTzRemark(String tzRemark) {
		this.tzRemark = tzRemark;
	}
	public double getNyRate() {
		return nyRate;
	}
	public void setNyRate(double nyRate) {
		this.nyRate = nyRate;
	}
	@Override
	public String toString() {
		return "TdProductApproveOverDraft [dealNo=" + dealNo + ", version="
				+ version + ", acctNo=" + acctNo + ", acctNm=" + acctNm
				+ ", acctBkno=" + acctBkno + ", ccy=" + ccy + ", vDate="
				+ vDate + ", mDate=" + mDate + ", creditAmt=" + creditAmt
				+ ", enableAmt=" + enableAmt + ", manageFeerate="
				+ manageFeerate + ", granceDays=" + granceDays
				+ ", lateExpiryDate=" + lateExpiryDate + ", continuePeried="
				+ continuePeried + ", odRate=" + odRate + ", chargeRate="
				+ chargeRate + ", platEarmark=" + platEarmark
				+ ", indusEarmark=" + indusEarmark + ", investFinal="
				+ investFinal + ", tzDate=" + tzDate + ", remainAmt="
				+ remainAmt + ", oMdate=" + oMdate + ", tzInvtype=" + tzInvtype
				+ ", tzRemark=" + tzRemark + ", nyRate=" + nyRate + "]";
	}
	

}
