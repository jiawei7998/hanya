package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;

public interface TdFeesPassAgewayDailyMapper extends Mapper<TdFeesPassAgewayDaily>{
	
	/**
	 * 获取通道每日计提信息
	 * @param map
	 * @return
	 */
	List<TdFeesPassAgewayDaily> getFeesDailyInterestList(Map<String, Object> map);
	
	public void deleteByDealNoAndCust(Map<String, Object> params);
	
	public void deleteByMap(Map<String, Object> params);
	
	/**
	 * 获取通道每日计提信息
	 * @param map
	 * @return
	 */
	public List<TdFeesPassAgewayDaily> getFeesDailySumInterestList(Map<String, Object> map);
	
	public double getSumInterest(Map<String, Object> map);
}
 