package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesPlan;

/**
 * @projectName 同业业务管理系统
 * @className 中间费用服务接口
 * @description TODO
 * @author dzy
 * @createDate 2016-10-14 下午3:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface FeesPlanService {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 中间费用对象列表
	 * @author dzy
	 * @date 2016-10-14
	 */
	public Page<TdFeesPlan> getFeesPlanPage (Map<String, Object> params);
	
	/**
	 * 新增
	 * 
	 * @param params - 请求参数
	 * @return 中间费用对象
	 * @author dzy
	 * @date 2016-10-14
	 */
	public void createFeesPlan(Map<String, Object> params);
	
	
	/**
	 * 修改
	 * 
	 * @param params - 请求参数
	 * @author dzy
	 * @date 2016-10-14
	 */
	public void updateFeesPlan(Map<String, Object> params);
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	public List<TdFeesPlan> searchFeesPlan(Map<String, Object> params);
	
	/**
	 * 删除
	 * @param dealNos
	 */
	public void deleteFeesPlan(String[] dealNos);
	
	/**
	 * 保存
	 * @param map
	 */
	public void saveFeesPlan(Map<String, Object> map);
	
	/**
	 * 新建放款审批中收信息
	 * @param map
	 */
	public void createTradeFeePlan(Map<String,Object> map);
	
}
