package com.singlee.capital.trade.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.trade.model.TdProductFeeCal;

public interface TdProductFeeCalService {
	/**
	 * 通过前台的交易 map   获取FEE配置，自动计算
	 * @param paramMap
	 * @return
	 */
	public List<?> getCalFeeForProductDeal(Map<String, Object> paramMap);
	/**
	 * 获取费用配置列表，设置界面那些费用栏位不可用
	 * @param paramMap
	 * @return
	 */
	public List<TdProductFeeCal> getProductFeeForPrdNo(Map<String, Object> paramMap);
	
}
