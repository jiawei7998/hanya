package com.singlee.capital.trade.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.approve.model.TtTrdOrder;
import com.singlee.capital.approve.service.ApproveManageService;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.AbstractDurationService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.capital.trade.mapper.TdRateResetMapper;
import com.singlee.capital.trade.mapper.TdTrdEventMapper;
import com.singlee.capital.trade.mapper.TrdTradeMapper;
import com.singlee.capital.trade.model.TdProductApproveMain;
import com.singlee.capital.trade.model.TdRateReset;
import com.singlee.capital.trade.model.TdTrdEvent;
import com.singlee.capital.trade.model.TtTrdTrade;
import com.singlee.capital.trade.service.ProductApproveService;
import com.singlee.capital.trade.service.RateResetService;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;

@Service
public class RateResetServiceImpl extends AbstractDurationService implements RateResetService,SlbpmCallBackInteface {

	@Autowired
	private TdRateResetMapper tdRateResetMapper;
	/** 交易单dao **/
	@Autowired
	private TrdTradeMapper tradeManageMapper;
	
	@Autowired
	private ProductApproveService productApproveService;
	
	@Autowired
	private ApproveManageService approveManageService;
	
	@Autowired
	private TdTrdEventMapper tdTrdEventMapper;
	
	@Autowired
	private DayendDateService dayendDateService;
	
	@Override
	public TdRateReset getRateChangeById(String dealNo) {
		return tdRateResetMapper.getRateChangeById(dealNo);
	}

	@Override
	public Page<TdRateReset> getRateChangePage(Map<String, Object> params, int isFinish) {
		Page<TdRateReset> page= new Page<TdRateReset>();
		if(isFinish == 1){
			page= tdRateResetMapper.getRateChangeApproveList(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 2){
			page= tdRateResetMapper.getRateChangeListFinish(params, ParameterUtil.getRowBounds(params));
		} else if(isFinish == 3){
			page= tdRateResetMapper.getRateChangeMine(params, ParameterUtil.getRowBounds(params));
		}
		List<TdRateReset> list =page.getResult();
		for(TdRateReset rc :list){
			TdProductApproveMain main = productApproveService.getProductApproveActivated(rc.getRefNo());
			if(main!=null){
				rc.setProduct(main.getProduct());
				rc.setParty(main.getCounterParty());
				rc.setPrdName(main.getConTitle());
			}
		}
		return page;
	}

	/**
	 * 创建利率变更数据（order or trade+ratechang）
	 */
	@Override
	@AutoLogMethod(value = "创建利率变更数据")
	public void createRateChange(Map<String, Object> params) {
		tdRateResetMapper.insert(rateChange(params));
	}

	/**
	 * 修改利率变更数据（order or trade+ratechange）
	 */
	@Override
	@AutoLogMethod(value = "修改利率变更数据")
	public void updateRateChange(Map<String, Object> params) {
		tdRateResetMapper.updateByPrimaryKey(rateChange(params));
	}
	
	/**
	 * map转换成需要的rateChange对象
	 * @param map
	 * @return
	 */
	public TdRateReset rateChange(Map<String,Object> map){
		TdRateReset rateChange = new TdRateReset();
		try {
			BeanUtil.populate(rateChange, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
     // 初始化表数据(1.如果是新增审批单应该从原交易表中获取数据；2.如果是审批完成生成核实单，就是审批单老数据
        TdRateReset change = new TdRateReset();
        change.setDealNo(String.valueOf(map.get("dealNo")));
        change.setDealType(rateChange.getDealType());
        change = tdRateResetMapper.searchRateChange(change);
        
      //审批完成，为交易单添加一条数据
        if(change ==null && DictConstants.DealType.Verify.equals(rateChange.getDealType())) {
        	rateChange.setRefNo(rateChange.getRefNo());
        	rateChange.setVersion(rateChange.getVersion());
        	rateChange.setRate(rateChange.getRate());
        	rateChange.setChangeRate(rateChange.getChangeRate());
        	rateChange.setChangeDate(rateChange.getChangeDate());
        	rateChange.setChangeReason(rateChange.getChangeReason());
        	rateChange.setEffectDate(rateChange.getEffectDate());
            rateChange.setSponsor(rateChange.getSponsor());
            rateChange.setSponInst(rateChange.getSponInst());
            rateChange.setRateDiff(Double.valueOf(rateChange.getRateDiff()));
            rateChange.setRateCode(rateChange.getRateCode());
        }else{
            rateChange.setSponsor(SlSessionHelper.getUserId());
            rateChange.setSponInst(SlSessionHelper.getInstitutionId());
            rateChange.setRateDiff(Double.valueOf(rateChange.getRateDiff()));
            rateChange.setRateCode(rateChange.getRateCode());
        }
        
        rateChange.setSponsor(SlSessionHelper.getUser().getUserId());
        rateChange.setSponInst(SlSessionHelper.getInstitution().getInstId());
        rateChange.setChangeDate(dayendDateService.getSettlementDate());
        rateChange.setLastUpdate(DateUtil.getCurrentDateTimeAsString());
		
		return rateChange;
	}
	

	/**
	 * 删除利率变更数据（order or trade+ratechange）
	 */
	@Override
	public void deleteRateChange(String[] dealNos) {
		TtTrdOrder order = new TtTrdOrder();
		for(int i=0;i<dealNos.length;i++){
			tdRateResetMapper.deleteByPrimaryKey(dealNos[i]);
			order.setOrder_id(dealNos[i]);
			approveManageService.delTrdOrder(order);
		}

	}

	@Override
	public TdProductApproveMain getProductApproveMain(Map<String, Object> params) {
		// TODO Auto-generated method stub
		TdRateReset rateChange = new TdRateReset();
		BeanUtil.populate(rateChange, params);
		rateChange = tdRateResetMapper.searchRateChange(rateChange);
		return productApproveService.getProductApproveActivated(rateChange.getRefNo());
	}

	@Override
	public void saveTrdEvent(Map<String, Object> params) {
		TdRateReset rateChange = new TdRateReset();
		rateChange.setDealNo(ParameterUtil.getString(params, "trade_id", ""));
		rateChange = tdRateResetMapper.searchRateChange(rateChange);
		
		TdTrdEvent tdTrdEvent =  new TdTrdEvent();//定时需要存储的事件结构
		tdTrdEvent.setDealNo(rateChange.getRefNo());//原交易流水号
		tdTrdEvent.setVersion(rateChange.getVersion());//原交易版本
		tdTrdEvent.setEventRefno(rateChange.getDealNo());//将利率变更的交易流水进行匹配赋值
		tdTrdEvent.setEventType(DictConstants.TrdType.CustomRateChange);//利率变更
		tdTrdEvent.setEventTable("TD_RATE_CHANGE");
		tdTrdEvent.setOpTime(DateUtil.getCurrentDateTimeAsString());
		tdTrdEventMapper.insert(tdTrdEvent);
	}

	@Override
	public void saveCompleteDuration(Map<String, Object> params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getBizObj(String flowType, String flowId, String serialNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void statusChange(String flowType, String flowId, String serial_no,
			String status, String flowCompleteType) {
		HashMap<String, Object> tradeMap = new HashMap<String, Object>();
		tradeMap.put("trade_id", serial_no);
		tradeMap.put("is_locked", true);
		TtTrdTrade ttTrdTrade = tradeManageMapper.selectTradeForTradeId(tradeMap);
		ttTrdTrade.setTrade_status(status);
		tradeManageMapper.updateTrade(ttTrdTrade);
	}

}
