package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 产品子表
 * @description TODO
 * @author 倪航
 * @createDate 2016-5-12 下午4:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_PRODUCT_APPROVE_SUB")
public class TdProductApproveSub implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7251936230237897783L;
	/**
	 * 通用表单编号
	 */
	@Id
	private String genMainNo;
	/**
	 * 表单字段名称
	 */
	@Id
	private String subFldName;
	/**
	 * 表单字段值
	 */
	private String subFldValue;
	public String getGenMainNo() {
		return genMainNo;
	}
	public void setGenMainNo(String genMainNo) {
		this.genMainNo = genMainNo;
	}
	public String getSubFldName() {
		return subFldName;
	}
	public void setSubFldName(String subFldName) {
		this.subFldName = subFldName;
	}
	public String getSubFldValue() {
		return subFldValue;
	}
	public void setSubFldValue(String subFldValue) {
		this.subFldValue = subFldValue;
	}
}
