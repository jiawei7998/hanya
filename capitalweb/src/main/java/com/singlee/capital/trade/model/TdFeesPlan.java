package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @projectName 同业业务管理系统
 * @className 提前到期
 * @description TODO
 * @author dzy
 * @createDate 2016-10-13 上午12:00:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_FEES_PLAN")
public class TdFeesPlan implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_PLAN.NEXTVAL FROM DUAL")
	private String dealNo;			//流水号 序列自动搞定
	private String refNo;			//原交易流水
	private String feeCcy;			//币种
	private String feeBasis;		//计息基础
	private String feeManner;		//收费方式
	private String feeType;			//费用类型
	private Integer feePeriod;		//期数
	private String feeSituation;	//收付情况 0.收费 1.付费
	private String feeFrequency;	//收付频率
	private double feeRate;			//收付利率
	private String feeStartDate;	//起始日
	private String feeEndDate;		//结束日
	private String feeDate;			//费用结息日
	private double predictAmt;		//预计金额
	private String remark;			//备注
	
	public TdFeesPlan(String refNo,	String feeCcy,String feeBasis,String feeManner,	String feeType,			
	                     Integer feePeriod,	String feeSituation,String feeFrequency,double feeRate,	String feeStartDate,	
	                   String feeEndDate,String feeDate,double predictAmt,	String remark){		
		this.refNo = refNo;			//原交易流水
		this.feeCcy = feeCcy;			//币种
		this.feeBasis = feeBasis;		//计息基础
		this.feeManner = feeManner;		//收费方式
		this.feeType = feeType;			//费用类型
		this.feePeriod = feePeriod;		//期数
		this.feeSituation = feeSituation;	//收付情况 0.收费 1.付费
		this.feeFrequency = feeFrequency;	//收付频率
		this.feeRate = feeRate;			//收付利率
		this.feeStartDate = feeStartDate;	//起始日
		this.feeEndDate = feeEndDate;		//结束日
		this.feeDate = feeDate;			//费用结息日
		this.predictAmt =  predictAmt;		//预计金额
		this.remark = remark;			//备注
	}
	
	public TdFeesPlan(){
		
	}
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getFeeManner() {
		return feeManner;
	}
	public void setFeeManner(String feeManner) {
		this.feeManner = feeManner;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public Integer getFeePeriod() {
		return feePeriod;
	}
	public void setFeePeriod(Integer feePeriod) {
		this.feePeriod = feePeriod;
	}
	public String getFeeSituation() {
		return feeSituation;
	}
	public void setFeeSituation(String feeSituation) {
		this.feeSituation = feeSituation;
	}
	public String getFeeFrequency() {
		return feeFrequency;
	}
	public void setFeeFrequency(String feeFrequency) {
		this.feeFrequency = feeFrequency;
	}
	public double getFeeRate() {
		return feeRate;
	}
	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}
	public String getFeeStartDate() {
		return feeStartDate;
	}
	public void setFeeStartDate(String feeStartDate) {
		this.feeStartDate = feeStartDate;
	}
	public String getFeeEndDate() {
		return feeEndDate;
	}
	public void setFeeEndDate(String feeEndDate) {
		this.feeEndDate = feeEndDate;
	}
	public String getFeeDate() {
		return feeDate;
	}
	public void setFeeDate(String feeDate) {
		this.feeDate = feeDate;
	}
	public double getPredictAmt() {
		return predictAmt;
	}
	public void setPredictAmt(double predictAmt) {
		this.predictAmt = predictAmt;
	}

	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFeeCcy() {
		return feeCcy;
	}
	public void setFeeCcy(String feeCcy) {
		this.feeCcy = feeCcy;
	}
	public String getFeeBasis() {
		return feeBasis;
	}
	public void setFeeBasis(String feeBasis) {
		this.feeBasis = feeBasis;
	}
	@Override
	public String toString() {
		return "TdFeesPlan [dealNo=" + dealNo + ", refNo=" + refNo + ", feeCcy=" + feeCcy + ", feeBasis=" + feeBasis
				+ ", feeManner=" + feeManner + ", feeType=" + feeType + ", feePeriod=" + feePeriod + ", feeSituation="
				+ feeSituation + ", feeFrequency=" + feeFrequency + ", feeRate=" + feeRate
				+ ", feeStartDate=" + feeStartDate + ", feeEndDate=" + feeEndDate + ", feeDate=" + feeDate
				+ ", predictAmt=" + predictAmt + ", remark=" + remark + "]";
	}
	
	
}
