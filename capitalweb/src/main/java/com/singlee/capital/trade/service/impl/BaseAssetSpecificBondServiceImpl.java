package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.trade.mapper.TdAssetSpecificBondMapper;
import com.singlee.capital.trade.model.TdAssetSpecificBond;
import com.singlee.capital.trade.service.BaseAssetSpecificBondService;

/**
 * @projectName 同业业务管理系统
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseAssetSpecificBondServiceImpl implements BaseAssetSpecificBondService {
	
	@Autowired
	public TdAssetSpecificBondMapper assetSpecificBondMapper;

	@Override
	public List<TdAssetSpecificBond> getTdAssetSpecificBondList(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return assetSpecificBondMapper.getTdAssetSpecificBondList(params);
	}
	
	/***
	 * ----以上为对外服务-------------------------------
	 */
}
