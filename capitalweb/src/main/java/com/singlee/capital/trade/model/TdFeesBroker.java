package com.singlee.capital.trade.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name = "TD_FEES_BROKER")
public class TdFeesBroker {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TD_FEES_BROKER.NEXTVAL FROM DUAL")
	private String  dealNo               ;//费用流水号  
	private String  refNo                ;//挂靠的原交易流水   
	private String  feeType              ;//费用类型品种（固定、费率）   
	private String  feeSituation         ;//收付情况 0.收费 1.付费   
	private double  feeRate              ;//中介费率(%)   
	private String  remark               ;//备注  
	private String  feeCcy               ;//币种   
	private String  feeBasis             ;//计息基础   
	private String  passagewayCno        ;//中介客户编号   
	private String  passagewayName       ;//中介方名称   
	private String  passagewayBankacccode;//银行帐号   
	private String  passagewayBankaccname;//账户名称   
	private String  passagewayBankcode   ;//开户行行号    
	private String  passagewayBankname   ;//开户行名称 
	private double  feesAmt              ;//手工输入的费用（固定）
	private double  feesFaceamt          ;//费率本金（一般和交易单的实际本金挂钩）
	private String  organization         ;//中介机构
	
	//新增2和属性  投资组合   成本中心
	@Transient
	private String cost;  //成本中心
	@Transient
	private String port;
	@Transient
	private String prdName;//合同标题 产品标题
	@Transient
	private String conTitle;//合同标题 产品标题
	@Transient
	private String dealDate;//交易日期
	@Transient
	private double amt;//本金
	@Transient
	private String vdate;//起息日
	@Transient
	private String mdate;//到息日
	@Transient
	private String partyName;//客户名称
	@Transient
	private String aDate;//审批开始日期
	
	
	
	
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public String getFeeType() {
		return feeType;
	}
	public String getFeeSituation() {
		return feeSituation;
	}
	public double getFeeRate() {
		return feeRate;
	}
	public String getRemark() {
		return remark;
	}
	public String getFeeCcy() {
		return feeCcy;
	}
	public String getFeeBasis() {
		return feeBasis;
	}
	public String getPassagewayCno() {
		return passagewayCno;
	}
	public String getPassagewayName() {
		return passagewayName;
	}
	public String getPassagewayBankacccode() {
		return passagewayBankacccode;
	}
	public String getPassagewayBankaccname() {
		return passagewayBankaccname;
	}
	public String getPassagewayBankcode() {
		return passagewayBankcode;
	}
	public String getPassagewayBankname() {
		return passagewayBankname;
	}
	public double getFeesAmt() {
		return feesAmt;
	}
	public double getFeesFaceamt() {
		return feesFaceamt;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public void setFeeSituation(String feeSituation) {
		this.feeSituation = feeSituation;
	}
	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setFeeCcy(String feeCcy) {
		this.feeCcy = feeCcy;
	}
	public void setFeeBasis(String feeBasis) {
		this.feeBasis = feeBasis;
	}
	public void setPassagewayCno(String passagewayCno) {
		this.passagewayCno = passagewayCno;
	}
	public void setPassagewayName(String passagewayName) {
		this.passagewayName = passagewayName;
	}
	public void setPassagewayBankacccode(String passagewayBankacccode) {
		this.passagewayBankacccode = passagewayBankacccode;
	}
	public void setPassagewayBankaccname(String passagewayBankaccname) {
		this.passagewayBankaccname = passagewayBankaccname;
	}
	public void setPassagewayBankcode(String passagewayBankcode) {
		this.passagewayBankcode = passagewayBankcode;
	}
	public void setPassagewayBankname(String passagewayBankname) {
		this.passagewayBankname = passagewayBankname;
	}
	public void setFeesAmt(double feesAmt) {
		this.feesAmt = feesAmt;
	}
	public void setFeesFaceamt(double feesFaceamt) {
		this.feesFaceamt = feesFaceamt;
	}
	public String getConTitle() {
		return conTitle;
	}
	public String getDealDate() {
		return dealDate;
	}
	public double getAmt() {
		return amt;
	}
	public void setConTitle(String conTitle) {
		this.conTitle = conTitle;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	
}
