package com.singlee.capital.trade.service;

import java.util.List;

import com.singlee.capital.trade.model.TcGuidePriceVo;

public interface TcGuidePriceService {

	public List<TcGuidePriceVo> getTcGuidePrices();
	
}
