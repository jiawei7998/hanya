package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdRateChange;
import com.singlee.capital.trade.service.RateChangeService;

@Controller
@RequestMapping(value="/TdRateChangeController")
public class TdRateChangeController extends CommonController{
	@Autowired
	RateChangeService rateChangeService;
	
	@ResponseBody
	@RequestMapping(value = "/searchPageRateChangeForDealNo")
	public RetMsg<PageInfo<TdRateChange>> searchPageRateChangeForDealNo(@RequestBody Map<String,Object> params){
		Page<TdRateChange> page = rateChangeService.getRateChangeForDealNoPage(params);
		return RetMsgHelper.ok(page);
	}
}
