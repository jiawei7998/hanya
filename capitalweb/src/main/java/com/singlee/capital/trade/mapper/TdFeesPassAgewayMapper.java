package com.singlee.capital.trade.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdFeesPassAgeway;
import com.singlee.capital.trade.model.TdFeesPassAgewayDaily;
import com.singlee.capital.trade.model.TdFeesPassAgewayVo;
import com.singlee.capital.trade.model.TmFeesPassagewy;

public interface TdFeesPassAgewayMapper extends Mapper<TdFeesPassAgeway>{

	public Page<TdFeesPassAgeway> getTdFeesPassAgeways(Map<String, Object> params, RowBounds rb);
	
	public List<TdFeesPassAgeway> getTdFeesPassAgeways(Map<String, Object> params);

	public void createTempPassageWay(HashMap<String, Object> map);
	
	public void insertTempPassageWay(TmFeesPassagewy tmFeesPassagewy);

	public void updateTempPassageWay(HashMap<String, Object> map);

	public Page<TdFeesPassAgeway> getPassageWayList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<TdFeesPassAgeway> getPassageWayListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<TdFeesPassAgeway> getPassageWayListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public void insertDaily(List<TdFeesPassAgewayDaily> list);

	public void deleteByDealNo(List<String> list);
	

	public String getTdFeesPassAgewaysByBean(Map<String, Object> map);

	public void addTdFeesPassway(TdFeesPassAgeway tdFeesPassAgeway);
	
	public void updateTdFeesPassway(TdFeesPassAgeway tdFeesPassAgeway);

	public TdFeesPassAgeway selectTrtrdById(String dealNo);

	public List<TdFeesPassAgeway> getTdFeesPassAgewaysByDeal(
			Map<String, Object> params);

	public void deleteByDealNoAndDate(String refNo);
	
	public void deleteByDealNoAndCust(Map<String, Object> params);

	public List<TdFeesPassAgeway> getTdPassageWayByRefNo(
			Map<String, Object> params);

	public List<TdFeesPassAgeway> getPassageWayByEndDate(
			Map<String, Object> params);

	public List<TdFeesPassAgeway> getTdFeesListByRef(Map<String, Object> map);

	public List<TdFeesPassAgewayVo> getTdFeesPassAgewayListVo(Map<String, Object> params);

	public double getSumFeeRateByRefNo(Map<String, Object> params);
}
