package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * CRMS延伸字段存储
 * @author SINGLEE
 *
 */
@Entity
@Table(name="TD_PRODUCT_APPROVE_CRMS")
public class TdProductApproveCrms implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dealNo;
	@Id
	private int version;
	private String productNameCom;//产品名称
	private String bondPriorityFlag;//债权优先级标志
	private String spvType;//特殊目的载体类型
	private String platEarmark;//平台投向
	private String indusEarmark;//行业投向
	private String investFinal;//最终投向
	private String baseProductRemark;//备注
	private String baseProductType;//基础资产类型
	private String shoreholderType;//股东性质
	private String creditType;//贷款用途
	private String trustType;//基础资产增信方式
	private String trustRemark;//基础资产增信描述
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getProductNameCom() {
		return productNameCom;
	}
	public void setProductNameCom(String productNameCom) {
		this.productNameCom = productNameCom;
	}
	public String getBondPriorityFlag() {
		return bondPriorityFlag;
	}
	public void setBondPriorityFlag(String bondPriorityFlag) {
		this.bondPriorityFlag = bondPriorityFlag;
	}
	public String getSpvType() {
		return spvType;
	}
	public void setSpvType(String spvType) {
		this.spvType = spvType;
	}
	public String getPlatEarmark() {
		return platEarmark;
	}
	public void setPlatEarmark(String platEarmark) {
		this.platEarmark = platEarmark;
	}
	public String getIndusEarmark() {
		return indusEarmark;
	}
	public void setIndusEarmark(String indusEarmark) {
		this.indusEarmark = indusEarmark;
	}
	public String getInvestFinal() {
		return investFinal;
	}
	public void setInvestFinal(String investFinal) {
		this.investFinal = investFinal;
	}
	public String getBaseProductRemark() {
		return baseProductRemark;
	}
	public void setBaseProductRemark(String baseProductRemark) {
		this.baseProductRemark = baseProductRemark;
	}
	public String getBaseProductType() {
		return baseProductType;
	}
	public void setBaseProductType(String baseProductType) {
		this.baseProductType = baseProductType;
	}
	public String getShoreholderType() {
		return shoreholderType;
	}
	public void setShoreholderType(String shoreholderType) {
		this.shoreholderType = shoreholderType;
	}
	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	public String getTrustType() {
		return trustType;
	}
	public void setTrustType(String trustType) {
		this.trustType = trustType;
	}
	public String getTrustRemark() {
		return trustRemark;
	}
	public void setTrustRemark(String trustRemark) {
		this.trustRemark = trustRemark;
	}
	@Override
	public String toString() {
		return "TdProductApproveCrms [dealNo=" + dealNo + ", version="
				+ version + ", productNameCom=" + productNameCom
				+ ", bondPriorityFlag=" + bondPriorityFlag + ", spvType="
				+ spvType + ", platEarmark=" + platEarmark + ", indusEarmark="
				+ indusEarmark + ", investFinal=" + investFinal
				+ ", baseProductRemark=" + baseProductRemark
				+ ", baseProductType=" + baseProductType + ", shoreholderType="
				+ shoreholderType + ", creditType=" + creditType
				+ ", trustType=" + trustType + ", trustRemark=" + trustRemark
				+ "]";
	}
	
	
	
}
