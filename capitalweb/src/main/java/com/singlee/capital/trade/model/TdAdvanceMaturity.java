package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.base.model.TcProduct;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;


/**
 * @projectName 同业业务管理系统
 * @className 提前到期
 * @description TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_ADVANCE_MATURITY")
public class TdAdvanceMaturity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3720610986116412801L;
	/**
	 * 交易单号 自动生成
	 */
	@Id
	private String dealNo;
	/**
	 * 引用编号
	 */
	private String refNo;
	/**
	 * 审批类型
	 */
	private String dealType;
	/**
	 * 审批发起人
	 */
	private String sponsor;
	/**
	 * 审批发起机构
	 */
	private String sponInst;
	/**
	 * 审批开始日期
	 */
	private String aDate;
	/**
	 * 交易日期
	 */
	private String dealDate;
	/**
	 * 提前还款金额（元）：（*）
	 */
	private double amAmt;
	/**
	 * 提前还款利息（元）：（*）
	 */
	private double amInt;
	/**
	 * 提前还款罚息(元)：（*）
	 */
	private double amPenalty;
	/**
	 * 提前还款日期：（*）
	 */
	private String amDate;
	/**
	 * 提前到期原因
	 */
	private String amReason;
	/**
	 * 影像资料
	 */
	private String image;
	/**
	 * 最后更新时间
	 */
	private String lastUpdate;
	
	/**
	 * 版本号
	 */
	private int version;
	/**
	 * 提前到期备注
	 */
	private String remark;
	/**
	 * 提前还款本金对应应收利息（元）
	 */
	private double amFiamt;
	
	/**
	 * 剩余本金对应应收利息（元）
	 */
	private double remainFiamt;
	/**
	 * 剩余本金
	 */
	private double remainAmt;
	
	private double amAddFiamt;//提前还款本金交易日与起息日之间差额天数所含利息
	private double remainAddFiamt;//剩余本金交易日与起息日之间差额天数所含利息
	private double returnIamt;//先收息前提下的归还利息
	private String intType;//先收息标志位
	private double amorFiamt;//已摊销金额SUM
	private double amorAddFiamt;//交易日与起息日之间差额天数所含摊销金额
	/**
	 * 审批状态
	 */
	@Transient
	private String approveStatus;
	@Transient
	private TaUser user;
	@Transient
	private TtInstitution institution;
	@Transient
	private String taskId;
	@Transient
	private TcProduct product;
	@Transient
	private TtCounterParty party;
	@Transient
	private String prdName;
	//现金流类型
	private String cfType;
	
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getIntType() {
		return intType;
	}
	public void setIntType(String intType) {
		this.intType = intType;
	}
	public double getAmorFiamt() {
		return amorFiamt;
	}
	public void setAmorFiamt(double amorFiamt) {
		this.amorFiamt = amorFiamt;
	}
	public double getAmorAddFiamt() {
		return amorAddFiamt;
	}
	public void setAmorAddFiamt(double amorAddFiamt) {
		this.amorAddFiamt = amorAddFiamt;
	}
	public double getAmAddFiamt() {
		return amAddFiamt;
	}
	public void setAmAddFiamt(double amAddFiamt) {
		this.amAddFiamt = amAddFiamt;
	}
	public double getRemainAddFiamt() {
		return remainAddFiamt;
	}
	public void setRemainAddFiamt(double remainAddFiamt) {
		this.remainAddFiamt = remainAddFiamt;
	}
	public double getReturnIamt() {
		return returnIamt;
	}
	public void setReturnIamt(double returnIamt) {
		this.returnIamt = returnIamt;
	}
	
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponInst() {
		return sponInst;
	}
	public void setSponInst(String sponInst) {
		this.sponInst = sponInst;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	
	public double getAmAmt() {
		return amAmt;
	}
	public void setAmAmt(double amAmt) {
		this.amAmt = amAmt;
	}
	public double getAmInt() {
		return amInt;
	}
	public void setAmInt(double amInt) {
		this.amInt = amInt;
	}
	public double getAmPenalty() {
		return amPenalty;
	}
	public void setAmPenalty(double amPenalty) {
		this.amPenalty = amPenalty;
	}
	public String getAmDate() {
		return amDate;
	}
	public void setAmDate(String amDate) {
		this.amDate = amDate;
	}
	public String getAmReason() {
		return amReason;
	}
	public void setAmReason(String amReason) {
		this.amReason = amReason;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public TaUser getUser() {
		return user;
	}
	public void setUser(TaUser user) {
		this.user = user;
	}
	public TtInstitution getInstitution() {
		return institution;
	}
	public void setInstitution(TtInstitution institution) {
		this.institution = institution;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public TcProduct getProduct() {
		return product;
	}
	public void setProduct(TcProduct product) {
		this.product = product;
	}
	public TtCounterParty getParty() {
		return party;
	}
	public void setParty(TtCounterParty party) {
		this.party = party;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public double getAmFiamt() {
		return amFiamt;
	}
	public void setAmFiamt(double amFiamt) {
		this.amFiamt = amFiamt;
	}
	public double getRemainFiamt() {
		return remainFiamt;
	}
	public void setRemainFiamt(double remainFiamt) {
		this.remainFiamt = remainFiamt;
	}
	public double getRemainAmt() {
		return remainAmt;
	}
	public void setRemainAmt(double remainAmt) {
		this.remainAmt = remainAmt;
	}
	
}
