package com.singlee.capital.trade.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdStockInfo;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产票据持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-10-14 下午16:52:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdStockInfoMapper extends Mapper<TdStockInfo> {

	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public Page<TdStockInfo> getStockInfoList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表
	 * 
	 * @param params - 请求参数
	 * @return 基础资产对象集合
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public List<TdStockInfo> getStockInfoList(Map<String, Object> params);
	
	/**
	 * 根据交易单号删除产品审批子表记录
	 * 
	 * @param dealNo - 交易单号
	 * @author Hunter
	 * @date 2016-10-14
	 */
	public void deleteStockInfoByDealNo(String dealNo);
}