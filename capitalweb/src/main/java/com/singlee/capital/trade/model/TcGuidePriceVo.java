package com.singlee.capital.trade.model;

import java.io.Serializable;

public class TcGuidePriceVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prdNo;
	private String prdInst;
	private String prdName;
	private String prdType;
	private double Rate_2W;
	private double Rate_1M;
	private double Rate_3M;
	private double Rate_6M;
	private double Rate_1Y;
	private double Rate_2Y;
	
	public String getPrdType() {
		return prdType;
	}
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	public double getRate_2W() {
		return Rate_2W;
	}
	public void setRate_2W(double rate_2w) {
		Rate_2W = rate_2w;
	}
	public double getRate_1M() {
		return Rate_1M;
	}
	public void setRate_1M(double rate_1m) {
		Rate_1M = rate_1m;
	}
	
	public double getRate_3M() {
		return Rate_3M;
	}
	public void setRate_3M(double rate_3m) {
		Rate_3M = rate_3m;
	}
	public double getRate_6M() {
		return Rate_6M;
	}
	public void setRate_6M(double rate_6m) {
		Rate_6M = rate_6m;
	}
	public double getRate_1Y() {
		return Rate_1Y;
	}
	public void setRate_1Y(double rate_1y) {
		Rate_1Y = rate_1y;
	}
	public double getRate_2Y() {
		return Rate_2Y;
	}
	public void setRate_2Y(double rate_2y) {
		Rate_2Y = rate_2y;
	}
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getPrdInst() {
		return prdInst;
	}
	public void setPrdInst(String prdInst) {
		this.prdInst = prdInst;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	@Override
	public String toString() {
		return "TcGuidePrice [prdNo=" + prdNo + ", prdInst=" + prdInst
				+ ", prdName=" + prdName + ", prdType=" + prdType
				+ ", Rate_2W=" + Rate_2W + ", Rate_1M=" + Rate_1M
				+ ", Rate_3M=" + Rate_3M + ", Rate_6M=" + Rate_6M
				+ ", Rate_1Y=" + Rate_1Y + ", Rate_2Y=" + Rate_2Y + "]";
	}
	
}
