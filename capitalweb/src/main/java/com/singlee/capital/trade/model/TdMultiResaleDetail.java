package com.singlee.capital.trade.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @projectName 同业业务管理系统
 * @className 资产卖断
 * @author xuhui
 * @createDate 2016-9-25 上午12:00:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Entity
@Table(name = "TD_MULTIRESALE_DETAIL")
public class TdMultiResaleDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8413279385562983763L;
	
	/**
	 * 转让资产原流水号
	 */
	private String dealNo;
	/**
	 * 转让流水号
	 */
	private String refNo;
	/**
	 * 实收本金
	 */
	private double mlAmt;
	/**
	 * 原业务金额
	 */
	private double orignAmt;
	/**
	 * 现应收本金
	 */
	private double needAmt;
	
	private String iCode;
	
	private String aType;
	
	private String mType;
	/**
	 * 交易对手名称
	 */
	private String cNo;
	/**
	 * 产品名称
	 */
	private String prdName;
	/**
	 * 是否虚拟记账
	 */
	private String virAccount;
	/**
	 * 应收息金额
	 */
	private double mlInterest;
	/**
	 * 实收利息
	 */
	private double realInterest;
	/**
	 * 资产转让收益
	 */
	private double transferIncome;
	/**
	 * 转让金额
	 */
	private double transferAmt;
	/**
	 *投资收益率 
	 */
	private double transferRate;
	/**
	 * 原交易版本
	 */
	private int version;
	@Transient
	private String cnoName;
	
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getiCode() {
		return iCode;
	}
	public void setiCode(String iCode) {
		this.iCode = iCode;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getVirAccount() {
		return virAccount;
	}
	public void setVirAccount(String virAccount) {
		this.virAccount = virAccount;
	}
	public double getMlAmt() {
		return mlAmt;
	}
	public void setMlAmt(double mlAmt) {
		this.mlAmt = mlAmt;
	}
	public double getOrignAmt() {
		return orignAmt;
	}
	public void setOrignAmt(double orignAmt) {
		this.orignAmt = orignAmt;
	}
	public double getNeedAmt() {
		return needAmt;
	}
	public void setNeedAmt(double needAmt) {
		this.needAmt = needAmt;
	}
	public double getMlInterest() {
		return mlInterest;
	}
	public void setMlInterest(double mlInterest) {
		this.mlInterest = mlInterest;
	}
	public double getRealInterest() {
		return realInterest;
	}
	public void setRealInterest(double realInterest) {
		this.realInterest = realInterest;
	}
	public double getTransferIncome() {
		return transferIncome;
	}
	public void setTransferIncome(double transferIncome) {
		this.transferIncome = transferIncome;
	}
	public double getTransferAmt() {
		return transferAmt;
	}
	public void setTransferAmt(double transferAmt) {
		this.transferAmt = transferAmt;
	}
	public double getTransferRate() {
		return transferRate;
	}
	public void setTransferRate(double transferRate) {
		this.transferRate = transferRate;
	}
	public String getCnoName() {
		return cnoName;
	}
	public void setCnoName(String cnoName) {
		this.cnoName = cnoName;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}
