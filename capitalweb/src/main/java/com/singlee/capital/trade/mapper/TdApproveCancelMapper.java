package com.singlee.capital.trade.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.trade.model.TdApproveCancel;

/**
 * @projectName 同业业务管理系统
 * @className 审批单撤销持久层
 * @author Hunter
 * @createDate 2016-9-25 下午2:04:28
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TdApproveCancelMapper extends Mapper<TdApproveCancel> {

	/**
	 * 根据主键查询对象
	 * 
	 * @param dealNo - 交易单号
	 * @return 审批单撤销对象
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public TdApproveCancel getApproveCancelById(String dealNo);
	
	/**
	 * 根据请求参数查询分页列表未审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 审批单撤销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdApproveCancel> getApproveCancelList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 审批单撤销对象列表
	 * @author Hunter
	 * @date 2016-9-25
	 */
	public Page<TdApproveCancel> getApproveCancelListFinished(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据请求参数查询分页列表 (我发起的)
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 审批单撤销对象列表
	 * @author Hunter
	 * @date 2016-8-9
	 */
	public Page<TdApproveCancel> getApproveCancelListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 对象传值查询对象
	 * @param tradeExtend
	 * @return
	 */
	public TdApproveCancel searchApproveCancel(TdApproveCancel approveCancel);
	
	/**
	 * 查询是否已存在交易单撤销
	 * @param approveCancel
	 * @return
	 */
	public TdApproveCancel ifExistApproveCancel(Map<String,Object> map);
}