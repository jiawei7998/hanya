package com.singlee.capital.trade.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.singlee.capital.common.util.TreeInterface;

/**
 * 附件 组织机构 树形 表 
 * 
 * @author x230i
 *
 */
@Entity
@Table(name = "TD_ATTACH_TREE_MAP")
public class TdAttachTreeMap implements Serializable, TreeInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TC_ATTACH.NEXTVAL FROM DUAL")
	private String treeId ;//	N	VARCHAR2(32)	N			
	private String refType;//REF_TYPE	N	VARCHAR2(20)	Y			所属关联类型
	private String refId;//REF_ID	N	VARCHAR2(32)	Y			所属关联编号
	private String treePid ;//	N	VARCHAR2(32)	N
	private String treeName;//FIELD_NAME	N	VARCHAR2(32)	Y			关联字段名
	private Integer sort;//CONTENT_TYPE	N	VARCHAR2(32)	N			附件内容类型
	private String createUserId;
	private String createUserName;
	private String createInstId;
	private String createInstName;
	private String createTime;
	private String updateUserId;
	private String updateUserName;
	private String updateInstId;
	private String updateInstName;
	private String updateTime;

	@Transient
	private List<TdAttachTreeMap> children;
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TdAttachTreeMap [treeId=");
		builder.append(treeId);
		builder.append(", refType=");
		builder.append(refType);
		builder.append(", refId=");
		builder.append(refId);
		builder.append(", treePid=");
		builder.append(treePid);
		builder.append(", treeName=");
		builder.append(treeName);
		builder.append(", sort=");
		builder.append(sort);
		builder.append("]");
		return builder.toString();
	}
	public String getTreeId() {
		return treeId;
	}
	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getTreePid() {
		return treePid;
	}
	public void setTreePid(String treePid) {
		this.treePid = treePid;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	@Override
	public int getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	@Override
	public String getId() {
		return getTreeId();
	}
	@Override
	public String getParentId() {
		return getTreePid();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void children( List<? extends TreeInterface> list) {
		setChildren((List<TdAttachTreeMap>) list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	
	public List<TdAttachTreeMap> getChildren() {
		return children;
	}
	public void setChildren(List<TdAttachTreeMap> children) {
		Collections.sort(children, new Comparator<TdAttachTreeMap>() {
			@Override
			public int compare(TdAttachTreeMap arg0, TdAttachTreeMap arg1) {
				return arg0.getSort() > arg1.getSort() ? 1 : 0;
			}
			
		});
		this.children = children;
	}
	
	@Override
	public String getText() {
		return getTreeName();
	}
	@Override
	public String getValue() {
		return getTreeId();
	}
	@Override
	public String getIconCls() {
		return null;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateInstId() {
		return createInstId;
	}
	public void setCreateInstId(String createInstId) {
		this.createInstId = createInstId;
	}
	public String getCreateInstName() {
		return createInstName;
	}
	public void setCreateInstName(String createInstName) {
		this.createInstName = createInstName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	public String getUpdateInstId() {
		return updateInstId;
	}
	public void setUpdateInstId(String updateInstId) {
		this.updateInstId = updateInstId;
	}
	public String getUpdateInstName() {
		return updateInstName;
	}
	public void setUpdateInstName(String updateInstName) {
		this.updateInstName = updateInstName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
