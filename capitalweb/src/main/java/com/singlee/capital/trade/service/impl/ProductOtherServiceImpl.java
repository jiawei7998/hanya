package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.base.mapper.TcProductMapper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdProductApproveAmtLineMapper;
import com.singlee.capital.trade.mapper.TdProductApproveNeedLoanMapper;
import com.singlee.capital.trade.model.ProductApproveParamVo;
import com.singlee.capital.trade.model.TdProductApproveAmtLine;
import com.singlee.capital.trade.model.TdProductApproveNeedLoan;
import com.singlee.capital.trade.service.ProductOtherService;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductOtherServiceImpl implements ProductOtherService {
	@Autowired
	private TdProductApproveNeedLoanMapper tdProductApproveNeedLoanMapper;
	@Autowired
	private TdProductApproveAmtLineMapper tdProductApproveAmtLineMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private TcProductMapper tcProductMapper;
	
	@Override
	public List<ProductApproveParamVo> getLoanParam() {
		List<ProductApproveParamVo> result = tcProductMapper.getLoanParam();
		return result;
	}
	
	@Override
	public List<TdProductApproveAmtLine> getAmtLineParam(String prdNo) {
		if(StringUtil.isEmpty(prdNo)){
			return null;
		}
		List<TdProductApproveAmtLine> result = tcProductMapper.getAmtLineParam(prdNo);
		return result;
	}
	
	@Override
	public void saveAllProductParams(Map<String,Object> map) throws Exception {
		String jsonString = ParameterUtil.getString(map, "result", "{}");
		List<TdProductApproveAmtLine> result = FastJsonUtil.parseArrays(jsonString, TdProductApproveAmtLine.class);
		
		if(result != null && result.size() > 0){

			//清空该产品下的金额信息
			tdProductApproveAmtLineMapper.deleteAllAmtLine(result.get(0).getPrdNo()+"");
			
			//insert
			batchDao.batch("com.singlee.capital.trade.mapper.TdProductApproveAmtLineMapper.insert", result);
		}
	}

	@Override
	public void openLoan(String prdNo) throws Exception {
		//改为不放款
		if(StringUtil.isNotEmpty(prdNo)){
			TdProductApproveNeedLoan tdProductApproveNeedLoan = new TdProductApproveNeedLoan();
			tdProductApproveNeedLoan.setPrdNo(Integer.parseInt(prdNo));
			tdProductApproveNeedLoanMapper.insert(tdProductApproveNeedLoan);
		}
	}

	@Override
	public void closeLoan(String prdNo) throws Exception {
		//改为不放款
		if(StringUtil.isNotEmpty(prdNo)){
			TdProductApproveNeedLoan tdProductApproveNeedLoan = new TdProductApproveNeedLoan();
			tdProductApproveNeedLoan.setPrdNo(Integer.parseInt(prdNo));
			tdProductApproveNeedLoanMapper.delete(tdProductApproveNeedLoan);
		}
	}

}
