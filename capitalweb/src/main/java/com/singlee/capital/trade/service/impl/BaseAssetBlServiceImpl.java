package com.singlee.capital.trade.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdBaseAssetBlMapper;
import com.singlee.capital.trade.model.TdBaseAssetBl;
import com.singlee.capital.trade.service.BaseAssetBlService;

/**
 * @projectName 同业业务管理系统
 * @className 基础资产服务实现
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-29 上午10:22:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseAssetBlServiceImpl implements BaseAssetBlService {
	
	@Autowired
	public TdBaseAssetBlMapper tdBaseAssetBlMapper;
	
	@Override
	public List<TdBaseAssetBl> getBaseAssetBlList(Map<String, Object> params) {
		return tdBaseAssetBlMapper.getTdBaseAssetBlList(params);
	}

	@Override
	public Page<TdBaseAssetBl> getBaseAssetBlLists(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TdBaseAssetBl> param = tdBaseAssetBlMapper.getTdBaseAssetBlLists(params, rowBounds);
		return param;
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
}
