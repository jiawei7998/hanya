package com.singlee.capital.trade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.trade.mapper.TdProductFeeDealMapper;
import com.singlee.capital.trade.model.TdProductFeeDeal;
import com.singlee.capital.trade.service.ProductFeeDealService;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProductFeeDealServiceImpl implements ProductFeeDealService {
	@Autowired
	private TdProductFeeDealMapper productFeeDealMapper;
	@Override
	public Page<TdProductFeeDeal> getFeeDealsByOrgDealNo(
			Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		return this.productFeeDealMapper.getPageProductFeeDeals(paramsMap, ParameterUtil.getRowBounds(paramsMap));
	}

}
