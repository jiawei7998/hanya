package com.singlee.capital.trade.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.trade.model.TdOverDueConfirm;
import com.singlee.capital.trade.service.TdOverDueConfirmService;

@Controller
@RequestMapping(value="/TdOverdueConfirmController")
public class TdOverdueConfirmController extends CommonController{
	@Autowired
	TdOverDueConfirmService overDueConfirmService;
	
	@ResponseBody
	@RequestMapping(value = "/searchPageOverdueConfirmForDealNo")
	public RetMsg<PageInfo<TdOverDueConfirm>> searchPageOverdueConfirmForDealNo(@RequestBody Map<String,Object> params){
		Page<TdOverDueConfirm> page = overDueConfirmService.getOverdueConfirmForDealNoPage(params);
		return RetMsgHelper.ok(page);
	}
}
