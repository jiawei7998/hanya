package com.singlee.capital.counterparty.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.counterparty.mapper.CounterPartyDao;
import com.singlee.capital.counterparty.mapper.TtCounterPartyAccMapper;
import com.singlee.capital.counterparty.mapper.TtCounterPartyExtMapper;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.model.TempPartyMap;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.counterparty.model.TtCounterPartyAcc;
import com.singlee.capital.counterparty.model.TtCounterPartyCon;
import com.singlee.capital.counterparty.model.TtCounterPartyExt;
import com.singlee.capital.counterparty.model.TtCounterPartyKind;
import com.singlee.capital.counterparty.service.CounterPartyService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;

/**
 * 交易对手 服务实现
 * 
 * @author LyonChen
 * 
 */
@Service("counterPartyService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CounterPartyServiceImpl implements CounterPartyService {

	@Autowired
	private CounterPartyDao counterPartyDao;
	
	@Autowired
	private TtCounterPartyAccMapper ttCounterPartyAccMapper;
	@Autowired
	private TtCounterPartyExtMapper ttCounterPartyExtMapper;
	/**
	 * 查询交易对手 分页数据 
	 * big_kind: 大类 (允许为空)
	 * small_kind：小类 （允许为空）
	 * cp_name: 交易对手 名称 (允许为空）
	 * begin_num:起始记录数(不允许为空)
	 * end_num:结束记录数(不允许为空)
	 */
	@Override
	public Page<CounterPartyVo> searchCp(Map<String,Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<CounterPartyVo> ph = counterPartyDao.searchCounterParty(map,rowBounds);
		return ph;
	}
	
	/**
	 * 交易对手 分页数据 
	 * @param cp_name	交易对手名称模糊查询
	 * @param limit 限制条数
	 * @return
	 */
	@Override
	public List<TtCounterParty> listCp(Map<String,Object> map){
		return counterPartyDao.listCounterParty(map);
	}
	
	/** 保存交易对手  */
	@Override
	public void saveCp(Map<String,Object> map) {
		CounterPartyVo cp=new CounterPartyVo();
		TtCounterPartyExt ext = new TtCounterPartyExt();
		TempPartyMap tp = new TempPartyMap();
		try {
			BeanUtil.populate(cp, map);
			BeanUtil.populate(ext, map);
			BeanUtil.populate(tp, map);
		} catch (Exception e) {
			JY.raise(e.getMessage());
		}
		
		//客户联系人格式处理
		String constact_listJson = ParameterUtil.getString(map, "constact_list", "");
		List<TtCounterPartyCon> conList = FastJsonUtil.parseArrays(constact_listJson, TtCounterPartyCon.class);
		cp.setConstact_list(conList);
		
		//模板格式处理
		/*String excelstact_list = ParameterUtil.getString(map, "excelstact_list", "");
		List<TempPartyMap> excelList = FastJsonUtil.parseArrays(excelstact_list, TempPartyMap.class);
		for (TempPartyMap tempPartyMap : excelList) {
			tp.setTemp_code(tempPartyMap.getTemp_code());
			tp.setTemp_type(tempPartyMap.getTemp_type());
			tp.setTemplate_name(tempPartyMap.getTemplate_name());
		}
		
		
		checkCPName(cp.getParty_code(),cp.getParty_name(),cp.getParty_shortname());*/
		//增加流程
		if ("add".equals(cp.getTypes())) {
			int count = counterPartyDao.queryCounterPartyCon(cp);
			if(count>0){
				JY.raise("核心客户号或营业执照有重复数据,请调整");
			}
			cp.setParty_smallkind("SL");
			// 生成客户代码 格式为 大类+小类+地区+no序列号
			cp.setParty_code(String.format("%s%s%s%s", 
					cp.getParty_bigkind(), cp.getParty_smallkind(), cp.getBelongarea(), 
					counterPartyDao.selectCounterPartyNo()));
			// 设置为启用状态
			cp.setIs_active(DictConstants.AccStatus.Enabled);
			// 交易对手插入 
			counterPartyDao.insertCounterParty(cp);
			// 如果有交易对手联系人
			if (cp.getConstact_list() != null && cp.getConstact_list().size() > 0) {
				// 设置交易对手联系人 的party_id
				for(TtCounterPartyCon con : cp.getConstact_list()){
					con.setParty_id(cp.getParty_id());
				}
				// 批量插入
				counterPartyDao.insertBatchCounterPartyCon(cp);
				
			}
			ext.setPartyId(cp.getParty_id());
			ttCounterPartyExtMapper.insert(ext);
		} else if ("edit".equals(cp.getTypes())) {
			int count = counterPartyDao.queryCounterPartyCon(cp);
			if(count>1){
				JY.raise("核心客户号或营业执照有重复数据,请调整");
			}
			//cp.setParty_smallkind("SL");
			// 更新 交易对手 
			counterPartyDao.updateCounterParty(cp);
			// 清空联系人信息
			counterPartyDao.deleteCounterPartyCon(cp.getParty_id());
			// 如果有交易对手联系人
			if (cp.getConstact_list() != null && cp.getConstact_list().size() > 0) {
				// 设置交易对手联系人 的party_id
				for(TtCounterPartyCon con : cp.getConstact_list()){
					con.setParty_id(cp.getParty_id());
				}
				// 批量插入
				counterPartyDao.insertBatchCounterPartyCon(cp);
			}
			ext.setPartyId(cp.getParty_id());
			ttCounterPartyExtMapper.updateByPrimaryKey(ext);
		} else {
			// types错误，不做任何事情返回
		}
		// 交易对手模板保存
		//tp.setParty_id(cp.getParty_id());
		//counterPartyDao.insertTempParty(tp);
	}


	/** 激活 停用 交易对手  */
	@Override
	public void activeCp(String party_id, String user_id, String time) {
		CounterPartyVo cp = selectCP(party_id, null);
		if(cp!=null){
			if(DictConstants.AccStatus.Enabled.equals(cp.getIs_active())){
				cp.setIs_active(DictConstants.AccStatus.Disabled);
			}else if(DictConstants.AccStatus.Disabled.equals(cp.getIs_active())){
				cp.setIs_active(DictConstants.AccStatus.Enabled);
			}else {
				return;
			}
			cp.setUpdate_time(time);
			cp.setUpdate_userid(user_id);
			counterPartyDao.updateCounterPartyActive(cp);
		}else{
			JY.raise(String.format("交易对手(%s)没有找到",party_id));
		}
	}
	/** 选中 交易对手 根据交易对手编号以及交易对手编码 */
	@Override
	public CounterPartyVo selectCP(String party_id, String party_code) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("party_id", party_id);
		map.put("party_code", party_code);
		List<CounterPartyVo> cpList = counterPartyDao.selectCounterParty(map);
		if(cpList.size() == 1){
			return cpList.get(0);
		}else{
			JY.require(cpList.size() == 1, "【party_id:" +party_id+"】和【party_code:"+party_code+"】的客户不存在！");
		}
		return null;
	}
	
	@Override
	public TtCounterPartyExt selectCounterPartyExt(Map<String,Object> map){
		TtCounterPartyExt ext =new TtCounterPartyExt();
		String partyId = ParameterUtil.getString(map, "party_id", "");
		ext.setPartyId(partyId);
		ext =ttCounterPartyExtMapper.selectByPrimaryKey(ext);
		return ext;
	}
	
	/** 选中 交易对手  根据交易对手简称 */
	@Override
	public CounterPartyVo selectCPByShortName(String party_shortname) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("party_shortname", party_shortname );
		List<CounterPartyVo> cpList = counterPartyDao.selectCounterParty(map);
		if(cpList != null && cpList.size() > 0) {
			return cpList.get(0);}
		return null;
	}
	
	/** 选中 交易对手 map */
	@Override
	public CounterPartyVo selectCPByMap(Map<String, String> map) {
		List<CounterPartyVo> cpList = counterPartyDao.selectCounterParty(map);
		if(cpList != null && cpList.size() > 0) {
			return cpList.get(0);}
		return null;
	}

	/** 
	 * 获得某个交易对手
	 *  issuer_id 发行人id
	 */
	@Override
	public CounterPartyVo selectCPByIssuerId(String issuer_id){
		Map<String, String> map = new HashMap<String, String>();
		map.put("issuer_id", issuer_id);
		List<CounterPartyVo> cpList = counterPartyDao.selectCounterParty(map);
		if(cpList != null && cpList.size() > 0) {
			return cpList.get(0);}
		return null;
	}

	/** 删除某个交易对手*/
	@Override
	public void removeCP(String party_id){
		counterPartyDao.deleteCounterParty(party_id);
		counterPartyDao.deleteCounterPartyAcc(party_id);
		counterPartyDao.deleteCounterPartyCon(party_id);
	}
	/** 查询交易对手分类 */
	@Override
	public Page<CounterPartyKindVo> searchCpKind(Map<String,Object> params){
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<CounterPartyKindVo> ph = counterPartyDao.searchCounterPartyKind(params, rowBounds);
		return ph;
	}
	/** 删除分类  分类与大类的映射 */
	@Override
	public void removeCPKind(String kind){
		if(counterPartyDao.checkCounterPartyKind(kind)>0){
			throw new RException(String.format("客户小类(%s)已经有交易对手存在，不允许删除！",kind));
		}
		counterPartyDao.deleteCounterPartyKind(kind);
		counterPartyDao.deleteCounterPartyKindMap(kind);
	}

	/** 保存分类 （必须是小类） */
	@Override
	public void saveCpKind(CounterPartyKindVo cp){
		cp.setKind_grade("1") ;//指明是小类
		if ("add".equals(cp.getTypes())) {
				counterPartyDao.insertCounterPartyKind(cp);
				counterPartyDao.insertCounterPartyKindMap(cp);
		} else if ("edit".equals(cp.getTypes())) {
			counterPartyDao.updateCounterPartyKind(cp);
			counterPartyDao.updateCounterPartyKindMap(cp);
		} else {
			// types错误，不做任何事情返回
		}
	}
	
	/** 根据分类查找对应的交易对手列表 */
	@Override
	public List<TtCounterParty> getCpByKind(List<String> bigKindList,List<String> smallKindList){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("is_active", DictConstants.YesNo.YES);
		map.put("partyBigKindList", bigKindList);
		map.put("partySmallKindList", smallKindList);
		return counterPartyDao.listCounterParty(map);
	}
	/** 根据分类名称查找对应的分类 */
	@Override
	public List<TtCounterPartyKind> getCpKindByKindName(String kindName,String kindType){
		return counterPartyDao.selectCounterPartyKindByName(kindName, kindType);
	}
	
	//除了code对应的对手，不能有重复的 全称和简称 相同的交易对手存在
	public void checkCPName(String code, String name, String shortname){
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("code", code);
		map.put("name", name);
		map.put("shortname", shortname);
		int c = counterPartyDao.checkCounterParty(map);
		JY.require(c<=0, "交易对手全称(%s)，简称(%s)已经在系统有存在！请查证修改！",name,shortname);
	}

	
	

	@Override
	public CounterPartyVo selectHeadBank(String party_smallKind) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("party_smallkind", party_smallKind);
		map.put("headbank_flag", DictConstants.YesNo.YES);
		List<CounterPartyVo> cpList = counterPartyDao.selectCounterParty(map);
		if(cpList != null && cpList.size() > 0) {
			return cpList.get(0);}
		return null;
	}

	/**
	 * 根据交易对手大类来判断交易对手是否非银
	 * @param ttCounterPartyKind
	 * @return
	 */
	@Override
	public TtCounterPartyKind searchPartyIsBank(TtCounterPartyKind ttCounterPartyKind) {
		return counterPartyDao.searchPartyIsBank(ttCounterPartyKind);
	}
	
	/**
	 * 交易对手编号
	 * @param
	 * @return
	 */
	@Override
	public String selectCounterPartyNo() {
		return counterPartyDao.selectCounterPartyNo();
	}
	
	/**
	 * 保存交易对手账户
	 * @param TtCounterPartyAcc
	 * @return
	 */
	@Override
	public void saveCPAcc(TtCounterPartyAcc cpAcc){
		cpAcc.setCny(DictConstants.Currency.CNY);
		cpAcc.setUpdatetime(DateTimeUtil.getLocalDateTime());
		ttCounterPartyAccMapper.insert(cpAcc);
	}
	
	/**
	 * 删除交易对手账户
	 * @param TtCounterPartyAcc
	 * @return
	 */
	@Override
	public void deleteCPAcc(TtCounterPartyAcc cpAcc){
		TtCounterPartyAcc accCheck = ttCounterPartyAccMapper.selectByPrimaryKey(cpAcc);
		if(accCheck != null){
			ttCounterPartyAccMapper.delete(cpAcc);
		}
	}
	
	/**
	 * 查询交易对手账户列表
	 * @param TtCounterPartyAcc
	 * @return
	 */
	@Override
	public List<TtCounterPartyAcc> searchCPAccList(Map<String, Object> map){
		List<TtCounterPartyAcc> accCheck = ttCounterPartyAccMapper.selectCounterPartyAccList(map);
		return accCheck;
	}
	
	/**
	 * 交易对手编号
	 * @param
	 * @return
	 */
	@Override
	public String selectPartyNo(String party_name) {
		return counterPartyDao.selectPartyNo(party_name);
	}

	@Override
	public void importCounterparty(List<Map<String, Object>> counterpartyList) {
		for(Map<String,Object> tradeInfoMap : counterpartyList){
			TtCounterParty ttCounterParty = new TtCounterParty();
			String PARTY_CODE = ParameterUtil.getString(tradeInfoMap, "PARTY_CODE", "");
			JY.require(StringUtils.isNotEmpty(PARTY_CODE), "客户编号不能为空，请维护!!");
			ttCounterParty.setParty_code(PARTY_CODE);
			String PARTY_NAME = ParameterUtil.getString(tradeInfoMap, "PARTY_NAME", "");
			JY.require(StringUtils.isNotEmpty(PARTY_NAME), "客户名称不能为空，请维护!!");
			ttCounterParty.setParty_name(PARTY_NAME);
			ttCounterParty.setBelongarea(ParameterUtil.getString(tradeInfoMap, "BELONGAREA", ""));
			ttCounterParty.setParty_shortname(ParameterUtil.getString(tradeInfoMap, "PARTY_SHORTNAME", ""));
			ttCounterParty.setParty_bigkind(ParameterUtil.getString(tradeInfoMap, "PARTY_BIGKIND", ""));
			ttCounterParty.setParty_smallkind("SL");
			ttCounterParty.setParty_business_license(ParameterUtil.getString(tradeInfoMap, "PARTY_BUSINESS_LICENSE", ""));
			ttCounterParty.setParty_organ_code(ParameterUtil.getString(tradeInfoMap, "PARTY_ORGAN_CODE", ""));
			ttCounterParty.setParty_finance_license(ParameterUtil.getString(tradeInfoMap, "PARTY_FINANCE_LICENSE", ""));
			ttCounterParty.setParty_address(ParameterUtil.getString(tradeInfoMap, "PARTY_ADDRESS", ""));
			ttCounterParty.setParty_postcode(ParameterUtil.getString(tradeInfoMap, "PARTY_ADDRESS", ""));
			ttCounterParty.setIs_active("1");
			ttCounterParty.setCreate_userid("admin");
			ttCounterParty.setCreate_time(DateUtil.getCurrentDateAsString()+" 00:00:00");
			ttCounterParty.setUpdate_userid("admin");
			ttCounterParty.setUpdate_time(DateUtil.getCurrentDateAsString()+" 00:00:00");
			ttCounterParty.setCore_custno(ParameterUtil.getString(tradeInfoMap, "CORE_CUSTNO", ""));
			ttCounterParty.setHeadbank_flag(ParameterUtil.getString(tradeInfoMap, "HEADBANK_FLAG", ""));
			ttCounterParty.setP_party_id(ParameterUtil.getString(tradeInfoMap, "P_PARTY_ID", ""));
			counterPartyDao.insertCounterParty(ttCounterParty);
		}
		
	}

	@Override
	public String selectCounterByName(String partyName) {
		return counterPartyDao.selectCounterByName(partyName);
	}

	@Override
	public void importCounterpartyAcc(List<Map<String, Object>> counterpartyAccList) {
		for(Map<String,Object> tradeInfoMap : counterpartyAccList){
			TtCounterPartyAcc ttCounterPartyAcc = new TtCounterPartyAcc();
			String PARTY_CODE = ParameterUtil.getString(tradeInfoMap, "PARTY_ID", "");
			JY.require(StringUtils.isNotEmpty(PARTY_CODE), "客户编号不能为空，请维护！！");
			String partyId = ttCounterPartyAccMapper.queryPartyId(PARTY_CODE);
			JY.require(StringUtils.isNotEmpty(partyId), "客户编号输入不正确，请维护！！");
			ttCounterPartyAcc.setParty_id(partyId);
			String BANKACCID =  ParameterUtil.getString(tradeInfoMap, "BANKACCID", "");
			JY.require(StringUtils.isNotEmpty(BANKACCID), "银行账号不能为空，请维护！！");
			ttCounterPartyAcc.setBankaccid(BANKACCID);
			ttCounterPartyAcc.setBankaccname(ParameterUtil.getString(tradeInfoMap, "BANKACCNAME", ""));
			String OPEN_BANK_LARGE_ACCNO =  ParameterUtil.getString(tradeInfoMap, "OPEN_BANK_LARGE_ACCNO", "");
			ttCounterPartyAcc.setOpen_bank_large_accno(OPEN_BANK_LARGE_ACCNO);
			String OPEN_BANK_NAME =  ParameterUtil.getString(tradeInfoMap, "OPEN_BANK_NAME", "");
			ttCounterPartyAcc.setOpen_bank_name(OPEN_BANK_NAME);
			String OPEN_DATE =  ParameterUtil.getString(tradeInfoMap, "OPEN_DATE", "");
			JY.require(StringUtils.isNotEmpty(OPEN_DATE), "开户日期不能为空,请维护！！");
			ttCounterPartyAcc.setOpen_date(OPEN_DATE);
			ttCounterPartyAcc.setStatus(ParameterUtil.getString(tradeInfoMap, "STATUS", ""));
			ttCounterPartyAcc.setCny(ParameterUtil.getString(tradeInfoMap, "CNY", ""));
			ttCounterPartyAcc.setCurr_rate(ParameterUtil.getDouble(tradeInfoMap, "CURR_RATE", 0));
			ttCounterPartyAcc.setUpdatetime(DateUtil.getCurrentDateAsString()+" 00:00:00");
			ttCounterPartyAccMapper.insert(ttCounterPartyAcc);
		}		
	}
	@Override
	public List<TempPartyMap> searchTemp(Map<String,Object> params){
		return counterPartyDao.searchTempList(params);
	}

	@Override
	public List<TempPartyMap> searchTempByPartyid(Map<String,Object> param) {
		return counterPartyDao.searchTempByPartyid(param);
	}

	@Override
	public void updateCounterKind(Map<String, Object> param) {
		counterPartyDao.updateCounterKind(param);
		
	}

	@Override
	public void updatePartyConByKey(Map<String, Object> param) {
		counterPartyDao.deleteCounterPartyConByKey(param);
		counterPartyDao.addCounterPartyCon(param);
	}
	
	@Override
	public List<CounterPartyVo> selectCPList(Map<String,String> map) {
		return counterPartyDao.selectCounterParty(map);
	}

	@Override
	public boolean updateCounterParty(TtCounterParty counterParty) {
		counterPartyDao.updateCounterParty(counterParty);
		return true;
	}

	@Override
	public boolean saveTTCounterParty(Map<String, Object> param) {
		int i = counterPartyDao.saveTTCounterParty(param);
		return i > 0 ? true : false;
	}

	@Override
	public List<TtCounterPartyKind> getMidPartyKind(Map<String, Object> map) {
		return counterPartyDao.getMidPartyKind(map);
	}

	@Override
	public List<TtCounterPartyKind> getSmallPartyKind(Map<String, Object> map) {
		return counterPartyDao.getSmallPartyKind(map);
	}

	@Override
	public Map<String, List<CounterPartyKindVo>> getAllCounterPartyKind(Map<String, Object> map) {
		map.put("kind_grade", "0");
		List<CounterPartyKindVo> fistKind = counterPartyDao.getAllPartyKind(map);
		
		map.put("kind_grade", "1");
		List<CounterPartyKindVo> secondKind = counterPartyDao.getAllPartyKind(map);
		
		map.put("kind_grade", "2");
		List<CounterPartyKindVo> thirdKind = counterPartyDao.getAllPartyKind(map);
		
		Map<String, List<CounterPartyKindVo>> kindMap = new HashMap<String, List<CounterPartyKindVo>>();
		CounterPartyKindVo tempkind = null;
		List<CounterPartyKindVo> fistKind_1 = new ArrayList<CounterPartyKindVo>();
		kindMap.put("-1", fistKind_1);//行业大类
		
		for (CounterPartyKindVo kind : fistKind) 
		{
			tempkind = new CounterPartyKindVo();
			tempkind.setId(kind.getId());
			tempkind.setText(kind.getText());
			fistKind_1.add(tempkind);
			
			for (CounterPartyKindVo skind : secondKind) 
			{
				if(kind.getKind_code().substring(0, 1).equals(skind.getKind_code().substring(0, 1))){
					kind.addChild(skind);
				}
			}
			kindMap.put(kind.getKind_code(), kind.getChildren());//行业中类
		}
		
		for (CounterPartyKindVo kind_1 : secondKind) 
		{
			tempkind = new CounterPartyKindVo();
			tempkind.setId(kind_1.getId());
			tempkind.setText(kind_1.getText());
			tempkind.setKind_code(kind_1.getKind_code());
			for (CounterPartyKindVo tkind : thirdKind) 
			{
				if(tempkind.getKind_code().substring(0, 3).equals(tkind.getKind_code().substring(0, 3))){
					tempkind.addChild(tkind);
				}
			}
			kindMap.put(tempkind.getKind_code(), tempkind.getChildren());//行业小类
		}
		
		return kindMap;
	}

	@Override
	public void createAcc(TtCounterPartyAcc ta, String type) {
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createAndOperateAcc(TtCounterPartyAcc ta, String type) {
		
		// TODO Auto-generated method stub
		
	}
	
	
}