package com.singlee.capital.counterparty.model;

import java.io.Serializable;


/***
 * 交易对手 对象
 * 
 * @author LyonChen
 * 
 */
public class TtCounterParty implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String party_id;// 交易对手内部编号
	private String party_code;// 交易对手客户编号
	private String party_name;// 交易对手全称
	private String belongarea;// 所属区域：AH 安徽
	private String zzd_accname;// 中债托管账户名称
	private String zzd_acccode;// 中债托管账号
	private String issuer_flag;// 是否发行人
	private String party_shortname;// 交易对手简称
	private String party_bigkind;// 客户大类
	private String party_midkind;// 客户中类
	private String party_smallkind;// 客户小类
	private String custAccty;// 会计类型
	private String party_business_license;// 交易对手营业执照
	private String party_organ_code;// 交易对手机构代码证
	private String party_finance_license;// 交易对手金融许可证
	private String party_address;// 地址
	private String party_postcode;// 邮编
	private String is_active;// 000031
	private String large_pay_accno;// 大额支付行号
	private String qss_accname;// 清算所托管账户名称
	private String qss_acccode;// 清算所托管账号
	private String belong_inst_id;// 交易对手所属机构
	private String issuer_id;//发行人ID
	private String core_custno;//核心客户号
	
	private String offsite_adm_org_code; //非现场监管统计机构编码
	private String offsite_adm_org_name; //非现场监管统计机构名称
	
	
	private String party_organ_type;//金融机构类型
	private String party_organ_grade;//金融机构评级
	
	
	private String create_userid;
	private String create_time;
	private String update_userid;
	private String update_time;
	private String headbank_flag;
	private String p_party_id;
	private String credit_code_type; //信贷系统证件类型
	private String credit_code;      //信贷系统证件号
	
	private String custType;			//客户类型
	private String idType;				//证件类型
	private String idOwnerCntry;		//证件所属国
	
	
	public String getParty_organ_type() {
		return party_organ_type;
	}

	public void setParty_organ_type(String party_organ_type) {
		this.party_organ_type = party_organ_type;
	}

	public String getParty_organ_grade() {
		return party_organ_grade;
	}

	public void setParty_organ_grade(String party_organ_grade) {
		this.party_organ_grade = party_organ_grade;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdOwnerCntry() {
		return idOwnerCntry;
	}

	public void setIdOwnerCntry(String idOwnerCntry) {
		this.idOwnerCntry = idOwnerCntry;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public TtCounterParty() {
	}

	public String getCore_custno() {
		return core_custno;
	}

	public void setCore_custno(String core_custno) {
		this.core_custno = core_custno;
	}

	public String getCreate_userid() {
		return create_userid;
	}

	public void setCreate_userid(String create_userid) {
		this.create_userid = create_userid;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getUpdate_userid() {
		return update_userid;
	}

	public void setUpdate_userid(String update_userid) {
		this.update_userid = update_userid;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getParty_id() {
		return party_id;
	}

	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}

	public String getParty_code() {
		return party_code;
	}

	public void setParty_code(String party_code) {
		this.party_code = party_code;
	}

	public String getParty_name() {
		return party_name;
	}

	public void setParty_name(String party_name) {
		this.party_name = party_name;
	}

	public String getBelongarea() {
		return belongarea;
	}

	public void setBelongarea(String belongarea) {
		this.belongarea = belongarea;
	}

	public String getZzd_accname() {
		return zzd_accname;
	}

	public void setZzd_accname(String zzd_accname) {
		this.zzd_accname = zzd_accname;
	}

	public String getZzd_acccode() {
		return zzd_acccode;
	}

	public void setZzd_acccode(String zzd_acccode) {
		this.zzd_acccode = zzd_acccode;
	}

	public String getIssuer_flag() {
		return issuer_flag;
	}

	public void setIssuer_flag(String issuer_flag) {
		this.issuer_flag = issuer_flag;
	}

	public String getParty_shortname() {
		return party_shortname;
	}

	public void setParty_shortname(String party_shortname) {
		this.party_shortname = party_shortname;
	}

	public String getParty_bigkind() {
		return party_bigkind;
	}

	public void setParty_bigkind(String party_bigkind) {
		this.party_bigkind = party_bigkind;
	}

	public String getParty_midkind() {
		return party_midkind;
	}

	public void setParty_midkind(String party_midkind) {
		this.party_midkind = party_midkind;
	}

	public String getParty_smallkind() {
		return party_smallkind;
	}

	public void setParty_smallkind(String party_smallkind) {
		this.party_smallkind = party_smallkind;
	}

	public String getParty_business_license() {
		return party_business_license;
	}

	public void setParty_business_license(String party_business_license) {
		this.party_business_license = party_business_license;
	}

	public String getParty_organ_code() {
		return party_organ_code;
	}

	public void setParty_organ_code(String party_organ_code) {
		this.party_organ_code = party_organ_code;
	}

	public String getParty_finance_license() {
		return party_finance_license;
	}

	public void setParty_finance_license(String party_finance_license) {
		this.party_finance_license = party_finance_license;
	}

	public String getParty_address() {
		return party_address;
	}

	public void setParty_address(String party_address) {
		this.party_address = party_address;
	}

	public String getParty_postcode() {
		return party_postcode;
	}

	public void setParty_postcode(String party_postcode) {
		this.party_postcode = party_postcode;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public String getLarge_pay_accno() {
		return large_pay_accno;
	}

	public void setLarge_pay_accno(String large_pay_accno) {
		this.large_pay_accno = large_pay_accno;
	}

	public String getQss_accname() {
		return qss_accname;
	}

	public void setQss_accname(String qss_accname) {
		this.qss_accname = qss_accname;
	}

	public String getQss_acccode() {
		return qss_acccode;
	}

	public void setQss_acccode(String qss_acccode) {
		this.qss_acccode = qss_acccode;
	}

	public String getBelong_inst_id() {
		return belong_inst_id;
	}

	public void setBelong_inst_id(String belong_inst_id) {
		this.belong_inst_id = belong_inst_id;
	}
	

	public String getIssuer_id() {
		return issuer_id;
	}

	public void setIssuer_id(String issuer_id) {
		this.issuer_id = issuer_id;
	}

	public String getOffsite_adm_org_code() {
		return offsite_adm_org_code;
	}

	public void setOffsite_adm_org_code(String offsite_adm_org_code) {
		this.offsite_adm_org_code = offsite_adm_org_code;
	}

	public String getOffsite_adm_org_name() {
		return offsite_adm_org_name;
	}

	public void setOffsite_adm_org_name(String offsite_adm_org_name) {
		this.offsite_adm_org_name = offsite_adm_org_name;
	}
	
	public String getHeadbank_flag() {
		return headbank_flag;
	}

	public void setHeadbank_flag(String headbank_flag) {
		this.headbank_flag = headbank_flag;
	}

	public String getP_party_id() {
		return p_party_id;
	}

	public void setP_party_id(String p_party_id) {
		this.p_party_id = p_party_id;
	}

	public String getCredit_code_type() {
		return credit_code_type;
	}

	public void setCredit_code_type(String credit_code_type) {
		this.credit_code_type = credit_code_type;
	}

	public String getCredit_code() {
		return credit_code;
	}

	public void setCredit_code(String credit_code) {
		this.credit_code = credit_code;
	}

	public String getCustAccty() {
		return custAccty;
	}

	public void setCustAccty(String custAccty) {
		this.custAccty = custAccty;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtCounterParty [party_id=");
		builder.append(party_id);
		builder.append(", party_code=");
		builder.append(party_code);
		builder.append(", party_name=");
		builder.append(party_name);
		builder.append(", belongarea=");
		builder.append(belongarea);
		builder.append(", zzd_accname=");
		builder.append(zzd_accname);
		builder.append(", zzd_acccode=");
		builder.append(zzd_acccode);
		builder.append(", issuer_flag=");
		builder.append(issuer_flag);
		builder.append(", party_shortname=");
		builder.append(party_shortname);
		builder.append(", party_bigkind=");
		builder.append(party_bigkind);
		builder.append(", party_smallkind=");
		builder.append(party_smallkind);
		builder.append(", party_business_license=");
		builder.append(party_business_license);
		builder.append(", party_organ_code=");
		builder.append(party_organ_code);
		builder.append(", party_finance_license=");
		builder.append(party_finance_license);
		builder.append(", party_address=");
		builder.append(party_address);
		builder.append(", party_postcode=");
		builder.append(party_postcode);
		builder.append(", is_active=");
		builder.append(is_active);
		builder.append(", large_pay_accno=");
		builder.append(large_pay_accno);
		builder.append(", qss_accname=");
		builder.append(qss_accname);
		builder.append(", qss_acccode=");
		builder.append(qss_acccode);
		builder.append(", belong_inst_id=");
		builder.append(belong_inst_id);
		builder.append(", issuer_id=");
		builder.append(issuer_id);
		builder.append(", core_custno=");
		builder.append(core_custno);
		builder.append(", credit_code_type=");
		builder.append(credit_code_type);
		builder.append(", credit_code=");
		builder.append(credit_code);
		builder.append(", offsite_adm_org_code=");
		builder.append(offsite_adm_org_code);
		builder.append(", offsite_adm_org_name=");
		builder.append(offsite_adm_org_name);
		builder.append(", create_userid=");
		builder.append(create_userid);
		builder.append(", create_time=");
		builder.append(create_time);
		builder.append(", update_userid=");
		builder.append(update_userid);
		builder.append(", update_time=");
		builder.append(update_time);
		builder.append("]");
		return builder.toString();
	}
}
