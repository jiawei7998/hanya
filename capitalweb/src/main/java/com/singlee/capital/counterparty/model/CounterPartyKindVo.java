package com.singlee.capital.counterparty.model;

import java.util.ArrayList;
import java.util.List;



/**
 * 交易对手 大对象 包含 账户 联系 列表
 * @author LyonChen
 *
 */
public class CounterPartyKindVo extends TtCounterPartyKind{
	

	/** 保存大类 编码 */
	private String bigKind;
	/** 大类名称 */
	private String bigKindName;
	/** 操作信息 */
	private String types;
	
	/** 数据字典ID */
	private String id;
	/** 数据字典text */
	private String text;
	private List<String> childrenText;
	private List<CounterPartyKindVo> children;
	
	public List<CounterPartyKindVo> getChildren() {
		return children;
	}

	public void setChildren(List<CounterPartyKindVo> children) {
		this.children = children;
	}
	
	public void addChild(CounterPartyKindVo kindVo){
		if(this.children == null){
			this.children = new ArrayList<CounterPartyKindVo>();
		}
		this.children.add(kindVo);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public CounterPartyKindVo() {
	}
	
	public String getTypes() {
		return types;
	}
	public void setTypes(String types) {
		this.types = types;
	}

	public String getBigKind() {
		return bigKind;
	}

	public void setBigKind(String bigKind) {
		this.bigKind = bigKind;
	}

	public String getBigKindName() {
		return bigKindName;
	}

	public void setBigKindName(String bigKindName) {
		this.bigKindName = bigKindName;
	}

	public List<String> getChildrenText() {
		return childrenText;
	}

	public void addChildrenText(String childrenText) {
		if(this.childrenText == null){
			this.childrenText = new ArrayList<String>();
		}
		this.childrenText.add(childrenText);
	}
	

}
