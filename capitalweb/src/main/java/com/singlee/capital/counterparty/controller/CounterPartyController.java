package com.singlee.capital.counterparty.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.singlee.capital.acc.model.TtAccOutCash;
import com.singlee.capital.acc.service.AccOutCashService;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.model.TempPartyMap;
import com.singlee.capital.counterparty.model.TtCounterPartyAcc;
import com.singlee.capital.counterparty.model.TtCounterPartyExt;
import com.singlee.capital.counterparty.model.TtCounterPartyKind;
import com.singlee.capital.counterparty.service.CounterPartyService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
 
/**
 * 交易对手 管理控制器 请求前缀 /CounterPartyController/
 * 
 * @author cz
 * 
 */
@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/CounterPartyController", method = RequestMethod.POST)
public class CounterPartyController  extends CommonController {
	
	/** 交易对手服务接口 spring 负责注入 */
	@Autowired
	private CounterPartyService counterPartyService;
	
	/** 参数服务 **/
	@Autowired
	private UserParamService userParamService;
	
	/** 外部资金账户服务 **/
	@Autowired
	private AccOutCashService accOutCashService;
	/**
	 * 查询交易对手 列表
	 * map 包含
	 * big_kind: 大类 (允许为空)
	 * small_kind：小类 （允许为空）
	 * cp_name: 交易对手 名称 (允许为空）
	 * begin_num:起始记录数(不允许为空)
	 * end_num:结束记录数(不允许为空)
	 */
	@RequestMapping(value = "/searchCounterParty")
	@ResponseBody
	public RetMsg<PageInfo<CounterPartyVo>> searchCounterParty(@RequestBody Map<String,Object> map,HttpServletRequest request) throws IOException  {
		TaUser user = SlSessionHelper.getUser();
		map.put("branchId", user.getBranchId());
		Page<CounterPartyVo> partyList = counterPartyService.searchCp(map);
		return RetMsgHelper.ok(partyList);
	}

	/**
	 * 查询交易对手账户列表 有限制条数限制
	 * map:
	 * party_name	为空所有记录
	 * 	limit			限制条数 缺省取20条
	
	@RequestMapping(value = "/listCounterParty")
	@ResponseBody
	public List<TtCounterParty> listCounterParty(HttpEntity<String> request) throws IOException {
		String json = request.getBody();
		@SuppressWarnings({ "unchecked" })
		Map<String,Object> paramMap = JacksonUtil.readJson2Entity(json, HashMap.class);
		paramMap.put("is_active", DictConstants.AccStatus.Enabled.value());
		return (List<TtCounterParty>) counterPartyService.listCp(paramMap);
	} */
	/**
	 * 保存交易对手/通过 types区分是新增/修改
	 * @param CounterPartyVo 对象 
	 */
	@RequestMapping(value = "/saveCounterParty")
	@ResponseBody
	public RetMsg<Serializable> saveCounterParty(@RequestBody Map<String,Object> map) throws IOException  {
 		map.put("create_time", DateUtil.getCurrentDateTimeAsString());
		map.put("update_time", DateUtil.getCurrentDateTimeAsString());
		map.put("create_userid", getThreadLocalUserId());
		map.put("update_userid", getThreadLocalUserId());
		try {
			counterPartyService.saveCp(map);
		} catch (Exception e) {
			RetMsgHelper.ok("保存失败","保存交易对手失败！"+e);
		}
		//counterPartyService.saveCp(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 获得一个交易对手，包含其联系人信息 及 账户信息
	 * map 
	 * @param party_id 指定的交易对手号
	 * @throws IOException 
	 */
	@RequestMapping(value = "/selectCounterParty")
	@ResponseBody
	public CounterPartyVo selectCounterParty(@RequestBody Map<?,?> map) throws IOException  {
		String party_id = ParameterUtil.getString(map,"party_id","");
		return counterPartyService.selectCP(party_id, null);
	}
	
	@RequestMapping(value = "/selectCounterPartyExt")
	@ResponseBody
	public TtCounterPartyExt selectCounterPartyExt(@RequestBody Map<String,Object> map){
		return counterPartyService.selectCounterPartyExt(map);
	}
	/**
	 * 删除一个交易对手，包含其联系人信息 及 账户信息
	 * map 
	 * @param party_id 指定的交易对手号
	 * @throws IOException 
	 */
	
	@RequestMapping(value = "/removeCounterParty")
	@ResponseBody
	public void removeCounterParty(@RequestBody HashMap<String, Object> map) throws IOException  {
		List<String> party_ids = (List<String>) map.get("party_ids");
		for(String party_id : party_ids){
			counterPartyService.removeCP(party_id);
		}
		
	}
	
	/**
	 * 重新激活/停用 某个交易对手
	 * @param party_id 指定的交易对手号
	 * @throws IOException 
	 */
	@RequestMapping(value = "/activeCounterParty/{party_id}")
	@ResponseBody
	public void activeCounterParty(@PathVariable String  party_id) throws IOException  {
		counterPartyService.activeCp(party_id,
				getThreadLocalUserId(),
				DateUtil.getCurrentDateTimeAsString());
	}
	
	
	
	
	/**
	 * *****************************************交易对手 分类 部分****************************************
	 */
	
	/**
	 * 查询交易对手 分类 列表
	 * map 包含
	 * big_kind: 大类 (允许为空)
	 * kind_name: 交易对手 名称 (允许为空）
	 * begin_num:起始记录数(不允许为空)
	 * end_num:结束记录数(不允许为空)
	 */
	@RequestMapping(value = "/searchCounterPartyKind")
	@ResponseBody
	public RetMsg<PageInfo<CounterPartyKindVo>> searchCounterPartyKind(@RequestBody Map<String,Object> map) throws IOException  {
		Page<CounterPartyKindVo> partyKindList = counterPartyService.searchCpKind(map);
		return  RetMsgHelper.ok(partyKindList);
	}
	
	
	/**
	 * 删除一个交易对手分类
	 * map 
	 * @param party_id 指定的交易对手号
	 * @throws IOException 
	 */
	@RequestMapping(value = "/removeCounterPartyKind/{kind}")
	@ResponseBody
	public RetMsg<Serializable> removeCounterPartyKind(@PathVariable String  kind) throws IOException  {
		counterPartyService.removeCPKind(kind);
		return RetMsgHelper.ok();
	}

	/**
	 * 保存交易对手分类/通过 types区分是新增/修改
	 * @param CounterPartyVo 对象 
	 */
	@RequestMapping(value = "/saveCounterPartyKind")
	@ResponseBody
	public CounterPartyKindVo saveCounterPartyKind(@RequestBody CounterPartyKindVo incomingInst) throws IOException  {
		counterPartyService.saveCpKind(incomingInst);
		return incomingInst;
	}
//	/**
//	 * 删除交易对手分类
//	 * @param CounterPartyVo 对象 
//	 */
//	@RequestMapping(value = "/deleteCounterPartyKind")
//	@ResponseBody
//	public ErrorMsg deleteCounterPartyKind(HttpEntity<String> request) throws IOException  {
//		String json = request.getBody();
//		CounterPartyKindVo incomingInst = JacksonUtil.readJson2Entity(json, CounterPartyKindVo.class);
//		counterPartyService.removeCPKind(incomingInst.getKind_code());
//		return Constants.eMsgOK;
//	}	

	
	/**
	 * 查询账户列表
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchCPAccList")
	public RetMsg<List<TtCounterPartyAcc>> searchCPAccList(@RequestBody Map<String,Object> params) {
		List<TtCounterPartyAcc> list = null;
		String partyId = ParameterUtil.getString(params, "partyId", "");
		if(!StringUtil.isNullOrEmpty(partyId)){
			CounterPartyVo cp = counterPartyService.selectCP(partyId, null);
			String companyName = userParamService.getSysParamByName("system.companyName", "");
			if(cp.getParty_name().equals(companyName)){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("acctype", DictConstants.AccOutCashType.Settle);
				List<TtAccOutCash> accOutCashs = accOutCashService.getAccList(map);
				if(accOutCashs != null && accOutCashs.size() > 0){
					list = new ArrayList<TtCounterPartyAcc>();
					for(TtAccOutCash accOutCash : accOutCashs){
						TtCounterPartyAcc cPartyAcc = new TtCounterPartyAcc();
						cPartyAcc.setBankaccid(accOutCash.getBankacc());
						cPartyAcc.setBankaccname(accOutCash.getAccname());
						cPartyAcc.setOpen_bank_large_accno(accOutCash.getBankLargeCode());
						cPartyAcc.setOpen_bank_name(accOutCash.getBankName());
						//暂时将accid放到bankaccold字段上
						cPartyAcc.setBankaccidOld(accOutCash.getAccid());
						list.add(cPartyAcc);
					}
				}
			}
			else{
				list = counterPartyService.searchCPAccList(params);
			}
		}
		else{
			list = counterPartyService.searchCPAccList(params);
		}
		return RetMsgHelper.ok(list);
	}
	
	
	/**
	 * 查询交易对手模板编号
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTemp")
	public RetMsg<List<TempPartyMap>> searchTemp(@RequestBody Map<String,Object> params) {
		List<TempPartyMap> list = counterPartyService.searchTemp(params);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 根据交易对手编号查询交易对手模板列表
	 * 
	 * @param map
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTempByPartyid")
	public RetMsg<List<TempPartyMap>> searchTempByPartyid(@RequestBody Map<String,Object> param) {
		List<TempPartyMap> list = counterPartyService.searchTempByPartyid(param);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获得一个交易对手，包含其联系人信息 及 账户信息
	 * map 
	 * @param party_id 指定的交易对手号
	 * @throws IOException 
	 */
	@RequestMapping(value = "/selectCounterPartyList")
	@ResponseBody
	public List<CounterPartyVo> selectCounterPartyList(@RequestBody Map<String,String> map) throws IOException  {
		return counterPartyService.selectCPList(map);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/saveTtcounterParty")
	public RetMsg<Boolean> saveTtcounterParty(@RequestBody Map<String,Object> param){
		Map<String, String> map = new HashMap<String, String>();
		map.put("party_id", String.valueOf(param.get("party_id")));
		CounterPartyVo counterParty = counterPartyService.selectCPByMap(map);
		boolean f = false;
		if(counterParty == null){
			f = counterPartyService.saveTTCounterParty(param);
			
		}else{
			counterParty.setParty_name(ParameterUtil.getString(param, "party_name", null));
			counterParty.setCustType(ParameterUtil.getString(param, "custType", null));
			f = counterPartyService.updateCounterParty(counterParty);
		}
		return RetMsgHelper.ok(f);
	}
	
	/**
	 * 获取二级类别
	 * @param map  前一位字母
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getMidPartyKind")
	public RetMsg<List<TtCounterPartyKind>> getSecondMT0001(@RequestBody Map<String,Object> map){
		List<TtCounterPartyKind> list = counterPartyService.getMidPartyKind(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获取三级类别
	 * @param map  前三位字母
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSmallPartyKind")
	public RetMsg<List<TtCounterPartyKind>> getThirdMT0001(@RequestBody Map<String,Object> map){
		List<TtCounterPartyKind> list = counterPartyService.getSmallPartyKind(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 获取三级类别
	 * @param map  前三位字母
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllCounterPartyKind")
	public RetMsg<Map<String, List<CounterPartyKindVo>>>  getAllCounterPartyKind(@RequestBody Map<String,Object> param){
		Map<String, List<CounterPartyKindVo>> map = null;
		try {
			map = counterPartyService.getAllCounterPartyKind(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RetMsgHelper.ok(map);
	}
	

}