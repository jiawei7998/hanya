package com.singlee.capital.counterparty.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

/**
 * 交易对手 银行账户 对象
 * 
 * @author LyonChen
 * 
 */
@Entity
@Table(name = "TT_TRD_COUNTERPARTYACC")
@NameStyle(Style.lowercase)
public class TtCounterPartyAcc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String party_id;// 交易对手编号
	@Id
	private String bankaccid;// 银行账号
	private String bankaccname;// 银行账户名称
	private String status;// 0：已启用, 1：已停用
	private String cny;// 币种
	private double curr_rate;// 活期利率
	private String open_bank_large_accno;// 开户行大额支付行号
	private String open_date;// 开户日期
	private String updatetime;// 上次更新时间 YYYY-MM-DD HH:MM:SS
	private String open_bank_name;// 开户行名称
	
	@Transient
	private String bankaccidOld;// 银行账号(原)
	
	
	
	public String getParty_id() {
		return party_id;
	}

	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}

	public String getBankaccid() {
		return bankaccid;
	}

	public void setBankaccid(String bankaccid) {
		this.bankaccid = bankaccid;
	}

	public String getBankaccname() {
		return bankaccname;
	}

	public void setBankaccname(String bankaccname) {
		this.bankaccname = bankaccname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCny() {
		return cny;
	}

	public void setCny(String cny) {
		this.cny = cny;
	}

	public double getCurr_rate() {
		return curr_rate;
	}

	public void setCurr_rate(double curr_rate) {
		this.curr_rate = curr_rate;
	}

	public String getOpen_bank_large_accno() {
		return open_bank_large_accno;
	}

	public void setOpen_bank_large_accno(String open_bank_large_accno) {
		this.open_bank_large_accno = open_bank_large_accno;
	}

	public String getOpen_date() {
		return open_date;
	}

	public void setOpen_date(String open_date) {
		this.open_date = open_date;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getOpen_bank_name() {
		return open_bank_name;
	}

	public void setOpen_bank_name(String open_bank_name) {
		this.open_bank_name = open_bank_name;
	}

	public String getBankaccidOld() {
		return bankaccidOld;
	}

	public void setBankaccidOld(String bankaccidOld) {
		this.bankaccidOld = bankaccidOld;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtCounterPartyAcc [party_id=");
		builder.append(party_id);
		builder.append(", bankaccid=");
		builder.append(bankaccid);
		builder.append(", bankaccname=");
		builder.append(bankaccname);
		builder.append(", status=");
		builder.append(status);
		builder.append(", cny=");
		builder.append(cny);
		builder.append(", curr_rate=");
		builder.append(curr_rate);
		builder.append(", open_bank_large_accno=");
		builder.append(open_bank_large_accno);
		builder.append(", open_date=");
		builder.append(open_date);
		builder.append(", updatetime=");
		builder.append(updatetime);
		builder.append(", open_bank_name=");
		builder.append(open_bank_name);
		builder.append("]");
		return builder.toString();
	}

}
