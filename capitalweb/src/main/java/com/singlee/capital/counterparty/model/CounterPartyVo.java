package com.singlee.capital.counterparty.model;

import java.util.List;


/**
 * 交易对手 大对象 包含 账户 联系 列表
 * @author LyonChen
 *
 */
public class CounterPartyVo extends TtCounterParty{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String party_bigkind_name;// 客户大类
	private String party_midkind_name;// 客户中类
	private String party_smallkind_name;// 客户小类
	
	//add edit
	private String types;
	
	//银行账户
	private List<TtCounterPartyAcc> account_list;
	//联系人
	private List<TtCounterPartyCon> constact_list;

	private String create_username;
	private String update_username;

	private String p_party_name;
	private String issuer_name;
	
	public String getCreate_username() {
		return create_username;
	}

	public void setCreate_username(String create_username) {
		this.create_username = create_username;
	}

	public String getUpdate_username() {
		return update_username;
	}

	public void setUpdate_username(String update_username) {
		this.update_username = update_username;
	}

	public CounterPartyVo() {
	}
	
	public String getParty_bigkind_name() {
		return party_bigkind_name;
	}
	public void setParty_bigkind_name(String party_bigkind_name) {
		this.party_bigkind_name = party_bigkind_name;
	}
	public String getParty_smallkind_name() {
		return party_smallkind_name;
	}
	public void setParty_smallkind_name(String party_smallkind_name) {
		this.party_smallkind_name = party_smallkind_name;
	}
	public String getTypes() {
		return types;
	}
	public void setTypes(String types) {
		this.types = types;
	}
	public List<TtCounterPartyAcc> getAccount_list() {
		return account_list;
	}
	public void setAccount_list(List<TtCounterPartyAcc> account_list) {
		this.account_list = account_list;
	}
	public List<TtCounterPartyCon> getConstact_list() {
		return constact_list;
	}
	public void setConstact_list(List<TtCounterPartyCon> constact_list) {
		this.constact_list = constact_list;
	}

	public String getP_party_name() {
		return p_party_name;
	}

	public void setP_party_name(String p_party_name) {
		this.p_party_name = p_party_name;
	}

	public String getIssuer_name() {
		return issuer_name;
	}

	public void setIssuer_name(String issuer_name) {
		this.issuer_name = issuer_name;
	}

	public String getParty_midkind_name() {
		return party_midkind_name;
	}

	public void setParty_midkind_name(String party_midkind_name) {
		this.party_midkind_name = party_midkind_name;
	}
	
}
