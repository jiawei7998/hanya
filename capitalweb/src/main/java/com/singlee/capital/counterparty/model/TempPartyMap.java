package com.singlee.capital.counterparty.model;

import java.io.Serializable;

public class TempPartyMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 交易对手客户编号
	 */
	private String party_id;
	/**
	 * 模板编号
	 */
	private String temp_code;
	/**
	 * 模板类型
	 */
	private String temp_type;
	/**
	 * 模板名称
	 */
	private String template_name;
	
	public String getTemplate_name() {
		return template_name;
	}
	public void setTemplate_name(String templateName) {
		template_name = templateName;
	}
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String partyId) {
		party_id = partyId;
	}
	public String getTemp_code() {
		return temp_code;
	}
	public void setTemp_code(String tempCode) {
		temp_code = tempCode;
	}
	public String getTemp_type() {
		return temp_type;
	}
	public void setTemp_type(String tempType) {
		temp_type = tempType;
	}

}