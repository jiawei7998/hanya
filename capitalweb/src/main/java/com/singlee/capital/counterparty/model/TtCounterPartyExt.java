package com.singlee.capital.counterparty.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 查询核心客户号响应数据包
 * 
 * @author Libo
 * 
 */
@Entity
@Table(name = "TT_TRD_COUNTERPARTY_EXT")
public class TtCounterPartyExt implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 客户号
	 * 
	 */
	@Id
	private String partyId;
    /**
     * 客户全称（英）
     */
    private String partyNameE;
    /**
     * 简称（英）
     */
    private String partyShortnameE;
    /**
     * 国税登记号
     */
    private String nationTaxId;
    /**
     * 地方税务证号
     */
    private String localTaxId;
    /**
     * 开户许可证编号
     */
    private String openAllowId;
    /**
     * 机构信用代码证编码
     */
    private String creditId;
    /**
     * 金融机构编码_国
     */
    private String bankNo;
    /**
     * 金融机构分类代码
     */
    private String fiCodeType;
    /**
     * 金融机构发证国家
     */
    private String fiIssCountry;
    /**
     * 注册资本币种
     */
    private String capitalCcy;
    /**
     * 注册资本
     */
    private double capitalRegist;
    /**
     * 实收资本
     */
    private double capitalAccual;
    /**
     * 证券业许可证/资格证书编号
     */
    private String licenseNo;
    /**
     * 注册地址
     */
    private String registerAddress;
    /**
     * 公司成立日期
     */
    private String incorDate;
    /**
     * 是否上市企业 Y：是  N：否

     */
    private String isList;
    /**
     * 上市地
     */
    private String listMarket;
    /**
     * 境内境外       I境内客户   O境外客户
     */
    private String inlandOffshore;
    /**
     * 经营范围
     */
    private String businessScope;
    /**
     * 新国标行业分类
     */
    private String gbCate;
    /**
     * 法定代表人姓名
     */
    private String repName;
    /**
     * 法定代表人证件类型   见ECIF标准代码文档 ‘证件类型’
     */
    private String repDocType;
    /**
     * 法定代表人证件号码
     */
    private String repDocId;
    /**
     * 企业规模_国内 2大型企业 3中型企业 4小型企业 5微型企业 9其他

     */
    private String companySize;
    /**
     * 员工数
     */
    private double empNum;



	public String getPartyNameE() {
		return partyNameE;
	}

	public void setPartyNameE(String partyNameE) {
		this.partyNameE = partyNameE;
	}

	public String getPartyShortnameE() {
		return partyShortnameE;
	}

	public void setPartyShortnameE(String partyShortnameE) {
		this.partyShortnameE = partyShortnameE;
	}

	public String getNationTaxId() {
		return nationTaxId;
	}

	public void setNationTaxId(String nationTaxId) {
		this.nationTaxId = nationTaxId;
	}

	public String getLocalTaxId() {
		return localTaxId;
	}

	public void setLocalTaxId(String localTaxId) {
		this.localTaxId = localTaxId;
	}

	public String getOpenAllowId() {
		return openAllowId;
	}

	public void setOpenAllowId(String openAllowId) {
		this.openAllowId = openAllowId;
	}

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public String getFiIssCountry() {
		return fiIssCountry;
	}

	public void setFiIssCountry(String fiIssCountry) {
		this.fiIssCountry = fiIssCountry;
	}

	public String getCapitalCcy() {
		return capitalCcy;
	}

	public void setCapitalCcy(String capitalCcy) {
		this.capitalCcy = capitalCcy;
	}
	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getIncorDate() {
		return incorDate;
	}

	public void setIncorDate(String incorDate) {
		this.incorDate = incorDate;
	}

	public String getIsList() {
		return isList;
	}

	public void setIsList(String isList) {
		this.isList = isList;
	}

	public String getListMarket() {
		return listMarket;
	}

	public void setListMarket(String listMarket) {
		this.listMarket = listMarket;
	}

	public String getInlandOffshore() {
		return inlandOffshore;
	}

	public void setInlandOffshore(String inlandOffshore) {
		this.inlandOffshore = inlandOffshore;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getGbCate() {
		return gbCate;
	}

	public void setGbCate(String gbCate) {
		this.gbCate = gbCate;
	}

	public String getRepName() {
		return repName;
	}

	public void setRepName(String repName) {
		this.repName = repName;
	}

	public String getRepDocType() {
		return repDocType;
	}

	public void setRepDocType(String repDocType) {
		this.repDocType = repDocType;
	}

	public String getRepDocId() {
		return repDocId;
	}

	public void setRepDocId(String repDocId) {
		this.repDocId = repDocId;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public double getEmpNum() {
		return empNum;
	}

	public void setEmpNum(double empNum) {
		this.empNum = empNum;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getBankNo() {
		return bankNo;
	}

	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}

	public String getFiCodeType() {
		return fiCodeType;
	}

	public void setFiCodeType(String fiCodeType) {
		this.fiCodeType = fiCodeType;
	}

	public double getCapitalRegist() {
		return capitalRegist;
	}

	public void setCapitalRegist(double capitalRegist) {
		this.capitalRegist = capitalRegist;
	}

	public double getCapitalAccual() {
		return capitalAccual;
	}

	public void setCapitalAccual(double capitalAccual) {
		this.capitalAccual = capitalAccual;
	}

	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}

}
