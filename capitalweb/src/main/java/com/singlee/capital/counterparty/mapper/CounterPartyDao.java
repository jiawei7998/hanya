package com.singlee.capital.counterparty.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.model.TempPartyMap;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.counterparty.model.TtCounterPartyKind;

/**
 * 交易对手 dao 接口定义
 * @author LyonChen
 *
 */
public interface CounterPartyDao extends Mapper<TtCounterPartyKind> {
	
	/** 获得 交易对手 列表 */
	Page<CounterPartyVo> searchCounterParty(Map<String, Object> map, RowBounds rowbounds);
	/** 获得 交易对手 数量 */
	int countCounterParty(Map<String, Object> map);
	/** 交易对手 限制条数 的列表 */
	List<TtCounterParty> listCounterParty(Map<String, Object> map);
	/**
	 *  获得某个交易对手
	 *  map->party_id 交易对手 id
	 *  map->party_code交易对手code 
	 */
	List<CounterPartyVo> selectCounterParty(Map<String, String> map);

	/** 
	 * 获得 交易对手 数量 
	 *  map->name 交易对手全称
	 *  map->shortname 交易对手全称
	 *  map->code交易对手code  
	 */
	int checkCounterParty(Map<String, Object> map);
	
	/**插入 交易对手 对象 */
	void insertCounterParty(TtCounterParty cp);
	/**获得 交易对手 编号  */
	String selectCounterPartyNo();
	/** 更新交易对手 对象*/
	void updateCounterParty(TtCounterParty cp);
	/** 更新交易对手启用标志*/
	void updateCounterPartyActive(TtCounterParty cp);
	/** 删除某个交易对手 */
	void deleteCounterParty(String party_id);
	
	/** 批量插入交易对手联系人 对象*/
	void insertBatchCounterPartyCon(CounterPartyVo cp);
	/** 删除交易对手的 所有 联系人*/
	void deleteCounterPartyCon(String cpid);
	
	/** 批量插入交易对手 账户 对象*/
	void insertBatchCounterPartyAcc(CounterPartyVo cp);
	/** 删除交易对手的 所有 账户*/
	void deleteCounterPartyAcc(String cpid);
	/** **********************************下拉列表 combobox 使用 ********************************/
	/** 获得大类列表 */
	List<TtCounterPartyKind> selectCounterPartyBigKind();
	/** 根据大类列表获得小类列表 */
	List<TtCounterPartyKind> selectCounterPartySmallKind(List<String> bigKindList);
	/** **********************************分类维护 使用 ********************************/
	/** 获得分类列表 */
	Page<CounterPartyKindVo> searchCounterPartyKind(Map<String, Object> map,RowBounds rowBounds);
	/** 获得分类数目 */
	int countCounterPartyKind(Map<String, Object> map);
	/** 更新分类 */
	void updateCounterPartyKind(TtCounterPartyKind kind);
	/** 插入分类 */
	void insertCounterPartyKind(TtCounterPartyKind kind );
	/** 插入分类与大类的映射 */
	void insertCounterPartyKindMap(CounterPartyKindVo map);
	/** 删除分类 */
	void deleteCounterPartyKind(String kind);
	/** 删除分类与大类的映射 */
	void deleteCounterPartyKindMap(String kind);
	/** 检查分类 是否已经有交易对手存在 */
	int checkCounterPartyKind(String kind);
	/** 根据类型名查询类型信息 **/
	List<TtCounterPartyKind> selectCounterPartyKindByName(@Param(value = "kind_name")String kindName,@Param(value = "kind_grade")String kindGrade);
//	/**查询交易对手授信额度**/
//	List<CreditVo> getCreditList(Map<String, Object> param);
	int countCreditList(Map<String, Object> param);
	/** 修改小类与大类的映射 */
	void updateCounterPartyKindMap(CounterPartyKindVo cp);
	//根据交易对手大类来判断交易对手是否非银
	TtCounterPartyKind searchPartyIsBank(TtCounterPartyKind ttCounterPartyKind);
	/** 校验营业执照或核心客户号是否重复*/
	int queryCounterPartyCon(CounterPartyVo cp);
	/**获得 交易对手 编号  */
	String selectPartyNo(String party_name);
	/**获取 客户类型编号 */
	String selectCounterByName(String party_name);
	
	/** 获取交易对手大类 **/
	TtCounterPartyKind selectCounterPartyKind(Map<String,Object> map);
	/**
	 * 查询交易对手打印模板编号
	 */
	public List<TempPartyMap> searchTempList(Map<String,Object> params);
	/**
	 * 交易对手模板保存
	 */
	public void insertTempParty(TempPartyMap tempPartyMap);
	/**
	 * 根据交易对手编号查询交易对手模板列表
	 */
	public List<TempPartyMap> searchTempByPartyid(Map<String,Object> param);
	
	
	/** 更新行业大类 种类 小类 客户类型  会计类型*/
	void updateCounterKind(Map<String,Object> param);
	
	public void deleteCounterPartyConByKey(Map<String, Object> param);
	
	public void addCounterPartyCon(Map<String, Object> param);
	
	int saveTTCounterParty(Map<String, Object> param);
	
	List<TtCounterPartyKind> getMidPartyKind(Map<String, Object> map);
	
	List<TtCounterPartyKind> getSmallPartyKind(Map<String, Object> map);
	
	List<CounterPartyKindVo> getAllPartyKind(Map<String, Object> map);
}

