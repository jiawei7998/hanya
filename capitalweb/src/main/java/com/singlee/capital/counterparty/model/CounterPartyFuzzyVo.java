package com.singlee.capital.counterparty.model;

import java.util.List;

/**
 * 交易对手（客户）模糊查询对象
 * @author gm
 *
 */
public class CounterPartyFuzzyVo {
	private String party_id;// 交易对手内部编号
	private String party_code;// 交易对手客户编号
	private String party_shortname;// 交易对手简称
	private String zzd_accname;//中债托管账户名称
	private String zzd_acccode;//中债托管账号
	private List<CounterPartyAccFuzzyVo> acclist;//关联账户列表
	
	public String getZzd_accname() {
		return zzd_accname;
	}
	public void setZzd_accname(String zzd_accname) {
		this.zzd_accname = zzd_accname;
	}
	public String getZzd_acccode() {
		return zzd_acccode;
	}
	public void setZzd_acccode(String zzd_acccode) {
		this.zzd_acccode = zzd_acccode;
	}
	public List<CounterPartyAccFuzzyVo> getAcclist() {
		return acclist;
	}
	public void setAcclist(List<CounterPartyAccFuzzyVo> acclist) {
		this.acclist = acclist;
	}
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
	public String getParty_code() {
		return party_code;
	}
	public void setParty_code(String party_code) {
		this.party_code = party_code;
	}
	public String getParty_shortname() {
		return party_shortname;
	}
	public void setParty_shortname(String party_shortname) {
		this.party_shortname = party_shortname;
	}
	
}
