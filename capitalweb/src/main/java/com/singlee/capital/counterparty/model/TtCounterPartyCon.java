package com.singlee.capital.counterparty.model;

import java.io.Serializable;


/**
 * 交易对手 联系人
 * @author LyonChen
 *
 */
public class TtCounterPartyCon implements Serializable{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String party_id;// 交易对手编号
	private String name;// 联系人姓名
	private String position;// 职位
	private String tel;// 固定电话
	private String cellphone;// 手机
	private String email;// 邮箱
	private String fax;// 传真
	private String im;// 即时通讯
	private String address;// 地址
	private String postcode;// 邮编
	private String updatetime;// 更新时间
	 
	 
	public TtCounterPartyCon() {
	}
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getCellphone() {
		return cellphone;
	}
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getIm() {
		return im;
	}
	public void setIm(String im) {
		this.im = im;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtCounterPartyCon [party_id=");
		builder.append(party_id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", position=");
		builder.append(position);
		builder.append(", tel=");
		builder.append(tel);
		builder.append(", cellphone=");
		builder.append(cellphone);
		builder.append(", email=");
		builder.append(email);
		builder.append(", fax=");
		builder.append(fax);
		builder.append(", im=");
		builder.append(im);
		builder.append(", address=");
		builder.append(address);
		builder.append(", postcode=");
		builder.append(postcode);
		builder.append(", updatetime=");
		builder.append(updatetime);
		builder.append("]");
		return builder.toString();
	}

	 
	 
}
