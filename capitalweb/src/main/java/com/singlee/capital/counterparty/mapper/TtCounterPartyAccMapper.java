package com.singlee.capital.counterparty.mapper;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.singlee.capital.counterparty.model.TtCounterPartyAcc;


public interface TtCounterPartyAccMapper extends Mapper<TtCounterPartyAcc> {
	
	/**
	 * 根据条件查询
	 * 
	 * @param 
	 */
	public TtCounterPartyAcc selectCounterPartyAcc(Map<String, Object> params);
	
	public List<TtCounterPartyAcc> selectCounterPartyAccList(Map<String, Object> params);

	public void updateCounterPartyAcc(TtCounterPartyAcc ttCounterPartyAcc);
	
	public TtCounterPartyAcc query(String P_PARTY_ID);
	
	public String queryPartyId(String PARTY_CODE);
	
	public List<TtCounterPartyAcc> queryId(String P_PARTY_ID);
}