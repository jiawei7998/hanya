package com.singlee.capital.counterparty.model;
/**
 * 交易对手关联账户列表
 * @author gm
 *
 */
public class CounterPartyAccFuzzyVo {

	private String ac_part_id;//交易对手编号
	private String bankaccid;//银行账号
	private String bankaccname;//银行账户名称
	private String open_bank_large_accno;//开户行大额支付行号
	private String open_bank_name;//开户行名称
	
	public String getAc_part_id() {
		return ac_part_id;
	}
	public void setAc_part_id(String ac_part_id) {
		this.ac_part_id = ac_part_id;
	}
	public String getBankaccid() {
		return bankaccid;
	}
	public void setBankaccid(String bankaccid) {
		this.bankaccid = bankaccid;
	}
	public String getBankaccname() {
		return bankaccname;
	}
	public void setBankaccname(String bankaccname) {
		this.bankaccname = bankaccname;
	}
	public String getOpen_bank_large_accno() {
		return open_bank_large_accno;
	}
	public void setOpen_bank_large_accno(String open_bank_large_accno) {
		this.open_bank_large_accno = open_bank_large_accno;
	}
	public String getOpen_bank_name() {
		return open_bank_name;
	}
	public void setOpen_bank_name(String open_bank_name) {
		this.open_bank_name = open_bank_name;
	}
	
}
