package com.singlee.capital.counterparty.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.singlee.capital.counterparty.model.CounterPartyKindVo;
import com.singlee.capital.counterparty.model.CounterPartyVo;
import com.singlee.capital.counterparty.model.TempPartyMap;
import com.singlee.capital.counterparty.model.TtCounterParty;
import com.singlee.capital.counterparty.model.TtCounterPartyAcc;
import com.singlee.capital.counterparty.model.TtCounterPartyExt;
import com.singlee.capital.counterparty.model.TtCounterPartyKind;



/**
 * 交易对手 服务
 * @author LyonChen
 *
 */
public interface CounterPartyService {

	/**
	 * 查询交易对手 分页数据 
	 * big_kind: 大类 (允许为空)
	 * small_kind：小类 （允许为空）
	 * cp_name: 交易对手 名称 (允许为空）
	 * begin_num:起始记录数(不允许为空)
	 * end_num:结束记录数(不允许为空)
	 */
	Page<CounterPartyVo> searchCp(Map<String,Object> map);
	
	/**
	 * 交易对手 分页数据 
	 * @param cp_name	交易对手名称模糊查询
	 * @param limit 限制条数
	 * @return
	 */
	List<TtCounterParty> listCp(Map<String,Object> map);

	/** 保存交易对手  */
	void saveCp(Map<String,Object> map);	

	/** 激活 停用 交易对手  */
	void activeCp(String party_id, String user_id, String time);

	/** 
	 * 获得某个交易对手
	 *  party_id 交易对手 id
	 *  party_code交易对手code 
	 */
	CounterPartyVo selectCP(String party_id, String party_code);
	/**
	 * 根据party_id获取ext表信息
	 * @param map
	 * @return
	 */
	TtCounterPartyExt selectCounterPartyExt(Map<String,Object> map);

	/** 选中 交易对手  根据交易对手简称 */
	CounterPartyVo selectCPByShortName(String party_shortname);
	/** 
	 * 获得某个交易对手
	 *  issuer_id 发行人id
	 */
	CounterPartyVo selectCPByIssuerId(String issuer_id);
	/** 
	 * 获得某个交易对手
	 *  map
	 */
	CounterPartyVo selectCPByMap(Map<String, String> param);
	
	/** 删除某个交易对手*/
	void removeCP(String party_id);
	
	/** 查询交易对手分类 */
	public Page<CounterPartyKindVo> searchCpKind(Map<String,Object> params);
	
	/** 删除分类  分类与大类的映射 */
	void removeCPKind(String kind);
	
	/** 保存分类 （必须是小类） */
	void saveCpKind(CounterPartyKindVo cpkind);
	
	/** 根据分类查找对应的交易对手列表 */
	public List<TtCounterParty> getCpByKind(List<String> bigKindList,List<String> smallKindList);
	public List<TtCounterPartyKind> getCpKindByKindName(String kindName,String kindType);
	/**
	 * 获取总行机构
	 * @param party_smallKind
	 * @return
	 */
	CounterPartyVo selectHeadBank(String party_smallKind);
	
	/**
	 * 根据交易对手大类来判断交易对手是否非银
	 * @param ttCounterPartyKind
	 * @return
	 */
	TtCounterPartyKind searchPartyIsBank(TtCounterPartyKind ttCounterPartyKind);
	
	/**
	 * 获取交易对手编号
	 * @param 
	 * @return
	 */
	public String selectCounterPartyNo();

	void createAcc(TtCounterPartyAcc ta, String type);

	void createAndOperateAcc(TtCounterPartyAcc ta, String type);

	void saveCPAcc(TtCounterPartyAcc cpAcc);

	void deleteCPAcc(TtCounterPartyAcc cpAcc);

	List<TtCounterPartyAcc> searchCPAccList(Map<String, Object> map);
	
	/**
	 * 获取交易对手编号
	 * @param 
	 * @return
	 */
	public String selectPartyNo(String party_name);
	
	public void importCounterparty(List<Map<String, Object>> counterpartyList);
	
	/**
	 * 获取客户类型ID
	 * 
	 */
	public String selectCounterByName(String partyName);
	
	public void importCounterpartyAcc(List<Map<String, Object>> counterpartyAccList);
	
	/*
	 * 查询交易对手打印模板编号
	 */
	public List<TempPartyMap> searchTemp(Map<String,Object> params);
	
	/*
	 * 根据交易对手编号查询交易对手模板列表
	 */
	public List<TempPartyMap> searchTempByPartyid(Map<String,Object> param);
	
	void updateCounterKind(Map<String, Object> param);
	
	void updatePartyConByKey(Map<String, Object> param);
	
	List<CounterPartyVo> selectCPList(Map<String,String> map);
	
	boolean updateCounterParty(TtCounterParty counterParty);
	
	public boolean saveTTCounterParty(Map<String, Object> param);

	public List<TtCounterPartyKind> getMidPartyKind(Map<String, Object> map);

	public List<TtCounterPartyKind> getSmallPartyKind(Map<String, Object> map);
	
	public Map<String, List<CounterPartyKindVo>> getAllCounterPartyKind(Map<String, Object> map) throws Exception;
}
