package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsApprovermbDepositMapper;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 同业存款
 */
@Service("IfsPrintIbdServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintIbdServiceImpl implements IfsPrintService {

    @Autowired
    IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsApprovermbDeposit deposit = ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(id);
        if (deposit != null) {
            mapEnd.put("sponInst", deposit.getInstName());
            mapEnd.put("sponsor", deposit.getUserName());
            mapEnd.put("counterpartyInstId", deposit.getCounterpartyInstId());
            mapEnd.put("cname", deposit.getCname());
            mapEnd.put("rate", deposit.getRate().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("accuredInterest", deposit.getAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstSettlementDate", deposit.getFirstSettlementDate());
            mapEnd.put("secondSettlementDate", deposit.getSecondSettlementDate());
            mapEnd.put("tenor", deposit.getTenor());
            mapEnd.put("occupancyDays", deposit.getOccupancyDays());
            mapEnd.put("amt", deposit.getAmt().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("settlementAmount", deposit.getSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            List<TaDictVo> list =  taDictVoMapper.getTadictTree("CouponFrequently");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(deposit.getIntFeq())){
                    mapEnd.put("intFeq", t.getDict_value());
                }
                if(t.getDict_key().equals(deposit.getAmtFeq())){
                    mapEnd.put("amtFeq", t.getDict_value());
                }
            }
            mapEnd.put("basis", deposit.getBasis());
        }
        return mapEnd;
    }
}
