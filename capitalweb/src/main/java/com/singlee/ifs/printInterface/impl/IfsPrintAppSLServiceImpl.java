package com.singlee.ifs.printInterface.impl;

import com.singlee.ifs.mapper.IfsApprovermbSlMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailSlMapper;
import com.singlee.ifs.model.IfsApprovermbSl;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 债券借贷事前审批
 *
 * @author tlcb
 */
@Service("IfsPrintAppSLServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintAppSLServiceImpl implements IfsPrintService {
    
    @Autowired
    private  IfsCfetsrmbDetailSlMapper ifsCfetsrmbDetailSlMapper;
    
    @Autowired
    private  IfsApprovermbSlMapper ifsApprovermbSlMapper;
    
    
    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();

        Map<String, Object> paramer = new HashMap<String, Object>();
        //债券借贷事前审批主体信息
        IfsApprovermbSl sl=ifsApprovermbSlMapper.searchBondLon(id);
        if(null!=sl) {
            
        
        mapEnd.put("cname",sl.getLendInst());
        mapEnd.put("secid",sl.getUnderlyingSecurityId());
        mapEnd.put("underlyingQty",sl.getUnderlyingQty());
        mapEnd.put("price",sl.getPrice());
        mapEnd.put("tenor",sl.getTenor());
        mapEnd.put("firstSettlementDate",sl.getFirstSettlementDate());
        mapEnd.put("secondSettlementDate",sl.getSecondSettlementDate());
        mapEnd.put("basis",sl.getBasis());
        mapEnd.put("marginTotalAmt",sl.getMarginTotalAmt());
//        mapEnd.put("creditamt",sl.getc);
//        mapEnd.put("creditProdamt",sl.getUnderlyingSecurityId());
        if("0".equals(sl.getOppoDir())) {
            mapEnd.put("oppoDir","融出");
        }
        else {
            mapEnd.put("oppoDir","融入"); 
        }
        
        if("0".equals(sl.getIspayint())) {
            mapEnd.put("ispayint","是");
        }
        else {
            mapEnd.put("ispayint","否");
        }
        
       
        if("0".equals(sl.getIscanum())) {
            mapEnd.put("iscanum","是");
        }
        else {
            mapEnd.put("iscanum","否"); 
        }
        
        }
        //债券借贷事前审批质押券信息
        List<IfsCfetsrmbDetailSl> Detail=ifsCfetsrmbDetailSlMapper.searchBondByTicketId(id);

        mapEnd.put("entry",Detail);



        return mapEnd;
    }
}
