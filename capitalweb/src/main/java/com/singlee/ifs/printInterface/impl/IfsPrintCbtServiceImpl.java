package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCfetsrmbCbtMapper;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 现券买卖
 */
@Service("IfsPrintCbtServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintCbtServiceImpl implements IfsPrintService {

    @Autowired
    IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(id);
        if (ifsCfetsrmbCbt != null) {

            String myDir = ifsCfetsrmbCbt.getMyDir();
            if("P".equals(myDir)){
                mapEnd.put("buyInst", ifsCfetsrmbCbt.getInstfullname());
                mapEnd.put("buyTrader", ifsCfetsrmbCbt.getMyUserName());
                mapEnd.put("sellInst", ifsCfetsrmbCbt.getCno());
                mapEnd.put("sellTrader", ifsCfetsrmbCbt.getSellTrader());
            }else if("S".equals(myDir)){
                mapEnd.put("buyInst", ifsCfetsrmbCbt.getCno());
                mapEnd.put("buyTrader", ifsCfetsrmbCbt.getSellTrader());
                mapEnd.put("sellInst", ifsCfetsrmbCbt.getInstfullname());
                mapEnd.put("sellTrader", ifsCfetsrmbCbt.getMyUserName());
            }
            mapEnd.put("bondCode", ifsCfetsrmbCbt.getBondCode());
            mapEnd.put("bondName", ifsCfetsrmbCbt.getBondName());
            mapEnd.put("cleanPrice", ifsCfetsrmbCbt.getCleanPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("totalFaceValue", ifsCfetsrmbCbt.getTotalFaceValue().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("yield", ifsCfetsrmbCbt.getYield().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("tradeAmount", ifsCfetsrmbCbt.getTradeAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("accuredInterest", ifsCfetsrmbCbt.getAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("totalAccuredInterest", ifsCfetsrmbCbt.getTotalAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("dirtyPrice", ifsCfetsrmbCbt.getDirtyPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("settlementAmount", ifsCfetsrmbCbt.getSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            List<TaDictVo> list =  taDictVoMapper.getTadictTree("SettlementMethod");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCfetsrmbCbt.getSettlementMethod())){
                    mapEnd.put("settlementMethod", t.getDict_value());
                }
            }
            mapEnd.put("settlementDate", ifsCfetsrmbCbt.getSettlementDate());
        }
        return mapEnd;
    }
}
