package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.hrbextra.limit.mapper.IfsCustLimitMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 利率互换审批单
 *
 * @author tlcb
 */
@Service("IfsPrintIrsServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintIrsServiceImpl implements IfsPrintService {

    @Autowired
    private IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;

    @Autowired
    private TtInstitutionMapper ttInstitutionMapper;

    @Autowired
    private IfsCustLimitMapper ifsCustLimitMapper;




    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        Map<String, Object> custmap = new HashMap<String, Object>();

        //利率互换
        IfsCfetsrmbIrs irs = ifsCfetsrmbIrsMapper.OneById(id);
        if (null != irs) {

            mapEnd.put("productName", irs.getProductName());
            mapEnd.put("fixedTrader", irs.getFixedTrader());
            mapEnd.put("lastQty", irs.getLastQty());
            mapEnd.put("ccy", irs.getCcy());
            mapEnd.put("legPrice", irs.getLegPrice());
            mapEnd.put("tenor", irs.getTenor());
            mapEnd.put("floatPaymentDate", irs.getFloatPaymentDate());
            mapEnd.put("endDate", irs.getEndDate());
            mapEnd.put("fixedPaymentDate", irs.getFixedPaymentDate());
            mapEnd.put("fixedLegDayCount", irs.getFixedLegDayCount());

            if ("0".equals(irs.getInterestRateAdjustment())) {
                mapEnd.put("interestRateAdjustment", "不调整");
            } else {
                mapEnd.put("interestRateAdjustment", "实际天数");
            }

            mapEnd.put("interestRateAdjustment", irs.getInterestRateAdjustment());

            mapEnd.put("coreClntNo", irs.getFloatInst());
           custmap= ifsCustLimitMapper.searchCust(mapEnd);
           if(null!=custmap){
               mapEnd.put("floatInst", custmap.get("SN"));
           }


            TtInstitution ttInstitution =ttInstitutionMapper.getInstById(irs.getFixedInst());
            if(ttInstitution!=null){
                mapEnd.put("fixedInst", ttInstitution.getInstName());
            }

            mapEnd.put("rightRate", irs.getRightRate());
            mapEnd.put("benchmarkSpread", irs.getBenchmarkSpread());
            mapEnd.put("floatPaymentDate", irs.getFloatPaymentDate());
            mapEnd.put("legInterestAccrualDate", irs.getLegInterestAccrualDate());
            mapEnd.put("floatLegDayCount", irs.getFloatLegDayCount());
            mapEnd.put("interestMethod", irs.getInterestMethod());

            mapEnd.put("floatPaymentDate", irs.getFloatPaymentDate());

            if ("0".equals(irs.getCouponPaymentDateReset())) {
                mapEnd.put("couponPaymentDateReset", "上一营业日");
            } else if ("1".equals(irs.getCouponPaymentDateReset())) {
                mapEnd.put("couponPaymentDateReset", "下一营业日");
            } else {
                mapEnd.put("couponPaymentDateReset", "经调整的下一营业日");
            }



        }
        return mapEnd;
    }
}