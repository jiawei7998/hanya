package com.singlee.ifs.printInterface;

import java.util.Map;

public interface IfsPrintService {
	
	public Map<String,Object> getData(String id);
	
}
