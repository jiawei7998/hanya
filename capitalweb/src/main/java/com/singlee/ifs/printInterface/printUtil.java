package com.singlee.ifs.printInterface;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/***
 * 翻译数据字典类
 * 例如：数据库里存的是1,翻译成"是"
 * @author singlee4
 *
 */
public class printUtil {
	/***
	 * 
	 * @param dict  数据字典的dict_id
	 * @param entityField   实体类的某个字段，该字段在页面中用了下拉框
	 * @param map   实体类转换成的map对象
	 * @param jsStr  内存中整个数据字典的json对象
	 */
	
	public static void translate(String dict,String entityField,Map<String,Object> map,JSONObject jsStr){
		Object yn =jsStr.get(dict);
		JSONArray jsonArray=JSONArray.parseArray(String.valueOf(yn));
         for (int i = 0; i < jsonArray.size(); i++) {     //遍历json数组内容  
                  JSONObject object = jsonArray.getJSONObject(i); 
                  Map<?,?> p=object;
                  if(p.get("id").equals(map.get(entityField))){
                    map.put(entityField, p.get("text"));
                  }  
         }
	}
}
