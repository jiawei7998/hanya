package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCustDVPMapper;
import com.singlee.ifs.model.IfsCustDVP;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DVP券款兑付
 * @author Liang
 * @date 2021/10/28 14:32
 * =======================
 */
@Service("IfsPrintDVPServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintDVPServiceImpl  implements IfsPrintService {
    @Autowired
    IfsCustDVPMapper ifsCustDVPMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;
    @Override
    public Map<String, Object> getData(String fedealno) {
        Map<String, Object> mapEnd = new HashMap<String, Object>();
//        String[] sp=id.split("-");
//        String fedealno=sp[0];
//        String amount=sp[1];
        IfsCustDVP ifsCustDVP=ifsCustDVPMapper.selectCustDVPByFedealno(fedealno);
        if (ifsCustDVP != null){
            List<TaDictVo> list =  taDictVoMapper.getTadictTree("busTypes");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCustDVP.getType())){
                    mapEnd.put("busTypes", t.getDict_value());
                }
            }
            mapEnd.put("fedealno",fedealno);
            mapEnd.put("vdate",ifsCustDVP.getVdate());
            mapEnd.put("cname",ifsCustDVP.getCname());
            mapEnd.put("amount",ifsCustDVP.getAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("ioper",ifsCustDVP.getIoper());
            mapEnd.put("voper",ifsCustDVP.getVoper());
            mapEnd.put("ccyauthoper",ifsCustDVP.getCcyauthoper());
            mapEnd.put("secauthoper",ifsCustDVP.getSecauthoper());
            mapEnd.put("authoriza",ifsCustDVP.getAuthoriza());
        }
        return mapEnd;
    }
}
