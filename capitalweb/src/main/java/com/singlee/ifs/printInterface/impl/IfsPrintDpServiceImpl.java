package com.singlee.ifs.printInterface.impl;

import com.singlee.ifs.mapper.IfsCfetsrmbDpMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDpOfferMapper;
import com.singlee.ifs.model.IfsCfetsrmbDp;
import com.singlee.ifs.model.IfsCfetsrmbDpOffer;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 债券发行
 */
@Service("IfsPrintDpServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintDpServiceImpl implements IfsPrintService {

    @Autowired
    IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;
    @Autowired
    IfsCfetsrmbDpOfferMapper offerDpMapper;

    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.searchBondLon(id);
        if (ifsCfetsrmbDp != null) {
            mapEnd.put("borrowInst", ifsCfetsrmbDp.getInstfullname());
            mapEnd.put("borrowTrader", ifsCfetsrmbDp.getMyUserName());
            mapEnd.put("depositCode", ifsCfetsrmbDp.getDepositCode());
            mapEnd.put("depositAllName", ifsCfetsrmbDp.getDepositAllName());
            mapEnd.put("publishPrice", ifsCfetsrmbDp.getPublishPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("actualAmount", ifsCfetsrmbDp.getActualAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("benchmarkcurvename", ifsCfetsrmbDp.getBenchmarkcurvename().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("publishDate", ifsCfetsrmbDp.getPublishDate());
            mapEnd.put("valueDate", ifsCfetsrmbDp.getValueDate());
            mapEnd.put("duedate", ifsCfetsrmbDp.getDuedate());
            mapEnd.put("depositTerm", ifsCfetsrmbDp.getDepositTerm());

            List<IfsCfetsrmbDpOffer> ifsCfetsrmbDpOfferList = offerDpMapper.searchOfferList(id);
            for (IfsCfetsrmbDpOffer icdo: ifsCfetsrmbDpOfferList) {
                icdo.setLendFax(icdo.getOfferAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                icdo.setLendTel(icdo.getShouldPayMoney().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            }
            mapEnd.put("list", ifsCfetsrmbDpOfferList);

        }
        return mapEnd;
    }
}
