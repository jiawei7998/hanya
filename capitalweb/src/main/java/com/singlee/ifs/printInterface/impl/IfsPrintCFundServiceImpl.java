package com.singlee.ifs.printInterface.impl;


import com.singlee.ifs.printInterface.IfsPrintService;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 基金申购审批单
 *
 * @author tlcb
 */
@Service("IfsPrintCFundServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintCFundServiceImpl implements IfsPrintService {
    
    @Autowired
    private CFtAfpMapper cFtAfpMapper;
    @Autowired
    private CFtInfoMapper cFtInfoMapper;


    
    
    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();
       //基金申購信息
        CFtAfp cFtAfp =cFtAfpMapper.selectBydealno(id);
        if(null!=cFtAfp) {

            mapEnd.put("fundCode",cFtAfp.getFundCode());


            mapEnd.put("cname",cFtAfp.getCname());

            String FundCode=cFtAfp.getFundCode().substring(0,6);
            //基金基本信息
            CFtInfo CFtInfo= cFtInfoMapper.selectByPrimaryKey(FundCode);
            if(null!=CFtInfo){
            mapEnd.put("comp",CFtInfo.getManagComp());
            mapEnd.put("estDate",CFtInfo.getEstDate());
            mapEnd.put("fundFullName",CFtInfo.getFundFullName());
            mapEnd.put("fundName",CFtInfo.getFundName());
            mapEnd.put("totalQty",CFtInfo.getTotalQty());
            }

            mapEnd.put("tdate",cFtAfp.getTdate());
            mapEnd.put("price",cFtAfp.getPrice());
            mapEnd.put("shareAmt",cFtAfp.getShareAmt());
            mapEnd.put("amt",cFtAfp.getAmt());
            mapEnd.put("ftpflag",cFtAfp.getFtpflag());
            mapEnd.put("ftpprice",cFtAfp.getFtpprice());


        }

        return mapEnd;
    }
}
