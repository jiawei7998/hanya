package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.hrbextra.limit.mapper.IfsCustLimitMapper;
import com.singlee.ifs.mapper.IfsCfetsfxFwdMapper;
import com.singlee.ifs.model.IfsCfetsfxFwd;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 利率互换审批单
 *
 * @author tlcb
 */
@Service("IfsPrintFwdServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintFwdServiceImpl implements IfsPrintService {



    @Autowired
    private IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;

    @Autowired
    private TtInstitutionMapper ttInstitutionMapper;


    @Autowired
    private IfsCustLimitMapper ifsCustLimitMapper;

    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        Map<String, Object> custmap = new HashMap<String, Object>();
        //外汇即期
        IfsCfetsfxFwd spt = ifsCfetsfxFwdMapper.getCredit(id);
        if (null != spt) {
            mapEnd.put("instId", spt.getInstId());
            TtInstitution ttInstitution =ttInstitutionMapper.getInstById(spt.getInstId());
            if(ttInstitution!=null){
                mapEnd.put("instId", ttInstitution.getInstName());
            }
            if(null!=spt.getCounterpartyInstId()){
                mapEnd.put("coreClntNo", spt.getCounterpartyInstId());
                custmap= ifsCustLimitMapper.searchCust(mapEnd);
                if(null!=custmap){
                    mapEnd.put("counterpartyInstId", custmap.get("CFN"));
                }

            }


            mapEnd.put("dealer", spt.getDealer());
            mapEnd.put("counterpartyDealer", spt.getCounterpartyDealer());
            mapEnd.put("forDate", spt.getForDate());
            mapEnd.put("forTime", spt.getForTime());
            mapEnd.put("currencyPair", spt.getCurrencyPair());
            mapEnd.put("opicsccy", spt.getOpicsccy());
            mapEnd.put("opicsctrccy", spt.getOpicsctrccy());
            mapEnd.put("valueDate", spt.getValueDate());
            mapEnd.put("price", spt.getPrice());
            mapEnd.put("spread", spt.getSpread());
            mapEnd.put("exchangeRate", spt.getExchangeRate());
            mapEnd.put("buyAmount", spt.getBuyAmount());
            mapEnd.put("sellAmount", spt.getSellAmount());
            mapEnd.put("tradingType", spt.getTradingType());

           if("S".equals(spt.getBuyDirection())){
               mapEnd.put("buyDirection","卖出");
           }else {
               mapEnd.put("buyDirection","买入");
           }

        }
        return mapEnd;
    }
}