package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.hrbextra.limit.mapper.IfsCustLimitMapper;
import com.singlee.ifs.mapper.IfsCfetsfxSwapMapper;
import com.singlee.ifs.model.IfsCfetsfxSwap;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 利率互换审批单
 *
 * @author tlcb
 */
@Service("IfsPrintSwapServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintSwapServiceImpl implements IfsPrintService {


    @Autowired
    private IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;

    @Autowired
    private TtInstitutionMapper ttInstitutionMapper;

    @Autowired
    private IfsCustLimitMapper ifsCustLimitMapper;
    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        Map<String, Object> custmap = new HashMap<String, Object>();
        //外汇掉期
        IfsCfetsfxSwap swap = ifsCfetsfxSwapMapper.getrmswap(id);
        if (null != swap) {
            mapEnd.put("instId", swap.getInstId());
            TtInstitution ttInstitution =ttInstitutionMapper.getInstById(swap.getInstId());
            if(ttInstitution!=null){
                mapEnd.put("instId", ttInstitution.getInstName());
            }

            mapEnd.put("coreClntNo",swap.getCounterpartyInstId());
            custmap= ifsCustLimitMapper.searchCust(mapEnd);
            if(null!=custmap){
                mapEnd.put("counterpartyInstId", custmap.get("CFN"));
            }

            mapEnd.put("dealer",swap.getDealer());
            mapEnd.put("counterpartyDealer", swap.getCounterpartyDealer());
            mapEnd.put("forDate", swap.getForDate());
            mapEnd.put("forTime", swap.getForTime());
            mapEnd.put("currencyPair", swap.getCurrencyPair());

            mapEnd.put("opicsccy", swap.getOpicsccy());
            mapEnd.put("opicsctrccy", swap.getOpicsctrccy());

            mapEnd.put("nearValuedate", swap.getNearValuedate());

            mapEnd.put("fwdValuedate", swap.getFwdValuedate());
            mapEnd.put("nearPrice", swap.getNearPrice());
            mapEnd.put("fwdPrice", swap.getFwdPrice());
            mapEnd.put("nearSpread", swap.getNearSpread());
            mapEnd.put("spread", swap.getSpread());
            mapEnd.put("nearRate", swap.getNearRate());
            mapEnd.put("fwdRate", swap.getFwdRate());
            mapEnd.put("nearReversePrice", swap.getNearReversePrice());
            mapEnd.put("fwdReversePrice", swap.getFwdReversePrice());
            mapEnd.put("tenor", swap.getTenor());
            mapEnd.put("price", swap.getPrice());

            mapEnd.put("tradingType", swap.getTradingType());



            if("S".equals(swap.getDirection())){
                mapEnd.put("direction","卖出");
            }else {
                mapEnd.put("direction","买入");
            }

        }
        return mapEnd;
    }
}