package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCfetsrmbOrMapper;
import com.singlee.ifs.model.IfsCfetsrmbOr;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 买断式回购
 */
@Service("IfsPrintOrServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintOrServiceImpl implements IfsPrintService {

    @Autowired
    IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchRepo(id);
        if (ifsCfetsrmbOr != null) {

            String myDir = ifsCfetsrmbOr.getMyDir();
            if("S".equals(myDir)){
                mapEnd.put("positiveInst", ifsCfetsrmbOr.getInstfullname());
                mapEnd.put("positiveTrader", ifsCfetsrmbOr.getMyUserName());
                mapEnd.put("reverseInst", ifsCfetsrmbOr.getCno());
                mapEnd.put("reverseTrader", ifsCfetsrmbOr.getReverseTrader());
            }else if("P".equals(myDir)) {
                mapEnd.put("positiveInst", ifsCfetsrmbOr.getCno());
                mapEnd.put("positiveTrader", ifsCfetsrmbOr.getReverseTrader());
                mapEnd.put("reverseInst", ifsCfetsrmbOr.getInstfullname());
                mapEnd.put("reverseTrader", ifsCfetsrmbOr.getMyUserName());
            }

            mapEnd.put("bondCode", ifsCfetsrmbOr.getBondCode());
            mapEnd.put("bondName", ifsCfetsrmbOr.getBondName());
            mapEnd.put("tradingProduct", ifsCfetsrmbOr.getTradingProduct());
            mapEnd.put("tenor", ifsCfetsrmbOr.getTenor());
            mapEnd.put("repoRate", ifsCfetsrmbOr.getRepoRate().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("totalFaceValue", ifsCfetsrmbOr.getTotalFaceValue().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstCleanPrice", ifsCfetsrmbOr.getFirstCleanPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstYield", ifsCfetsrmbOr.getFirstYield().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstAccuredInterest", ifsCfetsrmbOr.getFirstAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstDirtyPrice", ifsCfetsrmbOr.getFirstDirtyPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            List<TaDictVo> list =  taDictVoMapper.getTadictTree("SettlementMethod");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCfetsrmbOr.getFirstSettlementMethod())){
                    mapEnd.put("firstSettlementMethod", t.getDict_value());
                }
                if(t.getDict_key().equals(ifsCfetsrmbOr.getSecondSettlementMethod())){
                    mapEnd.put("secondSettlementMethod", t.getDict_value());
                }
            }

            mapEnd.put("firstSettlementAmount", ifsCfetsrmbOr.getFirstSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondCleanPrice", ifsCfetsrmbOr.getSecondCleanPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondYield", ifsCfetsrmbOr.getSecondYield().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondAccuredInterest", ifsCfetsrmbOr.getSecondAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondDirtyPrice", ifsCfetsrmbOr.getSecondDirtyPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondSettlementAmount", ifsCfetsrmbOr.getSecondSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("firstSettlementDate", ifsCfetsrmbOr.getFirstSettlementDate());
            mapEnd.put("secondSettlementDate", ifsCfetsrmbOr.getSecondSettlementDate());
            mapEnd.put("occupancyDays", ifsCfetsrmbOr.getOccupancyDays());
        }
        return mapEnd;
    }
}
