package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCfetsrmbCrMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailCrMapper;
import com.singlee.ifs.model.IfsCfetsrmbCr;
import com.singlee.ifs.model.IfsCfetsrmbDetailCr;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 质押式回购
 */
@Service("IfsPrintCrServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintCrServiceImpl implements IfsPrintService {

    @Autowired
    IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;
    @Autowired
    IfsCfetsrmbDetailCrMapper detailCrMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {

        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(id);
        if (ifsCfetsrmbCr != null) {

            String myDir = ifsCfetsrmbCr.getMyDir();
            if("S".equals(myDir)){
                mapEnd.put("positiveInst", ifsCfetsrmbCr.getInstfullname());
                mapEnd.put("positiveTrader", ifsCfetsrmbCr.getMyUserName());
                mapEnd.put("reverseInst", ifsCfetsrmbCr.getCno());
                mapEnd.put("reverseTrader", ifsCfetsrmbCr.getReverseTrader());
            }else if("P".equals(myDir)) {
                mapEnd.put("positiveInst", ifsCfetsrmbCr.getCno());
                mapEnd.put("positiveTrader", ifsCfetsrmbCr.getReverseTrader());
                mapEnd.put("reverseInst", ifsCfetsrmbCr.getInstfullname());
                mapEnd.put("reverseTrader", ifsCfetsrmbCr.getMyUserName());
            }

            mapEnd.put("tenor", ifsCfetsrmbCr.getTenor());
            mapEnd.put("repoRate", ifsCfetsrmbCr.getRepoRate().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("underlyingQty", ifsCfetsrmbCr.getUnderlyingQty().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("tradeAmount", ifsCfetsrmbCr.getTradeAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("accuredInterest", ifsCfetsrmbCr.getAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("secondSettlementAmount", ifsCfetsrmbCr.getSecondSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());

            List<TaDictVo> list =  taDictVoMapper.getTadictTree("SettlementMethod");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCfetsrmbCr.getFirstSettlementMethod())){
                    mapEnd.put("firstSettlementMethod", t.getDict_value());
                }
                if(t.getDict_key().equals(ifsCfetsrmbCr.getSecondSettlementMethod())){
                    mapEnd.put("secondSettlementMethod", t.getDict_value());
                }
            }
            mapEnd.put("firstSettlementDate", ifsCfetsrmbCr.getFirstSettlementDate());
            mapEnd.put("secondSettlementDate", ifsCfetsrmbCr.getSecondSettlementDate());
            mapEnd.put("occupancyDays", ifsCfetsrmbCr.getOccupancyDays());
            mapEnd.put("tradingProduct", ifsCfetsrmbCr.getTradingProduct());

            List<IfsCfetsrmbDetailCr> crList = detailCrMapper.searchBondByTicketId(id);
            for (IfsCfetsrmbDetailCr icdc: crList) {
                icdc.setProdb(icdc.getTotalFaceValue().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                icdc.setProdtypeb( icdc.getUnderlyingStipType().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            }
            mapEnd.put("list", crList);

        }
        return mapEnd;
    }
}
