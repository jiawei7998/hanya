package com.singlee.ifs.printInterface.impl;


import com.singlee.fund.mapper.FtReinMapper;
import com.singlee.fund.model.FtRein;
import com.singlee.ifs.printInterface.IfsPrintService;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 基金红利再投审批单
 *
 *
 * @author tlcb
 */
@Service("IfsPrintCFundReinServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintCFundReinServiceImpl implements IfsPrintService {
    
    @Autowired
    private FtReinMapper ftReinMapper;
    @Autowired
    private CFtInfoMapper cFtInfoMapper;


    
    
    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();
       //基金红利再投信息
        FtRein cFtAfp =ftReinMapper.selectByPrimaryKey(id);
        if(null!=cFtAfp) {

            mapEnd.put("fundName",cFtAfp.getFundName());
            mapEnd.put("fundCode",cFtAfp.getFundCode());

            mapEnd.put("cname",cFtAfp.getCname());
            String FundCode=cFtAfp.getFundCode().substring(0,6);
            //基金基本信息
            CFtInfo CFtInfo= cFtInfoMapper.selectByPrimaryKey(FundCode);
            mapEnd.put("fundName",CFtInfo.getFundName());
            mapEnd.put("fundFullName",CFtInfo.getFundFullName());
            mapEnd.put("estDate",CFtInfo.getEstDate());
            mapEnd.put("totalQty",CFtInfo.getTotalQty());
            mapEnd.put("comp",CFtInfo.getManagComp());
            mapEnd.put("vdate",cFtAfp.getVdate());

            mapEnd.put("eAmt",cFtAfp.geteAmt());
            mapEnd.put("price",cFtAfp.getPrice());
            mapEnd.put("eshareAmt",cFtAfp.getEshareAmt());
            mapEnd.put("amt",cFtAfp.getVdate());
            if("0".equals(cFtAfp.getInvType())){
                mapEnd.put("invType","以摊余成本计量");

            }else if("3".equals(cFtAfp.getInvType())){
                mapEnd.put("invType","以公允价值计量且其变动计入其他综合收益");
            }else{
                mapEnd.put("invType","以公允价值计量且其变动计入当期损益");
            }


        }

        return mapEnd;
    }
}
