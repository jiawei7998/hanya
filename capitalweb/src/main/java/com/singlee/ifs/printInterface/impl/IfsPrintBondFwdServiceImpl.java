package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.IfsCfetsrmbBondfwdMapper;
import com.singlee.ifs.model.IfsCfetsrmbBondfwd;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/10/13 19:34
 * 债券远期
 * =======================
 */
@Service("IfsPrintBondFwdServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintBondFwdServiceImpl implements IfsPrintService {

    @Autowired
    IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
    @Autowired
    TaDictVoMapper taDictVoMapper;

    @Override
    public Map<String, Object> getData(String id) {
        Map<String, Object> mapEnd = new HashMap<String, Object>();
        IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = ifsCfetsrmbBondfwdMapper.selectById(id);
        if (ifsCfetsrmbBondfwd != null) {

            String myDir = ifsCfetsrmbBondfwd.getMyDir();
            if("P".equals(myDir)){
                mapEnd.put("buyInst", ifsCfetsrmbBondfwd.getBuyInst());
                mapEnd.put("buyTrader", ifsCfetsrmbBondfwd.getMyUserName());
                mapEnd.put("sellInst", ifsCfetsrmbBondfwd.getCno());
                mapEnd.put("sellTrader", ifsCfetsrmbBondfwd.getSellTrader());
            }else if("S".equals(myDir)){
                mapEnd.put("buyInst", ifsCfetsrmbBondfwd.getCno());
                mapEnd.put("buyTrader", ifsCfetsrmbBondfwd.getSellTrader());
                mapEnd.put("sellInst", ifsCfetsrmbBondfwd.getBuyInst());
                mapEnd.put("sellTrader", ifsCfetsrmbBondfwd.getBuyTrader());
            }
            mapEnd.put("bondCode", ifsCfetsrmbBondfwd.getBondCode());
            mapEnd.put("bondName", ifsCfetsrmbBondfwd.getBondName());
            mapEnd.put("cleanPrice", ifsCfetsrmbBondfwd.getCleanPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("totalFaceValue", ifsCfetsrmbBondfwd.getTotalFaceValue().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("yield", ifsCfetsrmbBondfwd.getYield().setScale(6, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("tradeAmount", ifsCfetsrmbBondfwd.getTradeAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("accuredInterest", ifsCfetsrmbBondfwd.getAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("totalAccuredInterest", ifsCfetsrmbBondfwd.getTotalAccuredInterest().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("dirtyPrice", ifsCfetsrmbBondfwd.getDirtyPrice().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            mapEnd.put("settlementAmount", ifsCfetsrmbBondfwd.getSettlementAmount().setScale(4, BigDecimal.ROUND_HALF_UP).toString());
            List<TaDictVo> list =  taDictVoMapper.getTadictTree("SettlementMethod");
            for (TaDictVo t: list) {
                if(t.getDict_key().equals(ifsCfetsrmbBondfwd.getSettlementMethod())){
                    mapEnd.put("settlementMethod", t.getDict_value());
                }
            }
            mapEnd.put("settlementDate", ifsCfetsrmbBondfwd.getSettlementDate());
        }
        return mapEnd;
    }
}
