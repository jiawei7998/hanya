package com.singlee.ifs.printInterface.impl;


import com.singlee.fund.mapper.FtReddMapper;
import com.singlee.fund.model.FtRedd;
import com.singlee.ifs.printInterface.IfsPrintService;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 基金现金分红审批单
 *
 *
 * @author tlcb
 */
@Service("IfsPrintCFundReddServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPrintCFundReddServiceImpl implements IfsPrintService {
    
    @Autowired
    private FtReddMapper ftReddMapper;
    @Autowired
    private CFtInfoMapper cFtInfoMapper;


    
    
    @Override
    public Map<String, Object> getData(String id) {
        
        Map<String, Object> mapEnd = new HashMap<String, Object>();

        FtRedd cFtAfp =ftReddMapper.selectByPrimaryKey(id);
        if(null!=cFtAfp) {

            mapEnd.put("fundName",cFtAfp.getFundName());
            mapEnd.put("fundCode",cFtAfp.getFundCode());

            mapEnd.put("cname",cFtAfp.getCname());
            String FundCode=cFtAfp.getFundCode().substring(0,6);
            //基金基本信息
            CFtInfo CFtInfo= cFtInfoMapper.selectByPrimaryKey(FundCode);
            mapEnd.put("fundFullName",CFtInfo.getFundFullName());
            mapEnd.put("fundName",CFtInfo.getFundName());
            mapEnd.put("estDate",CFtInfo.getEstDate());
            mapEnd.put("totalQty",CFtInfo.getTotalQty());
            mapEnd.put("comp",CFtInfo.getManagComp());
            mapEnd.put("tdate",cFtAfp.getTdate());
            mapEnd.put("vdate",cFtAfp.getVdate());

            mapEnd.put("bshareAmt",cFtAfp.getBshareAmt());
            mapEnd.put("price",cFtAfp.getPrice());
            mapEnd.put("shareAmt",cFtAfp.getTdate());
            mapEnd.put("amt",cFtAfp.getVdate());
            if("0".equals(cFtAfp.getInvType())){
                mapEnd.put("invType","以摊余成本计量");

            }else if("3".equals(cFtAfp.getInvType())){
                mapEnd.put("invType","以公允价值计量且其变动计入其他综合收益");
            }else{
                mapEnd.put("invType","以公允价值计量且其变动计入当期损益");
            }


        }

        return mapEnd;
    }
}
