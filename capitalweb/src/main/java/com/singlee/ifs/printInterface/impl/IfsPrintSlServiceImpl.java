package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import com.singlee.ifs.model.IfsCfetsrmbSl;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 债券借贷
 */
@Service("IfsPrintSlServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintSlServiceImpl implements IfsPrintService {

	@Autowired
	IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper;
	@Autowired
	IfsCfetsrmbDetailSlMapper detailSlMapper;
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;

	@Autowired
	WindSeclMapper windSeclMapper;

	@Autowired
	WindBondAddMapper windBondAddMapper;
	@Autowired
	TaDictVoMapper taDictVoMapper;

	@Override
	public Map<String, Object> getData(String id) {

		Map<String, Object> mapEnd = new HashMap<String, Object>();
		IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(id);
		mapEnd.put("custInst", ifsCfetsrmbSl.getCno());//交易对手
		mapEnd.put("underlyingBond", ifsCfetsrmbSl.getUnderlyingSecurityId());//标的债券代码
		mapEnd.put("underlyingQty", ifsCfetsrmbSl.getUnderlyingQty().setScale(2,RoundingMode.HALF_UP));//标的债券券面总额
		/**
		 * 计算券面市值=标的债券券面总额*中债估值净价（万德：ecaluate_price）
		 */
		BigDecimal ecaluatePrice = windSeclMapper.getEcaluatePrice(ifsCfetsrmbSl.getUnderlyingSecurityId());
		BigDecimal marketAmount = ifsCfetsrmbSl.getUnderlyingQty().multiply(ecaluatePrice == null ? BigDecimal.ZERO : ecaluatePrice).setScale(2,RoundingMode.HALF_UP);
		mapEnd.put("marketAmount", marketAmount);//标的债券券面市值总额
		mapEnd.put("price", ifsCfetsrmbSl.getPrice().setScale(2,RoundingMode.HALF_UP));//借贷费率
		mapEnd.put("tenor", ifsCfetsrmbSl.getTenor());//借贷期限
		mapEnd.put("firstSettlementDate", ifsCfetsrmbSl.getFirstSettlementDate());//首次结算日
		mapEnd.put("secondSettlementDate", ifsCfetsrmbSl.getFirstSettlementDate());//到期结算日
		/*
		 * 质押比例:标的债券市值总额/质押债券市值总额
		 * */
		List<IfsCfetsrmbDetailSl> detail = detailSlMapper.searchBondByTicketId(id);
		final BigDecimal[] detailMarketAmout = {BigDecimal.ZERO};
		detail.stream().forEach(e -> {

			//查询债券
			IfsOpicsBond bond = ifsOpicsBondMapper.searchById(e.getMarginSecuritiesId());

			/*
			* 数据字典转换Rating
			* */
			AtomicReference<String> bondrating= new AtomicReference<>("");
			if(bond!=null){
				Map<String,Object> dicParamMap=new HashMap<>();
				dicParamMap.put("dict_id","Rating");
				List<TaDictVo> taDictByDictId = taDictVoMapper.getTaDictByDictId(dicParamMap);
				taDictByDictId.forEach(dictVo->{
					if(dictVo.getDict_key().equals(bond.getBondrating())){
						bondrating.set(dictVo.getDict_value());
					}
				});
			}
			e.setBondrating(bondrating.get());//设置债券主体评级
			BigDecimal ecaluatePrice2 = windSeclMapper.getEcaluatePrice(e.getMarginSecuritiesId());
			if(ecaluatePrice2==null){
				ecaluatePrice2=BigDecimal.ZERO;
			}
			detailMarketAmout[0] = detailMarketAmout[0].add(e.getMarginAmt().multiply(ecaluatePrice2));

			//计算发行量
			Map<String, Object> windBondAdd = windBondAddMapper.getWindBondAdd(e.getMarginSecuritiesId());
			BigDecimal faMount = BigDecimal.ZERO;
			BigDecimal zaMount = BigDecimal.ZERO;
			if (windBondAdd != null) {
				if (windBondAdd.get("faMount") != null) {
					faMount = new BigDecimal(windBondAdd.get("faMount").toString());
				}
				if (windBondAdd.get("zaMount") != null) {
					zaMount = new BigDecimal(windBondAdd.get("zaMount").toString());
				}
			}
			e.setIssuanceScale(faMount.add(zaMount));
		});
		mapEnd.put("detailMarketAmout", detailMarketAmout[0].setScale(2,RoundingMode.HALF_UP));//质押债券市值总额
		mapEnd.put("marginRatio", marketAmount.divide(detailMarketAmout[0], 2, RoundingMode.HALF_UP));//质押比例
		String dir="";
		if("P".equals(ifsCfetsrmbSl.getMyDir())){
			dir="融入";
		}else if("S".equals(ifsCfetsrmbSl.getMyDir())){
			dir="融出";
		}
		mapEnd.put("dir", dir);
        Map<String,Object> map=new HashMap<>();
		for (int i = 1; i <= detail.size(); i++) {
			map.put("map"+i,detail.get(i-1));
		}
		mapEnd.put("details", map);//质押券列表
		String approvalFormName="债券借贷业务审批单";
		if(ifsCfetsrmbSl.getApprovalFormName()!=null&&!"".equals(ifsCfetsrmbSl.getApprovalFormName())){
			approvalFormName=ifsCfetsrmbSl.getApprovalFormName();
		}
		mapEnd.put("approvalFormName", approvalFormName);
		return mapEnd;
	}
}
