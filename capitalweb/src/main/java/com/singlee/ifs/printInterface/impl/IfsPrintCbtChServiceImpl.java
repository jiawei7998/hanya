package com.singlee.ifs.printInterface.impl;

import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbCbtMapper;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.printInterface.IfsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 债券撮合交易审批单
 */
@Service("IfsPrintCbtChServiceImpl")
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsPrintCbtChServiceImpl implements IfsPrintService {

	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	@Autowired
	TaDictVoMapper taDictVoMapper;

	@Override
	public Map<String, Object> getData(String id) {

		Map<String, Object> mapEnd = new HashMap<String, Object>();
		IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(id);
		mapEnd.put("aDate", ifsCfetsrmbCbt.getaDate());
		mapEnd.put("settlementDate", ifsCfetsrmbCbt.getSettlementDate());
		mapEnd.put("buyInst", ifsCfetsrmbCbt.getInstfullname());
		mapEnd.put("sellInst", ifsCfetsrmbCbt.getSellInst());
		mapEnd.put("bondCode", ifsCfetsrmbCbt.getBondCode());
		mapEnd.put("totalFaceValue", ifsCfetsrmbCbt.getTotalFaceValue());
		mapEnd.put("cleanPrice", ifsCfetsrmbCbt.getCleanPrice());
		return mapEnd;
	}
}
