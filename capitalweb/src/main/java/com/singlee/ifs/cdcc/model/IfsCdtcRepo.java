package com.singlee.ifs.cdcc.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
public class IfsCdtcRepo implements Serializable {
    private String txid;

    private String txflowid;

    private String credttm;

    private String instrsts;

    private String ctrctid;

    private String biztp;

    private String instrid;

    private String givacctname;

    private String givaccid;

    private String takacctname;

    private String takacctid;

    private String sttlmtp1;

    private String sttlmtp2;

    private String bondid;

    private String bondshrtnm;

    private String bondamt;

    private String aggtfaceamt;

    private String rate;

    private String terms;

    private Date firstsettledate;

    private Date secondsettledate;

    private String givinfnm;

    private String takinfnm;

    private String repotype;

    private String val1;

    private String val2;

    private String createdate;

    private String createtime;

    private String val3;

    private String val4;

    private String matchstatus;

    private String sendstatus;

    private String contractstatus;

    private String text1;

    private String text2;

    private String text3;

    private String secondcontractstatus;

    private String matchmessage;

    private Date matchtime;

    private String sendmessage;

    private String dealno;

    private String branch;

    private String seq;

    private String orgtrcnfrmind;

    private String ctrcnfrmind;

    private String text4;

    private String text5;

    private String text6;

    private String creator;

    private static final long serialVersionUID = 1L;

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxflowid() {
        return txflowid;
    }

    public void setTxflowid(String txflowid) {
        this.txflowid = txflowid;
    }

    public String getCredttm() {
        return credttm;
    }

    public void setCredttm(String credttm) {
        this.credttm = credttm;
    }

    public String getInstrsts() {
        return instrsts;
    }

    public void setInstrsts(String instrsts) {
        this.instrsts = instrsts;
    }

    public String getCtrctid() {
        return ctrctid;
    }

    public void setCtrctid(String ctrctid) {
        this.ctrctid = ctrctid;
    }

    public String getBiztp() {
        return biztp;
    }

    public void setBiztp(String biztp) {
        this.biztp = biztp;
    }

    public String getInstrid() {
        return instrid;
    }

    public void setInstrid(String instrid) {
        this.instrid = instrid;
    }

    public String getGivacctname() {
        return givacctname;
    }

    public void setGivacctname(String givacctname) {
        this.givacctname = givacctname;
    }

    public String getGivaccid() {
        return givaccid;
    }

    public void setGivaccid(String givaccid) {
        this.givaccid = givaccid;
    }

    public String getTakacctname() {
        return takacctname;
    }

    public void setTakacctname(String takacctname) {
        this.takacctname = takacctname;
    }

    public String getTakacctid() {
        return takacctid;
    }

    public void setTakacctid(String takacctid) {
        this.takacctid = takacctid;
    }

    public String getSttlmtp1() {
        return sttlmtp1;
    }

    public void setSttlmtp1(String sttlmtp1) {
        this.sttlmtp1 = sttlmtp1;
    }

    public String getSttlmtp2() {
        return sttlmtp2;
    }

    public void setSttlmtp2(String sttlmtp2) {
        this.sttlmtp2 = sttlmtp2;
    }

    public String getBondid() {
        return bondid;
    }

    public void setBondid(String bondid) {
        this.bondid = bondid;
    }

    public String getBondshrtnm() {
        return bondshrtnm;
    }

    public void setBondshrtnm(String bondshrtnm) {
        this.bondshrtnm = bondshrtnm;
    }

    public String getBondamt() {
        return bondamt;
    }

    public void setBondamt(String bondamt) {
        this.bondamt = bondamt;
    }

    public String getAggtfaceamt() {
        return aggtfaceamt;
    }

    public void setAggtfaceamt(String aggtfaceamt) {
        this.aggtfaceamt = aggtfaceamt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public Date getFirstsettledate() {
        return firstsettledate;
    }

    public void setFirstsettledate(Date firstsettledate) {
        this.firstsettledate = firstsettledate;
    }

    public Date getSecondsettledate() {
        return secondsettledate;
    }

    public void setSecondsettledate(Date secondsettledate) {
        this.secondsettledate = secondsettledate;
    }

    public String getGivinfnm() {
        return givinfnm;
    }

    public void setGivinfnm(String givinfnm) {
        this.givinfnm = givinfnm;
    }

    public String getTakinfnm() {
        return takinfnm;
    }

    public void setTakinfnm(String takinfnm) {
        this.takinfnm = takinfnm;
    }

    public String getRepotype() {
        return repotype;
    }

    public void setRepotype(String repotype) {
        this.repotype = repotype;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getVal3() {
        return val3;
    }

    public void setVal3(String val3) {
        this.val3 = val3;
    }

    public String getVal4() {
        return val4;
    }

    public void setVal4(String val4) {
        this.val4 = val4;
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus;
    }

    public String getSendstatus() {
        return sendstatus;
    }

    public void setSendstatus(String sendstatus) {
        this.sendstatus = sendstatus;
    }

    public String getContractstatus() {
        return contractstatus;
    }

    public void setContractstatus(String contractstatus) {
        this.contractstatus = contractstatus;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getSecondcontractstatus() {
        return secondcontractstatus;
    }

    public void setSecondcontractstatus(String secondcontractstatus) {
        this.secondcontractstatus = secondcontractstatus;
    }

    public String getMatchmessage() {
        return matchmessage;
    }

    public void setMatchmessage(String matchmessage) {
        this.matchmessage = matchmessage;
    }

    public Date getMatchtime() {
        return matchtime;
    }

    public void setMatchtime(Date matchtime) {
        this.matchtime = matchtime;
    }

    public String getSendmessage() {
        return sendmessage;
    }

    public void setSendmessage(String sendmessage) {
        this.sendmessage = sendmessage;
    }

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getOrgtrcnfrmind() {
        return orgtrcnfrmind;
    }

    public void setOrgtrcnfrmind(String orgtrcnfrmind) {
        this.orgtrcnfrmind = orgtrcnfrmind;
    }

    public String getCtrcnfrmind() {
        return ctrcnfrmind;
    }

    public void setCtrcnfrmind(String ctrcnfrmind) {
        this.ctrcnfrmind = ctrcnfrmind;
    }

    public String getText4() {
        return text4;
    }

    public void setText4(String text4) {
        this.text4 = text4;
    }

    public String getText5() {
        return text5;
    }

    public void setText5(String text5) {
        this.text5 = text5;
    }

    public String getText6() {
        return text6;
    }

    public void setText6(String text6) {
        this.text6 = text6;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", txid=").append(txid);
        sb.append(", txflowid=").append(txflowid);
        sb.append(", credttm=").append(credttm);
        sb.append(", instrsts=").append(instrsts);
        sb.append(", ctrctid=").append(ctrctid);
        sb.append(", biztp=").append(biztp);
        sb.append(", instrid=").append(instrid);
        sb.append(", givacctname=").append(givacctname);
        sb.append(", givaccid=").append(givaccid);
        sb.append(", takacctname=").append(takacctname);
        sb.append(", takacctid=").append(takacctid);
        sb.append(", sttlmtp1=").append(sttlmtp1);
        sb.append(", sttlmtp2=").append(sttlmtp2);
        sb.append(", bondid=").append(bondid);
        sb.append(", bondshrtnm=").append(bondshrtnm);
        sb.append(", bondamt=").append(bondamt);
        sb.append(", aggtfaceamt=").append(aggtfaceamt);
        sb.append(", rate=").append(rate);
        sb.append(", terms=").append(terms);
        sb.append(", firstsettledate=").append(firstsettledate);
        sb.append(", secondsettledate=").append(secondsettledate);
        sb.append(", givinfnm=").append(givinfnm);
        sb.append(", takinfnm=").append(takinfnm);
        sb.append(", repotype=").append(repotype);
        sb.append(", val1=").append(val1);
        sb.append(", val2=").append(val2);
        sb.append(", createdate=").append(createdate);
        sb.append(", createtime=").append(createtime);
        sb.append(", val3=").append(val3);
        sb.append(", val4=").append(val4);
        sb.append(", matchstatus=").append(matchstatus);
        sb.append(", sendstatus=").append(sendstatus);
        sb.append(", contractstatus=").append(contractstatus);
        sb.append(", text1=").append(text1);
        sb.append(", text2=").append(text2);
        sb.append(", text3=").append(text3);
        sb.append(", secondcontractstatus=").append(secondcontractstatus);
        sb.append(", matchmessage=").append(matchmessage);
        sb.append(", matchtime=").append(matchtime);
        sb.append(", sendmessage=").append(sendmessage);
        sb.append(", dealno=").append(dealno);
        sb.append(", branch=").append(branch);
        sb.append(", seq=").append(seq);
        sb.append(", orgtrcnfrmind=").append(orgtrcnfrmind);
        sb.append(", ctrcnfrmind=").append(ctrcnfrmind);
        sb.append(", text4=").append(text4);
        sb.append(", text5=").append(text5);
        sb.append(", text6=").append(text6);
        sb.append(", creator=").append(creator);
        sb.append("]");
        return sb.toString();
    }
}