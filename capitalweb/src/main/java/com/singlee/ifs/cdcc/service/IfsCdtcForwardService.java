package com.singlee.ifs.cdcc.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdcc.model.IfsCdtcForward;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsCdtcForwardService{


    int insert(IfsCdtcForward record);

    int insertSelective(IfsCdtcForward record);


    Page<IfsCdtcForward> getDate(Map<String,Object> map);

}
