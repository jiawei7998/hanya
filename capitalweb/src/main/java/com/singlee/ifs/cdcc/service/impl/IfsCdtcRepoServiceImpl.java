package com.singlee.ifs.cdcc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.cdcc.mapper.IfsCdtcRepoMapper;
import com.singlee.ifs.cdcc.model.IfsCdtcRepo;
import com.singlee.ifs.cdcc.service.IfsCdtcRepoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsCdtcRepoServiceImpl implements IfsCdtcRepoService{

    @Resource
    private IfsCdtcRepoMapper ifsCdtcRepoMapper;

    @Override
    public int insert(IfsCdtcRepo record) {
        return ifsCdtcRepoMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcRepo record) {
        return ifsCdtcRepoMapper.insertSelective(record);
    }

    @Override
    public Page<IfsCdtcRepo> getDate(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCdtcRepo> result = ifsCdtcRepoMapper.getDate(map, rb);
        return result;

    }

}
