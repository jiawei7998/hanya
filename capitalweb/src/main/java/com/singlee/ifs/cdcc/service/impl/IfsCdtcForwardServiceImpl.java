package com.singlee.ifs.cdcc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.cdcc.mapper.IfsCdtcForwardMapper;
import com.singlee.ifs.cdcc.model.IfsCdtcForward;
import com.singlee.ifs.cdcc.service.IfsCdtcForwardService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsCdtcForwardServiceImpl implements IfsCdtcForwardService{

    @Resource
    private IfsCdtcForwardMapper ifsCdtcForwardMapper;

    @Override
    public int insert(IfsCdtcForward record) {
        return ifsCdtcForwardMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcForward record) {
        return ifsCdtcForwardMapper.insertSelective(record);
    }

    @Override
    public Page<IfsCdtcForward> getDate(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCdtcForward> result = ifsCdtcForwardMapper.getDate(map, rb);
        return result;
    }

}
