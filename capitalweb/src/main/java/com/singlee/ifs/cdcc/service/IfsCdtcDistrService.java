package com.singlee.ifs.cdcc.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdcc.model.IfsCdtcDistr;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
public interface IfsCdtcDistrService{


    int insert(IfsCdtcDistr record);

    int insertSelective(IfsCdtcDistr record);


    Page<IfsCdtcDistr> getDate(Map<String,Object> map);

}
