package com.singlee.ifs.cdcc.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdcc.model.IfsCdtcCbt;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:34
 * @description：${description}
 * @modified By：现券业务
 * @version:   
 */
public interface IfsCdtcCbtService{


    int insert(IfsCdtcCbt record);

    int insertSelective(IfsCdtcCbt record);

    Page<IfsCdtcCbt> getDate(Map<String,Object> map);

}
