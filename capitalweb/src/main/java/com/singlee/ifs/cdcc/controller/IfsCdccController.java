package com.singlee.ifs.cdcc.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.ifs.cdcc.model.IfsCdtcCbt;
import com.singlee.ifs.cdcc.model.IfsCdtcDistr;
import com.singlee.ifs.cdcc.model.IfsCdtcForward;
import com.singlee.ifs.cdcc.model.IfsCdtcRepo;
import com.singlee.ifs.cdcc.service.IfsCdtcCbtService;
import com.singlee.ifs.cdcc.service.IfsCdtcDistrService;
import com.singlee.ifs.cdcc.service.IfsCdtcForwardService;
import com.singlee.ifs.cdcc.service.IfsCdtcRepoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/13 16:47
 * @description：中债业务
 * @modified By：
 * @version:
 */
@Controller
@RequestMapping(value = "IfsCdccController")
public class IfsCdccController {

    @Resource
    IfsCdtcCbtService ifsCdtcCbtService;

    @Resource
    IfsCdtcDistrService  ifsCdtcDistrService;

    IfsCdtcForwardService ifsCdtcForwardService;

    @Resource
    IfsCdtcRepoService  ifsCdtcRepoService;
    @ResponseBody
    @RequestMapping(value = "/getPageIfsCdtcCbt")
    public RetMsg<PageInfo<IfsCdtcCbt>> getPageIfsCdtcCbt(Map<String, Object> map){
        Page<IfsCdtcCbt> page=ifsCdtcCbtService.getDate(map);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping(value = "/getPageIfsCdtcDistr")
    public RetMsg<PageInfo<IfsCdtcDistr>> getPageIfsCdtcDistr(Map<String, Object> map){
        Page<IfsCdtcDistr> page=ifsCdtcDistrService.getDate(map);
        return RetMsgHelper.ok(page);
    }


    @ResponseBody
    @RequestMapping(value = "/getPageIfsCdtcForward")
    public RetMsg<PageInfo<IfsCdtcForward>> getPageIfsCdtcForward(Map<String, Object> map){
        Page<IfsCdtcForward> page=ifsCdtcForwardService.getDate(map);
        return RetMsgHelper.ok(page);
    }



    @ResponseBody
    @RequestMapping(value = "/getPageIfsCdtcRepo")
    public RetMsg<PageInfo<IfsCdtcRepo>> getPageIfsCdtcRepo(Map<String, Object> map){
        Page<IfsCdtcRepo> page=ifsCdtcRepoService.getDate(map);
        return RetMsgHelper.ok(page);
    }

}
