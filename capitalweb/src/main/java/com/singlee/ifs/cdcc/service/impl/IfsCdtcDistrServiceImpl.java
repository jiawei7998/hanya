package com.singlee.ifs.cdcc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.cdcc.mapper.IfsCdtcDistrMapper;
import com.singlee.ifs.cdcc.model.IfsCdtcDistr;
import com.singlee.ifs.cdcc.service.IfsCdtcDistrService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Service
public class IfsCdtcDistrServiceImpl implements IfsCdtcDistrService{

    @Resource
    private IfsCdtcDistrMapper ifsCdtcDistrMapper;

    @Override
    public int insert(IfsCdtcDistr record) {
        return ifsCdtcDistrMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcDistr record) {
        return ifsCdtcDistrMapper.insertSelective(record);
    }

    @Override
    public Page<IfsCdtcDistr> getDate(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCdtcDistr> result = ifsCdtcDistrMapper.getDate(map, rb);
        return result;
    }

}
