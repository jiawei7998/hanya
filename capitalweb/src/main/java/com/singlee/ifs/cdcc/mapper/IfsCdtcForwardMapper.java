package com.singlee.ifs.cdcc.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdcc.model.IfsCdtcForward;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:35
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsCdtcForwardMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcForward record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcForward record);

    Page<IfsCdtcForward> getDate(Map<String,Object> map, RowBounds rb);
}