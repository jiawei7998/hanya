package com.singlee.ifs.cdcc.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.cdcc.mapper.IfsCdtcCbtMapper;
import com.singlee.ifs.cdcc.model.IfsCdtcCbt;
import com.singlee.ifs.cdcc.service.IfsCdtcCbtService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/12/13 14:34
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsCdtcCbtServiceImpl implements IfsCdtcCbtService {

    @Resource
    private IfsCdtcCbtMapper ifsCdtcCbtMapper;

    @Override
    public int insert(IfsCdtcCbt record) {
        return ifsCdtcCbtMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsCdtcCbt record) {
        return ifsCdtcCbtMapper.insertSelective(record);
    }

    @Override
    public Page<IfsCdtcCbt> getDate(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCdtcCbt> result = ifsCdtcCbtMapper.getDate(map, rb);
        return result;
    }

}
