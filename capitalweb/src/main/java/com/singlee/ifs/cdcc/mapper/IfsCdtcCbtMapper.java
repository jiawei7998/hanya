package com.singlee.ifs.cdcc.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.cdcc.model.IfsCdtcCbt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * @author     ：chenguo
 * @date       ：Created in 2021/12/13 14:34
 * @description：${description}
 * @modified By：
 * @version:   
 */
@Mapper
public interface IfsCdtcCbtMapper {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IfsCdtcCbt record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsCdtcCbt record);

    Page<IfsCdtcCbt> getDate(Map<String,Object> map, RowBounds rb);
}