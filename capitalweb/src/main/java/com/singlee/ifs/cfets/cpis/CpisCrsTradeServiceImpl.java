package com.singlee.ifs.cfets.cpis;

import com.singlee.financial.cfets.ICpisCrsTradeService;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecForeignCrsModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisCrsRecMapper;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class CpisCrsTradeServiceImpl implements ICpisCrsTradeService{
	
	private Logger logger = LogManager.getLogger(CpisCrsTradeServiceImpl.class);

	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	
	@Autowired
	IfsCpisCrsRecMapper ifsCpisCrsRecMapper;
	
	@Override
	public RetBean send(RecForeignCrsModel model)
			throws RemoteConnectFailureException, Exception {
		try {
			logger.info("开始更新交易明细！");
			//更新交易明细信息
			int count1 = ifsCpisCrsRecMapper.getRecSptFwdCount(model);
			if (count1 == 0) {
				ifsCpisCrsRecMapper.insertSelective(model);
			}else {
				ifsCpisCrsRecMapper.updateByConfirmId(model);
			}
			//获取返回的交易明细中的确认状态等信息
			RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
			recCPISFeedBackModel.setConfirmID(model.getConfirmid());
			recCPISFeedBackModel.setExecID(model.getExecId());
			recCPISFeedBackModel.setMarketIndicator(model.getMarketIndicator());
			recCPISFeedBackModel.setTradeDate(model.getTradedate());
			recCPISFeedBackModel.setConfirmStatus(model.getConfirmStatus());
			recCPISFeedBackModel.setExecType(model.getExecType());
			//更新反馈信息表中确认状态等信息
			int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);
			if (count == 0) {
				ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
			}else {
				ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
			}
			logger.info("确认编号："+recCPISFeedBackModel.getConfirmID()+"交易明细更新成功");
			return new RetBean(RetStatusEnum.S, "success", "交易明细更新成功！");
		} catch (Exception e) {
			logger.error("更新交易明细失败！"+e,e);
			throw e;
		}
	}

	@Override
	public RetBean send(List<RecForeignCrsModel> list)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
