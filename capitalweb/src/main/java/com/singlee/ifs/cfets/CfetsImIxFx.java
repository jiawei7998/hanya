package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixFx;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.FxSptFwdBean;
import com.singlee.financial.pojo.FxSwapBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.component.SettMode;
import com.singlee.financial.pojo.component.SptFwdTypeEnum;
import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.IfsCfetsfxFwd;
import com.singlee.ifs.model.IfsCfetsfxSpt;
import com.singlee.ifs.model.IfsCfetsfxSwap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 外汇交易业务处理类
 * 
 * 
 * ClassName: CfetsImIxFx <br/>
 * Function: 主要落地外汇交易即期/远端/掉期交易数据. <br/>
 * Reason: 用于审批+处理后接入OPICS系统中. <br/>
 * date: 2018-5-30 下午03:57:38 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Service
public class CfetsImIxFx implements ICfetsImixFx {
	@Autowired
	private IfsCfetsfxSwapMapper cfetsfxSwapMapper;
	@Autowired
	private IfsCfetsfxSptMapper cfetsfxSptMapper;
	@Autowired
	private IfsCfetsfxFwdMapper cfetsfxFwdMapper;
	@Autowired
	private IfsCfetsPackMsgMapper cfetsPackMsgMapper;
	@Autowired
	private CfetsRmbService cfetsService;
	@Autowired
	private TradeSettlsMapper tradeSettlsMapper;

	private static Logger logger = LoggerFactory.getLogger(CfetsImIxFx.class);

	@Override
	public RetBean send(FxSptFwdBean fxSptFwdBean, CfetsPackMsgBean cfetsPackMsg) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			logger.info("保存下行交易FXSPT报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易FXSPT报文异常:", e1);
		}

//		logger.info("外汇即期远期输入对象：" + fxSptFwdBean);
		RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		String retCode = null;
		String retMsg = null;
		try {
			//cfets 清算路径保存
			TradeSettlsBean settlsBean = new TradeSettlsBean();

			// 外汇即期
			if (fxSptFwdBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT) == 0) {
				logger.info("外汇即期输入对象：" + fxSptFwdBean);
				// 外汇即期对象
				IfsCfetsfxSpt spt = new IfsCfetsfxSpt();
				// 外汇即期
				spt.setDealSource("02");
				// 交易状态
				if (fxSptFwdBean.getStatus() != null) {
					spt.setDealTransType(StringUtils.trimToEmpty(fxSptFwdBean.getStatus().toString()));
				} else {
					spt.setDealTransType("1");
				}

				// 成交编号
				spt.setTicketId(cfetsService.getTicketId("SPT"));
				// 成交单编号
				spt.setContractId(fxSptFwdBean.getExecId());
				//交易后确认标识
				spt.setCfetsCnfmIndicator(fxSptFwdBean.getCfetsCnfmIndicator());
				// 汇率
				spt.setPrice(fxSptFwdBean.getCurrencyInfo().getSpotRate());
				// 升贴水点
				BigDecimal spread = fxSptFwdBean.getCurrencyInfo().getPoints()==null?new BigDecimal("0"):fxSptFwdBean.getCurrencyInfo().getPoints();
				if("JPY.CNY".equalsIgnoreCase(fxSptFwdBean.getCcypair())) {
					spt.setSpread(String.valueOf(spread.divide(new BigDecimal("100")).doubleValue()));
				}else if("USD.JPY".equalsIgnoreCase(fxSptFwdBean.getCcypair()) || "EUR.JPY".equalsIgnoreCase(fxSptFwdBean.getCcypair())) {
					spt.setSpread(String.valueOf(spread.multiply(new BigDecimal("100")).doubleValue()));
				}else {
					spt.setSpread(String.valueOf(spread));
				}
				// 成交汇率
				BigDecimal price = fxSptFwdBean.getCurrencyInfo().getRate() == null ? new BigDecimal("0") : fxSptFwdBean.getCurrencyInfo().getRate();
				spt.setExchangeRate(String.valueOf(price.doubleValue()));

				// 本方机构
				spt.setInstId(cfetsService.getLocalInstitution(spt,null, fxSptFwdBean.getInstitutionInfo().getInstitutionId()));
				// 交易方向 
				spt.setBuyDirection(fxSptFwdBean.getPs() == null ? "" : fxSptFwdBean.getPs().toString());// 交易方向
				// 对手方机构
				spt.setCounterpartyInstId(cfetsService.getLocalInstitution(spt,null,
						fxSptFwdBean.getContraPatryInstitutionInfo().getInstitutionId()));
				// 对手方交易员名称
				spt.setCounterpartyDealer(fxSptFwdBean.getContraPatryInstitutionInfo().getTradeName());
				// 交易日期
				spt.setForDate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getDealDate()));// 交易日期
				// 交易成交时间
				spt.setForTime(fxSptFwdBean.getTradeTime()); // 交易成交时间
				// 净额清算
				spt.setNettingStatus((fxSptFwdBean.getSettMode().equals(SettMode.Netting)
						|| fxSptFwdBean.getSettMode().equals(SettMode.Central)) ? DictConstants.YesNo.YES
								: DictConstants.YesNo.NO);
				// 交易模式
				if (fxSptFwdBean.getDealMode() != null) {
					if (StringUtil.isNotEmpty(fxSptFwdBean.getDealMode().toString())) {
						if ("Enquiry".equals(fxSptFwdBean.getDealMode().toString())) {// 询价
							spt.setTradingModel("02");
						} else if ("Competition".equals(fxSptFwdBean.getDealMode().toString())) {// 竟价
							spt.setTradingModel("01");
						} else if ("Married".equals(fxSptFwdBean.getDealMode().toString())) {
							spt.setTradingModel("05");
						}
					}
				}
				// 交易方式
				if (fxSptFwdBean.getTradeMethod() != null) {
					spt.setTradingType(StringUtils.trimToEmpty(fxSptFwdBean.getTradeMethod().toString()));
				}
				//货币对
				spt.setCurrencyPair(fxSptFwdBean.getCcypair());
				//交易货币
				spt.setOpicsccy(fxSptFwdBean.getCurrencyInfo().getCcy());
				//对应货币
				spt.setOpicsctrccy(fxSptFwdBean.getCurrencyInfo().getContraCcy());
				// 买方金额
				spt.setBuyAmount(fxSptFwdBean.getCurrencyInfo().getAmt());
				// 买卖金额
				spt.setSellAmount(fxSptFwdBean.getCurrencyInfo().getContraAmt());
				// 起息日
				spt.setValueDate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getValueDate()));// 起息日

				// 以下是清算信息

				// 本方清算币种
				spt.setBuyCurreny(fxSptFwdBean.getCurrencyInfo().getCcy());
				// 开户行名称 110
				spt.setBankName1(fxSptFwdBean.getSettlement().getBankName());
				// 开户行BIC 138
				spt.setBankBic1(fxSptFwdBean.getSettlement().getBankOpenNo());
				// 收款行名称 23
				spt.setDueBank1(fxSptFwdBean.getSettlement().getAcctName());
				// 收款行BIC CODE 16
				spt.setDueBankName1(fxSptFwdBean.getSettlement().getAcctOpenNo());
				// 收款账号 15
//				spt.setDueBankAccount1(fxSptFwdBean.getSettlement().getAcctNo());
				// 中间行名称 209
				spt.setIntermediaryBankName1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankName(), ""));
				// 中间行BIC 211
				spt.setIntermediaryBankBicCode1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankBicCode(), ""));
				// 中间行行号 213
				spt.setIntermediaryBankAcctNo1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankAcctNo(), ""));
				// 资金开户行 207
				spt.setCapitalBank1(fxSptFwdBean.getSettlement().getBankName());
				// 开户行账号xqq
				spt.setBankAccount1(fxSptFwdBean.getSettlement().getBankOpenNo());
				// 资金账户户名110
				spt.setCapitalBankName1(fxSptFwdBean.getSettlement().getAcctName());
				// 支付系统行号
				spt.setCnapsCode1(fxSptFwdBean.getSettlement().getBankAcctNo());
				// 资金账号
				spt.setCapitalAccount1(fxSptFwdBean.getSettlement().getAcctNo());
				if ("CNY".equalsIgnoreCase(fxSptFwdBean.getSettlement().getSettCcy())) {
					// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
					spt.setBankName1(fxSptFwdBean.getSettlement().getAcctName());
					spt.setDueBank1(fxSptFwdBean.getSettlement().getBankName());
				}

				// 对手方清算币种
				spt.setSellCurreny(fxSptFwdBean.getCurrencyInfo().getContraCcy());
				// 开户行名称 110
				spt.setBankName2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				// 开户行BIC 138
				spt.setBankBic2(fxSptFwdBean.getCounterPartySettlement().getBankOpenNo());
				// 收款行名称 23
				spt.setDueBank2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
				// 收款行BIC CODE 16
				spt.setDueBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctOpenNo());
				// 收款账号 15
//				spt.setDueBankAccount2(fxSptFwdBean.getCounterPartySettlement().getAcctNo());
				// 中间行名称 209
				spt.setIntermediaryBankName2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankName(), ""));
				// 中间行BIC 211
				spt.setIntermediaryBankBicCode2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankBicCode(), ""));
				// 中间行行号 213
				spt.setIntermediaryBankAcctNo2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankAcctNo(), ""));
				// 资金开户行 207
				spt.setCapitalBank2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				// 开户行账号xqq
				spt.setBankAccount2(fxSptFwdBean.getCounterPartySettlement().getBankOpenNo());
				// 资金账户户名110
				spt.setCapitalBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
				// 支付系统行号
				spt.setCnapsCode2(fxSptFwdBean.getCounterPartySettlement().getBankAcctNo());
				// 资金账号
				spt.setCapitalAccount2(fxSptFwdBean.getCounterPartySettlement().getAcctNo());
				if ("CNY".equalsIgnoreCase(fxSptFwdBean.getCounterPartySettlement().getSettCcy())) {
					// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
					spt.setBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
					spt.setDueBank2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				}
				// 延期交易
				spt.setDelaydElivind(fxSptFwdBean.getSettlType());
				//特殊处理modify by shenzl 20190807
				//针对交易币种和基础货币不想等的情况进行转换
				//if(!fxSptFwdBean.getCurrencyInfo().getCcy().equals(fxSptFwdBean.getCurrencyInfo().getCcy1())) {
					//方向
					//spt.setBuyDirection(spt.getBuyDirection().equals(PsEnum.S.name()) ? PsEnum.P.name():PsEnum.S.name());
				//}else{
					//清算货币方向调换
					//spt.setBuyCurreny(fxSptFwdBean.getCurrencyInfo().getContraCcy());
					//spt.setSellCurreny(fxSptFwdBean.getCurrencyInfo().getCcy());
				//}
				// 结算方式
				spt.setDeliveryType(fxSptFwdBean.getDeliveryType());
				

				// 查询本方交易员
				spt.setDealer(fxSptFwdBean.getInstitutionInfo().getTradeName());
				spt.setApproveStatus("3");
				spt.setApplyProd(TradeConstants.MarketIndicator.FXSPT);

				TaUser user = cfetsService.userMapping(spt, spt.getDealer());
				if (null == user) {
					spt.setDealer(SystemProperties.cfetsDefautUserId);
					spt.setInstId(SystemProperties.cfetsDefautUserInstId);
				} else {
					spt.setDealer(user.getUserId());
					spt.setInstId(user.getInstId());
				}

				//维护前置产品代码--即期交易
				spt.setfPrdCode(TradeConstants.ProductCode.RMBSPT);
//				cfetsfxSptMapper.addSpot(spt);
				settlsBean = tradeSettls(fxSptFwdBean);

				settlsBean.setDealNo(spt.getTicketId());
				settlsBean.setTradeType("SPT");
				map.put("feDealNo",fxSptFwdBean.getExecId().replaceAll("-", "").trim());
				map.put("vDate",(new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getValueDate()));
				map.put("dealDate",(new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getDealDate()));
				map.put("dealTime", fxSptFwdBean.getTradeTime());
				map.put("ps",String.valueOf(fxSptFwdBean.getPs()));
				map.put("ccy",fxSptFwdBean.getCurrencyInfo().getCcy());
				map.put("ccyAmt",String.valueOf(fxSptFwdBean.getCurrencyInfo().getAmt()));
				map.put("ctrCcy",fxSptFwdBean.getCurrencyInfo().getContraCcy());
				map.put("ctrAmt",String.valueOf(fxSptFwdBean.getCurrencyInfo().getContraAmt()));
				map.put("serverName", "SL_CFETS_SPOT");
				cfetsfxSptMapper.insertSptToOpics(map);
				retCode = (String)map.get("retCode");
				retMsg = (String)map.get("retMsg");
				if(!"999".equals(retCode)) {
					rtb = new RetBean(RetStatusEnum.F, retCode+retMsg, "交易发送失败");
				}else {
					rtb = new RetBean(RetStatusEnum.S, retCode+retMsg, "交易发送成功");
				}
			} else {
				logger.info("外汇远期输入对象：" + fxSptFwdBean);
				IfsCfetsfxFwd fwd = new IfsCfetsfxFwd();
				fwd.setDealSource("02");
				// 交易状态
				if (fxSptFwdBean.getStatus() != null) {
					fwd.setDealTransType(StringUtils.trimToEmpty(fxSptFwdBean.getStatus().toString()));
				} else {
					fwd.setDealTransType("1");
				}

				// 交易模式
				if (fxSptFwdBean.getDealMode() != null) {
					if (StringUtil.isNotEmpty(fxSptFwdBean.getDealMode().toString())) {
						if ("Enquiry".equals(fxSptFwdBean.getDealMode().toString())) {// 询价
							fwd.setTradingModel("02");
						} else if ("Competition".equals(fxSptFwdBean.getDealMode().toString())) {// 竟价
							fwd.setTradingModel("01");
						} else if ("Married".equals(fxSptFwdBean.getDealMode().toString())) {
							fwd.setTradingModel("05");
						}
					}
				}
				// 交易方式
				if (fxSptFwdBean.getTradeMethod() != null) {
					fwd.setTradingType(StringUtils.trimToEmpty(fxSptFwdBean.getTradeMethod().toString()));
				}
				//期限代码
				fwd.setTenorCode(fxSptFwdBean.getSettlType());
//				fwd.setTenorCode(fxSptFwdBean.getSettlTypeCode());
				// 成交单编号
				fwd.setTicketId(cfetsService.getTicketId("FWD"));
				// 成交单编号
				fwd.setContractId(fxSptFwdBean.getExecId());

				//交易后确认标识
				fwd.setCfetsCnfmIndicator(fxSptFwdBean.getCfetsCnfmIndicator());
				// 对手方交易员
				fwd.setCounterpartyDealer(fxSptFwdBean.getContraPatryInstitutionInfo().getTradeName());
				// 交易方向
				fwd.setBuyDirection(fxSptFwdBean.getPs() == null ? "" : fxSptFwdBean.getPs().toString());// 交易方向
				// 本方机构
				fwd.setInstId(
						cfetsService.getLocalInstitution(fwd,null, fxSptFwdBean.getInstitutionInfo().getInstitutionId()));
				// 对手方机构
				fwd.setCounterpartyInstId(cfetsService.getLocalInstitution(fwd,null,
						fxSptFwdBean.getContraPatryInstitutionInfo().getInstitutionId()));
				// 汇率
				fwd.setPrice(fxSptFwdBean.getCurrencyInfo().getSpotRate());
				// 汇率点差
				BigDecimal spread = fxSptFwdBean.getCurrencyInfo().getPoints()==null?new BigDecimal("0"):fxSptFwdBean.getCurrencyInfo().getPoints();
				if("JPY.CNY".equalsIgnoreCase(fxSptFwdBean.getCcypair())) {
					fwd.setSpread(String.valueOf(spread.divide(new BigDecimal("100")).doubleValue()));
				}else if("USD.JPY".equalsIgnoreCase(fxSptFwdBean.getCcypair()) || "EUR.JPY".equalsIgnoreCase(fxSptFwdBean.getCcypair())) {
					fwd.setSpread(String.valueOf(spread.multiply(new BigDecimal("100")).doubleValue()));
				}else {
					fwd.setSpread(String.valueOf(spread));
				}
				// 成交汇率
				BigDecimal price = fxSptFwdBean.getCurrencyInfo().getRate() == null ? new BigDecimal("0") : fxSptFwdBean.getCurrencyInfo().getRate();
				fwd.setExchangeRate(String.valueOf(price.doubleValue()));

				// 交易日期
				fwd.setForDate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getDealDate()));
				// 交易时间
				fwd.setForTime(fxSptFwdBean.getTradeTime());
				//货币对
				fwd.setCurrencyPair(fxSptFwdBean.getCcypair());
				//交易货币
				fwd.setOpicsccy(fxSptFwdBean.getCurrencyInfo().getCcy());
				//对应货币
				fwd.setOpicsctrccy(fxSptFwdBean.getCurrencyInfo().getContraCcy());
				// 买方金额
				fwd.setBuyAmount(fxSptFwdBean.getCurrencyInfo().getAmt());
				// 卖方金额
				fwd.setSellAmount(fxSptFwdBean.getCurrencyInfo().getContraAmt());
				// 起息日
				fwd.setValueDate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getValueDate()));
				// 计算期限
//				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//				String startDateS = sdf.format(fxSptFwdBean.getDealDate());
//				String endDateS = sdf.format(fxSptFwdBean.getValueDate());
				fwd.setFwdPeriod(String.valueOf(DateUtil.daysBetween(fxSptFwdBean.getDealDate(), fxSptFwdBean.getValueDate())));
				// 净额清算
				fwd.setNettingStatus((fxSptFwdBean.getSettMode().equals(SettMode.Netting)
						|| fxSptFwdBean.getSettMode().equals(SettMode.Central)) ? DictConstants.YesNo.YES
								: DictConstants.YesNo.NO);
				// 以下是清算信息
				// 本方清算币种
				fwd.setBuyCurreny(fxSptFwdBean.getCurrencyInfo().getCcy());
				// 开户行名称 110
				fwd.setBankName1(fxSptFwdBean.getSettlement().getBankName());
				// 开户行BIC 138
				fwd.setBankBic1(fxSptFwdBean.getSettlement().getBankOpenNo());
				// 收款行名称 23
				fwd.setDueBank1(fxSptFwdBean.getSettlement().getAcctName());
				// 收款行BIC CODE 16
				fwd.setDueBankName1(fxSptFwdBean.getSettlement().getAcctOpenNo());
				// 收款账号 15
//				fwd.setDueBankAccount1(fxSptFwdBean.getSettlement().getAcctNo());
				// 中间行名称 209
				fwd.setIntermediaryBankName1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankName(), ""));
				// 中间行BIC 211
				fwd.setIntermediaryBankBicCode1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankBicCode(), ""));
				// 中间行行号 213
				fwd.setIntermediaryBankAcctNo1(
						StringUtils.defaultIfEmpty(fxSptFwdBean.getSettlement().getIntermediaryBankAcctNo(), ""));
				// 资金开户行 207
				fwd.setCapitalBank1(fxSptFwdBean.getSettlement().getBankName());
				// xqq
				fwd.setBankAccount1(fxSptFwdBean.getSettlement().getBankOpenNo());
				// 资金账户户名110
				fwd.setCapitalBankName1(fxSptFwdBean.getSettlement().getAcctName());
				// 支付系统行号
				fwd.setCnapsCode1(fxSptFwdBean.getSettlement().getAcctOpenNo());
				// 资金账号
				fwd.setCapitalAccount1(fxSptFwdBean.getSettlement().getAcctNo());
				if ("CNY".equalsIgnoreCase(fxSptFwdBean.getSettlement().getSettCcy())) {
					// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
					fwd.setBankName1(fxSptFwdBean.getSettlement().getAcctName());
					fwd.setDueBank1(fxSptFwdBean.getSettlement().getBankName());
				}

				// 结算方式
				fwd.setDeliveryType(fxSptFwdBean.getDeliveryType());

				// 对手方清算币种
				fwd.setSellCurreny(fxSptFwdBean.getCurrencyInfo().getContraCcy());
				// 开户行名称 110
				fwd.setBankName2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				// 开户行BIC 138
				fwd.setBankBic2(fxSptFwdBean.getCounterPartySettlement().getBankOpenNo());
				// 收款行名称 23
				fwd.setDueBank2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
				// 收款行BIC CODE 16
				fwd.setDueBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctOpenNo());
				// 收款账号 15
//				fwd.setDueBankAccount2(fxSptFwdBean.getCounterPartySettlement().getAcctNo());
				// 中间行名称 209
				fwd.setIntermediaryBankName2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankName(), ""));
				// 中间行BIC 211
				fwd.setIntermediaryBankBicCode2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankBicCode(), ""));
				// 中间行行号 213
				fwd.setIntermediaryBankAcctNo2(StringUtils
						.defaultIfEmpty(fxSptFwdBean.getCounterPartySettlement().getIntermediaryBankAcctNo(), ""));
				// 资金开户行 207
				fwd.setCapitalBank2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				// xqq
				fwd.setBankAccount1(fxSptFwdBean.getCounterPartySettlement().getBankOpenNo());
				// 资金账户户名110
				fwd.setCapitalBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
				// 支付系统行号
				fwd.setCnapsCode2(fxSptFwdBean.getCounterPartySettlement().getBankAcctNo());
				// 资金账号
				fwd.setCapitalAccount2(fxSptFwdBean.getCounterPartySettlement().getAcctNo());
				if ("CNY".equalsIgnoreCase(fxSptFwdBean.getCounterPartySettlement().getSettCcy())) {
					// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
					fwd.setBankName2(fxSptFwdBean.getCounterPartySettlement().getAcctName());
					fwd.setDueBank2(fxSptFwdBean.getCounterPartySettlement().getBankName());
				}
				//特殊处理
				//获取本方的是Maker还是Taker
				if(!fxSptFwdBean.getCurrencyInfo().getCcy().equals(fxSptFwdBean.getCurrencyInfo().getCcy1())) {
					//调换方向
					fwd.setBuyDirection(fwd.getBuyDirection().equals(PsEnum.S.name()) ? PsEnum.P.name():PsEnum.S.name());
				}else{
					//调换清算货币
					fwd.setBuyCurreny(fxSptFwdBean.getCurrencyInfo().getContraCcy());
					fwd.setSellCurreny(fxSptFwdBean.getCurrencyInfo().getCcy());
				}

				// 根据编号查询对应的交易员
				fwd.setDealer(fxSptFwdBean.getInstitutionInfo().getTradeName());
				fwd.setApproveStatus("3");
				fwd.setApplyProd(TradeConstants.MarketIndicator.FXFOW);
				TaUser user = cfetsService.userMapping(fwd, fwd.getDealer());
				if (null == user) {
					fwd.setDealer(SystemProperties.cfetsDefautUserId);
					fwd.setInstId(SystemProperties.cfetsDefautUserInstId);
				} else {
					fwd.setDealer(user.getUserId());
					fwd.setInstId(user.getInstId());
				}
				//维护前置产品代码--远期交易
				fwd.setfPrdCode(TradeConstants.ProductCode.RMBFWD);
//				cfetsfxFwdMapper.addCredit(fwd);
				settlsBean = tradeSettls(fxSptFwdBean);
				settlsBean.setDealNo(fwd.getTicketId());
				settlsBean.setTradeType("FWD");
				map.put("feDealNo",fxSptFwdBean.getExecId().replaceAll("-", "").trim());
				map.put("vDate",(new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getValueDate()));
				map.put("dealDate",(new SimpleDateFormat("yyyy-MM-dd")).format(fxSptFwdBean.getDealDate()));
				map.put("dealTime", fxSptFwdBean.getTradeTime());
				map.put("ps",String.valueOf(fxSptFwdBean.getPs()));
				map.put("ccy",fxSptFwdBean.getCurrencyInfo().getCcy());
				map.put("ccyAmt",String.valueOf(fxSptFwdBean.getCurrencyInfo().getAmt()));
				map.put("ctrCcy",fxSptFwdBean.getCurrencyInfo().getContraCcy());
				map.put("ctrAmt",String.valueOf(fxSptFwdBean.getCurrencyInfo().getContraAmt()));
				map.put("serverName", "SL_CFETS_FWD");
				cfetsfxFwdMapper.insertFwdToOpics(map);
				retCode = (String)map.get("retCode");
				retMsg = (String)map.get("retMsg");
				if(!"999".equals(retCode)) {
					rtb = new RetBean(RetStatusEnum.F, retCode+retMsg, "交易发送失败");
				}else {
					rtb = new RetBean(RetStatusEnum.S, retCode+retMsg, "交易发送成功");
				}
			}
//			tradeSettlsMapper.insert(settlsBean);
		} catch (Exception e) {
			logger.error("外汇即期、远期下行交易异常", e);
			rtb = new RetBean(RetStatusEnum.F, retCode + retMsg + e.getMessage(), "交易发送失败");
		}
		logger.info("外汇即期远期输出对象：" + rtb);
		return rtb;
	}



	@Override
	public RetBean send(FxSwapBean fxSwapBean, CfetsPackMsgBean cfetsPackMsg) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			logger.info("保存下行交易FXSWAP报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易FXSWAP报文异常:", e1);
		}

		logger.info("外汇掉期输入对象：" + fxSwapBean);
		RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		String retCode = null;
		String retMsg = null;
		try {
			IfsCfetsfxSwap swap = new IfsCfetsfxSwap();
			swap.setDealSource("02");
			// 交易状态
			if (fxSwapBean.getStatus() != null) {
				swap.setDealTransType(StringUtils.trimToEmpty(fxSwapBean.getStatus().toString()));
			} else {
				swap.setDealTransType("1");
			}

			// 交易模式
			if (fxSwapBean.getDealMode() != null) {
				if (StringUtil.isNotEmpty(fxSwapBean.getDealMode().toString())) {
					if ("Enquiry".equals(fxSwapBean.getDealMode().toString())) {// 询价
						swap.setTradingModel("02");
					} else if ("Competition".equals(fxSwapBean.getDealMode().toString())) {// 竟价
						swap.setTradingModel("01");
					} else if ("Married".equals(fxSwapBean.getDealMode().toString())) {
						swap.setTradingModel("05");
					}
				}
			}

			// 交易方式
			if (fxSwapBean.getTradeMethod() != null) {
				swap.setTradingType(StringUtils.trimToEmpty(fxSwapBean.getTradeMethod().toString()));
			}
			// 成交单编号
			swap.setTicketId(cfetsService.getTicketId("SWAP"));
			// 成交单编号
			swap.setContractId(fxSwapBean.getExecId());
			//交易后确认标识
			swap.setCfetsCnfmIndicator(fxSwapBean.getCfetsCnfmIndicator());
			// 本方机构
			swap.setInstId(cfetsService.getLocalInstitution(swap,null, fxSwapBean.getInstitutionInfo().getInstitutionId()));
			// 对手方机构
			swap.setCounterpartyInstId(
					cfetsService.getLocalInstitution(swap,null, fxSwapBean.getContraPatryInstitutionInfo().getInstitutionId()));
			// 交易日期
			swap.setForDate(fxSwapBean.getDealDate() == null ? ""
					: (new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getDealDate()));
			// 交易成交时间
			swap.setForTime(fxSwapBean.getTradeTime());
			// 交易方向
			swap.setDirection(fxSwapBean.getPs() == null ? "" : fxSwapBean.getPs().toString());
			// 对手方交易员名称
			swap.setCounterpartyDealer(fxSwapBean.getContraPatryInstitutionInfo().getTradeName());
			//货币对
			swap.setCurrencyPair(fxSwapBean.getCcypair());
			//交易货币
			swap.setOpicsccy(fxSwapBean.getCurrencyInfo().getCcy());
			//对应货币
			swap.setOpicsctrccy(fxSwapBean.getCurrencyInfo().getContraCcy());
			// SPOT汇率
			swap.setPrice(fxSwapBean.getCurrencyInfo().getSpotRate() == null ? ""
					: fxSwapBean.getCurrencyInfo().getSpotRate().toString());
			// 净额清算
			swap.setNettingStatus((fxSwapBean.getSettMode().equals(SettMode.Netting)
					|| fxSwapBean.getSettMode().equals(SettMode.Central)) ? DictConstants.YesNo.YES
							: DictConstants.YesNo.NO);
			// 近端交易币种金额
			swap.setNearPrice(fxSwapBean.getCurrencyInfo().getAmt() == null ? ""
					: fxSwapBean.getCurrencyInfo().getAmt().toString());
			// 近端汇率
			swap.setNearRate(String.valueOf(fxSwapBean.getCurrencyInfo().getRate()));
			// 近端对手方交易币种金额
			swap.setNearReversePrice(fxSwapBean.getCurrencyInfo().getContraAmt() == null ? ""
					: fxSwapBean.getCurrencyInfo().getContraAmt().toString());
			// 近端汇率点差
			BigDecimal nearpoints = fxSwapBean.getCurrencyInfo().getPoints()==null?new BigDecimal("0"):fxSwapBean.getCurrencyInfo().getPoints();
			nearpoints = nearpoints.divide(new BigDecimal(10000));
			if("JPY.CNY".equalsIgnoreCase(fxSwapBean.getCcypair())) {
				swap.setNearSpread(String.valueOf(nearpoints.divide(new BigDecimal("100")).doubleValue()));
			}else if("USD.JPY".equalsIgnoreCase(fxSwapBean.getCcypair()) || "EUR.JPY".equalsIgnoreCase(fxSwapBean.getCcypair())) {
				swap.setNearSpread(String.valueOf(nearpoints.multiply(new BigDecimal("100")).doubleValue()));
			}else {
				swap.setNearSpread(String.valueOf(nearpoints));
			}
			// 远端金额
			swap.setFwdPrice(fxSwapBean.getFarCurrencyInfo().getAmt() == null ? ""
					: fxSwapBean.getFarCurrencyInfo().getAmt().toString());
			// 远端汇率
			swap.setFwdRate(String.valueOf(fxSwapBean.getFarCurrencyInfo().getRate()));
			// 远端对应货币金额
			swap.setFwdReversePrice(fxSwapBean.getFarCurrencyInfo().getContraAmt().toString());
			// 近端起息日
			swap.setNearValuedate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getValueDate()));
			// 远端起息日
			swap.setFwdValuedate((new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getFarValueDate()));
			// 远端汇率点差
			BigDecimal points = fxSwapBean.getFarCurrencyInfo().getPoints()==null?new BigDecimal("0"):fxSwapBean.getFarCurrencyInfo().getPoints();
			points = points.divide(new BigDecimal(10000));
			if("JPY.CNY".equalsIgnoreCase(fxSwapBean.getCcypair())) {
				swap.setSpread(String.valueOf(points.divide(new BigDecimal("100")).doubleValue()));
			}else if("USD.JPY".equalsIgnoreCase(fxSwapBean.getCcypair())|| "EUR.JPY".equalsIgnoreCase(fxSwapBean.getCcypair())) {
				swap.setSpread(String.valueOf(points.multiply(new BigDecimal("100")).doubleValue()));
			}else {
				swap.setSpread(String.valueOf(points));
			}
			// 结算方式
			swap.setDeliveryType(fxSwapBean.getDeliveryType());

			swap.setTenorCode(fxSwapBean.getCurrencyInfo().getLegSettlType());
			swap.setFwdTenorCode(fxSwapBean.getFarCurrencyInfo().getLegSettlType());

			// 以下是清算信息--近端
			// 本方清算币种
			swap.setBuyCurreny(fxSwapBean.getCurrencyInfo().getCcy());
			// 开户行名称 110
			swap.setBankName1(fxSwapBean.getSettlement().getBankName());
			// 开户行BIC 138
			swap.setBankBic1(fxSwapBean.getSettlement().getBankOpenNo());
			// 资金开户行 207
			swap.setBankAccount1(fxSwapBean.getSettlement().getBankOpenNo());
			// 收款行名称 23
			swap.setDueBank1(fxSwapBean.getSettlement().getAcctName());
			// 收款行BIC CODE 16
			swap.setDueBankName1(fxSwapBean.getSettlement().getAcctOpenNo());
			// 收款账号 15
			swap.setDueBankAccount1(fxSwapBean.getSettlement().getAcctNo());
			// 中间行名称 209
			swap.setIntermediaryBankName1(
					StringUtils.defaultIfEmpty(fxSwapBean.getSettlement().getIntermediaryBankName(), ""));
			// 中间行BIC 211
			swap.setIntermediaryBankBicCode1(
					StringUtils.defaultIfEmpty(fxSwapBean.getSettlement().getIntermediaryBankBicCode(), ""));
			// 中间行行号 213
			swap.setIntermediaryBankAcctNo1(
					StringUtils.defaultIfEmpty(fxSwapBean.getSettlement().getIntermediaryBankAcctNo(), ""));
			// 资金开户行 207
			swap.setCapitalBank1(fxSwapBean.getSettlement().getBankName());
			// 资金账户户名110
			swap.setCapitalBankName1(fxSwapBean.getSettlement().getAcctName());
			// 支付系统行号
			swap.setCnapsCode1(fxSwapBean.getSettlement().getBankAcctNo());
			// 资金账号
			swap.setCapitalAccount1(fxSwapBean.getSettlement().getAcctNo());
			if ("CNY".equalsIgnoreCase(fxSwapBean.getSettlement().getSettCcy())) {
				// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
				swap.setBankName1(fxSwapBean.getSettlement().getBankName());
				swap.setDueBank1(fxSwapBean.getSettlement().getAcctName());
			}

			// 对手方清算币种
			swap.setSellCurreny(fxSwapBean.getCurrencyInfo().getContraCcy());
			// 开户行名称 110
			swap.setBankName2(fxSwapBean.getCounterPartySettlement().getBankName());
			// 开户行BIC 138
			swap.setBankBic2(fxSwapBean.getCounterPartySettlement().getBankOpenNo());
			// 资金开户行 207
			swap.setBankAccount2(fxSwapBean.getCounterPartySettlement().getBankOpenNo());
			// 收款行名称 23
			swap.setDueBank2(fxSwapBean.getCounterPartySettlement().getAcctName());
			// 收款行BIC CODE 16
			swap.setDueBankName2(fxSwapBean.getCounterPartySettlement().getAcctOpenNo());
			// 收款账号 15
			swap.setDueBankAccount2(fxSwapBean.getCounterPartySettlement().getAcctNo());
			// 中间行名称 209
			swap.setIntermediaryBankName2(
					StringUtils.defaultIfEmpty(fxSwapBean.getCounterPartySettlement().getIntermediaryBankName(), ""));
			// 中间行BIC 211
			swap.setIntermediaryBankBicCode2(StringUtils
					.defaultIfEmpty(fxSwapBean.getCounterPartySettlement().getIntermediaryBankBicCode(), ""));
			// 中间行行号 213
			swap.setIntermediaryBankAcctNo2(
					StringUtils.defaultIfEmpty(fxSwapBean.getCounterPartySettlement().getIntermediaryBankAcctNo(), ""));
			// 资金开户行 207
			swap.setCapitalBank2(fxSwapBean.getCounterPartySettlement().getAcctName());
			swap.setBankAccount2(fxSwapBean.getCounterPartySettlement().getBankOpenNo());
			// 资金账户户名110
			swap.setCapitalBankName2(fxSwapBean.getCounterPartySettlement().getAcctName());
			// 支付系统行号
			swap.setCnapsCode2(fxSwapBean.getCounterPartySettlement().getBankAcctNo());
			// 资金账号
			swap.setCapitalAccount2(fxSwapBean.getCounterPartySettlement().getAcctNo());
			if ("CNY".equalsIgnoreCase(fxSwapBean.getCounterPartySettlement().getSettCcy())) {
				// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
				swap.setBankName2(fxSwapBean.getCounterPartySettlement().getBankName());
				swap.setDueBank2(fxSwapBean.getCounterPartySettlement().getAcctName());
			}

			// 以下是清算信息--远端
			// 本方清算币种
			swap.setBuyCurreny1(fxSwapBean.getFarCurrencyInfo().getContraCcy());
			// 开户行名称 110
			swap.setBankName3(fxSwapBean.getFarSettlement().getBankName());
			// 开户行BIC 138
			swap.setBankBic3(fxSwapBean.getFarSettlement().getBankOpenNo());
			// 资金开户行 207
			swap.setBankAccount3(fxSwapBean.getFarSettlement().getBankOpenNo());
			// 收款行名称 23
			swap.setDueBank3(fxSwapBean.getFarSettlement().getAcctName());
			// 收款行BIC CODE 16
			swap.setDueBankName3(fxSwapBean.getFarSettlement().getAcctOpenNo());
			// 收款账号 15
			swap.setDueBankAccount3(fxSwapBean.getFarSettlement().getAcctNo());
			// 中间行名称 209
			swap.setIntermediaryBankName3(
					StringUtils.defaultIfEmpty(fxSwapBean.getFarSettlement().getIntermediaryBankName(), ""));
			// 中间行BIC 211
			swap.setIntermediaryBankBicCode3(
					StringUtils.defaultIfEmpty(fxSwapBean.getFarSettlement().getIntermediaryBankBicCode(), ""));
			// 中间行行号 213
			swap.setIntermediaryBankAcctNo3(
					StringUtils.defaultIfEmpty(fxSwapBean.getFarSettlement().getIntermediaryBankAcctNo(), ""));

			if ("CNY".equalsIgnoreCase(fxSwapBean.getFarSettlement().getSettCcy())) {
				// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
				swap.setBankName3(fxSwapBean.getFarSettlement().getBankName());
				swap.setDueBank3(fxSwapBean.getFarSettlement().getAcctName());
			}
			// 资金开户行 207
			// swap.setCapitalBank3(fxSwapBean.getFarSettlement().getAcctName());
			// 资金账户户名110
			// swap.setCapitalBankName3(fxSwapBean.getFarSettlement().getAcctName());
			// 支付系统行号
			// swap.setCnapsCode3(fxSwapBean.getFarSettlement().getAcctNo());
			// 资金账号
//			 swap.setCapitalAccount3(fxSwapBean.getFarSettlement().getAcctNo());
			// 对手方清算币种
			swap.setSellCurreny1(fxSwapBean.getFarCurrencyInfo().getCcy());
			// 开户行名称 110
			swap.setBankName4(fxSwapBean.getFarCounterPartySettlement().getBankName());
			// 开户行BIC 138
			swap.setBankBic4(fxSwapBean.getFarCounterPartySettlement().getBankOpenNo());
			// 资金开户行 207
			swap.setBankAccount4(fxSwapBean.getFarCounterPartySettlement().getBankAcctNo());
			// 收款行名称 23
			swap.setDueBank4(fxSwapBean.getFarCounterPartySettlement().getAcctName());
			// 收款行BIC CODE 16
			swap.setDueBankName4(fxSwapBean.getFarCounterPartySettlement().getAcctOpenNo());
			// 收款账号 15
			swap.setDueBankAccount4(fxSwapBean.getFarCounterPartySettlement().getAcctNo());
			// 中间行名称 209
			swap.setIntermediaryBankName4(StringUtils
					.defaultIfEmpty(fxSwapBean.getFarCounterPartySettlement().getIntermediaryBankName(), ""));
			// 中间行BIC 211
			swap.setIntermediaryBankBicCode4(StringUtils
					.defaultIfEmpty(fxSwapBean.getFarCounterPartySettlement().getIntermediaryBankBicCode(), ""));
			// 中间行行号 213
			swap.setIntermediaryBankAcctNo4(StringUtils
					.defaultIfEmpty(fxSwapBean.getFarCounterPartySettlement().getIntermediaryBankAcctNo(), ""));
			if ("CNY".equalsIgnoreCase(fxSwapBean.getFarCounterPartySettlement().getSettCcy())) {
				// 人民币币种清算信息资金开户行与资金账户名由于页面中文描述反了，所以下行调换位置
				swap.setBankName4(fxSwapBean.getFarCounterPartySettlement().getBankName());
				swap.setDueBank4(fxSwapBean.getFarCounterPartySettlement().getAcctName());
			}
			// 资金开户行 207
			// swap.setCapitalBank4(fxSwapBean.getFarCounterPartySettlement().getAcctName());
			// 资金账户户名110
			// swap.setCapitalBankName4(fxSwapBean.getFarCounterPartySettlement().getAcctName());
			// 支付系统行号
			// swap.setCnapsCode4(fxSwapBean.getFarCounterPartySettlement().getBankAcctNo());
			// 资金账号
			// swap.setCapitalAccount4(fxSwapBean.getFarCounterPartySettlement().getBankAcctNo());
			// 计算期限
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			String startDateS = sdf.format(fxSwapBean.getValueDate());
//			String endDateS = sdf.format(fxSwapBean.getFarValueDate());
			swap.setTenor(String.valueOf(DateUtil.daysBetween(fxSwapBean.getValueDate(), fxSwapBean.getFarValueDate())));
			//特殊处理
			//获取本方的是Maker还是Taker
			if(!fxSwapBean.getCurrencyInfo().getCcy().equals(fxSwapBean.getCurrencyInfo().getCcy1())) {
				//方向调换
				swap.setDirection(swap.getDirection().equals(PsEnum.S.name()) ? PsEnum.P.name():PsEnum.S.name());
				//清算货币调换
				swap.setBuyCurreny(fxSwapBean.getFarCurrencyInfo().getContraCcy());
				swap.setSellCurreny(fxSwapBean.getFarCurrencyInfo().getCcy());
				swap.setBuyCurreny1(fxSwapBean.getFarCurrencyInfo().getCcy());
				swap.setSellCurreny1(fxSwapBean.getFarCurrencyInfo().getContraCcy());
			}

			// 查询本方交易员
			swap.setDealer(fxSwapBean.getInstitutionInfo().getTradeName());
			swap.setApproveStatus("3");
			swap.setApplyProd(TradeConstants.MarketIndicator.FXSWP);
			
			TaUser user = cfetsService.userMapping(swap, swap.getDealer());
			if (null == user) {
				swap.setDealer(SystemProperties.cfetsDefautUserId);
				swap.setInstId(SystemProperties.cfetsDefautUserInstId);
			} else {
				swap.setDealer(user.getUserId());
				swap.setInstId(user.getInstId());
			}

			//维护前置产品代码--掉期交易
			swap.setfPrdCode(TradeConstants.ProductCode.RMBSWAP);

			cfetsfxSwapMapper.addrmswap(swap);
			//cfets 清算路径保存
			TradeSettlsBean settlsBean = tradeSettls(fxSwapBean);
			settlsBean.setDealNo(swap.getTicketId());
//			tradeSettlsMapper.insert(settlsBean);
			Map<String, String> mapExec = new HashMap<String, String>();
			mapExec.put("feDealNo", fxSwapBean.getExecId().replaceAll("-", "").trim());
			mapExec.put("nvDate", (new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getValueDate()));
			mapExec.put("fvDate", (new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getFarValueDate()));
			mapExec.put("dealDate", fxSwapBean.getDealDate() == null ? ""
					: (new SimpleDateFormat("yyyy-MM-dd")).format(fxSwapBean.getDealDate()));
			mapExec.put("dealTime", fxSwapBean.getTradeTime());
			mapExec.put("ps", String.valueOf(fxSwapBean.getPs()));
			mapExec.put("ccy", fxSwapBean.getCurrencyInfo().getCcy());
			mapExec.put("ccyAmt", fxSwapBean.getCurrencyInfo().getAmt() == null ? ""
					: fxSwapBean.getCurrencyInfo().getAmt().toString());
			mapExec.put("ctrCcy", fxSwapBean.getCurrencyInfo().getContraCcy());
			mapExec.put("ctrAmt", fxSwapBean.getCurrencyInfo().getContraAmt() == null?"":fxSwapBean.getCurrencyInfo().getContraAmt().toString());
			mapExec.put("nccyRate_8", fxSwapBean.getCurrencyInfo().getRate().toString());
			mapExec.put("fccyRate_8", fxSwapBean.getFarCurrencyInfo().getRate().toString());
			System.out.println("!!!!!!!!!近端汇率:"+fxSwapBean.getCurrencyInfo().getRate().toString()+"远端汇率:"+fxSwapBean.getFarCurrencyInfo().getRate().toString());
			cfetsfxSwapMapper.insertSwapToOpics(mapExec);
			retCode = (String)mapExec.get("retCode");
			retMsg = (String)mapExec.get("retMsg");
			if(!"999".equals(retCode)) {
				rtb = new RetBean(RetStatusEnum.F, retCode+retMsg, "交易发送失败");
			}else {
				rtb = new RetBean(RetStatusEnum.S, retCode+retMsg, "交易发送成功");
			}
		} catch (Exception e) {
			logger.error("外汇掉期下行交易异常", e);
			rtb = new RetBean(RetStatusEnum.F, retCode + retMsg + e.getMessage(), "交易发送失败");
		}
		logger.info("外汇掉期输出对象：" + rtb);
		return rtb;
	}

	/**
	 * 即期远期清算路径处理
	 * @param fxSptFwdBean
	 * @return
	 */
	private TradeSettlsBean tradeSettls(FxSptFwdBean fxSptFwdBean) {
		TradeSettlsBean bean = new TradeSettlsBean();
		//近端清算货币
		bean.setSettccy(fxSptFwdBean.getCurrencyInfo().getCcy());
		// 远端清算币种
		bean.setcSettccy(fxSptFwdBean.getCurrencyInfo().getContraCcy());

		if(SettMode.Netting.equals(fxSptFwdBean.getSettMode())){
			// 1-双边净额清算
			bean.setNetGrossInd(String.valueOf(1));
		}else if(SettMode.Normal.equals(fxSptFwdBean.getSettMode())){
			// 2-双边全额清算
			bean.setNetGrossInd(String.valueOf(2));
		}else if(SettMode.Central.equals(fxSptFwdBean.getSettMode())){
			// 3-集中清算
			bean.setNetGrossInd(String.valueOf(3));
		}else if(SettMode.SGE.equals(fxSptFwdBean.getSettMode())){
			// 4-应急录入
			bean.setNetGrossInd(String.valueOf(4));
		}


		//市场标识
		bean.setMarketIndicator(fxSptFwdBean.getMarketIndicator());
		//成交单号
		bean.setExecId(fxSptFwdBean.getExecId());
		// 本方机构 21位码,
		bean.setInstitutionId(fxSptFwdBean.getInstitutionInfo().getInstitutionId());
		// 本方交易员编号
		bean.setTradeId(fxSptFwdBean.getInstitutionInfo().getTradeId());
		// 本方交易员名称
		bean.setTradeName(fxSptFwdBean.getInstitutionInfo().getTradeName());
		// 本方机构简称
		bean.setShortName(fxSptFwdBean.getInstitutionInfo().getShortName());
		// 本方机构全称
		bean.setFullName(fxSptFwdBean.getInstitutionInfo().getFullName());
		// 本方机构角色
		bean.setMarkerTakerTole(fxSptFwdBean.getSettlement().getMarkerTakerRole()==null?"":fxSptFwdBean.getSettlement().getMarkerTakerRole().toString());

		// 对手方机构 21位码,
		bean.setcInstitutionId(fxSptFwdBean.getContraPatryInstitutionInfo().getInstitutionId());
		// 对手方交易员编号
		bean.setcIradeId(fxSptFwdBean.getContraPatryInstitutionInfo().getTradeId());
		// 对手交易员名称
		bean.setcIradeName(fxSptFwdBean.getContraPatryInstitutionInfo().getTradeName());
		// 对手机构简称
		bean.setcShortName(fxSptFwdBean.getContraPatryInstitutionInfo().getShortName());
		// 对手机构全称
		bean.setcFullName(fxSptFwdBean.getContraPatryInstitutionInfo().getFullName());
		// 对手机构角色
		bean.setcMarkerTakerTole(fxSptFwdBean.getCounterPartySettlement().getMarkerTakerRole()==null?"":fxSptFwdBean.getCounterPartySettlement().getMarkerTakerRole().toString());
		bean.setDealNo("");


		if(fxSptFwdBean.getSptFwdType().compareTo(SptFwdTypeEnum.FORWARD) == 0){
			if(fxSptFwdBean.getSettlement().getSettCcy().equals(fxSptFwdBean.getCurrencyInfo().getCcy1())){
				covertNear(bean,fxSptFwdBean.getContraSettlement(),fxSptFwdBean.getCounterPartyContraSettlement());
			}else {
				covertNear(bean,fxSptFwdBean.getSettlement(),fxSptFwdBean.getCounterPartySettlement());
			}
		}else {
			covertNear(bean,fxSptFwdBean.getSettlement(),fxSptFwdBean.getCounterPartySettlement());
		}
		return bean;
	}
	/**
	 * 掉期清算路径
	 * @param fxSwapBean
	 */
	public TradeSettlsBean tradeSettls(FxSwapBean fxSwapBean){
		TradeSettlsBean bean = new TradeSettlsBean();
		//近端清算货币
		bean.setSettccy(fxSwapBean.getCurrencyInfo().getCcy());
		// 远端清算币种
		bean.setcSettccy(fxSwapBean.getCurrencyInfo().getContraCcy());

		if(SettMode.Netting.equals(fxSwapBean.getSettMode())){
			// 1-双边净额清算
			bean.setNetGrossInd(String.valueOf(1));
		}else if(SettMode.Normal.equals(fxSwapBean.getSettMode())){
			// 2-双边全额清算
			bean.setNetGrossInd(String.valueOf(2));
		}else if(SettMode.Central.equals(fxSwapBean.getSettMode())){
			// 3-集中清算
			bean.setNetGrossInd(String.valueOf(3));
		}else if(SettMode.SGE.equals(fxSwapBean.getSettMode())){
			// 4-应急录入
			bean.setNetGrossInd(String.valueOf(4));
		}

		bean.setTradeType("SWAP");
		//市场标识
		bean.setMarketIndicator(fxSwapBean.getMarketIndicator());
		//成交单号
		bean.setExecId(fxSwapBean.getExecId());
		// 本方机构 21位码,
		bean.setInstitutionId(fxSwapBean.getInstitutionInfo().getInstitutionId());
		// 本方交易员编号
		bean.setTradeId(fxSwapBean.getInstitutionInfo().getTradeId());
		// 本方交易员名称
		bean.setTradeName(fxSwapBean.getInstitutionInfo().getTradeName());
		// 本方机构简称
		bean.setShortName(fxSwapBean.getInstitutionInfo().getShortName());
		// 本方机构全称
		bean.setFullName(fxSwapBean.getInstitutionInfo().getFullName());
		// 本方机构角色
		bean.setMarkerTakerTole(fxSwapBean.getSettlement().getMarkerTakerRole()==null?"":fxSwapBean.getSettlement().getMarkerTakerRole().toString());

		// 对手方机构 21位码,
		bean.setcInstitutionId(fxSwapBean.getContraPatryInstitutionInfo().getInstitutionId());
		// 对手方交易员编号
		bean.setcIradeId(fxSwapBean.getContraPatryInstitutionInfo().getTradeId());
		// 对手交易员名称
		bean.setcIradeName(fxSwapBean.getContraPatryInstitutionInfo().getTradeName());
		// 对手机构简称
		bean.setcShortName(fxSwapBean.getContraPatryInstitutionInfo().getShortName());
		// 对手机构全称
		bean.setcFullName(fxSwapBean.getContraPatryInstitutionInfo().getFullName());
		// 对手机构角色
		bean.setcMarkerTakerTole(fxSwapBean.getCounterPartySettlement().getMarkerTakerRole()==null?"":fxSwapBean.getCounterPartySettlement().getMarkerTakerRole().toString());
		bean.setDealNo("");

		//近端 清算信息
		covertNear(bean,fxSwapBean.getSettlement(),fxSwapBean.getCounterPartySettlement());

		//远端 清算信息
		covertFar(bean,fxSwapBean.getFarSettlement(),fxSwapBean.getFarCounterPartySettlement());
		return bean;
	}

	/**
	 * 远端转化
	 * @param bean
	 * @param settlement
	 * @param counterPartySettlement
	 */
	private static void covertFar(TradeSettlsBean bean, FxSettBean settlement, FxSettBean counterPartySettlement) {
		//远端 本方 开户行SWIFT CODE(外币)/开户行行号(人民币)
		bean.setFarBankopenno(settlement.getBankOpenNo());
		//远端 本方 账户行SWIFT CODE/支付系统行号
		bean.setFarAcctopenno(settlement.getAcctOpenNo());
		//远端 本方 中间行名称
		bean.setFarIntermediarybankname(settlement.getIntermediaryBankName());
		//远端 本方 中间行SWIFT CODE
		bean.setFarIntermediarybankbiccode(settlement.getIntermediaryBankBicCode());
		//远端 本方 中间行在开户行的资金账户
		bean.setFarIntermediarybankacctno(settlement.getIntermediaryBankAcctNo());
		//远端 本方 附言
		bean.setFarRemark(settlement.getRemark());
		//远端 本方 开户行名称
		bean.setFarBankname(settlement.getBankName());
		//远端 本方 支付系统行号
		bean.setFarBankacctno(settlement.getBankAcctNo());
		//远端 本方 账户名称
		bean.setFarAcctname(settlement.getAcctName());
		//远端 本方 资金账号
		bean.setFarAcctno(settlement.getAcctNo());


		// 远端 对手方 开户行SWIFT CODE(外币)/开户行行号(人民币)
		bean.setcFarBankopenno(counterPartySettlement.getBankOpenNo());
		//远端 对手方 账户行SWIFT CODE/支付系统行号
		bean.setcFarAcctopenno(counterPartySettlement.getAcctOpenNo());
		//远端 对手方 中间行名称
		bean.setcFarIntermediarybankname(counterPartySettlement.getIntermediaryBankName());
		//远端 对手方 中间行SWIFT CODE
		bean.setcFarIntermediarybankbiccode(counterPartySettlement.getIntermediaryBankBicCode());
		//远端 对手方 中间行在开户行的资金账户
		bean.setcFarIntermediarybankacctno(counterPartySettlement.getIntermediaryBankAcctNo());
		//远端 对手方 附言
		bean.setcFarRemark(counterPartySettlement.getRemark());

		//远端 对手方 开户行名称
		bean.setcFarBankname(counterPartySettlement.getBankName());
		//远端 对手方 支付系统行号
		bean.setcFarBankacctno(counterPartySettlement.getBankAcctNo());
		//远端 对手方 账户名称
		bean.setcFarAcctname(counterPartySettlement.getAcctName());
		//远端 对手方 资金账号
		bean.setcFarAcctno(counterPartySettlement.getAcctNo());
	}

	/**
	 * 近端转化
	 * @param bean
	 * @param settlement
	 * @param counterPartySettlement
	 */
	private static void covertNear(TradeSettlsBean bean,FxSettBean settlement, FxSettBean counterPartySettlement) {

		//近端 本方 开户行SWIFT CODE(外币)/开户行行号(人民币)
		bean.setBankopenno(settlement.getBankOpenNo());
		//近端 本方 账户行SWIFT CODE/支付系统行号
		bean.setAcctopenno(settlement.getAcctOpenNo());
		//近端 本方 中间行名称
		bean.setIntermediarybankname(settlement.getIntermediaryBankName());
		//近端 本方 中间行SWIFT CODE
		bean.setIntermediarybankbiccode(settlement.getIntermediaryBankBicCode());
		//近端 本方 中间行在开户行的资金账户
		bean.setIntermediarybankacctno(settlement.getIntermediaryBankAcctNo());
		//近端 本方 附言
		bean.setRemark(settlement.getRemark());
		//近端 本方 开户行名称
		bean.setBankname(settlement.getBankName());
		//近端 本方 支付系统行号
		bean.setBankacctno(settlement.getBankAcctNo());
		//近端 本方 账户名称
		bean.setAcctname(settlement.getAcctName());
		//近端 本方 资金账号
		bean.setAcctno(settlement.getAcctNo());


		// 近端 对手方 开户行SWIFT CODE(外币)/开户行行号(人民币)
		bean.setcBankopenno(counterPartySettlement.getBankOpenNo());
		//近端 对手方 账户行SWIFT CODE/支付系统行号
		bean.setcAcctopenno(counterPartySettlement.getAcctOpenNo());
		//近端 对手方 中间行名称
		bean.setcIntermediarybankname(counterPartySettlement.getIntermediaryBankName());
		//近端 对手方 中间行SWIFT CODE
		bean.setcIntermediarybankbiccode(counterPartySettlement.getIntermediaryBankBicCode());
		//近端 对手方 中间行在开户行的资金账户
		bean.setcIntermediarybankacctno(counterPartySettlement.getIntermediaryBankAcctNo());
		//近端 对手方 附言
		bean.setcRemark(counterPartySettlement.getRemark());

		//近端 对手方 开户行名称
		bean.setcBankname(counterPartySettlement.getBankName());
		//近端 对手方 支付系统行号
		bean.setcBankacctno(counterPartySettlement.getBankAcctNo());
		//近端 对手方 账户名称
		bean.setcAcctname(counterPartySettlement.getAcctName());
		//近端 对手方 资金账号
		bean.setcAcctno(counterPartySettlement.getAcctNo());
	}

}
