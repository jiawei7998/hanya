package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixSwap;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SwapCrsBean;
import com.singlee.financial.pojo.SwapIrsBean;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.IrsDetailBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.IfsCfetsPackMsgMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 掉期交易业务处理类
 * 
 * ClassName: CfetsImixSwap <br/>
 * Function: 落地外汇中心掉期业务处理类. <br/>
 * Reason: 审批+处理后送入OPICS系统中. <br/>
 * date: 2018-5-30 下午04:38:04 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CfetsImixSwap implements ICfetsImixSwap {
	@Autowired
	private IfsCfetsrmbIrsMapper cfetsrmbIrsMapper;
	@Autowired
	private CfetsRmbService cfetsService;
	@Autowired
	private IfsCfetsPackMsgMapper cfetsPackMsgMapper;
	private static Logger logger = LoggerFactory.getLogger(CfetsImixSwap.class);

	@Resource
	IfsOpicsCustMapper ifsOpicsCustMapper;

	@Override
	public RetBean send(SwapIrsBean swapIrsBean, CfetsPackMsgBean cfetsPackMsg) {

		try {
			logger.info("保存下行交易IRS报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易IRS报文异常:", e1);
		}

		logger.info("利率互换输入对象：" + swapIrsBean.toString());
		RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		try {
			IfsCfetsrmbIrs irs = new IfsCfetsrmbIrs();
			irs.setfPrdCode(TradeConstants.ProductCode.IRS);
			SettBaseBean fixedIrs = swapIrsBean.getSettlement(); // 固定利率方
			SettBaseBean floatIrs = swapIrsBean.getCounterPartySettlement(); // 浮动利率方
			irs.setMyDir(PsEnum.P.toString()); // 本方方向
			irs.setOppoDir(PsEnum.S.toString()); // 对手方方向

			if (!swapIrsBean.getPs().equals(PsEnum.P)) {
				irs.setMyDir(PsEnum.S.toString()); // 本方方向
				irs.setOppoDir(PsEnum.P.toString()); // 对手方方向
//				fixedIrs = swapIrsBean.getContraPartySettInfo();
//				floatIrs = swapIrsBean.getSettInfo();
			}

			Date startDate = swapIrsBean.getIrsInfo().getStartDate();
			Date endDate = swapIrsBean.getIrsInfo().getEndDate();
			String tenor = DateUtil.daysBetween(startDate, endDate) + "";

			// 交易状态
			if(swapIrsBean.getStatus()!=null) {
				irs.setDealTransType(StringUtils.trimToEmpty(swapIrsBean.getStatus().toString()));
			}else {
				irs.setDealTransType("1");
			}

			// 交易模式
			if(swapIrsBean.getDealMode()!=null) {
				if (StringUtil.isNotEmpty(swapIrsBean.getDealMode().toString())) {
					if ("Enquiry".equals(swapIrsBean.getDealMode().toString())) {// 询价
						irs.setTradingModel("02");
					} else if ("Competition".equals(swapIrsBean.getDealMode().toString())) {// 竟价
						irs.setTradingModel("01");
					} else if ("Married".equals(swapIrsBean.getDealMode().toString())) {
						irs.setTradingModel("05");
					}
				}
			}

			// 成交编号
			irs.setTicketId(cfetsService.getTicketId("SWAP"));
			//交易来源
			irs.setDealSource("01");
			// 成交单编号
			irs.setContractId(swapIrsBean.getExecId());
			// 交易日期
			irs.setForDate(DateUtil.format(swapIrsBean.getDealDate()));
			// 本方支付机构
			irs.setFixedInst(cfetsService.getLocalInstitution(irs, swapIrsBean.getInstitutionInfo().getInstitutionId(),null));
			// 本方交易员
			irs.setFixedTrader(swapIrsBean.getInstitutionInfo().getTradeId());
			// 浮动利率支付方机构
			irs.setFloatInst(cfetsService.getLocalInstitution(irs,swapIrsBean.getContraPatryInstitutionInfo().getInstitutionId(),null));
			// 浮动利率支付方交易员
			irs.setFloatTrader(swapIrsBean.getContraPatryInstitutionInfo().getTradeId());

			// 产品名称
			irs.setProductName(swapIrsBean.getIrsInfo().getProductName());
			// 名义本金
			irs.setLastQty(swapIrsBean.getIrsInfo().getNotCcyAmt().divide(BigDecimal.valueOf(10000)));
			// 起息日
			irs.setStartDate(DateUtil.format(startDate));
			// 到期日
			irs.setEndDate(DateUtil.format(endDate));
			// 计算机构
			irs.setCalculateAgency(swapIrsBean.getIrsInfo().getCalculateAgency());
			// 期限
			irs.setTenor(tenor);
			// 计算天数调整
			irs.setInterestRateAdjustment(swapIrsBean.getIrsInfo().getBasisRest());
			// 首期起息日
			irs.setFirstPeriodStartDate(DateUtil.format(swapIrsBean.getIrsInfo().getFirstValueDate()));
			// 支付日调整
			irs.setCouponPaymentDateReset(swapIrsBean.getIrsInfo().getPayDateReset());
			// 交易品种
			irs.setApplyProd(swapIrsBean.getKindId());
			// 清算方式
			if ("13".equals(swapIrsBean.getIrsInfo().getSettType())) {
				irs.setTradeMethod("0");
			}
			if ("6".equals(swapIrsBean.getIrsInfo().getSettType())) {
				irs.setTradeMethod("1");
			}

			// 利率明细
			IrsDetailBean fixdIrsDetail = swapIrsBean.getIrsDetail();
			IrsDetailBean floadIrsDeatail = swapIrsBean.getContraPatryIrsDetail();
			// 转换固定/浮动
			int fixedValue = fixdIrsDetail.getLegPriceType();
			if (fixedValue == 3) {//
				irs.setFixedFloatL("Fixed");
				irs.setFixedFloatR("Float");
			} else {
				irs.setFixedFloatL("Float");
				irs.setFixedFloatR("Fixed");
			}

//			irs.setFixedFloatL(fixdIrsDetail.getLegPriceType() + "");
//			irs.setFixedFloatR(floadIrsDeatail.getLegPriceType() + "");
			// 左端参考利率
			irs.setBenchmarkCurveNameL(fixdIrsDetail.getLegCurveName());
			// 左端利差
			irs.setBenchmarkSpreadL(new BigDecimal(fixdIrsDetail.getRateDiff()));
			// 左端首次利率确定日
			irs.setLegInterestAccrualDateL(DateUtil.format(fixdIrsDetail.getFirstconfirmdate()));
			// 左端重置频率
			irs.setInterestResetFrequencyL(fixdIrsDetail.getRaterevcycle());
			// 左端计息方式
			irs.setInterestMethodL(fixdIrsDetail.getRateType());
			// 左端利率
			irs.setLegPrice(fixdIrsDetail.getIntRate());
			// 左端计息基准
			irs.setFixedLegDayCount(convertBasis(fixdIrsDetail.getBasis()));
			// 左端支付周期
			irs.setFixedPaymentFrequency(convertIntPayRule(fixdIrsDetail.getPaycycle()));
			// 左端首期定期支付日
			irs.setFixedPaymentDate(DateUtil.format(fixdIrsDetail.getFirstpaydate()));
			// 右端参考利率
			irs.setBenchmarkCurveName(floadIrsDeatail.getLegCurveName());
			// 右端利差
			irs.setBenchmarkSpread(new BigDecimal(floadIrsDeatail.getRateDiff()));
			// 右端首次利率确定日
			irs.setLegInterestAccrualDateL(DateUtil.format(floadIrsDeatail.getFirstconfirmdate()));
			// 右端重置频率
			irs.setInterestResetFrequency(floadIrsDeatail.getRaterevcycle());
			// 右端计息方式
			irs.setInterestMethod(floadIrsDeatail.getRateType());
			// 右端利率
			irs.setRightRate(floadIrsDeatail.getIntRate());
			// 右端计息基准
			irs.setFloatLegDayCount(convertBasis(floadIrsDeatail.getBasis()));
			// 右端支付周期
			irs.setFloatPaymentFrequency(convertIntPayRule(floadIrsDeatail.getPaycycle()));
			// 右端首期定期支付日
			irs.setFloatPaymentDate(DateUtil.format(floadIrsDeatail.getFirstpaydate()));
			// 固定费资金账户户名
			irs.setFixedAccname(fixedIrs.getAcctName());
			// 固定方资金开户行
			irs.setFixedOpenBank(fixedIrs.getBankName());
			// 固定方资金账户
			irs.setFixedAccnum(fixedIrs.getAcctNo());
			// 固定方支付系统行号
			irs.setFixedPsnum(fixedIrs.getBankAcctNo());
			// 浮动费资金账户户名
			irs.setFloatAccname(floatIrs.getAcctName());
			// 浮动方资金开户行
			irs.setFloatOpenBank(floatIrs.getBankName());
			// 浮动方资金账户
			irs.setFloatAccnum(floatIrs.getAcctNo());
			// 浮动方支付系统行号
			irs.setFloatPsnum(floatIrs.getBankAcctNo());
			irs.setApproveStatus("3");
			irs.setApplyProd(TradeConstants.MarketIndicator.INTEREST_RATE_SWAP);
			TaUser user = cfetsService.userMapping(irs, irs.getFixedTrader());
			if (null == user) {
				irs.setFixedTrader(SystemProperties.cfetsDefautUserId);
				irs.setFixedInst(SystemProperties.cfetsDefautUserInstId);
			} else {
				irs.setFixedTrader(user.getUserId());
				irs.setFixedInst(user.getInstId());
			}
			// 非结构性产品
			irs.setPrdFlag("0");
			cfetsrmbIrsMapper.insert(irs);
		} catch (Exception e) {
			logger.error("利率互换下行交易处理异常：", e);
			rtb.setRetStatus(RetStatusEnum.F);
			rtb.setRetCode("error" + e.getMessage());
			rtb.setRetMsg("交易发送失败");
		}
		logger.info("利率互换输出对象：" + swapIrsBean.toString());

		return rtb;
	}

	private String convertIntPayRule(String rule) {
		if ("0".equals(StringUtils.trimToEmpty(rule))) {// 半年
			return "S";
		}else if("1".equals(StringUtils.trimToEmpty(rule))) {//年
			return "A";
		}else if("2".equals(StringUtils.trimToEmpty(rule))) {//到期
			return "B";
		}else if("3".equals(StringUtils.trimToEmpty(rule))) {//月
			return "M";
		}else if("4".equals(StringUtils.trimToEmpty(rule))) {//季度
			return "Q";
		}else if("5".equals(StringUtils.trimToEmpty(rule))) {//两周
			return "W";
		}else if("6".equals(StringUtils.trimToEmpty(rule))) {//周
			return "W";
		}else if("7".equals(StringUtils.trimToEmpty(rule))) {//天
			return "W";
		}
		return null;
	}

	private String convertBasis(String basis) {
		if ("0".equals(StringUtils.trimToEmpty(basis))) {// 实际/实际
			return "ACTUAL";
		}else if("1".equals(StringUtils.trimToEmpty(basis))) {//实际/360
			return "A360";
		}else if("3".equals(StringUtils.trimToEmpty(basis))) {//实际/365
			return "A365";
		}else if("4".equals(StringUtils.trimToEmpty(basis))) {//实际/365F
			return "30F360";
		}else if("5".equals(StringUtils.trimToEmpty(basis))) {//30E/360
			return "EBOND";
		}
		return null;
	}
	
	@Override
	public RetBean send(SwapCrsBean swapCrsBean, CfetsPackMsgBean cfetsPackMsg) {

		try {
			logger.info("保存下行交易CRS报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易CRS报文异常:", e1);
		}

		return null;
	}

    @Override
    public RetBean sendFxIrs(SwapIrsBean irs, CfetsPackMsgBean cfetsPackMsg)
        throws RemoteConnectFailureException, Exception {
        // TODO Auto-generated method stub
         return null;
    }


}
