package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixSecurityLending;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SecurityLendingBean;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.IfsCfetsPackMsgMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailSlMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbSlMapper;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import com.singlee.ifs.model.IfsCfetsrmbSl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 债券借贷业务处理类
 * 
 * ClassName: CfetsImixLending <br/>
 * Function: 落地债券借贷业务处理类. <br/>
 * Reason: 审批+处理后送入OPICS系统中. <br/>
 * date: 2018-5-30 下午04:38:04 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CfetsImixLending implements ICfetsImixSecurityLending {
	@Autowired
	private IfsCfetsrmbSlMapper cfetsrmbSlMapper;
	@Autowired
	private IfsCfetsrmbDetailSlMapper detailSlMapper;
	@Autowired
	private CfetsRmbService cfetsService;
	@Autowired
	private IfsCfetsPackMsgMapper cfetsPackMsgMapper;

	private static Logger logger = LoggerFactory.getLogger(CfetsImixLending.class);

	@Override
	public RetBean send(SecurityLendingBean securityLendingBean, CfetsPackMsgBean cfetsPackMsg) {

		try {
			logger.info("保存下行交易SL报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易SL报文异常:", e1);
		}
		
		
		RetBean rtb = null;
		try {
			logger.info("债券借贷输入对象：" + securityLendingBean.toString());
			IfsCfetsrmbSl sl = new IfsCfetsrmbSl();

			SecurSettBean borrow = securityLendingBean.getSettInfo();
			SecurSettBean lend = securityLendingBean.getContraPartySettInfo();
			sl.setMyDir(PsEnum.P.toString()); // 本方方向
			sl.setOppoDir(PsEnum.S.toString()); // 对手方方向

			if (!securityLendingBean.getPs().equals(PsEnum.P)) { // 卖出
				sl.setMyDir(PsEnum.S.toString()); // 本方方向
				sl.setOppoDir(PsEnum.P.toString()); // 对手方方向
			}

			Date startDate = securityLendingBean.getComSecurityAmt().getSettDate();
			Date endDate = securityLendingBean.getMatSecurityAmt().getSettDate();
			String tenor = DateUtil.daysBetween(startDate, endDate) + "";

			// 交易状态
			if(securityLendingBean.getStatus()!=null) {
				sl.setDealTransType(StringUtils.trimToEmpty(securityLendingBean.getStatus().toString()));
			}else {
				sl.setDealTransType("1");
			}
			
			// 交易模式
			if(securityLendingBean.getDealMode()!=null) {
				if (StringUtil.isNotEmpty(securityLendingBean.getDealMode().toString())) {
					if ("Enquiry".equals(securityLendingBean.getDealMode().toString())) {// 询价
						sl.setTradingModel("02");
					} else if ("Competition".equals(securityLendingBean.getDealMode().toString())) {// 竟价
						sl.setTradingModel("01");
					} else if ("Married".equals(securityLendingBean.getDealMode().toString())) {
						sl.setTradingModel("05");
					}
				}
			}
			
			sl.setDealSource("01");
			// 成交编号
			sl.setTicketId(cfetsService.getTicketId("SPT"));
			// 成交编号
			sl.setContractId(securityLendingBean.getExecId());
			// 成交时间
			sl.setForDate(DateUtil.format(securityLendingBean.getDealDate()));
			// 融出机构
			sl.setLendInst(cfetsService.getLocalInstitution(sl,
					securityLendingBean.getContraPatryInstitutionInfo().getInstitutionId(),null));
			// 融入机构
			sl.setBorrowInst(
					cfetsService.getLocalInstitution(sl, securityLendingBean.getInstitutionInfo().getInstitutionId(),null));
			// 融入方交易员
			sl.setBorrowTrader(securityLendingBean.getInstitutionInfo().getTradeId());
			// 融出方交易员
			sl.setLendTrader(securityLendingBean.getContraPatryInstitutionInfo().getTradeId());
			// 标的债券代码
			sl.setUnderlyingSecurityId(securityLendingBean.getSecurityInfo().getSecurityId());
			// 标的债券名称
			sl.setUnderlyingSymbol(securityLendingBean.getSecurityInfo().getSecurityName());
			// 借贷期限
			sl.setTenor(tenor);
			// 标的债券券面总额
			sl.setUnderlyingQty(securityLendingBean.getSecurityInfo().getFaceAmt().divide(BigDecimal.valueOf(10000)));
			// 借贷费率
			sl.setPrice(securityLendingBean.getLendingFeeRate());
			// 借贷费用
			sl.setMiscFeeType(securityLendingBean.getLengingFeeCost());
			// 争议解决方案
			sl.setSolution(securityLendingBean.getDisputeWay());
			// 首次结算方式
			sl.setFirstSettlementMethod(securityLendingBean.getComSecurityAmt().getSettType());
			// 到期结算方式
			sl.setSecondSettlementMethod(securityLendingBean.getMatSecurityAmt().getSettType());
			// 实际占款天数
			sl.setOccupancyDays(tenor);
			// 首次结算日
			sl.setFirstSettlementDate(DateUtil.format(startDate));
			// 到期结算日
			sl.setSecondSettlementDate(DateUtil.format(endDate));
			// 交易品种
			sl.setTradingProduct(securityLendingBean.getProductName());
			// 付息日
			sl.setInterestPaymentDate(DateUtil.format(securityLendingBean.getSecurityAmtInfo().getSettDate()));
			// 应计利息总额
			sl.setInterestPaymentAmounts(securityLendingBean.getSecurityAmtInfo().getInterestTotalAmt());
			// 票面利率
			sl.setCouponRate(securityLendingBean.getSecurityAmtInfo().getYeld());
			// 质押券券面总额合计
			sl.setMarginTotalAmt(securityLendingBean.getMaginTotalAmt().divide(BigDecimal.valueOf(10000)));
			// 融入方账户信息
			sl.setBorrowAccname(borrow.getAcctName()); // 融入方资金账户户名
			sl.setBorrowOpenBank(borrow.getBankName()); // 融入方资金开户行
			sl.setBorrowAccnum(borrow.getAcctNo()); // 融入方资金账号
			sl.setBorrowPsnum(borrow.getBankAcctNo()); // 融入方支付系统行号
			sl.setBorrowCaname(borrow.getTrustAcctName()); // 融入方托管账户户名
			String borrowCustname = borrow.getTrustInstitutionId();
			if ("上海清算所".equals(borrowCustname)) {
				borrowCustname = "SHQS";
			} else if ("国债登记结算公司".equals(borrowCustname)) {
				borrowCustname = "CDTC";
			}
			sl.setBorrowCustname(borrowCustname); // 融入方托管机构
			sl.setBorrowCanum(borrow.getTrustAcctNo()); // 融入方托管帐号

			// 融出方账户信息
			sl.setLendAccname(lend.getAcctName()); // 融入方资金账户户名
			sl.setLendOpenBank(lend.getBankName()); // 融入方资金开户行
			sl.setLendAccnum(lend.getAcctNo()); // 融入方资金账号
			sl.setLendPsnum(lend.getBankAcctNo()); // 融入方支付系统行号
			sl.setLendCaname(lend.getTrustAcctName()); // 融入方托管账户户名
			String lendCustname = lend.getTrustInstitutionId();
			if ("上海清算所".equals(lendCustname)) {
				lendCustname = "SHQS";
			} else if ("国债登记结算公司".equals(lendCustname)) {
				lendCustname = "CDTC";
			}
			sl.setLendCustname(lendCustname); // 融入方托管机构
			sl.setLendAccnum(lend.getTrustAcctNo()); // 融入方托管帐号
			sl.setApproveStatus("3");
			sl.setApplyProd(TradeConstants.MarketIndicator.SECURITY_LENDING);
			TaUser user = cfetsService.userMapping(sl, sl.getBorrowTrader());
			if (null == user) {
				sl.setBorrowTrader(SystemProperties.cfetsDefautUserId);
				sl.setBorrowInst(SystemProperties.cfetsDefautUserInstId);
			} else {
				sl.setBorrowTrader(user.getUserId());
				sl.setBorrowInst(user.getInstId());
			}

			sl.setfPrdCode(TradeConstants.ProductCode.SL);
			cfetsrmbSlMapper.insert(sl);

			// 质押债券代码list
			List<SecurityInfoBean> list = securityLendingBean.getLendingInfos();
			if (null != list && list.size() > 0) {
				detailSlMapper.deleteByTicketId(sl.getTicketId());
				for (int i = 0; i < list.size(); i++) {
					SecurityInfoBean securInfo = list.get(i);
					IfsCfetsrmbDetailSl slDetail = new IfsCfetsrmbDetailSl();
					slDetail.setTicketId(sl.getTicketId());
					slDetail.setMarginSecuritiesId(securInfo.getSecurityId());
					slDetail.setMarginSymbol(securInfo.getSecurityName());
					slDetail.setMarginAmt(securInfo.getFaceAmt().divide(BigDecimal.valueOf(10000)));
					slDetail.setSeq(String.valueOf(i));
					detailSlMapper.insert(slDetail);
				}
			}

			rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");

		} catch (Exception e) {
			logger.error("债券借贷下行交易处理异常：", e);
			rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
		}
		return rtb;
	}
}
