package com.singlee.ifs.cfets.cpis;

import com.singlee.financial.cfets.ICpisIrsTradeService;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecInterestRateSwapModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import com.singlee.ifs.mapper.IfsCpisrmbIrsRecMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class CpisIrsTradeServiceImpl implements ICpisIrsTradeService{

	private Logger logger = LogManager.getLogger(CpisIrsTradeServiceImpl.class);
	
	@Autowired
	IfsCpisrmbIrsRecMapper ifsCpisrmbIrsRecMapper;
	
	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	
	@Override
	public RetBean send(RecInterestRateSwapModel model) throws RemoteConnectFailureException, Exception {
		try{
		int i =ifsCpisrmbIrsRecMapper.selectSwapModel(model.getExecID());
		if(i>0){
			ifsCpisrmbIrsRecMapper.updateSwapModel(model);
		}else{
			ifsCpisrmbIrsRecMapper.insertSelective(model);
		}
		}catch(Exception e){
			logger.info("利率互换确认反馈报文落地失败！"+e);
			return new RetBean(RetStatusEnum.F, "error", "利率互换确认反馈报文落地失败。");
		}
		RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
		recCPISFeedBackModel.setConfirmID(model.getConfirmID());
		recCPISFeedBackModel.setExecID(model.getExecID());
		recCPISFeedBackModel.setMarketIndicator(model.getMarketIndicator());
		recCPISFeedBackModel.setTradeDate(model.getTradeDate());
		recCPISFeedBackModel.setConfirmStatus(model.getConfirmStatus());
		recCPISFeedBackModel.setExecType(model.getExecType());
		int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);
		if (count == 0) {
			ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
		}else {
			ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
		}
		logger.info("利率互换确认反馈报文落地成功！");
		return new RetBean(RetStatusEnum.S, "success", "利率互换确认反馈报文落地成功！");
	}

	@Override
	public RetBean send(List<RecInterestRateSwapModel> list)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
