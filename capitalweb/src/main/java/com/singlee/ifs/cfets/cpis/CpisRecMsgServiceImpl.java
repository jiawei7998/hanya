package com.singlee.ifs.cfets.cpis;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.financial.cfets.ICpisRecMsgService;
import com.singlee.financial.cfets.bean.FailRecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecDueNotConfirmModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import com.singlee.ifs.model.TdQryMsg;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class CpisRecMsgServiceImpl implements ICpisRecMsgService{
	
	private Logger logger = LogManager.getLogger(CpisRecMsgServiceImpl.class);
	
	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	@Autowired
	private TdQryMsgMapper tdQryMsgMapper;

	@Override
	public RetBean send(RecCPISFeedBackModel recCPISFeedBackModel)
			throws RemoteConnectFailureException, Exception {
		try {
			logger.info("开始更新报文反馈结果信息！");
			Map<String, Object> msgMap = new HashMap<String, Object>();
			msgMap = BeanUtil.beanToMap(recCPISFeedBackModel);
			String tradeDate = msgMap.get("tradeDate")==null?"":msgMap.get("tradeDate").toString();
			if(!"".equals(tradeDate)){
				tradeDate=tradeDate.substring(0, 4)+"-"+tradeDate.substring(4, 6)+"-"+tradeDate.substring(6);
				msgMap.put("tradeDate", tradeDate);
			}
			//目的是部分外汇远期交易填充到即期里
			String marketIndicator=msgMap.get("marketIndicator")==null?"":msgMap.get("marketIndicator").toString();
			if("14".equals(marketIndicator)){
				Map<String, Object> p = new HashMap<String, Object>();
				p.put("execID", msgMap.get("execID")==null?"":msgMap.get("execID").toString());
				List<TdQryMsg> list = tdQryMsgMapper.searchQryMsg(p);
				if(list.size()>0){
					msgMap.put("marketIndicator", list.get(0).getMarketIndicator());
				}
			}

			if("0".equals(recCPISFeedBackModel.getAffirmStatus())){
				msgMap.put("sendState", "101");
			}

			//如果提示交易已确认即交易匹配成功
			//修改为发送过的不再定时重新发送
			String text=msgMap.get("text")==null?"":msgMap.get("text").toString();
			if(text.contains("附言")){
				msgMap.put("jobnum", "1");//需要定时重新发送一次
			}else{
				msgMap.put("jobnum", "2");
			}
			tdQryMsgMapper.update(msgMap);
			logger.info("确认编号："+recCPISFeedBackModel.getConfirmID()+"反馈结果更新成功");
			return new RetBean(RetStatusEnum.S, "success", "反馈结果更新成功！");
		} catch (Exception e) {
			logger.error("更新报文反馈结果失败！"+e,e);
			throw e;
		}
	}

	@Override
	public RetBean send(FailRecConfirmStatusModel arg0)
			throws RemoteConnectFailureException, Exception {
		RetBean retBean = new RetBean();
		retBean.setRetCode(arg0.getAffirmStatus());
		retBean.setRetMsg(arg0.getText());
		logger.info(retBean.getErrorMsg()+"|"+retBean.getRetMsg());
		return retBean;
	}

	@Override
	public RetBean send(List<RecConfirmStatusModel> arg0)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetBean send(RecConfirmStatusModel arg0)
			throws RemoteConnectFailureException, Exception {
		try {
			logger.info("开始更新报文查询结果信息！");
			RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
			recCPISFeedBackModel.setQueryRequestID(arg0.getQueryRequestID());
			recCPISFeedBackModel.setMessage(arg0.getMessage());

			recCPISFeedBackModel.setExecID(arg0.getExecID());
			recCPISFeedBackModel.setConfirmStatus(arg0.getConfirmStatus());
			int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);


			if (count == 0) {
				recCPISFeedBackModel.setMarketIndicator(arg0.getMarketIndicator());
				recCPISFeedBackModel.setTradeDate(arg0.getTradeDate());
				ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
			}else {
				ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
			}

			Map<String, Object> msgMap = new HashMap<String, Object>();
//			msgMap.put("CONFIRMID",);
			msgMap.put("execID",arg0.getExecID());
			msgMap.put("sendState",arg0.getConfirmStatus());
			tdQryMsgMapper.updateSendState(msgMap);
			logger.info("成交编号："+recCPISFeedBackModel.getExecID()+"查询编号："+recCPISFeedBackModel.getQueryRequestID()+"查询结果更新成功");
			return new RetBean(RetStatusEnum.S, "success", "查询结果更新成功！");
		} catch (Exception e) {
			logger.error("更新报文查询结果失败！"+e,e);
			throw e;
		}
	}

	@Override
	public RetBean send(RecDueNotConfirmModel arg0)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub

		logger.info("收到交易未确认提醒交易类型为："+"---------"+arg0.getMarketIndicator()+"交易笔数为:"+arg0.getTransactionNum());
		return new RetBean(RetStatusEnum.S, "success", "收到交易未确认提醒");
	}

	

}
