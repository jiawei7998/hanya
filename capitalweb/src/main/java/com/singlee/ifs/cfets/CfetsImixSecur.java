/**
 * Project Name:capitalweb
 * File Name:CfetsImixSecur.java
 * Package Name:com.singlee.ifs.cfets
 * Date:2018-5-30下午03:53:17
 * Copyright (c) 2018, singlee@singlee.com.cn All Rights Reserved.
 *
 */

package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixSecur;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.SubScriptionInstBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 债券业务处理类
 * 
 * ClassName:CfetsImixSecur <br/>
 * Function: 落地外汇中心现券买卖交易数据. <br/>
 * Reason: 审批+处理后送入OPICS系统. <br/>
 * Date: 2018-5-30 下午03:53:17 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 * @see
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CfetsImixSecur implements ICfetsImixSecur {

	@Autowired
	private IfsCfetsrmbCbtMapper cbtMapper;
	@Autowired
	private CfetsRmbService cfetsService;
	@Autowired
	private IfsCfetsPackMsgMapper cfetsPackMsgMapper;
	//存单发行
	@Autowired
	private IfsCfetsrmbDpMapper cfetsrmbDpMapper;
	//债券远期
	@Autowired
	private IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	@Autowired
	private IfsCfetsrmbDpOfferMapper offerDpMapper;
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;
	@Autowired
	IfsOpicsCustMapper ifsOpicsCustMapper;

	private static Logger logger = LoggerFactory.getLogger(CfetsImixSecur.class);

	@Override
	public RetBean send(FixedIncomeBean fiBean, CfetsPackMsgBean cfetsPackMsg) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			logger.info("保存下行交易CBT报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易CBT报文异常:", e1);
		}

		RetBean rtb = null;
		String deli = null;
		String retCode = null;
		String retMsg = null;
		try {
			logger.info("现券买卖输入对象：" + fiBean.toString());
			// 现券买卖
			IfsCfetsrmbCbt cbt = new IfsCfetsrmbCbt();
			IfsCfetsrmbCbt retCbt = new IfsCfetsrmbCbt();
			cbt.setExecID(fiBean.getExecId());
			cbt.setTradeDate(DateUtil.format(fiBean.getDealDate()));
			cbt.setTradeTime(fiBean.getTradeTime());
			SecurityInfoBean secInfoBean = fiBean.getSecurityInfo();
			cbt.setSide(String.valueOf(secInfoBean.getSide()));
			SecurityAmtDetailBean secAmtDetailBean = fiBean.getSecurityAmt();
			cbt.setTradeCashAmt(secAmtDetailBean.getAmt().toString());
			cbt.setSecurityID(secInfoBean.getSecurityId());
			cbt.setLastQty(secInfoBean.getFaceAmt().toString());
			cbt.setSettlDate(DateUtil.format(secAmtDetailBean.getSettDate()));
			cbt.setDeliveryType(Integer.valueOf(secAmtDetailBean.getSettType()));
			map.put("execID", fiBean.getExecId());
			map.put("tradeDate", DateUtil.format(fiBean.getDealDate()));
			map.put("tradeTime", fiBean.getTradeTime());
			map.put("side", String.valueOf(secInfoBean.getSide()));
			map.put("tradeCashAmt", secAmtDetailBean.getAmt().toString());
			map.put("securityID", secInfoBean.getSecurityId());
			map.put("lastQty", secInfoBean.getFaceAmt().toString());
			map.put("settlDate", DateUtil.format(secAmtDetailBean.getSettDate()));
			map.put("deliveryType", secAmtDetailBean.getSettType());
			map.put("price", fiBean.getSecurityAmt().getPrice().toString());
			map.put("settlCurrAmt", fiBean.getSecurityAmt().getSettAmt().toString());
			//前置产品号
//			cbt.setfPrdCode(TradeConstants.ProductCode.CBT);
//
//			SecurSettBean buy = fiBean.getSettInfo();
//			SecurSettBean sell = fiBean.getContraPartySettInfo();
//			cbt.setMyDir(PsEnum.P.toString()); // 本方方向
//			cbt.setOppoDir(PsEnum.S.toString()); // 对手方方向
//
//			if (!fiBean.getPs().equals(PsEnum.P)) { // 融出
//				cbt.setMyDir(PsEnum.S.toString()); // 本方方向
//				cbt.setOppoDir(PsEnum.P.toString()); // 对手方方向
////				buy = fiBean.getContraPartySettInfo();
////				sell = fiBean.getSettInfo();
//			}
//
//			// 交易状态
//			if(fiBean.getStatus()!=null) {
//				cbt.setDealTransType(StringUtils.trimToEmpty(fiBean.getStatus().toString()));
//			}else {
//				cbt.setDealTransType("1");
//			}
//			
//			// 交易模式
//			if(fiBean.getDealMode()!=null) {
//				if (StringUtil.isNotEmpty(fiBean.getDealMode().toString())) {
//					if ("Enquiry".equals(fiBean.getDealMode().toString())) {// 询价
//						cbt.setTradingModel("02");
//					} else if ("Competition".equals(fiBean.getDealMode().toString())) {// 竟价
//						cbt.setTradingModel("01");
//					} else if ("Married".equals(fiBean.getDealMode().toString())) {
//						cbt.setTradingModel("05");
//					}
//				}
//			}
//			// 成交单编号
//			cbt.setTicketId(cfetsService.getTicketId("SECUR"));
//			//交易来源
//			cbt.setDealSource("01");
//			// 成交单编号
//			cbt.setContractId(fiBean.getExecId());
//			// 成交日期
//			cbt.setForDate(DateUtil.format(fiBean.getDealDate()));
//			// 买入机构
//			cbt.setBuyInst(cfetsService.getLocalInstitution(cbt, fiBean.getInstitutionInfo().getInstitutionId(),null));
//			// 买入交易员
//			cbt.setBuyTrader(fiBean.getInstitutionInfo().getTradeId());
//			// 卖出机构
//			cbt.setSellInst(cfetsService.getLocalInstitution(cbt, fiBean.getContraPatryInstitutionInfo().getInstitutionId(),null));
//			// 卖出交易员
//			cbt.setSellTrader(fiBean.getContraPatryInstitutionInfo().getTradeId());
//			// 债券编号
//			cbt.setBondCode(fiBean.getSecurityInfo().getSecurityId());
//			// 债券名称
//			cbt.setBondName(fiBean.getSecurityInfo().getSecurityName());
//			// 债券面额
//			cbt.setTotalFaceValue(fiBean.getSecurityInfo().getFaceAmt());
//			// 债券净价
//			cbt.setCleanPrice(fiBean.getSecurityAmt().getPrice());
//			// 交易金额
//			cbt.setTradeAmount(fiBean.getSecurityAmt().getAmt());
//			// 到期收益率
//			cbt.setYield(fiBean.getSecurityAmt().getYeld());
//			// 应计利息总额
//			cbt.setTotalAccuredInterest(fiBean.getSecurityAmt().getInterestTotalAmt());
//			// 应计利息
//			cbt.setAccuredInterest(fiBean.getSecurityAmt().getInterestAmt());
//			// 全价
//			cbt.setDirtyPrice(fiBean.getSecurityAmt().getDirtyPrice());
//			// 结算金额
//			cbt.setSettlementAmount(fiBean.getSecurityAmt().getSettAmt());
//			// 结算方式
//			cbt.setSettlementMethod(fiBean.getSecurityAmt().getSettType());
//			// 结算日
//			cbt.setSettlementDate(DateUtil.format(fiBean.getSecurityAmt().getSettDate()));
//			// 买方账户名称
//			cbt.setBuyAccname(buy.getAcctName());
//			// 买方开户行
//			cbt.setBuyOpbank(buy.getBankName());
//			// 买方账户名称
//			cbt.setBuyAccnum(buy.getAcctNo());
//			// 买方中间行SWIFT CODE
//			cbt.setBuyPsnum(buy.getBankAcctNo());
//			// 买方托管账户名称
//			cbt.setBuyCaname(buy.getTrustAcctName());
//			// 买方托管账户
//			cbt.setBuyCanum(buy.getTrustAcctNo());
//			// 买方托管机构
//			String buyCustname = buy.getTrustInstitutionId();
//			if ("上海清算所".equals(buyCustname)) {
//				buyCustname = "SHQS";
//			} else if ("国债登记结算公司".equals(buyCustname)) {
//				buyCustname = "CDTC";
//			}
//			cbt.setBuyCustname(buyCustname);
//			// 卖方账户名称
//			cbt.setSellAccname(sell.getAcctName());
//			// 卖方开户行
//			cbt.setSellOpbank(sell.getBankName());
//			// 卖方账户名称
//			cbt.setSellAccnum(sell.getAcctNo());
//			// 卖方中间行SWIFT CODE
//			cbt.setSellPsnum(sell.getBankAcctNo());
//			// 卖方托管账户名称
//			cbt.setSellCaname(sell.getTrustAcctName());
//			// 卖方托管账户
//			cbt.setSellCanum(sell.getTrustAcctNo());
//			// 卖方托管机构
//			String sellCustname = sell.getTrustInstitutionId();
//			if ("上海清算所".equals(sellCustname)) {
//				sellCustname = "SHQS";
//			} else if ("国债登记结算公司".equals(sellCustname)) {
//				sellCustname = "CDTC";
//			}
//			cbt.setSellCustname(sellCustname);
//			// 审批标识
//			cbt.setApproveStatus("3");
//			cbt.setApplyProd(TradeConstants.MarketIndicator.CASH_BOND);
//			TaUser user = cfetsService.userMapping(cbt, cbt.getBuyTrader());
//			if (null == user) {
//				cbt.setBuyTrader(SystemProperties.cfetsDefautUserId);
//				cbt.setBuyInst(SystemProperties.cfetsDefautUserInstId);
//			} else {
//				cbt.setBuyTrader(user.getUserId());
//				cbt.setBuyInst(user.getInstId());
//			}

//			cbtMapper.insert(cbt);
			cbtMapper.insertCbtToOpics(map);
			retCode = (String)map.get("retCode");
			retMsg = (String)map.get("retMsg");
			if(!"999".equals(retCode)) {
				rtb = new RetBean(RetStatusEnum.F, retCode+retMsg, "交易发送失败");
			}else {
				rtb = new RetBean(RetStatusEnum.S, retCode+retMsg, "交易发送成功");
			}
		} catch (Exception e) {
			logger.error("现券买卖下行交易处理异常：", e);
			rtb = new RetBean(RetStatusEnum.F, retCode + retMsg + e.getMessage(), "交易发送失败");
		}
		logger.info("现券买卖输出对象：" + rtb.toString());

		return rtb;
	}
	
	@Override
	public RetBean send(BfdBean bfdBean, CfetsPackMsgBean cfetsPackMsg) {
		
		try {
			logger.info("保存下行交易BONDFWD报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易BONDFWD报文异常:", e1);
		}
		RetBean rtb = null;
		try {
			logger.info("债券远期输入对象：" + bfdBean.toString());
			
			IfsCfetsrmbBondfwd icrbf = new IfsCfetsrmbBondfwd();
			//前置产品代码
			icrbf.setfPrdCode(TradeConstants.ProductCode.BFWD);

			SecurSettBean buy = bfdBean.getSettInfo();
			SecurSettBean sell = bfdBean.getContraPartySettInfo();
			icrbf.setMyDir(PsEnum.P.toString()); // 本方方向
			icrbf.setOppoDir(PsEnum.S.toString()); // 对手方方向
			
			if (!bfdBean.getPs().equals(PsEnum.P)) { // 融出
				icrbf.setMyDir(PsEnum.S.toString()); // 本方方向
				icrbf.setOppoDir(PsEnum.P.toString()); // 对手方方向
			}
			
			// 交易状态
			if(bfdBean.getStatus()!=null) {
				icrbf.setDealTransType(StringUtils.trimToEmpty(bfdBean.getStatus().toString()));
			}else {
				icrbf.setDealTransType("1");
			}
			
			// 交易模式
			if(bfdBean.getDealMode()!=null) {
				if (StringUtil.isNotEmpty(bfdBean.getDealMode().toString())) {
					if ("Enquiry".equals(bfdBean.getDealMode().toString())) {// 询价
						icrbf.setTradingModel("02");
					} else if ("Competition".equals(bfdBean.getDealMode().toString())) {// 竟价
						icrbf.setTradingModel("01");
					} else if ("Married".equals(bfdBean.getDealMode().toString())) {
						icrbf.setTradingModel("05");
					}
				}
			}
			// 成交单编号
			icrbf.setTicketId(cfetsService.getTicketId("BONDFWD"));
			//交易来源
			icrbf.setDealSource("01");
			// 成交单编号
			icrbf.setContractId(bfdBean.getExecId());
			// 成交日期
			icrbf.setForDate(DateUtil.format(bfdBean.getDealDate()));
			//净额清算状态
			icrbf.setNettingStatus("0");
			// 买入机构
			icrbf.setBuyInst(cfetsService.getLocalInstitution(icrbf, bfdBean.getInstitutionInfo().getInstitutionId(),null));
			// 买入交易员
			icrbf.setBuyTrader(bfdBean.getInstitutionInfo().getTradeId());
			// 卖出机构
			icrbf.setSellInst(cfetsService.getLocalInstitution(icrbf, bfdBean.getContraPatryInstitutionInfo().getInstitutionId(),null));
			// 卖出交易员
			icrbf.setSellTrader(bfdBean.getContraPatryInstitutionInfo().getTradeId());
			// 债券编号
			icrbf.setBondCode(bfdBean.getSecurityInfo().getSecurityId());
			// 债券名称
			icrbf.setBondName(bfdBean.getSecurityInfo().getSecurityName());
			//价格
			icrbf.setCleanPrice(bfdBean.getSecurityInfo().getcPrice());
			//券面
			icrbf.setTotalFaceValue(bfdBean.getSecurityInfo().getFaceAmt());
			//收益率
			icrbf.setYield(bfdBean.getSecurityInfo().getYield2());
			//交易金额
			icrbf.setTradeAmount(bfdBean.getTradeAmt());
			//应计利息
			icrbf.setAccuredInterest(bfdBean.getSecurityInfo().getAccruedInt());
			//应计利息总额
			icrbf.setTotalAccuredInterest(bfdBean.getAccruedIntAmt());
			//全价
			icrbf.setDirtyPrice(bfdBean.getSecurityInfo().getdPrice());
			//结算金额
			icrbf.setSettlementAmount(bfdBean.getSettlCurrAmt());
			//期限
			icrbf.setTenor(bfdBean.getTradeLimitDays().toString());
			//交易品种
			icrbf.setTradingProduct(bfdBean.getTermToMaturity());
			//结算方式
			if("0".equals(bfdBean.getDeliveryType())){
				icrbf.setSettlementMethod("DVP");
			}else if("4".equals(bfdBean.getDeliveryType())){
				icrbf.setSettlementMethod("PAD");
			}else if("5".equals(bfdBean.getDeliveryType())){
				icrbf.setSettlementMethod("DAP");
			}else{
				icrbf.setSettlementMethod(bfdBean.getDeliveryType());
			}
			//结算日期
			if(null != bfdBean.getSettlDate() && !"".equals(bfdBean.getSettlDate()) && bfdBean.getSettlDate().length() == 8){
				icrbf.setSettlementDate(DateUtil.format(DateUtil.parse(bfdBean.getSettlDate(),"yyyyMMdd")));
			}
			// 买方账户名称
			icrbf.setBuyAccname(buy.getAcctName());
			// 买方开户行
			icrbf.setBuyOpbank(buy.getBankName());
			// 买方账户名称
			icrbf.setBuyAccnum(buy.getAcctNo());
			// 买方中间行SWIFT CODE
			icrbf.setBuyPsnum(buy.getBankAcctNo());
			// 买方托管账户名称
			icrbf.setBuyCaname(buy.getTrustAcctName());
			// 买方托管账户
			icrbf.setBuyCanum(buy.getTrustAcctNo());
			// 买方托管机构
			String buyCustname = buy.getTrustInstitutionId();
			if ("上海清算所".equals(buyCustname)) {
				buyCustname = "SHQS";
			} else if ("国债登记结算公司".equals(buyCustname)) {
				buyCustname = "CDTC";
			}
			icrbf.setBuyCustname(buyCustname);
			// 卖方账户名称
			icrbf.setSellAccname(sell.getAcctName());
			// 卖方开户行
			icrbf.setSellOpbank(sell.getBankName());
			// 卖方账户名称
			icrbf.setSellAccnum(sell.getAcctNo());
			// 卖方中间行SWIFT CODE
			icrbf.setSellPsnum(sell.getBankAcctNo());
			// 卖方托管账户名称
			icrbf.setSellCaname(sell.getTrustAcctName());
			// 卖方托管账户
			icrbf.setSellCanum(sell.getTrustAcctNo());
			// 卖方托管机构
			String sellCustname = sell.getTrustInstitutionId();
			if ("上海清算所".equals(sellCustname)) {
				sellCustname = "SHQS";
			} else if ("国债登记结算公司".equals(sellCustname)) {
				sellCustname = "CDTC";
			}
			icrbf.setSellCustname(sellCustname);
			TaUser user = cfetsService.userMapping(icrbf, icrbf.getBuyTrader());
			if (null == user) {
				icrbf.setBuyTrader(SystemProperties.cfetsDefautUserId);
				icrbf.setBuyInst(SystemProperties.cfetsDefautUserInstId);
			} else {
				icrbf.setBuyTrader(user.getUserId());
				icrbf.setBuyInst(user.getInstId());
			}
			// 审批标识
			icrbf.setApproveStatus("3");
			icrbf.setApplyProd(TradeConstants.MarketIndicator.BOND_FORWARD);
			ifsCfetsrmbBondfwdMapper.insertSelective(icrbf);
			rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		} catch (Exception e) {
			logger.error("债券远期下行交易处理异常：", e);
			rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
		}
		logger.info("债券远期输出对象：" + rtb.toString());

		return rtb;
	}
	
	@Override
	public RetBean send(StandardBfdBean standardBfdBean, CfetsPackMsgBean cfetsPackMsg) {
		RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		return rtb;
	}
	
	private String convertSettType(String setttype) {
		if ("0".equals(StringUtils.trimToEmpty(setttype))) {// DVP
			return "DVP";
		}else if("1".equals(StringUtils.trimToEmpty(setttype))) {//PUD
			return "PAD";
		}else if("3".equals(StringUtils.trimToEmpty(setttype))) {//DUP
			return "DAP";
		}else if("4".equals(StringUtils.trimToEmpty(setttype))) {// NDVP
			return "FOP";
		}
		return null;
	}

    @Override
    public RetBean send(IbncdBean ibncdBean, CfetsPackMsgBean cfetsPackMsg){
    	try {
			logger.info("保存下行交易DP报文");
			// 报文下行报文
			cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
		} catch (Exception e1) {
			logger.error("保存下行交易DP报文异常:", e1);
		}
    	
    	RetBean rtb = null;
		try {
			logger.info("存单发行输入对象：" + ibncdBean.toString());
			// 存单发行
			IfsCfetsrmbDp dp = new IfsCfetsrmbDp();
			//前置产品代码
			dp.setfPrdCode(TradeConstants.ProductCode.DPCD);
			// 交易状态
			if(ibncdBean.getStatus()!=null) {
				dp.setDealTransType(StringUtils.trimToEmpty(ibncdBean.getStatus().toString()));
			}else {
				dp.setDealTransType("1");
			}
			
			// 成交单编号
			dp.setTicketId(cfetsService.getTicketId("DP"));
			//交易来源
			dp.setDealSource("01");
			// 成交单编号
			dp.setContractId(ibncdBean.getExecId());
			// 成交日期
			dp.setForDate(DateUtil.format(ibncdBean.getDealDate()));
			//存单全称
			dp.setDepositAllName(ibncdBean.getFullName());
			//存单简称
			dp.setDepositName(ibncdBean.getShortName());
			//息票类型
			dp.setInterestType(ibncdBean.getRateType());
			//发行价格(元)
			dp.setPublishPrice(ibncdBean.getIssPrice());
			//币种
			dp.setCurrency(ibncdBean.getCcy());
			//存单代码
			dp.setDepositCode(ibncdBean.getSecId());
			
			IfsOpicsBond bond = ifsOpicsBondMapper.searchById(dp.getDepositCode());
			if(bond != null) {
				//发行日期
				dp.setPublishDate(bond.getPubdt());
				//起息日(缴款日期)
				dp.setValueDate(bond.getIssudt());
				//到期日
				dp.setDuedate(bond.getMatdt());
				//融入方托管机构
				dp.setBorrowCustname(bond.getAccountno());
				//存单期限
				dp.setDepositTerm(new BigDecimal(bond.getBondperiod() == null ? "0":bond.getBondperiod()));
			}
			
			//实际发行量(元)
			dp.setActualAmount(ibncdBean.getIssSize().multiply(new BigDecimal("100000000")));
			//参考收益率(%)
			if("1".equals(ibncdBean.getRateType())) {
				dp.setBenchmarkcurvename(ibncdBean.getSpread());
			}else if("0".equals(ibncdBean.getRateType())) {
				dp.setBenchmarkcurvename(ibncdBean.getCouponRate());
			}else if("2".equals(ibncdBean.getRateType())) {
				dp.setBenchmarkcurvename(new BigDecimal("0"));
			}
			//融入方机构
			dp.setBorrowInst(ibncdBean.getContraPatryInstitutionInfo().getInstitutionId());
			//融入方交易员
			dp.setBorrowTrader(ibncdBean.getContraPatryInstitutionInfo().getTradeId());
			//审批状态
			dp.setApproveStatus("3");
			//审批发起人
			dp.setSponsor(ibncdBean.getContraPatryInstitutionInfo().getTradeId());
			//审批发起机构
			dp.setSponInst(ibncdBean.getContraPatryInstitutionInfo().getInstitutionId());
			//净额清算状态
			dp.setNettingStatus("0");
			// 交易模式
			dp.setTradingModel("02");
			//工具类型
			dp.setInstrumentType("CD");
			
			TaUser user = cfetsService.userMapping(dp, dp.getBorrowTrader());
			if (null == user) {
				//融入方机构
				dp.setBorrowInst(SystemProperties.cfetsDefautUserInstId);
				//融入方交易员
				dp.setBorrowTrader(SystemProperties.cfetsDefautUserId);
				//审批发起人
				dp.setSponsor(SystemProperties.cfetsDefautUserId);
				//审批发起机构
				dp.setSponInst(SystemProperties.cfetsDefautUserInstId);
			} else {
				//融入方机构
				dp.setBorrowInst(user.getUserId());
				//融入方交易员
				dp.setBorrowTrader(user.getUserId());
				//审批发起人
				dp.setSponsor(user.getUserId());
				//审批发起机构
				dp.setSponInst(user.getUserId());
			}
			cfetsrmbDpMapper.insert(dp);
			
			//认购人信息
			List<SubScriptionInstBean> subScriptions = ibncdBean.getSubScriptions();
			for (int i = 0; i < subScriptions.size(); i++) {
				SubScriptionInstBean subBean = subScriptions.get(i);
				
				IfsCfetsrmbDpOffer rec = new IfsCfetsrmbDpOffer();
				//成交单编号
				rec.setTicketId(dp.getTicketId());
				//融出方机构
				rec.setLendInst(getLocaltion(subBean.getInstitutionId()));

				//融出方交易员
				rec.setLendTrader(subBean.getTradeId());
				//认购量(元)
				rec.setOfferAmount(subBean.getSubScriptionAmt().multiply(new BigDecimal("100000000")));
				//应缴纳金额(元)
				rec.setShouldPayMoney(subBean.getFeeActualAmt());
				//交易来源
				rec.setDealSource("01");
				
				rec.setSeq(String.valueOf(i+1));
				offerDpMapper.insert(rec);
				
			}
			
			
			rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		} catch (Exception e) {
			logger.error("存单发行下行交易处理异常：", e);
			rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
		}
		logger.info("存单发行输出对象：" + rtb.toString());

		return rtb;
    }

	private String getLocaltion(String institutionId) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cfetscn", institutionId);
		List<IfsOpicsCust> list = ifsOpicsCustMapper.searchByCfets(map);
		if (list.size() == 1) {
			// 授信占用主体
			IfsOpicsCust cust = list.get(0);
			return cust.getCno();
		}else{
			return institutionId;
		}
	}



		@Override
    public RetBean send(IbncdCommissionBean tradeBean, CfetsPackMsgBean cfetsPackMsg)
        throws RemoteConnectFailureException, Exception {
        // TODO Auto-generated method stub
         return null;
    }
}
