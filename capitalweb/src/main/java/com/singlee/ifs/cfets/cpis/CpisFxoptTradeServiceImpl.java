package com.singlee.ifs.cfets.cpis;

import com.singlee.financial.cfets.ICpisFxoptTradeService;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecForeignOptionModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisFxoptRecMapper;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class CpisFxoptTradeServiceImpl implements ICpisFxoptTradeService{
	
	private Logger logger = LogManager.getLogger(CpisFxoptTradeServiceImpl.class);

	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	
	@Autowired
	IfsCpisFxoptRecMapper ifsCpisFxoptRecMapper;
	
	@Override
	public RetBean send(RecForeignOptionModel model)
			throws RemoteConnectFailureException, Exception {
		try {
			logger.info("开始更新交易明细！");
			//更新交易明细信息
			int count1 = ifsCpisFxoptRecMapper.getRecFxoptCount(model);
			if (count1 == 0) {
				ifsCpisFxoptRecMapper.insertSelective(model);
			}
			//获取返回的交易明细中的确认状态等信息
			RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
			recCPISFeedBackModel.setConfirmID(model.getConfirmID());
			if(!"".equals(model.getCombinationID())&&null!=model.getCombinationID()){
				recCPISFeedBackModel.setExecID(model.getCombinationID());
			}else{
				recCPISFeedBackModel.setExecID(model.getExecID());
			}
			recCPISFeedBackModel.setMarketIndicator(model.getMarketIndicator());
			recCPISFeedBackModel.setTradeDate(model.getTradeDate());
			recCPISFeedBackModel.setConfirmStatus(model.getConfirmStatus());
			//更新反馈信息表中确认状态等信息
			int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);
			if (count == 0) {
				ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
			}else {
				ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
			}
			logger.info("确认编号："+recCPISFeedBackModel.getConfirmID()+"交易明细更新成功");
			return new RetBean(RetStatusEnum.S, "success", "交易明细更新成功！");
		} catch (Exception e) {
			logger.error("更新交易明细失败！"+e,e);
			throw e;
		}
	}

	@Override
	public RetBean send(List<RecForeignOptionModel> list)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
