package com.singlee.ifs.cfets.cpis;

import com.singlee.financial.cfets.ICpisFxMMTradeService;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecForeignFxmmModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import com.singlee.ifs.mapper.IfsCpisfxMmRecMapper;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class CpisFxMMTradeServiceImpl implements ICpisFxMMTradeService{
	
	private Logger logger = LogManager.getLogger(CpisFxMMTradeServiceImpl.class);
	
	@Autowired
	IfsCpisfxMmRecMapper ifsCpisfxMmRecMapper;
	
	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	@Autowired
	private TdQryMsgMapper tdQryMsgMapper;
	
	@Override
	public RetBean send(RecForeignFxmmModel model) throws RemoteConnectFailureException, Exception {
		try{
			int i =ifsCpisfxMmRecMapper.selectFxmmModel(model.getExecID());
			if(i>0){
				ifsCpisfxMmRecMapper.updateFxmmModel(model);
			}else{
			    ifsCpisfxMmRecMapper.insertSelective(model);
			}
			RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
			recCPISFeedBackModel.setConfirmID(model.getConfirmID());
			recCPISFeedBackModel.setExecID(model.getExecID());
			recCPISFeedBackModel.setMarketIndicator(model.getMarketIndicator());
			recCPISFeedBackModel.setTradeDate(model.getTradeDate());
			recCPISFeedBackModel.setConfirmStatus(model.getConfirmStatus());
			recCPISFeedBackModel.setExecType(model.getExecType());
			int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);
			if (count == 0) {
				ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
			}else {
				ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
			}

			//推送数据
			Map<String, Object> msgMap = new HashMap<String, Object>();
			msgMap.put("execType",model.getExecType());
			msgMap.put("execID",model.getExecID());
			tdQryMsgMapper.updateExecType(msgMap);

			}catch(Exception e){
				logger.info("外币拆借确认反馈报文落地失败！"+e);
				return new RetBean(RetStatusEnum.F, "error", "外币拆借确认反馈报文落地失败。");
			}
			logger.info("外币拆借确认反馈报文落地成功！");
			return new RetBean(RetStatusEnum.S, "success", "外币拆借确认反馈报文落地成功！");
	}

	@Override
	public RetBean send(List<RecForeignFxmmModel> list)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
