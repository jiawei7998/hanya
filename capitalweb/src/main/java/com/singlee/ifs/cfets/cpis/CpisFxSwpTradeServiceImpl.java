package com.singlee.ifs.cfets.cpis;

import com.singlee.financial.cfets.ICpisFxSwpTradeService;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecForeignSwapModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCpisFxSwpRecMapper;
import com.singlee.ifs.mapper.IfsCpisRecinfoMapper;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class CpisFxSwpTradeServiceImpl implements ICpisFxSwpTradeService{

	private Logger logger = LogManager.getLogger(CpisFxSwpTradeServiceImpl.class);

	@Autowired
	IfsCpisRecinfoMapper ifsCpisRecinfoMapper;
	
	@Autowired
	IfsCpisFxSwpRecMapper ifsCpisFxSwpRecMapper;

	@Autowired
	private TdQryMsgMapper tdQryMsgMapper;
	
	@Override
	public RetBean send(RecForeignSwapModel model)
			throws RemoteConnectFailureException, Exception {
		try {
			logger.info("开始更新交易明细！");
			//更新交易明细信息
			int count1 = ifsCpisFxSwpRecMapper.getRecSwpCount(model);
			if (count1 == 0) {
				ifsCpisFxSwpRecMapper.insertSelective(model);
			}else {
				ifsCpisFxSwpRecMapper.updateByConfirmId(model);
			}
			//获取返回的交易明细中的确认状态等信息
			RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
			recCPISFeedBackModel.setConfirmID(model.getConfirmID());
			recCPISFeedBackModel.setExecID(model.getExecID());
			recCPISFeedBackModel.setMarketIndicator(model.getMarketIndicator());
			recCPISFeedBackModel.setTradeDate(model.getTradeDate());
			recCPISFeedBackModel.setConfirmStatus(model.getConfirmStatus());
			recCPISFeedBackModel.setExecType(model.getExecType());
			//更新反馈信息表中确认状态等信息
			int count = ifsCpisRecinfoMapper.getRecinfoCount(recCPISFeedBackModel);
			if (count == 0) {
				ifsCpisRecinfoMapper.insertSelective(recCPISFeedBackModel);
			}else {
				ifsCpisRecinfoMapper.updateByExecId(recCPISFeedBackModel);
			}

			//推送数据
			Map<String, Object> msgMap = new HashMap<String, Object>();
			msgMap.put("execType",model.getExecType());
			msgMap.put("execID",model.getExecID());
			tdQryMsgMapper.updateExecType(msgMap);

			logger.info("确认编号："+recCPISFeedBackModel.getConfirmID()+"交易明细更新成功");
			return new RetBean(RetStatusEnum.S, "success", "交易明细更新成功！");
		} catch (Exception e) {
			logger.error("更新交易明细失败！"+e,e);
			throw e;
		}
	}

	@Override
	public RetBean send(List<RecForeignSwapModel> model)
			throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
