/**
 * Project Name:capitalweb
 * File Name:CfetsImixOption.java
 * Package Name:com.singlee.ifs.cfets
 * Date:2018-5-30下午03:56:41
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
 */

package com.singlee.ifs.cfets;

import com.singlee.financial.cfets.ICfetsImixOption;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.FxOptionBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * 期权交易业务处理类
 * 
 * ClassName:CfetsImixOption <br/>
 * Function: 主要落地外汇交易中心期权交易数据. <br/>
 * Reason: 审批+处理后送入OPICS系统. <br/>
 * Date: 2018-5-30 下午03:56:41 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 * @see
 */
public class CfetsImixOption implements ICfetsImixOption {

	//其他银行使用，泰隆银行目前没有20181217
	@Override
    public RetBean send(FxOptionBean optionBean, CfetsPackMsgBean cfetsPackMsg) {
		RetBean rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
		return rtb;
	}
}
