package com.singlee.ifs.cfets;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.ICfetsImixRepo;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RepoCrBean;
import com.singlee.financial.pojo.RepoOrBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SLFBean;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SLFSettAmtBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.baseUtil.CfetsRmbService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.IfsCfetsrmbCr;
import com.singlee.ifs.model.IfsCfetsrmbDetailCr;
import com.singlee.ifs.model.IfsCfetsrmbOr;
import com.singlee.ifs.model.IfsOpicsBond;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 回购业务处理类
 * <p>
 * ClassName: CfetsImixRepo <br/>
 * Function: 落地外汇中质押式回购和买断式回购交易数据. <br/>
 * Reason: 审批+处理后送入OPICS系统中. <br/>
 * date: 2018-5-30 下午04:03:56 <br/>
 *
 * @author shenzl
 * @since JDK 1.6
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class CfetsImixRepo implements ICfetsImixRepo {
    @Autowired
    private IfsCfetsrmbCrMapper cfetsrmbCrMapper;
    @Autowired
    private IfsCfetsrmbOrMapper cfetsrmbOrMapper;
    @Autowired
    private CfetsRmbService cfetsService;
    @Autowired
    private IfsCfetsrmbDetailCrMapper detailCrMapper;
    @Autowired
    private IfsCfetsPackMsgMapper cfetsPackMsgMapper;
    @Autowired
    private IfsOpicsBondMapper ifsOpicsBondMapper;
    private static Logger logger = LoggerFactory.getLogger(CfetsImixRepo.class);

    @Override
    public RetBean send(RepoCrBean repoCrBean, CfetsPackMsgBean cfetsPackMsg) {

        try {
            logger.info("保存下行交易CR报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易CR报文异常:", e1);
        }

        RetBean rtb = null;
        try {
            logger.info("质押式回购输入对象：" + repoCrBean.toString());
            IfsCfetsrmbCr cr = new IfsCfetsrmbCr();
            cr.setfPrdCode(TradeConstants.ProductCode.CR);
            SecurSettBean antiRepo = repoCrBean.getSettInfo(); // 正回购
            SecurSettBean sellRepo = repoCrBean.getContraPartySettInfo(); // 逆回购
            cr.setMyDir(PsEnum.P.toString()); // 本方方向
            cr.setOppoDir(PsEnum.S.toString()); // 对手方方向

            if (!repoCrBean.getPs().equals(PsEnum.P)) { // 融出
                cr.setMyDir(PsEnum.S.toString()); // 本方方向
                cr.setOppoDir(PsEnum.P.toString()); // 对手方方向
            }
            sellRepo = repoCrBean.getSettInfo();
            antiRepo = repoCrBean.getContraPartySettInfo();

            Date startDate = repoCrBean.getComSecuritySettAmt().getSettDate();
            Date endDate = repoCrBean.getMatSecuritySettAmt().getSettDate();
            String tenor = DateUtil.daysBetween(startDate, endDate) + "";

            // 交易状态
            if (repoCrBean.getStatus() != null) {
                cr.setDealTransType(StringUtils.trimToEmpty(repoCrBean.getStatus().toString()));
            } else {
                cr.setDealTransType("1");
            }
            // 交易模式
            if (repoCrBean.getDealMode() != null) {
                if (StringUtil.isNotEmpty(repoCrBean.getDealMode().toString())) {
                    if ("Enquiry".equals(repoCrBean.getDealMode().toString())) {// 询价
                        cr.setTradingModel("02");
                    } else if ("Competition".equals(repoCrBean.getDealMode().toString())) {// 竟价
                        cr.setTradingModel("01");
                    } else if ("Married".equals(repoCrBean.getDealMode().toString())) {
                        cr.setTradingModel("05");
                    }
                }
            }

            // 成交单编号
            cr.setTicketId(cfetsService.getTicketId("CR"));
            //交易来源
            cr.setDealSource("01");
            // 成交单编号
            cr.setContractId(repoCrBean.getExecId());
            // 成交日期
            cr.setForDate(DateUtil.format(repoCrBean.getDealDate()));
            // 录入日期
            cr.setInputTime(repoCrBean.getTradeTime());
            // 本方交易员
            cr.setPositiveTrader(repoCrBean.getInstitutionInfo().getTradeId());
            // 本方机构
            cr.setPositiveInst(cfetsService.getLocalInstitution(cr, repoCrBean.getInstitutionInfo().getInstitutionId(), null));
            // 对方机构
            cr.setReverseInst(cfetsService.getLocalInstitution(cr, repoCrBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
            // 对手方交易员
            cr.setReverseTrader(repoCrBean.getContraPatryInstitutionInfo().getTradeId());

            cr.setUnderlyingQty(repoCrBean.getTotalFaceAmt().divide(BigDecimal.valueOf(10000))); // 券面总额
            cr.setTradeAmount(repoCrBean.getAmtDeail().getAmt()); // 交易金额
            cr.setTradingProduct(repoCrBean.getKindId()); // 交易品种
            cr.setRepoRate(repoCrBean.getRate()); // 回购利率
            cr.setFirstSettlementMethod(convertSettType(repoCrBean.getComSecuritySettAmt().getSettType())); // 首次结算方式
            cr.setFirstSettlementDate(DateUtil.format(startDate)); // 首次结算日
            cr.setSecondSettlementMethod(convertSettType(repoCrBean.getMatSecuritySettAmt().getSettType())); // 到期结算方式
            cr.setSecondSettlementDate(DateUtil.format(endDate)); // 到期结算日
            cr.setTenor(tenor); // 回购期限
            cr.setOccupancyDays(BigDecimal.valueOf(new Double(tenor))); // 实际占款天数
            cr.setAccuredInterest(repoCrBean.getAmtDeail().getInterestTotalAmt()); // 应计利息
            cr.setSecondSettlementAmount(repoCrBean.getMatSecuritySettAmt().getSettAmt()); // 到期结算金额

            // 正回购方资金账户户名
            cr.setPositiveAccname(sellRepo.getAcctName());
            // 正回购方资金开户行
            cr.setPositiveOpbank(sellRepo.getBankName());
            // 正回购资金账号
            cr.setPositiveAccnum(sellRepo.getAcctNo());
            // 正回购支付系统行号
            cr.setPositivePsnum(sellRepo.getBankAcctNo());
            // 正回购托管账户户名
            cr.setPositiveCaname(sellRepo.getTrustAcctName());
            // 正回购托管机构
            String positiveCustname = sellRepo.getTrustInstitutionId();
            if ("上海清算所".equals(positiveCustname)) {
                positiveCustname = "SHQS";
            } else if ("国债登记结算公司".equals(positiveCustname)) {
                positiveCustname = "CDTC";
            }
            cr.setPositiveCustname(positiveCustname);
            // 正回购托管账号
            cr.setPositiveCanum(sellRepo.getTrustAcctNo());
            // 资金账户户名
            cr.setReverseAccname(antiRepo.getAcctName());
            // 资金开户行
            cr.setReverseOpbank(antiRepo.getBankName());
            // 资金账号
            cr.setReverseAccnum(antiRepo.getAcctNo());
            // 支付系统行号
            cr.setReversePsnum(antiRepo.getBankAcctNo());
            // 托管账户户名
            cr.setReverseCaname(antiRepo.getTrustAcctName());
            // 托管机构
            String reverseCustname = antiRepo.getTrustInstitutionId();
            if ("上海清算所".equals(reverseCustname)) {
                reverseCustname = "SHQS";
            } else if ("国债登记结算公司".equals(reverseCustname)) {
                reverseCustname = "CDTC";
            }
            cr.setReverseCustname(reverseCustname);
            // 托管账号
            cr.setReverseCanum(antiRepo.getTrustAcctNo());
            cr.setApproveStatus("3");
            cr.setApplyProd(TradeConstants.MarketIndicator.COLLATERAL_REPO);

            TaUser user = cfetsService.userMapping(cr, cr.getPositiveTrader());
            if (null == user) {
                cr.setPositiveTrader(SystemProperties.cfetsDefautUserId);
                cr.setPositiveInst(SystemProperties.cfetsDefautUserInstId);
            } else {
                cr.setPositiveTrader(user.getUserId());
                cr.setPositiveInst(user.getInstId());
            }

            cfetsrmbCrMapper.insert(cr);

            List<SecurityInfoBean> list = repoCrBean.getSecursInfo();
            BigDecimal tradeAmount = cr.getTradeAmount();
            if (null != list && list.size() > 0) {
                detailCrMapper.deleteByTicketId(cr.getTicketId());
                for (int i = 0; i < list.size(); i++) {
                    IfsCfetsrmbDetailCr crDetail = new IfsCfetsrmbDetailCr();
                    SecurityInfoBean securInfo = list.get(i);
                    crDetail.setTicketId(cr.getTicketId());
                    crDetail.setBondCode(securInfo.getSecurityId()); // 债券编码
                    crDetail.setBondName(securInfo.getSecurityName()); // 债券名称
                    crDetail.setTotalFaceValue(securInfo.getFaceAmt().divide(BigDecimal.valueOf(10000))); // 债券面值
                    crDetail.setUnderlyingStipType(
                            securInfo.getHaircut().multiply(BigDecimal.valueOf(new Double(100)))); // 折算比例
                    crDetail.setSeq(String.valueOf(i));
                    //4700
                    BigDecimal disAmt = securInfo.getFaceAmt().multiply(securInfo.getHaircut());//当前抵押债交易金额
                    tradeAmount = tradeAmount.subtract(disAmt);
                    crDetail.setParValue(disAmt);//债券面值*折算比例
                    if (list.size() != 1 && list.size() - 1 == i) {
                        crDetail.setParValue(tradeAmount.abs());
                    }
                    detailCrMapper.insert(crDetail);
                }
            }

            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.error("质押式回购下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }

        logger.info("质押式回购输出对象：" + rtb.toString());
        return rtb;
    }

    @Override
    public RetBean send(RepoOrBean repoOrBean, CfetsPackMsgBean cfetsPackMsg) {

        try {
            logger.info("保存下行交易OR报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易OR报文异常:", e1);
        }

        RetBean rtb = null;
        try {
            logger.info("买断式回购输入对象：" + repoOrBean.toString());
            IfsCfetsrmbOr or = new IfsCfetsrmbOr();
            or.setfPrdCode(TradeConstants.ProductCode.OR);
            or.setMyDir(PsEnum.P.toString()); // 本方方向
            or.setOppoDir(PsEnum.S.toString()); // 对手方方向
            SecurSettBean antiRepo = repoOrBean.getSettInfo(); // 逆回购
            SecurSettBean sellRepo = repoOrBean.getContraPartySettInfo(); // 正回购

            if (!repoOrBean.getPs().equals(PsEnum.P)) { // 正回购
                or.setMyDir(PsEnum.S.toString()); // 本方方向
                or.setOppoDir(PsEnum.P.toString()); // 对手方方向
            }
            sellRepo = repoOrBean.getSettInfo();
            antiRepo = repoOrBean.getContraPartySettInfo();

            Date startSettDate = repoOrBean.getComSecurityAmt().getSettDate();
            Date endSettDate = repoOrBean.getMatSecurityAmt().getSettDate();
            String tenor = DateUtil.daysBetween(startSettDate, endSettDate) + "";

            // 交易状态
            if (repoOrBean.getStatus() != null) {
                or.setDealTransType(StringUtils.trimToEmpty(repoOrBean.getStatus().toString()));
            } else {
                or.setDealTransType("1");
            }

            // 交易模式
            if (repoOrBean.getDealMode() != null) {
                if (StringUtil.isNotEmpty(repoOrBean.getDealMode().toString())) {
                    if ("Enquiry".equals(repoOrBean.getDealMode().toString())) {// 询价
                        or.setTradingModel("02");
                    } else if ("Competition".equals(repoOrBean.getDealMode().toString())) {// 竟价
                        or.setTradingModel("01");
                    } else if ("Married".equals(repoOrBean.getDealMode().toString())) {
                        or.setTradingModel("05");
                    }
                }
            }
            // 成交编号
            or.setTicketId(cfetsService.getTicketId("OR"));
            //交易来源
            or.setDealSource("01");
            // 成交编号
            or.setContractId(repoOrBean.getExecId());
            // 录入时间
            or.setInputTime(repoOrBean.getTradeTime());
            // 成交日期
            or.setForDate(DateUtil.format(repoOrBean.getDealDate()));
            // 本方机构
            or.setPositiveInst(cfetsService.getLocalInstitution(or, repoOrBean.getInstitutionInfo().getInstitutionId(), null));
            // 本方交易员
            or.setPositiveTrader(repoOrBean.getInstitutionInfo().getTradeId());
            // 对手方机构
            or.setReverseInst(cfetsService.getLocalInstitution(or, repoOrBean.getContraPatryInstitutionInfo().getInstitutionId(), null));
            // 对手方交易员
            or.setReverseTrader(repoOrBean.getContraPatryInstitutionInfo().getTradeId());
            // 债券代码
            or.setBondCode(repoOrBean.getSecurityInfo().getSecurityId());
            // 债券名称
            or.setBondName(repoOrBean.getSecurityInfo().getSecurityName());
            // 交易品种
            or.setTradingProduct(repoOrBean.getKindId());
            // 首次结算日
            or.setFirstSettlementDate(DateUtil.format(startSettDate));
            // 到期结算日
            or.setSecondSettlementDate(DateUtil.format(endSettDate));
            // 回购期限
            or.setTenor(tenor);
            // 占款天数
            or.setOccupancyDays(BigDecimal.valueOf(new Double(tenor)));
            // 债券面值总额
            or.setTotalFaceValue(repoOrBean.getSecurityInfo().getFaceAmt().divide(BigDecimal.valueOf(10000)));
            // 回收利率
            or.setRepoRate(repoOrBean.getSecurityInfo().getHaircut());
            // 首次收益率
            or.setFirstYield(repoOrBean.getComSecurityAmt().getYeld());
            // 首次净价
            or.setFirstCleanPrice(repoOrBean.getComSecurityAmt().getPrice());
            // 首次应计算利息
            or.setFirstAccuredInterest(repoOrBean.getComSecurityAmt().getInterestAmt());
            // 到期收益率
            or.setSecondYield(repoOrBean.getMatSecurityAmt().getYeld());
            // 到期净价
            or.setSecondCleanPrice(repoOrBean.getMatSecurityAmt().getPrice());
            // 到期应计利息
            or.setSecondAccuredInterest(repoOrBean.getMatSecurityAmt().getInterestAmt());
            // 首次结算金额x
            or.setFirstSettlementAmount(repoOrBean.getComSecurityAmt().getSettAmt());
            // 首次全价
            or.setFirstDirtyPrice(repoOrBean.getComSecurityAmt().getDirtyPrice());
            // 首次结算方式
            or.setFirstSettlementMethod(convertSettType(repoOrBean.getComSecurityAmt().getSettType()));
            // 到期结算金额x
            or.setSecondSettlementAmount(repoOrBean.getMatSecurityAmt().getSettAmt());
            // 到期全价
            or.setSecondDirtyPrice(repoOrBean.getMatSecurityAmt().getDirtyPrice());
            // 到期结算方式
            or.setSecondSettlementMethod(convertSettType(repoOrBean.getMatSecurityAmt().getSettType()));

            // 正回购方资金账户户名
            or.setPositiveAccname(sellRepo.getAcctName());
            // 正回购方资金开户行
            or.setPositiveOpenBank(sellRepo.getBankName());
            // 正回购资金账号
            or.setPositiveAccnum(sellRepo.getAcctNo());
            // 正回购支付系统行号
            or.setPositivePsnum(sellRepo.getBankAcctNo());
            // 正回购托管账户户名
            or.setPositiveCaname(sellRepo.getTrustAcctName());
            // 正回购托管机构
            String positiveCustname = sellRepo.getTrustInstitutionId();
            if ("上海清算所".equals(positiveCustname)) {
                positiveCustname = "SHQS";
            } else if ("国债登记结算公司".equals(positiveCustname)) {
                positiveCustname = "CDTC";
            }
            or.setPositiveCustname(positiveCustname);
            // 逆回购托管账号
            or.setPositiveCanum(sellRepo.getTrustAcctNo());
            // 资金账户户名
            or.setReverseAccname(antiRepo.getAcctName());
            // 资金开户行
            or.setReverseOpenBank(antiRepo.getBankName());
            // 资金账号
            or.setReverseAccnum(antiRepo.getAcctNo());
            // 支付系统行号
            or.setReversePsnum(antiRepo.getBankAcctNo());
            // 托管账户户名
            or.setReverseCaname(antiRepo.getTrustAcctName());
            // 托管机构
            String reverseCustname = antiRepo.getTrustInstitutionId();
            if ("上海清算所".equals(reverseCustname)) {
                reverseCustname = "SHQS";
            } else if ("国债登记结算公司".equals(reverseCustname)) {
                reverseCustname = "CDTC";
            }
            or.setReverseCustname(reverseCustname);
            // 托管账号
            or.setReverseCanum(antiRepo.getTrustAcctNo());
            or.setApproveStatus("3");
            or.setApplyProd(TradeConstants.MarketIndicator.OUTRIGHT_REPO);

            TaUser user = cfetsService.userMapping(or, or.getPositiveTrader());
            if (null == user) {
                or.setPositiveTrader(SystemProperties.cfetsDefautUserId);
                or.setPositiveInst(SystemProperties.cfetsDefautUserInstId);
            } else {
                or.setPositiveTrader(user.getUserId());
                or.setPositiveInst(user.getInstId());
            }

            cfetsrmbOrMapper.insert(or);

            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.info("买断式回购下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }
        logger.info("买断式回购输出对象：" + rtb.toString());

        return rtb;
    }

    private String convertSettType(String setttype) {
        if ("0".equals(StringUtils.trimToEmpty(setttype))) {// DVP
            return "DVP";
        } else if ("1".equals(StringUtils.trimToEmpty(setttype))) {//PUD
            return "PAD";
        } else if ("3".equals(StringUtils.trimToEmpty(setttype))) {//DUP
            return "DAP";
        } else if ("4".equals(StringUtils.trimToEmpty(setttype))) {// NDVP
            return "FOP";
        }
        return null;
    }


    @Override
    public RetBean send(SLFBean SLF, CfetsPackMsgBean cfetsPackMsg) {

        try {
            logger.info("保存下行交易常备借贷便利报文");
            // 报文下行报文
            cfetsPackMsgMapper.addcfetsPackage(cfetsPackMsg);
        } catch (Exception e1) {
            logger.error("保存下行交易常备借贷便利报文异常:", e1);
        }

        RetBean rtb = null;
        try {
            logger.info("常备借贷便利输入对象：" + SLF.toString());
            IfsCfetsrmbCr cr = new IfsCfetsrmbCr();

            cr.setfPrdCode(TradeConstants.ProductCode.CRCB);
            cr.setMyDir(PsEnum.S.toString()); // 本方方向(正回购)
            cr.setOppoDir(PsEnum.P.toString()); // 对手方方向(逆回购)

            String tenor = SLF.getOccupancyDays();

            // 交易状态
            if (SLF.getStatus() != null) {
                cr.setDealTransType(StringUtils.trimToEmpty(SLF.getStatus().toString()));
            } else {
                cr.setDealTransType("1");
            }
            // 交易模式
            if (SLF.getDealMode() != null) {
                if (StringUtil.isNotEmpty(SLF.getDealMode().toString())) {
                    if ("Enquiry".equals(SLF.getDealMode().toString())) {// 询价
                        cr.setTradingModel("02");
                    } else if ("Competition".equals(SLF.getDealMode().toString())) {// 竟价
                        cr.setTradingModel("01");
                    } else if ("Married".equals(SLF.getDealMode().toString())) {
                        cr.setTradingModel("05");
                    }
                }
            }

            // 成交单编号
            cr.setTicketId(cfetsService.getTicketId("CR"));
            //交易来源
            cr.setDealSource("01");
            // 成交单编号
            cr.setContractId(SLF.getExecId());
            // 成交日期
            cr.setForDate(DateUtil.format(SLF.getDealDate()));
            // 录入日期
            cr.setInputTime(SLF.getTradeTime());
            // 本方交易员
            cr.setPositiveTrader(SLF.getInstitutionInfo().getTradeId());
            // 本方机构
            cr.setPositiveInst(cfetsService.getLocalInstitution(cr, SLF.getInstitutionInfo().getInstitutionId(), null));
            // 对方机构
            cr.setReverseInst(cfetsService.getLocalInstitution(cr, SLF.getContraPatryInstitutionInfo().getInstitutionId(), null));
            // 对手方交易员
            cr.setReverseTrader(SLF.getContraPatryInstitutionInfo().getTradeId());

            cr.setUnderlyingQty(SLF.getLastQty().divide(BigDecimal.valueOf(10000))); // 券面总额
            cr.setTradeAmount(SLF.getTradeCashAmt()); // 交易金额
            cr.setTradingProduct(SLF.getKindId()); // 交易品种
            cr.setRepoRate(SLF.getPrice()); // 回购利率
            cr.setFirstSettlementMethod(convertSettType(SLF.getDeliveryType())); // 首次结算方式
            cr.setFirstSettlementDate(DateUtil.format(SLF.getSettlDate())); // 首次结算日
            cr.setSecondSettlementMethod(convertSettType(SLF.getDeliveryType())); // 到期结算方式
            cr.setSecondSettlementDate(DateUtil.format(SLF.getSettlDate2())); // 到期结算日
            cr.setTenor(tenor); // 回购期限
            //\][oi  cr.setOccupancyDays(new BigDecimal(SLF.getOccupancyDays())); // 实际占款天数
            if (SLF.getAccruedInterestAmt() == null) {
                BigDecimal AccuredInterest = SLF.getSettlAmt2().divide(SLF.getTradeCashAmt());
                cr.setAccuredInterest(AccuredInterest); // 应计利息
            }
            cr.setAccuredInterest(SLF.getAccruedInterestAmt()); // 应计利息
            cr.setSecondSettlementAmount(SLF.getSettlAmt2()); // 到期结算金额

            cr.setProdType("CB");//产品类型（常备借贷便利）
//
//            // 正回购方资金账户户名
//            cr.setPositiveAccname(sellRepo.getAcctName());
//            // 正回购方资金开户行
//            cr.setPositiveOpbank(sellRepo.getBankName());
            // 正回购资金账号
            cr.setPositiveAccnum(SLF.getsLFPledegBean().getSignFundID());
            // 正回购支付系统行号
//            cr.setPositivePsnum(sellRepo.getBankAcctNo());
//            //正回购托管账户户名
//            cr.setPositiveCaname(sellRepo.getTrustAcctName());
            // 正回购托管机构
            Integer positiveCustId = SLF.getCollateralType();

            if (positiveCustId.equals(5)) {
                String positiveCustname = "SHQS";
                cr.setPositiveCustname(positiveCustname);
            } else if (positiveCustId.equals(6)) {
                String positiveCustname = "CDTC";
                cr.setPositiveCustname(positiveCustname);
            }

            // 正回购托管账号
            cr.setPositiveCanum(SLF.getsLFPledegBean().getSignEscrowID());
//            // 逆回购资金账户户名
//            cr.setReverseAccname(antiRepo.getAcctName());
//            // 逆回购资金开户行
//            cr.setReverseOpbank(antiRepo.getBankName());
//            // 逆回购资金账号
//            cr.setReverseAccnum(SLF.getsLFPledegBean().getSignFundID());
//            // 逆回购支付系统行号
//            cr.setReversePsnum(antiRepo.getBankAcctNo());
//            // 逆回购托管账户户名
//            cr.setReverseCaname(antiRepo.getTrustAcctName());
//            // 逆回购托管机构
//            Integer reverseCustid = SLF.getCollateralType();
//            if ("5".equals(reverseCustid)) {
//                String   reverseCustName = "SHQS";
//                cr.setReverseCustname(reverseCustName);
//               } else if ("6".equals(reverseCustid)) {
//                   String  reverseCustName = "CDTC";
//              cr.setReverseCustname(reverseCustName);
//               }
//          
//            // 逆回购托管账号
//            cr.setReverseCanum(SLF.getsLFPledegBean().getSignEscrowID());
            cr.setApproveStatus("3");

            TaUser user = cfetsService.userMapping(cr, cr.getPositiveTrader());

            String user1 = SystemProperties.cfetsDefautUserId;
            if (null == user) {
                cr.setPositiveTrader(SystemProperties.cfetsDefautUserId);
                cr.setPositiveInst(SystemProperties.cfetsDefautUserInstId);
            } else {
                cr.setPositiveTrader(user.getUserId());
                cr.setPositiveInst(user.getInstId());
            }

            cfetsrmbCrMapper.insert(cr);

            List<SLFSettAmtBean> list = SLF.getsLFSettAmtBean();
            if (null != list && list.size() > 0) {
                detailCrMapper.deleteByTicketId(cr.getTicketId());
                for (int i = 0; i < list.size(); i++) {
                    IfsCfetsrmbDetailCr crDetail = new IfsCfetsrmbDetailCr();
                    SLFSettAmtBean SLFSettAmtBean = list.get(i);
                    crDetail.setTicketId(cr.getTicketId());
                    crDetail.setBondCode(SLFSettAmtBean.getUnderlyingSecurityID()); // 债券编码
                    IfsOpicsBond Bond = ifsOpicsBondMapper.searchById(SLFSettAmtBean.getUnderlyingSecurityID().trim());
                    if (Bond != null) {
                        crDetail.setBondName(Bond.getBndnm_cn()); // 债券名称
                    }

                    crDetail.setTotalFaceValue(SLFSettAmtBean.getUnderlyingQty().divide(BigDecimal.valueOf(10000))); // 债券面值
                    crDetail.setUnderlyingStipType(SLFSettAmtBean.getUnderlyingStipType()); // 折算比例
                    crDetail.setSeq(String.valueOf(i));
                    detailCrMapper.insert(crDetail);
                }
            }

            rtb = new RetBean(RetStatusEnum.S, "success", "交易发送成功");
        } catch (Exception e) {
            logger.error("常备借贷便利下行交易处理异常：", e);
            rtb = new RetBean(RetStatusEnum.F, "error" + e.getMessage(), "交易发送失败");
        }

        logger.info("常备借贷便利输出对象：" + rtb.toString());
        return rtb;
    }


}
