package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import com.singlee.ifs.model.TdQryMsg;
import com.singlee.ifs.service.QryMsgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用于报文反馈信息的查询
 * @author Administrator
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class QryMsgServiceImpl implements QryMsgService{

	@Resource
	TdQryMsgMapper tdQryMsgMapper;
	
	
	
	/***
	 * //根据条件查询报文反馈信息
	 */
	@Override
	public Page<TdQryMsg> searchQryMsg(Map<String, Object> params) {
		Page<TdQryMsg> page = new Page<TdQryMsg>();
		page = tdQryMsgMapper.searchQryMsg(params, ParameterUtil.getRowBounds(params));
		return page;
	}
	
	@Override
	public void update(Map<String, Object> params) {
		tdQryMsgMapper.update(params);
	}

	@Override
	public Page<TdQryMsg> searchNoSendDeal(Map<String, Object> params) {
		Page<TdQryMsg> page = new Page<TdQryMsg>();
		page = tdQryMsgMapper.searchNoSendDeal(params, ParameterUtil.getRowBounds(params));
		return page;
	}
}
