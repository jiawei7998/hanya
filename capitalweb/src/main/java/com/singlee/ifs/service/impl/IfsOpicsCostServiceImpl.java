package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlCostBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsCostMapper;
import com.singlee.ifs.model.IfsOpicsCost;
import com.singlee.ifs.service.IfsOpicsCostService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsCostServiceImpl implements IfsOpicsCostService {
	@Resource
	IfsOpicsCostMapper ifsOpicsCostMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsCost entity) {
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsCostMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsCost entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsCostMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsCostMapper.deleteById(id);

	}

	@Override
	public Page<IfsOpicsCost> searchPageOpicsCost(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsCost> result = ifsOpicsCostMapper.searchPageOpicsCost(map, rb);
		return result;
	}

	@Override
	public List<IfsOpicsCost> searchCost(HashMap<String, Object> map) {
		List<IfsOpicsCost> list = ifsOpicsCostMapper.searchCost(map);
		return TreeUtil.mergeChildrenList(list, "-1");
	}

	@Override
	public IfsOpicsCost searchById(String id) {
		return ifsOpicsCostMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsCost entity) {
		ifsOpicsCostMapper.updateStatus(entity);
	}

	@Override
	public SlOutBean batchCalibration(String type, String[] costcents) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从OPICS库查询所有数据
			List<SlCostBean> listCostOpics = iStaticServer.synchroCost();

			/**************** 对选中的行进行校准 **********************/
			if ("1".equals(type)) { // 选中行校准
				for (int i = 0; i < costcents.length; i++) {
					for (int j = 0; j < listCostOpics.size(); j++) {
						// 若OPICS库中有相同的成本中心，就将本地的更新成OPICS中的，其中备注保持不变
						if (listCostOpics.get(j).getCostCent().equals(costcents[i])) {
							IfsOpicsCost entity = new IfsOpicsCost();
							entity = ifsOpicsCostMapper.searchById(costcents[i]);

							entity.setCostcent(listCostOpics.get(j).getCostCent());
							entity.setCostdesc(listCostOpics.get(j).getCostDesc());
							entity.setBusunit(listCostOpics.get(j).getBusunit());
							entity.setOrgunit(listCostOpics.get(j).getOrgunit());
							entity.setLstmntdte(DateUtil.parse(listCostOpics.get(j).getLstmntdte(), "yyyy-MM-dd hh:mm:ss"));
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1"); // 设置为已同步
							ifsOpicsCostMapper.updateById(entity);
							break;
						}
						// 若OPICS库中无数据，则将状态改为'未同步'，其余不变
						if (j == listCostOpics.size() - 1) {
							ifsOpicsCostMapper.updateStatus0ById(costcents[i]);
						}
					}
				}

				/**************** 对本地库所有成本中心进行校准 **********************/
			} else { // 全部校准
				// 从本地库查询所有的数据
				List<IfsOpicsCost> listCostLocal = ifsOpicsCostMapper.searchAllOpicsCost();
				/** 1、以OPICS库为主，更新或插入到本地库 */
				for (int i = 0; i < listCostOpics.size(); i++) {

					// 若全部不同，则插入到本地库
					if (listCostLocal.size() == 0) {
						IfsOpicsCost entity = new IfsOpicsCost();
						entity.setCostcent(listCostOpics.get(i).getCostCent());
						entity.setCostdesc(listCostOpics.get(i).getCostDesc());
						entity.setBusunit(listCostOpics.get(i).getBusunit());
						entity.setOrgunit(listCostOpics.get(i).getOrgunit());
						entity.setLstmntdte(DateUtil.parse(listCostOpics.get(i).getLstmntdte(), "yyyy-MM-dd hh:mm:ss"));
						entity.setOperator(SlSessionHelper.getUserId());
						entity.setStatus("1"); // 设置为已同步
						ifsOpicsCostMapper.insert(entity);
					}

					for (int j = 0; j < listCostLocal.size(); j++) {
						IfsOpicsCost entity = new IfsOpicsCost();
						entity.setCostdesccn(listCostLocal.get(j).getCostdesccn());//成本中心中文描述
						
						entity.setCostcent(listCostOpics.get(i).getCostCent());
						entity.setCostdesc(listCostOpics.get(i).getCostDesc());
						entity.setBusunit(listCostOpics.get(i).getBusunit());
						entity.setOrgunit(listCostOpics.get(i).getOrgunit());
						entity.setLstmntdte(DateUtil.parse(listCostOpics.get(i).getLstmntdte(), "yyyy-MM-dd hh:mm:ss"));
						entity.setOperator(SlSessionHelper.getUserId());
						entity.setStatus("1"); // 设置为已同步
						// 与OPICS库中的会计类型对比，相同的内容不更新，只更新不同的内容
						if (listCostOpics.get(i).getCostCent().equals(listCostLocal.get(j).getCostcent())) {
							ifsOpicsCostMapper.updateById(entity);
							break;
						}
						// 若全部不同，则插入本地库
						if (j == listCostLocal.size() - 1) {
							ifsOpicsCostMapper.insert(entity);
						}
					}
				}
				/** 2、若本地库中的数据多于OPICS库，则将本地库中含有的但OPICS库中没有的数据的状态改为'未同步' */
				// 遍历完OPICS库之后，从本地库【再次】查询所有数据，遍历本地库
				List<IfsOpicsCost> listCostLocal2 = ifsOpicsCostMapper.searchAllOpicsCost();
				if (listCostLocal2.size() > listCostOpics.size()) {
					for (int i = 0; i < listCostLocal2.size(); i++) {
						for (int j = 0; j < listCostOpics.size(); j++) {
							if (listCostOpics.get(j).getCostCent().trim().equals(listCostLocal2.get(i).getCostcent())) {
								break;
							}
							// 若OPICS库中没有，则将状态改为'未同步'
							if (j == listCostOpics.size() - 1) {
								IfsOpicsCost cost = new IfsOpicsCost();
								cost.setCostcent(listCostLocal2.get(i).getCostcent());
								cost.setCostdesc(listCostLocal2.get(i).getCostdesc());
								cost.setBusunit(listCostLocal2.get(i).getBusunit());
								cost.setCostdesccn(listCostLocal2.get(i).getCostdesccn());
								cost.setOrgunit(listCostLocal2.get(i).getOrgunit());
								cost.setRemark(listCostLocal2.get(i).getRemark());
								cost.setLstmntdte(new Date());
								cost.setOperator(SlSessionHelper.getUserId());
								cost.setStatus("0"); // 设置为未同步
								cost.setCostcentpid(listCostLocal2.get(i).getCostcentpid());
								ifsOpicsCostMapper.updateById(cost);
							}
						}
					}
				}
			}
		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

}
