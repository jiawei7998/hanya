package com.singlee.ifs.service.impl;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class Cn2Spell {
	/**
	 * 汉字转换位汉语拼音首字母，英文字符不变?
	 * 
	 * @param chines
	 *            汉字
	 * @return 拼音
	 */
	public static String converterToFirstSpell(String chines) {
		if(null ==chines || "".equals(chines)) return  "XX";
		chines=chines.replaceAll("I", "1");
		chines=chines.replaceAll("II", "2");
		chines=chines.replaceAll("III", "3");
		chines=chines.replaceAll("IV", "4");
		chines=chines.replaceAll("V", "5");
		chines=chines.replaceAll("Ⅰ", "1");
		chines=chines.replaceAll("Ⅱ", "2");
		chines=chines.replaceAll("Ⅲ", "3");
		chines=chines.replaceAll("Ⅳ", "4");
		chines=chines.replaceAll("Ⅴ", "5");
		String pinyinName = "";
		char[] nameChar = chines.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < nameChar.length; i++) {
			if (nameChar[i] > 128) {
				try {
					pinyinName += PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat)[0].charAt(0);
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}

				continue;

			} else {
				pinyinName += nameChar[i];
			}

		}
		return pinyinName;
	}

	/**
	 * 汉字转换位汉语拼音，英文字符不变
	 * 
	 * @param chines
	 *            汉字
	 * @return 拼音
	 */
	public static String converterToSpell(String chines) {
		String pinyinName = "";
		char[] nameChar = chines.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < nameChar.length; i++) {
			if (nameChar[i] > 128) {
				try {
					pinyinName += PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat)[0];
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}

				continue;

			} else {
				pinyinName += nameChar[i];
			}

		}
		return pinyinName;
	}

	/**
	 * 判断字符串中是否包含全角字符,并且将全角字符转换为半角字符
	 * 
	 * @param str
	 * @return
	 */
	public static String isAllHalf(String str) {
		int length = str.length();
		int byteLength = str.getBytes().length;

		if (byteLength == length) {
			return str;
		} else {
			return Cn2Spell.ToDBC(str);
		}
	}

	/**
	 * 全角转半角
	 * 
	 * @param input
	 *            String.
	 * @return 半角字符串
	 */
	public static String ToDBC(String input) {

		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '\u3000') {
				c[i] = ' ';
			} else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
				c[i] = (char) (c[i] - 65248);

			}
		}
		String returnString = new String(c);

		return returnString;
	}

	public static void main(String[] args) {
		System.out.println(Cn2Spell.isAllHalf("05沪电力CP02"));
		System.out.println(Cn2Spell.converterToSpell("05沪电力CP02"));
		System.out.println(Cn2Spell.converterToFirstSpell("05沪电力CP02"));
		/*System.out.println("05浦发０２");
		System.out.println(Cn2Spell.ToDBC("05浦发０２"));*/
	}
}
