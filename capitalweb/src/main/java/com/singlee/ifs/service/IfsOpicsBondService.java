package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsBond;

import java.util.List;
import java.util.Map;

/***
 * 交易要素 service
 * 
 * @author singlee4
 * 
 */
public interface IfsOpicsBondService {

	// 新增
	public String insert(IfsOpicsBond entity);

	// 修改
	void updateById(IfsOpicsBond entity);

	// 删除
	void deleteById(String bndcd);

	// 分页债券信息查询
	Page<IfsOpicsBond> searchPageBondTrd(Map<String, Object> map);

	// 分页债券复核查询
	Page<IfsOpicsBond> searchPageCheckTrd(Map<String, Object> map);

	public String doBondExcelPropertyToDataBase(List<IfsOpicsBond> ifsOpicsBonds);
	
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeById(Map<String, Object> map);
	
	// 查询单个
	public IfsOpicsBond searchOneById(String bndcd);
	
	// 批量校准
	SlOutBean batchCalibration(String type,String[] bndcds);

}
