package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCreditRiskreview;

import java.util.Map;

/***
 * opics 交易对手表 service
 * 
 * @author singlee4
 */
public interface IfsOpicsRiskService {
	// 分页查询
	Page<IfsCreditRiskreview> searchPageOpicsRisk(Map<String, Object> map);
}