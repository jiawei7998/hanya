package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.ifs.mapper.IfsFlowMonitorMapper;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.service.IfsFlowMonitorService;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service
public class IfsFlowMonitorServiceImpl implements IfsFlowMonitorService {
	@Resource
	IfsFlowMonitorMapper ifsFlowMonitorMapper;
	@Autowired
	TtFlowSerialMapMapper ttFlowSerialMapMapper;
	@Autowired
	HistoryService historyService;
	@Autowired
	UserService userService;
	private static Logger logManager = LoggerFactory.getLogger(IfsFlowMonitorServiceImpl.class);
	@Override
	public void insert(IfsFlowMonitor entity) {
		// TODO Auto-generated method stub
		ifsFlowMonitorMapper.insert(entity);
	}
	
	@Override
	public void updateById(IfsFlowMonitor entity) {
		// TODO Auto-generated method stub
		ifsFlowMonitorMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		ifsFlowMonitorMapper.deleteById(id);
	}

	@Override
	public Page<IfsFlowMonitor> searchPageFlowMonitor(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsFlowMonitor> result = ifsFlowMonitorMapper.searchPageFlowMonitor(map, rb);

		return result;
	}

	@Override
	public Page<IfsFlowMonitor> searchPageAllFlowMonitor(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsFlowMonitor> result = ifsFlowMonitorMapper.searchPageAllFlowMonitor(map, rb);

		return result;
	}
	public List<IfsFlowMonitor> appvoreList(List<IfsFlowMonitor> list){
		for (IfsFlowMonitor ifsFlowMonitor : list) {//获得审批人列表
			 String ulist=null;
			 TtFlowSerialMap flowMap=ttFlowSerialMapMapper.get(null,ifsFlowMonitor.getTicketId(),null,0);
			 if(flowMap!=null){
	    		  List<HistoricTaskInstance> hislist=historyService.createHistoricTaskInstanceQuery().processInstanceId(String.valueOf(flowMap.getExecution())).orderByTaskCreateTime().asc().list();
	    		  for (HistoricTaskInstance his : hislist) {
	    	    	  if(StringUtil.isNotEmpty(his.getAssignee())){
	    	    		  TaUser user=userService.getUserById(his.getAssignee());
	    	    		  if(user!=null){
	    	    			  ulist=StringUtil.isEmpty(ulist)?user.getUserName():ulist+" - "+user.getUserName();
	    	    				logManager.info("	任务ID:"+his.getId()+"	任务名称:"+his.getName()+"	任务名称:"+his.getName()+"	任务的创建时间:"+his.getCreateTime()+"	任务的办理人:"+his.getAssignee()+"	流程实例ID："+his.getProcessInstanceId()+"	流程定义ID:"+his.getProcessDefinitionId());
	    	    		  }
	    	    	  }
	    	      }
	    	  }
			 ifsFlowMonitor.setApproveList(ulist);
		}
		return list;
	}
	@Override
	public Page<IfsFlowMonitor> searchDealCount(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		String user=ParameterUtil.getString(map, "oper","");
		map.put("isAdmin", 1);//查询所有审批中，审批完成交易
		Page<IfsFlowMonitor> result = null;
		if(StringUtil.isEmpty(user)){//查询所有交易  审批人未选择返回查询的交易
			result = ifsFlowMonitorMapper.searchPageAllFlowMonitor(map, rb);
			return (Page<IfsFlowMonitor>) appvoreList(result.getResult());
		}
		List <IfsFlowMonitor> list=ifsFlowMonitorMapper.searchPageAllFlowMonitor(map);
		String str=null;
		for (IfsFlowMonitor ifsFlowMonitor : list) {//获得每笔交易的审批人列表
	    	  TtFlowSerialMap flowMap=ttFlowSerialMapMapper.get(null,ifsFlowMonitor.getTicketId(),null,0);
	    	  boolean f=false;
	    	  if(flowMap!=null){
	    		  List<HistoricTaskInstance> hislist=historyService.createHistoricTaskInstanceQuery().processInstanceId(String.valueOf(flowMap.getExecution())).orderByTaskCreateTime().asc().list();
	    	      for (HistoricTaskInstance his : hislist) {//选择审批人是否存在审批列表
					   if(StringUtil.isNotEmpty(his.getAssignee())&user.equals(his.getAssignee())){
						   f=true;
						   continue;
				       }
	    	  }
	    	  if(f){//如果审批人存在，保存审批单编号
	    		  if(StringUtil.isEmpty(str)){
	    			  str="'"+ifsFlowMonitor.getTicketId()+"'";
	    		  }else{
	    			  str+=",'"+ifsFlowMonitor.getTicketId()+"'";
	    		  }
	    	  }
		 }
	 }
		if(StringUtil.isNotEmpty(str)){
			 map.put("approvelis", str);
			 result=(Page<IfsFlowMonitor>) appvoreList(ifsFlowMonitorMapper.searchPageAllFlowMonitor(map, rb));
		}
		return result;
}
}
