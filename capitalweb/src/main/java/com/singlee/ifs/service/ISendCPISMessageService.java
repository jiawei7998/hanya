package com.singlee.ifs.service;

import java.util.Map;

public interface ISendCPISMessageService {

	/**
	 * 取回反馈信息
	 * @return
	 */
	public int retrieveCpisReturnBackMsg();
	
	/**
	 * 发送外汇拆借交易后
	 * @param condMap
	 * @return
	 */
	public int sendFxLend(Map<String, Object> condMap);

	/**
	 * 发送外汇即期交易后
	 * @param condMap
	 * @return
	 */
	public int sendFxSpt(Map<String, Object> condMap);
	
	/**
	 * 发送外汇远期交易后
	 * @param condMap
	 * @return
	 */
	public int sendFxFwd(Map<String, Object> condMap);

    void sendFxSWAP(Map<String, Object> dateMap);
}
