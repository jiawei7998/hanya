/**
 * Project Name:capitalweb
 * File Name:IfsOpicsSetaServiceImpl.java
 * Package Name:com.singlee.ifs.service.impl
 * Date:2018-7-20下午05:38:53
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
 */

package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSetaBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsSetaMapper;
import com.singlee.ifs.model.IfsOpicsSeta;
import com.singlee.ifs.service.IfsOpicsSetaService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SETA
 * 
 * @author yanming
 */

@Service
public class IfsOpicsSetaServiceImpl implements IfsOpicsSetaService {

	@Resource
	IfsOpicsSetaMapper ifsOpicsSetaMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsSeta entity) {
		// 设置最后维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsSetaMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsSeta entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsSetaMapper.updateById(entity);

	}

	@Override
	public void deleteById(String id) {
		ifsOpicsSetaMapper.deleteById(id);

	}

	@Override
	public Page<IfsOpicsSeta> searchPageOpicsSeta(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsSeta> result = ifsOpicsSetaMapper.searchPageOpicsSeta(map, rb);
		return result;
	}

	@Override
	public IfsOpicsSeta searchById(Map<String, String> map) {

		return ifsOpicsSetaMapper.searchById(map);
	}

	@Override
	public void updateStatus(IfsOpicsSeta entity) {

		ifsOpicsSetaMapper.updateStatus(entity);

	}

	@Override
	public SlOutBean batchCalibration(String type, String[] smeanss, String[] saccts) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			List<SlSetaBean> listSetaOpics = iStaticServer.synchroSeta();
			HashMap<String, String> map = new HashMap<String, String>();
			if ("1".equals(type)) {// 选中行
				for (int i = 0; i < smeanss.length; i++) {
					for (int j = 0; j < listSetaOpics.size(); j++) {
						map.put("smeans", smeanss[i]);
						map.put("sacct", saccts[i]);
						if (listSetaOpics.get(j).getSmeans().trim().equals(smeanss[i])) {
							IfsOpicsSeta ifsOpicsSeta = ifsOpicsSetaMapper.searchById(map);
							String smeans = listSetaOpics.get(j).getSmeans().trim();
							String br = listSetaOpics.get(j).getBr();
							if ("".equals(br) || br == null) {
								ifsOpicsSeta.setBr("");
							} else {
								ifsOpicsSeta.setBr(br);
							}
							String sacct = listSetaOpics.get(j).getSacct();
							if ("".equals(sacct) || br == null) {
								ifsOpicsSeta.setSacct("");
							} else {
								ifsOpicsSeta.setSacct(sacct);
							}
							String ccy = listSetaOpics.get(j).getCcy();
							if ("".equals(ccy) || ccy == null) {
								ifsOpicsSeta.setCcy("");
							} else {
								ifsOpicsSeta.setCcy(ccy.toUpperCase());
							}
							Date lstmntdte = listSetaOpics.get(j).getLstmntdte();
							if ("".equals(lstmntdte) || lstmntdte == null) {
								ifsOpicsSeta.setLstmntdte(new Date());
							} else {
								ifsOpicsSeta.setLstmntdte(lstmntdte);
							}
							String costcent = listSetaOpics.get(j).getCostcent();
							if("".equals(costcent) || costcent == null){
								ifsOpicsSeta.setCostcent("");
							}else{
								ifsOpicsSeta.setCostcent(costcent);
							}
							String cno = listSetaOpics.get(j).getCno();
							if("".equals(cno) || cno == null){
								ifsOpicsSeta.setCno("");
							}else{
								ifsOpicsSeta.setCno(cno);
							}
							ifsOpicsSeta.setSmeans(smeans);
							ifsOpicsSeta.setOperator(SlSessionHelper.getUserId());
							ifsOpicsSeta.setStatus("1");
							ifsOpicsSetaMapper.updateById(ifsOpicsSeta);
							break;
						}
						if (j == listSetaOpics.size() - 1) {
							ifsOpicsSetaMapper.updateStatus0ById(smeanss[i]);
						}
					}
				}
			} else {// 所有行
				List<IfsOpicsSeta> listSetaLocal = ifsOpicsSetaMapper.searchAllOpicsSeta();
				for (int i = 0; i < listSetaOpics.size(); i++) {
					IfsOpicsSeta ifsOpicsSeta = new IfsOpicsSeta();
					String smeans = listSetaOpics.get(i).getSmeans().trim();
					String br = listSetaOpics.get(i).getBr();
					if ("".equals(br) || br == null) {
						ifsOpicsSeta.setBr("");
					} else {
						ifsOpicsSeta.setBr(br);
					}
					String sacct = listSetaOpics.get(i).getSacct();
					if ("".equals(sacct) || br == null) {
						ifsOpicsSeta.setSacct("");
					} else {
						ifsOpicsSeta.setSacct(sacct);
					}
					String ccy = listSetaOpics.get(i).getCcy();
					if ("".equals(ccy) || ccy == null) {
						ifsOpicsSeta.setCcy("");
					} else {
						ifsOpicsSeta.setCcy(ccy.toUpperCase());
					}
					Date lstmntdte = listSetaOpics.get(i).getLstmntdte();
					if ("".equals(lstmntdte) || lstmntdte == null) {
						ifsOpicsSeta.setLstmntdte(new Date());
					} else {
						ifsOpicsSeta.setLstmntdte(lstmntdte);
					}
					String costcent = listSetaOpics.get(i).getCostcent();
					if("".equals(costcent) || costcent == null){
						ifsOpicsSeta.setCostcent("");
					}else{
						ifsOpicsSeta.setCostcent(costcent);
					}
					String cno = listSetaOpics.get(i).getCno();
					if("".equals(cno) || cno == null){
						ifsOpicsSeta.setCno("");
					}else{
						ifsOpicsSeta.setCno(cno);
					}
					ifsOpicsSeta.setSmeans(smeans);
					ifsOpicsSeta.setOperator(SlSessionHelper.getUserId());
					ifsOpicsSeta.setStatus("1");
					if (listSetaLocal.size() == 0) {// 本地无数据
						ifsOpicsSetaMapper.insert(ifsOpicsSeta);
					} else {
						// 本地有数据
						for (int j = 0; j < listSetaLocal.size(); j++) {
							if (listSetaOpics.get(i).getSmeans().trim().equals(listSetaLocal.get(j).getSmeans()) && listSetaOpics.get(i).getBr().trim().equals(listSetaLocal.get(j).getBr())
									&& listSetaOpics.get(i).getSacct().trim().equals(listSetaLocal.get(j).getSacct())) {
								ifsOpicsSetaMapper.updateById(ifsOpicsSeta);
								break;
							}
							if (j == listSetaLocal.size() - 1) {
								ifsOpicsSetaMapper.insert(ifsOpicsSeta);
							}
						}
					}
				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				List<IfsOpicsSeta> listSetaLocal2 = ifsOpicsSetaMapper.searchAllOpicsSeta();
				if (listSetaLocal2.size() > listSetaOpics.size()) {
					for (int i = 0; i < listSetaLocal2.size(); i++) {
						for (int j = 0; j < listSetaOpics.size(); j++) {
							if (listSetaOpics.get(j).getSmeans().trim().equals(listSetaLocal2.get(i).getSmeans()) && listSetaOpics.get(j).getBr().trim().equals(listSetaLocal2.get(i).getBr())
									&& listSetaOpics.get(j).getSacct().trim().equals(listSetaLocal2.get(i).getSacct())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为未同步
							if (j == listSetaOpics.size() - 1) {
								IfsOpicsSeta ifsOpicsSeta = new IfsOpicsSeta();
								ifsOpicsSeta.setBr(listSetaLocal2.get(i).getBr());
								ifsOpicsSeta.setSmeans(listSetaLocal2.get(i).getSmeans());
								ifsOpicsSeta.setSacct(listSetaLocal2.get(i).getSacct());
								ifsOpicsSeta.setCcy(listSetaLocal2.get(i).getCcy());
								ifsOpicsSeta.setCostcent(listSetaLocal2.get(i).getCostcent());
								ifsOpicsSeta.setCno(listSetaLocal2.get(i).getCno());
								ifsOpicsSeta.setLstmntdte(new Date());
								ifsOpicsSeta.setStatus("0");// 设置状态为未同步
								ifsOpicsSeta.setOperator(SlSessionHelper.getUserId());
								ifsOpicsSetaMapper.updateById(ifsOpicsSeta);
							}
						}
					}
				}
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public Page<IfsOpicsSeta> searchPageOpicsSetaMini(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsSeta> result = ifsOpicsSetaMapper.searchPageOpicsSetaMini(map, rb);
		return result;
	}

}
