package com.singlee.ifs.service;

import java.util.List;
import java.util.Map;

import com.singlee.ifs.model.IfsIntfcIrevBean;

public interface IfsIntfcIrevService {
	
	List<IfsIntfcIrevBean> getlistIrev(Map<String, Object>  map);
	
	/**
     * 市场数据 先查询，后插入接口表
     */
    String doBussExcelPropertyToDataBase(List<IfsIntfcIrevBean> irevBean);

    /**
     * 市场数据 插入irev表
     */
    String dataBaseToIrev(String date);

}
