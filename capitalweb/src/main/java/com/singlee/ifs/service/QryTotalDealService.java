package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.*;
import com.singlee.refund.model.*;

import java.util.Map;

/**
 * 用于查询指定所做的所有交易
 * 
 * @author zhengss
 * 
 */
public interface QryTotalDealService {

	// 外汇即期
	public Page<IfsCfetsfxSpt> searchTotalDealRMBWBJQ(Map<String, Object> params, int isFinished);

	// 外汇远期
	public Page<IfsCfetsfxFwd> searchTotalDealRMBWBYQ(Map<String, Object> params, int isFinished);

	// 外汇掉期
	public Page<IfsCfetsfxSwap> searchTotalDealRMBWBDQ(Map<String, Object> params, int isFinished);

	// 信用拆借 同业借款
	public Page<IfsCfetsrmbIbo> searchTotalDealXYCJ(Map<String, Object> params, int isFinished);

	// 同业存款
	public Page<IfsApprovermbDeposit> searchTotalDealTYCK(Map<String, Object> params, int isFinished);

	// 现券买卖
	public Page<IfsCfetsrmbCbt> searchTotalDealXJMM(Map<String, Object> params, int isFinished);

	// 债券远期
	public Page<IfsCfetsrmbBondfwd> searchTotalDealZQYQ(Map<String, Object> params, int isFinished);

	// 债券借贷
	public Page<IfsCfetsrmbSl> searchTotalDealZJJD(Map<String, Object> params, int isFinished);

	// 债券发行
	public Page<IfsCfetsrmbDp> searchTotalDealZQFX(Map<String, Object> params, int isFinished);

	// 质押式回购
	public Page<IfsCfetsrmbCr> searchTotalDealZYSHG(Map<String, Object> params, int isFinished);

	// 买断式回购
	public Page<IfsCfetsrmbOr> searchTotalDealMDSHG(Map<String, Object> params, int isFinished);

	// 利率互换
	public Page<IfsCfetsrmbIrs> searchTotalDealLLHH(Map<String, Object> params, int isFinished);

	// 基金申购和复核
	public Page<CFtAfp> searchTotalDealJJSGANDFH(Map<String, Object> params, int isFinished);

	// 基金申购确认
	public Page<CFtAfpConf> searchTotalDealJJSGQR(Map<String, Object> params, int isFinished);

	// 基金赎回
	public Page<CFtRdp> searchTotalDealJJSH(Map<String, Object> params, int isFinished);

	// 基金赎回确认
	public Page<CFtRdpConf> searchTotalDealJJSHQR(Map<String, Object> params, int isFinished);

	// 基金现金分红
	public Page<CFtRedd> searchTotalDealJJXJFH(Map<String, Object> params, int isFinished);

	// 基金红利再投
	public Page<CFtRein> searchTotalDealJJHLZT(Map<String, Object> params, int isFinished);

	// 货币掉期
	public Page<IfsCfetsfxCswap> searchTotalDealHBDQ(Map<String, Object> params, int isFinished);

	// 人民币期权
	public Page<IfsCfetsfxOption> searchTotalDealRMBQQ(Map<String, Object> params, int isFinished);

	// 外币拆借
	public Page<IfsCfetsfxLend> searchTotalDealWBCQ(Map<String, Object> params, int isFinished);

	// 外币头寸调拨
	public Page<IfsCfetsfxAllot> searchTotalDealWBTCTB(Map<String, Object> params, int isFinished);

	// 外汇对即期
	public Page<IfsCfetsfxOnspot> searchTotalDealWBDJQ(Map<String, Object> params, int isFinished);

	// 黄金即远掉
	public Page<IfsCfetsmetalGold> searchTotalDealHJ(Map<String, Object> params, int isFinished);

	//债券借贷事前审批
	public Page<IfsApprovermbSl> searchTotalDealZQJDSQ(Map<String, Object> params, int isFinished);

}
