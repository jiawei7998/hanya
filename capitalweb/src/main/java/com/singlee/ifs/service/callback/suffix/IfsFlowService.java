package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.hrbreport.service.ReportService;
import com.singlee.ifs.mapper.IfsCfetsfxSwapMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxSwap;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/***
 * 外汇掉期回调函数
 * 
 * @author singlee4
 * 
 */
@Service(value = "IfsFlowService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsFlowService extends IfsApproveServiceBase implements ReportService {
	@Autowired
	private IFxdServer fxdServer;
	@Resource
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	UserService userService;
	@Autowired
	ISlGentServer slGentServer;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsfxSwap ifscfetsfxswap = new IfsCfetsfxSwap();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxSwapMapper.updateRmsSwapByID(serial_no, status, date);
		ifscfetsfxswap = ifsCfetsfxSwapMapper.getrmswap(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifscfetsfxswap) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifscfetsfxswap.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifscfetsfxswap.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifscfetsfxswap.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("外汇掉期");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("rmbswap");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifscfetsfxswap.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifscfetsfxswap.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifscfetsfxswap.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_SWAP");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_SWAP where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxSwapMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					LimitOccupy(serial_no, TradeConstants.ProductCode.RMBSWAP);
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
			
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {

			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			String callType = sysList.get(0).getP_type().trim();
			// 调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifscfetsfxswap.getBr());
				SlFxSwapBean slFxSwapBean = getEntry(ifscfetsfxswap, branchDate);
				// 验证
				/*
				 * SlOutBean verRet = fxdServer.verifiedfxSwap(slFxSwapBean); if
				 * (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
				 * JY.raise("验证未通过......"+verRet.getRetMsg()); }
				 */
				// 插入
				SlOutBean result = fxdServer.fxSwap(slFxSwapBean);

				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("审批未通过,外汇掉期保存OPICS失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifscfetsfxswap.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxSwapMapper.updateStatcodeByTicketId(map);
				}

			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程
				Map map1 = BeanUtils.describe(ifscfetsfxswap);
				SlOutBean result = fxdServer.fxSwapProc(map1);
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，调用存储过程失败......");
				}

			} else {
				JY.raise("系统参数未配置,未进行交易导入处理......");
			}

		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsfxSwap IfsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", IfsCfetsfxSwap.getCustNo());
		remap.put("custType", IfsCfetsfxSwap.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", IfsCfetsfxSwap.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", IfsCfetsfxSwap.getInstId());
		try {
			if("P".equals(IfsCfetsfxSwap.getDirection())) {
				BigDecimal amt = calcuAmt(new BigDecimal(IfsCfetsfxSwap.getFwdPrice()), IfsCfetsfxSwap.getWeight());
				remap.put("amt", ccyChange(amt, IfsCfetsfxSwap.getOpicsccy(),hrbCreditCustQuota.getCurrency()));
			}else {
				BigDecimal amt = calcuAmt(new BigDecimal(IfsCfetsfxSwap.getFwdReversePrice()), IfsCfetsfxSwap.getWeight());
				remap.put("amt", ccyChange(amt, IfsCfetsfxSwap.getOpicsctrccy(),hrbCreditCustQuota.getCurrency()));
			}
		} catch (Exception e) {
			JY.raise("货币汇率转化失败！");
		}
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", IfsCfetsfxSwap.getForDate());
		remap.put("mdate", IfsCfetsfxSwap.getFwdValuedate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	private SlFxSwapBean getEntry(IfsCfetsfxSwap ifscfetsfxswap, Date branchDate) {
		// String br=userService.getBrByUser(ifscfetsfxswap.getSponsor());
		SlFxSwapBean slFxSwapBean = new SlFxSwapBean(ifscfetsfxswap.getBr(), SlDealModule.FXD.SERVER,
				SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
		/************* 20180711新增：设置交易日期、近端起息日、远端起息日 ***************************/
		slFxSwapBean.setDealDate(DateUtil.parse(ifscfetsfxswap.getForDate()));
		slFxSwapBean.setValueDate(DateUtil.parse(ifscfetsfxswap.getNearValuedate()));
		slFxSwapBean.setFarValueDate(DateUtil.parse(ifscfetsfxswap.getFwdValuedate()));
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		/************* 20180711新增：设置交易对手 ***************************/
		contraPatryInstitutionInfo.setTradeId(ifscfetsfxswap.getCounterpartyDealer());
		// 设置对方机构
		// contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxswap.getCounterpartyInstId());
		// 设置对方机构id
		contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxswap.getCounterpartyInstId());
		slFxSwapBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);

		/************* 20180711新增：设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifscfetsfxswap.getDealer());// 交易员id
		userMap.put("branchId", ifscfetsfxswap.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
//		SlGentBean gentBean = new SlGentBean();
//		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
//		gentBean.setTablevalue(ifscfetsfxswap.getCost());
//		gentBean.setBr(ifscfetsfxswap.getBr());
//		// gentBean=slGentServer.getSlGent(gentBean);
//		String trad = (gentBean != null)
//				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
//				: user.getOpicsTrad();
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FXD.IOPER);
		institutionInfo.setTradeId(trad);
		slFxSwapBean.getInthBean().setIoper(trad);
		slFxSwapBean.setInstitutionInfo(institutionInfo);
		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		currencyInfo.setCcy(ifscfetsfxswap.getOpicsccy());// CFETS下行会报错
		currencyInfo.setContraCcy(ifscfetsfxswap.getOpicsctrccy());// cfets下行会报错
//		 currencyInfo.setCcy(ifscfetsfxswap.getSellCurreny());
//		 currencyInfo.setContraCcy(ifscfetsfxswap.getBuyCurreny());
//		 if("P".equals(ifscfetsfxswap.getDirection())){
//			 currencyInfo.setCcy(ifscfetsfxswap.getBuyCurreny());
//			 currencyInfo.setContraCcy(ifscfetsfxswap.getSellCurreny());
//		 }
		Double amt = Double.valueOf(ifscfetsfxswap.getNearPrice());
		currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
		Double contraAmt = Double.valueOf(ifscfetsfxswap.getFwdReversePrice());
		currencyInfo.setContraAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(contraAmt, 2)));
		Double points = Double.valueOf(ifscfetsfxswap.getNearSpread());
		currencyInfo.setPoints(BigDecimal.valueOf(points));

		Double rate = Double.valueOf(ifscfetsfxswap.getNearRate());
		currencyInfo.setRate(BigDecimal.valueOf(rate));
		// opics基础汇率信息
		currencyInfo.setSpotRate(new BigDecimal(StringUtils.defaultString(ifscfetsfxswap.getPrice(), "0")));
		slFxSwapBean.setCurrencyInfo(currencyInfo);
		FxCurrencyBean farCurrencyInfo = new FxCurrencyBean();
		Double points2 = Double.valueOf(ifscfetsfxswap.getSpread());
		farCurrencyInfo.setPoints(BigDecimal.valueOf(points2));
		Double rate2 = Double.valueOf(ifscfetsfxswap.getFwdRate());
		farCurrencyInfo.setRate(BigDecimal.valueOf(rate2));
		slFxSwapBean.setFarCurrencyInfo(farCurrencyInfo);
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifscfetsfxswap.getCost());
		externalBean.setProdcode(ifscfetsfxswap.getProduct());
		externalBean.setProdtype(ifscfetsfxswap.getProdType());
		externalBean.setPort(ifscfetsfxswap.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifscfetsfxswap.getTicketId());
		externalBean.setDealtext(ifscfetsfxswap.getContractId());
		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
		externalBean.setSiind(SlDealModule.FXD.SIIND);
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
		externalBean.setVerind(SlDealModule.FXD.VERIND);
		externalBean.setCtrsacct(ifscfetsfxswap.getCtrsacct());
		slFxSwapBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFxSwapBean.getInthBean();
		inthBean.setFedealno(ifscfetsfxswap.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFxSwapBean.setInthBean(inthBean);
		String ps = ifscfetsfxswap.getDirection();
		slFxSwapBean.setPs(StringUtils.equals(ps, "P") ? PsEnum.P : PsEnum.S);
		slFxSwapBean.setExecId(ifscfetsfxswap.getTicketId());
		slFxSwapBean.setValueDate(DateUtil.parse(ifscfetsfxswap.getNearValuedate()));
		slFxSwapBean.setFarValueDate(DateUtil.parse(ifscfetsfxswap.getFwdValuedate()));
		return slFxSwapBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsfxSwap> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsfxSwapMapper.getFxSwapMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsfxSwapMapper.getFxSwapMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsfxSwapMapper.getFxSwapMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyPairList = taDictVoMapper.getTadictTree("CurrencyPair").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_desc));
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> swaptradingList = taDictVoMapper.getTadictTree("Swaptrading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradeSourceList = taDictVoMapper.getTadictTree("TradeSource").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> transCodeList = taDictVoMapper.getTadictTree("transCode").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> purposeTypeList = taDictVoMapper.getTadictTree("purposeType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsfxSwap ifsCfetsfxSwap: list) {
			ifsCfetsfxSwap.setCurrencyPair(currencyPairList.get(ifsCfetsfxSwap.getCurrencyPair()));
			ifsCfetsfxSwap.setOpicsccy(currencyList.get(ifsCfetsfxSwap.getOpicsccy()));
			ifsCfetsfxSwap.setDirection(swaptradingList.get(ifsCfetsfxSwap.getDirection()));
			ifsCfetsfxSwap.setDealSource(tradeSourceList.get(ifsCfetsfxSwap.getDealSource()));
			ifsCfetsfxSwap.setTransKind(transCodeList.get(ifsCfetsfxSwap.getTransKind()));
			ifsCfetsfxSwap.setPurposeCode(purposeTypeList.get(ifsCfetsfxSwap.getPurposeCode()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
