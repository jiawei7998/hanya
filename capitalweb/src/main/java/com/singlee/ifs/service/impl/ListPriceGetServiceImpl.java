package com.singlee.ifs.service.impl;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.common.util.SingleeSFTPClient;
import com.singlee.financial.esb.hbcb.bean.GJ0011.*;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.marketdata.bean.FxRateTypes;
import com.singlee.financial.marketdata.bean.FxRevaluationRates;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.ifs.job.SwiftJobManager;
import com.singlee.ifs.model.FileNameList;
import com.singlee.ifs.service.ListPriceGetService;
import com.singlee.ifs.service.ListPriceJobService;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

@Service
public class ListPriceGetServiceImpl implements ListPriceGetService {
    private static Logger logManager = LoggerFactory.getLogger(SwiftJobManager.class);
    @Autowired
    ListPriceJobService listPriceJobService;
    @Autowired
    MarketDataTransServer marketDataTransServer;

    @Override
    public boolean getPrice() throws Exception {
        Date postdate = new Date();
        EsbPacket<SoapReqBody> requestPakcet = getgJ0011Req();
        System.out.println("========================GJ0011请求报文==============");
        System.out.println(XmlUtils.toXml(requestPakcet));
        System.out.println("==================================================================");
        HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
        httpHeadAttrConfig.setCharset("GB2312");
        httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//          httpHeadAttrConfig.setCharset("UTF-8");
        httpHeadAttrConfig.setConnTimeOut(3000);
        httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
        httpHeadAttrConfig.setContentLength(true);
        httpHeadAttrConfig.setContentMd5(true);
        String host = SystemProperties.esbIp;
        String port = SystemProperties.esbTransPort;
        String server = "S010003010GJ0011";
        String strURL = "http://" + host + ":" + port + "/" + server;
        httpHeadAttrConfig.setUrl(strURL);
        // 初始化输出对象
        EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
                "soapenv:Envelope");
        responsePakcet.setPackge_type("www.cqrcb.com.cn");
        responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
        responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

        // 解析返回对象
        SoapResBody soapResBody = responsePakcet.getBody();
        //获取牌价文件在ftp服务器上的绝对路径                                                     File1Name
        String fileName = soapResBody.getGJ0011Res().getResponseBody().getFile1Name();
        if (fileName == null) {
            return false;
        }
        String path = SystemProperties.esbRoot;
//          String Localpath = MessageFormat.format("{0}{1}{2}", SystemProperties.esbRoot,DateUtil.format(postdate, "yyyyMMdd"), "/in/");
        String localFileName = fileName + DateUtil.format(postdate, "yyyyMMdd");
        File filedir = new File(path);
        if (!filedir.exists()) {
            filedir.mkdirs();
        }
        logManager.info("=======开始取回文件:fileName=" + fileName + "==========");
        // 初始化FTP
        SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.esbIp, SystemProperties.esbFtpPort, SystemProperties.esbUser, SystemProperties.esbPasswd);
        // 开始连接
        sftp.connect();
        //下载文件
        boolean flag = sftp.downloadFile(SystemProperties.acupCatalog + "file_in/" + DateUtil.format(postdate, "yyyyMMdd") + "/", fileName, path, localFileName);
        if (flag) {
//              logManager.info("=======取文件成功:fileName="+fileName+"==========");
            //文件解析 明细
            List<String> liseDeali = new ArrayList<String>();
            logManager.info("=============开始解析文件===============");
            File file = new File(path, fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GB2312"));// 构造一个BufferedReader类来读取文件
            String s = null;
            while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
                logManager.info("====行内容" + s + "=====");
                liseDeali.add(s);
            }
            br.close();
            List<String> fileNameLists = new ArrayList<String>();
            //从第二行开始拿到所有的币种和消息
            for (int i = 1; i < liseDeali.size(); i++) {
                String line = liseDeali.get(i);
                //通过，分割拿到所有的价格
                String[] string = line.split("\\,");
                for (String str : string) {
                    fileNameLists.add(str);
                    logManager.info("==================解析完成 开始插入TD_CUST_CREDIT_DEAL表==============================");
                    //把解析的文件每一排都插入前置表
                }
                insertPrice(liseDeali, fileNameLists);
            }
            //根据时间拿到所有牌价交易
            ArrayList<String> filedlist = new ArrayList<String>() {
                {
                    add("base");
                    add("foreignMarketPrice");
                    add("foreignToUSDPrice");
                    add("purchasePrice");
                    add("cashSellingRate");
                    add("buyingRate");
                    add("sellingRate");
                    add("centralParity");
                    add("systemFlatBuying");
                    add("systemFlatSelling");
                    add("finalPrice");
                }
            };
            Map<String, Object> rmap = new HashMap<>();
            rmap.put("fileTime", DateUtil.format(postdate, "yyyyMMdd"));

            List<FileNameList> list = listPriceJobService.getlist(rmap);
            for (FileNameList price : list) {
                for (String filed : filedlist) {
                    Method method = price.getClass().getDeclaredMethod("get" + filed);
                    String basePirce = (String) method.invoke(price);
                    Map<String, FxRevaluationRates> frRateMap = new HashMap<>();
                    FxRevaluationRates base = new FxRevaluationRates();
                    //机构
                    base.setBr("01");
                    //币种
                    base.setCcy(price.getCurrency());
                    //即期汇率
                    base.setSpotRate(new BigDecimal(basePirce));
                    //价格日期
                    base.setSpotDate(postdate);
                    //基准价
                    frRateMap.put(price.getCurrency() + StringUtils.trim("SPOT"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("1D"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("1W"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("1M"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("3M"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("6M"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("9M"), base);
                    frRateMap.put(price.getCurrency() + StringUtils.trim("1Y"), base);
                    logManager.info("==================开始插入调用saveFxRateData方法，把数据和插入irev和inth表==============================");
                    marketDataTransServer.saveFxRateData(frRateMap, FxRateTypes.Rate);
                }
            }
        }
        return flag;
    }


    /**
     * 组装GJ0011请求对象
     *
     * @param request
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static EsbPacket<SoapReqBody> getgJ0011Req()
            throws InstantiationException, IllegalAccessException {
        // 初始化请求对象
        EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
                "soapenv:Envelope");
        request.setPackge_type("www.cqrcb.com.cn");
        request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
        // 获取当前请求报文体内容
        SoapReqBody soapReqBody = request.getBody();
        // 申明报文对象
        GJ0011Req GJ0011ReqBody = new GJ0011Req();
        RequestBody requestBody = new RequestBody();
        RequestHeader requestHeader = new RequestHeader();
        // 设置body中的请求头信息
        requestHeader.setBrchNo("00100");// 机构号
        // 设置报文中请求头对象
        GJ0011ReqBody.setRequestHeader(requestHeader);
        // body里面的内容
        requestBody.setMark("guojie");
        // 设置body
        GJ0011ReqBody.setRequestBody(requestBody);
        // 设置报文
        soapReqBody.setGJ0011Req(GJ0011ReqBody);
        return request;
    }


    /**
     * 组装GJ0011返回对象
     *
     * @param
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    @SuppressWarnings("unused")
    private EsbPacket<SoapResBody> getGJ0011Res()
            throws InstantiationException, IllegalAccessException {
        // 初始化输出对象
        EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
                "soapenv:Envelope");
        response.setPackge_type("www.cqrcb.com.cn");
        response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
        // 获取响应报文体
        SoapResBody soapResBody = response.getBody();
        // 申明报文对象
        GJ0011Res gJ0011ResBody = new GJ0011Res();
        ResponseBody responseBody = new ResponseBody();
        ResponseHeader responseHeader = new ResponseHeader();
        gJ0011ResBody.setResponseHeader(responseHeader);
        Fault fault = new Fault();
        fault.setFaultCode("002001000000");
        fault.setFaultString("成功");
        Detail detail = new Detail();
        detail.setTxnStat("SUCCESS");
        fault.setDetail(detail);
        gJ0011ResBody.setFault(fault);
        // 设置body
        gJ0011ResBody.setResponseBody(responseBody);
        // 设置报文
        soapResBody.setGJ0011Res(gJ0011ResBody);
        return response;
    }


    //插入前置表
    public void insertPrice(List<String> liseDeali, List<String> fileNameLists) {

        Map<String, Object> remp = new HashMap<>();
        remp.put("fileTime", liseDeali.get(0));
        String fileTime = (String) remp.get("fileTime");
        String fileDate = fileTime.substring(0, 10);
        remp.put("fileDate", fileDate);
        remp.put("currency", fileNameLists.get(0));
        remp.put("base", fileNameLists.get(1));
        remp.put("foreignMarketPrice", fileNameLists.get(2));
        remp.put("foreignToUSDPrice", fileNameLists.get(3));
        remp.put("foreignToUSDType", fileNameLists.get(4));
        remp.put("purchasePrice", fileNameLists.get(5));
        remp.put("cashSellingRate", fileNameLists.get(6));
        remp.put("buyingRate", fileNameLists.get(7));
        remp.put("sellingRate", fileNameLists.get(8));
        remp.put("centralParity", fileNameLists.get(9));
        remp.put("systemFlatBuying", fileNameLists.get(10));
        remp.put("systemFlatSelling", fileNameLists.get(11));
        remp.put("finalPrice", fileNameLists.get(12));
        List<FileNameList> list = listPriceJobService.getlist(remp);
//  list为空才插入
        if (list == null || list.size() == 0) {
            listPriceJobService.insert(remp);
        }
    }
}

