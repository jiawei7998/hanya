package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.mapper.IfsWdFundAllMapper;
import com.singlee.ifs.model.ChinaMutualFundDescription;
import com.singlee.ifs.service.IfsWdFundAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsWdFundAllServiceImpl implements IfsWdFundAllService{

    @Autowired
    IfsWdFundAllMapper ifsWdFundAllMapper;
    
    @Override
    public Page<ChinaMutualFundDescription> getWdFundAllList(Map<String, Object> map) {
        List<ChinaMutualFundDescription> list=ifsWdFundAllMapper.getFundAllList(map);
        
        int pageNumber=(int)map.get("pageNumber");
        int pageSize=(int)map.get("pageSize");
        
        Page<ChinaMutualFundDescription> page=HrbReportUtils.producePage(list, pageNumber, pageSize);
        
        return page;
    }




}
