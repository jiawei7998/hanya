package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevBred;

import java.util.Map;
/**
 * 
 * @author jiangzx
 * @date: 2018年7月24日 下午3:18:53 
 * @version
 * @since JDK 1.6
 */
public interface IfsRevBredService {

	// 新增
	void insert(IfsRevBred record);
	
	// 删除
	void deleteByPrimaryKey(String ticketId);
	
	// 查找
	IfsRevBred selectByPrimaryKey(Map<String, Object> map);
	
	// 更新
	void updateByPrimaryKey(IfsRevBred record);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author lij
	 * @date 2018-08-10
	 */
	public Page<IfsRevBred> getRevBredPage (Map<String, Object> params, int isFinished);

	Page<IfsRevBred> searchPageRevBred(Map<String, Object> map);
}
