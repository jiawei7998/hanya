package com.singlee.ifs.service.impl;

import com.singlee.ifs.mapper.IfsOpsCzsyMapper;
import com.singlee.ifs.model.IfsOpsCzsyBean;
import com.singlee.ifs.service.IfsOpsCzsyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 处置收益报表（买卖价差）,需要从"营业税"报表中取数。
 * @author copysun
 */
@Service
public class IfsOpsCzsyServiceImpl implements IfsOpsCzsyService {

	@Autowired
	IfsOpsCzsyMapper ifsOpsCzsyMapper;

	@Override
	public List<IfsOpsCzsyBean> getData(Map<String, Object> map) {
		ifsOpsCzsyMapper.generateData(map);
		return ifsOpsCzsyMapper.getData(map);
	}

}