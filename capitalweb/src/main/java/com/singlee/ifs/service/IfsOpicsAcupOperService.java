package com.singlee.ifs.service;

import com.singlee.financial.bean.SlOutBean;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;


public interface IfsOpicsAcupOperService {

	/**
	 * 发送总账
	 * @param parameters
	 * @return
	 */
	public SlOutBean acupSend(Map<String, Object> parameters) throws RemoteConnectFailureException, Exception;

	/**
	 * 取回总账
	 * @param parameters
	 * @return
	 */
	public SlOutBean acupGet(Map<String, Object> parameters) throws RemoteConnectFailureException, Exception;

}