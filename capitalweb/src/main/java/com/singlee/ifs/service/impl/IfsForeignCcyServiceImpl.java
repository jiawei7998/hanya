package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.esb.dlcb.IQueryForeignCcyServer;
import com.singlee.ifs.mapper.IfsForeignCcyMapper;
import com.singlee.ifs.model.IfsForeignccyDish;
import com.singlee.ifs.service.IfsForeignCcyService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 总行外币平盘数据查询接口实现类 7445
 */
@Service
public class IfsForeignCcyServiceImpl implements IfsForeignCcyService {
	@Autowired
	IQueryForeignCcyServer queryForeignCcyServer;

	@Autowired
	IfsForeignCcyMapper foreignCcyMapper;

	private static Logger logger = LoggerFactory.getLogger(IfsForeignCcyServiceImpl.class); // 日志记录

	@Override
	public SlOutBean queryForeignCcy(Map<String, Object> map) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 1,调用Opics项目接口,查询总行外币平盘数据,返回响应码和下载文件的绝对路径
		 */
		outBean = queryForeignCcyServer.queryForeignCcy(map);
		String queryDate = (String) map.get("queryDate");
		String retCode = outBean.getRetCode(); // 响应码
		String retMasg = outBean.getRetMsg(); // 响应码描述
		logger.info("查询接口返回结果" + retCode);
		/**
		 * 2,根据响应码判断,是否查询成功,并解析文件,保存到数据库
		 */
		if (null != retCode && "AAAAAAAAAA".equals(retCode)) {
			try {
				foreignCcyUpdate(retMasg, queryDate); // retMasg就是文件路径

				outBean.setRetMsg("singlee: " + "查询成功");
				logger.info("singlee: " + "保存到数据库成功" + retCode);
				return outBean;
			} catch (Exception e) {
				outBean.setRetMsg("singlee: " + "查询失败");
				logger.info("singlee: " + "保存到数据库失败" + retCode);
				e.printStackTrace();
			}
		}
		return outBean;
	}

	/**
	 * 解析文件并更新
	 */
	public void foreignCcyUpdate(String filePath, String queryDate) throws Exception {
		// 读取文件解析更新数据库
		List<IfsForeignccyDish> list = this.readFile(filePath, queryDate);
		if (!(list == null || list.size() < 1)) {
			for (IfsForeignccyDish row : list) {
				// 判断是否可以存储到本地数据库
				Integer flag = foreignCcyMapper.search(row);
				if (flag == 0) {
					foreignCcyMapper.insert(row);
				} else {
					continue;
				}

			}
		}
	}

	public List<IfsForeignccyDish> readFile(String filePath, String queryDate) throws Exception {
		// 读取文件解析
		File file = new File(filePath);
		logger.info("singlee: " + "文件是否存在!" + file.exists() + "filepath: " + filePath);
		List<IfsForeignccyDish> result = new ArrayList<IfsForeignccyDish>();
		if (file.exists()) {
			BufferedReader br = null;
			char splitChar = (char) 0x02;
			try {
				br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
				String line = "";
				// 循环读取每行数据
				while ((line = br.readLine()) != null) {
					if ("".equals(line)) {
						continue;
					}
					IfsForeignccyDish foreignccy = new IfsForeignccyDish();
					String[] row = line.split("\\" + splitChar + "");

					if (row.length > 0) {
						foreignccy.setForeignccyId(row[1] + row[0] + row[4] + row[8]); // 唯一序号5.29改
						// 业务类型(1-代客结售汇 2-代客外汇买卖 3-货币互换代客结售汇 4-货币互换代客外汇买卖
						// 5-自身结售汇税金结转 6-自身结售汇未结转利润分配)
						foreignccy.setBusType(row[0]);
						foreignccy.setBankTellerNo(row[1]); // 柜员流水
						foreignccy.setDealStatus(row[2]); // 交易状态(0-正常 1-冲销)
						foreignccy.setOrgNo(row[3]); // 机构号
						foreignccy.setDealDate(row[4]); // 成交日期
						foreignccy.setvDate(row[5]); // 交割日期
						foreignccy.setCurrencyPair(row[6]); // 货币对
						foreignccy.setDirection(row[7]); // 买卖方向
						foreignccy.setCcy1Amt(row[8]); // 币种1金额
						foreignccy.setCcy2Amt(row[9]); // 币种2金额
						foreignccy.setSetOrgNo(" "); // 创建机构号
						foreignccy.setSetPersonNo(" "); // 创建柜员号
						foreignccy.setNote(filePath); // 备注+文件路径
						foreignccy.setSyncStatus("0");// 同步状态为0-未同步
						// 货币对不为空,可以进行币种分割
						if (null != row[6] && row[6].length() > 0) {
							String[] ccys = row[6].split("/");
							foreignccy.setCcy1(ccys[0]);// 第一币种
							foreignccy.setCcy2(ccys[1]);// 第二币种
						}
					}
					result.add(foreignccy);

				}
			} catch (FileNotFoundException e) {
				logger.error("singlee: " + "解析文件异常 !" + e);
			} catch (IOException e) {
				logger.error("singlee: " + "IOException异常 !" + e);
			} finally {
				br.close();
			}
			return result;
		} else {
			logger.info("singlee: " + "文件是否存在!" + file.exists());
			return result;
		}

	}

	@Override
	public Page<IfsForeignccyDish> searchPageDish(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsForeignccyDish> result = foreignCcyMapper.searchPageDish(map, rb);
		return result;
	}

}
