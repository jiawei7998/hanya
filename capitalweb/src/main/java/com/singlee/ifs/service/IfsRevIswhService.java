package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIswh;

import java.util.Map;

public interface IfsRevIswhService {
	
	// 新增
	public void insert(IfsRevIswh entity);

	// 修改
	void updateById(IfsRevIswh entity);

	// 删除
	void deleteById(String id);
	
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author rongliu
	 * @date 2018-08-17
	 */
	Page<IfsRevIswh> getRevIswhPage(Map<String, Object> params,int isFinished);

}
