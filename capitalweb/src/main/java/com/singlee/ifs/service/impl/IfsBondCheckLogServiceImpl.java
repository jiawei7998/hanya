package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsBondCheckLogMapper;
import com.singlee.ifs.model.IfsBondCheckLog;
import com.singlee.ifs.service.IfsBondCheckLogService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
/**
 * 交易要素  实现类
 * @author singlee4
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsBondCheckLogServiceImpl implements IfsBondCheckLogService {
	@Autowired
	IfsBondCheckLogMapper ifsBondCheckLogMapper;
	
	
	@Override
	public void insert(IfsBondCheckLog entity) {
		//设置 录入时间
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		String id = DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setId(id);
		ifsBondCheckLogMapper.insert(entity);
	}

	@Override
	public Page<IfsBondCheckLog> searchPageCheckLog(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsBondCheckLog> result = ifsBondCheckLogMapper.searchPageCheckLog(map, rb);
		return result;
	}

}
