package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsWdSecService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsWdSecServiceImpl implements IfsWdSecService{
	@Autowired
	private IfsWdSecMapper ifsWdSecMapper;
	@Autowired
	private IfsWdOpicsSecmapMapper ifsWdOpicsSecmapMapper;
	@Autowired
	private IfsWdSeccustMapper ifsWdSeccustMapper;
	@Autowired
	IfsWindRateMapper ifsWindRateMapper;
	@Autowired
	IfsWindSeclMapper ifsWindSeclMapper;
	@Autowired
	IfsWindYcMapper ifsWindYcMapper;
	@Autowired
	IfsWindBondAddMapper ifsWindBondAddMapper;
	@Autowired
	IfsWindSecLevelMapper ifsWindSecLevelMapper;
	@Autowired
	IfsWindRiskMapper  ifsWindRiskMapper;
	/**
	 * 新万得 债券信息
	 */
	@Autowired
	IfsWindBondMapper ifsWindBondMapper;
	
	@Override
	public Page<IfsWdSec> getWdSecList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsWdSec> result = ifsWdSecMapper.getWdSecList(map,rb);
		return result;
	}

	@Override
	public Page<IfsWdOpicsSecmap> getWdOpicsSecmapList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsWdOpicsSecmap> result = ifsWdOpicsSecmapMapper.getWdOpicsSecmapList(map, rb);
		return result;
	}

	@Override
	public void insert(IfsWdOpicsSecmap entity) {
		String mapid = "MAP" + DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setMapid(mapid);
		ifsWdOpicsSecmapMapper.insert(entity);
	}

	@Override
	public void updateById(IfsWdOpicsSecmap entity) {
		ifsWdOpicsSecmapMapper.updateById(entity);
	}

	@Override
	public void delete(String mapid) {
		ifsWdOpicsSecmapMapper.delete(mapid);
	}

	@Override
	public Page<IfsWdSeccust> getWdSeccustList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsWdSeccust> result = ifsWdSeccustMapper.getWdSeccustList(map, rb);
		return result;
	}
	
	@Override
	public Page<IfsWindBondBean> getIfsWindBondList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsWindBondBean> result = ifsWindBondMapper.selectIfsWindBondList(map, rb);
		return result;
	}

    @Override
    public Page<IfsWindRateBean> getIfsWindRateList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindRateBean> result = ifsWindRateMapper.selectIfsWindRateList(map, rb);
        return result;
    }

    @Override
    public Page<IfsWindSeclBean> getIfsWindSeclList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindSeclBean> result = ifsWindSeclMapper.selectIfsWindSeclList(map, rb);
        return result;
    }

    @Override
    public Page<IfsWindYcBean> getIfsWindYcList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindYcBean> result = ifsWindYcMapper.selectIfsWindYcList(map, rb);
        return result;
    }

    @Override
    public Page<IfsWindBondAddBean> getIfsWindBondAddList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindBondAddBean> result = ifsWindBondAddMapper.selectIfsWindBondAddList(map, rb);
        return result;
       
    }

    @Override
    public Page<IfsWindSecLevelBean> getIfsWindSecLevelList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindSecLevelBean> result = ifsWindSecLevelMapper.selectIfsWindSecLevelList(map, rb);
        return result;
     
    }

    @Override
    public Page<IfsWindRiskBean> getIfsWindRiskList(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsWindRiskBean> result = ifsWindRiskMapper.selectIfsWindRiskList(map, rb);
        return result;
    }

}
