package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxLend;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsLendService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsLendService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Autowired
	private IMmServer mmServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	UserService userService;
	@Autowired
	ISlGentServer slGentServer;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxLendMapper.updatefxLendingByID(serial_no, status, date);
		IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsfxLend) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			try {
				if("S".equals(ifsCfetsfxLend.getDirection())){
					hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsfxLend.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsfxLend.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsfxLend.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("外币拆借");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("ccyLending");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifsCfetsfxLend.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsfxLend.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsfxLend.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_LEND");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_LEND where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxLendMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("S".equals(ifsCfetsfxLend.getDirection())) {
						LimitOccupy(serial_no, TradeConstants.ProductCode.CCYLENDING);
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifsCfetsfxLend.getBr());
				SlDepositsAndLoansBean mmBean = getEntry(ifsCfetsfxLend, branchDate);
				SlOutBean result = mmServer.mm(mmBean);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("审批未通过，插入opics失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsfxLend.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxLendMapper.updateStatcodeByTicketId(map);
				}
			} else {
				JY.raise("系统参数未配置,未进行交易导入处理......");
			}

		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}

	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsfxLend.getCustNo());
		remap.put("custType", ifsCfetsfxLend.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsfxLend.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsfxLend.getInstId());
		BigDecimal amt = calcuAmt(ifsCfetsfxLend.getAmount(), ifsCfetsfxLend.getWeight());
		remap.put("amt", ccyChange(amt,ifsCfetsfxLend.getCurrency(),hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsfxLend.getForDate());
		remap.put("mdate", ifsCfetsfxLend.getValueDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取交易类
	 * 
	 * @param ifsCfetsfxLend
	 * @param branchDate
	 * @return
	 */
	private SlDepositsAndLoansBean getEntry(IfsCfetsfxLend ifsCfetsfxLend, Date branchDate) {
		// String br=userService.getBrByUser(ifsCfetsfxLend.getSponsor());
		SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(ifsCfetsfxLend.getBr(), SlDealModule.DL.SERVER,
				SlDealModule.DL.TAG, SlDealModule.DL.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsfxLend.getCounterpartyInstId());
		mmBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsfxLend.getDealer());// 交易员id
		userMap.put("branchId", ifsCfetsfxLend.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
//		SlGentBean gentBean = new SlGentBean();
//		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
//		gentBean.setTablevalue(ifsCfetsfxLend.getCost());
//		gentBean.setBr(ifsCfetsfxLend.getBr());
//		// gentBean=slGentServer.getSlGent(gentBean);
//		String trad = (gentBean != null)
//				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
//				: user.getOpicsTrad();
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.DL.IOPER);
		institutionInfo.setTradeId(trad);
		mmBean.getInthBean().setIoper(trad);
		mmBean.setInstitutionInfo(institutionInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsfxLend.getCost());
		externalBean.setProdcode(ifsCfetsfxLend.getProduct());
		externalBean.setProdtype(ifsCfetsfxLend.getProdType());
		externalBean.setPort(ifsCfetsfxLend.getPort());
		externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setSiind(SlDealModule.DL.SIIND);
		externalBean.setSupconfind(SlDealModule.DL.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.DL.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.DL.SUPRECIND);
		externalBean.setVerind(SlDealModule.DL.VERIND);
		externalBean.setDealtext(ifsCfetsfxLend.getContractId());
		externalBean.setCustrefno(ifsCfetsfxLend.getTicketId());
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsfxLend.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsfxLend.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsfxLend.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsfxLend.getCtrsacct());
		mmBean.setExternalBean(externalBean);

		SlInthBean inthBean = mmBean.getInthBean();
		// fedealno
		inthBean.setFedealno(ifsCfetsfxLend.getTicketId());
		inthBean.setLstmntdate(branchDate);
		mmBean.setInthBean(inthBean);
		mmBean.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(ifsCfetsfxLend.getAmount().doubleValue(), 2)));
		mmBean.setValueDate(DateUtil.parse(ifsCfetsfxLend.getValueDate()));
		mmBean.setMaturityDate(DateUtil.parse(ifsCfetsfxLend.getMaturityDate()));
		mmBean.setDealDate(DateUtil.parse(ifsCfetsfxLend.getForDate()));
		mmBean.setTenor("99");
		mmBean.setIntdatedir("VF");
		mmBean.setIntdaterule("D");
		mmBean.setCommtypecode("S");
		mmBean.setCcy(ifsCfetsfxLend.getCurrency());
		mmBean.setRateCode(ifsCfetsfxLend.getRateCode());
		if (ifsCfetsfxLend.getBasis() != null) {
			mmBean.setBasis(ifsCfetsfxLend.getBasis());
			if (ifsCfetsfxLend.getBasis().endsWith("1")) {
				mmBean.setBasis("A360");
			}
			if (ifsCfetsfxLend.getBasis().endsWith("3")) {
				mmBean.setBasis("A365");
			}
		}
		mmBean.setIntrate(ifsCfetsfxLend.getRate() == null ? "" : ifsCfetsfxLend.getRate().toString());
		mmBean.setSpread(BigDecimal.valueOf(0));
		mmBean.setCommmeans(ifsCfetsfxLend.getCcysmeans());
		mmBean.setCommacct(ifsCfetsfxLend.getCcysacct());
		mmBean.setMatmeans(ifsCfetsfxLend.getCtrsmeans());
		mmBean.setMatacct(ifsCfetsfxLend.getCtrsacct());
		mmBean.setIntpaycycle("Q");
		mmBean.setIntcalcrule("Q");
		mmBean.setIntsmeans(ifsCfetsfxLend.getCtrsmeans());
		mmBean.setIntsacct(ifsCfetsfxLend.getCtrsacct());
		return mmBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsfxLend> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsfxLendMapper.getCcyLendingMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsfxLendMapper.getCcyLendingMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsfxLendMapper.getCcyLendingMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> lendingList = taDictVoMapper.getTadictTree("lending").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradeSourceList = taDictVoMapper.getTadictTree("TradeSource").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsfxLend ifsCfetsfxLend: list) {
			ifsCfetsfxLend.setCurrency(currencyList.get(ifsCfetsfxLend.getCurrency()));
			ifsCfetsfxLend.setDirection(lendingList.get(ifsCfetsfxLend.getDirection()));
			ifsCfetsfxLend.setDealSource(tradeSourceList.get(ifsCfetsfxLend.getDealSource()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
