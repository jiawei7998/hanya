package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlActyBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsActyMapper;
import com.singlee.ifs.model.IfsOpicsActy;
import com.singlee.ifs.service.IfsOpicsActyService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsActyServiceImpl implements IfsOpicsActyService {

	@Resource
	IfsOpicsActyMapper ifsOpicsActyMapper;

	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsActy entity) {
		// 设置最新维护时间
		entity.setLstmntdate(new Date());
		entity.setStatus("0");
		ifsOpicsActyMapper.insert(entity);
	}

	@Override
	public void updateById(IfsOpicsActy entity) {
		entity.setLstmntdate(new Date());
		ifsOpicsActyMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsActyMapper.deleteById(id);
	}

	// 分页查询
	@Override
	public Page<IfsOpicsActy> searchPageOpicsActy(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsActy> result = ifsOpicsActyMapper.searchPageOpicsActy(map, rb);
		return result;
	}

	// 查询所有
	@Override
	public Page<IfsOpicsActy> searchAllOpicsActy() {
		Page<IfsOpicsActy> result = ifsOpicsActyMapper.searchAllOpicsActy();
		return result;
	}

	@Override
	public List<IfsOpicsActy> actyList(Map<String, Object> map) {
		return ifsOpicsActyMapper.actyList(map);
	}

	@Override
	public IfsOpicsActy searchById(String id) {
		return ifsOpicsActyMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsActy entity) {
		ifsOpicsActyMapper.updateStatus(entity);
	}

	/**
	 * 批量校准：
	 * 
	 * 1、对选中某一行会计类型进行校准
	 * 
	 * 2、对所有会计类型进行进行校准
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] acctngtypes) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从OPICS库查询所有数据
			List<SlActyBean> listActyOpics = iStaticServer.synchroActy();

			/**************** 对选中的某一行进行校准 **********************/
			if ("1".equals(type)) { // 选中行校准
				for (int i = 0; i < acctngtypes.length; i++) {
					for (int j = 0; j < listActyOpics.size(); j++) {
						// 若OPICS库中有相同的会计类型，就将本地的更新成OPICS中的，其中中文描述以及备注保持不变
						if (listActyOpics.get(j).getAcctngType().trim().equals(acctngtypes[i].trim())) {
							IfsOpicsActy entity = new IfsOpicsActy();
							entity = ifsOpicsActyMapper.searchById(acctngtypes[i]);
							entity.setAcctngtype(listActyOpics.get(j).getAcctngType().trim());
							String acty = listActyOpics.get(j).getAcctDesc();
							if ("".equals(acty) || acty == null) {
								entity.setAcctdesc("");
							} else {
								entity.setAcctdesc(acty.trim());
							}
							Date lstmntdte = listActyOpics.get(j).getLstmntdate();
							// 若OPICS系统此条会计类型没有设置最后维护时间，就设置最新时间
							if (lstmntdte == null) {
								entity.setLstmntdate(new Date());
							} else {
								entity.setLstmntdate(lstmntdte);
							}
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1"); // 设置为已同步
							ifsOpicsActyMapper.updateById(entity);
							break;
						}
						// 若OPICS库中无数据，则将状态改为'未同步'，其余不变
						if (j == listActyOpics.size() - 1) {
							ifsOpicsActyMapper.updateStatus0ById(acctngtypes[i]);
						}
					}
				}

				/**************** 对本地库所有会计类型进行校准 **********************/
			} else { // 全部校准
				// 从本地库查询所有的数据
				List<IfsOpicsActy> listActyLocal = ifsOpicsActyMapper.searchAllOpicsActy();
				/** 1、以OPICS库为主，更新或插入到本地库 */
				for (int i = 0; i < listActyOpics.size(); i++) {
					// 若全部不同，则插入到本地库
					if (listActyLocal.size() == 0) {
						IfsOpicsActy entity = new IfsOpicsActy();
						entity.setAcctngtype(listActyOpics.get(i).getAcctngType().trim());
						String acty = listActyOpics.get(i).getAcctDesc();
						if ("".equals(acty) || acty == null) {
							entity.setAcctdesc("");
						} else {
							entity.setAcctdesc(acty.trim());
						}
						Date lstmntdte = listActyOpics.get(i).getLstmntdate();
						// 若OPICS系统此条会计类型没有设置最后维护时间，就设置最新时间
						if (lstmntdte == null) {
							entity.setLstmntdate(new Date());
						} else {
							entity.setLstmntdate(lstmntdte);
						}
						entity.setOperator(SlSessionHelper.getUserId());
						entity.setStatus("1"); // 设置为已同步
						ifsOpicsActyMapper.insert(entity);
					}

					for (int j = 0; j < listActyLocal.size(); j++) {
						IfsOpicsActy entity = new IfsOpicsActy();
						//将本地数据存到实体类中，2019-7-9处理批量更新中文名称会被覆盖问题
						//entity = ifsOpicsActyMapper.searchById(listActyOpics.get(i).getAcctngType().trim());
						entity.setAcctngtype(listActyOpics.get(i).getAcctngType().trim()); //会计类型
						String acty = listActyOpics.get(i).getAcctDesc();  //会计类型英文描述
						if ("".equals(acty) || acty == null) {
							entity.setAcctdesc("");
						} else {
							entity.setAcctdesc(acty.trim());
						}
						Date lstmntdte = listActyOpics.get(i).getLstmntdate();  //最后维护时间
						// 若OPICS系统此条会计类型没有设置最后维护时间，就设置最新时间
						if (lstmntdte == null) {
							entity.setLstmntdate(new Date());
						} else {
							entity.setLstmntdate(lstmntdte);
						}
						entity.setOperator(SlSessionHelper.getUserId()); //操作人
						entity.setType(listActyLocal.get(j).getType()); // 会计归属类型
						entity.setStatus("1"); // 设置为已同步
						// 与OPICS库中的会计类型对比，相同的内容不更新，只更新不同的内容
						if (listActyOpics.get(i).getAcctngType().trim().equals(listActyLocal.get(j).getAcctngtype().trim())) {
							entity.setAcctdesccn(listActyLocal.get(j).getAcctdesccn());//会计类型中文描述
							entity.setType(listActyLocal.get(j).getType());//归属类型：归属客户、归属债券、归属以上两者
							ifsOpicsActyMapper.updateById(entity);
							break;
						}
						// 若全部不同，则插入本地库
						if (j == listActyLocal.size() - 1) {
							ifsOpicsActyMapper.insert(entity);
						}
					}
				}
				/** 2、若本地库中的数据多于OPICS库，则将本地库中含有的但OPICS库中没有的数据的状态改为'未同步' */
				// 遍历完OPICS库之后，从本地库【再次】查询所有数据，遍历本地库
				List<IfsOpicsActy> listActyLocal2 = ifsOpicsActyMapper.searchAllOpicsActy();
				if (listActyLocal2.size() > listActyOpics.size()) {
					for (int i = 0; i < listActyLocal2.size(); i++) {
						for (int j = 0; j < listActyOpics.size(); j++) {
							if (listActyOpics.get(j).getAcctngType().trim().equals(listActyLocal2.get(i).getAcctngtype())) {
								break;
							}
							// 若OPICS库中没有，则将状态改为'未同步'
							if (j == listActyOpics.size() - 1) {
								IfsOpicsActy acty = new IfsOpicsActy();
								acty.setAcctngtype(listActyLocal2.get(i).getAcctngtype());
								acty.setAcctdesc(listActyLocal2.get(i).getAcctdesc());
								acty.setAcctdesccn(listActyLocal2.get(i).getAcctdesccn());
								acty.setRemark(listActyLocal2.get(i).getRemark());
								acty.setLstmntdate(new Date());
								acty.setOperator(SlSessionHelper.getUserId());
								acty.setStatus("0"); // 设置为未同步
								ifsOpicsActyMapper.updateById(acty);
							}
						}
					}
				}
			}
		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}
}