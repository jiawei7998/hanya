package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlIotdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IOtcServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCfetsfxOptionMapper;
import com.singlee.ifs.mapper.IfsRevIotdMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsRevIotd;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 期权类冲销-回调方法
 * 
 * @author
 * 
 */
@Service(value = "IfsIoptRevService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsIoptRevService extends IfsApproveServiceBase {

	@Autowired
	IfsRevIotdMapper ifsRevIotdMapper;// 期权类冲销mapper
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IOtcServer otcServer;// 直通opics期权接口

	@Autowired
	IfsCfetsfxOptionMapper optionMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		/** 根据id 查询实体类 */
		IfsRevIotd ifsRevIotd = new IfsRevIotd();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevIotd = ifsRevIotdMapper.searchById(map);

		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevIotdMapper.updateIotdStatusByID(serial_no, status, date);

		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				if (ifsRevIotd.equals(null)) {
					return;
				}
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}

				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				// 调用java代码
				String callType = sysList.get(0).getP_type().trim();
				// 3.3调用java代码
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					SlIotdBean slIotdBean = new SlIotdBean(SlDealModule.BASE.BR, SlDealModule.FXD.SERVER,
							SlDealModule.FXD.TAG_R, SlDealModule.FXD.DETAIL);

					SlInthBean inthBean = new SlInthBean();
					inthBean.setBr(SlDealModule.BASE.BR);
					inthBean.setServer(SlDealModule.OTC.SERVER_R);
					inthBean.setTag(SlDealModule.OTC.TAG_R);
					inthBean.setDetail(SlDealModule.OTC.DETAIL);
					inthBean.setSyst(SlDealModule.OTC.SYST);
					// 设置dealno
					inthBean.setDealno(ifsRevIotd.getDealno());
					// 设置fedealno
					inthBean.setFedealno(ifsRevIotd.getFedealno());
					// 设置seq
					inthBean.setSeq(SlDealModule.OTC.SEQ);
					// 设置inoutind
					inthBean.setInoutind(SlDealModule.OTC.INOUTIND);
					// 设置statcode
					inthBean.setStatcode(SlDealModule.OTC.STATCODE);
					// 设置priority
					inthBean.setPriority(SlDealModule.OTC.PRIORITY);
					slIotdBean.setInthBean(inthBean);
					// 设置冲销日期
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					slIotdBean.setRevDate(sdf.parse(ifsRevIotd.getAdate()));
					// 插入opics冲销表
					SlOutBean result = otcServer.otcRev(slIotdBean);// 用注入的方式调用opics相关方法

					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
					} else {
						// 冲销成功后修改交易的状态为已冲销
						String ticketId = StringUtils.trimToEmpty(ifsRevIotd.getFedealno());
						if (!"".equals(ticketId)) {
							optionMapper.updateApproveStatusFxOptionByID(ticketId);
						}
					}

				} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程

				} else {
					JY.raise("系统参数未配置,未进行交易导入处理......");
				}

			} catch (Exception e) {
				e.printStackTrace();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}

	}
}
