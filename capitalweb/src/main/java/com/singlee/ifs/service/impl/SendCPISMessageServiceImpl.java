package com.singlee.ifs.service.impl;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.cfets.cpis.IImixCPISManageServer;
import com.singlee.financial.model.AgencyBasicInformationModel;
import com.singlee.financial.model.SendForeignForwardSpotModel;
import com.singlee.financial.model.SendForeignFxmmModel;
import com.singlee.financial.model.SendForeignSwapModel;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.ClassNameUtil;
import com.singlee.ifs.mapper.CPISMessageMapper;
import com.singlee.ifs.mapper.TdQryMsgMapper;
import com.singlee.ifs.mapper.TtFxBasicMapper;
import com.singlee.ifs.model.TdQryMsg;
import com.singlee.ifs.model.TtFxBasic;
import com.singlee.ifs.service.ISendCPISMessageService;
import com.singlee.ifs.service.TradeSettlsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SendCPISMessageServiceImpl.java
 * @Description 交易后确认
 * @createTime 2021年11月02日 17:01:00
 */
public class SendCPISMessageServiceImpl implements ISendCPISMessageService {
    private static Logger LOGGER = LoggerFactory.getLogger(SendCPISMessageServiceImpl.class);

    @Autowired
    private CPISMessageMapper cpisMessageMapper;
    @Autowired
    private TtFxBasicMapper ttFxBasicMapper;
    @Autowired
    private TdQryMsgMapper tdQryMsgMapper;
    @Autowired
    private IAcupServer acupServer;
    @Autowired
    private IImixCPISManageServer iImixCPISManageServer;
    @Autowired
    TradeSettlsService tradeSettlsService;

    @Override
    public int retrieveCpisReturnBackMsg() {
        //取回反馈数据
        int count = 0;
        List<Object> recList = iImixCPISManageServer.getRecMessage();
        if (null == recList) {
            return count;
        }

        for (Object obj : recList) {
            try {
                count++;
                String className = ClassNameUtil.getClassName(obj);
                if("RecCPISFeedBackModel".equals(className)){
                    Map<String, Object> msgMap = new HashMap<String, Object>();
                    msgMap = BeanUtil.beanToMap(obj);
                    String tradeDate = msgMap.get("tradeDate")==null?"":msgMap.get("tradeDate").toString();
                    if(!"".equals(tradeDate)){
                        tradeDate=tradeDate.substring(0, 4)+"-"+tradeDate.substring(4, 6)+"-"+tradeDate.substring(6);
                        msgMap.put("tradeDate", tradeDate);
                    }
                    //目的是部分外汇远期交易填充到即期里
                    String marketIndicator=msgMap.get("marketIndicator")==null?"":msgMap.get("marketIndicator").toString();
                    if("14".equals(marketIndicator)){
                        Map<String, Object> p = new HashMap<String, Object>();
                        p.put("execID", msgMap.get("execID")==null?"":msgMap.get("execID").toString());
                        List<TdQryMsg> list = tdQryMsgMapper.searchQryMsg(p);
                        if(list.size()>0){
                            msgMap.put("marketIndicator", list.get(0).getMarketIndicator());
                        }
                    }

                    msgMap.put("sendState", "0");
                    //如果提示交易已确认即交易匹配成功
                    //修改为发送过的不再定时重新发送
                    String text=msgMap.get("text")==null?"":msgMap.get("text").toString();
                    if(text.contains("附言")){
                        msgMap.put("jobnum", "1");//需要定时重新发送一次
                    }else{
                        msgMap.put("jobnum", "2");
                    }
                    tdQryMsgMapper.update(msgMap);
                }else{
                    //推送数据
                    Map<String, Object> msgMap = new HashMap<String, Object>();
                    msgMap = BeanUtil.beanToMap(obj);
                    tdQryMsgMapper.updateExecType(msgMap);
                }
            } catch (Exception e) {
                LOGGER.error("处理异常: ", e);
            }
        }
        return count;
    }

    @Override
    public int sendFxLend(Map<String, Object> condMap) {
        Map<String, String> typeMap = new HashMap<String, String>();
        //清算信息
        typeMap.put("interAccount_type", "213");//中间行账号
        typeMap.put("interBank_type", "209");//中间行名称
        typeMap.put("interBicCode_type", "211");//中间行BICCODE
        typeMap.put("opAccount_type", "207");//开户行账号
        typeMap.put("opBank_type", "110");//开户行名称
        typeMap.put("opBicCode_type", "138");//开户行BICCODE
        typeMap.put("reAccount_type", "15");//收款行账号
        typeMap.put("reBank_type", "23");//收款行名称
        typeMap.put("reBicCode_type", "16");//收款行BICCODE
        //外币拆借
        LOGGER.info("==================开始发送外币拆借交易信息==============================");
        int count = sendFxLend(condMap, typeMap);
        LOGGER.info("==================完成发送外币拆借交易信息==============================");
        return count;
    }

    @Override
    public int sendFxSpt(Map<String, Object> condMap) {
        Map<String, String> typeMap = new HashMap<String, String>();
        //清算信息
        typeMap.put("interAccount_type", "213");//中间行账号
        typeMap.put("interBank_type", "209");//中间行名称
        typeMap.put("interBicCode_type", "211");//中间行BICCODE
        typeMap.put("opAccount_type", "207");//开户行账号
        typeMap.put("opBank_type", "110");//开户行名称
        typeMap.put("opBicCode_type", "138");//开户行BICCODE
        typeMap.put("reAccount_type", "15");//收款行账号
        typeMap.put("reBank_type", "23");//收款行名称
        typeMap.put("reBicCode_type", "16");//收款行BICCODE
        LOGGER.info("==================开始发送即期交易信息==============================");
        List<SendForeignForwardSpotModel> forwardSpotModels= cpisMessageMapper
                .searchForeignSptMessage(condMap);
        int count = sendSptFwd(forwardSpotModels,"12",typeMap);
        LOGGER.info("==================完成发送即期交易信息==============================");
        return count;
    }

    @Override
    public int sendFxFwd(Map<String, Object> condMap) {
        Map<String, String> typeMap = new HashMap<String, String>();
        //清算信息
        typeMap.put("interAccount_type", "213");//中间行账号
        typeMap.put("interBank_type", "209");//中间行名称
        typeMap.put("interBicCode_type", "211");//中间行BICCODE
        typeMap.put("opAccount_type", "207");//开户行账号
        typeMap.put("opBank_type", "110");//开户行名称
        typeMap.put("opBicCode_type", "138");//开户行BICCODE
        typeMap.put("reAccount_type", "15");//收款行账号
        typeMap.put("reBank_type", "23");//收款行名称
        typeMap.put("reBicCode_type", "16");//收款行BICCODE
        LOGGER.info("==================开始发送远期交易信息==============================");
        List<SendForeignForwardSpotModel> forwardSpotModels = cpisMessageMapper
                .searchForeignFwdMessage(condMap);
        int count = sendSptFwd(forwardSpotModels,"14",typeMap);
        LOGGER.info("==================完成发送远期交易信息==============================");
        return count;
    }

    @Override
    public void sendFxSWAP(Map<String, Object> dateMap) {
        LOGGER.info("==================开始发送掉期交易信息==============================");
        List<SendForeignSwapModel> sendForeignSwapModels = cpisMessageMapper.searchForeignSwapMessage(dateMap);
        int count = sendSwp(sendForeignSwapModels,"11");
        LOGGER.info("==================完成发送远期交易信息==============================");

    }

    private int sendSwp(List<SendForeignSwapModel> sendForeignSwapModels, String marketIndicator) {
        int count = 0;
        if (null==sendForeignSwapModels || sendForeignSwapModels.isEmpty()) {
            return 0;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tradeDate = sdf.format(new Date());//默认当前日期
        Map<String, Object> params = new HashMap<String, Object>();
        for (SendForeignSwapModel sendForeignSwapModel : sendForeignSwapModels) {
            try {
                LOGGER.info("处理外币掉期交易后 BEG["
                        + sendForeignSwapModel.getDealNo() + ":"
                        + sendForeignSwapModel.getExecID() + "]");

                params.put("ticketId", sendForeignSwapModel.getDealNo());


                TradeSettlsBean tradeSettlsBean = tradeSettlsService.getTradeSettls(params);
                tradeSettlsBean.setFullName("");//本方中文全称
                tradeSettlsBean.setcFullName("");//对手方中文全称
                sendForeignSwapModel.setTradeSettlsBean(tradeSettlsBean);

                sendForeignSwapModel.setMarketIndicator(marketIndicator);
                sendForeignSwapModel.setExecID(sendForeignSwapModel.getExecId());
                sendForeignSwapModel.setPartyID_2(sendForeignSwapModel.getPartyID_2());

                boolean flag = false;
                try {
                    iImixCPISManageServer.confirmCPISTransaction(sendForeignSwapModel);
                    flag = true;
                } catch (Exception e) {
                    LOGGER.error("iImixCPISManageServer.confirmCPISTransaction(sendForeignSwapModel)", e);
                }
                if (flag) {
                    LOGGER.info("处理外币掉期交易后 SUCC["
                            + sendForeignSwapModel.getDealNo() + ":"
                            + sendForeignSwapModel.getExecID() + "]");
                    Map<String, Object> msgDetail = new HashMap<String, Object>();
                    msgDetail.put("dealNo", sendForeignSwapModel.getDealNo());
                    msgDetail.put("execID", sendForeignSwapModel.getExecID());
                    msgDetail.put("confirmID", sendForeignSwapModel.getConfirmID());
                    msgDetail.put("br", sendForeignSwapModel.getBr());
                    msgDetail.put("sendState", "0");
                    msgDetail.put("tradeDate", tradeDate);
                    msgDetail.put("marketIndicator", marketIndicator);
                    msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
                    List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
                    if (!(recList!=null && recList.size()>0)) {
                        tdQryMsgMapper.insert(msgDetail);
                    }
                }else {
                    LOGGER.info("处理外币掉期远期交易后 FAIL["
                            + sendForeignSwapModel.getDealNo() + ":"
                            + sendForeignSwapModel.getExecID() + "]");
                }
                count++;
            }catch (Exception e){
                LOGGER.error("处理外币掉期远期交易后 EXCEPTION ["
                        + sendForeignSwapModel.getDealNo() + ":"
                        + sendForeignSwapModel.getExecID() + "]");
            }
        }

        return count;
    }

    private int sendFxLend(
            Map<String, Object> dateMap,
            Map<String, String> typeMap) {
        int count = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tradeDate = sdf.format(new Date());//默认当前日期
        Map<String, Object> params = new HashMap<String, Object>();
        String myInst= SystemProperties.cfetsInstId;//本方机构21码
        List<SendForeignFxmmModel> fxmmList = cpisMessageMapper.searchForeignFxmmMessage(dateMap);
        if (fxmmList==null || fxmmList.isEmpty()) {
            return 0;
        }

        for (SendForeignFxmmModel sendForeignFxmmModel : fxmmList) {
            try {
                LOGGER.info("处理外币拆借交易后 BEG["
                        + sendForeignFxmmModel.getDealNo() + ":"
                        + sendForeignFxmmModel.getExecID() + "]");
                params.put("ticketId", sendForeignFxmmModel.getDealNo());
                List<AgencyBasicInformationModel> list1 = new ArrayList<AgencyBasicInformationModel>();//本方机构清算信息
                List<AgencyBasicInformationModel> list2 = new ArrayList<AgencyBasicInformationModel>();//对手方机构清算信息
                List<TtFxBasic> fxlist1 = ttFxBasicMapper.searchMyInstBasicFXMM(params);
                list1 = fun1(list1, fxlist1, typeMap);
                List<TtFxBasic> fxlist2 = ttFxBasicMapper.searchMyInstBasicFXMMOpponet(params);
                list2 = fun1(list2, fxlist2, typeMap);
                //添加其他信息
                String packmsg = ttFxBasicMapper.searchMyInstBasicFXMMOther(params);
                try {
                    list1=funOther(list1,packmsg,myInst,sendForeignFxmmModel.getPartyCfetsNo(),"5,124,139","21");
                    list2=funOther(list2,packmsg,sendForeignFxmmModel.getPartyCfetsNo(),myInst,"5,124,139","21");
                } catch (Exception e) {
                    LOGGER.error("处理异常: ", e);
                }
                sendForeignFxmmModel.setList1(list1);
                sendForeignFxmmModel.setList2(list2);

                //add by lzb
                sendForeignFxmmModel.setMarketIndicator("21");
                sendForeignFxmmModel.setExecID(sendForeignFxmmModel.getExecId());
                sendForeignFxmmModel.setPartyID_2(sendForeignFxmmModel.getPartyCfetsNo());

                boolean flag = false;
                try {
                    flag = iImixCPISManageServer.confirmCPISTransaction(sendForeignFxmmModel);
                } catch(Exception e) {
                    LOGGER.error("发送交易后确认异常:", e);
                }
                if (flag) {
                    LOGGER.info("处理外币拆借交易后 SUCC["
                            + sendForeignFxmmModel.getDealNo() + ":"
                            + sendForeignFxmmModel.getExecID() + "]");
                    Map<String, Object> msgDetail = new HashMap<String, Object>();
                    msgDetail.put("dealNo", sendForeignFxmmModel.getDealNo());
                    msgDetail.put("execID", sendForeignFxmmModel.getExecID());
                    msgDetail.put("confirmID", sendForeignFxmmModel.getConfirmID());
                    msgDetail.put("br", sendForeignFxmmModel.getBr());
                    msgDetail.put("sendState", "0");
                    msgDetail.put("tradeDate", tradeDate);
                    msgDetail.put("marketIndicator", "21");
                    msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
                    List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
                    if (!(recList!=null && recList.size()>0)) {
                        tdQryMsgMapper.insert(msgDetail);
                    }
                } else {
                    LOGGER.info("处理外币拆借交易后 FAIL["
                            + sendForeignFxmmModel.getDealNo() + ":"
                            + sendForeignFxmmModel.getExecID() + "]");
                }
                count++;
            } catch (Exception e) {
                LOGGER.error("处理外币拆借交易后 EXCEPTION ["
                        + sendForeignFxmmModel.getDealNo() + ":"
                        + sendForeignFxmmModel.getExecID() + "]", e);
            }
        }
        return count;
    }

    private int sendSptFwd(
            List<SendForeignForwardSpotModel> forwardSpotModels,
            String marketIndicator,
            Map<String, String> typeMap) {
        int count = 0;
        if (null==forwardSpotModels || forwardSpotModels.isEmpty()) {
            return 0;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tradeDate = sdf.format(new Date());//默认当前日期
        Map<String, Object> params = new HashMap<String, Object>();
        String myInst=SystemProperties.cfetsInstId;//本方机构21码
        for (SendForeignForwardSpotModel sendForeignForwardSpotModel : forwardSpotModels) {
            try {
                LOGGER.info("处理外币即期交易后 BEG["
                        + sendForeignForwardSpotModel.getDealNo() + ":"
                        + sendForeignForwardSpotModel.getExecID() + "]");
                params.put("ticketId", sendForeignForwardSpotModel.getDealNo());
                Map<String, String> typeMap1 = new HashMap<String, String>();
                typeMap1.put("opBank_type", "110");//开户行名称
                typeMap1.put("reAccount_type", "15");//收款行账号
                typeMap1.put("reBank_type", "23");//收款行名称
                typeMap1.put("reBicCode_type", "16");//收款行BICCODE
                List<AgencyBasicInformationModel> list1 = new ArrayList<AgencyBasicInformationModel>();//本方机构清算信息
                List<AgencyBasicInformationModel> list2 = new ArrayList<AgencyBasicInformationModel>();//对手方机构清算信息

                //卖出币种为外币时对手方清算信息处理
                String soldCurrency = sendForeignForwardSpotModel.getCurrency2();//卖出币种
                //卖出币种为外币时对手方清算信息处理
                List<TtFxBasic> fxlist2;
                if("12".equals(marketIndicator)){
                    fxlist2 = ttFxBasicMapper.searchMyInstBasicFXSPTOpponet(params);
                }else{
                    fxlist2 = ttFxBasicMapper.searchMyInstBasicFXFWDOpponet(params);
                }
                if (!"CNY".equals(soldCurrency)) {
                    list2 = fun1(list2, fxlist2, typeMap);
                }else {//人民币
                    list2 = fun1(list2, fxlist2, typeMap1);
                }
                //买入币种为人民币时清算机构信息处理
                List<TtFxBasic> fxlist1;
                String packmsg="";//添加其他信息
                if("12".equals(marketIndicator)){
                    packmsg = ttFxBasicMapper.searchMyInstBasicFXSPTOther(params);
                    fxlist1 = ttFxBasicMapper.searchMyInstBasicFXSPT(params);
                }else{
                    packmsg = ttFxBasicMapper.searchMyInstBasicFXFWDOther(params);
                    fxlist1 = ttFxBasicMapper.searchMyInstBasicFXFWD(params);
                }
                if (sendForeignForwardSpotModel.getCurrency1().equals("CNY")) {
                    list1 = fun1(list1, fxlist1, typeMap1);
                }else {
                    list1 = fun1(list1, fxlist1, typeMap);
                }
                try {
                    Map<String, Object> msgDetail1 = new HashMap<String, Object>();
                    msgDetail1.put("dealNo", sendForeignForwardSpotModel.getDealNo());
                    List<TdQryMsg> recList1 = tdQryMsgMapper.searchQryMsg(msgDetail1);
                    String str1="124,139";
                    String str2="124,145";
                    if (recList1!=null && recList1.size()>0) {
                        if("1".equals(recList1.get(0).getJobnum())){
                            str1="124,145";
                            str2="124,139";
                        }
                    }
                    list1=funOther(list1,packmsg,myInst,sendForeignForwardSpotModel.getPartyCFETSNO(),str1,marketIndicator);
                    list2=funOther(list2,packmsg,sendForeignForwardSpotModel.getPartyCFETSNO(),myInst,str2,marketIndicator);
                } catch (Exception e) {
                    LOGGER.error("funOther异常["
                            + sendForeignForwardSpotModel.getDealNo() + ":"
                            + sendForeignForwardSpotModel.getExecID() + "]!");
                }
                sendForeignForwardSpotModel.setList1(list1);
                sendForeignForwardSpotModel.setList2(list2);
                sendForeignForwardSpotModel.setMarketIndicator(marketIndicator);
                sendForeignForwardSpotModel.setExecID(sendForeignForwardSpotModel.getExecId());
                sendForeignForwardSpotModel.setPartyID_2(sendForeignForwardSpotModel.getPartyCFETSNO());

                boolean flag = false;
                try {
                    iImixCPISManageServer.confirmCPISTransaction(sendForeignForwardSpotModel);
                    flag = true;
                } catch (Exception e) {
                    LOGGER.error("iImixCPISManageServer.confirmCPISTransaction(sendForeignForwardSpotModel)", e);
                }

                if (flag) {
                    LOGGER.info("处理外币即期交易后 SUCC["
                            + sendForeignForwardSpotModel.getDealNo() + ":"
                            + sendForeignForwardSpotModel.getExecID() + "]");
                    Map<String, Object> msgDetail = new HashMap<String, Object>();
                    msgDetail.put("dealNo", sendForeignForwardSpotModel.getDealNo());
                    msgDetail.put("execID", sendForeignForwardSpotModel.getExecID());
                    msgDetail.put("confirmID", sendForeignForwardSpotModel.getConfirmID());
                    msgDetail.put("br", sendForeignForwardSpotModel.getBr());
                    msgDetail.put("sendState", "0");
                    msgDetail.put("tradeDate", tradeDate);
                    msgDetail.put("marketIndicator", marketIndicator);
                    msgDetail.put("affirmStatus", "INI");//校验状态，默认是会员上传，目的防止接收报文有误导致无线定时发送确认。
                    List<TdQryMsg> recList = tdQryMsgMapper.searchQryMsg(msgDetail);
                    if (!(recList!=null && recList.size()>0)) {
                        tdQryMsgMapper.insert(msgDetail);
                    }
                } else {
                    LOGGER.info("处理外币远期交易后 FAIL["
                            + sendForeignForwardSpotModel.getDealNo() + ":"
                            + sendForeignForwardSpotModel.getExecID() + "]");
                }
                count++;
            } catch (Exception e) {
                LOGGER.error("处理外币即期远期交易后 EXCEPTION ["
                        + sendForeignForwardSpotModel.getDealNo() + ":"
                        + sendForeignForwardSpotModel.getExecID() + "]");
            }
        }
        return count;
    }


    /**
     *
     * @param basiclist
     * @param param
     * @param codeMap
     * @return
     */
    private List<AgencyBasicInformationModel> fun1(List<AgencyBasicInformationModel> basiclist, List<TtFxBasic> fxlist,
                                                   Map<String, String> codeMap){
        if (fxlist != null && fxlist.size() > 0) {
            for (TtFxBasic ttFxBasic : fxlist) {
                if (StringUtil.isNotEmpty(ttFxBasic.getInterAccount())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getInterAccount());
                    informationModel.setPartySubIDType(codeMap.get("interAccount_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getInterBank())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getInterBank());
                    informationModel.setPartySubIDType(codeMap.get("interBank_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getInterBicCode())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getInterBicCode());
                    informationModel.setPartySubIDType(codeMap.get("interBicCode_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getOpAccount())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getOpAccount());
                    informationModel.setPartySubIDType(codeMap.get("opAccount_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getOpBank())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getOpBank());
                    informationModel.setPartySubIDType(codeMap.get("opBank_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getOpBicCode())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getOpBicCode());
                    informationModel.setPartySubIDType(codeMap.get("opBicCode_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getReAccount())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getReAccount());
                    informationModel.setPartySubIDType(codeMap.get("reAccount_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getReBank())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getReBank());
                    informationModel.setPartySubIDType(codeMap.get("reBank_type"));
                    basiclist.add(informationModel);
                }
                if (StringUtil.isNotEmpty(ttFxBasic.getReBicCode())) {
                    AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                    informationModel.setPartySubID(ttFxBasic.getReBicCode());
                    informationModel.setPartySubIDType(codeMap.get("reBicCode_type"));
                    basiclist.add(informationModel);
                }
            }
        }
        return basiclist;
    }

    private List<AgencyBasicInformationModel> funOther(List<AgencyBasicInformationModel> basiclist,
                                                       String packmsg,String myInst,String operInst,String values,String marketIndicator){
        if(packmsg==null||"".equals(packmsg)){
            return basiclist;
        }
        int myInt = packmsg.indexOf(myInst);
        int operInt = packmsg.indexOf(operInst);
        if(myInt<operInt){
            packmsg=packmsg.substring(myInt,operInt);
        }else{
            packmsg=packmsg.substring(myInt);
        }
        String[] valuesStr = values.split(",");//eg:124,5,139
        for(int i=0;i<valuesStr.length;i++){
            int valueInt = packmsg.indexOf("803="+valuesStr[i]);
            if(valueInt!=-1){
                String valueMsg= packmsg.substring(0,valueInt);
                valueMsg = valueMsg.substring(valueMsg.lastIndexOf("523=")+4);
                AgencyBasicInformationModel informationModel = new AgencyBasicInformationModel();
                informationModel.setPartySubID(valueMsg);
                if("145".equals(valuesStr[i])){
                    informationModel.setPartySubIDType("139");
                }else{
                    informationModel.setPartySubIDType(valuesStr[i]);
                }

                basiclist.add(informationModel);
            }
        }
        return basiclist;
    }

}
