package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsRmbDepositoutMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsRmbDepositout;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service(value = "ifsRmbDepositOutService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbDepositOutService extends IfsApproveServiceBase {


    @Autowired
    IfsRmbDepositoutMapper ifsRmbDepositoutMapper;
    @Autowired
    TaSysParamMapper taSysParamMapper;// 系统参数mapper
    @Autowired
    IBaseServer baseServer;// 查询opics系统时间
    @Autowired
    private IMmServer mmServer;
    @Autowired
    TaUserMapper taUserMapper;

    @Override
    public void statusChange(String flow_type, String flow_id, String serial_no, String status,
                             String flowCompleteType) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
        String date = dateFormat.format(now);
        ifsRmbDepositoutMapper.updateDepositoutRmbByID(serial_no, status, date);
        IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(serial_no);
        // 0.1 当前业务为空则不进行处理
        if (null == ifsRmbDepositout) {
            return;
        }

        // 1.交易审批中状态,需要将交易纳入监控审批
        if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
            // 插入审批监控表，需要整合字段
            IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
                ifsFlowMonitorMapper.deleteById(serial_no);
            }
            // 1.2 保存监控表数据
            IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
            ifsFlowMonitor.setTicketId(ifsRmbDepositout.getTicketId());// 设置监控表-内部id
            ifsFlowMonitor.setContractId(ifsRmbDepositout.getContractId());// 设置监控表-成交单编号
            ifsFlowMonitor.setApproveStatus(ifsRmbDepositout.getApproveStatus());// 设置监控表-审批状态
            ifsFlowMonitor.setPrdName("存放同业");// 设置监控表-产品名称
            ifsFlowMonitor.setPrdNo("rmbDepositOut");// 设置监控表-产品编号：作为分类
            ifsFlowMonitor.setTradeDate(ifsRmbDepositout.getForDate());// 设置监控表-交易日期
            ifsFlowMonitor.setSponsor(ifsRmbDepositout.getSponsor());// 设置监控表-审批发起人
            ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
            String custNo = ifsRmbDepositout.getCustId();
            if (null != custNo || "".equals(custNo)) {
                ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
                IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
                ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
            }
            ifsFlowMonitorMapper.insert(ifsFlowMonitor);

        }
        // 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
        if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
                || StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
                || StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
            IfsFlowMonitor monitor = new IfsFlowMonitor();
            monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
            ifsFlowMonitorMapper.deleteById(serial_no);
            Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
            ifsFlowCompletedMapper.insert(map1);
        }
        // 3.1未审批通过则不导入Opics系统
        if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
            return;
        }
        try {
            // 3.2获取字典中的配置
            Map<String, Object> sysMap = new HashMap<String, Object>();
            sysMap.put("p_code", "000002");// 参数代码
            sysMap.put("p_value", "ifJorP");// 参数值
            List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
            if (sysList.size() != 1) {
                JY.raise("系统参数有误......");
            }
            if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
                JY.raise("系统参数[参数类型]为空......");
            }
            String callType = sysList.get(0).getP_type().trim();
            // 3.3调用java代码
            if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
                // 获取当前账务日期
                Date branchDate = baseServer.getOpicsSysDate(ifsRmbDepositout.getBr());
                // 导入opics中
                SlOutBean result = mmServer.mm(getEntry(ifsRmbDepositout, branchDate));
                if (!result.getRetStatus().equals(RetStatusEnum.S)) {
                    JY.raise("审批未通过，插入opcis表失败......");
                } else {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("ticketId", ifsRmbDepositout.getTicketId());
                    map.put("satacode", "-1");
                    ifsRmbDepositoutMapper.updateStatcodeByTicketId(map);
                }
            } else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {
                Map<String, String> map = BeanUtils.describe(ifsRmbDepositout);
                SlOutBean result = mmServer.idldProc(map);// 本币信用拆借存储过程
                if (!"999".equals(result.getRetCode())) {
                    JY.raise("审批未通过，执行存储过程失败......");
                }
            } else {
                JY.raise("系统参数未配置,未进行交易导入处理......");
            }
        } catch (Exception e) {
            JY.raiseRException("审批未通过......", e);
        }
    }

    /**
     * 获取交易实体
     *
     * @return
     */
    private SlDepositsAndLoansBean getEntry(IfsRmbDepositout ifsRmbDepositout, Date branchDate) {
        SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(ifsRmbDepositout.getBr(), SlDealModule.DL.SERVER,
                SlDealModule.DL.TAG, SlDealModule.DL.DETAIL);
        InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsRmbDepositout.getRemoveInst());
        contraPatryInstitutionInfo.setInstitutionId(ifsRmbDepositout.getCustId());
        mmBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
        // 本方交易员
        InstitutionBean institutionInfo = new InstitutionBean();
        Map<String, Object> userMap = new HashMap<String, Object>();
        userMap.put("userId", ifsRmbDepositout.getSponsor());// 交易员id
        userMap.put("branchId", ifsRmbDepositout.getBranchId());// 机构
        TaUser user = taUserMapper.queryUserByMap(userMap);
        String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.DL.IOPER);
        institutionInfo.setTradeId(trad);
        mmBean.getInthBean().setIoper(trad);
        mmBean.setInstitutionInfo(institutionInfo);
        SlExternalBean externalBean = new SlExternalBean();
        externalBean.setCost(ifsRmbDepositout.getCost());
        externalBean.setProdcode(ifsRmbDepositout.getProduct());
        externalBean.setProdtype(ifsRmbDepositout.getProdType());
        externalBean.setPort(ifsRmbDepositout.getPort());
        externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
        externalBean.setBroker(SlDealModule.BROKER);
        externalBean.setSiind(SlDealModule.DL.SIIND);
        externalBean.setSupconfind(SlDealModule.DL.SUPCONFIND);
        externalBean.setSuppayind(SlDealModule.DL.SUPPAYIND);
        externalBean.setSuprecind(SlDealModule.DL.SUPRECIND);
        externalBean.setVerind(SlDealModule.DL.VERIND);
        externalBean.setDealtext(ifsRmbDepositout.getContractId());
        externalBean.setCustrefno(ifsRmbDepositout.getTicketId());
        // 设置清算路径1
        externalBean.setCcysmeans(ifsRmbDepositout.getCcysmeans());
        // 设置清算账户1
        externalBean.setCcysacct(ifsRmbDepositout.getCcysacct());
        // 设置清算路径2
        externalBean.setCtrsmeans(ifsRmbDepositout.getCtrsmeans());
        // 设置清算账户2
        externalBean.setCtrsacct(ifsRmbDepositout.getCtrsacct());
        mmBean.setExternalBean(externalBean);
        SlInthBean inthBean = mmBean.getInthBean();
        inthBean.setFedealno(ifsRmbDepositout.getTicketId());
        inthBean.setLstmntdate(branchDate);
        mmBean.setInthBean(inthBean);
        double amt = ifsRmbDepositout.getAmt().doubleValue() * 10000;
        mmBean.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
        mmBean.setValueDate(DateUtil.parse(ifsRmbDepositout.getFirstSettlementDate()));
        mmBean.setMaturityDate(DateUtil.parse(ifsRmbDepositout.getSecondSettlementDate()));
        mmBean.setDealDate(DateUtil.parse(ifsRmbDepositout.getForDate()));
        mmBean.setTenor("99");
        mmBean.setIntdatedir("VF");
        mmBean.setCommtypecode("S");
        mmBean.setCcy("CNY");//本币交易
        mmBean.setBasis(ifsRmbDepositout.getBasis());
        mmBean.setRateCode(ifsRmbDepositout.getBasisRateCode());
        mmBean.setIntrate(String.valueOf(ifsRmbDepositout.getRate()));
        mmBean.setCommmeans(ifsRmbDepositout.getCcysmeans());
        mmBean.setCommacct(ifsRmbDepositout.getCcysacct());
        mmBean.setMatmeans(ifsRmbDepositout.getCtrsmeans());
        mmBean.setMatacct(ifsRmbDepositout.getCtrsacct());
        mmBean.setIntcalcrule("Q");
        mmBean.setIntsmeans(ifsRmbDepositout.getCtrsmeans());
        mmBean.setIntsacct(ifsRmbDepositout.getCtrsacct());

//        mmBean.setSpread(BigDecimal.valueOf(0));//TODO 重复
//        mmBean.setIntdaterule("D");//TODO 重复
//        mmBean.setIntpaycycle("Q");//TODO 重复

        mmBean.setScheduleType(ifsRmbDepositout.getScheduleType());//付息类型 一次性还本付息/按期付息
        mmBean.setIntPayCycle(ifsRmbDepositout.getPaymentFrequency());//付息频率
        mmBean.setIntPayDay(ifsRmbDepositout.getIntpayday());//利息付款日
        mmBean.setIntDateRule(ifsRmbDepositout.getIntdaterule());//付息日调整规则
        double Spread8 = ifsRmbDepositout.getBenchmarkSpread() == null ? BigDecimal.valueOf(0).doubleValue() :
                ifsRmbDepositout.getBenchmarkSpread().divide(new BigDecimal(100)).doubleValue();//点差
        mmBean.setSpread8(BigDecimal.valueOf(MathUtil.roundWithNaN(Spread8,6)));
//		mmBean.setSpread8(BigDecimal.valueOf(ifsRmbDepositout.getSpread8()));
        return mmBean;
    }


}
