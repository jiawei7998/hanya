package com.singlee.ifs.service.callback.prefix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.ifs.mapper.IfsApprovermbCrMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsApprovermbCr;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 质押式回购【事前审批】回调函数-【暂未使用】
 */
@Deprecated
@Service(value="IfsApproveRmbCrService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApproveRmbCrService extends IfsApproveServiceBase {
	
	@Autowired
	IfsApprovermbCrMapper ifsCfetsrmbCrMapper;
	
	@Override
	public void statusChange(String flow_type, String flow_id,
			String serial_no, String status, String flowCompleteType) {
		Date now = new Date(); 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
		String date = dateFormat.format( now ); 
		ifsCfetsrmbCrMapper.updateRmbCrByID(serial_no,status,date);
		IfsApprovermbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(serial_no);
		if(ifsCfetsrmbCr.equals(null)){
			return;
		}
		IfsFlowMonitor ifsFlowMonitor11 = new IfsFlowMonitor();
		///////以下将审批中的记录插入审批监控表
		if(StringUtils.equals(ApproveOrderStatus.Approving, status)){//状态为"审批中"并且为第一次，才可以插入 审批监控表
			//插入审批监控表，需要整合字段
			
			ifsFlowMonitor11 = ifsFlowMonitorMapper.getEntityById(serial_no);
			if(null  == ifsFlowMonitor11 ){//没有相同的id记录才会插入
				IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
				ifsFlowMonitor.setTicketId(ifsCfetsrmbCr.getTicketId());//设置监控表-内部id
				ifsFlowMonitor.setContractId(ifsCfetsrmbCr.getTicketId());//设置监控表-成交单编号
				ifsFlowMonitor.setApproveStatus(ifsCfetsrmbCr.getApproveStatus());//设置监控表-审批状态
				ifsFlowMonitor.setPrdName("质押式回购");//设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("cr");//设置监控表-产品编号：作为分类
				ifsFlowMonitor.setTradeDate(ifsCfetsrmbCr.getPostDate());//设置监控表-交易日期
				ifsFlowMonitor.setSponsor(ifsCfetsrmbCr.getSponsor());//设置监控表-审批发起人
				String custNo = ifsCfetsrmbCr.getCounterpartyInstId();
				ifsFlowMonitor.setTradeType("0");//交易类型，0：事前交易，1：正式交易
				if(null != custNo || "".equals(custNo)){
					ifsFlowMonitor.setCustNo(custNo);//设置监控表-客户号
					IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
					ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());//设置监控表-客户名称(全称)	
				}
				StringBuffer buffer=new StringBuffer("");
				List<LinkedHashMap<String,String>> listmap=ifsApproveElementMapper.searchTradeElements("IFS_APPROVERMB_CR");
				if(listmap.size()==1){
				LinkedHashMap<String,String> hashmap=listmap.get(0);
				String tradeElements=hashmap.get("TRADE_GROUP");
				String commentsGroup=hashmap.get("COMMENTSGROUPS");
					if(tradeElements!="null"||tradeElements!=""){
						buffer.append("{");
						String sqll="select "+tradeElements+" from IFS_APPROVERMB_CR where TICKET_ID="+"'"+serial_no+"'";
						LinkedHashMap<String,Object> swap=ifsCfetsrmbCrMapper.searchProperty(sqll);
						String[] str=tradeElements.split(",");
						String[] comments=commentsGroup.split(",");
						for(int j=0;j<str.length;j++){
							String kv=str[j];
							String key=comments[j];
							String value=String.valueOf(swap.get(kv));
							buffer.append(key).append(":").append(value);
							if(j!=str.length-1){
								buffer.append(",");
							}
						}
						buffer.append("}");
					}
				}
				ifsFlowMonitor.setTradeElements(buffer.toString());
				ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			}
			
		}
		//审批通过  删掉审批监控表中的数据，并将该记录插入审批完成表
		if(StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)
				|| StringUtils.equals(ApproveOrderStatus.New, status)){
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22  = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);//设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map3 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map3);
		}
				
		//只有审批状态为'审批通过'才调用存储过程
		/*if(!status.equals("6")){
			return;
		}
		Map<String, String> map=null;  
		
        try{
        	//将实体对象转换成map
        	map = BeanUtils.describe(ifsCfetsrmbCr);
        	//正式调用存储过程
        	RetBean result = repoServer.ircaProc(map);//质押式回购存储过程
        	if(!result.getRetCode().equals("999")){
        		JY.raise("审批未通过......");
			}
        }catch(Exception e){
        	JY.raise("审批未通过......",e.getMessage());
        }*/
		
		
	}

}
