package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProdBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsProdMapper;
import com.singlee.ifs.model.IfsOpicsProd;
import com.singlee.ifs.service.IfsOpicsProdService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsProdServiceImpl implements IfsOpicsProdService {
	@Resource
	IfsOpicsProdMapper ifsOpicsProdMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsProd entity) {
		String psn = DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setPsn(psn);
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsProdMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsProd entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsProdMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsProdMapper.deleteById(id);

	}

	@Override
	public Page<IfsOpicsProd> searchPageOpicsProd(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsProd> result = ifsOpicsProdMapper.searchPageOpicsProd(map, rb);
		return result;
	}

	@Override
	public List<IfsOpicsProd> searchProd(HashMap<String, Object> map) {
		List<IfsOpicsProd> list = ifsOpicsProdMapper.searchProd(map);
		return TreeUtil.mergeChildrenList(list, "-1");
	}

	@Override
	public IfsOpicsProd searchById(String id) {
		return ifsOpicsProdMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsProd entity) {
		ifsOpicsProdMapper.updateStatus(entity);
	}

	@Override
	public SlOutBean batchCalibration(String type, String[] pcodes) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从OPICS库查询所有数据
			List<SlProdBean> listProdOpics = iStaticServer.synchroProd();

			/**************** 对选中的行进行校准 **********************/
			if ("1".equals(type)) { // 选中行校准
				for (int i = 0; i < pcodes.length; i++) {
					for (int j = 0; j < listProdOpics.size(); j++) {
						// 若OPICS库中有相同的产品，就将本地的更新成OPICS中的，其中备注保持不变
						if (listProdOpics.get(j).getPcode().equals(pcodes[i])) {
							IfsOpicsProd entity = new IfsOpicsProd();
							entity = ifsOpicsProdMapper.searchById(pcodes[i]);

							entity.setPcode(listProdOpics.get(j).getPcode());
							entity.setPdesc(listProdOpics.get(j).getPdesc());
							entity.setSys(listProdOpics.get(j).getSys());

							entity.setLstmntdte(listProdOpics.get(j).getLstmntdte());
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1"); // 设置为已同步
							ifsOpicsProdMapper.updateById(entity);
							break;
						}
						// 若OPICS库中无数据，则将状态改为'未同步'，其余不变
						if (j == listProdOpics.size() - 1) {
							ifsOpicsProdMapper.updateStatus0ById(pcodes[i]);
						}
					}
				}

				/**************** 对本地库所有进行校准 **********************/
			} else { // 全部校准
				// 从本地库查询所有的数据
				List<IfsOpicsProd> listProdLocal = ifsOpicsProdMapper.searchAllOpicsProd();
				/** 1、以OPICS库为主，更新或插入到本地库 */
				for (int i = 0; i < listProdOpics.size(); i++) {
					// 本地库没有数据，opics数据全部插入
					if (listProdLocal.size() == 0) {
						IfsOpicsProd entity = new IfsOpicsProd();
						entity.setPcode(listProdOpics.get(i).getPcode());
						entity.setPdesc(listProdOpics.get(i).getPdesc());
						entity.setSys(listProdOpics.get(i).getSys());
						entity.setLstmntdte(listProdOpics.get(i).getLstmntdte());
						entity.setOperator(SlSessionHelper.getUserId());
						entity.setStatus("1"); // 设置为已同步
						ifsOpicsProdMapper.insert(entity);
					} else {
						for (int j = 0; j < listProdLocal.size(); j++) {
							IfsOpicsProd entity = new IfsOpicsProd();
							entity.setPcode(listProdOpics.get(i).getPcode());
							entity.setPdesc(listProdOpics.get(i).getPdesc());
							entity.setSys(listProdOpics.get(i).getSys());
							entity.setLstmntdte(listProdOpics.get(i).getLstmntdte());
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1"); // 设置为已同步
							// 与OPICS库中的产品对比，相同的内容不更新，只更新不同的内容
							if (listProdOpics.get(i).getPcode().equals(listProdLocal.get(j).getPcode())) {
								entity.setPdesccn(listProdLocal.get(j).getPdesccn());
								entity.setPsn(listProdLocal.get(j).getPsn());
								entity.setPpid(listProdLocal.get(j).getPpid());
								entity.setRemark(listProdLocal.get(j).getRemark());
								ifsOpicsProdMapper.updateById(entity);
								break;
							}
							// 若全部不同，则插入本地库
							if (j == listProdLocal.size() - 1) {
								ifsOpicsProdMapper.insert(entity);
							}

						}// end for
					}
				}// end for

				/** 2、若本地库中的数据多于OPICS库，则将本地库中含有的但OPICS库中没有的数据的状态改为'未同步' */
				// 遍历完OPICS库之后，从本地库【再次】查询所有数据，遍历本地库
				List<IfsOpicsProd> listCostLocal2 = ifsOpicsProdMapper.searchAllOpicsProd();
				if (listCostLocal2.size() > listProdOpics.size()) {
					for (int i = 0; i < listCostLocal2.size(); i++) {
						for (int j = 0; j < listProdOpics.size(); j++) {
							if (listProdOpics.get(j).getPcode().trim().equals(listCostLocal2.get(i).getPcode())) {
								break;
							}
							// 若OPICS库中没有，则将状态改为'未同步'
							if (j == listProdOpics.size() - 1) {
								IfsOpicsProd prod = new IfsOpicsProd();
								prod.setPcode(listCostLocal2.get(i).getPcode());
								prod.setPdesc(listCostLocal2.get(i).getPdesc());
								prod.setSys(listCostLocal2.get(i).getSys());

								prod.setRemark(listCostLocal2.get(i).getRemark());
								prod.setLstmntdte(new Date());
								prod.setOperator(SlSessionHelper.getUserId());
								prod.setStatus("0"); // 设置为未同步
								prod.setPpid(listCostLocal2.get(i).getPpid());
								ifsOpicsProdMapper.updateById(prod);
							}
						}
					}
				}
			}
		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

}
