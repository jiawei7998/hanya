package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsPort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IfsOpicsPortService {
	// 新增
	public void insert(IfsOpicsPort entity);

	// 修改
	void updateById(IfsOpicsPort entity);

	// 删除
	void deleteById(Map<String, Object> map);

	// 分页查询
	Page<IfsOpicsPort> searchPageOpicsPort(Map<String, Object> map);

	public List<IfsOpicsPort> searchPort(HashMap<String, Object> map);

	IfsOpicsPort searchById(Map<String, String> map);

	void updateStatus(IfsOpicsPort entity);

	public SlOutBean batchCalibration(String type, String[] brs, String[] portfolios);

	public List<IfsOpicsPort> searchAllOpicsPort();

}
