package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.cashflow.mapper.TdCashflowCapitalMapper;
import com.singlee.capital.cashflow.mapper.TdCashflowInterestMapper;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.mapper.IfsRmbDepositoutMapper;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import com.singlee.ifs.model.IfsRmbDepositout;
import com.singlee.ifs.service.IfsRmbDepOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2021/12/23
 *
 * @Auther:zk
 */
@Service
public class IfsRmbDepOutServiceImpl implements IfsRmbDepOutService {

    @Resource
    IFSMapper ifsMapper;
    @Autowired
    IfsRmbDepositoutMapper ifsRmbDepositoutMapper;
    @Autowired
    CalCashFlowService calCashFlowService;
    @Autowired
    TdRateRangeMapper tdRateRangeMapper;
    @Autowired
    TdCashflowCapitalMapper cashflowCapitalMapper;
    @Autowired
    TdCashflowInterestMapper cashflowInterestMapper;

    @Override
    public void editRmbDepositOut(IfsRmbDepositout record) {
        record.setApproveStatus(DictConstants.ApproveStatus.New);
        ifsRmbDepositoutMapper.updateByPrimaryKey(record);
        //重新生成现金流
//        deleteCashFlow(record.getTicketId());
        generateCashFlow(record);
    }

    @Override
    public String addRmbDepositOut(IfsRmbDepositout record) {
        record.setApproveStatus(DictConstants.ApproveStatus.New);
        record.setTicketId(getTicketId());
        String str = "";
        List<IfsRmbDepositout> list = ifsRmbDepositoutMapper.searchById(record.getContractId());
        if (list.isEmpty()) {
            try {
                ifsRmbDepositoutMapper.insert(record);
                //创建现金流
                generateCashFlow(record);
                str = "保存成功";
            } catch (Exception e) {
                str = "保存失败";
                JY.raiseRException("保存失败",e);
            }
        } else {
            str = "保存失败,该笔成交单号已存在";
        }
        return str;
    }

    @Override
    public void deleteRmbDepositOut(String ticketid) {
        ifsRmbDepositoutMapper.deleteByTicketid(ticketid);
        //删除现金流
        deleteCashFlow(ticketid);
    }

    @Override
    public Page<IfsRmbDepositout> searchRmbDepositOut(Map<String, Object> params) {
        String ticketId = ParameterUtil.getString(params, "ticketId", "");
        Page<IfsRmbDepositout> page = ifsRmbDepositoutMapper.selectByTicketId(ticketId, ParameterUtil.getRowBounds(params));
        return page;
    }

    @Override
    public Page<IfsRmbDepositout> getDepositOutRmbMinePage(Map<String, Object> params, int isFinished) {
        Page<IfsRmbDepositout> page = new Page<IfsRmbDepositout>();
        if (isFinished == 1) {// 待审批
            page = ifsRmbDepositoutMapper.getDepositoutRmbMineList(params, ParameterUtil.getRowBounds(params));
        } else if (isFinished == 2) {// 已审批
            page = ifsRmbDepositoutMapper.getDepositoutRmbMineListFinished(params, ParameterUtil.getRowBounds(params));
        } else {// 我发起的
            page = ifsRmbDepositoutMapper.getDepositoutRmbMineListMine(params, ParameterUtil.getRowBounds(params));
        }
        return page;
    }

    @Override
    public IfsRmbDepositout searchDepositOutRmbByTicketId(Map<String, Object> params) {
        String ticketId = ParameterUtil.getString(params, "ticketId", "");
        IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(ticketId);
        return ifsRmbDepositout;
    }

    @Override
    public IfsRmbDepositout searchFlowDepositOutRmbByTicketId(Map<String, Object> params) {
        IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchFlowByTicketId(params);
        return ifsRmbDepositout;
    }

    public String getTicketId() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("sStr", "");// 前缀
        hashMap.put("trdType", "2");// 贸易类型，正式交易
        String ticketId = ifsMapper.getTradeId(hashMap);
        return ticketId;
    }


    /**
     * 生成现金流
     * @param deposit
     */
    @Override
    public void generateCashFlow(IfsRmbDepositout deposit) {
        List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
        List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
        Map<String,Object> queryMap = new HashMap<>();

        String vDate = deposit.getFirstSettlementDate();//起息日
        String mDate = deposit.getSecondSettlementDate();//到期日
        BigDecimal amt = deposit.getAmt();//金额
        //double contactRate = deposit.getRate().doubleValue()*0.01;//利率
        double contactRate = deposit.getRate().add(deposit.getBenchmarkSpread().multiply(new BigDecimal("0.1")))
                                .multiply(new BigDecimal("0.01"))
                                .doubleValue();

        /**
         * 利率区间插入
         */
        queryMap.put("dealNo",deposit.getTicketId());
        tdRateRangeMapper.deleteRateRanges(queryMap);
        TdRateRange rate = new TdRateRange();
        rate.setDealNo(deposit.getTicketId());
        rate.setVersion(0);
        rate.setBeginDate(vDate);
        rate.setEndDate(mDate);
        rate.setExecRate(contactRate);
        tdRateRangeMapper.insert(rate);

        PrincipalInterval principalInterval = new PrincipalInterval();
        principalInterval.setStartDate(vDate);
        principalInterval.setEndDate(mDate);
        principalInterval.setResidual_Principal(amt.doubleValue());
        principalIntervals.add(principalInterval);

        InterestRange interestRange = new InterestRange();
        interestRange.setStartDate(vDate);
        interestRange.setEndDate(mDate);
        interestRange.setRate(contactRate);
        interestRanges.add(interestRange);

        Map<String,Object> map = new HashMap<>();

        map.put("dealNo",deposit.getTicketId());
        map.put("vDate",vDate);//起息日
        map.put("mDate",mDate);//到期日

        map.put("meDate",mDate);//到账日
        map.put("sDate",vDate);//首次付息日
        map.put("eDate",mDate);//最后一个付息日
        map.put("amt",amt.doubleValue());
        map.put("contractRate",contactRate);
        map.put("acturalRate",contactRate);
        map.put("dealType",2);
        map.put("version",0);
        map.put("prdNo",deposit.getfPrdCode());
        map.put("intType","2");//后收息
        map.put("intFre","D".equals(deposit.getScheduleType())? "0" : valueOf(deposit.getPaymentFrequency()));//付息频率
        map.put("amtFre","0");//还本频率 0-到期一次性
        map.put("basis",basisValueOf(deposit.getBasis()));//计息基础
        calCashFlowService.createHrbCashFLow(principalIntervals, interestRanges, map);
    }

    /**
     * 删除现金流
     * @param ticketId
     */
    private void deleteCashFlow(String ticketId) {
        Map<String,Object> queryMap = new HashMap<>();
        queryMap.put("dealNo",ticketId);
        queryMap.put("dealType",2);
        calCashFlowService.deleteAllCashFlowsForDeleteDeal(queryMap);
    }

    @Override
    public List<CashflowCapital> searchAmt(Map<String, Object> params) {
        List<CashflowCapital> capitalList = cashflowCapitalMapper.getCapitalList(params);
        return capitalList;
    }

    @Override
    public List<CashflowInterest> searchInt(Map<String, Object> params) {
        List<CashflowInterest> interestList = cashflowInterestMapper.getInterestList(params);
        return interestList;
    }

    //根据本金计划生成付息计划
    @Override
    public List<CashflowInterest> getInterestByCapital(Map<String,Object> param){
        //查询交易信息
        String dealNo = ParameterUtil.getString(param, "dealNo", null);
        Map<String, Object> map = new HashMap<>();
        map.put("ticketId",dealNo);
        IfsRmbDepositout ifsRmbDepositout = searchDepositOutRmbByTicketId(map);

        //准本利率信息
        BigDecimal spread = ifsRmbDepositout.getBenchmarkSpread() == null? new BigDecimal(0):ifsRmbDepositout.getBenchmarkSpread();
        BigDecimal rate = ifsRmbDepositout.getRate().add(spread.multiply(new BigDecimal("0.1")))
                            .multiply(new BigDecimal("0.01"));
        List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
        InterestRange interestRange = new InterestRange();
        interestRange.setStartDate(ifsRmbDepositout.getFirstSettlementDate());
        interestRange.setEndDate(ifsRmbDepositout.getSecondSettlementDate());
        interestRange.setRate(rate.doubleValue());
        interestRanges.add(interestRange);
        //准本其他传值
        param.put("vDate",ifsRmbDepositout.getFirstSettlementDate());//交易起息日
        param.put("mDate",ifsRmbDepositout.getFirstSettlementDate());//交易到期日
        param.put("amt",ifsRmbDepositout.getAmt());//交易本金
        param.put("basis",basisValueOf(ifsRmbDepositout.getBasis()));//计息基础

        return calCashFlowService.calInterestFlowListHrb(interestRanges,param);
    }

    @Override
    public void saveCashFlow(Map<String, Object> map) {
        map.put("dealType","2");
        calCashFlowService.saveCaptialAndInterestCashFlowHrb(map);
    }

    /**
     * 付息频率页面字典到现金流内置字典转换
     * @param intFeq
     * @return
     */
    private String valueOf(String intFeq){
        switch (intFeq) {
            case "A":
                return "1";//年
            case "S":
                return "2";//半年
            case "Q":
                return "4";//季度
            case "M":
                return "12";//月
            default:
                return null;
        }
    }

    /**
     * 计息基础字典转换
     */
    private String basisValueOf(String basis){
        switch(basis){
            case "A360":
                return DictConstants.DayCounter.Actual360;
            case "A365":
                return DictConstants.DayCounter.Actual365;
            default:
                return basis;
        }
    }



}
