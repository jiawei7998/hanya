package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.eu.mapper.TcEdProdMapper;
import com.singlee.capital.eu.model.TcEdProd;
import com.singlee.capital.interfacex.model.QuotaSyncopateRec;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.PdAttrInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsLimitService;
import com.singlee.ifs.service.IfsOpicsBondService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(value = "IfsLimitConditionService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsLimitConditionServiceImpl implements IfsLimitService {

	@Autowired
	private IfsLimitConditionMapper ifsLimitConditionMapper;

	@Autowired
	private IfsLimitTemplateMapper ifslimittemplatemapper;

	@Autowired
	private BondLimitTemplateMapper bondLimitTemplateMapper;

	@Autowired
	private IfsOpicsCostMapper ifsopicscostmapper;

	@Autowired
	IAcupServer acupServer;

	@Autowired
	TcEdProdMapper tcEdProdMapper;

	@Autowired
	IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper;
	
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	
	@Autowired
	IBaseServer iBaseServer;
	
	@Override
	public Page<IfsLimitCondition> searchPageLimit(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsLimitCondition> result = ifsLimitConditionMapper.searchPageLimit(map, rb);
		return result;
	}

	@Override
	public void update(IfsLimitCondition entity) {
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		ifsLimitConditionMapper.update(entity);
	}

	@Override
	public void insert(IfsLimitCondition entity) {
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		String id = "id" + DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setId(id);
		ifsLimitConditionMapper.insert(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsLimitConditionMapper.deleteById(id);
	}

	@Override
	public IfsLimitCondition searchIfsLimitCondition(String id) {
		IfsLimitCondition entity = ifsLimitConditionMapper.searchIfsLimitCondition(id);
		return entity;
	}

	@Override
	public List<Map<String, Object>> searchResult(Map<String, String> map) {
		return ifsLimitConditionMapper.searchResult(map);
	}

	@Override
	public List<Map<String, Object>> searchColumsValue(Map<String, String> map) {
		return null;
	}

	/**
	 * 20180531新增：查询交易员的总授信额度、总可用额度
	 * 
	 * @see com.singlee.ifs.service.IfsLimitService#searchTraderTotalAndAvlAmt(java.util.Map)
	 */
	@Override
	public Page<List<Map<String, Object>>> searchTraderTotalAndAvlAmt(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return ifsLimitConditionMapper.searchTraderTotalAndAvlAmt(map, rb);
	}

	/**
	 * 20180531新增：查询交易员的分授信额度、分可用额度
	 * 
	 * @see com.singlee.ifs.service.IfsLimitService#searchTraderEdu(java.util.Map)
	 */
	@Override
	public Page<IfsLimitCondition> searchTraderEdu(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return ifsLimitConditionMapper.searchTraderEdu(map, rb);
	}

	// 外汇限额插入
	@Override
	public void insertLimit(Map<String, Object> map) {
		ifsLimitTemplate limit = new ifsLimitTemplate();
		ParentChildUtil.HashMapToClass(map, limit);
		ifslimittemplatemapper.insert(limit);
	}

	// 外汇限额查询
	@Override
	public Page<ifsLimitTemplate> searchTemplateLimit(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<ifsLimitTemplate> result = ifslimittemplatemapper.searchTemplateLimit(map, rb);
		return result;
	}

	@Override
	public void updateLimit(Map<String, Object> map) {
		ifsLimitTemplate limit = new ifsLimitTemplate();
		ParentChildUtil.HashMapToClass(map, limit);
		ifslimittemplatemapper.updateByPrimaryKey(limit);
	}

	@Override
	public void deleteIfsLimit(Map<String, Object> map) {
		ifsLimitTemplate limit = new ifsLimitTemplate();
		ParentChildUtil.HashMapToClass(map, limit);
		ifslimittemplatemapper.deleteByPrimaryKey(limit);
	}

	@Override
	public void insertBond(Map<String, Object> map) {
		BondTemplate bond = new BondTemplate();
		map.put("totalAmount", "0");
		ParentChildUtil.HashMapToClass(map, bond);
		bondLimitTemplateMapper.insert(bond);
	}

	@Override
	public Page<BondTemplate> searchTemplateBond(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<BondTemplate> result = bondLimitTemplateMapper.searchTemplateBond(map, rb);
		return result;
	}

	@Override
	public void deleteIfsBond(Map<String, Object> map) {
		BondTemplate limit = new BondTemplate();
		ParentChildUtil.HashMapToClass(map, limit);
		bondLimitTemplateMapper.deleteByPrimaryKey(limit);
	}

	@Override
	public void updateBond(Map<String, Object> map) {
		BondTemplate limit = new BondTemplate();
		ParentChildUtil.HashMapToClass(map, limit);
		bondLimitTemplateMapper.updateByPrimaryKey(limit);
	}

	/**
	 * 业务流水号定义为ticketId:value,交易员trader:value,产品:prdCode:value
	 * 产品类型prdTypeCode:value
	 * ,金额amt:value,交易方向direction:value,交易来源dealSource:value
	 * 期限term:value,成本中心cost:value,币种ccyCode:value,检测控制ctlType:value(1监测，2控制)
	 * 确定是外汇还是债券code:value(0,是债券，1是外汇)limitType(1 单笔, 2累计,3总量累计)
	 */
	@Override
	public Map<String, Object> occupyLimit(Map<String, Object> map) {
		
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		map.put("flag", "1");
		Map<String, Object> result = new HashMap<String, Object>();
		// String ticketId = ParameterUtil.getString(map, "ticketId", "");
		String code = ParameterUtil.getString(map, "code", "");
		// int amt = Integer.parseInt(ParameterUtil.getString(map, "amt", ""));
		double amt = Double.parseDouble(ParameterUtil.getString(map, "amt", ""));
		DecimalFormat df = new DecimalFormat("#0.00");
		// /获取当前日期和昨日日期
		String nowDateTime = DateUtil.getCurrentDateAsString();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date time = cal.getTime();
		String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(time);
		map.put("yesterday", yesterday);
		map.put("today", nowDateTime);

		if ("1".equals(code)) {// 外汇
			map.put("iTime", DateUtil.getCurrentDateAsString());
			String rate = "1";
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", "USD");
			mapRate.put("br", "01");// 机构号，送01
			BigDecimal dayRate = acupServer.queryRate(mapRate);
			if (dayRate == null || "".equals(dayRate)) {
				JY.raise("货币USD不存在汇率");
			}
			rate = dayRate + "";// 以后需要修改为BigDecimal

			// 金额除以汇率
			amt = amt / Double.parseDouble(rate);
			double formalAmt = amt;
			map.put("amt", amt);
			
			//外币报表计算统计，开始
			Map<String, Object> resultForeign = new HashMap<String, Object>();
			String amtL = df.format(amt);//金额，折美元
			String ccyL = ParameterUtil.getString(map, "ccy", "");//币种
			resultForeign.put("usd", "0");
			resultForeign.put("eur", "0");
			resultForeign.put("jpy", "0");
			resultForeign.put("hkd", "0");
			resultForeign.put("gbp", "0");
			if("USD".equals(ccyL)){
				resultForeign.put("usd", amtL);
			}else if("EUR".equals(ccyL)){
				resultForeign.put("eur", amtL);
			}else if("JPY".equals(ccyL)){
				resultForeign.put("jpy", amtL);
			}else if("HKD".equals(ccyL)){
				resultForeign.put("hkd", amtL);
			}else if("GBP".equals(ccyL)){
				resultForeign.put("gbp", amtL);
			}
			String direction = ParameterUtil.getString(map, "direction", "");//交易方向
			if("P".equals(direction)){
				resultForeign.put("noUse1", "1");
			}else{
				resultForeign.put("noUse1", "-1");
			}
			String oprTime = ifsOpicsSettleManageMapper.getCurDate();//当前系统时间
			resultForeign.put("foreignLimitType", "0");
			resultForeign.put("oprTime", oprTime);
			resultForeign.put("noUse2", "0");//1为总计，其他明细
			resultForeign.put("ticketId", ParameterUtil.getString(map, "ticketId", ""));
			resultForeign.put("total", amtL);
			//FOREIGN_LIMIT_TYPE,USD,EUR,JPY,HKD,GBP,TOTAL,OPR_TIME,NO_USE1,NO_USE2,TICKET_ID,LIMIT_ID
			
			String prdType = ParameterUtil.getString(map, "prdType", "");//交易产品,外汇报表只读取FX的数据
			if("FX".equals(prdType)){//交易产品,外汇报表只读取FX的数据
				ifslimittemplatemapper.insertForeignLimit(resultForeign);
			}
			//限额明细报表插入结束
			
			
			
			List<ifsLimitTemplate> ifsLimitTemplate = ifslimittemplatemapper.queryOccupyLimit(map);// 查询外汇限额信息
			for (int i = 0; i < ifsLimitTemplate.size(); i++) {

				String limitType = ifsLimitTemplate.get(i).getLimitType();// 1
																		  // 单笔,
																		  // 2累计,3累计量,4累计笔数
				String limitId = ifsLimitTemplate.get(i).getLimitId();// 业务流水号
				map.put("limitId", limitId);

				// 获取昨日外汇限额最大值，昨日敞口
				/*
				 * IfsLimitTemplateDetail ifsLimitTemplateDetail = new
				 * IfsLimitTemplateDetail(); ifsLimitTemplateDetail =
				 * ifslimittemplatemapper.searchYesterdayTotal(map);
				 * if(ifsLimitTemplateDetail == null ||
				 * "".equals(ifsLimitTemplateDetail)){
				 */
				map.put("lastdayTotal", ifsLimitTemplate.get(i).getLastAmount());
				map.put("lastdayMax", ifsLimitTemplate.get(i).getMaxLast());
				/*
				 * }else{ map.put("lastdayTotal",
				 * ifsLimitTemplateDetail.getTodayTotal());
				 * map.put("lastdayMax", ifsLimitTemplateDetail.getMaxToday());
				 * }
				 */

				// String oprDayTime =
				// ifsLimitTemplate.get(i).getOprDayTime();//当日限额时间

				// double availAmount =
				// Double.parseDouble(ifsLimitTemplate.get(i).getAvailAmount());//额度上线
				// double quotaAmount =
				// Double.parseDouble(ifsLimitTemplate.get(i).getQuotaAmount());//额度下线
				double availAmount = Double.parseDouble(ifsLimitTemplate.get(i).getAvailAmountU());// 美元额度上线
				double quotaAmount = Double.parseDouble(ifsLimitTemplate.get(i).getQuotaAmountU());// 美元额度下线

				double totalAmount = Double.parseDouble(ifsLimitTemplate.get(i).getTotalAmount());// 已经使用的额度，累计
				double maxToday = Double.parseDouble(ifsLimitTemplate.get(i).getMaxToday());// 今日最大值
				String ctlType = ifsLimitTemplate.get(i).getCtlType();// 1监测，2控制

				if ("3".equals(limitType)) {// 额度累计量计算
					amt = new Double(Math.abs(amt));
					map.put("amt", amt);
				} else {
					amt = formalAmt;
					map.put("amt", formalAmt);
				}
				double sumAmount = totalAmount + amt;// 已经做过的交易加新做交易

				if (Math.abs(sumAmount) > Math.abs(maxToday)) {
					maxToday = sumAmount;
				}

				map.put("maxToday", maxToday);
				map.put("totalAmount", sumAmount);// 总额
				// map.put("totalAmount", sumAmount);

				if ("1".equals(limitType) || limitType == null) {
					if (quotaAmount > amt && "1".equals(ctlType)) {
						result.put("errorCode", "WARN");
						result.put("errorMesg", "外汇单笔下线不足,单笔下线限额" + df.format(quotaAmount) + "本次交易" + df.format(amt));
					} else if (quotaAmount > amt && "2".equals(ctlType)) {
						result.put("errorCode", "ERROR");
						result.put("errorMesg", "外汇单笔下线不足,单笔下线限额" + df.format(quotaAmount) + "本次交易" + df.format(amt));
						break;
					} else if (availAmount < amt && "1".equals(ctlType)) {
						result.put("errorCode", "WARN");
						result.put("errorMesg", "外汇超过单笔限额,单笔上线限额" + df.format(availAmount) + "本次交易" + df.format(amt));
					} else if (availAmount < amt && "2".equals(ctlType)) {
						result.put("errorCode", "ERROR");
						result.put("errorMesg", "外汇超过单笔限额,单笔上线限额" + df.format(availAmount) + "本次交易" + df.format(amt));
						break;
					} else {
						result.put("errorCode", "SUCCESS");
						result.put("errorMesg", "交易成功");
					}
					// ifslimittemplatemapper.insertIntoTemplateDetail(map);
				}

				if ("2".equals(limitType) || limitType == null) {
					if ("1".equals(ctlType)) {// 监测
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
						}
						// 增加限额
						ifslimittemplatemapper.updateOccupyLimit(map);
						ifslimittemplatemapper.insertIntoTemplateDetail(map);
					} else {
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计限额不足,累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
							// 增加限额
							// ifslimittemplatemapper.updateOccupyLimit(map);
							ifslimittemplatemapper.insertIntoTemplateDetail(map);
						}
					}
				} else if ("3".equals(limitType)) {
					if ("1".equals(ctlType)) {// 监测
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
						}
						// 增加限额
						ifslimittemplatemapper.updateOccupyLimit(map);
						ifslimittemplatemapper.insertIntoTemplateDetail(map);

					} else {
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计限额不足,累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
							// 增加限额
							// ifslimittemplatemapper.updateOccupyLimit(map);
							ifslimittemplatemapper.insertIntoTemplateDetail(map);
						}
					}
				} else if ("4".equals(limitType)) {
					if ("1".equals(ctlType)) {// 监测
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
						}

						// 增加限额
						ifslimittemplatemapper.updateOccupyLimit(map);
						ifslimittemplatemapper.insertIntoTemplateDetail(map);

					} else {
						double limitCount = Double.parseDouble(ifsLimitTemplate.get(i).getLimitCount());// 需要限额的交易笔数
						double limitCountSum = Double.parseDouble(ifsLimitTemplate.get(i).getLimitCountSum()) + 1;// 已经交易的笔数
						if (limitCount != 0) {
							if (limitCountSum > limitCount) {
								result.put("errorCode", "ERROR");
								result.put("errorMesg", "累计笔数超过限额,累计笔数" + limitCountSum);
								break;
							}
						}
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计限额不足,累计下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计超过限额,累计上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
							// 增加限额
							// ifslimittemplatemapper.updateOccupyLimit(map);
							ifslimittemplatemapper.insertIntoTemplateDetail(map);
						}
					}
				}
			}
		} else if ("0".equals(code)) {// 债券
			// String bondProperties = ParameterUtil.getString(map,
			// "bondProperties", "");//信用债

			//保存债券明细,2018.12.18注释，报表数据全部取opics中
			/*
			String investLimit = ParameterUtil.getString(map, "amt", "");//投资限额
			String bondCode = ParameterUtil.getString(map, "bondCode", "");//债券代码
			String invtype = ParameterUtil.getString(map, "invtype", "");//账户性质 H A T
			
			String bondProperties = ParameterUtil.getString(map, "bondProperties", "");//债券性质，存单还是债券
			String bondFlag = "1";//债券类型，1债券投资，2存单投资
			if("SE-CD,".contains(bondProperties)){
				bondFlag = "2";
			}
			
			String creditInvestLimit ="0";
			IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bondCode);
			int resultBond = 14;
			String bondRating = "14";//默认主体评级：无评级
			String lowLevelLimit = "0";//信用债AA级及以下债券余额
			String lowLevelLimitL = "0";//信用债AA-级及以下债券余额
			
			if("SE-QYZ,SE-GSZ,SE-ZP,SE-DR,SE-CDR,SE-DXGJ,".contains(bondProperties)){//信用债限额
				creditInvestLimit = investLimit;
				
				if(ifsOpicsBond!=null){
					bondRating=ifsOpicsBond.getBondrating()==null?"":ifsOpicsBond.getBondrating();
				}
				if(!"".equals(bondRating)){
					resultBond = Integer.parseInt(bondRating);
				}
				
				if("T".equals(invtype)){//A类型账户账户类型，信用债等级为AA级及以下，HT类信用债为AA-级及以下
					if(resultBond > 3){
						lowLevelLimit = investLimit;
					}
				}else{
					if(resultBond > 4){
						lowLevelLimitL = investLimit;
					}
				}
			}
			Map<String, Object> marketMap = new HashMap<String, Object>();
			//marketMap.put("costCent", ParameterUtil.getString(map, "cost", ""));
			String codeType = "";
			if(invtype.equals("A")){
				codeType = "SEN_AFS";
			}else if(invtype.equals("T")){
				codeType = "SEN_TB";
			}
			marketMap.put("code", codeType);
			String duration = "0";
			String convexity = "0";
			String delta = "0";
			try {
				SlCommonBean slCommonBean = new SlCommonBean();
				slCommonBean= iBaseServer.queryMarketData(marketMap);
				if(slCommonBean != null){
					duration = slCommonBean.getDuration();//久期
					convexity = slCommonBean.getConvexity();//止损
					delta = slCommonBean.getDelta();//DV01
				}
			} catch (RemoteConnectFailureException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String oprTime = ifsOpicsSettleManageMapper.getCurDate();
			String LastOprTime = nDaysAfterOneDateString(oprTime,-1);//获取昨日日期
			String lastMonthDay = oprTime.substring(0,8)+"01";
			lastMonthDay = nDaysAfterOneDateString(lastMonthDay,-1);
			String lastMonth = lastMonthDay.substring(0,7);
			Map<String, Object> mapBondQry = new HashMap<String, Object>();
			mapBondQry.put("LastOprTime", LastOprTime);//昨日日期
			mapBondQry.put("lastMonth", lastMonth);//上一个月的日期
			mapBondQry.put("invtype", invtype);//操作类型
			String convexityMonth = "0";
			String monthZ = bondLimitTemplateMapper.qryLastMonthLimit(mapBondQry);
			if("".equals(monthZ) || null == monthZ){
				convexityMonth = convexity;
			}else{
				convexityMonth = Double.parseDouble(convexity)-Double.parseDouble(monthZ)+"";
			}
			
			
			if("1".equals(bondFlag)){//债券类型
				if("A".equals(invtype)){//A类账户
					lowLevelLimitL = "";
				}else if("T".equals(invtype)){//T类账户
					lowLevelLimit = "";
				}else if("H".equals(invtype)){//H类账户
					delta = "";
					convexity = "";
					convexityMonth = "";
					lowLevelLimit = "";
				}
			}else{//存单
				if("A".equals(invtype)){//A类账户
					lowLevelLimitL = "";
					creditInvestLimit = "";
				}else if("T".equals(invtype)){//T类账户
					lowLevelLimit = "";
					creditInvestLimit = "";
				}else if("H".equals(invtype)){//H类账户
					delta = "";
					convexity = "";
					convexityMonth = "";
					creditInvestLimit = "";
					lowLevelLimit = "";
				}
			}
			
			Map<String, Object> bondLimitMap = new HashMap<String, Object>();
			bondLimitMap.put("invtype", invtype);//账户类型 A T H
			bondLimitMap.put("creditInvestLimit", creditInvestLimit);//信用债金额
			bondLimitMap.put("investLimit", investLimit);//投资金额
			bondLimitMap.put("lowLevelLimit", lowLevelLimit);//AA级及以下金额
			bondLimitMap.put("lowLevelLimitL", lowLevelLimitL);//AA级及以下金额
			bondLimitMap.put("duration", duration);//久期限额
			bondLimitMap.put("convexityYear", convexity);//止损年
			bondLimitMap.put("convexityMonth", convexityMonth);//止损月
			bondLimitMap.put("dv01", delta);//DV01
			bondLimitMap.put("bondFlag", bondFlag);//债券类型
			bondLimitMap.put("todayFlag","0");//0是明细，1是总计
			bondLimitMap.put("oprTime", ifsOpicsSettleManageMapper.getCurDate());//当前时间
			bondLimitMap.put("ticketId", ParameterUtil.getString(map, "ticketId", ""));
			
			//加入到报表明细
			bondLimitTemplateMapper.insertBondInvestLimit(bondLimitMap);*/
			
			
			List<BondTemplate> bondTemplate = bondLimitTemplateMapper.queryOccupyBond(map);// 查询债券

			for (int i = 0; i < bondTemplate.size(); i++) {

				String ctlType = bondTemplate.get(i).getCtlType();// 1监测，2控制
				double availAmount = Double.parseDouble(bondTemplate.get(i).getAvailAmount());// 额度上线
				double quotaAmount = Double.parseDouble(bondTemplate.get(i).getQuotaAmount());// 额度下线
				double totalAmount = Double.parseDouble(bondTemplate.get(i).getTotalAmount());// 已经使用的额度，累计
				String limitType = bondTemplate.get(i).getLimitType();// 1 单笔,
																	  // 2累计
				String bondId = bondTemplate.get(i).getBondId();// 业务流水号
				map.put("bondId", bondId);

				double sumAmount = totalAmount + amt;// 已经做过的交易加新做交易
				map.put("totalAmount", sumAmount);
				map.put("amt", amt);
				map.put("ctlType", ctlType);
				map.put("limitType", limitType);

				if ("1".equals(limitType) || limitType == null) {
					if (quotaAmount > amt && "1".equals(ctlType)) {
						result.put("errorCode", "WARN");
						result.put("errorMesg", "单笔累计限额不足,单笔下线限额" + df.format(quotaAmount) + "本次交易" + df.format(amt));
					} else if (quotaAmount > amt && "2".equals(ctlType)) {
						result.put("errorCode", "ERROR");
						result.put("errorMesg", "单笔限额不足,单笔下线限额" + df.format(quotaAmount) + "本次交易" + df.format(amt));
						break;
					} else if (availAmount < amt && "2".equals(ctlType)) {
						result.put("errorCode", "ERROR");
						result.put("errorMesg", "超过单笔限额,单笔上线限额" + df.format(availAmount) + "本次交易" + df.format(amt));
						break;
					} else {
						result.put("errorCode", "SUCCESS");
						result.put("errorMesg", "交易成功");
					}
					// bondLimitTemplateMapper.insertIntoBondDetail(map);
				}

				if ("2".equals(limitType) || limitType == null) {
					if ("1".equals(ctlType)) {// 监测
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计下线限额不足,下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "WARN");
							result.put("errorMesg", "累计超过限额,上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
						}
						// 增加限额
						// bondLimitTemplateMapper.updateOccupyBond(map);
						bondLimitTemplateMapper.insertIntoBondDetail(map);
					} else {
						if (quotaAmount > sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计限额不足,下线限额" + df.format(quotaAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else if (availAmount < sumAmount) {
							result.put("errorCode", "ERROR");
							result.put("errorMesg", "累计超过限额,上线线限额" + df.format(availAmount) + "累计交易" + df.format(sumAmount));
							break;
						} else {
							result.put("errorCode", "SUCCESS");
							result.put("errorMesg", "交易成功");
							// 增加限额
							// bondLimitTemplateMapper.updateOccupyBond(map);
							bondLimitTemplateMapper.insertIntoBondDetail(map);
						}
					}
				}
			}
		}
		return result;

	}

	@Override
	public List<IfsOpicsCost> searchCostList(Map<String, Object> params) {
		RowBounds rb = ParameterUtil.getRowBounds(params);
		return ifsopicscostmapper.searchPageOpicsCost(params, rb);
	}

	/**
	 * 取消占用限额
	 */
	@Override
	public Map<String, Object> revokeLimit(Map<String, Object> map) {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		map.put("flag", "1");
		int amt = Integer.parseInt(ParameterUtil.getString(map, "amt", ""));
		map.put("amt", -amt);
		Map<String, Object> result = new HashMap<String, Object>();
		String ticketId = ParameterUtil.getString(map, "ticketId", "");
		int count = bondLimitTemplateMapper.queryDetail(ticketId);
		if (count > 0) {
			// bondLimitTemplateMapper.updateFlag(ticketId);
			bondLimitTemplateMapper.insertLimitDetail(map);
			result.put("errorCode", "SUCCESS");
			result.put("errorMesg", "交易成功");
		} else {
			result.put("errorCode", "WARN");
			result.put("errorMesg", "未查到该笔业务的占用限额信息");
		}
		return result;
	}

	/**
	 * 查询限额
	 */
	@Override
	public double queryLimit(Map<String, Object> map) {
		map.put("operateUser", SlSessionHelper.getUserId());
		map.put("itime", DateUtil.getCurrentDateAsString());
		map.put("flag", "1");
		String code = ParameterUtil.getString(map, "code", "");
		double amt = 0;
		if ("1".equals(code)) {
			List<ifsLimitTemplate> ifsLimitTemplate = ifslimittemplatemapper.queryOccupyLimit(map);
			for (int i = 0; i < ifsLimitTemplate.size(); i++) {
				double count = bondLimitTemplateMapper.queryCount(map);
				amt = Double.parseDouble(ifsLimitTemplate.get(i).getQuotaAmount()) - count;
			}
		} else if ("0".equals(code)) {
			List<BondTemplate> bondTemplate = bondLimitTemplateMapper.queryOccupyBond(map);
			for (int i = 0; i < bondTemplate.size(); i++) {
				// int count = bondLimitTemplateMapper.queryCount(map);
				// Integer.parseInt(bondTemplate.get(i).getQuotaAmount())-count;
				double count = bondLimitTemplateMapper.queryCount(map);
				amt = Double.parseDouble(bondTemplate.get(i).getQuotaAmount()) - count;
			}

		}
		return amt;
	}

	/**
	 * 插入产品属性信息表
	 */
	@Override
	public void insertCredit(Map<String, Object> map) {
		ifslimittemplatemapper.insertCredit(map);
	}

	/**
	 * 查询所有审批件编号
	 */
	@Override
	public Page<PdAttrInfoArray> searchAllProd(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<PdAttrInfoArray> result = ifslimittemplatemapper.searchAllProd(map, rb);
		return result;
	}

	/**
	 * 查询是否有当前审批件编号的数据
	 */
	@Override
	public List<PdAttrInfoArray> searchProd(String approveFlNo) {
		List<PdAttrInfoArray> appList = ifslimittemplatemapper.searchProd(approveFlNo);
		return appList;
	}

	/**
	 * 插入业务种类信息表
	 */
	@Override
	public void insertBussType(Map<String, Object> map) {
		ifslimittemplatemapper.insertBussType(map);
		ifslimittemplatemapper.insertOperateDetail(map);
	}

	/**
	 * 对已经授信的主体加入到准入名单
	 */
	@Override
	public void insertLimitToAccess(Map<String, Object> map) {

		String nowDate = ifsOpicsSettleManageMapper.getCurDate();// 获取当日日期
		List<QuotaSyncopateRec> list = FastJsonUtil.parseArrays(map.get("EdCust").toString(), QuotaSyncopateRec.class);
		for (int i = 0; i < list.size(); i++) {
			String ecifNo = list.get(i).getEcifNum();
			String customer = list.get(i).getCustomer();
			String productType = list.get(i).getProductType();// 授信品种代码

			List<TcEdProd> prodList = new ArrayList<TcEdProd>();
			prodList = tcEdProdMapper.selectTcEdProdsByCreditId(productType);
			for (int j = 0; j < prodList.size(); j++) {
				String prdName = prodList.get(j).getPrdName();
				String prdNo = prodList.get(j).getProductCode();
				Map<String, Object> upmap = new HashMap<String, Object>();
				upmap.put("cORE_CLNT_NO", ecifNo);// 客户号
				upmap.put("coreno", ecifNo);// 客户号
				upmap.put("cO_NM", customer);// 名字
				upmap.put("bUSS_TYPE", prdNo);// 业务类型
				upmap.put("bUSS_TP_NM", prdName);// 业务名称
				upmap.put("bRANCH_CODE", "");// 机构号，现在为空
				upmap.put("bRANCH_NAME", "");// 机构名称，现在空
				upmap.put("iNCL_SUB_FLG", "true");// 是否包含下级
				upmap.put("aPPROVAL_DATE", nowDate);// 今日时间
				List<SlIfsBean> bussList = ifslimittemplatemapper.queryBussInfo(upmap);
				if (bussList.size() == 0) { // 若无此条数据，则插入
					upmap.put("operator", SlSessionHelper.getUserId());
					upmap.put("oprTime", nowDate);
					upmap.put("oprType", "新增");
					ifslimittemplatemapper.insertBussType(upmap);
					ifslimittemplatemapper.insertOperateDetail(upmap);
				}
			}
		}
	}

	/**
	 * 插入业务种类信息表(黑名单)
	 */
	@Override
	public void insertBussTypeBk(Map<String, Object> map) {
		ifslimittemplatemapper.insertBussTypeBk(map);
	}

	/**
	 * 查询是否有当前业务种类的数据
	 */
	@Override
	public List<SlIfsBean> searchBuss(Map<String, Object> map) {
		List<SlIfsBean> appList = ifslimittemplatemapper.searchBuss(map);
		return appList;
	}

	/**
	 * 查询所有业务种类
	 */
	@Override
	public Page<BussVrtyInfoArray> searchAllBuss(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<BussVrtyInfoArray> result = ifslimittemplatemapper.searchAllBuss(map, rb);
		return result;
	}

	/**
	 * 查询所有业务种类
	 */
	@Override
	public Page<IfsTradeAccess> searchLocalDetail(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTradeAccess> result = ifslimittemplatemapper.searchLocalDetail(map, rb);
		return result;
	}

	/**
	 * 查询所有业务种类(黑名单)
	 */
	@Override
	public Page<BussVrtyInfoArray> searchAllBussBk(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<BussVrtyInfoArray> result = ifslimittemplatemapper.searchAllBussBk(map, rb);
		return result;
	}

	/**
	 * 根据客户号去更新某一条记录
	 */
	@Override
	public void updateByEcifNo(Map<String, Object> map) {
		ifslimittemplatemapper.updateByEcifNo(map);
		ifslimittemplatemapper.insertOperateDetail(map);
	}

	/**
	 * 根据客户号去更新某一条记录(黑名单)
	 */
	@Override
	public void updateByEcifNoBk(Map<String, Object> map) {
		ifslimittemplatemapper.updateByEcifNoBk(map);
	}

	/**
	 * 根据客户号去删除某一条记录
	 */
	@Override
	public void deleteByEcifNo(Map<String, Object> map) {
		ifslimittemplatemapper.insertOperateDetail(map);
		ifslimittemplatemapper.deleteByEcifNo(map);
	}

	/**
	 * 根据客户号去删除某一条记录(黑名单)
	 */
	@Override
	public void deleteByEcifNoBk(Map<String, Object> map) {
		ifslimittemplatemapper.deleteByEcifNoBk(map);
	}

	/**
	 * 客户准入 先查询，后插入
	 */
	@Override
	public String doBussExcelPropertyToDataBase(List<IfsTradeAccess> slIfsBean) {
		int seccess = 0;
		int defeat = 0;
		for (int i = 0; i < slIfsBean.size(); i++) {
			String ecifno = slIfsBean.get(i).getCoreClntNo();
			String bussTpNm = slIfsBean.get(i).getBussTpnm();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ecifno", ecifno);
			map.put("bussTpNm", bussTpNm);
			if (ecifno != "") {
				if (ifslimittemplatemapper.searchBuss(map).size() == 0) { // 判断表中是否有同样数据
					// 若表中没有，则插入
					ifslimittemplatemapper.insertIntoAccess(slIfsBean.get(i));
					seccess = seccess + 1;
				} else {
					defeat = defeat + 1;
				}
			} else {
				defeat = defeat + 1;
			}
		}
		if (defeat > 0) {
			return "插入失败" + defeat + "条(该同业客户已存在或没有输入客户信息)";
		}
		return "插入成功" + seccess + "条";
	}

	/**
	 * 根据审批件编号去更新某一条记录
	 */
	@Override
	public void updateByAppNo(Map<String, Object> map) {
		ifslimittemplatemapper.updateByAppNo(map);
	}

	/**
	 * 根据审批件编号去删除某一条记录
	 */
	@Override
	public void deleteByAppNo(Map<String, Object> map) {
		ifslimittemplatemapper.deleteByAppNo(map);
	}

	/**
	 * 风险审查 先查询，后插入
	 */
	@Override
	public String doSIngleExcelPropertyToDataBase(List<PdAttrInfoArray> pdAttrInfo) {
		int seccess = 0;
		int defeat = 0;
		for (int i = 0; i < pdAttrInfo.size(); i++) {
			String approveFlNo = pdAttrInfo.get(i).getAPPROVE_FL_NO();
			if (approveFlNo != "") {
				if (ifslimittemplatemapper.searchProd(approveFlNo).size() == 0) { // 判断表中是否有同样数据
					// 若表中没有，则插入
					ifslimittemplatemapper.insertIntoProd(pdAttrInfo.get(i));
					seccess = seccess + 1;
				} else {
					defeat = defeat + 1;
				}
			} else {
				defeat = defeat + 1;
			}
		}
		if (defeat > 0) {
			return "插入失败" + defeat + "条(该信用风险已存在或没有输入风险审查信息)";
		}
		return "插入成功" + seccess + "条";
	}

	/**
	 * 同业准入 查询所有数据
	 */
	@Override
	public List<SlIfsBean> searchAllData() {
		List<SlIfsBean> list = ifslimittemplatemapper.searchAllData();
		return list;
	}

	/**
	 * 同业准入 查询本地数据
	 */
	@Override
	public Page<SlIfsBean> searchLocalInfo(Map<String, String> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlIfsBean> page = ifslimittemplatemapper.searchLocalInfo(map,rb);
		return page;
	}

	/**
	 * 同业准入 查询本地数据(黑名单)
	 */
	@Override
	public List<SlIfsBean> searchLocalInfoBk(Map<String, String> map) {
		List<SlIfsBean> list = ifslimittemplatemapper.searchLocalInfoBk(map);
		return list;
	}

	/**
	 * 风险审查 查询所有数据
	 */
	@Override
	public List<PdAttrInfoArray> searchAllDataRisk() {
		List<PdAttrInfoArray> list = ifslimittemplatemapper.searchAllDataRisk();
		return list;
	}

	/**
	 * 风险审查 查询本地数据
	 */
	@Override
	public List<PdAttrInfoArray> searchLocalData(Map<String, String> map) {
		List<PdAttrInfoArray> list = ifslimittemplatemapper.searchLocalData(map);
		return list;
	}

	@Override
	public Page<IfsLimitTemplateDetail> searchAllTemplateDetail(Map<String, Object> map) {
		return ifslimittemplatemapper.searchAllTemplateDetail(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<IfsLimitBondDetail> searchAllBondDetail(Map<String, Object> map) {
		return bondLimitTemplateMapper.searchAllBondDetail(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<SlIfsBean> queryBussInfo(Map<String, Object> map) {
		return ifslimittemplatemapper.queryBussInfo(map);
	}

	@Override
	public List<SlIfsBean> queryBussInfoBk(Map<String, Object> map) {
		return ifslimittemplatemapper.queryBussInfoBk(map);
	}

	@Override
	public Page<InvestLimtlog> getBondInvestmentLimit(Map<String, Object> map) {
		return bondLimitTemplateMapper.getBondInvestmentLimit(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<ForeignLimitLog> getForeignLimit(Map<String, Object> map) {
		return ifslimittemplatemapper.searchForeignLimitForPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public void updateForeignLimit() {
		String oprTime = ifsOpicsSettleManageMapper.getCurDate();
		Map<String,String> brpsMap=new HashMap<String, String>();
		brpsMap.put("br", "01");
		String LastOprTime = "";// 获取上一个跑批日期
		try {
			LastOprTime = acupServer.getAcupDate(brpsMap);
		} catch (RemoteConnectFailureException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}	
		//定时当日23点生成当日报表，23时已经跑批切日，当日日期赋值为上一个跑批日期,2018.12.18日修改
		if("".equals(LastOprTime)||null == LastOprTime){//如果上一个跑批日为空，赋值为当日，当日报表数据为空
			LastOprTime = oprTime;
		}else{
			oprTime = LastOprTime;
		}
		
		String lastMonthDay = oprTime.substring(0,8)+"01";
		lastMonthDay = nDaysAfterOneDateString(lastMonthDay,-1);
		String lastMonth = lastMonthDay.substring(0,7);
		String year = oprTime.substring(0, 4);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("oprTime", oprTime);//今日日期
		//map.put("LastOprTime", LastOprTime);//上一个跑批日期
		map.put("lastMonth", lastMonth);//上一个月的日期
		map.put("limitId", oprTime);
		map.put("year", year);
		
		BigDecimal FxdSum = new BigDecimal(0);//损益总合
		try {
			FxdSum = acupServer.queryFxdSum(map);//查询外汇损益总合，币种人民币
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", "USD");
			mapRate.put("br", "01");//机构号，送01
			BigDecimal dayRate =  acupServer.queryRate(mapRate);
			FxdSum = FxdSum.divide(dayRate,4,BigDecimal.ROUND_HALF_UP);
			
		} catch (RemoteConnectFailureException e) {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("totalYear", FxdSum+"");
		String totalMonth = ifslimittemplatemapper.getLastMonthSum(map);//获取上个月止损统计
		if("".equals(totalMonth) || null == totalMonth){
			totalMonth = FxdSum+"";
		}else{
			totalMonth = Double.parseDouble(FxdSum+"")-Double.parseDouble(totalMonth)+"";
		}
		map.put("totalMonth", totalMonth);
		
		//计算获取当日单笔交易限额最大值（区分方向）
		List<ForeignLimitLog> foreignLimitLog = new ArrayList<ForeignLimitLog>();
		foreignLimitLog = ifslimittemplatemapper.selectForeignLimitSingle(map);//单笔明细查询
		double usdLast = 0;
		double eurLast = 0;
		double jpyLast = 0;
		double hkdLast = 0;
		double gbpLast = 0;
		for(int i=0;i<foreignLimitLog.size();i++){
			double usd = Double.parseDouble(foreignLimitLog.get(i).getUsd());
			double eur = Double.parseDouble(foreignLimitLog.get(i).getEur());
			double jpy = Double.parseDouble(foreignLimitLog.get(i).getJpy());
			double hkd = Double.parseDouble(foreignLimitLog.get(i).getHkd());
			double gbp = Double.parseDouble(foreignLimitLog.get(i).getGbp());
			if(Math.abs(usdLast)<Math.abs(usd)){
				usdLast = usd;
			}
			if(Math.abs(eurLast)<Math.abs(eur)){
				eurLast = eur;
			}
			if(Math.abs(jpyLast)<Math.abs(jpy)){
				jpyLast = jpy;
			}
			if(Math.abs(hkdLast)<Math.abs(hkd)){
				hkdLast = hkd;
			}
			if(Math.abs(gbpLast)<Math.abs(gbp)){
				gbpLast = gbp;
			}
		}
		double total = usdLast + eurLast + jpyLast + hkdLast + gbpLast;
		map.put("usd", usdLast+"");
		map.put("eur", eurLast+"");
		map.put("jpy", jpyLast+"");
		map.put("hkd", hkdLast+"");
		map.put("gbp", gbpLast+"");
		map.put("total", total+"");
		
		ifslimittemplatemapper.deleteExicstForeignLimit(oprTime);//删除当日已存在的外汇报表信息
		ifslimittemplatemapper.insertForeignLimitLastdaySum(map);//隔夜敞口增加
		ifslimittemplatemapper.insertForeignLimitSingle(map);//单笔明细增加
		ifslimittemplatemapper.insertForeignLimitDaySum(map);//日间敞口增加
		
		ifslimittemplatemapper.insertForeignLimitYearSum(map);//年度止损增加
		ifslimittemplatemapper.insertForeignLimitMonthSum(map);//月度止损增加
	}
	
	@Override
	public void updateBondLimit() {
		String oprDate = ifsOpicsSettleManageMapper.getCurDate();
		for(int j=1;j<3;j++){
			String bondFlag = j + "";//属于债券还是存单1，债券，2存单
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprDate", oprDate);
			map.put("bondFlag", bondFlag);
			List<InvestLimtlog> bondList = new ArrayList<InvestLimtlog>();
			bondList = bondLimitTemplateMapper.getBondLimitForAddDayLimit(map);
			bondLimitTemplateMapper.deleteExistBond(map);
			
			for(int i=0;i<bondList.size();i++){
				Map<String, Object> bondMap = new HashMap<String, Object>();
				bondMap.put("invtype",bondList.get(i).getInvtype());
				bondMap.put("investLimit",bondList.get(i).getInvestLimit());
				bondMap.put("lowLevelLimit",bondList.get(i).getLowLevelLimit());
				bondMap.put("duration",bondList.get(i).getDuration());
				bondMap.put("dv01",bondList.get(i).getDv01());
				bondMap.put("convexityYear",bondList.get(i).getConvexityYear());
				bondMap.put("convexityMonth",bondList.get(i).getConvexityMonth());
				bondMap.put("bondFlag",bondFlag);
				bondMap.put("oprTime",oprDate);
				bondMap.put("creditInvestLimit",bondList.get(i).getCreditInvestLimit());
				bondMap.put("ticketId",bondList.get(i).getOprTime());
				bondMap.put("lowLevelLimitL",bondList.get(i).getNoUse1());
				bondMap.put("todayFlag","1");
				bondLimitTemplateMapper.insertBondInvestLimit(bondMap);
			}
		}
		
	}
	
	// 给定一个日期型字符串，返回加减n天后的日期型字符串
	public String nDaysAfterOneDateString(String basicDate, int n) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date tmpDate = null;
		try {
			tmpDate = df.parse(basicDate);
		} catch (Exception e) {
			// 日期型字符串格式错误
		}
		long nDay = (tmpDate.getTime() / (24 * 60 * 60 * 1000) + 1 + n) * (24 * 60 * 60 * 1000);
		tmpDate.setTime(nDay);
		return df.format(tmpDate);
	}
}