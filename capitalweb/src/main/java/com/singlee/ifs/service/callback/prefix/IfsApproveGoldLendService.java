package com.singlee.ifs.service.callback.prefix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.ifs.mapper.IfsApprovegoldLendMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsApprovegoldLend;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 黄金拆借【事前审批】回调函数-【暂未使用】
 * 
 * ClassName: IfsApproveGoldLendService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午08:16:52 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */
@Deprecated
@Service(value = "IfsApproveGoldLendService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApproveGoldLendService extends IfsApproveServiceBase {

	@Autowired
	IfsApprovegoldLendMapper ifsCfetgoldLendMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetgoldLendMapper.updateGoldLendByID(serial_no, status, date);
		IfsApprovegoldLend ifsApprovegoldLend = ifsCfetgoldLendMapper.searchGoldLend(serial_no);
		IfsFlowMonitor ifsFlowMonitor11 = new IfsFlowMonitor();
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			ifsFlowMonitor11 = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (ifsFlowMonitor11 == null) {
				IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
				ifsFlowMonitor.setTicketId(ifsApprovegoldLend.getTicketId());// 设置监控表-内部id
				ifsFlowMonitor.setContractId(ifsApprovegoldLend.getTicketId());// 设置监控表-成交单编号
				ifsFlowMonitor.setApproveStatus(ifsApprovegoldLend.getApproveStatus());// 设置监控表-审批状态
				ifsFlowMonitor.setPrdName("黄金拆借");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("goldLend");// 设置监控表-产品编号：作为分类
				ifsFlowMonitor.setTradeDate(ifsApprovegoldLend.getPostDate());// 设置监控表-交易日期
				ifsFlowMonitor.setSponsor(ifsApprovegoldLend.getSponsor());// 设置监控表-审批发起人
				ifsFlowMonitor.setTradeType("0");// 交易类型，0：事前交易，1：正式交易
				String custNo = ifsApprovegoldLend.getCounterpartyInstId();
				if (null != custNo || "".equals(custNo)) {
					ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
					IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
					ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)
				}
				StringBuffer buffer = new StringBuffer("");
				List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("IFS_APPROVEGOLD_LEND");
				if (listmap.size() == 1) {
					LinkedHashMap<String, String> hashmap = listmap.get(0);
					String tradeElements = hashmap.get("TRADE_GROUP");
					String commentsGroup = hashmap.get("COMMENTSGROUPS");
					if (tradeElements != "null" || tradeElements != "") {
						buffer.append("{");
						String sqll = "select " + tradeElements + " from IFS_APPROVEGOLD_LEND where ticket_id=" + "'" + serial_no + "'";
						LinkedHashMap<String, Object> swap = ifsCfetgoldLendMapper.searchProperty(sqll);
						String[] str = tradeElements.split(",");
						String[] comments = commentsGroup.split(",");
						for (int j = 0; j < str.length; j++) {
							String kv = str[j];
							String key = comments[j];
							String value = String.valueOf(swap.get(kv));
							buffer.append(key).append(":").append(value);
							if (j != str.length - 1) {
								buffer.append(",");
							}
						}
						buffer.append("}");
					}
				}
				ifsFlowMonitor.setTradeElements(buffer.toString());
				ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			}
		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)
				|| StringUtils.equals(ApproveOrderStatus.New, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map1);

		}
	}

}
