package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsLimitTotal;

import java.util.Map;

public interface IfsLimitTotalService {
    /**
     * @title 分页查询
     * @description 
     * @author  Luozb
     * @updateTime 2021/9/9 0009 16:36 
     * @throws 
     */
    Page<IfsLimitTotal> searchPageLimit(Map<String, Object> map);

    void deleteByLimitId(String limitId);

    void insert(IfsLimitTotal entity);

    void update(IfsLimitTotal entity);
}
