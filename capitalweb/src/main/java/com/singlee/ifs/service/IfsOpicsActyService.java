package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsActy;

import java.util.List;
import java.util.Map;

public interface IfsOpicsActyService {
	// 新增
	public void insert(IfsOpicsActy entity);

	// 修改
	void updateById(IfsOpicsActy entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsActy> searchPageOpicsActy(Map<String, Object> map);

	// 根据ID查询实体类
	IfsOpicsActy searchById(String id);

	// 更新同步状态
	void updateStatus(IfsOpicsActy entity);

	// 批量校准
	SlOutBean batchCalibration(String type,String[] acctngtypes);
	
	//查询所有
	Page<IfsOpicsActy> searchAllOpicsActy();

	List<IfsOpicsActy> actyList(Map<String, Object> map);
}