package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsSico;

import java.util.List;
import java.util.Map;

/***
 * SIC码
 * @author lij
 *
 */
public interface IfsOpicsSicoService {
	
	//新增
	public void insert(IfsOpicsSico entity);
	
	//修改
	void updateById(IfsOpicsSico entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsSico> searchPageOpicsSico(Map<String,Object> map);
	
	//根据id查询实体类
	IfsOpicsSico searchById(String id);
	
	//更新同步状态
	void updateStatus(IfsOpicsSico entity);
	
	//批量校准
	public SlOutBean batchCalibration(String type,String[] sics);

	// 查询所有
	List<IfsOpicsSico> searchAllOpicsSico();

	// 查询所有(下拉框)
	List<IfsOpicsSico> searchAllOpicsSicoDict();
}