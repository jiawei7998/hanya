package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.base.cal.InterestRange;
import com.singlee.capital.base.cal.PrincipalInterval;
import com.singlee.capital.cashflow.mapper.TdRateRangeMapper;
import com.singlee.capital.cashflow.model.TdRateRange;
import com.singlee.capital.cashflow.service.CalCashFlowService;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.mapper.IfsApprovermbDepositMapper;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import com.singlee.ifs.service.IfsApprovermbDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 同业存款
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApprovermbDepositServiceImpl implements IfsApprovermbDepositService {
    @Autowired
    IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
    @Resource
    IFSMapper ifsMapper;
    @Autowired
    TdRateRangeMapper tdRateRangeMapper;
    @Autowired
    CalCashFlowService calCashFlowService;

    @Override
    public void deleteDeposit(IfsApprovermbDeposit key) {
        ifsApprovermbDepositMapper.deleteByPrimaryKey(key.getTicketId());
        //ifsApprovermbDepositMapper.delete(key);
        deleteCashFlow(key);
    }



    @Override
    public Page<IfsApprovermbDeposit> getRmbDepositMinePage(Map<String, Object> params, int isFinished) {
        Page<IfsApprovermbDeposit> page = new Page<IfsApprovermbDeposit>();
        if (isFinished == 1) {// 待审批
            page = ifsApprovermbDepositMapper.getRmbDepositMineList(params, ParameterUtil.getRowBounds(params));
        } else if (isFinished == 2) {// 已审批
            page = ifsApprovermbDepositMapper.getRmbDepositMineListFinished(params, ParameterUtil.getRowBounds(params));
        } else {// 我发起的
            page = ifsApprovermbDepositMapper.getRmbDepositMineListMine(params, ParameterUtil.getRowBounds(params));
        }
        return page;
    }

    @Override
    public IfsApprovermbDeposit getDepositById(IfsApprovermbDeposit deposit) {
        return ifsApprovermbDepositMapper.selectByPrimaryKey(deposit);
    }

    @Override
    public IfsApprovermbDeposit getDepositById(Map<String, Object> params) {
        return ifsApprovermbDepositMapper.searchIfsApprovermbDeposit(params);
    }

    @Override
    public String insertDeposit(IfsApprovermbDeposit deposit) {
        deposit.setTicketId(getTicketId("IBD"));
        deposit.setApproveStatus(DictConstants.ApproveStatus.New);
        deposit.setInputTime(DateUtil.getCurrentDateTimeAsString());
        deposit.setLastTime(deposit.getInputTime());
        String str = "";
        try {
            ifsApprovermbDepositMapper.insert(deposit);
            //创建现金流
            generateCashFlow(deposit);
            str = "保存成功";
        } catch (Exception e) {
            e.printStackTrace();
            str = "保存失败";
        }
        return str;
    }

    /**
     * 删除现金流
     * @param key
     */
    private void deleteCashFlow(IfsApprovermbDeposit key) {
        Map<String,Object> queryMap = new HashMap<>();
        queryMap.put("dealNo",key.getTicketId());
        queryMap.put("dealType",2);
        calCashFlowService.deleteAllCashFlowsForDeleteDeal(queryMap);
    }

    @Override
    public void updateDeposit(IfsApprovermbDeposit deposit) {
        deposit.setApproveStatus(DictConstants.ApproveStatus.New);
        deposit.setLastTime(DateUtil.getCurrentDateTimeAsString());
        ifsApprovermbDepositMapper.updateByPrimaryKeySelective(deposit);
        //新增现金流
        generateCashFlow(deposit);
    }

    /**
     * 生成现金流
     * @param deposit
     */
    @Override
    public void generateCashFlow(IfsApprovermbDeposit deposit) {
        List<PrincipalInterval> principalIntervals = new ArrayList<PrincipalInterval>();//本金变化区间
        List<InterestRange> interestRanges = new ArrayList<InterestRange>();//利率变化区间
        Map<String,Object> queryMap = new HashMap<>();

        String vDate = deposit.getFirstSettlementDate();//起息日
        String mDate = deposit.getSecondSettlementDate();//到期日
        BigDecimal amt = deposit.getAmt();//金额
        //double contactRate = deposit.getRate().doubleValue()*0.01;//利率
        double contactRate = deposit.getRate().multiply(new BigDecimal("0.01")).doubleValue();

        /**
         * 利率区间插入
         */
        queryMap.put("dealNo",deposit.getTicketId());
        tdRateRangeMapper.deleteRateRanges(queryMap);
        TdRateRange rate = new TdRateRange();
        rate.setDealNo(deposit.getTicketId());
        rate.setVersion(0);
        rate.setBeginDate(vDate);
        rate.setEndDate(mDate);
        rate.setExecRate(contactRate);
        tdRateRangeMapper.insert(rate);


        PrincipalInterval principalInterval = new PrincipalInterval();
        principalInterval.setStartDate(vDate);
        principalInterval.setEndDate(mDate);
        principalInterval.setResidual_Principal(amt.doubleValue());
        principalIntervals.add(principalInterval);

        InterestRange interestRange = new InterestRange();
        interestRange.setStartDate(vDate);
        interestRange.setEndDate(mDate);
        interestRange.setRate(contactRate);
        interestRanges.add(interestRange);


        Map<String,Object> map = new HashMap<>();

        map.put("dealNo",deposit.getTicketId());
        map.put("vDate",vDate);//起息日
        map.put("mDate",mDate);//到期日

        map.put("meDate",mDate);//到账日
        map.put("sDate",vDate);//首次付息日
        map.put("eDate",mDate);//最后一个付息日
        map.put("amt",amt.doubleValue());
        map.put("contractRate",contactRate);
        map.put("acturalRate",contactRate);
        map.put("dealType",2);
        map.put("version",0);
        map.put("prdNo",deposit.getProduct());
        map.put("intType","2");//后收息
        map.put("intFre",deposit.getIntFeq());//付息频率
        map.put("amtFre",deposit.getAmtFeq());//还本频率
        map.put("basis",deposit.getBasis());//计息基础
        calCashFlowService.createHrbCashFLow(principalIntervals, interestRanges, map);

    }

    public String getTicketId(String str) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("sStr", str);// 前缀
        hashMap.put("trdType", "2");// 贸易类型，正式交易
        // CSWAP20170831339000707
        String ticketId = ifsMapper.getTradeId(hashMap);
        return ticketId;
    }

    /**
     * 根据ticketId查询详情
     * @param ticketId
     * @return
     */
    public IfsApprovermbDeposit searchApproveRmbDepositByTicketId(String ticketId){
        return ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(ticketId);
    }
}
