package com.singlee.ifs.service.impl;

import com.singlee.capital.common.util.StringUtil;
import com.singlee.generated.model.field.*;
import com.singlee.ifs.mapper.IfsSwiftSettleInfoMapper;
import com.singlee.ifs.model.IfsSwiftSettleInfo;
import com.singlee.ifs.service.IfsSwiftCallBackService;
import com.singlee.swift.model.SwiftBlock2Output;
import com.singlee.swift.model.SwiftMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service("ifsSwiftAnalyzeServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsSwiftAnalyzeServiceImpl implements IfsSwiftCallBackService {

	@Autowired
	IfsSwiftSettleInfoMapper ifsSwiftSettleMapper;

	/**
	 * 解析报文 保存报文
	 * 
	 * @param list
	 */
	@Override
	public void swiftSave(InputStream in) {
		try {
			List<String> list = read(in);
			IfsSwiftSettleInfo SwiftSettleInfo = null;
			for (String swift : list) {
				SwiftSettleInfo = new IfsSwiftSettleInfo();
				SwiftMessage swiftMsg = SwiftMessage.parse(swift);
				SwiftSettleInfo.setSwiftStatus(swiftMsg.getBlock4().getTagValue(Field451.NAME));// ack报文状态
				swiftMsg = SwiftMessage.parse(swiftMsg.getUnparsedTexts().getText(0));
				if (swiftMsg.isOutput()) {// 判断是否取回报文
					SwiftBlock2Output out = (SwiftBlock2Output) swiftMsg.getBlock2();
					SwiftSettleInfo.setMsgtype("MT" + swiftMsg.getBlock2().getMessageType());// 报文类型
					SwiftSettleInfo.setReciverBank(swiftMsg.getReceiver());// 收报行
					SwiftSettleInfo.setSendBank(swiftMsg.getSender());// 发报行
					SwiftSettleInfo.setSwiftflag("O");// 报文 io标识
					SwiftSettleInfo.setSendDate(out.getReceiverOutputDate());// 收报行发报日期
					if (StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field20.NAME))) {
						SwiftSettleInfo.setTag20(swiftMsg.getBlock4().getTagValue(Field20.NAME));
					}
					if (StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field21.NAME))) {
						SwiftSettleInfo.setTag21(swiftMsg.getBlock4().getTagValue(Field21.NAME));
					}
					if (StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field32A.NAME))) {
						String curAmt = swiftMsg.getBlock4().getTagValue(Field32A.NAME);
						SwiftSettleInfo.setCcy(curAmt.substring(6, 9));
						if (curAmt.endsWith(",")) {
							SwiftSettleInfo.setAmount(curAmt.substring(9, curAmt.length() - 1));
						} else {
							SwiftSettleInfo.setAmount(curAmt.substring(9, curAmt.length()));
						}
					} else if (StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field32B.NAME))) {
						String curAmt = swiftMsg.getBlock4().getTagValue(Field32B.NAME);
						SwiftSettleInfo.setCcy(curAmt.substring(0, 3));
						if (curAmt.endsWith(",")) {
							SwiftSettleInfo.setAmount(curAmt.substring(3, curAmt.length() - 1));
						} else {
							SwiftSettleInfo.setAmount(curAmt.substring(3, curAmt.length()));
						}
					}
					// 保存解析报文信息
					ifsSwiftSettleMapper.insertSwiftSettle(SwiftSettleInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 读取文件 分隔报文
	 * 
	 * @param in
	 * @return
	 */
	public static List<String> read(InputStream in) {
		List<String> swiftList = new ArrayList<String>();
		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = reader.readLine()) != null) {// 读取行数据
				int position = line.indexOf('$');
				if (position >= 0) {
					buffer.append(line.substring(0, position));
					swiftList.add(buffer.toString()); // 报文以$分割 读取到完整报文
					buffer = new StringBuffer(line.substring(position + 1)); // 清空buffer,读取下个报文
					buffer.append("\r\n");
				} else {
					buffer.append(line).append("\r\n");
				}
			}
			// 最后一个报文
			String swift_str = buffer.toString();
			if (!StringUtil.isEmptyString(swift_str)) {
				swiftList.add(swift_str.substring(0, swift_str.length() - 1));
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return swiftList;
	}

	
	public static void main(String[] args) throws FileNotFoundException {
		String path = "H:/2021-04-19_085044886.txt";
		FileInputStream fileInputStream = new FileInputStream(path);
		new IfsSwiftAnalyzeServiceImpl().swiftSave(fileInputStream);
	}
}
