package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6770.RequestBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.MS6770Bean;
import com.singlee.financial.esb.hbcb.service.S003003990MS6770Service;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.model.IfsTrdSettle;
import com.singlee.ifs.service.IfsTrdSettleService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 大额经办管理
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsTrdSettleServiceImpl implements IfsTrdSettleService {

	@Autowired
	private IfsTrdSettleMapper trdsettlemapper;
	@Autowired
	private  IBaseServer baseServer;
	@Autowired
	private S003003990MS6770Service s003003990MS6770Service;
	@Autowired
	private TaSysParamMapper taSysParamMapper;
    @Autowired
    private IfsTrdSettleMapper ifsTrdSettleMapper;
 

	@Override
	public Page<IfsTrdSettle> getTrdSettleList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTrdSettle> result = trdsettlemapper.getTrdSettleList(map, rb);
		return result;
	}

	/**
	 * 交易经办
	 */
	@Override
	public RetMsg<Serializable> submitSettle(Map<String, Object> map) throws ParseException {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "经办成功");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dealno", map.get("dealno")); // opics编号
		param.put("fedealno", map.get("fedealno")); // 审批单编号
		// param.put("is_locked", true);//加锁查询记录,查询时将交易记录上锁 悲观所for update
		IfsTrdSettle trdSettle_1 = trdsettlemapper.queryTrdSettleByDealNo(param);

		/* 设置收款信息 */
		trdSettle_1.setRecBankid(String.valueOf(map.get("recBankid")));
		trdSettle_1.setRecBankname(String.valueOf(map.get("recBankname")));
		trdSettle_1.setRecUserid(String.valueOf(map.get("recUserid")));
		trdSettle_1.setRecUsername(String.valueOf(map.get("recUsername")));

		ret.setObj(trdSettle_1);
		if (StringUtils.isEmpty(trdSettle_1.getRecBankid()) || StringUtils.isEmpty(trdSettle_1.getRecBankname()) || StringUtils.isEmpty(trdSettle_1.getRecUserid())
				|| StringUtils.isEmpty(trdSettle_1.getRecUsername())) {
			ret.setCode("999999");
			ret.setDesc("您所需要确认的往账交易信息:<br>账号:" + trdSettle_1.getRecUserid() + "<br>户名:" + trdSettle_1.getRecUsername() + "<br>行号:" + trdSettle_1.getRecBankid()
					+ "<br>行名:" + trdSettle_1.getRecBankname() + "<br><html><font color=red>金额:" + trdSettle_1.getAmount().doubleValue() + "</font></html><br>清算信息空缺！");
			return ret;
		}

		if (!"0".equals(trdSettle_1.getDealflag()) && StringUtils.isNotEmpty(trdSettle_1.getIoper())) {
			ret.setCode("999999");
			ret.setDesc("您所确认的往账交易信息:<br>账号:" + trdSettle_1.getRecUserid() + "<br>户名:" + trdSettle_1.getRecUsername() + "<br>行号:" + trdSettle_1.getRecBankid()
					+ "<br>行名:" + trdSettle_1.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle_1.getAmount().abs().doubleValue()
					+ "</font></html><br>已经被: " + trdSettle_1.getIoper() + "经办处理！<br>经办时间: " + trdSettle_1.getItime());
			return ret;
		}
		trdSettle_1.setIoper(SlSessionHelper.getUserId());
		String itime = DateUtil.getCurrentDateTimeAsString();
		trdSettle_1.setItime(itime);
		trdSettle_1.setDealflag("1");//改为已经办
		trdSettle_1.setRemarkFy(map.get("remarkFy").toString());
		trdSettle_1.setHvpType(map.get("hvpType").toString());

		param = new HashMap<String, Object>();
		param.put("dealno", trdSettle_1.getDealno());
		param.put("fedealno",trdSettle_1.getFedealno()); // 审批单编号
		param.put("payBankid", map.get("payBankid"));
		param.put("payBankname", map.get("payBankname"));
		param.put("payUserid", map.get("payUserid"));
		param.put("payUsername", map.get("payUsername"));

		param.put("recBankid", map.get("recBankid"));
		param.put("recBankname", map.get("recBankname"));
		param.put("recUserid", map.get("recUserid"));
		param.put("recUsername", map.get("recUsername"));
		param.put("ioper", trdSettle_1.getIoper());
		param.put("itime", trdSettle_1.getItime());
		param.put("dealflag", trdSettle_1.getDealflag());
		param.put("remarkFy", trdSettle_1.getRemarkFy());
		param.put("hvpType", trdSettle_1.getHvpType());
		trdsettlemapper.updateTrdSettle(param);
		return ret;

	}

	@Override
	public IfsTrdSettle queryTrdSettleByDealNo(Map<String, Object> param) throws Exception {
		return trdsettlemapper.queryTrdSettleByDealNo(param);
	}

	@Override
	public RetMsg<Serializable> validSettleState(IfsTrdSettle td) throws Exception {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealno", td.getDealno());
		map.put("is_locked", true);
		IfsTrdSettle trdSettle = trdsettlemapper.queryTrdSettleByDealNo(map);
		ret.setObj(trdSettle);
		if (StringUtils.isBlank(trdSettle.getVoper()) || StringUtils.isBlank(trdSettle.getVtime())) {
			if (StringUtils.isBlank(trdSettle.getDealuser())) {
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("dealno", td.getDealno());
				param.put("fedealno", td.getFedealno());
				param.put("dealflag", td.getDealflag());
				param.put("voidflag", td.getVoidflag());
				param.put("settflag", td.getSettflag());
				param.put("dealuser", td.getDealuser());
				param.put("dealtime", td.getDealtime());
				if (trdsettlemapper.updateTrdSettle(param) > 0) {
					ret.setDesc("验证成功");
				} else {
					ret.setCode("999999");
					ret.setDesc("更新记录状态失败,请联系数据库管理员");
				}

			} else if (td.getDealuser().equals(trdSettle.getDealuser())) {
				ret.setDesc("验证成功");
			} else {
				ret.setCode("999999");
				ret.setDesc("<br><br><br>您所确认的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid()
						+ "<br>行名:" + trdSettle.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle.getAmount().abs().doubleValue()
						+ "</font></html><br>已有用户: " + trdSettle.getDealuser() + "正在处理！<br>处理时间: " + trdSettle.getDealtime());
			}
		} else if (StringUtils.isNotBlank(trdSettle.getVoper())) {
			ret.setCode("999999");
			ret.setDesc("<br><br><br>您所确认的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid()
					+ "<br>行名:" + trdSettle.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle.getAmount().abs().doubleValue() + "</font></html><br>已经被: "
					+ trdSettle.getDealuser() + "复核处理！<br>复核时间: " + trdSettle.getDealtime());
		}
		return ret;
	}

	@Override
	public int updateTrdSettle(Map<String, Object> param) throws Exception {
		return trdsettlemapper.updateTrdSettle(param);
	}

	@Override
	public RetMsg<Serializable> backSettle(HashMap<String, Object> paramMap) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dealno", paramMap.get("dealno")); // opics编号
		map.put("fedealno", paramMap.get("fedealno")); // 审批单编号
		map.put("is_locked", true);
		IfsTrdSettle trdSettle = trdsettlemapper.queryTrdSettleByDealNo(map);
		ret.setObj(trdSettle);
		if (StringUtils.isNotBlank(trdSettle.getDealuser()) && StringUtils.isBlank(trdSettle.getVoper())) {
			ret.setCode("999999");
			ret.setDesc("您所确认的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid() + "<br>行名:"
					+ trdSettle.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle.getAmount().abs().doubleValue() + "</font></html><br>已有用户: "
					+ trdSettle.getDealuser() + "正在处理！<br>处理时间: " + trdSettle.getDealtime());
			return ret;
		}
		if (StringUtils.isNotBlank(trdSettle.getVoper()) && "0".equals(trdSettle.getDealflag()))// 复核人不为空,该交易已经被退回
		{
			ret.setCode("999999");
			ret.setDesc("您所确认的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid() + "<br>行名:"
					+ trdSettle.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle.getAmount().abs().doubleValue() + "</font></html><br>已经被: "
					+ trdSettle.getVoper() + "退回处理！<br>退回时间: " + trdSettle.getCtime());
			return ret;
		}
		if (StringUtils.isNotBlank(trdSettle.getVoper()) && "PR07".equals(trdSettle.getSettflag()))// 复核人不为空,该交易已经被复核
		{
			ret.setCode("999999");
			ret.setDesc("您所确认的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid() + "<br>行名:"
					+ trdSettle.getRecBankname() + "<br><html><font color=red>金额: " + trdSettle.getAmount().abs().doubleValue() + "</font></html><br>已经被: "
					+ trdSettle.getVoper() + "签发处理！<br>签发时间: " + trdSettle.getVtime());
			return ret;
		}

		trdSettle.setDealflag("0");//改为未经办
		trdSettle.setVoper(SlSessionHelper.getUserId());
		// trdSettle.setCtime(DateUtil.getCurrentCompactDateTimeAsString());

		paramMap = new HashMap<String, Object>();
		paramMap.put("dealno", trdSettle.getDealno());
		paramMap.put("fedealno",trdSettle.getFedealno()); // 审批单编号
		paramMap.put("dealflag", trdSettle.getDealflag());
		paramMap.put("voper", trdSettle.getVoper());
		// paramMap.put("ctime", trdSettle.getCtime());
		if (trdsettlemapper.updateTrdSettle(paramMap) > 0) {
			ret.setCode(RetMsgHelper.codeOk);
			ret.setDesc("您所确认签发的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid() + "<br>行名:"
					+ trdSettle.getRecBankname() + "<br><html><font color=red>金额:" + trdSettle.getAmount().abs().doubleValue() + "</font></html><br>退回处理成功！");
			return ret;
		} else {
			ret.setCode("999999");
			ret.setDesc("您所确认签发的往账交易信息:<br>账号:" + trdSettle.getRecUserid() + "<br>户名:" + trdSettle.getRecUsername() + "<br>行号:" + trdSettle.getRecBankid() + "<br>行名:"
					+ trdSettle.getRecBankname() + "<br><html><font color=red>金额:" + trdSettle.getAmount().abs().doubleValue()
					+ "</font></html><br>退回失败,原因:更新数据库状态失败,请联系数据库管理员");
			return ret;
		}
	}

	@Override
	public Page<IfsTrdSettle> getTrdSettleCount(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTrdSettle> result = trdsettlemapper.getTrdSettleCount(map, rb);
		return result;
	}

	@Override
	public IfsTrdSettle getSettleCnaps(String fedealno) {
		return trdsettlemapper.getSettleCnaps(fedealno);
	}

	//分页查询 10-19
	@Override
	public Page<IfsTrdSettle> queryAllTrdSettle(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTrdSettle> result =trdsettlemapper.queryAllTrdSettle(map,rb);
		return result;
	}

	//分页查询 10-19
	@Override
	public Page<IfsTrdSettle> queryRMBTrdSettle(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTrdSettle> result =trdsettlemapper.queryRMBTrdSettle(map,rb);
		return result;
	}

	//分页查询 10-19
	@Override
	public Page<IfsTrdSettle> queryFXTrdSettle(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTrdSettle> result =trdsettlemapper.queryFXTrdSettle(map,rb);
		return result;
	}

	//超时查询
	
    @Override
    public Boolean sendCheck(Map<String, Object> param) throws RemoteConnectFailureException, Exception {
     // 获取当前账务日期
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- 二代】，总账超时查询，开始······");
        Date postdates = baseServer.getOpicsSysDate("01");
        String strSendDate = DateUtil.format(postdates);
   
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 二代】，总账超时查询，总账日期："+strSendDate+"······");
            


        Map<String, Object> sysMap = new HashMap<String, Object>();
        sysMap.put("p_type", "ESB");
        List<TaSysParam> EsbList = taSysParamMapper.selectTaSysParam(sysMap);
        sysMap.put("p_type", "MS6770");
        List<TaSysParam> MS6770List = taSysParamMapper.selectTaSysParam(sysMap);
        EsbList.addAll(MS6770List);
        MS6770Bean mS6770Bean = new MS6770Bean();
        RequestBody requestBody = new RequestBody();
        RequestHeader requestHeader = new RequestHeader();
        ReqMsgHead reqMsgHead=new ReqMsgHead();
        // 初始化数据
        GenerateMS6770Bean(requestHeader, EsbList,reqMsgHead);
    
       // param传Sn
         IfsTrdSettle ifsTrdSettle=ifsTrdSettleMapper.getTrdSettleOne(param);
         
         if(ifsTrdSettle!=null) {

          String  dealnoString=  ifsTrdSettle.getSn();
         
          requestHeader.setReqTm(new SimpleDateFormat("yyyyMMdd").format(new Date()));//请求方交易时间戳
          requestHeader.setReqSeqNo(ifsTrdSettle.getDealno());//请求方流水号
          requestHeader.setBrchNo("00100");//机构号
        requestBody.setMatchmode("1");//匹配方式
        requestBody.setMsgdetailflow(dealnoString.trim());//支付平台流水号
        reqMsgHead.setChandt(ifsTrdSettle.getCdate().substring(0,10).replace("-", ""));//原渠道日期
        reqMsgHead.setChanflow(ifsTrdSettle.getDealno());//渠道流水号
        reqMsgHead.setBizdate(ifsTrdSettle.getCdate().substring(0,10).replace("-", ""));//交易日期
        reqMsgHead.setSbno("0" + ifsTrdSettle.getBr() + "00");// 交易发起机构号
        
        mS6770Bean.setRequestHeader(requestHeader);
        mS6770Bean.setReqMsgHead(reqMsgHead);
        mS6770Bean.setRequestBody(requestBody);
        EsbOutBean outBean = s003003990MS6770Service.send(mS6770Bean);
        //返回失败信息则返回false（说明该业务不存在，需要重发）
        if (!"0020010000".equals(outBean.getRetCode())||!"7".equals(outBean.getClientNo())) {
            
            return false;
        }
     
    
      }else {
             return false;
         }
        return true;
    }
    /**
     * 初始化请求报文
     * 
     * @param requestBody
     * @param requestHeader
     * @param sysList
     */
    public static void GenerateMS6770Bean(RequestHeader requestHeader,
            List<TaSysParam> sysList,ReqMsgHead reqMsgHead) {
        for (TaSysParam taSysParam : sysList) {if("ChnlNo".equals(taSysParam.getP_code())) {
            requestHeader.setChnlNo(taSysParam.getP_value().trim());//渠道号
            reqMsgHead.setChanid(taSysParam.getP_value().trim());//发起渠道
        }else if("TlrNo".equals(taSysParam.getP_code())) {
            requestHeader.setTlrNo(taSysParam.getP_value().trim());//柜员号
            reqMsgHead.setTlno(taSysParam.getP_value().trim());//受理柜员
        }
        
        }
        }
    
    
    
}