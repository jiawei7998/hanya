package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSApproveService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 用于外部接口数据操作，下行、上行接口操作等
 * 
 * @author Singlee
 * 
 */

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IFSApproveServiceImpl implements IFSApproveService {

	@Resource
	IFSMapper ifsMapper;
	@Resource
	IfsApprovefxCswapMapper ifsApprovefxCswapMapper;
	@Resource
	IfsApprovefxFwdMapper ifsApprovefxFwdMapper;
	@Resource
	IfsApprovefxLendMapper ifsApprovefxLendMapper;
	@Resource
	IfsApprovefxOnspotMapper ifsApprovefxOnspotMapper;
	@Resource
	IfsApprovefxOptionMapper ifsApprovefxOptionMapper;
	@Resource
	IfsApprovefxSptMapper ifsApprovefxSptMapper;
	@Resource
	IfsApprovefxSwapMapper ifsApprovefxSwapMapper;
	@Resource
	IfsApproveAllocateMapper ifsApproveAllocateMapper;
	@Resource
	IfsApprovefxDebtMapper ifsApprovefxDebtMapper;
	@Autowired
	IfsApprovegoldLendMapper ifsCfetgoldLendMapper;
	@Autowired
	private IfsApprovemetalGoldMapper cfetsmetalGoldMapper;
	@Autowired
	private IfsApprovermbCbtMapper cfetsrmbCbtMapper;

	
	@Autowired
	private IfsCfetsrmbDetailSlMapper detailSlMapper;
	@Autowired
	private IfsApprovermbCrMapper cfetsrmbCrMapper;
	@Autowired
	private IfsApprovermbIboMapper cfetsrmbIboMapper;
	@Autowired
	private IfsApprovermbIrsMapper cfetsrmbIrsMapper;
	@Autowired
	private IfsApprovermbOrMapper cfetsrmbOrMapper;
	@Autowired
	private IfsApprovermbSlMapper cfetsrmbSlMapper;
	
	

	@Override
	public IfsDownPaket getDownPaket(Map<String, Object> map) {
		return ifsMapper.getDownPaket(map);
	}

	@Override
	public List<IfsDownPaket> getDownPakets(Map<String, Object> map) {
		return ifsMapper.getDownPakets(map);
	}

	/*
	 * 货币掉期
	 */
	// 分页查询
	@Override
    public Page<IfsApprovefxCswap> searchCcySwapPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxCswap> result = ifsApprovefxCswapMapper.searchCcySwapPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcySwap(String ticketid) {
		ifsApprovefxCswapMapper.deleteCcySwap(ticketid);

	}

	@Override
	public void addCcySwap(IfsApprovefxCswap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("CSWAP"));
		ifsApprovefxCswapMapper.addCcySwap(map);
	}

	@Override
	public void editCcySwap(IfsApprovefxCswap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxCswapMapper.editCcySwap(map);
	}

	@Override
	public IfsApprovefxCswap searchCcySwap(String ticketid) {

		IfsApprovefxCswap IfsApprovefxCswap = ifsApprovefxCswapMapper.searchCcySwap(ticketid);
		return IfsApprovefxCswap;
	}
	
	@Override
	public IfsApprovefxCswap getCcySwapAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxCswap ccySwap=ifsApprovefxCswapMapper.selectFlowIdByPrimaryKey(key);
		return ccySwap;
	}

	
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxCswap> getCcySwapMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxCswap> page= new Page<IfsApprovefxCswap>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxCswapMapper.getCcySwapMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxCswapMapper.getCcySwapMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxCswapMapper.getCcySwapMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 外币拆借
	 */

	@Override
	public Page<IfsApprovefxLend> searchCcyLendingPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxLend> result = ifsApprovefxLendMapper.searchCcyLendingPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcyLending(String ticketid) {
		ifsApprovefxLendMapper.deleteCcyLending(ticketid);

	}

	@Override
	public void editCcyLending(IfsApprovefxLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxLendMapper.editCcyLending(map);

	}

	@Override
	public void addCcyLending(IfsApprovefxLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("LEND"));
		ifsApprovefxLendMapper.addCcyLending(map);
	}

	@Override
	public IfsApprovefxLend searchCcyLending(String ticketid) {

		IfsApprovefxLend IfsApprovefxLend = ifsApprovefxLendMapper.searchCcyLending(ticketid);
		return IfsApprovefxLend;
	}
	
	/***
	 * 根据请求参数查询分页列表-外币拆借-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxLend> getCcyLendingMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxLend> page= new Page<IfsApprovefxLend>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxLendMapper.getCcyLendingMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxLendMapper.getCcyLendingMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxLendMapper.getCcyLendingMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 外汇对即期
	 */

	@Override
	public Page<IfsApprovefxOnspot> searchCcyOnSpotPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxOnspot> result = ifsApprovefxOnspotMapper.searchCcyOnSpotPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcyOnSpot(String ticketid) {

		ifsApprovefxOnspotMapper.deleteCcyOnSpot(ticketid);
	}

	@Override
	public void addCcyOnSpot(IfsApprovefxOnspot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("SPOT"));
		ifsApprovefxOnspotMapper.addCcyOnSpot(map);
	}

	@Override
	public void editCcyOnSpot(IfsApprovefxOnspot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxOnspotMapper.editCcyOnSpot(map);

	}

	@Override
	public IfsApprovefxOnspot searchCcyOnSpot(String ticketid) {

		IfsApprovefxOnspot IfsApprovefxOnspot = ifsApprovefxOnspotMapper.searchCcyOnSpot(ticketid);
		return IfsApprovefxOnspot;
	}

	/***
	 * 根据请求参数查询分页列表-外汇对即期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxOnspot> getCcyOnSpotMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxOnspot> page= new Page<IfsApprovefxOnspot>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxOnspotMapper.getCcyOnspotMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxOnspotMapper.getCcyOnspotMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxOnspotMapper.getCcyOnspotMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 
	 * 人民币远期
	 */

	@Override
	public Page<IfsApprovefxFwd> getCreditEdit(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxFwd> result = ifsApprovefxFwdMapper.getCreditEdit(map, rowBounds);
		return result;
	}

	@Override
	public IfsApprovefxFwd getCredit(String ticketid) {
		IfsApprovefxFwd result = ifsApprovefxFwdMapper.getCredit(ticketid);
		return result;
	}

	@Override
	public void addCreditCondition(IfsApprovefxFwd map) {
		map.setTicketId(getTicketId("FWD"));
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxFwdMapper.addCredit(map);
	}

	@Override
	public void editCredit(IfsApprovefxFwd map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxFwdMapper.editCredit(map);

	}

	@Override
	public void deleteCredit(String ticketId) {
		ifsApprovefxFwdMapper.deleteCredit(ticketId);

	}
	
	@Override
	public IfsApprovefxFwd getFwdAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxFwd fwd=ifsApprovefxFwdMapper.selectFlowIdByPrimaryKey(key);
		return fwd;
	}

	
	/***
	 * 根据请求参数查询分页列表-人民币远期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxFwd> getFxFwdMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxFwd> page= new Page<IfsApprovefxFwd>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxFwdMapper.getFxFwdMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxFwdMapper.getFxFwdMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxFwdMapper.getFxFwdMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 人民币掉期
	 * 
	 * @see com.singlee.ifs.service.IFSService#getswapPage(java.util.Map)
	 */

	@Override
	public Page<IfsApprovefxSwap> getswapPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxSwap> result = ifsApprovefxSwapMapper.getswapPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsApprovefxSwap getrmswap(String ticketId) {
		IfsApprovefxSwap result = ifsApprovefxSwapMapper.getrmswap(ticketId);
		return result;
	}

	@Override
	public void addrmswap(IfsApprovefxSwap map) {
		map.setTicketId(getTicketId("SWAP"));
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		/*map.setAdate(DateUtil.getCurrentDateTimeAsString());*/
		ifsApprovefxSwapMapper.addrmswap(map);
	}

	@Override
	public void editrmswap(IfsApprovefxSwap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxSwapMapper.editrmswap(map);

	}

	@Override
	public void deletermswap(String ticketId) {
		ifsApprovefxSwapMapper.deletermswap(ticketId);

	}
	@Override
	public IfsApprovefxSwap getRmbSwapAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxSwap swap=ifsApprovefxSwapMapper.selectFlowIdByPrimaryKey(key);
		return swap;
	}
	
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxSwap> getFxSwapMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxSwap> page= new Page<IfsApprovefxSwap>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxSwapMapper.getFxSwapMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxSwapMapper.getFxSwapMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxSwapMapper.getFxSwapMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 人民币即期
	 * 
	 * @see com.singlee.ifs.service.IFSService#getSpotPage(java.util.Map)
	 */

	@Override
	public Page<IfsApprovefxSpt> getSpotPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxSpt> result = ifsApprovefxSptMapper.getSpotPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsApprovefxSpt getSpot(String ticketId) {
		IfsApprovefxSpt result = ifsApprovefxSptMapper.getSpot(ticketId);
		return result;
	}

	@Override
	public void addSpot(IfsApprovefxSpt map) {
		map.setTicketId(getTicketId("SPT"));
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxSptMapper.addSpot(map);
	}

	@Override
	public void editSpot(IfsApprovefxSpt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxSptMapper.editSpot(map);

	}

	@Override
	public void deleteSpot(String ticketId) {
		ifsApprovefxSptMapper.deleteSpot(ticketId);

	}
	
	@Override
	public IfsApprovefxSpt getSpotAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxSpt spot=ifsApprovefxSptMapper.selectFlowIdByPrimaryKey(key);
		return spot;
	}

	/***
	 * 根据请求参数查询分页列表-人民币即期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxSpt> getSpotMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxSpt> page= new Page<IfsApprovefxSpt>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxSptMapper.getSpotMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxSptMapper.getSpotMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxSptMapper.getSpotMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 人民币期权
	 * 
	 * @see com.singlee.ifs.service.IFSService#searchOptionPage(java.util.Map)
	 */

	@Override
	public Page<IfsApprovefxOption> searchOptionPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxOption> result = ifsApprovefxOptionMapper.searchOptionPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsApprovefxOption searchOption(String ticketId) {
		IfsApprovefxOption result = ifsApprovefxOptionMapper.searchOption(ticketId);
		return result;
	}

	@Override
	public void addOption(IfsApprovefxOption map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("OPT"));
		ifsApprovefxOptionMapper.addOption(map);
	}

	@Override
	public void editOption(IfsApprovefxOption map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxOptionMapper.editOption(map);

	}

	@Override
	public void deleteOption(String ticketId) {
		ifsApprovefxOptionMapper.deleteOption(ticketId);
	}

	/***
	 * 根据请求参数查询分页列表-人民币期权-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxOption> getOptionMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxOption> page= new Page<IfsApprovefxOption>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxOptionMapper.getOptionMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxOptionMapper.getOptionMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxOptionMapper.getOptionMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 黄金拆借
	 */
	@Override
	public Page<IfsApprovegoldLend> searchGoldLendPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovegoldLend> result = ifsCfetgoldLendMapper.searchGoldLendPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteGoldLend(String ticketid) {
		ifsCfetgoldLendMapper.deleteGoldLend(ticketid);

	}

	@Override
	public void addGoldLend(IfsApprovegoldLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("GLEND"));
		ifsCfetgoldLendMapper.addGoldLend(map);

	}

	@Override
	public void editGoldLend(IfsApprovegoldLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetgoldLendMapper.editGoldLend(map);

	}

	@Override
	public IfsApprovegoldLend searchGoldLend(String ticketid) {

		IfsApprovegoldLend result = ifsCfetgoldLendMapper.searchGoldLend(ticketid);
		return result;
	}
	/***
	 * 根据请求参数查询分页列表- 黄金拆借-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovegoldLend> getGoldLendMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovegoldLend> page= new Page<IfsApprovegoldLend>();
		if(isFinished == 1){//待审批
			page=ifsCfetgoldLendMapper.getGoldLendMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsCfetgoldLendMapper.getGoldLendMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsCfetgoldLendMapper.getGoldLendMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 外币头寸调拨
	 * 
	 * @see
	 * com.singlee.ifs.service.IFSService#searchIfsAllocatePage(java.util.Map)
	 */
	@Override
	public Page<IfsApprovefxAllot> searchIfsAllocatePage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxAllot> result = ifsApproveAllocateMapper.searchIfsAllocatePage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteIfsAllocate(String ticketid) {
		ifsApproveAllocateMapper.deleteIfsAllocate(ticketid);

	}

	@Override
	public void addIfsAllocate(IfsApprovefxAllot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("ALLOT"));
		ifsApproveAllocateMapper.addIfsAllocate(map);
	}

	@Override
	public void editIfsAllocate(IfsApprovefxAllot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApproveAllocateMapper.editIfsAllocate(map);

	}

	@Override
	public IfsApprovefxAllot searchIfsAllocate(String ticketid) {
		IfsApprovefxAllot result = ifsApproveAllocateMapper.searchIfsAllocate(ticketid);
		return result;
	}
	/***
	 * 根据请求参数查询分页列表- 外币头寸调拨-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxAllot> getAllocateMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxAllot> page= new Page<IfsApprovefxAllot>();
		if(isFinished == 1){//待审批
			page=ifsApproveAllocateMapper.getAllocateMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApproveAllocateMapper.getAllocateMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApproveAllocateMapper.getAllocateMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	/*
	 * 外币债
	 */
	@Override
	public Page<IfsApprovefxDebt> searchIfsDebtPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsApprovefxDebt> result = ifsApprovefxDebtMapper.searchIfsDebtPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteIfsDebt(String ticketid) {
		ifsApprovefxDebtMapper.deleteIfsDebt(ticketid);

	}

	@Override
	public void addIfsDebt(IfsApprovefxDebt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("DEBT"));
		ifsApprovefxDebtMapper.addIfsDebt(map);
	}

	@Override
	public void editIfsDebt(IfsApprovefxDebt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsApprovefxDebtMapper.editIfsDebt(map);

	}

	@Override
	public IfsApprovefxDebt searchIfsDebt(String ticketid) {
		IfsApprovefxDebt result = ifsApprovefxDebtMapper.searchIfsDebt(ticketid);
		return result;
	}
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovefxDebt> getDebtMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovefxDebt> page= new Page<IfsApprovefxDebt>();
		if(isFinished == 1){//待审批
			page=ifsApprovefxDebtMapper.getDebtMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=ifsApprovefxDebtMapper.getDebtMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=ifsApprovefxDebtMapper.getDebtMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	
	/*
	 * 黄金即远掉
	 */
	@Override
	public void insertGold(IfsApprovemetalGold record) {
		if("fwd".equals(record.getType())){
			record.setTicketId(getTicketId("GFWD"));
		}else if("spt".equals(record.getType())){
			record.setTicketId(getTicketId("GSPT"));
		}else{
			record.setTicketId(getTicketId("GSWAP"));
		}
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsmetalGoldMapper.insertSelective(record);
	}

	@Override
	public void updateGold(IfsApprovemetalGold record) {
		cfetsmetalGoldMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteGold(IfsApprovemetalGold key) {
		cfetsmetalGoldMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovemetalGold> getGoldList(IfsApprovemetalGold record) {
		Page<IfsApprovemetalGold> page = cfetsmetalGoldMapper.selectIfsCfetsmetalGoldList(record);
		return page;
	}

	@Override
	public IfsApprovemetalGold getGoldById(IfsApprovemetalGold key) {
		IfsApprovemetalGold gold = cfetsmetalGoldMapper.selectByPrimaryKey(key);
		return gold;
	}
	
	@Override
	public IfsApprovemetalGold getGoldAndFlowById(Map<String, Object> key) {
		IfsApprovemetalGold gold = cfetsmetalGoldMapper.selectFlowByPrimaryKey(key);
		return gold;
	}
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovemetalGold> getMetalGoldMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovemetalGold> page= new Page<IfsApprovemetalGold>();
		if(isFinished == 1){//待审批
			page=cfetsmetalGoldMapper.getMetalGoldMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsmetalGoldMapper.getMetalGoldMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsmetalGoldMapper.getMetalGoldMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	
	/*
	 * 现券买卖CBT
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertCbt(com.singlee.ifs.model.
	 * IfsCfetsrmbCbt)
	 */

	@Override
	public void insertCbt(IfsApprovermbCbt record) {
		record.setTicketId(getTicketId("CBT"));
		/**新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);

		cfetsrmbCbtMapper.insertSelective(record);
	}

	@Override
	public void updateCbt(IfsApprovermbCbt record) {
		cfetsrmbCbtMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteCbt(IfsApprovermbCbt key) {
		cfetsrmbCbtMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbCbt> getCbtList(IfsApprovermbCbt record) {
		Page<IfsApprovermbCbt> page = cfetsrmbCbtMapper.selectIfsApprovermbCbtList(record);
		return page;
	}

	@Override
	public IfsApprovermbCbt getCbtById(IfsApprovermbCbt key) {
		IfsApprovermbCbt cbt = cfetsrmbCbtMapper.selectByPrimaryKey(key);
		return cbt;
	}
	
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbCbt> getRmbCbtMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbCbt> page= new Page<IfsApprovermbCbt>();
		if(isFinished == 1){//待审批
			page=cfetsrmbCbtMapper.getRmbCbtMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbCbtMapper.getRmbCbtMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbCbtMapper.getRmbCbtMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	//20180509 新增 
	@Override
	public IfsApprovermbCbt getCbtAndFlowIdById(Map<String,Object>  key) {
		IfsApprovermbCbt cbt = cfetsrmbCbtMapper.selectFlowIdByPrimaryKey(key);
		return cbt;
	}

	/*
	 * 质押式回购CR
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertCr(com.singlee.ifs.model.
	 * IfsCfetsrmbCr)
	 */

	@Override
	public void insertCr(IfsApprovermbCr record) {
		/**新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("CR"));
		cfetsrmbCrMapper.insertSelective(record);
	}

	@Override
	public void updateCr(IfsApprovermbCr record) {
		cfetsrmbCrMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteCr(IfsApprovermbCr key) {
		cfetsrmbCrMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbCr> getCrList(IfsApprovermbCr record) {
		Page<IfsApprovermbCr> page = cfetsrmbCrMapper.selectIfsCfetsrmbCrList(record);
		return page;
	}

	@Override
	public IfsApprovermbCr getCrById(IfsApprovermbCr key) {
		IfsApprovermbCr cr = cfetsrmbCrMapper.selectByPrimaryKey(key);
		return cr;
	}
	
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbCr> getRmbCrMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbCr> page= new Page<IfsApprovermbCr>();
		if(isFinished == 1){//待审批
			page=cfetsrmbCrMapper.getRmbCrMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbCrMapper.getRmbCrMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbCrMapper.getRmbCrMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}


	/*
	 * 
	 * 信用拆借IBO
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertIbo(com.singlee.ifs.model.
	 * IfsCfetsrmbIbo)
	 */

	@Override
	public void insertIbo(IfsApprovermbIbo record) {
		/**新增时将 审批状态 改为 新建 */
		record.setTicketId(getTicketId("IBO"));
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbIboMapper.insertSelective(record);
	}

	@Override
	public void updateIbo(IfsApprovermbIbo record) {
		cfetsrmbIboMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteIbo(IfsApprovermbIbo key) {
		cfetsrmbIboMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbIbo> getIboList(IfsApprovermbIbo record) {
		Page<IfsApprovermbIbo> page = cfetsrmbIboMapper.selectIfsCfetsrmbIboList(record);
		return page;
	}

	@Override
	public IfsApprovermbIbo getIboById(IfsApprovermbIbo key) {
		IfsApprovermbIbo ibo = cfetsrmbIboMapper.selectByPrimaryKey(key);
		return ibo;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbIbo> getRmbIboMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbIbo> page= new Page<IfsApprovermbIbo>();
		if(isFinished == 1){//待审批
			page=cfetsrmbIboMapper.getRmbIboMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbIboMapper.getRmbIboMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbIboMapper.getRmbIboMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}


	/*
	 * 利率互换IRS
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertIrs(com.singlee.ifs.model.
	 * IfsCfetsrmbIrs)
	 */
	@Override
	public void insertIrs(IfsApprovermbIrs record) {
		/**新增时将 审批状态 改为 新建 */
		record.setTicketId(getTicketId("IRS"));
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbIrsMapper.insertSelective(record);
	}

	@Override
	public void updateIrs(IfsApprovermbIrs record) {
		cfetsrmbIrsMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteIrs(IfsApprovermbIrs key) {
		cfetsrmbIrsMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbIrs> getIrsList(IfsApprovermbIrs record) {
		Page<IfsApprovermbIrs> page = cfetsrmbIrsMapper.selectIfsCfetsrmbIrsList(record);
		return page;
	}

	@Override
	public IfsApprovermbIrs getIrsById(IfsApprovermbIrs key) {
		IfsApprovermbIrs irs = cfetsrmbIrsMapper.selectByPrimaryKey(key);
		return irs;
	}
	//20180507 新增 
	@Override
	public IfsApprovermbIrs getIrsAndFlowIdById(Map<String,Object>  key) {
		IfsApprovermbIrs irs = cfetsrmbIrsMapper.selectFlowIdByPrimaryKey(key);
		return irs;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbIrs> getRmbIrsMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbIrs> page= new Page<IfsApprovermbIrs>();
		if(isFinished == 1){//待审批
			page=cfetsrmbIrsMapper.getRmbIrsMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbIrsMapper.getRmbIrsMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbIrsMapper.getRmbIrsMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	/*
	 * 买断式回购OR
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertOr(com.singlee.ifs.model.
	 * IfsCfetsrmbOr)
	 */
	@Override
	public void insertOr(IfsApprovermbOr record) {
		record.setTicketId(getTicketId("OR"));
		/**新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbOrMapper.insertSelective(record);
	}

	@Override
	public void updateOr(IfsApprovermbOr record) {
		cfetsrmbOrMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void deleteOr(IfsApprovermbOr key) {
		cfetsrmbOrMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbOr> getOrList(IfsApprovermbOr record) {
		Page<IfsApprovermbOr> page = cfetsrmbOrMapper.selectIfsCfetsrmbOrList(record);
		return page;
	}

	@Override
	public IfsApprovermbOr getOrById(IfsApprovermbOr key) {
		IfsApprovermbOr or = cfetsrmbOrMapper.selectByPrimaryKey(key);
		return or;
	}
	
	/*   增加： 根据id获取实体对象和流程id*/
	@Override
	public IfsApprovermbOr getOrAndFlowIdById(Map<String,Object>  key){
		IfsApprovermbOr ifsCfetsrmbOr = cfetsrmbOrMapper.selectFlowIdByPrimaryKey(key);
		return ifsCfetsrmbOr;
	};
	
	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbOr> getRmbOrMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbOr> page= new Page<IfsApprovermbOr>();
		if(isFinished == 1){//待审批
			page=cfetsrmbOrMapper.getRmbOrMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbOrMapper.getRmbOrMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbOrMapper.getRmbOrMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	/*
	 * 债券借贷SL
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertSl(com.singlee.ifs.model.
	 * IfsCfetsrmbSl)
	 */
//	@Override
//	public void insertSl(IfsApprovermbSl record) {
//		/**新增时将 审批状态 改为 新建 */
//		record.setApproveStatus(DictConstants.ApproveStatus.New);
//		record.setTicketId(getTicketId("SL"));
//		cfetsrmbSlMapper.insert(record);
//	}
	
	@Override
	public String insertSl(IfsApprovermbSl record) {
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("SL"));
		record.setDealTransType("1");
		String str = "";
		List<IfsApprovermbSl> list = cfetsrmbSlMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbSlMapper.insert(record);
				List<IfsCfetsrmbDetailSl> records=record.getBondDetailsList();
				for(int i=0;i<records.size();i++){
					IfsCfetsrmbDetailSl rec=records.get(i);
					rec.setTicketId(record.getTicketId());
					rec.setSeq((i+1)+"");
					detailSlMapper.insert(rec);
				}
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}
	
	
	

	@Override
	public void updateSl(IfsApprovermbSl record) {
		cfetsrmbSlMapper.updateByPrimaryKey(record);
		//债券详情修改  为先删除再插入  不做修改操作 
		detailSlMapper.deleteByTicketId(record.getTicketId());
		List<IfsCfetsrmbDetailSl> records=record.getBondDetailsList();
		for(int i=0;i<records.size();i++){
			IfsCfetsrmbDetailSl rec=records.get(i);
			rec.setTicketId(record.getTicketId());
			rec.setSeq((i+1)+"");
			detailSlMapper.insert(rec);
		}
	}

	@Override
	public Page<IfsCfetsrmbDetailSl> searchBondSlDetails(Map<String, Object> params) {
		return detailSlMapper.searchBondSlDetails(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public Page<Map<String, Object>> getSlMini(Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbSlMapper.getSlMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public void deleteSl(IfsApprovermbSl key) {
		cfetsrmbSlMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsApprovermbSl> getSlList(IfsApprovermbSl record) {
		Page<IfsApprovermbSl> page = cfetsrmbSlMapper.selectIfsCfetsrmbSlList(record);
		return page;
	}

	@Override
	public IfsApprovermbSl getSlById(IfsApprovermbSl key) {
		IfsApprovermbSl sl = cfetsrmbSlMapper.selectByPrimaryKey(key);
		return sl;
	}

	/***
	 * 根据请求参数查询分页列表-债券借贷-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsApprovermbSl> getRmbSlMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbSl> page= new Page<IfsApprovermbSl>();
		if(isFinished == 1){//待审批
			page=cfetsrmbSlMapper.getRmbSlMineList(params,
					ParameterUtil.getRowBounds(params));
		}else if (isFinished == 2){//已审批
			page=cfetsrmbSlMapper.getRmbSlMineListFinished(params,
					ParameterUtil.getRowBounds(params));
		}else {//我发起的
			page=cfetsrmbSlMapper.getRmbSlMineListMine(params,
					ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public IfsApprovermbIbo getIboAndFlowIdById(Map<String, Object> key) {
		IfsApprovermbIbo ibo = cfetsrmbIboMapper.selectFlowIdByPrimaryKey(key);
		return ibo;
	}

	@Override
	public IfsApprovermbSl getSlAndFlowIdById(Map<String, Object> key) {
		IfsApprovermbSl sl = cfetsrmbSlMapper.selectFlowIdByPrimaryKey(key);
		return sl;
	}

	@Override
	public IfsApprovermbCr getCrAndFlowIdById(Map<String, Object> key) {
		IfsApprovermbCr cr = cfetsrmbCrMapper.selectFlowIdByPrimaryKey(key);
		return cr;
	}

	@Override
	public IfsApprovegoldLend getGoldLendAndFlowById(Map<String, Object> key) {
		IfsApprovegoldLend gold = ifsCfetgoldLendMapper.selectFlowByPrimaryKey(key);
		return gold;
	}

	@Override
	public IfsApprovefxDebt getDebtAndFlowById(Map<String, Object> key) {
		IfsApprovefxDebt debt= ifsApprovefxDebtMapper.selectFlowByPrimaryKey(key);
		return debt;
	}

	@Override
	public IfsApprovefxOption searchRmbOptAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxOption opt= ifsApprovefxOptionMapper.selectFlowByPrimaryKey(key);
		return opt;
	}
	@Override
	public IfsApprovefxLend searchRmbCcylendingAndFlowIdById(
			Map<String, Object> key) {
		IfsApprovefxLend lend= ifsApprovefxLendMapper.selectFlowByPrimaryKey(key);
		return lend;
	}

	@Override
	public IfsApprovefxAllot searchRmbCcypaAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxAllot allot= ifsApproveAllocateMapper.selectFlowByPrimaryKey(key);
		return allot;
	}

	@Override
	public IfsApprovefxOnspot searchRmbSpotAndFlowIdById(Map<String, Object> key) {
		IfsApprovefxOnspot sport= ifsApprovefxOnspotMapper.selectFlowByPrimaryKey(key);
		return sport;
	}

	@Override
	public Page<IfsApprovermbIrs> getApprovePassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbIrs> page = cfetsrmbIrsMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovermbCbt> getApproveCbtPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbCbt> page = cfetsrmbCbtMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovermbCr> getApproveCrPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbCr> page = cfetsrmbCrMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovermbIbo> getApproveIboPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbIbo> page = cfetsrmbIboMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovermbOr> getApproveOrPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbOr> page = cfetsrmbOrMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovermbSl> getApproveSlPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovermbSl> page = cfetsrmbSlMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovegoldLend> getApproveGoldLendPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovegoldLend> page = ifsCfetgoldLendMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovemetalGold> getApprovemetalGoldPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovemetalGold> page = cfetsmetalGoldMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	@Override
	public Page<IfsApprovefxOption> getApprovefxOptionPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxOption> page = ifsApprovefxOptionMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxSpt> getApprovefxSptPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxSpt> page = ifsApprovefxSptMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxFwd> getApprovefxFwdPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxFwd> page = ifsApprovefxFwdMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxOnspot> getApproveOnspotPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxOnspot> page = ifsApprovefxOnspotMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxSwap> getApprovefxSwapPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxSwap> page = ifsApprovefxSwapMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxCswap> getApprovefxCswapPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxCswap> page = ifsApprovefxCswapMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxLend> getApprovefxLendPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxLend> page = ifsApprovefxLendMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxAllot> getApprovefxAllotPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxAllot> page = ifsApproveAllocateMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsApprovefxDebt> getApprovefxDebtPassedPage(
			Map<String, Object> params) {
		Page<IfsApprovefxDebt> page = ifsApprovefxDebtMapper.searchApprovePassed(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	public String getTicketId(String str){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("sStr",str);//前缀
		hashMap.put("trdType","1");//贸易类型，事前交易
		//CSWAP20170831339000707
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}
}
