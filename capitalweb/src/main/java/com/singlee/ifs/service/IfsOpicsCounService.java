package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsCoun;

import java.util.Map;

/***
 * 国家代码1
 * @author lij
 *
 */
public interface IfsOpicsCounService {
	//新增
	public void insert(IfsOpicsCoun entity);
	
	//修改
	void updateById(IfsOpicsCoun entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsCoun> searchPageOpicsCoun(Map<String,Object> map);
	
	//根据id查询实体类
	IfsOpicsCoun searchById(String id);
	
	//更新同步状态
	void updateStatus(IfsOpicsCoun entity);
	
	//批量校准
	SlOutBean batchCalibration(String type,String[] ccodes);
	
	//查询所有的国家代码
	Page<IfsOpicsCoun> searchAllOpicsCoun();
}