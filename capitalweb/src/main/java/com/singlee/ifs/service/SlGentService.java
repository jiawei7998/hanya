package com.singlee.ifs.service;

import java.util.Map;

import com.singlee.financial.bean.SlIntfcGent;
import com.singlee.financial.page.PageInfo;

public interface SlGentService {

	PageInfo<SlIntfcGent> querySlGent(Map<String, String> params);

	void gentAdd(SlIntfcGent entity);

	void gentEdit(SlIntfcGent entity);

	void deleteSlGent(SlIntfcGent entity);
	
	SlIntfcGent querySlIntfcGentById(SlIntfcGent entity);
}
