package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.ifs.mapper.TradeSettlsMapper;
import com.singlee.ifs.service.TradeSettlsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName TradeSettlsServiceImpl.java
 * @Description 外币清算路径
 * @createTime 2021年11月09日 17:07:00
 */
@Service
public class TradeSettlsServiceImpl implements TradeSettlsService {
    @Autowired
    TradeSettlsMapper tradeSettlsMapper;


    @Override
    public TradeSettlsBean getTradeSettls(Map<String, Object> map) {
        return tradeSettlsMapper.getTradeSettls(map);
    }

    @Override
    public Page<TradeSettlsBean> searchSptFwdSettle(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<TradeSettlsBean> searchSwapSettle(Map<String, Object> map) {
        return null;
    }
}
