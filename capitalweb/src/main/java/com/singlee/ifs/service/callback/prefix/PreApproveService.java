package com.singlee.ifs.service.callback.prefix;

import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Deprecated
@Service(value = "preApproveService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PreApproveService extends IfsApproveServiceBase {

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key)
			throws Exception {
		return super.fireEvent(flow_id, serial_no, task_id, task_def_key);
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {
		return super.getBizObj(flow_type, flow_id, serial_no);
	}

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		
	}

}
