package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.KYgzBean;

import java.math.BigDecimal;
import java.util.Map;

public interface KYgzBeanService{

    Page<KYgzBean> getPageList(Map<String,Object> map);

    int deleteByPrimaryKey(BigDecimal id);

    int insert(KYgzBean record);

    int insertSelective(KYgzBean record);

    KYgzBean selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(KYgzBean record);

    int updateByPrimaryKey(KYgzBean record);

}
