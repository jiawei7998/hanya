package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFeesServer;
import com.singlee.financial.opics.IRepoServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbCrMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailCrMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsRmbCrService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbCrService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper;
	@Autowired
	IfsOpicsBondMapper bondMapper;
	@Autowired
	IRepoServer repoServer;
	@Autowired
	IFeesServer iFeesServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Autowired
	IfsCfetsrmbDetailCrMapper detailCrMapper;
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;// 查询债券一手的量

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbCrMapper.updateRmbCrByID(serial_no, status, date);
		IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(serial_no);
		List<IfsCfetsrmbDetailCr> crList = detailCrMapper.searchBondByTicketId(serial_no);

		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsrmbCr || crList.size() <= 0) {
			JY.raise("交易数据不完全，请检查交易数据！");
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("P".equals(ifsCfetsrmbCr.getMyDir())) {//逆回购
					for (IfsCfetsrmbDetailCr cr:crList) {
						if(!"0".equals(ifsCfetsrmbCr.getQuotaOccupyType())){
							hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no + "_" + cr.getBondCode());
						}
					}
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		
		String prdNo = ifsCfetsrmbCr.getfPrdCode();
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbCr.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbCr.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbCr.getApproveStatus());// 设置监控表-审批状态

			if (TradeConstants.ProductCode.CRJY.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("交易所回购");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("CRJY");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CRJY;
			} else if (TradeConstants.ProductCode.CR.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("质押式回购");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("cr");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CR;
			} else if (TradeConstants.ProductCode.CRCB.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("常备借贷便利");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("CRCB");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CRCB;
			} else if (TradeConstants.ProductCode.CRZQ.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("中期借贷便利");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("CRZQ");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CRZQ;
			} else if (TradeConstants.ProductCode.CRZD.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("线下押券");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("CRZD");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CRZD;
			} else if(TradeConstants.ProductCode.CRGD.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("国库定期存款");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("CRGD");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.CRGD;
			} else {
				JY.raise("审批未通过，产品号" + prdNo + "未配置");
			}
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbCr.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbCr.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbCr.getReverseInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer();
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_CR");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CR where TICKET_ID=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbCrMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no+"_"+crList.get(0).getBondCode());
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("P".equals(ifsCfetsrmbCr.getMyDir())) {//逆回购
						if(!"0".equals(ifsCfetsrmbCr.getQuotaOccupyType())){
							LimitOccupy(serial_no, prdNo);
						}
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map3 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map3);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}

			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			// 3.3调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				// 获取当前账务日期
				Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbCr.getBr());
				SlRepoCrBean slRepoCrBean = getEntry(ifsCfetsrmbCr, branchDate);
				SlOutBean result = repoServer.repoCr(slRepoCrBean);
				//交易所回购才有手续费
				if(TradeConstants.ProductCode.CRJY.equals(ifsCfetsrmbCr.getfPrdCode())){
					//将手续费存入费用表
					SlIfeeBean slIfeeBean=setSlIfeeBean(ifsCfetsrmbCr);
					SlOutBean feesResult = iFeesServer.fees(slIfeeBean);
				}

				if (result.getRetStatus().equals(RetStatusEnum.S)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsrmbCr.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsrmbCrMapper.updateStatcodeByTicketId(map);
					/************ 质押的券 ***************/
//					List<IfsCfetsrmbDetailCr> crList = detailCrMapper.searchBondByTicketId(ifsCfetsrmbCr.getTicketId());
					for (int i = 0; i < crList.size(); i++) {
						SlRepoCrBean crBean = getEntryDetail(ifsCfetsrmbCr, i, crList.size(), crList.get(i),
								slRepoCrBean, branchDate);
						SlOutBean outBean = repoServer.repoCr(crBean);
						if (outBean.getRetStatus().equals(RetStatusEnum.S)) {
							Map<String, Object> map2 = new HashMap<String, Object>();
							map2.put("ticketId", crList.get(i).getTicketId());
							map2.put("bondCode", crList.get(i).getBondCode());
							map2.put("statcode", "-5");
							map2.put("seq", String.valueOf(i + 1));
							detailCrMapper.updateStatcodeByTicketId(map2);
						} else {
							JY.raise("审批未通过，质押券插入opics失败......");
						}
					}
				} else {
					JY.raise("审批未通过，交易插入opcis失败......");
				}
			} else {
				// 将实体对象转换成map
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbCr);
				// 正式调用存储过程
				SlOutBean result = repoServer.ircaProc(map);// 质押式回购存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，执行存储过程失败......");
				}
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(serial_no);
		List<IfsCfetsrmbDetailCr> crList = detailCrMapper.searchBondByTicketId(serial_no);
		for (IfsCfetsrmbDetailCr cr : crList) {
			Map<String, Object> remap = new HashMap<>();
			remap.put("custNo", cr.getCustNo());
			remap.put("custType", cr.getCustType());
			HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
			remap.put("weight", cr.getWeight());
			remap.put("productCode", prd_no);
			remap.put("institution", ifsCfetsrmbCr.getPositiveInst());
			BigDecimal amt = cr.getTotalFaceValue().multiply(new BigDecimal("100")).multiply(cr.getUnderlyingStipType());
			amt = calcuAmt(amt, cr.getWeight());
			remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
			remap.put("currency", hrbCreditCustQuota.getCurrency());
			remap.put("vdate", ifsCfetsrmbCr.getForDate());
			remap.put("mdate", ifsCfetsrmbCr.getSecondSettlementDate());
			remap.put("serial_no", serial_no+"_"+cr.getBondCode());
			hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
		}
	}

	/**
	 * 设置费用信息
	 * @param ifsCfetsrmbCr
	 * @return
	 */
	private SlIfeeBean setSlIfeeBean(IfsCfetsrmbCr ifsCfetsrmbCr){

		Map<String, Object> sysMap = new HashMap<>();
		sysMap.put("p_type", "FEE");// 参数代码
		List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
		Map<String,Object> param=sysList.stream().collect(Collectors.toMap(TaSysParam::getP_code,TaSysParam::getP_value));

		SlIfeeBean slIfeeBean=new SlIfeeBean("01",SlDealModule.FEES.SERVER,SlDealModule.FEES.TAG,SlDealModule.FEES.DETAIL);
		slIfeeBean.getInthBean().setFedealno(ifsCfetsrmbCr.getTicketId());
		slIfeeBean.setFeeprodtype(param.get("feeprodtype").toString());
		slIfeeBean.setFeeproduct(param.get("feeprod").toString());
//		slIfeeBean.setProduct("REPO");
//		slIfeeBean.setProdtype("JY");
		slIfeeBean.setCcy("CNY");
		slIfeeBean.setCcyamt(ifsCfetsrmbCr.getHandleAmt().multiply(new BigDecimal("-1")).toString());
		slIfeeBean.setCost(param.get("cost").toString());
		slIfeeBean.setCno(ifsCfetsrmbCr.getReverseInst());
		slIfeeBean.setTenor(param.get("tenor").toString());
		slIfeeBean.setAl(param.get("tenor").toString());
		slIfeeBean.setAcctgmethod(param.get("acctgmethod").toString());
		slIfeeBean.setVerind(param.get("verind").toString());
		slIfeeBean.setSettmeans(param.get("settmeans").toString());
		slIfeeBean.setSettacct(param.get("settacct").toString());
		slIfeeBean.setSettauthind(param.get("settauthind").toString());
		slIfeeBean.setFeetext(ifsCfetsrmbCr.getTicketId());
		slIfeeBean.setSettauthdte(new Date());
		slIfeeBean.setVdate(DateUtil.parse(ifsCfetsrmbCr.getForDate()));
		slIfeeBean.setStartdate(DateUtil.parse(ifsCfetsrmbCr.getFirstSettlementDate()));
		slIfeeBean.setEnddate(DateUtil.parse(ifsCfetsrmbCr.getSecondSettlementDate()));
		slIfeeBean.setInputdate(new Date());
		slIfeeBean.setInputtime(DateUtil.getCurrentTimeAsString());
		slIfeeBean.setLastmntdte(new Date());
		slIfeeBean.setFeesind(param.get("feesind").toString());
		slIfeeBean.setFeeper_8(param.get("feeper_8").toString());
		slIfeeBean.setPort(param.get("port").toString());
		slIfeeBean.setBrprcindte(new Date());
		slIfeeBean.setVerdate(new Date());
		slIfeeBean.setDealmdate(new Date());
		//设置交易员
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbCr.getPositiveTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbCr.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.REPO.IOPER);
		slIfeeBean.getInthBean().setIoper(trad);
		slIfeeBean.setTrad(trad);
		slIfeeBean.setDealdate(new Date());
		slIfeeBean.setIoper(trad);
		slIfeeBean.setSettoper(trad);
		slIfeeBean.setVeroper(trad);
		slIfeeBean.setSiind(param.get("siind").toString());
		slIfeeBean.setSuppconfind(param.get("suppconfind").toString());
		slIfeeBean.setSuppayind(param.get("suppayind").toString());
		slIfeeBean.setSuprecind(param.get("suprecind").toString());

		return slIfeeBean;
	}
	/**
	 * 回购交易主体信息
	 * 
	 * @param ifsCfetsrmbCr
	 * @param branchDate
	 * @return
	 */
	private SlRepoCrBean getEntry(IfsCfetsrmbCr ifsCfetsrmbCr, Date branchDate) {
		SlRepoCrBean slRepoCrBean = new SlRepoCrBean(ifsCfetsrmbCr.getBr(), SlDealModule.REPO.SERVER,
				SlDealModule.REPO.TAG, SlDealModule.REPO.DETAIL);
		// 交易对手
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbCr.getReverseInst());
		// 本方交易员
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbCr.getPositiveTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbCr.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.REPO.IOPER);
		institutionInfo.setTradeId(trad);
		slRepoCrBean.getInthBean().setIoper(trad);
		// OPICS拓展属性
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setProdcode(ifsCfetsrmbCr.getProduct());
		externalBean.setProdtype(ifsCfetsrmbCr.getProdType());
		externalBean.setAuthsi(SlDealModule.REPO.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbCr.getContractId());
		externalBean.setSiind(SlDealModule.REPO.SIIND);
		externalBean.setSupconfind(SlDealModule.REPO.SUPPCONFIND);
		externalBean.setSuppayind(SlDealModule.REPO.SUPPPAYIND);
		externalBean.setSuprecind(SlDealModule.REPO.SUPPRECIND);
		externalBean.setVerind(SlDealModule.REPO.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbCr.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbCr.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbCr.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbCr.getCtrsacct());
		/************ 回购本身 ***************/
		slRepoCrBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slRepoCrBean.setInstitutionInfo(institutionInfo);
		slRepoCrBean.setExternalBean(externalBean);
		SlInthBean inthBean = slRepoCrBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbCr.getTicketId());
		inthBean.setStatcode("-1");
		inthBean.setSeq("0");
		inthBean.setLstmntdate(branchDate);
		slRepoCrBean.setInthBean(inthBean);
		slRepoCrBean.setAssignseq("0");
		slRepoCrBean.setSafekeepacct(ifsCfetsrmbCr.getPositiveCustname());
		slRepoCrBean.setCcy("CNY");
		slRepoCrBean.setValueDate(DateUtil.parse(ifsCfetsrmbCr.getFirstSettlementDate()));
		slRepoCrBean.setMaturityDate(DateUtil.parse(ifsCfetsrmbCr.getSecondSettlementDate()));
		slRepoCrBean.setCollateralcode(SlDealModule.REPO.COLLATERALCODE);
		slRepoCrBean.setRepocost(ifsCfetsrmbCr.getCost());
		// slRepoCrBean.setRepoport(ifsCfetsrmbCr.getPort());
		slRepoCrBean.setDealDate(DateUtil.parse(ifsCfetsrmbCr.getForDate()));
		slRepoCrBean.setDelivtype(SlDealModule.REPO.DELIVTYPE);
		slRepoCrBean.setCoupreinvest("N");
		slRepoCrBean.setBasis(ifsCfetsrmbCr.getBasis());
		slRepoCrBean.setReporate(ifsCfetsrmbCr.getRepoRate());
		slRepoCrBean.setTabindicator("1");
		slRepoCrBean.setTenor("99");
		slRepoCrBean.setCollunit(SlDealModule.REPO.COLLUNIT_CR);
		// 券面总额，单位是万元
//		slRepoCrBean.setQty(String.valueOf(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsrmbCr.getUnderlyingQty().multiply(new BigDecimal(10000)) + ""), 2)));
		//20190520
		SecurityAmtDetailBean matSecuritySettAmtSeq0 = new SecurityAmtDetailBean();
		matSecuritySettAmtSeq0.setSettAmt(ifsCfetsrmbCr.getTradeAmount());
		//此处需将值set入对象，将对象带入XML
		slRepoCrBean.setMatSecuritySettAmt(matSecuritySettAmtSeq0);

		slRepoCrBean.setDcpriceind(SlDealModule.REPO.DCPRICEIND);
		slRepoCrBean.setIntcalctype(SlDealModule.REPO.INTCALCTYPE);
		slRepoCrBean.setFullyassignind("N");
		slRepoCrBean.setPmvind(SlDealModule.REPO.PMVIND);
		return slRepoCrBean;
	}

	/**
	 * 回购质押券明细
	 * 
	 * @param ifsCfetsrmbCr
	 * @param i
	 * @param lstSize
	 * @param detailCr
	 * @param slRepoCrBean
	 * @param branchDate
	 * @return
	 */
	private SlRepoCrBean getEntryDetail(IfsCfetsrmbCr ifsCfetsrmbCr, int i, int lstSize, IfsCfetsrmbDetailCr detailCr,
			SlRepoCrBean slRepoCrBean, Date branchDate) {
		SlRepoCrBean CrBean = new SlRepoCrBean(ifsCfetsrmbCr.getBr(), SlDealModule.REPO.SERVER, SlDealModule.REPO.TAG,
				SlDealModule.REPO.DETAIL);
		CrBean.setContraPatryInstitutionInfo(slRepoCrBean.getContraPatryInstitutionInfo());
		CrBean.setInstitutionInfo(slRepoCrBean.getInstitutionInfo());
		// 券的成本中心和投资组合
		// OPICS拓展属性
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setProdcode(ifsCfetsrmbCr.getProduct());
		externalBean.setProdtype(ifsCfetsrmbCr.getProdType());
		externalBean.setAuthsi(SlDealModule.REPO.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbCr.getContractId());
		externalBean.setSiind(SlDealModule.REPO.SIIND);
		externalBean.setSupconfind(SlDealModule.REPO.SUPPCONFIND);
		externalBean.setSuppayind(SlDealModule.REPO.SUPPPAYIND);
		externalBean.setSuprecind(SlDealModule.REPO.SUPPRECIND);
		externalBean.setVerind(SlDealModule.REPO.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbCr.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbCr.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbCr.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbCr.getCtrsacct());
		externalBean.setCost(detailCr.getCostb());
		externalBean.setPort(detailCr.getPortb());
		CrBean.setExternalBean(externalBean);
		SlInthBean slInthBean = slRepoCrBean.getInthBean();
		slInthBean.setFedealno(ifsCfetsrmbCr.getTicketId());
		slInthBean.setSeq(String.valueOf(i + 1));
		slInthBean.setStatcode("-5");
		slInthBean.setLstmntdate(branchDate);
		CrBean.setInthBean(slInthBean);
		CrBean.setAssignseq(String.valueOf(i + 1));
		CrBean.setSafekeepacct(ifsCfetsrmbCr.getPositiveCustname());
		CrBean.setCcy("CNY");
		CrBean.setValueDate(DateUtil.parse(ifsCfetsrmbCr.getFirstSettlementDate()));
		CrBean.setMaturityDate(DateUtil.parse(ifsCfetsrmbCr.getSecondSettlementDate()));
		CrBean.setCollateralcode(SlDealModule.REPO.COLLATERALCODE);
		// 回购的成本中心和投资组合
		CrBean.setRepocost(ifsCfetsrmbCr.getCost());
		// CrBean.setRepoport(ifsCfetsrmbCr.getPort());
		CrBean.setDealDate(DateUtil.parse(ifsCfetsrmbCr.getForDate()));
		CrBean.setDelivtype(SlDealModule.REPO.DELIVTYPE);
		CrBean.setCoupreinvest("N");
		CrBean.setBasis(ifsCfetsrmbCr.getBasis());
		// 回购利率
		CrBean.setReporate(ifsCfetsrmbCr.getRepoRate());
		CrBean.setTabindicator("2");
		CrBean.setTenor("99");
		CrBean.setSecurityId(detailCr.getBondCode());
		CrBean.setCollunit(SlDealModule.REPO.COLLUNIT_CR);
		IfsOpicsBond opicsBondDetail = ifsOpicsBondService.searchOneById(detailCr.getBondCode());
		//单只券面值
		BigDecimal totalFaceValue =detailCr.getTotalFaceValue();
		CrBean.setQty(String.valueOf(totalFaceValue));
		/*
		 * 对于每只债券的交易金额处理逻辑如下： 债券存在交易金额 不等于按照面值进行折扣的情况
		 * 需要拿当前券额面额/总的券的面额*总的交易金额
		 */
		SecurityAmtDetailBean matSecuritySettAmt = new SecurityAmtDetailBean();
		//将表中折算金额方式SettAmt20190520
		matSecuritySettAmt.setSettAmt(detailCr.getParValue().multiply(new BigDecimal(opicsBondDetail.getDenom())));
		CrBean.setMatSecuritySettAmt(matSecuritySettAmt);
		CrBean.setInvtype(detailCr.getInvtype());
		CrBean.setDcpriceind(SlDealModule.REPO.DCPRICEIND);
		CrBean.setIntcalctype(SlDealModule.REPO.INTCALCTYPE);
		if (i == lstSize - 1) {
			CrBean.setFullyassignind("Y");
		} else {
			CrBean.setFullyassignind("N");
		}
		CrBean.setPmvind(SlDealModule.REPO.PMVIND);
		return CrBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");

		String id = (String)paramer.get("id");
		String[] prodtype = {"",""};
		if("jyzyshg".equals(id)) {//质押式回购
			prodtype[0] = "VP";
			prodtype[1] = "RP";
			paramer.put("applyProd", TradeConstants.MarketIndicator.COLLATERAL_REPO);
		}
		if("jygkdqck".equals(id)) {//国库定期存款
			prodtype[0] = "GD";
		}
		if("jyjyshg".equals(id)) {//交易所回购
			prodtype[0] = "JY";
		}
		if("jycbjdbl".equals(id)) {//常备借贷便利
			prodtype[0] = "CB";
		}
		if("jyzqjdbl".equals(id)) {//中期借贷便利
			prodtype[0] = "ZQ";
		}
		if("jyzxzdk".equals(id)) {//线下押券
			prodtype[0] = "ZD";
		}
		paramer.put("prodtype", prodtype);

		List<IfsCfetsrmbCr> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbCrMapper.getRmbCrMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbCrMapper.getRmbCrMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbCrMapper.getRmbCrMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> repoingList = taDictVoMapper.getTadictTree("repoing").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> basisList = taDictVoMapper.getTadictTree("Basis").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradeSourceList = taDictVoMapper.getTadictTree("TradeSource").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbCr ifsCfetsrmbCr: list) {
			ifsCfetsrmbCr.setMyDir(repoingList.get(ifsCfetsrmbCr.getMyDir()));
			ifsCfetsrmbCr.setBasis(basisList.get(ifsCfetsrmbCr.getBasis()));
			ifsCfetsrmbCr.setDealSource(tradeSourceList.get(ifsCfetsrmbCr.getDealSource()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
