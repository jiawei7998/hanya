package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.KWabacusGuozhaimianshui;

import java.math.BigDecimal;
import java.util.Map;

public interface KWabacusGuozhaimianshuiService{

    Page<KWabacusGuozhaimianshui> getPageList(Map<String,Object> map);

    int deleteByPrimaryKey(BigDecimal id);

    int insert(KWabacusGuozhaimianshui record);

    int insertSelective(KWabacusGuozhaimianshui record);

    KWabacusGuozhaimianshui selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(KWabacusGuozhaimianshui record);

    int updateByPrimaryKey(KWabacusGuozhaimianshui record);

}
