package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsSwiftSettleInfo;

import java.util.Map;

public interface IfsSwiftSettleService {

	/**
	 * 获取报文清算信息
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<IfsSwiftSettleInfo> getSetleInfo(Map<String, Object> map);
}
