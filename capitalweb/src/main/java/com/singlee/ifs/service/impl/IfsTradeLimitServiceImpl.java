package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsTradeLimitMapper;
import com.singlee.ifs.model.IfsTradeLimit;
import com.singlee.ifs.service.IfsTradeLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class IfsTradeLimitServiceImpl implements IfsTradeLimitService {

	@Autowired
	private IfsTradeLimitMapper ifsTradeLimitMapper;

	@Override
	public Page<IfsTradeLimit> getDataPage(Map<String, Object> map) {
		return ifsTradeLimitMapper.getDataPage(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public void addData(Map<String,Object> map) {
		ifsTradeLimitMapper.addData(map);
	}

	@Override
	public void update(Map<String,Object> map) {
		ifsTradeLimitMapper.update(map);
	}

	@Override
	public void delData(List<Map<String,Object>> list) {
		ifsTradeLimitMapper.delData(list);
	}


}
