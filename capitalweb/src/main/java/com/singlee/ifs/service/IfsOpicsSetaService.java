package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsSeta;

import java.util.Map;

/**
 * SETA
 * @author   yanming
 */
public interface IfsOpicsSetaService {
	
	//新增
	public void insert(IfsOpicsSeta entity);
	
	//修改
	void updateById(IfsOpicsSeta entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsSeta> searchPageOpicsSeta(Map<String,Object> map);
	
	//连表分页查询
	Page<IfsOpicsSeta> searchPageOpicsSetaMini(Map<String,Object> map);
	
	//根据id查询实体类
	IfsOpicsSeta searchById(Map<String,String> map);
	
	//更新同步状态
	void updateStatus(IfsOpicsSeta entity);
	
	//批量校准
	SlOutBean batchCalibration(String type,String[] smeanss, String[] saccts);
}

