package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbcap.service.impl.HrbCapSettleOrderCallback;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.mapper.CFtReddMapper;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.model.CFtRedd;
import com.singlee.refund.model.CFtTpos;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "CFtReddService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtReddService extends IfsApproveServiceBase {

	@Autowired
	private CFtReddMapper cFtReddMapper;
	@Autowired
	private CFtInfoMapper cFtInfoMapper;
	@Resource
	IFSMapper ifsMapper;
	@Autowired
	private CFtTposMapper cFtTposMapper;
	@Autowired
	TaUserMapper taUserMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Autowired
	HrbCapSettleOrderCallback hrbCapSettleOrderCallback;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		cFtReddMapper.updateCFtReddByDealNo(serial_no, status, date);
		CFtRedd cFtRedd = cFtReddMapper.selectByPrimaryKey(serial_no);

		// 0.1 当前业务为空则不进行处理
		if (null == cFtRedd) {
			return;
		}
		
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {

		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}

			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(cFtRedd.getDealNo());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(cFtRedd.getDealNo());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(cFtRedd.getApproveStatus());// 设置监控表-审批状态

			// 判断基金是否存在
			CFtInfo cFtInfo = cFtInfoMapper.selectByPrimaryKey(cFtRedd.getFundCode());
			if (cFtInfo == null) {
				JY.raise("交易的基金不存在......");
			}
			if (cFtInfo.getCcy() == null) {
				JY.raise("交易的基金的币种不存在......");
			}
			if (TradeConstants.ProductCode.CURRENCY_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("货币基金现金分红");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.FUND_REDD);
			} else if (TradeConstants.ProductCode.BOND_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("债券基金现金分红");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.FUND_REDD);
			} else if (TradeConstants.ProductCode.SPEC_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("专户基金现金分红");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.FUND_REDD);
			}
			ifsFlowMonitor.setTradeDate(cFtRedd.getVdate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(cFtRedd.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("34");//分红流程
			String cname = cFtRedd.getCname();
			if (null != cname || "".equals(cname)) {
				ifsFlowMonitor.setCustName(cname);
			}

			// 2.3查询客制化字段
			StringBuffer buffer = new StringBuffer("");
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("FT_RDP");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CBT where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = cFtReddMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}

			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

		}

		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map1);
		}
		
		// 3.1未审批通过则不进行后续操作
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		
		try {
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("fundCode", cFtRedd.getFundCode());
			String curDate= DayendDateServiceProxy.getInstance().getSettlementDate();
			params.put("postdate", curDate);
			params.put("sponinst", cFtRedd.getSponInst());
			params.put("invtype", cFtRedd.getInvType());
			params.put("prdNo", cFtRedd.getPrdNo());
			CFtTpos cFtTpos = cFtTposMapper.selectByMap(params);
			if(cFtTpos != null ) {
				
				if(cFtTpos.getyUnplAmt().compareTo(cFtRedd.getAmt()) == 1) {
					cFtTpos.setyUnplAmt(cFtTpos.getyUnplAmt().subtract(cFtRedd.getAmt()));
				}else {
					cFtTpos.setyUnplAmt(new BigDecimal("0"));
				}
				//累计分红
				cFtTpos.setShAmt(cFtTpos.getShAmt().add(cFtRedd.getAmt()));
				cFtTposMapper.updateByPrimaryKeySelective(cFtTpos);

				//生成会计分录
				Map<String, Object> map = new HashMap<>();
				map.put("serial_no",cFtRedd.getDealNo());
				map.put("prdNo",cFtRedd.getPrdNo());
				hrbCapSettleOrderCallback.generateBOND(map);

			}else {
				JY.raise("审批未通过，该基金持仓数据！");
			}
			
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
		
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<CFtRedd> list = null;
		paramer.put("dealNo", paramer.get("contractId"));
		paramer.put("tdate", paramer.get("forDate"));
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = cFtReddMapper.searchCFtReddPageMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = cFtReddMapper.searchCFtReddPageUnfinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = cFtReddMapper.searchCFtReddPageFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tbAccountTypeList = taDictVoMapper.getTadictTree("tbAccountType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (CFtRedd cFtRedd: list) {
			cFtRedd.setCcy(currencyList.get(cFtRedd.getCcy()));
			cFtRedd.setInvType(tbAccountTypeList.get(cFtRedd.getInvType()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

}
