package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsFlowCompletedMapper;
import com.singlee.ifs.model.IfsFlowCompleted;
import com.singlee.ifs.service.IfsFlowCompletedService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsFlowCompletedServiceImpl implements IfsFlowCompletedService {
	@Resource
	IfsFlowCompletedMapper ifsFlowCompletedMapper;

	@Override
	public Page<IfsFlowCompleted> searchPageFlowCompleted(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsFlowCompleted> result = ifsFlowCompletedMapper.searchPageFlowCompleted(map, rb);
		return result;
	}

}
