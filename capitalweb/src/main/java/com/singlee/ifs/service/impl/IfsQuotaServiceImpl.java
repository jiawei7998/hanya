package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsQuotaMapper;
import com.singlee.ifs.model.IfsQuota;
import com.singlee.ifs.model.IfsQuotaKey;
import com.singlee.ifs.service.IfsQuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsQuotaServiceImpl implements IfsQuotaService {

	@Autowired
	private IfsQuotaMapper ifsQuotaMapper;
	
	@Override
	public Page<IfsQuota> searchIfsQuotaList(Map<String, Object> params) {
		Page<IfsQuota> page = new Page<IfsQuota>();
		page = ifsQuotaMapper.searchIfsQuotaPage(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public IfsQuota selectByPrimaryKey(IfsQuotaKey key) {
		return ifsQuotaMapper.selectByPrimaryKey(key);
	}

	@Override
	public int saveOrUpdateIfsQuota(IfsQuota ifsQuota) {
		IfsQuotaKey key =new IfsQuotaKey();
		key.setUserId(ifsQuota.getUserId());
		key.setProduct(ifsQuota.getProduct());
		key.setProdType(ifsQuota.getProdType());
		key.setCost(ifsQuota.getCost());
		IfsQuota obj = ifsQuotaMapper.selectByPrimaryKey(key);
		if(obj == null) {
			return ifsQuotaMapper.insertSelective(ifsQuota);
		}else {
			return ifsQuotaMapper.updateByPrimaryKeySelective(ifsQuota);
		}
	}

	@Override
	public int deleteIfsQuota(IfsQuotaKey key) {
		return ifsQuotaMapper.deleteByPrimaryKey(key);
	}
	
}