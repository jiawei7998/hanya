/**
 * Project Name:capitalweb
 * File Name:IfsOpicsSettleManageServiceImpl.java
 * Package Name:com.singlee.ifs.service.impl
 * Date:2018-9-7下午04:12:10
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlCspiCsriBean;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlSdvpBean;
import com.singlee.financial.opics.ICspiCsriSdvpServer;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsSettleManageService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName:IfsOpicsSettleManageServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2018-9-7 下午04:12:10 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 * @see
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsOpicsSettleManageServiceImpl implements IfsOpicsSettleManageService {
	@Autowired
	private IfsOpicsSettleManageMapper opicsSettleManageMapper;
	@Autowired
	ICspiCsriSdvpServer iCspiCsriSdvpServer;

	@Override
	public void saveCspiAndCsri(IfsCspiCsriHead record) {
		String fedealno = getFedealno("");
		if ("P".equals(record.getPayrecind())) {
			record.setServer(SlDealModule.SETTLE.SERVER_CSPI);
		} else {
			record.setServer(SlDealModule.SETTLE.SERVER_CSRI);
		}
		record.setFedealno(fedealno);
		record.setSeq("0");
		record.setInoutind(SlDealModule.SETTLE.INOUTIND);
		record.setSlflag("1");// 已经办待复核
		record.setStatus("0");
		record.setAddupdelind("A");
		String curDate = opicsSettleManageMapper.getCurDate();
		record.setSlidate(DateUtil.parse(curDate, "yyyy-MM-dd"));
		opicsSettleManageMapper.insertCspiCsri(record);
		List<IfsCspiCsriDetail> cspiCsriDetailList = record.getCspiCsriDetailList();
		for (int i = 0; i < cspiCsriDetailList.size(); i++) {
			IfsCspiCsriDetail ifsCspiCsriDetail = cspiCsriDetailList.get(i);
			ifsCspiCsriDetail.setFedealno(fedealno);
			ifsCspiCsriDetail.setSeq(String.valueOf(i));
			ifsCspiCsriDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
			ifsCspiCsriDetail.setBr(record.getBr());
			ifsCspiCsriDetail.setProduct(record.getProduct());
			ifsCspiCsriDetail.setProdtype(record.getProdtype());
			ifsCspiCsriDetail.setPayrecind(record.getPayrecind());
			ifsCspiCsriDetail.setCno(record.getCno());
			ifsCspiCsriDetail.setCcy(record.getCcy());
			if ("P".equals(record.getPayrecind())) {
				ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSPI);
				opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
			} else {
				ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSRI);
				if (!(StringUtil.isEmpty(ifsCspiCsriDetail.getBic()) && StringUtil.isEmpty(ifsCspiCsriDetail.getP1())
						&& StringUtil.isEmpty(ifsCspiCsriDetail.getP2())
						&& StringUtil.isEmpty(ifsCspiCsriDetail.getP3())
						&& StringUtil.isEmpty(ifsCspiCsriDetail.getP4()))) {
					opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
				}

			}

		}

	}

	public String getFedealno(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("sStr", "");
		hashMap.put("trdType", "");
		String fedealno = opicsSettleManageMapper.getTradeId(hashMap);
		return fedealno;
	}

	@Override
	public Page<IfsCspiCsriHead> searchPageCspiAndCsri(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCspiCsriHead> result = opicsSettleManageMapper.searchPageCspiAndCsri(map, rb);
		return result;
	}

	@Override
	public void updateCspiAndCsri(IfsCspiCsriHead record) {
		if ("2".equals(record.getStatus()) || "3".equals(record.getStatus())) {// 已导入opics的，修改需要重新生成fedealno
			record.setSlflag("1");// 已经办待复核
			record.setStatus("0");// 未同步
			String curDate = opicsSettleManageMapper.getCurDate();
			record.setSlidate(DateUtil.parse(curDate, "yyyy-MM-dd"));
			String fedealno = getFedealno("");
			record.setFedealno(fedealno);
			opicsSettleManageMapper.updateCspiCsri(record);
			opicsSettleManageMapper.deleteCspiCsriDetailById(record);
			List<IfsCspiCsriDetail> cspiCsriDetailList = record.getCspiCsriDetailList();
			for (int i = 0; i < cspiCsriDetailList.size(); i++) {
				IfsCspiCsriDetail ifsCspiCsriDetail = cspiCsriDetailList.get(i);
				ifsCspiCsriDetail.setFedealno(fedealno);
				ifsCspiCsriDetail.setSeq(String.valueOf(i));
				ifsCspiCsriDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
				ifsCspiCsriDetail.setBr(record.getBr());
				ifsCspiCsriDetail.setProduct(record.getProduct());
				ifsCspiCsriDetail.setProdtype(record.getProdtype());
				ifsCspiCsriDetail.setCno(record.getCno());
				ifsCspiCsriDetail.setCcy(record.getCcy());
				ifsCspiCsriDetail.setPayrecind(record.getPayrecind());
				if ("P".equals(record.getPayrecind())) {
					ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSPI);
					opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
				} else {
					ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSRI);
					if (!(StringUtil.isEmpty(ifsCspiCsriDetail.getBic())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP1())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP2())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP3())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP4()))) {
						opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
					}
				}

			}
		} else {
			opicsSettleManageMapper.updateCspiCsri(record);
			opicsSettleManageMapper.deleteCspiCsriDetailById(record);
			List<IfsCspiCsriDetail> cspiCsriDetailList = record.getCspiCsriDetailList();
			for (int i = 0; i < cspiCsriDetailList.size(); i++) {
				IfsCspiCsriDetail ifsCspiCsriDetail = cspiCsriDetailList.get(i);
				ifsCspiCsriDetail.setFedealno(record.getFedealno());
				ifsCspiCsriDetail.setSeq(String.valueOf(i));
				ifsCspiCsriDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
				ifsCspiCsriDetail.setBr(record.getBr());
				ifsCspiCsriDetail.setProduct(record.getProduct());
				ifsCspiCsriDetail.setProdtype(record.getProdtype());
				ifsCspiCsriDetail.setCno(record.getCno());
				ifsCspiCsriDetail.setCcy(record.getCcy());
				ifsCspiCsriDetail.setPayrecind(record.getPayrecind());
				if ("P".equals(record.getPayrecind())) {
					ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSPI);
					opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
				} else {
					ifsCspiCsriDetail.setServer(SlDealModule.SETTLE.SERVER_CSRI);
					if (!(StringUtil.isEmpty(ifsCspiCsriDetail.getBic())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP1())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP2())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP3())
							&& StringUtil.isEmpty(ifsCspiCsriDetail.getP4()))) {
						opicsSettleManageMapper.insertCspiCsriDetail(ifsCspiCsriDetail);
					}
				}

			}
		}

	}

	@Override
	public List<IfsCspiCsriDetail> searchCspiCsriDetails(Map<String, Object> map) {
		List<IfsCspiCsriDetail> list = opicsSettleManageMapper.searchCspiCsriDetails(map);
		return list;
	}

	@Override
	public void updateSlflag(String fedealno, String userid, Date date, String slflag) {
		if ("2".equals(slflag) || "0".equals(slflag)) {// 复核或退回经办
			opicsSettleManageMapper.updateVerSlflag(fedealno, userid, date, slflag);
		} else {
			opicsSettleManageMapper.updateSlflag(fedealno, userid, date, slflag);
		}

	}

	@Override
	public void deleteByFedealno(Map<String, Object> map) {
		opicsSettleManageMapper.deleteHeadByFedealno(map);
		opicsSettleManageMapper.deleteDetailByFedealno(map);

	}

	@Override
	public Page<IfsSdvpHead> searchPageSdvp(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsSdvpHead> result = opicsSettleManageMapper.searchPageSdvp(map, rb);
		return result;
	}

	@Override
	public List<IfsSdvpDetail> searchSdvpDetails(Map<String, Object> map) {
		List<IfsSdvpDetail> list = opicsSettleManageMapper.searchSdvpDetails(map);
		return list;
	}

	@Override
	public void saveSdvp(IfsSdvpHead record) {
		String fedealno = getFedealno("");
		record.setServer(SlDealModule.SETTLE.SERVER_SDVP);
		record.setFedealno(fedealno);
		record.setSeq("0");
		record.setInoutind(SlDealModule.SETTLE.INOUTIND);
		record.setSlflag("1");
		record.setStatus("0");
		record.setAddupddel("A");
		record.setSlidate(DateUtil.parse(getCurDate(), "yyyy-MM-dd"));
		opicsSettleManageMapper.insertSdvp(record);
		List<IfsSdvpDetail> sdvpDetailList = record.getSdvpDetailList();
		for (int i = 0; i < sdvpDetailList.size(); i++) {
			IfsSdvpDetail ifsSdvpDetail = sdvpDetailList.get(i);
			ifsSdvpDetail.setServer(SlDealModule.SETTLE.SERVER_SDVP);
			ifsSdvpDetail.setFedealno(fedealno);
			ifsSdvpDetail.setSeq(String.valueOf(i));
			ifsSdvpDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
			ifsSdvpDetail.setBr(record.getBr());
			ifsSdvpDetail.setProduct(record.getProduct());
			ifsSdvpDetail.setProdtype(record.getProdtype());
			ifsSdvpDetail.setCno(record.getCno());
			ifsSdvpDetail.setCcy(record.getCcy());
			ifsSdvpDetail.setDelrecind(record.getDelrecind());
			ifsSdvpDetail.setSafekeepacct(record.getSafekeepacct());
			opicsSettleManageMapper.insertSdvpDetail(ifsSdvpDetail);
		}
	}

	@Override
	public void updateSdvp(IfsSdvpHead record) {
		if ("2".equals(record.getStatus()) || "3".equals(record.getStatus())) {// 已导入opics的，修改需要重新生成fedealno
			record.setSlflag("1");// 已经办待复核
			record.setStatus("0");// 未同步
			String fedealno = getFedealno("");
			record.setFedealno(fedealno);
			String curDate = opicsSettleManageMapper.getCurDate();
			record.setSlidate(DateUtil.parse(curDate, "yyyy-MM-dd"));
			opicsSettleManageMapper.updateSdvp(record);
			opicsSettleManageMapper.deleteSdvpDetailById(record);
			List<IfsSdvpDetail> sdvpDetailList = record.getSdvpDetailList();
			for (int i = 0; i < sdvpDetailList.size(); i++) {
				IfsSdvpDetail ifsSdvpDetail = sdvpDetailList.get(i);
				ifsSdvpDetail.setServer(SlDealModule.SETTLE.SERVER_SDVP);
				ifsSdvpDetail.setFedealno(fedealno);
				ifsSdvpDetail.setSeq(String.valueOf(i));
				ifsSdvpDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
				ifsSdvpDetail.setBr(record.getBr());
				ifsSdvpDetail.setProduct(record.getProduct());
				ifsSdvpDetail.setProdtype(record.getProdtype());
				ifsSdvpDetail.setCno(record.getCno());
				ifsSdvpDetail.setCcy(record.getCcy());
				ifsSdvpDetail.setDelrecind(record.getDelrecind());
				ifsSdvpDetail.setSafekeepacct(record.getSafekeepacct());
				opicsSettleManageMapper.insertSdvpDetail(ifsSdvpDetail);
			}
		} else {
			opicsSettleManageMapper.updateSdvp(record);
			opicsSettleManageMapper.deleteSdvpDetailById(record);
			List<IfsSdvpDetail> sdvpDetailList = record.getSdvpDetailList();
			for (int i = 0; i < sdvpDetailList.size(); i++) {
				IfsSdvpDetail ifsSdvpDetail = sdvpDetailList.get(i);
				ifsSdvpDetail.setServer(SlDealModule.SETTLE.SERVER_SDVP);
				ifsSdvpDetail.setFedealno(record.getFedealno());
				ifsSdvpDetail.setSeq(String.valueOf(i));
				ifsSdvpDetail.setInoutind(SlDealModule.SETTLE.INOUTIND);
				ifsSdvpDetail.setBr(record.getBr());
				ifsSdvpDetail.setProduct(record.getProduct());
				ifsSdvpDetail.setProdtype(record.getProdtype());
				ifsSdvpDetail.setCno(record.getCno());
				ifsSdvpDetail.setCcy(record.getCcy());
				ifsSdvpDetail.setDelrecind(record.getDelrecind());
				ifsSdvpDetail.setSafekeepacct(record.getSafekeepacct());
				opicsSettleManageMapper.insertSdvpDetail(ifsSdvpDetail);
			}
		}

	}

	@Override
	public void updateSdvpSlflag(String fedealno, String userid, Date date, String slflag) {
		if ("2".equals(slflag) || "0".equals(slflag)) {// 复核或退回经办
			opicsSettleManageMapper.updateSdvpVerSlflag(fedealno, userid, date, slflag);
		} else {
			opicsSettleManageMapper.updateSdvpSlflag(fedealno, userid, date, slflag);
		}
	}

	@Override
	public void deleteSdvpByFedealno(Map<String, Object> map) {
		opicsSettleManageMapper.deleteSdvpByFedealno(map);
		opicsSettleManageMapper.deleteSdvpDetailByFedealno(map);
	}

	@Override
	public Page<IfsSdvpDetail> searchPageSdvpDetails(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsSdvpDetail> result = opicsSettleManageMapper.searchPageSdvpDetails(map, rb);
		return result;
	}

	@Override
	public Page<IfsCspiCsriDetail> searchPageCspiAndCsriDetails(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCspiCsriDetail> result = opicsSettleManageMapper.searchPageCspiAndCsriDetails(map, rb);
		return result;
	}

	@Override
	public IfsCspiCsriHead searchCspiCsriHeadById(String fedealno) {
		return opicsSettleManageMapper.searchCspiCsriHeadById(fedealno);
	}

	@Override
	public void updateSyncStatus(IfsCspiCsriHead cspiCsriHead) {
		opicsSettleManageMapper.updateSyncStatus(cspiCsriHead);

	}

	@Override
	public void updateSdvpSyncStatus(IfsSdvpHead record) {
		opicsSettleManageMapper.updateSdvpSyncStatus(record);
	}

	@Override
	public IfsSdvpHead searchSdvpHeadById(String fedealno) {
		return opicsSettleManageMapper.searchSdvpHeadById(fedealno);
	}

	@Override
	public void updateCspiCsriByFedealno(Map<String, Object> opicsmap) {
		opicsSettleManageMapper.updateCspiCsriByFedealno(opicsmap);
	}

	@Override
	public void updateSdvpByFedealno(Map<String, Object> opicsmap) {
		opicsSettleManageMapper.updateSdvpByFedealno(opicsmap);
	}

	@Override
	public String getCurDate() {
		return opicsSettleManageMapper.getCurDate();
	}

	@Override
	public int getCspiCsriById(IfsCspiCsriHead record) {
		return opicsSettleManageMapper.getCspiCsriById(record);
	}

	@Override
	public int getSdvpById(IfsSdvpHead record) {
		return opicsSettleManageMapper.getSdvpById(record);
	}

	@Override
	public String checkSearchSettInfoAll(String prdNo, String ticketId) {
		String msg = "";
		try {
			if ("dp".equals(prdNo)) {
				List<IfsSearchSettleBean> settleBeanList = opicsSettleManageMapper.searchProdLimitList(ticketId);
				for (IfsSearchSettleBean issb : settleBeanList) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("cno", issb.getCno());
					map.put("ccy", issb.getPccy());
					map.put("product", issb.getProduct());
					map.put("prodtype", issb.getProdtype());
					map.put("safekeepacct", issb.getSafekeepacct());
					List<IfsSdvpHead> headsList = opicsSettleManageMapper.searchSettInfoList(map);
					if (headsList.size() == 0) {
						msg = "交易对手为【" + issb.getCno() + "】的付款路径没有！";
						break;
					}
					for (IfsSdvpHead ish : headsList) {
						String settmeans = ish.getSettmeans() == null ? "" : ish.getSettmeans();
						String settacct = ish.getSettacct() == null ? "" : ish.getSettacct();
						if ("".equals(settmeans) || "".equals(settacct)) {
							msg = "交易对手为【" + issb.getCno() + "】的付款路径没有！";
							break;
						}
					}
					IfsSdvpHead heads = opicsSettleManageMapper.recsearchSettInfo(map);
					if (heads == null) {
						msg = "交易对手为【" + issb.getCno() + "】的收款路径没有！";
						break;
					} else {
						String settmeans = heads.getSettmeans() == null ? "" : heads.getSettmeans();
						String settacct = heads.getSettacct() == null ? "" : heads.getSettacct();
						if ("".equals(settmeans) || "".equals(settacct)) {
							msg = "交易对手为【" + issb.getCno() + "】的收款路径没有！";
							break;
						}
					}
					if (!"".equals(msg)) {
						break;
					}
				}
			} else if ("cbt".equals(prdNo) || "or".equals(prdNo) || "sl".equals(prdNo) || "cr".equals(prdNo)) {
				// 债券类
				IfsSearchSettleBean settleBean = opicsSettleManageMapper.searchProdLimit(ticketId);
				String product = settleBean.getProduct();
				String prodtype = settleBean.getProdtype();
				String cno = settleBean.getCno();
				String pccy = settleBean.getPccy();
				String rccy = settleBean.getRccy();
				String safekeepacct = settleBean.getSafekeepacct(); // 托管账户

				String mydircn = settleBean.getMydircn();
				String prdname = settleBean.getPrdname();
				settleBean.setMydircn(mydircn); // 本方方向
				settleBean.setPrdname(prdname); // 产品名称

				/* 查询付款清算路径 */
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("cno", cno);
				map.put("ccy", pccy);
				map.put("product", product);
				map.put("type", prodtype);
				map.put("safekeepacct", safekeepacct);
				SlSdvpBean headsList1 = iCspiCsriSdvpServer.searchpaysdvphead(map); // 付券收钱
				if (headsList1 == null) {
					if ("现券买卖".equals(prdname)) {
						if ("卖出".equals(mydircn)) {
							msg = "【" + ticketId + "】路径不存在";
						}
					} else {
						msg = "【" + ticketId + "】路径不存在";
					}
				}

				/* 查询收款清算路径 */
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("cno", cno);
				map2.put("ccy", rccy);
				map2.put("product", product);
				map2.put("prodtype", prodtype);
				map2.put("safekeepacct", safekeepacct);
				SlSdvpBean headsList2 = iCspiCsriSdvpServer.searchrecsdvphead(map2); // 收券付钱
				if (headsList2 == null) {
					if ("现券买卖".equals(prdname)) {
						if ("买入".equals(mydircn)) {
							msg = "【" + ticketId + "】路径不存在";
						}
					} else {
						msg = "【" + ticketId + "】路径不存在";
					}
				}
			} else if ("rmbswap".equals(prdNo) || "goldswap".endsWith(prdNo)) {
				// 外汇掉期、黄金掉期
				IfsSearchSettleBean settleBean = opicsSettleManageMapper.searchProdLimit(ticketId);
				String product = settleBean.getProduct();
				String prodtype = settleBean.getProdtype();
				String cno = settleBean.getCno();
				String pccy = settleBean.getPccy();
				String rccy = settleBean.getRccy();

				/* 近端付款清算路径 */
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("cno", cno);
				map.put("ccy", pccy);
				map.put("product", product);
				map.put("type", prodtype);
				SlCspiCsriBean headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 近端付款
				if (headsList1 == null) {
					map.put("product", "DEFSI");
					map.put("type", "DF");
					headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 近端付款
				}
				if (headsList1 == null) {
					msg = "【" + ticketId + "】路径不存在";
				}

				/* 近端收款清算路径 */
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("cno", cno);
				map2.put("ccy", rccy);
				map2.put("product", product);
				map2.put("type", prodtype);
				SlCspiCsriBean headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 近端收款
				if (headsList2 == null) {
					map2.put("product", "DEFSI");
					map2.put("type", "DF");
					headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 近端收款
				}
				if (headsList2 == null) {
					msg = "【" + ticketId + "】路径不存在";
				}

				/* 远端付款清算路径 */
				Map<String, Object> map3 = new HashMap<String, Object>();
				map3.put("cno", cno);
				map3.put("ccy", rccy);
				map3.put("product", product);
				map3.put("type", prodtype);
				SlCspiCsriBean headsList3 = iCspiCsriSdvpServer.searchpaycspicsrihead(map3); // 远端付款
				if (headsList3 == null) {
					map3.put("product", "DEFSI");
					map3.put("type", "DF");
					headsList3 = iCspiCsriSdvpServer.searchpaycspicsrihead(map3); // 远端付款
				}
				if (headsList3 == null) {
					msg = "【" + ticketId + "】路径不存在";
				}
				/* 远端收款清算路径 */
				Map<String, Object> map4 = new HashMap<String, Object>();
				map4.put("cno", cno);
				map4.put("ccy", pccy);
				map4.put("product", product);
				map4.put("type", prodtype);
				SlCspiCsriBean headsList4 = iCspiCsriSdvpServer.searchreccspicsrihead(map4); // 远端收款
				if (headsList4 == null) {
					map4.put("product", "DEFSI");
					map4.put("type", "DF");
					headsList4 = iCspiCsriSdvpServer.searchreccspicsrihead(map4); // 远端收款
				}
				if (headsList4 == null) {
					msg = "【" + ticketId + "】路径不存在";
				}
			} else {
				IfsSearchSettleBean settleBean = opicsSettleManageMapper.searchProdLimit(ticketId);
				String product = settleBean.getProduct();
				String prodtype = settleBean.getProdtype();
				String cno = settleBean.getCno();
				String pccy = settleBean.getPccy();
				String rccy = settleBean.getRccy();

				/* 查询付款ifs_cspicsri_head清算路径 */
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("cno", cno);
				map.put("ccy", pccy);
				map.put("product", product);
				map.put("type", prodtype);
				SlCspiCsriBean headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 付款
				if (headsList1 == null) {
					map.put("product", "DEFSI");
					map.put("type", "DF");
					headsList1 = iCspiCsriSdvpServer.searchpaycspicsrihead(map); // 付款
				}

				if (headsList1 == null) {
					msg = "【" + ticketId + "】路径不存在";
				} else {
					/* 查询收款ifs_cspicsri_head清算路径 */
					Map<String, Object> map2 = new HashMap<String, Object>();
					map2.put("cno", cno);
					map2.put("ccy", rccy);
					map2.put("product", product);
					map2.put("type", prodtype);
					SlCspiCsriBean headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 收款
					if (headsList2 == null) {
						map2.put("product", "DEFSI");
						map2.put("type", "DF");
						headsList2 = iCspiCsriSdvpServer.searchreccspicsrihead(map2); // 收款
					}
					if (headsList2 == null) {
						msg = "【" + ticketId + "】路径不存在";
					}
				}
			}
			// 外汇需要校验其他信息（付外币才校验）
			/*
			 * if ("".equals(msg)) { msg = checkFxFlag(ticketId, prdNo); }
			 */
			return msg;
		} catch (RemoteConnectFailureException e) {
			JY.raiseRException("RemoteConnectFailureException", e);
		} catch (Exception e) {
			JY.raiseRException("Exception", e);
		}
		return msg;
	}

}
