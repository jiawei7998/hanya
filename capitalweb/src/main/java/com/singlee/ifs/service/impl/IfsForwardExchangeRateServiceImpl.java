package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlIrevBean;
import com.singlee.financial.marketdata.bean.RateFeedTypes;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.ifs.mapper.IfsForwardExchangeRateMapper;
import com.singlee.ifs.model.IfsForwardExchangeRateBean;
import com.singlee.ifs.service.IfsForwardExchangeRateService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author ：chenguo
 * @date ：Created in 2021/9/17 11:00
 * @description：外汇远掉点
 * @modified By：
 * @version:
 */

@Service
public class IfsForwardExchangeRateServiceImpl  implements IfsForwardExchangeRateService {

    @Autowired
    IfsForwardExchangeRateMapper ifsForwardExchangeRateMapper;

    @Autowired
    MarketDataTransServer marketDataTransServer;


    @Autowired
    BatchDao batchDao;

    @Override
    public Page<IfsForwardExchangeRateBean> searchPageRate(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsForwardExchangeRateBean> result = ifsForwardExchangeRateMapper.searchPageRate(map, rb);
        return result;
    }

    @Override
    public String doBussExcelPropertyToDataBase(List<IfsForwardExchangeRateBean> rateBean) {
        int seccess = 0;
        int defeat = 0;
        for (int i = 0; i < rateBean.size(); i++) {
            String ccy = rateBean.get(i).getCcy();
            String postdate = rateBean.get(i).getPostdate();
            String isBuy = rateBean.get(i).getIsBuy();
            IfsForwardExchangeRateBean rateBean1 = new IfsForwardExchangeRateBean();
            rateBean1.setPostdate(postdate);
            rateBean1.setCcy(ccy);
            rateBean1.setIsBuy(isBuy);
            if (ccy != "") {
                if (ifsForwardExchangeRateMapper.select(rateBean1).size() == 0) { // 判断表中是否有同样数据
                    // 若表中没有，则插入
                    ifsForwardExchangeRateMapper.insert(rateBean.get(i));
                    seccess = seccess + 1;
                } else {
                    defeat = defeat + 1;
                }
            } else {
                defeat = defeat + 1;
            }
        }
        if (defeat > 0) {
            return "插入失败" + defeat + "条(该远掉点信息已存在或没有输入市场数据)";
        }
        return "插入成功" + seccess + "条";
    }

    @Override
    public String dataBaseToIrev(String date) {
        Map<String, Object> remap = new HashMap<>();
        remap.put("OPICS", SystemProperties.opicsUserName);

        List<SlIrevBean> irevBeans = new ArrayList<SlIrevBean>();
        List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
        String result = "插入成功";
        int count = 0;
        Date postdate = null;
        // 当前系统日期
        postdate = DateUtil.parse(date);
        // ISEL表最指定日期最大流水号
        count = marketDataTransServer.getMaxFedealNo(postdate, RateFeedTypes.FXRATE);
        List<Map<String, String>> revps = ifsForwardExchangeRateMapper.getRevp(remap);
        for (Map<String, String> revp : revps) {
            SlIrevBean irevBean = new SlIrevBean(revp.get("BR"));
            Map<String, Object> rem = new HashMap<>();
            rem.put("ccy", revp.get("CCY") + "/CNY");
            rem.put("postdate", date);
            List<IfsForwardExchangeRateBean> rateList = ifsForwardExchangeRateMapper.searchRateList(rem);
            if (rateList.size() != 2) {
                continue;
            }

            IfsForwardExchangeRateBean rate1 = rateList.get(0);
            IfsForwardExchangeRateBean rate2 = rateList.get(1);
            BigDecimal num = new BigDecimal("2");
            irevBean.setCcy(revp.get("CCY"));
            irevBean.setXccy("CNY");
            SlInthBean inthBean = irevBean.getInthBean();
            inthBean.setPriority("1");
            count++;
            // 初始化接口对象
            inthBean.setFedealno(marketDataTransServer.createFedealno(postdate, count));
            if(rate1.getSpotRate()==null||rate1.getSpotRate()==null){
                 continue;
            }
            //即期汇率
            BigDecimal spotrate = (new BigDecimal(rate1.getSpotRate()).add(new BigDecimal(rate2.getSpotRate()))).divide(num);
            irevBean.setSpotrate(spotrate.toString());// 即期汇率
            irevBean.setPeriod1Rate(Getrat(rate1.getPeriod2Rate(),rate2.getPeriod2Rate()));//1D
            irevBean.setPeriod2Rate(Getrat(rate1.getPeriod4Rate(),rate2.getPeriod4Rate()));//1W
            irevBean.setPeriod3Rate(Getrat(rate1.getPeriod6Rate(),rate2.getPeriod6Rate()));//1M
            irevBean.setPeriod4Rate(Getrat(rate1.getPeriod8Rate(),rate2.getPeriod8Rate()));//3M
            irevBean.setPeriod5Rate(Getrat(rate1.getPeriod9Rate(),rate2.getPeriod9Rate()));//6M
            irevBean.setPeriod6Rate(Getrat(rate1.getPeriod10Rate(),rate2.getPeriod10Rate()));//9M
            irevBean.setPeriod7Rate(Getrat(rate1.getPeriod11Rate(),rate2.getPeriod11Rate()));//1Y
            irevBean.setLstmntdate(date);
            inthBeans.add(inthBean);
            irevBeans.add(irevBean);
        }
        // 批量提交
       Boolean flag= marketDataTransServer.insertToIrev(irevBeans, inthBeans);
        if (flag==false){
            result="插入失败";
        }
        return result;

    }

     public String Getrat(String rate1,String rate2) {
         BigDecimal num = new BigDecimal("2");
         BigDecimal Rate1 = new BigDecimal(rate1);
         BigDecimal Rate2 = new BigDecimal(rate2);
         BigDecimal multiple = new BigDecimal("10000");
         //利率=（（买入价+卖出价）/2）/10000
         String result=(((Rate1.add(Rate2)).divide(num)).divide(multiple)).setScale(8,BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString();

       return result;
     }


}
