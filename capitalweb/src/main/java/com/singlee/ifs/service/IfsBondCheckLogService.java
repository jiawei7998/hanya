package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBondCheckLog;

import java.util.Map;

/***
 * 复核日志 service
 * @author singlee4
 *
 */
public interface IfsBondCheckLogService {
	
	//新增
	public void insert(IfsBondCheckLog entity);
		
	//分页查询
	Page<IfsBondCheckLog> searchPageCheckLog(Map<String,Object> map);
	
	
	
	
	
	
	
	
}
