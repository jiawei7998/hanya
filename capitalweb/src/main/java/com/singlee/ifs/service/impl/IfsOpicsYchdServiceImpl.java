package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlYchdBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.baseUtil.noBlankInString;
import com.singlee.ifs.mapper.IfsOpicsYchdMapper;
import com.singlee.ifs.model.IfsOpicsYchd;
import com.singlee.ifs.service.IfsOpicsYchdService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class IfsOpicsYchdServiceImpl implements IfsOpicsYchdService {
	@Resource
	IfsOpicsYchdMapper ifsOpicsYchdMapper;
	@Autowired
	IStaticServer iStaticServer;
	
	
	@Override
	public Page<IfsOpicsYchd> searchPageOpicsYchd(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsYchd> result = ifsOpicsYchdMapper.searchPageOpicsYchd(map, rb);
		return result;
	}

	@Override
	public SlOutBean batchCalibration(String type, List<IfsOpicsYchd> list) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.YCHDSUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.YCHDSUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			//从opics库查询所有的数据
			List<SlYchdBean> listYchdOpics = iStaticServer.synchroYchd();
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
			
			/*****  对选中的行进行校准 ****************************/
			if("1".equals(type)){//选中行校准
				
				for(int i=0;i<list.size();i++){
					for(int j=0;j<listYchdOpics.size();j++){
						//若opics库里有相同的利率曲线,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if(listYchdOpics.get(j).getYieldcurve().trim().equals(list.get(i).getYieldcurve()) &&
						   listYchdOpics.get(j).getBr().trim().equals(SlDealModule.BASE.BR) &&
						   listYchdOpics.get(j).getCcy().trim().equals(list.get(i).getCcy())){
							IfsOpicsYchd entity = new IfsOpicsYchd();
							Map<String,Object> keyMap = new HashMap<String,Object>();
							keyMap.put("br",SlDealModule.BASE.BR);
							keyMap.put("yieldcurve",list.get(i).getYieldcurve() );
							keyMap.put("ccy", list.get(i).getCcy());
							entity = ifsOpicsYchdMapper.searchById(keyMap);
							//entity.setRatecode(listYchdOpics.get(j).getRatecode().trim());
							//去掉opics库 中实体类中参数为STRING类型的属性的前后空格
							noBlankInString.stripStringProperty(listYchdOpics.get(j));
							//opics库中的实体转成map
							Map<String, Object> map1 = BeanUtil.beanToMap(listYchdOpics.get(j));
							//其中中文描述、备注还是用本地的,不更新
							map1.put("status","1");
							map1.put("operator",SlSessionHelper.getUserId());
							map1.put("remark",entity.getRemark());
							map1.put("yieldcurvecn",entity.getYieldcurvecn());
							//把map转成本地库对应的实体类
							entity =  (IfsOpicsYchd) BeanUtil.mapToBean(IfsOpicsYchd.class, map1) ;  
							
							
							ifsOpicsYchdMapper.updateById(entity);
							break;
						}
						//若opics库里没有，就改变同步状态为   未同步,其余不改变
						if(j == listYchdOpics.size()-1){
							Map<String,Object> keyMap1 = new HashMap<String,Object>();
							keyMap1.put("br",SlDealModule.BASE.BR);
							keyMap1.put("yieldcurve",list.get(i).getYieldcurve() );
							keyMap1.put("ccy", list.get(i).getCcy());
							ifsOpicsYchdMapper.updateStatus0ById(keyMap1);
						}
						
					}
					
				}
					
				
			/*************  对本地库所有利率曲线记录进行校准***************/	
			}else{//全部校准
				//从本地库查询所有的数据
				List<IfsOpicsYchd> listRateLocal = ifsOpicsYchdMapper.searchAllOpicsYchd();
				/**1.以opics库为主，更新或插入本地库*/
				for(int i=0;i<listYchdOpics.size();i++){
					
					/*****++++++++++++++++++++  实体设置  start ++++++++++++++++++++++***********/
					IfsOpicsYchd entity = new IfsOpicsYchd();
					
					//去掉实体类中参数为STRING类型的属性的前后空格
					noBlankInString.stripStringProperty(listYchdOpics.get(i));
					//opics库中的实体转成map
					Map<String, Object> map1 = BeanUtil.beanToMap(listYchdOpics.get(i));
					//其中中文描述、备注还是用本地的,不更新
					map1.put("status","1");
					map1.put("operator",SlSessionHelper.getUserId());
					map1.put("remark",entity.getRemark());
					map1.put("yieldcurvecn",entity.getYieldcurvecn());
					//把map转成本地库对应的实体类
					entity =  (IfsOpicsYchd) BeanUtil.mapToBean(IfsOpicsYchd.class, map1) ;
					
					/*****++++++++++++++++++++  实体设置  end +++++++++++++++++++++++***********/
					
					
					if(listRateLocal.size() == 0){//本地库无数据
						ifsOpicsYchdMapper.insert(entity);
					}else{//本地库有数据
						for(int j=0;j<listRateLocal.size();j++){
							
							//opics库中利率曲线有与本地的利率曲线相等，就更新本地的利率曲线其他的内容
							if(listYchdOpics.get(i).getYieldcurve().trim().equals(listRateLocal.get(j).getYieldcurve())&&
								listYchdOpics.get(i).getBr().trim().equals(listRateLocal.get(j).getBr()) &&
								listYchdOpics.get(i).getCcy().trim().equals(listRateLocal.get(j).getCcy())){
								ifsOpicsYchdMapper.updateById(entity);
								break;
							}
							//若没有，就插入本地库
							if(j == listRateLocal.size()-1){
								ifsOpicsYchdMapper.insert(entity);
							}
						}
						
					}
					
				}
				/**2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步*/
				//遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				List<IfsOpicsYchd> listYchdLocal2 = ifsOpicsYchdMapper.searchAllOpicsYchd();
				if(listYchdLocal2.size() > listYchdOpics.size()){
					for(int i=0;i<listYchdLocal2.size();i++){
						for(int j=0;j<listYchdOpics.size();j++){
							if(listYchdOpics.get(j).getYieldcurve().trim().equals(listYchdLocal2.get(i).getYieldcurve())&&
								listYchdOpics.get(j).getBr().trim().equals(SlDealModule.BASE.BR) && 
								listYchdOpics.get(j).getCcy().trim().equals(listYchdLocal2.get(i).getCcy())){
								break;
							}
							//若opics库里没有，就改变同步状态为   未同步
							if(j == listYchdOpics.size()-1){
								
								listYchdLocal2.get(i).setStatus("0");
								listYchdLocal2.get(i).setOperator(SlSessionHelper.getUserId());
								listYchdLocal2.get(i).setLstmntdte(new Date());
								
								ifsOpicsYchdMapper.updateById(listYchdLocal2.get(i));
								
							}
							
						}
						
					}
					
				}
				
				
			}
		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
		

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
