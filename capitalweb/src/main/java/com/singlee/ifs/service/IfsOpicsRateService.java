package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsRate;

import java.util.Map;

/***
 * 利率代码
 * @author lij
 *
 */
public interface IfsOpicsRateService {
	//分页查询
	Page<IfsOpicsRate> searchPageOpicsRate(Map<String,Object> map);
	
	//批量校准
	SlOutBean batchCalibration(String type,String[] ratecodes, String[] brs);

}
