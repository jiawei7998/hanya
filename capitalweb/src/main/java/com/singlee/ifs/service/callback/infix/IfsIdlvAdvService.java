package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlDepositsAndLoansBean;
import com.singlee.financial.bean.SlExternalBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsAdvIdlvMapper;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIboMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsAdvIdlv;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拆借提前还款
 *
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IfsIdlvAdvService extends IfsApproveServiceBase {

	@Autowired
    private IfsAdvIdlvMapper ifsAdvIdlvMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IMmServer mmServer;
	@Autowired
	IfsCfetsfxLendMapper lendMapper;
	@Autowired
	IfsCfetsrmbIboMapper iboMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		
		IfsAdvIdlv ifsAdvIdlv = new IfsAdvIdlv();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsAdvIdlv = ifsAdvIdlvMapper.searchIdlvById(map);
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsAdvIdlvMapper.updateIdlvStatusByID(serial_no, status, date);
		if (null == ifsAdvIdlv) {
			return;
		}
		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}
				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				String callType = sysList.get(0).getP_type().trim();
				// 调用java代码
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(SlDealModule.BASE.BR,
							SlDealModule.DL.SERVER_LV, SlDealModule.DL.TAG_LV_ALL, SlDealModule.DL.DETAIL_LV);
					// 设置dealno
					mmBean.getInthBean().setDealno(ifsAdvIdlv.getDealno());
					mmBean.getInthBean().setFedealno(ifsAdvIdlv.getFedealno());
					
					SlExternalBean externalBean = new SlExternalBean();
					externalBean.setProdcode(ifsAdvIdlv.getProduct());
					externalBean.setProdtype(ifsAdvIdlv.getProdType());
					
					mmBean.setExternalBean(externalBean);
					mmBean.getInstitutionInfo().setTradeId(ifsAdvIdlv.getSponsor());
					mmBean.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));
					mmBean.setInputtime(DateUtil.getCurrentTimeAsString());
					mmBean.setDealno(ifsAdvIdlv.getDealno());
					mmBean.setVdate(DateUtil.parse(ifsAdvIdlv.getVdate()));
					mmBean.setMdate(DateUtil.parse(ifsAdvIdlv.getMdate()));
					mmBean.setEmdate(DateUtil.parse(ifsAdvIdlv.getEmdate()));
					mmBean.setSettdate(DateUtil.parse(ifsAdvIdlv.getSettdate()));
					mmBean.setAdvAmt(ifsAdvIdlv.getAdvAmt());
					mmBean.setInterestAmt(ifsAdvIdlv.getInterestAmt());
					mmBean.setPenaltyAmt(ifsAdvIdlv.getPenaltyAmt());
					mmBean.setPayAmt(ifsAdvIdlv.getPayAmt());
					mmBean.setCapintind(ifsAdvIdlv.getCapintind());
					mmBean.setPrinincdecind(ifsAdvIdlv.getPrinincdecind());
					mmBean.setProrateind(ifsAdvIdlv.getProrateind());
					
					// 插入opics冲销表
					SlOutBean result = mmServer.mmAdv(mmBean);// 用注入的方式调用opics相关方法
					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
						return;
					} else {
						String ticketId = StringUtils.trimToEmpty(ifsAdvIdlv.getFedealno());
						if (!"".equals(ticketId)) {
							if (ifsAdvIdlv.getDealType().endsWith("LEND")) {
								// 外币拆借
								lendMapper.updateApproveStatusFxLendByID(ticketId);
							} else {
								// 信用拆借
								iboMapper.updateApproveStatusRmbIboByID(ticketId);
							}
						}
					}
				} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程

				} else {
					JY.raise("系统参数未配置,未进行交易导入处理......");
				}
			} catch (Exception e) {
				e.printStackTrace();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}
	}
}
