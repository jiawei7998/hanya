package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsBico;

import java.util.List;
import java.util.Map;

/***
 * 工业标准码
 * 
 * @author lij
 * 
 */
public interface IfsOpicsBicoService {
	// 新增
	public void insert(IfsOpicsBico entity);

	// 修改
	void updateById(IfsOpicsBico entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsBico> searchPageOpicsBico(Map<String, Object> map);

	// 根据id查询实体类
	IfsOpicsBico searchById(String id);

	// 更新同步状态
	void updateStatus(IfsOpicsBico entity);

	// 批量校准
	SlOutBean batchCalibration(String type, String[] bics);

	// 查询所有
	List<IfsOpicsBico> searchAllOpicsBico();
}