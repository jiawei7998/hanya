package com.singlee.ifs.service.impl;

import com.singlee.financial.bean.SlPmtqBean;
import com.singlee.ifs.mapper.IfsCustSettleMapper;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.service.IfsPmtqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsPmtqServiceImpl implements IfsPmtqService {

	@Autowired
	IfsTrdSettleMapper trdSettleMapper;
	@Autowired
	IfsCustSettleMapper custSettleMapper;

	@Override
	public void processPmtq(List<SlPmtqBean> list) {

		for (SlPmtqBean pmtqBean : list) {
			if (!"AR".equalsIgnoreCase(pmtqBean.getStatus()) && !"H".equalsIgnoreCase(pmtqBean.getStatus())) {
				return;
			}
			if (!"CNY".equalsIgnoreCase(pmtqBean.getCcy())) {
				return;
			}
			if (!"CNAPS".equalsIgnoreCase(pmtqBean.getSetmeans()) && !"DVP".equalsIgnoreCase(pmtqBean.getSetmeans()) && !"OTHERS".equalsIgnoreCase(pmtqBean.getSetmeans())) {
				return;
			}
			// 过滤人民币债券清算信息(对应OPICS 520，522，523报文)
			if (pmtqBean.getSwiftfmt().startsWith("5") || "9".equals(pmtqBean.getSwiftfmt().substring(2, 3))) {
				return;
			}
			// 判断收付标识
			if (!"P".equalsIgnoreCase(pmtqBean.getPayrecind()) && !"S".equalsIgnoreCase(pmtqBean.getPayrecind())) {
				return;
			}
			// 本方收款
			if ("R".equals(pmtqBean.getPayrecind())) {
				
			}
		}

	}
}
