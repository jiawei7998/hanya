package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.*;

import java.util.List;
import java.util.Map;


/**
 * 用于处理第三方接口的服务接口表单录入功能
 * 
 * @author shenzl
 * 
 */
public interface IFSApproveService {
	IfsDownPaket getDownPaket(Map<String, Object> map);

	List<IfsDownPaket> getDownPakets(Map<String, Object> map);

	/*
	 * 货币掉期
	 */

	public Page<IfsApprovefxCswap> searchCcySwapPage(Map<String, Object> map);

	public void deleteCcySwap(String ticketid);

	public void addCcySwap(IfsApprovefxCswap map);

	public void editCcySwap(IfsApprovefxCswap map);

	public IfsApprovefxCswap searchCcySwap(String ticketid);
	
	public IfsApprovefxCswap getCcySwapAndFlowIdById(Map<String,Object>  key);

	
	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxCswap> getCcySwapMinePage (Map<String, Object> params, int isFinished);
	/*
	 * 外币拆借
	 */

	public Page<IfsApprovefxLend> searchCcyLendingPage(Map<String, Object> map);

	public void deleteCcyLending(String ticketid);

	public void editCcyLending(IfsApprovefxLend map);

	public void addCcyLending(IfsApprovefxLend map);

	public IfsApprovefxLend searchCcyLending(String ticketid);
	

	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxLend> getCcyLendingMinePage (Map<String, Object> params, int isFinished);
	/*
	 * 外汇对即期
	 */

	public Page<IfsApprovefxOnspot> searchCcyOnSpotPage(Map<String, Object> map);

	public void deleteCcyOnSpot(String ticketid);

	public void addCcyOnSpot(IfsApprovefxOnspot map);

	public void editCcyOnSpot(IfsApprovefxOnspot map);

	public IfsApprovefxOnspot searchCcyOnSpot(String ticketid);
	/**
	 * 根据请求参数查询分页列表-外汇对即期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxOnspot> getCcyOnSpotMinePage (Map<String, Object> params, int isFinished);
	/*
	 * 外汇远期
	 */
	public Page<IfsApprovefxFwd> getCreditEdit(Map<String, Object> map);

	public IfsApprovefxFwd getCredit(String ticketId);

	public void addCreditCondition(IfsApprovefxFwd map);

	public void editCredit(IfsApprovefxFwd map);

	public void deleteCredit(String ticketId);
	
	public IfsApprovefxFwd getFwdAndFlowIdById(Map<String,Object>  key);

	/**
	 * 根据请求参数查询分页列表-外汇远期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxFwd> getFxFwdMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 外汇掉期
	 */

	public Page<IfsApprovefxSwap> getswapPage(Map<String, Object> map);

	public IfsApprovefxSwap getrmswap(String ticketId);

	public void addrmswap(IfsApprovefxSwap map);
	


	public void editrmswap(IfsApprovefxSwap map);

	public void deletermswap(String ticketId);
	
	public IfsApprovefxSwap getRmbSwapAndFlowIdById(Map<String,Object>  key);
	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxSwap> getFxSwapMinePage (Map<String, Object> params, int isFinished);
	

	/*
	 * 外汇即期
	 */

	public Page<IfsApprovefxSpt> getSpotPage(Map<String, Object> map);

	public IfsApprovefxSpt getSpot(String ticketId);

	public void addSpot(IfsApprovefxSpt map);

	public void editSpot(IfsApprovefxSpt map);

	public void deleteSpot(String ticketId);
	
	public IfsApprovefxSpt getSpotAndFlowIdById(Map<String,Object>  key);

	/**
	 * 根据请求参数查询分页列表-外汇即期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxSpt> getSpotMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 人民币期权
	 */

	public Page<IfsApprovefxOption> searchOptionPage(Map<String, Object> map);

	public IfsApprovefxOption searchOption(String ticketId);

	public void addOption(IfsApprovefxOption map);

	public void editOption(IfsApprovefxOption map);

	public void deleteOption(String ticketId);

	/**
	 * 根据请求参数查询分页列表-人民币期权-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxOption> getOptionMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 黄金拆借
	 */
	public Page<IfsApprovegoldLend> searchGoldLendPage(Map<String, Object> map);

	public void deleteGoldLend(String ticketid);

	public void addGoldLend(IfsApprovegoldLend map);

	public void editGoldLend(IfsApprovegoldLend map);

	public IfsApprovegoldLend searchGoldLend(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币头寸调拨-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovegoldLend> getGoldLendMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 外币头寸调拨
	 */

	public Page<IfsApprovefxAllot> searchIfsAllocatePage(Map<String, Object> map);

	public void deleteIfsAllocate(String ticketid);

	public void addIfsAllocate(IfsApprovefxAllot map);

	public void editIfsAllocate(IfsApprovefxAllot map);

	public IfsApprovefxAllot searchIfsAllocate(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币头寸调拨-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxAllot> getAllocateMinePage (Map<String, Object> params, int isFinished);
	/*
	 * 外币债
	 */

	public Page<IfsApprovefxDebt> searchIfsDebtPage(Map<String, Object> map);

	public void deleteIfsDebt(String ticketid);

	public void addIfsDebt(IfsApprovefxDebt map);

	public void editIfsDebt(IfsApprovefxDebt map);

	public IfsApprovefxDebt searchIfsDebt(String ticketid);

	
	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovefxDebt> getDebtMinePage (Map<String, Object> params, int isFinished);
	
	/*
	 * 黄金即远掉期
	 */
	public void insertGold(IfsApprovemetalGold record);

	public void updateGold(IfsApprovemetalGold record);

	public void deleteGold(IfsApprovemetalGold key);

	public Page<IfsApprovemetalGold> getGoldList(IfsApprovemetalGold record);

	public IfsApprovemetalGold getGoldById(IfsApprovemetalGold key);
	
	public IfsApprovemetalGold getGoldAndFlowById(Map<String, Object> key);
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovemetalGold> getMetalGoldMinePage (Map<String, Object> params, int isFinished);
	
	/*
	 * 现券买卖CBT
	 */
	public void insertCbt(IfsApprovermbCbt record);

	public void updateCbt(IfsApprovermbCbt record);

	public void deleteCbt(IfsApprovermbCbt key);

	public Page<IfsApprovermbCbt> getCbtList(IfsApprovermbCbt record);

	public IfsApprovermbCbt getCbtById(IfsApprovermbCbt key);

	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbCbt> getRmbCbtMinePage (Map<String, Object> params, int isFinished);

	public IfsApprovermbCbt getCbtAndFlowIdById(Map<String,Object>  key);


	/*
	 * 质押式回购CR
	 */
	public void insertCr(IfsApprovermbCr record);

	public void updateCr(IfsApprovermbCr record);

	public void deleteCr(IfsApprovermbCr key);

	public Page<IfsApprovermbCr> getCrList(IfsApprovermbCr record);

	public IfsApprovermbCr getCrById(IfsApprovermbCr key);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbCr> getRmbCrMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 信用拆借IBO
	 */
	public void insertIbo(IfsApprovermbIbo record);

	public void updateIbo(IfsApprovermbIbo record);

	public void deleteIbo(IfsApprovermbIbo key);

	public Page<IfsApprovermbIbo> getIboList(IfsApprovermbIbo record);

	public IfsApprovermbIbo getIboById(IfsApprovermbIbo key);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbIbo> getRmbIboMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 利率互换IRS
	 */
	public void insertIrs(IfsApprovermbIrs record);

	public void updateIrs(IfsApprovermbIrs record);

	public void deleteIrs(IfsApprovermbIrs key);

	public Page<IfsApprovermbIrs> getIrsList(IfsApprovermbIrs record);

	public IfsApprovermbIrs getIrsById(IfsApprovermbIrs key);
	/*   增加： 根据id获取实体对象和流程id*/
	public IfsApprovermbIrs getIrsAndFlowIdById(Map<String,Object>  key);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbIrs> getRmbIrsMinePage (Map<String, Object> params, int isFinished);
	/*
	 * 买断式回购OR
	 */
	public void insertOr(IfsApprovermbOr record);

	public void updateOr(IfsApprovermbOr record);

	public void deleteOr(IfsApprovermbOr key);

	public Page<IfsApprovermbOr> getOrList(IfsApprovermbOr record);

	public IfsApprovermbOr getOrById(IfsApprovermbOr key);
	
	/*   增加： 根据id获取实体对象和流程id*/
	public IfsApprovermbOr getOrAndFlowIdById(Map<String,Object>  key);
	
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbOr> getRmbOrMinePage (Map<String, Object> params, int isFinished);

	/*
	 * 债券借贷SL
	 */
	public String insertSl(IfsApprovermbSl record);

	public void updateSl(IfsApprovermbSl record);

	public void deleteSl(IfsApprovermbSl key);

	public Page<IfsApprovermbSl> getSlList(IfsApprovermbSl record);

	public IfsApprovermbSl getSlById(IfsApprovermbSl key);
	
	/**
	 * 根据请求参数查询分页列表-、我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author 
	 * @date 2013-3-22
	 */
	public Page<IfsApprovermbSl> getRmbSlMinePage (Map<String, Object> params, int isFinished);

	IfsApprovermbIbo getIboAndFlowIdById(Map<String, Object> key);

	IfsApprovermbSl getSlAndFlowIdById(Map<String, Object> key);

	IfsApprovermbCr getCrAndFlowIdById(Map<String, Object> key);

	IfsApprovegoldLend getGoldLendAndFlowById(Map<String, Object> key);

	IfsApprovefxDebt getDebtAndFlowById(Map<String, Object> key);

	IfsApprovefxOption searchRmbOptAndFlowIdById(Map<String, Object> key);

	IfsApprovefxLend searchRmbCcylendingAndFlowIdById(Map<String, Object> key);

	IfsApprovefxAllot searchRmbCcypaAndFlowIdById(Map<String, Object> key);

	IfsApprovefxOnspot searchRmbSpotAndFlowIdById(Map<String, Object> key);

	Page<IfsApprovermbIrs> getApprovePassedPage(Map<String, Object> params);

	Page<IfsApprovermbCbt> getApproveCbtPassedPage(Map<String, Object> params);

	Page<IfsApprovermbCr> getApproveCrPassedPage(Map<String, Object> params);

	Page<IfsApprovermbIbo> getApproveIboPassedPage(Map<String, Object> params);

	Page<IfsApprovermbOr> getApproveOrPassedPage(Map<String, Object> params);

	Page<IfsApprovermbSl> getApproveSlPassedPage(Map<String, Object> params);

	Page<IfsApprovegoldLend> getApproveGoldLendPassedPage(
			Map<String, Object> params);

	Page<IfsApprovemetalGold> getApprovemetalGoldPassedPage(
			Map<String, Object> params);

	Page<IfsApprovefxOption> getApprovefxOptionPassedPage(
			Map<String, Object> params);

	Page<IfsApprovefxSpt> getApprovefxSptPassedPage(Map<String, Object> params);

	Page<IfsApprovefxFwd> getApprovefxFwdPassedPage(Map<String, Object> params);

	Page<IfsApprovefxOnspot> getApproveOnspotPassedPage(
			Map<String, Object> params);

	Page<IfsApprovefxSwap> getApprovefxSwapPassedPage(Map<String, Object> params);

	Page<IfsApprovefxCswap> getApprovefxCswapPassedPage(
			Map<String, Object> params);

	Page<IfsApprovefxLend> getApprovefxLendPassedPage(Map<String, Object> params);

//	Page<Map<String, Object>> getDpMini(Map<String, Object> params);
	
	Page<IfsApprovefxAllot> getApprovefxAllotPassedPage(
			Map<String, Object> params);

	Page<IfsApprovefxDebt> getApprovefxDebtPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbDetailSl> searchBondSlDetails(Map<String, Object> params);

	Page<Map<String, Object>> getSlMini(Map<String, Object> params);

//	IfsApprovermbSl searchCfetsrmbSlByTicketId(Map<String, Object> map);
}
