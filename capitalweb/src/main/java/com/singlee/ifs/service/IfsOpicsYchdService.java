package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsYchd;

import java.util.List;
import java.util.Map;

/***
 * 利率曲线
 * @author lij
 *
 */
public interface IfsOpicsYchdService {
	
	//分页查询
	Page<IfsOpicsYchd> searchPageOpicsYchd(Map<String,Object> map);
	
	//批量校准
	SlOutBean batchCalibration(String type,List<IfsOpicsYchd> list);

}
