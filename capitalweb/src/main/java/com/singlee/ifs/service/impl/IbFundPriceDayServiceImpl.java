package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IbFundPriceDayMapper;
import com.singlee.ifs.model.IbFundPriceDay;
import com.singlee.ifs.service.IbFundPriceDayService;
import com.singlee.refund.service.CFtInfoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/10/14 16:18
 * @description：
 * @modified By：
 * @version:
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class IbFundPriceDayServiceImpl implements IbFundPriceDayService {

	@Autowired
	IbFundPriceDayMapper ibFundPriceDayMapper;

	@Autowired
	CFtInfoService cFtInfoServiceImpl;

	/*
	 * 分页查询
	 *
	 * */
	@Override
	public Page<IbFundPriceDay> getFundPricePage(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IbFundPriceDay> result = ibFundPriceDayMapper.getFundPricePage(map, rb);
		return result;
	}

	@Override
	public void insert(IbFundPriceDay ibFundPriceDay) {

		ibFundPriceDayMapper.insert(ibFundPriceDay);
	}

	@Override
	public void delete(IbFundPriceDay ibFundPriceDay) {
		ibFundPriceDayMapper.delete(ibFundPriceDay);


	}

	@Override
	public String doExcelToData(List<IbFundPriceDay> fundBean) {
		int seccess = 0;
		int oldseccess = 0;
		int defeat = 0;

		for (int i = 0; i < fundBean.size(); i++) {
			IbFundPriceDay fund = fundBean.get(i);
			String fundCode = fundBean.get(i).getFundCode();
			String postdate = fundBean.get(i).getPostdate();
			Map<String, Object> param = new HashMap<>();
			param.put("fundCode", fundCode);
			//判断原基金是否存在
			Integer count = cFtInfoServiceImpl.getCount(param);
			if (count == 0) {
				defeat++;
			} else {
				IbFundPriceDay ibFundPriceDay = ibFundPriceDayMapper.getFundPriceDay(fundCode, postdate);
				if (fund != null) {
					if (ibFundPriceDay == null) { // 判断表中是否有同样数据
						// 若表中没有，则插入
						ibFundPriceDayMapper.insert(fundBean.get(i));
						seccess = seccess + 1;
					} else {
						//先删除
						ibFundPriceDayMapper.delete(ibFundPriceDay);
						//再插入
						ibFundPriceDayMapper.insert(fundBean.get(i));
						oldseccess = oldseccess + 1;
					}
				} else {
					defeat = defeat + 1;
				}
			}
		}
		if (defeat > 0) {
			return "插入失败" + defeat + "条";
		}
		return "新增数据" + seccess + "条" + ",更新数据" + oldseccess + "条";

	}


}
