package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.ifs.mapper.IfsCfetsrmbDpMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDpOfferMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/***
 * 债券借贷 回调函数
 * 
 * @author singlee4
 * 
 */
@Service(value = "IfsRmbDpService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbDpService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;
	@Autowired
	ISecurServer securServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IfsCfetsrmbDpOfferMapper offerDpMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdEdDealLogMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	// 债券
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbDpMapper.updateRmbDpByID(serial_no, status, date);
		IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.searchBondLon(serial_no);
		if (null == ifsCfetsrmbDp) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			edCustManangeService.eduReleaseFlowService(serial_no);
			tdEdDealLogMapper.updateNoUseDealStatus(serial_no);// 修改回退,拒绝，撤销交易的历史交易记录
		}
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbDp.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbDp.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbDp.getApproveStatus());// 设置监控表-审批状态
			if ("CD".equals(ifsCfetsrmbDp.getInstrumentType())) {
				ifsFlowMonitor.setPrdName("存单发行");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("dpcd");// 设置监控表-产品编号：作为分类
			} else {
				ifsFlowMonitor.setPrdName("债券发行");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("dp");// 设置监控表-产品编号：作为分类
			}
			// ifsFlowMonitor.setAmt(String.valueOf(ifsCfetsrmbSl.getUnderlyingQty()==null?"":ifsCfetsrmbSl.getUnderlyingQty()));//设置监控表-本金
			// ifsFlowMonitor.setRate(String.valueOf(ifsCfetsrmbSl.getPrice()==null?"":ifsCfetsrmbSl.getPrice()));//设置监控表-利率
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbDp.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbDp.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
//				String custNo = ifsCfetsrmbDp.getBorrowInst();
//				if (null != custNo || "".equals(custNo)) {
//					ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
//					IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
//					ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)
//				}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_DP");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_DP where TICKET_ID=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbDpMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map3 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map3);

		}
		// 只有审批状态为'审批通过'才调用存储过程
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 生成多条记录
			List<IfsCfetsrmbDpOffer> ifsCfetsrmbDpOfferList = offerDpMapper.searchOfferList(serial_no);
			String callType = sysList.get(0).getP_type().trim();
			for (int i = 0; i < ifsCfetsrmbDpOfferList.size(); i++) {
				// 调用java代码
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					// 获取当前账务日期
					Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbDp.getBr());
					SlFixedIncomeBean slFixedIncomeBean = getEntry(ifsCfetsrmbDp, ifsCfetsrmbDpOfferList.get(i),
							branchDate);
					SlOutBean result = securServer.fi(slFixedIncomeBean);
					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise("审批未通过，插入opics失败......");
					} else {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("ticketId", ifsCfetsrmbDp.getTicketId());
						map.put("seq", ifsCfetsrmbDpOfferList.get(i).getSeq());
						map.put("satacode", "-1");
						offerDpMapper.updateStatcodeByTicketId(map);
					}
				} else {
					// 将实体对象转换成map
					Map<String, String> map = BeanUtils.describe(ifsCfetsrmbDp);
					SlOutBean result = securServer.bredProc(map);// 现券买卖存储过程
					if (!"999".equals(result.getRetCode())) {
						JY.raise("审批未通过，执行存储过程失败......");
					}
				}
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	/**
	 * 获取交易信息
	 * 
	 * @param ifsCfetsrmbDp
	 * @param dpOffer
	 * @param branchDate
	 * @return
	 */
	private SlFixedIncomeBean getEntry(IfsCfetsrmbDp ifsCfetsrmbDp, IfsCfetsrmbDpOffer dpOffer, Date branchDate) {
		SlFixedIncomeBean slFixedIncomeBean = new SlFixedIncomeBean(ifsCfetsrmbDp.getBr(), SlDealModule.FI.SERVER,
				SlDealModule.FI.TAG, SlDealModule.FI.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsCfetsrmbDpOfferList.get(i).getLendInst());
		contraPatryInstitutionInfo.setInstitutionId(dpOffer.getLendInst());
		slFixedIncomeBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slFixedIncomeBean.setSecsacct(ifsCfetsrmbDp.getBorrowCustname());
		/************* 设置交易方向 ***************************/
		slFixedIncomeBean.setPs(PsEnum.S);
//		if (ifsCfetsrmbCbt.getMyDir().endsWith("P")) {
//			slFixedIncomeBean.setPs(PsEnum.P);
//		}
//		if (ifsCfetsrmbCbt.getMyDir().endsWith("S")) {
//			slFixedIncomeBean.setPs(PsEnum.S);
//		}
		// TODO 投资类型没有：默认I发行类
		slFixedIncomeBean.setInvtype(ifsCfetsrmbDp.getInterestType());
		slFixedIncomeBean.setOrigamt(
				String.valueOf(MathUtil.roundWithNaN(Double.parseDouble(dpOffer.getShouldPayMoney() + ""), 2)));
		/************* 设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbDp.getBorrowTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbDp.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FI.IOPER);
		institutionInfo.setTradeId(trad);
		slFixedIncomeBean.getInthBean().setIoper(trad);
		slFixedIncomeBean.setInstitutionInfo(institutionInfo);
		/************* 设置债券信息 ***************************/
		SecurityInfoBean securityInfo = new SecurityInfoBean();
		securityInfo.setSecurityId(ifsCfetsrmbDp.getDepositCode());// 债券代码
		slFixedIncomeBean.setSecurityInfo(securityInfo);
		slFixedIncomeBean.setValueDate(DateUtil.parse(ifsCfetsrmbDp.getValueDate()));// 结算日：缴款日期
		slFixedIncomeBean.setDealDate(DateUtil.parse(ifsCfetsrmbDp.getForDate()));
		/*
		 * SecurityAmtDetailBean securityAmt=new SecurityAmtDetailBean();
		 * securityAmt.setPrice(ifsCfetsrmbCbt.getCleanPrice());
		 * slFixedIncomeBean.setSecurityAmt(securityAmt);
		 */
		SecurityAmtDetailBean securityAmt = new SecurityAmtDetailBean();
		slFixedIncomeBean.setSecurityAmt(securityAmt);
		/*
		 * if (!String.valueOf(ifsCfetsrmbCbt.getCleanPrice()).equals("0")) {
		 * securityAmt.setPrice(ifsCfetsrmbCbt.getCleanPrice()); } if
		 * (String.valueOf(ifsCfetsrmbCbt.getCleanPrice()).equals("0")) {
		 * slFixedIncomeBean.setOrigqty(String.valueOf(ifsCfetsrmbCbt.getTradeAmount()))
		 * ; }
		 */
//		slFixedIncomeBean.setOrigqty(String.valueOf(dpOffer.getOfferAmount()));
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(dpOffer.getCost());
		externalBean.setPort(dpOffer.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbDp.getContractId());// TODO 成交单编号同一个
		externalBean.setVerind(SlDealModule.FI.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(dpOffer.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(dpOffer.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(dpOffer.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(dpOffer.getCtrsacct());
		slFixedIncomeBean.setVerdate(branchDate);

		slFixedIncomeBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFixedIncomeBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbDp.getTicketId());
		inthBean.setLstmntdate(branchDate);
		inthBean.setSeq(dpOffer.getSeq());
		slFixedIncomeBean.setInthBean(inthBean);
		slFixedIncomeBean.setStandinstr("Y");
		slFixedIncomeBean.setUsualid("");
		// 判断是否延期交易
		IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbDp.getDepositCode());
		String settleDays = "0";
		if (ifsOpicsBond != null) {
			settleDays = ifsOpicsBond.getSettdays();
			settleDays = settleDays.trim();
		}
		if ("".equals(settleDays)) {
			settleDays = "0";
		}
		int betweendays = DateUtil.daysBetween(ifsCfetsrmbDp.getForDate(), ifsCfetsrmbDp.getValueDate());
		int IsettleDays = Integer.valueOf(settleDays);
		if (betweendays > IsettleDays) {
			slFixedIncomeBean.setDelaydelivind("Y");
		} else {
			slFixedIncomeBean.setDelaydelivind("N");
		}

		String unit = ifsOpicsBond != null && null != ifsOpicsBond.getSecunit() && !"".equals(ifsOpicsBond.getSecunit()) ?
				ifsOpicsBond.getSecunit() : "FMT";
		if("FMT".equals(unit)) {
			slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
					Double.parseDouble(dpOffer.getOfferAmount().divide(new BigDecimal(1),4,BigDecimal.ROUND_HALF_UP) + ""), 4)));
		}else {
			slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
					Double.parseDouble(dpOffer.getOfferAmount().divide(new BigDecimal(10000),4,BigDecimal.ROUND_HALF_UP) + ""), 4)));
		}

		return slFixedIncomeBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");

		String id = (String)paramer.get("id");
		if("jyzqfx".equals(id)){
			paramer.put("instrumentType","SE");
		}else if("jycdfx".equals(id)){
			paramer.put("instrumentType","CD");
		}

		List<IfsCfetsrmbDp> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbDpMapper.getRmbDpMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbDpMapper.getRmbDpMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbDpMapper.getRmbDpMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> basisList = taDictVoMapper.getTadictTree("Basis").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbDp ifsCfetsrmbDp: list) {
			ifsCfetsrmbDp.setBasis(basisList.get(ifsCfetsrmbDp.getBasis()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
