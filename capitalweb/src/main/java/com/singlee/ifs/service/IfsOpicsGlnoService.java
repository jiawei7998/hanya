package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsKCoreKm;

import java.util.Map;

public interface IfsOpicsGlnoService {

	// 分页查询
	Page<IfsOpicsKCoreKm> searchPageOpicsGlno(Map<String, Object> map);

}