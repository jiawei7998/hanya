package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsSecAcctMapper;
import com.singlee.ifs.model.IfsSecAcct;
import com.singlee.ifs.service.IfsSecAcctService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("ifsSecAcctServiceImpl")
public class IfsSecAcctServiceImpl implements IfsSecAcctService {
	@Autowired
	private IfsSecAcctMapper ifsSecAcctMapper;
	@Override
	public Page<IfsSecAcct> searchPageSecAcct(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsSecAcct> result = ifsSecAcctMapper.selectPageSecAcct(map, rb);
		return result;
	}

}
