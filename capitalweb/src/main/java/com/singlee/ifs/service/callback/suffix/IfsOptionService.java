package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IOtcServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.ifs.mapper.IfsCfetsfxOptionMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxOption;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(value = "IfsOptionService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsOptionService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IOtcServer otcServer;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdEdDealLogMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxOptionMapper.updateOptionByID(serial_no, status, date);
		IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsfxOption) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			edCustManangeService.eduReleaseFlowService(serial_no);
			tdEdDealLogMapper.updateNoUseDealStatus(serial_no);// 修改回退,拒绝，撤销交易的历史交易记录
		}
		// /////以下将审批中的记录插入审批监控表
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsfxOption.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsfxOption.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsfxOption.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("外汇期权");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("rmbopt");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifsCfetsfxOption.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsfxOption.getSponsor());// 设置监控表-审批发起人
			String custNo = ifsCfetsfxOption.getCno();
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_OPTION");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_OPTION where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxOptionMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);

		}

		/**
		 * ++++++++++++++++++ 以下代码为审批通过时直通到OPICS库
		 * +++++++++++++++++++++++++++++++++++++++++++++
		 */
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate("01");
				SlIotdBean slIotdBean = getEntry(ifsCfetsfxOption, branchDate);
				// 插入opics表
				SlOutBean result = otcServer.otc(slIotdBean);
				if (result.getRetStatus().equals(RetStatusEnum.S)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsfxOption.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxOptionMapper.updateStatcodeByTicketId(map);
				} else {
					JY.raise("插入opics表失败......");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.raiseRException("审批未通过......", e);
		}

	}

	/**
	 * 获取交易信息
	 * 
	 * @param ifsCfetsfxOption
	 * @param branchDate
	 * @return
	 */
	private SlIotdBean getEntry(IfsCfetsfxOption ifsCfetsfxOption, Date branchDate) {
		SlIotdBean slIotdBean = new SlIotdBean("01", SlDealModule.OTC.SERVER, SlDealModule.OTC.TAG,
				SlDealModule.OTC.DETAIL);
		/** 设置标识以及产品代码、产品类型、成本中心、投资组合等 */
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsfxOption.getCost());
		externalBean.setProdcode(ifsCfetsfxOption.getProduct());
		externalBean.setProdtype(ifsCfetsfxOption.getProdType());
		externalBean.setPort(ifsCfetsfxOption.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifsCfetsfxOption.getTicketId());
		externalBean.setDealtext(ifsCfetsfxOption.getContractId());
		externalBean.setAuthsi(SlDealModule.OTC.AUTHSIIND);
		externalBean.setSiind(SlDealModule.OTC.SIIND);
		externalBean.setSupconfind(SlDealModule.OTC.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.OTC.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.OTC.SUPRECIND);
		externalBean.setVerind(SlDealModule.OTC.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsfxOption.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsfxOption.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsfxOption.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsfxOption.getCtrsacct());
		slIotdBean.setExternalBean(externalBean);
		/** 设置流水号 */
		SlInthBean inthBean = slIotdBean.getInthBean();
		inthBean.setFedealno(ifsCfetsfxOption.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slIotdBean.setInthBean(inthBean);

		/** 设置买卖方向 */
		String ps = ifsCfetsfxOption.getDirection();
		if (ps.endsWith("P")) {
			slIotdBean.setPs((PsEnum.P).toString());
		}
		if (ps.endsWith("S")) {
			slIotdBean.setPs((PsEnum.S).toString());
		}
		/** 设置本方交易员 */
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsfxOption.getDealer());// 交易员id
		userMap.put("branchId", ifsCfetsfxOption.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		slIotdBean.setTrad(user.getOpicsTrad());
		/** 设置交易对手 */
		slIotdBean.setCno(ifsCfetsfxOption.getCounterpartyInstId());
		/** 设置交易日期 */
		slIotdBean.setDealDate(DateUtil.parse(ifsCfetsfxOption.getForDate()));
		/** 设置行权日 */
		slIotdBean.setExpDate(DateUtil.parse(ifsCfetsfxOption.getExpiryDate()));
		/** 设置行权截止时间 */
		slIotdBean.setCutOff(ifsCfetsfxOption.getCutOffTime());
		/** 设置交割日 */
		slIotdBean.setSettleDte(DateUtil.parse(ifsCfetsfxOption.getDeliveryDate()));
		/** 设置期权类型 */
		// slIotdBean.setOtcType(ifsCfetsfxOption.getOptionStrategy());
		slIotdBean.setOtcType("FX");
		/** 设置损益计算方法 */
		slIotdBean.setPlmethod(ifsCfetsfxOption.getPlmethod());
		/** 设置执行价格 */
		slIotdBean.setStrike_8(new BigDecimal(ifsCfetsfxOption.getStrikePrice()));
		/** 设置执行方式 EURO - 欧式 AMER - 美式 */
		slIotdBean.setStyle(ifsCfetsfxOption.getExerciseType());
		/** 设置币种1 */
		slIotdBean.setCcy(ifsCfetsfxOption.getCcy());
		/** 设置币种1金额 */
		slIotdBean.setCcyPrin(new BigDecimal(
				MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsfxOption.getBuyNotionalAmount().toString()), 2)));
		/** 设置币种1交易类型（看涨call/看跌put） */
		slIotdBean.setCcyCp(ifsCfetsfxOption.getTradingType());
		/** 设置币种2 */
		slIotdBean.setCtrCcy(ifsCfetsfxOption.getCtrccy());
		/** 设置币种2金额 */
		slIotdBean.setCtrPrin(new BigDecimal(
				MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsfxOption.getSellNotionalAmount().toString()), 2)));
		/** 设置期权类型optionStrategy */
		slIotdBean.setExotic(ifsCfetsfxOption.getOptionStrategy());
		/** 设置期权费币种 */
		slIotdBean.setPremCcy("CNY");
		/** 设置期权费金额 */
		slIotdBean.setPremAmt(
				new BigDecimal(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsfxOption.getPremiumAmount()), 2)));
		/** 设置默认地方时间 */
		slIotdBean.setLocation("BEIJING");
		/** 设置期限 */
		slIotdBean.setTenor("99");
		/** 设置DELTA_8 */
		slIotdBean.setDelta_8(new BigDecimal("10"));
		/** 设置RATE1_8 */
		slIotdBean.setRate1_8(new BigDecimal("10"));
		return slIotdBean;
	}
}
