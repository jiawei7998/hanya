package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.financial.wks.expand.FieldMapType;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.expand.SldaWksBean;
import com.singlee.financial.wks.intfc.SeclenService;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailSlMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbSlMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsFlowMonitorService;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/***
 * 债券借贷 回调函数
 * 
 * @author singlee4
 * 
 */
@Service(value = "IfsRmbSlService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbSlService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper;
	@Autowired
	SeclenService seclenService;
	@Autowired
	ISecurServer securServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IfsCfetsrmbDetailSlMapper detailSlMapper;
	@Autowired
	IfsOpicsBondMapper bondMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Autowired
	IfsFlowMonitorService ifsFlowMonitorService;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbSlMapper.updateRmbSlByID(serial_no, status, date);
		IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(serial_no);
		if (null == ifsCfetsrmbSl) {
			return;
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("S".equals(ifsCfetsrmbSl.getMyDir())){//借出
					if(!"0".equals(ifsCfetsrmbSl.getQuotaOccupyType())){
						hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
					}
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		
		// /////以下将审批中的记录插入审批监控表
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbSl.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbSl.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbSl.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("债券借贷");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("sl");// 设置监控表-产品编号：作为分类
			// ifsFlowMonitor.setAmt(String.valueOf(ifsCfetsrmbSl.getUnderlyingQty()==null?"":ifsCfetsrmbSl.getUnderlyingQty()));//设置监控表-本金
			// ifsFlowMonitor.setRate(String.valueOf(ifsCfetsrmbSl.getPrice()==null?"":ifsCfetsrmbSl.getPrice()));//设置监控表-利率
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbSl.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbSl.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbSl.getLendInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_SL");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_SL where TICKET_ID=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbSlMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("S".equals(ifsCfetsrmbSl.getMyDir())) {//借出
						if(!"0".equals(ifsCfetsrmbSl.getQuotaOccupyType())){
							LimitOccupy(serial_no, TradeConstants.ProductCode.SL);
						}
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			
		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map3 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map3);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbSl.getBr());
				SldhWksBean sldhWksBean = getEntryWksBean(ifsCfetsrmbSl, branchDate);

				SlOutBean outBean = seclenService.saveEntry(sldhWksBean);
				if (outBean.getRetStatus().equals(RetStatusEnum.S)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsrmbSl.getTicketId());
					map.put("dealno", outBean.getClientNo());
					map.put("satacode", "-4");
					ifsCfetsrmbSlMapper.updateStatcodeByTicketId(map);
				} else {
					JY.raise("审批未通过,导入OPICS异常......");
				}
			} else {
				// 将实体对象转换成map
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbSl);
				// 正式调用存储过程
				SlOutBean result = securServer.bredProc(map);// 债券借贷存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过......");
				}
			}
		} catch (Exception e) {
			JY.error(String.format("[%s]交易审批失败",serial_no), e);
			JY.raiseRException("审批未通过......", e);
		}
	}

	//直连
	private SldhWksBean getEntryWksBean(IfsCfetsrmbSl ifsCfetsrmbSl, Date branchDate) {
		/***************************交易主体****************************/
		SldhWksBean sldh =new SldhWksBean();
		//产品+类型+成本中心
//		sldh.setBook(ifsCfetsrmbSl.getProduct()+"_"+ifsCfetsrmbSl.getProdType()+"_"+ifsCfetsrmbSl.getCost());
//		//投资组合
//		sldh.setDesk(ifsCfetsrmbSl.getPort());
		//交易员
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbSl.getBorrowTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbSl.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		sldh.setTrad(user.getOpicsTrad());
		//机构编号
//		sldh.setInstId(ifsCfetsrmbSl.getBr());
		//交易编号
		sldh.setTradeNo(ifsCfetsrmbSl.getTicketId());
		//抵押券总量,多只券需要相加处理
		//sldh.setAssignQty(ifsCfetsrmbSl.getMarginTotalAmt());
		//计息基础
		sldh.setBasis(ifsCfetsrmbSl.getBasis());
		//交易货币
		sldh.setCcy("CNY");
		//交易对手
		sldh.setCno(ifsCfetsrmbSl.getLendInst());
		//交易日
		sldh.setDealDate(ifsCfetsrmbSl.getForDate());
		//起息日
		sldh.setVdate(ifsCfetsrmbSl.getFirstSettlementDate());
		//到期日
		sldh.setMatDate(ifsCfetsrmbSl.getSecondSettlementDate());
		//付息日
		sldh.setPayIntDate(ifsCfetsrmbSl.getInterestPaymentDate());
		//买卖方向
		sldh.setPs(ifsCfetsrmbSl.getMyDir());
//		sldh.setFieldMapType(FieldMapType.WKS);
		/***************************标的债券[借入/出的债券]*************************************/
		SldaWksBean slda = new SldaWksBean();
		IfsOpicsBond bond=bondMapper.searchById(ifsCfetsrmbSl.getUnderlyingSecurityId());

//		slda.setInstId(sldh.getInstId());
		slda.setDispricerateind("C");
//		slda.setBook(bond.getProduct()+"_"+bond.getProdtype()+"_"+ifsCfetsrmbSl.getBondCost());
//		slda.setDesk(ifsCfetsrmbSl.getBondPort());
		slda.setInvtype(ifsCfetsrmbSl.getInvestType());

		slda.setSecid(ifsCfetsrmbSl.getUnderlyingSecurityId());//债券号
		slda.setQty(ifsCfetsrmbSl.getUnderlyingQty());//手数
		slda.setFaceamt(ifsCfetsrmbSl.getUnderlyingQty().multiply(new BigDecimal(10000)));//标的债券面值
		sldh.setSlda(slda);

		/***************************抵押债列表****************************/
		List<SldaWksBean> sldaList = new ArrayList<SldaWksBean>();
		List<IfsCfetsrmbDetailSl> slList = detailSlMapper.searchBondByTicketId(ifsCfetsrmbSl.getTicketId());
		for (IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl : slList) {
			SldaWksBean sldaWksBean =new SldaWksBean();
			IfsOpicsBond secid=bondMapper.searchById(ifsCfetsrmbDetailSl.getMarginSecuritiesId());
//			sldaWksBean.setBook(secid.getProduct()+"_"+secid.getProdtype()+"_"+ifsCfetsrmbDetailSl.getCostb());
//			sldaWksBean.setDesk(ifsCfetsrmbDetailSl.getPortb());
			sldaWksBean.setInvtype(ifsCfetsrmbDetailSl.getInvtype());
			//全价/净价标识，C净价，D-全价录入的价格是以全价计算还是以净价计算
			sldaWksBean.setDispricerateind("D");
			//债券代码
			sldaWksBean.setSecid(ifsCfetsrmbDetailSl.getMarginSecuritiesId());
			//借入/借出债券交易手数
			sldaWksBean.setQty(ifsCfetsrmbDetailSl.getMarginAmt());
			//单指债券面值
			sldaWksBean.setFaceamt(ifsCfetsrmbDetailSl.getMarginAmt().multiply(new BigDecimal(10000)));
			sldaList.add(sldaWksBean);
		}
		sldh.setSldaList(sldaList);
		/**********************债券借贷费用对象*****************************/
//		sldh.setSettacct(ifsCfetsrmbSl.getSettleAcct());
//		sldh.setSettmeans(ifsCfetsrmbSl.getSettleMeans());
		FeeWksBean feesWksBean = new FeeWksBean();
//		feesWksBean.setInstId(sldh.getInstId());
//		feesWksBean.setBook(sldh.getBook());
//		feesWksBean.setDesk(sldh.getDesk());
//		feesWksBean.setCno(sldh.getCno());
//		feesWksBean.setCcy(sldh.getCcy());
//		feesWksBean.setAcctgMethod("A");
//		//费用金额
//		feesWksBean.setCcyAmt(ifsCfetsrmbSl.getMiscFeeType());
//		//费率
//		feesWksBean.setFeePer8(ifsCfetsrmbSl.getPrice());
//		//费用方向:支出|收取
//		feesWksBean.setFeeDir("SECLEN|FE");
//
//		feesWksBean.setStartDate(sldh.getVdate());
//		feesWksBean.setEndDate(sldh.getMatDate());
//		feesWksBean.setTrad(sldh.getTrad());
//
//		feesWksBean.setFieldMapType(FieldMapType.WKS);
		sldh.setFeesWksBean(feesWksBean);
		/**********************清算信息*****************************/
		SettleInfoWksBean settleAcct = new SettleInfoWksBean();
//		settleAcct.setHostCcyAwBiccode(ifsCfetsrmbSl.getBorrowPsnum());// 账户行,对于人民币填写大额行号
//		settleAcct.setHostCcyAwName(ifsCfetsrmbSl.getBorrowOpenBank());// 账户行
//		settleAcct.setHostCcyBeAcctno(ifsCfetsrmbSl.getBorrowAccnum());// 受益人,对于人民币填写帐号
//		settleAcct.setHostCcyBeName(ifsCfetsrmbSl.getBorrowAccname());// 受益人
//		settleAcct.setGuestCcyAwBiccode(ifsCfetsrmbSl.getLendPsnum());// 账户行,对于人民币填写大额行号
//		settleAcct.setGuestCcyAwName(ifsCfetsrmbSl.getLendOpenBank());// 账户行
//		settleAcct.setGuestCcyBeAcctno(ifsCfetsrmbSl.getLendAccnum());// 受益人,对于人民币填写帐号
//		settleAcct.setGuestCcyBeName(ifsCfetsrmbSl.getLendAccname());// 受益人
		sldh.setSettleAcct(settleAcct);
		return sldh;
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbSl.getCustNo());
		remap.put("custType", ifsCfetsrmbSl.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbSl.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbSl.getBorrowInst());
		// 标的券券面总额
		BigDecimal amt = calcuAmt(ifsCfetsrmbSl.getUnderlyingQty().multiply(new BigDecimal("10000")), ifsCfetsrmbSl.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbSl.getForDate());
		remap.put("mdate", ifsCfetsrmbSl.getSecondSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取债券借贷实体
	 * 
	 * @param ifsCfetsrmbSl
	 * @param branchDate
	 * @return
	 */
	private SlSecurityLendingBean getEntry(IfsCfetsrmbSl ifsCfetsrmbSl, Date branchDate) {
		// 交易对手
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsCfetsrmbSl.getLendInst());
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbSl.getLendInst());
		// 本方交易员
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbSl.getBorrowTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbSl.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.SECLEND.IOPER);
		institutionInfo.setTradeId(trad);
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setAuthsi(SlDealModule.SECLEND.AUTHSI);
		externalBean.setVerind(SlDealModule.SECLEND.VERIND);
		externalBean.setDealtext(ifsCfetsrmbSl.getContractId());
		// ========== 债券借贷标的 =========
		SlSecurityLendingBean securityLendingBean = new SlSecurityLendingBean(ifsCfetsrmbSl.getBr(),
				SlDealModule.SECLEND.SERVER, SlDealModule.SECLEND.TAG, SlDealModule.SECLEND.DETAIL);
		securityLendingBean.getInthBean().setIoper(trad);
		securityLendingBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		securityLendingBean.setInstitutionInfo(institutionInfo);
		// 债券借贷标的 opics要素
		externalBean.setCost(ifsCfetsrmbSl.getBondCost());
		externalBean.setPort(ifsCfetsrmbSl.getBondPort());
		externalBean.setProdcode(ifsCfetsrmbSl.getProduct());
		externalBean.setProdtype(ifsCfetsrmbSl.getProdType());
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbSl.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbSl.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbSl.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbSl.getCtrsacct());
		securityLendingBean.setExternalBean(externalBean);
		SlInthBean inthBean = securityLendingBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbSl.getTicketId());
		inthBean.setSeq("0");
		inthBean.setStatcode("-1");
		inthBean.setLstmntdate(branchDate);
		securityLendingBean.setInthBean(inthBean);
		securityLendingBean.setDealseq(null);
		securityLendingBean.setAddsi(SlDealModule.SECLEND.ADDSI);
		securityLendingBean.setBasis(ifsCfetsrmbSl.getBasis());
		securityLendingBean.setCashcost(ifsCfetsrmbSl.getCost());
		securityLendingBean.setCcy("CNY");
		securityLendingBean.setCollateralcode(SlDealModule.SECLEND.COLLATERALCODE);
		securityLendingBean.setCollrequiredind(SlDealModule.SECLEND.COLLREQUIREDIND);
		securityLendingBean.setDealDate(DateUtil.parse(ifsCfetsrmbSl.getForDate(), "yyyy-MM-dd"));
		securityLendingBean.setDealind(SlDealModule.SECLEND.DEALIND);
		if ("01".equals(ifsCfetsrmbSl.getDealSource())) {
			securityLendingBean.setDealscre("D");
		} else {
			securityLendingBean.setDealscre(ifsCfetsrmbSl.getDealSource());
		}
		securityLendingBean.setDelivtype(SlDealModule.SECLEND.DELIVTYPE);
		// 借贷费用,保留两位小数
		securityLendingBean.setFeeamt(ifsCfetsrmbSl.getMiscFeeType().setScale(2, BigDecimal.ROUND_HALF_UP));
		securityLendingBean.setFeeProdtype("FE");
		securityLendingBean.setFeeProduct("SECLEN");
		securityLendingBean.setFullyassignind("N");
		securityLendingBean.setInvtype(ifsCfetsrmbSl.getInvestType());
		securityLendingBean.setDrprice(BigDecimal.valueOf(100));
		securityLendingBean.setValueDate(DateUtil.parse(ifsCfetsrmbSl.getFirstSettlementDate(), "yyyy-MM-dd"));
		securityLendingBean.setMaturityDate(DateUtil.parse(ifsCfetsrmbSl.getSecondSettlementDate(), "yyyy-MM-dd"));
		securityLendingBean.setPmvind(SlDealModule.SECLEND.PMVIND);
		securityLendingBean.setFaceamt(new BigDecimal(MathUtil
				.roundWithNaN(ifsCfetsrmbSl.getUnderlyingQty().multiply(new BigDecimal(10000)).doubleValue(), 2)));
		securityLendingBean.setSafekeepacct(ifsCfetsrmbSl.getBorrowCustname());
		securityLendingBean.setSecurityId(ifsCfetsrmbSl.getUnderlyingSecurityId());
		securityLendingBean.setSpreadrate(BigDecimal.valueOf(0));
		securityLendingBean.setSubind(SlDealModule.SECLEND.SUBIND);
		securityLendingBean.setSuppcashind(SlDealModule.SECLEND.SUPPCASHIND);
		securityLendingBean.setSuppconfind(SlDealModule.SECLEND.SUPPCONFIND);
		securityLendingBean.setSuppfeeind(SlDealModule.SECLEND.SUPPFEEIND);
		securityLendingBean.setSupsecmoveind(SlDealModule.SECLEND.SUPSECMOVEIND);
		securityLendingBean.setTenor("99");
		return securityLendingBean;

	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsrmbSl> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbSlMapper.getRmbSlMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbSlMapper.getRmbSlMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbSlMapper.getRmbSlMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> slTradingList = taDictVoMapper.getTadictTree("SlTrading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbSl ifsCfetsrmbSl: list) {
			ifsCfetsrmbSl.setMyDir(slTradingList.get(ifsCfetsrmbSl.getMyDir()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

}
