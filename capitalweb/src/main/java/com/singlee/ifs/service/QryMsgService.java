package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.TdQryMsg;

import java.util.Map;

/**
 * 用于查询报文反馈信息
 * 
 * @author cmj
 * 
 */
public interface QryMsgService {

	//报文反馈信息查询
	public Page<TdQryMsg> searchQryMsg(Map<String, Object> params);
	
	//修改
	public void update(Map<String, Object> params);
	//未发送报文交易
	public Page<TdQryMsg> searchNoSendDeal(Map<String, Object> params);
	//异常交易重发
}
