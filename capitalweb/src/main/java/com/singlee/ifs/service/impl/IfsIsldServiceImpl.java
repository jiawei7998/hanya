package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IfsRevIsldMapper;
import com.singlee.ifs.model.IfsRevIsld;
import com.singlee.ifs.service.IfsIsldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
/**
 * 交易要素  实现类
 * @author singlee4
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsIsldServiceImpl implements IfsIsldService{

	@Autowired
	IfsRevIsldMapper ifsRevIsldMapper;
	
	@Override
	public void deleteById(String ticketId) {
		ifsRevIsldMapper.deleteById(ticketId);
	}

	@Override
	public void insert(IfsRevIsld entity) {
		/** 新增时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId = ifsRevIsldMapper.getId();
		entity.setTicketId(ticketId);
		ifsRevIsldMapper.insert(entity);
		
	}

	@Override
	public void updateById(IfsRevIsld entity) {
		/** 修改时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRevIsldMapper.updateById(entity);
	}

	@Override
	public Page<IfsRevIsld> getRevIsldPage(Map<String, Object> params,int isFinished) {
		Page<IfsRevIsld> page = new Page<IfsRevIsld>();
		if (isFinished == 1) {// 待审批
			page = ifsRevIsldMapper.getRevIsldUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRevIsldMapper.getRevIsldFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRevIsldMapper.getRevIsldMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

}
