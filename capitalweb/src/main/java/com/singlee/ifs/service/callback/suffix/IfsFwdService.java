package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsfxFwdMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxFwd;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsFwdService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsFwdService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
	@Autowired
	private IFxdServer fxdServer;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	UserService userService;
	@Autowired
	ISlGentServer slGentServer;

	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsfxFwd ifscfetsfxfwd = new IfsCfetsfxFwd();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxFwdMapper.updateCreditByID(serial_no, status, date);
		ifscfetsfxfwd = ifsCfetsfxFwdMapper.getCredit(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifscfetsfxfwd) {
			return;
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}

		}
		
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			// 2.1需要考虑第一次审批和撤回再次审批。
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifscfetsfxfwd.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifscfetsfxfwd.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifscfetsfxfwd.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("外汇远期");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("rmbfwd");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifscfetsfxfwd.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifscfetsfxfwd.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifscfetsfxfwd.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_FWD");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_FWD where ticket_id=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxFwdMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					LimitOccupy(serial_no, TradeConstants.ProductCode.RMBFWD);
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			// 3.2获取字典中的配置
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}

			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			// 3.3调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifscfetsfxfwd.getBr());
				SlFxSptFwdBean slFxSptFwdBean = getEntry(ifscfetsfxfwd, branchDate);
				// 验证
				/*
				 * SlOutBean verRet = fxdServer.verifiedfxSptFwd(slFxSptFwdBean); if
				 * (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
				 * JY.raise("验证失败......"+verRet.getRetMsg()); }
				 */
				// 插入opics表
				SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);

				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("插入opics表失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifscfetsfxfwd.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxFwdMapper.updateStatcodeByTicketId(map);
				}

			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程
				Map map1 = BeanUtils.describe(ifscfetsfxfwd);
				SlOutBean result = fxdServer.fxSptAndFwdProc(map1);
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过,执行存储过程失败......");
				}

			} else {
				JY.raise("系统参数未配置,未进行交易导入处理......");
			}

		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsfxFwd IfsFwdService = ifsCfetsfxFwdMapper.getCredit(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", IfsFwdService.getCustNo());
		remap.put("custType", IfsFwdService.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", IfsFwdService.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", IfsFwdService.getInstId());
		if("P".equals(IfsFwdService.getBuyDirection())) {
			BigDecimal amt = calcuAmt(IfsFwdService.getSellAmount(), IfsFwdService.getWeight());
			remap.put("amt", ccyChange(amt, IfsFwdService.getOpicsctrccy(),hrbCreditCustQuota.getCurrency()));
		}else {
			BigDecimal amt = calcuAmt(IfsFwdService.getBuyAmount(), IfsFwdService.getWeight());
			remap.put("amt", ccyChange(amt, IfsFwdService.getOpicsccy(),hrbCreditCustQuota.getCurrency()));
		}
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", IfsFwdService.getForDate());
		remap.put("mdate", IfsFwdService.getValueDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取对象
	 * 
	 * @param ifscfetsfxfwd
	 * @param branchDate
	 * @return
	 */
	private SlFxSptFwdBean getEntry(IfsCfetsfxFwd ifscfetsfxfwd, Date branchDate) {
		// String br=userService.getBrByUser(ifscfetsfxfwd.getSponsor());
		SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean(ifscfetsfxfwd.getBr(), SlDealModule.FXD.SERVER,
				SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
		/************* 20180710新增：设置交易日期、起息日期 ***************************/
		slFxSptFwdBean.setDealDate(DateUtil.parse(ifscfetsfxfwd.getForDate()));
		slFxSptFwdBean.setValueDate(DateUtil.parse(ifscfetsfxfwd.getValueDate()));

		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		/************* 20180710新增：设置对方交易对手 ***************************/
		contraPatryInstitutionInfo.setTradeId(ifscfetsfxfwd.getCounterpartyDealer());
		/** 设置对方机构 */
		contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxfwd.getCounterpartyInstId());
		slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);

		/************* 20180710新增：设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifscfetsfxfwd.getDealer());// 交易员id
		userMap.put("branchId", ifscfetsfxfwd.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
//		SlGentBean gentBean = new SlGentBean();
//		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
//		gentBean.setTablevalue(ifscfetsfxfwd.getCost());
//		gentBean.setBr(ifscfetsfxfwd.getBr());
//		// gentBean=slGentServer.getSlGent(gentBean);
//		String trad = (gentBean != null)
//				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
//				: user.getOpicsTrad();
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FXD.IOPER);
		institutionInfo.setTradeId(trad);
		slFxSptFwdBean.getInthBean().setIoper(trad);
		slFxSptFwdBean.setInstitutionInfo(institutionInfo);
		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		Double amt = Double.valueOf(ifscfetsfxfwd.getBuyAmount().toString());// 币种1
		currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
		/************* 20180711修改：设置本方币种 ***************************/
		String currencyPair = ifscfetsfxfwd.getCurrencyPair();
//		 currencyInfo.setCcy(currencyPair.substring(0,3));
		currencyInfo.setCcy(ifscfetsfxfwd.getOpicsccy());// 对于cfets下行不适用
		/************* 20180710新增：设置汇率 ***************************/
		// 传入成交汇率
//		 Double rate1 = Double.valueOf(ifscfetsfxfwd.getPrice().toString());
		if (ifscfetsfxfwd.getExchangeRate() == null) {
			JY.raise("审批未通过,成交汇率不能为空......");
		}
		Double rate1 = Double.valueOf(ifscfetsfxfwd.getExchangeRate());
		currencyInfo.setRate(BigDecimal.valueOf(rate1));
//		 currencyInfo.setContraCcy(currencyPair.substring(4,7));
		currencyInfo.setContraCcy(ifscfetsfxfwd.getOpicsctrccy());//// 对于cfets下行不适用
//		Double contraAmt = Double.valueOf(ifscfetsfxfwd.getBuyAmount());
		// currencyInfo.setContraAmt(BigDecimal.valueOf(contraAmt));
		// 点差
		Double points = Double.valueOf(ifscfetsfxfwd.getSpread());
		currencyInfo.setPoints(BigDecimal.valueOf(points));
		// opics基础
		currencyInfo.setSpotRate(ifscfetsfxfwd.getPrice());
		slFxSptFwdBean.setCurrencyInfo(currencyInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifscfetsfxfwd.getCost());
		externalBean.setProdcode(ifscfetsfxfwd.getProduct());
		externalBean.setProdtype(ifscfetsfxfwd.getProdType());
		externalBean.setPort(ifscfetsfxfwd.getPort());

		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifscfetsfxfwd.getTicketId());
		externalBean.setDealtext(ifscfetsfxfwd.getContractId());
		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
		externalBean.setSiind(SlDealModule.FXD.SIIND);
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
		externalBean.setVerind(SlDealModule.FXD.VERIND);
		/*
		 * if(ifscfetsfxfwd.getNettingStatus().equals("1")){//净额清算
		 * PropertiesConfiguration properties =
		 * PropertiesUtil.parseFile("netting.properties"); // 设置清算路径1
		 * externalBean.setCcysmeans(properties.getString("ccysmeans")); // 设置清算账户1
		 * externalBean.setCcysacct(properties.getString("ccysacct")); // 设置清算路径2
		 * externalBean.setCtrsmeans(properties.getString("ctrsmeans")); // 设置清算账户2
		 * externalBean.setCtrsacct(properties.getString("ctrsacct")); }
		 */
		slFxSptFwdBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFxSptFwdBean.getInthBean();
		inthBean.setFedealno(ifscfetsfxfwd.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFxSptFwdBean.setInthBean(inthBean);
		String ps = ifscfetsfxfwd.getBuyDirection();
		slFxSptFwdBean.setPs(StringUtils.equals(ps, "P") ? PsEnum.P : PsEnum.S);
		slFxSptFwdBean.setExecId(ifscfetsfxfwd.getTicketId());
		return slFxSptFwdBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsfxFwd> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsfxFwdMapper.getFxFwdMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsfxFwdMapper.getFxFwdMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsfxFwdMapper.getFxFwdMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyPairList = taDictVoMapper.getTadictTree("CurrencyPair").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_desc));
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradingList = taDictVoMapper.getTadictTree("trading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradeSourceList = taDictVoMapper.getTadictTree("TradeSource").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> transCodeList = taDictVoMapper.getTadictTree("transCode").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> purposeTypeList = taDictVoMapper.getTadictTree("purposeType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsfxFwd ifsCfetsfxFwd: list) {
			ifsCfetsfxFwd.setCurrencyPair(currencyPairList.get(ifsCfetsfxFwd.getCurrencyPair()));
			ifsCfetsfxFwd.setOpicsccy(currencyList.get(ifsCfetsfxFwd.getOpicsccy()));
			ifsCfetsfxFwd.setBuyDirection(tradingList.get(ifsCfetsfxFwd.getBuyDirection()));
			ifsCfetsfxFwd.setDealSource(tradeSourceList.get(ifsCfetsfxFwd.getDealSource()));
			ifsCfetsfxFwd.setTransKind(transCodeList.get(ifsCfetsfxFwd.getTransKind()));
			ifsCfetsfxFwd.setPurposeCode(purposeTypeList.get(ifsCfetsfxFwd.getPurposeCode()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
