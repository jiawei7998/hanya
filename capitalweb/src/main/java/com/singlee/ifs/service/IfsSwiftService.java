package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.ifs.model.IfsSwift;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public interface IfsSwiftService {

	public Page<IfsSwift> getHandling(Map<String, Object> map);

	public void updateSwiftStatus(Date tm, String batchNo,
			String userid,String verind);

	public void deleteSwift(Map<String, Object> map);
		
	public void updateSwiftDelete(Map<String, Object> map);
	
	public void updateSwiftComfirm(Map<String, Object> map);
	
	//获取删除复核状态报文信息
	public Page<IfsSwift> getVerifyHandling(Map<String, Object> map);
	
	public RetMsg<Serializable>	swiftBondCreat(Map<String, Object> map);
	//查询299报文
	public Map<String,Object> getSwiftBond(Map<String, Object> map);
}
