package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.ifs.mapper.IfsFxDepositInMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsFxDepositIn;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service(value = "IfsCcyDepositInService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCcyDepositInService extends IfsApproveServiceBase {

    @Autowired
    IfsFxDepositInMapper ifsFxDepositInMapper;

    @Override
    public void statusChange(String flow_type, String flow_id, String serial_no, String status,
                             String flowCompleteType) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
        String date = dateFormat.format(now);
        ifsFxDepositInMapper.updatefxDepositByID(serial_no, status, date);
        IfsFxDepositIn ifsFxDepositIn = ifsFxDepositInMapper.searchByTicketId(serial_no);
        // 0.1 当前业务为空则不进行处理
        if (null == ifsFxDepositIn) {
            return;
        }

        // 1.交易审批中状态,需要将交易纳入监控审批
        if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
            // 插入审批监控表，需要整合字段
            IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
                ifsFlowMonitorMapper.deleteById(serial_no);
            }
            // 1.2 保存监控表数据
            IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
            ifsFlowMonitor.setTicketId(ifsFxDepositIn.getTicketId());// 设置监控表-内部id
            ifsFlowMonitor.setContractId(ifsFxDepositIn.getContractId());// 设置监控表-成交单编号
            ifsFlowMonitor.setApproveStatus(ifsFxDepositIn.getApproveStatus());// 设置监控表-审批状态
            ifsFlowMonitor.setPrdName("同业存放(外币)");// 设置监控表-产品名称
            ifsFlowMonitor.setPrdNo("ccyDepositIn");// 设置监控表-产品编号：作为分类
            ifsFlowMonitor.setTradeDate(ifsFxDepositIn.getForDate());// 设置监控表-交易日期
            ifsFlowMonitor.setSponsor(ifsFxDepositIn.getSponsor());// 设置监控表-审批发起人
            ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
            String custNo = ifsFxDepositIn.getCustId();
            if (null != custNo || "".equals(custNo)) {
                ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
                IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
                ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
            }
            ifsFlowMonitorMapper.insert(ifsFlowMonitor);

        }
        // 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
        if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
                || StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
                || StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
            IfsFlowMonitor monitor = new IfsFlowMonitor();
            monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
            ifsFlowMonitorMapper.deleteById(serial_no);
            Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
            ifsFlowCompletedMapper.insert(map1);
        }
        //同业存放不进Opics
    }
}
