package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFeesServer;
import com.singlee.financial.opics.ISwapServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsRmbIrsService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbIrsService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;
	@Autowired
	private ISwapServer swapserver;
	@Autowired
	private IFeesServer feeserver;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	TaUserMapper taUserMapper;// 用于查询 本方交易员
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Autowired
	IfsOpicsBondMapper bondMapper;
	@Autowired
	IAcupServer acupServer;
	@Resource
	TaDictVoMapper taDictVoMapper;

	/*
	 * @Override public Map<String, Object> fireEvent(String flow_id, String
	 * serial_no, String task_id, String task_def_key) throws Exception {
	 * //查询当前节点下配置的事件 #{flow_id} AND TASK_DEF_KEY = #{task_def_key}
	 * Map<String,Object> map = new HashMap<String, Object>(); map.put("flow_id",
	 * flow_id); map.put("task_def_key", task_def_key); map.put("prd_code", "441");
	 * IfsCfetsrmbIrs ifscfetsrmbirs=
	 * ifsCfetsrmbIrsMapper.searchIfsCfetsrmbIrs(serial_no); accountAmt(
	 * map,"IFS_CFETSRMB_IRS",BeanUtil.beanToMap(ifscfetsrmbirs),"lastQty"); return
	 * null; }
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsrmbIrs ifscfetsrmbirs = new IfsCfetsrmbIrs();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbIrsMapper.updateRmbIrsByID(serial_no, status, date);
		ifscfetsrmbirs = ifsCfetsrmbIrsMapper.searchIfsCfetsrmbIrs(serial_no);

		// 0.1 当前业务为空则不进行处理
		if (null == ifscfetsrmbirs) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("S".equals(ifscfetsrmbirs.getMyDir())){//支付
					hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		// /////以下将审批中的记录插入审批监控表
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifscfetsrmbirs.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifscfetsrmbirs.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifscfetsrmbirs.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("利率互换");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("irs");// 设置监控表-产品编号：作为分类
			if ("1".equals(ifscfetsrmbirs.getPrdFlag())) {
				ifsFlowMonitor.setPrdName("结构衍生");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("irsDerive");// 设置监控表-产品编号：作为分类
			}
			ifsFlowMonitor.setTradeDate(ifscfetsrmbirs.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifscfetsrmbirs.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifscfetsrmbirs.getFloatInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_IRS");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_IRS where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbIrsMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("S".equals(ifscfetsrmbirs.getMyDir())) {//支付
						LimitOccupy(serial_no, TradeConstants.ProductCode.IRS);
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			
		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				// 获取当前账务日期
				Date branchDate = baseServer.getOpicsSysDate(ifscfetsrmbirs.getBr());
				SlSwapIrsBean slSwapIrsBean = getEntry(ifscfetsrmbirs, branchDate);
				// 验证
				SlOutBean verRet = swapserver.verifiedSwapIrs(slSwapIrsBean);
				if (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("验证未通过......" + verRet.getRetMsg());
				}
				SlOutBean result = swapserver.swapIrs(slSwapIrsBean);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("IRS审批未通过，插入opcis表失败......");
				} else {
					// fee模块导入
//					if("1".equals(ifscfetsrmbirs.getPrdFlag())){
//						SlIfeeBean feeBean = new SlIfeeBean("01", SlDealModule.FEES.SERVER, SlDealModule.FEES.TAG, SlDealModule.FEES.DETAIL);
//						feeBean.setFeeproduct("FEES");
//						feeBean.setFeeprodtype(ifscfetsrmbirs.getFeeFloat().equalsIgnoreCase("P")?"FE":"FC");
//						feeBean.setCcy(ifscfetsrmbirs.getCcy());
//						double feePrice = Double.parseDouble(ifscfetsrmbirs.getFeePrice());
//						feeBean.setCcyamt(String.valueOf(MathUtil.roundWithNaN(feePrice, 2)));
//						feeBean.setCost(ifscfetsrmbirs.getCost());
//						feeBean.setCno(ifscfetsrmbirs.getFloatInst());
//						feeBean.setTenor("99");
//						feeBean.setAcctgmethod("C");
//						feeBean.setStartdate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setEnddate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setVdate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setVerind("Y");
//						feeBean.setVeroper("SYS1");
//						feeBean.setVerdate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setSettmeans("CNAPS");
//						feeBean.setSettacct(ifscfetsrmbirs.getFeeFloat().equalsIgnoreCase("P")?"CNAP-P":"CNAP-R");
//						feeBean.setSettauthind("Y");
//						feeBean.setSettauthdte(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setFeetext("SWAP FEES");
//						feeBean.setDealmdate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
//						feeBean.setPort("SPDK");
//						feeBean.setTrad("TRAD");
//						feeBean.setDealdate(DateUtil.parse(ifscfetsrmbirs.getForDate()));
//						feeBean.setSiind("Y");
//						feeBean.setSuppconfind("Y");
//						feeBean.setSuppayind("Y");
//						feeBean.setSuprecind("Y");
//						feeBean.setSupplemental("Y");
//						feeBean.setBasis(StringUtils.trimToEmpty(ifscfetsrmbirs.getFeeLegDayCount()).equals("")?"A360":ifscfetsrmbirs.getFeeLegDayCount());
//						feeBean.setAmortmethod("N");
//						feeBean.setBaseccyamt(String.valueOf(MathUtil.roundWithNaN(feePrice, 2)));
//						
//						SlInthBean inthBean1 = feeBean.getInthBean();
//						inthBean1.setFedealno(ifscfetsrmbirs.getTicketId());
//						inthBean1.setLstmntdate(baseServer.getOpicsSysDate("01"));
//						feeBean.setInthBean(inthBean1);
//						
//						SlOutBean result1=feeserver.fees(feeBean);
//						
//						if (!result1.getRetStatus().equals(RetStatusEnum.S)) {
//							JY.raise("FEES审批未通过......");
//						} else {
//							Map<String, Object> map = new HashMap<String, Object>();
//							map.put("ticketId", ifscfetsrmbirs.getTicketId());
//							map.put("satacode", "-1");
//							ifsCfetsrmbIrsMapper.updateStatcodeByTicketId(map);
//						}
//					}else{
//						Map<String, Object> map = new HashMap<String, Object>();
//						map.put("ticketId", ifscfetsrmbirs.getTicketId());
//						map.put("satacode", "-1");
//						ifsCfetsrmbIrsMapper.updateStatcodeByTicketId(map);
//					}
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifscfetsrmbirs.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsrmbIrsMapper.updateStatcodeByTicketId(map);
				}

			} else {// 调用存储过程
				Map map = BeanUtils.describe(ifscfetsrmbirs);
				SlOutBean result = swapserver.irsSwapProc(map);// 利率互换存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，调用存储过程失败......");
				}
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbIrs ifsCfetsrmbIrs = ifsCfetsrmbIrsMapper.searchIfsCfetsrmbIrs(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbIrs.getCustNo());
		remap.put("custType", ifsCfetsrmbIrs.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbIrs.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbIrs.getFixedInst());
		BigDecimal amt = calcuAmt(ifsCfetsrmbIrs.getLastQty().multiply(new BigDecimal("10000")), ifsCfetsrmbIrs.getWeight());
		remap.put("amt", ccyChange(amt,ifsCfetsrmbIrs.getCcy(),hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbIrs.getStartDate());
		remap.put("mdate", ifsCfetsrmbIrs.getEndDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取互换的交易实体
	 * 
	 * @param ifscfetsrmbirs
	 * @param branchDate
	 * @return
	 */
	private SlSwapIrsBean getEntry(IfsCfetsrmbIrs ifscfetsrmbirs, Date branchDate) {
		SlSwapIrsBean slSwapIrsBean = new SlSwapIrsBean(ifscfetsrmbirs.getBr(), SlDealModule.SWAP.SERVER,
				SlDealModule.SWAP.TAG, SlDealModule.SWAP.DETAIL);
		/** 设置对方信息 */
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		// 设置对方机构id
		// IfsOpicsCust
		// cust=custMapper.searchIfsOpicsCust(ifscfetsrmbirs.getFloatInst());
		// contraPatryInstitutionInfo.setInstitutionId(cust.getCname());
		contraPatryInstitutionInfo.setInstitutionId(ifscfetsrmbirs.getFloatInst());
		// 设置对方交易员
		contraPatryInstitutionInfo.setTradeId(ifscfetsrmbirs.getFloatTrader());
		slSwapIrsBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		/************* 20180725新增：设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifscfetsrmbirs.getFixedTrader());// 交易员id
		userMap.put("branchId", ifscfetsrmbirs.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.SWAP.IOPER);
		institutionInfo.setTradeId(trad);
		slSwapIrsBean.getInthBean().setIoper(trad);
		slSwapIrsBean.setInstitutionInfo(institutionInfo);
		slSwapIrsBean.setNetpayind(SlDealModule.SWAP.NETPAYIND);
		slSwapIrsBean.setSetoff(SlDealModule.SWAP.SETOFF);
		slSwapIrsBean.setPaypostnotional(SlDealModule.SWAP.PAYPOSTNOTIONAL);
		slSwapIrsBean.setRecpostnotional(SlDealModule.SWAP.RECPOSTNOTIONAL);
		// 判断计息天数调整0-不调整 1-实际天数
		if ("0".equalsIgnoreCase(ifscfetsrmbirs.getInterestRateAdjustment())) {
			slSwapIrsBean.setPayIntpayRule("D");
			slSwapIrsBean.setRecIntpayRule("D");
		} else if ("1".equalsIgnoreCase(ifscfetsrmbirs.getInterestRateAdjustment())) {
			slSwapIrsBean.setPayIntpayRule("M");
			slSwapIrsBean.setRecIntpayRule("M");
		}
		// 支付日期调整0-上一营业日 1-下一营业日 3经调整的下一营业日
		if ("0".equalsIgnoreCase(ifscfetsrmbirs.getCouponPaymentDateReset())) {
			slSwapIrsBean.setPayDateDir("VF");
			slSwapIrsBean.setRecDateDir("VF");
		} else {
			slSwapIrsBean.setPayDateDir("MB");
			slSwapIrsBean.setRecDateDir("MB");
		}
		// 币种
		if ("CNY".equalsIgnoreCase(ifscfetsrmbirs.getCcy())) {
			slSwapIrsBean.setRecschdconv("CHINA");
			slSwapIrsBean.setPayschdconv("CHINA");
		} else {
			slSwapIrsBean.setRecschdconv("ISDA");
			slSwapIrsBean.setPayschdconv("ISDA");
		}
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifscfetsrmbirs.getCost());
		externalBean.setProdcode(ifscfetsrmbirs.getProduct());
		externalBean.setProdtype(ifscfetsrmbirs.getProdType());
		externalBean.setPort(ifscfetsrmbirs.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifscfetsrmbirs.getTicketId());
		externalBean.setDealtext(ifscfetsrmbirs.getContractId());
		externalBean.setAuthsi(SlDealModule.SWAP.AUTHSIIND);
		externalBean.setSiind(SlDealModule.SWAP.SIIND);
		externalBean.setSupconfind(SlDealModule.SWAP.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.SWAP.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.SWAP.SUPRECIND);
		externalBean.setVerind(SlDealModule.SWAP.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifscfetsrmbirs.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifscfetsrmbirs.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifscfetsrmbirs.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifscfetsrmbirs.getCtrsacct());
		slSwapIrsBean.setExternalBean(externalBean);
		SlInthBean inthBean = slSwapIrsBean.getInthBean();
		inthBean.setFedealno(ifscfetsrmbirs.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slSwapIrsBean.setInthBean(inthBean);
		slSwapIrsBean.setExecId(ifscfetsrmbirs.getTicketId());
		slSwapIrsBean.setValueDate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
		slSwapIrsBean.setDealDate(DateUtil.parse(ifscfetsrmbirs.getForDate()));
		if ("S".equals(ifscfetsrmbirs.getMyDir())) {// 本方支付
			if (ifscfetsrmbirs.getFixedLegDayCount().endsWith("1")) {
				slSwapIrsBean.setPayBasis("A360");
			} else if (ifscfetsrmbirs.getFixedLegDayCount().endsWith("3")) {
				slSwapIrsBean.setPayBasis("A365");
			}
			if (ifscfetsrmbirs.getFloatLegDayCount().endsWith("1")) {
				slSwapIrsBean.setRecBasis("A360");
			} else if (ifscfetsrmbirs.getFloatLegDayCount().endsWith("3")) {
				slSwapIrsBean.setRecBasis("A365");
			}
			// 支付周期转换
			String fixedPayIrsFreq = ifscfetsrmbirs.getFixedPaymentFrequency();
			String floatPayIrsFreq = ifscfetsrmbirs.getFloatPaymentFrequency();
			slSwapIrsBean.setPayIntpaycycle(fixedPayIrsFreq);
			slSwapIrsBean.setRecIntpaycycle(floatPayIrsFreq);
			if ("6".equals(fixedPayIrsFreq) || "6".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("W");
				slSwapIrsBean.setRecIntpaycycle("W");
			} else if ("3".equals(fixedPayIrsFreq) || "3".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("M");
				slSwapIrsBean.setRecIntpaycycle("M");
			} else if ("4".equals(fixedPayIrsFreq) || "4".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("Q");
				slSwapIrsBean.setRecIntpaycycle("Q");
			} else if ("0".equals(fixedPayIrsFreq) || "0".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("S");
				slSwapIrsBean.setRecIntpaycycle("S");
			} else if ("1".equals(fixedPayIrsFreq) || "1".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("A");
				slSwapIrsBean.setRecIntpaycycle("A");
			}
			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getLegPrice());
			// 右端固定/浮动利率
			slSwapIrsBean.setRecIntRate(ifscfetsrmbirs.getRightRate());
			slSwapIrsBean.setTenor("99");
			// 左端利率曲线
			slSwapIrsBean.setPayYieldcurve(ifscfetsrmbirs.getLeftDiscountCurve());
			// 右端利率曲线
			slSwapIrsBean.setRecYieldcurve(ifscfetsrmbirs.getRightDiscountCurve());
			// 左端利率代码
			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getLeftRateCode());
			// 右端利率代码
			slSwapIrsBean.setRecRateCode(ifscfetsrmbirs.getRightRateCode());
			// 交易方式
			slSwapIrsBean.setPlmethod(ifscfetsrmbirs.getAccountingMethod());
			// 设置固定/浮动方向
			slSwapIrsBean.setSwapType(ifscfetsrmbirs.getFixedFloatDir());
			// 左端币种
			slSwapIrsBean.setPayNotCcy(ifscfetsrmbirs.getCcy());
			// 右端币种
			slSwapIrsBean.setRecNotCcy(ifscfetsrmbirs.getCcy());
			// 到期日
			slSwapIrsBean.setMaturityDate(ifscfetsrmbirs.getEndDate());
			// 左端金额
			double lastQty = ifscfetsrmbirs.getLastQty().doubleValue() * 10000;
			slSwapIrsBean.setPayNotCcyAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(lastQty, 2)));
			// 右端金额
			slSwapIrsBean.setRecNotCcyAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(lastQty, 2)));
		} else {// 右边是支付
			if (ifscfetsrmbirs.getFixedLegDayCount().endsWith("1")) {
				slSwapIrsBean.setRecBasis("A360");
			} else if (ifscfetsrmbirs.getFixedLegDayCount().endsWith("3")) {
				slSwapIrsBean.setRecBasis("A365");
			}
			if (ifscfetsrmbirs.getFloatLegDayCount().endsWith("1")) {
				slSwapIrsBean.setPayBasis("A360");
			} else if (ifscfetsrmbirs.getFloatLegDayCount().endsWith("3")) {
				slSwapIrsBean.setPayBasis("A365");
			}
			// 支付周期转换
			String fixedPayIrsFreq = ifscfetsrmbirs.getFixedPaymentFrequency();
			String floatPayIrsFreq = ifscfetsrmbirs.getFloatPaymentFrequency();
			slSwapIrsBean.setPayIntpaycycle(floatPayIrsFreq);
			slSwapIrsBean.setRecIntpaycycle(fixedPayIrsFreq);
			if ("6".equals(fixedPayIrsFreq) || "6".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("W");
				slSwapIrsBean.setRecIntpaycycle("W");
			} else if ("3".equals(fixedPayIrsFreq) || "3".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("M");
				slSwapIrsBean.setRecIntpaycycle("M");
			} else if ("4".equals(fixedPayIrsFreq) || "4".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("Q");
				slSwapIrsBean.setRecIntpaycycle("Q");
			} else if ("0".equals(fixedPayIrsFreq) || "0".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("S");
				slSwapIrsBean.setRecIntpaycycle("S");
			} else if ("1".equals(fixedPayIrsFreq) || "1".equals(floatPayIrsFreq)) {
				slSwapIrsBean.setPayIntpaycycle("A");
				slSwapIrsBean.setRecIntpaycycle("A");
			}
			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getRightRate());
			// 右端固定/浮动利率
			slSwapIrsBean.setRecIntRate(ifscfetsrmbirs.getLegPrice());
			slSwapIrsBean.setTenor("99");
			// 左端利率曲线
			slSwapIrsBean.setPayYieldcurve(ifscfetsrmbirs.getRightDiscountCurve());
			// 右端利率曲线
			slSwapIrsBean.setRecYieldcurve(ifscfetsrmbirs.getLeftDiscountCurve());
			// 左端利率代码
			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getRightRateCode());
			// 右端利率代码
			slSwapIrsBean.setRecRateCode(ifscfetsrmbirs.getLeftRateCode());
			// 交易方式
			slSwapIrsBean.setPlmethod(ifscfetsrmbirs.getAccountingMethod());
			// 设置固定/浮动方向
			slSwapIrsBean.setSwapType(ifscfetsrmbirs.getFixedFloatDir());
			// 左端币种
			slSwapIrsBean.setPayNotCcy(ifscfetsrmbirs.getCcy());
			// 右端币种
			slSwapIrsBean.setRecNotCcy(ifscfetsrmbirs.getCcy());
			// 到期日
			slSwapIrsBean.setMaturityDate(ifscfetsrmbirs.getEndDate());
			// 左端金额
			double lastQty = ifscfetsrmbirs.getLastQty().doubleValue() * 10000;
			slSwapIrsBean.setPayNotCcyAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(lastQty, 2)));
			// 右端金额
			slSwapIrsBean.setRecNotCcyAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(lastQty, 2)));
		}

		String myDir = ifscfetsrmbirs.getMyDir();// 支付方向
		if ("S".equals(myDir)) {
			// 本方支付固定或者本方收取浮动
			// 左边
//			slSwapIrsBean.setsetPayBasis(ifscfetsrmbirs.getFixedFloatL());
			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getLegPrice());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getBenchmarkSpreadL());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getFixedPaymentDate());
			slSwapIrsBean.setPayIntpaycycle(ifscfetsrmbirs.getFixedPaymentFrequency());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getLegInterestAccrualDateL());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getInterestResetFrequencyL());
			// 单利复利
			if ("0".equalsIgnoreCase(ifscfetsrmbirs.getInterestMethodL())) {
				slSwapIrsBean.setPaycompound("F");
			} else {
				slSwapIrsBean.setPaycompound("S");
			}
			slSwapIrsBean.setPayBasis(ifscfetsrmbirs.getFixedLegDayCount());
			slSwapIrsBean.setPayYieldcurve(ifscfetsrmbirs.getLeftDiscountCurve());
			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getLeftRateCode());
			// 右边
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getFixedFloatR());
			slSwapIrsBean.setRecIntRate(ifscfetsrmbirs.getRightRate());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getBenchmarkSpread());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getFloatPaymentDate());
			slSwapIrsBean.setRecIntpaycycle(ifscfetsrmbirs.getFloatPaymentFrequency());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getLegInterestAccrualDate());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getInterestResetFrequency());
			// 单利复利
			if ("0".equalsIgnoreCase(ifscfetsrmbirs.getInterestMethod())) {
				slSwapIrsBean.setReccompound("F");
			} else {
				slSwapIrsBean.setReccompound("S");
			}
			slSwapIrsBean.setRecBasis(ifscfetsrmbirs.getFloatLegDayCount());
			slSwapIrsBean.setRecYieldcurve(ifscfetsrmbirs.getRightDiscountCurve());
			slSwapIrsBean.setRecRateCode(ifscfetsrmbirs.getRightRateCode());
		} else {
			// 本方支付浮动或者本方收取固定
			// 左边
//			slSwapIrsBean.setsetPayBasis(ifscfetsrmbirs.getFixedFloatL());
			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getRightRate());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getBenchmarkSpreadL());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getFixedPaymentDate());
			slSwapIrsBean.setPayIntpaycycle(ifscfetsrmbirs.getFloatPaymentFrequency());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getLegInterestAccrualDateL());
//			slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getInterestResetFrequencyL());
			// 单利复利
			if ("0".equalsIgnoreCase(ifscfetsrmbirs.getInterestMethod())) {
				slSwapIrsBean.setPaycompound("F");
			} else {
				slSwapIrsBean.setPaycompound("S");
			}
			slSwapIrsBean.setPayBasis(ifscfetsrmbirs.getFloatLegDayCount());
			slSwapIrsBean.setPayYieldcurve(ifscfetsrmbirs.getRightDiscountCurve());
			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getRightRateCode());
			// 右边
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getFixedFloatR());
			slSwapIrsBean.setRecIntRate(ifscfetsrmbirs.getLegPrice());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getBenchmarkSpread());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getFloatPaymentDate());
			slSwapIrsBean.setRecIntpaycycle(ifscfetsrmbirs.getFixedPaymentFrequency());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getLegInterestAccrualDate());
//			slSwapIrsBean.setRateCode(ifscfetsrmbirs.getInterestResetFrequency());
			// 单利复利
			if ("0".equalsIgnoreCase(ifscfetsrmbirs.getInterestMethodL())) {
				slSwapIrsBean.setReccompound("F");
			} else {
				slSwapIrsBean.setReccompound("S");
			}
			slSwapIrsBean.setRecBasis(ifscfetsrmbirs.getFixedLegDayCount());
			slSwapIrsBean.setRecYieldcurve(ifscfetsrmbirs.getLeftDiscountCurve());
			slSwapIrsBean.setRecRateCode(ifscfetsrmbirs.getLeftRateCode());
		}
		/*
		 * if (ifscfetsrmbirs.getFixedLegDayCount().endsWith("1")) {
		 * slSwapIrsBean.setPayBasis("A360"); } else if
		 * (ifscfetsrmbirs.getFixedLegDayCount().endsWith("3")) {
		 * slSwapIrsBean.setPayBasis("A365"); }
		 * 
		 * if (ifscfetsrmbirs.getFloatLegDayCount().endsWith("1")) {
		 * slSwapIrsBean.setRecBasis("A360"); } else if
		 * (ifscfetsrmbirs.getFloatLegDayCount().endsWith("3")) {
		 * slSwapIrsBean.setRecBasis("A365"); } //
		 * slSwapIrsBean.setPayBasis(ifscfetsrmbirs.getFixedLegDayCount()); //
		 * slSwapIrsBean.setRecBasis(ifscfetsrmbirs.getFloatLegDayCount()); //支付周期转换
		 * String fixedPayIrsFreq = ifscfetsrmbirs.getFixedPaymentFrequency(); String
		 * floatPayIrsFreq = ifscfetsrmbirs.getFloatPaymentFrequency(); //
		 * slSwapIrsBean.setPayIntpaycycle(ifscfetsrmbirs.getFixedPaymentFrequency());
		 * //
		 * slSwapIrsBean.setRecIntpaycycle(ifscfetsrmbirs.getFloatPaymentFrequency());
		 * if("6".equals(fixedPayIrsFreq) || "6".equals(floatPayIrsFreq)){
		 * slSwapIrsBean.setPayIntpaycycle("W"); slSwapIrsBean.setRecIntpaycycle("W");
		 * }else if("3".equals(fixedPayIrsFreq) || "3".equals(floatPayIrsFreq)){
		 * slSwapIrsBean.setPayIntpaycycle("M"); slSwapIrsBean.setRecIntpaycycle("M");
		 * }else if("4".equals(fixedPayIrsFreq) || "4".equals(floatPayIrsFreq)){
		 * slSwapIrsBean.setPayIntpaycycle("Q"); slSwapIrsBean.setRecIntpaycycle("Q");
		 * }else if("0".equals(fixedPayIrsFreq) || "0".equals(floatPayIrsFreq)){
		 * slSwapIrsBean.setPayIntpaycycle("S"); slSwapIrsBean.setRecIntpaycycle("S");
		 * }else if("1".equals(fixedPayIrsFreq) || "1".equals(floatPayIrsFreq)){
		 * slSwapIrsBean.setPayIntpaycycle("A"); slSwapIrsBean.setRecIntpaycycle("A"); }
		 * 
		 * slSwapIrsBean.setPayIntRate(ifscfetsrmbirs.getLegPrice()); // 右端固定/浮动利率
		 * slSwapIrsBean.setRecIntRate(ifscfetsrmbirs.getRightRate());
		 * slSwapIrsBean.setTenor("99");
		 * 
		 * // 左端利率曲线
		 * slSwapIrsBean.setPayYieldcurve(ifscfetsrmbirs.getLeftDiscountCurve()); //
		 * 右端利率曲线
		 * slSwapIrsBean.setRecYieldcurve(ifscfetsrmbirs.getRightDiscountCurve()); //
		 * 左端利率代码 slSwapIrsBean.setRateCode(ifscfetsrmbirs.getLeftRateCode()); // 右端利率代码
		 * slSwapIrsBean.setRecRateCode(ifscfetsrmbirs.getRightRateCode()); // 交易方式
		 * slSwapIrsBean.setPlmethod(ifscfetsrmbirs.getAccountingMethod()); // 设置固定/浮动方向
		 * slSwapIrsBean.setSwapType(ifscfetsrmbirs.getFixedFloatDir()); // 左端币种
		 * slSwapIrsBean.setPayNotCcy(ifscfetsrmbirs.getCcy()); // 右端币种
		 * slSwapIrsBean.setRecNotCcy(ifscfetsrmbirs.getCcy()); // 到期日
		 * slSwapIrsBean.setMaturityDate(ifscfetsrmbirs.getEndDate()); // 左端金额 double
		 * lastQty=ifscfetsrmbirs.getLastQty().doubleValue()*10000;
		 * slSwapIrsBean.setPayNotCcyAmt(BigDecimal.valueOf(lastQty)); // 右端金额
		 * slSwapIrsBean.setRecNotCcyAmt(BigDecimal.valueOf(lastQty));
		 */
		return slSwapIrsBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsrmbIrs> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbIrsMapper.getRmbIrsMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbIrsMapper.getRmbIrsMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbIrsMapper.getRmbIrsMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> irsingList = taDictVoMapper.getTadictTree("irsing").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> calInstList = taDictVoMapper.getTadictTree("calInst").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> cleanMethoedList = taDictVoMapper.getTadictTree("CleanMethoed").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbIrs ifsCfetsrmbIrs: list) {
			ifsCfetsrmbIrs.setMyDir(irsingList.get(ifsCfetsrmbIrs.getMyDir()));
			ifsCfetsrmbIrs.setCalculateAgency(calInstList.get(ifsCfetsrmbIrs.getCalculateAgency()));
			ifsCfetsrmbIrs.setTradeMethod(cleanMethoedList.get(ifsCfetsrmbIrs.getTradeMethod()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
