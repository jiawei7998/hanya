package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsOpicsSposMapper;
import com.singlee.ifs.model.IfsOpicsSpos;
import com.singlee.ifs.service.IfsOpicsSposService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/17 13:53
 * @description：${description}
 * @modified By：
 * @version:
 */
@Service
public class IfsOpicsSposServiceImpl implements IfsOpicsSposService {

    @Resource
    private IfsOpicsSposMapper ifsOpicsSposMapper;

    @Override
    public int insert(IfsOpicsSpos record) {
        return ifsOpicsSposMapper.insert(record);
    }

    @Override
    public int insertSelective(IfsOpicsSpos record) {
        return ifsOpicsSposMapper.insertSelective(record);
    }

    @Override
    public int batchInsert(List<IfsOpicsSpos> list) {
        return ifsOpicsSposMapper.batchInsert(list);
    }

    @Override
    public int deleteByPrimaryKey(String secid, String invtype, String cost, String port, String acctngtype) {
        return ifsOpicsSposMapper.deleteByPrimaryKey(secid, invtype, cost, port, acctngtype);
    }

    @Override
    public IfsOpicsSpos selectByPrimaryKey(String secid, String invtype, String cost, String port, String acctngtype) {
        return ifsOpicsSposMapper.selectByPrimaryKey(secid, invtype, cost, port, acctngtype);
    }

    @Override
    public int updateByPrimaryKeySelective(IfsOpicsSpos record) {
        return ifsOpicsSposMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(IfsOpicsSpos record) {
        return ifsOpicsSposMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<IfsOpicsSpos> list) {
        return ifsOpicsSposMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<IfsOpicsSpos> list) {
        return ifsOpicsSposMapper.updateBatchSelective(list);
    }

    @Override
    public Page<IfsOpicsSpos> getSposPage(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        ifsOpicsSposMapper.ifsOpicsSposCreate(map);//无论是否成功直接展现
        return ifsOpicsSposMapper.getSposPage(map, rb);
    }

}

