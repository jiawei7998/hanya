package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IRepoServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsrmbOr;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsRmbOrService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbOrService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper;
	@Autowired
	IfsOpicsBondMapper bondMapper;
	@Autowired
	IRepoServer repoServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key)
			throws Exception {
		// 查询当前节点下配置的事件 #{flow_id} AND TASK_DEF_KEY = #{task_def_key}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("flow_id", flow_id);
		map.put("task_def_key", task_def_key);
		map.put("prd_code", "441");
		IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchRepo(serial_no);
		accountAmt(map, "IFS_CFETSRMB_OR", BeanUtil.beanToMap(ifsCfetsrmbOr), "totalFaceValue");
		return null;
	}

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbOrMapper.updateRmbOrByID(serial_no, status, date);
		// 获取branch_id
		IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchRepo(serial_no);

		if (null == ifsCfetsrmbOr) {
			return;
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("P".equals(ifsCfetsrmbOr.getMyDir())){//逆回购
					if(!"0".equals(ifsCfetsrmbOr.getQuotaOccupyType())){
						hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
					}
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		/*
		 * 买断式回购按到期日释放额度，不在继续按照券的正回购清算是释放额度 if("6".equals(status)){//6的状态为结算完成
		 * //释放额度,重新占用 List<EdInParams> creditListLast = new ArrayList<EdInParams>();
		 * creditListLast = ifsCfetsfxLendMapper.searchAllInfo(serial_no); for(int
		 * i=0;i<creditListLast.size();i++){ String prdNo =
		 * creditListLast.get(i).getPrdNo(); String direction =
		 * creditListLast.get(i).getDirection(); if("S".equals(direction)){//卖出
		 * Map<String ,String> paramMap = new HashMap<String, String>();
		 * paramMap.put("serial_no", serial_no); paramMap.put("product_type",
		 * creditListLast.get(i).getPrdNo()); paramMap.put("pureFlag",
		 * "1");//清算之后卖出的限额进行限制 edCustManangeService.chooseOccupy(paramMap);
		 * 
		 * paramMap.put("code", "0"); paramMap.put("product_type", prdNo); String inFlag
		 * = "1";//作为判断标识符，为0时是审批台发起，为1为清算后发起
		 * edCustManangeService.limitBondExchange(paramMap,inFlag);
		 * 
		 * } }
		 * 
		 * }
		 */
		// /////以下将审批中的记录插入审批监控表
		if ("5".equals(status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段

			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbOr.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbOr.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbOr.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("买断式回购");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("or");// 设置监控表-产品编号：作为分类
			// BigDecimal amt = ifsCfetsrmbOr.getTotalFaceValue();
			// BigDecimal mul = new BigDecimal("10000");
			// ifsFlowMonitor.setAmt(String.valueOf(amt==null?"":amt.multiply(mul)));//设置监控表-本金
			// ifsFlowMonitor.setRate(String.valueOf(ifsCfetsrmbOr.getRepoRate()==null?"":ifsCfetsrmbOr.getRepoRate()));//设置监控表-利率
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbOr.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbOr.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbOr.getReverseInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_OR");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_OR where TICKET_ID=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbOrMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("P".equals(ifsCfetsrmbOr.getMyDir())) {//逆回购
						if(!"0".equals(ifsCfetsrmbOr.getQuotaOccupyType())){
							LimitOccupy(serial_no, TradeConstants.ProductCode.OR);
						}
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> mapEntity = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(mapEntity);
		}
		// 只有审批状态为'审批通过'才调用存储过程
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			String callType = sysList.get(0).getP_type().trim();
			// 调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbOr.getBr());
				SlRepoOrBean slRepoOrBean = getEntry(ifsCfetsrmbOr, branchDate);
				SlOutBean result = repoServer.repoOr(slRepoOrBean);
				if (result.getRetStatus().equals(RetStatusEnum.S)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsrmbOr.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsrmbOrMapper.updateStatcodeByTicketId(map);
					/************ 买断的券 ***************/
					SlRepoOrBean orBeanDetail = getEntryDetail(ifsCfetsrmbOr, slRepoOrBean, branchDate);
					SlOutBean result2 = repoServer.repoOr(orBeanDetail);
					if (result2.getRetStatus().equals(RetStatusEnum.S)) {
						Map<String, Object> map2 = new HashMap<String, Object>();
						map2.put("ticketId", ifsCfetsrmbOr.getTicketId());
						map2.put("bondStatcode", "-5");
						ifsCfetsrmbOrMapper.updateBondStatcodeByTicketId(map2);
					} else {
						JY.raise("审批未通过,券插入opics失败......");
					}

				} else {
					JY.raise("审批未通过,交易插入opcis失败......");
				}
			} else {
				// 将实体对象转换成map
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbOr);
				// 正式调用存储过程
				SlOutBean result = repoServer.ircaProc(map);// 买断式回购存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，调用存储过程失败......");
				}
			}
		} catch (Exception e) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("fedealno", serial_no);
			map.put("errorcode", e.getMessage());
			ifsCfetsrmbOrMapper.updateCfetsrmbOrByFedealno(map);
			JY.raiseRException("审批未通过......", e);
		}

	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchRepo(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbOr.getCustNo());
		remap.put("custType", ifsCfetsrmbOr.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbOr.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbOr.getPositiveInst());
		BigDecimal amt = calcuAmt(ifsCfetsrmbOr.getFirstSettlementAmount(), ifsCfetsrmbOr.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbOr.getForDate());
		remap.put("mdate", ifsCfetsrmbOr.getSecondSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取回购交易主体
	 * 
	 * @param ifsCfetsrmbOr
	 * @param branchDate
	 * @return
	 */
	private SlRepoOrBean getEntry(IfsCfetsrmbOr ifsCfetsrmbOr, Date branchDate) {
		// 交易对手
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsCfetsrmbOr.getReverseInst());
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbOr.getReverseInst());
		// 本方交易员
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbOr.getPositiveTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbOr.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.REPO.IOPER);
		institutionInfo.setTradeId(trad);
		// OPICS拓展属性
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setProdcode(ifsCfetsrmbOr.getProduct());
		externalBean.setProdtype(ifsCfetsrmbOr.getProdType());
		externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbOr.getContractId());
		externalBean.setSiind(SlDealModule.REPO.SIIND);
		externalBean.setSupconfind(SlDealModule.REPO.SUPPCONFIND);
		externalBean.setSuppayind(SlDealModule.REPO.SUPPPAYIND);
		externalBean.setSuprecind(SlDealModule.REPO.SUPPRECIND);
		externalBean.setVerind(SlDealModule.REPO.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbOr.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbOr.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbOr.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbOr.getCtrsacct());

		/************ 回购本身 ***************/
		SlRepoOrBean slRepoOrBean = new SlRepoOrBean(ifsCfetsrmbOr.getBr(), SlDealModule.REPO.SERVER,
				SlDealModule.REPO.TAG, SlDealModule.REPO.DETAIL);
		slRepoOrBean.getInthBean().setIoper(trad);
		slRepoOrBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slRepoOrBean.setInstitutionInfo(institutionInfo);
		slRepoOrBean.setAssignseq("0");
		slRepoOrBean.setSafekeepacct(ifsCfetsrmbOr.getPositiveCustname());
		slRepoOrBean.setCcy("CNY");
		slRepoOrBean.setValueDate(DateUtil.parse(ifsCfetsrmbOr.getFirstSettlementDate()));
		slRepoOrBean.setMaturityDate(DateUtil.parse(ifsCfetsrmbOr.getSecondSettlementDate()));
		slRepoOrBean.setCollateralcode(SlDealModule.REPO.COLLATERALCODE);
		// 回购成本中心和投资组合
		slRepoOrBean.setRepocost(ifsCfetsrmbOr.getCost());
		// slRepoOrBean.setRepoport(ifsCfetsrmbOr.getPort());
		slRepoOrBean.setDealDate(DateUtil.parse(ifsCfetsrmbOr.getForDate()));
		slRepoOrBean.setDelivtype(SlDealModule.REPO.DELIVTYPE);
		slRepoOrBean.setCoupreinvest("N");
		slRepoOrBean.setBasis(ifsCfetsrmbOr.getBasis());
		// 回购利率
		slRepoOrBean.setReporate(ifsCfetsrmbOr.getRepoRate());
		slRepoOrBean.setTabindicator("1");
		slRepoOrBean.setTenor("99");
		slRepoOrBean.setCollunit(SlDealModule.REPO.COLLUNIT_OR);
		slRepoOrBean.setQty(String.valueOf(MathUtil.roundWithNaN(
				Double.parseDouble((ifsCfetsrmbOr.getTotalFaceValue().multiply(new BigDecimal(10000)) + "")), 2)));
		slRepoOrBean.setInvtype(ifsCfetsrmbOr.getInvtype());
		slRepoOrBean.setDcpriceind(SlDealModule.REPO.DCPRICEIND);
		slRepoOrBean.setIntcalctype(SlDealModule.REPO.INTCALCTYPE);
		slRepoOrBean.setFullyassignind("N");
		slRepoOrBean.setPmvind(SlDealModule.REPO.PMVIND);
		slRepoOrBean.setExternalBean(externalBean);
		SlInthBean inthBean = slRepoOrBean.getInthBean();
		inthBean.setSeq("0");
		inthBean.setStatcode("-1");
		inthBean.setLstmntdate(branchDate);
		inthBean.setFedealno(ifsCfetsrmbOr.getTicketId());
		slRepoOrBean.setInthBean(inthBean);
		slRepoOrBean.setComccysmeans(ifsCfetsrmbOr.getCcysmeans());
		slRepoOrBean.setComccysacct(ifsCfetsrmbOr.getCcysacct());
		slRepoOrBean.setMatccysmeans(ifsCfetsrmbOr.getCtrsmeans());
		slRepoOrBean.setMatccysacct(ifsCfetsrmbOr.getCtrsacct());
		return slRepoOrBean;
	}

	/**
	 * 获取抵押权信息
	 * 
	 * @param ifsCfetsrmbOr
	 * @param slRepoOrBean
	 * @param branchDate
	 * @return
	 */
	private SlRepoOrBean getEntryDetail(IfsCfetsrmbOr ifsCfetsrmbOr, SlRepoOrBean slRepoOrBean, Date branchDate) {
		SlRepoOrBean slRepoOrBean2 = new SlRepoOrBean(ifsCfetsrmbOr.getBr(), SlDealModule.REPO.SERVER,
				SlDealModule.REPO.TAG, SlDealModule.REPO.DETAIL);
		slRepoOrBean2.setContraPatryInstitutionInfo(slRepoOrBean.getContraPatryInstitutionInfo());
		slRepoOrBean2.setInstitutionInfo(slRepoOrBean.getInstitutionInfo());
		slRepoOrBean2.setAssignseq("1");
		slRepoOrBean2.setSafekeepacct(ifsCfetsrmbOr.getPositiveCustname());
		slRepoOrBean2.setCcy("CNY");
		slRepoOrBean2.setValueDate(DateUtil.parse(ifsCfetsrmbOr.getFirstSettlementDate()));
		slRepoOrBean2.setMaturityDate(DateUtil.parse(ifsCfetsrmbOr.getSecondSettlementDate()));
		slRepoOrBean2.setCollateralcode(SlDealModule.REPO.COLLATERALCODE);
		// 回购的成本中心和投资组合
		slRepoOrBean2.setRepocost(ifsCfetsrmbOr.getCost());
		// slRepoOrBean2.setRepoport(ifsCfetsrmbOr.getPort());
		slRepoOrBean2.setDealDate(DateUtil.parse(ifsCfetsrmbOr.getForDate()));
		slRepoOrBean2.setDelivtype(SlDealModule.REPO.DELIVTYPE);
		slRepoOrBean2.setCoupreinvest("N");
		slRepoOrBean2.setBasis(ifsCfetsrmbOr.getBasis());
		// 回购利率
		slRepoOrBean2.setReporate(ifsCfetsrmbOr.getRepoRate());
		slRepoOrBean2.setTabindicator("2");
		slRepoOrBean2.setTenor("99");
		slRepoOrBean2.setSecurityId(ifsCfetsrmbOr.getBondCode());
		slRepoOrBean2.setCollunit(SlDealModule.REPO.COLLUNIT_OR);
		// QTY
		slRepoOrBean2.setQty(String.valueOf(ifsCfetsrmbOr.getTotalFaceValue().multiply(new BigDecimal(10000))));
		// AMOUNT-- 首期结算金额
//		SecurityAmtDetailBean comSecurityAmt=new SecurityAmtDetailBean();
//		comSecurityAmt.setAmt(ifsCfetsrmbOr.getFirstSettlementAmount());
//		slRepoOrBean2.setComSecurityAmt(comSecurityAmt);
		// 全价
		SecurityAmtDetailBean matSecurityAmt = new SecurityAmtDetailBean();
		matSecurityAmt.setPrice(ifsCfetsrmbOr.getSecondDirtyPrice());
		slRepoOrBean2.setMatSecurityAmt(matSecurityAmt);
		slRepoOrBean2.setInvtype(ifsCfetsrmbOr.getInvtype());
		slRepoOrBean2.setDcpriceind(SlDealModule.REPO.DCPRICEIND);
		slRepoOrBean2.setIntcalctype(SlDealModule.REPO.INTCALCTYPE);
		slRepoOrBean2.setFullyassignind("Y");
		slRepoOrBean2.setPmvind(SlDealModule.REPO.PMVIND);
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setProdcode(ifsCfetsrmbOr.getProduct());
		externalBean.setProdtype(ifsCfetsrmbOr.getProdType());
		externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbOr.getContractId());
		externalBean.setSiind(SlDealModule.REPO.SIIND);
		externalBean.setSupconfind(SlDealModule.REPO.SUPPCONFIND);
		externalBean.setSuppayind(SlDealModule.REPO.SUPPPAYIND);
		externalBean.setSuprecind(SlDealModule.REPO.SUPPRECIND);
		externalBean.setVerind(SlDealModule.REPO.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbOr.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbOr.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbOr.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbOr.getCtrsacct());
		// 券的成本中心和投资组合
		externalBean.setCost(ifsCfetsrmbOr.getBondCost());
		externalBean.setPort(ifsCfetsrmbOr.getBondPort());
		slRepoOrBean2.setExternalBean(externalBean);
		SlInthBean inthBean2 = slRepoOrBean2.getInthBean();
		inthBean2.setSeq("1");
		inthBean2.setStatcode("-5");
		inthBean2.setLstmntdate(branchDate);
		inthBean2.setFedealno(ifsCfetsrmbOr.getTicketId());
		slRepoOrBean2.setInthBean(inthBean2);
		slRepoOrBean2.setComccysmeans(ifsCfetsrmbOr.getCcysmeans());
		slRepoOrBean2.setComccysacct(ifsCfetsrmbOr.getCcysacct());
		slRepoOrBean2.setMatccysmeans(ifsCfetsrmbOr.getCtrsmeans());
		slRepoOrBean2.setMatccysacct(ifsCfetsrmbOr.getCtrsacct());
		return slRepoOrBean2;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsrmbOr> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbOrMapper.getRmbOrMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbOrMapper.getRmbOrMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbOrMapper.getRmbOrMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> repoingList = taDictVoMapper.getTadictTree("repoing").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> basisList = taDictVoMapper.getTadictTree("Basis").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbOr ifsCfetsrmbOr: list) {
			ifsCfetsrmbOr.setMyDir(repoingList.get(ifsCfetsrmbOr.getMyDir()));
			ifsCfetsrmbOr.setBasis(basisList.get(ifsCfetsrmbOr.getBasis()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

}
