package com.singlee.ifs.service.impl;


import com.singlee.ifs.mapper.ListPriceMapper;
import com.singlee.ifs.model.FileNameList;
import com.singlee.ifs.service.ListPriceJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class ListPriceJobServiceImpl implements ListPriceJobService {

    @Autowired
    ListPriceMapper listPriceMapper;
    

    
    @Override
    public void insert(Map<String, Object> map) {
        
        listPriceMapper.insertPrice(map);
    }

    @Override
    public List<FileNameList> getlist(Map<String, Object>  map) {
      
        
        List<FileNameList> list=listPriceMapper.getPrice(map);
        return list;
    }


}

