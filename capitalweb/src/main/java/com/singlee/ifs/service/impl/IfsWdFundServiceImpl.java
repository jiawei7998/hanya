package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.mapper.IfsWdFundMapper;
import com.singlee.ifs.model.ChinaMutualFundNAV;
import com.singlee.ifs.service.IfsWdFundService;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsWdFundServiceImpl implements IfsWdFundService{

    @Autowired
    IfsWdFundMapper  ifsWdFundMapper;
    @Autowired
    CFtInfoMapper  cFtInfoMapper;


    @Override
    public Page<ChinaMutualFundNAV> getWdFundList(Map<String, Object> map) {
        
        List<ChinaMutualFundNAV> list=ifsWdFundMapper.getFundList(map);
        
        int pageNumber=(int)map.get("pageNumber");
        int pageSize=(int)map.get("pageSize");
        
        Page<ChinaMutualFundNAV> page=HrbReportUtils.producePage(list, pageNumber, pageSize);
        
        return page;
    }

    @Override
    public List<ChinaMutualFundNAV> getWdFundAll(Map<String, Object> map) {

        List<ChinaMutualFundNAV> list=ifsWdFundMapper.getFundList(map);
        return list;
    }

    @Override
    public String doFundExcelToData(List<CFtInfo> cFtInfoBean) {
        int seccess = 0;
        int defeat = 0;
        for (int i = 0; i < cFtInfoBean.size(); i++) {
            String fundCode = cFtInfoBean.get(i).getFundCode();
            if (fundCode!= "") {
                if (cFtInfoMapper.selectByPrimaryKey(fundCode)==null) { // 判断表中是否有同样数据
                    // 若表中没有，则插入
                    cFtInfoMapper.insert(cFtInfoBean.get(i));
                    seccess = seccess + 1;
                } else {
                    defeat = defeat + 1;
                }
            } else {
                defeat = defeat + 1;
            }
        }
        if (defeat > 0) {
            return "插入失败" + defeat + "条(该同业客户已存在或没有输入客户信息)";
        }
        return "插入成功" + seccess + "条";

    }

}
