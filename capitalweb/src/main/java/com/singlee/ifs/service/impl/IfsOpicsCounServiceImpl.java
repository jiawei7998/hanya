package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlCounBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsCounMapper;
import com.singlee.ifs.model.IfsOpicsCoun;
import com.singlee.ifs.service.IfsOpicsCounService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 国家代码1
 * 
 * @author lij
 * 
 */
@Service
public class IfsOpicsCounServiceImpl implements IfsOpicsCounService {
	@Resource
	IfsOpicsCounMapper ifsOpicsCounMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsCoun entity) {
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsCounMapper.insert(entity);
	}

	@Override
	public void updateById(IfsOpicsCoun entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsCounMapper.updateById(entity);

	}

	@Override
	public void deleteById(String id) {
		ifsOpicsCounMapper.deleteById(id);
	}

	@Override
	public Page<IfsOpicsCoun> searchPageOpicsCoun(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsCoun> result = ifsOpicsCounMapper.searchPageOpicsCoun(map, rb);
		return result;
	}

	@Override
	public IfsOpicsCoun searchById(String id) {
		return ifsOpicsCounMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsCoun entity) {
		ifsOpicsCounMapper.updateStatus(entity);
	}

	/***
	 * 批量校准 ：
	 * 
	 * @param type
	 *            1:对选中的行进行校准 2.对本地库所有工业标准码进行校准
	 * @param ccodes
	 *            存ccode的数组，用逗号隔开
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] ccodes) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从opics库查询所有的数据
			List<SlCounBean> listCounOpics = iStaticServer.synchroCoun();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			/***** 对选中的行进行校准 ****************************/
			if ("1".equals(type)) {// 选中行校准

				for (int i = 0; i < ccodes.length; i++) {
					for (int j = 0; j < listCounOpics.size(); j++) {
						// 若opics库里有相同的国家代码,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if (listCounOpics.get(j).getCcode().trim().equals(ccodes[i])) {
							IfsOpicsCoun entity = new IfsOpicsCoun();
							entity = ifsOpicsCounMapper.searchById(ccodes[i]);
							entity.setCcode(listCounOpics.get(j).getCcode().trim());

							String coun = listCounOpics.get(j).getCoun();
							if ("".equals(coun) || coun == null) {
								entity.setCoun("");
							} else {
								entity.setCoun(coun.trim());
							}

							String lstmntdte = listCounOpics.get(j).getLstmntdte();
							// 若opics系统此条工业代码没有设置最后维护时间,就设置最新时间
							if ("".equals(lstmntdte) || lstmntdte == null) {
								entity.setLstmntdte(new Date());
							} else {
								entity.setLstmntdte(sdf.parse(lstmntdte));
							}
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");// 设置为已同步
							ifsOpicsCounMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listCounOpics.size() - 1) {
							ifsOpicsCounMapper.updateStatus0ById(ccodes[i]);
						}

					}

				}

				/************* 对本地库所有国家代码记录进行校准 ***************/
			} else {// 全部校准
					// 从本地库查询所有的数据
				List<IfsOpicsCoun> listCounLocal = ifsOpicsCounMapper.searchAllOpicsCoun();
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listCounOpics.size(); i++) {

					/***** ++++++++++++++++++++ 实体设置 start ++++++++++++++++++++++ ***********/
					IfsOpicsCoun entity = new IfsOpicsCoun();
					//将本地数据存到实体类中，2019-4-9处理批量更新中文名称会被覆盖问题
					//entity = ifsOpicsCounMapper.searchById(listCounOpics.get(i).getCcode().trim());
					entity.setCcode(listCounOpics.get(i).getCcode().trim());
					// entity.setCoun(listCounOpics.get(i).getCoun());

					String coun = listCounOpics.get(i).getCoun();
					if ("".equals(coun) || coun == null) {
						entity.setCoun("");
					} else {
						entity.setCoun(coun.trim());
					}
					// entity.setLstmntdte(sdf.parse(listCounOpics.get(i).getLstmntdte()));
					String lstmntdte = listCounOpics.get(i).getLstmntdte();
					// 若opics系统此条工业代码没有设置最后维护时间,就设置最新时间
					if ("".equals(lstmntdte) || lstmntdte == null) {
						entity.setLstmntdte(new Date());
					} else {
						entity.setLstmntdte(sdf.parse(lstmntdte));
					}
					entity.setStatus("1");// 设置为已同步
					entity.setOperator(SlSessionHelper.getUserId());
					/***** ++++++++++++++++++++ 实体设置 end +++++++++++++++++++++++ ***********/

					if (listCounLocal.size() == 0) {// 本地库无数据
						ifsOpicsCounMapper.insert(entity);
					} else {// 本地库有数据
						for (int j = 0; j < listCounLocal.size(); j++) {

							// opics库中国家代码有与本地的国家代码相等，就更新本地的国家代码其他的内容
							if (listCounOpics.get(i).getCcode().trim().equals(listCounLocal.get(j).getCcode())) {
								ifsOpicsCounMapper.updateById(entity);
								break;
							}
							// 若没有，就插入本地库
							if (j == listCounLocal.size() - 1) {
								ifsOpicsCounMapper.insert(entity);
							}
						}

					}

				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				// 遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				List<IfsOpicsCoun> listCounLocal2 = ifsOpicsCounMapper.searchAllOpicsCoun();
				if (listCounLocal2.size() > listCounOpics.size()) {
					for (int i = 0; i < listCounLocal2.size(); i++) {
						for (int j = 0; j < listCounOpics.size(); j++) {
							if (listCounOpics.get(j).getCcode().trim().equals(listCounLocal2.get(i).getCcode())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为 未同步
							if (j == listCounOpics.size() - 1) {
								IfsOpicsCoun entity = new IfsOpicsCoun();
								entity.setCcode(listCounLocal2.get(i).getCcode());
								entity.setCoun(listCounLocal2.get(i).getCoun());
								entity.setCouncn(listCounLocal2.get(i).getCouncn());
								entity.setRemark(listCounLocal2.get(i).getRemark());
								entity.setLstmntdte(new Date());
								entity.setStatus("0");// 设置为未同步
								entity.setOperator(SlSessionHelper.getUserId());
								ifsOpicsCounMapper.updateById(entity);

							}

						}

					}

				}

			}

		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

	/**
	 * 查询所有的国家代码
	 */
	@Override
	public Page<IfsOpicsCoun> searchAllOpicsCoun() {
		Page<IfsOpicsCoun> result = ifsOpicsCounMapper.searchAllOpicsCoun();
		return result;
	}

}
