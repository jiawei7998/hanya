package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSaccBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsSaccMapper;
import com.singlee.ifs.model.IfsOpicsSacc;
import com.singlee.ifs.service.IfsOpicsSaccService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SACC
 * 
 * @author yanming
 */

@Service
public class IfsOpicsSaccServiceImpl implements IfsOpicsSaccService {

	@Resource
	IfsOpicsSaccMapper ifsOpicsSaccMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsSacc entity) {

		// 设置最新维护时间
		entity.setLstmntdate(new Date());
		entity.setStatus("0");
		ifsOpicsSaccMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsSacc entity) {

		entity.setLaststmtdate(new Date());
		ifsOpicsSaccMapper.updateById(entity);

	}

	@Override
	public void deleteById(Map<String, String> map) {

		ifsOpicsSaccMapper.deleteById(map);

	}

	@Override
	public Page<IfsOpicsSacc> searchPageOpicsSacc(Map<String, Object> map) {

		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsSacc> result = ifsOpicsSaccMapper.searchPageOpicsSacc(map, rb);
		return result;
	}

	@Override
	public IfsOpicsSacc searchById(Map<String, String> map) {

		return ifsOpicsSaccMapper.searchById(map);
	}

	@Override
	public void updateStatus(IfsOpicsSacc entity) {

		ifsOpicsSaccMapper.updateStatus(entity);

	}

	@Override
	public SlOutBean batchCalibration(String type, String[] accountnos, String[] brs) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			List<SlSaccBean> listSaccOpics = iStaticServer.synchroSacc();
			HashMap<String, String> map = new HashMap<String, String>();
			if ("1".equals(type)) {// 选中行
				for (int i = 0; i < accountnos.length; i++) {
					for (int j = 0; j < listSaccOpics.size(); j++) {
						map.put("br", brs[i]);
						map.put("accountno", accountnos[i]);
						if (listSaccOpics.get(j).getAccountno().trim().equals(accountnos[i]) && listSaccOpics.get(j).getBr().trim().equals(brs[i])) {

							IfsOpicsSacc entity = new IfsOpicsSacc();
							entity = ifsOpicsSaccMapper.searchById(map);

							entity.setAccountno(listSaccOpics.get(j).getAccountno().trim());
							entity.setBr(listSaccOpics.get(j).getBr().trim());

							String trad = listSaccOpics.get(j).getTrad();
							if ("".equals(trad) || trad == null) {
								entity.setTrad("0");
							} else {
								entity.setTrad(trad);
							}
							Date opendate = listSaccOpics.get(j).getOpendate();
							if ("".equals(opendate) || opendate == null) {
								entity.setOpendate(new Date());
							} else {
								entity.setOpendate(opendate);
							}
							String accttitle = listSaccOpics.get(j).getAccttitle();
							if ("".equals(accttitle) || accttitle == null) {
								entity.setAccttitle("");
							} else {
								entity.setAccttitle(accttitle);
							}
							String cost = listSaccOpics.get(j).getCost();
							if ("".equals(cost) || cost == null) {
								entity.setCost("");
							} else {
								entity.setCost(cost);
							}
							String cno = listSaccOpics.get(j).getCno();
							if ("".equals(cno) || cno == null) {
								entity.setCno("");
							} else {
								entity.setCno(cno);
							}
							Date lstmntdate = listSaccOpics.get(j).getLstmntdate();
							if ("".equals(lstmntdate) || lstmntdate == null) {
								entity.setLstmntdate(new Date());
							} else {
								entity.setLstmntdate(lstmntdate);
							}
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");// 设置为已同步
							ifsOpicsSaccMapper.updateById(entity);
							break;
						}
						if (j == listSaccOpics.size() - 1) {
							HashMap<String, String> statusmap = new HashMap<String, String>();
							statusmap.put("br", brs[i]);
							statusmap.put("accountno", accountnos[i]);
							ifsOpicsSaccMapper.updateStatus0ById(statusmap);
						}
					}
				}
			} else {// 全部校准
				List<IfsOpicsSacc> listSaccLocal = ifsOpicsSaccMapper.searchAllOpicsSacc();
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listSaccOpics.size(); i++) {
					IfsOpicsSacc entity = new IfsOpicsSacc();
					entity.setAccountno(listSaccOpics.get(i).getAccountno().trim());
					entity.setBr(listSaccOpics.get(i).getBr().trim());
					String trad = listSaccOpics.get(i).getTrad();
					if ("".equals(trad) || trad == null) {
						entity.setTrad("0");
					} else {
						entity.setTrad(trad);
					}
					Date opendate = listSaccOpics.get(i).getOpendate();
					if ("".equals(opendate) || opendate == null) {
						entity.setOpendate(new Date());
					} else {
						entity.setOpendate(opendate);
					}
					String accttitle = listSaccOpics.get(i).getAccttitle();
					if ("".equals(accttitle) || accttitle == null) {
						entity.setAccttitle("");
					} else {
						entity.setAccttitle(accttitle);
					}
					String cost = listSaccOpics.get(i).getCost();
					if ("".equals(cost) || cost == null) {
						entity.setCost("");
					} else {
						entity.setCost(cost);
					}
					String cno = listSaccOpics.get(i).getCno();
					if ("".equals(cno) || cno == null) {
						entity.setCno("");
					} else {
						entity.setCno(cno);
					}
					Date lstmntdate = listSaccOpics.get(i).getLstmntdate();
					if ("".equals(lstmntdate) || lstmntdate == null) {
						entity.setLstmntdate(new Date());
					} else {
						entity.setLstmntdate(lstmntdate);
					}
					entity.setStatus("1");// 设置为已同步
					entity.setOperator(SlSessionHelper.getUserId());
					if (listSaccLocal.size() == 0) {// 本地无数据，插入
						ifsOpicsSaccMapper.insert(entity);
					} else {
						// 本地有数据，更新
						for (int j = 0; j < listSaccLocal.size(); j++) {
							if (listSaccOpics.get(i).getBr().trim().equals(listSaccLocal.get(j).getBr()) && listSaccOpics.get(i).getAccountno().trim().equals(listSaccLocal.get(j).getAccountno())) {
								ifsOpicsSaccMapper.updateById(entity);
								break;
							}
							// 若没有数据，插入
							if (j == listSaccLocal.size() - 1) {
								ifsOpicsSaccMapper.insert(entity);
							}
						}
					}
				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				List<IfsOpicsSacc> listSaccLocal2 = ifsOpicsSaccMapper.searchAllOpicsSacc();
				if (listSaccLocal2.size() > listSaccOpics.size()) {
					for (int i = 0; i < listSaccLocal2.size(); i++) {
						for (int j = 0; j < listSaccOpics.size(); j++) {
							if (listSaccOpics.get(j).getBr().trim().equals(listSaccLocal2.get(i).getBr()) && listSaccOpics.get(j).getAccountno().trim().equals(listSaccLocal2.get(i).getAccountno())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为 未同步
							if (j == listSaccOpics.size() - 1) {
								IfsOpicsSacc entity = new IfsOpicsSacc();
								entity.setBr(listSaccLocal2.get(i).getBr());
								entity.setAccountno(listSaccLocal2.get(i).getAccountno());
								entity.setTrad(listSaccLocal2.get(i).getTrad());
								entity.setOpendate(new Date());
								entity.setAccttitle(listSaccLocal2.get(i).getAccttitle());
								entity.setCost(listSaccLocal2.get(i).getCost());
								entity.setCno(listSaccLocal2.get(i).getCno());
								entity.setLstmntdate(new Date());
								entity.setStatus("0");// 设置状态为未同步
								entity.setOperator(SlSessionHelper.getUserId());
								ifsOpicsSaccMapper.updateById(entity);
							}
						}
					}
				}
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}
}
