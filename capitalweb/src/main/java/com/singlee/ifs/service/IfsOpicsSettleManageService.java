/**
 * Project Name:capitalweb
 * File Name:IfsOpicsSettleManageService.java
 * Package Name:com.singlee.ifs.service
 * Date:2018-9-7下午03:59:39
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCspiCsriDetail;
import com.singlee.ifs.model.IfsCspiCsriHead;
import com.singlee.ifs.model.IfsSdvpDetail;
import com.singlee.ifs.model.IfsSdvpHead;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * ClassName:IfsOpicsSettleManageService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-9-7 下午03:59:39 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public interface IfsOpicsSettleManageService {

	public void saveCspiAndCsri(IfsCspiCsriHead record);

	public Page<IfsCspiCsriHead> searchPageCspiAndCsri(Map<String, Object> map);

	public void updateCspiAndCsri(IfsCspiCsriHead record);

	public List<IfsCspiCsriDetail> searchCspiCsriDetails(Map<String, Object> map);

	public void updateSlflag(String fedealno, String userid, Date date,
			String slflag);

	public void deleteByFedealno(Map<String, Object> map);

	public Page<IfsSdvpHead> searchPageSdvp(Map<String, Object> map);

	public List<IfsSdvpDetail> searchSdvpDetails(Map<String, Object> map);

	public void saveSdvp(IfsSdvpHead record);

	public void updateSdvp(IfsSdvpHead record);

	public void updateSdvpSlflag(String fedealno, String userid, Date date,
			String slflag);

	public void deleteSdvpByFedealno(Map<String, Object> map);

	public Page<IfsSdvpDetail> searchPageSdvpDetails(Map<String, Object> map);

	public Page<IfsCspiCsriDetail> searchPageCspiAndCsriDetails(
			Map<String, Object> map);

	public IfsCspiCsriHead searchCspiCsriHeadById(String fedealno);
	
	public IfsSdvpHead searchSdvpHeadById(String fedealno);

	public void updateSyncStatus(IfsCspiCsriHead cspiCsriHead);
	
	public void updateSdvpSyncStatus(IfsSdvpHead record);

	public void updateCspiCsriByFedealno(Map<String, Object> opicsmap);

	public void updateSdvpByFedealno(Map<String, Object> opicsmap);
	
	//获取系统当前时间
	public String getCurDate();

	public int getCspiCsriById(IfsCspiCsriHead record);
	
	public int getSdvpById(IfsSdvpHead record);
	/**
	 * 所有产品校验清算路径
	 * @param prdNo
	 * @param ticketId
	 * @return
	 */
	public String checkSearchSettInfoAll( String prdNo,String ticketId);
}

