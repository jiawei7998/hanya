package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsTradeElementParamMapper;
import com.singlee.ifs.model.IfsTradeParaGroup;
import com.singlee.ifs.service.IfsTradeElementParamService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
/**
 * 交易要素  实现类
 * @author singlee4
 *
 */
@Service
public class IfsTradeElementParamServiceImpl implements IfsTradeElementParamService {
	@Autowired
	IfsTradeElementParamMapper ifsTradeParamMapper;
	
	
	@Override
	public void insert(IfsTradeParaGroup entity) {
		//设置 录入时间
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		//设置 上次修改时间
		entity.setLastTime(DateUtil.getCurrentDateTimeAsString());
		String groupId = DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setGroupId(groupId);
		ifsTradeParamMapper.insert(entity);
	}

	@Override
	public void updateById(IfsTradeParaGroup entity) {
		//设置 上次修改时间
		entity.setLastTime(DateUtil.getCurrentDateTimeAsString());
		
		ifsTradeParamMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsTradeParamMapper.deleteById(id);
	}

	@Override
	public Page<IfsTradeParaGroup> searchPageOpicsTrdParam(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsTradeParaGroup> result = ifsTradeParamMapper.searchPageOpicsTrdParam(map, rb);
		return result;
	}

}
