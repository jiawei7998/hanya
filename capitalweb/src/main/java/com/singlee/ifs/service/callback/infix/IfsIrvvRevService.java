package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsRevIrvv;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拆解类冲销-回调方法
 * 
 * @author zhuangzy
 *
 */
@Service(value = "IrvvRev")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsIrvvRevService extends IfsApproveServiceBase {

	@Autowired
	IfsRevIrvvMapper ifsRevIrvvMapper;// 拆借类冲销mapper
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IMmServer mmServer;

	@Autowired
	IfsCfetsfxLendMapper lendMapper;
	@Autowired
	IfsCfetsrmbIboMapper iboMapper;
	@Autowired
	IfsFxDepositoutMapper fxDepositoutMapper;
	@Autowired
	IfsRmbDepositoutMapper rmbDepositoutMapper;

	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {

		IfsRevIrvv ifsRevIrvv = new IfsRevIrvv();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevIrvv = ifsRevIrvvMapper.searchById(map);

		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevIrvvMapper.updateIrvvStatusByID(serial_no, status, date);
		if (null == ifsRevIrvv) {
			return;
		}

		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}
				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				String callType = sysList.get(0).getP_type().trim();
				// 调用java代码
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(SlDealModule.BASE.BR,
							SlDealModule.DL.SERVER_R, SlDealModule.DL.TAG_R, SlDealModule.DL.DETAIL);

					SlInthBean inthBean = new SlInthBean();
					inthBean.setBr(SlDealModule.BASE.BR);
					inthBean.setServer(SlDealModule.DL.SERVER_R);
					inthBean.setTag(SlDealModule.DL.TAG_R);
					inthBean.setDetail(SlDealModule.DL.DETAIL);
					inthBean.setSyst(SlDealModule.DL.SYST);
					inthBean.setSeq(SlDealModule.DL.SEQ);
					inthBean.setInoutind(SlDealModule.DL.INOUTIND);
					inthBean.setStatcode(SlDealModule.DL.STATCODE);
					inthBean.setPriority(SlDealModule.DL.PRIORITY);
					// 设置dealno
					inthBean.setDealno(ifsRevIrvv.getDealno());
					// 设置fedealno
					inthBean.setFedealno(ifsRevIrvv.getFedealno());
					mmBean.setInthBean(inthBean);
					SlExternalBean externalBean = new SlExternalBean();
					externalBean.setProdcode(ifsRevIrvv.getProduct());
					externalBean.setProdtype(ifsRevIrvv.getProdtype());
					mmBean.setExternalBean(externalBean);

					// 插入opics冲销表
					SlOutBean result = mmServer.mmRev(mmBean);// 用注入的方式调用opics相关方法

					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
						return;
					} else {
						/*
						 * Map<String, Object> map = new HashMap<String, Object>(); map.put("ticketId",
						 * ifsRevIfxd.getTicketId()); map.put("satacode", "-1");
						 * ifsCfetsfxSptMapper.updateStatcodeByTicketId(map);
						 */
						String ticketId = StringUtils.trimToEmpty(ifsRevIrvv.getFedealno());
						if (!"".equals(ticketId)) {
							if (ifsRevIrvv.getDealType().equalsIgnoreCase("IFS_CFETSFX_LEND")) {// 外币拆借
								lendMapper.updateApproveStatusFxLendByID(ticketId);
							} else if((ifsRevIrvv.getDealType().equalsIgnoreCase("creditLoan") // 信用拆借
									|| (ifsRevIrvv.getDealType().equalsIgnoreCase("tyLoan"))  //同业借款
									|| (ifsRevIrvv.getDealType().equalsIgnoreCase("IFS_CFETSRMB_IBO")))) {//同业拆放（对俄）
								iboMapper.updateApproveStatusRmbIboByID(ticketId);
							}else if(ifsRevIrvv.getDealType().equalsIgnoreCase("IFS_FX_DEPOSITOUT")){//存放同业（对俄）
								fxDepositoutMapper.updateApproveStatusFxDepositOutByID(ticketId);
							}else if (ifsRevIrvv.getDealType().equalsIgnoreCase("IFS_RMB_DEPOSITOUT")){//存放同业
								rmbDepositoutMapper.updateApproveStatusRmbDepositOutByID(ticketId);
							}
						}
					}

				} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程

				} else {
					JY.raise("系统参数未配置,未进行交易导入处理......");
				}

			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}

	}
}
