package com.singlee.ifs.service.impl;

import com.singlee.ifs.mapper.IfsCorePositionMapper;
import com.singlee.ifs.model.IfsCorePositionBean;
import com.singlee.ifs.service.IfsFxHxImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsFxHxImportServiceImpl implements IfsFxHxImportService{
	@Autowired
	private IfsCorePositionMapper ifsCorePositionMapper;

    @Override
    public List<IfsCorePositionBean> getAlList(Map<String, Object> map) {
        List<IfsCorePositionBean> list=ifsCorePositionMapper.getAllList(map);
        return list;
    }
	
}
