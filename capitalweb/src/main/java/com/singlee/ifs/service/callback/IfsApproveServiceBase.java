/**
 * Project Name:capitalweb
 * File Name:IfsApproveServiceBase.java
 * Package Name:com.singlee.ifs.service
 * Date:2018年5月30日上午10:58:53
 * Copyright (c) 2018, singlee@singlee.com.cn All Rights Reserved.
 *
 */

package com.singlee.ifs.service.callback;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.hrbreport.service.ReportService;
import com.singlee.ifs.mapper.*;
import com.singlee.quota.Limit;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import com.singlee.slbpm.mapper.ProcTaskEventMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: IfsApproveServiceBase <br/>
 * Function: 流程审批回调函数基础类，用于审批时的金额限制. <br/>
 * date: 2018年5月30日 上午10:58:53 <br/>
 * 
 * @author zhangcm
 * @version
 * @since JDK 1.6
 */

public class IfsApproveServiceBase implements SlbpmCallBackInteface, TaskEventInterface, ReportService {
	// 注入流程监控表
	@Autowired
	public IfsFlowMonitorMapper ifsFlowMonitorMapper;
	// 注入流程完成表
	@Autowired
	public IfsFlowCompletedMapper ifsFlowCompletedMapper;
	// 交易对手mapper
	@Autowired
	public IfsOpicsCustMapper ifsOpicsCustMapper;
	/** 流程环节事件dao */
	@Autowired
	public ProcTaskEventMapper procTaskEventMapper;
	@Autowired
	public IfsApproveElementMapper ifsApproveElementMapper;
	@Autowired
	public IfsLimitConditionMapper ifsLimitConditionMapper;
	@Autowired
	public Limit limit;
	@Autowired
	IAcupServer acupServer;

	@Override
	public Map<String, Object> getReportMap(Map<String, String> paramer, Date postdate) {
		return null;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		return null;
	}

	/**
	 * 接口调用模式
	 * @author shenzl
	 * @date 2018/12/06
	 */
	public static class InterfaceMode{
        public static final String PROCEDURE  = "PROCDURE";//过程
        public static final String JAVA = "JAVA";//默认
	}

	public static class ApproveStatus{
		public static final String APPROVESTATUSUNFINAL = DictConstants.ApproveStatus.New + "," + DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.VerifyRefuse + "," +
				DictConstants.ApproveStatus.WaitVerify + "," + DictConstants.ApproveStatus.Verified;//待审批

		public static final String APPROVESTATUSFINAL = DictConstants.ApproveStatus.ApprovedPass + "," +DictConstants.ApproveStatus.ApprovedNoPass + "," + DictConstants.ApproveStatus.Cancle+ "," +
				DictConstants.ApproveStatus.Executing + "," + DictConstants.ApproveStatus.Executed+ "," +DictConstants.ApproveStatus.Verified+ "," +
				DictConstants.ApproveStatus.Accounting + "," + DictConstants.ApproveStatus.Accounted+ "," +DictConstants.ApproveStatus.Approving;//已审批
	}
	
	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key) throws Exception {
		return null;
	}

	@Override
	public Object getBizObj(String flow_type, String flow_id, String serial_no) {

		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 审批时的回调函数.
	 * 
	 * @see com.singlee.slbpm.externalInterface.SlbpmCallBackInteface#statusChange(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {

		// TODO Auto-generated method stub

	}

	/**
	 * 审批时验证该笔交易占额.
	 * 
	 * @see可配置节点操作，当配置的节点触发后 ，调用该函数进行占额
	 * @param map查询节点事件所用信息
	 *            ，production该限额产品表名，mapObj该审批单信息，amt金额字段
	 */
	@SuppressWarnings("unused")
	public void accountAmt(Map<String, Object> map, String product, Map<String, Object> mapObj, String amt) {
		List<String> eventList = procTaskEventMapper.getProcTaskEvents(map);
		if (null != eventList) {
			for (int i = 0; i < eventList.size(); i++) {
				// quotaAutoOccupy 额度实际占用（自动）
				// quotaManualOccupy 额度实际占用（人工额度岗）
				// 目前设定就设置这两个事件时有效，因为目前只有占额
				// 进行额度实际占用操作
				if ("quotaAutoOccupy".equals(eventList.get(i)) || "quotaManualOccupy".equals(eventList.get(i))) {
					// 调用占额接口，map中含有交易单号等信息
					// 产品存放为表名，交易员userId
					Map<String, String> hashMap = new HashMap<String, String>();
					hashMap.put("product", product);
					hashMap.put("trader", SlSessionHelper.getUserId());
					List<Map<String, Object>> list = ifsLimitConditionMapper.searchResult(hashMap);

					Map<String, Object> flag = limit.limitAmt(list, mapObj, "limitCondition", amt, "availAmount", "type");
					// if(flag!=true){
					// JY.raise("成交金额超限!");
					// }
				}
			}
		}
	}

	public BigDecimal ccyChange(BigDecimal amt, String vccy,String mccy) {

		/**
		 * vccy币种转CNY
		 */
		BigDecimal toCNYRate = new BigDecimal("1");
		//获取汇率
		if (!"CNY".equals(vccy)) {
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", vccy);
			mapRate.put("br", "01");// 机构号，送01
			toCNYRate = acupServer.queryRate(mapRate);
			if (toCNYRate == null || "".equals(toCNYRate)) {
				JY.raise("货币" + vccy + "不存在汇率");
			}
		}
		//获取ccy  兑  CNY的乘除关系
		String toCNYRateType = acupServer.queryRateCCType("01", vccy, "M");
		if (!StringUtil.isEmptyString(toCNYRateType)) {
			if ("M".equals(toCNYRateType)) {
				amt = amt.multiply(toCNYRate);
			} else {
				amt = amt.divide(toCNYRate, 4, RoundingMode.HALF_UP);
			}
		} else {
			JY.raise("货币" + vccy + "不存在汇率计算方式");
		}

		/**
		 *CNY转mccy
		 */
		BigDecimal dayRate = new BigDecimal("1");
		//获取汇率
		if (!"CNY".equals(mccy)) {
			Map<String, String> mapRate = new HashMap<String, String>();
			mapRate.put("ccy", mccy);
			mapRate.put("br", "01");// 机构号，送01
			dayRate = acupServer.queryRate(mapRate);
			if (dayRate == null || "".equals(dayRate)) {
				JY.raise("货币" + mccy + "不存在汇率");
			}
		}
		//获取mccy  兑  CNY的乘除关系
		String rateCCType = acupServer.queryRateCCType("01", mccy, "M");
		if (!StringUtil.isEmptyString(rateCCType)) {
			if ("M".equals(rateCCType)) {
				amt = amt.divide(dayRate, 4, RoundingMode.HALF_UP);
			} else {
				amt = amt.multiply(dayRate);
			}
		} else {
			JY.raise("货币" + mccy + "不存在汇率计算方式");
		}
		return amt;
	}

 	public BigDecimal calcuAmt(BigDecimal amt,String weight){
		BigDecimal divide = new BigDecimal(weight).divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
		return amt.multiply(divide);
	}

    @Override
    public void LimitOccupy(String serial_no, String prd_no) throws Exception {
        // TODO Auto-generated method stub
       
    }
}
