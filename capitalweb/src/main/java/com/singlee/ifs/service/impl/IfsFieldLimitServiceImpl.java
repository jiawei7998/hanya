package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsFieldLimitMapper;
import com.singlee.ifs.model.IfsLimitColumns;
import com.singlee.ifs.model.IfsOpicsProd;
import com.singlee.ifs.service.IfsFieldLimitService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class IfsFieldLimitServiceImpl implements IfsFieldLimitService {
	@Autowired
	IfsFieldLimitMapper ifsFieldLimitMapper;

	@Override
	public void insert(Map<String, Object> map) {
		map.put("inputTime", DateUtil.getCurrentDateTimeAsString());
		ifsFieldLimitMapper.insert(map);

	}

	@Override
	public void updateById(Map<String, Object> map) {
		map.put("lastTime", DateUtil.getCurrentDateTimeAsString());
		ifsFieldLimitMapper.updateById(map);
	}

	@Override
	public void deleteById(String id) {
		ifsFieldLimitMapper.deleteById(id);
	}

	@Override
	public Page<IfsLimitColumns> searchPageFieldLimit(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsLimitColumns> result = ifsFieldLimitMapper.searchPageFieldLimit(map, rb);
		return result;
	}

	@Override
	public List<Map<String, Object>> searchColumns(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchColumns(map);
		return list;
	}

	@Override
	public List<Map<String, Object>> searchTables(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchTables(map);
		return list;
	}

	@Override
	public List<Map<String, Object>> searchDict(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchDict(map);
		return list;
	}

	@Override
	public String searchLimitFieldAndDict(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchLimitFieldAndDict(map);
		StringBuffer condition = new StringBuffer("");
		if (null != list) {

			for (int i = 0; i < list.size(); i++) {
				condition.append((String) list.get(i).get("COLUM"));
				condition.append(":");
				condition.append((String) list.get(i).get("DICT_ID1"));
				condition.append(";");

			}
		}
		return condition.toString();
	}

	@Override
	public List<Map<String, Object>> searchAllByPrd(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchLimitFieldAndDict(map);
		return list;
	}

	@Override
	public List<Map<String, Object>> searchColum(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsFieldLimitMapper.searchColum(map);
		return list;
	}

	@Override
	public List<Map<String, Object>> proColuDic(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ifsFieldLimitMapper.proColuDic(map);
	}

	@Override
	public List<Map<String, Object>> selectColum(Map<String, String> map) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 查询外汇限额的产品信息
	 */
	@Override
	public Page<IfsOpicsProd> searchPrdInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsProd> list = ifsFieldLimitMapper.searchPrdInfo(map,rowBounds);
		return list;
	}
	
	/**
	 * 查询外汇限额的产品类型信息
	 */
	@Override
	public Page<Map<String, Object>> searchTypeInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		String type = ParameterUtil.getString(map, "type", "");
		List<String> result = Arrays.asList(type.split(","));
		map.put("list", result);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchTypeInfo(map,rowBounds);
		return list;
	}
	
	/**
	 * 查询外汇限额的币种信息
	 */
	@Override
	public Page<Map<String, Object>> searchCcyInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchCcyInfo(map,rowBounds);
		return list;
	}
	
	/**
	 * 查询外汇限额的币种信息
	 */
	@Override
	public Page<Map<String, Object>> searchBondInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchBondInfo(map,rowBounds);
		return list;
	}
	
	
	/**
	 * 查询账户类型信息
	 */
	@Override
	public Page<Map<String, Object>> searchAccTypeInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchAccTypeInfo(map,rowBounds);
		return list;
	}

	/**
	 * 查询用户名信息
	 */
	@Override
	public Page<Map<String, Object>> searchTradInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchTradInfo(map,rowBounds);
		return list;
	}
	/**
	 * 查询用户名信息
	 */
	@Override
	public Page<Map<String, Object>> searchCostInfo(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<Map<String, Object>> list = ifsFieldLimitMapper.searchCostInfo(map,rowBounds);
		return list;
	}
}
