package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISwapServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsCfetsfxCswapMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxCswap;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(value = "IfsCswapService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCswapService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsfxCswapMapper ifsCfetsfxCswapMapper;
	@Autowired
	private ISwapServer swapserver;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsfxCswap ifscfetsfxcswap = new IfsCfetsfxCswap();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxCswapMapper.updateCcySwapByID(serial_no, status, date);
		ifscfetsfxcswap = ifsCfetsfxCswapMapper.searchCcySwap(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (ifscfetsfxcswap.equals(null)) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			// 后续处理
		}
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (monitor == null) {
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifscfetsfxcswap.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifscfetsfxcswap.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifscfetsfxcswap.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("货币掉期");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("ccyswap");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifscfetsfxcswap.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifscfetsfxcswap.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifscfetsfxcswap.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_CSWAP");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_CSWAP where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxCswapMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}

		try {
			// 3.2获取字典中的配置
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			String callType = sysList.get(0).getP_type().trim();
			// 调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				// 获取当前账务日期
				Date branchDate = baseServer.getOpicsSysDate(ifscfetsfxcswap.getBr());
				SlSwapCrsBean slSwapCrsBean = getEntry(ifscfetsfxcswap, branchDate);
				// 插入opics表
				SlOutBean result = swapserver.swapCrs(slSwapCrsBean);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("插入opics表失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifscfetsfxcswap.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxCswapMapper.updateStatcodeByTicketId(map);
				}
			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程
				Map map1 = BeanUtils.describe(ifscfetsfxcswap);
				SlOutBean result = swapserver.crsSwapProc(map1);
				if (!"999".equals(result.getRetCode())) {
					JY.raise("调用存储过程失败......");
				}
			} else {
				JY.raise("系统参数未配置,未进行交易导入处理......");
			}

		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}

	}

	/**
	 * 获取对象
	 * 
	 * @param ifscfetsfxcswap
	 * @param branchDate
	 * @return
	 */
	private SlSwapCrsBean getEntry(IfsCfetsfxCswap ifscfetsfxcswap, Date branchDate) {
		SlSwapCrsBean slSwapCrsBean = new SlSwapCrsBean("01", SlDealModule.SWAP.SERVER, SlDealModule.SWAP.TAG,
				SlDealModule.SWAP.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		// 设置交易对手
		contraPatryInstitutionInfo.setTradeId(ifscfetsfxcswap.getCounterpartyDealer());
		// 设置对方机构id
		contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxcswap.getCounterpartyInstId());
		slSwapCrsBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		// 设置本方交易员
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifscfetsfxcswap.getDealer());// 交易员id
		userMap.put("branchId", ifscfetsfxcswap.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		institutionInfo.setTradeId(user.getOpicsTrad());
		slSwapCrsBean.setInstitutionInfo(institutionInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifscfetsfxcswap.getCost());
		externalBean.setProdcode(ifscfetsfxcswap.getProduct());
		externalBean.setProdtype(ifscfetsfxcswap.getProdType());
		externalBean.setPort(ifscfetsfxcswap.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifscfetsfxcswap.getContractId());
		externalBean.setAuthsi(SlDealModule.SWAP.AUTHSIIND);
		externalBean.setSiind(SlDealModule.SWAP.SIIND);
		externalBean.setSupconfind(SlDealModule.SWAP.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.SWAP.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.SWAP.SUPRECIND);
		externalBean.setVerind(SlDealModule.SWAP.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifscfetsfxcswap.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysmeans(ifscfetsfxcswap.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifscfetsfxcswap.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifscfetsfxcswap.getCtrsacct());
		slSwapCrsBean.setExternalBean(externalBean);
		SlInthBean inthBean = slSwapCrsBean.getInthBean();
		inthBean.setFedealno(ifscfetsfxcswap.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slSwapCrsBean.setInthBean(inthBean);
		slSwapCrsBean.setExecId(ifscfetsfxcswap.getTicketId());
		slSwapCrsBean.setValueDate(DateUtil.parse(ifscfetsfxcswap.getStartDate()));// 开始日期
		slSwapCrsBean.setDealDate(DateUtil.parse(ifscfetsfxcswap.getForDate()));// 交易日期
		// slSwapCrsBean.setPayBasis(ifscfetsfxcswap.getLeftBasis());// 计息方式
		// slSwapCrsBean.setRecBasis(ifscfetsfxcswap.getRightBasis());
		if (ifscfetsfxcswap.getLeftBasis().endsWith("1")) {
			slSwapCrsBean.setPayBasis("A360");
		} else if (ifscfetsfxcswap.getLeftBasis().endsWith("3")) {
			slSwapCrsBean.setPayBasis("A365");
		}
		if (ifscfetsfxcswap.getRightBasis().endsWith("1")) {
			slSwapCrsBean.setRecBasis("A360");
		} else if (ifscfetsfxcswap.getRightBasis().endsWith("3")) {
			slSwapCrsBean.setRecBasis("A365");
		}
		slSwapCrsBean.setPayIntpaycycle(ifscfetsfxcswap.getLeftFrequency());// 定息周期
		slSwapCrsBean.setRecIntpaycycle(ifscfetsfxcswap.getRightFrequency());
		slSwapCrsBean.setTenor("99");
		slSwapCrsBean.setDealind("S");
		slSwapCrsBean.setNetpayind("N");
		slSwapCrsBean.setPaypostnotional("Y");
		slSwapCrsBean.setRecpostnotional("Y");
		// 左端固定/浮动利率
		slSwapCrsBean.setPayIntRate(new BigDecimal(ifscfetsfxcswap.getLeftRate()));
		// 右端固定/浮动利率
		slSwapCrsBean.setRecIntRate(new BigDecimal(ifscfetsfxcswap.getRightRate()));
		// 左端币种
		slSwapCrsBean.setPayNotCcy(ifscfetsfxcswap.getSellCurreny());
		// 右端币种
		slSwapCrsBean.setRecNotCcy(ifscfetsfxcswap.getBuyCurreny());
		// 左端金额
		slSwapCrsBean.setPayNotCcyAmt(new BigDecimal(ifscfetsfxcswap.getLeftNotion()));
		// 右端金额
		slSwapCrsBean.setRecNotCcyAmt(new BigDecimal(ifscfetsfxcswap.getRightNotion()));
		// 左端利率曲线
		slSwapCrsBean.setPayYieldcurve(ifscfetsfxcswap.getLeftDiscountCurve());
		// 右端利率曲线
		slSwapCrsBean.setRecYieldcurve(ifscfetsfxcswap.getRightDiscountCurve());
		// 左端利率代码
		slSwapCrsBean.setRateCode(ifscfetsfxcswap.getLeftRateCode());
		// 右端利率代码
		slSwapCrsBean.setRecRateCode(ifscfetsfxcswap.getRightRateCode());
		// 左端方向
		slSwapCrsBean.setPayDateDir(ifscfetsfxcswap.getLeftDirection());
		// 右端方向
		slSwapCrsBean.setRecDateDir(ifscfetsfxcswap.getRightDirection());
		// 左端付息周期
		slSwapCrsBean.setPayIntpaycycle(ifscfetsfxcswap.getLeftPayment());
		// 右端付息周期
		slSwapCrsBean.setRecIntpaycycle(ifscfetsfxcswap.getRightPayment());
		// 到期日
		slSwapCrsBean.setMaturityDate(ifscfetsfxcswap.getUnadjustmaturityDate());
		// 起息日
		slSwapCrsBean.setValueDate(DateUtil.parse(ifscfetsfxcswap.getStartDate()));
		// 交易方式
		slSwapCrsBean.setPlmethod(ifscfetsfxcswap.getAccountingMethod());
		// 设置固定/浮动方向
		slSwapCrsBean.setSwapType(ifscfetsfxcswap.getFixedFloatDir());

		return slSwapCrsBean;
	}

}
