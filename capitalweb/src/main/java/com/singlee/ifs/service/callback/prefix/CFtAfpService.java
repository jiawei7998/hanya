package com.singlee.ifs.service.callback.prefix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.common.util.DataUtils;
import com.singlee.financial.esb.hbcb.service.S003003990MS5702Service;
import com.singlee.hrbcap.service.impl.HrbCapSettleOrderCallback;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCustSettle;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsTrdSettle;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.model.CFtInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "CFtAfpService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtAfpService extends IfsApproveServiceBase {

	@Autowired
	private CFtAfpMapper cFtAfpMapper;
	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;
	@Autowired
	private CFtInfoMapper cFtInfoMapper;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	private IfsTrdSettleMapper trdsettlemapper;
	@Resource
	IFSMapper ifsMapper;
	@Autowired
	private IfsCustSettleMapper ifsCustSettleMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;
	@Resource
	private IfsOpicsCustMapper ifsOpicsCustMapper;
	@Autowired
	HrbCapSettleOrderCallback hrbCapSettleOrderCallback;
	@Resource
	TaDictVoMapper taDictVoMapper;
	
	@Autowired
	private S003003990MS5702Service  s003003990MS5702Service;
	
	@Autowired
	private IfsEsbMassageMapper ifsEsbMassageMapper;
	
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		cFtAfpMapper.updateAfpByID(serial_no, status, date);
		CFtAfp cFtAfp = cFtAfpMapper.selectByPrimaryKey(serial_no);
		
		// 0.1 当前业务为空则不进行处理
		if (null == cFtAfp) {
			return;
		}
		
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {

		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(cFtAfp.getDealNo());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(cFtAfp.getDealNo());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(cFtAfp.getApproveStatus());// 设置监控表-审批状态

			// 判断基金是否存在
			CFtInfo cFtInfo = cFtInfoMapper.selectByPrimaryKey(cFtAfp.getFundCode());
			if (cFtInfo == null) {
				JY.raise("交易的基金不存在......");
			}
			if (cFtInfo.getCcy() == null) {
				JY.raise("交易的基金的币种不存在......");
			}
			if (TradeConstants.ProductCode.CURRENCY_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("货币基金申购");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.CURY_FUND_AFP);
			} else if (TradeConstants.ProductCode.BOND_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("债券基金申购");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.BD_FUND_AFP);
			} else if (TradeConstants.ProductCode.SPEC_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("专户基金申购");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.FUND_AFP);
			}
			ifsFlowMonitor.setTradeDate(cFtAfp.getTdate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(cFtAfp.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("30");//基金申购流程
			String cname = cFtAfp.getCname();
			if (null != cname || "".equals(cname)) {
				ifsFlowMonitor.setCustName(cname);
			}
			
			// 2.3查询客制化字段
			StringBuffer buffer = new StringBuffer("");
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("FT_AFP");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CBT where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = cFtAfpMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
		}
		
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map1);
		}
		
		// 3.1未审批通过则不进行后续操作
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		
		try {

			String tdate = DataUtils.formatDate(new Date(), 1, "yyyy-MM-dd");
				
			//复核通过后生成清算信息
			//Map<String,Object> map = generateCustSettle(cFtAfp);
			//ifsCustSettleMapper.addCustSettle(map);

			//生产会计分录

			Map<String, Object> map = new HashMap<>();
			map.put("serial_no",cFtAfp.getDealNo());
			map.put("prdNo",cFtAfp.getPrdNo());
			hrbCapSettleOrderCallback.generateAcup(map);

			//生成往账经办信息
			IfsTrdSettle ifsTrdSettle= generateIfsTrdSettle(cFtAfp);
			trdsettlemapper.insertSettle(ifsTrdSettle);
			
//			//发送经办信息到二代
//			FundSettle(ifsTrdSettle);
			
			//复核完成后创建确认流程
			CFtAfpConf cFtAfpConf = generateAfpConf(cFtAfp);
			
			cFtAfpConfMapper.insertSelective(cFtAfpConf);



		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	
	//获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

	/**
	 * 生成清算信息
	 * @param cFtAfp
	 * @return
	 */
	public Map<String,Object> generateCustSettle(CFtAfp cFtAfp){
		Map<String,Object> map = new HashMap<>();
		map.put("cno", cFtAfp.getCno());
		map.put("cmneEn", cFtAfp.getCno().substring(0, 3));
		map.put("cmneCn", cFtAfp.getCname());
		map.put("product", TradeConstants.TrdType.FUND_AFP);
		map.put("dealFlag", "1");
		map.put("isdefault", "1");
		map.put("pbnkno", cFtAfp.getSelfBankcode());
		map.put("pbnknm", cFtAfp.getSelfBankname());
		map.put("pacctno", cFtAfp.getSelfAcccode());
		map.put("pacctnm", cFtAfp.getSelfAccname());
		map.put("rbnkno", cFtAfp.getPartyBankcode());
		map.put("rbnknm", cFtAfp.getPartyBankname());
		map.put("racctno", cFtAfp.getPartyAcccode());
		map.put("racctnm", cFtAfp.getPartyAccname());
		map.put("manager", SlSessionHelper.getUserId());

		String curDate= DayendDateServiceProxy.getInstance().getSettlementDate();
		map.put("manageDate", DateUtil.parse(curDate, "yyyy-MM-dd"));

		IfsCustSettle ifsCustSettle = ifsCustSettleMapper.selectMaxSeqByCno(map);
		if(ifsCustSettle==null){
			map.put("seq", "0");
		}else{
			int num=Integer.valueOf(ifsCustSettle.getSeq());
			map.put("seq", String.valueOf(num+1));
		}
		return map;
	}
	/**
	 * 生成往账经办
	 * @param cFtAfp
	 * @param map
	 * @return
	 */
	public IfsTrdSettle generateIfsTrdSettle(CFtAfp cFtAfp){
		Map<String, Object> userMap = new HashMap<String, Object>();
		IfsTrdSettle ifsTrdSettle=new IfsTrdSettle();
		userMap.put("userId",cFtAfp.getSponsor());// 交易员id
		userMap.put("branchId", cFtAfp.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		ifsTrdSettle.setSn(trdsettlemapper.getSn());
		ifsTrdSettle.setBr(user.getOpicsBr());
		ifsTrdSettle.setProduct(TradeConstants.TrdType.FUND_AFP);
		ifsTrdSettle.setProdtype("AFP");
		ifsTrdSettle.setDealno(cFtAfp.getDealNo().substring(8, cFtAfp.getDealNo().length()-1));
		ifsTrdSettle.setServer("0019901");
		ifsTrdSettle.setFedealno(cFtAfp.getDealNo());
		ifsTrdSettle.setCno(cFtAfp.getFundCode());
		ifsTrdSettle.setVdate(cFtAfp.getTdate());
		ifsTrdSettle.setPayrecind("P");
		ifsTrdSettle.setCcy(cFtAfp.getCcy());
		ifsTrdSettle.setAmount(cFtAfp.getAmt());
		ifsTrdSettle.setHandleAmt(cFtAfp.getHandleAmt());
		ifsTrdSettle.setCdate(DateUtil.format(new Date(),"yyyy-MM-dd"));
		ifsTrdSettle.setCtime(DateUtil.format(new Date(),"HH:mm:ss"));
		ifsTrdSettle.setSetmeans("CNAPS");
		ifsTrdSettle.setSetacct("CNAPS");
		ifsTrdSettle.setStatus("AR");
		ifsTrdSettle.setHvpType("A100");
	

		//ifsTrdSettle.setSeq((String) map.get("seq"));
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cno", cFtAfp.getCno());
		map.put("product", TradeConstants.TrdType.FUND_AFP);
		IfsTrdSettle settle = trdsettlemapper.selectMaxSeqByCno(map);
		if(settle==null){
			ifsTrdSettle.setSeq("0");
		}else{
			int num=Integer.valueOf(settle.getSeq());
			ifsTrdSettle.setSeq(String.valueOf(num+1));
		}
		
		//ifsTrdSettle.setStartBankid(startBankid);
		//收款方
		ifsTrdSettle.setRecUserid(cFtAfp.getPartyAcccode());
		ifsTrdSettle.setRecUsername(cFtAfp.getPartyBankname());
		ifsTrdSettle.setRecBankid(cFtAfp.getPartyBankcode());
		ifsTrdSettle.setRecBankname(cFtAfp.getPartyBankname());
		
		Map<String, Object> sysMap = new HashMap<String, Object>();
		sysMap.put("p_type", "HRBCB");
		List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
		for (TaSysParam taSysParam : sysList) {
			if("Bankid".equals(taSysParam.getP_code())) {
				ifsTrdSettle.setPayBankid(taSysParam.getP_value().trim());//付款行行号
			}else if("Userid".equals(taSysParam.getP_code())) {
				ifsTrdSettle.setPayUserid(taSysParam.getP_value().trim());//付款账号
			}else if("Bankname".equals(taSysParam.getP_code())) {
				ifsTrdSettle.setPayBankname(taSysParam.getP_value().trim());//付款行行名
			}else if("Username".equals(taSysParam.getP_code())) {
				ifsTrdSettle.setPayUsername(taSysParam.getP_value().trim());//付款账户名称
			}
		}
		
		ifsTrdSettle.setVoidflag("0");
		ifsTrdSettle.setDealflag("0");
		ifsTrdSettle.setSettflag("1");
		return ifsTrdSettle;
	}
	/**
	 * 生成申购确认
	 * @param cFtAfp
	 * @return
	 */
	public CFtAfpConf generateAfpConf(CFtAfp cFtAfp){
		CFtAfpConf cFtAfpConf = new CFtAfpConf();
		cFtAfpConf.setPrdNo(cFtAfp.getPrdNo());
		cFtAfpConf.setDealType(cFtAfp.getDealType());
		cFtAfpConf.setSponsor(cFtAfp.getSponsor());
		cFtAfpConf.setSponInst(cFtAfp.getSponInst());
		cFtAfpConf.setCno(cFtAfp.getCno());
		cFtAfpConf.setCname(cFtAfp.getCname());
		cFtAfpConf.setContact(cFtAfp.getContact());
		cFtAfpConf.setContactPhone(cFtAfp.getContactPhone());
		cFtAfpConf.setFundCode(cFtAfp.getFundCode());
		cFtAfpConf.setCcy(cFtAfp.getCcy());
		cFtAfpConf.setShareAmt(cFtAfp.getShareAmt());
		cFtAfpConf.setAmt(cFtAfp.getAmt());
		cFtAfpConf.setPrice(cFtAfp.getPrice());
		cFtAfpConf.setTdate(cFtAfp.getTdate());
		cFtAfpConf.setFtpprice(cFtAfp.getFtpprice());
		cFtAfpConf.setInvType(cFtAfp.getInvType());
		cFtAfpConf.setRemark(cFtAfp.getRemark());
		cFtAfpConf.setSelfAcccode(cFtAfp.getSelfAcccode());
		cFtAfpConf.setSelfAccname(cFtAfp.getSelfAccname());
		cFtAfpConf.setSelfBankcode(cFtAfp.getSelfBankcode());
		cFtAfpConf.setSelfBankname(cFtAfp.getSelfBankname());
		cFtAfpConf.setPartyAcccode(cFtAfp.getPartyAcccode());
		cFtAfpConf.setPartyAccname(cFtAfp.getPartyAccname());
		cFtAfpConf.setPartyBankcode(cFtAfp.getPartyBankcode());
		cFtAfpConf.setPartyBankname(cFtAfp.getPartyBankname());
		cFtAfpConf.setHandleAmt(cFtAfp.getHandleAmt());
		cFtAfpConf.setReShareAmt(new BigDecimal("0"));
		cFtAfpConf.setFtpflag(cFtAfp.getFtpflag());
		cFtAfpConf.setIsDirect(cFtAfp.getIsDirect());
		cFtAfpConf.setRefNo(cFtAfp.getDealNo());

		String ticketId = getTicketId(TradeConstants.TrdType.FUND_AFPCONF);
		cFtAfpConf.setDealNo(ticketId);
		cFtAfpConf.setApproveStatus(DictConstants.ApproveStatus.New);
		return cFtAfpConf;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");

		String id = (String)paramer.get("id");
		if("jyhjsg".equals(id)){
			paramer.put("prdNo", "801");
		}else if("jyzjsg".equals(id)){
			paramer.put("prdNo", "802");
		}else if("jyzhjsg".equals(id)){
			paramer.put("prdNo", "803");
		}
		paramer.put("dealNo", paramer.get("contractId"));
		paramer.put("tdate", paramer.get("forDate"));

		List<CFtAfp> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = cFtAfpMapper.searchCFtAfpPageMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = cFtAfpMapper.searchCFtAfpPageUnfinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = cFtAfpMapper.searchCFtAfpPageFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tbAccountTypeList = taDictVoMapper.getTadictTree("tbAccountType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (CFtAfp cFtAfp: list) {
			cFtAfp.setCcy(currencyList.get(cFtAfp.getCcy()));
			cFtAfp.setInvType(tbAccountTypeList.get(cFtAfp.getInvType()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

}
