package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.hrbreport.service.ReportService;
import com.singlee.ifs.mapper.IfsCfetsfxSptMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsfxSpt;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsSptService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsSptService extends IfsApproveServiceBase implements ReportService {
	@Autowired
	private IFxdServer fxdServer;
	@Resource
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	UserService userService;
	@Autowired
	ISlGentServer slGentServer;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	IAcupServer acupServer;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsfxSpt ifscfetsfxspt = new IfsCfetsfxSpt();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsfxSptMapper.updateSpotByID(serial_no, status, date);//
		ifscfetsfxspt = ifsCfetsfxSptMapper.getSpot(serial_no);

		// 0.1 当前业务为空则不进行处理
		if (null == ifscfetsfxspt) {
			return;
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {

		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifscfetsfxspt.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifscfetsfxspt.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifscfetsfxspt.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName("外汇即期");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("rmbspt");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor
					.setAmt(String.valueOf(ifscfetsfxspt.getBuyAmount() == null ? "" : ifscfetsfxspt.getBuyAmount()));// 设置监控表-本金
			ifsFlowMonitor.setRate(String.valueOf(ifscfetsfxspt.getPrice() == null ? "" : ifscfetsfxspt.getPrice()));// 设置监控表-利率
			ifsFlowMonitor.setTradeDate(ifscfetsfxspt.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifscfetsfxspt.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifscfetsfxspt.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSFX_SPT");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSFX_SPT where ticket_id=" + "'" + serial_no
							+ "'";
					LinkedHashMap<String, Object> swap = ifsCfetsfxSptMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate(ifscfetsfxspt.getBr());
				SlFxSptFwdBean slFxSptFwdBean = getEntry(ifscfetsfxspt, branchDate);
				// 验证
				/*
				 * SlOutBean verRet = fxdServer.verifiedfxSptFwd(slFxSptFwdBean); if
				 * (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
				 * JY.raise("验证未通过......"+verRet.getRetMsg()); }
				 */
				// 插入opics表
				SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);// 用注入的方式调用opics相关方法
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("插入opics表失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifscfetsfxspt.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsfxSptMapper.updateStatcodeByTicketId(map);
				}

			} else {// 调用存储过程
				Map map1 = BeanUtils.describe(ifscfetsfxspt);
				SlOutBean result = fxdServer.fxSptAndFwdProc(map1);
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过......");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JY.raise("审批未通过Exception......", e);
		}
	}

	/**
	 * 获取即期远期交易实体
	 *
	 * @param ifscfetsfxspt
	 * @param branchDate
	 * @return
	 */
	private SlFxSptFwdBean getEntry(IfsCfetsfxSpt ifscfetsfxspt, Date branchDate) {
		// String br=userService.getBrByUser(ifscfetsfxspt.getSponsor());
		SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean(ifscfetsfxspt.getBr(), SlDealModule.FXD.SERVER,
				SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
		/************* 20180709新增：设置交易日期、起息日 ***************************/
		slFxSptFwdBean.setDealDate(DateUtil.parse(ifscfetsfxspt.getForDate()));
		slFxSptFwdBean.setValueDate(DateUtil.parse(ifscfetsfxspt.getValueDate()));
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		/** 设置对方机构 */
		// contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxspt.getCounterpartyInstId());
		// 设置对方机构id
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifscfetsfxspt.getCounterpartyInstId());
		contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxspt.getCounterpartyInstId());
		/************* 20180709新增：设置交易对手交易员 ***************************/
		contraPatryInstitutionInfo.setTradeId(ifscfetsfxspt.getCounterpartyDealer());
		// contraPatryInstitutionInfo.setInstitutionId(ifscfetsfxspt.getCounterpartyInstId());
		slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		/************* 20180709新增：设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifscfetsfxspt.getDealer());// 交易员id
		userMap.put("branchId", ifscfetsfxspt.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
//		SlGentBean gentBean = new SlGentBean();
//		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
//		gentBean.setTablevalue(ifscfetsfxspt.getCost());
//		gentBean.setBr(ifscfetsfxspt.getBr());
//		// gentBean=slGentServer.getSlGent(gentBean);
//		String trad = (gentBean != null)
//				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
//				: user.getOpicsTrad();
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FXD.IOPER);
		institutionInfo.setTradeId(trad);
		slFxSptFwdBean.getInthBean().setIoper(trad);
		slFxSptFwdBean.setInstitutionInfo(institutionInfo);
		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		Double amt = Double.valueOf(ifscfetsfxspt.getBuyAmount().toString());// 金额1
		currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
//		 currencyInfo.setCcy(ifscfetsfxspt.getSellCurreny());
		currencyInfo.setCcy(ifscfetsfxspt.getOpicsccy());// 主币种
//		String currencyPair=ifscfetsfxspt.getCurrencyPair();
//		currencyInfo.setCcy(currencyPair.substring(0,3));//主币种
		/************* 20180709新增：设置汇率 ***************************/
		// 传入成交汇率
//		Double rate1 = Double.valueOf(ifscfetsfxspt.getPrice().toString());
		if (ifscfetsfxspt.getExchangeRate() == null) {
			JY.raise("审批未通过,成交汇率不能为空......");
		}
		Double rate1 = Double.valueOf(ifscfetsfxspt.getExchangeRate());
		currencyInfo.setRate(BigDecimal.valueOf(rate1));
		// currencyInfo.setContraCcy(ifscfetsfxspt.getBuyCurreny());
		currencyInfo.setContraCcy(ifscfetsfxspt.getOpicsctrccy());// 次币种
//		currencyInfo.setContraCcy(currencyPair.substring(4,7));//次币种
//		Double contraAmt = Double.valueOf(ifscfetsfxspt.getBuyAmount());
		// currencyInfo.setContraAmt(BigDecimal.valueOf(contraAmt));
		// 点差
		Double points = Double.valueOf(ifscfetsfxspt.getSpread());
		currencyInfo.setPoints(BigDecimal.valueOf(points));
		currencyInfo.setSpotRate(ifscfetsfxspt.getPrice());
		slFxSptFwdBean.setCurrencyInfo(currencyInfo);
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifscfetsfxspt.getCost());
		externalBean.setProdcode(ifscfetsfxspt.getProduct());
		externalBean.setProdtype(ifscfetsfxspt.getProdType());
		externalBean.setPort(ifscfetsfxspt.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifscfetsfxspt.getTicketId());
		//判断交易是否时PVP交易
		if("12".equals(ifscfetsfxspt.getDeliveryType())){
			externalBean.setDealtext(ifscfetsfxspt.getContractId()+"|PVP");
		}else{
			externalBean.setDealtext(ifscfetsfxspt.getContractId());
		}

		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
		externalBean.setSiind(SlDealModule.FXD.SIIND);
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
		externalBean.setVerind(SlDealModule.FXD.VERIND);
		slFxSptFwdBean.setExternalBean(externalBean);

		SlInthBean inthBean = slFxSptFwdBean.getInthBean();
		inthBean.setFedealno(ifscfetsfxspt.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFxSptFwdBean.setInthBean(inthBean);
		slFxSptFwdBean.setDealDate(DateUtil.parse(ifscfetsfxspt.getForDate()));
		String ps = ifscfetsfxspt.getBuyDirection();
		if (ps.endsWith("P")) {
			slFxSptFwdBean.setPs(PsEnum.P);
		}
		if (ps.endsWith("S")) {
			slFxSptFwdBean.setPs(PsEnum.S);
		}
		slFxSptFwdBean.setExecId(ifscfetsfxspt.getTicketId());
		return slFxSptFwdBean;
	}


	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsfxSpt> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsfxSptMapper.getSpotMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsfxSptMapper.getSpotMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsfxSptMapper.getSpotMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}

		//字典转换
		Map<String, String> currencyPairList = taDictVoMapper.getTadictTree("CurrencyPair").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_desc));
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradingList = taDictVoMapper.getTadictTree("trading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tradeSourceList = taDictVoMapper.getTadictTree("TradeSource").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> transCodeList = taDictVoMapper.getTadictTree("transCode").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> purposeTypeList = taDictVoMapper.getTadictTree("purposeType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsfxSpt ifsCfetsfxSpt: list) {
			ifsCfetsfxSpt.setCurrencyPair(currencyPairList.get(ifsCfetsfxSpt.getCurrencyPair()));
			ifsCfetsfxSpt.setOpicsccy(currencyList.get(ifsCfetsfxSpt.getOpicsccy()));
			ifsCfetsfxSpt.setBuyDirection(tradingList.get(ifsCfetsfxSpt.getBuyDirection()));
			ifsCfetsfxSpt.setDealSource(tradeSourceList.get(ifsCfetsfxSpt.getDealSource()));
			ifsCfetsfxSpt.setTransKind(transCodeList.get(ifsCfetsfxSpt.getTransKind()));
			ifsCfetsfxSpt.setPurposeCode(purposeTypeList.get(ifsCfetsfxSpt.getPurposeCode()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
