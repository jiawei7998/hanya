package com.singlee.ifs.service;

import java.util.Map;

/***
 * 增值税数据生成
 * 
 * @author copysun
 */
public interface IfsOpsCreateFifoService {
	void generateData(Map<String,Object> map);
}