package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsEsbMassage;

import java.util.Map;


public interface IfsEsbMassageService {
    
	public Page<IfsEsbMassage> getEsbMassagesList(Map<String, Object> map);
	

	
    
}
