package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbIboMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsrmbIbo;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;



@Service(value = "IfsRmbIboService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbIboService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbIboMapper ifsCfetsrmbIboMapper;
	@Autowired
	private IMmServer mmServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbIboMapper.updateRmbIboByID(serial_no, status, date);
		IfsCfetsrmbIbo ifsCfetsrmbIbo = ifsCfetsrmbIboMapper.searchCredLo(serial_no);

		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsrmbIbo) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			try {
				if("S".equals(ifsCfetsrmbIbo.getMyDir())){//拆出 释放
					hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}
		
		String prdNo = ifsCfetsrmbIbo.getfPrdCode();
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 2.1需要考虑第一次审批和撤回再次审批。
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbIbo.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbIbo.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbIbo.getApproveStatus());// 设置监控表-审批状态
			if (TradeConstants.ProductCode.IBO.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("信用拆借");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("ibo");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.IBO;
			}else if (TradeConstants.ProductCode.TYIBO.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("同业借款");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("tyibo");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.TYIBO;
			}else if (TradeConstants.ProductCode.FXTYIBO.equals(prdNo)) {
				ifsFlowMonitor.setPrdName("同业拆放");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("fxtyibo");// 设置监控表-产品编号：作为分类
				prdNo = TradeConstants.ProductCode.FXTYIBO;
			}
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbIbo.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbIbo.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbIbo.getRemoveInst();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_IBO");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_IBO where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbIboMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("S".equals(ifsCfetsrmbIbo.getMyDir())) {//拆出 占用
						LimitOccupy(serial_no, prdNo);
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);

		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		try {
			// 3.2获取字典中的配置
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			String callType = sysList.get(0).getP_type().trim();
			// 3.3调用java代码
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				// 获取当前账务日期
				Date branchDate = baseServer.getOpicsSysDate(ifsCfetsrmbIbo.getBr());
				// 导入opics中
				SlOutBean result = mmServer.mm(getEntry(ifsCfetsrmbIbo, branchDate));
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("审批未通过，插入opcis表失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsrmbIbo.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsrmbIboMapper.updateStatcodeByTicketId(map);
				}
			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbIbo);
				SlOutBean result = mmServer.idldProc(map);// 本币信用拆借存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，执行存储过程失败......");
				}
			} else {
				JY.raise("系统参数未配置,未进行交易导入处理......");
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}

	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbIbo ifsCfetsrmbIbo = ifsCfetsrmbIboMapper.searchCredLo(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbIbo.getCustNo());
		remap.put("custType", ifsCfetsrmbIbo.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbIbo.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbIbo.getBorrowInst());
		BigDecimal amt = calcuAmt(ifsCfetsrmbIbo.getAmt().multiply(new BigDecimal("10000")), ifsCfetsrmbIbo.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbIbo.getForDate());
		remap.put("mdate", ifsCfetsrmbIbo.getSecondSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取交易实体
	 * 
	 * @return
	 */
	private SlDepositsAndLoansBean getEntry(IfsCfetsrmbIbo ifsCfetsrmbIbo, Date branchDate) {
		SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(ifsCfetsrmbIbo.getBr(), SlDealModule.DL.SERVER,
				SlDealModule.DL.TAG, SlDealModule.DL.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsCfetsrmbIbo.getRemoveInst());
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbIbo.getRemoveInst());
		mmBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		// 本方交易员
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbIbo.getBorrowTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbIbo.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.DL.IOPER);
		institutionInfo.setTradeId(trad);
		mmBean.getInthBean().setIoper(trad);
		mmBean.setInstitutionInfo(institutionInfo);
		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsrmbIbo.getCost());
		externalBean.setProdcode(ifsCfetsrmbIbo.getProduct());
		externalBean.setProdtype(ifsCfetsrmbIbo.getProdType());
		externalBean.setPort(ifsCfetsrmbIbo.getPort());
		externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setSiind(SlDealModule.DL.SIIND);
		externalBean.setSupconfind(SlDealModule.DL.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.DL.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.DL.SUPRECIND);
		externalBean.setVerind(SlDealModule.DL.VERIND);
		externalBean.setDealtext(ifsCfetsrmbIbo.getContractId());
		externalBean.setCustrefno(ifsCfetsrmbIbo.getTicketId());
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbIbo.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbIbo.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbIbo.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbIbo.getCtrsacct());
		mmBean.setExternalBean(externalBean);
		SlInthBean inthBean = mmBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbIbo.getTicketId());
		inthBean.setLstmntdate(branchDate);
		mmBean.setInthBean(inthBean);
		double amt = ifsCfetsrmbIbo.getAmt().doubleValue() * 10000;
		mmBean.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
		mmBean.setValueDate(DateUtil.parse(ifsCfetsrmbIbo.getFirstSettlementDate()));
		mmBean.setMaturityDate(DateUtil.parse(ifsCfetsrmbIbo.getSecondSettlementDate()));
		mmBean.setDealDate(DateUtil.parse(ifsCfetsrmbIbo.getForDate()));
		mmBean.setTenor("99");
		mmBean.setIntdatedir("VF");
		mmBean.setIntdaterule("D");
		mmBean.setCommtypecode("S");
		mmBean.setCcy("CNY");
		mmBean.setBasis(ifsCfetsrmbIbo.getBasis());
		mmBean.setRateCode(ifsCfetsrmbIbo.getRateCode());
		mmBean.setIntrate(String.valueOf(ifsCfetsrmbIbo.getRate()));
		mmBean.setSpread(BigDecimal.valueOf(0));
		mmBean.setCommmeans(ifsCfetsrmbIbo.getCcysmeans());
		mmBean.setCommacct(ifsCfetsrmbIbo.getCcysacct());
		mmBean.setMatmeans(ifsCfetsrmbIbo.getCtrsmeans());
		mmBean.setMatacct(ifsCfetsrmbIbo.getCtrsacct());
		mmBean.setIntpaycycle("Q");
		mmBean.setIntcalcrule("Q");
		mmBean.setIntsmeans(ifsCfetsrmbIbo.getCtrsmeans());
		mmBean.setIntsacct(ifsCfetsrmbIbo.getCtrsacct());
		mmBean.setScheduleType(ifsCfetsrmbIbo.getScheduleType());
		if (!StringUtil.isNullOrEmpty(ifsCfetsrmbIbo.getScheduleType())&&"IR".equals(ifsCfetsrmbIbo.getScheduleType())){
			mmBean.setIntPayCycle(ifsCfetsrmbIbo.getIntPayCycle());
			mmBean.setIntPayDay(ifsCfetsrmbIbo.getIntPayDay());
			mmBean.setIntDateRule(ifsCfetsrmbIbo.getIntDateRule());
		}
		double Spread8 = ifsCfetsrmbIbo.getSpread8().doubleValue();
		mmBean.setSpread8(BigDecimal.valueOf(MathUtil.roundWithNaN(Spread8,2)));
//		mmBean.setSpread8(BigDecimal.valueOf(ifsCfetsrmbIbo.getSpread8()));
//		mmBean.setFirstIPayDate(DateUtil.parse(ifsCfetsrmbIbo.getFirstIPayDate()));
//		mmBean.setLastIntPayDate(DateUtil.parse(ifsCfetsrmbIbo.getLastIntPayDate()));
		return mmBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		String id = (String)paramer.get("id");

		String[] prodtype = {"",""};
		if("jyxycj".equals(id)){
			prodtype[0] = "CC";
			prodtype[1] = "CR";
			paramer.put("applyProd", TradeConstants.MarketIndicator.INTER_BANK_OFFERING);
		}else if("jytyjk".equals(id)){
			prodtype[0] = "CT";
		}
		paramer.put("prodtype",prodtype);

		List<IfsCfetsrmbIbo> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbIboMapper.getRmbIboMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbIboMapper.getRmbIboMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbIboMapper.getRmbIboMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> lendingList = taDictVoMapper.getTadictTree("lending").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> basisList = taDictVoMapper.getTadictTree("Basis").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbIbo ifsCfetsrmbIbo: list) {
			ifsCfetsrmbIbo.setMyDir(lendingList.get(ifsCfetsrmbIbo.getMyDir()));
			ifsCfetsrmbIbo.setBasis(basisList.get(ifsCfetsrmbIbo.getBasis()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

	@Override
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key) throws Exception {
		IfsCfetsrmbIbo ifsCfetsrmbIbo = ifsCfetsrmbIboMapper.searchCredLo(serial_no);
		Map<String, Object> paramer = new HashMap<>();
		//当前操作人
		paramer.put("trad",SlSessionHelper.getUserId());
		//产品
		paramer.put("prdCode",ifsCfetsrmbIbo.getProduct());
		//产品类型
		paramer.put("prdCode",ifsCfetsrmbIbo.getProduct());
		return null;
	}
}
