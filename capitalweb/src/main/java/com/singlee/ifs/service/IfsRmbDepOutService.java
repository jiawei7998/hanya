package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.capital.cashflow.model.parent.CashflowCapital;
import com.singlee.capital.cashflow.model.parent.CashflowInterest;
import com.singlee.ifs.model.IfsRmbDepositout;

import java.util.List;
import java.util.Map;

/**
 * 2021/12/23
 *
 * @Auther:zk
 */
public interface IfsRmbDepOutService {
    void editRmbDepositOut(IfsRmbDepositout record);
    String addRmbDepositOut(IfsRmbDepositout record);
    void deleteRmbDepositOut(String ticketid);
    Page<IfsRmbDepositout> searchRmbDepositOut(Map<String, Object> params);
    Page<IfsRmbDepositout> getDepositOutRmbMinePage(Map<String, Object> params, int isFinished);
    IfsRmbDepositout searchDepositOutRmbByTicketId(Map<String, Object> params);
    IfsRmbDepositout searchFlowDepositOutRmbByTicketId(Map<String, Object> params);
    void generateCashFlow(IfsRmbDepositout deposit);
    List<CashflowCapital> searchAmt(Map<String, Object> params);
    List<CashflowInterest> searchInt(Map<String, Object> params);
    public List<CashflowInterest> getInterestByCapital(Map<String,Object> param);
    void saveCashFlow(Map<String, Object> map);
}
