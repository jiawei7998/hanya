package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProdTypeBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsTypeMapper;
import com.singlee.ifs.model.IfsOpicsType;
import com.singlee.ifs.service.IfsOpicsTypeService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsTypeServiceImpl implements IfsOpicsTypeService {
	@Resource
	IfsOpicsTypeMapper ifsOpicsTypeMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsType entity) {
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsTypeMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsType entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsTypeMapper.updateById(entity);
	}

	@Override
	public void deleteById(Map<String, Object> map) {
		ifsOpicsTypeMapper.deleteById(map);

	}

	@Override
	public Page<IfsOpicsType> searchPageOpicsType(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsType> result = ifsOpicsTypeMapper.searchPageOpicsType(map, rb);
		return result;
	}

	@Override
	public IfsOpicsType searchById(Map<String, String> map) {
		return ifsOpicsTypeMapper.searchById(map);
	}

	@Override
	public void updateStatus(IfsOpicsType entity) {
		ifsOpicsTypeMapper.updateStatus(entity);
	}

	@Override
	public SlOutBean batchCalibration(String type, String[] prodcodes, String[] prodtypes) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			List<SlProdTypeBean> listTypeOpics = iStaticServer.synchroProdType();
			HashMap<String, String> map = new HashMap<String, String>();
			if ("1".equals(type)) {// 选中行
				for (int i = 0; i < prodcodes.length; i++) {
					for (int j = 0; j < listTypeOpics.size(); j++) {
						map.put("prodcode", prodcodes[i]);
						map.put("type", prodtypes[i]);
						if (listTypeOpics.get(j).getProdCode().equals(prodcodes[i]) && listTypeOpics.get(j).getProdType().equals(prodtypes[i])) {
							IfsOpicsType ifsOpicsType = ifsOpicsTypeMapper.searchById(map);

							ifsOpicsType.setProdcode(listTypeOpics.get(j).getProdCode());
							ifsOpicsType.setType(listTypeOpics.get(j).getProdType());
							ifsOpicsType.setDescr(listTypeOpics.get(j).getDescr());
							ifsOpicsType.setLstmntdte(listTypeOpics.get(j).getLstmntdte());
							ifsOpicsType.setAl(listTypeOpics.get(j).getAl());

							ifsOpicsType.setOperator(SlSessionHelper.getUserId());
							ifsOpicsType.setStatus("1");
							ifsOpicsTypeMapper.updateById(ifsOpicsType);
							break;
						}
						if (j == listTypeOpics.size() - 1) {
							HashMap<String, String> statusmap = new HashMap<String, String>();
							statusmap.put("prodcode", prodcodes[i]);
							statusmap.put("prodtype", prodtypes[i]);
							ifsOpicsTypeMapper.updateStatus0ById(statusmap);
						}
					}
				}
			} else {// 所有行
				List<IfsOpicsType> listOpicsLocal = ifsOpicsTypeMapper.searchAllOpicsType();
				for (int i = 0; i < listTypeOpics.size(); i++) {
					IfsOpicsType ifsOpicsType = new IfsOpicsType();
					//将本地数据存到实体类中，2019-4-9处理批量更新中文名称会被覆盖问题
					map.put("prodcode", listTypeOpics.get(i).getProdCode());
					map.put("type", listTypeOpics.get(i).getProdType());
					IfsOpicsType ifsOpicsTypeCopy = ifsOpicsTypeMapper.searchById(map);
					if(ifsOpicsTypeCopy != null) {
						ifsOpicsType = ifsOpicsTypeCopy;
					}
					ifsOpicsType.setProdcode(listTypeOpics.get(i).getProdCode());
					ifsOpicsType.setType(listTypeOpics.get(i).getProdType());
					ifsOpicsType.setDescr(listTypeOpics.get(i).getDescr());
					ifsOpicsType.setLstmntdte(listTypeOpics.get(i).getLstmntdte());
					ifsOpicsType.setAl(listTypeOpics.get(i).getAl());
					ifsOpicsType.setOperator(SlSessionHelper.getUserId());
					ifsOpicsType.setStatus("1");
					if (listOpicsLocal.size() == 0) {// 本地无数据
						ifsOpicsTypeMapper.insert(ifsOpicsType);
					} else {
						// 本地有数据
						for (int j = 0; j < listOpicsLocal.size(); j++) {
							if (listTypeOpics.get(i).getProdCode().equals(listOpicsLocal.get(j).getProdcode()) && listTypeOpics.get(i).getProdType().equals(listOpicsLocal.get(j).getType())) {
								ifsOpicsTypeMapper.updateById(ifsOpicsType);
								break;
							}
							if (j == listOpicsLocal.size() - 1) {
								ifsOpicsTypeMapper.insert(ifsOpicsType);
							}
						}
					}
				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				List<IfsOpicsType> listOpicsLocal2 = ifsOpicsTypeMapper.searchAllOpicsType();
				if (listOpicsLocal2.size() > listTypeOpics.size()) {
					for (int i = 0; i < listOpicsLocal2.size(); i++) {
						for (int j = 0; j < listTypeOpics.size(); j++) {
							if (listTypeOpics.get(j).getProdCode().equals(listOpicsLocal2.get(i).getProdcode()) && listTypeOpics.get(j).getProdType().equals(listOpicsLocal2.get(i).getType())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为未同步
							if (j == listTypeOpics.size() - 1) {
								IfsOpicsType ifsOpicsType = new IfsOpicsType();
								ifsOpicsType.setProdcode(listOpicsLocal2.get(i).getProdcode());
								ifsOpicsType.setType(listOpicsLocal2.get(i).getType());
								ifsOpicsType.setDescr(listOpicsLocal2.get(i).getDescr());
								ifsOpicsType.setAl(listOpicsLocal2.get(i).getAl());
								ifsOpicsType.setLstmntdte(new Date());
								ifsOpicsType.setStatus("0");// 设置状态为未同步
								ifsOpicsType.setOperator(SlSessionHelper.getUserId());
								ifsOpicsTypeMapper.updateById(ifsOpicsType);
							}
						}
					}
				}
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}


	@Override
	public String getAlByType(String type, String prodcode) {
		return ifsOpicsTypeMapper.getAlByType(type, prodcode);
	}

}
