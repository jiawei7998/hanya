package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlRepoOrBean;
import com.singlee.financial.opics.IRepoServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbCrMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbOrMapper;
import com.singlee.ifs.mapper.IfsRevBredMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsRevBred;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 回购类冲销-回调方法
 * 
 * @author lij
 * 
 */

@Service(value = "IfsBredRevService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsBredRevService extends IfsApproveServiceBase {

	@Autowired
	IfsRevBredMapper ifsRevBredMapper;// 回购类冲销mapper
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IRepoServer repoServer;
	@Autowired
	IfsCfetsrmbCrMapper crMapper;
	@Autowired
	IfsCfetsrmbOrMapper orMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsRevBred ifsRevBred = new IfsRevBred();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevBred = ifsRevBredMapper.searchById(map);
		// 1.0 null 则不处理
		if (null == ifsRevBred) {
			return;
		}
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevBredMapper.updateIfxdStatusByID(serial_no, status, date);
		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}
				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				// 1.1调用java代码
				if ("JAVA".equals(sysList.get(0).getP_type().trim())) {
					SlRepoOrBean repoOrBean = getEntry(ifsRevBred);
					// 插入opics冲销表
					SlOutBean result = repoServer.repoOrRev(repoOrBean);// 用注入的方式调用opics相关方法
					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
					} else {
						// 冲销成功后修改交易的状态为已冲销
						String ticketId = StringUtils.trimToEmpty(ifsRevBred.getFedealno());
						if (!"".equals(ticketId)) {
							if ("442".equals(ifsRevBred.getDealType())) {
							    // 买断式回购
                                orMapper.updateApproveStatusRmbOrByID(ticketId);
							} else {
							    // 质押式回购
                                crMapper.updateApproveStatusRmbCrByID(ticketId);
							}
						}
					}
				} else if ("PROC".equals(sysList.get(0).getP_type().trim())) {// 调用存储过程

				} else if ("SMT_JAVA".equals(sysList.get(0).getP_type().trim())) {

				} else if ("SMT_PROC".equals(sysList.get(0).getP_type().trim())) {
				}

			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}
	}

	/**
	 * 组装bean
	 * 
	 * @param ifsRevBred
	 * @return
	 */
	private SlRepoOrBean getEntry(IfsRevBred ifsRevBred) {
		SlRepoOrBean repoOrBean = new SlRepoOrBean(SlDealModule.BASE.BR, SlDealModule.REPO.SERVER_R,
				SlDealModule.REPO.TAG_R, SlDealModule.REPO.DETAIL);
		SlInthBean inthBean = new SlInthBean();
		inthBean.setBr(SlDealModule.BASE.BR);
		inthBean.setServer(SlDealModule.REPO.SERVER_R);
		inthBean.setTag(SlDealModule.REPO.TAG_R);
		inthBean.setDetail(SlDealModule.REPO.DETAIL);
		inthBean.setSyst(SlDealModule.REPO.SYST);
		inthBean.setSeq(SlDealModule.REPO.SEQ);
		inthBean.setInoutind(SlDealModule.REPO.INOUTIND);
		inthBean.setStatcode(SlDealModule.REPO.STATCODE);
		inthBean.setPriority(SlDealModule.REPO.PRIORITY);
		// 设置dealno
		inthBean.setDealno(ifsRevBred.getDealno());
		// 设置fedealno
		inthBean.setFedealno(ifsRevBred.getFedealno());
		repoOrBean.setInthBean(inthBean);
		return repoOrBean;
	}
}
