package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBondPledgeBean;

import jxl.Sheet;

import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
public interface IfsBondPledgeService {

    Page<IfsBondPledgeBean> selectIfsCfetsrmbCrList(Map<String, Object> paramap);

    IfsBondPledgeBean selectBondPledgeByPrimaryKey(Map<String, Object> paramap);

    void insert(IfsBondPledgeBean record);

    void updateByPrimaryKey(IfsBondPledgeBean record);

    void deleteByPrimaryKey(IfsBondPledgeBean record);
    
    Map<String, Object> saveReadBondExcelImportToOpicsService(Sheet sheet) throws Exception;
}
