package com.singlee.ifs.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.mapper.SlDerivativesStatusMapper;
import com.singlee.ifs.model.SlDerivativesStatus;
import com.singlee.ifs.service.SlDerivativesStatusService;

@Service
public class SlDerivativesStatusServiceImpl implements SlDerivativesStatusService{

	@Resource
	SlDerivativesStatusMapper slDerivativesStatusMapper;
	
	
	@Override
	public PageInfo<SlDerivativesStatus> querySlDerivativesStatus(Map<String, String> params) {
		
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<SlDerivativesStatus> result = slDerivativesStatusMapper.querySlDerivativesStatus(params, rowBounds);
		return new PageInfo<SlDerivativesStatus>(result);
	}


	@Override
	public PageInfo<RevaluationTmaxBean> queryAllRevaluationProc(Map<String, String> params) {
		// TODO Auto-generated method stub
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<RevaluationTmaxBean> result = slDerivativesStatusMapper.queryAllRevaluationProc(params, rowBounds);
		return new PageInfo<RevaluationTmaxBean>(result);
	}

}
