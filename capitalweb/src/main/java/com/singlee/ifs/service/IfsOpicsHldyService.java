package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsHldy;

import java.util.List;
import java.util.Map;

/***
 * 节假日
 * @author lij
 *
 */
public interface IfsOpicsHldyService {
	//分页查询
	Page<IfsOpicsHldy> searchPageOpicsHldy(Map<String,Object> map);
	
	//批量校准
	SlOutBean batchCalibration(String type,List<IfsOpicsHldy> list);
	
	int queryDateById(IfsOpicsHldy entity);
	
	//根据id查询实体
	IfsOpicsHldy queryEntityById(Map<String, Object> map);
	
	IfsOpicsHldy searchEntityById(IfsOpicsHldy entity);
	
	String insert(IfsOpicsHldy entity);
	
	void delete(IfsOpicsHldy entity);
	
	public List<IfsOpicsHldy> getCalendarByMonth(Map<String, Object> param)  throws Exception;
	

}
