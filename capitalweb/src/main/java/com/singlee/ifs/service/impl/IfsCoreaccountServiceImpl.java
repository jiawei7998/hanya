package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsCoreaccountMapper;
import com.singlee.ifs.model.IfsCoreaccount;
import com.singlee.ifs.service.IfsCoreaccountService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCoreaccountServiceImpl.java
 * @Description 日终对账
 * @createTime 2021年10月18日 11:08:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCoreaccountServiceImpl implements IfsCoreaccountService {
    @Autowired
    IfsCoreaccountMapper ifsCoreaccountMapper;
    @Override
    public Page<IfsCoreaccount> getList(Map<String, Object> map) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        return ifsCoreaccountMapper.searchPage(map,rowBounds);
    }

    @Override
    public void insertList(List<IfsCoreaccount> list) {
        ifsCoreaccountMapper.insertList(list);
    }

    @Override
    public void delByPostDate(String postdate) {
        ifsCoreaccountMapper.delByPostDate(postdate);
    }
}
