package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbBondfwd;

import java.util.List;
import java.util.Map;


public interface IfsCfetsrmbBondfwdService {

	/**
	 * 分页查询
	 * @param map
	 * @param isFinished
	 * @return
	 */
	Page<IfsCfetsrmbBondfwd> searchICBFPage(Map<String, Object> map,Integer isFinished);
	
	List<IfsCfetsrmbBondfwd> searchICBFList(Map<String, Object> map);
	
	IfsCfetsrmbBondfwd searchICBF(Map<String, Object> map);
	
	void saveICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd);
	
	int updateICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd);
	
	int deleteICBF(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd);

	IfsCfetsrmbBondfwd getBondById(IfsCfetsrmbBondfwd key);

	IfsCfetsrmbBondfwd getIfsCfetsrmbBondfwdByTicketId(Map<String, Object> map);
}