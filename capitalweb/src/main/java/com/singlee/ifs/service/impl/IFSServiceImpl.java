package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IFSService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用于外部接口数据操作，下行、上行接口操作等
 * 
 * @author Singlee
 * 
 */

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IFSServiceImpl implements IFSService {

	@Autowired
	WindSeclMapper windSeclMapper;

	@Resource
	IFSMapper ifsMapper;
	@Resource
	IfsCfetgoldLendMapper ifsCfetgoldLendMapper;
	@Resource
	IfsCfetsfxCswapMapper ifsCfetsfxCswapMapper;
	@Resource
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
	@Resource
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Resource
	IfsCfetsfxOnspotMapper ifsCfetsfxOnspotMapper;
	@Resource
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;
	@Resource
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
	@Resource
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
	@Resource
	IfsCfetsAllocateMapper ifsCfetsAllocateMapper;
	@Resource
	IfsCfetsfxDebtMapper ifsCfetsfxDebtMapper;
	@Autowired
	EdCustManangeService edCustManangeService;
	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;
	@Autowired
	IfsCfetsrmbIboMapper ifsCfetsrmbIboMapper;
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	@Resource
	IfsOpicsCustMapper ifsOpicsCustMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	//同业存放(外币)
	@Autowired
	IfsFxDepositInMapper ifsFxDepositInMapper;
	//存放同业
	@Autowired
	IfsFxDepositoutMapper ifsFxDepositoutMapper;
	//同业存放
	@Autowired
	IfsRmbDepositinMapper ifsRmbDepositinMapper;
	//存放同业
	@Autowired
	IfsRmbDepositoutMapper ifsRmbDepositoutMapper;
	@Autowired
	private IfsCfetsmetalGoldMapper cfetsmetalGoldMapper;
	//现券买卖
	@Autowired
	private IfsCfetsrmbCbtMapper cfetsrmbCbtMapper;
	//质押式回购
	@Autowired
	private IfsCfetsrmbCrMapper cfetsrmbCrMapper;
	//信用拆借
	@Autowired
	private IfsCfetsrmbIboMapper cfetsrmbIboMapper;
	@Autowired
	private IfsCfetsrmbIrsMapper cfetsrmbIrsMapper;
	@Autowired
	private IfsCfetsrmbOrMapper cfetsrmbOrMapper;
	//债券借贷
	@Autowired
	private IfsCfetsrmbSlMapper cfetsrmbSlMapper;
	@Autowired
	private IfsCfetsrmbDetailCrMapper detailCrMapper;
	@Autowired
	private IfsCfetsrmbDetailSlMapper detailSlMapper;
	@Autowired
    private IfsRevIrvvMapper ifsRevIrvvMapper;
	@Autowired
    private IfsAdvIdlvMapper ifsAdvIdlvMapper;
	//存单发行
	@Autowired
	private IfsCfetsrmbDpMapper cfetsrmbDpMapper;
	@Autowired
	private IfsCfetsrmbDpOfferMapper offerDpMapper;
	//事前交易
	@Autowired
	private IfsCfetsBeforeDealMapper cfetsBeforeDealMapper;
	//流程特殊用户
	@Autowired
	private IfsCfetsFlowUserMapper cfetsFlowUserMapper;
	//交易台账
	@Autowired
	private IfsDealDeskReportMapper ifsDealDeskReportMapper;

	/**
	 * 计算质押比例
	 * @param map
	 * @return
	 */
	@Override
	public BigDecimal getMarginRatio(Map<String, Object> map) {
		BigDecimal underlyingQty=new BigDecimal(map.get("underlyingQty").toString());
		BigDecimal marginAmt=new BigDecimal(map.get("marginAmt").toString());
		String biaodi=map.get("biaodi").toString();
		String zhiya=map.get("zhiya").toString();
		BigDecimal baodiEcaluatePrice=windSeclMapper.getEcaluatePrice(biaodi);
		BigDecimal zhiyaEcaluatePrice=windSeclMapper.getEcaluatePrice(zhiya);
		if(baodiEcaluatePrice==null){
			baodiEcaluatePrice=BigDecimal.ZERO;
		}
		if(zhiyaEcaluatePrice==null){
			zhiyaEcaluatePrice=BigDecimal.ZERO;
		}
		BigDecimal marginRatio=BigDecimal.ZERO;
		if(!(zhiyaEcaluatePrice.equals(BigDecimal.ZERO)||baodiEcaluatePrice.equals(BigDecimal.ZERO))){
			marginRatio=baodiEcaluatePrice.multiply(underlyingQty).divide(zhiyaEcaluatePrice.multiply(marginAmt),6, RoundingMode.HALF_UP);
		}
		marginRatio=marginRatio.multiply(new BigDecimal(100));
		return marginRatio;
	}

	@Override
	public IfsDownPaket getDownPaket(Map<String, Object> map) {
		return ifsMapper.getDownPaket(map);
	}

	@Override
	public List<IfsDownPaket> getDownPakets(Map<String, Object> map) {
		return ifsMapper.getDownPakets(map);
	}

	/*
	 * 货币掉期
	 */
	// 分页查询
	@Override
    public Page<IfsCfetsfxCswap> searchCcySwapPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxCswap> result = ifsCfetsfxCswapMapper.searchCcySwapPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcySwap(String ticketid) {
		ifsCfetsfxCswapMapper.deleteCcySwap(ticketid);

	}

	@Override
	public String addCcySwap(IfsCfetsfxCswap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("CSWAP"));
		String str = "";
		List<IfsCfetsfxCswap> list = ifsCfetsfxCswapMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxCswapMapper.addCcySwap(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void editCcySwap(IfsCfetsfxCswap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxCswapMapper.editCcySwap(map);
	}

	@Override
	public IfsCfetsfxCswap searchCcySwap(String ticketid) {

		IfsCfetsfxCswap ifsCfetsfxCswap = ifsCfetsfxCswapMapper.searchCcySwap(ticketid);
		return ifsCfetsfxCswap;
	}

	@Override
	public IfsCfetsfxCswap getCcySwapAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxCswap ccySwap = ifsCfetsfxCswapMapper.selectFlowIdByPrimaryKey(key);
		return ccySwap;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxCswap> getCcySwapMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxCswap> page = new Page<IfsCfetsfxCswap>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxCswapMapper.getCcySwapMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxCswapMapper.getCcySwapMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxCswapMapper.getCcySwapMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 外币拆借
	 */

	@Override
	public Page<IfsCfetsfxLend> searchCcyLendingPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxLend> result = ifsCfetsfxLendMapper.searchCcyLendingPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcyLending(String ticketid) {
		ifsCfetsfxLendMapper.deleteCcyLending(ticketid);

	}

	@Override
	public void editCcyLending(IfsCfetsfxLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxLendMapper.editCcyLending(map);

	}

	@Override
	public String addCcyLending(IfsCfetsfxLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("LEND"));
		String str = "";
		List<IfsCfetsfxLend> list = ifsCfetsfxLendMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxLendMapper.addCcyLending(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public IfsCfetsfxLend searchCcyLending(String ticketid) {

		IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(ticketid);
		return ifsCfetsfxLend;
	}

	/***
	 * 根据请求参数查询分页列表-外币拆借-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxLend> getCcyLendingMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxLend> page = new Page<IfsCfetsfxLend>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxLendMapper.getCcyLendingMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxLendMapper.getCcyLendingMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxLendMapper.getCcyLendingMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 外汇对即期
	 */

	@Override
	public Page<IfsCfetsfxOnspot> searchCcyOnSpotPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxOnspot> result = ifsCfetsfxOnspotMapper.searchCcyOnSpotPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteCcyOnSpot(String ticketid) {

		ifsCfetsfxOnspotMapper.deleteCcyOnSpot(ticketid);
	}

	@Override
	public String addCcyOnSpot(IfsCfetsfxOnspot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("SPOT"));
		String str = "";
		List<IfsCfetsfxOnspot> list = ifsCfetsfxOnspotMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxOnspotMapper.addCcyOnSpot(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void editCcyOnSpot(IfsCfetsfxOnspot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxOnspotMapper.editCcyOnSpot(map);

	}

	@Override
	public IfsCfetsfxOnspot searchCcyOnSpot(String ticketid) {

		IfsCfetsfxOnspot ifsCfetsfxOnspot = ifsCfetsfxOnspotMapper.searchCcyOnSpot(ticketid);
		return ifsCfetsfxOnspot;
	}

	/***
	 * 根据请求参数查询分页列表-外汇对即期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxOnspot> getCcyOnSpotMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxOnspot> page = new Page<IfsCfetsfxOnspot>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxOnspotMapper.getCcyOnspotMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxOnspotMapper.getCcyOnspotMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxOnspotMapper.getCcyOnspotMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 
	 * 人民币远期
	 */

	@Override
	public Page<IfsCfetsfxFwd> getCreditEdit(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxFwd> result = ifsCfetsfxFwdMapper.getCreditEdit(map, rowBounds);
		return result;
	}

	@Override
	public IfsCfetsfxFwd getCredit(String ticketid) {
		IfsCfetsfxFwd result = ifsCfetsfxFwdMapper.getCredit(ticketid);
		return result;
	}

	@Override
	public String addCreditCondition(IfsCfetsfxFwd map) {
		map.setTicketId(getTicketId("FWD"));
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsfxFwd> list = ifsCfetsfxFwdMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxFwdMapper.addCredit(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}
		}else{
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void editCredit(IfsCfetsfxFwd map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxFwdMapper.editCredit(map);

	}

	@Override
	public void deleteCredit(String ticketId) {
		ifsCfetsfxFwdMapper.deleteCredit(ticketId);

	}

	@Override
	public IfsCfetsfxFwd getFwdAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxFwd fwd = ifsCfetsfxFwdMapper.selectFlowIdByPrimaryKey(key);
		return fwd;
	}

	/***
	 * 根据请求参数查询分页列表-人民币远期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxFwd> getFxFwdMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxFwd> page = new Page<IfsCfetsfxFwd>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxFwdMapper.getFxFwdMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxFwdMapper.getFxFwdMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxFwdMapper.getFxFwdMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 人民币掉期
	 * 
	 * @see com.singlee.ifs.service.IFSService#getswapPage(java.util.Map)
	 */

	@Override
	public Page<IfsCfetsfxSwap> getswapPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxSwap> result = ifsCfetsfxSwapMapper.getswapPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsCfetsfxSwap getrmswap(String ticketId) {
		IfsCfetsfxSwap result = ifsCfetsfxSwapMapper.getrmswap(ticketId);
		return result;
	}

	@Override
	public String addrmswap(IfsCfetsfxSwap map) {
		map.setTicketId(getTicketId("SWAP"));
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsfxSwap> list = ifsCfetsfxSwapMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxSwapMapper.addrmswap(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}
		}else{
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void editrmswap(IfsCfetsfxSwap map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxSwapMapper.editrmswap(map);

	}

	@Override
	public void deletermswap(String ticketId) {
		ifsCfetsfxSwapMapper.deletermswap(ticketId);

	}

	@Override
	public IfsCfetsfxSwap getRmbSwapAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxSwap swap = ifsCfetsfxSwapMapper.selectFlowIdByPrimaryKey(key);
		return swap;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxSwap> getFxSwapMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxSwap> page = new Page<IfsCfetsfxSwap>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxSwapMapper.getFxSwapMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxSwapMapper.getFxSwapMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxSwapMapper.getFxSwapMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 人民币即期
	 * 
	 * @see com.singlee.ifs.service.IFSService#getSpotPage(java.util.Map)
	 */

	@Override
	public Page<IfsCfetsfxSpt> getSpotPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxSpt> result = ifsCfetsfxSptMapper.getSpotPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsCfetsfxSpt getSpot(String ticketId) {
		IfsCfetsfxSpt result = ifsCfetsfxSptMapper.getSpot(ticketId);
		return result;
	}

	@Override
	public String addSpot(IfsCfetsfxSpt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("SPT"));
		String str = "";
		List<IfsCfetsfxSpt> list = ifsCfetsfxSptMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxSptMapper.addSpot(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}
		}else{
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void editSpot(IfsCfetsfxSpt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxSptMapper.editSpot(map);

	}

	@Override
	public void deleteSpot(String ticketId) {
		ifsCfetsfxSptMapper.deleteSpot(ticketId);

	}

	@Override
	public IfsCfetsfxSpt getSpotAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxSpt spot = ifsCfetsfxSptMapper.selectFlowIdByPrimaryKey(key);
		return spot;
	}

	/***
	 * 根据请求参数查询分页列表-人民币即期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxSpt> getSpotMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxSpt> page = new Page<IfsCfetsfxSpt>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxSptMapper.getSpotMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxSptMapper.getSpotMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxSptMapper.getSpotMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 人民币期权
	 * 
	 * @see com.singlee.ifs.service.IFSService#searchOptionPage(java.util.Map)
	 */

	@Override
	public Page<IfsCfetsfxOption> searchOptionPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxOption> result = ifsCfetsfxOptionMapper.searchOptionPage(map, rowBounds);
		return result;
	}

	@Override
	public IfsCfetsfxOption searchOption(String ticketId) {
		IfsCfetsfxOption result = ifsCfetsfxOptionMapper.searchOption(ticketId);
		return result;
	}

	@Override
	public String addOption(IfsCfetsfxOption map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("OPT"));
		String str = "";
		List<IfsCfetsfxOption> list = ifsCfetsfxOptionMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxOptionMapper.addOption(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void editOption(IfsCfetsfxOption map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxOptionMapper.editOption(map);

	}

	@Override
	public void deleteOption(String ticketId) {
		ifsCfetsfxOptionMapper.deleteOption(ticketId);
	}

	/***
	 * 根据请求参数查询分页列表-人民币期权-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxOption> getOptionMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxOption> page = new Page<IfsCfetsfxOption>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxOptionMapper.getOptionMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxOptionMapper.getOptionMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxOptionMapper.getOptionMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 黄金拆借
	 */
	@Override
	public Page<IfsCfetgoldLend> searchGoldLendPage(Map<String, Object> map) {

		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetgoldLend> result = ifsCfetgoldLendMapper.searchGoldLendPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteGoldLend(String ticketid) {
		ifsCfetgoldLendMapper.deleteGoldLend(ticketid);

	}

	@Override
	public String addGoldLend(IfsCfetgoldLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("GLEND"));
		String str = "";
		List<IfsCfetgoldLend> list = ifsCfetgoldLendMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetgoldLendMapper.addGoldLend(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void editGoldLend(IfsCfetgoldLend map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetgoldLendMapper.editGoldLend(map);

	}

	@Override
	public IfsCfetgoldLend searchGoldLend(String ticketid) {

		IfsCfetgoldLend result = ifsCfetgoldLendMapper.searchGoldLend(ticketid);
		return result;
	}

	/***
	 * 根据请求参数查询分页列表- 黄金拆借-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetgoldLend> getGoldLendMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetgoldLend> page = new Page<IfsCfetgoldLend>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetgoldLendMapper.getGoldLendMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetgoldLendMapper.getGoldLendMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetgoldLendMapper.getGoldLendMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 外币头寸调拨
	 * 
	 * @see
	 * com.singlee.ifs.service.IFSService#searchIfsAllocatePage(java.util.Map)
	 */
	@Override
	public Page<IfsCfetsfxAllot> searchIfsAllocatePage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxAllot> result = ifsCfetsAllocateMapper.searchIfsAllocatePage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteIfsAllocate(String ticketid) {
		ifsCfetsAllocateMapper.deleteIfsAllocate(ticketid);

	}

	@Override
	public String addIfsAllocate(IfsCfetsfxAllot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("ALLOT"));
		String str = "";
		List<IfsCfetsfxAllot> list = ifsCfetsAllocateMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsAllocateMapper.addIfsAllocate(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void editIfsAllocate(IfsCfetsfxAllot map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsAllocateMapper.editIfsAllocate(map);

	}

	@Override
	public IfsCfetsfxAllot searchIfsAllocate(String ticketid) {
		IfsCfetsfxAllot result = ifsCfetsAllocateMapper.searchIfsAllocate(ticketid);
		return result;
	}

	/***
	 * 根据请求参数查询分页列表- 外币头寸调拨-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxAllot> getAllocateMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxAllot> page = new Page<IfsCfetsfxAllot>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsAllocateMapper.getAllocateMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsAllocateMapper.getAllocateMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsAllocateMapper.getAllocateMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 外币债
	 */
	@Override
	public Page<IfsCfetsfxDebt> searchIfsDebtPage(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxDebt> result = ifsCfetsfxDebtMapper.searchIfsDebtPage(map, rowBounds);
		return result;
	}

	@Override
	public void deleteIfsDebt(String ticketid) {
		ifsCfetsfxDebtMapper.deleteIfsDebt(ticketid);

	}

	@Override
	public String addIfsDebt(IfsCfetsfxDebt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		map.setTicketId(getTicketId("DEBT"));
		String str = "";
		List<IfsCfetsfxDebt> list = ifsCfetsfxDebtMapper.searchById(map.getContractId());
		if (list.isEmpty()) {
			try {
				ifsCfetsfxDebtMapper.addIfsDebt(map);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void editIfsDebt(IfsCfetsfxDebt map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsCfetsfxDebtMapper.editIfsDebt(map);

	}

	@Override
	public IfsCfetsfxDebt searchIfsDebt(String ticketid) {
		IfsCfetsfxDebt result = ifsCfetsfxDebtMapper.searchIfsDebt(ticketid);
		return result;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsfxDebt> getDebtMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxDebt> page = new Page<IfsCfetsfxDebt>();
		if (isFinished == 1) {// 待审批
			page = ifsCfetsfxDebtMapper.getDebtMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsCfetsfxDebtMapper.getDebtMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsCfetsfxDebtMapper.getDebtMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 黄金即远掉
	 */
	@Override
	public String insertGold(IfsCfetsmetalGold record) {
		if ("fwd".equals(record.getType())) {
			record.setTicketId(getTicketId("GFWD"));
		} else if ("spt".equals(record.getType())) {
			record.setTicketId(getTicketId("GSPT"));
		} else {
			record.setTicketId(getTicketId("GSWAP"));
		}
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsmetalGold> list = cfetsmetalGoldMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsmetalGoldMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void updateGold(IfsCfetsmetalGold record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsmetalGoldMapper.updateByPrimaryKey(record);
	}

	@Override
	public void deleteGold(IfsCfetsmetalGoldKey key) {
		cfetsmetalGoldMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsCfetsmetalGold> getGoldList(IfsCfetsmetalGold record) {
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldMapper.selectIfsCfetsmetalGoldList(record);
		return page;
	}

	@Override
	public IfsCfetsmetalGold getGoldById(IfsCfetsmetalGoldKey key) {
		IfsCfetsmetalGold gold = cfetsmetalGoldMapper.selectByPrimaryKey(key);
		return gold;
	}

	@Override
	public IfsCfetsmetalGold getGoldAndFlowById(Map<String, Object> key) {
		IfsCfetsmetalGold gold = cfetsmetalGoldMapper.selectFlowByPrimaryKey(key);
		return gold;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsmetalGold> getMetalGoldMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsmetalGold> page = new Page<IfsCfetsmetalGold>();
		if (isFinished == 1) {// 待审批
			page = cfetsmetalGoldMapper.getMetalGoldMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsmetalGoldMapper.getMetalGoldMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsmetalGoldMapper.getMetalGoldMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 现券买卖CBT
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertCbt(com.singlee.ifs.model.
	 * IfsCfetsrmbCbt)
	 */

	@Override
	public String insertCbt(IfsCfetsrmbCbt record) {
		record.setTicketId(getTicketId("CBT"));
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsrmbCbt> list = cfetsrmbCbtMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbCbtMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void updateCbt(IfsCfetsrmbCbt record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbCbtMapper.updateByPrimaryKey(record);
	}

	@Override
	public void deleteCbt(IfsCfetsrmbCbtKey key) {
		cfetsrmbCbtMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsCfetsrmbCbt> getCbtList(IfsCfetsrmbCbt record) {
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtMapper.selectIfsCfetsrmbCbtList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbCbt getCbtById(IfsCfetsrmbCbtKey key) {
		IfsCfetsrmbCbt cbt = cfetsrmbCbtMapper.selectByPrimaryKey(key);
		return cbt;
	}

	// 20180509 新增
	@Override
	public IfsCfetsrmbCbt getCbtAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbCbt cbt = cfetsrmbCbtMapper.selectFlowIdByPrimaryKey(key);
		return cbt;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbCbt> getRmbCbtMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbCbt> page = new Page<IfsCfetsrmbCbt>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbCbtMapper.getRmbCbtMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbCbtMapper.getRmbCbtMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbCbtMapper.getRmbCbtMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 质押式回购CR
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertCr(com.singlee.ifs.model.
	 * IfsCfetsrmbCr)
	 */
	@Override
	public String insertCr(IfsCfetsrmbCr record) {
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("CR"));
		String str = "";
		List<IfsCfetsrmbCr> list = cfetsrmbCrMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbCrMapper.insert(record);
				List<IfsCfetsrmbDetailCr> records=record.getBondDetailsList();
				for(int i=0;i<records.size();i++){
					IfsCfetsrmbDetailCr rec=records.get(i);
					rec.setTicketId(record.getTicketId());
					rec.setSeq((i+1)+"");
					detailCrMapper.insert(rec);
				}
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void updateCr(IfsCfetsrmbCr record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbCrMapper.updateByPrimaryKey(record);
		detailCrMapper.deleteByTicketId(record.getTicketId());
		List<IfsCfetsrmbDetailCr> records=record.getBondDetailsList();
		for(int i=0;i<records.size();i++){
			IfsCfetsrmbDetailCr rec=records.get(i);
			rec.setTicketId(record.getTicketId());
			rec.setSeq((i+1)+"");
			detailCrMapper.insert(rec);
		}
	}

	@Override
	public void deleteCr(IfsCfetsrmbCrKey key) {
		cfetsrmbCrMapper.deleteByPrimaryKey(key);
		detailCrMapper.deleteByTicketId(key.getTicketId());
	}

	@Override
	public Page<IfsCfetsrmbCr> getCrList(IfsCfetsrmbCr record) {
		Page<IfsCfetsrmbCr> page = cfetsrmbCrMapper.selectIfsCfetsrmbCrList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbCr getCrById(IfsCfetsrmbCrKey key) {
		IfsCfetsrmbCr cr = cfetsrmbCrMapper.selectByPrimaryKey(key);
		return cr;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbCr> getRmbCrMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbCr> page = new Page<IfsCfetsrmbCr>();
		//查询条件准备
		String prdNo = ParameterUtil.getString(params, "prdNo", "446");
		if (isFinished == 1) {// 待审批
			page = cfetsrmbCrMapper.getRmbCrMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbCrMapper.getRmbCrMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbCrMapper.getRmbCrMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 
	 * 信用拆借IBO
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertIbo(com.singlee.ifs.model.
	 * IfsCfetsrmbIbo)
	 */

	@Override
	public String insertIbo(IfsCfetsrmbIbo record) {
		record.setTicketId(getTicketId("IBO"));
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsrmbIbo> list = cfetsrmbIboMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbIboMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void updateIbo(IfsCfetsrmbIbo record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbIboMapper.updateByPrimaryKey(record);
	}

	@Override
	public void deleteIbo(IfsCfetsrmbIboKey key) {
		cfetsrmbIboMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsCfetsrmbIbo> getIboList(IfsCfetsrmbIbo record) {
		Page<IfsCfetsrmbIbo> page = cfetsrmbIboMapper.selectIfsCfetsrmbIboList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbIbo getIboById(IfsCfetsrmbIboKey key) {
		IfsCfetsrmbIbo ibo = cfetsrmbIboMapper.selectByPrimaryKey(key);
		return ibo;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbIbo> getRmbIboMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbIbo> page = new Page<IfsCfetsrmbIbo>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbIboMapper.getRmbIboMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbIboMapper.getRmbIboMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbIboMapper.getRmbIboMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 利率互换IRS
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertIrs(com.singlee.ifs.model.
	 * IfsCfetsrmbIrs)
	 */
	@Override
	public String insertIrs(IfsCfetsrmbIrs record) {
		record.setTicketId(getTicketId("IRS"));
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsrmbIrs> list = cfetsrmbIrsMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbIrsMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void updateIrs(IfsCfetsrmbIrs record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbIrsMapper.updateByPrimaryKey(record);
	}

	@Override
	public void deleteIrs(IfsCfetsrmbIrsKey key) {
		cfetsrmbIrsMapper.deleteByPrimaryKey(key);
	}

	@Override
	public Page<IfsCfetsrmbIrs> getIrsList(IfsCfetsrmbIrs record) {
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsMapper.selectIfsCfetsrmbIrsList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbIrs getIrsById(IfsCfetsrmbIrsKey key) {
		IfsCfetsrmbIrs irs = cfetsrmbIrsMapper.selectByPrimaryKey(key);
		return irs;
	}

	// 20180507 新增
	@Override
	public IfsCfetsrmbIrs getIrsAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbIrs irs = cfetsrmbIrsMapper.selectFlowIdByPrimaryKey(key);
		return irs;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbIrs> getRmbIrsMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbIrs> page = new Page<IfsCfetsrmbIrs>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbIrsMapper.getRmbIrsMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbIrsMapper.getRmbIrsMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbIrsMapper.getRmbIrsMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 买断式回购OR
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertOr(com.singlee.ifs.model.
	 * IfsCfetsrmbOr)
	 */
	@Override
	public String insertOr(IfsCfetsrmbOr record) {
		record.setTicketId(getTicketId("OR"));
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String str = "";
		List<IfsCfetsrmbOr> list = cfetsrmbOrMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbOrMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void updateOr(IfsCfetsrmbOr record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbOrMapper.updateByPrimaryKey(record);
	}

	@Override
	public void deleteOr(IfsCfetsrmbOrKey key) {
		cfetsrmbOrMapper.deleteByPrimaryKey(key);
	}
	@Override
	public IfsCfetsrmbOr selectById(Map<String, String> map) {
		IfsCfetsrmbOr or = cfetsrmbOrMapper.selectById(map);
		return or;
	}

	@Override
	public Page<IfsCfetsrmbOr> getOrList(IfsCfetsrmbOr record) {
		Page<IfsCfetsrmbOr> page = cfetsrmbOrMapper.selectIfsCfetsrmbOrList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbOr getOrById(IfsCfetsrmbOrKey key) {
		IfsCfetsrmbOr or = cfetsrmbOrMapper.selectByPrimaryKey(key);
		return or;
	}

	/* 增加： 根据id获取实体对象和流程id */
	@Override
	public IfsCfetsrmbOr getOrAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbOr ifsCfetsrmbOr = cfetsrmbOrMapper.selectFlowIdByPrimaryKey(key);
		return ifsCfetsrmbOr;
	}

	/***
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbOr> getRmbOrMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbOr> page = new Page<IfsCfetsrmbOr>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbOrMapper.getRmbOrMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbOrMapper.getRmbOrMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbOrMapper.getRmbOrMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	/*
	 * 债券借贷SL
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertSl(com.singlee.ifs.model.
	 * IfsCfetsrmbSl)
	 */
	@Override
	public String insertSl(IfsCfetsrmbSl record) {
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("SL"));
		String str = "";
		List<IfsCfetsrmbSl> list = cfetsrmbSlMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbSlMapper.insert(record);
				List<IfsCfetsrmbDetailSl> records=record.getBondDetailsList();
				for(int i=0;i<records.size();i++){
					IfsCfetsrmbDetailSl rec=records.get(i);
					rec.setTicketId(record.getTicketId());
					rec.setSeq((i+1)+"");
					detailSlMapper.insert(rec);
				}
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}

	@Override
	public void updateSl(IfsCfetsrmbSl record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		cfetsrmbSlMapper.updateByPrimaryKey(record);
		detailSlMapper.deleteByTicketId(record.getTicketId());
		List<IfsCfetsrmbDetailSl> records=record.getBondDetailsList();
		for(int i=0;i<records.size();i++){
			IfsCfetsrmbDetailSl rec=records.get(i);
			rec.setTicketId(record.getTicketId());
			rec.setSeq((i+1)+"");
			detailSlMapper.insert(rec);
		}
	}

	@Override
	public void deleteSl(IfsCfetsrmbSlKey key) {
		cfetsrmbSlMapper.deleteByPrimaryKey(key);
		detailSlMapper.deleteByTicketId(key.getTicketId());
	}

	@Override
	public Page<IfsCfetsrmbSl> getSlList(IfsCfetsrmbSl record) {
		Page<IfsCfetsrmbSl> page = cfetsrmbSlMapper.selectIfsCfetsrmbSlList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbSl getSlById(IfsCfetsrmbSlKey key) {
		IfsCfetsrmbSl sl = cfetsrmbSlMapper.selectByPrimaryKey(key);
		return sl;
	}

	/***
	 * 根据请求参数查询分页列表-债券借贷-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbSl> getRmbSlMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbSl> page = new Page<IfsCfetsrmbSl>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbSlMapper.getRmbSlMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbSlMapper.getRmbSlMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbSlMapper.getRmbSlMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	/**
	 * 事前交易
	 */
	@Override
	public Page<IfsCfetsBeforeDeal> searchCfetsBeforeDeal(IfsCfetsBeforeDeal record) {
		Page<IfsCfetsBeforeDeal> page = cfetsBeforeDealMapper.searchCfetsBeforeDeal(record);
		return page;
	}
	@Override
	public IfsCfetsBeforeDeal queryCfetsBeforeDealById(Map<String, Object> key) {
		IfsCfetsBeforeDeal dp = cfetsBeforeDealMapper.queryCfetsBeforeDealById(key);
		return dp;
	}
	@Override
	public void insertBeforeDeal(Map<String, Object> key) {
		cfetsBeforeDealMapper.insertBeforeDeal(key);
	}
	@Override
	public void updateBeforeDeal(Map<String, Object> key) {
		cfetsBeforeDealMapper.updateBeforeDeal(key);
	}
	@Override
	public void deleteBeforeDeal(Map<String, Object> key) {
		cfetsBeforeDealMapper.deleteBeforeDeal(key);
	}
	@Override
	public void updateBeforeDealStatus(Map<String, Object> key) {
		cfetsBeforeDealMapper.updateBeforeDealStatus(key);
	}
	/**
	 * 流程特殊用户
	 */
	@Override
	public Page<IfsCfetsFlowUser> searchCfetsFlowUserManage(IfsCfetsFlowUser record) {
		Page<IfsCfetsFlowUser> page = cfetsFlowUserMapper.searchCfetsFlowUserManage(record);
		return page;
	}
	@Override
	public IfsCfetsFlowUser queryCfetsFlowUserById(Map<String, Object> key) {
		IfsCfetsFlowUser dp = cfetsFlowUserMapper.queryCfetsFlowUserById(key);
		return dp;
	}
	@Override
	public void insertFlowUser(Map<String, Object> key) {
		cfetsFlowUserMapper.insertFlowUser(key);
	}
	@Override
	public void updateFlowUser(Map<String, Object> key) {
		cfetsFlowUserMapper.updateFlowUser(key);
	}
	@Override
	public void deleteFlowUser(Map<String, Object> key) {
		cfetsFlowUserMapper.deleteFlowUser(key);
	}
	/*
	 * 存单发行DP
	 * 
	 * @see com.singlee.ifs.service.IFSService#insertSl(com.singlee.ifs.model.
	 * IfsCfetsrmbSl)
	 */
	@Override
	public String insertDp(IfsCfetsrmbDp record) {
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("M"));
		String str = "";
		List<IfsCfetsrmbDp> list = cfetsrmbDpMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				cfetsrmbDpMapper.insert(record);
				List<IfsCfetsrmbDpOffer> records=record.getIfsCfetsrmbDpOffer();
				for(int i=0;i<records.size();i++){
					IfsCfetsrmbDpOffer rec = records.get(i);
					rec.setTicketId(record.getTicketId());
					rec.setSeq(String.valueOf(i+1));
					offerDpMapper.insert(rec);
				}
				str = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;

	}
	@Override
	public void updateDp(IfsCfetsrmbDp record) {
		try {
			record.setApproveStatus(DictConstants.ApproveStatus.New);
			cfetsrmbDpMapper.updateByPrimaryKey(record);
			offerDpMapper.deleteByTicketId(record.getTicketId());
			List<IfsCfetsrmbDpOffer> records=record.getIfsCfetsrmbDpOffer();
			for(int i=0;i<records.size();i++){
				IfsCfetsrmbDpOffer rec = records.get(i);
				rec.setTicketId(record.getTicketId());
				rec.setSeq(String.valueOf(i+1));
				offerDpMapper.insert(rec);
			}
		} catch (Exception e) {
			JY.raiseRException("修改失败",e);
		}
	}

	@Override
	public void deleteDp(IfsCfetsrmbDpKey key) {
		cfetsrmbDpMapper.deleteByPrimaryKey(key);
		offerDpMapper.deleteByTicketId(key.getTicketId());
	}

	@Override
	public Page<IfsCfetsrmbDp> getDpList(IfsCfetsrmbDp record) {
		Page<IfsCfetsrmbDp> page = cfetsrmbDpMapper.selectIfsCfetsrmbDpList(record);
		return page;
	}

	@Override
	public IfsCfetsrmbDp getDpById(IfsCfetsrmbDpKey key) {
		IfsCfetsrmbDp dp = cfetsrmbDpMapper.selectByPrimaryKey(key);
		return dp;
	}
	
	@Override
	public Page<IfsCfetsrmbDpOffer> searchOfferDpDetails(
			Map<String, Object> params) {
		return offerDpMapper.searchOfferDpDetails(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	public IfsCfetsrmbDp getDpAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbDp dp = cfetsrmbDpMapper.selectFlowIdByPrimaryKey(key);
		return dp;
	}
	
	/**
	 * 根据请求参数查询分页列表-存单发行-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsCfetsrmbDp> getRmbDpMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbDp> page = new Page<IfsCfetsrmbDp>();
		if (isFinished == 1) {// 待审批
			page = cfetsrmbDpMapper.getRmbDpMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = cfetsrmbDpMapper.getRmbDpMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = cfetsrmbDpMapper.getRmbDpMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}


	//同业存放(外币)

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Override
	public String addCcyDepositIn(IfsFxDepositIn record){
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("LEND"));
		String str = "";
		List<IfsFxDepositIn> list = ifsFxDepositInMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				ifsFxDepositInMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	/**
	 * 修改
	 * @param map
	 */
	@Override
	public void editCcyDepositIn(IfsFxDepositIn map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsFxDepositInMapper.updateByPrimaryKey(map);
	}

	/**
	 * 删除
	 * @param ticketid
	 */
	@Override
	public void deleteCcyDepositIn(String ticketid) {
		ifsFxDepositInMapper.deleteByTicketid(ticketid);
	}

	/**
	 * 查询
	 * @param params
	 * @return
	 */
	@Override
	public Page<IfsFxDepositIn> searchCcyDepositIn(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		Page<IfsFxDepositIn> page = ifsFxDepositInMapper.selectByTicketId(ticketId, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 根据ticketid查询
	 * @param params
	 * @return
	 */
	@Override
	public IfsFxDepositIn searchByTicketId(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		IfsFxDepositIn ifsFxDepositIn = ifsFxDepositInMapper.searchByTicketId(ticketId);
		return ifsFxDepositIn;
	}

	/**
	 * 根据ticketid查询
	 * @param params
	 * @return
	 */
	@Override
	public IfsFxDepositIn searchDepositInFXFlowByTicketId(Map<String, Object> params) {
		IfsFxDepositIn ifsFxDepositIn = ifsFxDepositInMapper.selectFlowByTicketId(params);
		return ifsFxDepositIn;
	}

	/**
	 * 根据请求参数查询分页列表-存单发行-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsFxDepositIn> getDepositInFXMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsFxDepositIn> page = new Page<IfsFxDepositIn>();
		if (isFinished == 1) {// 待审批
			page = ifsFxDepositInMapper.getDepositInFXMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsFxDepositInMapper.getDepositInFXMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsFxDepositInMapper.getDepositInFXMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	//存放同业(外币)
	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Override
	public String addCcyDepositout(IfsFxDepositout record){
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("LEND"));
		String str = "";
		List<IfsFxDepositout> list = ifsFxDepositoutMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				ifsFxDepositoutMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	/**
	 * 修改
	 * @param map
	 */
	@Override
	public void editCcyDepositout(IfsFxDepositout map) {
		map.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsFxDepositoutMapper.updateByPrimaryKey(map);
	}

	/**
	 * 删除
	 * @param ticketid
	 */
	@Override
	public void deleteCcyDepositout(String ticketid) {
		ifsFxDepositoutMapper.deleteByTicketid(ticketid);
	}

	/**
	 * 查询
	 * @param params
	 * @return
	 */
	@Override
	public Page<IfsFxDepositout> searchCcyDepositout(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		Page<IfsFxDepositout> page = ifsFxDepositoutMapper.selectByTicketId(ticketId, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 根据ticketid查询
	 * @param params
	 * @return
	 */
	@Override
	public IfsFxDepositout searchDepositoutFXByTicketId(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchByTicketId(ticketId);
		return ifsFxDepositout;
	}

	/**
	 * 根据ticketid查询
	 * @param params
	 * @return
	 */
	@Override
	public IfsFxDepositout searchFlowDepositoutFXByTicketId(Map<String, Object> params) {
		IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchFlowByTicketId(params);
		return ifsFxDepositout;
	}

	/**
	 * 根据请求参数查询分页列表-存单发行-我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsFxDepositout> getDepositoutFXMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsFxDepositout> page = new Page<IfsFxDepositout>();
		if (isFinished == 1) {// 待审批
			page = ifsFxDepositoutMapper.getDepositoutFXMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsFxDepositoutMapper.getDepositoutFXMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsFxDepositoutMapper.getDepositoutFXMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}


	//同业存放

	@Override
	public void editRmbDepositIn(IfsRmbDepositin record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRmbDepositinMapper.updateByPrimaryKey(record);
	}

	@Override
	public String addRmbDepositIn(IfsRmbDepositin record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("LEND"));
		String str = "";
		List<IfsRmbDepositin> list = ifsRmbDepositinMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				ifsRmbDepositinMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void deleteRmbDepositIn(String ticketid) {
		ifsRmbDepositinMapper.deleteByTicketid(ticketid);
	}

	@Override
	public Page<IfsRmbDepositin> searchRmbDepositIn(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		Page<IfsRmbDepositin> page = ifsRmbDepositinMapper.selectByTicketId(ticketId, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsRmbDepositin> getDepositInRmbMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsRmbDepositin> page = new Page<IfsRmbDepositin>();
		if (isFinished == 1) {// 待审批
			page = ifsRmbDepositinMapper.getDepositInRmbMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRmbDepositinMapper.getDepositInRmbMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRmbDepositinMapper.getDepositInRmbMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public IfsRmbDepositin searchDepositInRmbByTicketId(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		IfsRmbDepositin ifsRmbDepositin = ifsRmbDepositinMapper.searchByTicketId(ticketId);
		return ifsRmbDepositin;
	}

	@Override
	public IfsRmbDepositin searchFlowDepositInRmbByTicketId(Map<String, Object> params) {
		IfsRmbDepositin ifsRmbDepositin = ifsRmbDepositinMapper.searchFlowByTicketId(params);
		return ifsRmbDepositin;
	}


	//存放同业
	@Override
	public void editRmbDepositOut(IfsRmbDepositout record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRmbDepositoutMapper.updateByPrimaryKey(record);
	}

	@Override
	public String addRmbDepositOut(IfsRmbDepositout record) {
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		record.setTicketId(getTicketId("LEND"));
		String str = "";
		List<IfsRmbDepositout> list = ifsRmbDepositoutMapper.searchById(record.getContractId());
		if (list.isEmpty()) {
			try {
				ifsRmbDepositoutMapper.insert(record);
				str = "保存成功";
			} catch (Exception e) {
				str = "保存失败";
				JY.raiseRException("保存失败",e);
			}

		} else {
			str = "保存失败,该笔成交单号已存在";
		}
		return str;
	}

	@Override
	public void deleteRmbDepositOut(String ticketid) {
		ifsRmbDepositoutMapper.deleteByTicketid(ticketid);
	}

	@Override
	public Page<IfsRmbDepositout> searchRmbDepositOut(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		Page<IfsRmbDepositout> page = ifsRmbDepositoutMapper.selectByTicketId(ticketId, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsRmbDepositout> getDepositOutRmbMinePage(Map<String, Object> params, int isFinished) {
		Page<IfsRmbDepositout> page = new Page<IfsRmbDepositout>();
		if (isFinished == 1) {// 待审批
			page = ifsRmbDepositoutMapper.getDepositoutRmbMineList(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRmbDepositoutMapper.getDepositoutRmbMineListFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRmbDepositoutMapper.getDepositoutRmbMineListMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public IfsRmbDepositout searchDepositOutRmbByTicketId(Map<String, Object> params) {
		String ticketId = ParameterUtil.getString(params, "ticketId", "");
		IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(ticketId);
		return ifsRmbDepositout;
	}

	@Override
	public IfsRmbDepositout searchFlowDepositOutRmbByTicketId(Map<String, Object> params) {
		IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchFlowByTicketId(params);
		return ifsRmbDepositout;
	}


	@Override
	public IfsCfetsrmbIbo getIboAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbIbo ibo = cfetsrmbIboMapper.selectFlowIdByPrimaryKey(key);
		return ibo;
	}

	@Override
	public IfsCfetsrmbSl getSlAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbSl sl = cfetsrmbSlMapper.selectFlowIdByPrimaryKey(key);
		return sl;
	}

	@Override
	public IfsCfetsrmbCr getCrAndFlowIdById(Map<String, Object> key) {
		IfsCfetsrmbCr cr = cfetsrmbCrMapper.selectFlowIdByPrimaryKey(key);
		return cr;
	}

	@Override
	public IfsCfetgoldLend getGoldLendAndFlowById(Map<String, Object> key) {
		IfsCfetgoldLend gold = ifsCfetgoldLendMapper.selectFlowByPrimaryKey(key);
		return gold;
	}

	@Override
	public IfsCfetsfxDebt getDebtAndFlowById(Map<String, Object> key) {
		IfsCfetsfxDebt debt = ifsCfetsfxDebtMapper.selectFlowByPrimaryKey(key);
		return debt;
	}

	@Override
	public IfsCfetsfxAllot searchRmbCcypaAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxAllot allot = ifsCfetsAllocateMapper.selectFlowByPrimaryKey(key);
		return allot;
	}

	@Override
	public IfsCfetsfxOnspot searchRmbSpotAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxOnspot sport = ifsCfetsfxOnspotMapper.selectFlowByPrimaryKey(key);
		return sport;
	}

	@Override
	public IfsCfetsfxOption searchRmbOptAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxOption opt = ifsCfetsfxOptionMapper.selectFlowByPrimaryKey(key);
		return opt;
	}

	@Override
	public IfsCfetsfxLend searchRmbCcylendingAndFlowIdById(Map<String, Object> key) {
		IfsCfetsfxLend lend = ifsCfetsfxLendMapper.selectFlowByPrimaryKey(key);
		return lend;
	}

	@Override
	public Page<IfsCfetsrmbIrs> getApprovePassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbIrs> page = cfetsrmbIrsMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}
	
	

	@Override
	public Page<IfsCfetgoldLend> getApproveGoldLendPassedPage(Map<String, Object> params) {
		Page<IfsCfetgoldLend> page = ifsCfetgoldLendMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsmetalGold> getApprovemetalGoldPassedPage(Map<String, Object> params) {
		Page<IfsCfetsmetalGold> page = cfetsmetalGoldMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsrmbCbt> getApproveCbtPassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbCbt> page = cfetsrmbCbtMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsrmbCr> getApproveCrPassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbCr> page = cfetsrmbCrMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsrmbIbo> getApproveIboPassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbIbo> page = cfetsrmbIboMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsrmbOr> getApproveOrPassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbOr> page = cfetsrmbOrMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsrmbSl> getApproveSlPassedPage(Map<String, Object> params) {
		Page<IfsCfetsrmbSl> page = cfetsrmbSlMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxCswap> getApprovefxCswapPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxCswap> page = ifsCfetsfxCswapMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxLend> getApprovefxLendPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxLend> page = ifsCfetsfxLendMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxAllot> getApprovefxAllotPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxAllot> page = ifsCfetsAllocateMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxOnspot> getApproveOnspotPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxOnspot> page = ifsCfetsfxOnspotMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxDebt> getApprovefxDebtPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxDebt> page = ifsCfetsfxDebtMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxSwap> getApprovefxSwapPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxSwap> page = ifsCfetsfxSwapMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxFwd> getApprovefxFwdPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxFwd> page = ifsCfetsfxFwdMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxSpt> getApprovefxSptPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxSpt> page = ifsCfetsfxSptMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public Page<IfsCfetsfxOption> getApprovefxOptionPassedPage(Map<String, Object> params) {
		Page<IfsCfetsfxOption> page = ifsCfetsfxOptionMapper.searchApprovePassed(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("sStr", "");// 前缀
		hashMap.put("trdType", "2");// 贸易类型，正式交易
		// CSWAP20170831339000707
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

	/**
	 * @see com.singlee.ifs.service.IFSService#rebackData(java.util.Map)
	 */
	@Override
	public void rebackData(Map<String, Object> params) {
		String updatesql = "update " + params.get("tableName") + " set DEAL_TRANS_TYPE="+"'" + params.get("dealTransType") + "'"+" where TICKET_ID=" + "'" + params.get("ticketId") + "'";
		cfetsrmbSlMapper.updateTransTypeByPrimaryKey(updatesql);
		
		String dealNo = ParameterUtil.getString(params,"ticketId","");
		//撤销的交易进行额度释放
		edCustManangeService.eduReleaseFlowService(dealNo);
		//到期释放债券
		bondLimitTemplateMapper.deleteOverTimeBond(dealNo);	

			
	}

	@Override
	public Page<IfsCfetsrmbDetailCr> searchBondCrDetails(
			Map<String, Object> params) {
		return detailCrMapper.searchBondCrDetails(params, ParameterUtil.getRowBounds(params));
	}

	@Override
	public Page<IfsCfetsrmbDetailSl> searchBondSlDetails(
			Map<String, Object> params) {
		return detailSlMapper.searchBondSlDetails(params, ParameterUtil.getRowBounds(params));
	}
	
	

	@Override
	public Page<IfsRevIrvv> searchPageIrvv(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsRevIrvv> result = ifsRevIrvvMapper.searchPageIrvv(map, rb);
		return result;
	}
	
	@Override
	public Page<IfsRevIrvv> searchIrvv(Map<String,Object> map,int isFinished){
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<IfsRevIrvv> result;
		if(isFinished == 1) {//待审批
			result=ifsRevIrvvMapper.searchIrvvUnfinished(map, rb);
		}else if(isFinished == 2) {//已审批
			result=ifsRevIrvvMapper.searchIrvvFinished(map, rb);
		}else {//我发起的
			result=ifsRevIrvvMapper.searchIrvvMine(map, rb);
		}
		
		return result;
	}

	@Override
	public void insert(IfsRevIrvv entity) {
		/** 新增时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId = ifsRevIrvvMapper.getId();
		entity.setTicketId(ticketId);
		ifsRevIrvvMapper.insert(entity);
		
	}

	@Override
	public void deleteById(String id) {
		ifsRevIrvvMapper.deleteById(id);
		
	}

	@Override
	public void updateById(IfsRevIrvv entity) {
		/** 修改时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRevIrvvMapper.updateById(entity);
		
	}
	
	//外汇类mini弹框
	@Override
	public Page<Map<String, Object>> getMini(Map<String, Object> params) {
		String dealType=(String)params.get("dealType");
		String type="";
		if("spt".equals(dealType)|| "fwd".equals(dealType)|| "swap".equals(dealType)){
			type=dealType;
		}
		params.put("type", type);
		Page<Map<String, Object>> page = ifsCfetsfxSwapMapper.getMini(params, ParameterUtil.getRowBounds(params));
		return page;
	}
	
	//回购类mini弹框
	@Override
	public Page<Map<String, Object>> getBondMini(Map<String, Object> params) {
	    String tableName = ParameterUtil.getString(params, "tableName", "");
	    String[] prodtype = {"","","",""};
	    String[] cost = {"8400000051"};
	    String costind = null;

	    if("446".equals(tableName)) {//质押式回购
            prodtype[0] = "VP";
            prodtype[1] = "RP";
        }
        if("467".equals(tableName)) {//国库定期存款
            prodtype[0] = "GD";
        }
        if("468".equals(tableName)) {//交易所回购
			prodtype[0] = "VP";
			prodtype[1] = "RP";
			prodtype[2] = "VB";
			prodtype[3] = "RB";
			costind = "1";
        }
        if("469".equals(tableName)) {//常备借贷便利
            prodtype[0] = "CB";
        }
        if("470".equals(tableName)) {//中期借贷便利
            prodtype[0] = "ZQ";
        }
        if("472".equals(tableName)) {//线下押券
            prodtype[0] = "ZD";
        }
	    if("442".equals(tableName)) {
	        tableName = "IFS_CFETSRMB_OR";
	        prodtype = null;
	    } else {
	        tableName = "IFS_CFETSRMB_CR";
	    }
	    params.put("costind", costind);
	    params.put("cost", cost);
	    params.put("prodtype", prodtype);
	    params.put("tableName", tableName);
		Page<Map<String, Object>> page = cfetsrmbCrMapper.getBondMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	
	//拆借类mini弹框
	@Override
	public Page<Map<String, Object>> getLendMini(Map<String, Object> params) {
	    String prdNo = ParameterUtil.getString(params, "prdNo", "444");
        params.put("prdNo", prdNo);
		Page<Map<String, Object>> page = cfetsrmbIboMapper.getLendMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	
	//拆借类mini弹框
    @Override
    public Page<Map<String, Object>> getDELendMini(Map<String, Object> params) {
        Page<Map<String, Object>> page = cfetsrmbIboMapper.getDELendMini(params,ParameterUtil.getRowBounds(params));
        return page;
    }
	
	//期权类mini弹框
	@Override
	public Page<Map<String, Object>> getOptMini(Map<String, Object> params) {
		Page<Map<String, Object>> page = ifsCfetsfxOptionMapper.getOptMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	
	//互换类mini弹框
	@Override
	public Page<Map<String, Object>> getIswhMini(Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbIrsMapper.getIswhMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}
	
	//债券类mini弹框
	@Override
	public Page<Map<String, Object>> getSlMini(Map<String, Object> params) {
		Page<Map<String, Object>> page = cfetsrmbSlMapper.getSlMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public IfsCfetsrmbIbo searchCfetsRmbIboById(String ticketId) {
		
		IfsCfetsrmbIbo result = cfetsrmbIboMapper.searchCfetsRmbIboById(ticketId);
		return result;
		
	}

	@Override
	public IfsCfetsrmbCr searchCfetsrmbCrByTicketId(String ticketId) {
		
		IfsCfetsrmbCr result = cfetsrmbCrMapper.searchCfetsrmbCrByTicketId(ticketId);
		return result;
	}

	@Override
	public IfsCfetsrmbOr searchCfetsrmbOrByTicketId(String ticketId) {
		
		IfsCfetsrmbOr result = cfetsrmbOrMapper.searchCfetsrmbOrByTicketId(ticketId);
		return result;
	}

	@Override
	public IfsCfetsrmbIrs searchIrsByTicketId(String ticketId) {
		IfsCfetsrmbIrs ifsCfetsrmbIrs = cfetsrmbIrsMapper.searchIrsByTicketId(ticketId);
		return ifsCfetsrmbIrs;
	}

	@Override
	public IfsCfetsrmbSl searchCfetsrmbSlByTicketId(Map<String, Object> map) {
		IfsCfetsrmbSl result = cfetsrmbSlMapper.searchCfetsrmbSlByTicketId(map);
		return result;
	}

	@Override
	public IfsCfetsrmbCbt searchCfetsrmbCbtByTicketId(Map<String, Object> map) {
		IfsCfetsrmbCbt result = cfetsrmbCbtMapper.searchCfetsrmbCbtByTicketId(map);
		return result;
	}
	//交易台账
	@Override
	public Page<IfsDealDeskReport> searchPageDealDesk(@RequestBody Map<String, Object> params) {
		Page<IfsDealDeskReport> page = ifsDealDeskReportMapper.searchPageDealDesk(params);
		return page;
	}

	/**
	 * 更新日期
	 */
	@Override
    public void updateCurDate(String date) {
		ifsMapper.updateCurDate(date);
	}
	
	@Override
	public void nbhAdd(Map<String, Object> map) {
		ifsMapper.nbhAdd(map);
	}
	
	@Override
	public Page<IfsNbhBean> searchData(IfsNbhBean entity) {
		Page<IfsNbhBean> page = ifsMapper.searchData(entity);
		return page;
	}

	@Override
	public void deleteNbh(Map<String, String> map) {
		ifsMapper.deleteNbh(map);
	}

	@Override
	public void updateById(IfsNbhBean entity) {
		ifsMapper.updateById(entity);
	}

	@Override
	public Page<Map<String, Object>> getDpMini(Map<String, Object> params) {
	    String tableName = ParameterUtil.getString(params, "tableName", "IFS_CFETSRMB_DP");
	    if("IFS_CFETSRMB_DP_CD".equals(tableName)) {
	        params.put("tableName", "IFS_CFETSRMB_DP");
	        params.put("instrumentType", "CD");
	    } else {
	        params.put("instrumentType", "SE");
	    }
		Page<Map<String, Object>> page = cfetsrmbSlMapper.getDpMini(params,ParameterUtil.getRowBounds(params));
		return page;
	}

	@Override
	public IfsCfetsrmbDp searchCfetsrmbDpByTicketId(Map<String, Object> map) {
		IfsCfetsrmbDp result = cfetsrmbDpMapper.searchCfetsrmbDpByTicketId(map);
		return result;
	}
	
	@Override
	public String queryDictValue(Map<String, String> map) {
		String value = ifsMapper.queryDictValue(map);
		return value;
	}
	
	/**
	 * 外汇头寸估值 分页查询
	 */
	@Override
	public Page<IfsNbhGatherBean> searchNbhPage(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsNbhGatherBean> result = ifsMapper.searchNbhPage(map, rb);
		return result;
	}
	
	/**
	 * 外汇头寸估值  新增保存
	 */
	@Override
	public void nbhGatherAdd(IfsNbhGatherBean entity) {
		entity.setLocalstatus("0"); // 未同步
		ifsMapper.nbhGatherAdd(entity);
	}
	
	/**
	 * 外汇头寸估值  修改时保存
	 */
	@Override
	public void updateByGatherId(IfsNbhGatherBean entity) {
		ifsMapper.updateByGatherId(entity);
	}
	
	/**
	 * 外汇头寸估值  删除
	 */
	@Override
	public void deleteNbhAcct(Map<String, String> map) {
		ifsMapper.deleteNbhAcct(map);
	}

	/**
	 * 根据fedealno查询实体类
	 */
	@Override
	public IfsNbhGatherBean searchNbhPageSingle(String fedealno) {
		IfsNbhGatherBean bean = ifsMapper.searchNbhPageSingle(fedealno);
		return bean;
	}
	
	/**
	 * 修改处理状态
	 */
	@Override
	public void updateStatus(IfsNbhGatherBean entity) {
		ifsMapper.updateStatus(entity);
	}

	@Override
	public IfsCfetsrmbCr searchCrDetailByTicketId(Map<String, Object> map) {
		return cfetsrmbCrMapper.searchCrDetailByTicketId(map);
	}

	@Override
	public IfsCfetsfxFwd searchFwdDetailByTicketId(Map<String, Object> map) {
		return ifsCfetsfxFwdMapper.searchFwdDetailByTicketId(map);
	}

	@Override
	public IfsCfetsfxLend searchLendDetailByTicketId(Map<String, Object> map) {
		return ifsCfetsfxLendMapper.searchLendDetailByTicketId(map);
	}

	@Override
	public IfsCfetsfxOption searchOptionDetailByTicketId(Map<String, Object> map) {
		return ifsCfetsfxOptionMapper.searchOptionDetailByTicketId(map);
	}

	@Override
	public IfsCfetsfxSpt searchSptDetailByTicketId(Map<String, Object> map) {
		return ifsCfetsfxSptMapper.searchSptDetailByTicketId(map);
	}

	@Override
	public IfsCfetsfxSwap searchSwapDetailByTicketId(Map<String, Object> map) {
		return ifsCfetsfxSwapMapper.searchSwapDetailByTicketId(map);
	}

	@Override
	public IfsCfetsrmbIbo searchIboDetailByTicketId(Map<String, Object> map) {
		return cfetsrmbIboMapper.searchIboDetailByTicketId(map);
	}

	@Override
	public IfsCfetsrmbIrs searchIrsDetailByTicketId(Map<String, Object> map) {
		return cfetsrmbIrsMapper.searchIrsDetailByTicketId(map);
	}

	@Override
	public IfsCfetsrmbOr searchOrDetailByTicketId(Map<String, Object> map) {
		return cfetsrmbOrMapper.searchOrDetailByTicketId(map);
	}

	@Override
	public PageInfo<IfsCfetsfxSpt> searchSptCheck(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsfxSpt> result = ifsCfetsfxSptMapper.searchSptCheck(map, rb);
		return new PageInfo<IfsCfetsfxSpt>(result);
	}

	@Override
	public PageInfo<IfsCfetsrmbCbt> searchCbtCheck(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsrmbCbt> result = cfetsrmbCbtMapper.searchCbtCheck(map, rb);
		return new PageInfo<IfsCfetsrmbCbt>(result);
	}

	@Override
	public PageInfo<IfsCfetsrmbOr> searchCapitalCheck(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCfetsrmbOr> result = cfetsrmbOrMapper.searchCapitalCheck(map, rb);
		return new PageInfo<IfsCfetsrmbOr>(result);
	}

	@Override
	public List<IfsCfetsfxSpt> searchSptCheckList(Map<String, Object> map) {
		return ifsCfetsfxSptMapper.searchSptCheckList(map);
	}

	@Override
	public List<IfsCfetsrmbCbt> searchCbtCheckList(Map<String, Object> map) {
		return cfetsrmbCbtMapper.searchCbtCheckList(map);
	}

	@Override
	public List<IfsCfetsrmbOr> searchCapitalCheckList(Map<String, Object> map) {
		return cfetsrmbOrMapper.searchCapitalCheckList(map);
	}

	@Override
	public List<IfsRepoReportBean> searchRepoReportList(Map<String, Object> map) {
		return cfetsrmbOrMapper.searchRepoReportList(map);
	}

	@Override
	public IfsRepoReportBean searchRepoReportListTotal(Map<String, Object> map) {
		return cfetsrmbOrMapper.searchRepoReportListTotal(map);
	}
	
	@Override
	public List<IfsIboReportBean> searchIboReportList(Map<String, Object> map) {
		return ifsCfetsrmbIboMapper.searchIboReportList(map);
	}

	@Override
	public IfsIboReportBean searchIboReportListTotal(Map<String, Object> map) {
		return ifsCfetsrmbIboMapper.searchIboReportListTotal(map);
	}
	
	@Override
	public List<IfsCbtReportBean> searchCbtReportList(Map<String, Object> map) {
		return ifsCfetsrmbCbtMapper.searchCbtReportList(map);
	}

	@Override
	public IfsCbtReportBean searchCbtReportListTotal(Map<String, Object> map) {
		return ifsCfetsrmbCbtMapper.searchCbtReportListTotal(map);
	}

	@Override
	public IfsCfetsmetalGold getGoldByIdAndType(Map<String, String> map) {
		return cfetsmetalGoldMapper.getGoldByIdAndType(map);
	}

	@Override
	public IfsCfetsmetalGold searchMetalGold(Map<String, Object> map) {
		return cfetsmetalGoldMapper.searchMetalGold(map);
	}

	@Override
	public Page<IfsAdvIdlv> searchPageIdlv(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsAdvIdlv> result = ifsAdvIdlvMapper.searchPageIdlv(map, rb);
		return result;
	}

	@Override
	public Page<IfsAdvIdlv> searchIdlv(Map<String, Object> map, int isFinished) {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<IfsAdvIdlv> result;
		if(isFinished == 1) {//待审批
			result=ifsAdvIdlvMapper.searchIdlvUnfinished(map, rb);
		}else if(isFinished == 2) {//已审批
			result=ifsAdvIdlvMapper.searchIdlvFinished(map, rb);
		}else {//我发起的
			result=ifsAdvIdlvMapper.searchIdlvMine(map, rb);
		}
		return result;
	}

	@Override
	public void deleteIdlvById(String ticketId) {
		ifsAdvIdlvMapper.deleteByPrimaryKey(ticketId);
	}

	@Override
	public void insertIdlv(IfsAdvIdlv idlv) {
		/** 新增时将 审批状态 改为 新建 */
		idlv.setApproveStatus(DictConstants.ApproveStatus.New);
		idlv.setTicketId(getTicketId("MM"));
		ifsAdvIdlvMapper.insertSelective(idlv);
	}

	@Override
	public void updateIdlvById(IfsAdvIdlv idlv) {
		/** 修改时将 审批状态 改为 新建 */
		idlv.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsAdvIdlvMapper.updateByPrimaryKeySelective(idlv);
	}
	
	/**
	 * 
	 * @param cfetsno 本币8位机构码
	 * @param cfetscn 外币21位机构码
	 * @return
	 */
	@Override
    public String checkCust(String dealSource, String cno, String cfetsno, String cfetscn, String product, String ProdType) {
		IfsOpicsCust cust = ifsOpicsCustMapper.searchIfsOpicsCust(cno);
		String retStr ="";
		//检查交易对手会计类型
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dict_id", "CustAcctngType");
		params.put("dict_key", product + "-" + ProdType);
		TaDictVo taDict = taDictVoMapper.getTaDict(params);
		if (cust != null && taDict != null) {
			if (!taDict.getDict_value().contains(cust.getAcctngtype())) {
				retStr = "客户会计类型错误，应为 '" + taDict.getDict_value() + "'类型";
				return retStr;
			}
		}
		//从CFETS下行的交易才做映射  CFETS-OPICS交易对手映射
		if(!"1".equals(dealSource) && (StringUtils.isNotBlank(cfetsno) || StringUtils.isNotBlank(cfetscn))) {
			Map<String, Object> map = new HashMap<String, Object>();
			if(cfetsno == null && StringUtils.isNotBlank(cfetscn) && !cno.equals(cfetscn)) {
				map.put("cfetscn", cfetscn);
				List<IfsOpicsCust> list = ifsOpicsCustMapper.searchByCfets(map);
				if(list.size() > 0) {
					for (IfsOpicsCust ifsOpicsCust : list) {
						ifsOpicsCust.setCfetscn(null);
						ifsOpicsCustMapper.updateById(ifsOpicsCust);
					}
				}
				map.put("cno", cno);
				ifsOpicsCustMapper.updateByCftescn(map);
			}
			if(cfetscn == null && StringUtils.isNotBlank(cfetsno)&& !cno.equals(cfetsno)) {
				map.put("cfetsno", cfetsno);
				List<IfsOpicsCust> list = ifsOpicsCustMapper.searchByCfets(map);
				if(list.size() > 0) {
					for (IfsOpicsCust ifsOpicsCust : list) {
						ifsOpicsCust.setCfetsno(null);
						ifsOpicsCustMapper.updateById(ifsOpicsCust);
					}
				}
				map.put("cno", cno);
				ifsOpicsCustMapper.updateByCftesno(map);
			}
		}
		return retStr;
	}
}