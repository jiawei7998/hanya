package com.singlee.ifs.service;

import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.IfsNetting;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface IfsNettingService {

	public PageInfo<IfsNetting> queryNettingList(Map<String, Object> map);
	public List<IfsNetting> queryNettingSum(@RequestBody Map<String, Object> map);
	
	//付款金额统计
	public List<IfsNetting> queryNettingSumPay(@RequestBody Map<String, Object> map);
}
