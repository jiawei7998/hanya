package com.singlee.ifs.service;

import com.singlee.financial.bean.SlPmtqBean;

import java.util.List;

/**
 * 用于处理OPICS 清算信息的服务类
 * 
 * @author shenzl
 * 
 */
public interface IfsPmtqService {

	void processPmtq(List<SlPmtqBean> list);
}
