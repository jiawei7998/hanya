package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsProd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/***
 * opics 产品表 service
 *
 */
public interface IfsOpicsProdService {
		//新增
		public void insert(IfsOpicsProd entity);
		
		//修改
		void updateById(IfsOpicsProd entity);
		
		//删除
		void deleteById(String id);
		
		//分页查询
		Page<IfsOpicsProd> searchPageOpicsProd(Map<String,Object> map);
		
		public List<IfsOpicsProd> searchProd(HashMap<String, Object> map);
		
		IfsOpicsProd searchById(String id);
		
		void updateStatus(IfsOpicsProd entity);

		public SlOutBean batchCalibration(String type, String[] pcodes);
		
}
