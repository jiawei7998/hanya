package com.singlee.ifs.service.impl;

import com.singlee.capital.credit.mapper.SecurServerMapper;
import com.singlee.capital.credit.pojo.SecurServerBean;
import com.singlee.ifs.service.IfsSecurServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;




@Service(value = "IfsSecurServerService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsSecurServerServiceImpl implements IfsSecurServerService {

	@Autowired
	SecurServerMapper securServerMapper;

	
	@Override
	public SecurServerBean getAdjAmtList(Map<String, Object> map) {
		SecurServerBean securServerBean = securServerMapper.getAdjAmtList(map);
		return securServerBean;
	}


	@Override
	public void updateByDealNo(Map<String, Object> map) {
		securServerMapper.updateByDealNo(map);
		
	}


	@Override
	public void insertAdjAmtList(Map<String, Object> map) {
		securServerMapper.insertAdjAmtList(map);
		
	}


	@Override
	public void deleteByDealNo(String dealno) {
		// TODO Auto-generated method stub
		
	}
	
	
}