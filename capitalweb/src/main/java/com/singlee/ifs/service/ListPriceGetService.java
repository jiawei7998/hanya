package com.singlee.ifs.service;

public interface ListPriceGetService {
    //获取牌价信息
    boolean getPrice() throws Exception;
}
