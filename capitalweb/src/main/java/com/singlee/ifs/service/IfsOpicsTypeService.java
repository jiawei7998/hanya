package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsType;

import java.util.Map;

public interface IfsOpicsTypeService {
	// 新增
	public void insert(IfsOpicsType entity);

	// 修改
	void updateById(IfsOpicsType entity);

	// 删除
	void deleteById(Map<String, Object> map);

	// 分页查询
	Page<IfsOpicsType> searchPageOpicsType(Map<String, Object> map);

	IfsOpicsType searchById(Map<String, String> map);

	void updateStatus(IfsOpicsType entity);

	public SlOutBean batchCalibration(String type, String[] prodcodes, String[] prodtypes);
	
	String getAlByType(String type,String prodcode);

}
