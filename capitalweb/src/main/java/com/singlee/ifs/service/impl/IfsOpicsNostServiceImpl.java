package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlNostBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsNostMapper;
import com.singlee.ifs.model.IfsOpicsNost;
import com.singlee.ifs.service.IfsOpicsNostService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsNostServiceImpl implements IfsOpicsNostService {

	@Autowired
	IfsOpicsNostMapper ifsOpicsNostMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public Page<IfsOpicsNost> searchPageOpicsNost(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsNost> page = ifsOpicsNostMapper.searchPageOpicsNost(map, rb);
		return page;
	}

	@Override
	public void insert(IfsOpicsNost entity) {

		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsNostMapper.insert(entity);
	}

	@Override
	public void updateById(IfsOpicsNost entity) {

		entity.setLstmntdte(new Date());
		ifsOpicsNostMapper.updateById(entity);

	}

	@Override
	public void deleteById(String nos) {

		ifsOpicsNostMapper.deleteById(nos);

	}

	@Override
	public IfsOpicsNost searchNostById(String id) {

		return ifsOpicsNostMapper.searchNostById(id);
	}

	@Override
	public void updateStatus(IfsOpicsNost entity) {

		ifsOpicsNostMapper.updateStatus(entity);

	}

	@Override
	public SlOutBean batchCalibration(String type, String[] noss) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			List<SlNostBean> listNostOpics = iStaticServer.synchroNost();
			if ("1".equals(type)) {// 选中行校准
				for (int i = 0; i < noss.length; i++) {
					for (int j = 0; j < listNostOpics.size(); j++) {
						if (listNostOpics.get(j).getNos().trim().equals(noss[i])) {
							IfsOpicsNost entity = new IfsOpicsNost();
							entity = ifsOpicsNostMapper.searchNostById(noss[i]);
							String nos = listNostOpics.get(j).getNos().trim();
							String br = listNostOpics.get(j).getBr().trim();
							if ("".equals(br) || br == null) {
								entity.setBr("");
							} else {
								entity.setBr(br);
							}
							String cost = listNostOpics.get(j).getCost();
							if ("".equals(cost) || cost == null) {
								entity.setCost("");
							} else {
								entity.setCost(cost);
							}
							String cust = listNostOpics.get(j).getCust();
							if ("".equals(cust) || cust == null) {
								entity.setCust("");
							} else {
								entity.setCust(cust);
							}
							String ccy = listNostOpics.get(j).getCcy();
							if ("".equals(ccy) || ccy == null) {
								entity.setCcy("");
							} else {
								entity.setCcy(ccy);
							}
							Date lstmntdte = listNostOpics.get(j).getLstmntdte();
							if ("".equals(lstmntdte) || lstmntdte == null) {
								entity.setLstmntdte(new Date());

							} else {
								entity.setLstmntdte(lstmntdte);
							}
							entity.setNos(nos);
							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");
							ifsOpicsNostMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listNostOpics.size() - 1) {
							ifsOpicsNostMapper.updateStatus0ById(noss[i]);
						}
					}
				}
			} else {// 所有行
				List<IfsOpicsNost> listNostLocal = ifsOpicsNostMapper.searchAllOpicsNost();
				for (int i = 0; i < listNostOpics.size(); i++) {
					IfsOpicsNost entity = new IfsOpicsNost();
					String nos = listNostOpics.get(i).getNos().trim();
					String br = listNostOpics.get(i).getBr();
					if ("".equals(br) || br == null) {
						entity.setBr("");
					} else {
						entity.setBr(br);
					}
					String cost = listNostOpics.get(i).getCost();
					if ("".equals(cost) || cost == null) {
						entity.setCost("");
					} else {
						entity.setCost(cost);
					}
					String cust = listNostOpics.get(i).getCust();
					if ("".equals(cust) || cust == null) {
						entity.setCust("");
					} else {
						entity.setCust(cust);
					}
					String ccy = listNostOpics.get(i).getCcy();
					if ("".equals(ccy) || ccy == null) {
						entity.setCcy("");
					} else {
						entity.setCcy(ccy);
					}
					Date lstmntdte = listNostOpics.get(i).getLstmntdte();
					if ("".equals(lstmntdte) || lstmntdte == null) {

						entity.setLstmntdte(new Date());

					} else {
						entity.setLstmntdte(lstmntdte);
					}
					entity.setNos(nos);
					entity.setOperator(SlSessionHelper.getUserId());
					entity.setStatus("1");
					if (listNostLocal.size() == 0) {// 本地无数据
						ifsOpicsNostMapper.insert(entity);
					} else {// 本地有数据
						for (int j = 0; j < listNostLocal.size(); j++) {
							if (listNostOpics.get(i).getBr().trim().equals(listNostLocal.get(j).getBr()) && listNostOpics.get(i).getNos().trim().equals(listNostLocal.get(j).getNos())) {
								ifsOpicsNostMapper.updateById(entity);
								break;
							}
							// 若没有，就插入本地库
							if (j == listNostLocal.size() - 1) {
								ifsOpicsNostMapper.insert(entity);
							}
						}
					}
				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				List<IfsOpicsNost> listNostLocal2 = ifsOpicsNostMapper.searchAllOpicsNost();
				if (listNostLocal2.size() > listNostOpics.size()) {
					for (int i = 0; i < listNostLocal2.size(); i++) {
						for (int j = 0; j < listNostOpics.size(); j++) {
							if (listNostOpics.get(j).getBr().trim().equals(listNostLocal2.get(i).getBr()) && listNostOpics.get(j).getNos().trim().equals(listNostLocal2.get(i).getNos())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为未同步
							if (j == listNostOpics.size() - 1) {
								IfsOpicsNost entity = new IfsOpicsNost();
								entity.setBr(listNostLocal2.get(i).getBr());
								entity.setNos(listNostLocal2.get(i).getNos());
								entity.setCost(listNostLocal2.get(i).getCost());
								entity.setCcy(listNostLocal2.get(i).getCcy());
								entity.setCust(listNostLocal2.get(i).getCust());
								entity.setLstmntdte(new Date());
								entity.setStatus("0");// 设置状态为未同步
								entity.setOperator(SlSessionHelper.getUserId());
								ifsOpicsNostMapper.updateById(entity);
							}
						}
					}
				}
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

}
