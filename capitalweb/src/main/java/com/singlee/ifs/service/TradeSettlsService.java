package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.pojo.TradeSettlsBean;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName TradeSettlsService.java
 * @Description 外币清算路径
 * @createTime 2021年11月09日 17:07:00
 */
public interface TradeSettlsService {
    TradeSettlsBean getTradeSettls(Map<String,Object> map);

    Page<TradeSettlsBean> searchSptFwdSettle(Map<String, Object> map);

    Page<TradeSettlsBean> searchSwapSettle(Map<String, Object> map);
}
