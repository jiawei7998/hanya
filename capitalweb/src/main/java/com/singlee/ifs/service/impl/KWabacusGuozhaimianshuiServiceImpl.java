package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.KWabacusGuozhaimianshuiMapper;
import com.singlee.ifs.model.KWabacusGuozhaimianshui;
import com.singlee.ifs.service.KWabacusGuozhaimianshuiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;
@Service
public class KWabacusGuozhaimianshuiServiceImpl implements KWabacusGuozhaimianshuiService{

    @Resource
    private KWabacusGuozhaimianshuiMapper kWabacusGuozhaimianshuiMapper;


    @Override
    public Page<KWabacusGuozhaimianshui> getPageList(Map<String, Object> map) {
        return kWabacusGuozhaimianshuiMapper.getPageList(map, ParameterUtil.getRowBounds(map));
    }

    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return kWabacusGuozhaimianshuiMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(KWabacusGuozhaimianshui record) {
        return kWabacusGuozhaimianshuiMapper.insert(record);
    }

    @Override
    public int insertSelective(KWabacusGuozhaimianshui record) {
        return kWabacusGuozhaimianshuiMapper.insertSelective(record);
    }

    @Override
    public KWabacusGuozhaimianshui selectByPrimaryKey(BigDecimal id) {
        return kWabacusGuozhaimianshuiMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(KWabacusGuozhaimianshui record) {
        return kWabacusGuozhaimianshuiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(KWabacusGuozhaimianshui record) {
        return kWabacusGuozhaimianshuiMapper.updateByPrimaryKey(record);
    }

}
