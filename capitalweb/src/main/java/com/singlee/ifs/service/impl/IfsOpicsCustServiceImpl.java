package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IExternalServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.wks.intfc.AdnmMidService;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.IfsOpicsCustService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsOpicsCustServiceImpl implements IfsOpicsCustService {

	@Resource
	IfsOpicsCustMapper ifsOpicsCustMapper;

	@Autowired
	IExternalServer externalServer;

	@Autowired
	AdnmMidService adnmMidService;

	@Override
	public void insert(IfsOpicsCust entity) {
		// 设置 上次修改时间
		entity.setLstmntdate(DateUtil.getCurrentDateTimeAsString());

		//生成cno
		String custNo = adnmMidService.createCustNo();
		entity.setCno(custNo);
		/*
		 * String cno = "CNO"+DateUtil.getCurrentCompactDateTimeAsString()+ Integer.toString((int) ((Math.random() * 9 + 1) * 100)); entity.setCno(cno);
		 */
		HashMap<String, Object> map = new HashMap<String, Object>();
		String fedealNo = ifsOpicsCustMapper.getFedealNo(map);
		entity.setFedealno(fedealNo);
		entity.setLocalstatus("0"); // 未同步
		//entity.setCustclass("0"); // 客户分类
		ifsOpicsCustMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsCust entity) {
		// 设置 上次修改时间
		HashMap<String, Object> map = new HashMap<String, Object>();
		String fedealNo = ifsOpicsCustMapper.getFedealNo(map);
		entity.setFedealno(fedealNo);
		entity.setLstmntdate(DateUtil.getCurrentDateTimeAsString());
		ifsOpicsCustMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsCustMapper.deleteById(id);
	}

	@Override
	public Page<IfsOpicsCust> searchPageOpicsCust(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsCust> result = ifsOpicsCustMapper.searchPageOpicsCust(map, rb);
		return result;
	}

	@Override
	public IfsOpicsCust searchIfsOpicsCust(String cno) {
		IfsOpicsCust entity = ifsOpicsCustMapper.searchIfsOpicsCust(cno);
		return entity;
	}

	@Override
	public String getFedealNo(HashMap<String, Object> hashMap) {
		String fedealNo = ifsOpicsCustMapper.getFedealNo(hashMap);
		return fedealNo;
	}

	// 查询所有
	@Override
	public Page<IfsOpicsCust> searchAllOpicsCust() {
		Page<IfsOpicsCust> result = ifsOpicsCustMapper.searchAllOpicsCust();
		return result;
	}

	@Override
	public void updateStatus(IfsOpicsCust entity) {
		ifsOpicsCustMapper.updateStatus(entity);
	}

	/**
	 * 批量校准： 1、对选中某一行交易对手进行校准 2、对所有交易对手进行进行校准
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] cnos) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从OPICS库查询所有数据
			List<SlCustBean> listCustOpics = externalServer.synchroCust();

			/**************** 对选中的某一行进行校准 **********************/
			if ("1".equals(type)) { // 选中行校准
				for (int i = 0; i < cnos.length; i++) {
					for (int j = 0; j < listCustOpics.size(); j++) {
						// 若OPICS库中有相同的交易对手编号，就将本地的更新成OPICS中的
						if (listCustOpics.get(j).getCno().trim().equals(cnos[i])) {
							IfsOpicsCust entity = new IfsOpicsCust();
							entity = ifsOpicsCustMapper.searchById(cnos[i]);
							entity.setCno(StringUtils.trimToEmpty(listCustOpics.get(j).getCno()));
							entity.setCname(StringUtils.trimToEmpty(listCustOpics.get(j).getCmne()));
							if (listCustOpics.get(j).getBic() != null) {
								entity.setBic(StringUtils.trimToEmpty(listCustOpics.get(j).getBic()));
							}
							entity.setSic(StringUtils.trimToEmpty(listCustOpics.get(j).getSic()));
							entity.setCfn(StringUtils.trimToEmpty(listCustOpics.get(j).getCfn1()));
							entity.setSn(StringUtils.trimToEmpty(listCustOpics.get(j).getSn()));
							entity.setUccode(StringUtils.trimToEmpty(listCustOpics.get(j).getUccode()));
							entity.setCcode(StringUtils.trimToEmpty(listCustOpics.get(j).getCcode()));
							entity.setLocation(StringUtils.trimToEmpty(listCustOpics.get(j).getUccode()));
							if (listCustOpics.get(j).getCa4() != null) {
								entity.setCa(StringUtils.trimToEmpty(listCustOpics.get(j).getCa4()));
							}
							if (listCustOpics.get(j).getLstmntdte() != null) {
								entity.setLstmntdate(StringUtils.trimToEmpty(listCustOpics.get(j).getLstmntdte()));
							}
							if (listCustOpics.get(j).getCpost() != null) {
								entity.setCpost(StringUtils.trimToEmpty(listCustOpics.get(j).getCpost()));
							}
							entity.setCtype(listCustOpics.get(j).getCtype().trim());
							entity.setAcctngtype(StringUtils.trimToEmpty(listCustOpics.get(j).getAcctngtype()));
							if (listCustOpics.get(j).getTaxid() != null) {
								entity.setTaxid(StringUtils.trimToEmpty(listCustOpics.get(j).getTaxid()));
							}
							entity.setLocalstatus("1"); // 设置为已同步
							ifsOpicsCustMapper.updateLocalById(entity);
							break;
						}
						// 若OPICS库(CUST表)中无数据，则将状态改为'未同步'，其余不变
						if (j == listCustOpics.size() - 1) {
							ifsOpicsCustMapper.updateStatus0ById(cnos[i]);
						}
					}
				}

				/**************** 对本地库所有交易对手进行校准 **********************/
			} else { // 全部校准
				// 从本地库查询所有的数据
				List<IfsOpicsCust> listCustLocal = ifsOpicsCustMapper.searchAllOpicsCust();
				/** 1、以OPICS库为主，更新或插入到本地库 */
				for (int i = 0; i < listCustOpics.size(); i++) {
					// 若全部不同，则插入到本地库
					if (listCustLocal.size() == 0) { // 本地无数据
						IfsOpicsCust entity = new IfsOpicsCust();
						entity.setCno(StringUtils.trimToEmpty(listCustOpics.get(i).getCno()));
						entity.setCname(StringUtils.trimToEmpty(listCustOpics.get(i).getCmne()));
						if (listCustOpics.get(i).getBic() != null) {
							entity.setBic(StringUtils.trimToEmpty(listCustOpics.get(i).getBic()));
						}
						entity.setSic(StringUtils.trimToEmpty(listCustOpics.get(i).getSic()));
						entity.setCfn(StringUtils.trimToEmpty(listCustOpics.get(i).getCfn1()));
						entity.setSn(StringUtils.trimToEmpty(listCustOpics.get(i).getSn()));
						entity.setUccode(StringUtils.trimToEmpty(listCustOpics.get(i).getUccode()));
						entity.setCcode(StringUtils.trimToEmpty(listCustOpics.get(i).getCcode()));
						entity.setLocation(StringUtils.trimToEmpty(listCustOpics.get(i).getUccode()));
						if (listCustOpics.get(i).getCa4() != null) {
							entity.setCa(StringUtils.trimToEmpty(listCustOpics.get(i).getCa4()));
						}
						entity.setLstmntdate(StringUtils.trimToEmpty(listCustOpics.get(i).getLstmntdte()));
						if (listCustOpics.get(i).getCpost() != null) {
							entity.setCpost(StringUtils.trimToEmpty(listCustOpics.get(i).getCpost()));
						}
						entity.setCtype(StringUtils.trimToEmpty(listCustOpics.get(i).getCtype()));
						entity.setAcctngtype(StringUtils.trimToEmpty(listCustOpics.get(i).getAcctngtype()));
						if (listCustOpics.get(i).getTaxid() != null) {
							entity.setTaxid(StringUtils.trimToEmpty(listCustOpics.get(i).getTaxid()));
						}
						entity.setCliname(StringUtils.trimToEmpty(listCustOpics.get(i).getCfn1()));
						if(listCustOpics.get(i).getCfn1() != null && listCustOpics.get(i).getCfn1().contains("集团")) {
							entity.setCustclass("1");
						}else {
							entity.setCustclass("0");
						}
						entity.setLocalstatus("1"); // 设置为已同步
						ifsOpicsCustMapper.insertOpics(entity);
					} else { // 本地有数据
						for (int j = 0; j < listCustLocal.size(); j++) {
							IfsOpicsCust entity = new IfsOpicsCust();
							entity.setCno(StringUtils.trimToEmpty(listCustOpics.get(i).getCno()));
							entity.setCname(StringUtils.trimToEmpty(listCustOpics.get(i).getCmne()));
							if (listCustOpics.get(i).getBic() != null) {
								entity.setBic(StringUtils.trimToEmpty(listCustOpics.get(i).getBic()));
							}
							entity.setSic(StringUtils.trimToEmpty(listCustOpics.get(i).getSic()));
							entity.setCfn(StringUtils.trimToEmpty(listCustOpics.get(i).getCfn1()));
							entity.setSn(StringUtils.trimToEmpty(listCustOpics.get(i).getSn()));
							if (listCustOpics.get(i).getUccode() != null) {
								entity.setUccode(StringUtils.trimToEmpty(listCustOpics.get(i).getUccode()));
								entity.setLocation(StringUtils.trimToEmpty(listCustOpics.get(i).getUccode()));
							}
							entity.setCcode(StringUtils.trimToEmpty(listCustOpics.get(i).getCcode()));
							if (listCustOpics.get(i).getCa4() != null) {
								entity.setCa(StringUtils.trimToEmpty(listCustOpics.get(i).getCa4()));
							}
							if (listCustOpics.get(i).getLstmntdte() != null) {
								entity.setLstmntdate(StringUtils.trimToEmpty(listCustOpics.get(i).getLstmntdte()));
							}
							if (listCustOpics.get(i).getCpost() != null) {
								entity.setCpost(StringUtils.trimToEmpty(listCustOpics.get(i).getCpost()));
							}
							entity.setCtype(StringUtils.trimToEmpty(listCustOpics.get(i).getCtype()));
							entity.setAcctngtype(StringUtils.trimToEmpty(listCustOpics.get(i).getAcctngtype()));
							if (listCustOpics.get(i).getTaxid() != null) {
								entity.setTaxid(StringUtils.trimToEmpty(listCustOpics.get(i).getTaxid()));
							}
							entity.setLocalstatus("1"); // 设置为已同步
							// 与OPICS库中的交易对手对比，相同的内容不更新，只更新不同的内容
							if (listCustOpics.get(i).getCno().trim().equals(listCustLocal.get(j).getCno())) {
								ifsOpicsCustMapper.updateLocalById(entity);
								break;
							}
							// 若没有，则插入本地库
							if (j == listCustLocal.size() - 1) {
								entity.setCliname(StringUtils.trimToEmpty(listCustOpics.get(i).getCfn1()));
								if(listCustOpics.get(i).getCfn1() != null && listCustOpics.get(i).getCfn1().contains("集团")) {
									entity.setCustclass("1");
								}else {
									entity.setCustclass("0");
								}
								ifsOpicsCustMapper.insertOpics(entity);
							}
						}
					}
				}
				/** 2、若本地库中的数据多于OPICS库，则将本地库中含有的但OPICS库中没有的数据的状态改为'未同步' */
				// 遍历完OPICS库之后，从本地库【再次】查询所有数据，遍历本地库
				List<IfsOpicsCust> listCustLocal2 = ifsOpicsCustMapper.searchAllOpicsCust();
				if (listCustLocal2.size() > listCustOpics.size()) {
					for (int i = 0; i < listCustLocal2.size(); i++) {
						for (int j = 0; j < listCustOpics.size(); j++) {
							if (listCustOpics.get(j).getCno().trim().equals(listCustLocal2.get(i).getCno())) {
								break;
							}
							// 若OPICS库中没有，则将状态改为'未同步'
							if (j == listCustOpics.size() - 1) {
								IfsOpicsCust entity = new IfsOpicsCust();
								entity.setCno(listCustLocal2.get(i).getCno().trim());
								entity.setCname(listCustLocal2.get(i).getCname().trim());
								if (listCustLocal2.get(i).getBic() != null) {
									entity.setBic(listCustLocal2.get(i).getBic().trim());
								}
								if (listCustLocal2.get(i).getSic() != null) {
									entity.setSic(listCustLocal2.get(i).getSic().trim());
								}
								if (listCustLocal2.get(i).getCfn() != null) {
									entity.setCfn(listCustLocal2.get(i).getCfn().trim());
								}
								if (listCustLocal2.get(i).getSn() != null) {
									entity.setSn(listCustLocal2.get(i).getSn().trim());
								}
								if (listCustLocal2.get(i).getUccode() != null) {
									entity.setUccode(listCustLocal2.get(i).getUccode().trim());
								}
								if (listCustLocal2.get(i).getCcode() != null) {
									entity.setCcode(listCustLocal2.get(i).getCcode().trim());
								}
								if (listCustLocal2.get(i).getUccode() != null) {
									entity.setLocation(listCustLocal2.get(i).getUccode().trim());
								}
								if (listCustLocal2.get(i).getCa() != null) {
									entity.setCa(listCustLocal2.get(i).getCa().trim());
								}
								if (listCustLocal2.get(i).getLstmntdate() != null) {
									entity.setLstmntdate(listCustLocal2.get(i).getLstmntdate().trim());
								}
								if (listCustLocal2.get(i).getCpost() != null) {
									entity.setCpost(listCustLocal2.get(i).getCpost().trim());
								}
								if (listCustLocal2.get(i).getCtype() != null) {
									entity.setCtype(listCustLocal2.get(i).getCtype().trim());
								}
								if (listCustLocal2.get(i).getAcctngtype() != null) {
									entity.setAcctngtype(listCustLocal2.get(i).getAcctngtype().trim());
								}
								if (listCustLocal2.get(i).getTaxid() != null) {
									entity.setTaxid(listCustLocal2.get(i).getTaxid().trim());
								}
								entity.setLocalstatus("0"); // 设置为未同步
								ifsOpicsCustMapper.updateLocalById(entity);
							}
						}
					}
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			e.printStackTrace();
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/**
	 * 更新客户信息
	 */
	@Override
	public void updateCustInfo(SlCustBean slCustBean) {
		ifsOpicsCustMapper.updateCustInfo(slCustBean);
	}
	
	/**
	 * 根据客户号和交易对手编号查询某一条
	 */
	@Override
	public List<IfsOpicsCust> searchSingle(IfsOpicsCust slCustBean){
		List<IfsOpicsCust> list = ifsOpicsCustMapper.searchSingle(slCustBean);
		return list;
	}

	public String searchCnameByCno(Map<String, Object> map){
		return ifsOpicsCustMapper.searchCnameByCno(map);
	}

    @Override
    public List<Map<String,Object>> searchSingleAll(Map<String, Object> map) {
       
        List<Map<String,Object>> list = ifsOpicsCustMapper.searchSingleAll(map);
        return list;
    }

    //经办（状态由待经办1改为待复核2）
	@Override
	public void sendCust(IfsOpicsCust entity) {
		ifsOpicsCustMapper.updateById(entity);
	}
}