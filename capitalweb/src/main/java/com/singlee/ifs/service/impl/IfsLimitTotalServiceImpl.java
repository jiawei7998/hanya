package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsLimitTotalMapper;
import com.singlee.ifs.model.IfsLimitTotal;
import com.singlee.ifs.service.IfsLimitTotalService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsLimitTotalServiceImpl.java
 * @Description 限额控制
 * @createTime 2021年09月09日 16:08:00
 */
@Service(value = "IfsLimitTotalService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsLimitTotalServiceImpl implements IfsLimitTotalService {

    @Autowired
    IfsLimitTotalMapper ifsLimitTotalMapper;

    /**
     * @title 分页查询
     * @description 
     * @author  Luozb
     * @updateTime 2021/9/9 0009 16:21 
     * @throws 
     */
    @Override
    public Page<IfsLimitTotal> searchPageLimit(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsLimitTotal> result = ifsLimitTotalMapper.searchPageLimit(map, rb);
        return result;
    }

    @Override
    public void deleteByLimitId(String limitId) {
        ifsLimitTotalMapper.deleteByLimitId(limitId);
    }

    @Override
    public void insert(IfsLimitTotal entity) {
        ifsLimitTotalMapper.insert(entity);
    }

    @Override
    public void update(IfsLimitTotal entity) {
        ifsLimitTotalMapper.updateByPrimaryKey(entity);
    }
}
