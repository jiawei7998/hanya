package com.singlee.ifs.service;

import com.singlee.ifs.model.IfsOpsNssbBean;

import java.util.List;
import java.util.Map;

/***
 * 增值税送交易  纳税申报申请取数
 * 
 * @author copysun
 */
public interface IfsOpsNssbService {

	List<IfsOpsNssbBean> getData(Map<String,Object> map);

}