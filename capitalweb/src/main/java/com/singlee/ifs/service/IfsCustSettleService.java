package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCustSettle;

import java.util.List;
import java.util.Map;

public interface IfsCustSettleService {

	Page<IfsCustSettle> getCSList(Map<String, Object> map);

	void updateByCno(Map<String, Object> map);

	IfsCustSettle selectByCno(Map<String, Object> map);

	void addCustSettle(Map<String, Object> map);

	void deleteSettle(Map<String, Object> map);

	IfsCustSettle selectById(Map<String, Object> map);

	List<IfsCustSettle> getListByCno(Map<String, Object> map);

	void updateIsdefaultById(Map<String, Object> map);

	void updateDealFlagById(Map<String, Object> map);
	
}
