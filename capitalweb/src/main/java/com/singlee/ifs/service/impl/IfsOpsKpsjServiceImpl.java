package com.singlee.ifs.service.impl;

import com.singlee.ifs.mapper	.IfsOpsKpsjMapper;
import com.singlee.ifs.model.IfsOpsKpsjBean;
import com.singlee.ifs.service.IfsOpsKpsjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 增值税送交易 开票数据接口，此报表为日报，每天晚上23点生成,涉及到的交易有拆借，回购，债券三类交易。
 * @author copysun
 */
@Service
public class IfsOpsKpsjServiceImpl implements IfsOpsKpsjService {

	@Autowired
	IfsOpsKpsjMapper ifsOpsKpsjMapper;

	@Override
	public List<IfsOpsKpsjBean> getData(Map<String, Object> map) {
		ifsOpsKpsjMapper.generateData(map);
		return ifsOpsKpsjMapper.getData(map);
	}

}