package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.QryTotalDealService;
import com.singlee.refund.mapper.*;
import com.singlee.refund.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用于总交易的查询
 * 
 * @author Administrator
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class QryTotalDealServiceImpl implements QryTotalDealService {

	@Resource
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;
	@Resource
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;
	@Resource
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;
	@Resource
	IfsCfetsfxCswapMapper ifsCfetsfxCswapMapper;
	@Resource
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper;
	@Resource
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Resource
	IfsCfetsAllocateMapper ifsCfetsAllocateMapper;
	@Resource
	IfsCfetsfxOnspotMapper ifsCfetsfxOnspotMapper;
	@Resource
	IfsCfetsrmbIrsMapper cfetsrmbIrsMapper;
	@Autowired
	private IfsCfetsrmbOrMapper cfetsrmbOrMapper;
	@Autowired
	private IfsCfetsrmbCbtMapper cfetsrmbCbtMapper;
	@Autowired
	private IfsCfetsrmbIboMapper cfetsrmbIboMapper;
	@Autowired
	private IfsCfetsrmbSlMapper cfetsrmbSlMapper;
	@Autowired
	private IfsCfetsrmbCrMapper cfetsrmbCrMapper;
	@Autowired
	private IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper;
	@Autowired
	private IfsCfetsmetalGoldMapper cfetsmetalGoldMapper;
	@Autowired
	private CFtAfpMapper cFtAfpMapper;
	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;
	@Autowired
	private CFtRdpMapper cFtRdpMapper;
	@Autowired
	private CFtRdpConfMapper cFtRdpConfMapper;
	@Autowired
	private CFtReddMapper cFtReddMapper;
	@Autowired
	private CFtReinMapper cFtReinMapper;
	@Autowired
	private IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	@Autowired
	private IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
	@Autowired
	private IfsApprovermbSlMapper ifsApproverrmbSlMapper;

	/***
	 * //外汇即期
	 */
	@Override
	public Page<IfsCfetsfxSpt> searchTotalDealRMBWBJQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxSpt> page = new Page<IfsCfetsfxSpt>();
		page = ifsCfetsfxSptMapper.searchTotalDealRMBWBJQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 外汇远期
	 */
	@Override
	public Page<IfsCfetsfxFwd> searchTotalDealRMBWBYQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxFwd> page = new Page<IfsCfetsfxFwd>();
		page = ifsCfetsfxFwdMapper.searchTotalDealRMBWBYQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 外汇掉期
	 */
	@Override
	public Page<IfsCfetsfxSwap> searchTotalDealRMBWBDQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxSwap> page = new Page<IfsCfetsfxSwap>();
		page = ifsCfetsfxSwapMapper.searchTotalDealRMBWBDQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 外币拆借
	 */
	@Override
	public Page<IfsCfetsfxLend> searchTotalDealWBCQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxLend> page = new Page<IfsCfetsfxLend>();
		page = ifsCfetsfxLendMapper.searchTotalDealWBCQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 信用拆借
	 */
	@Override
	public Page<IfsCfetsrmbIbo> searchTotalDealXYCJ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbIbo> page = new Page<IfsCfetsrmbIbo>();
		page = cfetsrmbIboMapper.searchTotalDealXYCJ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 同业存款
	 */
	@Override
	public Page<IfsApprovermbDeposit> searchTotalDealTYCK(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbDeposit> page = new Page<IfsApprovermbDeposit>();
		page = ifsApprovermbDepositMapper.searchTotalDealTYCK(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 现券买卖
	 */
	@Override
	public Page<IfsCfetsrmbCbt> searchTotalDealXJMM(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbCbt> page = new Page<IfsCfetsrmbCbt>();
		page = cfetsrmbCbtMapper.searchTotalDealXJMM(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 债券远期
	 */
	@Override
	public Page<IfsCfetsrmbBondfwd> searchTotalDealZQYQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbBondfwd> page = new Page<IfsCfetsrmbBondfwd>();
		page = ifsCfetsrmbBondfwdMapper.searchTotalDealZQYQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 债券借贷
	 */
	@Override
	public Page<IfsCfetsrmbSl> searchTotalDealZJJD(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbSl> page = new Page<IfsCfetsrmbSl>();
		page = cfetsrmbSlMapper.searchTotalDealZJJD(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 债券发行
	 */
	@Override
	public Page<IfsCfetsrmbDp> searchTotalDealZQFX(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbDp> page = new Page<IfsCfetsrmbDp>();
		page = ifsCfetsrmbDpMapper.searchTotalDealZQFX(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 质押式回购
	 */
	@Override
	public Page<IfsCfetsrmbCr> searchTotalDealZYSHG(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbCr> page = new Page<IfsCfetsrmbCr>();
		page = cfetsrmbCrMapper.searchTotalDealZYSHG(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 买断式回购
	 */
	@Override
	public Page<IfsCfetsrmbOr> searchTotalDealMDSHG(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbOr> page = new Page<IfsCfetsrmbOr>();
		page = cfetsrmbOrMapper.searchTotalDealMDSHG(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 利率互换
	 */
	@Override
	public Page<IfsCfetsrmbIrs> searchTotalDealLLHH(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsrmbIrs> page = new Page<IfsCfetsrmbIrs>();
		page = cfetsrmbIrsMapper.searchTotalDealLLHH(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金申购和复核
	 */
	@Override
	public Page<CFtAfp> searchTotalDealJJSGANDFH(Map<String, Object> params, int isFinished) {
		Page<CFtAfp> page = new Page<CFtAfp>();
		params.put("dealType", isFinished);
		page = cFtAfpMapper.searchTotalDealJJSGANDFH(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金申购确认
	 */
	@Override
	public Page<CFtAfpConf> searchTotalDealJJSGQR(Map<String, Object> params, int isFinished) {
		Page<CFtAfpConf> page = new Page<CFtAfpConf>();
		page = cFtAfpConfMapper.searchTotalDealJJSGQR(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金赎回
	 */
	@Override
	public Page<CFtRdp> searchTotalDealJJSH(Map<String, Object> params, int isFinished) {
		Page<CFtRdp> page = new Page<CFtRdp>();
		page = cFtRdpMapper.searchTotalDealJJSH(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金赎回确认
	 */
	@Override
	public Page<CFtRdpConf> searchTotalDealJJSHQR(Map<String, Object> params, int isFinished) {
		Page<CFtRdpConf> page = new Page<CFtRdpConf>();
		page = cFtRdpConfMapper.searchTotalDealJJSHQR(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金现金分红
	 */
	@Override
	public Page<CFtRedd> searchTotalDealJJXJFH(Map<String, Object> params, int isFinished) {
		Page<CFtRedd> page = new Page<CFtRedd>();
		page = cFtReddMapper.searchTotalDealXJFH(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 基金红利再投
	 */
	@Override
	public Page<CFtRein> searchTotalDealJJHLZT(Map<String, Object> params, int isFinished) {
		Page<CFtRein> page = new Page<CFtRein>();
		page = cFtReinMapper.searchTotalDealHLZT(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 货币掉期
	 */
	@Override
	public Page<IfsCfetsfxCswap> searchTotalDealHBDQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxCswap> page = new Page<IfsCfetsfxCswap>();
		page = ifsCfetsfxCswapMapper.searchTotalDealHBDQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 人民币期权
	 */
	@Override
	public Page<IfsCfetsfxOption> searchTotalDealRMBQQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxOption> page = new Page<IfsCfetsfxOption>();
		page = ifsCfetsfxOptionMapper.searchTotalDealRMBQQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 外币头寸调拨
	 */
	@Override
	public Page<IfsCfetsfxAllot> searchTotalDealWBTCTB(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxAllot> page = new Page<IfsCfetsfxAllot>();
		page = ifsCfetsAllocateMapper.searchTotalDealWBTCTB(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 外汇对即期
	 */
	@Override
	public Page<IfsCfetsfxOnspot> searchTotalDealWBDJQ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsfxOnspot> page = new Page<IfsCfetsfxOnspot>();
		page = ifsCfetsfxOnspotMapper.searchTotalDealWBDJQ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/***
	 * 黄金即/远/掉期
	 */
	@Override
	public Page<IfsCfetsmetalGold> searchTotalDealHJ(Map<String, Object> params, int isFinished) {
		Page<IfsCfetsmetalGold> page = new Page<IfsCfetsmetalGold>();
		page = cfetsmetalGoldMapper.searchTotalDealHJ(params, ParameterUtil.getRowBounds(params));
		return page;
	}

	/**
	 * 债券借贷事前审批
	 * @param params
	 * @param isFinished
	 * @return
	 */
	@Override
	public Page<IfsApprovermbSl> searchTotalDealZQJDSQ(Map<String, Object> params, int isFinished) {
		Page<IfsApprovermbSl> page = new Page<IfsApprovermbSl>();
		page = ifsApproverrmbSlMapper.searchTotalDealZJJD(params, ParameterUtil.getRowBounds(params));
		return page;
	}

}
