package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IfsRevIotdMapper;
import com.singlee.ifs.model.IfsRevIotd;
import com.singlee.ifs.service.IfsRevIotdService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IfsRevIotdServiceImpl implements IfsRevIotdService {
	
	@Autowired
	private IfsRevIotdMapper ifsRevIotdMapper;

	@Override
	public void insert(IfsRevIotd entity) {
		/** 新增时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId = ifsRevIotdMapper.getId();
		entity.setTicketId(ticketId);
		ifsRevIotdMapper.insert(entity);
	}

	@Override
	public void updateById(IfsRevIotd entity) {
		ifsRevIotdMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsRevIotdMapper.deleteById(id);
	}

	@Override
	public Page<IfsRevIotd> searchPageOptParam(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsRevIotd> result = ifsRevIotdMapper.searchPageOptParam(map,rb);
		return result;
	}
	
	/***
	 * 根据请求参数查询期权冲销分页列表--我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsRevIotd> getRevIotdPage(Map<String, Object> params,int isFinished) {
		Page<IfsRevIotd> page = new Page<IfsRevIotd>();
		if (isFinished == 1) {// 待审批
			page = ifsRevIotdMapper.getRevIotdUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRevIotdMapper.getRevIotdFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRevIotdMapper.getRevIotdMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

}
