package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFlowCompleted;

import java.util.Map;

public interface IfsFlowCompletedService {
	
	
	//分页查询
	Page<IfsFlowCompleted> searchPageFlowCompleted(Map<String,Object> map);

}
