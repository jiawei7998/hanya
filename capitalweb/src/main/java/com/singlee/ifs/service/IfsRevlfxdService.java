package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIfxd;

import java.util.Map;

public interface IfsRevlfxdService {
	
	// 新增
	public void insert(IfsRevIfxd entity);

	// 修改
	void updateById(IfsRevIfxd entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsRevIfxd> searchPageForexParam(Map<String, Object> map);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author lij
	 * @date 2018-08-10
	 */
	public Page<IfsRevIfxd> getRevIfxdPage (Map<String, Object> params, int isFinished);

}
