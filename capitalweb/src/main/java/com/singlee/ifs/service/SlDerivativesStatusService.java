package com.singlee.ifs.service;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.SlDerivativesStatus;

public interface SlDerivativesStatusService {

	PageInfo<SlDerivativesStatus> querySlDerivativesStatus(Map<String, String> params);

	PageInfo<RevaluationTmaxBean> queryAllRevaluationProc(Map<String, String> params);

}
