package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTaxes;

import java.util.List;
import java.util.Map;

public interface IfsTaxesService {

	 Page<IfsTaxes> getDataPage(Map<String, Object> map);

	 void addData(List<Map<String,Object>> list);

	void update(List<Map<String,Object>> list);

	void delData(List<Map<String,Object>> list);
}
