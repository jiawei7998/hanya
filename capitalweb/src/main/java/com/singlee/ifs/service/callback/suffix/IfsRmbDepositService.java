package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbcap.service.impl.HrbCapSettleOrderCallback;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsApprovermbDepositMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(value = "IfsRmbDepositService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbDepositService extends IfsApproveServiceBase {
	@Autowired
	IfsApprovermbDepositMapper ifsApprovermbDepositMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCapSettleOrderCallback hrbCapSettleOrderCallback;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsApprovermbDepositMapper.updateAllocateByID(serial_no, status, date);
		IfsApprovermbDeposit deposit = ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == deposit) {
			return;
		}
		
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			//数据准备
			String prodType = deposit.getProduct();
			String prdName = "";
			String prdNo = "";
			if("800".equals(prodType)){
				prdName = "同业存款(线上)";
				prdNo = "IBD";
			}else if ("799".equals(prodType)){
				prdName = "同业存款(线下)";
				prdNo = "IBDOFF";
			}
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(deposit.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(deposit.getTicketId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(deposit.getApproveStatus());// 设置监控表-审批状态
			ifsFlowMonitor.setPrdName(prdName);// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo(prdNo);// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(deposit.getPostDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(deposit.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setSponsor(deposit.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = deposit.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)即交易对手
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_APPROVERMB_DEPOSIT");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_APPROVERMB_DEPOSIT where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsApprovermbDepositMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			
			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					LimitOccupy(serial_no, prodType);
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}
			
		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		//审批通过
		if(StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)){
			//同业存款--线下
			if(TradeConstants.ProductCode.IB_deposit_offline.equals(deposit.getProduct())){
				Map<String, Object> map = new HashMap<>();
				map.put("serial_no",deposit.getTicketId());
				map.put("prdNo",deposit.getProduct());
				hrbCapSettleOrderCallback.generateAcup(map);
			}
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsApprovermbDeposit ifsApprovermbDeposit = ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsApprovermbDeposit.getCustNo());
		remap.put("custType", ifsApprovermbDeposit.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsApprovermbDeposit.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsApprovermbDeposit.getSponInst());
		BigDecimal amt = calcuAmt(ifsApprovermbDeposit.getAmt(), ifsApprovermbDeposit.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsApprovermbDeposit.getPostDate());
		remap.put("mdate", ifsApprovermbDeposit.getSecondSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");

		String id = (String)paramer.get("id");
		if("jytyckxs".equals(id)){
			paramer.put("product", "800");
		}else if("jytyckxx".equals(id)){
			paramer.put("product", "799");
		}

		List<IfsApprovermbDeposit> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsApprovermbDepositMapper.getRmbDepositMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsApprovermbDepositMapper.getRmbDepositMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsApprovermbDepositMapper.getRmbDepositMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
