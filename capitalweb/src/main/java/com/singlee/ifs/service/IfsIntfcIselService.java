package com.singlee.ifs.service;

import java.util.List;
import java.util.Map;

import com.singlee.capital.choistrd.model.OpicsSecm;
import com.singlee.capital.choistrd.model.Secl;
import com.singlee.ifs.model.IfsIntfcIselBean;

public interface IfsIntfcIselService {
	
	List<IfsIntfcIselBean> getlistIsel(Map<String, Object>  map);
	
	/**
     * 市场数据 先查询，后插入接口表
     */
    String doBussExcelPropertyToDataBase(List<IfsIntfcIselBean> irevBean);

    /**
     * 市场数据 插入isel表
     */
    String dataBaseToIsel(String date);
    
    List<OpicsSecm> selectByPrimaryKey (Map<String, Object> map);

}
