package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.ChinaMutualFundDescription;

import java.util.Map;


public interface IfsWdFundAllService {
    
	public Page<ChinaMutualFundDescription> getWdFundAllList(Map<String, Object> map);
	

	
    
}
