package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IbFundPriceDay;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/10/14 16:13
 * @description：
 * @modified By：
 * @version:
 */


public interface IbFundPriceDayService {

     Page<IbFundPriceDay> getFundPricePage(Map<String, Object> map);


      void insert(IbFundPriceDay ibFundPriceDay);

      void delete(IbFundPriceDay ibFundPriceDay);

    /**
     *  先查询，后插入
     */
    String doExcelToData(List<IbFundPriceDay> fundBean);
}
