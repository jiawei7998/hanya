package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSicoBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsSicoMapper;
import com.singlee.ifs.model.IfsOpicsSico;
import com.singlee.ifs.service.IfsOpicsSicoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * SIC码
 * 
 * @author lij
 * 
 */
@Service
public class IfsOpicsSicoServiceImpl implements IfsOpicsSicoService {

	@Resource
	IfsOpicsSicoMapper ifsOpicsSicoMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsSico entity) {
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsSicoMapper.insert(entity);
	}

	@Override
	public void updateById(IfsOpicsSico entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsSicoMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsSicoMapper.deleteById(id);
	}

	@Override
	public Page<IfsOpicsSico> searchPageOpicsSico(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsSico> result = ifsOpicsSicoMapper.searchPageOpicsSico(map,
				rb);
		return result;
	}

	@Override
	public IfsOpicsSico searchById(String id) {
		return ifsOpicsSicoMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsSico entity) {
		ifsOpicsSicoMapper.updateStatus(entity);
	}

	/***
	 * 批量校准 ：
	 * 
	 * @param type
	 *            1:对选中的行进行校准 2.对本地库所有sic码进行校准
	 * @param sics
	 *            存sic的数组，用逗号隔开
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] sics) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {

			// 从opics库查询所有的 SIC 数据
			List<SlSicoBean> listSicoOpics = iStaticServer.synchroSico();
			/***** 对选中的行进行校准 ****************************/
			if ("1".equals(type)) {// 选中行校准

				for (int i = 0; i < sics.length; i++) {
					for (int j = 0; j < listSicoOpics.size(); j++) {
						// 若opics库里有相同的sic码,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if (listSicoOpics.get(j).getSic().trim()
								.equals(sics[i])) {
							IfsOpicsSico entity = new IfsOpicsSico();
							entity = ifsOpicsSicoMapper.searchById(sics[i]);
							entity.setSic(listSicoOpics.get(j).getSic().trim());
							String sd = listSicoOpics.get(j).getSd();
							if ("".equals(sd) || sd == null) {
								entity.setSd("");
							} else {
								entity.setSd(sd.trim());
							}
							Date lstmntdte = listSicoOpics.get(j)
									.getLstmntdte();
							// 若opics系统此条sic码没有设置最后维护时间,就设置最新时间
							if ("".equals(lstmntdte) || lstmntdte == null) {
								entity.setLstmntdte(new Date());
							} else {
								entity.setLstmntdte(lstmntdte);
							}

							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");// 设置为已同步
							ifsOpicsSicoMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listSicoOpics.size() - 1) {
							ifsOpicsSicoMapper.updateStatus0ById(sics[i]);
						}

					}

				}

				/************* 对本地库所有sic码记录进行校准 ***************/
			} else {

				// 从本地库查询所有的数据
				Map<String,Object> mapsic1 = new HashMap<String,Object>();
				mapsic1.put("sic", "0");
				List<IfsOpicsSico> listSicoLocal = ifsOpicsSicoMapper.searchAllOpicsSico(mapsic1);
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listSicoOpics.size(); i++) {

					/***** ++++++++++++++++++++ 实体设置 start ++++++++++++++++++++++ ***********/
					IfsOpicsSico entity = new IfsOpicsSico();
					entity.setSic(listSicoOpics.get(i).getSic().trim());

					String sd = listSicoOpics.get(i).getSd();
					if ("".equals(sd) || sd == null) {
						entity.setSd("");
					} else {
						entity.setSd(sd.trim());
					}
					Date lstmntdte = listSicoOpics.get(i).getLstmntdte();
					// 若opics系统此条sic码没有设置最后维护时间,就设置最新时间
					if ("".equals(lstmntdte) || lstmntdte == null) {
						entity.setLstmntdte(new Date());
					} else {
						entity.setLstmntdte(lstmntdte);
					}

					entity.setStatus("1");// 设置为已同步
					entity.setOperator(SlSessionHelper.getUserId());
					/***** ++++++++++++++++++++实体设置 end +++++++++++++++++++++++ ***********/

					if (listSicoLocal.size() == 0) {// 本地库无数据
						ifsOpicsSicoMapper.insert(entity);

					} else {// 本地库有数据
						for (int j = 0; j < listSicoLocal.size(); j++) {
							// opics库中sic码有与本地的sic码相等，就更新本地的sic码其他的内容
							if (listSicoOpics.get(i).getSic().trim().equals(listSicoLocal.get(j).getSic())) {
								entity.setSdcn(listSicoLocal.get(j).getSdcn());
								ifsOpicsSicoMapper.updateById(entity);
								break;
							}
							// 若没有，就插入本地库
							if (j == listSicoLocal.size() - 1) {
								ifsOpicsSicoMapper.insert(entity);
							}
						}
					}

				}

				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				// 遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				Map<String,Object> mapsic2 = new HashMap<String,Object>();
				mapsic2.put("sic", "0");
				List<IfsOpicsSico> listSicoLocal2 = ifsOpicsSicoMapper.searchAllOpicsSico(mapsic2);
				if (listSicoLocal2.size() > listSicoOpics.size()) {
					for (int i = 0; i < listSicoLocal2.size(); i++) {
						for (int j = 0; j < listSicoOpics.size(); j++) {
							if (listSicoOpics.get(j).getSic().trim()
									.equals(listSicoLocal2.get(i).getSic())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为 未同步
							if (j == listSicoOpics.size() - 1) {
								IfsOpicsSico entity = new IfsOpicsSico();
								entity.setSic(listSicoLocal2.get(i).getSic());
								entity.setSd(listSicoLocal2.get(i).getSd());
								entity.setSdcn(listSicoLocal2.get(i).getSdcn());
								entity.setRemark(listSicoLocal2.get(i)
										.getRemark());
								entity.setLstmntdte(new Date());
								entity.setStatus("0");// 设置为未同步
								entity.setOperator(SlSessionHelper.getUserId());
								ifsOpicsSicoMapper.updateById(entity);

							}

						}

					}

				}

			}

		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		
		return outBean;

	}

	// 查询所有
	@Override
	public List<IfsOpicsSico> searchAllOpicsSico() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("sic", "0");
		List<IfsOpicsSico> ifsOpicsSico = ifsOpicsSicoMapper.searchAllOpicsSico(map);
		return ifsOpicsSico;
	}

	// 查询所有(下拉框)
	@Override
	public List<IfsOpicsSico> searchAllOpicsSicoDict() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("sic", "0");
		List<IfsOpicsSico> ifsOpicsSico = ifsOpicsSicoMapper.searchAllOpicsSico(map);
		return ifsOpicsSico;
	}
}