package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCurDeposit;
import com.singlee.ifs.model.IfsCurrentDepositOut;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
public interface IfsCurrentDepositOutService {

    Page<IfsCurDeposit> getCurrentDepositOutPages(Map<String,Object> params);

    String importCurrentDeposit(List<IfsCurDeposit> records);

}
