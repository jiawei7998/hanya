package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsCustDVPMapper;
import com.singlee.ifs.model.IfsCustDVP;
import com.singlee.ifs.service.IfsCustDVPService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Liang
 * @date 2021/10/28 18:30
 * =======================
 */
@Service
public class IfsCustDVPServiceImlp implements IfsCustDVPService {

    @Autowired
    private IfsCustDVPMapper ifsCustDVPMapper;

    @Override
    public void addCustDVP(List<IfsCustDVP> list) {
        ifsCustDVPMapper.addCustDVP(list);
    }

    @Override
    public List<IfsCustDVP> selectCustDVP(Map<String,Object> map) {
        return ifsCustDVPMapper.selectCustDVPByDealno(map);
    }

    @Override
    public Page<IfsCustDVP> selectCustDVPByCno(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<IfsCustDVP> ifsCustDVP= ifsCustDVPMapper.selectCustDVP(map,rb);
        return ifsCustDVP;
    }
}
