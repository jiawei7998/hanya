package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSpos;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/17 13:53
 * @description：${description}
 * @modified By：
 * @version:
 */
public interface IfsOpicsSposService {

    int insert(IfsOpicsSpos record);

    int insertSelective(IfsOpicsSpos record);

    int batchInsert(List<IfsOpicsSpos> list);

    int deleteByPrimaryKey(String secid, String invtype, String cost, String port, String acctngtype);

    IfsOpicsSpos selectByPrimaryKey(String secid, String invtype, String cost, String port, String acctngtype);

    int updateByPrimaryKeySelective(IfsOpicsSpos record);

    int updateByPrimaryKey(IfsOpicsSpos record);

    int updateBatch(List<IfsOpicsSpos> list);

    int updateBatchSelective(List<IfsOpicsSpos> list);

    Page<IfsOpicsSpos> getSposPage(Map<String, Object> map);
}

