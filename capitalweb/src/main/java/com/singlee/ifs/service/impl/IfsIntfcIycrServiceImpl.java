package com.singlee.ifs.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.opics.model.SlInthBean;
import com.singlee.capital.opics.model.SlIrhsBean;
import com.singlee.capital.opics.model.SlIycrBean;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.marketdata.bean.RateFeedTypes;
import com.singlee.ifs.mapper.IfsIntfcIycrMapper;
import com.singlee.ifs.model.IfsIntfcIrhsBean;
import com.singlee.ifs.model.IfsIntfcIycrBean;
import com.singlee.ifs.service.IfsIntfcIycrService;
@Service
public class IfsIntfcIycrServiceImpl implements IfsIntfcIycrService {
	
	@Autowired
	IfsIntfcIycrMapper ifsIntfcIycrMapper;
	
	@Override
	public List<IfsIntfcIycrBean> getlistIycr(Map<String, Object> map) {
		List<IfsIntfcIycrBean> list=ifsIntfcIycrMapper.getIycr(map);
        return list;
	}

	@Override
	public String doBussExcelPropertyToDataBase(List<IfsIntfcIycrBean> iycrBean) {
		int seccess = 0;
        int defeat = 0;
        for (int i = 0; i < iycrBean.size(); i++) {
        	String br = iycrBean .get(i).getBr();
            String ccy = iycrBean.get(i).getCcy();
            String mtyend = iycrBean.get(i).getMtyend();
            String yieldcurve = iycrBean.get(i).getYieldcurve();
            String sourse = iycrBean.get(i).getSourse();
            String lstmntdte = iycrBean.get(i).getLstmntdte();
            IfsIntfcIycrBean iycrBean1 = new IfsIntfcIycrBean();
            iycrBean1.setBr(br);
            iycrBean1.setCcy(ccy);
            iycrBean1.setYieldcurve(yieldcurve);
            iycrBean1.setMtyend(mtyend);
            iycrBean1.setSourse(sourse);
            iycrBean1.setLstmntdte(lstmntdte);
            if (ccy != ""|| yieldcurve != "" || mtyend != "" || ccy != null || yieldcurve != null || mtyend != null) {//select(irevBean1).size() == 0)
//                if (ifsIntfcIycrMapper.selectIntfcIycr(iycrBean1) == null){ // 判断表中是否有同样数据
//                    // 若表中没有，则插入
//                	ifsIntfcIycrMapper.insertIntfcIycr(iycrBean.get(i));
//                    seccess = seccess + 1;
//                } else {
//                    defeat = defeat + 1;
//                }
                if (ifsIntfcIycrMapper.selectIntfcIycr(iycrBean1) != null) {
                	//若存在，先删除再添加
                	ifsIntfcIycrMapper.deleteIntfcIycr(iycrBean.get(i));
                }
                ifsIntfcIycrMapper.insertIntfcIycr(iycrBean.get(i));
                seccess = seccess + 1;
            } else {
                defeat = defeat + 1;
            }
        }
        if (defeat > 0) {
            return "插入失败" + defeat + "条(该收益率曲线信息已存在或没有输入市场数据)";
        }
        return "插入成功" + seccess + "条";
	}

	@Override
	public String dataBaseToIycr(String date) {
		List<SlIycrBean> iycrBeans = new ArrayList<SlIycrBean>();
        List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
        String result = "SUCCESS";
        int count = 0;
        Date postdate = null;
        // 当前系统日期
        postdate = DateUtil.parse(date);
        // ISEL表最指定日期最大流水号
        count = getMaxFedealNo(postdate, RateFeedTypes.YCRATE);
        
        Map<String, Object> rem = new HashMap<>();
        rem.put("br","01");
        rem.put("lstmntdte", date);
        List<IfsIntfcIycrBean> rateList = ifsIntfcIycrMapper.searchIycrList(rem);
        if(rateList.size() == 0) {
        	result="没有获取到导入的收益率曲线数据";
        }else{
        	for(int i = 0; i < rateList.size(); i++) {
        		if(rateList.get(i).getMidrate_8()==null || rateList.get(i).getMidrate_8().toString().equals("")) {
					continue;
				}
        		SlIycrBean iycrBean = new SlIycrBean(rem.get("br").toString());
        		iycrBean.setBr(rem.get("br").toString());//
        		iycrBean.setServer(SlDealModule.IYCR.SERVER);
            	SlInthBean inthBean = iycrBean.getInthBean();
                inthBean.setPriority("1");
                inthBean.setLstmntdate(postdate);
                count++;
             // 初始化接口对象
                inthBean.setFedealno(createFedealno(postdate, count));
                iycrBean.setFedealno(inthBean.getFedealno());
                iycrBean.setSeq(SlDealModule.IYCR.SEQ);
                iycrBean.setInoutind(SlDealModule.IYCR.INOUTIND);
                iycrBean.setRatetype(rateList.get(i).getRatetype());
                iycrBean.setCcy(rateList.get(i).getCcy());
                iycrBean.setYieldcurve(rateList.get(i).getYieldcurve());
                iycrBean.setMtystart("0M");
                iycrBean.setMtyend(rateList.get(i).getMtyend());
                iycrBean.setBidrate_8(new BigDecimal(rateList.get(i).getBidrate_8()));
                iycrBean.setMidrate_8(new BigDecimal(rateList.get(i).getMidrate_8()));
                iycrBean.setOfferrate_8(new BigDecimal(rateList.get(i).getOfferrate_8()));
                iycrBean.setLstmntdte(rateList.get(i).getLstmntdte());
                
                inthBeans.add(inthBean);
                iycrBeans.add(iycrBean);
            }
        	// 批量提交
            Boolean flag= insertToIycr(iycrBeans, inthBeans);
            
            if(flag==false) {
            	result="插入失败";
            }
        }
        
        return result;
	}
	
	public int getMaxFedealNo(Date postdate,RateFeedTypes rateFeedType) {
		int count = 0;
		String fedealno = null;
		String srtPostdate = null;
		String seq = null;
		
		srtPostdate = String.format("%1$tY%1$tm%1$td%2$s",postdate,"%");
		
		if(rateFeedType.equals(RateFeedTypes.YCRATE)) {
			fedealno = ifsIntfcIycrMapper.selectIycrMaxFedealno(null,SlDealModule.IYCR.SERVER,srtPostdate);
		}else if(rateFeedType.equals(RateFeedTypes.INTRATE)) {
			fedealno = ifsIntfcIycrMapper.selectIrhsMaxFedealno(null,SlDealModule.IRHS.SERVER,srtPostdate);
		}else
		{
			throw new RuntimeException("市场数据类型["+rateFeedType.name()+"]不支持");
		}
				
		if(org.springframework.util.StringUtils.hasText(fedealno)) {
			seq = StringUtils.trim(fedealno.substring(8));
			//查询ISEL表最大流水号
			count = Integer.parseInt(seq);
		}else {
			count = 0;
		}
		
		return count;
	}
	
	public String createFedealno(Date postdate,int count) {
		if(count > 9999999) {
			throw new RuntimeException("Opics接口表FEDEALNO字段序号最大只支持9999999!");
		}
		return String.format("%1$tY%1$tm%1$td%2$07d",postdate,count);
	}
	
	public Boolean insertToIycr(List<SlIycrBean> iycrBeans, List<SlInthBean> inthBeans) {
		Boolean flag=true;
		try {
			for(int i = 0; i < iycrBeans.size(); i++) {
				ifsIntfcIycrMapper.addIycr(iycrBeans.get(i));
				ifsIntfcIycrMapper.addSlFundInth(inthBeans.get(i));
			}
		}catch (Exception e){
			flag=false;
			JY.info("批量插入iycr异常", e);
			throw new RuntimeException(e);
		}


		return flag;
	}
	
	@Override
	public String dataBaseToIrhs(String date) {
		List<SlIrhsBean> irhsBeans = new ArrayList<SlIrhsBean>();
        List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
        String result = "SUCCESS";
        int count = 0;
        Date postdate = null;
        // 当前系统日期
        postdate = DateUtil.parse(date);
        // ISEL表最指定日期最大流水号
        count = getMaxFedealNo(postdate, RateFeedTypes.INTRATE);
        
        Map<String, Object> rem = new HashMap<>();
        rem.put("br","01");
        rem.put("lstmntdte", date);
        List<IfsIntfcIrhsBean> rateList = ifsIntfcIycrMapper.searchIrhsList(rem);
        if(rateList.size() == 0) {
        	result="没有获取到导入的定盘利率数据";
        }else{
        	for(int i = 0; i < rateList.size(); i++) {
        		if(rateList.get(i).getIntrate_8()==null || rateList.get(i).getIntrate_8().toString().equals("")) {
					continue;
				}
        		SlIrhsBean irhsBean = new SlIrhsBean(rem.get("br").toString());
        		irhsBean.setBr(rem.get("br").toString());//
        		irhsBean.setServer(SlDealModule.IRHS.SERVER);
            	SlInthBean inthBean = irhsBean.getInthBean();
                inthBean.setPriority("1");
                inthBean.setLstmntdate(postdate);
                count++;
             // 初始化接口对象
                inthBean.setFedealno(createFedealno(postdate, count));
                irhsBean.setFedealno(inthBean.getFedealno());
                irhsBean.setSeq(SlDealModule.IRHS.SEQ);
                irhsBean.setInoutind(SlDealModule.IRHS.INOUTIND);
                irhsBean.setRatecode(rateList.get(i).getRatecode());
                irhsBean.setEffdate(date);
                irhsBean.setIntrate_8(rateList.get(i).getIntrate_8());
                irhsBean.setLstmntdte(rateList.get(i).getLstmntdte());
                
                inthBeans.add(inthBean);
                irhsBeans.add(irhsBean);
            }
        	// 批量提交
            Boolean flag= insertToIrhs(irhsBeans, inthBeans);
            
            if(flag==false) {
            	result="插入失败";
            }
        }
        
        return result;
	}
	
	public Boolean insertToIrhs(List<SlIrhsBean> irhsBeans, List<SlInthBean> inthBeans) {
		Boolean flag=true;
		try {
			for(int i = 0; i < irhsBeans.size(); i++) {
				ifsIntfcIycrMapper.addIrhs(irhsBeans.get(i));
				ifsIntfcIycrMapper.addSlFundInth(inthBeans.get(i));
			}
		}catch (Exception e){
			flag=false;
			JY.info("批量插入irhs异常", e);
			throw new RuntimeException(e);
		}


		return flag;
	}

}
