package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IfsRevIfxdMapper;
import com.singlee.ifs.model.IfsRevIfxd;
import com.singlee.ifs.service.IfsRevlfxdService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IfsRevlfxdServiceImpl implements IfsRevlfxdService {
	
	@Autowired
	IfsRevIfxdMapper ifsRevlfxdMapper;

	@Override
	public void insert(IfsRevIfxd entity) {
		/** 新增时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId=ifsRevlfxdMapper.getId();
		entity.setTicketId(ticketId);
		ifsRevlfxdMapper.insert(entity);
	}

	@Override
	public void updateById(IfsRevIfxd entity) {
		/** 修改时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRevlfxdMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		
		ifsRevlfxdMapper.deleteById(id);
		
	}

	@Override
	public Page<IfsRevIfxd> searchPageForexParam(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsRevIfxd> result = ifsRevlfxdMapper.searchPageForexParam(map, rb);
		return result;
	}
	
	
	/***
	 * 根据请求参数查询外汇冲销分页列表--我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsRevIfxd> getRevIfxdPage(Map<String, Object> params,int isFinished) {
		Page<IfsRevIfxd> page = new Page<IfsRevIfxd>();
		if (isFinished == 1) {// 待审批
			page = ifsRevlfxdMapper.getRevIfxdUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRevlfxdMapper.getRevIfxdFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRevlfxdMapper.getRevIfxdMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
