package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFlowMonitor;

import java.util.Map;

public interface IfsFlowMonitorService {
	
	//新增
	public void insert(IfsFlowMonitor entity);
	
	//修改
	void updateById(IfsFlowMonitor entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsFlowMonitor> searchPageFlowMonitor(Map<String,Object> map);

	//分页查询待审及完成
	Page<IfsFlowMonitor> searchPageAllFlowMonitor(Map<String,Object> map);
	//交易审批统计
	Page<IfsFlowMonitor> searchDealCount (Map<String,Object> map);
}
