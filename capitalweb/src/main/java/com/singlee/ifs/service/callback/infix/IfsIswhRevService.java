package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSwapCrsBean;
import com.singlee.financial.opics.ISwapServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.mapper.IfsRevIswhMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import com.singlee.ifs.model.IfsRevIswh;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 互换类冲销-回调方法
 * 
 * @author rongliu
 * 
 */
@Service(value = "IfsIswhRevService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsIswhRevService extends IfsApproveServiceBase {

    @Autowired
    IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper;
	@Autowired
	IfsRevIswhMapper ifsRevIswhMapper;// 互换类冲销mapper
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	ISwapServer swapServer;

	@Autowired
	IfsCfetsrmbIrsMapper irsMapper;


	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(new Date());
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevIswhMapper.updateIswhStatusByID(serial_no, status, date);

		IfsRevIswh ifsRevIswh = new IfsRevIswh();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevIswh = ifsRevIswhMapper.searchById(map);
		IfsCfetsrmbIrs ifscfetsrmbirs = new IfsCfetsrmbIrs();
		ifscfetsrmbirs = ifsCfetsrmbIrsMapper.searchIfsCfetsrmbIrs(ifsRevIswh.getFedealno());
		if (ifsRevIswh.equals(null)) {
			return;
		}

		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}
				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				// 调用java代码
				String callType = sysList.get(0).getP_type().trim();
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					SlSwapCrsBean slSwapCrsBean = new SlSwapCrsBean(SlDealModule.BASE.BR, SlDealModule.SWAP.SERVER,
							SlDealModule.SWAP.TAG_R, SlDealModule.SWAP.DETAIL);

					SlInthBean inthBean = new SlInthBean();
					inthBean.setBr(SlDealModule.BASE.BR);
					inthBean.setServer(SlDealModule.SWAP.SERVER_R);
					inthBean.setTag(SlDealModule.SWAP.TAG_R);
					inthBean.setDetail(SlDealModule.SWAP.DETAIL);
					inthBean.setSyst(SlDealModule.SWAP.SYST);
					inthBean.setSeq(SlDealModule.SWAP.SEQ);
					inthBean.setInoutind(SlDealModule.SWAP.INOUTIND);
					inthBean.setStatcode(SlDealModule.SWAP.STATCODE);
					inthBean.setPriority(SlDealModule.SWAP.PRIORITY);
					// 设置dealno
					inthBean.setDealno(ifsRevIswh.getDealno());
					// 设置fedealno
					inthBean.setFedealno(ifsRevIswh.getFedealno());
					slSwapCrsBean.setInthBean(inthBean);
					slSwapCrsBean.setValueDate(DateUtil.parse(ifscfetsrmbirs.getStartDate()));
					slSwapCrsBean.setMaturityDate(ifscfetsrmbirs.getEndDate());
					// 插入opics冲销表
					SlOutBean result = swapServer.swapCrsRev(slSwapCrsBean);// 用注入的方式调用opics相关方法

					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
					} else {
						// 冲销成功后修改交易的状态为已冲销
						String ticketId = StringUtils.trimToEmpty(ifsRevIswh.getFedealno());
						if (!"".equals(ticketId)) {
							// 利率互换
							irsMapper.updateApproveStatusRmbIrsByID(ticketId);
						}
					}

				} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程

				} else {
					JY.raise("系统参数未配置,未进行交易导入处理......");
				}

			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}

	}
}
