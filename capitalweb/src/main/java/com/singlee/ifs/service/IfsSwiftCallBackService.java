package com.singlee.ifs.service;

import java.io.InputStream;

public interface IfsSwiftCallBackService {
/**
 * 解析并保存取回swift文件
 * @param in  传入远程取回文件输入流
 */
	public void swiftSave(InputStream in);
		
}
