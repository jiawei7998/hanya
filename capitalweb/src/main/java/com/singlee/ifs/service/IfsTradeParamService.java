package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsParaGroup;

import java.util.Map;

/***
 * 交易要素 service
 * 
 * @author singlee4
 * 
 */
public interface IfsTradeParamService {

	// 新增
	public void insert(IfsOpicsParaGroup entity);

	// 修改
	void updateById(IfsOpicsParaGroup entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsParaGroup> searchPageOpicsTrdParam(Map<String, Object> map);

}
