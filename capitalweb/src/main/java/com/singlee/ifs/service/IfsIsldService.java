package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIsld;

import java.util.Map;

public interface IfsIsldService {

	void deleteById(String ticketId);

	void insert(IfsRevIsld entity);

	void updateById(IfsRevIsld entity);

	Page<IfsRevIsld> getRevIsldPage(Map<String, Object> params, int i);

}
