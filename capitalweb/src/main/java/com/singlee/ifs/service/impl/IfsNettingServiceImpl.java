package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.mapper.IfsNettingMapper;
import com.singlee.ifs.model.IfsNetting;
import com.singlee.ifs.service.IfsNettingService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Service
public class IfsNettingServiceImpl implements IfsNettingService{

	@Autowired
	IfsNettingMapper ifsNettingMapper;
	
	@Override
	public PageInfo<IfsNetting> queryNettingList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsNetting> result = ifsNettingMapper.queryNettingList(map, rowBounds);
		return new PageInfo<IfsNetting>(result);
	}
	@Override
	public List<IfsNetting> queryNettingSum(@RequestBody Map<String, Object> map) {
		List<IfsNetting> list = ifsNettingMapper.queryNettingSum(map);
		return list;
	}
	
	//付款金额统计
	@Override
	public List<IfsNetting> queryNettingSumPay(@RequestBody Map<String, Object> map) {
		List<IfsNetting> list = ifsNettingMapper.queryNettingSumPay(map);
		return list;
	}
}
