package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApproveElement;

import java.util.List;
import java.util.Map;

/**
 * 事前审批交易要素服务类
 * 
 * ClassName: IfsApproveElementService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午08:14:24 <br/>
 * 
 * @author zhengfl
 * @version
 * @since JDK 1.6
 */
public interface IfsApproveElementService {
	// 查询
	public Page<IfsApproveElement> searchPageLimit(Map<String, Object> map);

	// 修改
	public void update(IfsApproveElement entity);

	// 新增
	public void insert(IfsApproveElement entity);

	// 删除
	public void deleteById(String id);

	public IfsApproveElement searchIfsLimitCondition(String id);

	public List<Map<String, Object>> searchColumns(Map<String, Object> map);

	public List<Map<String, Object>> searchTables(Map<String, Object> map);

}
