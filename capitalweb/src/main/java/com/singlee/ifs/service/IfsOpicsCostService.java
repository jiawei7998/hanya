package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsCost;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IfsOpicsCostService {
	// 新增
	public void insert(IfsOpicsCost entity);

	// 修改
	void updateById(IfsOpicsCost entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsCost> searchPageOpicsCost(Map<String, Object> map);

	// 下拉树查询
	public List<IfsOpicsCost> searchCost(HashMap<String, Object> map);

	IfsOpicsCost searchById(String id);

	void updateStatus(IfsOpicsCost entity);

	// 批量校准
	public SlOutBean batchCalibration(String type, String[] costcents);

}
