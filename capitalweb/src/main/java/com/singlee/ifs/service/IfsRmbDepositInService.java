package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRmbDepositin;

import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
public interface IfsRmbDepositInService {

    Page<IfsRmbDepositin> selectAdjustDepositPage(Map<String, Object> map);

    void updateDepositAdjust();

}
