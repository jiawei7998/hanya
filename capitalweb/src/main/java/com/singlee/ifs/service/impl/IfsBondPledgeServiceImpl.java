package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsBondPledgeMapper;
import com.singlee.ifs.mapper.WindSeclMapper;
import com.singlee.ifs.model.IfsBondPledgeBean;
import com.singlee.ifs.service.IfsBondPledgeService;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsBondPledgeServiceImpl implements IfsBondPledgeService {

    @Autowired
    IfsBondPledgeMapper ifsBondPledgeMapper;
    @Autowired
    WindSeclMapper windSeclMapper;

    @Override
    public Page<IfsBondPledgeBean> selectIfsCfetsrmbCrList(Map<String, Object> paramap) {
        Page<IfsBondPledgeBean> ifsBondPledgeBeans = ifsBondPledgeMapper.selectBondPledgeList(paramap, ParameterUtil.getRowBounds(paramap));
        return ifsBondPledgeBeans;
    }

    @Override
    public IfsBondPledgeBean selectBondPledgeByPrimaryKey(Map<String, Object> paramap) {
        return ifsBondPledgeMapper.selectBondPledgeByPrimaryKey(paramap);
    }

    @Override
    public void insert(IfsBondPledgeBean record) {
        ifsBondPledgeMapper.insert(record);
    }

    @Override
    public void updateByPrimaryKey(IfsBondPledgeBean record) {
        ifsBondPledgeMapper.updateByPrimaryKey(record);
    }

    @Override
    public void deleteByPrimaryKey(IfsBondPledgeBean record) {
        ifsBondPledgeMapper.deleteByPrimaryKey(record);
    }

	@SuppressWarnings("unused")
	@Override
	public Map<String, Object> saveReadBondExcelImportToOpicsService(Sheet sheet) throws Exception{

		int count = 0;
		int rows = 0;
	
		Workbook workbook = null;
		Map<String, Object> bondMap=new HashMap<String, Object>();
		Map<String, Object> countMap=new HashMap<String, Object>();
		try {
			rows = sheet.getRows();
			System.out.println("----------------" + rows);
			countMap.put("rows", rows-2);
			if (rows > 0) {
				for (int i = 1; i < rows; i++) {
					/**
					 * 判断是否已经存在这个债券，是的话不进行插入操作，增量
					 */
					if (!sheet.getCell(1, i).getContents().trim().equals("")) {
						int num = windSeclMapper.querySlIsecBndcd(sheet.getCell(1, i).getContents().trim());
						if (num<=0) {
//							bondMap.put("bndnm_cn",sheet.getCell(0, i).getContents() == null ? ""
//									: sheet.getCell(0, i).getContents().trim());// 债券中文名称
							bondMap.put("bndnm_en",sheet.getCell(0, i).getContents() == null ? ""
									: Cn2Spell.converterToSpell(
											Cn2Spell.isAllHalf(sheet.getCell(0, i).getContents().trim())));// 债券英文名称1

							bondMap.put("bndcd",sheet.getCell(1, i).getContents() == null ? ""
									: sheet.getCell(1, i).getContents().trim()); // 债券代码1

							bondMap.put("pubdt",sheet.getCell(2, i).getContents() == null ? ""
									: sheet.getCell(2, i).getContents().trim());// 发行日期1
							bondMap.put("issuevol",sheet.getCell(3, i).getContents() == null ? ""
									: sheet.getCell(3, i).getContents().trim());// 实际发行量1

							bondMap.put("intpaymethod",sheet.getCell(4, i).getContents() == null ? ""
									: Cn2Spell.converterToFirstSpell(
											Cn2Spell.isAllHalf(sheet.getCell(4, i).getContents().trim())));// 付息方式1

							bondMap.put("intpaycircle",sheet.getCell(5, i).getContents() == null ? ""
									: sheet.getCell(5, i).getContents().trim());// 付息周期1

							bondMap.put("couponrate",sheet.getCell(6, i).getContents() == null ? ""
									: String.valueOf(sheet.getCell(6, i).getContents().trim()));// 票面利率1

							bondMap.put("issudt",sheet.getCell(7, i).getContents() == null ? ""
									: sheet.getCell(7, i).getContents().trim());// 息票信息-起息日1
							bondMap.put("matdt",sheet.getCell(8, i).getContents() == null ? ""
									: sheet.getCell(8, i).getContents().trim());// 到期日1

//							bondMap.put("circuldaylist",sheet.getCell(9, i).getContents() == null ? ""
//									: sheet.getCell(9, i).getContents().trim());// 上市流通日
//
//							bondMap.put("issuenumber",sheet.getCell(10, i).getContents() == null ? ""
//									: sheet.getCell(10, i).getContents().trim());// 发行次数

							bondMap.put("bondperiod",sheet.getCell(11, i).getContents() == null ? ""
									: Cn2Spell.converterToFirstSpell(
											Cn2Spell.isAllHalf(sheet.getCell(11, i).getContents().trim())));// 债券期限1

							bondMap.put("basicspread",sheet.getCell(12, i).getContents() == null ? ""
									: sheet.getCell(12, i).getContents().trim());// 基本利差1

//							bondMap.put("curtbaserate",sheet.getCell(13, i).getContents() == null ? ""
//									: sheet.getCell(13, i).getContents().trim());// 当期基础利率

							bondMap.put("paragraphfirstplan",sheet.getCell(14, i).getContents() == null ? ""
									: sheet.getCell(14, i).getContents().trim());// 首次划款日1

//							bondMap.put("issuefeerate",sheet.getCell(15, i).getContents() == null ? ""
//									: sheet.getCell(15, i).getContents().trim());// 发行手续费率
//
//							bondMap.put("chargesagainsttaxtate",sheet.getCell(16, i).getContents() == null ? ""
//									: sheet.getCell(16, i).getContents().trim());// 兑税手续费率
//
//							bondMap.put("issueprice",sheet.getCell(17, i).getContents() == null ? ""
//									: String.valueOf((sheet.getCell(17, i).getContents().trim())));// 发行价格
//
//							bondMap.put("referenceyield",sheet.getCell(18, i).getContents() == null ? ""
//									: sheet.getCell(18, i).getContents().trim());// 参考收益率

//							bondMap.put("optioncategory",sheet.getCell(19, i).getContents() == null ? ""
//									: sheet.getCell(19, i).getContents().trim());// 选择权类别
//
//							bondMap.put("note",sheet.getCell(20, i).getContents() == null ? ""
//									: sheet.getCell(20, i).getContents().trim());// 备注
//							bondMap.put("isincode",sheet.getCell(21, i).getContents() == null ? ""
//									: sheet.getCell(21, i).getContents().trim());// ISIN码
//
//							bondMap.put("bondcreditrating",sheet.getCell(22, i).getContents() == null ? ""
//									: sheet.getCell(22, i).getContents().trim());// 债券信用评级
//
//							bondMap.put("bondcreditratingagencies",sheet.getCell(23, i).getContents() == null ? ""
//									: sheet.getCell(23, i).getContents().trim());// 债券信用评级机构
//
//							bondMap.put("maincreditrating",sheet.getCell(24, i).getContents() == null ? ""
//									: sheet.getCell(24, i).getContents().trim());// 主体信用评级
//
//							bondMap.put("maincreditratingagencies",sheet.getCell(25, i).getContents() == null ? ""
//									: sheet.getCell(25, i).getContents().trim());// 主体信用评级机构

							bondMap.put("bondproperties",sheet.getCell(26, i).getContents() == null ? ""
									: Cn2Spell.converterToFirstSpell(
											Cn2Spell.isAllHalf(sheet.getCell(26, i).getContents().trim())));// 债券品种1
//
//							bondMap.put("plancirculation",sheet.getCell(27, i).getContents() == null ? ""
//									: sheet.getCell(27, i).getContents().trim());// 计划发行量
//
//							bondMap.put("circulationlogo",sheet.getCell(28, i).getContents() == null ? ""
//									: sheet.getCell(28, i).getContents().trim());// 流通标志
//
//							bondMap.put("circulationareas",sheet.getCell(29, i).getContents() == null ? ""
//									: sheet.getCell(29, i).getContents().trim());// 流通场所
//
//							bondMap.put("firstissuescope",sheet.getCell(30, i).getContents() == null ? ""
//									: sheet.getCell(30, i).getContents().trim()); // 首次发行范围

							bondMap.put("issuer",sheet.getCell(31, i).getContents() == null ? ""
									: sheet.getCell(31, i).getContents().trim());// 发行人1

							bondMap.put("1floatratemark",sheet.getCell(32, i).getContents() == null ? ""
									: Cn2Spell.converterToFirstSpell(
											Cn2Spell.isAllHalf(sheet.getCell(32, i).getContents().trim())));// 浮动利率基准1
//
//							bondMap.put("remainprincipal",sheet.getCell(33, i).getContents() == null ? ""
//									: sheet.getCell(33, i).getContents().trim());// 剩余本金值
//
//							bondMap.put("inputdate",new SimpleDateFormat("yyyy-MM-dd HH:mmss").format(new Date()));

							this.windSeclMapper.execIsecProcedure(bondMap);
							count++;
						}

					}

				}
				countMap.put("count", count);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			/*
			 * super.setChanged(); ObserverModel observerModel = new ObserverModel();
			 * observerModel.setSecmExcelFlag(true); observerModel.setSecmExcelRows(count);
			 * observerModel.setSecmExcelRowsCount(rows); super.setChanged();
			 * notifyObservers(observerModel);
			 */
			// workbook.close();
		}
		return countMap;
	}
    
}
