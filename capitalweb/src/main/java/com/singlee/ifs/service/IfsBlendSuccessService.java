package com.singlee.ifs.service;


import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBlendSuccess;

import java.util.Map;




public interface IfsBlendSuccessService {
	//新增
	public void insert(IfsBlendSuccess entity);
	//查询
	public Page<IfsBlendSuccess> searchPageSuccess(Map<String, Object> map);
	public void deleteById(String id);

}
