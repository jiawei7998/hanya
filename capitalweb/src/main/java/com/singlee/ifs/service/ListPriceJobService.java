package com.singlee.ifs.service;

import com.singlee.ifs.model.FileNameList;

import java.util.List;
import java.util.Map;


public interface ListPriceJobService {
    
    void insert(Map<String,Object> map);

    List<FileNameList> getlist(Map<String, Object>  map);

    
    

}
