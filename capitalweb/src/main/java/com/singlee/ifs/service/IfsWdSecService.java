package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.*;

import java.util.Map;

public interface IfsWdSecService {
	public Page<IfsWdSec> getWdSecList(Map<String, Object> map);
	
	public Page<IfsWdOpicsSecmap> getWdOpicsSecmapList(Map<String, Object> map);

	public void insert(IfsWdOpicsSecmap entity);

	public void updateById(IfsWdOpicsSecmap entity);

	public void delete(String mapid);

	public Page<IfsWdSeccust> getWdSeccustList(Map<String, Object> map);
	
	/**
	 * 哈尔滨  新万得
	 * @param map
	 * @return
	 */
	public Page<IfsWindBondBean> getIfsWindBondList(Map<String, Object> map);
	
	
	
	
	   /**
     * 哈尔滨  新万得利率
     * @param map
     * @return
     */
   public Page<IfsWindRateBean> getIfsWindRateList(Map<String, Object> map);
    
    
   /**
    * 哈尔滨  万得收盘价
    * @param map
    * @return
    */
  public Page<IfsWindSeclBean> getIfsWindSeclList(Map<String, Object> map);
   
  /**
   * 哈尔滨  万得利率曲线
   * @param map
   * @return
   */
 public Page<IfsWindYcBean> getIfsWindYcList(Map<String, Object> map);
 
 
 
 
 /**
  * 哈尔滨  万得债券额外信息
  * @param map
  * @return
  */
public Page<IfsWindBondAddBean> getIfsWindBondAddList(Map<String, Object> map);


/**
 * 哈尔滨  万得债券评级
 * @param map
 * @return
 */
public Page<IfsWindSecLevelBean> getIfsWindSecLevelList(Map<String, Object> map);

/**
 * 哈尔滨  万得债券评级
 * @param map
 * @return
 */
public Page<IfsWindRiskBean> getIfsWindRiskList(Map<String, Object> map);


}
