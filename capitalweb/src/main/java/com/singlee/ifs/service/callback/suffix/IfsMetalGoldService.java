package com.singlee.ifs.service.callback.suffix;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.opics.ISlGentServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsCfetsmetalGoldMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsmetalGold;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Deprecated
@Service(value = "IfsMetalGoldService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsMetalGoldService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsmetalGoldMapper cfetsmetalGoldMapper;
	@Autowired
	private IFxdServer fxdServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	IfsOpicsCustMapper custMapper;// 用于查询 对方机构
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	UserService userService;
	@Autowired
	ISlGentServer slGentServer;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsCfetsmetalGold ifsCfetsmetalGold = new IfsCfetsmetalGold();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		cfetsmetalGoldMapper.updateMetalGoldByID(serial_no, status, date);
		ifsCfetsmetalGold = cfetsmetalGoldMapper.searchGoldspt(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsmetalGold) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
		}
		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 2.1需要考虑第一次审批和撤回再次审批。
			// 插入审批监控表，需要整合字段
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsmetalGold.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsmetalGold.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsmetalGold.getApproveStatus());// 设置监控表-审批状态
			if ("spt".equals(ifsCfetsmetalGold.getType())) {
				ifsFlowMonitor.setPrdName("黄金即期");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("goldspt");// 设置监控表-产品编号：作为分类
			} else if ("fwd".equals(ifsCfetsmetalGold.getType())) {
				ifsFlowMonitor.setPrdName("黄金远期");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("goldfwd");// 设置监控表-产品编号：作为分类
			} else {
				ifsFlowMonitor.setPrdName("黄金掉期");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("goldswap");// 设置监控表-产品编号：作为分类
			}
			ifsFlowMonitor.setAmt(
					String.valueOf(ifsCfetsmetalGold.getAmount1() == null ? "" : ifsCfetsmetalGold.getAmount1()));// 设置监控表-本金
			ifsFlowMonitor
					.setRate(String.valueOf(ifsCfetsmetalGold.getPrice() == null ? "" : ifsCfetsmetalGold.getPrice()));// 设置监控表-利率
			ifsFlowMonitor.setTradeDate(ifsCfetsmetalGold.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsmetalGold.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsmetalGold.getCounterpartyInstId();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSMETAL_GOLD");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSMETAL_GOLD where ticket_id=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = cfetsmetalGoldMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

		}
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(map1);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}
		// 黄金掉期交易
		String br = userService.getBrByUser(ifsCfetsmetalGold.getSponsor());
		if ("swap".equals(ifsCfetsmetalGold.getType())) {
			try {
				SlFxSwapBean slFxSwapBean = getFxSwapEntry(ifsCfetsmetalGold, br);
				/*
				 * //验证 SlOutBean verRet = fxdServer.verifiedfxSwap(slFxSwapBean); if
				 * (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
				 * JY.raise("验证未通过......"+verRet.getRetMsg()); }
				 */
				// 插入
				SlOutBean result = fxdServer.fxSwap(slFxSwapBean);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("审批未通过.....");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsmetalGold.getTicketId());
					map.put("satacode", "-1");
					cfetsmetalGoldMapper.updateStatcodeByTicketId(map);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				JY.raise("审批未通过ParseException......", e.getMessage());
			} catch (RemoteConnectFailureException e) {
				e.printStackTrace();
				JY.raise("审批未通过RemoteConnectFailureException......", e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
				JY.raise("审批未通过Exception......", e);
			}
		} else {
			try {
				Date branchDate = baseServer.getOpicsSysDate("01");
				SlFxSptFwdBean slFxSptFwdBean = getSptFwdEntry(ifsCfetsmetalGold, br, branchDate);
				/*
				 * //验证 SlOutBean verRet = fxdServer.verifiedfxSptFwd(slFxSptFwdBean); if
				 * (!verRet.getRetStatus().equals(RetStatusEnum.S)) {
				 * JY.raise("验证未通过......"+verRet.getRetMsg()); }
				 */
				// 插入opics表
				SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);

				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("插入opics表失败.....");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsmetalGold.getTicketId());
					map.put("satacode", "-1");
					cfetsmetalGoldMapper.updateStatcodeByTicketId(map);
				}

			} catch (ParseException e) {
				e.printStackTrace();
				JY.raise("审批未通过11......", e.getMessage());
			} catch (RemoteConnectFailureException e) {
				e.printStackTrace();
				JY.raise("审批未通过11......", e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
				JY.raise("审批未通过11......", e.getMessage());
			}

		}

	}

	/**
	 * 获取掉期
	 * 
	 * @param ifsCfetsmetalGold
	 * @param br
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	private SlFxSwapBean getFxSwapEntry(IfsCfetsmetalGold ifsCfetsmetalGold, String br)
			throws RemoteConnectFailureException, Exception {
		SlFxSwapBean slFxSwapBean = new SlFxSwapBean(br, SlDealModule.FXD.SERVER, SlDealModule.FXD.TAG,
				SlDealModule.FXD.DETAIL);
		/************* 设置交易日期、近端起息日、远端起息日 ***************************/
		slFxSwapBean.setDealDate(DateUtil.parse(ifsCfetsmetalGold.getForDate()));
		slFxSwapBean.setValueDate(DateUtil.parse(ifsCfetsmetalGold.getValueDate1()));
		slFxSwapBean.setFarValueDate(DateUtil.parse(ifsCfetsmetalGold.getValueDate2()));
		/************* 设置交易对手 ***************************/
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setTradeId(ifsCfetsmetalGold.getCounterpartyDealer());
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsmetalGold.getCounterpartyInstId());
		slFxSwapBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		/************* 设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsmetalGold.getDealer());// 交易员id
		userMap.put("branchId", ifsCfetsmetalGold.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		SlGentBean gentBean = new SlGentBean();
		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
		gentBean.setTablevalue(ifsCfetsmetalGold.getCost());
		gentBean.setBr(user.getOpicsBr());
		gentBean = slGentServer.getSlGent(gentBean);
		String trad = (gentBean != null)
				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
				: user.getOpicsTrad();
		institutionInfo.setTradeId(trad);
		slFxSwapBean.setInstitutionInfo(institutionInfo);

		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		FxCurrencyBean farCurrencyInfo = new FxCurrencyBean();
		// 本方交易币种
		// currencyInfo.setCcy(ifsCfetsmetalGold.getOpicsccy());
		String currencyPair = ifsCfetsmetalGold.getCurrencyPair();
		currencyInfo.setCcy(currencyPair.substring(0, 3));
		// 对手方交易币种
		// currencyInfo.setContraCcy(ifsCfetsmetalGold.getOpicsctrccy());
		currencyInfo.setContraCcy(currencyPair.substring(4, 7));
		// 近端数量
		Double amt = Double.valueOf(ifsCfetsmetalGold.getQuantity());
		currencyInfo.setAmt(BigDecimal.valueOf(amt));
		// 远端数量
		Double contraAmt = Double.valueOf(ifsCfetsmetalGold.getQuantity());
		currencyInfo.setContraAmt(BigDecimal.valueOf(contraAmt));
		// 近端点差：
		/*
		 * Double points=Double.valueOf(ifsCfetsmetalGold.getSpread());
		 * currencyInfo.setPoints(BigDecimal.valueOf(points));
		 * currencyInfo.setPoints(BigDecimal.valueOf(points));
		 */
		// 近端汇率
		Double rate = Double.valueOf(ifsCfetsmetalGold.getPrix());
		currencyInfo.setRate(BigDecimal.valueOf(rate));

		// 远端点差
		Double points2 = Double.valueOf(ifsCfetsmetalGold.getSpread());
		farCurrencyInfo.setPoints(BigDecimal.valueOf(points2));
		// 远端汇率
		Double rate2 = Double.valueOf(ifsCfetsmetalGold.getTenor2());

		// 新加逻辑：远端为T+2的交易，将近端点差率置为（-远端），将远端置为0；
		String res = null;
		String re = null;
		res = fxdServer.fxdCheckedTdate(ifsCfetsmetalGold.getOpicsccy(), ifsCfetsmetalGold.getOpicsctrccy());
		re = fxdServer.fxdCheckedTdate(ifsCfetsmetalGold.getOpicsctrccy(), ifsCfetsmetalGold.getOpicsccy());
		if (null != res && DateUtil.parse(res).equals(DateUtil.parse(ifsCfetsmetalGold.getValueDate2()))
				|| null != re && DateUtil.parse(re).equals(DateUtil.parse(ifsCfetsmetalGold.getValueDate2()))) {
			farCurrencyInfo.setPoints(BigDecimal.valueOf(0));
			currencyInfo.setPoints(BigDecimal.valueOf(0 - points2));
		}
		farCurrencyInfo.setRate(BigDecimal.valueOf(rate2));
		slFxSwapBean.setCurrencyInfo(currencyInfo);
		slFxSwapBean.setFarCurrencyInfo(farCurrencyInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsmetalGold.getCost());
		externalBean.setProdcode(ifsCfetsmetalGold.getProduct());
		externalBean.setProdtype(ifsCfetsmetalGold.getProdType());
		externalBean.setPort(ifsCfetsmetalGold.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifsCfetsmetalGold.getTicketId());
		externalBean.setDealtext(ifsCfetsmetalGold.getContractId());
		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
		// SIIND默认为Y
		externalBean.setSiind(SlDealModule.FXD.SIIND);
		if (null != ifsCfetsmetalGold.getSiindyn() && "0".equals(ifsCfetsmetalGold.getSiindyn())) {
			// SIIND设置为1为Y
			externalBean.setSiind(SlDealModule.FXD.SUPPAYIND);
		}
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
		externalBean.setVerind(SlDealModule.FXD.VERIND);
		/************* 20180724新增：设置清算路径，清算账户 ***************************/
		/*
		 * //清算路径1 externalBean.setCcysmeans(ifsCfetsmetalGold.getCcysmeans()); //清算账户1
		 * externalBean.setCcysacct(ifsCfetsmetalGold.getCcysacct()); //清算路径2
		 * externalBean.setCtrsmeans(ifsCfetsmetalGold.getCtrsmeans()); //清算账户2
		 * externalBean.setCtrsacct(ifsCfetsmetalGold.getCtrsacct());
		 */
		slFxSwapBean.setExternalBean(externalBean);

		SlInthBean inthBean = slFxSwapBean.getInthBean();
		inthBean.setFedealno(ifsCfetsmetalGold.getTicketId());
		inthBean.setLstmntdate(baseServer.getOpicsSysDate("01"));
		slFxSwapBean.setInthBean(inthBean);
		String ps = ifsCfetsmetalGold.getDirection1();
		if (ps.endsWith("P")) {
			slFxSwapBean.setPs(PsEnum.P);
		}
		if (ps.endsWith("S")) {
			slFxSwapBean.setPs(PsEnum.S);
		}
		slFxSwapBean.setExecId(ifsCfetsmetalGold.getTicketId());
		slFxSwapBean.setValueDate(DateUtil.parse(ifsCfetsmetalGold.getValueDate1()));
		slFxSwapBean.setFarValueDate(DateUtil.parse(ifsCfetsmetalGold.getValueDate2()));
		return slFxSwapBean;
	}

	/**
	 * 获取即远期
	 * 
	 * @param ifsCfetsmetalGold
	 * @param br
	 * @param branchDate
	 * @return
	 */
	private SlFxSptFwdBean getSptFwdEntry(IfsCfetsmetalGold ifsCfetsmetalGold, String br, Date branchDate) {
		// 黄金即远期交易
		/************* 设置交易日期、起息日期 ***************************/
		SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean(br, SlDealModule.FXD.SERVER, SlDealModule.FXD.TAG,
				SlDealModule.FXD.DETAIL);
		slFxSptFwdBean.setDealDate(DateUtil.parse(ifsCfetsmetalGold.getForDate()));
		slFxSptFwdBean.setValueDate(DateUtil.parse(ifsCfetsmetalGold.getValueDate1()));
		/************* 新增：设置交易对手 ***************************/
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setTradeId(ifsCfetsmetalGold.getCounterpartyDealer());
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsmetalGold.getCounterpartyInstId());
		slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);

		/************* 新增：设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		userMap.put("userId", ifsCfetsmetalGold.getDealer());// 交易员id
		userMap.put("branchId", inst.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		SlGentBean gentBean = new SlGentBean();
		gentBean.setTableid("SL_COST_TRAD");// 根据成本中心查询交易员
		gentBean.setTablevalue(ifsCfetsmetalGold.getCost());
		gentBean.setBr(user.getOpicsBr());
		gentBean = slGentServer.getSlGent(gentBean);
		String trad = (gentBean != null)
				? (StringUtil.isNotEmpty(gentBean.getTabletext()) ? gentBean.getTabletext() : user.getOpicsTrad())
				: user.getOpicsTrad();
		institutionInfo.setTradeId(trad);
		slFxSptFwdBean.setInstitutionInfo(institutionInfo);
		/***************** 数量 ******************************/
		FxCurrencyBean currencyInfo = new FxCurrencyBean();
		Double amt = Double.valueOf(ifsCfetsmetalGold.getQuantity());
		currencyInfo.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
		/************* 修改：设置本方币种 ***************************/
		// currencyInfo.setCcy(ifsCfetsmetalGold.getOpicsccy());
		String currencyPair = ifsCfetsmetalGold.getCurrencyPair();
		currencyInfo.setCcy(currencyPair.substring(0, 3));
		/************* 价格 ***************************/
		Double rate1 = Double.valueOf(ifsCfetsmetalGold.getPrix());
		currencyInfo.setRate(BigDecimal.valueOf(rate1));
		/************* 修改：设置对手方币种 ***************************/
		// currencyInfo.setContraCcy(ifsCfetsmetalGold.getOpicsctrccy());
		currencyInfo.setContraCcy(currencyPair.substring(4, 7));
		// 点差
		currencyInfo.setPoints(BigDecimal.valueOf(0));
		slFxSptFwdBean.setCurrencyInfo(currencyInfo);

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsmetalGold.getCost());
		externalBean.setProdcode(ifsCfetsmetalGold.getProduct());
		externalBean.setProdtype(ifsCfetsmetalGold.getProdType());
		externalBean.setPort(ifsCfetsmetalGold.getPort());

		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setCustrefno(ifsCfetsmetalGold.getTicketId());
		externalBean.setDealtext(ifsCfetsmetalGold.getContractId());
		externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
		externalBean.setSiind(SlDealModule.FXD.SIIND);
		if (null != ifsCfetsmetalGold.getSiindyn() && "0".equals(ifsCfetsmetalGold.getSiindyn())) {
			// SIIND设置为1为Y
			externalBean.setSiind(SlDealModule.FXD.SUPPAYIND);
		}
		externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
		externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
		externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
		externalBean.setVerind(SlDealModule.FXD.VERIND);
		/************* 20180724新增：设置清算路径，清算账户 ***************************/
		// 清算路径1
		/*
		 * externalBean.setCcysmeans(ifsCfetsmetalGold.getCcysmeans()); //清算账户1
		 * externalBean.setCcysacct(ifsCfetsmetalGold.getCcysacct()); //清算路径2
		 * externalBean.setCtrsmeans(ifsCfetsmetalGold.getCtrsmeans()); //清算账户2
		 * externalBean.setCtrsacct(ifsCfetsmetalGold.getCtrsacct());
		 */
		slFxSptFwdBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFxSptFwdBean.getInthBean();
		inthBean.setFedealno(ifsCfetsmetalGold.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFxSptFwdBean.setInthBean(inthBean);

		// 设置交易方向
		String ps = ifsCfetsmetalGold.getDirection1();
		if (ps.endsWith("P")) {
			slFxSptFwdBean.setPs(PsEnum.P);
		}
		if (ps.endsWith("S")) {
			slFxSptFwdBean.setPs(PsEnum.S);
		}
		slFxSptFwdBean.setExecId(ifsCfetsmetalGold.getTicketId());
		return slFxSptFwdBean;
	}
}
