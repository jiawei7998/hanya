package com.singlee.ifs.service;

import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.SlFiDefault;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SlFiDefaultService.java
 * @Description 债券违约
 * @createTime 2021年09月29日 16:57:00
 */
public interface SlFiDefaultService {
    PageInfo<SlFiDefault> searchPageSlFi(Map<String, Object> map);

    String insert(SlFiDefault entity);

    void updateById(SlFiDefault entity);

    void deleteById(SlFiDefault entity);
}
