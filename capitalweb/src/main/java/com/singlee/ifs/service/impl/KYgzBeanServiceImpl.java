package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.KYgzBeanMapper;
import com.singlee.ifs.model.KYgzBean;
import com.singlee.ifs.service.KYgzBeanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;
@Service
public class KYgzBeanServiceImpl implements KYgzBeanService{

    @Resource
    private KYgzBeanMapper kYgzBeanMapper;

    @Override
    public Page<KYgzBean> getPageList(Map<String, Object> map) {
        return kYgzBeanMapper.getPageList(map, ParameterUtil.getRowBounds(map));
    }

    @Override
    public int deleteByPrimaryKey(BigDecimal id) {
        return kYgzBeanMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(KYgzBean record) {
        return kYgzBeanMapper.insert(record);
    }

    @Override
    public int insertSelective(KYgzBean record) {
        return kYgzBeanMapper.insertSelective(record);
    }

    @Override
    public KYgzBean selectByPrimaryKey(BigDecimal id) {
        return kYgzBeanMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(KYgzBean record) {
        return kYgzBeanMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(KYgzBean record) {
        return kYgzBeanMapper.updateByPrimaryKey(record);
    }

}
