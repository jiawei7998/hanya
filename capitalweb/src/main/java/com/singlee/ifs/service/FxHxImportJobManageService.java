package com.singlee.ifs.service;

import java.util.Map;

public interface FxHxImportJobManageService {

    public boolean execute(Map<String, Object> arg0) throws Exception;
    
}
