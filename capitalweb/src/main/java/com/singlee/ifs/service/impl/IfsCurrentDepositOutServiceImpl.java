package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsCurDepositMapper;
import com.singlee.ifs.mapper.IfsCurrentDepositOutMapper;
import com.singlee.ifs.model.IfsCurDeposit;
import com.singlee.ifs.model.IfsCurrentDepositOut;
import com.singlee.ifs.service.IfsCurrentDepositOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCurrentDepositOutServiceImpl implements IfsCurrentDepositOutService {

    @Autowired
    IfsCurrentDepositOutMapper ifsCurrentDepositOutMapper;
    @Autowired
    IfsCurDepositMapper ifsCurDepositMapper;

    @Override
    public Page<IfsCurDeposit> getCurrentDepositOutPages(Map<String, Object> params) {
        return ifsCurDepositMapper.selectallPage(params, ParameterUtil.getRowBounds(params));
    }

    @Override
    public String importCurrentDeposit(List<IfsCurDeposit> records) {

        int seccess = 0;
        int oldseccess = 0;
        int defeat = 0;

        for (int i = 0; i < records.size(); i++) {
            IfsCurDeposit depositOut = records.get(i);
            if (depositOut!= null) {
                // 若表中没有，则插入
                ifsCurDepositMapper.insertSelective(records.get(i));
                seccess = seccess + 1;
            } else {
                defeat = defeat + 1;
            }
        }
        if (defeat > 0) {
            return "插入失败" + defeat + "条";
        }
        return "新增数据" + seccess + "条"+",更新数据"+oldseccess+"条";
    }
}
