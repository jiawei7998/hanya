package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlBicoBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsBicoMapper;
import com.singlee.ifs.model.IfsOpicsBico;
import com.singlee.ifs.service.IfsOpicsBicoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 工业标准码
 * 
 * @author lij
 * 
 */
@Service
public class IfsOpicsBicoServiceImpl implements IfsOpicsBicoService {
	@Resource
	IfsOpicsBicoMapper ifsOpicsBicoMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsBico entity) {
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		ifsOpicsBicoMapper.insert(entity);
	}

	@Override
	public void updateById(IfsOpicsBico entity) {
		entity.setLstmntdte(new Date());
		ifsOpicsBicoMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsBicoMapper.deleteById(id);
	}

	@Override
	public Page<IfsOpicsBico> searchPageOpicsBico(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsBico> result = ifsOpicsBicoMapper.searchPageOpicsBico(map, rb);
		return result;
	}

	@Override
	public IfsOpicsBico searchById(String id) {
		return ifsOpicsBicoMapper.searchById(id);
	}

	@Override
	public void updateStatus(IfsOpicsBico entity) {
		ifsOpicsBicoMapper.updateStatus(entity);
	}

	/***
	 * 批量校准 ：
	 * 
	 * @param type
	 *            1:对选中的行进行校准 2.对本地库所有工业标准码进行校准
	 * @param bics
	 *            存bic的数组，用逗号隔开
	 * @return 
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] bics) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从opics库查询所有的 工业标准码 数据
			List<SlBicoBean> listBicoOpics = iStaticServer.synchroBico();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			/***** 对选中的行进行校准 ****************************/
			if ("1".equals(type)) {// 选中行校准

				for (int i = 0; i < bics.length; i++) {
					for (int j = 0; j < listBicoOpics.size(); j++) {
						// 若opics库里有相同的工业代码,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if (listBicoOpics.get(j).getBic().trim().equals(bics[i])) {
							IfsOpicsBico entity = new IfsOpicsBico();
							entity = ifsOpicsBicoMapper.searchById(bics[i]);
							entity.setBic(listBicoOpics.get(j).getBic().trim());
							String sn = listBicoOpics.get(j).getSn();
							if ("".equals(sn) || sn == null) {
								entity.setSn("");
							} else {
								entity.setSn(sn.trim());
							}
							String lstmntdte = listBicoOpics.get(j).getLstmntdte();
							// 若opics系统此条工业代码没有设置最后维护时间,就设置最新时间
							if ("".equals(lstmntdte) || lstmntdte == null) {
								entity.setLstmntdte(new Date());
							} else {
								entity.setLstmntdte(sdf.parse(lstmntdte));
							}

							entity.setOperator(SlSessionHelper.getUserId());
							entity.setStatus("1");// 设置为已同步
							ifsOpicsBicoMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listBicoOpics.size() - 1) {
							ifsOpicsBicoMapper.updateStatus0ById(bics[i]);
						}

					}

				}

				/************* 对本地库所有工业代码记录进行校准 ***************/
			} else {

				// 从本地库查询所有的数据
				List<IfsOpicsBico> listBicoLocal = ifsOpicsBicoMapper.searchAllOpicsBico();
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listBicoOpics.size(); i++) {

					/***** ++++++++++++++++++++ 实体设置 start ++++++++++++++++++++++ ***********/
					IfsOpicsBico entity = new IfsOpicsBico();
					entity.setBic(listBicoOpics.get(i).getBic().trim());

					String sn = listBicoOpics.get(i).getSn();
					if ("".equals(sn) || sn == null) {
						entity.setSn("");
					} else {
						entity.setSn(sn.trim());
					}
					String lstmntdte = listBicoOpics.get(i).getLstmntdte();
					// 若opics系统此条工业代码没有设置最后维护时间,就设置最新时间
					if ("".equals(lstmntdte) || lstmntdte == null) {
						entity.setLstmntdte(new Date());
					} else {
						entity.setLstmntdte(sdf.parse(lstmntdte));
					}
					entity.setStatus("1");// 设置为已同步
					entity.setOperator(SlSessionHelper.getUserId());
					/***** ++++++++++++++++++++ 实体设置 end +++++++++++++++++++++++ ***********/

					if (listBicoLocal.size() == 0) {// 本地库无数据
						ifsOpicsBicoMapper.insert(entity);
					} else {// 本地库有数据
						for (int j = 0; j < listBicoLocal.size(); j++) {
							// opics库中工业代码有与本地的工业代码相等，就更新本地的工业代码其他的内容
							if (listBicoOpics.get(i).getBic().trim().equals(listBicoLocal.get(j).getBic())) {
								entity.setSncn(listBicoLocal.get(j).getSncn());
								ifsOpicsBicoMapper.updateById(entity);
								break;
							}
							// 若没有，就插入本地库
							if (j == listBicoLocal.size() - 1) {
								ifsOpicsBicoMapper.insert(entity);
							}
						}
					}

				}

				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				// 遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				List<IfsOpicsBico> listBicoLocal2 = ifsOpicsBicoMapper.searchAllOpicsBico();
				if (listBicoLocal2.size() > listBicoOpics.size()) {
					for (int i = 0; i < listBicoLocal2.size(); i++) {
						for (int j = 0; j < listBicoOpics.size(); j++) {
							if (listBicoOpics.get(j).getBic().trim().equals(listBicoLocal2.get(i).getBic())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为 未同步
							if (j == listBicoOpics.size() - 1) {
								IfsOpicsBico entity = new IfsOpicsBico();
								entity.setBic(listBicoLocal2.get(i).getBic());
								entity.setSn(listBicoLocal2.get(i).getSn());
								entity.setSncn(listBicoLocal2.get(i).getSncn());
								entity.setRemark(listBicoLocal2.get(i).getRemark());
								entity.setLstmntdte(new Date());
								entity.setStatus("0");// 设置为未同步
								entity.setOperator(SlSessionHelper.getUserId());
								ifsOpicsBicoMapper.updateById(entity);

							}

						}

					}

				}

			}

		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	// 查询所有
	@Override
	public List<IfsOpicsBico> searchAllOpicsBico() {
		List<IfsOpicsBico> IfsOpicsBico = ifsOpicsBicoMapper.searchAllOpicsBico();
		return IfsOpicsBico;
	}
}