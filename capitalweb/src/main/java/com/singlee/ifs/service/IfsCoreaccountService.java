package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCoreaccount;

import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCoreaccountService.java
 * @Description 日终对账
 * @createTime 2021年10月18日 11:05:00
 */
public interface IfsCoreaccountService {
    Page<IfsCoreaccount> getList(Map<String, Object> map);

    void insertList(List<IfsCoreaccount> list);

    void delByPostDate(String postdate);
}
