package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.esb.dlcb.IAcupGetServer;
import com.singlee.financial.esb.dlcb.IAcupSendServer;
import com.singlee.financial.esb.hbcb.service.S100001001700105Service;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsBatchStepMapper;
import com.singlee.ifs.model.IfsBatchStep;
import com.singlee.ifs.service.IfsOpicsAcupOperService;
import com.singlee.ifs.service.IfsOpicsAcupService;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class IfsOpicsAcupServiceImpl implements IfsOpicsAcupService, Runnable {
	@Resource
	IfsBatchStepMapper ifsBatchStepMapper;

	@Autowired
	IStaticServer iStaticServer;

	@Autowired
	private IAcupServer acupServer;

	@Autowired
	private IAcupSendServer acupSendServer;
	@Autowired
	S100001001700105Service s100001001700105Service;
	@Autowired
	IfsOpicsAcupOperService ifsOpicsAcupOperService;

	@Autowired
	private IAcupGetServer acupGetServer;

	@Autowired
	IBaseServer iBaseServer;

	@Autowired
	DayendDateService dayendDateService;

	@Resource
	TaDictVoMapper taDictVoMapper;

	// 第一步的编号,用于确定主键和查询条件
	private String firstStep = "A01";
	// 第二步编号
	private String secondStep = "A02";
	// 第三步编号
	private String thirdStep = "A03";

	private Map<String, String> jobStep = new LinkedHashMap<String, String>();

	private Map<String, String> stepStatus = new LinkedHashMap<String, String>();

	private StopWatch stopWatch = new StopWatch();

	private static Logger logManager = LoggerFactory.getLogger(IfsOpicsAcupServiceImpl.class);

	public IfsOpicsAcupServiceImpl() {
		jobStep.put("A01", "生成总账");
		jobStep.put("A02", "发送总账");
		jobStep.put("A03", "取回总账");
		stepStatus.put("pending", "等待中");
		stepStatus.put("complete", "处理完成");
	}

	@Override
	public PageInfo<IfsBatchStep> autoSendAcup(Map<String, String> map) {

		/*
		 * try { //调用OPICS批量 String opicsBsysMsg = acupServer.callOpicsBsys();
		 * logManager.info(" OPICS批量执行结果:" + opicsBsysMsg); } catch
		 * (RemoteConnectFailureException e1) { e1.printStackTrace();
		 * logManager.error(" OPICS批量执行结果:" ,e1); } catch (Exception e1) {
		 * e1.printStackTrace(); logManager.error(" OPICS批量执行结果:" ,e1); }
		 */
		stopWatch.reset();
		stopWatch.start();// 开始记录消耗时间
		SlOutBean msg = new SlOutBean();
		//String postdate = map.get("postdate");
		String postdate = map.get("effDate");
		// 2.判断是否存在批次号,如果存在则复用,不存在则新增
		maxDbTimeCheck(map);

		// 生成总账
		SlOutBean acupProcOutBean = createAcupProc(map);
		String retCode = acupProcOutBean.getRetCode();
		String retMsg = acupProcOutBean.getRetMsg();

		// 5 , 只有总账生成成功,才能发送总账
		SlAcupBean acupBean = new SlAcupBean();
		acupBean.setPostDate(postdate);
		if ("AAAAAAAAAA".equals(retCode)) {
			stopWatch.reset();// 重置消耗时间记录
			stopWatch.start();// 开始记录消耗时间
			logManager.info("==================总账生成成功==============================");
			IfsBatchStep batchStep = ifsBatchStepMapper.selectByPrimaryKey(map.get(secondStep));
			String sendCode = batchStep.getRetCode();
			String sendMsg = batchStep.getRetMsg();
			// 5.1, 要是总账发送成功 ,更新操作码
			if ("AAAAAAAAAA".equals(sendCode)) {
				// 5.2成功.更新操作码
				stopWatch.stop();// 结束记录消耗时间
				batchStep.setBatStatus("complete"); // 设置操作码
				batchStep.setBatLog("总账已发送,不允许重复发送!"); // 设置操作描述
				batchStep.setStartTime(DateUtil.format(new Date(stopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
				batchStep.setEndTime(DateUtil.format(new Date(stopWatch.getStartTime() + stopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
				batchStep.setExecTime((int) (stopWatch.getTime() / 1000)); // 运行时间
				ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表
				//0912加只要总账发送失败，就不允许再次发送
			} else if("pending".equals(sendCode)) {
				logManager.info("==================开始发送总账==============================");
				//msg = acupSendServer.acupSend(acupBean); // 发送总账A02
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("coreDate",acupBean.getPostDate());

				try {
					msg = ifsOpicsAcupOperService.acupSend(parameters);
				} catch (RemoteConnectFailureException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				sendCode = msg.getRetCode(); // 响应码
				sendMsg = msg.getRetMsg(); // 响应状态
				stopWatch.stop();// 结束记录消耗时间
				batchStep.setRetCode(sendCode); // 设置响应码
				batchStep.setRetMsg(sendMsg); // 设置响应状态
				batchStep.setBatStatus(sendCode); // 设置操作码
				batchStep.setBatLog(sendMsg); // 设置操作描述
				batchStep.setStartTime(DateUtil.format(new Date(stopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
				batchStep.setEndTime(DateUtil.format(new Date(stopWatch.getStartTime() + stopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
				batchStep.setExecTime((int) (stopWatch.getTime() / 1000)); // 运行时间
				ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表
			}

			stopWatch.reset();// 重置消耗时间记录
			stopWatch.start();// 开始记录消耗时间
			// 6, 只有总账发送成功,才能取回总账
			if ("AAAAAAAAAA".equals(sendCode)) {
				logManager.info("==================总账发送成功==============================");

				batchStep = ifsBatchStepMapper.selectByPrimaryKey(map.get(thirdStep));
				String getCode = batchStep.getRetCode();
				String getMsg = batchStep.getRetMsg();

				// 6.1, 要是总账取回成功 ,就不用取回
				if ("AAAAAAAAAA".equals(getCode)) {
					// 4.1成功.更新操作码
					stopWatch.stop();// 结束记录消耗时间
					batchStep.setBatStatus("complete"); // 设置操作码
					batchStep.setBatLog("总账已取回!"); // 设置操作描述
					batchStep.setStartTime(DateUtil.format(new Date(stopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
					batchStep.setEndTime(DateUtil.format(new Date(stopWatch.getStartTime() + stopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
					batchStep.setExecTime((int) (stopWatch.getTime() / 1000)); // 运行时间
					ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表

				} else {
					long whileSTime = System.currentTimeMillis();
					logManager.info("==================开始取回总账==============================");
					while (true) {
						stopWatch.reset();// 重置消耗时间记录
						stopWatch.start();// 开始记录消耗时间
						//msg = acupGetServer.acupGet(acupBean); // 取回总账A03

						Map<String, Object> parameters = new HashMap<String, Object>();
						parameters.put("coreDate",acupBean.getPostDate());

						try {
							msg = ifsOpicsAcupOperService.acupGet(parameters);
						} catch (RemoteConnectFailureException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						getCode = msg.getRetCode(); // 响应码
						getMsg = msg.getRetMsg(); // 响应状态
						// 6.2取回成功后，更新日期
						if ("success".equals(msg.getClientNo())) {
							Date opicsDate = null;
							try {
								/* 取OPICS系统时间 */
								opicsDate = iBaseServer.getOpicsSysDate("01");
								Map<String, String> map2 = new HashMap<String, String>();
								map2.put("RETCODE", "");
								map2.put("RETMSG", "");
							} catch (Exception e) {
								logManager.error("", e);
							}
							String date = DateUtil.format(opicsDate); // 转成字符串格式
							TtDayendDate tdd = this.dayendDateService.getTtDayendDate();
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("newStatus", ExternalDictConstants.DayendDateStatus_Normal);
							updateMap.put("newDate", date);
							updateMap.put("curDate", tdd.getCurDate());
							updateMap.put("status", tdd.getStatus());
							updateMap.put("version", Integer.valueOf(tdd.getVersion()));
							/* 更新TT_DAYEND_DATE表日期 */
							dayendDateService.updateTtDayendDate(updateMap);
						}
						stopWatch.stop();// 结束记录消耗时间
						batchStep.setRetCode(getCode); // 设置响应码
						batchStep.setRetMsg(getMsg); // 设置响应状态
						batchStep.setBatStatus(getCode); // 设置操作码
						batchStep.setBatLog(getMsg); // 设置操作描述
						batchStep.setStartTime(DateUtil.format(new Date(stopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
						batchStep.setEndTime(DateUtil.format(new Date(stopWatch.getStartTime() + stopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
						batchStep.setExecTime((int) (stopWatch.getTime() / 1000)); // 运行时间
						ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表
						long whileETime = System.currentTimeMillis();
						int whiletime = (int) (whileETime - whileSTime) / 1000 / 60; // 整个while用时多少分
						// 6.3 失败,需要间隔一段时间,再重新生成
						if ("AAAAAAAAAA".equals(getCode) || "singleeInfo".equals(getCode) || whiletime > 15) {
							// 6.4 成功.跳过,不用做任何操作
							break;
						} else {
							// 这里需要加线程
							run();
						}

					}
				}
			} else {
				logManager.info("==================总账发送失败==============================" + sendCode + sendMsg);
				logManager.info("==================执行完成AutoSendAcupJobManager==============================");
				ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表return
			}
		} else {
			logManager.info("==================总账生成失败==============================" + retCode + retMsg);
			logManager.info("==================执行完成AutoSendAcupJobManager==============================");

		}
		// 返回步骤
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsBatchStep> page = ifsBatchStepMapper.getBatchStepPage(map, rb);
		return new PageInfo<IfsBatchStep>(page);
	}

	/**
	 * 查询最新的列表
	 */
	@Override
	public PageInfo<IfsBatchStep> selectIfsBatchStep(Map<String, String> map) {

		// 判断是否存在批次号,如果存在则复用,不存在则新增
		maxDbTimeCheck(map);

		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsBatchStep> page = ifsBatchStepMapper.getBatchStepPage(map, rb);

		return new PageInfo<IfsBatchStep>(page);
	}

	@Override
	public RetMsg<SlOutBean> retrysendAcup(Map<String, String> map) {
		StopWatch retrystopWatch = new StopWatch();
		String setno = map.get("setno");
		String postdate = map.get("postdate");
		String br = map.get("br");
		// 格式化日期
		if (postdate.length() > 10) {
			postdate = postdate.substring(0, 10);
			map.put("postdate", postdate);
		}
		SlOutBean msg = new SlOutBean();
		String code = "";
		String retMsg = "";
		SlAcupBean acupBean = new SlAcupBean();
		acupBean.setRetMsg(setno);
		acupBean.setPostDate(postdate);
		// 开始时间
		retrystopWatch.reset();
		retrystopWatch.start();
		logManager.info("=================开始重发总账==============================");

		Map<String, Object> parameters = new HashMap<String, Object>();
		//parameters.put("coreDate",acupBean.getPostDate());
		parameters.put("coreDate",map.get("effDate"));
		parameters.put("setno", setno);

		try {
			msg = ifsOpicsAcupOperService.acupSend(parameters);
		} catch (RemoteConnectFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//msg = acupSendServer.acupSend(acupBean);

		code = msg.getRetCode();
		retMsg = msg.getRetMsg();
		logManager.info("=================重发总账结果: " + code + "  " + retMsg + "==============================");
		// 判断是否存在批次号,如果存在则复用,不存在则新增
		maxDbTimeCheck(map);
		// 查最新的批次号
		String maxDbTime = ifsBatchStepMapper.getMaxDbTime(map);
		// 2.根据id,查出生成总账的code
		String stepId = secondStep + br + maxDbTime;
		IfsBatchStep batchStep = ifsBatchStepMapper.selectByPrimaryKey(stepId);
		// 结束日期
		retrystopWatch.stop();
		batchStep.setBatStatus(code); // 设置操作码
		batchStep.setBatLog("重发结果:" + retMsg); // 设置操作描述
		batchStep.setStartTime(DateUtil.format(new Date(retrystopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
		batchStep.setEndTime(DateUtil.format(new Date(retrystopWatch.getStartTime() + retrystopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
		batchStep.setExecTime((int) (retrystopWatch.getTime() / 1000)); // 运行时间
		ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表
		logManager.info("==================开始取回总账==============================");

		// 判断是否执行成功,成功,不处理,失败就再执行一遍
		if ("AAAAAAAAAA".equals(code)) {
			long whileSTime = System.currentTimeMillis();
			while (true) {
				retrystopWatch.reset();// 重置消耗时间
				retrystopWatch.start();
				//msg = acupGetServer.acupGet(acupBean); // 取回总账A03

				try {
					msg = ifsOpicsAcupOperService.acupGet(parameters);
				} catch (RemoteConnectFailureException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				code = msg.getRetCode(); // 响应码
				retMsg = msg.getRetMsg(); // 响应状态
				// 2.根据id,查出生成总账的code
				String getstepId = thirdStep + br  + maxDbTime;
				IfsBatchStep getbatchStep = ifsBatchStepMapper.selectByPrimaryKey(getstepId);
				// 4.3 "AAAAAAAAAA" 和 "singleeInfo" 直接退出
				long whileETime = System.currentTimeMillis();
				int whiletime = (int) (whileETime - whileSTime) / 1000 / 60; // 整个while用时多少分

				if ("AAAAAAAAAA".equals(code) || "singleeInfo".equals(code)) {
					// 必须要使用这个错误代码,否则前台提示会有问题
					msg.setRetCode("error.common.0000");
					msg.setRetMsg("重发完成");
					retrystopWatch.stop();
					getbatchStep.setBatStatus(code); // 设置操作码
					getbatchStep.setBatLog("重发结果:" + retMsg); // 设置操作描述
					batchStep.setStartTime(DateUtil.format(new Date(retrystopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
					batchStep.setEndTime(DateUtil.format(new Date(retrystopWatch.getStartTime() + retrystopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
					batchStep.setExecTime((int) (retrystopWatch.getTime() / 1000)); // 运行时间
					ifsBatchStepMapper.updateByPrimaryKey(getbatchStep); // 更新送账日志表
					break;
				} else if (whiletime > 15) {
					// 必须要使用这个错误代码,否则前台提示会有问题
					msg.setRetCode("error.common.0000");
					msg.setRetMsg("重发超时");
					retrystopWatch.stop();
					getbatchStep.setBatStatus(code); // 设置操作码
					getbatchStep.setBatLog("重发结果:" + retMsg); // 设置操作描述
					batchStep.setStartTime(DateUtil.format(new Date(retrystopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
					batchStep.setEndTime(DateUtil.format(new Date(retrystopWatch.getStartTime() + retrystopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
					batchStep.setExecTime((int) (retrystopWatch.getTime() / 1000)); // 运行时间
					ifsBatchStepMapper.updateByPrimaryKey(getbatchStep); // 更新送账日志表
					break;
				} else {
					// 等待30秒继续执行
					run();
				}
			}
		}
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());

	}

	public String maxDbTimeCheck(Map<String, String> map) {
		StopWatch checkStopWatch = new StopWatch();
		// 判断是否存在批次号,如果存在则复用,不存在则新增
		try {
			String maxDbTime = ifsBatchStepMapper.getMaxDbTime(map);
			String br = map.get("br");
			if (maxDbTime.length() > 4) {
				for (String job : jobStep.keySet()) {
					// 0829自贸区改造
					if (job.equals(firstStep)) {
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("dict_id", "opicsBr");
						List<TaDictVo> opicsBrs = taDictVoMapper.getTaDictByDictId(params);
						if (opicsBrs != null && opicsBrs.size() > 1) {
							for (TaDictVo opicsBr : opicsBrs) {
								String opicsBrKey = opicsBr.getDict_key();
								map.put(job + opicsBrKey, job + opicsBrKey + maxDbTime);
							}
						} else {
							map.put(job + br, job + br + maxDbTime);
						}
					} else {
						map.put(job, job + br + maxDbTime);
					}
				}
				map.put("dbTime", maxDbTime);
			} else {
				// 任务代码加上这个值,可以作为批次号,
				String dbTime = DateUtil.getCurrentTimeAsString("yyyyMMddHHmmss");
				map.put("dbTime", dbTime);
				checkStopWatch.reset();
				checkStopWatch.start();

				IfsBatchStep batchStep = new IfsBatchStep();
				batchStep.setBr(map.get("br")); // 部门
				batchStep.setJobname("autoSendAcup"); // job名称
				batchStep.setJobtype("A"); // job类型
				batchStep.setBatStatus("pending"); // 执行结果
				batchStep.setBatLog("等待中");// 执行结果描述
				batchStep.setBatDate(map.get("postdate")); // 日期
				batchStep.setRetCode("pending"); // 执行结果
				batchStep.setRetMsg("等待中"); // 执行结果描述

				checkStopWatch.stop();

				batchStep.setStartTime(DateUtil.format(new Date(checkStopWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
				batchStep.setEndTime(DateUtil.format(new Date(checkStopWatch.getStartTime() + checkStopWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
				batchStep.setExecTime((int) (checkStopWatch.getTime() / 1000)); // 运行时间
				batchStep.setDbTime(dbTime); // 数据库时间

				for (String job : jobStep.keySet()) {
					// 0829自贸区改造
					if (job.equals(firstStep)) {
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("dict_id", "opicsBr");
						List<TaDictVo> opicsBrs = taDictVoMapper.getTaDictByDictId(params);
						if (opicsBrs != null && opicsBrs.size() > 1) {
							for (TaDictVo opicsBr : opicsBrs) {
								String opicsBrKey = opicsBr.getDict_key();
								batchStep.setBr(opicsBrKey); // 部门
								batchStep.setId(job + opicsBrKey + dbTime); // 主键
								batchStep.setBatId(job); // 步骤编号
								batchStep.setBatName(jobStep.get(job)); // 步骤名称
								ifsBatchStepMapper.insert(batchStep);
								map.put(job + opicsBrKey, job + opicsBrKey + dbTime);
							}
						} else {
							batchStep.setBr(br); // 部门
							batchStep.setId(job + br + dbTime); // 主键
							batchStep.setBatId(job); // 步骤编号
							batchStep.setBatName(jobStep.get(job)); // 步骤名称
							ifsBatchStepMapper.insert(batchStep);
							map.put(job + br, job + br + dbTime);
						}
					} else {
						batchStep.setBr(br); // 部门
						batchStep.setId(job + br + dbTime); // 主键
						batchStep.setBatId(job); // 步骤编号
						batchStep.setBatName(jobStep.get(job)); // 步骤名称
						ifsBatchStepMapper.insert(batchStep);
						map.put(job, job + br + dbTime);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logManager.error("创建批次号异常:", e);
		}
		return "yes";
	}

	@Override
	public String dateCheck(Map<String, String> params) {
		try {
			// 系统时间
			// String curDate =
			// DayendDateServiceProxy.getInstance().getSettlementDate();
			// String opicsDate = acupServer.getAcupDate(params);
			// opics 日期
			// Date opicsDate =iBaseServer.getOpicsSysDate("01");
			// opcis上一跑批日
			Date opicsDate = DateUtils.parseDate(acupServer.getAcupDate(params), "yyyy-MM-dd");

			// 所选日期
			// String postdate = params.get("postdate");
			Date postdate = DateUtils.parseDate(params.get("postdate"), "yyyy-MM-dd");

			if (postdate.after(opicsDate)) {
				// 如果所选日期postdate大于opcis上一跑批日相同不能生成和发送,前台根据返回码,控制按钮不可点击
				return "error";
			} else if (opicsDate.equals(postdate)) {
				// 如果所选日期postdate和opcis上一跑批日相同是可以发送的
				return "yes";
			} else {
				// 如果所选日期postdate小于opcis上一跑批日相同是可以发送的,前台提醒
				return "warn";
			}

		} catch (RemoteConnectFailureException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 前台通过这个返回码进行判断,不允许修改
		return "no";
	}

	@Override
	public RetMsg<SlOutBean> saveAcupDetail(List<AcupSendBean> acupBeans) {

		SlOutBean msg = new SlOutBean();
		for (AcupSendBean slAcupBean : acupBeans) {
			try {
				acupServer.updateAcup(slAcupBean);
			} catch (RemoteConnectFailureException e) {
				msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + "Cannot connect to Hessian");
				msg.setRetStatus(RetStatusEnum.F);
				logManager.error(" singlee: ", e);
				e.printStackTrace();
			} catch (Exception e) {
				msg.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
				msg.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + "Cannot connect to Hessian");
				msg.setRetStatus(RetStatusEnum.F);
				logManager.error(" singlee: ", e);
				e.printStackTrace();
			}
		}
		return new RetMsg<SlOutBean>(msg.getRetCode(), msg.getRetMsg());
	}

	@Override
	public void run() {
		try {
			// 等30秒钟之后,再判断
			logManager.info("进入循环等待队列,等待时间为30s");
			TimeUnit.MILLISECONDS.sleep(30000);
			logManager.info("进入循环等待队列,等待完成!");
		} catch (InterruptedException e) {
			logManager.error(" singlee: ", e);
			e.printStackTrace();
		}
	}

	@Override
	public SlOutBean createAcupProc(Map<String, String> map) {
		SlOutBean acupProcmsg = new SlOutBean();
		StopWatch acupProcWatch = new StopWatch();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dict_id", "opicsBr");
		List<TaDictVo> opicsBrs = taDictVoMapper.getTaDictByDictId(params);
		if (opicsBrs != null && opicsBrs.size() > 0) {
			// 0829自贸区改造，需要每个机构都生成账务成功，才能给核心发送账务。
			int brNumber = 1;
			for (TaDictVo opicsBr : opicsBrs) {
				String br = opicsBr.getDict_key();
				acupProcWatch.reset();// 重置消耗时间记录
				acupProcWatch.start();// 开始记录消耗时间
				brNumber++; // 部门数每次循环都+1
				// 1,根据id,查出生成总账的code,进行判断
				IfsBatchStep batchStep = ifsBatchStepMapper.selectByPrimaryKey(map.get(firstStep + br));
				String retCode = batchStep.getRetCode();
				String retMsg = batchStep.getRetMsg();
				acupProcmsg.setRetCode(retCode);
				acupProcmsg.setRetMsg(retMsg);
				logManager.info("====opics部门=====" + br + "===总账步骤ID=====" + map.get(firstStep + br));
				// 1.判断总账是否生成成功
				if ("AAAAAAAAAA".equals(retCode)) {
					// 成功.更新操作码
					acupProcWatch.stop();// 时间消耗记录结束
					batchStep.setBatStatus("complete"); // 设置操作码
					batchStep.setBatLog("总账已发送,不允许重复生成!"); // 设置操作描述
					batchStep.setStartTime(DateUtil.format(new Date(acupProcWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
					batchStep.setEndTime(DateUtil.format(new Date(acupProcWatch.getStartTime() + acupProcWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
					batchStep.setExecTime((int) (acupProcWatch.getTime() / 1000)); // 运行时间
					ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表

				} else {
					// 失败,需要生成
					long whileSTime = System.currentTimeMillis();
					logManager.info("==================开始生成总账==============================");
					acupProcWatch.reset();// 重置消耗时间记录
					acupProcWatch.start();// 开始记录消耗时间
					map.put("br", br); // 需要更新opics部门
					acupProcmsg = acupServer.createAcupProc(map); // 生成总账
					retCode = acupProcmsg.getRetCode(); // 响应码
					retMsg = acupProcmsg.getRetMsg(); // 响应状态
					acupProcWatch.stop();// 时间消耗记录结束
					batchStep.setRetCode(retCode); // 设置响应码
					batchStep.setRetMsg(retMsg); // 设置响应状态
					batchStep.setBatStatus(retCode); // 设置操作码
					batchStep.setBatLog(retMsg); // 设置操作描述
					batchStep.setStartTime(DateUtil.format(new Date(acupProcWatch.getStartTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 开始时间
					batchStep.setEndTime(DateUtil.format(new Date(acupProcWatch.getStartTime() + acupProcWatch.getTime()), "yyyy-MM-dd HH:mm:ss SSS")); // 结束时间
					batchStep.setExecTime((int) (acupProcWatch.getTime() / 1000)); // 运行时间
					ifsBatchStepMapper.updateByPrimaryKey(batchStep); // 更新送账日志表
					// 要是修改状态码 singleeInfo 需要修改 ledgerAutoSendManage.jsp
					long whileETime = System.currentTimeMillis();
					int whiletime = (int) (whileETime - whileSTime) / 1000 / 60; // 整个while用时多少分

					if ("singleeInfo".equals(retCode)) {

						break;
					}
					if (whiletime > 10) {
						acupProcmsg.setRetCode("singleeInfo");
						acupProcmsg.setRetMsg("生成总账超时" + br);
						logManager.info("====生成总账超时==" + br);
						break;
					}
					if ("AAAAAAAAAA".equals(retCode)) {
						// 成功.跳过,生成下一个部门的数据，如果最后一个部门也成功，就整个方法返回成功。
						if (brNumber > opicsBrs.size()) {
							break;
						} else {
							continue;
						}
					} else if ("ProcSButNoData".equals(retCode)) {
						// 存储过程执行成功，但是没有数据，需要更新日期，更新状态码，
						// ProcSuccessButNoData
						if (brNumber > opicsBrs.size()) {
							logManager.info("==================存储过程执行成功，但是没有数据，需要更新日期 ProcSuccessButNoData==============================");
							Date opicsDate = null;
							try {
								/* 取OPICS系统时间 */
								opicsDate = iBaseServer.getOpicsSysDate("01");
								Map<String, String> map2 = new HashMap<String, String>();
								map2.put("RETCODE", "");
								map2.put("RETMSG", "");
							} catch (Exception e) {
								logManager.error("", e);
							}
							String date = DateUtil.format(opicsDate); // 转成字符串格式
							TtDayendDate tdd = this.dayendDateService.getTtDayendDate();
							Map<String, Object> updateMap = new HashMap<String, Object>();
							updateMap.put("newStatus", ExternalDictConstants.DayendDateStatus_Normal);
							updateMap.put("newDate", date);
							updateMap.put("curDate", tdd.getCurDate());
							updateMap.put("status", tdd.getStatus());
							updateMap.put("version", Integer.valueOf(tdd.getVersion()));
							/* 更新TT_DAYEND_DATE表日期 */
							dayendDateService.updateTtDayendDate(updateMap);
							logManager.info("==================更新TT_DAYEND_DATE表日期==============================");
							for (String job : jobStep.keySet()) {
								if("A01".equals(job)){
									IfsBatchStep batchStepUP = ifsBatchStepMapper.selectByPrimaryKey(map.get(job+br));
									batchStepUP.setRetCode("AAAAAAAAAA");
									batchStepUP.setRetMsg("当天没有账务数据");
									ifsBatchStepMapper.updateByPrimaryKey(batchStepUP); // 更新送账日志表
								}else{
									IfsBatchStep batchStepUP = ifsBatchStepMapper.selectByPrimaryKey(map.get(job));
									batchStepUP.setRetCode("AAAAAAAAAA");
									batchStepUP.setRetMsg("当天没有账务数据");
									ifsBatchStepMapper.updateByPrimaryKey(batchStepUP); // 更新送账日志表
								}
							}
							batchStep.setRetCode("AAAAAAAAAA");
							batchStep.setRetMsg("当天没有账务数据" + batchStep.getRetMsg());
							// 需要直接返回，不能继续执行！
							break;
						} else {
							//没有数据，需要更新状态码,不能发空文件。
							IfsBatchStep batchStepUP = ifsBatchStepMapper.selectByPrimaryKey(map.get(firstStep + br));
							batchStepUP.setRetCode("AAAAAAAAAA");
							batchStepUP.setRetMsg("当天没有账务数据");
							ifsBatchStepMapper.updateByPrimaryKey(batchStepUP); // 更新送账日志表
							continue;
						}
					}
				}
			}
		} else {
			acupProcmsg.setRetCode("singleeInfo");
			acupProcmsg.setRetMsg("请在数据字典中添加【dict_id】为 【opicsBr】的数据");
			logManager.info("==================请在数据字典中添加【dict_id】为 【opicsBr】的数据==============================");
			return acupProcmsg;
		}
		return acupProcmsg;
	}

}
