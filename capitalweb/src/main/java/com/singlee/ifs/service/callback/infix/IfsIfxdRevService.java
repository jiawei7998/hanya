package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlFxSptFwdBean;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsRevIfxd;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import com.singlee.slbpm.dict.DictConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 外汇类冲销-回调方法
 * 
 * @author lij
 * 
 */
@Service(value = "IfsIfxdRevService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsIfxdRevService extends IfsApproveServiceBase {

	@Autowired
	IfsRevIfxdMapper ifsRevIfxdMapper;// 外汇类冲销mapper

	@Autowired
	IFxdServer fxdServer;//

	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsCfetsfxSptMapper sptMapper;
	@Autowired
	IfsCfetsfxFwdMapper fwdMapper;
	@Autowired
	IfsCfetsfxSwapMapper swapMapper;
	@Autowired
	IfsCfetsmetalGoldMapper metalGoldMapper;

	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {

		IfsRevIfxd ifsRevIfxd = new IfsRevIfxd();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevIfxd = ifsRevIfxdMapper.searchById(map);

		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevIfxdMapper.updateIfxdStatusByID(serial_no, status, date);

		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				if (ifsRevIfxd.equals(null)) {
					return;

				}
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}

				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				// 调用java代码
				String callType = sysList.get(0).getP_type().trim();
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean(SlDealModule.BASE.BR, SlDealModule.FXD.SERVER,
							SlDealModule.FXD.TAG_R, SlDealModule.FXD.DETAIL);
					SlInthBean inthBean = new SlInthBean();
					inthBean.setBr(SlDealModule.BASE.BR);
					inthBean.setServer(SlDealModule.FXD.SERVER_R);
					inthBean.setTag(SlDealModule.FXD.TAG_R);
					inthBean.setDetail(SlDealModule.FXD.DETAIL);
					inthBean.setSyst(SlDealModule.FXD.SYST);
					inthBean.setSeq(SlDealModule.FXD.SEQ);
					inthBean.setInoutind(SlDealModule.FXD.INOUTIND);
					inthBean.setStatcode(SlDealModule.FXD.STATCODE);
					inthBean.setPriority(SlDealModule.FXD.PRIORITY);
					// 设置dealno
					inthBean.setDealno(ifsRevIfxd.getDealNo());
					// 设置fedealno
					inthBean.setFedealno(ifsRevIfxd.getFedealno());
					slFxSptFwdBean.setInthBean(inthBean);
					// 插入opics冲销表
					SlOutBean result = fxdServer.fxSptFwdRev(slFxSptFwdBean);// 用注入的方式调用opics相关方法
					if (!result.getRetStatus().equals(RetStatusEnum.S)) {
						JY.raise(result.getRetMsg());
					} else {
						/*
						 * Map<String, Object> map = new HashMap<String, Object>(); map.put("ticketId",
						 * ifsRevIfxd.getTicketId()); map.put("satacode", "-1");
						 * ifsCfetsfxSptMapper.updateStatcodeByTicketId(map);
						 */
						String ticketId = StringUtils.trimToEmpty(ifsRevIfxd.getFedealno());
						if (!"".equals(ticketId)) {
							if (ifsRevIfxd.getDealType().endsWith("SWAP")) {
								// 外汇掉期
								swapMapper.updateApproveStatusFxSwapByID(ticketId);
							} else if (ifsRevIfxd.getDealType().endsWith("FWD")) {
								// 外汇远期
								fwdMapper.updateApproveStatusFxFwdByID(ticketId);
							} else if (ifsRevIfxd.getDealType().endsWith("SPT")) {
								// 外汇即期
								sptMapper.updateApproveStatusFxSptByID(ticketId);
							} else if ("spt".equals(ifsRevIfxd.getDealType()) || "fwd".equals(ifsRevIfxd.getDealType())
									|| "swap".equals(ifsRevIfxd.getDealType())) {
								// 黄金即远掉
								Map<String, Object> revmap = new HashMap<String, Object>();
								revmap.put("approveStatus", DictConstants.ApproveStatus.TradeOffset);
								revmap.put("fedealno", ticketId);
								metalGoldMapper.updateReverseCfetsMetalGoldByFedealno(revmap);
							}
						}
					}

				} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {// 调用存储过程

				} else {
					JY.raise("系统参数未配置,未进行交易导入处理......");
				}

			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}
	}





}
