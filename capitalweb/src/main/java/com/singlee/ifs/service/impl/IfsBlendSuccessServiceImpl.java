package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsBlendSuccessMapper;
import com.singlee.ifs.model.IfsBlendSuccess;
import com.singlee.ifs.service.IfsBlendSuccessService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsBlendSuccessServiceImpl implements IfsBlendSuccessService {
	 @Autowired
	private IfsBlendSuccessMapper ifsBlendSuccessMapper;
	 
	@Override
	public Page<IfsBlendSuccess> searchPageSuccess(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsBlendSuccess> result = ifsBlendSuccessMapper.searchPageSuccess(map,rb);
		return result;
	}
	@Override
	public void insert(IfsBlendSuccess entity) {
		entity.setInputtime(DateUtil.getCurrentDateTimeAsString());
		String id = "id"+DateUtil.getCurrentCompactDateTimeAsString()+ Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setId(id);
		ifsBlendSuccessMapper.insert(entity);		
	}
	@Override
	public void deleteById(String id) {
		ifsBlendSuccessMapper.deleteById(id);
		
	}

}
