package com.singlee.ifs.service.impl;

import com.singlee.ifs.mapper.IfsOpsCreateFifoMapper;
import com.singlee.ifs.service.IfsOpsCreateFifoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 增值税数据生成
 * @author copysun
 */
@Service
public class IfsOpsCreateFifoServiceImpl implements IfsOpsCreateFifoService {

	@Autowired
	IfsOpsCreateFifoMapper ifsOpsCreateFifoMapper;
	@Override
	public void generateData(Map<String, Object> map) {
		ifsOpsCreateFifoMapper.generateData(map);
	}
}