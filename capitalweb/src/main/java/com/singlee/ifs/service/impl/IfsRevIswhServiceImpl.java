package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IfsRevIswhMapper;
import com.singlee.ifs.model.IfsRevIswh;
import com.singlee.ifs.service.IfsRevIswhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IfsRevIswhServiceImpl implements IfsRevIswhService {
	@Autowired
	IfsRevIswhMapper ifsRevIswhMapper;

	@Override
	public Page<IfsRevIswh> getRevIswhPage(Map<String, Object> params,int isFinished) {
		Page<IfsRevIswh> page = new Page<IfsRevIswh>();
		if (isFinished == 1) {// 待审批
			page = ifsRevIswhMapper.getRevIswhUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRevIswhMapper.getRevIswhFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRevIswhMapper.getRevIswhMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}

	@Override
	public void insert(IfsRevIswh entity) {
		/** 新增时将 审批状态 改为 新建 */
		entity.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId = ifsRevIswhMapper.getId();
		entity.setTicketId(ticketId);
		ifsRevIswhMapper.insert(entity);
	}

	@Override
	public void updateById(IfsRevIswh entity) {
		ifsRevIswhMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsRevIswhMapper.deleteById(id);
	}

}
