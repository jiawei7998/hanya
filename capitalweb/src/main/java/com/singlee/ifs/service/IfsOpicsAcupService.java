package com.singlee.ifs.service;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.IfsBatchStep;

import java.util.List;
import java.util.Map;

/***
 * opics SL_acup日终批量发送 service
 * 
 * @author singlee
 */
public interface IfsOpicsAcupService {

	/**
	 * 总账自动发送
	 */
	PageInfo<IfsBatchStep> autoSendAcup(Map<String, String> map);

	/**
	 * 总账发送日志查询
	 */
	PageInfo<IfsBatchStep> selectIfsBatchStep(Map<String, String> map);

	
	/**
	 * 总账的当日错账重发
	 */
	RetMsg<SlOutBean> retrysendAcup(Map<String, String> map);

	/**
	 * 总账的当日错账重发
	 */
	String dateCheck(Map<String, String> map);

	/**
	 * 总账按照套号保存(大连使用)
	 */
	RetMsg<SlOutBean> saveAcupDetail(List<AcupSendBean> acupBeans);

	/**
	 * 生产总账(大连使用)
	 */
	SlOutBean createAcupProc(Map<String, String> map);
	
}