package com.singlee.ifs.service;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCdtcTestConnectService.java
 * @Description 中债连通性测试
 * @createTime 2021年11月25日 14:17:00
 */
public interface IfsCdtcTestConnectService {
    String createHertMessageXml(Map<String,Object> paramer);

    String sendHertMessageXml(Map<String, Object> map);
}
