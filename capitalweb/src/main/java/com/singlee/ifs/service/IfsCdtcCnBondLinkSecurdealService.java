package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCdtcCnBondLinkSecurdeal;

import java.util.Map;

public interface IfsCdtcCnBondLinkSecurdealService {
	// 分页查询
	Page<IfsCdtcCnBondLinkSecurdeal> searchPageCnBondLinkSecurdeal(Map<String, Object> map);
	
	IfsCdtcCnBondLinkSecurdeal queryByDealNo(String dealNo);
}
