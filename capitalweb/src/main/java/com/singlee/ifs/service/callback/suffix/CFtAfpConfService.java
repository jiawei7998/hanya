package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateServiceProxy;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.hrbcap.service.impl.HrbCapSettleOrderCallback;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import com.singlee.refund.mapper.CFtAfpConfMapper;
import com.singlee.refund.mapper.CFtAfpMapper;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.CFtAfp;
import com.singlee.refund.model.CFtAfpConf;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.model.CFtTpos;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "CFtAfpConfService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CFtAfpConfService extends IfsApproveServiceBase {

	@Autowired
	private CFtAfpConfMapper cFtAfpConfMapper;
	@Autowired
	private CFtAfpMapper cFtAfpMapper;
	@Autowired
	private CFtInfoMapper cFtInfoMapper;
	@Autowired
	private CFtTposMapper cFtTposMapper;
	@Resource
	IFSMapper ifsMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Autowired
	HrbCapSettleOrderCallback hrbCapSettleOrderCallback;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		cFtAfpConfMapper.updateAfpConfByDealNo(serial_no, status, date);
		CFtAfpConf cFtAfpConf = cFtAfpConfMapper.selectByPrimaryKey(serial_no);
		
		// 0.1 当前业务为空则不进行处理
		if (null == cFtAfpConf) {
			return;
		}

		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {

		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(cFtAfpConf.getDealNo());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(cFtAfpConf.getDealNo());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(cFtAfpConf.getApproveStatus());// 设置监控表-审批状态

			// 判断基金是否存在
			CFtInfo cFtInfo = cFtInfoMapper.selectByPrimaryKey(cFtAfpConf.getFundCode());
			if (cFtInfo == null) {
				JY.raise("交易的基金不存在......");
			}
			if (cFtInfo.getCcy() == null) {
				JY.raise("交易的基金的币种不存在......");
			}
			if (TradeConstants.ProductCode.CURRENCY_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("货币基金申购份额确认");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.CURY_FUND_AFPCONF);
			} else if (TradeConstants.ProductCode.BOND_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("债券基金申购份额确认");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.BD_FUND_AFPCONF);
			} else if (TradeConstants.ProductCode.SPEC_FUND.equals(cFtInfo.getFundType())) {
				ifsFlowMonitor.setPrdName("专户基金申购份额确认");
				ifsFlowMonitor.setPrdNo(TradeConstants.TrdType.FUND_AFPCONF);
			}
			ifsFlowMonitor.setTradeDate(cFtAfpConf.getTdate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(cFtAfpConf.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("31");//基金申购份额确认流程
			String cname = cFtAfpConf.getCname();
			if (null != cname || "".equals(cname)) {
				ifsFlowMonitor.setCustName(cname);
			}
			
			// 2.3查询客制化字段
			StringBuffer buffer = new StringBuffer("");
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("FT_AFP_CONF");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CBT where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = cFtAfpConfMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);
		
		}
		
		// 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成

			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map1);
		}
		
		// 3.1未审批通过则不进行后续操作
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("fundCode", cFtAfpConf.getFundCode());
			String curDate= DayendDateServiceProxy.getInstance().getSettlementDate();
			params.put("postdate", curDate);
			params.put("sponinst", cFtAfpConf.getSponInst());
			params.put("invtype", cFtAfpConf.getInvType());
			params.put("prdNo", cFtAfpConf.getPrdNo());
			CFtTpos obj = cFtTposMapper.selectByMap(params);
			if (obj == null) {
				// 创建持仓
				CFtTpos cFtTpos = generateFtTpos(cFtAfpConf);
				cFtTpos.setPostdate(curDate);

				CFtAfp objt = cFtAfpMapper.selectByPrimaryKey(cFtAfpConf.getRefNo());
				if (objt != null) {
					cFtTpos.setAfpDate(objt.getTdate());
				}

				cFtTposMapper.insertSelective(cFtTpos);
			} else {
				obj.setUtQty(obj.getUtQty().add(cFtAfpConf.getShareAmt()));
				obj.setCost(obj.getCost().add(cFtAfpConf.getAtpAmt()));
				obj.setUcCost(obj.getUcCost().add(cFtAfpConf.getAtpAmt().subtract(cFtAfpConf.getAmt())));
				obj.setAfpAmt(obj.getAfpAmt().add(cFtAfpConf.getAmt()));
				obj.setHandleAmt(obj.getHandleAmt().add(cFtAfpConf.getHandleAmt()));
				cFtTposMapper.updateByPrimaryKeySelective(obj);
			}

			//生成会计分录
			Map<String, Object> map = new HashMap<>();
			map.put("serial_no",cFtAfpConf.getDealNo());
			map.put("prdNo",cFtAfpConf.getPrdNo());
			map.put("refNo",cFtAfpConf.getRefNo());
			
			boolean flag = hrbCapSettleOrderCallback.generateVerifyAcup(map);
			
			//如果flag为true ，报文发核心
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	// 获取交易编号
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// 前缀
		hashMap.put("sStr", "");
		// 贸易类型 正式交易
		hashMap.put("trdType", "2");
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}
	
	/**
	 *生成持仓
	 * @param cFtAfp
	 * @param tdate
	 * @return
	 */
	public CFtTpos generateFtTpos(CFtAfpConf cFtAfpConf){
		CFtTpos cFtTpos = new CFtTpos();
		cFtTpos.setPrdNo(cFtAfpConf.getPrdNo());
		cFtTpos.setInvtype(cFtAfpConf.getInvType());
		cFtTpos.setFundCode(cFtAfpConf.getFundCode());
		cFtTpos.setSponInst(cFtAfpConf.getSponInst());
		cFtTpos.setcNo(cFtAfpConf.getCno());
		cFtTpos.setCcy(cFtAfpConf.getCcy());
		cFtTpos.setIsAssmt("1");

		cFtTpos.setUtQty(cFtAfpConf.getShareAmt());//持仓份额
		cFtTpos.setCost(cFtAfpConf.getAtpAmt());//基金投资成本
		cFtTpos.setUcCost(cFtAfpConf.getAtpAmt().subtract(cFtAfpConf.getAmt()));//基金未确认成本
		cFtTpos.setAfpAmt(cFtAfpConf.getAmt());//累计申购金额
		cFtTpos.setHandleAmt(cFtAfpConf.getHandleAmt());//累计手续费
		
		cFtTpos.setRevalAmt(new BigDecimal("0"));//估值
		cFtTpos.settUnplAmt(new BigDecimal("0"));//累计总估值
		
		cFtTpos.setNewQty(new BigDecimal("0"));//赎回份额
		cFtTpos.setUtAmt(new BigDecimal("0"));//赎回成本
		cFtTpos.setRdpIntAmt(new BigDecimal("0"));//赎回累计红利
		
		cFtTpos.setyUnplAmt(new BigDecimal("0"));//累计未付收益
		
		cFtTpos.setShAmt(new BigDecimal("0"));//累计已分红金额
		cFtTpos.setNcvAmt(new BigDecimal("0"));//累计已转投金额
		return cFtTpos;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");

		String id = (String)paramer.get("id");
		if("jyhjsgqr".equals(id)){
			paramer.put("prdNo", "801");
		}else if("jyzjsgqr".equals(id)){
			paramer.put("prdNo", "802");
		}else if("jyzhjsgqr".equals(id)){
			paramer.put("prdNo", "803");
		}
		paramer.put("dealNo", paramer.get("contractId"));
		paramer.put("tdate", paramer.get("forDate"));

		List<CFtAfpConf> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = cFtAfpConfMapper.searchCFtAfpConfPageMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = cFtAfpConfMapper.searchCFtAfpConfPageUnfinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = cFtAfpConfMapper.searchCFtAfpConfPageFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> currencyList = taDictVoMapper.getTadictTree("Currency").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> tbAccountTypeList = taDictVoMapper.getTadictTree("tbAccountType").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (CFtAfpConf cFtAfpConf: list) {
			cFtAfpConf.setCcy(currencyList.get(cFtAfpConf.getCcy()));
			cFtAfpConf.setInvType(tbAccountTypeList.get(cFtAfpConf.getInvType()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
