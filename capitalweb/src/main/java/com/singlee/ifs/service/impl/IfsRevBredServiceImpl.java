package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.ifs.mapper.IfsRevBredMapper;
import com.singlee.ifs.model.IfsRevBred;
import com.singlee.ifs.service.IfsRevBredService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 * @author jiangzx
 * @date: 2018年7月24日 下午3:18:53 
 * @version
 * @since JDK 1.6
 */

@Service
public class IfsRevBredServiceImpl implements IfsRevBredService{
	
	@Autowired
	IfsRevBredMapper ifsRevBredMapper;

	// 新增
	@Override
	public void insert(IfsRevBred record) {
		/** 新增时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		String ticketId = ifsRevBredMapper.getId();
		record.setTicketId(ticketId);
        ifsRevBredMapper.insert(record);
	}

	// 删除
	@Override
	public void deleteByPrimaryKey(String tacketId) {
		ifsRevBredMapper.deleteByPrimaryKey(tacketId);
	}

	// 查询一条数据
	@Override
	public IfsRevBred selectByPrimaryKey(Map<String, Object> map) {
		IfsRevBred ifsRevBred =  ifsRevBredMapper.selectByPrimaryKey(map);
		return ifsRevBred;
	}

	// 更新一条数据
	@Override
	public void updateByPrimaryKey(IfsRevBred record) {
		/** 修改时将 审批状态 改为 新建 */
		record.setApproveStatus(DictConstants.ApproveStatus.New);
		ifsRevBredMapper.updateByPrimaryKey(record);
	}

	//	分页查询
	@Override
	public Page<IfsRevBred> searchPageRevBred(Map<String,Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsRevBred> result = ifsRevBredMapper.searchPageRevBred(map, rb);
		return result;
	}

	/***
	 * 根据请求参数查询债券冲销分页列表--我发起的、待审批、已审批列表
	 */
	@Override
	public Page<IfsRevBred> getRevBredPage(Map<String, Object> params,
			int isFinished) {
		Page<IfsRevBred> page = new Page<IfsRevBred>();
		if (isFinished == 1) {// 待审批
			page = ifsRevBredMapper.getRevBredUnfinished(params, ParameterUtil.getRowBounds(params));
		} else if (isFinished == 2) {// 已审批
			page = ifsRevBredMapper.getRevBredFinished(params, ParameterUtil.getRowBounds(params));
		} else {// 我发起的
			page = ifsRevBredMapper.getRevBredMine(params, ParameterUtil.getRowBounds(params));
		}
		return page;
	}
	
}
