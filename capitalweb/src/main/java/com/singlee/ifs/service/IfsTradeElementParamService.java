package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTradeParaGroup;

import java.util.Map;

/***
 * 交易要素 service
 * @author singlee4
 *
 */
public interface IfsTradeElementParamService {
	
	//新增
	public void insert(IfsTradeParaGroup entity);
	
	//修改
	void updateById(IfsTradeParaGroup entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsTradeParaGroup> searchPageOpicsTrdParam(Map<String,Object> map);
	
	
	
	
	
	
	
	
}
