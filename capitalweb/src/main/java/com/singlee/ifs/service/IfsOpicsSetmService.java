package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsSetm;

import java.util.List;
import java.util.Map;

/**
 * 清算方式
 * @author   yanming 
 */
public interface IfsOpicsSetmService {
	
	//新增
	public void insert(IfsOpicsSetm entity);
	
	//修改
	void updateById(IfsOpicsSetm entity);
	
	//删除
	void deleteById(IfsOpicsSetm entity);
	
	//分页查询
	Page<IfsOpicsSetm> searchPageOpicsSetm(Map<String,Object> map);
	
	//根据id查询实体类
	IfsOpicsSetm searchById(IfsOpicsSetm entity);
	
	//更新同步状态
	void updateStatus(IfsOpicsSetm entity);
	
	//批量校准
	SlOutBean batchCalibration(List<IfsOpicsSetm> list);
	
	//查询所有
	Page<IfsOpicsSetm> searchAllOpicsSetm();
}

