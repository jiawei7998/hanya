package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.mxgraph.util.svg.ParseException;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.opics.IExternalServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsWindSecLevelMapper;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.IfsOpicsCustService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 交易要素 实现类
 * 
 * @author singlee4
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsOpicsBondServiceImpl implements IfsOpicsBondService {
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;
	@Autowired
	IExternalServer externalServer;
	@Autowired
	IfsWindSecLevelMapper IfsWindSecLevelMapper;

	@Autowired
	IfsOpicsCustService ifsOpicsCustService;
	@Override
	public String insert(IfsOpicsBond entity) {
		// 设置 录入时间
		entity.setInputdate(DateUtil.getCurrentDateTimeAsString());
		String dealno = getTicketId();
		entity.setDealno(dealno);
		String bndcd = entity.getBndcd();
		String str = "保存成功";
		if (ifsOpicsBondMapper.searchById(bndcd) == null) {
			ifsOpicsBondMapper.insert(entity);
		} else {
			str = "保存失败,该债券信息已存在";
		}
		return str;
	}

	public String getTicketId() {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// hashMap.put("sStr", "");// 前缀
		// hashMap.put("trdType", "");// 贸易类型，正式交易
		String dealno = ifsOpicsBondMapper.getTradeId(hashMap);
		return dealno;
	}

	@Override
	public void updateById(IfsOpicsBond entity) {
		if("".equals(entity.getDealno())){
			entity.setDealno(getTicketId());
		}
		ifsOpicsBondMapper.updateById(entity);
	}

	@Override
	public void deleteById(String id) {
		ifsOpicsBondMapper.deleteById(id);
	}

	@Override
	public Page<IfsOpicsBond> searchPageBondTrd(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsBond> result = ifsOpicsBondMapper.searchPageBondTrd(map, rb);
		return result;
	}

	@Override
	public Page<IfsOpicsBond> searchPageCheckTrd(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsBond> result = ifsOpicsBondMapper.searchPageCheckTrd(map, rb);
		return result;
	}

	@Override
	public String doBondExcelPropertyToDataBase(List<IfsOpicsBond> ifsOpicsBonds) {
		int seccess = 0;
		int defeat = 0;
		for (int i = 0; i < ifsOpicsBonds.size(); i++) {
			String bndcd = ifsOpicsBonds.get(i).getBndcd();
			if (bndcd != "") {
				if (ifsOpicsBondMapper.searchById(bndcd) == null) {
					ifsOpicsBondMapper.insert(ifsOpicsBonds.get(i));
					seccess = seccess + 1;
				} else {
					defeat = defeat + 1;
				}
			} else {
				defeat = defeat + 1;
			}

		}
		return "插入成功" + seccess + "条，插入失败" + defeat + "条(该债券代码已存在或没有输入债券代码)";
	}

	@Override
	public void updateStatcodeById(Map<String, Object> map) {
		ifsOpicsBondMapper.updateStatcodeById(map);
	}

	@Override
	public IfsOpicsBond searchOneById(String bndcd) {
		return ifsOpicsBondMapper.searchById(bndcd);
	}

	@Override
	public SlOutBean batchCalibration(String type, String[] bndcds) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			
			/**************** 对选中的某一行进行校准 **********************/
				if ("1".equals(type)) { // 选中行校准
					for (int i = 0; i < bndcds.length; i++) {
						IfsOpicsBond checkEntity = ifsOpicsBondMapper.searchById(bndcds[i]);//判断本地库是否有
						SlSecmBean SecmOpics=externalServer.getSemmById(bndcds[i]);//查询opics记录
						if(SecmOpics!=null){
							IfsOpicsBond entity = new IfsOpicsBond();
							if(checkEntity!=null){
								entity = ifsOpicsBondMapper.searchById(bndcds[i]);
							}
							
							entity.setBndcd(SecmOpics.getSecid().trim());
							entity.setBr("01");//部门
							entity.setProduct(StringUtils.trimToEmpty(SecmOpics.getProduct()));
							entity.setProdtype(StringUtils.trimToEmpty(SecmOpics.getProdtype()));
							entity.setIntcalcrule(StringUtils.trimToEmpty(SecmOpics.getIntcalcrule()));
							entity.setAcctngtype(StringUtils.trimToEmpty(SecmOpics.getAcctngtype()));
							entity.setCcy(StringUtils.trimToEmpty(SecmOpics.getCcy()));
							entity.setSettccy(StringUtils.trimToEmpty(SecmOpics.getSettccy()));
							entity.setSecunit(StringUtils.trimToEmpty(SecmOpics.getSecunit()));
							entity.setDenom(StringUtils.trimToEmpty(SecmOpics.getDenom()));
							entity.setIssuer(StringUtils.trimToEmpty(SecmOpics.getIssuer()));
							//保存发行机构中文名
							IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(SecmOpics.getIssuer());
							if(ifsOpicsCust!=null){
								entity.setIssuername(StringUtils.trimToEmpty(ifsOpicsCust.getCliname()));
							}
							// 发行日期
							Date issdate = SecmOpics.getIssdate();
							entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
							String secmSic = SecmOpics.getSecmsic();
							if(secmSic==null){
								secmSic="";
							}
							entity.setBondproperties(secmSic.trim());
							// 起息日
							Date vdate = SecmOpics.getVdate();
							entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
							// 到期日
							Date mdate = SecmOpics.getMdate();
							entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
							entity.setCouponrate(StringUtils.trimToEmpty(SecmOpics.getCouprate()));
//							entity.setIssprice(SecmOpics.getParamt()==null?null:BigDecimal.valueOf(new Double(SecmOpics.getParamt())));
							entity.setIntpaycircle(StringUtils.trimToEmpty(SecmOpics.getIntpaycycle()));
							entity.setIntpaymethod(StringUtils.trimToEmpty(SecmOpics.getIntpayrule()));
							entity.setIntenddaterule(StringUtils.trimToEmpty(SecmOpics.getIntenddaterule()));
//							entity.setAccountno(SecmOpics.getSecsacct());
							entity.setBasis(StringUtils.trimToEmpty(SecmOpics.getBasis()));
							entity.setFloatratemark(StringUtils.trimToEmpty(SecmOpics.getRatecode()));
							entity.setBasicspread(StringUtils.trimToEmpty(SecmOpics.getSpreadRate()));
							entity.setDescr(StringUtils.trimToEmpty(SecmOpics.getDescr()));
							entity.setBndnm_cn(StringUtils.trimToEmpty(SecmOpics.getDescr()));
							entity.setBndnm_en(StringUtils.trimToEmpty(SecmOpics.getDescr()));
							entity.setSettdays(StringUtils.trimToEmpty(SecmOpics.getSettdays()));
							entity.setUserid(SlSessionHelper.getUserId());
							//MODIFY BY SHENZL 20181112
							entity.setAccountno(StringUtils.trimToEmpty(SecmOpics.getSecsacct()));
							if (SecmOpics.getLstmntdate() != null) {
								Date lstmntdate = SecmOpics.getLstmntdate();
								entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
							} else {
								entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
							}
							entity.setStatus("C"); // 设置为已同步
							if(checkEntity!=null){
								ifsOpicsBondMapper.updateById(entity);
							}else{
								ifsOpicsBondMapper.insert(entity);
							}
						}else{
							//如果opics删除了需要改为未同步
							if(checkEntity!=null){
								IfsOpicsBond entity = new IfsOpicsBond();
								entity = ifsOpicsBondMapper.searchById(bndcds[i]);
								if (entity.getStatus().endsWith("C")) {
									ifsOpicsBondMapper.updateStatusById(bndcds[i]);
								}
							}
						}
						/*for (int j = 0; j < listSecmOpics.size(); j++) {
							// 若OPICS库中有相同的债券信息，就将本地的更新成OPICS中的，opics没有的字段保持不变
							if (listSecmOpics.get(j).getSecid().trim().equals(bndcds[i])) {
								IfsOpicsBond entity = new IfsOpicsBond();
								entity = ifsOpicsBondMapper.searchById(bndcds[i]);
								
								entity.setBndcd(listSecmOpics.get(j).getSecid().trim());
								entity.setBr("1");//部门
								entity.setProduct(listSecmOpics.get(j).getProduct());
								entity.setProdtype(listSecmOpics.get(j).getProdtype());
								entity.setIntcalcrule(listSecmOpics.get(j).getIntcalcrule());
								entity.setAcctngtype(listSecmOpics.get(j).getAcctngtype());
								entity.setCcy(listSecmOpics.get(j).getCcy());
								entity.setSecunit(listSecmOpics.get(j).getSecunit());
								entity.setDenom(listSecmOpics.get(j).getDenom());
								entity.setIssuer(listSecmOpics.get(j).getIssuer());
								//保存发行机构中文名
								IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(listSecmOpics.get(j).getIssuer());
								entity.setIssuername(ifsOpicsCust.getCliname());
								// 发行日期
								Date issdate = listSecmOpics.get(j).getIssdate();
								entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
								String secmSic = listSecmOpics.get(j).getSecmsic();
								if(secmSic==null){
									secmSic="";
								}
								entity.setBondproperties(secmSic.trim());
								// 起息日
								Date vdate = listSecmOpics.get(j).getVdate();
								entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
								// 到期日
								Date mdate = listSecmOpics.get(j).getMdate();
								entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
								entity.setCouponrate(listSecmOpics.get(j).getCouprate());
								entity.setIssprice(listSecmOpics.get(j).getTenor()==null?null:BigDecimal.valueOf(new Double(listSecmOpics.get(j).getTenor())));
								entity.setIntpaycircle(listSecmOpics.get(j).getIntpaycycle());
								entity.setIntpaymethod(listSecmOpics.get(j).getIntpayrule());
								entity.setIntenddaterule(listSecmOpics.get(j).getIntenddaterule());
//								entity.setAccountno(listSecmOpics.get(j).getSecsacct());
								entity.setBasis(listSecmOpics.get(j).getBasis());
								entity.setFloatratemark(listSecmOpics.get(j).getRatecode());
								entity.setBasicspread(listSecmOpics.get(j).getSpreadRate());
								entity.setDescr(listSecmOpics.get(j).getDescr());
								entity.setSettdays(listSecmOpics.get(j).getSettdays());
								entity.setUserid(SlSessionHelper.getUserId());
								if (listSecmOpics.get(j).getLstmntdate() != null) {
									Date lstmntdate = listSecmOpics.get(j).getLstmntdate();
									entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
								} else {
									entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
								}
								entity.setStatus("C"); // 设置为已同步
								ifsOpicsBondMapper.updateById(entity);
								break;
							}
							// 若OPICS库中无数据，则将状态改为'未同步'，其余不变
							if (j == listSecmOpics.size() - 1) {
								IfsOpicsBond entity = new IfsOpicsBond();
								entity = ifsOpicsBondMapper.searchById(bndcds[i]);
								if (entity.getStatus().endsWith("C")) {
									ifsOpicsBondMapper.updateStatusById(bndcds[i]);
								}

							}
						}*/
					}

					/**************** 对本地库所有债券信息进行校准 **********************/
				} else { // 全部校准
					// 从OPICS库查询所有数据
					List<SlSecmBean> listSecmOpics = externalServer.synchroSemm();
					//循环opics数据:前置环境没有新增，有修改
					for (int i = 0; i < listSecmOpics.size(); i++) {
						IfsOpicsBond entity = ifsOpicsBondMapper.searchById(listSecmOpics.get(i).getSecid().trim());
						if(entity==null){
							entity = new IfsOpicsBond();
							entity.setBndcd(listSecmOpics.get(i).getSecid().trim());
							entity.setBr("01");//部门
							entity.setProduct(StringUtils.trimToEmpty(listSecmOpics.get(i).getProduct()));
							entity.setProdtype(StringUtils.trimToEmpty(listSecmOpics.get(i).getProdtype()));
							entity.setIntcalcrule(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntcalcrule()));
							entity.setAcctngtype(StringUtils.trimToEmpty(listSecmOpics.get(i).getAcctngtype()));
							entity.setCcy(StringUtils.trimToEmpty(listSecmOpics.get(i).getCcy()));
							entity.setSettccy(StringUtils.trimToEmpty(listSecmOpics.get(i).getSettccy()));
							entity.setSecunit(StringUtils.trimToEmpty(listSecmOpics.get(i).getSecunit()));
							entity.setDenom(StringUtils.trimToEmpty(listSecmOpics.get(i).getDenom()));
							entity.setIssuer(StringUtils.trimToEmpty(listSecmOpics.get(i).getIssuer()));
							//保存发行机构中文名
							IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(listSecmOpics.get(i).getIssuer());
							if(ifsOpicsCust!=null){
								entity.setIssuername(StringUtils.trimToEmpty(ifsOpicsCust.getCliname()));
							}
							
							// 发行日期
							Date issdate = listSecmOpics.get(i).getIssdate();
							entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
							String secmSic = listSecmOpics.get(i).getSecmsic();
							if(secmSic==null){
								secmSic="";
							}
							entity.setBondproperties(secmSic.trim());
							// 起息日
							Date vdate = listSecmOpics.get(i).getVdate();
							entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
							// 到期日
							Date mdate = listSecmOpics.get(i).getMdate();
							entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
							entity.setCouponrate(StringUtils.trimToEmpty(listSecmOpics.get(i).getCouprate()));
//							entity.setIssprice(listSecmOpics.get(i).getParamt()==null?null:BigDecimal.valueOf(new Double(listSecmOpics.get(i).getParamt())));
							entity.setIntpaycircle(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntpaycycle()));
							entity.setIntpaymethod(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntpayrule()));
							entity.setIntenddaterule(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntenddaterule()));
//							entity.setAccountno(listSecmOpics.get(i).getSecsacct());
							entity.setBasis(StringUtils.trimToEmpty(listSecmOpics.get(i).getBasis()));
							entity.setFloatratemark(StringUtils.trimToEmpty(listSecmOpics.get(i).getRatecode()));
							entity.setBasicspread(StringUtils.trimToEmpty(listSecmOpics.get(i).getSpreadRate()));
							entity.setDescr(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setBndnm_cn(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setBndnm_en(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setSettdays(StringUtils.trimToEmpty(listSecmOpics.get(i).getSettdays()));
							entity.setStatus("C"); // 设置为已同步
							entity.setUserid(SlSessionHelper.getUserId());
							if (listSecmOpics.get(i).getLstmntdate() != null) {
								Date lstmntdate = listSecmOpics.get(i).getLstmntdate();
								entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
							} else {
								entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
							}
							
//							entity.setIssprice(new BigDecimal("100"));//发行价格
//							entity.setBondratingclass("2");//评级类型
//							IfsWindSecLevelBean ifsWindSecLevelBean = IfsWindSecLevelMapper.selectByPrimaryKey(entity.getBndcd().trim());
//							entity.setBondratingagency(ifsWindSecLevelBean.getZtInst());//评级机构
//							entity.setBondrating(ifsWindSecLevelBean.getZxZtLevel());//评级等级
//							entity.setParagraphfirstplan("");//首次付息日
//							entity.setRateType("");//利率类型
//							entity.setSlPubMethod("01");//发行方式
//							entity.setMarketDate(DateUtil.format(issdate, "yyyy-MM-dd"));//上市日期  
							
							//MODIFY BY SHENZL 20181112
							entity.setAccountno(StringUtils.trimToEmpty(listSecmOpics.get(i).getSecsacct()));
							ifsOpicsBondMapper.insert(entity);
						}else{
							entity.setBndcd(listSecmOpics.get(i).getSecid().trim());
							entity.setBr("01");//部门
							entity.setProduct(StringUtils.trimToEmpty(listSecmOpics.get(i).getProduct()));
							entity.setProdtype(StringUtils.trimToEmpty(listSecmOpics.get(i).getProdtype()));
							entity.setIntcalcrule(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntcalcrule()));
							entity.setAcctngtype(StringUtils.trimToEmpty(listSecmOpics.get(i).getAcctngtype()));
							entity.setCcy(StringUtils.trimToEmpty(listSecmOpics.get(i).getCcy()));
							entity.setSettccy(StringUtils.trimToEmpty(listSecmOpics.get(i).getSettccy()));
							entity.setSecunit(StringUtils.trimToEmpty(listSecmOpics.get(i).getSecunit()));
							entity.setDenom(StringUtils.trimToEmpty(listSecmOpics.get(i).getDenom()));
							entity.setIssuer(StringUtils.trimToEmpty(listSecmOpics.get(i).getIssuer()));
							//保存发行机构中文名
							IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(listSecmOpics.get(i).getIssuer());
							if(ifsOpicsCust!=null){
								entity.setIssuername(StringUtils.trimToEmpty(ifsOpicsCust.getCliname()));
							}
							// 发行日期
							Date issdate = listSecmOpics.get(i).getIssdate();
							entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
							String secmSic = listSecmOpics.get(i).getSecmsic();
							if(secmSic==null){
								secmSic="";
							}
							entity.setBondproperties(secmSic.trim());
							// 起息日
							Date vdate = listSecmOpics.get(i).getVdate();
							entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
							// 到期日
							Date mdate = listSecmOpics.get(i).getMdate();
							entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
							entity.setCouponrate(StringUtils.trimToEmpty(listSecmOpics.get(i).getCouprate()));
//							entity.setIssprice(listSecmOpics.get(i).getParamt()==null?null:BigDecimal.valueOf(new Double(listSecmOpics.get(i).getParamt())));
							entity.setIntpaycircle(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntpaycycle()));
							entity.setIntpaymethod(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntpayrule()));
							entity.setIntenddaterule(StringUtils.trimToEmpty(listSecmOpics.get(i).getIntenddaterule()));
//							entity.setAccountno(listSecmOpics.get(i).getSecsacct());
							entity.setBasis(StringUtils.trimToEmpty(listSecmOpics.get(i).getBasis()));
							entity.setFloatratemark(StringUtils.trimToEmpty(listSecmOpics.get(i).getRatecode()));
							entity.setBasicspread(StringUtils.trimToEmpty(listSecmOpics.get(i).getSpreadRate()));
							entity.setDescr(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setBndnm_cn(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setBndnm_en(StringUtils.trimToEmpty(listSecmOpics.get(i).getDescr()));
							entity.setSettdays(StringUtils.trimToEmpty(listSecmOpics.get(i).getSettdays()));
							entity.setUserid(SlSessionHelper.getUserId());
							if (listSecmOpics.get(i).getLstmntdate() != null) {
								Date lstmntdate = listSecmOpics.get(i).getLstmntdate();
								entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
							} else {
								entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
							}
							entity.setStatus("C"); // 设置为已同步
							//MODIFY BY SHENZL 20181112
                            entity.setAccountno(StringUtils.trimToEmpty(listSecmOpics.get(i).getSecsacct()));
							ifsOpicsBondMapper.updateById(entity);
						}
					}
					// 从本地库查询所有的数据
//					List<IfsOpicsBond> listBondLocal = ifsOpicsBondMapper.searchAllOpicsBond();
					// 1、以OPICS库为主，更新或插入本地库 
//					for (int i = 0; i < listSecmOpics.size(); i++) {
//						// 若全部不同，则插入到本地库
//						if (listBondLocal.size() == 0) { // 本地无数据
//							IfsOpicsBond entity = new IfsOpicsBond();
//							entity.setBndcd(listSecmOpics.get(i).getSecid().trim());
//							entity.setBr("1");//部门
//							entity.setProduct(listSecmOpics.get(i).getProduct());
//							entity.setProdtype(listSecmOpics.get(i).getProdtype());
//							entity.setIntcalcrule(listSecmOpics.get(i).getIntcalcrule());
//							entity.setAcctngtype(listSecmOpics.get(i).getAcctngtype());
//							entity.setCcy(listSecmOpics.get(i).getCcy());
//							entity.setSecunit(listSecmOpics.get(i).getSecunit());
//							entity.setDenom(listSecmOpics.get(i).getDenom());
//							entity.setIssuer(listSecmOpics.get(i).getIssuer());
//							//保存发行机构中文名
//							IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(listSecmOpics.get(i).getIssuer());
//							entity.setIssuername(ifsOpicsCust.getCliname());
//							// 发行日期
//							Date issdate = listSecmOpics.get(i).getIssdate();
//							entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
//							String secmSic = listSecmOpics.get(i).getSecmsic();
//							if(secmSic==null){
//								secmSic="";
//							}
//							entity.setBondproperties(secmSic.trim());
//							// 起息日
//							Date vdate = listSecmOpics.get(i).getVdate();
//							entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
//							// 到期日
//							Date mdate = listSecmOpics.get(i).getMdate();
//							entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
//							entity.setCouponrate(listSecmOpics.get(i).getCouprate());
//							entity.setIssprice(listSecmOpics.get(i).getTenor()==null?null:BigDecimal.valueOf(new Double(listSecmOpics.get(i).getTenor())));
//							entity.setIntpaycircle(listSecmOpics.get(i).getIntpaycycle());
//							entity.setIntpaymethod(listSecmOpics.get(i).getIntpayrule());
//							entity.setIntenddaterule(listSecmOpics.get(i).getIntenddaterule());
////							entity.setAccountno(listSecmOpics.get(i).getSecsacct());
//							entity.setBasis(listSecmOpics.get(i).getBasis());
//							entity.setFloatratemark(listSecmOpics.get(i).getRatecode());
//							entity.setBasicspread(listSecmOpics.get(i).getSpreadRate());
//							entity.setDescr(listSecmOpics.get(i).getDescr());
//							entity.setSettdays(listSecmOpics.get(i).getSettdays());
//							entity.setStatus("C"); // 设置为已同步
//							entity.setUserid(SlSessionHelper.getUserId());
//							if (listSecmOpics.get(i).getLstmntdate() != null) {
//								Date lstmntdate = listSecmOpics.get(i).getLstmntdate();
//								entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
//							} else {
//								entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
//							}
//							ifsOpicsBondMapper.insert(entity);
//						} else {
//							for (int j = 0; j < listBondLocal.size(); j++) {
//								IfsOpicsBond entity = new IfsOpicsBond();
//								entity.setBndcd(listSecmOpics.get(i).getSecid().trim());
//								entity.setBr("1");//部门
//								entity.setProduct(listSecmOpics.get(i).getProduct());
//								entity.setProdtype(listSecmOpics.get(i).getProdtype());
//								entity.setIntcalcrule(listSecmOpics.get(i).getIntcalcrule());
//								entity.setAcctngtype(listSecmOpics.get(i).getAcctngtype());
//								entity.setCcy(listSecmOpics.get(i).getCcy());
//								entity.setSecunit(listSecmOpics.get(i).getSecunit());
//								entity.setDenom(listSecmOpics.get(i).getDenom());
//								entity.setIssuer(listSecmOpics.get(i).getIssuer());
//								//保存发行机构中文名
//								IfsOpicsCust ifsOpicsCust = ifsOpicsCustService.searchIfsOpicsCust(listSecmOpics.get(i).getIssuer());
//								entity.setIssuername(ifsOpicsCust.getCliname());
//								// 发行日期
//								Date issdate = listSecmOpics.get(i).getIssdate();
//								entity.setPubdt(DateUtil.format(issdate, "yyyy-MM-dd"));
//								String secmSic = listSecmOpics.get(i).getSecmsic();
//								if(secmSic==null){
//									secmSic="";
//								}
//								entity.setBondproperties(secmSic.trim());
//								// 起息日
//								Date vdate = listSecmOpics.get(i).getVdate();
//								entity.setIssudt(DateUtil.format(vdate, "yyyy-MM-dd"));
//								// 到期日
//								Date mdate = listSecmOpics.get(i).getMdate();
//								entity.setMatdt(DateUtil.format(mdate, "yyyy-MM-dd"));
//								entity.setCouponrate(listSecmOpics.get(i).getCouprate());
//								entity.setIssprice(BigDecimal.valueOf(new Double(listSecmOpics.get(i).getTenor())));
//								entity.setIntpaycircle(listSecmOpics.get(i).getIntpaycycle());
//								entity.setIntpaymethod(listSecmOpics.get(i).getIntpayrule());
//								entity.setIntenddaterule(listSecmOpics.get(i).getIntenddaterule());
////								entity.setAccountno(listSecmOpics.get(i).getSecsacct());
//								entity.setBasis(listSecmOpics.get(i).getBasis());
//								entity.setFloatratemark(listSecmOpics.get(i).getRatecode());
//								entity.setBasicspread(listSecmOpics.get(i).getSpreadRate());
//								entity.setDescr(listSecmOpics.get(i).getDescr());
//								entity.setSettdays(listSecmOpics.get(i).getSettdays());
//								entity.setUserid(SlSessionHelper.getUserId());
//								if (listSecmOpics.get(i).getLstmntdate() != null) {
//									Date lstmntdate = listSecmOpics.get(i).getLstmntdate();
//									entity.setInputdate(DateUtil.format(lstmntdate, "yyyy-MM-dd"));
//								} else {
//									entity.setInputdate(DateUtil.format(new Date(), "yyyy-MM-dd"));
//								}
//								entity.setStatus("C"); // 设置为已同步
//								// 与OPICS库中的债券信息对比，相同的内容不更新，只更新不同的内容
//								if (listSecmOpics.get(i).getSecid().trim().equals(listBondLocal.get(j).getBndcd())) {
//									entity.setDealno(listBondLocal.get(j).getDealno());
//									entity.setBndnm_cn(listBondLocal.get(j).getBndnm_cn());
//									entity.setBndnm_en(listBondLocal.get(j).getBndnm_en());
//									entity.setBr(listBondLocal.get(j).getBr());
//									entity.setBasicspread(listBondLocal.get(j).getBasicspread());
//									entity.setBondproperties(listBondLocal.get(j).getBondproperties());
//									entity.setBondrating(listBondLocal.get(j).getBondrating());
//									entity.setBondratingagency(listBondLocal.get(j).getBondratingagency());
//									entity.setBondratingclass(listBondLocal.get(j).getBondratingclass());
//									entity.setParagraphfirstplan(listBondLocal.get(j).getParagraphfirstplan());
//									entity.setBondperiod(listBondLocal.get(j).getBondperiod());
//									entity.setIssuevol(listBondLocal.get(j).getIssuevol());
//									entity.setNote(listBondLocal.get(j).getNote());
//
//									ifsOpicsBondMapper.updateById(entity);
//									break;
//								}
//								// 若全部不同，则插入本地库
//								if (j == listBondLocal.size() - 1) {
//									ifsOpicsBondMapper.insert(entity);
//								}
//							}
//						}
//					}
					// 2、若本地库中的数据多于OPICS库，则将本地库中含有的但OPICS库中没有的数据的状态改为'未同步'
					// 遍历完OPICS库之后，从本地库【再次】查询所有数据，遍历本地库
					List<IfsOpicsBond> listBondLocal2 = ifsOpicsBondMapper.searchAllOpicsBond();

					for (int i = 0; i < listBondLocal2.size(); i++) {
						SlSecmBean SecmOpics=externalServer.getSemmById(listBondLocal2.get(i).getBndcd());//查询opics记录
						if(SecmOpics==null){
							IfsOpicsBond bond=listBondLocal2.get(i);
							if(bond.getStatus().endsWith("C")){
								ifsOpicsBondMapper.updateById(bond);
							}
						}
//						for (int j = 0; j < listSecmOpics.size(); j++) {
//							if (listSecmOpics.get(j).getSecid().trim().equals(listBondLocal2.get(i).getBndcd())) {
//								break;
//							}
//							// 若OPICS库中没有，则将状态改为'未同步'
//							if (j == listSecmOpics.size() - 1) {
//								IfsOpicsBond bond = new IfsOpicsBond();
//								bond = ifsOpicsBondMapper.searchById(listBondLocal2.get(i).getBndcd());
//								if (bond.getStatus().endsWith("C")) {
//									bond.setBndcd(listBondLocal2.get(i).getBndcd().trim());
//									// 发行日期
//									bond.setPubdt(listBondLocal2.get(i).getPubdt());
//									// 起息日
//									bond.setIssudt(listBondLocal2.get(i).getIssudt());
//									// 到期日
//									bond.setMatdt(listBondLocal2.get(i).getMatdt());
//									bond.setDescr(listBondLocal2.get(i).getDescr());
//									bond.setProduct(listBondLocal2.get(i).getProduct());
//									bond.setProdtype(listBondLocal2.get(i).getProdtype());
//									bond.setAccountno(listBondLocal2.get(i).getAccountno());
//									bond.setAcctngtype(listBondLocal2.get(i).getAcctngtype());
//									bond.setBasis(listBondLocal2.get(i).getBasis());
//									bond.setCcy(listBondLocal2.get(i).getCcy());
//									bond.setSettccy(listBondLocal2.get(i).getSettccy());
//									bond.setSettdays(listBondLocal2.get(i).getSettdays());
//									bond.setSecunit(listBondLocal2.get(i).getSecunit());
//									bond.setDenom(listBondLocal2.get(i).getDenom());
//									bond.setIntcalcrule(listBondLocal2.get(i).getIntcalcrule());
//									bond.setIssuer(listBondLocal2.get(i).getIssuer());
//									bond.setCouponrate(listBondLocal2.get(i).getCouponrate());
//									bond.setIntpaymethod(listBondLocal2.get(i).getIntpaymethod());
//									bond.setIntenddaterule(listBondLocal2.get(i).getIntenddaterule());
//									bond.setIntpaycircle(listBondLocal2.get(i).getIntpaycircle());
//									bond.setUserid(SlSessionHelper.getUserId());
//									bond.setStatus("A"); // 设置为未同步
//									ifsOpicsBondMapper.updateById(bond);
//								}
//							}
//						}
					}
				}

		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

}
