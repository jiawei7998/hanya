package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTradeLimit;

import java.util.List;
import java.util.Map;

public interface IfsTradeLimitService {

	 Page<IfsTradeLimit> getDataPage(Map<String, Object> map);

	 void addData(Map<String,Object> map);

	void update(Map<String,Object> map);

	void delData(List<Map<String,Object>> list);
}
