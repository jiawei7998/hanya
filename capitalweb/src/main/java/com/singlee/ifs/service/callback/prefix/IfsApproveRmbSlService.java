package com.singlee.ifs.service.callback.prefix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.ifs.mapper.IfsApprovermbSlMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsApprovermbSl;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/***
 * 债券借贷 回调函数
 * 
 * @author singlee4
 * 
 */

@Service(value = "IfsApproveRmbSlService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApproveRmbSlService extends IfsApproveServiceBase {

	@Autowired
	IfsApprovermbSlMapper ifsCfetsrmbSlMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbSlMapper.updateRmbSlByID(serial_no, status, date);
		IfsApprovermbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(serial_no);
		if (ifsCfetsrmbSl.equals(null)) {
			return;
		}
		IfsFlowMonitor ifsFlowMonitor11 = new IfsFlowMonitor();
		// /////以下将审批中的记录插入审批监控表
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 插入审批监控表，需要整合字段

			ifsFlowMonitor11 = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null == ifsFlowMonitor11) {// 没有相同的id记录才会插入
				IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
				ifsFlowMonitor.setTicketId(ifsCfetsrmbSl.getTicketId());// 设置监控表-内部id
				ifsFlowMonitor.setContractId(ifsCfetsrmbSl.getTicketId());// 设置监控表-成交单编号
				ifsFlowMonitor.setApproveStatus(ifsCfetsrmbSl.getApproveStatus());// 设置监控表-审批状态
				ifsFlowMonitor.setPrdName("债券借贷事前审批");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("sl");// 设置监控表-产品编号：作为分类
				ifsFlowMonitor.setTradeDate(ifsCfetsrmbSl.getPostDate());// 设置监控表-交易日期
				ifsFlowMonitor.setSponsor(ifsCfetsrmbSl.getSponsor());// 设置监控表-审批发起人
				String custNo = ifsCfetsrmbSl.getCounterpartyInstId();
				ifsFlowMonitor.setTradeType("0");// 交易类型，0：事前交易，1：正式交易
				if (null != custNo || "".equals(custNo)) {
					ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
					IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
					ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)
				}
				StringBuffer buffer = new StringBuffer("");
				List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("IFS_APPROVERMB_SLA");
				if (listmap.size() == 1) {
					LinkedHashMap<String, String> hashmap = listmap.get(0);
					String tradeElements = hashmap.get("TRADE_GROUP");
					String commentsGroup = hashmap.get("COMMENTSGROUPS");
					if (tradeElements != "null" || tradeElements != "") {
						buffer.append("{");
						String sqll = "select " + tradeElements + " from IFS_APPROVERMB_SLA where TICKET_ID=" + "'" + serial_no + "'";
						LinkedHashMap<String, Object> swap = ifsCfetsrmbSlMapper.searchProperty(sqll);
						String[] str = tradeElements.split(",");
						String[] comments = commentsGroup.split(",");
						for (int j = 0; j < str.length; j++) {
							String kv = str[j];
							String key = comments[j];
							String value = String.valueOf(swap.get(kv));
							buffer.append(key).append(":").append(value);
							if (j != str.length - 1) {
								buffer.append(",");
							}
						}
						buffer.append("}");
					}
				}
				ifsFlowMonitor.setTradeElements(buffer.toString());
				ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			}

		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)
				|| StringUtils.equals(ApproveOrderStatus.New, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map3 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map3);

		}

		/*
		 * //只有审批状态为'审批通过'才调用存储过程 if(!status.equals("6")){ return; } Map<String, String> map=null;
		 * 
		 * try{ //将实体对象转换成map map = BeanUtils.describe(ifsCfetsrmbSl); //正式调用存储过程 RetBean result = securServer.bredProc(map);//债券借贷存储过程 if(!result.getRetCode().equals("999")){ JY.raise("审批未通过......");
		 * } }catch(Exception e){ JY.raise("审批未通过......",e.getMessage()); }
		 */

	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsApprovermbSl> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbSlMapper.getRmbSlMineListMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbSlMapper.getRmbSlMineList(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbSlMapper.getRmbSlMineListFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> oppoDirList = taDictVoMapper.getTadictTree("oppoDir").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsApprovermbSl ifsApprovermbSl: list) {
			ifsApprovermbSl.setOppoDir(oppoDirList.get(ifsApprovermbSl.getOppoDir()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}

}
