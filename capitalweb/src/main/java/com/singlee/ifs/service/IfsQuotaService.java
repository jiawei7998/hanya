package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsQuota;
import com.singlee.ifs.model.IfsQuotaKey;

import java.util.Map;

/**
 * 用户限额
 * @author 
 *
 */
public interface IfsQuotaService {

	/**
	 * 分页查询
	 * @param params
	 * @return
	 */
	Page<IfsQuota> searchIfsQuotaList(Map<String, Object> params);
	
	/**
	 * 单条查询
	 * @param key
	 * @return
	 */
	IfsQuota selectByPrimaryKey(IfsQuotaKey key);
	
	/**
	 * 保存或者修改
	 * @param ifsQuota
	 * @return
	 */
	int saveOrUpdateIfsQuota(IfsQuota ifsQuota);
	
	/**
	 * 删除
	 * @param key
	 * @return
	 */
	int deleteIfsQuota(IfsQuotaKey key);
}