package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.ChinaMutualFundNAV;
import com.singlee.refund.model.CFtInfo;

import java.util.List;
import java.util.Map;


public interface IfsWdFundService {
    
	
	

	 Page<ChinaMutualFundNAV> getWdFundList(Map<String, Object> map);


	List<ChinaMutualFundNAV> getWdFundAll(Map<String, Object> map);



	/**
	 *  先查询，后插入
	 */
	String doFundExcelToData(List<CFtInfo> cFtInfoBean);


}
