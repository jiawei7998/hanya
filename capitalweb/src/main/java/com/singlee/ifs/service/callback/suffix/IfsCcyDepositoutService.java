package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsFxDepositoutMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsFxDepositout;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service(value = "IfsCcyDepositoutService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsCcyDepositoutService extends IfsApproveServiceBase {

    @Autowired
    IfsFxDepositoutMapper ifsFxDepositoutMapper;
    @Autowired
    TaSysParamMapper taSysParamMapper;// 系统参数mapper
    @Autowired
    IBaseServer baseServer;// 查询opics系统时间
    @Autowired
    private IMmServer mmServer;
    @Autowired
    TaUserMapper taUserMapper;


    @Override
    public void statusChange(String flow_type, String flow_id, String serial_no, String status,
                             String flowCompleteType) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
        String date = dateFormat.format(now);
        ifsFxDepositoutMapper.updatefxDepositByID(serial_no, status, date);
        IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchByTicketId(serial_no);
        // 0.1 当前业务为空则不进行处理
        if (null == ifsFxDepositout) {
            return;
        }

        // 1.交易审批中状态,需要将交易纳入监控审批
        if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
            // 插入审批监控表，需要整合字段
            IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
                ifsFlowMonitorMapper.deleteById(serial_no);
            }
            // 1.2 保存监控表数据
            IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
            ifsFlowMonitor.setTicketId(ifsFxDepositout.getTicketId());// 设置监控表-内部id
            ifsFlowMonitor.setContractId(ifsFxDepositout.getContractId());// 设置监控表-成交单编号
            ifsFlowMonitor.setApproveStatus(ifsFxDepositout.getApproveStatus());// 设置监控表-审批状态
            ifsFlowMonitor.setPrdName("存放同业(外币)");// 设置监控表-产品名称
            ifsFlowMonitor.setPrdNo("ccyDepositout");// 设置监控表-产品编号：作为分类
            ifsFlowMonitor.setTradeDate(ifsFxDepositout.getForDate());// 设置监控表-交易日期
            ifsFlowMonitor.setSponsor(ifsFxDepositout.getSponsor());// 设置监控表-审批发起人
            ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
            String custNo = ifsFxDepositout.getCustId();
            if (null != custNo || "".equals(custNo)) {
                ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
                IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
                ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
            }
            ifsFlowMonitorMapper.insert(ifsFlowMonitor);

        }
        // 3.0 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表
        if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
                || StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
                || StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
            IfsFlowMonitor monitor = new IfsFlowMonitor();
            monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
            monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
            ifsFlowMonitorMapper.deleteById(serial_no);
            Map<String, Object> map1 = BeanUtil.beanToMap(monitor);
            ifsFlowCompletedMapper.insert(map1);
        }
        // 3.1未审批通过则不导入Opics系统
        if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
            return;
        }
        try {
            // 3.2获取字典中的配置
            Map<String, Object> sysMap = new HashMap<String, Object>();
            sysMap.put("p_code", "000002");// 参数代码
            sysMap.put("p_value", "ifJorP");// 参数值
            List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
            if (sysList.size() != 1) {
                JY.raise("系统参数有误......");
            }
            if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
                JY.raise("系统参数[参数类型]为空......");
            }
            String callType = sysList.get(0).getP_type().trim();
            // 3.3调用java代码
            if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
                // 获取当前账务日期
                Date branchDate = baseServer.getOpicsSysDate(ifsFxDepositout.getBr());
                // 导入opics中
                SlOutBean result = mmServer.mm(getEntry(ifsFxDepositout, branchDate));
                if (!result.getRetStatus().equals(RetStatusEnum.S)) {
                    JY.raise("审批未通过，插入opcis表失败......");
                } else {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("ticketId", ifsFxDepositout.getTicketId());
                    map.put("satacode", "-1");
                    ifsFxDepositoutMapper.updateStatcodeByTicketId(map);
                }
            } else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {
                Map<String, String> map = BeanUtils.describe(ifsFxDepositout);
                SlOutBean result = mmServer.idldProc(map);// 本币信用拆借存储过程
                if (!"999".equals(result.getRetCode())) {
                    JY.raise("审批未通过，执行存储过程失败......");
                }
            } else {
                JY.raise("系统参数未配置,未进行交易导入处理......");
            }
        } catch (Exception e) {
            JY.raiseRException("审批未通过......", e);
        }
    }

    /**
     * 获取交易实体
     *
     * @return
     */
    private SlDepositsAndLoansBean getEntry(IfsFxDepositout ifsFxDepositout, Date branchDate) {
        SlDepositsAndLoansBean mmBean = new SlDepositsAndLoansBean(ifsFxDepositout.getBr(), SlDealModule.DL.SERVER,
                SlDealModule.DL.TAG, SlDealModule.DL.DETAIL);
        InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
//		IfsOpicsCust cust = custMapper.searchIfsOpicsCust(ifsRmbDepositout.getRemoveInst());
        contraPatryInstitutionInfo.setInstitutionId(ifsFxDepositout.getCustId());
        mmBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
        // 本方交易员
        InstitutionBean institutionInfo = new InstitutionBean();
        Map<String, Object> userMap = new HashMap<String, Object>();
        userMap.put("userId", ifsFxDepositout.getSponsor());// 交易员id
        userMap.put("branchId", ifsFxDepositout.getBranchId());// 机构
        TaUser user = taUserMapper.queryUserByMap(userMap);
        String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.DL.IOPER);
        institutionInfo.setTradeId(trad);
        mmBean.getInthBean().setIoper(trad);
        mmBean.setInstitutionInfo(institutionInfo);
        SlExternalBean externalBean = new SlExternalBean();
        externalBean.setCost(ifsFxDepositout.getCost());
        externalBean.setProdcode(ifsFxDepositout.getProduct());
        externalBean.setProdtype(ifsFxDepositout.getProdType());
        externalBean.setPort(ifsFxDepositout.getPort());
        externalBean.setAuthsi(SlDealModule.DL.AUTHSI);
        externalBean.setBroker(SlDealModule.BROKER);
        externalBean.setSiind(SlDealModule.DL.SIIND);
        externalBean.setSupconfind(SlDealModule.DL.SUPCONFIND);
        externalBean.setSuppayind(SlDealModule.DL.SUPPAYIND);
        externalBean.setSuprecind(SlDealModule.DL.SUPRECIND);
        externalBean.setVerind(SlDealModule.DL.VERIND);
        externalBean.setDealtext(ifsFxDepositout.getContractId());
        externalBean.setCustrefno(ifsFxDepositout.getTicketId());
        // 设置清算路径1
        externalBean.setCcysmeans(ifsFxDepositout.getCcysmeans());
        // 设置清算账户1
        externalBean.setCcysacct(ifsFxDepositout.getCcysacct());
        // 设置清算路径2
        externalBean.setCtrsmeans(ifsFxDepositout.getCtrsmeans());
        // 设置清算账户2
        externalBean.setCtrsacct(ifsFxDepositout.getCtrsacct());
        mmBean.setExternalBean(externalBean);
        SlInthBean inthBean = mmBean.getInthBean();
        inthBean.setFedealno(ifsFxDepositout.getTicketId());
        inthBean.setLstmntdate(branchDate);
        mmBean.setInthBean(inthBean);
        double amt = ifsFxDepositout.getAmt().doubleValue() * 10000;
        mmBean.setAmt(BigDecimal.valueOf(MathUtil.roundWithNaN(amt, 2)));
        mmBean.setValueDate(DateUtil.parse(ifsFxDepositout.getFirstSettlementDate()));
        mmBean.setMaturityDate(DateUtil.parse(ifsFxDepositout.getSecondSettlementDate()));
        mmBean.setDealDate(DateUtil.parse(ifsFxDepositout.getForDate()));
        mmBean.setTenor("99");
        mmBean.setIntdatedir("VF");
        mmBean.setCommtypecode("S");
        mmBean.setCcy(ifsFxDepositout.getCurrency());
        mmBean.setBasis(ifsFxDepositout.getBasis());
        mmBean.setRateCode(ifsFxDepositout.getBasisRateCode());
        mmBean.setIntrate(String.valueOf(ifsFxDepositout.getRate()));
        mmBean.setCommmeans(ifsFxDepositout.getCcysmeans());
        mmBean.setCommacct(ifsFxDepositout.getCcysacct());
        mmBean.setMatmeans(ifsFxDepositout.getCtrsmeans());
        mmBean.setMatacct(ifsFxDepositout.getCtrsacct());
        mmBean.setIntcalcrule("Q");
        mmBean.setIntsmeans(ifsFxDepositout.getCtrsmeans());
        mmBean.setIntsacct(ifsFxDepositout.getCtrsacct());

//        mmBean.setSpread(BigDecimal.valueOf(0));//TODO 重复
//        mmBean.setIntdaterule("D");//TODO 重复
//        mmBean.setIntpaycycle("Q");//TODO 重复

//        mmBean.setScheduleType(ifsRmbDepositout.getScheduleType());
        mmBean.setIntPayCycle(ifsFxDepositout.getPaymentFrequency());
//        mmBean.setIntPayDay(ifsRmbDepositout.getIntpayday());
        mmBean.setIntDateRule(ifsFxDepositout.getIntdaterule());
        double Spread8 = ifsFxDepositout.getBenchmarkSpread() == null ? BigDecimal.valueOf(0).doubleValue() :
                ifsFxDepositout.getBenchmarkSpread().divide(new BigDecimal(100)).doubleValue();
        mmBean.setSpread8(BigDecimal.valueOf(MathUtil.roundWithNaN(Spread8,6)));
//		mmBean.setSpread8(BigDecimal.valueOf(ifsRmbDepositout.getSpread8()));
        return mmBean;
    }
}
