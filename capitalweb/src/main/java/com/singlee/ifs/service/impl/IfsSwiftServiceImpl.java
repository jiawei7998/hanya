package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.generated.model.field.Field108;
import com.singlee.generated.model.field.Field20;
import com.singlee.generated.model.field.Field21;
import com.singlee.generated.model.field.Field79;
import com.singlee.generated.model.mt.mt2xx.MT299;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.ifs.model.IfsSwift;
import com.singlee.ifs.service.IfsSwiftCallBackService;
import com.singlee.ifs.service.IfsSwiftService;
import com.singlee.swift.io.PPCReader;
import com.singlee.swift.model.*;
import com.singlee.swift.model.SwiftBlock2.MessagePriority;
import com.singlee.swift.model.mt.ServiceIdType;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("ifsSwiftServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsSwiftServiceImpl implements IfsSwiftService ,IfsSwiftCallBackService{
	@Autowired
	IfsSwiftMapper ifsSwiftMapper;

	@Override
	public Page<IfsSwift> getHandling(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsSwift> result = ifsSwiftMapper.getHandling(map, rowBounds);
		return result;
	}
	
	//获取删除复核状态报文信息
	@Override
	public Page<IfsSwift> getVerifyHandling(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsSwift> result = ifsSwiftMapper.getVerifyHandling(map, rowBounds);
		return result;
	}

	@Override
	public void updateSwiftStatus(Date tm, String batchNo, String userid, String verind) {
		if ("2".equals(verind)) {
			ifsSwiftMapper.updateSwiftApproveStatus(tm, batchNo, userid, verind);
		} else {
			ifsSwiftMapper.updateSwiftStatus(tm, batchNo, userid, verind);
		}
	}

	@Override
	public void deleteSwift(Map<String, Object> map) {
		ifsSwiftMapper.deleteSwift(map);
	}
	
	@Override
	public void updateSwiftDelete(Map<String, Object> map) {
		ifsSwiftMapper.updateSwiftDelete(map);
	}
	
	@Override
	public void updateSwiftComfirm(Map<String, Object> map) {
		ifsSwiftMapper.updateSwiftComfirm(map);
	}
    /**
     * 解析并保存返回ack报文
     */
	@Override
	public void swiftSave(InputStream in) {
		Map<String,Object> pmap=new HashMap<String, Object>();
		 try {
				Iterable<String> swiftIb = new PPCReader(in);//获得本地保存ack
				
				for (Iterator iterator=swiftIb.iterator();iterator.hasNext();) {
					SwiftMessage swiftMsg=SwiftMessage.parse((String)iterator.next());
					String check=swiftMsg.isAck()?"0":swiftMsg.isNack()?"1":swiftMsg.isOutgoing()?"2":null;//获得ack处理状态
					swiftMsg=SwiftMessage.parse(swiftMsg.getUnparsedTexts().getText(0));
					String dealno=StringUtils.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field20.NAME))?swiftMsg.getBlock4().getTagValue(Field20.NAME).substring(2,9):null;//获得dealno
					String msgType=swiftMsg.getBlock2().getMessageType();//获得报文类型
					System.out.println("Ack:"+msgType+";"+dealno+"\n"+swiftMsg);
					pmap.put("msgType", msgType);
					pmap.put("dealno", dealno);
					pmap.put("check", check);
					ifsSwiftMapper.updateStatu(pmap);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
  /**
   * 新增299报文
   */
	@Override
	public RetMsg<Serializable> swiftBondCreat(Map<String, Object> map) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		String dealNo=ParameterUtil.getString(map, "dealNo", "");
		String sendBank=ParameterUtil.getString(map, "sendBank", "");
		String recivBank=ParameterUtil.getString(map,"recivBank", "");
		String refNo=ParameterUtil.getString(map,"refNo", "");
		String msg=ParameterUtil.getString(map,"msg", "");
		map.put("msgType", MT299.NAME);
		map.put("dealNo", dealNo.trim());
		List<SlSwiftBean> list=ifsSwiftMapper.getMessage(map);
		/**********************299报文内容**************************/
		MT299 mt299=new MT299();
		SwiftBlock1 block1 = new SwiftBlock1();
		SwiftBlock3 b3 = new SwiftBlock3();
	    b3.append(new Field108("OPX"));
		SwiftBlock4 block4=new SwiftBlock4();
		block1.setApplicationId(SwiftBlock1.APPLICATION_ID_FIN);
	    block1.setServiceId(ServiceIdType._01.number());
	    SwiftBlock2Input block2Input =new SwiftBlock2Input();
	    block2Input.setMessageType(MT299.NAME);
	    block1.setLogicalTerminal(new LogicalTerminalAddress(sendBank).getSenderLogicalTerminalAddress());
	    block1.setSessionNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSessionNum(), 4, "0"));
	    block1.setSequenceNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSequenceNum(),6, "0"));
	    block2Input.setReceiverAddress(new LogicalTerminalAddress(recivBank).getReceiverLogicalTerminalAddress());
	    block2Input.setMessagePriority(MessagePriority.N.name());
	    //控制长度不能超过16位
		block4.append(new Field20(MessageFormat.format("DLCB{0}",dealNo.length() > 16?refNo.substring(0, 16):dealNo)));					
		block4.append(new Field21(refNo.length() > 16?refNo.substring(0, 16):refNo));
		List<String> resultMsg = splitComponents(msg.toString(),50);//按照48个字符分割
		resultMsg = (resultMsg.size() > 34) ? resultMsg.subList(0, 34) : resultMsg;//只截取35行超出部分舍去.
		//去掉换行
		Field79 field = new Field79();
		field.setComponents(resultMsg);
		block4.append(field);
		mt299.getSwiftMessage().setBlock1(block1);
		mt299.getSwiftMessage().setBlock2(block2Input);
		mt299.getSwiftMessage().setBlock3(b3);
		mt299.getSwiftMessage().setBlock4(block4);
		/**********************299报文内容**************************/
		//去掉空格
		map.put("msg", mt299.message().replaceAll("((\r\n)|\n)[\\s\t ]*(\\1)+", "$1").replaceAll("^((\r\n)|\n)", ""));
		map.put("msgType", MT299.NAME);
		map.put("prodcode", "SECUR");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		map.put("cDate", sdf1.format(new Date()));
		map.put("iDate", sdf.format(new Date()));
		map.put("swftcDate", sdf1.format(new Date()));
		map.put("iOper", SlSessionHelper.getUserId());
		
		if(list.size()>0){
			map.put("batchNo",list.get(0).getBatchNo());
		   ifsSwiftMapper.submitSwiftBond(map);
		   ret.setDesc("交易编号为"+dealNo+"的299报文已修改成功");
		   return ret;
		}else{
			String batchNo=DateUtil.getCurrentDateAsString("yyMMdd")+DateUtil.getCurrentTimeAsString("mmssSSS").substring(DateUtil.getCurrentTimeAsString("mmssSSS").length()-6);
			map.put("batchNo", batchNo);
			ifsSwiftMapper.insertSwift(map);
			ret.setDesc("299报文生成成功");
			return ret;
		}
		
	}
	
		
	public static List<String> splitComponents(String str, int splitLen) {
	    int len=(str.length() % splitLen==0)?str.length() / splitLen:str.length() / splitLen + 1;
	    List<String> result = new ArrayList<String>(len);
	    Pattern pattern = Pattern.compile(".{1," + splitLen + "}(=?\\s|$)");
	    System.out.println(pattern);
	    Matcher matcher = pattern.matcher(str);
	    System.out.println(matcher);
	    while (matcher.find()) {
	      result.add(matcher.group());
	    }
	    return result;
	  }
		
		
	/**
	 * 299报文查询
	 */
	@Override
	public Map<String, Object> getSwiftBond(Map<String, Object> map) {
		String dealNo=ParameterUtil.getString(map, "dealNo", "");
		map.put("dealNo", dealNo.trim());
		List<SlSwiftBean> list=ifsSwiftMapper.getMessage(map);
		Map<String,Object> remap=new HashMap<String,Object>();
		if(list.size()>0){
			SlSwiftBean swiftBond=list.get(0);
			try {
				SwiftMessage swiftMsg = SwiftMessage.parse(swiftBond.getMsg());
				remap.put("sendBank", swiftMsg.getSender());
				remap.put("recivBank", swiftMsg.getReceiver());
				remap.put("dealNo", swiftBond.getDealNo());
				remap.put("refNo", swiftMsg.getBlock4().getTagValue(Field21.NAME));
				remap.put("msg",swiftMsg.getBlock4().getTagValue(Field79.NAME));
				remap.put("settflag", swiftBond.getSettFlag());
				return remap;
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return remap;
	}
}
