package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.ifs.model.IfsTrdSettle;
import org.springframework.remoting.RemoteConnectFailureException;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public interface IfsTrdSettleService {
	public Page<IfsTrdSettle> getTrdSettleList(Map<String, Object> map);

	public IfsTrdSettle queryTrdSettleByDealNo(Map<String, Object> param) throws Exception;

	public RetMsg<Serializable> submitSettle(Map<String, Object> map) throws ParseException;

	public RetMsg<Serializable> validSettleState(IfsTrdSettle trdSettle) throws Exception;

	public int updateTrdSettle(Map<String, Object> param) throws Exception;
	
	public RetMsg<Serializable> backSettle(HashMap<String, Object> paramMap);
	
	public Page<IfsTrdSettle> getTrdSettleCount(Map<String, Object> map);

	public IfsTrdSettle getSettleCnaps(String fedealno);

	public Page<IfsTrdSettle> queryAllTrdSettle(Map<String, Object> map);

	public Page<IfsTrdSettle> queryRMBTrdSettle(Map<String, Object> map);

	public Page<IfsTrdSettle> queryFXTrdSettle(Map<String, Object> map);

	
	public Boolean sendCheck(Map<String, Object> param) throws RemoteConnectFailureException, Exception;
}
