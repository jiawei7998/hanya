package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsRmbDepositinMapper;
import com.singlee.ifs.model.IfsRmbDepositin;
import com.singlee.ifs.service.IfsRmbDepositInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Service
public class IfsRmbDepositInServiceImpl implements IfsRmbDepositInService {

    @Autowired
    IfsRmbDepositinMapper ifsRmbDepositinMapper;

    @Override
    public Page<IfsRmbDepositin> selectAdjustDepositPage(Map<String, Object> map) {
        Page<IfsRmbDepositin> ifsRmbDepositins = ifsRmbDepositinMapper.selectAdjustDepositPage(map, ParameterUtil.getRowBounds(map));
        return ifsRmbDepositins;
    }

    @Override
    public void updateDepositAdjust() {

    }
}
