package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.generated.model.field.*;
import com.singlee.ifs.mapper.IfsSwiftSettleInfoMapper;
import com.singlee.ifs.model.IfsSwiftSettleInfo;
import com.singlee.ifs.service.IfsSwiftCallBackService;
import com.singlee.ifs.service.IfsSwiftSettleService;
import com.singlee.swift.io.PPCReader;
import com.singlee.swift.model.SwiftBlock2Output;
import com.singlee.swift.model.SwiftMessage;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

@Service("ifsSwiftSettleServiceImpl")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsSwiftSettleServiceImpl implements IfsSwiftSettleService ,IfsSwiftCallBackService{
	//private   static List<String> arrayA=new ArrayList<String>(Arrays.asList("103","202","910"));//32	A
	//private   static List<String> arrayB=new ArrayList<String>(Arrays.asList("210","292","360","305"));//32B
	
	@Autowired
     IfsSwiftSettleInfoMapper ifsSwiftSettleMapper; 

	@Override
	public Page<IfsSwiftSettleInfo> getSetleInfo(Map<String, Object> map) {
		String msgtype=ParameterUtil.getString(map, "msgtype" , "" );
		if(StringUtil.isNotEmpty(msgtype)&msgtype.indexOf("MT")<0){
			map.put("msgtype", "MT"+msgtype);
		}
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsSwiftSettleInfo> result = ifsSwiftSettleMapper.getSetleInfo(map, rowBounds);
		return result;
	}

	@Override
	public void swiftSave(InputStream in) {
	  try {
			Iterable<String> swiftIb=new PPCReader(in);
			for(Iterator itertor=swiftIb.iterator();itertor.hasNext();){
				IfsSwiftSettleInfo SwiftSettleInfo=new IfsSwiftSettleInfo();
				String str=(String) itertor.next();
				SwiftSettleInfo.setMsg(str);
				SwiftMessage swiftMsg = SwiftMessage.parse(str);
				SwiftSettleInfo.setSwiftStatus(swiftMsg.getBlock4().getTagValue(Field451.NAME));//ack报文状态
				swiftMsg=SwiftMessage.parse(swiftMsg.getUnparsedTexts().getText(0));
				if(swiftMsg.isOutput()){//判断是否取回报文
					SwiftBlock2Output out=(SwiftBlock2Output) swiftMsg.getBlock2();
					SwiftSettleInfo.setMsgtype("MT"+swiftMsg.getBlock2().getMessageType());//报文类型
					SwiftSettleInfo.setReciverBank(swiftMsg.getReceiver());//收报行
					SwiftSettleInfo.setSendBank(swiftMsg.getSender());//发报行
					SwiftSettleInfo.setSwiftflag("O");//报文	io标识
					SwiftSettleInfo.setSendDate(out.getReceiverOutputDate());//收报行发报日期
					if(StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field20.NAME))){
						SwiftSettleInfo.setTag20(swiftMsg.getBlock4().getTagValue(Field20.NAME));
					}
					if(StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field21.NAME))){
						SwiftSettleInfo.setTag21(swiftMsg.getBlock4().getTagValue(Field21.NAME));
					}
					if(StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field32A.NAME))){
							String curAmt=swiftMsg.getBlock4().getTagValue(Field32A.NAME);
							SwiftSettleInfo.setCcy(curAmt.substring(6,9));
							if(curAmt.endsWith(",")){
								SwiftSettleInfo.setAmount(curAmt.substring(9,curAmt.length()-1));
							}else{
								SwiftSettleInfo.setAmount(curAmt.substring(9,curAmt.length()));
							}
					}else if(StringUtil.isNotEmpty(swiftMsg.getBlock4().getTagValue(Field32B.NAME))){
							String curAmt=swiftMsg.getBlock4().getTagValue(Field32B.NAME);
							SwiftSettleInfo.setCcy(curAmt.substring(0,3));
							if(curAmt.endsWith(",")){
								SwiftSettleInfo.setAmount(curAmt.substring(3,curAmt.length()-1));
							}else{
								SwiftSettleInfo.setAmount(curAmt.substring(3,curAmt.length()));
							}
				    }
					//保存解析报文信息
					ifsSwiftSettleMapper.insertSwiftSettle(SwiftSettleInfo);
				}
			 }
		 } catch (Exception e) {
				e.printStackTrace();
			}
	  }
}
