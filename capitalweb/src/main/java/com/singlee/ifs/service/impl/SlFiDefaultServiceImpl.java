package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.mapper.SlFiDefaultMapper;
import com.singlee.ifs.model.SlFiDefault;
import com.singlee.ifs.service.SlFiDefaultService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SlFiDefaultServiceImpl.java
 * @Description 债券违约实现类
 * @createTime 2021年09月29日 17:01:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlFiDefaultServiceImpl implements SlFiDefaultService {
    @Autowired
    SlFiDefaultMapper slFiDefaultMapper;


    @Override
    public PageInfo<SlFiDefault> searchPageSlFi(Map<String, Object> map) {
        RowBounds rb = ParameterUtil.getRowBounds(map);
        Page<SlFiDefault> page = slFiDefaultMapper.searchPage(map, rb);
        return new PageInfo<SlFiDefault>(page);
    }

    @Override
    public String insert(SlFiDefault entity) {
        String code="成功";
        SlFiDefault slFiDefault=slFiDefaultMapper.selectByPrimaryKey(entity);
        if(slFiDefault!=null){
            code="该类型的债券已经存在，请重新选择";
        }
       else {
            slFiDefaultMapper.insert(entity);
        }
       return code;
    }

    @Override
    public void updateById(SlFiDefault entity) {
        slFiDefaultMapper.deleteById(entity);

    }

    @Override
    public void deleteById(SlFiDefault entity) {
        slFiDefaultMapper.deleteById(entity);
    }
}
