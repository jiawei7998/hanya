package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.hrbreport.util.HrbReportUtils;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.model.IfsEsbMassage;
import com.singlee.ifs.service.IfsEsbMassageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;



@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsEsbMassageServiceImpl implements IfsEsbMassageService{
    @Autowired
    IfsEsbMassageMapper ifsEsbMassageMapper;
    
    @Override
    public Page<IfsEsbMassage> getEsbMassagesList(Map<String, Object> map) {
        List<IfsEsbMassage> list=ifsEsbMassageMapper.getEsbMassage(map);
        
        int pageNumber=(int)map.get("pageNumber");
        int pageSize=(int)map.get("pageSize");
        
        Page<IfsEsbMassage> page=HrbReportUtils.producePage(list, pageNumber, pageSize);
        
        return page;
    }




}
