package com.singlee.ifs.service.impl;

import com.singlee.financial.cdtc.hrb.HrbCdtcTestConnectServer;
import com.singlee.financial.pojo.RetBean;
import com.singlee.ifs.service.IfsCdtcTestConnectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCdtcTestConnectServiceImpl.java
 * @Description 中债测试连通性
 * @createTime 2021年11月25日 14:23:00
 */
@Service
public class IfsCdtcTestConnectServiceImpl implements IfsCdtcTestConnectService {
    @Autowired
    HrbCdtcTestConnectServer hrbCdtcTestConnectServer;


    @Override
    public String createHertMessageXml(Map<String, Object> paramer) {
        RetBean retBean = new RetBean();
        try {
             retBean = hrbCdtcTestConnectServer.createHertMessageXml(paramer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(retBean!=null){
            return retBean.getRetMsg();
        }else {
            return "创建失败";
        }

    }

    @Override
    public String sendHertMessageXml(Map<String, Object> map) {
        RetBean retBean = new RetBean();
        try {
            retBean = hrbCdtcTestConnectServer.sendHertMessageXml(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(retBean!=null){
            return retBean.getRetMsg();
        }else {
            return "创建失败";
        }
    }
}
