package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsCustSettleMapper;
import com.singlee.ifs.model.IfsCustSettle;
import com.singlee.ifs.service.IfsCustSettleService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class IfsCustSettleServiceImpl implements IfsCustSettleService {
	@Autowired
	IfsCustSettleMapper ifsCustSettleMapper;
	@Override
	public Page<IfsCustSettle> getCSList(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<IfsCustSettle> page = ifsCustSettleMapper.getCSList(map,rowBounds);
		return page;
	}
	@Override
	public void updateByCno(Map<String, Object> map) {
		ifsCustSettleMapper.updateByCno(map);
		
	}
	@Override
	public IfsCustSettle selectByCno(Map<String, Object> map) {
		return ifsCustSettleMapper.selectByCno(map);
	}
	@Override
	public void addCustSettle(Map<String, Object> map) {
		IfsCustSettle acct=ifsCustSettleMapper.selectMaxSeqByCno(map);
		if(acct==null){
			map.put("seq", "0");
			map.put("dealFlag", "1");
			
		}else{
			int num=Integer.valueOf(acct.getSeq());
			map.put("seq", String.valueOf(num+1));
		}
		ifsCustSettleMapper.addCustSettle(map);
		
	}
	@Override
	public void deleteSettle(Map<String, Object> map) {
		// TODO Auto-generated method stub
		ifsCustSettleMapper.deleteSettle(map);
	}
	@Override
	public IfsCustSettle selectById(Map<String, Object> map) {
		return ifsCustSettleMapper.selectById(map);
	}
	@Override
	public List<IfsCustSettle> getListByCno(Map<String, Object> map) {
		return ifsCustSettleMapper.getListByCno(map);
	}
	@Override
	public void updateIsdefaultById(Map<String, Object> map) {
		ifsCustSettleMapper.updateIsdefaultById(map);
	}
	@Override
	public void updateDealFlagById(Map<String, Object> map) {
		if("1".equals(map.get("dealFlag"))){//经办
			ifsCustSettleMapper.updateManageDealFlagById(map);
		}else{//复核或退回经办
			ifsCustSettleMapper.updateDealFlagById(map);
		}
		
		
	}
	
}
