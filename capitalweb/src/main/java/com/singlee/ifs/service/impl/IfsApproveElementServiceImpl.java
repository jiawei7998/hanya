package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsApproveElementMapper;
import com.singlee.ifs.model.IfsApproveElement;
import com.singlee.ifs.service.IfsApproveElementService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApproveElementServiceImpl implements IfsApproveElementService {
	@Autowired
	private IfsApproveElementMapper ifsLimitConditionMapper;

	@Override
	public Page<IfsApproveElement> searchPageLimit(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsApproveElement> result = ifsLimitConditionMapper.searchPageLimit(map, rb);
		return result;
	}

	@Override
	public void update(IfsApproveElement entity) {
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		ifsLimitConditionMapper.update(entity);
	}

	@Override
	public void insert(IfsApproveElement entity) {
		entity.setInputTime(DateUtil.getCurrentDateTimeAsString());
		String id = "id" + DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setId(id);
		ifsLimitConditionMapper.insert(entity);
	}

	@Override
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		ifsLimitConditionMapper.deleteById(id);
	}

	@Override
	public IfsApproveElement searchIfsLimitCondition(String id) {
		IfsApproveElement entity = ifsLimitConditionMapper.searchIfsLimitCondition(id);
		return entity;

	}

	@Override
	public List<Map<String, Object>> searchColumns(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsLimitConditionMapper.searchColumns(map);
		return list;
	}

	@Override
	public List<Map<String, Object>> searchTables(Map<String, Object> map) {
		List<Map<String, Object>> list = ifsLimitConditionMapper.searchTables(map);
		return list;
	}

}
