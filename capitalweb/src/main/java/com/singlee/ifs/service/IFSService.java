package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.page.PageInfo;
import com.singlee.ifs.model.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 用于处理第三方接口的服务接口表单录入功能
 * 
 * @author shenzl
 * 
 */
public interface IFSService {

	BigDecimal getMarginRatio(Map<String,Object> map);

	IfsDownPaket getDownPaket(Map<String, Object> map);

	List<IfsDownPaket> getDownPakets(Map<String, Object> map);

	/*
	 * 货币掉期
	 */

	Page<IfsCfetsfxCswap> searchCcySwapPage(Map<String, Object> map);

	void deleteCcySwap(String ticketid);

	String addCcySwap(IfsCfetsfxCswap map);

	void editCcySwap(IfsCfetsfxCswap map);

	IfsCfetsfxCswap searchCcySwap(String ticketid);

	IfsCfetsfxCswap getCcySwapAndFlowIdById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxCswap> getCcySwapMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外币拆借
	 */

	Page<IfsCfetsfxLend> searchCcyLendingPage(Map<String, Object> map);

	void deleteCcyLending(String ticketid);

	void editCcyLending(IfsCfetsfxLend map);

	String addCcyLending(IfsCfetsfxLend map);

	IfsCfetsfxLend searchCcyLending(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxLend> getCcyLendingMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外汇对即期
	 */

	Page<IfsCfetsfxOnspot> searchCcyOnSpotPage(Map<String, Object> map);

	void deleteCcyOnSpot(String ticketid);

	String addCcyOnSpot(IfsCfetsfxOnspot map);

	void editCcyOnSpot(IfsCfetsfxOnspot map);

	IfsCfetsfxOnspot searchCcyOnSpot(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外汇对即期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxOnspot> getCcyOnSpotMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外汇远期
	 */
	Page<IfsCfetsfxFwd> getCreditEdit(Map<String, Object> map);

	IfsCfetsfxFwd getCredit(String ticketId);

	String addCreditCondition(IfsCfetsfxFwd map);

	void editCredit(IfsCfetsfxFwd map);

	void deleteCredit(String ticketId);

	IfsCfetsfxFwd getFwdAndFlowIdById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-外汇远期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxFwd> getFxFwdMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外汇掉期
	 */

	Page<IfsCfetsfxSwap> getswapPage(Map<String, Object> map);

	IfsCfetsfxSwap getrmswap(String ticketId);

	String addrmswap(IfsCfetsfxSwap map);

	void editrmswap(IfsCfetsfxSwap map);

	void deletermswap(String ticketId);

	IfsCfetsfxSwap getRmbSwapAndFlowIdById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxSwap> getFxSwapMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外汇即期
	 */

	Page<IfsCfetsfxSpt> getSpotPage(Map<String, Object> map);

	IfsCfetsfxSpt getSpot(String ticketId);

	String addSpot(IfsCfetsfxSpt map);

	void editSpot(IfsCfetsfxSpt map);

	void deleteSpot(String ticketId);

	IfsCfetsfxSpt getSpotAndFlowIdById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-外汇即期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxSpt> getSpotMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 人民币期权
	 */

	Page<IfsCfetsfxOption> searchOptionPage(Map<String, Object> map);

	IfsCfetsfxOption searchOption(String ticketId);

	String addOption(IfsCfetsfxOption map);

	void editOption(IfsCfetsfxOption map);

	void deleteOption(String ticketId);

	/**
	 * 根据请求参数查询分页列表-人民币期权-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxOption> getOptionMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 黄金拆借
	 */
	Page<IfsCfetgoldLend> searchGoldLendPage(Map<String, Object> map);

	void deleteGoldLend(String ticketid);

	String addGoldLend(IfsCfetgoldLend map);

	void editGoldLend(IfsCfetgoldLend map);

	IfsCfetgoldLend searchGoldLend(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币头寸调拨-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetgoldLend> getGoldLendMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外币头寸调拨
	 */

	Page<IfsCfetsfxAllot> searchIfsAllocatePage(Map<String, Object> map);

	void deleteIfsAllocate(String ticketid);

	String addIfsAllocate(IfsCfetsfxAllot map);

	void editIfsAllocate(IfsCfetsfxAllot map);

	IfsCfetsfxAllot searchIfsAllocate(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币头寸调拨-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxAllot> getAllocateMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 外币债
	 */

	Page<IfsCfetsfxDebt> searchIfsDebtPage(Map<String, Object> map);

	void deleteIfsDebt(String ticketid);

	String addIfsDebt(IfsCfetsfxDebt map);

	void editIfsDebt(IfsCfetsfxDebt map);

	IfsCfetsfxDebt searchIfsDebt(String ticketid);

	/**
	 * 根据请求参数查询分页列表-外币掉期-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsfxDebt> getDebtMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 黄金即远掉期
	 */
	String insertGold(IfsCfetsmetalGold record);

	void updateGold(IfsCfetsmetalGold record);

	void deleteGold(IfsCfetsmetalGoldKey key);

	Page<IfsCfetsmetalGold> getGoldList(IfsCfetsmetalGold record);

	IfsCfetsmetalGold getGoldById(IfsCfetsmetalGoldKey key);

	IfsCfetsmetalGold getGoldAndFlowById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsmetalGold> getMetalGoldMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 现券买卖CBT
	 */
	String insertCbt(IfsCfetsrmbCbt record);

	void updateCbt(IfsCfetsrmbCbt record);

	void deleteCbt(IfsCfetsrmbCbtKey key);

	Page<IfsCfetsrmbCbt> getCbtList(IfsCfetsrmbCbt record);

	IfsCfetsrmbCbt getCbtById(IfsCfetsrmbCbtKey key);

	/* 增加： 根据id获取实体对象和流程id */
	IfsCfetsrmbCbt getCbtAndFlowIdById(Map<String, Object> key);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbCbt> getRmbCbtMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 质押式回购CR
	 */
	String insertCr(IfsCfetsrmbCr record);

	void updateCr(IfsCfetsrmbCr record);

	void deleteCr(IfsCfetsrmbCrKey key);

	Page<IfsCfetsrmbCr> getCrList(IfsCfetsrmbCr record);

	IfsCfetsrmbCr getCrById(IfsCfetsrmbCrKey key);

	IfsCfetsrmbCr searchCfetsrmbCrByTicketId(String ticketId);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbCr> getRmbCrMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 信用拆借IBO
	 */
	String insertIbo(IfsCfetsrmbIbo record);

	void updateIbo(IfsCfetsrmbIbo record);

	void deleteIbo(IfsCfetsrmbIboKey key);

	Page<IfsCfetsrmbIbo> getIboList(IfsCfetsrmbIbo record);

	IfsCfetsrmbIbo getIboById(IfsCfetsrmbIboKey key);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbIbo> getRmbIboMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 利率互换IRS
	 */
	String insertIrs(IfsCfetsrmbIrs record);

	void updateIrs(IfsCfetsrmbIrs record);

	void deleteIrs(IfsCfetsrmbIrsKey key);

	Page<IfsCfetsrmbIrs> getIrsList(IfsCfetsrmbIrs record);

	IfsCfetsrmbIrs getIrsById(IfsCfetsrmbIrsKey key);

	/* 增加： 根据id获取实体对象和流程id */
	IfsCfetsrmbIrs getIrsAndFlowIdById(Map<String, Object> key);

	// 根据id查询利率互换元素
	IfsCfetsrmbIrs searchIrsByTicketId(String ticketId);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbIrs> getRmbIrsMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 买断式回购OR
	 */
	String insertOr(IfsCfetsrmbOr record);

	void updateOr(IfsCfetsrmbOr record);

	void deleteOr(IfsCfetsrmbOrKey key);

	Page<IfsCfetsrmbOr> getOrList(IfsCfetsrmbOr record);

	IfsCfetsrmbOr getOrById(IfsCfetsrmbOrKey key);
	IfsCfetsrmbOr selectById(Map<String, String> map);

	/* 增加： 根据id获取实体对象和流程id */
	IfsCfetsrmbOr getOrAndFlowIdById(Map<String, Object> key);

	IfsCfetsrmbOr searchCfetsrmbOrByTicketId(String ticketId);

	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbOr> getRmbOrMinePage(Map<String, Object> params, int isFinished);

	/*
	 * 债券借贷SL
	 */
	String insertSl(IfsCfetsrmbSl record);

	void updateSl(IfsCfetsrmbSl record);

	void deleteSl(IfsCfetsrmbSlKey key);

	Page<IfsCfetsrmbSl> getSlList(IfsCfetsrmbSl record);

	IfsCfetsrmbSl getSlById(IfsCfetsrmbSlKey key);

	/**
	 * 根据请求参数查询分页列表-、我发起的、待审批、已审批列表
	 * 
	 * @param params
	 *            - 请求参数
	 * @return
	 * @author
	 * @date 2013-3-22
	 */
	Page<IfsCfetsrmbSl> getRmbSlMinePage(Map<String, Object> params, int isFinished);

	IfsCfetsrmbIbo searchCfetsRmbIboById(String ticketId);

	IfsCfetsrmbIbo getIboAndFlowIdById(Map<String, Object> key);

	IfsCfetsrmbSl getSlAndFlowIdById(Map<String, Object> key);

	IfsCfetsrmbCr getCrAndFlowIdById(Map<String, Object> key);

	IfsCfetgoldLend getGoldLendAndFlowById(Map<String, Object> key);

	IfsCfetsfxDebt getDebtAndFlowById(Map<String, Object> key);

	IfsCfetsfxAllot searchRmbCcypaAndFlowIdById(Map<String, Object> key);

	IfsCfetsfxOnspot searchRmbSpotAndFlowIdById(Map<String, Object> key);

	IfsCfetsfxOption searchRmbOptAndFlowIdById(Map<String, Object> key);

	IfsCfetsfxLend searchRmbCcylendingAndFlowIdById(Map<String, Object> key);

	Page<IfsCfetsrmbIrs> getApprovePassedPage(Map<String, Object> params);

	Page<IfsCfetgoldLend> getApproveGoldLendPassedPage(Map<String, Object> params);

	Page<IfsCfetsmetalGold> getApprovemetalGoldPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbCbt> getApproveCbtPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbCr> getApproveCrPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbIbo> getApproveIboPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbOr> getApproveOrPassedPage(Map<String, Object> params);

	Page<IfsCfetsrmbSl> getApproveSlPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxCswap> getApprovefxCswapPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxLend> getApprovefxLendPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxAllot> getApprovefxAllotPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxOnspot> getApproveOnspotPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxDebt> getApprovefxDebtPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxSwap> getApprovefxSwapPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxFwd> getApprovefxFwdPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxSpt> getApprovefxSptPassedPage(Map<String, Object> params);

	Page<IfsCfetsfxOption> getApprovefxOptionPassedPage(Map<String, Object> params);

	/**
	 * rebackData:(这里用一句话描述这个方法的作用). <br/>
	 * 
	 * @author Administrator
	 * @param params
	 * @since JDK 1.6
	 */
	void rebackData(Map<String, Object> params);

	Page<IfsCfetsrmbDetailCr> searchBondCrDetails(Map<String, Object> params);

	Page<IfsCfetsrmbDetailSl> searchBondSlDetails(Map<String, Object> params);

	/**
	 * @param map
	 * @return
	 */
	Page<IfsRevIrvv> searchPageIrvv(Map<String, Object> map);

	/**
	 * 根据请求参数查询分页列表- 我发起的，待审批，已审批
	 * 
	 * @param map
	 * @param isFinished
	 * @return
	 */
	Page<IfsRevIrvv> searchIrvv(Map<String, Object> map, int isFinished);

	void insert(IfsRevIrvv entity);

	void deleteById(String pcode);

	void updateById(IfsRevIrvv entity);

	/**
	 * 存续期管理平台 ：外汇类，回购类,债券类，拆借类，期权类，互换类mini弹框列表数据查询
	 */

	Page<Map<String, Object>> getMini(Map<String, Object> params);

	Page<Map<String, Object>> getBondMini(Map<String, Object> params);

	Page<Map<String, Object>> getLendMini(Map<String, Object> params);

	Page<Map<String, Object>> getDELendMini(Map<String, Object> params);
	
	Page<Map<String, Object>> getOptMini(Map<String, Object> params);

	Page<Map<String, Object>> getSlMini(Map<String, Object> params);

	Page<Map<String, Object>> getIswhMini(Map<String, Object> params);

	IfsCfetsrmbSl searchCfetsrmbSlByTicketId(Map<String, Object> map);

	IfsCfetsrmbCbt searchCfetsrmbCbtByTicketId(Map<String, Object> map);

	/**
	 * 存单发行 xuqq
	 */
	String insertDp(IfsCfetsrmbDp record);

	void updateDp(IfsCfetsrmbDp record);

	void deleteDp(IfsCfetsrmbDpKey key);

	Page<IfsCfetsrmbDp> getDpList(IfsCfetsrmbDp record);

	Page<IfsCfetsrmbDp> getRmbDpMinePage(Map<String, Object> params, int isFinished);

	IfsCfetsrmbDp getDpById(IfsCfetsrmbDpKey key);

	Page<IfsCfetsrmbDpOffer> searchOfferDpDetails(Map<String, Object> params);

	IfsCfetsrmbDp getDpAndFlowIdById(Map<String, Object> key);


	/**
	 * 同业存放(外币)
	 */
	void editCcyDepositIn(IfsFxDepositIn record);
	String addCcyDepositIn(IfsFxDepositIn record);
	void deleteCcyDepositIn(String ticketid);
	Page<IfsFxDepositIn> searchCcyDepositIn(Map<String, Object> params);
	Page<IfsFxDepositIn> getDepositInFXMinePage(Map<String, Object> params, int isFinished);
	IfsFxDepositIn searchByTicketId(Map<String, Object> params);
	IfsFxDepositIn searchDepositInFXFlowByTicketId(Map<String, Object> params);

	/**
	 * 存放同业(外币)
	 */
	void editCcyDepositout(IfsFxDepositout record);
	String addCcyDepositout(IfsFxDepositout record);
	void deleteCcyDepositout(String ticketid);
	Page<IfsFxDepositout> searchCcyDepositout(Map<String, Object> params);
	Page<IfsFxDepositout> getDepositoutFXMinePage(Map<String, Object> params, int isFinished);
	IfsFxDepositout searchDepositoutFXByTicketId(Map<String, Object> params);
	IfsFxDepositout searchFlowDepositoutFXByTicketId(Map<String, Object> params);

	/**
	 * 同业存放
	 */
	void editRmbDepositIn(IfsRmbDepositin record);
	String addRmbDepositIn(IfsRmbDepositin record);
	void deleteRmbDepositIn(String ticketid);
	Page<IfsRmbDepositin> searchRmbDepositIn(Map<String, Object> params);
	Page<IfsRmbDepositin> getDepositInRmbMinePage(Map<String, Object> params, int isFinished);
	IfsRmbDepositin searchDepositInRmbByTicketId(Map<String, Object> params);
	IfsRmbDepositin searchFlowDepositInRmbByTicketId(Map<String, Object> params);

	/**
	 * 存放同业
	 */
	void editRmbDepositOut(IfsRmbDepositout record);
	String addRmbDepositOut(IfsRmbDepositout record);
	void deleteRmbDepositOut(String ticketid);
	Page<IfsRmbDepositout> searchRmbDepositOut(Map<String, Object> params);
	Page<IfsRmbDepositout> getDepositOutRmbMinePage(Map<String, Object> params, int isFinished);
	IfsRmbDepositout searchDepositOutRmbByTicketId(Map<String, Object> params);
	IfsRmbDepositout searchFlowDepositOutRmbByTicketId(Map<String, Object> params);

	/**
	 * 事前交易
	 */
	Page<IfsCfetsBeforeDeal> searchCfetsBeforeDeal(IfsCfetsBeforeDeal record);
	IfsCfetsBeforeDeal queryCfetsBeforeDealById(Map<String, Object> key);
	void insertBeforeDeal(Map<String, Object> key);
	void updateBeforeDeal(Map<String, Object> key);
	void deleteBeforeDeal(Map<String, Object> key);
	void updateBeforeDealStatus(Map<String, Object> key);
	/**
	 * 流程特殊用户
	 */
	Page<IfsCfetsFlowUser> searchCfetsFlowUserManage(IfsCfetsFlowUser record);
	IfsCfetsFlowUser queryCfetsFlowUserById(Map<String, Object> key);
	void insertFlowUser(Map<String, Object> key);
	void updateFlowUser(Map<String, Object> key);
	void deleteFlowUser(Map<String, Object> key);
	/**
	 * 报表查询
	 */
	Page<IfsDealDeskReport> searchPageDealDesk(Map<String, Object> params);

	/**
	 * 更新日期
	 */
	void updateCurDate(String date);

	
	
	void nbhAdd(Map<String, Object> map);

	Page<IfsNbhBean> searchData(IfsNbhBean entity);

	void deleteNbh(Map<String, String> map);

	void updateById(IfsNbhBean entity);

	Page<Map<String, Object>> getDpMini(Map<String, Object> params);

	IfsCfetsrmbDp searchCfetsrmbDpByTicketId(Map<String, Object> map);
	
	String queryDictValue(Map<String, String> map);
	
	/**
	 * 查询外汇头寸估值
	 */
	Page<IfsNbhGatherBean> searchNbhPage(Map<String, Object> map);
	
	/**
	 * 外汇头寸估值  新增时保存
	 */
	void nbhGatherAdd(IfsNbhGatherBean entity);
	
	/**
	 * 外汇头寸估值 修改时保存
	 */
	void updateByGatherId(IfsNbhGatherBean entity);
	
	/**
	 * 外汇头寸估值 删除
	 */
	void deleteNbhAcct(Map<String, String> map);
	
	/**
	 * 根据fedealno查询实体类
	 */
	IfsNbhGatherBean searchNbhPageSingle(String fedealno);
	
	/**
	 * 修改处理状态
	 */
	void updateStatus(IfsNbhGatherBean entity);

	IfsCfetsrmbCr searchCrDetailByTicketId(Map<String, Object> map);

	IfsCfetsfxFwd searchFwdDetailByTicketId(Map<String, Object> map);

	IfsCfetsfxLend searchLendDetailByTicketId(Map<String, Object> map);

	IfsCfetsfxOption searchOptionDetailByTicketId(Map<String, Object> map);

	IfsCfetsfxSpt searchSptDetailByTicketId(Map<String, Object> map);

	IfsCfetsfxSwap searchSwapDetailByTicketId(Map<String, Object> map);

	IfsCfetsrmbIbo searchIboDetailByTicketId(Map<String, Object> map);

	IfsCfetsrmbIrs searchIrsDetailByTicketId(Map<String, Object> map);

	IfsCfetsrmbOr searchOrDetailByTicketId(Map<String, Object> map);
	
	PageInfo<IfsCfetsfxSpt> searchSptCheck(Map<String, Object> map);
	
	PageInfo<IfsCfetsrmbCbt> searchCbtCheck(Map<String, Object> map);

	PageInfo<IfsCfetsrmbOr> searchCapitalCheck(Map<String, Object> map);
	
    List<IfsCfetsfxSpt> searchSptCheckList(Map<String, Object> map);
	
    List<IfsCfetsrmbCbt> searchCbtCheckList(Map<String, Object> map);

    List<IfsCfetsrmbOr> searchCapitalCheckList(Map<String, Object> map);

	List<IfsRepoReportBean> searchRepoReportList(Map<String, Object> map);
	
	IfsRepoReportBean searchRepoReportListTotal(Map<String, Object> map);
	
	//查询拆借业务报表
	List<IfsIboReportBean> searchIboReportList(Map<String, Object> map);
	//查询当年总计拆借业务报表
	IfsIboReportBean searchIboReportListTotal(Map<String, Object> map);
	
	//查询现劵买卖业务报表
	List<IfsCbtReportBean> searchCbtReportList(Map<String, Object> map);
	//查询当年总计拆借业务报表
	IfsCbtReportBean searchCbtReportListTotal(Map<String, Object> map);

	IfsCfetsmetalGold getGoldByIdAndType(Map<String, String> map);

	IfsCfetsmetalGold searchMetalGold(Map<String, Object> map);
	
	/**
	 * 拆借提前还款
	 * @param map
	 * @return
	 */
	Page<IfsAdvIdlv> searchPageIdlv(Map<String, Object> map);
	
	/**
	 * 根据请求参数查询分页列表- 我发起的，待审批，已审批
	 * @param map
	 * @param isFinished
	 * @return
	 */
	Page<IfsAdvIdlv> searchIdlv(Map<String, Object> map, int isFinished);
	
	/**
	 * 删除
	 * @param ticketId
	 */
	void deleteIdlvById(String ticketId);
	
	/**
	 * 保存
	 * @param idlv
	 */
	void insertIdlv(IfsAdvIdlv idlv);

	/**
	 * 修改
	 * @param idlv
	 */
	void updateIdlvById(IfsAdvIdlv idlv);
	
	String checkCust(String dealTransType, String cno, String cfetsno, String cfetscn, String product, String ProdType) ;
}