package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsLimitColumns;
import com.singlee.ifs.model.IfsOpicsProd;

import java.util.List;
import java.util.Map;

/***
 * 
 * @author singlee4
 * 
 */
public interface IfsFieldLimitService {

	// 新增
	public void insert(Map<String, Object> map);

	// 修改
	void updateById(Map<String, Object> map);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsLimitColumns> searchPageFieldLimit(Map<String, Object> map);

	// 查询表的结构
	List<Map<String, Object>> searchColumns(Map<String, Object> map);

	// 查询产品表
	List<Map<String, Object>> searchTables(Map<String, Object> map);

	// 查询字典表
	List<Map<String, Object>> searchDict(Map<String, Object> map);

	// 根据产品类型 查询限额字段以及字典值
	String searchLimitFieldAndDict(Map<String, Object> map);

	// 根据产品类型查询出所有
	List<Map<String, Object>> searchAllByPrd(Map<String, Object> map);

	// 根据限额字段去查询数据
	public List<Map<String, Object>> searchColum(Map<String, Object> map);

	// 根据产品类型 限额字段 字典值 查询出所有
	public List<Map<String, Object>> proColuDic(Map<String, Object> map);

	// 根据产品类型 查出 限额字段
	public List<Map<String, Object>> selectColum(Map<String, String> map);
	
	//外汇限额管理查询产品信息
	public Page<IfsOpicsProd> searchPrdInfo(Map<String, Object> map);
	
	//外汇限额管理查询产品类型信息
	public Page<Map<String, Object>> searchTypeInfo(Map<String, Object> map);
	
	//外汇限额管理查询币种信息
	public Page<Map<String, Object>> searchCcyInfo(Map<String, Object> map);
	
	//外汇限额管理查询债券类型信息
	public Page<Map<String, Object>> searchBondInfo(Map<String, Object> map);
	
	//账户类型信息
	public Page<Map<String, Object>> searchAccTypeInfo(Map<String, Object> map);
	
	//查询用户名
	public Page<Map<String, Object>> searchTradInfo(Map<String, Object> map);
	//查询成本中心
	public Page<Map<String, Object>> searchCostInfo(Map<String, Object> map);
	
}
