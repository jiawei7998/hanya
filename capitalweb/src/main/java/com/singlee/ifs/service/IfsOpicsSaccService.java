package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsSacc;

import java.util.Map;

/**
 * SACC
 * 
 * @author yanming
 */
public interface IfsOpicsSaccService {

	// 新增
	public void insert(IfsOpicsSacc entity);

	// 修改
	void updateById(IfsOpicsSacc entity);

	// 删除
	void deleteById(Map<String, String> map);

	// 分页查询
	Page<IfsOpicsSacc> searchPageOpicsSacc(Map<String, Object> map);

	// 根据id查询实体类
	IfsOpicsSacc searchById(Map<String, String> map);

	// 更新同步状态
	void updateStatus(IfsOpicsSacc entity);

	// 批量校准
	SlOutBean batchCalibration(String type, String[] accountnos, String[] brs);
}
