package com.singlee.ifs.service.impl;

import com.singlee.ifs.service.SummitService;
import org.springframework.stereotype.Service;

/**
 * 用于summit系统相关操作,接口导入，数据查询获取等
 * 
 * @author shenzl
 * 
 */
@Service
public class SummitServiceImpl implements SummitService {

}
