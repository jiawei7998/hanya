package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsSubjectdetail;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

//opics科目余额

@Service
public interface IfsSubjectdetailService {

    Page<IfsSubjectdetail> getList(Map<String, Object> map);

    void updateOpicsBalance(Map<String, Object> map);

    void addOpicsBalance(Map<String, Object> map);

    List<IfsSubjectdetail> getListNoPage(Map<String, Object> map);

    void insertList(List<IfsSubjectdetail> list);

    void updateList(List<IfsSubjectdetail> list);
}
