package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlRateBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.baseUtil.noBlankInString;
import com.singlee.ifs.mapper.IfsOpicsRateMapper;
import com.singlee.ifs.model.IfsOpicsRate;
import com.singlee.ifs.service.IfsOpicsRateService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class IfsOpicsRateServiceImpl implements IfsOpicsRateService {
	@Resource
	IfsOpicsRateMapper ifsOpicsRateMapper;
	@Autowired
	IStaticServer iStaticServer;
	/***
	 * 批量校准
	 */
	@Override
	public SlOutBean batchCalibration(String type, String[] ratecodes, String[] brs) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			//从opics库查询所有的数据
			List<SlRateBean> listRateOpics = iStaticServer.synchroRate();
			
			/*****  对选中的行进行校准 ****************************/
			if("1".equals(type)){//选中行校准
				
				for(int i=0;i<ratecodes.length;i++){
					for(int j=0;j<listRateOpics.size();j++){
						//若opics库里有相同的利率代码,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if(listRateOpics.get(j).getRatecode().trim().equals(ratecodes[i]) &&
						   listRateOpics.get(j).getBr().trim().equals(brs[i])){
							IfsOpicsRate entity = new IfsOpicsRate();
							Map<String,Object> keyMap = new HashMap<String,Object>();
							keyMap.put("br",brs[i]);
							keyMap.put("ratecode", ratecodes[i]);
							entity = ifsOpicsRateMapper.searchById(keyMap);
							entity.setRatecode(listRateOpics.get(j).getRatecode().trim());
							//去掉实体类中参数为STRING类型的属性的前后空格
							noBlankInString.stripStringProperty(listRateOpics.get(j));
							//opics库中的实体转成map
							Map<String, Object> map1 = BeanUtil.beanToMap(listRateOpics.get(j));
							//其中中文描述、备注还是用本地的,不更新
							map1.put("status","1");
							map1.put("operator",SlSessionHelper.getUserId());
							map1.put("remark",entity.getRemark());
							map1.put("ratecodecn",entity.getRatecodecn());
							//把map转成本地库对应的实体类
							entity =  (IfsOpicsRate) BeanUtil.mapToBean(IfsOpicsRate.class, map1) ;  
							
							
							ifsOpicsRateMapper.updateById(entity);
							break;
						}
						//若opics库里没有，就改变同步状态为   未同步,其余不改变
						if(j == listRateOpics.size()-1){
							Map<String,Object> keyMap1 = new HashMap<String,Object>();
							keyMap1.put("br",brs[i]);
							keyMap1.put("ratecode", ratecodes[i]);
							ifsOpicsRateMapper.updateStatus0ById(keyMap1);
						}
						
					}
					
				}
					
				
			/*************  对本地库所有利率代码记录进行校准***************/	
			}else{//全部校准
				//从本地库查询所有的数据
				List<IfsOpicsRate> listRateLocal = ifsOpicsRateMapper.searchAllOpicsRate();
				/**1.以opics库为主，更新或插入本地库*/
				for(int i=0;i<listRateOpics.size();i++){
					
					/*****++++++++++++++++++++  实体设置  start ++++++++++++++++++++++***********/
					IfsOpicsRate entity = new IfsOpicsRate();
					
					//去掉实体类中参数为STRING类型的属性的前后空格
					noBlankInString.stripStringProperty(listRateOpics.get(i));
					//opics库中的实体转成map
					Map<String, Object> map1 = BeanUtil.beanToMap(listRateOpics.get(i));
					//其中中文描述、备注还是用本地的,不更新
					map1.put("status","1");
					map1.put("operator",SlSessionHelper.getUserId());
					map1.put("remark",entity.getRemark());
					map1.put("ratecodecn",entity.getRatecodecn());
					//把map转成本地库对应的实体类
					entity =  (IfsOpicsRate) BeanUtil.mapToBean(IfsOpicsRate.class, map1) ;
					
					/*****++++++++++++++++++++  实体设置  end +++++++++++++++++++++++***********/
					
					
					if(listRateLocal.size() == 0){//本地库无数据
						ifsOpicsRateMapper.insert(entity);
					}else{//本地库有数据
						for(int j=0;j<listRateLocal.size();j++){
							
							//opics库中利率代码有与本地的利率代码相等，就更新本地的利率代码其他的内容
							if(listRateOpics.get(i).getRatecode().trim().equals(listRateLocal.get(j).getRatecode())&&
							   listRateOpics.get(i).getBr().trim().equals(listRateLocal.get(j).getBr())){
								ifsOpicsRateMapper.updateById(entity);
								break;
							}
							//若没有，就插入本地库
							if(j == listRateLocal.size()-1){
								ifsOpicsRateMapper.insert(entity);
							}
						}
						
					}
					
				}
				/**2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步*/
				//遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				List<IfsOpicsRate> listRateLocal2 = ifsOpicsRateMapper.searchAllOpicsRate();
				if(listRateLocal2.size() > listRateOpics.size()){
					for(int i=0;i<listRateLocal2.size();i++){
						for(int j=0;j<listRateOpics.size();j++){
							if(listRateOpics.get(j).getRatecode().trim().equals(listRateLocal2.get(i).getRatecode())&&
							   listRateOpics.get(j).getBr().trim().equals(listRateLocal2.get(i).getBr())){
								break;
							}
							//若opics库里没有，就改变同步状态为   未同步
							if(j == listRateOpics.size()-1){
								listRateLocal2.get(i).setStatus("0");
								listRateLocal2.get(i).setOperator(SlSessionHelper.getUserId());
								listRateLocal2.get(i).setLstmntdate(new Date());
								
								ifsOpicsRateMapper.updateById(listRateLocal2.get(i));
								
							}
							
						}
						
					}
					
				}
				
				
			}
		}catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}
	
	
	@Override
	public Page<IfsOpicsRate> searchPageOpicsRate(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsRate> result = ifsOpicsRateMapper.searchPageOpicsRate(map, rb);
		return result;
		
	}

}
