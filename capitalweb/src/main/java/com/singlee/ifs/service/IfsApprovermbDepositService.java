package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbDeposit;

import java.util.Map;

public interface IfsApprovermbDepositService {
    void deleteDeposit(IfsApprovermbDeposit key);

    Page<IfsApprovermbDeposit> getRmbDepositMinePage(Map<String, Object> params, int i);

    IfsApprovermbDeposit getDepositById(IfsApprovermbDeposit deposit);

    IfsApprovermbDeposit getDepositById(Map<String, Object> params);

    String insertDeposit(IfsApprovermbDeposit deposit);

    void updateDeposit(IfsApprovermbDeposit deposit);

    void generateCashFlow(IfsApprovermbDeposit deposit);

    IfsApprovermbDeposit searchApproveRmbDepositByTicketId(String ticketId);
}
