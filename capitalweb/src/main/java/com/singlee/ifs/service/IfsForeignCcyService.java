package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsForeignccyDish;

import java.util.Map;


/**
 * 总行外币平盘数据查询接口 7445
 */
public interface IfsForeignCcyService {
	
	public SlOutBean queryForeignCcy(Map<String, Object> map) ;
	
	// 分页查询
	Page<IfsForeignccyDish> searchPageDish(Map<String, Object> map);
}
