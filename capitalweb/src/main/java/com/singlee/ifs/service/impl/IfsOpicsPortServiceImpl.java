package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlPortBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.mapper.IfsOpicsPortMapper;
import com.singlee.ifs.model.IfsOpicsPort;
import com.singlee.ifs.service.IfsOpicsPortService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IfsOpicsPortServiceImpl implements IfsOpicsPortService {
	@Resource
	IfsOpicsPortMapper ifsOpicsPortMapper;
	@Autowired
	IStaticServer iStaticServer;

	@Override
	public void insert(IfsOpicsPort entity) {
		String portid = DateUtil.getCurrentCompactDateTimeAsString() + Integer.toString((int) ((Math.random() * 9 + 1) * 100));
		entity.setPortid(portid);
		// 设置最新维护时间
		entity.setLstmntdte(new Date());
		entity.setStatus("0");
		entity.setUpdatecounter("0");
		ifsOpicsPortMapper.insert(entity);

	}

	@Override
	public void updateById(IfsOpicsPort entity) {
		int counter = Integer.parseInt(entity.getUpdatecounter()) + 1;
		String updatecounter = String.valueOf(counter);
		entity.setUpdatecounter(updatecounter);
		entity.setLstmntdte(new Date());
		ifsOpicsPortMapper.updateById(entity);
	}

	@Override
	public void deleteById(Map<String, Object> map) {
		ifsOpicsPortMapper.deleteById(map);

	}

	@Override
	public Page<IfsOpicsPort> searchPageOpicsPort(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsPort> result = ifsOpicsPortMapper.searchPageOpicsPort(map, rb);
		return result;
	}

	@Override
	public List<IfsOpicsPort> searchPort(HashMap<String, Object> map) {
		List<IfsOpicsPort> list = ifsOpicsPortMapper.searchPort(map);
		return TreeUtil.mergeChildrenList(list, "-1");
	}

	@Override
	public IfsOpicsPort searchById(Map<String, String> map) {
		return ifsOpicsPortMapper.searchById(map);
	}

	@Override
	public void updateStatus(IfsOpicsPort entity) {
		ifsOpicsPortMapper.updateStatus(entity);
	}

	@Override
	public SlOutBean batchCalibration(String type, String[] brs, String[] portfolios) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			List<SlPortBean> listPortOpics = iStaticServer.synchroPort();
			HashMap<String, String> map = new HashMap<String, String>();
			if ("1".equals(type)) {// 选中行
				for (int i = 0; i < brs.length; i++) {
					for (int j = 0; j < listPortOpics.size(); j++) {
						map.put("br", brs[i]);
						map.put("portfolio", portfolios[i]);
						if (listPortOpics.get(j).getBr().equals(brs[i]) && listPortOpics.get(j).getPortfolio().equals(portfolios[i])) {
							IfsOpicsPort ifsOpicsPort = ifsOpicsPortMapper.searchById(map);

							ifsOpicsPort.setPortfolio(listPortOpics.get(j).getPortfolio());
							ifsOpicsPort.setBr(listPortOpics.get(j).getBr());
							ifsOpicsPort.setAmount1(listPortOpics.get(j).getAmount1());
							ifsOpicsPort.setAmount2(listPortOpics.get(j).getAmount2());
							ifsOpicsPort.setCost(listPortOpics.get(j).getCost());
							ifsOpicsPort.setDate1(listPortOpics.get(j).getDate1());
							ifsOpicsPort.setDate2(listPortOpics.get(j).getDate2());
							ifsOpicsPort.setLstmntdte(listPortOpics.get(j).getLstmntdte());
							ifsOpicsPort.setPortdesc(listPortOpics.get(j).getPortdesc());
							ifsOpicsPort.setUpdatecounter(listPortOpics.get(j).getUpdateCounter());

							ifsOpicsPort.setOperator(SlSessionHelper.getUserId());
							ifsOpicsPort.setStatus("1");
							ifsOpicsPortMapper.updateById(ifsOpicsPort);
							break;
						}
						if (j == listPortOpics.size() - 1) {
							HashMap<String, String> statusmap = new HashMap<String, String>();
							statusmap.put("br", brs[i]);
							statusmap.put("portfolio", portfolios[i]);
							ifsOpicsPortMapper.updateStatus0ById(statusmap);
						}
					}
				}
			} else {// 所有行
				List<IfsOpicsPort> listOpicsLocal = ifsOpicsPortMapper.searchAllOpicsPort();
				for (int i = 0; i < listPortOpics.size(); i++) {
					IfsOpicsPort ifsOpicsPort = new IfsOpicsPort();
					ifsOpicsPort.setPortfolio(listPortOpics.get(i).getPortfolio());
					ifsOpicsPort.setBr(listPortOpics.get(i).getBr());
					ifsOpicsPort.setAmount1(listPortOpics.get(i).getAmount1());
					ifsOpicsPort.setAmount2(listPortOpics.get(i).getAmount2());
					ifsOpicsPort.setCost(listPortOpics.get(i).getCost());
					ifsOpicsPort.setDate1(listPortOpics.get(i).getDate1());
					ifsOpicsPort.setDate2(listPortOpics.get(i).getDate2());
					ifsOpicsPort.setLstmntdte(listPortOpics.get(i).getLstmntdte());
					ifsOpicsPort.setPortdesc(listPortOpics.get(i).getPortdesc());
					ifsOpicsPort.setUpdatecounter(listPortOpics.get(i).getUpdateCounter());
					ifsOpicsPort.setOperator(SlSessionHelper.getUserId());
					ifsOpicsPort.setStatus("1");
					if (listOpicsLocal.size() == 0) {// 本地无数据
						ifsOpicsPortMapper.insert(ifsOpicsPort);
					} else {
						// 本地有数据
						for (int j = 0; j < listOpicsLocal.size(); j++) {
							if (listPortOpics.get(i).getBr().equals(listOpicsLocal.get(j).getBr()) && listPortOpics.get(i).getPortfolio().equals(listOpicsLocal.get(j).getPortfolio())) {
								ifsOpicsPort.setPortdesccn(listOpicsLocal.get(j).getPortdesccn());
								ifsOpicsPortMapper.updateById(ifsOpicsPort);
								break;
							}
							if (j == listOpicsLocal.size() - 1) {
								ifsOpicsPortMapper.insert(ifsOpicsPort);
							}
						}
					}
				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				List<IfsOpicsPort> listOpicsLocal2 = ifsOpicsPortMapper.searchAllOpicsPort();
				if (listOpicsLocal2.size() > listPortOpics.size()) {
					for (int i = 0; i < listOpicsLocal2.size(); i++) {
						for (int j = 0; j < listPortOpics.size(); j++) {
							if (listPortOpics.get(j).getBr().equals(listOpicsLocal2.get(i).getBr()) && listPortOpics.get(j).getPortfolio().equals(listOpicsLocal2.get(i).getPortfolio())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为未同步
							if (j == listPortOpics.size() - 1) {
								IfsOpicsPort ifsOpicsPort = new IfsOpicsPort();

								ifsOpicsPort.setPortfolio(listOpicsLocal2.get(i).getPortfolio());
								ifsOpicsPort.setBr(listOpicsLocal2.get(i).getBr());
								ifsOpicsPort.setAmount1(listOpicsLocal2.get(i).getAmount1());
								ifsOpicsPort.setAmount2(listOpicsLocal2.get(i).getAmount2());
								ifsOpicsPort.setCost(listOpicsLocal2.get(i).getCost());
								ifsOpicsPort.setDate1(listOpicsLocal2.get(i).getDate1());
								ifsOpicsPort.setDate2(listOpicsLocal2.get(i).getDate2());
								ifsOpicsPort.setPortdesc(listOpicsLocal2.get(i).getPortdesc());
								ifsOpicsPort.setUpdatecounter(listOpicsLocal2.get(i).getUpdatecounter());
								ifsOpicsPort.setLstmntdte(new Date());
								ifsOpicsPort.setStatus("0");// 设置状态为未同步
								ifsOpicsPort.setOperator(SlSessionHelper.getUserId());
								ifsOpicsPortMapper.updateById(ifsOpicsPort);
							}
						}
					}
				}
			}
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

	@Override
	public List<IfsOpicsPort> searchAllOpicsPort() {
		List<IfsOpicsPort> result = ifsOpicsPortMapper.searchAllOpicsPort();
		return result;
	}

}
