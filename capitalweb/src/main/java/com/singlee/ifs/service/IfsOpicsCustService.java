package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsCust;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * opics 交易对手表 service
 * 
 * @author singlee4
 */
public interface IfsOpicsCustService {

	// 新增
	public void insert(IfsOpicsCust entity);

	// 修改
	void updateById(IfsOpicsCust entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsCust> searchPageOpicsCust(Map<String, Object> map);

	// 根据交易对手编号查询
	IfsOpicsCust searchIfsOpicsCust(String cno);

	// 获取feDealNo交易流水号
	public String getFedealNo(HashMap<String, Object> map);

	// 批量校准
	SlOutBean batchCalibration(String type, String[] cnos);

	// 查询所有
	Page<IfsOpicsCust> searchAllOpicsCust();

	// 更新同步状态
	void updateStatus(IfsOpicsCust entity);

	/**
	 * 更新客户信息
	 */
	void updateCustInfo(SlCustBean slCustBean);
	
	/**
	 * 根据客户号和交易对手编号查询某一条
	 */
	List<IfsOpicsCust> searchSingle(IfsOpicsCust slCustBean);
	
	/**
     * 根据客户号和交易对手编号查询某一条
     */
    List<Map<String, Object>> searchSingleAll(Map<String,Object> map );

	String searchCnameByCno(Map<String, Object> map);
	// 经办
	void sendCust(IfsOpicsCust entity);
}