package com.singlee.ifs.service.callback.prefix;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.ifs.mapper.IfsApprovefxCswapMapper;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsApprovefxCswap;
import com.singlee.ifs.model.IfsFlowMonitor;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 货币掉期【事前审批】回调函数-【暂未使用】
 * 
 * ClassName: IfsApproveCswapService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018-5-30 下午08:10:05 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 */
@Deprecated
@Service(value = "IfsApproveCswapService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsApproveCswapService extends IfsApproveServiceBase {

	@Resource
	IfsApprovefxCswapMapper ifsApprovefxCswapMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status, String flowCompleteType) {
		IfsApprovefxCswap ifsApprovefxCswap = new IfsApprovefxCswap();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		/* RetBean result = null; */
		String date = dateFormat.format(now);
		ifsApprovefxCswapMapper.updateCcySwapByID(serial_no, status, date);

		ifsApprovefxCswap = ifsApprovefxCswapMapper.searchCcySwap(serial_no);
		IfsFlowMonitor ifsFlowMonitor11 = new IfsFlowMonitor();
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {
			ifsFlowMonitor11 = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (ifsFlowMonitor11 == null) {
				IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
				ifsFlowMonitor.setTicketId(ifsApprovefxCswap.getTicketId());// 设置监控表-内部id
				ifsFlowMonitor.setContractId(ifsApprovefxCswap.getTicketId());// 设置监控表-成交单编号
				ifsFlowMonitor.setApproveStatus(ifsApprovefxCswap.getApproveStatus());// 设置监控表-审批状态
				ifsFlowMonitor.setPrdName("货币掉期");// 设置监控表-产品名称
				ifsFlowMonitor.setPrdNo("ccyswap");// 设置监控表-产品编号：作为分类
				ifsFlowMonitor.setTradeDate(ifsApprovefxCswap.getPostDate());// 设置监控表-交易日期
				ifsFlowMonitor.setSponsor(ifsApprovefxCswap.getSponsor());// 设置监控表-审批发起人
				ifsFlowMonitor.setTradeType("0");// 交易类型，0：事前交易，1：正式交易
				String custNo = ifsApprovefxCswap.getCounterpartyInstId();
				if (null != custNo || "".equals(custNo)) {
					ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
					IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
					ifsFlowMonitor.setCustName(ifsOpicsCust.getCfn());// 设置监控表-客户名称(全称)
				}
				StringBuffer buffer = new StringBuffer("");
				List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper.searchTradeElements("IFS_APPROVEFX_CSWAP");
				if (listmap.size() == 1) {
					LinkedHashMap<String, String> hashmap = listmap.get(0);
					String tradeElements = hashmap.get("TRADE_GROUP");
					String commentsGroup = hashmap.get("COMMENTSGROUPS");
					if (tradeElements != "null" || tradeElements != "") {
						buffer.append("{");
						String sqll = "select " + tradeElements + " from IFS_APPROVEFX_CSWAP where ticket_id=" + "'" + serial_no + "'";
						LinkedHashMap<String, Object> swap = ifsApprovefxCswapMapper.searchProperty(sqll);
						String[] str = tradeElements.split(",");
						String[] comments = commentsGroup.split(",");
						for (int j = 0; j < str.length; j++) {
							String kv = str[j];
							String key = comments[j];
							String value = String.valueOf(swap.get(kv));
							buffer.append(key).append(":").append(value);
							if (j != str.length - 1) {
								buffer.append(",");
							}
						}
						buffer.append("}");
					}
				}
				ifsFlowMonitor.setTradeElements(buffer.toString());
				ifsFlowMonitorMapper.insert(ifsFlowMonitor);
			}
		}
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)
				|| StringUtils.equals(ApproveOrderStatus.New, status)) {
			IfsFlowMonitor ifsFlowMonitor22 = new IfsFlowMonitor();
			ifsFlowMonitor22 = ifsFlowMonitorMapper.getEntityById(serial_no);
			ifsFlowMonitor22.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> map1 = BeanUtil.beanToMap(ifsFlowMonitor22);
			ifsFlowCompletedMapper.insert(map1);
		}

		if (!"6".equals(status)) {
			return;
		}

		/*
		 * try {
		 * 
		 * if(ifscfetsfxcswap.equals(null)){ return; } map =
		 * BeanUtils.describe(ifscfetsfxcswap); result =
		 * swapserver.crsSwapProc(map);//货币掉期存储过程
		 * if(!result.getRetCode().equals("999")){ JY.raise("审批未通过......"); } }
		 * catch (Exception e) { //JY.raise("审批未通过......");
		 * JY.raise("审批未通过......",e.getMessage()); }
		 */

	}
}
