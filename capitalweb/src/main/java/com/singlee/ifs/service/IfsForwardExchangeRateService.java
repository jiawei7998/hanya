package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsForwardExchangeRateBean;

import java.util.List;
import java.util.Map;

public interface IfsForwardExchangeRateService {
    /**
     * @title 分页查询
     * @description 
     * @author  cg
     * @updateTime 2021-09-17
     * @throws 
     */
    Page<IfsForwardExchangeRateBean> searchPageRate(Map<String, Object> map);

    /**
     * 市场数据 先查询，后插入
     */
    String doBussExcelPropertyToDataBase(List<IfsForwardExchangeRateBean> rateBean);

    /**
     * 市场数据 插入irev表
     */
    String dataBaseToIrev(String date);
}
