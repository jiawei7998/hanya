package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsCdtcCnBondLinkSecurdealMapper;
import com.singlee.ifs.model.IfsCdtcCnBondLinkSecurdeal;
import com.singlee.ifs.service.IfsCdtcCnBondLinkSecurdealService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IfsCdtcCnBondLinkSecurdealServiceImpl implements IfsCdtcCnBondLinkSecurdealService{
	@Autowired
	private IfsCdtcCnBondLinkSecurdealMapper  ifsCdtcCnBondLinkSecurdealMapper;
	@Override
	public Page<IfsCdtcCnBondLinkSecurdeal> searchPageCnBondLinkSecurdeal(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsCdtcCnBondLinkSecurdeal> result = ifsCdtcCnBondLinkSecurdealMapper.selectPageCnBondLinkSecurdeal(map, rb);
		return result;
	}
	@Override
	public IfsCdtcCnBondLinkSecurdeal queryByDealNo(String dealNo) {
		return ifsCdtcCnBondLinkSecurdealMapper.selectByDealNo(dealNo);
	}
	

}
