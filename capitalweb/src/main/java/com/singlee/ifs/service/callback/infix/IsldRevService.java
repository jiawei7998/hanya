package com.singlee.ifs.service.callback.infix;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlFixedIncomeBean;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.financial.wks.intfc.SeclenService;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.ApproveOrderStatus;
import com.singlee.ifs.model.IfsCfetsrmbDpOffer;
import com.singlee.ifs.model.IfsRevIsld;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "IsldRevService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IsldRevService extends IfsApproveServiceBase {

	@Autowired
	IfsRevIsldMapper ifsRevIsldMapper;// 外汇类冲销mapper
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	ISecurServer securServer;
	@Autowired
	IfsCfetsrmbDpOfferMapper offerDpMapper;
	@Autowired
	IfsCfetsrmbCbtMapper cbtMapper;
	@Autowired
	IfsCfetsrmbSlMapper slMapper;
	@Autowired
	IfsCfetsrmbDpMapper dpMapper;
	@Autowired
    IBaseServer baseServer;// 查询opics系统时间
    @Autowired
    SeclenService seclenService;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		IfsRevIsld ifsRevIsld = new IfsRevIsld();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ticketId", serial_no);
		map.put("branchId", inst.getBranchId());
		ifsRevIsld = ifsRevIsldMapper.searchById(map);
		if (null == ifsRevIsld) {
			return;
		}
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(now);
		/** 根据id修改 审批状态 、审批发起时间 */
		ifsRevIsldMapper.updateIsldStatusByID(serial_no, status, date);

		/** 审批通过 插入opics冲销表 */
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			try {
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				Map<String, Object> sysMap = new HashMap<String, Object>();
				sysMap.put("p_code", "000002");// 参数代码
				sysMap.put("p_value", "ifJorP");// 参数值
				List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
				if (sysList.size() != 1) {
					JY.raise("系统参数有误......");
				}
				if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
					JY.raise("系统参数[参数类型]为空......");
				}
				// 调用java代码
				String callType = sysList.get(0).getP_type().trim();
				if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
					if (ifsRevIsld.getDealType().endsWith("SL")) {// 债券借贷
//						SlSecurityLendingBean slBean = new SlSecurityLendingBean(SlDealModule.BASE.BR,
//								SlDealModule.SECLEND.SERVER, SlDealModule.SECLEND.TAG, SlDealModule.SECLEND.DETAIL);
//						SlInthBean inthBean = new SlInthBean();
//						inthBean.setBr(SlDealModule.BASE.BR);
//						inthBean.setServer(SlDealModule.SECLEND.SERVER_R);
//						inthBean.setTag(SlDealModule.SECLEND.TAG_R);
//						inthBean.setDetail(SlDealModule.SECLEND.DETAIL);
//						inthBean.setSyst(SlDealModule.SECLEND.SYST);
//						inthBean.setSeq(SlDealModule.SECLEND.SEQ);
//						inthBean.setInoutind(SlDealModule.SECLEND.INOUTIND);
//						inthBean.setStatcode(SlDealModule.SECLEND.STATCODE);
//						inthBean.setPriority(SlDealModule.SECLEND.PRIORITY);
//						slBean.setInthBean(inthBean);
//						slBean.getInthBean().setFedealno(ifsRevIsld.getFedealno());
//						slBean.getInthBean().setDealno(ifsRevIsld.getDealNo());
//						// 插入opics冲销表
//						SlOutBean result = securServer.seclendRev(slBean);// 用注入的方式调用opics相关方法
//
//						if (!result.getRetStatus().equals(RetStatusEnum.S)) {
//							JY.raise("债券插入opics冲销表失败......");
//						} else {
//							// 冲销成功后修改交易的状态为已冲销
//							String ticketId = StringUtils.trimToEmpty(ifsRevIsld.getFedealno());
//							if (!"".equals(ticketId)) {
//								slMapper.updateApproveStatusRmbSlByID(ticketId);
//							}
//						}
					    
					    //直连
					    Date branchDate = baseServer.getOpicsSysDate(ifsRevIsld.getBr());
		                SldhWksBean sldhWksBean = getEntryWksBean(ifsRevIsld, branchDate);

		                SlOutBean outBean = seclenService.reverseTrading(sldhWksBean);
		                if (outBean.getRetStatus().equals(RetStatusEnum.S)) {
		                    //冲销成功后修改交易的状态为已冲销
		                    slMapper.updateApproveStatusRmbSlByID(ifsRevIsld.getTicketId());
		                } else {
		                    JY.raise("审批未通过......");
		                }

					} else if (ifsRevIsld.getDealType().endsWith("CBT")) {// 现券买卖

						SlFixedIncomeBean fiBean = new SlFixedIncomeBean(SlDealModule.BASE.BR, SlDealModule.FI.SERVER_R,
								SlDealModule.FI.TAG_R, SlDealModule.FI.DETAIL);
						SlInthBean inthBean = new SlInthBean();
						inthBean.setBr(SlDealModule.BASE.BR);
						inthBean.setServer(SlDealModule.FI.SERVER_R);
						inthBean.setTag(SlDealModule.FI.TAG_R);
						inthBean.setDetail(SlDealModule.FI.DETAIL);
						inthBean.setSyst(SlDealModule.FI.SYST);
						inthBean.setSeq(SlDealModule.FI.SEQ);
						inthBean.setInoutind(SlDealModule.FI.INOUTIND);
						inthBean.setStatcode(SlDealModule.FI.STATCODE);
						inthBean.setPriority(SlDealModule.FI.PRIORITY);
						fiBean.setInthBean(inthBean);
						fiBean.getInthBean().setFedealno(ifsRevIsld.getFedealno());
						fiBean.getInthBean().setDealno(ifsRevIsld.getDealNo());

						// 插入opics冲销表
						SlOutBean result = securServer.fiRev(fiBean);// 用注入的方式调用opics相关方法

						if (!result.getRetStatus().equals(RetStatusEnum.S)) {
							JY.raise(result.getRetMsg());
						} else {
							// 冲销成功后修改交易的状态为已冲销
							String ticketId = StringUtils.trimToEmpty(ifsRevIsld.getFedealno());
							if (!"".equals(ticketId)) {
								cbtMapper.updateApproveStatusRmbCbtByID(ticketId);
							}
						}

					} else if (ifsRevIsld.getDealType().endsWith("DP")) {// 债券发行
						List<IfsCfetsrmbDpOffer> ifsCfetsrmbDpOfferList = offerDpMapper
								.searchOfferList(ifsRevIsld.getFedealno());
						for (int i = 0; i < ifsCfetsrmbDpOfferList.size(); i++) {
							SlFixedIncomeBean fiBean = new SlFixedIncomeBean(SlDealModule.BASE.BR,
									SlDealModule.FI.SERVER_R, SlDealModule.FI.TAG_R, SlDealModule.FI.DETAIL);
							SlInthBean inthBean = new SlInthBean();
							inthBean.setBr(SlDealModule.BASE.BR);
							inthBean.setServer(SlDealModule.FI.SERVER_R);
							inthBean.setTag(SlDealModule.FI.TAG_R);
							inthBean.setDetail(SlDealModule.FI.DETAIL);
							inthBean.setSyst(SlDealModule.FI.SYST);
							inthBean.setSeq(i + "");
							inthBean.setInoutind(SlDealModule.FI.INOUTIND);
							inthBean.setStatcode(SlDealModule.FI.STATCODE);
							inthBean.setPriority(SlDealModule.FI.PRIORITY);
							fiBean.setInthBean(inthBean);
							fiBean.getInthBean().setFedealno(ifsCfetsrmbDpOfferList.get(i).getTicketId());
							fiBean.getInthBean().setDealno(ifsCfetsrmbDpOfferList.get(i).getDealNo());

							// 插入opics冲销表
							SlOutBean result = securServer.fiRev(fiBean);// 用注入的方式调用opics相关方法

							if (!result.getRetStatus().equals(RetStatusEnum.S)) {
								JY.raise(result.getRetMsg());
							} else {
								// 冲销成功后修改交易的状态为已冲销
								String ticketId = StringUtils.trimToEmpty(ifsRevIsld.getFedealno());
								if (!"".equals(ticketId)) {
									dpMapper.updateApproveStatusRmbDpByID(ticketId);
								}
							}
						}

					}

				} else if ("OPS_PROC".equals(callType)) {// 调用存储过程
					JY.raise("不支持冲销类型!!!!" + ifsRevIsld.getDealType());
				} else if ("SMT_JAVA".equals(callType)) {

				} else if ("SMT_PROC".equals(callType)) {
				}

			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				JY.raise("审批未通过......(" + e.getMessage() + ")");
			}
		}

	}

	/**
	 * 债券借贷冲销直连OPICS,数据准备
	 * @param ifsRevIsld
	 * @param branchDate
	 * @return
	 */
    private SldhWksBean getEntryWksBean(IfsRevIsld ifsRevIsld, Date branchDate) {
        SldhWksBean sldh =new SldhWksBean();
        FeeWksBean fee = new FeeWksBean();
        //InstId(BR)  TradeNo(ticket_id)
//        sldh.setInstId(ifsRevIsld.getBr());
        sldh.setTradeNo(ifsRevIsld.getFedealno());
        
        //费用信息 FEES  InstId(BR)  TradeNo(ticket_id)
//        fee.setInstId(ifsRevIsld.getBr());
//        fee.setTradeNo(ifsRevIsld.getDealNo());
        
        sldh.setFeesWksBean(fee);
        
        return sldh;
    }
}
