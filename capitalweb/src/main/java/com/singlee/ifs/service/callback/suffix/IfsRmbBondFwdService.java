package com.singlee.ifs.service.callback.suffix;

import com.singlee.capital.common.util.*;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.eu.mapper.TdEdDealLogMapper;
import com.singlee.capital.eu.model.TdEdDealLog;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.mapper.HrbCreditCustMapper;
import com.singlee.hrbextra.credit.model.HrbCreditCustQuota;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.ifs.service.callback.IfsApproveServiceBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "IfsRmbBondFwdService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IfsRmbBondFwdService extends IfsApproveServiceBase {

	@Resource
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper;
	@Resource
	IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper;
	@Autowired
	IfsOpicsBondMapper ifsOpicsBondMapper;
	@Autowired
	ISecurServer securServer;
	@Autowired
	TaUserMapper taUserMapper;
	@Autowired
	TaSysParamMapper taSysParamMapper;// 系统参数mapper
	@Autowired
	IfsOpicsCustMapper custMapper;
	@Autowired
	IBaseServer baseServer;// 查询opics系统时间
	@Autowired
	EdCustManangeService edCustManangeService;
	// 债券
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;
	@Autowired
	BondLimitTemplateMapper bondLimitTemplateMapper;
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;
	@Autowired
	TdEdDealLogMapper tdEdDealLogMapper;
	@Resource
	TaDictVoMapper taDictVoMapper;
	@Autowired
	TdEdDealLogMapper tdDealLogMapper;
	@Autowired
	HrbCreditLimitOccupyService hrbCreditLimitOccupyService;
	@Autowired
	HrbCreditCustMapper hrbCreditCustMapper;

	@Override
	public void statusChange(String flow_type, String flow_id, String serial_no, String status,
			String flowCompleteType) {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
		String date = dateFormat.format(now);
		ifsCfetsrmbBondfwdMapper.updateIfsCfetsrmbBondfwdByID(serial_no, status, date);
		IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = ifsCfetsrmbBondfwdMapper.selectById(serial_no);
		// 0.1 当前业务为空则不进行处理
		if (null == ifsCfetsrmbBondfwd) {
			return;
		}
		// 1.判断交易状态3:新增 8:注销 7:审批拒绝,进行额度释放
		if (StringUtils.equals(ApproveOrderStatus.New, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {// 3为退回状态，7为拒绝状态,8为作废
			try {
				if("P".equals(ifsCfetsrmbBondfwd.getMyDir())) {//买入
					hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(serial_no);
				}
			} catch (Exception e) {
				JY.raiseRException(e.getMessage());
			}
		}

		// 2.交易审批中状态,需要将交易纳入监控审批
		if (StringUtils.equals(ApproveOrderStatus.Approving, status)) {// 状态为"审批中"并且为第一次，才可以插入 审批监控表
			// 2.1需要考虑第一次审批和撤回再次审批。
			IfsFlowMonitor monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			if (null != monitor) {// 不存在,则说明存在,需要删除再进行保存
				ifsFlowMonitorMapper.deleteById(serial_no);
			}
			// 2.2 保存监控表数据
			IfsFlowMonitor ifsFlowMonitor = new IfsFlowMonitor();
			ifsFlowMonitor.setTicketId(ifsCfetsrmbBondfwd.getTicketId());// 设置监控表-内部id
			ifsFlowMonitor.setContractId(ifsCfetsrmbBondfwd.getContractId());// 设置监控表-成交单编号
			ifsFlowMonitor.setApproveStatus(ifsCfetsrmbBondfwd.getApproveStatus());// 设置监控表-审批状态
			// 判断是现券买卖还是外币债（人民币或非人民币）
			// 债券信息实体类
			IfsOpicsBond ifsOpicsBond = ifsOpicsBondMapper.searchById(ifsCfetsrmbBondfwd.getBondCode());
			if (ifsOpicsBond == null) {
				JY.raise("交易的券不存在......");
			}
			if (ifsOpicsBond.getCcy() == null) {
				JY.raise("交易的券的币种不存在......");
			}
			ifsFlowMonitor.setPrdName("债券远期");// 设置监控表-产品名称
			ifsFlowMonitor.setPrdNo("bfwd");// 设置监控表-产品编号：作为分类
			ifsFlowMonitor.setTradeDate(ifsCfetsrmbBondfwd.getForDate());// 设置监控表-交易日期
			ifsFlowMonitor.setSponsor(ifsCfetsrmbBondfwd.getSponsor());// 设置监控表-审批发起人
			ifsFlowMonitor.setTradeType("1");// 交易类型，0：事前交易，1：正式交易
			String custNo = ifsCfetsrmbBondfwd.getCno();
			if (null != custNo || "".equals(custNo)) {
				ifsFlowMonitor.setCustNo(custNo);// 设置监控表-客户号
				IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchIfsOpicsCust(custNo);
				ifsFlowMonitor.setCustName(ifsOpicsCust.getCliname());// 设置监控表-客户名称(全称)
			}
			StringBuffer buffer = new StringBuffer("");
			// 2.3查询客制化字段
			List<LinkedHashMap<String, String>> listmap = ifsApproveElementMapper
					.searchTradeElements("IFS_CFETSRMB_BONDFWD");
			if (listmap.size() == 1) {
				LinkedHashMap<String, String> hashmap = listmap.get(0);
				String tradeElements = hashmap.get("TRADE_GROUP");
				String commentsGroup = hashmap.get("COMMENTSGROUPS");
				if (tradeElements != "null" || tradeElements != "") {
					buffer.append("{");
					String sqll = "select " + tradeElements + " from IFS_CFETSRMB_CBT where TICKET_ID=" + "'"
							+ serial_no + "'";
					LinkedHashMap<String, Object> swap = ifsCfetsrmbCbtMapper.searchProperty(sqll);
					String[] str = tradeElements.split(",");
					String[] comments = commentsGroup.split(",");
					for (int j = 0; j < str.length; j++) {
						String kv = str[j];
						String key = comments[j];
						String value = String.valueOf(swap.get(kv));
						buffer.append(key).append(":").append(value);
						if (j != str.length - 1) {
							buffer.append(",");
						}
					}
					buffer.append("}");
				}
			}
			ifsFlowMonitor.setTradeElements(buffer.toString());
			ifsFlowMonitorMapper.insert(ifsFlowMonitor);

			TdEdDealLog dealLog = tdDealLogMapper.selectDealLog(serial_no);
			if(null == dealLog || "0".equals(dealLog.getIsActive())) {
				try {
					if("P".equals(ifsCfetsrmbBondfwd.getMyDir())) {//买入
						LimitOccupy(serial_no, TradeConstants.ProductCode.BFWD);
					}
				} catch (Exception e) {
					JY.raiseRException(e.getMessage());
				}
			}

		}
		// 审批通过 删掉审批监控表中的数据，并将该记录插入审批完成表,7审批拒绝，8
		if (StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)
				|| StringUtils.equals(ApproveOrderStatus.ApprovedNoPass, status)
				|| StringUtils.equals(ApproveOrderStatus.Cancle, status)) {
			IfsFlowMonitor monitor = new IfsFlowMonitor();
			monitor = ifsFlowMonitorMapper.getEntityById(serial_no);
			monitor.setApproveStatus(status);// 设置审批完成表的审批状态 为 6，即审批完成
			ifsFlowMonitorMapper.deleteById(serial_no);
			Map<String, Object> mapEntity = BeanUtil.beanToMap(monitor);
			ifsFlowCompletedMapper.insert(mapEntity);
		}
		// 3.1未审批通过则不导入Opics系统
		if (!StringUtils.equals(ApproveOrderStatus.ApprovedPass, status)) {
			return;
		}

		try {
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_code", "000002");// 参数代码
			sysMap.put("p_value", "ifJorP");// 参数值
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			if (sysList.size() != 1) {
				JY.raise("系统参数有误......");
			}
			if (null == sysList.get(0).getP_type() || "".equals(sysList.get(0).getP_type())) {
				JY.raise("系统参数[参数类型]为空......");
			}
			// 调用java代码
			String callType = sysList.get(0).getP_type().trim();
			if (StringUtils.equals(callType, InterfaceMode.JAVA)) {
				Date branchDate = baseServer.getOpicsSysDate("01");
				SlFixedIncomeBean slFixedIncomeBean = getEntry(ifsCfetsrmbBondfwd, branchDate);
				SlOutBean result = securServer.fi(slFixedIncomeBean);
				if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("审批未通过，插入opics失败......");
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("ticketId", ifsCfetsrmbBondfwd.getTicketId());
					map.put("satacode", "-1");
					ifsCfetsrmbBondfwdMapper.updateStatcodeByTicketId(map);
				}
			} else if (StringUtils.equals(callType, InterfaceMode.PROCEDURE)) {
				// 将实体对象转换成map
				Map<String, String> map = BeanUtils.describe(ifsCfetsrmbBondfwd);
				SlOutBean result = securServer.bredProc(map);// 现券买卖存储过程
				if (!"999".equals(result.getRetCode())) {
					JY.raise("审批未通过，执行存储过程失败......");
				}
			}
		} catch (Exception e) {
			JY.raiseRException("审批未通过......", e);
		}
	}

	@Override
	public void LimitOccupy(String serial_no, String prd_no) throws Exception {
		IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd = ifsCfetsrmbBondfwdMapper.selectById(serial_no);
		Map<String, Object> remap = new HashMap<>();
		remap.put("custNo", ifsCfetsrmbBondfwd.getCustNo());
		remap.put("custType", ifsCfetsrmbBondfwd.getCustType());
		HrbCreditCustQuota hrbCreditCustQuota = hrbCreditCustMapper.selectHrbCreditCustQuota(remap);//获取额度的币种
		remap.put("weight", ifsCfetsrmbBondfwd.getWeight());
		remap.put("productCode", prd_no);
		remap.put("institution", ifsCfetsrmbBondfwd.getBuyInst());
		BigDecimal amt = calcuAmt(ifsCfetsrmbBondfwd.getTradeAmount(), ifsCfetsrmbBondfwd.getWeight());
		remap.put("amt", ccyChange(amt,"CNY",hrbCreditCustQuota.getCurrency()));
		remap.put("currency", hrbCreditCustQuota.getCurrency());
		remap.put("vdate", ifsCfetsrmbBondfwd.getForDate());
		remap.put("mdate", ifsCfetsrmbBondfwd.getSettlementDate());
		remap.put("serial_no", serial_no);
		hrbCreditLimitOccupyService.LimitOccupyInsert(remap);
	}

	/**
	 * 获取交易信息
	 * 
	 * @param ifsCfetsrmbBondfwd
	 * @param branchDate
	 * @return
	 */
	private SlFixedIncomeBean getEntry(IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd, Date branchDate) {
		SlFixedIncomeBean slFixedIncomeBean = new SlFixedIncomeBean(ifsCfetsrmbBondfwd.getBr(), SlDealModule.FI.SERVER, SlDealModule.FI.TAG,
				SlDealModule.FI.DETAIL);
		InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
		contraPatryInstitutionInfo.setInstitutionId(ifsCfetsrmbBondfwd.getCno());
		slFixedIncomeBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
		slFixedIncomeBean.setSecsacct(ifsCfetsrmbBondfwd.getBuyCustname());
		/************* 设置交易方向 ***************************/
		if (ifsCfetsrmbBondfwd.getMyDir().endsWith("P")) {
			slFixedIncomeBean.setPs(PsEnum.P);
		}
		if (ifsCfetsrmbBondfwd.getMyDir().endsWith("S")) {
			slFixedIncomeBean.setPs(PsEnum.S);
		}
		slFixedIncomeBean.setInvtype(ifsCfetsrmbBondfwd.getInvtype());
		// 结算金额
		slFixedIncomeBean.setOrigamt(
				String.valueOf(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsrmbBondfwd.getSettlementAmount() + ""), 2)));
		/************* 设置本方交易员 ***************************/
		InstitutionBean institutionInfo = new InstitutionBean();
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", ifsCfetsrmbBondfwd.getBuyTrader());// 交易员id
		userMap.put("branchId", ifsCfetsrmbBondfwd.getBranchId());// 机构
		TaUser user = taUserMapper.queryUserByMap(userMap);
		String trad = StringUtils.defaultIfEmpty(user.getOpicsTrad(),SlDealModule.FI.IOPER);
		institutionInfo.setTradeId(trad);
		slFixedIncomeBean.getInthBean().setIoper(trad);
		slFixedIncomeBean.setInstitutionInfo(institutionInfo);
		/************* 设置债券信息 ***************************/
		SecurityInfoBean securityInfo = new SecurityInfoBean();
		securityInfo.setSecurityId(ifsCfetsrmbBondfwd.getBondCode());
		slFixedIncomeBean.setSecurityInfo(securityInfo);
		slFixedIncomeBean.setValueDate(DateUtil.parse(ifsCfetsrmbBondfwd.getSettlementDate()));
		slFixedIncomeBean.setDealDate(DateUtil.parse(ifsCfetsrmbBondfwd.getForDate()));

		SlExternalBean externalBean = new SlExternalBean();
		externalBean.setCost(ifsCfetsrmbBondfwd.getCost());
		externalBean.setPort(ifsCfetsrmbBondfwd.getPort());
		externalBean.setBroker(SlDealModule.BROKER);
		externalBean.setDealtext(ifsCfetsrmbBondfwd.getContractId());
		externalBean.setVerind(SlDealModule.FI.VERIND);
		// 设置清算路径1
		externalBean.setCcysmeans(ifsCfetsrmbBondfwd.getCcysmeans());
		// 设置清算账户1
		externalBean.setCcysacct(ifsCfetsrmbBondfwd.getCcysacct());
		// 设置清算路径2
		externalBean.setCtrsmeans(ifsCfetsrmbBondfwd.getCtrsmeans());
		// 设置清算账户2
		externalBean.setCtrsacct(ifsCfetsrmbBondfwd.getCtrsacct());
		slFixedIncomeBean.setVerdate(branchDate);
		slFixedIncomeBean.setCcysmeans(ifsCfetsrmbBondfwd.getCcysmeans());
		slFixedIncomeBean.setCcysacct(ifsCfetsrmbBondfwd.getCcysacct());
		slFixedIncomeBean.setExternalBean(externalBean);
		SlInthBean inthBean = slFixedIncomeBean.getInthBean();
		inthBean.setFedealno(ifsCfetsrmbBondfwd.getTicketId());
		inthBean.setLstmntdate(branchDate);
		slFixedIncomeBean.setInthBean(inthBean);
		slFixedIncomeBean.setStandinstr("Y");
		slFixedIncomeBean.setUsualid("");
		// 判断是否延期交易
		IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(ifsCfetsrmbBondfwd.getBondCode());
		String settleDays = "0";
		if (ifsOpicsBond != null) {
			settleDays = ifsOpicsBond.getSettdays();
			settleDays = settleDays.trim();
		}
		if ("".equals(settleDays)) {
			settleDays = "0";
		}
		int betweendays = DateUtil.daysBetween(ifsCfetsrmbBondfwd.getForDate(), ifsCfetsrmbBondfwd.getSettlementDate());
		int IsettleDays = Integer.valueOf(settleDays);
		if (betweendays > IsettleDays) {
			slFixedIncomeBean.setDelaydelivind("Y");
		} else {
			slFixedIncomeBean.setDelaydelivind("N");
		}

		// 交易金额
		String unit = ifsOpicsBond != null && null != ifsOpicsBond.getSecunit() && !"".equals(ifsOpicsBond.getSecunit()) ?
				ifsOpicsBond.getSecunit() : "FMT";
		if("FMT".equals(unit)) {
			slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
					Double.parseDouble(ifsCfetsrmbBondfwd.getTotalFaceValue().multiply(new BigDecimal(10000)) + ""), 2)));
		}else {
			//
			slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(
					Double.parseDouble(ifsCfetsrmbBondfwd.getTotalFaceValue().multiply(new BigDecimal(1)) + ""), 2)));
		}

//		// price
//		SecurityAmtDetailBean securityAmt = new SecurityAmtDetailBean();
////		securityAmt.setPrice(ifsCfetsrmbBondfwd.getPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
//		securityAmt.setPrice(ifsCfetsrmbBondfwd.getCleanPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
//		slFixedIncomeBean.setSecurityAmt(securityAmt);
//		// 交易金额
//		// slFixedIncomeBean.setOrigqty(String.valueOf(MathUtil.roundWithNaN(Double.parseDouble(ifsCfetsrmbBondfwd.getAmount().multiply(new
//		// BigDecimal(10000))+""),2)));

		return slFixedIncomeBean;
	}

	@Override
	public Map<String, Object> getReportExcel(Map<String, Object> paramer) {
		Object approveType = paramer.get("approveType");
		List<IfsCfetsrmbBondfwd> list = null;
		paramer.put("isActive", DictConstants.YesNo.YES);
		paramer.put("userId", SlSessionHelper.getUserId());
		if ("mine".equals(approveType)) {// 我发起的
			list = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdMine(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else if ("approve".equals(approveType)) {// 待审批
			String[] orderStatus = ApproveStatus.APPROVESTATUSUNFINAL.split(",");
			paramer.put("approveStatusNo", orderStatus);
			list = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdUnfinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		} else {// 已审批
			String approveStatusNo = ParameterUtil.getString(paramer, "approveStatus", "");
			if (StringUtil.isNullOrEmpty(approveStatusNo)) {
				String[] orderStatus = ApproveStatus.APPROVESTATUSFINAL.split(",");
				paramer.put("approveStatusNo", orderStatus);
			}
			list = ifsCfetsrmbBondfwdMapper.searchIfsCfetsrmbBondfwdFinished(paramer, ParameterUtil.getRowBounds(paramer)).getResult();
		}
		//字典转换
		Map<String, String> tradingList = taDictVoMapper.getTadictTree("trading").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> invtypeList = taDictVoMapper.getTadictTree("invtype").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		Map<String, String> settlementMethodList = taDictVoMapper.getTadictTree("SettlementMethod").stream().collect(Collectors.toMap(TaDictVo::getDict_key, TaDictVo::getDict_value));
		for (IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd: list) {
			ifsCfetsrmbBondfwd.setMyDir(tradingList.get(ifsCfetsrmbBondfwd.getMyDir()));
			ifsCfetsrmbBondfwd.setInvtype(invtypeList.get(ifsCfetsrmbBondfwd.getInvtype()));
			ifsCfetsrmbBondfwd.setSettlementMethod(settlementMethodList.get(ifsCfetsrmbBondfwd.getSettlementMethod()));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list",list);
		return map;
	}
}
