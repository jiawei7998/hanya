package com.singlee.ifs.service;

import java.util.List;
import java.util.Map;

import com.singlee.ifs.model.IfsIntfcIycrBean;

public interface IfsIntfcIycrService {
	
	List<IfsIntfcIycrBean> getlistIycr(Map<String, Object>  map);
	
	/**
     * 市场数据 先查询，后插入接口表
     */
    String doBussExcelPropertyToDataBase(List<IfsIntfcIycrBean> irevBean);

    /**
     * 市场数据 插入irev表
     */
    String dataBaseToIycr(String date);
    /**
     * 市场数据 插入IRHS表
     */
    String dataBaseToIrhs(String date);

}
