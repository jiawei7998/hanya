package com.singlee.ifs.service;

import com.singlee.ifs.model.IfsOpsKpsjBean;

import java.util.List;
import java.util.Map;

/***
 * 增值税送交易 开票数据接口，此报表为日报，每天晚上23点生成,涉及到的交易有拆借，回购，债券三类交易。
 * 
 * @author copysun
 */
public interface IfsOpsKpsjService {

	List<IfsOpsKpsjBean> getData(Map<String,Object> map);

}