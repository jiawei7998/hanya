package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.ifs.model.IfsOpicsNost;

import java.util.Map;

/***
 * 清算路径
 * @author lij
 *
 */
public interface IfsOpicsNostService {
	
	//分页查询
	Page<IfsOpicsNost> searchPageOpicsNost(Map<String,Object> map);
	
	//新增
	void insert(IfsOpicsNost entity);
	
	//修改
	void updateById(IfsOpicsNost entity);
	
	//删除
	void deleteById(String nos);
	
	//根据id查询实体类
	IfsOpicsNost searchNostById(String id);
	
	//更新同步状态
	void updateStatus(IfsOpicsNost entity);
	
	//批量校准
	SlOutBean batchCalibration(String type,String[] noss);
}
