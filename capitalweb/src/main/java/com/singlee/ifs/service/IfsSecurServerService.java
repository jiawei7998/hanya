package com.singlee.ifs.service;

import com.singlee.capital.credit.pojo.SecurServerBean;

import java.util.Map;

public interface IfsSecurServerService {
	
	public SecurServerBean getAdjAmtList(Map<String, Object> map);
	
	public void updateByDealNo(Map<String, Object> map);

	public void insertAdjAmtList(Map<String, Object> map);

	public void deleteByDealNo(String dealno);
	
}



