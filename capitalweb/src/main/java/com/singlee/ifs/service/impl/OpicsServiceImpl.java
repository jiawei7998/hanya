package com.singlee.ifs.service.impl;

import com.singlee.ifs.service.OpicsService;
import org.springframework.stereotype.Service;

/**
 * 用于opics系统数据相关操作，接口导入，数据查询等
 * 
 * @author shenzl
 * 
 */
@Service
public class OpicsServiceImpl implements OpicsService {
	
}
