package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsSubjectdetailMapper;
import com.singlee.ifs.model.IfsSubjectdetail;
import com.singlee.ifs.service.IfsSubjectdetailService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/10/18 14:24
 * @description：opics科目余额
 * @modified By：
 * @version:
 */
@Service
public class IfsSubjectdetailServiceImpl  implements IfsSubjectdetailService {

    @Autowired
    IfsSubjectdetailMapper ifsSubjectdetailMapper;
    @Override
    public Page<IfsSubjectdetail> getList(Map<String, Object> map) {
        RowBounds rowBounds = ParameterUtil.getRowBounds(map);
        return ifsSubjectdetailMapper.searchPage(map,rowBounds);
    }

    @Override
    public void updateOpicsBalance(Map<String, Object> map) {
        ifsSubjectdetailMapper.updateOpicsBalance(map);
    }

    @Override
    public void addOpicsBalance(Map<String, Object> map) {
        ifsSubjectdetailMapper.addOpicsBalance(map);
    }

    @Override
    public List<IfsSubjectdetail> getListNoPage(Map<String, Object> map) {
        return ifsSubjectdetailMapper.getListNoPage(map);
    }

    @Override
    public void insertList(List<IfsSubjectdetail> list){
        ifsSubjectdetailMapper.insertList(list);
    }

    @Override
    public void updateList(List<IfsSubjectdetail> list) {
        ifsSubjectdetailMapper.updateList(list);
    }
}
