package com.singlee.ifs.service;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIotd;

import java.util.Map;

public interface IfsRevIotdService {
	
	// 新增
	public void insert(IfsRevIotd entity);

	// 修改
	void updateById(IfsRevIotd entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsRevIotd> searchPageOptParam(Map<String, Object> map);
	
	/**
	 * 根据请求参数查询分页列表-我发起的、待审批、已审批列表
	 * @param params - 请求参数
	 * @return 
	 * @author ym
	 * @date 2018-08-13
	 */
	public Page<IfsRevIotd> getRevIotdPage (Map<String, Object> params, int isFinished);

}
