package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.ifs.mapper.IfsOpicsGlnoMapper;
import com.singlee.ifs.model.IfsOpicsKCoreKm;
import com.singlee.ifs.service.IfsOpicsGlnoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class IfsOpicsGlnoServiceImpl implements IfsOpicsGlnoService {

	@Resource
	IfsOpicsGlnoMapper ifsOpicsGlnoMapper;

	@Override
	public Page<IfsOpicsKCoreKm> searchPageOpicsGlno(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsKCoreKm> result = ifsOpicsGlnoMapper.searchPageOpicsGlno(map, rb);
		return result;
	}

}