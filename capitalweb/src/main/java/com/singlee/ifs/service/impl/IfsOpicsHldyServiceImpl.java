package com.singlee.ifs.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.calendar.util.CalendarUtil;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlHldyBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IStaticServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.ifs.baseUtil.noBlankInString;
import com.singlee.ifs.mapper.IfsOpicsHldyMapper;
import com.singlee.ifs.model.IfsOpicsHldy;
import com.singlee.ifs.service.IfsOpicsHldyService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class IfsOpicsHldyServiceImpl implements IfsOpicsHldyService {
	@Resource
	IfsOpicsHldyMapper ifsOpicsHldyMapper;
	@Autowired
	IStaticServer iStaticServer;
	@Autowired
	BatchDao batchDao;
	@Override
	public Page<IfsOpicsHldy> searchPageOpicsHldy(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<IfsOpicsHldy> result = ifsOpicsHldyMapper.searchPageOpicsHldy(map, rb);
		return result;
	}

	@Override
	public SlOutBean batchCalibration(String type, List<IfsOpicsHldy> list) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			// 从opics库查询所有的数据
			List<SlHldyBean> listHldyOpics = iStaticServer.synchroHldy();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			List<IfsOpicsHldy> insertHldy = new ArrayList<>();

			/***** 对选中的行进行校准 ****************************/
			if ("1".equals(type)) {// 选中行校准

				for (int i = 0; i < list.size(); i++) {
					for (int j = 0; j < listHldyOpics.size(); j++) {
						// 若opics库里有相同的节假日,就将本地库的更新成opics里的,其中中文描述、备注还是用本地的,不更新
						if (listHldyOpics.get(j).getCalendarid().trim().equals(list.get(i).getCalendarid()) && listHldyOpics.get(j).getHolidaytype().trim().equals(list.get(i).getHolidaytype())
								&& listHldyOpics.get(j).getHolidate().equals(list.get(i).getHolidate())) {
							IfsOpicsHldy entity = new IfsOpicsHldy();
							Map<String, Object> keyMap = new HashMap<String, Object>();
							keyMap.put("calendarid", list.get(i).getCalendarid());
							keyMap.put("holidaytype", list.get(i).getHolidaytype());
							keyMap.put("holidate", list.get(i).getHolidate());
							entity = ifsOpicsHldyMapper.searchById(keyMap);

							// 去掉opics库 中实体类中参数为STRING类型的属性的前后空格
							noBlankInString.stripStringProperty(listHldyOpics.get(j));
							// opics库中的实体转成map
							Map<String, Object> map1 = BeanUtil.beanToMap(listHldyOpics.get(j));
							// 其中中文描述、备注还是用本地的,不更新
							map1.put("status", "1");
							map1.put("operator", SlSessionHelper.getUserId());
							map1.put("remark", entity.getRemark());

							// 把map转成本地库对应的实体类
							entity = (IfsOpicsHldy) BeanUtil.mapToBean(IfsOpicsHldy.class, map1);

							ifsOpicsHldyMapper.updateById(entity);
							break;
						}
						// 若opics库里没有，就改变同步状态为 未同步,其余不改变
						if (j == listHldyOpics.size() - 1) {
							Map<String, Object> keyMap1 = new HashMap<String, Object>();
							keyMap1.put("calendarid", list.get(i).getCalendarid());
							keyMap1.put("holidaytype", list.get(i).getHolidaytype());
							keyMap1.put("holidate", list.get(i).getHolidate());
							ifsOpicsHldyMapper.updateStatus0ById(keyMap1);
						}

					}

				}

				/************* 对本地库所有节假日记录进行校准 ***************/
			} else {// 全部校准
					// 从本地库查询所有的数据
				List<IfsOpicsHldy> listHldyLocal = ifsOpicsHldyMapper.searchAllOpicsHldy();
				/** 1.以opics库为主，更新或插入本地库 */
				for (int i = 0; i < listHldyOpics.size(); i++) {

					/***** ++++++++++++++++++++ 实体设置 start ++++++++++++++++++++++ ***********/
					IfsOpicsHldy entity = new IfsOpicsHldy();

					// 去掉实体类中参数为STRING类型的属性的前后空格
					noBlankInString.stripStringProperty(listHldyOpics.get(i));
					// opics库中的实体转成map
					Map<String, Object> map1 = BeanUtil.beanToMap(listHldyOpics.get(i));
					// 其中中文描述、备注还是用本地的,不更新
					map1.put("status", "1");
					map1.put("operator", SlSessionHelper.getUserId());
					map1.put("remark", entity.getRemark());

					// 把map转成本地库对应的实体类
					entity = (IfsOpicsHldy) BeanUtil.mapToBean(IfsOpicsHldy.class, map1);

					/***** ++++++++++++++++++++ 实体设置 end +++++++++++++++++++++++ ***********/
					insertHldy.add(entity);
//					if (listHldyLocal.size() == 0) {// 本地库无数据
//						ifsOpicsHldyMapper.insert(entity);
//					} else {// 本地库有数据
//						ifsOpicsHldyMapper.deleteById1(entity);
//						ifsOpicsHldyMapper.insert(entity);
//						for (int j = 0; j < listHldyLocal.size(); j++) {
//
//							// opics库中节假日有与本地的节假日相等，就更新本地的节假日其他的内容
//							if (listHldyOpics.get(i).getCalendarid().trim().equals(listHldyLocal.get(j).getCalendarid())
//									&& listHldyOpics.get(i).getHolidaytype().trim().equals(listHldyLocal.get(j).getHolidaytype())
//									&& listHldyOpics.get(i).getHolidate().equals(listHldyLocal.get(j).getHolidate())) {
//								ifsOpicsHldyMapper.updateById(entity);
//								break;
//							}
//							// 若没有，就插入本地库
//							if (j == listHldyLocal.size() - 1) {
//								ifsOpicsHldyMapper.insert(entity);
//							}
//						}
//
//					}

				}
				/** 2.若本地库的数据多于opics库，则本地库里有，opics库里没有的，状态要改为未同步 */
				// 遍历完opics库之后，从本地库【再次】查询所有的数据,遍历本地库
				List<IfsOpicsHldy> listHldyLocal2 = ifsOpicsHldyMapper.searchAllOpicsHldy();
				if (listHldyLocal2.size() > listHldyOpics.size()) {
					for (int i = 0; i < listHldyLocal2.size(); i++) {
						for (int j = 0; j < listHldyOpics.size(); j++) {
							if (listHldyOpics.get(j).getCalendarid().trim().equals(listHldyLocal2.get(i).getCalendarid())
									&& listHldyOpics.get(j).getHolidaytype().trim().equals(listHldyLocal2.get(i).getHolidaytype())
									&& listHldyOpics.get(j).getHolidate().equals(listHldyLocal2.get(i).getHolidate())) {
								break;
							}
							// 若opics库里没有，就改变同步状态为 未同步
							if (j == listHldyOpics.size() - 1) {

								listHldyLocal2.get(i).setStatus("0");
								listHldyLocal2.get(i).setOperator(SlSessionHelper.getUserId());
								listHldyLocal2.get(i).setLstmntdte(new Date());
								insertHldy.add(listHldyLocal2.get(i));
//								ifsOpicsHldyMapper.updateById(listHldyLocal2.get(i));

							}

						}

					}

				}
				batchDao.batch("com.singlee.ifs.mapper.IfsOpicsHldyMapper.deleteById1",insertHldy,500);
				batchDao.batch("com.singlee.ifs.mapper.IfsOpicsHldyMapper.insert",insertHldy,500);

			}

		} catch (ParseException e) {
			outBean.setRetCode(SlErrors.FAILED_PASER.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_PASER.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (RemoteConnectFailureException e) {
			outBean.setRetCode(SlErrors.FAILED_RMI_OPICS.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED_RMI_OPICS.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		} catch (Exception e) {
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			outBean.setRetMsg(SlErrors.FAILED.getErrMsg() + ":" + e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;

	}

	@Override
	public int queryDateById(IfsOpicsHldy entity) {
		int i = ifsOpicsHldyMapper.searchById1(entity);
		return i;
	}

	@Override
	public String insert(IfsOpicsHldy entity) {
		int t = ifsOpicsHldyMapper.searchById1(entity);

		if (t != 0) {
			JY.info(MsgUtils.getMessage("error.trade.0002"));
			return "日期(" + entity.getHolidate() + ")已经存在!";
		} else {
			ifsOpicsHldyMapper.insert(entity);

			return "000";
		}

	}

	@Override
	public void delete(IfsOpicsHldy entity) {
		ifsOpicsHldyMapper.deleteById1(entity);

	}

	@Override
	public List<IfsOpicsHldy> getCalendarByMonth(Map<String, Object> param) throws Exception {
		Date bizDate = CalendarUtil.getUtilDate(param.get("bizDate").toString().trim(), "yyyy-MM-dd");
		param.put("startDate", CalendarUtil.getDateStr(CalendarUtil.getFirstDayOfMonth(bizDate), "yyyy-MM-dd"));
		param.put("endDate", CalendarUtil.getDateStr(CalendarUtil.getLastDayOfMonth(bizDate), "yyyy-MM-dd"));

		List<IfsOpicsHldy> list = ifsOpicsHldyMapper.getCalendarByMonth(param);
		return list;
	}

	/***
	 * 根据id查询实体
	 */
	@Override
	public IfsOpicsHldy queryEntityById(Map<String, Object> map) {

		IfsOpicsHldy entity1 = ifsOpicsHldyMapper.searchById(map);

		return entity1;
	}

	@Override
	public IfsOpicsHldy searchEntityById(IfsOpicsHldy entity) {
		IfsOpicsHldy entity1 = ifsOpicsHldyMapper.searchEntity(entity);
		return entity1;
	}

}
