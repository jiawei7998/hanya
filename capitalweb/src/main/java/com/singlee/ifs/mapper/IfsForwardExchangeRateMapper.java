package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsForwardExchangeRateBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface IfsForwardExchangeRateMapper extends Mapper<IfsForwardExchangeRateBean>{

	/**
	 * 分页查询
	 */
	Page<IfsForwardExchangeRateBean> searchPageRate(Map<String, Object> map, RowBounds rb);

	List<String> getBr();

	List<IfsForwardExchangeRateBean> searchRateList(Map<String, Object> map);

	List<Map<String,String>> getRevp(Map<String,Object> remap);

}