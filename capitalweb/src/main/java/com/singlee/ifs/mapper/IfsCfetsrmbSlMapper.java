package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbSl;
import com.singlee.ifs.model.IfsCfetsrmbSlKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbSlMapper {
    int deleteByPrimaryKey(IfsCfetsrmbSlKey key);

    int insert(IfsCfetsrmbSl record);

    IfsCfetsrmbSl selectByPrimaryKey(IfsCfetsrmbSlKey key);
    
    IfsCfetsrmbSl selectById(Map<String, String> map);

    int updateByPrimaryKey(IfsCfetsrmbSl record);
    
    Page<IfsCfetsrmbSl> selectIfsCfetsrmbSlList(IfsCfetsrmbSl record);

	void updateRmbSlByID(String serial_no, String status,String date);
	
	void updateApproveStatusRmbSlByID(String ticketId);

	Page<IfsCfetsrmbSl> getRmbSlMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbSl> getRmbSlMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbSl> getRmbSlMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbSl searchBondLon(@Param(value = "ticketId") String ticketId);

	IfsCfetsrmbSl selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	public List<IfsCfetsrmbSl> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbSl> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/**
	 * updateTransTypeByPrimaryKey:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author Administrator
	 * @param updatesql
	 * @since JDK 1.6
	 */
	void updateTransTypeByPrimaryKey(@Param("updatesql")String updatesql);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);

	List<IfsCfetsrmbSl> getCfetsrmbSlList();

	void updateCfetsrmbSlByFedealno(Map<String, Object> map);

	/**
	 * 总交易查询（债券借贷）
	 * @param map
	 */
	Page<IfsCfetsrmbSl> searchTotalDealZJJD(Map<String, Object> params,
			RowBounds rowBounds);

	Page<Map<String, Object>> getSlMini(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbSl searchCfetsrmbSlByTicketId(Map<String, Object> map);

	public String selectApproveStatuesByDealNo(@Param("selectSql")String selectSql);

	Page<Map<String, Object>> getDpMini(Map<String, Object> params,
			RowBounds rowBounds);
}