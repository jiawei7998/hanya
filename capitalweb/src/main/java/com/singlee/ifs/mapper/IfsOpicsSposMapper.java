package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSpos;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/17 14:28
 * @description：${description}
 * @modified By：
 * @version:
 */
@Mapper
public interface IfsOpicsSposMapper {
    /**
     * delete by primary key
     *
     * @param secid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("secid") String secid, @Param("invtype") String invtype, @Param("cost") String cost, @Param("port") String port, @Param("acctngtype") String acctngtype);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(IfsOpicsSpos record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(IfsOpicsSpos record);

    /**
     * select by primary key
     *
     * @param secid primary key
     * @return object by primary key
     */
    IfsOpicsSpos selectByPrimaryKey(@Param("secid") String secid, @Param("invtype") String invtype, @Param("cost") String cost, @Param("port") String port, @Param("acctngtype") String acctngtype);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IfsOpicsSpos record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IfsOpicsSpos record);

    int updateBatch(List<IfsOpicsSpos> list);

    int updateBatchSelective(List<IfsOpicsSpos> list);

    int batchInsert(@Param("list") List<IfsOpicsSpos> list);

    Page<IfsOpicsSpos> getSposPage(Map<String, Object> map, RowBounds row);

    /**
     * 调用存储过程
     * @param map
     */
    void ifsOpicsSposCreate(Map<String, Object> map);
}