package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCurrentDepositOut;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsCurrentDepositOutMapper {
    int deleteByPrimaryKey(String ticketId);

    int insert(IfsCurrentDepositOut record);

    int insertSelective(IfsCurrentDepositOut record);

    IfsCurrentDepositOut selectByPrimaryKey(String ticketId);

    Page<IfsCurrentDepositOut> selectCurrentPage(Map<String,Object> map, RowBounds rb);

    int updateByPrimaryKeySelective(IfsCurrentDepositOut record);

    int updateByPrimaryKey(IfsCurrentDepositOut record);
}