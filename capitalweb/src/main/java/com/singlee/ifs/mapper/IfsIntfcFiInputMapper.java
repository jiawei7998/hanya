package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.ifs.model.IfsIntfcFi;
import com.singlee.ifs.model.IfsIntfcFiReverse;
import com.singlee.ifs.model.IfsIntfcFiStatus;

public interface IfsIntfcFiInputMapper {
	
	String getOpicsSysDate(Map<String,Object> remap);
	
	List<IfsIntfcFi> queryAllConditionFiList(Map<String,Object> map);
	
	IfsIntfcFiStatus queryFiStatusByDealnoSeq(Map<String,Object> map);
	
	int updateFiStatusByDealnoSeq(IfsIntfcFiStatus fiStatus);
	
	int insertFiStatusByNew(IfsIntfcFiStatus fiStatus);
	
	List<IfsIntfcFiReverse> queryAllConditionFiReverseList(Map<String,Object> map);
	
	int updateFxStatusByChoisRefHisNo(IfsIntfcFiStatus fiStatus);

}
