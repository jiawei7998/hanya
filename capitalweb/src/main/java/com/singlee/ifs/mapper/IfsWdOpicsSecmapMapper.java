package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWdOpicsSecmap;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


public interface IfsWdOpicsSecmapMapper{
	// 新增
	void insert(IfsWdOpicsSecmap entity);
	/**
	 * 分页查询
	 * 
	 * @param map
	 * @param rb 
	 * @return
	 */
	Page<IfsWdOpicsSecmap> getWdOpicsSecmapList(Map<String, Object> map, RowBounds rb);
	void updateById(IfsWdOpicsSecmap entity);
	void delete(String mapid);
	//发行人
	List<IfsWdOpicsSecmap> queryCustList();
	
	//类型
	List<IfsWdOpicsSecmap> queryTypeList();
	
	//付息周期
	IfsWdOpicsSecmap queryOpicsId(String wdid);
	

	//查询映射
	IfsWdOpicsSecmap queryKeyMapping(Map<String, String> map);
}
