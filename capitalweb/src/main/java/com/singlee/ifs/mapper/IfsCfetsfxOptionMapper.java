package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxOption;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxOptionMapper {

	public Page<IfsCfetsfxOption> searchOptionPage(Map<String, Object> map,RowBounds rowBounds);

	public IfsCfetsfxOption searchOption(@Param("ticketId")String ticketId);

	public void addOption(IfsCfetsfxOption map);

	public void editOption(IfsCfetsfxOption map);

	public void deleteOption(@Param("ticketId")String ticketId);

	public void updateOptionByID(String serial_no, String status,String date);
	
	void updateApproveStatusFxOptionByID(String ticketId);

	public Page<IfsCfetsfxOption> getOptionMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxOption> getOptionMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxOption> getOptionMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public IfsCfetsfxOption getOptionAndFlowIdById(Map<String, Object> map);
	
	public List<IfsCfetsfxOption> searchById(@Param("contractId")String contractId);

	public IfsCfetsfxOption selectFlowByPrimaryKey(Map<String, Object> key);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxOption> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
	
	//期权类mini弹框
	public Page<Map<String, Object>> getOptMini(Map<String, Object> params,RowBounds rowBounds);
    
	//总交易查询(人民币期权)
	public Page<IfsCfetsfxOption> searchTotalDealRMBQQ(Map<String, Object> params, RowBounds rowBounds);
	/**
	 * 状态回查使用,查询所有statcode为-1和-3的数据
	 * @return
	 */
	public List<IfsCfetsfxOption> getCfetsfxOptionList();
	/**
	 * 根据fedealno回填opics处理信息
	 * @param fedealno
	 */
	public void updateCfetsfxOptionByFedealno(Map<String, Object> map);
	/**
	 * 数据导入opics,将statcode设为-1
	 */
	public void updateStatcodeByTicketId(Map<String, Object> map);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsfxOption searchOptionDetailByTicketId(Map<String, Object> map);
	
	//增加未交割头寸损益
	void updateProfitLess(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsfxOption> searchSwiftNotExist();
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsfxOption> searchSwiftNotExistByDate(Map<String, Object> map);
}