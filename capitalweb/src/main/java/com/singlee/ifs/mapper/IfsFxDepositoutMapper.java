package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFxDepositout;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsFxDepositoutMapper {
    int deleteByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);

    int deleteByTicketid(@Param("ticketId") String ticketId);

    int insert(IfsFxDepositout record);

    int insertSelective(IfsFxDepositout record);

    IfsFxDepositout selectByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);

    Page<IfsFxDepositout> selectByTicketId(@Param("ticketId") String ticketId, RowBounds rowBounds);

    List<IfsFxDepositout> searchDepositByTicketId();

    IfsFxDepositout searchByTicketId(@Param("ticketId") String ticketId);

    IfsFxDepositout searchFlowByTicketId(Map<String, Object> params);

    List<IfsFxDepositout> searchById(@Param("contractId")String contractId);

    int updatefxDepositByID(String serial_no, String status,String date);

    void updateStatcodeByTicketId(Map<String, Object> map);

    void updateApproveStatusFxDepositOutByID(@Param("ticketId") String ticketId);

    void updateDepositByFedealno(Map<String, Object> map);

    int updateByPrimaryKeySelective(IfsFxDepositout record);

    int updateByPrimaryKey(IfsFxDepositout record);

    Page<IfsFxDepositout> getDepositoutFXMineList(Map<String, Object> params,
                                                RowBounds rowBounds);

    Page<IfsFxDepositout> getDepositoutFXMineListFinished(Map<String, Object> params,
                                                        RowBounds rowBounds);

    Page<IfsFxDepositout> getDepositoutFXMineListMine(Map<String, Object> params,
                                                    RowBounds rowBounds);


}