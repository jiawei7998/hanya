package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovemetalGold;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;

public interface IfsApprovemetalGoldMapper {
    int deleteByPrimaryKey(IfsApprovemetalGold key);

    int insert(IfsApprovemetalGold record);

    int insertSelective(IfsApprovemetalGold record);

    IfsApprovemetalGold selectByPrimaryKey(IfsApprovemetalGold key);
    
    IfsApprovemetalGold selectFlowByPrimaryKey(Map<String, Object> key);

    int updateByPrimaryKeySelective(IfsApprovemetalGold record);

    int updateByPrimaryKey(IfsApprovemetalGold record);
    
    Page<IfsApprovemetalGold> selectIfsCfetsmetalGoldList(IfsApprovemetalGold record);

	void updateMetalGoldByID(String serial_no, String status,String date);

	Page<IfsApprovemetalGold> getMetalGoldMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovemetalGold> getMetalGoldMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	Page<IfsApprovemetalGold> getMetalGoldMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	IfsApprovemetalGold searchGoldspt(@Param(value = "ticketId") String ticketId);
	
	//public List<IfsApprovemetalGold> searchById(@Param("contractId")String contractId);

	/*Page<IfsCfetsmetalGold> getMetalGoldMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsApprovemetalGold> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}