package com.singlee.ifs.mapper;


import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCreditRiskreview;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/***
 * 
 * opics 交易对手表 mapper
 * 
 */
public interface IfsOpicsRiskMapper {

	/**
	 * 分页查询
	 */
	Page<IfsCreditRiskreview> searchPageOpicsRisk(Map<String, Object> map, RowBounds rb);
}