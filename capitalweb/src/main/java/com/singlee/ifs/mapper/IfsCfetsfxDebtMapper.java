package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxDebt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsfxDebtMapper {

	public Page<IfsCfetsfxDebt> searchIfsDebtPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteIfsDebt(@Param("ticketId")String ticketid);

	public void addIfsDebt(IfsCfetsfxDebt map);

	public void editIfsDebt(IfsCfetsfxDebt map);

	public IfsCfetsfxDebt searchIfsDebt(@Param("ticketId")String ticketid);

	public void updateDebtByID(String serial_no, String status,String date);

	
	public Page<IfsCfetsfxDebt> getDebtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxDebt> getDebtMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxDebt> getDebtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	public List<IfsCfetsfxDebt> searchById(@Param("contractId")String contractId);

	public IfsCfetsfxDebt selectFlowByPrimaryKey(Map<String, Object> key);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxDebt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	
	/*public Page<IfsCfetsfxDebt> getDebtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxDebt> getDebtMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxDebt> getDebtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
		
}
