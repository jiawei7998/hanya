package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIotd;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;


public interface IfsRevIotdMapper {
	// 新增
	public void insert(IfsRevIotd entity);

	// 修改
	void updateById(IfsRevIotd entity);

	// 删除
	void deleteById(String id);

	Page<IfsRevIotd> searchPageOptParam(Map<String, Object> map,RowBounds rb);
	
	//获取id
	String getId();
	
	/***
	 * 根据id修改  审批状态 、审批发起时间 
	 */
	void updateIotdStatusByID(String serial_no, String status,String date);
	
	
	/***
	 * 我发起的
	 */
	Page<IfsRevIotd> getRevIotdMine(Map<String, Object> params,RowBounds rowBounds);
	/***
	 * 待审批
	 */
	Page<IfsRevIotd> getRevIotdUnfinished(Map<String, Object> params,RowBounds rowBounds);
	/***
	 * 已审批
	 */
	Page<IfsRevIotd> getRevIotdFinished(Map<String, Object> params,RowBounds rowBounds);
	
	/***
	 * 根据id查询  实体类
	 */
	IfsRevIotd searchById(Map<String, Object> map);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}