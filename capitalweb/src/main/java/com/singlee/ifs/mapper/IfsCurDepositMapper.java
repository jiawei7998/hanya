package com.singlee.ifs.mapper;
import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCurDeposit;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 *
 * 2021/11/29
 * @Auther:zk
 */    
    
public interface IfsCurDepositMapper {


    int insertSelective(IfsCurDeposit ifsCurDeposit);

    Page<IfsCurDeposit> selectallPage(Map<String,Object> map, RowBounds rb);

}