package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBlendSuccess;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsBlendSuccessMapper {

	public Page<IfsBlendSuccess> searchPageSuccess(Map<String, Object> map,RowBounds rb);


	public void insert(IfsBlendSuccess entity);


	public void deleteById(String id);
	
}
