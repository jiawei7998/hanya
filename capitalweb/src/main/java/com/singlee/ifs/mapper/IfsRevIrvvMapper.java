package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIrvv;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsRevIrvvMapper {
	
	Page<IfsRevIrvv> searchPageIrvv(Map<String, Object> map, RowBounds rb);

	void insert(IfsRevIrvv entity);

	void deleteById(String id);

	void updateById(IfsRevIrvv entity);
	
	String getId();
	
	Page<IfsRevIrvv> searchIrvvMine(Map<String,Object> map,RowBounds rb);
	
	Page<IfsRevIrvv> searchIrvvUnfinished(Map<String,Object> map,RowBounds rb);
	
	Page<IfsRevIrvv> searchIrvvFinished(Map<String,Object> map,RowBounds rb);
	
	int updateIrvvStatusByID(String serial_no, String status, String date);
	
	/***
	 * 根据id查询  实体类
	 */
	IfsRevIrvv searchById(Map<String, Object> map);
	
	
   /* int deleteByPrimaryKey(String ticketId);

    int insert(IfsRevIrvv record);

    IfsRevIrvv selectByPrimaryKey(String ticketId);

    int updateByPrimaryKey(IfsRevIrvv record);
*/
   IfsRevIrvv getOneById(String ticketId);
	
}