package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.capital.choistrd.model.Secl;
import com.singlee.capital.opics.model.SlInthBean;
import com.singlee.capital.opics.model.SlIselBean;
import com.singlee.ifs.model.IfsIntfcIselBean;

import tk.mybatis.mapper.common.Mapper;

public interface IfsIntfcIselMapper extends Mapper<IfsIntfcIselBean>{
	
	List<IfsIntfcIselBean> getIsel(Map<String, Object>  map);
	
	IfsIntfcIselBean selectIntfcIsel(IfsIntfcIselBean iselbean);
	
	int insertIntfcIsel(IfsIntfcIselBean iselbean);
	
	int deleteIntfcIsel(IfsIntfcIselBean iselbean);
	
	List<IfsIntfcIselBean> searchIselList(Map<String, Object> map);
	
	String selectIselMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
	
	int addIsel(SlIselBean iselBean);
	int addSlFundInth(SlInthBean inthBean);

	
}
