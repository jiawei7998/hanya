package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindYcBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindYcMapper extends Mapper<IfsWindYcBean>{
	
    // 分页查询
    Page<IfsWindYcBean> selectIfsWindYcList(Map<String, Object> map, RowBounds rb);

    
    
}