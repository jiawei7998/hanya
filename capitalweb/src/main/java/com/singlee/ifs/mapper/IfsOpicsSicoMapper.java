package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSico;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * sic码
 * @author lij
 *
 */
public interface IfsOpicsSicoMapper {
	
	//新增
	void insert(IfsOpicsSico entity);
	
	//修改
	void updateById(IfsOpicsSico entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsSico> searchPageOpicsSico(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsSico searchById(String id);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsSico entity);
	
	// 根据id 更新同步状态为0(未同步)
	void updateStatus0ById(String id);
	
	//查询所有的客户sic码
	List<IfsOpicsSico>  searchAllOpicsSico(Map<String,Object> map);

	//查询所有的客户sic码(下拉款)
	List<IfsOpicsSico>  searchAllOpicsSicoDict(Map<String,Object> map);
}