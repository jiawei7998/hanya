package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsKCoreKm;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsOpicsGlnoMapper {

	// 分页查询
	Page<IfsOpicsKCoreKm> searchPageOpicsGlno(Map<String, Object> map, RowBounds rb);

}