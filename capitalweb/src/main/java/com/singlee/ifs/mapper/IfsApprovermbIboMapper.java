package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbIbo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsApprovermbIboMapper {
    int deleteByPrimaryKey(IfsApprovermbIbo key);

    int insert(IfsApprovermbIbo record);

    int insertSelective(IfsApprovermbIbo record);

    IfsApprovermbIbo selectByPrimaryKey(IfsApprovermbIbo key);

    int updateByPrimaryKeySelective(IfsApprovermbIbo record);

    int updateByPrimaryKey(IfsApprovermbIbo record);
    
    Page<IfsApprovermbIbo> selectIfsCfetsrmbIboList(IfsApprovermbIbo record);

	void updateRmbIboByID(String serial_no, String status,String date);

	Page<IfsApprovermbIbo> getRmbIboMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbIbo> getRmbIboMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbIbo> getRmbIboMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbIbo searchCredLo(@Param(value = "ticketId") String ticketId);

	IfsApprovermbIbo selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	public List<IfsApprovermbIbo> searchById(@Param("contractId")String contractId);


	/*Page<IfsCfetsrmbIbo> getRmbIboMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIbo> getRmbIboMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIbo> getRmbIboMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsApprovermbIbo> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}