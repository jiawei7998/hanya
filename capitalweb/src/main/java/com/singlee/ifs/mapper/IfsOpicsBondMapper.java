package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsBond;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 交易要素mapper
 * @author singlee4
 *
 */
public interface IfsOpicsBondMapper {
	
	//新增
	void insert(IfsOpicsBond entity);
	
	//修改
	void updateById(IfsOpicsBond entity);
	
	//修改
	void updateByIdAndAcct(IfsOpicsBond entity);
	
	//删除
	void deleteById(String id);
	
	//分页债券信息查询
	Page<IfsOpicsBond> searchPageBondTrd(Map<String,Object> map,RowBounds rb);
	
	//分页债券复核查询
	Page<IfsOpicsBond> searchPageCheckTrd(Map<String,Object> map,RowBounds rb);
	
	//通过主键查询实体
	public IfsOpicsBond searchById(@Param("bndcd")String bndcd);
	
	//通过债券代码 和 发行人查询实体
	public IfsOpicsBond searchByIdAndAcct(@Param("bndcd")String bndcd,@Param("issuer")String issuer);
	
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeById(Map<String, Object> map);
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsOpicsBond> getOpicsBondList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateBondByFedealno(Map<String,Object> map);
	/**
	 * 插入opics处理成功后修改状态
	 * @param bndcd
	 */
	void  updateStatus(Map<String,Object> map);
	
	/**
	 * 获取导入opics流水号
	 * @return
	 */
	String getTradeId(HashMap<String,Object> map);
	
	/**
	 * 查询已同步的列表
	 * @return
	 */
	List<IfsOpicsBond> getSyncOpicsBondList();
	
	// 根据id 更新同步状态
	void updateStatusById(String id);
	
	// 查询所有的
	Page<IfsOpicsBond> searchAllOpicsBond();
	
	/**
	 * 查询到期的债券
	 * @return
	 */
	List<IfsOpicsBond> duedateBondListQry(Map<String, Object> map);
}
