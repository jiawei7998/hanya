package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsCorePositionBean;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface IfsCorePositionMapper extends Mapper<IfsCorePositionBean>{
	
    
    List<IfsCorePositionBean> getAllList(Map<String, Object> map);
    
    String getNm();
}