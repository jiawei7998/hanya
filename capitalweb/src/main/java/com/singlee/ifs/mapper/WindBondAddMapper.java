package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsWindBondAddBean;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface WindBondAddMapper extends Mapper<IfsWindBondAddBean> {

	// 分页查询
	Map<String, Object> getWindBondAdd(@Param("secid") String secid);

}