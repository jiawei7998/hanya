package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindRiskBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindRiskMapper extends Mapper<IfsWindRiskBean>{
	
 // 分页查询
    Page<IfsWindRiskBean> selectIfsWindRiskList(Map<String, Object> map, RowBounds rb);

    
}