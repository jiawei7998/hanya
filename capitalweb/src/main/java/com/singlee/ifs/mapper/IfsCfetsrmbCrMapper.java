package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbCr;
import com.singlee.ifs.model.IfsCfetsrmbCrKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbCrMapper {
    int deleteByPrimaryKey(IfsCfetsrmbCrKey key);

    int insert(IfsCfetsrmbCr record);

    IfsCfetsrmbCr selectByPrimaryKey(IfsCfetsrmbCrKey key);
    
    /**存储过程时 调用*/
    IfsCfetsrmbCr selectById(Map<String, String> map);

    int updateByPrimaryKey(IfsCfetsrmbCr record);
    
    Page<IfsCfetsrmbCr> selectIfsCfetsrmbCrList(IfsCfetsrmbCr record);

	void updateRmbCrByID(String serial_no, String status,String date);
	
	void updateApproveStatusRmbCrByID(String ticketId);

	Page<IfsCfetsrmbCr> getRmbCrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbCr searchPleDge( @Param(value = "ticketId") String ticketId);

	IfsCfetsrmbCr selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	public List<IfsCfetsrmbCr> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbCr> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbCr> getRmbCrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/

	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsrmbCr> getCfetsrmbCrList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsrmbCrByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	Page<Map<String, Object>> getBondMini(Map<String, Object> params,RowBounds rowBounds);
	
	/**
	 * 总交易查询（质押式回购）
	 * @param map
	 */
	Page<IfsCfetsrmbCr> searchTotalDealZYSHG(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbCr searchCfetsrmbCrByTicketId(@Param(value="ticketId") String ticketId);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsrmbCr searchCrDetailByTicketId(Map<String, Object> map);
}