package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsForeignccyDish;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 更新总行外币平盘数据表 IFS_FOREIGNCCY_DISH
 * @author singlee
 *
 */
public interface IfsForeignCcyMapper {

	// 新增
	public void insert(IfsForeignccyDish entity);
	
	// 根据id查询记录数
	public int search(IfsForeignccyDish entity);
	
	// 根据日期和币种查询日期
	public String searchDate(Map<String, Object> map);
	
	Page<IfsForeignccyDish> searchPageDish(Map<String, Object> map, RowBounds rowBounds);
	
	List<IfsForeignccyDish> searchDishList(Map<String, Object> map);
	
	/**
	 * 获取导入opics流水号
	 * @return
	 */
	String getFedealno(HashMap<String,Object> map);

	void updateSyncStatus(IfsForeignccyDish bean);

	public List<IfsForeignccyDish> searchSyncDishList();

	public void updateByFedealno(Map<String, Object> map);
	
}
