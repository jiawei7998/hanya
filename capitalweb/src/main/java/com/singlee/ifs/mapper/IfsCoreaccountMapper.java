package com.singlee.ifs.mapper;


import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCoreaccount;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsCoreaccountMapper.java
 * @Description 日终对账表
 * @createTime 2021年10月18日 10:59:00
 */
public interface IfsCoreaccountMapper extends Mapper<IfsCoreaccount> {
    Page<IfsCoreaccount> searchPage(Map<String, Object> map, RowBounds rb);

	void insertList(List<IfsCoreaccount> list);

	void delByPostDate(String postdate);
}
