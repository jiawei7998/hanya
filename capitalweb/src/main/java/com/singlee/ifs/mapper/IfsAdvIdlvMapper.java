package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsAdvIdlv;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsAdvIdlvMapper extends Mapper<IfsAdvIdlv>{

	/**
	 * 分页查询
	 */
	Page<IfsAdvIdlv> searchPageIdlv(Map<String, Object> map, RowBounds rb);

	/**
	 * 我发起的
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<IfsAdvIdlv> searchIdlvMine(Map<String, Object> map, RowBounds rb);

	/**
	 * 待审批
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<IfsAdvIdlv> searchIdlvUnfinished(Map<String, Object> map, RowBounds rb);

	/**
	 * 审批完成
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<IfsAdvIdlv> searchIdlvFinished(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 单个查询
	 * @param map
	 * @return
	 */
	IfsAdvIdlv searchIdlvById(Map<String, Object> map);
	
	/**
	 * 修改状态
	 * @param serial_no
	 * @param status
	 * @param date
	 */
	void updateIdlvStatusByID(String serial_no, String status, String date);

}