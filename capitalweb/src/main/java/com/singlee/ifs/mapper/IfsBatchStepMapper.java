package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBatchStep;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsBatchStepMapper {

	int deleteByPrimaryKey(String id);

	int insert(IfsBatchStep record);

	int insertSelective(IfsBatchStep record);

	IfsBatchStep selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(IfsBatchStep record);

	int updateByPrimaryKey(IfsBatchStep record);

	/**
	 * 根据条件查询数据
	 */
	Page<IfsBatchStep> getBatchStepPage(Map<String, String> params, RowBounds rb);

	/**
	 * 查询最大的id号
	 */
	String getMaxDbTime(Map<String, String> params);

}