package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsLimitColumns;
import com.singlee.ifs.model.IfsOpicsProd;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 字段限额
 * 
 * @author singlee4
 * 
 */
public interface IfsFieldLimitMapper {

	// 新增
	void insert(Map<String, Object> map);

	// 修改
	void updateById(Map<String, Object> map);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsLimitColumns> searchPageFieldLimit(Map<String, Object> map,
			RowBounds rb);

	// 查询表的结构
	List<Map<String, Object>> searchColumns(Map<String, Object> map);

	// 查询产品表
	List<Map<String, Object>> searchTables(Map<String, Object> map);

	// 查询字典表
	List<Map<String, Object>> searchDict(Map<String, Object> map);

	// 根据产品类型 查询限额字段以及字典值
	List<Map<String, Object>> searchLimitFieldAndDict(Map<String, Object> map);

	// 根据限额字段去查询数据
	List<Map<String, Object>> searchColum(Map<String, Object> map);

	List<Map<String, Object>> proColuDic(Map<String, Object> map);

	List<Map<String, Object>> selectColum(Map<String, String> map);
	
	//查询外汇限额产品信息
	public Page<IfsOpicsProd> searchPrdInfo(Map<String,Object> map,RowBounds rowBounds);
	
	//查询外汇限额产品类型信息
	public Page<Map<String, Object>> searchTypeInfo(Map<String,Object> map,RowBounds rowBounds);
	
	//查询外汇限额币种信息
	public Page<Map<String, Object>> searchCcyInfo(Map<String,Object> map,RowBounds rowBounds);
	
	//查询外汇限额债券类型
	public Page<Map<String, Object>> searchBondInfo(Map<String,Object> map,RowBounds rowBounds);

	//债券类型
	public Page<Map<String, Object>> searchAccTypeInfo(Map<String,Object> map,RowBounds rowBounds);
	
	//查询用户名信息
	public Page<Map<String, Object>> searchTradInfo(Map<String,Object> map,RowBounds rowBounds);
	
	//查询成本中心
	public Page<Map<String, Object>> searchCostInfo(Map<String,Object> map,RowBounds rowBounds);
}
