package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbSl;
import com.singlee.ifs.model.IfsCfetsrmbSl;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsApprovermbSlMapper {
    int deleteByPrimaryKey(IfsApprovermbSl key);

    int insert(IfsApprovermbSl record);

    int insertSelective(IfsApprovermbSl record);

    IfsApprovermbSl selectByPrimaryKey(IfsApprovermbSl key);
    
    IfsApprovermbSl selectById(Map<String, String> map);

    int updateByPrimaryKeySelective(IfsApprovermbSl record);

    int updateByPrimaryKey(IfsApprovermbSl record);
    
    Page<IfsApprovermbSl> selectIfsCfetsrmbSlList(IfsApprovermbSl record);

	void updateRmbSlByID(String serial_no, String status,String date);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	Page<IfsApprovermbSl> getRmbSlMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbSl> getRmbSlMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbSl> getRmbSlMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbSl searchBondLon(@Param(value = "ticketId") String ticketId);

	IfsApprovermbSl selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	public List<IfsApprovermbSl> searchById(@Param("contractId")String contractId);

	Page<IfsApprovermbSl> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbSl> getRmbSlMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbSl> getRmbSlMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbSl> getRmbSlMineListMine(Map<String, Object> params,
	
			RowBounds rowBounds);*/
	
	/**
	 * 总交易查询（债券借贷）
	 * @param map
	 */
	Page<IfsApprovermbSl> searchTotalDealZJJD(Map<String, Object> params,
			RowBounds rowBounds);

	Page<Map<String, Object>> getSlMini(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbSl searchCfetsrmbSlByTicketId(Map<String, Object> map);

	public String selectApproveStatuesByDealNo(@Param("selectSql")String selectSql);

	Page<Map<String, Object>> getDpMini(Map<String, Object> params,
			RowBounds rowBounds);
}

