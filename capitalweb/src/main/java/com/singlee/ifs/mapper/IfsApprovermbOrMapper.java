package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbOr;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsApprovermbOrMapper {
    int deleteByPrimaryKey(IfsApprovermbOr key);

    int insert(IfsApprovermbOr record);

    int insertSelective(IfsApprovermbOr record);

    IfsApprovermbOr selectByPrimaryKey(IfsApprovermbOr key);
    
    IfsApprovermbOr selectById(Map<String, String> map);
    
    IfsApprovermbOr selectFlowIdByPrimaryKey(Map<String,Object>  key);
    
    int updateByPrimaryKeySelective(IfsApprovermbOr record);

    int updateByPrimaryKey(IfsApprovermbOr record);
    public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
    Page<IfsApprovermbOr> selectIfsCfetsrmbOrList(IfsApprovermbOr record);

	void updateRmbOrByID(String serial_no, String status,String date);

	Page<IfsApprovermbOr> getRmbOrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbOr> getRmbOrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbOr> getRmbOrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbOr searchRepo(@Param(value = "ticketId") String ticketId);
	
	public List<IfsApprovermbOr> searchById(@Param("contractId")String contractId);

	Page<IfsApprovermbOr> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbOr> getRmbOrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/

}