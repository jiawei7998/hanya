package com.singlee.ifs.mapper;

import com.singlee.financial.pojo.TradeSettlsBean;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName TradeSettlsMapper.java
 * @Description 外币清算路径
 * @createTime 2021年11月09日 15:46:00
 */
public interface TradeSettlsMapper extends Mapper<TradeSettlsBean> {
    TradeSettlsBean getTradeSettls(Map<String, Object> map);
}
