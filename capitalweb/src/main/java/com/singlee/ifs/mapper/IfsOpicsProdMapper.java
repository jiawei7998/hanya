package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsProd;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 
 * opics 产品表 mapper
 * 
 */
public interface IfsOpicsProdMapper {
	
	// 新增
	void insert(IfsOpicsProd entity);

	// 修改
	void updateById(IfsOpicsProd entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsProd> searchPageOpicsProd(Map<String, Object> map, RowBounds rb);

	List<IfsOpicsProd> searchProd(HashMap<String, Object> map);

	IfsOpicsProd searchById(String id);

	void updateStatus(IfsOpicsProd entity);

	void updateStatus0ById(String id);

	List<IfsOpicsProd> searchAllOpicsProd();

	/**
	 * 查询已同步的产品代码
	 */
	List<IfsOpicsProd> searchProdcode();
}