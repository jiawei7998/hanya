package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.SlFiDefault;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SlFiDefaultMapper.java
 * @Description 债券违约处理
 * @createTime 2021年09月29日 16:37:00
 */
public interface SlFiDefaultMapper extends Mapper<SlFiDefault> {


    Page<SlFiDefault> searchPage(Map<String, Object> map, RowBounds rb);

    //修改
    void deleteById(SlFiDefault entity);

}
