package com.singlee.ifs.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.capital.choistrd.model.RevaluationTmaxBean;
import com.singlee.ifs.model.SlDerivativesStatus;

public interface SlDerivativesStatusMapper {

	Page<SlDerivativesStatus> querySlDerivativesStatus(Map<String, String> params, RowBounds rb);
	
	Page<RevaluationTmaxBean> queryAllRevaluationProc(Map<String, String> params, RowBounds rb);

}
