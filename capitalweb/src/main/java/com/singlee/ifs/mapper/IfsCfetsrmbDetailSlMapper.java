package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbDetailSl;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


public interface IfsCfetsrmbDetailSlMapper {
    int insert(IfsCfetsrmbDetailSl record);

    int insertSelective(IfsCfetsrmbDetailSl record);
    void deleteByTicketId(@Param("ticketId")String ticketId);

	Page<IfsCfetsrmbDetailSl> searchBondSlDetails(Map<String, Object> params,
			RowBounds rowBounds);
	List<IfsCfetsrmbDetailSl> searchBondByTicketId(@Param("ticketId")String ticketId);

	List<IfsCfetsrmbDetailSl> getCfetsrmbDetailSlList();

	void updateDetailSlByFedealno(Map<String, Object> map);

	void updateStatcodeByTicketId(Map<String, Object> map2);
}