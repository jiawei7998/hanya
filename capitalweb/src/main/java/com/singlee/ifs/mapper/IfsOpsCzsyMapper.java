package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsOpsCzsyBean;

import java.util.List;
import java.util.Map;

/***
 * 
 * 处置收益报表（买卖价差）,需要从"营业税"报表中取数。
 *
 * @author copysun
 */

public interface IfsOpsCzsyMapper {

	List<IfsOpsCzsyBean> getData(Map<String,Object> map);

	void generateData(Map<String,Object> map);
}