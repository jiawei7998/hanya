package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsParaGroup;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/***
 * 交易要素mapper
 * @author singlee4
 *
 */
public interface IfsTradeParamMapper {
	
	//新增
	void insert(IfsOpicsParaGroup entity);
	
	//修改
	void updateById(IfsOpicsParaGroup entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsParaGroup> searchPageOpicsTrdParam(Map<String,Object> map,RowBounds rb);
	

}
