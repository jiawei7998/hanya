package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsNost;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 外币清算路径
 * 
 *
 */
public interface IfsOpicsNostMapper {
	
	//分页查询
	Page<IfsOpicsNost> searchPageOpicsNost(Map<String,Object> map,RowBounds rb);
	
	//增加
	void insert(IfsOpicsNost entity);
	
	//修改
	void updateById(IfsOpicsNost entity);
	
	//删除
	void deleteById(String nos);
	
	//根据id查询实体类
	IfsOpicsNost searchNostById(String id);
	
	//根据实体修改同步状态
	void updateStatus(IfsOpicsNost entity);
	
	//根据id修改同步状态
	void updateStatus0ById(String id);
	
	//查询所有NOST
	List<IfsOpicsNost> searchAllOpicsNost();
}
