package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbOr;
import com.singlee.ifs.model.IfsCfetsrmbOrKey;
import com.singlee.ifs.model.IfsRepoReportBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbOrMapper {
    int deleteByPrimaryKey(IfsCfetsrmbOrKey key);

    int insert(IfsCfetsrmbOr record);

    IfsCfetsrmbOr selectByPrimaryKey(IfsCfetsrmbOrKey key);
    
    IfsCfetsrmbOr selectById(Map<String, String> map);
    
    IfsCfetsrmbOr selectFlowIdByPrimaryKey(Map<String,Object>  key);

    int updateByPrimaryKey(IfsCfetsrmbOr record);
    
    void updateApproveStatusRmbOrByID(String ticketId);
    
    Page<IfsCfetsrmbOr> selectIfsCfetsrmbOrList(IfsCfetsrmbOr record);

	void updateRmbOrByID(String serial_no, String status,String date);

	Page<IfsCfetsrmbOr> getRmbOrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbOr searchRepo(@Param(value = "ticketId") String ticketId);
	
	public List<IfsCfetsrmbOr> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbOr> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbOr> getRmbOrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbOr> getRmbOrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/

	/**
	 * 状态回查使用,回购本身
	 * @return
	 */
	List<IfsCfetsrmbOr> getCfetsrmbOrList();
	/**
	 * 根据fedealno更改opics,回购
	 * @param fedealno
	 */
	void  updateCfetsrmbOrByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode,回购本身
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	/**
	 * 状态回查使用,券
	 * @return
	 */
	List<IfsCfetsrmbOr> getCfetsrmbOrBondList();
	/**
	 * 根据fedealno更改opics,券
	 * @param fedealno
	 */
	void  updateOrBondByFedealno(Map<String,Object> map);
	
	
	/**
	 * 插入opics成功回填statcode,券
	 * @param map
	 */
	void updateBondStatcodeByTicketId(Map<String, Object> map);
	
	/**
	 * 总交易查询（买断式回购）
	 * @param map
	 */
	Page<IfsCfetsrmbOr> searchTotalDealMDSHG(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbOr searchCfetsrmbOrByTicketId(@Param(value="ticketId") String ticketId);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsrmbOr searchOrDetailByTicketId(Map<String, Object> map);

	Page<IfsCfetsrmbOr> searchCapitalCheck(Map<String, Object> map, RowBounds rb);

	List<IfsCfetsrmbOr> searchCapitalCheckList(Map<String, Object> map);
	
	List<IfsRepoReportBean> searchRepoReportList(Map<String, Object> map);
	
	IfsRepoReportBean searchRepoReportListTotal(Map<String, Object> map);
}