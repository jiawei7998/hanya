package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsNetting;
import org.apache.ibatis.session.RowBounds;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface IfsNettingMapper {
	
	Page<IfsNetting> queryNettingList(Map<String, Object> map, RowBounds rowBounds);
	public List<IfsNetting> queryNettingSum(@RequestBody Map<String, Object> map);
	
	/**
	 * 查询返回全部数据
	 */
	List<IfsNetting> queryNettingData(Map<String, Object> map);
	
	/**
	 * 付款金额统计
	 */
	public List<IfsNetting> queryNettingSumPay(@RequestBody Map<String, Object> map);
}