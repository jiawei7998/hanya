package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIsld;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsRevIsldMapper {

	String getId();

	void insert(IfsRevIsld entity);

	void deleteById(String ticketId);

	void updateById(IfsRevIsld entity);

	Page<IfsRevIsld> getRevIsldUnfinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsRevIsld> getRevIsldFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsRevIsld> getRevIsldMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsRevIsld searchById(Map<String, Object> map);

	void updateIsldStatusByID(String serial_no, String status, String date);

	IfsRevIsld getOneById(String ticketId);
}
