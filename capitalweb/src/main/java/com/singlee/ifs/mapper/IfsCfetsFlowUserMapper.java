package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsFlowUser;

import java.util.Map;

public interface IfsCfetsFlowUserMapper {

	Page<IfsCfetsFlowUser> searchCfetsFlowUserManage(IfsCfetsFlowUser record);
	IfsCfetsFlowUser queryCfetsFlowUserById(Map<String, Object> key);
	void insertFlowUser(Map<String, Object> key);
	void updateFlowUser(Map<String, Object> key);
	void deleteFlowUser(Map<String, Object> key);
}
