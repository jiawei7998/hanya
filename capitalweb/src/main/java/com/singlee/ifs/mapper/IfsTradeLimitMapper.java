package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTradeLimit;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 
 * 交易限额
 *
 */
public interface IfsTradeLimitMapper {
	
	 Page<IfsTradeLimit> getDataPage(Map<String, Object> map, RowBounds rb);

	 void addData(Map<String,Object> map);

	void update(Map<String,Object> map);

	void delData(List<Map<String,Object>> list);
}
