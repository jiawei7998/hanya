package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIswh;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsRevIswhMapper {
	
	//新增
	void insert(IfsRevIswh entity);
	
	//修改
	void updateById(IfsRevIswh entity);
	
	//删除
	void deleteById(String id);
	
	//获取主键id
	String getId();
	
	//根据id修改  审批状态 、审批发起时间 
	void updateIswhStatusByID(String serial_no,String status,String date);
	
	/***
	 * 我发起的
	 */
	Page<IfsRevIswh> getRevIswhMine(Map<String, Object> params,RowBounds rb);
	
	/***
	 * 待审批
	 */
	Page<IfsRevIswh> getRevIswhUnfinished(Map<String, Object> params,RowBounds rb);
	
	/***
	 * 已审批
	 */
	Page<IfsRevIswh> getRevIswhFinished(Map<String, Object> params,RowBounds rb);
	
	
	/***
	 * 根据id查询  实体类
	 */
	IfsRevIswh searchById(Map<String, Object> map);


	IfsRevIswh getOneById(String ticketId);
	
	
	
	
	
	
	
	
	
	
	
}