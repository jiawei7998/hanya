package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCustSettle;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsCustSettleMapper {
	Page<IfsCustSettle> getCSList(Map<String, Object> map, RowBounds rowBounds);

	void updateByCno(Map<String, Object> map);

	IfsCustSettle selectByCno(Map<String, Object> map);

	void addCustSettle(Map<String, Object> map);

	void deleteSettle(Map<String, Object> map);

	IfsCustSettle selectById(Map<String, Object> map);
	
	IfsCustSettle selectMaxSeqByCno(Map<String, Object> map);
	
	List<IfsCustSettle> getListByCno(Map<String, Object> map);
	//修改是否默认账户
	void updateIsdefaultById(Map<String, Object> map);
	//复核或退回经办修改处理状态
	void updateDealFlagById(Map<String, Object> map);
	//经办修改处理状态
	void updateManageDealFlagById(Map<String, Object> map);
}
