package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.KWabacusGuozhaimianshui;
import org.apache.ibatis.session.RowBounds;

import java.math.BigDecimal;
import java.util.Map;

public interface KWabacusGuozhaimianshuiMapper {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(KWabacusGuozhaimianshui record);

    int insertSelective(KWabacusGuozhaimianshui record);

    KWabacusGuozhaimianshui selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(KWabacusGuozhaimianshui record);

    int updateByPrimaryKey(KWabacusGuozhaimianshui record);

    Page<KWabacusGuozhaimianshui> getPageList(Map<String,Object> map, RowBounds rowBounds);
}