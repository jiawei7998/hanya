package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSacc;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * SACC
 * @author yanming	 
 */
public interface IfsOpicsSaccMapper {
	
	//新增
	void insert(IfsOpicsSacc entity);
	
	//修改
	void updateById(IfsOpicsSacc entity);
	
	//删除
	void deleteById(Map<String,String> map);
	
	//分页查询
	Page<IfsOpicsSacc> searchPageOpicsSacc(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsSacc searchById(Map<String,String> map);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsSacc entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(Map<String,String> map);
	
	
	//查询所有的SACC
	List<IfsOpicsSacc>  searchAllOpicsSacc();
}

