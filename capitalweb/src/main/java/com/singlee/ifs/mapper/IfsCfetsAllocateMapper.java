package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxAllot;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsAllocateMapper {

	public Page<IfsCfetsfxAllot> searchIfsAllocatePage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteIfsAllocate(@Param("ticketId")String ticketid);

	public void addIfsAllocate(IfsCfetsfxAllot map);

	public void editIfsAllocate(IfsCfetsfxAllot map);

	public IfsCfetsfxAllot searchIfsAllocate(@Param("ticketId")String ticketid);

	public void updateAllocateByID(String serial_no, String status,String date);

	public Page<IfsCfetsfxAllot> getAllocateMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxAllot> getAllocateMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxAllot> getAllocateMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public IfsCfetsfxAllot searchCfetsAllotAndFlowIdById(Map<String, Object> map);
	
	public List<IfsCfetsfxAllot> searchById(@Param("contractId")String contractId);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public IfsCfetsfxAllot selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsCfetsfxAllot> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*public Page<IfsCfetsfxAllot> getAllocateMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxAllot> getAllocateMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxAllot> getAllocateMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
*/
	//总交易查询（外币头寸调拨）
	public Page<IfsCfetsfxAllot> searchTotalDealWBTCTB(
			Map<String, Object> params, RowBounds rowBounds);
	
	
}
