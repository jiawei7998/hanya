package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTaxes;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 
 * 税费维护
 *
 */
public interface IfsTaxesMapper {
	
	 Page<IfsTaxes> getDataPage(Map<String, Object> map, RowBounds rb);

	 void addData(List<Map<String,Object>> list);

	void update(List<Map<String,Object>> list);

	void delData(List<Map<String,Object>> list);
}
