package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxOnspot;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsApprovefxOnspotMapper {

	public Page<IfsApprovefxOnspot> searchCcyOnSpotPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcyOnSpot(@Param("ticketId") String ticketid);

	public void addCcyOnSpot(IfsApprovefxOnspot map);

	public void editCcyOnSpot(IfsApprovefxOnspot map);

	public IfsApprovefxOnspot searchCcyOnSpot(@Param("ticketId")String ticketid);

	public void updateCcyOnSpotID(String serial_no, String status,String date);

	public Page<IfsApprovefxOnspot> getCcyOnspotMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOnspot> getCcyOnspotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOnspot> getCcyOnspotMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
/*
	public Page<IfsApprovefxOnspot> getCcyOnspotMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOnspot> getCcyOnspotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOnspot> getCcyOnspotMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/

	public IfsApprovefxOnspot searchCfetsOnspotAndFlowIdById(
			Map<String, Object> map);
	
	public List<IfsApprovefxOnspot> searchById(@Param("contractId")String contractId);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public IfsApprovefxOnspot selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsApprovefxOnspot> searchApprovePassed(
			Map<String, Object> params,RowBounds rowBounds);
}