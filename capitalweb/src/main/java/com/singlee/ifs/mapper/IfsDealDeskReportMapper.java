package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsDealDeskReport;

import java.util.Map;

public interface IfsDealDeskReportMapper {

	public Page<IfsDealDeskReport> searchPageDealDesk(Map<String, Object> params);
}
