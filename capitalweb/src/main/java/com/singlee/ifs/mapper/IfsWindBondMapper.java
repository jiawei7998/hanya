package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindBondBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindBondMapper extends Mapper<IfsWindBondBean>{
	
	// 分页查询
	Page<IfsWindBondBean> selectIfsWindBondList(Map<String, Object> map, RowBounds rb);


}