package com.singlee.ifs.mapper;

import java.util.Map;

/***
 * 
 * 增值税数据生成
 *
 * @author copysun
 */

public interface IfsOpsCreateFifoMapper {

	void generateData(Map<String,Object> map);
}