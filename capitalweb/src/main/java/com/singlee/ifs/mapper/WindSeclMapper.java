package com.singlee.ifs.mapper;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

public interface WindSeclMapper {
	/**
	 * 查询债券的最新中债估值
	 *
	 * @return
	 */
	BigDecimal getEcaluatePrice(@Param("secid") String secid);
	
	int querySlIsecBndcd(String bndcd);

	void execIsecProcedure(Map<String, Object> bondMap);
}
