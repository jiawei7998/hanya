package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbCbt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;

public interface IfsApprovermbCbtMapper {
    int deleteByPrimaryKey(IfsApprovermbCbt key);

    int insert(IfsApprovermbCbt record);

    int insertSelective(IfsApprovermbCbt record);

    IfsApprovermbCbt selectByPrimaryKey(IfsApprovermbCbt key);
    
    IfsApprovermbCbt selectById(Map<String, String> map);
    
    IfsApprovermbCbt selectFlowIdByPrimaryKey(Map<String,Object>  key);

    int updateByPrimaryKeySelective(IfsApprovermbCbt record);

    int updateByPrimaryKey(IfsApprovermbCbt record);
    
    Page<IfsApprovermbCbt> selectIfsCfetsrmbCbtList(IfsApprovermbCbt record);

	void updateRmbCbtByID(String serial_no, String status,String date);

	Page<IfsApprovermbCbt> getRmbCbtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbCbt> getRmbCbtMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbCbt> getRmbCbtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbCbt searchIfsBondTra(@Param(value = "ticketId") String ticketId);

	Page<IfsApprovermbCbt> selectIfsApprovermbCbtList(IfsApprovermbCbt record);

	/*Page<IfsCfetsrmbCbt> getRmbCbtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsApprovermbCbt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}