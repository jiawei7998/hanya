package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.ifs.model.IfsIntfcSeclStatus;
import com.singlee.ifs.model.IfsIntfcSecurityPrice;

public interface IfsIntfcSecurityPriceMapper {
	
	String getOpicsSysDate(Map<String,Object> remap);
	
	List<IfsIntfcSecurityPrice> queryAllOpicsSecurityPrice(Map<String,Object> map);
	
	IfsIntfcSeclStatus querySeclStatusByDealnoSeq(Map<String,Object> map);
	
	int updateSeclStatusByDealnoSeq(IfsIntfcSeclStatus seclStatus);
	
	int insertSeclStatusByNew(IfsIntfcSeclStatus seclStatus);

}
