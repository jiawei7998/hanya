package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbDeposit;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 同业存款
 */
public interface IfsApprovermbDepositMapper extends Mapper<IfsApprovermbDeposit> {
    /**
     * 待审批
     * @param params
     * @param rowBounds
     * @return
     */
    Page<IfsApprovermbDeposit> getRmbDepositMineList(Map<String, Object> params, RowBounds rowBounds);

    /**
     * 已审批
     * @param params
     * @param rowBounds
     * @return
     */
    Page<IfsApprovermbDeposit> getRmbDepositMineListFinished(Map<String, Object> params, RowBounds rowBounds);

    /**
     * 我发起
     * @param params
     * @param rowBounds
     * @return
     */
    Page<IfsApprovermbDeposit> getRmbDepositMineListMine(Map<String, Object> params, RowBounds rowBounds);
    
    
    Page<IfsApprovermbDeposit> searchTotalDealTYCK(Map<String, Object> params, RowBounds rowBounds);

    void updateAllocateByID(String serial_no, String status, String date);

    IfsApprovermbDeposit searchIfsApprovermbDepositById(@Param("ticketId") String serial_no);

    IfsApprovermbDeposit searchIfsApprovermbDeposit(Map<String, Object> params);
    LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
}