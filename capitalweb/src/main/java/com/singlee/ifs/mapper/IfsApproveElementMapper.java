package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApproveElement;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsApproveElementMapper {

	public Page<IfsApproveElement> searchPageLimit(Map<String, Object> map,RowBounds rb);

	public void update(IfsApproveElement entity);

	public void insert(IfsApproveElement entity);

	public void deleteById(String id);


	public IfsApproveElement searchIfsLimitCondition(String id);

	public List<Map<String, Object>> searchColumns(Map<String, Object> map);

	public List<Map<String, Object>> searchTables(Map<String, Object> map);
	
	public List<LinkedHashMap<String, String>> searchTradeElements(String tableName);
	
}
