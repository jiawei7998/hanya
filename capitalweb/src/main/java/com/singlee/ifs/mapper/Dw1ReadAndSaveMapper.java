package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

public interface Dw1ReadAndSaveMapper {

   List<Map<String, Object>> dldtList(Map<String, Object> remap);

  List<Map<String, Object>> rprhList(Map<String, Object> remap);

  List<Map<String, Object>> tposList(Map<String, Object> remap);

  List<Map<String, Object>> callList(Map<String, Object> remap);
  //通知存款余额
  String callOrigbalance(Map<String, Object> remap);

  //获取金额变动值，即OPICS到期日偿付金额
  String paymentAmt(Map<String, Object> remap);

  // 获取利息收入/支出值，即OPICS FIND金额
  String interestAmt(Map<String, Object> remap);

  // 获取利息收入/支出值，即OPICS FIND金额
  String costdesc(Map<String, Object> remap);

  String Balance(Map<String, Object> remap);

  List<Map<String, Object>> rev(Map<String, Object> remap);

  String acctdesc(Map<String, Object> remap);

  Map<String, Object> secmOne(Map<String, Object> remap);

  List<Map<String, Object>> secsOne(Map<String, Object> remap);

  List<Map<String, Object>> descr(Map<String, Object> remap);

  List<Map<String, Object>> nextRate(Map<String, Object> remap);

  Map<String, Object> nextPay(Map<String, Object> remap);

  List<Map<String, Object>> payAmt(Map<String, Object> remap);

  List<Map<String, Object>> LastRepairRate(Map<String, Object> remap);
  //获得债券的卖出日期
  List<Map<String, Object>> getSelldate14(Map<String, Object> remap);
  //公允价值变动损益核算科目
  String MktCoreNo43(Map<String, Object> remap);

  //本金支付科目号（债券）
  String PrincipalCoreNo6(Map<String, Object> remap);
  //本金支付科目号（拆借）
  String dldtCore(Map<String, Object> remap);
  //利息收支科目(拆借)
  String dldtInterest(Map<String, Object> remap);
  //本金支付科目号（回购）
  String rprhCore(Map<String, Object> remap);
  //利利息收支科目（回购）
  String rprhInterest(Map<String, Object> remap);

  String ccyCode(Map<String, Object> remap);
  //同业存放
  Map<String, Object> callCore(Map<String, Object> remap);

  //同业存放
  List<Map<String, Object>> TypeDesc(Map<String, Object> remap);

  //上次付款日
  List<Map<String, Object>> lastPay(Map<String, Object> remap);

}
