package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsSubjectdetail;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName IfsSubjectdetailMapper.java
 * @Description OPICS科目余额
 * @createTime 2021年10月18日 10:48:00
 */
public interface IfsSubjectdetailMapper extends Mapper<IfsSubjectdetail> {

    Page<IfsSubjectdetail> searchPage(Map<String, Object> map, RowBounds rb);

   void updateOpicsBalance(Map<String, Object> map);

   void addOpicsBalance(Map<String, Object> map);

	List<IfsSubjectdetail> getListNoPage(Map<String, Object> map);

	void updateList(List<IfsSubjectdetail> list);

	void insertList(List<IfsSubjectdetail> list);
}
