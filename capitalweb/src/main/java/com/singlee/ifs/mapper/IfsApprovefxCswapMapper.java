package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxCswap;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxCswapMapper {

	public Page<IfsApprovefxCswap> searchCcySwapPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcySwap(@Param("ticketId")String ticketid);

	public void addCcySwap(IfsApprovefxCswap map);

	public void editCcySwap(IfsApprovefxCswap map);

	public IfsApprovefxCswap searchCcySwap(@Param("ticketId")String ticketid);

	public void updateCcySwapByID(String serial_no, String status,String date);

	public Page<IfsApprovefxCswap> getCcySwapMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxCswap> getCcySwapMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxCswap> getCcySwapMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
	
	IfsApprovefxCswap selectFlowIdByPrimaryKey(Map<String,Object>  key);
	
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	/*public Page<IfsApprovefxCswap> getCcySwapMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxCswap> getCcySwapMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxCswap> getCcySwapMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/

	public Page<IfsApprovefxCswap> searchApprovePassed(
			Map<String, Object> params,RowBounds rowBounds);
    
}