package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecForeignFxmmModel;
import org.apache.ibatis.annotations.Param;

public interface IfsCpisfxMmRecMapper {
    int insert(RecForeignFxmmModel record);

    int insertSelective(RecForeignFxmmModel record);

	int selectFxmmModel(@Param(value = "execID")String execID);

	void updateFxmmModel(RecForeignFxmmModel foreignFxmmModel);
}