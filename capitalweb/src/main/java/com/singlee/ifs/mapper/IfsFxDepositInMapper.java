package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFxDepositIn;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsFxDepositInMapper {
    int deleteByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);

    int deleteByTicketid(@Param("ticketId") String ticketId);

    int insert(IfsFxDepositIn record);

    int insertSelective(IfsFxDepositIn record);

    IfsFxDepositIn selectByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);

    Page<IfsFxDepositIn> selectByTicketId(@Param("ticketId") String ticketId,RowBounds rowBounds);

    IfsFxDepositIn searchByTicketId(@Param("ticketId") String ticketId);

    IfsFxDepositIn selectFlowByTicketId(Map<String, Object> params);

    List<IfsFxDepositIn> searchById(@Param("contractId")String contractId);

    int updatefxDepositByID(String serial_no, String status,String date);

    int updateByPrimaryKeySelective(IfsFxDepositIn record);

    int updateByPrimaryKey(IfsFxDepositIn record);

    Page<IfsFxDepositIn> getDepositInFXMineList(Map<String, Object> params,
                                         RowBounds rowBounds);

    Page<IfsFxDepositIn> getDepositInFXMineListFinished(Map<String, Object> params,
                                                 RowBounds rowBounds);

    Page<IfsFxDepositIn> getDepositInFXMineListMine(Map<String, Object> params,
                                             RowBounds rowBounds);

}