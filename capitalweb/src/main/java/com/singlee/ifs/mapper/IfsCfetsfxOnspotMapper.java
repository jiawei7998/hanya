package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxOnspot;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxOnspotMapper {

	public Page<IfsCfetsfxOnspot> searchCcyOnSpotPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcyOnSpot(@Param("ticketId") String ticketid);

	public void addCcyOnSpot(IfsCfetsfxOnspot map);

	public void editCcyOnSpot(IfsCfetsfxOnspot map);

	public IfsCfetsfxOnspot searchCcyOnSpot(@Param("ticketId")String ticketid);

	public void updateCcyOnSpotID(String serial_no, String status,String date);

	public Page<IfsCfetsfxOnspot> getCcyOnspotMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxOnspot> getCcyOnspotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxOnspot> getCcyOnspotMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
/*
	public Page<IfsCfetsfxOnspot> getCcyOnspotMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxOnspot> getCcyOnspotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxOnspot> getCcyOnspotMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/

	public IfsCfetsfxOnspot searchCfetsOnspotAndFlowIdById(
			Map<String, Object> map);
	
	public List<IfsCfetsfxOnspot> searchById(@Param("contractId")String contractId);

	public IfsCfetsfxOnspot selectFlowByPrimaryKey(Map<String, Object> key);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxOnspot> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	//总交易查询（外汇对即期）
	public Page<IfsCfetsfxOnspot> searchTotalDealWBDJQ(
			Map<String, Object> params, RowBounds rowBounds);
}
