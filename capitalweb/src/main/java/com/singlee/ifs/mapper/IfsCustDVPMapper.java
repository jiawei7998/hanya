package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCustDVP;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsCustDVPMapper {
    //新增交易
    void addCustDVP(List<IfsCustDVP> list);

    List<IfsCustDVP> selectCustDVPByDealno(Map<String,Object> map);

    Page<IfsCustDVP> selectCustDVP(Map<String,Object> map, RowBounds rowBounds);

    IfsCustDVP selectCustDVPByFedealno(@Param(value = "fedealno") String fedealno);
}
