package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindSecLevelBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindSecLevelMapper extends Mapper<IfsWindSecLevelBean>{
	
 // 分页查询
    Page<IfsWindSecLevelBean> selectIfsWindSecLevelList(Map<String, Object> map, RowBounds rb);

    
    
}