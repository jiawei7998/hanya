package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsCoun;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/***
 * 国家代码1
 * @author lij
 *
 */
public interface IfsOpicsCounMapper {
	
	
	//新增
	void insert(IfsOpicsCoun entity);
	
	//修改
	void updateById(IfsOpicsCoun entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsCoun> searchPageOpicsCoun(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsCoun searchById(String id);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsCoun entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(String id);
	
	
	//查询所有的国家代码
	Page<IfsOpicsCoun> searchAllOpicsCoun();
}
