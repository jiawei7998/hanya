package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsSecAcct;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsSecAcctMapper {
	Page<IfsSecAcct> selectPageSecAcct(Map<String, Object> map, RowBounds rowBounds);

}
