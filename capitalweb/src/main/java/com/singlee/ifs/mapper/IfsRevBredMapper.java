package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevBred;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsRevBredMapper {
	// 删除
    void deleteByPrimaryKey(String tacketId);

    // 增加
    void insert(IfsRevBred record);

    // 查找
    IfsRevBred selectByPrimaryKey(Map<String, Object> map);

    // 修改
    void updateByPrimaryKey(IfsRevBred record);
    
    // 分页查询
  	Page<IfsRevBred> searchPageRevBred(Map<String,Object> map,RowBounds rb);
  	
  	//主键生成
  	String getId();

	Page<IfsRevBred> getRevBredUnfinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsRevBred> getRevBredFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsRevBred> getRevBredMine(Map<String, Object> params,
			RowBounds rowBounds);

	void updateIfxdStatusByID(String serial_no, String status, String date);
	
	/***
	 * 根据id查询  实体类
	 */
	IfsRevBred searchById(Map<String, Object> map);


	IfsRevBred getOneByPrimaryKey(String ticketId);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}