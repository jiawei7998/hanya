package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbIbo;
import com.singlee.ifs.model.IfsCfetsrmbIboKey;
import com.singlee.ifs.model.IfsIboReportBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbIboMapper {
    int deleteByPrimaryKey(IfsCfetsrmbIboKey key);

    int insert(IfsCfetsrmbIbo record);

    IfsCfetsrmbIbo selectByPrimaryKey(IfsCfetsrmbIboKey key);

    int updateByPrimaryKey(IfsCfetsrmbIbo record);
    
    Page<IfsCfetsrmbIbo> selectIfsCfetsrmbIboList(IfsCfetsrmbIbo record);

	void updateRmbIboByID(String serial_no, String status,String date);
	
	void updateApproveStatusRmbIboByID(String ticketId);

	Page<IfsCfetsrmbIbo> getRmbIboMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIbo> getRmbIboMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIbo> getRmbIboMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbIbo searchCredLo(@Param(value = "ticketId") String ticketId);

	IfsCfetsrmbIbo selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	public List<IfsCfetsrmbIbo> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbIbo> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsrmbIbo> getCfetsIboList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsIboByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	Page<Map<String, Object>> getLendMini(Map<String, Object> params,RowBounds rowBounds);
	
	Page<Map<String, Object>> getDELendMini(Map<String, Object> params,RowBounds rowBounds);

	/**
	 * 总交易查询（信用拆借）
	 * @param map
	 */
	Page<IfsCfetsrmbIbo> searchTotalDealXYCJ(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbIbo searchCfetsRmbIboById(@Param(value="ticketId") String ticketId);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsrmbIbo searchIboDetailByTicketId(Map<String, Object> map);
	
	//查询报表每个月总计
	List<IfsIboReportBean> searchIboReportList(Map<String, Object> map);
	
	//查询所有总计
	IfsIboReportBean searchIboReportListTotal(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsrmbIbo> searchSwiftNotExist();
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsrmbIbo> searchSwiftNotExistByDate(Map<String, Object> map);
}