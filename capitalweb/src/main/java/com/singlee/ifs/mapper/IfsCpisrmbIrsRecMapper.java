package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecInterestRateSwapModel;
import org.apache.ibatis.annotations.Param;

public interface IfsCpisrmbIrsRecMapper {

    int insert(RecInterestRateSwapModel record);

    int insertSelective(RecInterestRateSwapModel record);

	int selectSwapModel(@Param(value = "execID")String execID);

	void updateSwapModel(RecInterestRateSwapModel model);

}