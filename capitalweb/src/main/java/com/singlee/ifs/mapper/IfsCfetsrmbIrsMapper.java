package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import com.singlee.ifs.model.IfsCfetsrmbIrsKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbIrsMapper {
    int deleteByPrimaryKey(IfsCfetsrmbIrsKey key);

    int insert(IfsCfetsrmbIrs record);

    IfsCfetsrmbIrs selectByPrimaryKey(IfsCfetsrmbIrsKey key);
    
    IfsCfetsrmbIrs selectFlowIdByPrimaryKey(Map<String,Object>  key);
    
    IfsCfetsrmbIrs selectById(Map<String, String> map);

	IfsCfetsrmbIrs OneById(String ticketId);

    int updateByPrimaryKey(IfsCfetsrmbIrs record);
    
    void updateApproveStatusRmbIrsByID(String ticketId);
    
    Page<IfsCfetsrmbIrs> selectIfsCfetsrmbIrsList(IfsCfetsrmbIrs record);

	void updateRmbIrsByID(String serial_no, String status,String date);

	Page<IfsCfetsrmbIrs> getRmbIrsMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbIrs searchIfsCfetsrmbIrs(@Param(value = "ticketId") String ticketId);
	
	public List<IfsCfetsrmbIrs> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbIrs> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbIrs> getRmbIrsMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsrmbIrs> getCfetsrmbIrsList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsrmbIrsByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	//存续管理台 互换类mini弹框
	Page<Map<String, Object>> getIswhMini(Map<String, Object> params,RowBounds rowBounds);
	
	//总交易查询（利率互换）
	Page<IfsCfetsrmbIrs> searchTotalDealLLHH(Map<String, Object> params,
			RowBounds rowBounds);
	
	//根据id查询利率互换元素
	IfsCfetsrmbIrs searchIrsByTicketId(@Param(value = "ticketId")String ticketId);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsrmbIrs searchIrsDetailByTicketId(Map<String, Object> map);
	
	//增加未交割头寸损益
	void updateProfitLess(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsrmbIrs> searchSwiftNotExist();
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsrmbIrs> searchSwiftNotExistByDate(Map<String, Object> map);
}