package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxSwap;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxSwapMapper {

	public Page<IfsApprovefxSwap> getswapPage(Map<String, Object> map,RowBounds rowBounds);

	public IfsApprovefxSwap getrmswap(@Param("ticketId")String ticketId);

	public void addrmswap(IfsApprovefxSwap map);

	public void editrmswap(IfsApprovefxSwap map);

	public void deletermswap(@Param("ticketId")String ticketId);
	
	public void updateRmsSwapByID(String serial_no,String status,String date);
	
	/**
	 * 根据请求参数查询分页列表    我发起的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxSwap> getFxSwapMineListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表    已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxSwap> getFxSwapMineListFinished(Map<String, Object> params, RowBounds rb);

	/**
	 * 根据请求参数查询分页列表    待审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxSwap> getFxSwapMineList(Map<String, Object> params, RowBounds rb);
	
	IfsApprovefxSwap selectFlowIdByPrimaryKey(Map<String,Object>  key);

	public LinkedHashMap<String, Object> searchrmswap(@Param("sqll")String sqll);

	public Page<IfsApprovefxSwap> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
	
	
}