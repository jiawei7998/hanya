package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsWdSectype;

import java.util.List;

public interface IfsWdSectypeMapper {
	// 新增
	void insert(IfsWdSectype entity);
	//查询
	List<IfsWdSectype> queryByVDate(String vDate);
	// 删除
	void delete();
}
