package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.BussVrtyInfoArray;
import com.singlee.financial.bean.PdAttrInfoArray;
import com.singlee.financial.bean.SlIfsBean;
import com.singlee.ifs.model.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface IfsLimitTemplateMapper extends Mapper<ifsLimitTemplate>{
	
	public Page<ifsLimitTemplate> searchTemplateLimit(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 根据条件查询外汇限额信息
	 * @param map
	 * @return
	 */
	public List<ifsLimitTemplate> queryOccupyLimit(Map<String, Object> map);
	
	/**
	 * 将查询出的数据插入表中
	 */
	public void insertCredit(Map<String,Object> map);
	
	/**
	 * 查询所有
	 */
	Page<PdAttrInfoArray> searchAllProd(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询是否有当前审批件编号的数据
	 */
	List<PdAttrInfoArray> searchProd(String approveFlNo);
	
	/**
	 * 将查询出的数据插入ifs_trade_access表中
	 */
	public void insertBussType(Map<String,Object> map);
	
	/**
	 * 将查询出的数据插入ifs_trade_access_bk表中
	 */
	public void insertBussTypeBk(Map<String,Object> map);
	
	/**
	 * 根据核心客户号和业务类型名称查询唯一一条数据
	 */
	List<SlIfsBean> searchBuss(Map<String,Object> map);
	
	/**
	 * 查询所有业务种类
	 */
	Page<BussVrtyInfoArray> searchAllBuss(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询所有业务种类(黑名单)
	 */
	Page<BussVrtyInfoArray> searchAllBussBk(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 根据交易流水号查询该笔交易单笔风险是否通过
	 * @return
	 */
	public IfsCreditRiskreview querySingleDealLimit(@Param("DealNo")String DealNo);
	
	/**
	 * 根据客户号校验同业准入是否通过
	 * @return
	 */
	public IfsTradeAccess querySingleCustNoLimit(@Param("CustNo")String CustNo,@Param("prdNo")String prdNo);
	
	/**
	 * 根据客户号校验同业准入黑名单是否通过
	 * @return
	 */
	public IfsTradeAccess querySingleCustNoLimitBK(@Param("CustNo")String CustNo,@Param("prdNo")String prdNo);
	
	/**
	 * 根据ECIF客户号更新某一条数据
	 */
	void updateByEcifNo(Map<String, Object> map);
	
	/**
	 * 根据ECIF客户号更新某一条数据
	 */
	void updateByEcifNoBk(Map<String, Object> map);
	
	/**
	 * 根据ECIF客户号及业务类型名称删除某一条数据
	 */
	void deleteByEcifNo(Map<String, Object> map);
	
	/**
	 * 根据ECIF客户号及业务类型名称删除某一条数据(黑名单)
	 */
	void deleteByEcifNoBk(Map<String, Object> map);
	
	/**
	 * 插入同业准入数据
	 */
	void insertIntoAccess(IfsTradeAccess slIfsBean);
	
	/**
	 * 根据审批件编号更新某一条数据
	 */
	void updateByAppNo(Map<String, Object> map);
	
	/**
	 * 根据审批件编号删除某一条数据
	 */
	void deleteByAppNo(Map<String, Object> map);
	
	/**
	 * 插入风险审查数据
	 */
	void insertIntoProd(PdAttrInfoArray pdAttrInfo);
		
	/**
	 * 已经使用的额度，累计
	 */
	void updateOccupyLimit(Map<String, Object> map);
	
	/**
	 * 跟新当日限额时间
	 */
	void updateOccupyDayTime(Map<String, Object> map);
	
	/**
	 * 查询所有客户名称
	 */
	List<IfsOpicsCust> queryCustNm();
	
	/**
	 * 同业准入  查询所有数据
	 */
	List<SlIfsBean> searchAllData();
	
	/**
	 * 同业准入  查询本地数据
	 */
	Page<SlIfsBean> searchLocalInfo(Map<String, String> map, RowBounds rb);
	
	/**
	 * 同业准入  查询本地数据(黑名单)
	 */
	List<SlIfsBean> searchLocalInfoBk(Map<String, String> map);
	
	/**
	 * 风险审查  查询所有数据
	 */
	List<PdAttrInfoArray> searchAllDataRisk();
	
	/**
	 * 风险审查  查询本地数据
	 */
	List<PdAttrInfoArray> searchLocalData(Map<String, String> map);
	
	/**
	 * 外汇限额昨日敞口查询
	 * @param map
	 * @return
	 */
	IfsLimitTemplateDetail searchYesterdayTotal(Map<String, Object> map);
	
	/**
	 * 外汇限额明细增加
	 * @param map
	 * @return
	 */
	void insertIntoTemplateDetail(Map<String, Object> map);
	
	/**
	 * 外汇限额明细查询
	 * @param map
	 * @return
	 */
	Page<IfsLimitTemplateDetail> searchAllTemplateDetail(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 删除指定deal_no的数据
	 * 
	 * @param sceneId
	 */
	public void deleteTemplateByDealNo(String ticket_id);
	
	/**
	 * 同业准入 查询是否有此条记录
	 */
	List<SlIfsBean> queryBussInfo(Map<String, Object> map);
	
	/**
	 * 同业准入(黑名单) 查询是否有此条记录
	 */
	List<SlIfsBean> queryBussInfoBk(Map<String, Object> map);
	
	
	/**
	 *准入名单信息有修改时进行增加数据
	 */
	public void insertOperateDetail(Map<String,Object> map);
	
	/**
	 * 查询准入历史明细
	 */
	public Page<IfsTradeAccess> searchLocalDetail(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询外币明细翻页
	 * @param map
	 * @return
	 */
	public Page<ForeignLimitLog> searchForeignLimitForPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询外币明细
	 * @param map
	 * @return
	 */
	ForeignLimitLog searchSingleForeignLimit(Map<String, Object> map);
	
	/**
	 * 外币明细增加
	 */
	public void insertForeignLimit(Map<String,Object> map);
	
	/**
	 * 外币明细删除
	 */
	void deleteForeignLimitByDate(String ticketId);
	
	/**
	 * 删除当日已存在的外汇报表信息
	 */
	void deleteExicstForeignLimit(String oprTime);
	
	/**
	 * 单笔明细增加
	 */
	void insertForeignLimitSingle(Map<String,Object> map);
	
	/**
	 * 日间单笔最大值查询
	 * @param map
	 * @return
	 */
	List<ForeignLimitLog> selectForeignLimitSingle(Map<String, Object> map);
	
	/**
	 * 日间敞口增加
	 */
	void insertForeignLimitDaySum(Map<String,Object> map);
	
	/**
	 * 隔夜敞口增加
	 */
	void insertForeignLimitLastdaySum(Map<String,Object> map);
	
	/**
	 * 年度止损增加
	 */
	void insertForeignLimitYearSum(Map<String,Object> map);
	
	/**
	 * 月度止损增加
	 */
	void insertForeignLimitMonthSum(Map<String,Object> map);
	
	/**
	 * 查询截止上个月止损
	 */
	public String getLastMonthSum(Map<String,Object> map);
	
	/**
	 * 根据业务类型名称查询编码
	 */
	String queryPrdNo(String bussTpNm);
}