package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsType;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 
 * opics 产品类型表 mapper
 *
 */
public interface IfsOpicsTypeMapper {
	
	//新增
	void insert(IfsOpicsType entity);
	
	//修改
	void updateById(IfsOpicsType entity);
	
	//删除
	void deleteById(Map<String,Object> map);
	
	//分页查询
	Page<IfsOpicsType> searchPageOpicsType(Map<String,Object> map,RowBounds rb);
	
	IfsOpicsType searchById(Map<String,String> map);
	
	void updateStatus(IfsOpicsType entity);

	void updateStatus0ById(Map<String,String> map);

	List<IfsOpicsType> searchAllOpicsType();
	
	String getAlByType(String type,String prodcode);
	
	/**
	 * 查询已同步的产品类型
	 */
	List<IfsOpicsType> searchProdtype();
}