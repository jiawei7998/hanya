package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.*;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主要用于CSPI CSRI SDVP增删改查
 * 
 * @author shenzl
 * 
 */
public interface IfsOpicsSettleManageMapper {

	public void insertCspiCsri(IfsCspiCsriHead cspiCsriHead);

	public void insertCspiCsriDetail(IfsCspiCsriDetail cspiCsriDetail);

	public Page<IfsCspiCsriHead> searchPageCspiAndCsri(Map<String, Object> map, RowBounds rb);

	public void updateCspiCsri(IfsCspiCsriHead record);

	public void deleteCspiCsriDetailById(IfsCspiCsriHead record);

	public List<IfsCspiCsriDetail> searchCspiCsriDetails(Map<String, Object> map);

	public void updateVerSlflag(String fedealno, String userid, Date date, String slflag);

	public void updateSlflag(String fedealno, String userid, Date date, String slflag);

	/**
	 * 根据不同的产品不同的操作类型获取审批单编号
	 * 
	 * @return
	 */
	public String getTradeId(HashMap<String, Object> hashMap);

	public void deleteHeadByFedealno(Map<String, Object> map);

	public Page<IfsSdvpHead> searchPageSdvp(Map<String, Object> map, RowBounds rb);

	public List<IfsSdvpDetail> searchSdvpDetails(Map<String, Object> map);

	public void insertSdvp(IfsSdvpHead record);

	public void insertSdvpDetail(IfsSdvpDetail ifsSdvpDetail);

	public void updateSdvp(IfsSdvpHead record);

	public void deleteSdvpDetailById(IfsSdvpHead record);

	public void updateSdvpVerSlflag(String fedealno, String userid, Date date, String slflag);

	public void updateSdvpSlflag(String fedealno, String userid, Date date, String slflag);

	public void deleteSdvpByFedealno(Map<String, Object> map);

	public Page<IfsSdvpDetail> searchPageSdvpDetails(Map<String, Object> map, RowBounds rb);

	public Page<IfsCspiCsriDetail> searchPageCspiAndCsriDetails(Map<String, Object> map, RowBounds rb);

	public IfsCspiCsriHead searchCspiCsriHeadById(String fedealno);

	public void updateSyncStatus(IfsCspiCsriHead cspiCsriHead);

	public void updateSdvpSyncStatus(IfsSdvpHead record);

	public IfsSdvpHead searchSdvpHeadById(String fedealno);

	public List<IfsCspiCsriHead> getSyncCspiCsriList();

	public void updateCspiCsriByFedealno(Map<String, Object> map);

	public List<IfsCspiCsriHead> getSyncSCspiCsriList();

	public List<IfsSdvpHead> getSyncSdvp();

	public void updateSdvpByFedealno(Map<String, Object> map);

	public List<IfsSdvpHead> getSyncSSdvpList();
	
	//获取系统当前时间
	public String getCurDate();

	/**
	 * 根据成交单编号查询产品等信息
	 */
	public IfsSearchSettleBean searchProdLimit(String ticketId);

	/**
	 * 查询清算路径
	 */
	public IfsCspiCsriHead searchcspicsri(Map<String, Object> map);
	
	/**
	 * 查询收款清算路径
	 */
	public IfsCspiCsriHead recsearchcspicsri(Map<String, Object> map);
	
	/**
	 * 查询列表ifs_cspicsri_detail内容
	 */
	public List<IfsCspiCsriDetail> searchcspicsridetail(Map<String, Object> upmap);
	
	/**
	 * 托管账户  查询清算路径
	 */
	public IfsSdvpHead searchSettInfo(Map<String, Object> map);
	
	/**
	 * 托管账户  查询清算路径
	 */
	public List<IfsSdvpHead> searchSettInfoList(Map<String, Object> map);
	
	/**
	 * 托管账户  查询收款清算路径
	 */
	public IfsSdvpHead recsearchSettInfo(Map<String, Object> map);
	
	/**
	 * 托管账户  查询列表ifs_sdvp_detail内容
	 */
	public List<IfsSdvpDetail> paysearchsdvpdetail(Map<String, Object> upmap);
	
	/**
	 * 返回LIST
	 */
	public List<IfsSdvpDetail> searchsdvpdetail(Map<String, Object> upmap);
	
	/**
	 * 托管账户  查询收款列表ifs_sdvp_detail内容
	 */
	public List<IfsSdvpDetail> recsearchsdvpdetail(Map<String, Object> upmap);
	
	/**
	 * 根据成交单编号查询产品等信息(存单)
	 */
	public List<IfsSearchSettleBean> searchProdLimitList(String ticketId);
	
	public IfsSearchSettleBean searchSettInfoListDetail(Map<String, String> map);

	public void deleteDetailByFedealno(Map<String, Object> map);

	public void deleteSdvpDetailByFedealno(Map<String, Object> map);

	public int getCspiCsriById(IfsCspiCsriHead record);

	public int getSdvpById(IfsSdvpHead record);
}