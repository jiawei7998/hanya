package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.ifs.model.IfsSwift;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IfsSwiftMapper {

	Page<IfsSwift> getHandling(Map<String, Object> map, RowBounds rowBounds);
	
	/**
	 * 获取删除复核状态报文信息
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<IfsSwift> getVerifyHandling(Map<String, Object> map, RowBounds rowBounds);

	// void updateSwiftByID(String serial_no, String status, String date);
	void updateSwiftStatus(Date tm, String batchNo, String userid, String verind);

	void updateSwiftApproveStatus(Date tm, String batchNo, String userid, String verind);

	/**
	 * 将查询出的SWIFT数据插入到本地库的IFS_OPICS_SWIFT表中
	 */
	void insertSwiftData(SlSwiftBean bean);

	/**
	 * 查询对比数据
	 */
	List<SlSwiftBean> searchKey();

	/**
	 * 根据流水号查报文内容
	 */
	List<SlSwiftBean> getMsg(Map<String, Object> map);

	/**
	 * 发送成功后将清算状态改为已发送
	 */
	void updateSettFlag(Map<String, Object> map);

	/**
	 * 根据流水号查询清算状态
	 */
	String searchSettflag(Map<String, Object> map);

	/**
	 * 退回经办,改变流程状态
	 */
	void backHandle(Map<String, Object> map);

	void updateSwiftData(SlSwiftBean bean);

	/**
	 * 查报文内容
	 */
	List<SlSwiftBean> getMessage(Map<String, Object> map);

	void deleteSwift(Map<String, Object> map);
	
	/**
	 * 删除
	 * @param bean
	 */
	void updateSwiftDelete(Map<String, Object> map);
	
	/**
	 * 删除复合
	 * @param bean
	 */
	void updateSwiftComfirm(Map<String, Object> map);
	/**
	 * swift报文发送经办
	 * @param map
	 */
	void submitSwift(Map<String, Object> map);
	/**
	 * 根据流水查询swift信息
	 * @param map
	 * @return
	 */
	IfsSwift querySwiftByBatchNo(Map<String, Object> map);
	/**
     * 更新报文状态（ack）
	 * @param map
	 * @return
     */
	void updateStatu(Map<String, Object> map);
	/**
	 * 新增299报文 
	 * @param map
	 */
	void insertSwift(Map<String, Object> map) ;
	void submitSwiftBond(Map<String, Object> map);
	String getSessionNum();
	String getSequenceNum();
}