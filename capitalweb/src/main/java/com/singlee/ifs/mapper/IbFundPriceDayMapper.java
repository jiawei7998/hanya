package com.singlee.ifs.mapper;


import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.model.IbFundPriceDay;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


public interface IbFundPriceDayMapper extends MyMapper<IbFundPriceDay>{


    List<IbFundPriceDay> getFundPriceInfo(Map<String, Object> map);

    Page<IbFundPriceDay> getFundPricePage(Map<String, Object> map, RowBounds rowBounds);

    int getFundPriceDayVersion(@Param("fundCode") String fundCode, @Param("postdate") String postdate);

    void addFundPriceDayHis(Map<String, Object> map);

    void delFundPriceDay(Map<String, Object> map);

    List<String> getNeedAssmtDate(Map<String, Object> map);

    String getMaxDateHis(Map<String, Object> map);

    IbFundPriceDay getCurFtPrice(Map<String, Object> map);

    void insertPriceHis(IbFundPriceDay fundPriceDay);
    void updatePriceHis(IbFundPriceDay fundPriceDay);
    //查询个数
    int getFundPriceDayCount(Map<String, Object> map);

    List<IbFundPriceDay> queryFundcodePriceList(Map<String,Object> map);

    /**
     * 顺序排列净值
     * @param map
     * @return
     */
    public List<IbFundPriceDay> getPriceList(Map<String,Object> map);

    void insertPriceDaily(IbFundPriceDay fundPriceDay);

    public List<IbFundPriceDay> getPriceDailyList(Map<String,Object> map);


    IbFundPriceDay getFundPriceDay(@Param("fundCode") String fundCode, @Param("postdate") String postdate);
}
