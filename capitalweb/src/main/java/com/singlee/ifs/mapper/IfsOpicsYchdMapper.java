package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsYchd;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 利率曲线
 * @author lij
 *
 */
public interface IfsOpicsYchdMapper {
	
	//新增
	void insert(IfsOpicsYchd entity);
	
	//修改
	void updateById(IfsOpicsYchd entity);
	
	//删除
	void deleteById(Map<String,Object> map);
	
	//分页查询
	Page<IfsOpicsYchd> searchPageOpicsYchd(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsYchd searchById(Map<String,Object> map);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsYchd entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(Map<String,Object> map);
	
	
	//查询所有的利率曲线
	List<IfsOpicsYchd> searchAllOpicsYchd();

}
