package com.singlee.ifs.mapper;
import com.singlee.ifs.model.TtFxBasic;

import java.util.List;
import java.util.Map;

public interface TtFxBasicMapper{

	/**
	 * 根据条件查询本方清算机构基本信息
	 * @param params
	 * @return
	 */
	public List<TtFxBasic> searchInstBasic(Map<String, Object> params);
	
	public List<TtFxBasic> searchMyInstBasicFXMM(Map<String, Object> params);//本方清算信息
	public List<TtFxBasic> searchMyInstBasicFXMMOpponet(Map<String, Object> params);//对手方清算信息
	public String searchMyInstBasicFXMMOther(Map<String, Object> params);//其他信息
	public String searchMyInstBasicFXSPTOther(Map<String, Object> params);
	public List<TtFxBasic> searchMyInstBasicFXFWD(Map<String, Object> params);
	public List<TtFxBasic> searchMyInstBasicFXFWDOpponet(Map<String, Object> params);
	public String searchMyInstBasicFXFWDOther(Map<String, Object> params);

	List<TtFxBasic> searchMyInstBasicFXSPT(Map<String, Object> params);

	List<TtFxBasic> searchMyInstBasicFXSPTOpponet(Map<String, Object> params);
}
