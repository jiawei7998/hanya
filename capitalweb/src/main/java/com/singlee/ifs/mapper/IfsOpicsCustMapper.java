package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.ifs.model.IfsOpicsCust;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 
 * opics 交易对手表 mapper
 * 
 */
public interface IfsOpicsCustMapper {
	/**
	 * 新增
	 */
	void insert(IfsOpicsCust entity);

	/**
	 * 修改
	 */
	void updateById(IfsOpicsCust entity);
	
	/**
	 * 根据CFETS单号修改
	 */
	void updateByCftesno(Map<String, Object> map);
	void updateByCftescn(Map<String, Object> map);
	
	void updateNullByCftesno(String cfetsno);
	void updateNullByCftescn(String cfetscn);

	/**
	 * 删除
	 */
	void deleteById(String id);

	/**
	 * 分页查询
	 */
	Page<IfsOpicsCust> searchPageOpicsCust(Map<String, Object> map, RowBounds rb);

	
	/**
     * 分页查询
     */
    Page<IfsOpicsCust> searchPageOpicsCustRelease(Map<String, Object> map, RowBounds rb);

	/**
	 * 根据交易对手编号查询实体类
	 */
	IfsOpicsCust searchIfsOpicsCust(String cno);

	/**
	 * 状态回查使用
	 * 
	 * @return
	 */
	List<IfsOpicsCust> getOpicsCustList();
	
	
	List<Map<String,Object>> searchSingleAll(Map<String,Object> map);

	/**
	 * 根据fedealno更改opics
	 * 
	 * @param fedealno
	 */
	void updateCustByFedealno(Map<String, Object> map);

	public String getFedealNo(HashMap<String, Object> map);

	public String searchCnameByCno(Map<String, Object> map);

	/**
	 * 查询所有的交易对手
	 */
	Page<IfsOpicsCust> searchAllOpicsCust();

	IfsOpicsCust searchById(String id);

	/**
	 * 根据id 更新同步状态
	 */
	void updateStatus0ById(String id);

	void updateStatus(IfsOpicsCust entity);

	/**
	 * 更新客户信息
	 */
	void updateCustInfo(SlCustBean slCustBean);

	/**
	 * 更新本地的OPICS信息
	 */
	void updateLocalById(IfsOpicsCust entity);

	/**
	 * 将OPICS信息插入到本地
	 */
	void insertOpics(IfsOpicsCust entity);

	/**
	 * 根据交易对手代号查询
	 */
	IfsOpicsCust searchByCname(String cname);

	/**
	 * CFETS 21位机构编码
	 */
	IfsOpicsCust searchByCfetsno(String cfetsno);
	
	/**
	 * CFETS 21位机构编码 和 6 位机构编码
	 */
	List<IfsOpicsCust> searchByCfets(Map<String, Object> map);

	/**
	 * 根据客户号和交易对手编号查询某一条
	 */
	List<IfsOpicsCust> searchSingle(IfsOpicsCust slCustBean);

	/**
	 * 查询所有客户信息返回List
	 */
	List<IfsOpicsCust> searchListOpicsCust(Map<String, Object> map);
}