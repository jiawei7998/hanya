package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecForeignCrsModel;

public interface IfsCpisCrsRecMapper {

    int insertSelective(RecForeignCrsModel record);

    int getRecSptFwdCount(RecForeignCrsModel record);
    
    int updateByConfirmId(RecForeignCrsModel record);
    
}