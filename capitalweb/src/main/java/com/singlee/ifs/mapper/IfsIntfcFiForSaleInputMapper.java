package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.ifs.model.IfsIntfcFiForSale;
import com.singlee.ifs.model.IfsIntfcFiStatus;

public interface IfsIntfcFiForSaleInputMapper {
	
	String getOpicsSysDate(Map<String,Object> remap);
	
	List<IfsIntfcFiForSale> queryAllConditionFiList(Map<String,Object> map);
	
	IfsIntfcFiStatus queryFiStatusByDealnoSeq(Map<String,Object> map);
	
	int updateFiStatusByDealnoSeq(IfsIntfcFiStatus fiStatus);
	
	int insertFiStatusByNew(IfsIntfcFiStatus fiStatus);

}
