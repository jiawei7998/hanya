package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsHldy;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 节假日
 * @author lij
 *
 */
public interface IfsOpicsHldyMapper {
	
	//新增
	void insert(IfsOpicsHldy entity);
	
	//修改
	void updateById(IfsOpicsHldy entity);
	
	//删除
	void deleteById(Map<String,Object> map);
	
	//删除
	void deleteById1(IfsOpicsHldy entity);
	
	//分页查询
	Page<IfsOpicsHldy> searchPageOpicsHldy(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsHldy searchById(Map<String,Object> map);
	
	IfsOpicsHldy searchEntity(IfsOpicsHldy entity);
	
	int searchById1(IfsOpicsHldy entity);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsHldy entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(Map<String,Object> map);
	
	
	//查询所有的利率曲线
	List<IfsOpicsHldy> searchAllOpicsHldy();
	
	
	public List<IfsOpicsHldy> getCalendarByMonth(Map<String, Object> param);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
