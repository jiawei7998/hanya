package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecForeignSwapModel;

public interface IfsCpisFxSwpRecMapper {

    int insertSelective(RecForeignSwapModel record);

    int getRecSwpCount(RecForeignSwapModel record);
    
    int updateByConfirmId(RecForeignSwapModel record);
}