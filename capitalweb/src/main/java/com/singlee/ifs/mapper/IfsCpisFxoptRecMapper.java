package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecForeignOptionModel;

public interface IfsCpisFxoptRecMapper {

    int insertSelective(RecForeignOptionModel record);

    int getRecFxoptCount(RecForeignOptionModel record);
    
    int updateByExecId(RecForeignOptionModel record);
    
}