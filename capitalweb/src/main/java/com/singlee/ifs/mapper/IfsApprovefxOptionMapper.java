package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxOption;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxOptionMapper {

	public Page<IfsApprovefxOption> searchOptionPage(Map<String, Object> map,RowBounds rowBounds);

	public IfsApprovefxOption searchOption(@Param("ticketId")String ticketId);

	public void addOption(IfsApprovefxOption map);

	public void editOption(IfsApprovefxOption map);

	public void deleteOption(@Param("ticketId")String ticketId);

	public void updateOptionByID(String serial_no, String status,String date);

	public Page<IfsApprovefxOption> getOptionMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxOption> getOptionMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOption> getOptionMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
/*
	public Page<IfsApprovefxOption> getOptionMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxOption> getOptionMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxOption> getOptionMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/

	public IfsApprovefxOption getOptionAndFlowIdById(Map<String, Object> map);
	
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public IfsApprovefxOption selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsApprovefxOption> searchApprovePassed(
			Map<String, Object> params,RowBounds rowBounds);
}