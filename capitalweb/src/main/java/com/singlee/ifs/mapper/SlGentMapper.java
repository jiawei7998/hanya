package com.singlee.ifs.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIntfcGent;

public interface SlGentMapper {
	
	Page<SlIntfcGent> querySlGent(Map<String, String> params, RowBounds rb);
	
	void gentAdd(SlIntfcGent entity);
	
	void gentEdit(SlIntfcGent entity);
	
	void deleteSlGent(SlIntfcGent entity);
	
	SlIntfcGent querySlIntfcGentById(SlIntfcGent entity);
}
