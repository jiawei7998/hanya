package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxAllot;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;

public interface IfsApproveAllocateMapper {

	public Page<IfsApprovefxAllot> searchIfsAllocatePage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteIfsAllocate(@Param("ticketId")String ticketid);

	public void addIfsAllocate(IfsApprovefxAllot map);

	public void editIfsAllocate(IfsApprovefxAllot map);

	public IfsApprovefxAllot searchIfsAllocate(@Param("ticketId")String ticketid);

	public void updateAllocateByID(String serial_no, String status,String date);

	public Page<IfsApprovefxAllot> getAllocateMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxAllot> getAllocateMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxAllot> getAllocateMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public IfsApprovefxAllot searchCfetsAllotAndFlowIdById(Map<String, Object> map);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public IfsApprovefxAllot selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsApprovefxAllot> searchApprovePassed(
			Map<String, Object> params,RowBounds rowBounds);
	

	/*public Page<IfsApprovefxAllot> getAllocateMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxAllot> getAllocateMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxAllot> getAllocateMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
*/
}
