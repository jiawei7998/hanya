package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.capital.opics.model.SlInthBean;
import com.singlee.capital.opics.model.SlIrevBean;
import com.singlee.ifs.model.IfsIntfcIrevBean;

import tk.mybatis.mapper.common.Mapper;

public interface IfsIntfcIrevMapper extends Mapper<IfsIntfcIrevBean>{
	
	List<IfsIntfcIrevBean> getIrev(Map<String, Object>  map);
	
	IfsIntfcIrevBean selectIntfcIrev(IfsIntfcIrevBean irevbean);
	
	int insertIntfcIrev(IfsIntfcIrevBean irevbean);
	
	int deleteIntfcIrev(IfsIntfcIrevBean irevbean);
	
	List<Map<String,String>> getRevp(Map<String,Object> remap);
	
	List<IfsIntfcIrevBean> searchIrevList(Map<String, Object> map);
	
	String selectIrevMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
	
	int addIrev(SlIrevBean irevBean);
	int addSlFundInth(SlInthBean inthBean);

}
