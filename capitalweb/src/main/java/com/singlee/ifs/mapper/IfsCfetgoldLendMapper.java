package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetgoldLend;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetgoldLendMapper {

	public Page<IfsCfetgoldLend> searchGoldLendPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteGoldLend(@Param("ticketId")String ticketid);

	public void addGoldLend(IfsCfetgoldLend map);

	public void editGoldLend(IfsCfetgoldLend map);

	public IfsCfetgoldLend searchGoldLend(@Param("ticketId")String ticketid);

	public void updateGoldLendByID(String serial_no, String status,String date);

	public Page<IfsCfetgoldLend> getGoldLendMineList(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetgoldLend> getGoldLendMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetgoldLend> getGoldLendMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
	
	public List<IfsCfetgoldLend> searchById(@Param("contractId")String contractId);

	public IfsCfetgoldLend selectFlowByPrimaryKey(Map<String, Object> key);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetgoldLend> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

}