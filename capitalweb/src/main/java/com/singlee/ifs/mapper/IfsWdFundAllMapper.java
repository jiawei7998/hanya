package com.singlee.ifs.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.model.ChinaMutualFundDescription;

import java.util.List;
import java.util.Map;

public interface IfsWdFundAllMapper extends MyMapper<ChinaMutualFundDescription>{
    
    List<ChinaMutualFundDescription> getFundAllList(Map<String, Object> map);
    
    //获取所有的基金公司
    List<String> getComp();

    void insertFundAll(ChinaMutualFundDescription chinaMutualFundDescription);

    void deleteFundAll(ChinaMutualFundDescription chinaMutualFundDescription);
}
