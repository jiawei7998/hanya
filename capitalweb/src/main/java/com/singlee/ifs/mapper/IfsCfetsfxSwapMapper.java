package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxSwap;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxSwapMapper {

	public Page<IfsCfetsfxSwap> getswapPage(Map<String, Object> map,RowBounds rowBounds);

	public IfsCfetsfxSwap getrmswap(@Param("ticketId")String ticketId);

	public void addrmswap(IfsCfetsfxSwap map);

	public void editrmswap(IfsCfetsfxSwap map);

	public void deletermswap(@Param("ticketId")String ticketId);
	
	public void updateRmsSwapByID(String serial_no,String status,String date);
	
	void updateApproveStatusFxSwapByID(String ticketId);
	
	/**
	 * 根据请求参数查询分页列表    我发起的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsCfetsfxSwap> getFxSwapMineListMine(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表    已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsCfetsfxSwap> getFxSwapMineListFinished(Map<String, Object> params, RowBounds rb);

	/**
	 * 根据请求参数查询分页列表    待审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsCfetsfxSwap> getFxSwapMineList(Map<String, Object> params, RowBounds rb);
	
	IfsCfetsfxSwap selectFlowIdByPrimaryKey(Map<String,Object>  key);
	
	public List<IfsCfetsfxSwap> searchById(@Param("contractId")String contractId);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxSwap> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsfxSwap> getCfetsfxSwapList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsfxSwapByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	Page<Map<String, Object>> getMini(Map<String, Object> params, RowBounds rowBounds);
	
	/**
	 * 外汇掉期
	 * @param map
	 */
	public Page<IfsCfetsfxSwap> searchTotalDealRMBWBDQ(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsfxSwap searchSwapDetailByTicketId(Map<String, Object> map);
	
	//增加未交割头寸损益
	void updateProfitLess(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsfxSwap> searchSwiftNotExist();
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsfxSwap> searchSwiftNotExistByDate(Map<String, Object> map);

	public void insertSwapToOpics(Map<String, String> map);
}