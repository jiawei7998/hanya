package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWdSec;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


public interface IfsWdSecMapper{
	// 新增
	void insert(IfsWdSec entity);
	//查询
	List<IfsWdSec> queryByVDate(String vDate);
	// 删除
	void delete();
	//查询
	List<IfsWdSec> queryAll();
	/**
	 * 分页查询
	 * 
	 * @param map
	 * @param rb 
	 * @return
	 */
	Page<IfsWdSec> getWdSecList(Map<String, Object> map, RowBounds rb);
	//通过发行人查询债券
	List<IfsWdSec> querySecidsByIssuer(String issuer);
}
