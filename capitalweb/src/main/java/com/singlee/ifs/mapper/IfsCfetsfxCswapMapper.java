package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxCswap;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxCswapMapper {

	public Page<IfsCfetsfxCswap> searchCcySwapPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcySwap(@Param("ticketId")String ticketid);

	public void addCcySwap(IfsCfetsfxCswap map);

	public void editCcySwap(IfsCfetsfxCswap map);

	public IfsCfetsfxCswap searchCcySwap(@Param("ticketId")String ticketid);

	public void updateCcySwapByID(String serial_no, String status,String date);

	public Page<IfsCfetsfxCswap> getCcySwapMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxCswap> getCcySwapMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxCswap> getCcySwapMineListMine(
			Map<String, Object> params, RowBounds rowBounds);
	
	IfsCfetsfxCswap selectFlowIdByPrimaryKey(Map<String,Object>  key);
	
	public List<IfsCfetsfxCswap> searchById(@Param("contractId")String contractId);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxCswap> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*public Page<IfsCfetsfxCswap> getCcySwapMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxCswap> getCcySwapMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxCswap> getCcySwapMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsfxCswap> getCfetsfxCswapList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsfxCswapByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	/**
	 * 总交易查询（外币掉期）
	 * @param map
	 */
	public Page<IfsCfetsfxCswap> searchTotalDealHBDQ(
			Map<String, Object> params, RowBounds rowBounds);
}