package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.model.OpicsRisk;
import com.singlee.ifs.model.IfsCfetsfxLend;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxLendMapper {

	public Page<IfsCfetsfxLend> searchCcyLendingPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcyLending(@Param("ticketId")String ticketid);

	public void editCcyLending(IfsCfetsfxLend map);

	public void addCcyLending(IfsCfetsfxLend map);

	public IfsCfetsfxLend searchCcyLending(@Param("ticketId")String ticketid);

	public void updatefxLendingByID(String serial_no, String status,String date);
	
	void updateApproveStatusFxLendByID(String ticketId);

	public Page<IfsCfetsfxLend> getCcyLendingMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxLend> getCcyLendingMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxLend> getCcyLendingMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public IfsCfetsfxLend getCcyLendAndFlowIdById(Map<String, Object> map);
	
	public List<IfsCfetsfxLend> searchById(@Param("contractId")String contractId);

	public IfsCfetsfxLend selectFlowByPrimaryKey(Map<String, Object> key);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxLend> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*public Page<IfsCfetsfxLend> getCcyLendingMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxLend> getCcyLendingMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxLend> getCcyLendingMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsfxLend> getCfetsfxLendList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateFxLendByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String,Object> map);
	
	/**
	 * 查询ecif编码
	 */
	public String queryIfsOpicsCust(String cNo);
	
	public List<EdInParams> searchAllInfo(@Param("ticketId")String ticketid);
	
	public List<OpicsRisk> searchProductSupervise(@Param("prdNo")String prdNo,@Param("operator")String operator);
   
	/**
	 * 总交易查询（外币拆借）
	 */
	public Page<IfsCfetsfxLend> searchTotalDealWBCQ(
			Map<String, Object> params, RowBounds rowBounds);
	
	/**
	 * 由 dealNo 查询权重，组织机构号信息
	 */
	public List<EdInParams> searchSingleInstInfo(@Param("dealNo")String dealNo);
	
	public List<EdInParams> searchForeignExchangeInfo(@Param("ticketId")String ticketid);
	

	/**
	 * 外汇债券存单控制
	 */
	public List<EdInParams> limitBondExchangeInfo(Map<String, Object> map);
	
	/**
	 * 查询opics中交易总额
	 * @param map
	 * @return
	 */
	public double queryTotalCount();
	
	/**
	 * 查询信用拆借和外币拆借交易
	 */
	
	public EdInParams searchBondInfo (@Param("opicsDealNo")String opicsDealNo);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsfxLend searchLendDetailByTicketId(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsfxLend> searchSwiftNotExist(); 
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsfxLend> searchSwiftNotExistByDate(Map<String, Object> map);
	
}
