package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCdtcCnBondLinkSecurdeal;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;


/**
 *
 * @author singlee
 *
 */
public interface IfsCdtcCnBondLinkSecurdealMapper {
	
	Page<IfsCdtcCnBondLinkSecurdeal> selectPageCnBondLinkSecurdeal(Map<String, Object> map, RowBounds rowBounds);
	
	IfsCdtcCnBondLinkSecurdeal selectByDealNo(String dealNo);
}
