package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbDetailCr;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;


public interface IfsCfetsrmbDetailCrMapper {
    int insert(IfsCfetsrmbDetailCr record);

    int insertSelective(IfsCfetsrmbDetailCr record);

	void deleteByTicketId(@Param("ticketId")String ticketId);

	Page<IfsCfetsrmbDetailCr> searchBondCrDetails(Map<String, Object> params,
			RowBounds rowBounds);
	List<IfsCfetsrmbDetailCr> searchBondByTicketId(@Param("ticketId")String ticketId);
	
	/**
	 * 状态回查使用,未复核的质押券
	 * @return
	 */
	List<IfsCfetsrmbDetailCr> getCfetsrmbDetailCrList();
	
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void updateDetailCrByFedealno(Map<String, Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
}