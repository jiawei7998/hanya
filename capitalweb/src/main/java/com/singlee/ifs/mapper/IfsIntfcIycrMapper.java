package com.singlee.ifs.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.capital.opics.model.SlInthBean;
import com.singlee.capital.opics.model.SlIrhsBean;
import com.singlee.capital.opics.model.SlIycrBean;
import com.singlee.ifs.model.IfsIntfcIrhsBean;
import com.singlee.ifs.model.IfsIntfcIycrBean;

import tk.mybatis.mapper.common.Mapper;

public interface IfsIntfcIycrMapper extends Mapper<IfsIntfcIycrBean>{
	
	List<IfsIntfcIycrBean> getIycr(Map<String, Object>  map);
	
	IfsIntfcIycrBean selectIntfcIycr(IfsIntfcIycrBean irevbean);
	
	int insertIntfcIycr(IfsIntfcIycrBean irevbean);
	
	int deleteIntfcIycr(IfsIntfcIycrBean irevbean);
	
	List<IfsIntfcIycrBean> searchIycrList(Map<String, Object> map);
	
	List<IfsIntfcIrhsBean> searchIrhsList(Map<String, Object> map);
	
	String selectIycrMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
	
	String selectIrhsMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
	
	int addIycr(SlIycrBean iycrBean);
	int addSlFundInth(SlInthBean inthBean);
	
	int addIrhs(SlIrhsBean irhsBean);

}
