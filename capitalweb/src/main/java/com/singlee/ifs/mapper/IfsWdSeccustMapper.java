package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWdSeccust;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsWdSeccustMapper {
	// 新增
	void insert(IfsWdSeccust entity);
	//查询
	List<IfsWdSeccust> query();
	// 删除
	void delete();
	Page<IfsWdSeccust> getWdSeccustList(Map<String, Object> map, RowBounds rb);
}
