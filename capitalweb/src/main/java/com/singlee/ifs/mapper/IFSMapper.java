package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.financial.bean.GlInfoArray;
import com.singlee.ifs.model.IfsDownPaket;
import com.singlee.ifs.model.IfsNbhBean;
import com.singlee.ifs.model.IfsNbhGatherBean;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IFSMapper extends MyMapper<IfsDownPaket> {
	/**
	 * 获取接口下行报文,并保存
	 * 
	 * @param downPaket
	 */
	void insertDownPaket(IfsDownPaket downPaket);

	/**
	 * 查询接口下行报文
	 * 
	 * @param map
	 * @return
	 */
	IfsDownPaket getDownPaket(Map<String, Object> map);

	/**
	 * 查询接口下行报文
	 * 
	 * @param map
	 * @return
	 */
	List<IfsDownPaket> getDownPakets(Map<String, Object> map);

	/**
	 * 根据不同的产品不同的操作类型获取审批单编号
	 * 
	 * @return
	 */
	String getTradeId(HashMap<String, Object> map);

	/**
	 * 跑批结束后记录日期
	 */
	void updateCurDate(String date);
	
	/**
	 * 内部户新增时保存
	 */
	void nbhAdd(Map<String, Object> map);
	
	/**
	 * 查询所有内部户数据
	 */
	Page<IfsNbhBean> searchData(IfsNbhBean entity);
	
	/**
	 * 删除某一条记录
	 */
	void deleteNbh(Map<String, String> map);
	
	/**
	 * 内部户修改时保存
	 */
	void updateById(IfsNbhBean entity);
	
	/**
	 * 查询内部户
	 */
	List<IfsNbhBean> queryAcct();
	
	/**
	 * 插入数组明细表
	 */
	void insertDetail(GlInfoArray glInfoBean);
	
	/**
	 * 查询明细表
	 */
	List<GlInfoArray> queryContent(Map<String,String> upmap);
	
	/**
	 * 查询一对交易
	 */
	List<IfsNbhBean> queryData();
	
	/**
	 * 查询数据字典VALUE值
	 */
	String queryDictValue(Map<String,String> map);
	
	/**
	 * 查询外汇头寸估值
	 */
	Page<IfsNbhGatherBean> searchNbhPage(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 外汇头寸估值  新增时保存
	 */
	void nbhGatherAdd(IfsNbhGatherBean entity);
	
	/**
	 * 外汇头寸估值  修改时保存
	 */
	void updateByGatherId(IfsNbhGatherBean entity);
	
	/**
	 * 外汇头寸估值  删除某一条记录
	 */
	void deleteNbhAcct(Map<String, String> map);
	
	/**
	 * 根据fedealno查询实体类
	 */
	IfsNbhGatherBean searchNbhPageSingle(String fedealno);
	
	/**
	 * 修改处理状态
	 */
	void updateStatus(IfsNbhGatherBean entity);
}