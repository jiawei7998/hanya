package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.BondTemplate;
import com.singlee.ifs.model.IfsLimitBondDetail;
import com.singlee.ifs.model.InvestLimtlog;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface BondLimitTemplateMapper extends Mapper<BondTemplate>{
	public Page<BondTemplate> searchTemplateBond(Map<String, Object> map,RowBounds rb);
	/**
	 * 根据条件查询债券限额信息
	 * @param map
	 * @return
	 */
	public List<BondTemplate> queryOccupyBond(Map<String, Object> map);
	
	/**
	 * 插入限额明细信息
	 * @param map
	 * @return
	 */
	public void insertLimitDetail(Map<String, Object> map);
	
	/**
	 * 查询占限额总数
	 * @param map
	 * @return
	 */
	public double queryCount(Map<String, Object> map);
	
	/**
	 * 查询是否有限额信息
	 * @param map
	 * @return
	 */
	public int queryDetail(String id);
	
	/**
	 * 更新明细有效状态
	 * @param map
	 * @return
	 */
	public int updateFlag(String id);
	
	/**
	 * 更新限额的额度
	 * @param map
	 * @return
	 */
	public int updateOccupyBond(Map<String, Object> map);
	
	/**
	 * 增加债券信息明细
	 * @param map
	 * @return
	 */
	void insertIntoBondDetail(Map<String, Object> map);
	
	/**
	 * 删除当日限额债券信息明细
	 * @param map
	 * @return
	 */
	void deleteIntoBondDetail(String nowDate);
	
	/**
	 * 债券明细查询
	 * @param map
	 * @return
	 */
	Page<IfsLimitBondDetail> searchAllBondDetail(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 删除到期日期的债券
	 * 
	 * @param sceneId
	 */
	public void deleteOverTimeBond(String ticket_id);
	
	/**
	 * 限额报表明细
	 * @param map
	 * @return
	 */
	Page<InvestLimtlog> getBondInvestmentLimit(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 限额报表明细
	 * @param List
	 * @return
	 */
	List<InvestLimtlog> getBondLimitForAddDayLimit(Map<String, Object> map);
	
	/**
	 * 增加限额报表明细
	 * @param map
	 * @return
	 */
	void insertBondInvestLimit(Map<String, Object> map);
	
	/**
	 * 由日期和账户类型查询
	 * @param map
	 * @return
	 */
	InvestLimtlog investLimtlog (Map<String, Object> map);
	
	/**
	 * 撤回业务进行删除
	 * 
	 * @param sceneId
	 */
	public void deleteOverTimeBondLog(String ticketId);
	
	//获取上一个月止损
	public String qryLastMonthLimit(Map<String, Object> map);
	
	/**
	 * 删除指定日期的债券
	 * 
	 * @param sceneId
	 */
	public void deleteExistBond(Map<String, Object> map);
}
