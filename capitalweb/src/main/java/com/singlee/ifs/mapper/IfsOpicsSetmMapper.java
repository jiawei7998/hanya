package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSetm;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 清算方式表
 * @author lij
 *
 */
public interface IfsOpicsSetmMapper {
	//新增
	void insert(IfsOpicsSetm entity);
	
	//修改
	void updateById(IfsOpicsSetm entity);
	
	//删除
	void deleteById(IfsOpicsSetm entity);
	
	//分页查询
	Page<IfsOpicsSetm> searchPageOpicsSetm(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsSetm searchById(IfsOpicsSetm entity);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsSetm entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(IfsOpicsSetm entity);
	
	
	//查询所有的清算方式
	List<IfsOpicsSetm>  searchAllOpicsSetm();
	
	/**
	 * 查询已同步的清算方式
	 */
	List<IfsOpicsSetm> searchSettmeans();
}