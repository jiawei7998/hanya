package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsActy;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 
 * opics 会计分类参数表 mapper
 * 
 */
public interface IfsOpicsActyMapper {

	// 新增
	void insert(IfsOpicsActy entity);

	// 修改
	void updateById(IfsOpicsActy entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsActy> searchPageOpicsActy(Map<String, Object> map, RowBounds rb);

	IfsOpicsActy searchById(String id);

	void updateStatus(IfsOpicsActy entity);

	// 根据id 更新同步状态
	void updateStatus0ById(String id);

	// 查询所有的会计类型
	Page<IfsOpicsActy> searchAllOpicsActy();

	List<IfsOpicsActy> actyList(Map<String, Object> map);
}