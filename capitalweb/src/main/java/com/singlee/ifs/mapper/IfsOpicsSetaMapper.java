package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsSeta;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * opics seta mapper
 * @author yanming
 */
public interface IfsOpicsSetaMapper {
	
	// 新增
	void insert(IfsOpicsSeta entity);

	// 修改
	void updateById(IfsOpicsSeta entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsOpicsSeta> searchPageOpicsSeta(Map<String, Object> map, RowBounds rb);
	
	//连表分页查询
	Page<IfsOpicsSeta> searchPageOpicsSetaMini(Map<String, Object> map, RowBounds rb);

	// 根据id查询实体类
	IfsOpicsSeta searchById(Map<String,String> map);

	// 根据实体 更新同步状态
	void updateStatus(IfsOpicsSeta entity);

	// 根据id 更新同步状态
	void updateStatus0ById(String id);

	// 查询所有的SETA
	List<IfsOpicsSeta> searchAllOpicsSeta();

}
