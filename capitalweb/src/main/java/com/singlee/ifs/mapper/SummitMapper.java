package com.singlee.ifs.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.model.IfsBaseBean;

/**
 * 用于处理导入SUMMIT系统中相关sql
 * 
 * @author shenzl
 * 
 */
public interface SummitMapper extends MyMapper<IfsBaseBean> {

}
