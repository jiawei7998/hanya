package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsQuota;
import com.singlee.ifs.model.IfsQuotaKey;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsQuotaMapper {
    int deleteByPrimaryKey(IfsQuotaKey key);

    int insert(IfsQuota record);

    int insertSelective(IfsQuota record);

    IfsQuota selectByPrimaryKey(IfsQuotaKey key);
    
    Page<IfsQuota> searchIfsQuotaPage(Map<String, Object> params,RowBounds rowBounds);

    int updateByPrimaryKeySelective(IfsQuota record);

    int updateByPrimaryKey(IfsQuota record);
}