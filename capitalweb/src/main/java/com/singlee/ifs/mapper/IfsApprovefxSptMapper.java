package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxSpt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxSptMapper {

	public Page<IfsApprovefxSpt> getSpotPage(Map<String, Object> map, RowBounds rowBounds);

	public IfsApprovefxSpt getSpot(@Param("ticketId")String ticketId);

	public void addSpot(IfsApprovefxSpt map);

	public void editSpot(IfsApprovefxSpt map);

	public void deleteSpot(@Param("ticketId")String ticketId);

	public void updateSpotByID(String serial_no, String status,String date);

	public Page<IfsApprovefxSpt> getSpotMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxSpt> getSpotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxSpt> getSpotMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
	
	IfsApprovefxSpt selectFlowIdByPrimaryKey(Map<String,Object>  key);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsApprovefxSpt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}