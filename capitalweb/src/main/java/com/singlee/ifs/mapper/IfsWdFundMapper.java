package com.singlee.ifs.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.model.ChinaMutualFundNAV;

import java.util.List;
import java.util.Map;



public interface IfsWdFundMapper extends MyMapper<ChinaMutualFundNAV>{
    
    List<ChinaMutualFundNAV> getFundList(Map<String, Object> map);
    
   ChinaMutualFundNAV getFundOne(ChinaMutualFundNAV chinaMutualFundNAV);
    
 void insertFund(ChinaMutualFundNAV chinaMutualFundNAV);
 
 void deleteFund(ChinaMutualFundNAV chinaMutualFundNAV);

}
