package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsmetalGold;
import com.singlee.ifs.model.IfsCfetsmetalGoldKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsmetalGoldMapper {
    int deleteByPrimaryKey(IfsCfetsmetalGoldKey key);

    int insert(IfsCfetsmetalGold record);

    IfsCfetsmetalGold selectByPrimaryKey(IfsCfetsmetalGoldKey key);
    
    IfsCfetsmetalGold selectFlowByPrimaryKey(Map<String, Object> key);

    int updateByPrimaryKey(IfsCfetsmetalGold record);
    
    Page<IfsCfetsmetalGold> selectIfsCfetsmetalGoldList(IfsCfetsmetalGold record);

	void updateMetalGoldByID(String serial_no, String status,String date);

	Page<IfsCfetsmetalGold> getMetalGoldMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	IfsCfetsmetalGold searchGoldspt(@Param(value = "ticketId") String ticketId);
	
	public List<IfsCfetsmetalGold> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsmetalGold> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsmetalGold> getMetalGoldMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	Page<IfsCfetsmetalGold> getMetalGoldMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsmetalGold> getCfetsMetalGoldList();
	/**
	 * 冲销回查使用
	 * @return
	 */
	List<IfsCfetsmetalGold> getCfetsMetalGoldReverseList();
	
	void  updateCfetsMetalGoldByFedealno(Map<String,Object> map);
	void  updateReverseCfetsMetalGoldByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	/**
	 * 总交易查询（黄金即期）
	 * @param map
	 */
	Page<IfsCfetsmetalGold> searchTotalDealHJ(Map<String, Object> params,RowBounds rowBounds);

	List<IfsCfetsmetalGold> searchSwiftNotExist();
	
	IfsCfetsmetalGold getGoldByIdAndType(Map<String, String> map);

	IfsCfetsmetalGold searchMetalGold(Map<String, Object> map);
}