package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsCpisFailrec;

import java.util.List;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/12 18:40
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsCpisFailrecMapper {
    int insert(IfsCpisFailrec record);

    int insertSelective(IfsCpisFailrec record);

    List<IfsCpisFailrec> selectByAll(IfsCpisFailrec ifsCpisFailrec);


}