package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxDebt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsApprovefxDebtMapper {

	public Page<IfsApprovefxDebt> searchIfsDebtPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteIfsDebt(@Param("ticketId")String ticketid);

	public void addIfsDebt(IfsApprovefxDebt map);

	public void editIfsDebt(IfsApprovefxDebt map);

	public IfsApprovefxDebt searchIfsDebt(@Param("ticketId")String ticketid);

	public void updateDebtByID(String serial_no, String status,String date);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	
	public Page<IfsApprovefxDebt> getDebtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxDebt> getDebtMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxDebt> getDebtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	public List<IfsApprovefxDebt> searchById(@Param("contractId")String contractId);

	public IfsApprovefxDebt selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsApprovefxDebt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	
	/*public Page<IfsApprovefxDebt> getDebtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxDebt> getDebtMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxDebt> getDebtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
		
}
