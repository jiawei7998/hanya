package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsfxSpt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface IfsCfetsfxSptMapper {

	public Page<IfsCfetsfxSpt> getSpotPage(Map<String, Object> map, RowBounds rowBounds);

	public IfsCfetsfxSpt getSpot(@Param("ticketId")String ticketId);

	public void addSpot(IfsCfetsfxSpt map);

	public void editSpot(IfsCfetsfxSpt map);

	public void deleteSpot(@Param("ticketId")String ticketId);

	public void updateSpotByID(String serial_no, String status,String date);
	
	void updateApproveStatusFxSptByID(String ticketId);

	public Page<IfsCfetsfxSpt> getSpotMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxSpt> getSpotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxSpt> getSpotMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
	
	IfsCfetsfxSpt selectFlowIdByPrimaryKey(Map<String,Object>  key);

	IfsCfetsfxSpt selectOneByid(@Param("ticketId")String ticketId);
	
	public List<IfsCfetsfxSpt> searchById(@Param("contractId")String contractId);

	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public Page<IfsCfetsfxSpt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*public Page<IfsCfetsfxSpt> getSpotMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsCfetsfxSpt> getSpotMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsCfetsfxSpt> getSpotMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/

	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsfxSpt> getCfetsfxSptList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsfxSptByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	/**
	 * 搜索机构下所有交易
	 */
	public Page<IfsCfetsfxSpt> searchTotalDealRMBWBJQ(Map<String, Object> params,
			RowBounds rowBounds);
	/**
	 * 审批台查询交易详情
	 */
	IfsCfetsfxSpt searchSptDetailByTicketId(Map<String, Object> map);
	
	/** 即期外汇交易业务校验    */
	public Page<IfsCfetsfxSpt> searchSptCheck(Map<String, Object> map,RowBounds rowBounds);

	public List<IfsCfetsfxSpt> searchSptCheckList(Map<String, Object> map);
	
	/**
	 * 获取当天，净额清算的swift中dealno不存在的交易
	 */
	public List<IfsCfetsfxSpt> searchSwiftNotExist();
	
	/**
	 * 获取指定日期，净额清算的swift中dealno不存在的交易
	 * 日期：forDate
	 */
	public List<IfsCfetsfxSpt> searchSwiftNotExistByDate(Map<String, Object> map);

	public void insertSptToOpics(Map<String,String> map);
	
}