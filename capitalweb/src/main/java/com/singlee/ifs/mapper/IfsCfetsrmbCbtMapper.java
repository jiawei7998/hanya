package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCbtReportBean;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.model.IfsCfetsrmbCbtKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbCbtMapper {
    int deleteByPrimaryKey(IfsCfetsrmbCbtKey key);

    int insert(IfsCfetsrmbCbt record);

    IfsCfetsrmbCbt selectByPrimaryKey(IfsCfetsrmbCbtKey key);
    
    IfsCfetsrmbCbt selectFlowIdByPrimaryKey(Map<String,Object>  key);
    
    IfsCfetsrmbCbt selectById(Map<String, String> map);

    int updateByPrimaryKey(IfsCfetsrmbCbt record);
    
    Page<IfsCfetsrmbCbt> selectIfsCfetsrmbCbtList(IfsCfetsrmbCbt record);

	void updateRmbCbtByID(String serial_no, String status,String date);
	
	void updateApproveStatusRmbCbtByID(String ticketId);

	Page<IfsCfetsrmbCbt> getRmbCbtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbCbt searchIfsBondTra(@Param(value = "ticketId") String ticketId);
	
	public List<IfsCfetsrmbCbt> searchById(@Param("contractId")String contractId);

	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsCfetsrmbCbt> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*Page<IfsCfetsrmbCbt> getRmbCbtMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCbt> getRmbCbtMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsrmbCbt> getCfetsrmbCbtList();
	/**
	 * 根据fedealno更改opics
	 * @param fedealno
	 */
	void  updateCfetsrmbCbtByFedealno(Map<String,Object> map);
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);

	/**
	 * 总交易查询（现券买卖）
	 * @param map
	 */
	Page<IfsCfetsrmbCbt> searchTotalDealXJMM(Map<String, Object> params,
			RowBounds rowBounds);

	IfsCfetsrmbCbt searchCfetsrmbCbtByTicketId(Map<String, Object> map);
	
	/**
	 * （现券买卖）历史查询List
	 * @param map
	 */
	List<IfsCfetsrmbCbt> searchCfetsrmbCbtWithList(Map<String, Object> map);
	
	/** 现券交易类业务校验     */
	public Page<IfsCfetsrmbCbt> searchCbtCheck(Map<String, Object> map,RowBounds rowBounds);

	List<IfsCfetsrmbCbt> searchCbtCheckList(Map<String, Object> map);
	
	//查询报表每个月总计
	List<IfsCbtReportBean> searchCbtReportList(Map<String, Object> map);
	
	//查询所有总计
	IfsCbtReportBean searchCbtReportListTotal(Map<String, Object> map);

	void insertCbtToOpics(Map<String,String> map);
}