package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsPort;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 
 * opics 投资组合参数表 mapper
 *
 */
public interface IfsOpicsPortMapper {
	
	//新增
	void insert(IfsOpicsPort entity);
	
	//修改
	void updateById(IfsOpicsPort entity);
	
	//删除
	void deleteById(Map<String,Object> map);
	
	//分页查询
	Page<IfsOpicsPort> searchPageOpicsPort(Map<String,Object> map,RowBounds rb);

	List<IfsOpicsPort> searchPort(HashMap<String, Object> map);
	
	IfsOpicsPort searchById(Map<String,String> map);
	
	void updateStatus(IfsOpicsPort entity);

	void updateStatus0ById(HashMap<String, String> statusmap);

	List<IfsOpicsPort> searchAllOpicsPort();
	
}
