package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbIrs;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsApprovermbIrsMapper {
    int deleteByPrimaryKey(IfsApprovermbIrs key);

    int insert(IfsApprovermbIrs record);

    int insertSelective(IfsApprovermbIrs record);

    IfsApprovermbIrs selectByPrimaryKey(IfsApprovermbIrs key);
    
    IfsApprovermbIrs selectFlowIdByPrimaryKey(Map<String,Object>  key);

    int updateByPrimaryKeySelective(IfsApprovermbIrs record);

    int updateByPrimaryKey(IfsApprovermbIrs record);
    
    Page<IfsApprovermbIrs> selectIfsCfetsrmbIrsList(IfsApprovermbIrs record);

	void updateRmbIrsByID(String serial_no, String status,String date);

	Page<IfsApprovermbIrs> getRmbIrsMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbIrs> getRmbIrsMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbIrs> getRmbIrsMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbIrs searchIfsCfetsrmbIrs(@Param(value = "ticketId") String ticketId);
	
	public List<IfsApprovermbIrs> searchById(@Param("contractId")String contractId);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	/*Page<IfsCfetsrmbIrs> getRmbIrsMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbIrs> getRmbIrsMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/

	Page<IfsApprovermbIrs> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}