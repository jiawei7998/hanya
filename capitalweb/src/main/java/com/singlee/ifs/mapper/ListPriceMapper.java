package com.singlee.ifs.mapper;

import com.singlee.ifs.model.FileNameList;

import java.util.List;
import java.util.Map;


public interface ListPriceMapper  {
    
    void insertPrice(Map<String,Object> map);
    
    List<FileNameList> getPrice(Map<String, Object>  map);
 
}
