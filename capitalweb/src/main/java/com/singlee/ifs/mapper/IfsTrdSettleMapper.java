package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.model.IfsTrdSettle;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 清算信息对象查询
 * 
 * @author shenzl
 * 
 */
public interface IfsTrdSettleMapper extends MyMapper<IfsTrdSettle> {
	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	Page<IfsTrdSettle> getTrdSettleList(Map<String, Object> map, RowBounds rb);

	/**
     * 查询所有
     * 
     * @param map
     * @return
     */
    List<IfsTrdSettle> getTrdSettleAllList(Map<String, Object> map);
    
    
    /**
     * 查询单个
     * 
     * @param map
     * @return
     */
   IfsTrdSettle getTrdSettleOne(Map<String, Object> map);

	
	/**
	 * 更新数据
	 * 
	 * @param map
	 * @return
	 */
	public int updateTrdSettle(Map<String, Object> map);

	/**
	 * 查询信息
	 * 
	 * @param param
	 * @return
	 */
	IfsTrdSettle queryTrdSettleByDealNo(Map<String, Object> param);
	
	int insertSettle(IfsTrdSettle record);
	
	String getSn();
	
	IfsTrdSettle selectMaxSeqByCno(Map<String, Object> map);
	
	/**
	 * 更新处理状态
	 */
	void updateFlag(Map<String,Object> map);
	
	/**
	 * 更新清算状态
	 */
	void updateDealStatus(Map<String,Object> map);
	
	/**
	 * 根据条件查询ESB返回的业务流水号
	 */
	String querySerialNo(Map<String,Object> map);
	
	/**
	 * 根据条件更新某一笔的流水
	 */
	void updateSeqno(Map<String,Object> map);
	/**
	 * 查询已处理未更新状态列表(无状态/处理中状态)
	 */
	List<IfsTrdSettle> getIfsTrdSettleList();
	/**
	 * 查询交易到期日期即结算日
	 */
	String getMaxVdate(Map<String, Object> map);
	/**
	 * 更新结算日期
	 */
	void updateVdate(Map<String,Object> map);
	/**
	 * 查询数据
	 * @param param
	 * @return
	 */
	IfsTrdSettle queryTrdSettle(Map<String, Object> param);

	/**
	 * 分页查询数据
	 * @param param
	 * @return
	 */
	Page<IfsTrdSettle> queryAllTrdSettle(Map<String, Object> param, RowBounds rowBounds);

	/**
	 * 本币支出
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	Page<IfsTrdSettle> queryRMBTrdSettle(Map<String, Object> param, RowBounds rowBounds);

	/**
	 * 外币收取
	 * @param param
	 * @param rowBounds
	 * @return
	 */
	Page<IfsTrdSettle> queryFXTrdSettle(Map<String, Object> param, RowBounds rowBounds);

	void updateVoidflag(Map<String, Object> param);
	/**
	 * 更新记账状态
	 * @param param
	 */
	void updateAccFlag(Map<String, Object> param);
	/**
	 * 大额清算  经办/复核  交易统计
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<IfsTrdSettle> getTrdSettleCount(Map<String, Object> map, RowBounds rb);

	IfsTrdSettle getSettleCnaps(@Param("fedealno")String fedealno);
	
	//查询所有需要发送的基金申购数据
	List<IfsTrdSettle> getIfsTrdSettleFunds(Map<String, Object> map);
	
}