package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRmbDepositin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
public interface IfsRmbDepositinMapper {
    int deleteByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);

    int deleteByTicketid(@Param("ticketId") String ticketId);

    int insert(IfsRmbDepositin record);

    int insertSelective(IfsRmbDepositin record);

    IfsRmbDepositin selectByPrimaryKey(@Param("dealTransType") String dealTransType, @Param("ticketId") String ticketId);


    Page<IfsRmbDepositin> selectByTicketId(@Param("ticketId") String ticketId,RowBounds rowBounds);

    IfsRmbDepositin searchByTicketId(@Param("ticketId") String ticketId);

    IfsRmbDepositin searchFlowByTicketId(Map<String, Object> params);

    List<IfsRmbDepositin> searchById(@Param("contractId")String contractId);

    int updateDepositInRmbByID(String serial_no, String status,String date);

    int updateByPrimaryKeySelective(IfsRmbDepositin record);

    int updateByPrimaryKey(IfsRmbDepositin record);

    Page<IfsRmbDepositin> getDepositInRmbMineList(Map<String, Object> params,
                                                RowBounds rowBounds);

    Page<IfsRmbDepositin> getDepositInRmbMineListFinished(Map<String, Object> params,
                                                        RowBounds rowBounds);

    Page<IfsRmbDepositin> getDepositInRmbMineListMine(Map<String, Object> params,
                                                    RowBounds rowBounds);

    Page<IfsRmbDepositin> selectAdjustDepositPage(Map<String, Object> params,RowBounds rowBounds);

}