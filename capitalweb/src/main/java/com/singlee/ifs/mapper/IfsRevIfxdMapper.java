package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsRevIfxd;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsRevIfxdMapper{
	// 新增
	public void insert(IfsRevIfxd entity);

	// 修改
	void updateById(IfsRevIfxd entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	Page<IfsRevIfxd> searchPageForexParam(Map<String, Object> map,RowBounds rb);
	
	//获取id
	String getId();
	
	//++++++++++++++++++++++++++++++++++++++++++++++++/
	/***
	 * 我发起的
	 */
	Page<IfsRevIfxd> getRevIfxdMine(Map<String, Object> params,RowBounds rowBounds);
	/***
	 * 待审批
	 */
	Page<IfsRevIfxd> getRevIfxdUnfinished(Map<String, Object> params,RowBounds rowBounds);
	/***
	 * 已审批
	 */
	Page<IfsRevIfxd> getRevIfxdFinished(Map<String, Object> params,RowBounds rowBounds);
	
	
	/***
	 * 根据id修改  审批状态 、审批发起时间 
	 */
	void updateIfxdStatusByID(String serial_no, String status,String date);
	
	/***
	 * 根据id查询  实体类
	 */
	IfsRevIfxd searchById(Map<String, Object> map);

	IfsRevIfxd getOneById(String ticketId);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}