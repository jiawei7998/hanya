package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsLimitTotal;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsLimitTotalMapper extends Mapper<IfsLimitTotal> {
    Page<IfsLimitTotal> searchPageLimit(Map<String, Object> map, RowBounds rb);

    void deleteByLimitId(String limitId);
}
