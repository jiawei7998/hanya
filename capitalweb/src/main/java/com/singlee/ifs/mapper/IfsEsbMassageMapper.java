package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsEsbMassage;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface IfsEsbMassageMapper extends Mapper<IfsEsbMassage>{
	
    
    List<IfsEsbMassage> getEsbMassage(Map<String, Object> map);
}
