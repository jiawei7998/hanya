package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxFwd;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxFwdMapper {

	public Page<IfsApprovefxFwd> getCreditEdit(Map<String, Object> map,RowBounds rowBounds);

	public IfsApprovefxFwd getCredit(@Param("ticketId")String ticketid);

	public void addCredit(IfsApprovefxFwd map);

	public void editCredit(IfsApprovefxFwd map);

	public void deleteCredit(@Param("ticketId")String ticketId);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	/**
	 * 根据请求参数查询分页列表    我发起的
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxFwd> getFxFwdMineList(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据请求参数查询分页列表    已审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxFwd> getFxFwdMineListFinished(Map<String, Object> params, RowBounds rb);

	/**
	 * 根据请求参数查询分页列表    待审批
	 * 
	 * @param params - 请求参数
	 * @param rb - 分页对象
	 * @return 外汇掉期  实体对象
	 * @author 
	 * @date 2018-3-22
	 */
	public Page<IfsApprovefxFwd> getFxFwdMineListMine(Map<String, Object> params, RowBounds rb);

	public void updateCreditByID(String serial_no, String status,String date);
	
	IfsApprovefxFwd selectFlowIdByPrimaryKey(Map<String,Object>  key);

	public Page<IfsApprovefxFwd> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
	
   
}