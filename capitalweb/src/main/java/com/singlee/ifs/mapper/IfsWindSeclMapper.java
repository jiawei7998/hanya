package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindSeclBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface IfsWindSeclMapper extends Mapper<IfsWindSeclBean>{

    // 分页查询
    Page<IfsWindSeclBean> selectIfsWindSeclList(Map<String, Object> map, RowBounds rb);

    List<IfsWindSeclBean> selectIfsWindSeclLst(Map<String, Object> map);
}
