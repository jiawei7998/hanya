package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsRate;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;
/***
 * 利率代码
 * @author lij
 *
 */
public interface IfsOpicsRateMapper {
	//新增
	void insert(IfsOpicsRate entity);
	
	//修改
	void updateById(IfsOpicsRate entity);
	
	//删除
	void deleteById(Map<String,Object> map);
	
	//分页查询
	Page<IfsOpicsRate> searchPageOpicsRate(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsRate searchById(Map<String,Object> map);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsRate entity);
	
	//根据id   更新同步状态
	void updateStatus0ById(Map<String,Object> map);
	
	
	//查询所有的利率代码
	List<IfsOpicsRate> searchAllOpicsRate();

}
