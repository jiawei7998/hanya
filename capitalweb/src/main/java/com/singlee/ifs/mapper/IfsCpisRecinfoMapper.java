package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import org.apache.ibatis.annotations.Param;

public interface IfsCpisRecinfoMapper {
    int insert(RecCPISFeedBackModel record);

    int insertSelective(RecCPISFeedBackModel record);

    int getRecinfoCount(RecCPISFeedBackModel record);
    
    int updateByExecId(RecCPISFeedBackModel record);

	String getConfirm(@Param(value = "exttradeid")String exttradeid);
}