package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.choistrd.model.CustRecieve;
import com.singlee.capital.choistrd.model.Secm;
import com.singlee.capital.choistrd.model.SlNupdStatus;
import com.singlee.ifs.model.IfsBondPledgeBean;
import com.singlee.ifs.model.SecmStatus;

import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
public interface IfsBondPledgeMapper {

    Page<IfsBondPledgeBean> selectBondPledgeList(Map<String,Object> map, RowBounds rb);

    IfsBondPledgeBean selectBondPledgeByPrimaryKey(Map<String,Object> map);

    int insert(IfsBondPledgeBean record);

    int updateByPrimaryKey(IfsBondPledgeBean record);

    int deleteByPrimaryKey(IfsBondPledgeBean record);
    
	void deleteSecmStatus(String secid, String tableName);

	void insertSecmStatus(SecmStatus secmStatus);

	SecmStatus querySecmStatusByDealnoSeq(String secid);
	
	void updateSecmStatusByDealnoSeq(SecmStatus secmStatus);

	void insertCust(CustRecieve custRecieve);

	List<Secm> queryAllSecm(Map<String, Object> hashMap);

	String queryBrps();

	List<SlNupdStatus> queryAllSlNupdStatus();

	void updateSlNupdStatusByDealNo(SlNupdStatus nupdStatus);
}
