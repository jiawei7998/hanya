package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsWdSecissuer;

import java.util.List;

public interface IfsWdSecissuerMapper {
	// 新增
	void insert(IfsWdSecissuer entity);
	//查询
	List<IfsWdSecissuer> queryByVDate(String vDate);
	// 删除
	void delete();
}
