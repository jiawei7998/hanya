package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsTradeParaGroup;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/***
 * 交易要素mapper
 * @author singlee4
 *
 */
public interface IfsTradeElementParamMapper {
	
	//新增
	void insert(IfsTradeParaGroup entity);
	
	//修改
	void updateById(IfsTradeParaGroup entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsTradeParaGroup> searchPageOpicsTrdParam(Map<String,Object> map,RowBounds rb);
	

}
