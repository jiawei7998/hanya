package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsSwiftSettleInfo;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsSwiftSettleInfoMapper {

	/**
	 * 获取报文清算信息
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<IfsSwiftSettleInfo> getSetleInfo(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 将返回清算报文保存本地库的IFS_SWIFT_SettleInfo表中
	 */
	void insertSwiftSettle(IfsSwiftSettleInfo bean);
}