package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFlowCompleted;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;



public interface IfsFlowCompletedMapper {
		//新增
		void insert(Map<String,Object> map);
		
		//根据 id 查询实体对象
		IfsFlowCompleted getEntityById(@Param("ticketId") String ticketId);
		
		//分页查询-查询 审批完成 的记录
		Page<IfsFlowCompleted> searchPageFlowCompleted(Map<String,Object> map,RowBounds rb);
		Page<IfsFlowCompleted> searchPageFlowCompletedDate(Map<String,Object> map,RowBounds rb);
}
