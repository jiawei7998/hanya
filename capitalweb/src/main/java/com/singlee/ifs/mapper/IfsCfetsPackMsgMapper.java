package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public interface IfsCfetsPackMsgMapper {

	// 保存CFETS下行过来文件
	public void addcfetsPackage(CfetsPackMsgBean cfetsPackMsg);

	// 根据ExecId 获取报文
	public CfetsPackMsgBean getcfetsPackageById(CfetsPackMsgBean cfetsPackMsg);

	// 分页查询
	public Page<CfetsPackMsgBean> searchPagecfetsPackage(Map<String, Object> map, RowBounds rb);
}
