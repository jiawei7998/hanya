package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsFlowMonitor;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 审批监控表 mapper 主要用于首页工作台
 * 
 * @author lijing
 * 
 */
public interface IfsFlowMonitorMapper {

	// 新增
	void insert(IfsFlowMonitor entity);

	// 修改
	void updateById(IfsFlowMonitor entity);

	// 删除
	void deleteById(@Param("ticketId") String id);

	// 分页查询-查询 审批中 的记录
	Page<IfsFlowMonitor> searchPageFlowMonitor(Map<String, Object> map, RowBounds rb);
	
	// 分页查询-查询 审批中 的记录
	Page<IfsFlowMonitor> searchPageAllFlowMonitor(Map<String, Object> map, RowBounds rb);

	// 根据 id 查询实体对象
	IfsFlowMonitor getEntityById(@Param("ticketId") String ticketId);

	// 列表查询-查询 审批中 的记录
	List<IfsFlowMonitor> searchListFlowMonitor(Map<String, String> map);
	List<IfsFlowMonitor> searchPageAllFlowMonitor(Map<String, Object> map);
	// 根据 id 查询prdNo
	IfsFlowMonitor getPrdNoById(@Param("ticketId") String ticketId);
	
	//根据交易单号获取该笔交易信息
	List<IfsFlowMonitor> searchListByDealno(Map<String, Object> map);
}




