package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsBondCheckLog;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

/***
 * 交易要素复核日志mapper
 * @author singlee4
 *
 */
public interface IfsBondCheckLogMapper {
	
	//新增
	void insert(IfsBondCheckLog entity);

	//分页查询
	Page<IfsBondCheckLog> searchPageCheckLog(Map<String,Object> map,RowBounds rb);
	

}
