package com.singlee.ifs.mapper;

import com.singlee.ifs.model.IfsCpisRecduenot;

import java.util.List;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/12 18:40
 * @description：${description}
 * @modified By：
 * @version:     
 */
public interface IfsCpisRecduenotMapper {
    int insert(IfsCpisRecduenot record);

    int insertSelective(IfsCpisRecduenot record);

    List<IfsCpisRecduenot> selectByAll(IfsCpisRecduenot ifsCpisRecduenot);


}