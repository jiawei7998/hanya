package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindRateBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindRateMapper extends Mapper<IfsWindRateBean>{
	
    // 分页查询
    Page<IfsWindRateBean> selectIfsWindRateList(Map<String, Object> map, RowBounds rb);

    
}