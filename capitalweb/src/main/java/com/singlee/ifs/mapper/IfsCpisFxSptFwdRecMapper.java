package com.singlee.ifs.mapper;

import com.singlee.financial.cfets.bean.RecForeignForwardSpotModel;

public interface IfsCpisFxSptFwdRecMapper {

    int insertSelective(RecForeignForwardSpotModel record);

    int getRecSptFwdCount(RecForeignForwardSpotModel record);
    
    int updateByConfirmId(RecForeignForwardSpotModel record);
}