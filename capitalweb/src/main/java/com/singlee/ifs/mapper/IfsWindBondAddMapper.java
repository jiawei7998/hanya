package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsWindBondAddBean;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface IfsWindBondAddMapper extends Mapper<IfsWindBondAddBean> {

	// 分页查询
	Page<IfsWindBondAddBean> selectIfsWindBondAddList(Map<String, Object> map, RowBounds rb);

}