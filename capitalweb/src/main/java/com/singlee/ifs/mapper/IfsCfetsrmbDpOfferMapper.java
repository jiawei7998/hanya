package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbDpOffer;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbDpOfferMapper {

	int insert(IfsCfetsrmbDpOffer record);
	
	void deleteByTicketId(@Param("ticketId")String ticketId);
	
	Page<IfsCfetsrmbDpOffer> searchOfferDpDetails(Map<String, Object> params,
			RowBounds rowBounds);
	
	public List<IfsCfetsrmbDpOffer> searchOfferList(@Param(value = "ticketId") String ticketId);
	
	/**
	 * 插入opics成功回填statcode
	 * @param map
	 */
	void updateStatcodeByTicketId(Map<String, Object> map);
	
	/**
	 * 状态回查使用
	 * @return
	 */
	List<IfsCfetsrmbDpOffer> getCfetsrmbDpOfferList();
	
	void  updateCfetsrmbDpOfferByFedealno(Map<String,Object> map);
}
