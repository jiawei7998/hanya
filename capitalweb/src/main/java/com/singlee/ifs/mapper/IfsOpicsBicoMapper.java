package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsBico;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/***
 * 工业标准码
 * @author lij
 *
 */
public interface IfsOpicsBicoMapper {
	//新增
	void insert(IfsOpicsBico entity);
	
	//修改
	void updateById(IfsOpicsBico entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsBico> searchPageOpicsBico(Map<String,Object> map,RowBounds rb);
	
	//根据id查询实体类
	IfsOpicsBico searchById(String id);
	
	//根据实体   更新同步状态
	void updateStatus(IfsOpicsBico entity);
	
	//根据id   更新同步状态为0(未同步)
	void updateStatus0ById(String id);
	
	//查询所有的工业标准码
	List<IfsOpicsBico>  searchAllOpicsBico();

}
