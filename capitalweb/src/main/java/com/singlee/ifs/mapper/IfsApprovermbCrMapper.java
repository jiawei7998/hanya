package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovermbCr;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;

public interface IfsApprovermbCrMapper {
    int deleteByPrimaryKey(IfsApprovermbCr key);

    int insert(IfsApprovermbCr record);

    int insertSelective(IfsApprovermbCr record);

    IfsApprovermbCr selectByPrimaryKey(IfsApprovermbCr key);
    
    /**存储过程时 调用*/
    IfsApprovermbCr selectById(Map<String, String> map);

    int updateByPrimaryKeySelective(IfsApprovermbCr record);

    int updateByPrimaryKey(IfsApprovermbCr record);
    
    Page<IfsApprovermbCr> selectIfsCfetsrmbCrList(IfsApprovermbCr record);

	void updateRmbCrByID(String serial_no, String status,String date);

	Page<IfsApprovermbCr> getRmbCrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbCr> getRmbCrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsApprovermbCr> getRmbCrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);

	IfsApprovermbCr searchPleDge( @Param(value = "ticketId") String ticketId);

	IfsApprovermbCr selectFlowIdByPrimaryKey(Map<String, Object> key);
	
	//public List<IfsApprovermbCr> searchById(@Param("contractId")String contractId);

	/*Page<IfsCfetsrmbCr> getRmbCrMineList(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbCr> getRmbCrMineListMine(Map<String, Object> params,
			RowBounds rowBounds);*/
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	Page<IfsApprovermbCr> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);
}