package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsLimitCondition;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface IfsLimitConditionMapper {

	public Page<IfsLimitCondition> searchPageLimit(Map<String, Object> map,RowBounds rb);

	public void update(IfsLimitCondition entity);
	
	public void insert(IfsLimitCondition entity);

	public void deleteById(String id);


	public IfsLimitCondition searchIfsLimitCondition(String id);
	
	

	
	//20180517新增
	public void updateMap(Map<String, Object> map);
	
    //根据产品和交易员查出所有结果
	public List<Map<String, Object>> searchResult(Map<String, String> map);
	
	//20180531新增：查询交易员的总授信额度、总可用额度
	Page<List<Map<String, Object>>> searchTraderTotalAndAvlAmt(Map<String, Object> map,RowBounds rb);
	
	//20180531新增：查询交易员的分授信额度、分可用额度  
	Page<IfsLimitCondition>  searchTraderEdu(Map<String, Object> map,RowBounds rb);
	
	//查询美元汇率
	public String searchRate();
	
	
}
