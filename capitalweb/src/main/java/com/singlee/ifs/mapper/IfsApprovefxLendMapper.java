package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsApprovefxLend;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;


public interface IfsApprovefxLendMapper {

	public Page<IfsApprovefxLend> searchCcyLendingPage(Map<String, Object> map,RowBounds rowBounds);

	public void deleteCcyLending(@Param("ticketId")String ticketid);

	public void editCcyLending(IfsApprovefxLend map);

	public void addCcyLending(IfsApprovefxLend map);

	public IfsApprovefxLend searchCcyLending(@Param("ticketId")String ticketid);

	public void updatefxLendingByID(String serial_no, String status,String date);

	public Page<IfsApprovefxLend> getCcyLendingMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxLend> getCcyLendingMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxLend> getCcyLendingMineListMine(
			Map<String, Object> params, RowBounds rowBounds);

	public IfsApprovefxLend getCcyLendAndFlowIdById(Map<String, Object> map);
	public LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);

	public IfsApprovefxLend selectFlowByPrimaryKey(Map<String, Object> key);

	public Page<IfsApprovefxLend> searchApprovePassed(Map<String, Object> params,RowBounds rowBounds);

	/*public Page<IfsApprovefxLend> getCcyLendingMineList(Map<String, Object> params,
			RowBounds rowBounds);

	public Page<IfsApprovefxLend> getCcyLendingMineListFinished(
			Map<String, Object> params, RowBounds rowBounds);

	public Page<IfsApprovefxLend> getCcyLendingMineListMine(
			Map<String, Object> params, RowBounds rowBounds);*/
   
}