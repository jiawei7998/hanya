package com.singlee.ifs.mapper;
import com.github.pagehelper.Page;
import com.singlee.ifs.model.TdQryMsg;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TdQryMsgMapper{

	/**
	 * 根据条件查询报文反馈信息
	 * @param params
	 * @return
	 */
	public Page<TdQryMsg> searchQryMsg(Map<String, Object> params,RowBounds rowBounds);
	
	public List<TdQryMsg> searchQryMsg(Map<String, Object> params);
	
	public void insert(Map<String, Object> params);
	
	public void update(Map<String, Object> params);
	public void updateExecType(Map<String, Object> params);
	public Page<TdQryMsg> searchNoSendDeal(Map<String, Object> params,RowBounds rowBounds);

	void updateSendState(Map<String, Object> msgMap);
}
