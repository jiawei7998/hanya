package com.singlee.ifs.mapper;

import com.singlee.financial.model.SendForeignForwardSpotModel;
import com.singlee.financial.model.SendForeignFxmmModel;
import com.singlee.financial.model.SendForeignSwapModel;
import com.singlee.financial.model.SendIRSModel;

import java.util.List;
import java.util.Map;


public interface CPISMessageMapper {
	
	public List<SendForeignFxmmModel> searchForeignFxmmMessage(Map<String, Object> params);
	
	public List<SendForeignSwapModel> searchForeignSwapMessage(Map<String, Object> params);
	
	public List<SendForeignForwardSpotModel> searchForeignSptMessage(Map<String, Object> params);
	public List<SendForeignForwardSpotModel> searchForeignFwdMessage(Map<String, Object> params);
	
	public List<SendForeignSwapModel> searchForeignSwapForwardMessage(Map<String, Object> params);
	
	public List<SendIRSModel> searchIRSMessage();
	
	public String getLegCouponPaymentDate(Map<String, Object> params);
	
	public String getLegInterestAccrualDate(Map<String, Object> params);
	
	public String getCurrentDate();
	
	public void updateSettleInfo(String confirmID);

}
