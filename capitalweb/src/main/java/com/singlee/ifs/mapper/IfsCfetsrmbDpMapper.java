package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsCfetsrmbDp;
import com.singlee.ifs.model.IfsCfetsrmbDpKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface IfsCfetsrmbDpMapper {

	Page<IfsCfetsrmbDp> getRmbDpMineList(Map<String, Object> params,
			RowBounds rowBounds);
	
	Page<IfsCfetsrmbDp> getRmbDpMineListFinished(Map<String, Object> params,
			RowBounds rowBounds);

	Page<IfsCfetsrmbDp> getRmbDpMineListMine(Map<String, Object> params,
			RowBounds rowBounds);
	
	public List<IfsCfetsrmbDp> searchById(@Param("contractId")String contractId);
	
	IfsCfetsrmbDp selectById(Map<String, String> map);
	
	int insert(IfsCfetsrmbDp record);
	
	IfsCfetsrmbDp selectByPrimaryKey(IfsCfetsrmbDpKey key);
	
	Page<IfsCfetsrmbDp> selectIfsCfetsrmbDpList(IfsCfetsrmbDp record);
	
	int deleteByPrimaryKey(IfsCfetsrmbDpKey key);
	
	int updateByPrimaryKey(IfsCfetsrmbDp record);
	
	void updateApproveStatusRmbDpByID(String ticketId);
	
	void updateRmbDpByID(String serial_no, String status,String date);
	
	IfsCfetsrmbDp searchBondLon(@Param(value = "ticketId") String ticketId);
	
	LinkedHashMap<String, Object> searchProperty(@Param("sqll")String sqll);
	
	IfsCfetsrmbDp selectFlowIdByPrimaryKey(Map<String, Object> key);

	IfsCfetsrmbDp searchCfetsrmbDpByTicketId(Map<String, Object> map);
	
	/**
	 * 总交易查询（债券发行）
	 * @param map
	 */
	Page<IfsCfetsrmbDp> searchTotalDealZQFX(Map<String, Object> params,RowBounds rowBounds);
}
