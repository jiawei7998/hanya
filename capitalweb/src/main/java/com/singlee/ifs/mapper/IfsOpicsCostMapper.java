package com.singlee.ifs.mapper;

import com.github.pagehelper.Page;
import com.singlee.ifs.model.IfsOpicsCost;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 
 * opics 系统成本中心表 mapper
 *
 */
public interface IfsOpicsCostMapper {
	
	//新增
	void insert(IfsOpicsCost entity);
	
	//修改
	void updateById(IfsOpicsCost entity);
	
	//删除
	void deleteById(String id);
	
	//分页查询
	Page<IfsOpicsCost> searchPageOpicsCost(Map<String,Object> map,RowBounds rb);
	
	List<IfsOpicsCost> searchCost(HashMap<String, Object> map);
	
	IfsOpicsCost searchById(String id);
	
	void updateStatus(IfsOpicsCost entity);

	// 根据id 更新同步状态
	void updateStatus0ById(String id);

	List<IfsOpicsCost> searchAllOpicsCost();
	
}
