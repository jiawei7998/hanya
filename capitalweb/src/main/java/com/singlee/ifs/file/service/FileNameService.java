package com.singlee.ifs.file.service;

import com.singlee.ifs.file.entity.FileName;

import java.util.List;

/**
 * (FileName)表服务接口
 *
 * @author makejava
 * @since 2021-10-20 19:47:58
 */
public interface FileNameService {

    /**
     * 通过ID查询单条数据
     *
     * @param  主键
     * @return 实例对象
     */
    FileName queryById( );

    /**
     * 查询多条数据
     *
     * @return 对象列表
     */
    List<String> queryAllByLimit();

    /**
     * 新增数据
     *
     * @param fileName 实例对象
     * @return 实例对象
     */
    FileName insert(FileName fileName);

    /**
     * 修改数据
     *
     * @param fileName 实例对象
     * @return 实例对象
     */
    FileName update(FileName fileName);

    /**
     * 通过主键删除数据
     *
     * @param  主键
     * @return 是否成功
     */
    boolean deleteById( );

}