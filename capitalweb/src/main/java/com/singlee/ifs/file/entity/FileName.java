package com.singlee.ifs.file.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * (FileName)实体类
 *
 * @author makejava
 * @since 2021-10-20 19:47:44
 */
@Entity
@Table(name="FILE_NAME")
public class FileName implements Serializable {
    private static final long serialVersionUID = -62839327430208576L;
    /**
    * 
    */
    @Id
    private String filename;
    
    private String inputdate;


    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Object getInputdate() {
        return inputdate;
    }

    public void setInputdate(String inputdate) {
        this.inputdate = inputdate;
    }

}