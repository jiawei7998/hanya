package com.singlee.ifs.file.controller;

import com.singlee.ifs.file.service.FileNameService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (FileName)表控制层
 *
 * @author makejava
 * @since 2021-10-20 19:48:01
 */
@RestController
@RequestMapping("fileName")
public class FileNameController {
    /**
     * 服务对象
     */
    @Resource
    private FileNameService fileNameService;

//    /**
//     * 通过主键查询单条数据
//     *
//     * @param id 主键
//     * @return 单条数据
//     */
//    @GetMapping("selectOne")
//    public FileName selectOne() {
//        return this.fileNameService.queryById(id);
//    }

}