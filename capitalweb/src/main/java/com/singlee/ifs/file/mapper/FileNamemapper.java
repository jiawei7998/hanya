package com.singlee.ifs.file.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.ifs.file.entity.FileName;

import java.util.List;

/**
 * (FileName)表数据库访问层
 *s
 * @author makejava
 * @since 2021-10-20 19:48:01
 */
public interface FileNamemapper extends MyMapper<FileName> {

    /**
     * 通过ID查询单条数据
     *
     * @param
     * @return 实例对象
     */
    FileName queryById( );

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<String> queryAllByLimit();


    /**
     * 通过实体作为筛选条件查询
     *
     * @param fileName 实例对象
     * @return 对象列表
     */
    List<FileName> queryAll(FileName fileName);

    /**
     * 新增数据
     *
     * @param fileName 实例对象
     * @return 影响行数
     */
    void insertall(FileName fileName);

    /**
     * 修改数据
     *
     * @param fileName 实例对象
     * @return 影响行数
     */
    int update(FileName fileName);

    /**
     * 通过主键删除数据
     *
     * @param  主键
     * @return 影响行数
     */
    int deleteById( );

}