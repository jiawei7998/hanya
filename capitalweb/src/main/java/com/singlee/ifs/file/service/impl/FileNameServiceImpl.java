package com.singlee.ifs.file.service.impl;

import com.singlee.ifs.file.entity.FileName;
import com.singlee.ifs.file.mapper.FileNamemapper;
import com.singlee.ifs.file.service.FileNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (FileName)表服务实现类
 *
 * @author makejava
 * @since 2021-10-20 19:48:00
 */
@Service
public class FileNameServiceImpl implements FileNameService {
    @Autowired
    private FileNamemapper fileNamemapper;

    /**
     * 通过ID查询单条数据
     *
     * @return 实例对象
     */
    @Override
    public FileName queryById( ) {
        return this.fileNamemapper.queryById();
    }

    /**
     * 查询多条数据
     *
     * @return 对象列表
     */
    @Override
    public List<String> queryAllByLimit() {

        return fileNamemapper.queryAllByLimit();
    }

    /**
     * 新增数据
     *
     * @param fileName 实例对象
     * @return 实例对象
     */
    @Override
    public FileName insert(FileName fileName) {
        this.fileNamemapper.insert(fileName);
        return fileName;
    }

    @Override
    public FileName update(FileName fileName) {
        return null;
    }

    @Override
    public boolean deleteById() {
        return false;
    }


}