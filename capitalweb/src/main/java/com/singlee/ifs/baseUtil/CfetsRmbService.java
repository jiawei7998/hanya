package com.singlee.ifs.baseUtil;

import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.model.IfsBaseFlow;
import com.singlee.ifs.model.IfsOpicsCust;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "CfetsRmbService")
public class CfetsRmbService {

	@Autowired
	private TaUserMapper taUserMapper;
	@Resource
	IFSMapper ifsMapper;
	@Autowired
	private IfsOpicsCustMapper ifsOpicsCustMapper;

	// private static Logger logger =
	// LoggerFactory.getLogger(CfetsRmbService.class);

	/**
	 * 根据CFETS接口用户对应TA_USER.APPROVE_USER_ID字段对应OPICS用户id和机构 适用于本外币 本币：交易员中文名称
	 * 外币：交易员账号
	 */
	public TaUser userMapping(IfsBaseFlow base, String traderId) {
		TaUser user = taUserMapper.getUserByCfetsTrad(traderId);
		if (null == user) {
			base.setSponsor(SystemProperties.cfetsDefautUserId);
			base.setSponInst(SystemProperties.cfetsDefautUserInstId);
			return null;
		}
		base.setSponsor(user.getUserId());
		base.setSponInst(user.getInstId());
		return user;
	}

	/**
	 * 下行交易流水号
	 * 
	 * @param str
	 * @return
	 */
	public String getTicketId(String str) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("sStr", "");// 前缀
		hashMap.put("trdType", "2");// 贸易类型，正式交易
		// CSWAP20170831339000707
		String ticketId = ifsMapper.getTradeId(hashMap);
		return ticketId;
	}

	/**
	 * 下行交易交易对手转换 本币：6 位机构码 外币：21位机构码
	 * cfetsno 本币：6 位机构码
	 * cfetscn 外币：21位机构码
	 * @return
	 */
	public String getLocalInstitution(IfsBaseFlow base, String cfetsno,String cfetscn) {
		if(StringUtil.isEmptyString(cfetsno) && StringUtil.isEmptyString(cfetscn)) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cfetsno", cfetsno);
		map.put("cfetscn", cfetscn);
		List<IfsOpicsCust> list = ifsOpicsCustMapper.searchByCfets(map);
		if (list.size() == 1) {
			// 授信占用主体
			IfsOpicsCust cust = list.get(0);
			base.setCustNo(cust.getCreditsub());
			return cust.getCno();
		}else{
			return StringUtil.isEmptyString(cfetsno) ? cfetscn : cfetsno;
		}
	}

}
