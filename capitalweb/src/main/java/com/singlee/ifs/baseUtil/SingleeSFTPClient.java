package com.singlee.ifs.baseUtil;

import com.jcraft.jsch.*;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * sftp工具类
 *
 * @author shenzl
 * @date 2019/02/23
 */
public class SingleeSFTPClient {

    private static Logger log = LoggerFactory.getLogger(SingleeSFTPClient.class);

    private String host;// 服务器连接ip
    private String username;// 用户名
    private String password;// 密码
    private int port = 22;// 端口号
    private ChannelSftp sftp = null;
    private Session sshSession = null;

    public SingleeSFTPClient() {
    }

    public SingleeSFTPClient(String host, int port, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.port = port;
    }

    public SingleeSFTPClient(String host, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
    }

    /**
     * 通过SFTP连接服务器
     */
    public void connect() {
        try {
            com.jcraft.jsch.Logger logger = new com.jcraft.jsch.Logger() {
                @Override
                public boolean isEnabled(int i) {
                    // 开启、关闭调试
                    return true;
                }

                @Override
                public void log(int i, String s) {
                    // 打印日志
                    log.info(s);
                }
            };
            JSch.setLogger(logger);
            JSch jsch = new JSch();
            jsch.getSession(username, host, port);
            sshSession = jsch.getSession(username, host, port);
            log.info("Session created.");
            sshSession.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            log.error(sshSession.toString());
            sshSession.connect();
            log.info("Session connected.");
            Channel channel = sshSession.openChannel("sftp");
            channel.connect();
           
            log.info("Opening Channel.");
            sftp = (ChannelSftp) channel;
            log.info("Connected to " + host + ".");
        } catch (Exception e) {
            log.error("Sftp连接失败Exception" + e);
        }
    }

    /**
     * 关闭连接
     */
    public void disconnect() {
        if (this.sftp != null) {
            if (this.sftp.isConnected()) {
                this.sftp.disconnect();
                log.info("sftp is closed already");
            }
        }
        if (this.sshSession != null) {
            if (this.sshSession.isConnected()) {
                this.sshSession.disconnect();
                log.info("sshSession is closed already");
            }
        }
    }

    /**
     * 批量下载文件
     *
     * @param remotePath：远程下载目录(以路径符号结束,可以为相对路径eg:/sftp/2014/)
     * @param localPath：本地保存目录(以路径符号结束,D:\sftp\)
     * @param fileFormat：下载文件格式(以特定字符开头,为空不做检验)
     * @param fileEndFormat：下载文件格式(文件格式)
     * @param del：下载后是否删除sftp文件
     * @return
     */
    public List<String> batchDownLoadFile(String remotePath, String localPath, String fileFormat, String fileEndFormat,
                                          boolean del) {
        List<String> filenames = new ArrayList<String>();
        try {
            // connect()
            //屏蔽父级目录的文件信息
            Vector<?> v = listFiles(remotePath + "*");
            // sftp.cd(remotePath);
            if (v.size() > 0) {
                log.info("本次处理文件个数不为零,开始下载...fileSize=" + v.size());
                Iterator<?> it = v.iterator();
                while (it.hasNext()) {
                    LsEntry entry = (LsEntry) it.next();
                    String filename = entry.getFilename();
                    SftpATTRS attrs = entry.getAttrs();
                    if (!attrs.isDir()) {
                        boolean flag = false;
                        String localFileName = localPath + filename;
                        fileFormat = fileFormat == null ? "" : fileFormat.trim();
                        fileEndFormat = fileEndFormat == null ? "" : fileEndFormat.trim();
                        // 三种情况
                        if (fileFormat.length() > 0 && fileEndFormat.length() > 0) {
                            if (filename.startsWith(fileFormat) && filename.endsWith(fileEndFormat)) {
                                flag = downloadFile(remotePath, filename, localPath, filename);
                                if (flag) {
                                    filenames.add(localFileName);
                                    if (flag && del) {
                                        deleteSFTP(remotePath, filename);
                                    }
                                }
                            }
                        } else if (fileFormat.length() > 0 && "".equals(fileEndFormat)) {
                            if (filename.startsWith(fileFormat)) {
                                flag = downloadFile(remotePath, filename, localPath, filename);
                                if (flag) {
                                    filenames.add(localFileName);
                                    if (flag && del) {
                                        deleteSFTP(remotePath, filename);
                                    }
                                }
                            }
                        } else if (fileEndFormat.length() > 0 && "".equals(fileFormat)) {
                            if (filename.endsWith(fileEndFormat)) {
                                flag = downloadFile(remotePath, filename, localPath, filename);
                                if (flag) {
                                    filenames.add(localFileName);
                                    if (flag && del) {
                                        deleteSFTP(remotePath, filename);
                                    }
                                }
                            }
                        } else {
                            flag = downloadFile(remotePath, filename, localPath, filename);
                            if (flag) {
                                filenames.add(localFileName);
                                /*if (flag && del) {
                                    deleteSFTP(remotePath, filename);
                                }*/
                            }
                        }
                    }
                }
            }
            log.info("download file is success:remotePath=" + remotePath + "and localPath=" + localPath
                    + ",file size is" + v.size());
        } catch (SftpException e) {
            log.error("batchDownLoadFile SftpException" + e);
        } finally {
            this.disconnect();
        }
        return filenames;
    }

    /**
     * 下载单个文件
     *
     * @param remotePath：远程下载目录(以路径符号结束)
     * @param remoteFileName：下载文件名
     * @param localPath：本地保存目录(以路径符号结束)
     * @param localFileName：保存文件名
     * @return
     */
    public boolean downloadFile(String remotePath, String remoteFileName, String localPath, String localFileName) {
        FileOutputStream fieloutput = null;
        try {
            // sftp.cd(remotePath);
            log.info("本地下载目录--" + localPath + localFileName);
            File file = new File(localPath + localFileName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            // mkdirs(localPath + localFileName);
            fieloutput = new FileOutputStream(file);
            sftp.get(remotePath + remoteFileName, fieloutput);
            log.info("===DownloadFile:" + remotePath + ":" + remoteFileName + " success from sftp.");
            return true;
        } catch (FileNotFoundException e) {
            log.error("downloadFile FileNotFoundException" + e);
        } catch (SftpException e) {
            log.error("downloadFile SftpException" + e);
        } finally {
            if (null != fieloutput) {
                try {
                    fieloutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * 上传单个文件
     *
     * @param remotePath：远程保存目录
     * @param remoteFileName：保存文件名
     * @param localPath：本地上传目录(以路径符号结束)
     * @param localFileName：上传的文件名
     * @return
     */
    public boolean uploadFile(String remotePath, String remoteFileName, String localPath, String localFileName) {
        FileInputStream in = null;
        try {
            createDir(remotePath);
            File file = new File(localPath + localFileName);
            in = new FileInputStream(file);
            sftp.put(in, remoteFileName);
            return true;
        } catch (FileNotFoundException e) {
            log.error("uploadFile FileNotFoundException" + e);
        } catch (SftpException e) {
            log.error("uploadFile SftpException" + e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * 批量上传文件
     *
     * @param remotePath：远程保存目录
     * @param localPath：本地上传目录(以路径符号结束)
     * @param del：上传后是否删除本地文件
     * @return
     */
    public boolean bacthUploadFile(String remotePath, String localPath, boolean del) {
        try {
            connect();
            File file = new File(localPath);
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile() && files[i].getName().indexOf("bak") == -1) {
                    if (this.uploadFile(remotePath, files[i].getName(), localPath, files[i].getName()) && del) {
                        deleteFile(localPath + files[i].getName());
                    }
                }
            }
            log.info("upload file is success:remotePath=" + remotePath + "and localPath=" + localPath
                    + ",file size is " + files.length);
            return true;
        } catch (Exception e) {
            log.error("uploadFile Exception" + e);
        } finally {
            this.disconnect();
        }
        return false;

    }

    /**
     * 删除本地文件
     *
     * @param filePath
     * @return
     */
    public boolean deleteFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }

        if (!file.isFile()) {
            return false;
        }
        boolean rs = file.delete();
        if (rs) {
            log.info("delete file success from local.");
        }
        return rs;
    }

    /**
     * 创建目录
     *
     * @param createpath
     * @return
     */
    public boolean createDir(String createpath) {
        try {
            if (isDirExist(createpath)) {
                this.sftp.cd(createpath);
                return true;
            }
            String[] pathArry = createpath.split("/");
            StringBuffer filePath = new StringBuffer("/");
            for (String path : pathArry) {
                if ("".equals(path)) {
                    continue;
                }
                filePath.append(path + "/");
                if (isDirExist(filePath.toString())) {
                    sftp.cd(filePath.toString());
                } else {
                    // 建立目录
                    sftp.mkdir(filePath.toString());
                    // 进入并设置为当前目录
                    sftp.cd(filePath.toString());
                }

            }
            this.sftp.cd(createpath);
            return true;
        } catch (SftpException e) {
            log.error("uploadFile SftpException" + e);
        }
        return false;
    }

    /**
     * 判断目录是否存在
     *
     * @param directory
     * @return
     */
    public boolean isDirExist(String directory) {
        boolean isDirExistFlag = false;
        try {
            SftpATTRS sftpATTRS = sftp.lstat(directory);
            isDirExistFlag = true;
            return sftpATTRS.isDir();
        } catch (Exception e) {
            if ("no such file".equals(e.getMessage().toLowerCase())) {
                isDirExistFlag = false;
            }
        }
        return isDirExistFlag;
    }

    /**
     * 删除stfp文件
     *
     * @param directory：要删除文件所在目录
     * @param deleteFile：要删除的文件
     */
    public void deleteSFTP(String directory, String deleteFile) {
        try {
            // sftp.cd(directory);
            sftp.rm(directory + deleteFile);
            log.info("delete file success from sftp.");
        } catch (Exception e) {
            log.error("deleteSFTP Exception" + e);
        }
    }

    /**
     * 如果目录不存在就创建目录
     *
     * @param path
     */
    public void mkdirs(String path) {
        File f = new File(path);

        String fs = f.getParent();

        f = new File(fs);

        if (!f.exists()) {
            f.mkdirs();
        }
    }

    /**
     * 列出目录下的文件
     *
     * @param directory：要列出的目录
     * @return
     * @throws SftpException
     */
    public Vector<?> listFiles(String directory) throws SftpException {
        return sftp.ls(directory);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ChannelSftp getSftp() {
        return sftp;
    }

    public void setSftp(ChannelSftp sftp) {
        this.sftp = sftp;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        SingleeSFTPClient sftp = null;
//        String localPath = "/appdata/was/interface/wind/20211011/";
//        String sftpPath = "/Download/";
        try {
            sftp = new SingleeSFTPClient("130.1.10.10", 22, "qindejin@was@130.1.14.66", "1234Abcd");
            sftp.connect();
            List<String> list = sftp.batchDownLoadFile("/appdata/was/interface/windFund/ChinaMutualFundNAV/", "D:\\ChinaMutualFundNAV\\", null, ".gz", false);
            for (String str : list) {
                System.out.println(str);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sftp.disconnect();
        }
    }



    /**
     * 下载单个文件（根本文件名模糊下载）
     * chenguo  11-18
     *  remotePath：远程下载目录(以路径符号结束)
     *  remoteFileName：下载文件名(不带文件格式)
     *  localPath：本地保存目录(以路径符号结束)
     * @return
     */
    public boolean downloadFile(Map<String ,Object> map) {
        String remotePath= (String) map.get("fPath");
        String remoteFileName=(String) map.get("fileName");
        String localPath=(String) map.get("filePath");

        FileOutputStream fieloutput = null;
        try {
            String all= String.valueOf(sftp.ls("*"));
            log.info("目录下的文件是--"  + all);
            sftp.cd(remotePath);
            String All= String.valueOf(sftp.ls("*"));
            log.info("目录下的文件是--"  + All);
            String realName= String.valueOf(sftp.ls(remoteFileName+"*"));
            int b=realName.lastIndexOf(" ");
            int c=realName.length();
            String fileName = realName.substring(b+1,c-1);
            map.put("fullName",fileName);
            log.info("--------------文件名为------------------" +fileName);
            log.info("本地下载目录--" + localPath + fileName);
            File file = new File(localPath + fileName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            fieloutput = new FileOutputStream(file);
            sftp.get(remotePath + fileName, fieloutput);
            log.info("===DownloadFile:" + remotePath + ":" + fileName + " success from sftp.");
            return true;
        } catch (FileNotFoundException e) {
            log.error("downloadFile FileNotFoundException" + e);
        } catch (SftpException e) {
            log.error("downloadFile SftpException" + e);
        } finally {
            if (null != fieloutput) {
                try {
                    fieloutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }










}
