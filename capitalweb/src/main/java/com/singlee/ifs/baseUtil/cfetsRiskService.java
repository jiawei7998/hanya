package com.singlee.ifs.baseUtil;

import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.financial.bean.SlCommonBean;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service(value = "cfetsRiskService")
public class cfetsRiskService {
	
	@Resource
	EdCustManangeService edCustManangeService;
	
	@Resource
	IfsOpicsBondMapper ifsOpicsBondMapper;
	
	Map<String, Object> map = new HashMap<String, Object>();
	Map<String, String> map2 = new HashMap<String, String>();
	Map<String, Object> riskMap = new HashMap<String, Object>();
	
	//获取外汇即期权重，止损,偏离度
	public Map<String,Object> getRiskValue(IfsCfetsfxSpt spt) throws Exception{
		
		 String vDate = spt.getForDate();//交易日期
		 String mDate = spt.getValueDate();//起息日
		 String weight = "100";
		 if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			 weight = "100";
		 }else{
			 weight = edCustManangeService.getNowDateWeight(vDate,mDate,"411");//权重
		 } 
		 
		 map.put("padNo", "411");
		 String lossLimit = edCustManangeService.queryFxdSum(map);//止损
		 
		 BigDecimal price = spt.getPrice();//spot汇率
		 String currencyPair = spt.getCurrencyPair();//货币对
		 String paiceDeviation = "";//偏离度
		 map2.put("prdNo", "411");
		 map2.put("price", price+"");
		 map2.put("currencyPair", currencyPair);
		 if(StringUtil.isEmpty(price+"") || StringUtil.isEmpty(currencyPair)){
			 paiceDeviation = "";
		 }else{
			 paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
		 } 
		 
		 riskMap.put("weight", weight);
		 riskMap.put("lossLimit", lossLimit);
		 riskMap.put("paiceDeviation", paiceDeviation);
		 return riskMap;
		 
	}
	
	//获取外汇远期权重
	public Map<String,Object> getRiskValue(IfsCfetsfxFwd fwd) throws Exception{
		
		 String vDate = fwd.getForDate();//交易日期
		 String mDate = fwd.getValueDate();//起息日
		 String weight = "100";
		 if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			 weight = "100";
		 }else{
			 weight = edCustManangeService.getNowDateWeight(vDate,mDate,"391");//权重
		 }
		 riskMap.put("weight", weight);
		 return riskMap;
	}
	
	//获取外汇掉期权重
	public Map<String,Object> getRiskValue(IfsCfetsfxSwap swap) throws Exception{
		
		String vDate = swap.getForDate();//交易日期
		String mDate = swap.getFwdValuedate();//远端起息日
		String weight = "100";
		 if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			 weight = "100";
		 }else{
			 weight = edCustManangeService.getNowDateWeight(vDate,mDate,"432");//权重
		 }
		riskMap.put("weight", weight);
		return riskMap;
	}
	
	//获取外币拆借权重、偏离度
	public Map<String,Object> getRiskValue(IfsCfetsfxLend lend) throws Exception{
		
		String vDate = lend.getValueDate();//起息日期
		String mDate = lend.getMaturityDate();//到期结算日
		String weight = "100";
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			weight = edCustManangeService.getNowDateWeight(vDate,mDate,"438");//权重
		}
		
		BigDecimal rate = lend.getRate();//拆借利率
		String rateCode = lend.getRateCode();//利率代码
		String paiceDeviation = "";//偏离度
		map2.put("prdNo", "438");
		map2.put("rate", rate+"");
		map2.put("rateCode", rateCode);
		if(StringUtil.isEmpty(rate+"") || StringUtil.isEmpty(rateCode)){
			paiceDeviation = "";
		}else{
			paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
		} 
		 
		riskMap.put("weight", weight);
		riskMap.put("paiceDeviation", paiceDeviation);
		return riskMap;
	}
	
	
	//获取利率互换、结构衍生权重
	public Map<String,Object> getRiskValue(IfsCfetsrmbIrs irs) throws Exception{
		
		String vDate = irs.getStartDate();//起息日期
		String mDate = irs.getEndDate();//到期结算日
		
		String weight = "100";//权重
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			String prdFlag = irs.getPrdFlag();
			if("1".equals(prdFlag)){
				weight = edCustManangeService.getNowDateWeight(vDate,mDate,"400");//结构衍生权重
			}else{
				weight = edCustManangeService.getNowDateWeight(vDate,mDate,"438");//利率互换权重
			}
		}
		riskMap.put("weight", weight);
		return riskMap;
	}
	
	
	//获取买断式回购权重、偏离度
	public Map<String,Object> getRiskValue(IfsCfetsrmbOr or) throws Exception{
		
		String vDate = or.getForDate();//起息日期
		String mDate = or.getSecondSettlementDate();//到期结算日
		String weight = "100";//权重
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			weight = edCustManangeService.getNowDateWeight(vDate,mDate,"442");//权重
		}
		
		BigDecimal repoRate = or.getRepoRate();//回购利率
		String tradingProduct = or.getTradingProduct();//交易品种
		String paiceDeviation = "";//偏离度
		map2.put("prdNo", "442");
		map2.put("repoRate", repoRate+"");
		map2.put("tradingProduct", tradingProduct);
		if(StringUtil.isEmpty(repoRate+"") || StringUtil.isEmpty(tradingProduct)){
			paiceDeviation = "";
		}else{
			paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
		} 
		 
		riskMap.put("weight", weight);
		riskMap.put("paiceDeviation", paiceDeviation);
		return riskMap;
	}
	
	
	//获取现券买卖权重、DV01、久期、止损偏离度
	public Map<String,Object> getRiskValue(IfsCfetsrmbCbt cbt) throws Exception{
		
		String bondCode = cbt.getBondCode();//债券代码
		if(StringUtil.isEmpty(bondCode)){
			return riskMap;
		}
		IfsOpicsBond ifsOpicsBond = new IfsOpicsBond();
		ifsOpicsBond = ifsOpicsBondMapper.searchById(bondCode);
		String CCY = ifsOpicsBond.getCcy();
		if("CNY".equals(CCY)){//CNY为现劵买卖，其他为外币债
			String vDate = cbt.getForDate();//交易日期
			String mDate = cbt.getSettlementDate();//结算日
			String weight = "100";//权重
			if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
				weight = "100";
			}else{
				weight = edCustManangeService.getNowDateWeight(vDate,mDate,"443");//权重
			}
			
			BigDecimal cleanPrice = cbt.getCleanPrice();//拆借利率
			String paiceDeviation = "";//偏离度
			map2.put("prdNo", "443");
			map2.put("cleanPrice", cleanPrice+"");
			map2.put("bondCode", bondCode);
			if(StringUtil.isEmpty(cleanPrice+"") || StringUtil.isEmpty(bondCode)){
				paiceDeviation = "";
			}else{
				paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
			}
			
			String costCent = cbt.getCost();//成本中心
			String code = cbt.getInvtype();//交易品种
			map.put("costCent", costCent);
			map.put("code", code);
			SlCommonBean slCommonBean= new SlCommonBean();
			if(!StringUtil.isEmpty(costCent) && !StringUtil.isEmpty(code)){
				slCommonBean = edCustManangeService.queryMarketData(map);//久期、止损、DV01
			}
			
			riskMap.put("weight", weight);
			riskMap.put("paiceDeviation", paiceDeviation);
			riskMap.put("longLimit", slCommonBean.getDuration());//久期
			riskMap.put("lossLimit", slCommonBean.getConvexity());//止损
			riskMap.put("DV01Risk", slCommonBean.getDelta());//DV01
		}else{//外币债
			String vDate = cbt.getForDate();//交易日期
			String mDate = cbt.getSettlementDate();//结算日
			String weight = "100";//权重
			if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
				weight = "100";
			}else{
				weight = edCustManangeService.getNowDateWeight(vDate,mDate,"451");//权重
			}
			
			riskMap.put("weight", weight);
		}
		
		return riskMap;
	}
	
	//获取信用拆借权重、偏离度
	public Map<String,Object> getRiskValue(IfsCfetsrmbIbo ibo) throws Exception{
		
		String vDate = ibo.getForDate();//交易日期
		String mDate = ibo.getSecondSettlementDate();//到期结算日
		String weight = "100";//权重
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			weight = edCustManangeService.getNowDateWeight(vDate,mDate,"444");//权重
		}
		
		BigDecimal rate = ibo.getRate();//拆借利率
		String tradingProduct = ibo.getTradingProduct();//交易品种
		String paiceDeviation = "";//偏离度
		map2.put("prdNo", "444");
		map2.put("rate", rate+"");
		map2.put("currencyPair", tradingProduct);
		if(StringUtil.isEmpty(rate+"") || StringUtil.isEmpty(tradingProduct)){
			paiceDeviation = "";
		}else{
			paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
		}
		 
		riskMap.put("weight", weight);
		riskMap.put("paiceDeviation", paiceDeviation);
		return riskMap;
	}
	
	//获取债券借贷权重
	public Map<String,Object> getRiskValue(IfsCfetsrmbSl sl) throws Exception{
		
		String vDate = sl.getForDate();//交易日期
		String mDate = sl.getSecondSettlementDate();//到期结算日
		String weight = "100";//权重
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			weight = edCustManangeService.getNowDateWeight(vDate,mDate,"445");//权重
		}
		 
		riskMap.put("weight", weight);
		return riskMap;
	}
	
	
	//获取质押式回购权重、偏离度
	public Map<String,Object> getRiskValue(IfsCfetsrmbCr cr) throws Exception{
		
		String vDate = cr.getForDate();//起息日期
		String mDate = cr.getSecondSettlementDate();//到期结算日
		String weight = "100";//权重
		if(StringUtil.isEmpty(vDate) || StringUtil.isEmpty(mDate)){
			weight = "100";
		}else{
			weight = edCustManangeService.getNowDateWeight(vDate,mDate,"446");//权重
		}
		
		BigDecimal repoRate = cr.getRepoRate();//回购利率
		String tradingProduct = cr.getTradingProduct();//交易品种
		String paiceDeviation = "";//偏离度
		map2.put("prdNo", "446");
		map2.put("repoRate", repoRate+"");
		map2.put("tradingProduct", tradingProduct);
		if(StringUtil.isEmpty(repoRate+"") || StringUtil.isEmpty(tradingProduct)){
			paiceDeviation = "";
		}else{
			paiceDeviation = edCustManangeService.queryRhisIntrate(map2);//偏离度
		}
		 
		riskMap.put("weight", weight);
		riskMap.put("paiceDeviation", paiceDeviation);
		return riskMap;
	}
	
}
