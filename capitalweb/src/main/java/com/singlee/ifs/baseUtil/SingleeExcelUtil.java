package com.singlee.ifs.baseUtil;

import com.singlee.financial.bean.SlHldyInfo;
import com.singlee.financial.common.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 读取Excel数据
 */
public class SingleeExcelUtil {

    private static Logger logger = Logger.getLogger(SingleeExcelUtil.class);

    public static List<SlHldyInfo> readDataByIndex(InputStream in, String suffix) {
        int isAdj = 0;
        //记录调休日所在行
        int exchangeday = 0;
        int holiday = 0;
        Workbook wb = null;
        Sheet sheet;
        try {
            //初始化返回对象
            List<SlHldyInfo> hildyData = new ArrayList<>();
            if (".xls".endsWith(suffix)) {
                wb = new HSSFWorkbook(in);
            } else if (".xlsx".endsWith(suffix)) {
                wb = new XSSFWorkbook(in);
            }
            sheet = wb.getSheet("CurrencyView");
            if (null == sheet) {
                logger.info("不存在CurrencyView,请检查导入模板");
                return null;
            }
            //获取excel最大范围行数
            int lastRowNum = sheet.getLastRowNum();
            //开始寻找holiday和exchangeday出现的位置
            for (int rowIndex = 0; rowIndex <= lastRowNum; rowIndex++) {
                //获取第一列出现
                Row row = sheet.getRow(rowIndex);
                if (null == row) {
                    continue;
                }
                Cell cell = row.getCell(0);
                if (null == cell) {
                    continue;
                }
                if (StringUtils.equalsIgnoreCase(cell.getStringCellValue(), "[HOLIDAYS]")) {
                    holiday = cell.getRow().getRowNum();
                } else if (StringUtils.equalsIgnoreCase(cell.getStringCellValue(), "[EXCHANGE HOLIDAYS] ")) {
                    exchangeday = cell.getRow().getRowNum();
                }
            }
            //开始按列解析逻辑，获取[HOLIDAYS]下一列最大值
            int maxRowNum = sheet.getRow(holiday + 1).getLastCellNum();
            for (int rowIndex = 0; rowIndex <= maxRowNum; rowIndex++) {
                SlHldyInfo slHldyInfo = new SlHldyInfo();
                List<Date> exchangeLst = new ArrayList<>();
                List<Date> hildyLst = new ArrayList<>();
                String ccy = "";
                //获取当前最大列
                Row row = sheet.getRow(rowIndex);
                if (null == row) {
                    continue;
                }
                for (int columnIndex = 0; columnIndex <= lastRowNum; columnIndex++) {
                    Row rows = sheet.getRow(columnIndex);
                    if (null == rows) {
                        continue;
                    }
                    Cell cells = rows.getCell(rowIndex);
                    if (null == cells) {
                        continue;
                    }
                    //添加在[HOLIDAYS] 之间的假日
                    if (exchangeday > cells.getRow().getRowNum() && cells.getRow().getRowNum() > holiday) {
                        hildyLst.add(DateUtil.parse(cells.getStringCellValue().substring(4)));
                    } else if (cells.getRow().getRowNum() > exchangeday) {
                        exchangeLst.add(DateUtil.parse(cells.getStringCellValue().substring(4)));
                    }
                    //判断是否包含货币代码,则进行记录
                    if (cells.getStringCellValue().matches("^[A-Z]{3}\\=\\d{8}$")) {
                        //截取对应的货币
                        ccy = cells.getStringCellValue().replaceAll("[^(A-Z)]", "");
                    } else {
                        ccy = "";
                    }
                }
                //非空货币保存假日
                if (StringUtils.isNotBlank(ccy)) {
                    slHldyInfo.setCcy(ccy);
                    slHldyInfo.setHilDayLst(hildyLst);
                    slHldyInfo.setAdjDayLst(exchangeLst);
                    hildyData.add(slHldyInfo);
                }
            }
            return hildyData;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
