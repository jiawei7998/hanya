package com.singlee.ifs.baseUtil;

import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsOpicsBond;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class OpicsBondCheck {
	@Autowired
	IfsOpicsBondMapper bondMapper;

	/**
	 * 检查债券是否存在
	 * 
	 * @param secid
	 * @return
	 */
	public boolean bondExits(String secid) {
		IfsOpicsBond opicsBond = bondMapper.searchById(secid);
		if (null == opicsBond) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * 检查债券到期
	 * 
	 * @param secid
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public boolean bondMatuity(String secid, Date date) throws ParseException {
		IfsOpicsBond opicsBond = bondMapper.searchById(secid);
		if (bondExits(secid)) {
			int o = DateUtils.truncatedCompareTo(date, new SimpleDateFormat("yyyy-MM-dd").parse(opicsBond.getMatdt()),
					Calendar.DAY_OF_MONTH);
			if (1 == o) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}
}
