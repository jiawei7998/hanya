package com.singlee.ifs.baseUtil;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.controller.IfsFlowController;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.ifs.service.IfsOpicsBondService;
import com.singlee.refund.mapper.*;
import com.singlee.refund.model.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class IfsFlowApprove {

	private static final Logger logManager = LoggerFactory.getLogger(IfsFlowController.class);
	@Autowired
	IfsFlowMonitorMapper ifsFlowMonitorMapper;// 流程监控
	@Autowired
	IfsCfetsfxLendMapper ifsCfetsfxLendMapper;// 外币拆借
	@Autowired
	IfsCfetsfxSptMapper ifsCfetsfxSptMapper;// 人民币即期
	@Autowired
	IfsCfetsfxFwdMapper ifsCfetsfxFwdMapper;// 人民币远期
	@Autowired
	IfsCfetsfxSwapMapper ifsCfetsfxSwapMapper;// 人命币外币掉期
	@Autowired
	IfsCfetsfxOptionMapper ifsCfetsfxOptionMapper; // 人民币期权
	@Autowired
	IfsCfetsrmbIrsMapper ifsCfetsrmbIrsMapper; // 利率互换
	@Autowired
	IfsCfetsrmbOrMapper ifsCfetsrmbOrMapper; // 买断式回购
	@Autowired
	IfsCfetsrmbCbtMapper ifsCfetsrmbCbtMapper; // 现劵买卖
	@Autowired
	IfsCfetsrmbBondfwdMapper ifsCfetsrmbBondfwdMapper; //债券远期
	@Autowired
	IfsCfetsrmbSlMapper ifsCfetsrmbSlMapper; // 债券借贷
	@Autowired
	IfsCfetsrmbDetailSlMapper ifsCfetsrmbDetailSlMapper;
	@Autowired
	IfsCfetsrmbCrMapper ifsCfetsrmbCrMapper; // 质押式回购
	@Autowired
	IfsCfetsrmbDetailCrMapper ifsCfetsrmbDetailCrMapper;
	@Autowired
	IfsCfetsrmbDpMapper ifsCfetsrmbDpMapper; // 存单发行
	@Autowired
	IfsCfetsrmbDpOfferMapper ifsCfetsrmbDpOfferMapper;
	@Autowired
	IfsOpicsCustMapper ifsOpicsCustMapper;// 交易对手
	@Autowired
	IfsOpicsBondService ifsOpicsBondService;// 债券信息
	@Autowired
	IfsApprovermbDepositMapper ifsApprovermbDepositMapper;//同业存款
	@Autowired
	IfsApprovermbSlMapper ifsApprovermbSlMapper;//债券借贷事前审批
	
	//基金信息
	@Autowired
	CFtInfoMapper cFtInfoMapper;
	//申购
	@Autowired
	CFtAfpMapper cFtAfpMapper;
	//申购确认
	@Autowired
	CFtAfpConfMapper cFtAfpConfMapper;
	//赎回
	@Autowired
	CFtRdpMapper cFtRdpMapper;
	//赎回确认
	@Autowired
	CFtRdpConfMapper cFtRdpConfMapper;
	//现金分红
	@Autowired
	CFtReddMapper cFtReddMapper;
	//红利再投
	@Autowired
	CFtReinMapper cFtReinMapper;
	//同业存放(外币)
	@Autowired
	IfsFxDepositInMapper ifsFxDepositInMapper;
	//存放同业(外币)
	@Autowired
	IfsFxDepositoutMapper ifsFxDepositoutMapper;
	//同业存放
	@Autowired
	IfsRmbDepositinMapper ifsRmbDepositinMapper;
	//同业存放
	@Autowired
	IfsRmbDepositoutMapper ifsRmbDepositoutMapper;
	@Autowired
	private IfsCfetsrmbIboMapper cfetsrmbIboMapper; // 信用拆借

	/**
	 * 批量提交前检查交易对手、债券信息
	 * 
	 * @param param
	 * @return
	 */
	public String approveBeforeCheck(Map<String, Object> param) {
		String msg = "";// 校验信息返回
		String tradeType = "";
		String ticketId = ParameterUtil.getString(param, "ticketId", "");// 交易审批单号
		String prdno = ParameterUtil.getString(param, "prdNo", "");// 产品类型
		if ("".equals(ticketId)) {
			msg = "审批单号不存在";
			return msg;
		}
		//approveBatchCommit 方法调用首次提交 ticketId、prdNo除基金交易不会进入
		//approveBatchCommit 方法调用再次提交 ticketId所有交易进入
		if ("".equals(prdno)||TradeConstants.ProductCode.CURRENCY_FUND.equals(prdno)||TradeConstants.ProductCode.BOND_FUND.equals(prdno)||TradeConstants.ProductCode.SPEC_FUND.equals(prdno)) {

			IfsFlowMonitor ifsFlowMonitor = ifsFlowMonitorMapper.getEntityById(ticketId);
			if (ifsFlowMonitor != null) {//再次提交 监控表存在
				prdno = ifsFlowMonitor.getPrdNo();
				tradeType = ifsFlowMonitor.getTradeType();
			}else{// 首次提交基金部分 监控表不存在  进入视图查询数据
				IfsFlowMonitor flowMonitor = ifsFlowMonitorMapper.getPrdNoById(ticketId);
				if (flowMonitor != null) {
					prdno = flowMonitor.getPrdNo();
				}
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ticketId", ticketId);

//			IfsFiveClassification five = ifsFiveClassificationMapper.searchById(map);
//			if (five != null) {
//				prdno = "507"; //投后五级分类产品编号
//			}

		}
		logManager.info("==========提交审批信息参数值prdno=[" + prdno + "],ticketId=[" + ticketId + "]=================");
		if (!"".equals(prdno)) {
			if ("rmbspt".equals(prdno) || "411".equals(prdno)) {
				// 外汇即期
				IfsCfetsfxSpt ifsCfetsfxSpt = ifsCfetsfxSptMapper.getSpot(ticketId);
				if (ifsCfetsfxSpt != null) {
					msg = checkCust(ifsCfetsfxSpt.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("rmbBondFwd".equals(prdno) || "447".equals(prdno)) {
				// 债券远期
				IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd=ifsCfetsrmbBondfwdMapper.selectById(ticketId);
				if (ifsCfetsrmbBondfwd != null) {
					msg = checkCust(ifsCfetsrmbBondfwd.getCustNo(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("rmbfwd".equals(prdno) || "391".equals(prdno)) {
				// 外汇远期
				IfsCfetsfxFwd ifsCfetsfxFwd = ifsCfetsfxFwdMapper.getCredit(ticketId);
				if (ifsCfetsfxFwd != null) {
					msg = checkCust(ifsCfetsfxFwd.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("rmbswap".equals(prdno) || "432".equals(prdno)) {
				// 外汇掉期
				IfsCfetsfxSwap ifsCfetsfxSwap = ifsCfetsfxSwapMapper.getrmswap(ticketId);
				if (ifsCfetsfxSwap != null) {
					msg = checkCust(ifsCfetsfxSwap.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("ccyLending".equals(prdno) || "438".equals(prdno)) {
				// 外币拆借
				IfsCfetsfxLend ifsCfetsfxLend = ifsCfetsfxLendMapper.searchCcyLending(ticketId);
				if (ifsCfetsfxLend != null) {
					msg = checkCust(ifsCfetsfxLend.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("ibo".equals(prdno) || "444".equals(prdno)||"tyibo".equals(prdno) || "544".equals(prdno)) {
				// 信用拆借
				IfsCfetsrmbIbo ifsCfetsrmbIbo = cfetsrmbIboMapper.searchCredLo(ticketId);
				if (ifsCfetsrmbIbo != null) {
					msg = checkCust(ifsCfetsrmbIbo.getRemoveInst(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("IBD".equals(prdno) || "800".equals(prdno)||"IBDOFF".equals(prdno) || "799".equals(prdno)) {
				//同业存款
				IfsApprovermbDeposit deposit = ifsApprovermbDepositMapper.searchIfsApprovermbDepositById(ticketId);
				if (deposit != null) {
					msg = checkCust(deposit.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("cbt".equals(prdno) || "443".equals(prdno) 
					|| "debt".equals(prdno) || "451".equals(prdno)
					||"mbs".equals(prdno) ||"471".equals(prdno)) {
				// 现券买卖、外币债
				IfsCfetsrmbCbt ifsCfetsrmbCbt = ifsCfetsrmbCbtMapper.searchIfsBondTra(ticketId);
				if (ifsCfetsrmbCbt != null) {
					msg = checkCust(ifsCfetsrmbCbt.getSellInst(), ticketId);
					// 客户校验不通过，债券就没必要校验了
					if ("".equals(msg)) {
						msg = checkBond(ifsCfetsrmbCbt.getBondCode(), ticketId);
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("sl".equals(prdno) || "445".equals(prdno)) {
				// 债券借贷
				if("1".equals(tradeType)) {
					IfsCfetsrmbSl ifsCfetsrmbSl = ifsCfetsrmbSlMapper.searchBondLon(ticketId);
					if (ifsCfetsrmbSl != null) {
						msg = checkCust(ifsCfetsrmbSl.getLendInst(), ticketId);
						// 客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							// 标的券
							msg = checkBond(ifsCfetsrmbSl.getUnderlyingSecurityId(), ticketId);
							if ("".equals(msg)) {
								// 质押券
								List<IfsCfetsrmbDetailSl> list = ifsCfetsrmbDetailSlMapper.searchBondByTicketId(ticketId);
								for (IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl : list) {
									msg = checkBond(ifsCfetsrmbDetailSl.getMarginSecuritiesId(), ticketId);
									if (!"".equals(msg)) {
										break;
									}
								}
							}
						}
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				}else {
					IfsApprovermbSl ifsApprovermbSl = ifsApprovermbSlMapper.searchBondLon(ticketId);
					if (ifsApprovermbSl != null) {
						msg = checkCust(ifsApprovermbSl.getLendInst(), ticketId);
						// 客户校验不通过，债券就没必要校验了
						if ("".equals(msg)) {
							// 标的券
							msg = checkBond(ifsApprovermbSl.getUnderlyingSecurityId(), ticketId);
							if ("".equals(msg)) {
								// 质押券
								List<IfsCfetsrmbDetailSl> list = ifsCfetsrmbDetailSlMapper.searchBondByTicketId(ticketId);
								for (IfsCfetsrmbDetailSl ifsCfetsrmbDetailSl : list) {
									msg = checkBond(ifsCfetsrmbDetailSl.getMarginSecuritiesId(), ticketId);
									if (!"".equals(msg)) {
										break;
									}
								}
							}
						}
					}
				}
			} else if ("dp".equals(prdno) || "452".equals(prdno) || "dpcd".equals(prdno) || "425".equals(prdno)) {
				// 债券发行，循环判断
				List<IfsCfetsrmbDpOffer> list = ifsCfetsrmbDpOfferMapper.searchOfferList(ticketId);
				for (IfsCfetsrmbDpOffer ifsCfetsrmbDpOffer : list) {
					msg = checkCust(ifsCfetsrmbDpOffer.getLendInst(), ticketId);
					if (!"".equals(msg)) {
						break;
					}
				}
				// 客户校验不通过，债券就没必要校验了
				if ("".equals(msg)) {
					// 校验债券
					Map<String, String> map = new HashMap<String, String>();
					map.put("ticketId", ticketId);
					map.put("branchId", "TLONGBANK");
					IfsCfetsrmbDp ifsCfetsrmbDp = ifsCfetsrmbDpMapper.selectById(map);
					if (ifsCfetsrmbDp != null) {
						msg = checkBond(ifsCfetsrmbDp.getDepositCode(), ticketId);
					} else {
						msg = "[" + ticketId + "]交易不存在";
					}
				}
				// modify by zfl for BA20191103 20200519 begin
			} else if ("or".equals(prdno) || "442".equals(prdno)) {
				// 买断式回购
				IfsCfetsrmbOr ifsCfetsrmbOr = ifsCfetsrmbOrMapper.searchCfetsrmbOrByTicketId(ticketId);
				if (ifsCfetsrmbOr != null) {
					// 校验客户信息
					msg = checkCust(ifsCfetsrmbOr.getReverseInst(), ticketId);
					// 客户校验不通过，债券就没必要校验了
					if ("".equals(msg)) {
						msg = checkBond(ifsCfetsrmbOr.getBondCode(), ticketId);
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("cr".equals(prdno) || "446".equals(prdno)
					||"CRJY".equals(prdno) || "468".equals(prdno)
					||"CRCB".equals(prdno) || "469".equals(prdno)
					||"CRZQ".equals(prdno) || "470".equals(prdno)
					||"CRZD".equals(prdno) || "472".equals(prdno)
					||"CRGD".equals(prdno) || "467".equals(prdno)) {
				// 质押式回购
				IfsCfetsrmbCr ifsCfetsrmbCr = ifsCfetsrmbCrMapper.searchPleDge(ticketId);
				if (ifsCfetsrmbCr != null) {
					msg = checkCust(ifsCfetsrmbCr.getReverseInst(), ticketId);
					// 客户校验不通过，债券就没必要校验了
					if ("".equals(msg)) {
						// 质押券
						List<IfsCfetsrmbDetailCr> list = ifsCfetsrmbDetailCrMapper.searchBondByTicketId(ticketId);
						for (IfsCfetsrmbDetailCr ifsCfetsrmbDetailCr : list) {
							msg = checkBond(ifsCfetsrmbDetailCr.getBondCode(), ticketId);
							if (!"".equals(msg)) {
								break;
							}
						}
					}
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("irs".equals(prdno) || "441".equals(prdno) || "irsDerive".equals(prdno)) {
				// 利率互换,结构衍生
				Map<String, String> map = new HashMap<String, String>();
				map.put("ticketId", ticketId);
				map.put("branchId", "TLONGBANK");
				IfsCfetsrmbIrs IfsCfetsrmbIrs = ifsCfetsrmbIrsMapper.selectById(map);
				if (IfsCfetsrmbIrs != null) {
					msg = checkCust(IfsCfetsrmbIrs.getFloatInst(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if(TradeConstants.TrdType.CURY_FUND_AFP.equals(prdno)|| "货币基金申购".equals(prdno) ||
					TradeConstants.TrdType.BD_FUND_AFP.equals(prdno)|| "债券基金申购".equals(prdno) ||
					TradeConstants.TrdType.FUND_AFP.equals(prdno) || "专户基金申购".equals(prdno) ){
				//基金申购
				CFtAfp cFtAfp = cFtAfpMapper.selectByPrimaryKey(ticketId);
				if(cFtAfp!=null){
					msg=checkFund(cFtAfp.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else if(TradeConstants.TrdType.CURY_FUND_AFPCONF.equals(prdno)|| "货币基金申购份额确认".equals(prdno) ||
					TradeConstants.TrdType.BD_FUND_AFPCONF.equals(prdno) || "债券基金申购份额确认".equals(prdno) ||
					TradeConstants.TrdType.FUND_AFPCONF.equals(prdno) || "专户基金申购份额确认".equals(prdno)){
				//基金申购份额确认
				CFtAfpConf cFtAfpConf = cFtAfpConfMapper.selectByPrimaryKey(ticketId);
				if(cFtAfpConf!=null){
					msg=checkFund(cFtAfpConf.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else if(TradeConstants.TrdType.CURY_FUND_RDP.equals(prdno)|| "货币基金赎回".equals(prdno) ||
					TradeConstants.TrdType.BD_FUND_RDP.equals(prdno) || "债券基金赎回".equals(prdno) ||
					TradeConstants.TrdType.FUND_RDP.equals(prdno) || "专户基金赎回".equals(prdno)){
				//基金赎回
				CFtRdp cFtRdp = cFtRdpMapper.selectByPrimaryKey(ticketId);
				if(cFtRdp!=null){
					msg=checkFund(cFtRdp.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else if(TradeConstants.TrdType.CURY_FUND_RDPCONF.equals(prdno)|| "货币基金赎回份额确认".equals(prdno) ||
					TradeConstants.TrdType.BD_FUND_RDPCONF.equals(prdno) || "债券基金赎回份额确认".equals(prdno)||
					TradeConstants.TrdType.FUND_RDPCONF.equals(prdno) || "专户基金赎回份额确认".equals(prdno)){
				//基金赎回份额确认
				CFtRdpConf cFtRdpConf = cFtRdpConfMapper.selectByPrimaryKey(ticketId);
				if(cFtRdpConf!=null){
					msg=checkFund(cFtRdpConf.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else if(TradeConstants.TrdType.FUND_REDD.equals(prdno) || "现金分红".equals(prdno)
					|| "货币基金现金分红".equals(prdno) || "债券基金现金分红".equals(prdno) || "专户基金现金分红".equals(prdno) ){
				//基金现金分红
				CFtRedd cFtRedd = cFtReddMapper.selectByPrimaryKey(ticketId);
				if(cFtRedd!=null){
					msg=checkFund(cFtRedd.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			}else if(TradeConstants.TrdType.FUND_REIN.equals(prdno) || "红利再投".equals(prdno)
					|| "货币基金红利再投".equals(prdno) || "债券基金红利再投".equals(prdno) ||"专户基金红利再投".equals(prdno)){
				//基金红利再投
				CFtRein cFtRein = cFtReinMapper.selectByPrimaryKey(ticketId);
				if(cFtRein!=null){
					msg=checkFund(cFtRein.getFundCode(),ticketId);
					
				}else{
					msg="["+ticketId+"]交易不存在";
				}
			} else if ("rmbopt".equals(prdno) || "433".equals(prdno)) {
				// 外汇期权
				IfsCfetsfxOption ifsCfetsfxOption = ifsCfetsfxOptionMapper.searchOption(ticketId);
				if (ifsCfetsfxOption != null) {
					msg = checkCust(ifsCfetsfxOption.getCounterpartyInstId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else if ("507".equals(prdno)) {
				// 提交审批前修改发起人信息
//				ifsFiveClassificationMapper.updateApproveByID(ticketId, SlSessionHelper.getUserId(),
//						SlSessionHelper.getInstitutionId());
				// modify by zfl for BA20191103 20200519 end
			}  else if ("ccyDepositIn".equals(prdno) || "436".equals(prdno)) {
				//同业存放(外币)
				IfsFxDepositIn ifsFxDepositIn = ifsFxDepositInMapper.searchByTicketId(ticketId);
				if (ifsFxDepositIn != null) {
					msg = checkCust(ifsFxDepositIn.getCustId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			}  else if ("ccyDepositout".equals(prdno) || "437".equals(prdno)) {
				//存放同业(外币)
				IfsFxDepositout ifsFxDepositout = ifsFxDepositoutMapper.searchByTicketId(ticketId);
				if (ifsFxDepositout != null) {
					msg = checkCust(ifsFxDepositout.getCustId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			}  else if ("rmbDepositIn".equals(prdno) || "790".equals(prdno)) {
				//同业存放
				IfsRmbDepositin ifsRmbDepositin = ifsRmbDepositinMapper.searchByTicketId(ticketId);
				if (ifsRmbDepositin != null) {
					msg = checkCust(ifsRmbDepositin.getCustId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			}  else if ("rmbDepositOut".equals(prdno) || "791".equals(prdno)) {
				//存放同业
				IfsRmbDepositout ifsRmbDepositout = ifsRmbDepositoutMapper.searchByTicketId(ticketId);
				if (ifsRmbDepositout != null) {
					msg = checkCust(ifsRmbDepositout.getCustId(), ticketId);
				} else {
					msg = "[" + ticketId + "]交易不存在";
				}
			} else {
				msg = "交易产品类型匹配失败！";
			}
		} else {
			msg = "交易产品类型查询失败，无数据！";
		}
		return msg;
	}

	private String checkCust(String cno, String ticketId) {
		String msg = "";
		logManager.info("==========提交审批客户信息检查cno=[" + cno + "],ticketId=[" + ticketId + "]=================");
		if ("".equals(cno)) {
			msg = "交易本身没有交易对手";
		} else {
			IfsOpicsCust ifsOpicsCust = ifsOpicsCustMapper.searchById(cno);
			if (ifsOpicsCust != null) {
				msg = checkCustDetail(ifsOpicsCust);
			} else {
				msg = "交易对手[" + cno + "]不存在";
			}
		}
		if (!"".equals(msg)) {
			msg = "[" + ticketId + "]" + msg;
		}
		return msg;
	}

	private String checkBond(String bndcd, String ticketId) {
		String msg = "";
		logManager.info("==========提交审批债券信息检查bndcd=[" + bndcd + "],ticketId=[" + ticketId + "]=================");
		if ("".equals(bndcd)) {
			msg = "交易本身没有债券";
		} else {
			IfsOpicsBond ifsOpicsBond = ifsOpicsBondService.searchOneById(bndcd);
			if (ifsOpicsBond != null) {
				msg = checkBondDetail(ifsOpicsBond);
			} else {
				msg = "的[" + bndcd + "]债券信息不存在";
			}
		}
		if (!"".equals(msg)) {
			msg = "[" + ticketId + "]这笔交易的" + msg;
		}
		return msg;
	}

	private String checkCustDetail(IfsOpicsCust ifsOpicsCust) {
		String msg = "";
		if (StringUtils.isEmpty(ifsOpicsCust.getClitype())) {
			msg = "交易对手客户类型为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCliname())) {
			msg = "交易对手客户中文名称为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getFlag())) {
			msg = "交易对手境内外标识为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCno())) {
			msg = "交易对手编号为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCname())) {
			msg = "交易对手代码为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCfn())) {
			msg = "交易对手全称为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getSn())) {
			msg = "交易对手简称为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getSic())) {
			msg = "交易对手工业标准代码为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getUccode())) {
			msg = "交易对手国家代码为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCcode())) {
			msg = "交易对手城市代码为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getLocation())) {
			msg = "交易对手所在城市为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getAcctngtype())) {
			msg = "交易对手会计类型为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCtype())) {
			msg = "交易对手类型为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCreditsub())) {
			msg = "交易对手授信占用主体为空";
		} else if (StringUtils.isEmpty(ifsOpicsCust.getCreditsubno())) {
			msg = "交易对手授信占用主体编号为空";
		}
		return msg;
	}

	private String checkBondDetail(IfsOpicsBond ifsOpicsBond) {
		String msg="";
		if(StringUtils.isEmpty(ifsOpicsBond.getBndcd())){
			msg="债券代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getDescr())){
			msg="债券描述为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBndnm_en())){
			msg="债券简称为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBndnm_cn())){
			msg="债券全称为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getProduct())){
			msg="债券产品代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getProdtype())){
			msg="债券产品类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntcalcrule())){
			msg="债券计息规则为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getAcctngtype())){
			msg="债券会计类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getCcy())){
			msg="债券货币为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSecunit())){
			msg="债券股权类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getDenom())){
			msg="债券单价为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssuer())){
			msg="债券发行人为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getPubdt())){
			msg="债券发行日期为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondproperties())){
			msg="债券标准行业代码为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSettccy())){
			msg="债券结算币种为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssudt())){
			msg="债券起息日为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getMatdt())){
			msg="债券到期日为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIssprice()==null?"":ifsOpicsBond.getIssprice().toString())){
			msg="债券面值为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntpaymethod())){
			msg="债券利息支付规则为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getIntenddaterule())){
			msg="债券利息结束日规则为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZtInst())){
			msg="主体评级机构为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZxZtLevel())){
			msg="主体评级级别为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZxInst())){
			msg="债项评级机构为空";
		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getZxZxLevel())){
			msg="债项评级机构为空";
		}
//		else if(StringUtils.isEmpty(ifsOpicsBond.getBondratingclass())){
//			msg="债券评级类型为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondratingagency())){
//			msg="债券评级机构为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBondrating())){
//			msg="债券评级结果为空";
//		}
		else if(StringUtils.isEmpty(ifsOpicsBond.getAccountno())){
			msg="债券托管机构为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getBasis())){
			msg="债券计息方式为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getParagraphfirstplan())){
			msg="债券第一个息票日期为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getRateType())){
			msg="债券利率类型为空";
		}else if(StringUtils.isEmpty(ifsOpicsBond.getSlPubMethod())){
			msg="债券发行方式为空";
		}
//		else if(StringUtils.isEmpty(ifsOpicsBond.getMarketDate())){
//			msg="债券上市日为空";
//		}else if(StringUtils.isEmpty(ifsOpicsBond.getBr())){
//			msg="债券部门为空";
//		}
		return msg;
	}
	
	private String checkFund(String fundCode,String ticketId){
		String msg="";
		logManager.info("==========提交审批基金信息检查fundCode=["+fundCode+"],ticketId=["+ticketId+"]=================");
		if("".equals(fundCode)){
			msg="交易本身没有基金";
		}else{
			CFtInfo info = cFtInfoMapper.selectByPrimaryKey(fundCode);
			if(info==null){
				msg="的["+fundCode+"]基金信息不存在";
			}
		}
		if(!"".equals(msg)){
			msg="["+ticketId+"]这笔交易的"+msg;
		}
		return msg;
	}
}
