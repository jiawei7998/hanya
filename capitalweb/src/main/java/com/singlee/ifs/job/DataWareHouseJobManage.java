package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.*;
import com.singlee.capital.system.dict.DictConstants;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.mapper.Dw1ReadAndSaveMapper;
import com.singlee.ifs.model.DataWareHouse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.*;

public class DataWareHouseJobManage  implements CronRunnable {



    private Dw1ReadAndSaveMapper dw1ReadAndSaveMapper = SpringContextHolder.getBean(Dw1ReadAndSaveMapper.class);


        String postdate= DateUtil.getCurrentDateAsString("yyyyMMdd");

        Map<String, Object> pmap=new HashMap<>();

     @Override
    public boolean execute(Map<String,Object> parameters) throws Exception {
  String postDate= DateUtil.getCurrentDateAsString("yyyyMMdd");

         Map<String, Object> remap=new HashMap<>();
         remap.put("postDate",postDate);
         remap.put("OPICS", SystemProperties.opicsUserName);
        List<DataWareHouse> dwList = new ArrayList<DataWareHouse>();
        //处理当天的交易
        List<Map<String, Object>> dldtList = dw1ReadAndSaveMapper.dldtList(remap);
        JY.info("处理拆借DLDT交易,数据量为->" + dldtList.size());
        packageDLDT(dldtList,dwList);

        List<Map<String, Object>> rprhList = dw1ReadAndSaveMapper.rprhList(remap);
        JY.info("处理回购RPRH交易,数据量为->"+rprhList.size());
        packageRPRH(rprhList,dwList);

        List<Map<String, Object>> tposList = dw1ReadAndSaveMapper.tposList(remap);
        JY.info("处理债券TPOS交易,数据量为->"+tposList.size());
        packageTPOS(tposList,dwList);

        List<Map<String, Object>> callList = dw1ReadAndSaveMapper.callList(remap);
        JY.info("处理CALL AND NOTICE交易,数据量为->"+callList.size());
        packageCALL(callList,dwList);

        JY.info("*********************数据处理完成，需要处理的数据量为"+dwList.size()+"*********************************");
//        if(dwList!=null && dwList.size()>0){
//            //插入管会记录表
//            batchDao.batch("com.singlee.capital.trade.mapper.TdMulResaleDetailMapper.insert", dwList);
//        }
        JY.info("*********************数据入库成功，准备生成文件*********************************");


        //本地保存地址
        String src_folder= SystemProperties.dwPath;
        //文件名
        String fileName= SystemProperties.dwFilename+postDate+ ".del";
        JY.info("*********************本地保存路径为"+src_folder+"   文件名为"+fileName+"*********************************");

        //判断文件目录是否存在
        File filedir = new File(src_folder);
        if (!filedir.exists()) {
            filedir.mkdirs();
        }
        //写入数据
        String filePath=src_folder + fileName;
      //  File dat = new File(src_folder + fileName);
        JY.info("*********************生成的文件名为"+fileName+"*********************************");

      //  FileWriter fw = new FileWriter(dat);
      //  BufferedWriter bw = new BufferedWriter(fw);
          BufferedWriter  bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(filePath)),"GBK"));
        for(DataWareHouse write : dwList){

            bw.write(write.toDWHString());
            bw.newLine();
        }
        bw.flush();
        bw.close();
//        fw.close();

        return true;



    }
    //组装回购业务
    private void packageRPRH(List<Map<String, Object>> rprhList, List<DataWareHouse> dwList) {
        for(Map<String, Object> rprh:rprhList){
            DataWareHouse dw = new DataWareHouse();
            Date Postdate=new Date();
            String br=ParameterUtil.getString(rprh,"BR","");
            String dealno=ParameterUtil.getString(rprh,"DEALNO","");
            String costcent=ParameterUtil.getString(rprh,"COST","");
            pmap.put("dealno",dealno);
            pmap.put("br",br);
            pmap.put("costcent",costcent);
            pmap.put("OPICS", SystemProperties.opicsUserName);
            rprh.put("OPICS", SystemProperties.opicsUserName);
            String  Balance= dw1ReadAndSaveMapper.Balance(pmap);
            String  costdesc= dw1ReadAndSaveMapper.costdesc(pmap);
            String  rprhCore= dw1ReadAndSaveMapper.rprhCore(rprh);
            String  rprhInterest= dw1ReadAndSaveMapper.rprhInterest(rprh);
            String rprhInterestCode="";
            String rprhCored="";
            if(null!=rprhInterest){
                rprhInterestCode=rprhInterest.trim();
            }

            if(null!=rprhCore){
                rprhCored=rprhCore.trim();
            }

            try{

                dw.setData_date(DateUtil.format(new Date())); // 1:当前日期
                dw.setSource_sys_code(DictConstants.Dw.OPICS_SYS_NAME); // 2:源系统标志
                dw.setBusiness_type(DictConstants.Dw.PROD_REPO); // 3
                dw.setAccount_id(ParameterUtil.getString(rprh,"DEALNO","")); // 4:OPICS交易号
                dw.setOrg_unit_code(getOrg5(rprh)); // 5:记账机构号
                dw.setAccount_code(rprhCored); // 6:本金对应的科目号
                dw.setAsset_type(""); // 7:放空
                dw.setProduct_code(getTypeDesc(rprh)); // 8
                dw.setBusiness_line_code(""); // 9:放空
                dw.setCurrency_code(ccyCode(rprh)); // 10:得到币种对就的数字，如CNY对应01
                dw.setOrigination_date(ParameterUtil.getString(rprh,"VDATE","").substring(0,10)); // 11
                dw.setMaturity_date(ParameterUtil.getString(rprh,"MATDATE","").substring(0,10)); // 12
                dw.setInitial_date(ParameterUtil.getString(rprh,"VDATE","").substring(0,10)); // 13
                dw.setSell_date(ParameterUtil.getString(rprh,"VDATE","").substring(0,10)); // 14

                if(DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(dw.getSell_date()))>=0){
                    //跑批日期比卖出日大,表示已到期
                    dw.setPar_balance(0.0); // 15
                    dw.setCur_balance(0.0); // 16

                }else
                {
                    if(null==Balance){
                        dw.setPar_balance(0.00);
                    }else {
                        dw.setPar_balance(Double.parseDouble(Balance));
                    }

                    dw.setCur_balance(ParameterUtil.getDouble(rprh,"NOTAMT",0.0)); // 16
                }
                dw.setOrg_balance(dw.getPar_balance()); // 17
                dw.setOrg_par_balance(Math.abs(ParameterUtil.getDouble(rprh,"NOTAMT",0)));// 18
                dw.setCur_net_rate(Math.abs(ParameterUtil.getDouble(rprh,"REPORATE_8",0))); // 19:利率
                dw.setAdjustable_type_code(""); // 20
                dw.setLast_reprice_date(""); // 21
                dw.setReprice_freq(""); // 22
                dw.setReprice_freq_mult(""); // 23
                dw.setNext_reprice_date(""); // 24
                dw.setPayment_type("一次性"); // 25
                dw.setInt_type("一次性"); // 26
                dw.setLast_payment_date(""); // 27
                dw.setPaymnet_freq(""); // 28
                dw.setPayment_freq_mult(""); // 29
                dw.setNext_payment_date(""); // 30INTCAPAMT
                dw.setPayment_amount(Math.abs(ParameterUtil.getDouble(rprh,"REPOAMT",0))); // 31
                dw.setInterest_amount(Math.abs(ParameterUtil.getDouble(rprh,"TOTALINT",0)));// 32
                dw.setInvest_gain_amount(0.0); // 33
                dw.setMkt_value_change_amount(0.0); // 34
                dw.setInt_adjust_amount(0.0); // 35
                dw.setCustomer_code(ParameterUtil.getString(rprh,"CNO","").trim()); // 36
                dw.setTransaction_cnt(""); // 37
                dw.setTransaction_purpose(getPurpose38(rprh)); // 38
                dw.setPar_rate("0"); // 39
                dw.setLost_amount(0.0); // 40
                dw.setInterest_account_code(rprhInterestCode); // 41利息收支科目号
                dw.setInvest_gain_account_code(""); // 42
                dw.setMkt_value_account_code(""); // 43INTPAYCYCLE
                dw.setPaymnet_int_freq(""); // 44
                dw.setPayment_int_freq_mult("");		//45
                String Revflag= ParameterUtil.getString(rprh,"REVTIME","0");
                if(!Revflag.equals("0")){
                    dw.setRevflag("1");
                }
                dw.setRevflag("0");		//46

                dw.setIntime(DateUtil.getCurrentDateAsString("yyyy-MM-dd"));



                //只保正常交易和当天冲销的交易
                if(dw.getRevflag().equals("0")){//未冲销
                    //跑批日期就是卖出日,需要传送,以后则不需要传送
                    if(dw.getSell_date()!=null && DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(dw.getSell_date()))<=0){
                     //新增
                        dwList.add(dw);
                    }else{
                      JY.info(dw.getBusiness_type()+"交易被过滤掉[到期], 交易号:"+dw.getAccount_id());
//                        dwList.add(dw);
                    }
                }else{
                    //冲销交易
                    String revdate = ParameterUtil.getString(rprh,"REVDATE","");
                    //判断冲销日期是否与前一个跑批日相等
                    if(DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(revdate, "yyyy-MM-dd"))==0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[冲销], 交易号:"+dw.getAccount_id());
                    }
                }
            }catch(Exception e){
                JY.info(dw.getBusiness_type()+"交易解析错误, 交易号:"+dw.getAccount_id(), e);
            }
        }
    }


    //组装债券业务
    private void packageTPOS(List<Map<String, Object>> tposList, List<DataWareHouse> dwList) {
        Map<String, Object> hmap=new HashMap<>();
        pmap.put("postDate",postdate);
        pmap.put("OPICS", SystemProperties.opicsUserName);
        List<Map<String, Object>> revList= dw1ReadAndSaveMapper.rev(pmap);
        List<String> revIdList = null;
        if(revList!=null && revList.size()>0){
            revIdList = new ArrayList<String>();
            for(Map<String, Object> rev : revList){
                revIdList.add(ParameterUtil.getString(rev,"REVID",""));
            }
        }


        for(Map<String, Object> tpos:tposList){
            DataWareHouse dw = new DataWareHouse();
            String secid=ParameterUtil.getString(tpos,"SECID","");
            String costcent=ParameterUtil.getString(tpos,"COST","");
            hmap.put("secid",secid);
            hmap.put("ps","P");
            hmap.put("OPICS", SystemProperties.opicsUserName);
            pmap.put("secid",secid);
            pmap.put("costcent",costcent);
            pmap.put("ps","S");
            pmap.put("OPICS", SystemProperties.opicsUserName);
            pmap.put("postdate", postdate);
            tpos.put("OPICS", SystemProperties.opicsUserName);

            String  acctdesc =dw1ReadAndSaveMapper.acctdesc(pmap);
            Map<String, Object> secm=dw1ReadAndSaveMapper.secmOne(pmap);
            List<Map<String, Object>> secs=dw1ReadAndSaveMapper.secsOne(pmap);


            secm.put("OPICS", SystemProperties.opicsUserName);
            //获得债券的初次买入日期
            List<Map<String, Object>> Initdate=dw1ReadAndSaveMapper.getSelldate14(hmap);


            try{

                dw.setData_date(DateUtil.format(new Date())); // 1:当前日期
                dw.setSource_sys_code(DictConstants.Dw.OPICS_SYS_NAME); // 2:源系统标志
                dw.setBusiness_type(DictConstants.Dw.PROD_SECUR); // 3
                dw.setAccount_id(getAccountId4(tpos).trim()); // 4:OPICS交易号
                dw.setOrg_unit_code(getOrg5(tpos)); // 5:记账机构号
                dw.setAccount_code(InterestCoreNo41(tpos,"4")); // 6:科目号
                dw.setAsset_type(getAssetType7(tpos).trim()); // 7:放空
                dw.setProduct_code(acctdesc.trim()); // 8
                dw.setBusiness_line_code(""); // 9:放空
                dw.setCurrency_code(ccyCode(secm)); // 10:得到币种对就的数字，如CNY对应01
                dw.setOrigination_date(ParameterUtil.getString(secm,"VDATE","").substring(0,10)); // 11
                dw.setMaturity_date(ParameterUtil.getString(secm,"MDATE","").substring(0,10)); // 12

                if(Initdate!=null && Initdate.size()>0) {
                    String Initdate13 = ParameterUtil.getString(Initdate.get(0), "SETTDATE", "");

                    dw.setInitial_date(Initdate13.substring(0,10)); // 13
                }
                //获得债券的卖出日期
                List<Map<String, Object>> Selldate=dw1ReadAndSaveMapper.getSelldate14(pmap);
                if(Selldate!=null && Selldate.size()>0) {
                    String Selldate14 = ParameterUtil.getString(Selldate.get(0), "SETTDATE", "");
                    if (""!=Selldate14){
                        Selldate14=Selldate14.substring(0,10);
                    }
                    dw.setSell_date(Selldate14); // 14
                }



                dw.setInitial_date(DateUtil.format(new Date())); // 13
                dw.setSell_date(DateUtil.format(new Date())); // 14
                dw.setPar_balance(Math.abs(ParameterUtil.getDouble(tpos,"PRINAMT",0))); // 15
                dw.setCur_balance(Math.abs(ParameterUtil.getDouble(tpos,"SETTAVGCOST",0))); // 16
                dw.setOrg_balance(Math.abs(ParameterUtil.getDouble(tpos,"AVGCOST",0))); // 17
                dw.setOrg_par_balance(Math.abs(ParameterUtil.getDouble(tpos,"PRINAMT",0)));// 18
                dw.setCur_net_rate(Math.abs(ParameterUtil.getDouble(secs.get(0),"INTRATE_8",0))); // 19:利率
                dw.setAdjustable_type_code(getRateCode20(tpos,secid,secm));  // 20:利率代码类型
                dw.setLast_reprice_date(getLastRepairRate21(tpos,secm,secid,postdate)); // 21:上次利率调整日期

                dw.setReprice_freq(getRePrice(tpos,secid,secm).split(",")[0].trim()); // 22
                dw.setReprice_freq_mult(getRePrice(tpos,secid,secm).split(",")[1].trim()); // 23
                dw.setNext_reprice_date(getNextRepairRate24(tpos,secm,secid,postdate)); // 24

                String prodtype = ParameterUtil.getString(secm,"PRODTYPE","0");
                if("SD".equalsIgnoreCase(prodtype)){
                    dw.setPayment_type("分期付息"); // 25

                }else if("IM".equalsIgnoreCase(prodtype)){
                    dw.setPayment_type("一次性"); // 25
                }
                 dw.setInt_type(""); // 26
                dw.setLast_payment_date(getLastPayment27(tpos,secm,secid,postdate)); // 27
                dw.setPaymnet_freq(getRePrice(tpos,secid,secm).split(",")[0].trim()); // 28
                dw.setPayment_freq_mult(getRePrice(tpos,secid,secm).split(",")[1].trim()); // 29

                dw.setNext_payment_date(getNextPayment30(tpos,secm,secid,postdate)); // 30

                dw.setPayment_amount(getPaymentAmount31(tpos,secm,secid,postdate)); // 31

               //发生额
                String tdyintincexp = ParameterUtil.getString(tpos,"TDYINTINCEXP","0");
                String ystintincexp = ParameterUtil.getString(tpos,"YSTINTINCEXP","0");
                Double intincexp = DoubleUtil.sub(Math.abs(Double.parseDouble(tdyintincexp)), Math.abs(Double.parseDouble(ystintincexp)));

                String tdydscincexp = ParameterUtil.getString(tpos,"TDYDSCINCEXP","0");
                String ystdscincexp = ParameterUtil.getString(tpos,"YSTDSCINCEXP","0");
                Double dscincexp = DoubleUtil.sub(Double.parseDouble(tdydscincexp), Double.parseDouble(ystdscincexp));
                dw.setInterest_amount(DoubleUtil.add(intincexp,dscincexp));// 32

                //投资收益
                String tdyintincexps = ParameterUtil.getString(tpos,"TDYDSCINCEXP","0");
                String ystintincexps = ParameterUtil.getString(tpos,"TDYDSCINCEXP","0");

                Double tdcore = -1*Double.parseDouble(tdyintincexps);
                Double ystcore = -1*Double.parseDouble(ystintincexps);
                dw.setInvest_gain_amount(DoubleUtil.sub(tdcore, ystcore)); // 33
                //公允价值变动
                String tdyintincexpx = ParameterUtil.getString(tpos,"TDYDSCINCEXP","0");
                String ystintincexpx = ParameterUtil.getString(tpos,"TDYDSCINCEXP","0");




                dw.setMkt_value_change_amount(DoubleUtil.sub(Double.parseDouble(tdyintincexpx), Double.parseDouble(ystintincexpx))); // 34
                dw.setInt_adjust_amount(Double.parseDouble(ParameterUtil.getString(tpos,"UNAMORTAMT","0"))); // 35
                dw.setCustomer_code(""); // 36
                dw.setTransaction_cnt(""); // 37
                dw.setTransaction_purpose(getPurpose38(tpos)); // 38
                dw.setPar_rate(ParameterUtil.getString(secm,"COUPRATE_8","0")); // 39
                dw.setLost_amount(0.0); // 40
                dw.setInterest_account_code(InterestCoreNo41(tpos,"2")); // 41利息收支科目号
                dw.setInvest_gain_account_code(InterestCoreNo41(tpos,"3")); // 42 投资收益科目
                dw.setMkt_value_account_code(InterestCoreNo41(tpos,"1")); // 43利息收支科目号

                dw.setPaymnet_int_freq(getRePrice(tpos,secid,secm).split(",")[0].trim()); // 44
                dw.setPayment_int_freq_mult(getRePrice(tpos,secid,secm).split(",")[1].trim());		//45
                String Revflag= ParameterUtil.getString(tpos,"" +
                        "","未冲销");
                if(!Revflag.equals("未冲销")){
                    dw.setRevflag("1");
                }
                dw.setRevflag("0");		//46

                dw.setIntime(DateUtil.getCurrentDateAsString("yyyy-MM-dd"));

                String postdate= DateUtil.getCurrentDateAsString("yyyyMMdd");

                //只保正常交易和当天冲销的交易
                if(dw.getRevflag().equals("0")){//未冲销
                    //跑批日期就是卖出日,需要传送,以后则不需要传送
                    if(dw.getSell_date()!=null && DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(dw.getSell_date()))<=0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[到期], 交易号:"+dw.getAccount_id());
                    }
                }else{
                    //冲销交易
                    String revdate = ParameterUtil.getString(tpos,"REVDATE","");
                    //判断冲销日期是否与前一个跑批日相等
                    if(DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(revdate, "yyyy-MM-dd"))==0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[冲销], 交易号:"+dw.getAccount_id());
                    }
                }
            }catch(Exception e){
                JY.info(dw.getBusiness_type()+"交易解析错误, 交易号:"+dw.getAccount_id(), e);
            }
        }
    }

    //组装通知存款交易业务
    private void packageCALL(List<Map<String, Object>> callList, List<DataWareHouse> dwList) {
        String POSTDATE= DateUtil.getCurrentDateAsString("yyyy-MM-dd");
        for(Map<String, Object> call:callList){
            DataWareHouse dw = new DataWareHouse();
            String accountnos=ParameterUtil.getString(call,"ACCOUNTNO","");
            pmap.put("accountno",accountnos);
            pmap.put("OPICS", SystemProperties.opicsUserName);
            call.put("OPICS", SystemProperties.opicsUserName);
//            //科目号=0100+ccyCode+callCore
            String ccyCode= dw1ReadAndSaveMapper.ccyCode(call);

//
//           String callCore= dw1ReadAndSaveMapper.callCore(pmap);
//           String callInterest= dw1ReadAndSaveMapper.callInterest(call);
            try{

                dw.setData_date(DateUtil.format(new Date())); // 1:当前日期
                dw.setSource_sys_code(DictConstants.Dw.OPICS_SYS_NAME); // 2:源系统标志
                dw.setBusiness_type(DictConstants.Dw.PROD_CALL); // 3
                dw.setAccount_id(ParameterUtil.getString(call,"ACCOUNTNO","")); // 4:OPICS交易号
                dw.setOrg_unit_code(getOrg5(call)); // 5:记账机构号
                //科目号=0100+ccyCode+callCore
                String callCore6=callCore(call,"3");
                dw.setAccount_code("0100"+ccyCode(call)+callCore6); // 6:对应的科目号
                dw.setAsset_type(""); // 7:放空
                dw.setProduct_code(getTypeDesc8(call)); // 8:拆借-拆入、拆借-拆出
                dw.setBusiness_line_code(""); // 9:放空
                dw.setCurrency_code(ccyCode(call)); // 10:得到币种对就的数字，如CNY对应01
                dw.setOrigination_date(getVdate(call,POSTDATE)); // 11
                dw.setMaturity_date(getMdate(call,POSTDATE)); // 12
                dw.setInitial_date(getVdate(call,POSTDATE)); // 13
                dw.setSell_date(getMdate(call,POSTDATE)); // 14
                dw.setPar_balance(0.0); // 15
                dw.setCur_balance(dw.getPar_balance()); // 16
                dw.setOrg_balance(dw.getPar_balance()); // 17
                dw.setOrg_par_balance(dw.getPar_balance());// 18
                dw.setCur_net_rate(Math.abs(ParameterUtil.getDouble(call,"INTRATE_8",0))); // 19:利率
                dw.setAdjustable_type_code(""); // 20
                dw.setLast_reprice_date(""); // 21
                dw.setReprice_freq(""); // 22
                dw.setReprice_freq_mult(""); // 23
                dw.setNext_reprice_date(""); // 24
                dw.setPayment_type("一次性"); // 25
                dw.setInt_type(""); // 26
                dw.setLast_payment_date(""); // 27
                dw.setPaymnet_freq(""); // 28
                dw.setPayment_freq_mult(""); // 29
                dw.setNext_payment_date(""); // 30INTCAPAMT
                dw.setPayment_amount(0.0); // 31
                String dlyincexp = ParameterUtil.getString(call,"DLYINCEXP","0");
                String ystincexp = ParameterUtil.getString(call,"YSTINCEXP","0");
                Double  amount  =Math.abs(Double.parseDouble(dlyincexp))-Math.abs(Double.parseDouble(ystincexp));
                dw.setInterest_amount(amount);// 32
                dw.setInvest_gain_amount(0.0); // 33
                dw.setMkt_value_change_amount(0.0); // 34
                dw.setInt_adjust_amount(0.0); // 35
                dw.setCustomer_code(ParameterUtil.getString(call,"CNO","").trim()); // 36
                dw.setTransaction_cnt(""); // 37
                dw.setTransaction_purpose(""); // 38
                dw.setPar_rate("0"); // 39
                dw.setLost_amount(0.0); // 40
                //科目号=0100+ccyCode+callCore
                String callCore41=callCore(call,"5");
                dw.setInterest_account_code("0100"+ccyCode(call)+callCore41); // 41利息收支科目号
                dw.setInvest_gain_account_code(""); // 42
                dw.setMkt_value_account_code(""); // 43INTPAYCYCLE
                dw.setPaymnet_int_freq(""); // 44
                dw.setPayment_int_freq_mult("");		//45
                String Revflag= ParameterUtil.getString(call,"REVDATE","0");
                if(!Revflag.equals("0")){
                    dw.setRevflag("1");
                }
                dw.setRevflag("0");		//46

                dw.setIntime(DateUtil.getCurrentDateAsString("yyyy-MM-dd"));

                String postdate= DateUtil.getCurrentDateAsString("yyyyMMdd");

                //只保正常交易和当天冲销的交易
                if(dw.getRevflag().equals("0")){//未冲销
                    //跑批日期就是卖出日,需要传送,以后则不需要传送
                    if(dw.getSell_date()!=null && DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(dw.getSell_date()))<=0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[到期], 交易号:"+dw.getAccount_id());
                    }
                }else{
                    //冲销交易
                    String revdate = ParameterUtil.getString(call,"REVDATE","");
                    //判断冲销日期是否与前一个跑批日相等
                    if(DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(revdate, "yyyy-MM-dd"))==0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[冲销], 交易号:"+dw.getAccount_id());
                    }
                }
            }catch(Exception e){
                JY.info(dw.getBusiness_type()+"交易解析错误, 交易号:"+dw.getAccount_id(), e);
            }
        }
    }

    //组装拆借业务
    private void packageDLDT(List<Map<String, Object>> dldtList, List<DataWareHouse> dwList) {

        Map<String, Object> didtmap=new HashMap<>();
        for(Map<String, Object> dldt:dldtList){
            String br=ParameterUtil.getString(dldt,"BR","");
            String dealno=ParameterUtil.getString(dldt,"DEALNO","");
            String product=ParameterUtil.getString(dldt,"PRODUCT","");
            String prodtype=ParameterUtil.getString(dldt,"PRODTYPE","");
            String costcent=ParameterUtil.getString(dldt,"COST","");
            String accountno=ParameterUtil.getString(dldt,"ACCOUNTNO","");
            pmap.put("accountno", accountno);
            pmap.put("OPICS", SystemProperties.opicsUserName);
            pmap.put("dealno",dealno);
            pmap.put("br",br);
            pmap.put("product",product);
            pmap.put("prodtype",prodtype);
            pmap.put("costcent",costcent);
            dldt.put("code","0335");
            dldt.put("OPICS", SystemProperties.opicsUserName);
            String  paymentAmt= dw1ReadAndSaveMapper.paymentAmt(pmap);
            String  interestAmt= dw1ReadAndSaveMapper.interestAmt(pmap);
            String  dldtCore=dw1ReadAndSaveMapper.dldtCore(dldt);
            String  dldtInterest=dw1ReadAndSaveMapper.dldtInterest(dldt);
            DataWareHouse dw = new DataWareHouse();

            if(null!=dldtCore){
                dldtCore=dldtCore.trim();
            }

            if(null!=dldtInterest){
                dldtInterest=dldtInterest.trim();
            }

            try{

                dw.setData_date(DateUtil.format(new Date())); // 1:当前日期
                dw.setSource_sys_code(DictConstants.Dw.OPICS_SYS_NAME); // 2:源系统标志
                dw.setBusiness_type(DictConstants.Dw.PROD_MM); // 3
                dw.setAccount_id(ParameterUtil.getString(dldt,"DEALNO","").trim()); // 4:OPICS交易号

                dw.setOrg_unit_code(getOrg5(dldt)); // 5:记账机构号
                dw.setAccount_code(dldtCore); // 6:拆借本金对应的科目号
                dw.setAsset_type(""); // 7:放空
                dw.setProduct_code(getTypeDesc8(dldt)); // 8:拆借-拆入、拆借-拆出
                dw.setBusiness_line_code(""); // 9:放空
                dw.setCurrency_code(ccyCode(dldt)); // 10:得到币种对就的数字，如CNY对应01
                dw.setOrigination_date(ParameterUtil.getString(dldt,"VDATE","").substring(0,10)); // 11
                dw.setMaturity_date(ParameterUtil.getString(dldt,"MDATE","").substring(0,10)); // 12
                dw.setInitial_date(ParameterUtil.getString(dldt,"VDATE","").substring(0,10)); // 13
                dw.setSell_date(ParameterUtil.getString(dldt,"MDATE","").substring(0,10)); // 14
                dw.setPar_balance(Math.abs(ParameterUtil.getDouble(dldt,"CYYAMT",0))); // 15
                dw.setCur_balance(Math.abs(ParameterUtil.getDouble(dldt,"CYYAMT",0))); // 16
                dw.setOrg_balance(Math.abs(ParameterUtil.getDouble(dldt,"CYYAMT",0))); // 17
                dw.setOrg_par_balance(Math.abs(ParameterUtil.getDouble(dldt,"CYYAMT",0)));// 18
                dw.setCur_net_rate(Math.abs(ParameterUtil.getDouble(dldt,"INTRATE",0))); // 19:利率
                dw.setAdjustable_type_code(""); // 20
                dw.setLast_reprice_date(""); // 21
                dw.setReprice_freq(""); // 22
                dw.setReprice_freq_mult(""); // 23
                dw.setNext_reprice_date(""); // 24
                dw.setPayment_type("一次性"); // 25
                dw.setInt_type(""); // 26
                dw.setLast_payment_date(""); // 27

                dw.setPaymnet_freq(getPayInsFreq44and45(dldt).split(",")[0].trim()); // 28
                dw.setPayment_freq_mult(getPayInsFreq44and45(dldt).split(",")[1].trim()); // 29
                dw.setNext_payment_date(""); // 30INTCAPAMT
                dw.setPayment_amount(Double.parseDouble(paymentAmt)); // 31
                dw.setInterest_amount(Double.parseDouble(interestAmt));// 32
                dw.setInvest_gain_amount(0.0); // 33
                dw.setMkt_value_change_amount(0.0); // 34
                dw.setInt_adjust_amount(0.0); // 35
                dw.setCustomer_code(ParameterUtil.getString(dldt,"CNO","").trim()); // 36
                dw.setTransaction_cnt(""); // 37
                dw.setTransaction_purpose(getPurpose38(dldt)); // 38
                dw.setPar_rate("0"); // 39
                dw.setLost_amount(0.0); // 40
                dw.setInterest_account_code(dldtInterest); // 41利息收支科目号
                dw.setInvest_gain_account_code(""); // 42
                dw.setMkt_value_account_code(""); // 43INTPAYCYCLE
                dw.setPaymnet_int_freq(""); // 44
                dw.setPayment_int_freq_mult("");		//45
                String Revflag= ParameterUtil.getString(dldt,"REVDATE","0");
                if(!Revflag.equals("0")){
                    dw.setRevflag("1");
                }
                dw.setRevflag("0");		//46

                dw.setIntime(DateUtil.getCurrentDateAsString("yyyy-MM-dd"));

                String postdate= DateUtil.getCurrentDateAsString("yyyyMMdd");

                //只保正常交易和当天冲销的交易
                if(dw.getRevflag().equals("0")){//未冲销
                    //跑批日期就是卖出日,需要传送,以后则不需要传送
                    if(dw.getSell_date()!=null && DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(dw.getSell_date()))<=0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[到期], 交易号:"+dw.getAccount_id());
                    }
                }else{
                    //冲销交易
                    String revdate = ParameterUtil.getString(dldt,"REVDATE","");
                    //判断冲销日期是否与前一个跑批日相等
                    if(DateUtil.parse(postdate, "yyyy-MM-dd").compareTo(DateUtil.parse(revdate, "yyyy-MM-dd"))==0){
                        //新增
                        dwList.add(dw);
                    }else{
                        JY.info(dw.getBusiness_type()+"交易被过滤掉[冲销], 交易号:"+dw.getAccount_id());
                    }
                }
            }catch(Exception e){
                JY.info(dw.getBusiness_type()+"交易解析错误, 交易号:"+dw.getAccount_id(), e);
            }
        }
    }

    public static String getTypeDesc(Map<String, Object> rprh) throws Exception{
        String prodtype = ParameterUtil.getString(rprh,"PRODTYPE","");
        String result = "";

        if(prodtype.equalsIgnoreCase("GD"))
            result="卖出-国库定期";
        else if(prodtype.equalsIgnoreCase("RB"))
            result="卖出-买断";
        else if(prodtype.equalsIgnoreCase("RP"))
            result="卖出-质押";
        else if(prodtype.equalsIgnoreCase("VB"))
            result="买入-买断";
        else if(prodtype.equalsIgnoreCase("VL"))
            result="买入-理财产品";
        else if(prodtype.equalsIgnoreCase("CB"))
            result="卖出-常备借贷便利-SLF";
        else if(prodtype.equalsIgnoreCase("JY"))
            result="买入-交易所回购";
        else if(prodtype.equalsIgnoreCase("ZD"))
            result="卖出-线下押券";
        else if(prodtype.equalsIgnoreCase("ZQ"))
            result="卖出-中期借贷便利-MLF";


        return result;
    }


    public static String getAccountId4(Map<String, Object> tpos){
        String br = ParameterUtil.getString(tpos,"BR","").trim();
        String secid = ParameterUtil.getString(tpos,"SECID","").trim();
        String cost = ParameterUtil.getString(tpos,"COST","").trim();
        String invtype = ParameterUtil.getString(tpos,"INVTYPE","").trim();
        String port = ParameterUtil.getString(tpos,"PORT","").trim();

        return br+secid+invtype+cost+port;
    }

    public static String getOrg5(String br){

        if(br.equalsIgnoreCase("01"))
            return "0103";
        else if (br.equalsIgnoreCase("02"))
            return "0105";
        else
            return "";
    }

    public static String getAssetType7(Map<String, Object> tpos){
        String asset = ParameterUtil.getString(tpos,"INVTYPE","");
        if("A".equalsIgnoreCase(asset)){
            return "可供出售";
        }else if("H".equalsIgnoreCase(asset)){
            return "持有至到期";
        }else if("T".equalsIgnoreCase(asset)){
            return "交易性";
        }else{
            return "";
        }

    }

    /**
     * 返回利率代码描述(浮动)或"固定利率"
     * @param tpos
     * 
     * @return
     * @throws Exception
     */
    public String getRateCode20(Map<String, Object> tpos,String secid,Map<String, Object> rep) throws Exception{

        String RateCode20="";
        String ratecode =   ParameterUtil.getString(rep,"RATECODE","");
            if("".equals(ratecode) || null==ratecode || "CNYFIX".equals(ratecode)){
                return DictConstants.Dw.RATE_FIXED;
            }else{
                String br = ParameterUtil.getString(tpos,"BR","");
                /**
                 * 取浮动利率代码的描述
                 */
                rep.put("ratecode",ratecode);
                rep.put("br",br);
                rep.put("OPICS", SystemProperties.opicsUserName);
                List<Map<String, Object>> rateMaplist = dw1ReadAndSaveMapper.descr(rep);

                if(rateMaplist!=null && rateMaplist.size()>0){
                    RateCode20=ParameterUtil.getString(rateMaplist.get(0),"DESCR","");
                    if(""!=RateCode20){
                        RateCode20=RateCode20.trim();
                    }
                    return RateCode20;
                }
            }


        return "";
    }

    /**
     * 返回利率调整频率和频率,如"Y,1"
     * @param tpos
     * 
     * @return
     * @throws Exception
     */
    public  String getRePrice(Map<String, Object> tpos,String secid,Map<String, Object> rep) throws Exception{


        String rateDesc = getRateCode20(tpos,secid, rep);
        String intpaycycle="";

        if(DictConstants.Dw.RATE_FIXED.equals(rateDesc)){//固定利率
            return getIntPayCycle(intpaycycle);
        }else{//浮动利率

                intpaycycle = ParameterUtil.getString(rep,"INTPAYCYCLE","").trim();


            return getIntPayCycle(intpaycycle);
        }

    }


    /**
     * 得到付息频率和付息频率乘数
     * @param dldt
     * @return
     */
    public static String getPayInsFreq44and45(Map<String, Object> dldt){

        String intpaycycle = ParameterUtil.getString(dldt,"INTPAYCYCLE","");

        return getIntPayCycle(intpaycycle);
    }


    /**
     * 得到付息频率和付息频率乘数,返回形式如"Y,1"
     *
     * @return
     */
    public static String getIntPayCycle(String intpaycycle){

        String result=" , ";
        if(intpaycycle!=null && !intpaycycle.equals("")){
            if("A".equals(intpaycycle)){
                result="Y,1";
            }else if("S".equals(intpaycycle)){
                result="M,6";
            }else if("Q".equals(intpaycycle)){
                result="M,3";
            }else if("M".equals(intpaycycle)){
                result="M,1";
            }else if("W".equals(intpaycycle)){
                result="D,7";
            }else
                result="D,"+intpaycycle;
        }

        return result;
    }



    /**
     * 下次利率调整日期
     * @param tpos
     * 
     * @return
     * @throws Exception
     */
    public  String getNextRepairRate24(Map<String, Object> tpos,Map<String, Object> secm,String secid,String postdate) throws Exception{

        Map<String, Object> ratemap=new HashMap<>();
        ratemap.put("secid",secid);
        ratemap.put("postdate",postdate);
        ratemap.put("OPICS", SystemProperties.opicsUserName);
        String rateDesc = getRateCode20(tpos,secid,secm);

        if(DictConstants.Dw.RATE_FIXED.equals(rateDesc)){//固定利率
            return "";
        }else{//浮动利率
            List<Map<String, Object>> ramap=dw1ReadAndSaveMapper.nextRate(ratemap);
            if(ramap!=null && ramap.size()>0&&null!=ramap.get(0)) {
                String ratefixdate = ParameterUtil.getString(ramap.get(0), "RATEFIXDATE", "");
                if(""!=ratefixdate){
                    return ratefixdate.substring(0,10);
                }
                return ratefixdate;
            }else {
                return "";
            }
        }
    }



    /**
     * 下次付款日
     * @param tpos
     * 
     * @return
     * @throws Exception
     */
    public  String getNextPayment30(Map<String, Object> tpos,Map<String, Object> secm,String secid,String postdate) throws Exception{

        Map<String, Object> ratemap=new HashMap<>();
        ratemap.put("secid",secid);
        ratemap.put("ipaydate",ParameterUtil.getString(secm,"IPAYDATE",""));
        ratemap.put("OPICS", SystemProperties.opicsUserName);
        String rateDesc = getRateCode20(tpos,secid,secm);

        if(DictConstants.Dw.RATE_FIXED.equals(rateDesc)){//固定利率
            return "";
        }else{//浮动利率
            Map<String,Object>  ramap=dw1ReadAndSaveMapper.nextPay(ratemap);

            String ipaydate= ParameterUtil.getString(ramap,"IPAYDATE","");
            if(""!=ipaydate){
                return ipaydate.substring(0,10);
            }
            return ipaydate;
        }
    }






    /**
     * 上次付款日
     *
     * 2015年5月11日 修改 当利率为浮动利率且未付过息债券需要返回债券起息日
     *
     * @param tpos
     * @return
     * @throws Exception
     */
    public String getLastPayment27(Map<String, Object> tpos,Map<String, Object> secm,String secid,String postdate) throws Exception{

        Map<String, Object> ratemap=new HashMap<>();
        ratemap.put("secid",secid);
        ratemap.put("ipaydate",ParameterUtil.getString(secm,"IPAYDATE",""));
        ratemap.put("OPICS", SystemProperties.opicsUserName);
        String rateDesc = getRateCode20(tpos,secid,secm);

        if(DictConstants.Dw.RATE_FIXED.equals(rateDesc)){//固定利率
            return "";
        }else{//浮动利率


            Map<String,Object>  ramap=dw1ReadAndSaveMapper.nextPay(ratemap);

            if(ramap!=null && ramap.size()>0){

                /**
                 * 2015年5月11日 修改 当利率为浮动利率且未付过息债券需要返回债券起息日
                 * start
                 */
                String ipaydate = ParameterUtil.getString(ramap,"ipaydate","");
                if(StringUtil.isEmptyString(ipaydate))
                    return ParameterUtil.getString(secm,"VDATE","");
                else
                    return ipaydate;
                /**
                 * end
                 */
            }else{
                return ParameterUtil.getString(secm,"VDATE","");
            }
        }
    }
















    /**
     * 本日还本金额，如果当日没有还本，填0
     * @param tpos
     * 
     * @param postdate
     * @return
     * @throws Exception
     */
    public Double getPaymentAmount31(Map<String, Object> tpos,Map<String, Object> secm,String secid,String postdate) throws Exception{
        String qty = ParameterUtil.getString(tpos,"QTY","");

        Map<String, Object> ratemap=new HashMap<>();
        ratemap.put("secid",secid);
        ratemap.put("ipaydate",postdate);
        ratemap.put("OPICS", SystemProperties.opicsUserName);
        List<Map<String,Object> > ramap=dw1ReadAndSaveMapper.payAmt(ratemap);
        if(ramap!=null && ramap.size()>0) {
            Double payamt = ParameterUtil.getDouble(ramap.get(0), "PRINPAYAMT_8", 0.0);
            return payamt;
        }else {
            return 0.0;
        }

    }


    /**
     * 上次利率调整日期
     * @param tpos
     * 
     * @return
     * @throws Exception
     */
    public  String getLastRepairRate21(Map<String, Object> tpos,Map<String, Object> secm,String secid,String postdate) throws Exception {


        String rateDesc = getRateCode20(tpos, secid, secm);

        if (DictConstants.Dw.RATE_FIXED.equals(rateDesc)) {//固定利率
            return "";
        } else {//浮动利率
            Map<String, Object> ratemap = new HashMap<>();
            ratemap.put("secid", secid);
            ratemap.put("postdate", postdate);
            ratemap.put("OPICS", SystemProperties.opicsUserName);
            List<Map<String, Object>> ramap = dw1ReadAndSaveMapper.LastRepairRate(ratemap);
            if (ramap != null && ramap.size() > 0) {
                String lastRepairRate = ParameterUtil.getString(ramap.get(0), "RATEFIXDATE", "");
                if(""!=lastRepairRate){
                    return lastRepairRate.substring(0,10);
                }
                return lastRepairRate;

            } else {
                return "";
            }
        }

    }
//本金科目代码
//    public  String getPrincipalCoreNo6(Map<String, Object> tpos){
//
//        String invtype = ParameterUtil.getString(tpos, "INVTYPE", "");
//        String principalCoreNo6="";
//        if(invtype.equals("A")){
//
//            tpos.put("code", "2250");
//            tpos.put("glno", "141");
//            principalCoreNo6 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
//
//        }else if(invtype.equals("T")){
//
//            tpos.put("code", "2280");
//            tpos.put("glno", "14");
//            principalCoreNo6 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
//        }else if(invtype.equals("H")){
//
//            tpos.put("code", "2260");
//            tpos.put("glno", "142");
//            principalCoreNo6 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
//        }
//      if(null!=principalCoreNo6&&""!=principalCoreNo6){
//          principalCoreNo6=principalCoreNo6.trim();
//      }
//        return principalCoreNo6;
//    }



//科目号
    public  String InterestCoreNo41(Map<String, Object> tpos,String type) {
        String invtype = ParameterUtil.getString(tpos, "INVTYPE", "");

        String interestCoreNo41 ="";
        //--公允价值变动损益科目(债券) 1
       if(type.equals("1")) {

           if (invtype.equals("T")) {
               tpos.put("code", "2440");
               tpos.put("glno", "620");
//          BR = '01' AND code = '2440' and glno like '620%'
            interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           } else if (invtype.equals("A")) {
               tpos.put("code", "2410");
               tpos.put("glno", "340");
//        code = '2410' and glno like '340%'
              interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
           }
       } //利息收支科目号
       else if (type.equals("2")) {

           if (invtype.equals("T")) {
               tpos.put("code", "2400");
               tpos.put("glno", "44");
           interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);


           } else if (invtype.equals("A")) {
               tpos.put("code", "2370");
               tpos.put("glno", "441");
                interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           }else  if(invtype.equals("H")){
               tpos.put("code", "2380");
               tpos.put("glno", "442");
               interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           }

       }
       //投资收益科目代码
           else if(type.equals("3")){
           if (invtype.equals("A")) {
               tpos.put("code", "2450");
               tpos.put("glno", "690");
            interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           } else if (invtype.equals("H")) {
               tpos.put("code", "2460");
               tpos.put("glno", "621");
             interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           }

       }//本金科目（债券）
           else if(type.equals("4")){

           if(invtype.equals("A")){

               tpos.put("code", "2250");
               tpos.put("glno", "141");
               interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);

           }else if(invtype.equals("T")){

               tpos.put("code", "2280");
               tpos.put("glno", "14");
               interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
           }else if(invtype.equals("H")){

               tpos.put("code", "2260");
               tpos.put("glno", "142");
               interestCoreNo41 = dw1ReadAndSaveMapper.PrincipalCoreNo6(tpos);
           }

       }

        if(null!=interestCoreNo41&&""!=interestCoreNo41){
            interestCoreNo41=interestCoreNo41.trim();
        }
        return interestCoreNo41;
    }



        //--本金支付科目(同业存放)
     public  String callCore(Map<String, Object> call,String SEQ){
      //本金
        if(SEQ.equals("3")){
            call.put("SEQ","3");
            Map<String, Object> callCore=dw1ReadAndSaveMapper.callCore(call);
          return ParameterUtil.getString(callCore, "TEXT", "");
      }//利息
        else if(SEQ.equals("5")){
            call.put("SEQ","5");

            Map<String, Object>  callCore=dw1ReadAndSaveMapper.callCore(call);
            return ParameterUtil.getString(callCore, "TEXT", "");

        }else {
          return "";
      }
    }


    /**
     * 国际业务部：7101  COST非8开头
     * 金融市场部：0103  COST为8开头
     * 投    行：0105  BR=02
     * @param call
     * @return
     */
    public static String getOrg5(Map<String, Object> call){
        String cost = ParameterUtil.getString(call, "COST", "");
        String br = ParameterUtil.getString(call, "BR", "");

        if(br.equalsIgnoreCase("02"))
            return "0105";
        else if (cost.startsWith("8"))
            return "0103";
        else
            return "7101";
    }


    /**
     * 返回管理会计的产品代码，如 拆借-拆入、拆借-拆出
     *
     *
     * @return
     * @throws Exception
     */
    public String getTypeDesc8(Map<String, Object>  rep) throws Exception{
        String product = ParameterUtil.getString(rep, "PRODUCT", "").trim();
//        StringBuffer typeSql = new StringBuffer();
//        typeSql.append("SELECT al from type where type='");
//        typeSql.append(prodtype);
//        typeSql.append("' and prodcode='MM'");

        List<Map<String, Object>> typedesc = dw1ReadAndSaveMapper.TypeDesc(rep);
        String result="";
        if(typedesc!=null && typedesc.size()>0){
            String al = ParameterUtil.getString(typedesc.get(0), "AL", "");
            if(product.equals("MM")){
                if(al.equalsIgnoreCase("A"))
                    result="拆借-拆出";
                else if(al.equalsIgnoreCase("L"))
                    result="拆借-拆入";
            }
          else {
                if(al.equalsIgnoreCase("A"))
                    result="存放同业";
                else if(al.equalsIgnoreCase("L"))
                    result="同业存放";
            }
        }

        return result;
    }

    public String  ccyCode(Map<String, Object>  rep){

        String ccyCode= dw1ReadAndSaveMapper.ccyCode(rep);

        return ccyCode;
     }


    /**
     * 得到 交易目的 (投资/流动性)
     *
     * 通过DLDT.COST到COST表中查询COSTDESC字段
     * 如果有"投资"描述则此处填写"投资性",若有"流动性"描述则此处填写"流动性",否则填""
     * @param dealMap
     * @return
     * @throws Exception
     */
    public  String getPurpose38(Map<String, Object> dealMap) throws Exception{
        String cost =ParameterUtil.getString(dealMap, "COST", "").trim();

        dealMap.put("costcent",cost);

        String  costdesc= dw1ReadAndSaveMapper.costdesc(dealMap);

        if(costdesc.contains("投"))
            return "投资性";
        else if(costdesc.contains("流"))
            return "流动性";
        else
            return "";
    }


    /**
     * 活期：当前时间;(ACCT.MATDATE IS NULL) 定期：ACCT.OPENDATE
     */
    public static String getVdate(Map<String, Object> call,String POSTDATE) {
        String matdate = ParameterUtil.getString(call, "MATDATE", "");
        if(matdate.equals("") || matdate==null){//活期
            return POSTDATE;
        }else{//定期
            String opendate = ParameterUtil.getString(call, "OPENDATE", "").substring(0,10);
            return opendate;
        }
    }


    /**
     * 活期：明天;(ACCT.MATDATE IS NULL)定期：ACCT.MATDATE

     */
    public static String getMdate(Map<String, Object> call,String POSTDATE) throws ParseException {
        String matdate = ParameterUtil.getString(call, "MATDATE", "");
        if(matdate.equals("") || matdate==null){//活期

            String nextDay = DateUtil.getNextDayOfMonth(POSTDATE, "yyyy-MM-dd");
            return nextDay;
        }else{//定期
            return matdate;
        }
    }



    @Override
    public void terminate() {

    }
}
