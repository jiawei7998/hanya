package com.singlee.ifs.job;


import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.hrbextra.wind.Util.WindUtils;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.file.entity.FileName;
import com.singlee.ifs.file.mapper.FileNamemapper;
import com.singlee.ifs.mapper.IfsWdFundMapper;
import com.singlee.ifs.model.ChinaMutualFundNAV;
import com.singlee.ifs.model.IbFundPriceDay;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 万得 基金净值信息
 */
public class ChinaMutualFundNAVJobManage implements CronRunnable {

    private static Logger logManager = LoggerFactory.getLogger(ChinaMutualFundNAVJobManage.class);
    //    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
//    private IfsCorePositionMapper corePositionMapper = SpringContextHolder.getBean(IfsCorePositionMapper.class);
//
    private static BatchDao batchDao = SpringContextHolder.getBean(BatchDao.class);


    private IfsWdFundMapper ifsWdFundMpper = SpringContextHolder.getBean(IfsWdFundMapper.class);

    private FileNamemapper fileNamemapper = SpringContextHolder.getBean(FileNamemapper.class);


    @Override
    public boolean execute(Map<String, Object> arg0) throws Exception {
        logManager.info("==================在本地创建文件夹==============================");
        Date spotDate = new Date();
        String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);
        //本地文件文件保存目录(压缩文件)
        String fileGZPath = SystemProperties.marketdataLocalfilenameFundNAVGZpath;
        //本地文件文件保存目录(解压后文件)
        String filePath = SystemProperties.marketdataLocalfilenameFundNAVpath;
        //远程下载文件夹名
      //  String fileName = SystemProperties.marketdataWindfilenameFundNAV;
        String fileName= "/appdata/was/interface/windFund/ChinaMutualFundNAV/";

        File filedir = new File(fileGZPath);
        //创建文件夹
        if (!filedir.exists()) {
            filedir.mkdirs();
        }

        File filedirPath = new File(filePath);
        //创建文件夹
        if (!filedirPath.exists()) {
            filedirPath.mkdirs();
        }
        JY.info("==================开始执行 准备连接sftp下载万得债券文件==============================");
        // 初始化FTP
        SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.windsftpId, SystemProperties.windsftpPort, SystemProperties.windsftpUser, SystemProperties.windsftpPasswd);
        // 开始连接
        sftp.connect();
        // 下载文件 （远程下载目录、本地保存目录、下载文件格式、下载文件格式）
        List<String> filenames = sftp.batchDownLoadFile( fileName,fileGZPath, null, ".gz", false);
        sftp.disconnect();
        List<FileName> FileList = new ArrayList<>();
        List<String> files = fileNamemapper.queryAllByLimit();
        //如果数据库里没有数据，则全部解析，全部入表（第一次执行）
        if (files.size() != filenames.size()) {
            for (int i = 0; i < filenames.size(); i++) {
                //表里没有的文件名才操作
                if (!files.contains(filenames.get(i))) {
                    //绝对路径
                    String filename = filenames.get(i);
                    //解压前文件名
                    String realName=WindUtils.getFullName(filename);
                    FileName FileBean = new FileName();
                    FileBean.setFilename(filename);
                    FileBean.setInputdate(postDate);
                    FileList.add(FileBean);
                    JY.info("==================开始解压文件==============================");
                    //解压文件的绝对地址
                    String FileNamePath = fileGZPath + realName;
                    JY.info("==================解压文件绝对路径为：" + FileNamePath);
//                    //解压后文件名
                  String reFilename = WindUtils.getFileName(realName);
                    //解压后文件的绝对路径为
                    String OutFileNamePath = filePath + reFilename;
                    JY.info("==================解压后文件绝对路径为：" + OutFileNamePath);
                    boolean flag = WindUtils.doUncompressFile(FileNamePath, OutFileNamePath);
                    if (flag) {
                        JY.info("=========开始解压文件=========");
                        boolean intoFlag = intoDate(OutFileNamePath);
                        if (!intoFlag) {
                            JY.info("=========解析文件插入失败=========");
                            return false;
                        }
                    }
                }
            }
            batchDao.batch("com.singlee.ifs.file.mapper.FileNamemapper.insertall", FileList);
        } else {
            JY.info("=========没有文件需要更新=========");
        }
        return true;
    }


    @Override
    public void terminate() {
        // TODO Auto-generated method stub
    }

    public void convertValue(Field field, Object obj, String value)
            throws Exception {
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if ("class java.lang.Integer".equals(field.getGenericType().toString())) {
            field.set(obj, Integer.parseInt(value));
        } else if ("boolean".equals(field.getGenericType().toString())) {
            field.set(obj, Boolean.parseBoolean(value));
        } else if ("class java.util.Date".equals(
                field.getGenericType().toString())) {
            field.set(obj, sim.parse(value));
        } else if ("class java.math.BigDecimal".equals(
                field.getGenericType().toString())) {
            field.set(obj, new BigDecimal(value));
        } else {
            field.set(obj, value);
        }

    }

    public boolean intoDate(String fileName) {
        List<ChinaMutualFundNAV> lists = new ArrayList<ChinaMutualFundNAV>();
        List<IbFundPriceDay> priceList = new ArrayList<IbFundPriceDay>();
        SAXReader saxReader = new SAXReader();
        Document doc = null;
        Class<ChinaMutualFundNAV> cls = ChinaMutualFundNAV.class;
        try {
            // String pathname = SystemProperties.marketdataLocalfilenameFundNAV;
            doc = saxReader.read(new File(fileName));
            Element root = doc.getRootElement();
            List<Element> list = root.elements("Product");

            for (Element e : list) {
                List<Element> li = e.elements();
                Class<?> cl = (Class<?>) Class.forName(cls.getName());
                Object ob = cl.newInstance();
                IbFundPriceDay ibFundPriceDay = new IbFundPriceDay();
                ChinaMutualFundNAV chinaMutualFundNAV = new ChinaMutualFundNAV();
                for (Element element2 : li) {
                    String name = element2.getName();
                    final Field[] declaredFields = ChinaMutualFundNAV.class.getDeclaredFields();
                    for (Field fiels:declaredFields){
                        String cName= fiels.getName();
                        if (name.equals(cName)){
                            String value = element2.getText();
                            Field field = ob.getClass().getDeclaredField(name);
                            field.setAccessible(true);
                            convertValue(field, ob, value);
                        }
                    }

                }
                chinaMutualFundNAV = (ChinaMutualFundNAV) ob;
               if (chinaMutualFundNAV.getF_INFO_WINDCODE()!=null&&chinaMutualFundNAV.getANN_DATE()!=null){


                String date = chinaMutualFundNAV.getANN_DATE().trim();
                StringBuffer sb = new StringBuffer(date);
                sb.insert(6, "-");
                sb.insert(4, "-");

                ibFundPriceDay.setFundPrice(chinaMutualFundNAV.getF_NAV_UNIT());//单位净值/万分收益率
                ibFundPriceDay.setFundCode(chinaMutualFundNAV.getF_INFO_WINDCODE());//基金代码
                ibFundPriceDay.setPostdate(String.valueOf(sb));//账务日期
                lists.add((ChinaMutualFundNAV) ob);
                priceList.add(ibFundPriceDay);
               }

            }

            logManager.info("==================报表数据解析完成，开始插入前置库==============================");

            if (lists != null && lists.size() > 0) {
                for (ChinaMutualFundNAV chinaMutualFundNAV : lists) {
                    ifsWdFundMpper.deleteFund(chinaMutualFundNAV);
                    logManager.info("==================实体类为" + chinaMutualFundNAV.toString());
                    ifsWdFundMpper.insertFund(chinaMutualFundNAV);

                }
            }
            logManager.info("==================单位净值表插入完成，开始插入万份收益表表==============================");

            if (priceList.size() > 0) {
                batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.delete", priceList);
                batchDao.batch("com.singlee.ifs.mapper.IbFundPriceDayMapper.insert", priceList);
            }

        } catch (DocumentException e) {
            e.printStackTrace();
            logManager.error("DocumentException异常：",e);
            return false;
        } catch (InstantiationException e) {
            e.printStackTrace();
            logManager.error("InstantiationException异常：",e);
            return false;
        } catch (NoSuchFieldException e) {
            logManager.error("NoSuchFieldException异常：",e);
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logManager.error("IllegalAccessException异常：",e);
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logManager.error("ClassNotFoundException异常：",e);
            return false;
        } catch (Exception exception) {
            exception.printStackTrace();
            logManager.error("Exception异常：",exception);
            return false;
        }
        logManager.info("==================End ChinaMutualFundNAVJobManage execute==============================");
        return true;
    }
}
