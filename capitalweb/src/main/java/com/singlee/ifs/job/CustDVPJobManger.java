package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.financial.bean.SlPmtqBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IPmtqServer;
import com.singlee.ifs.model.IfsCustDVP;
import com.singlee.ifs.service.IfsCustDVPService;
import com.singlee.ifs.service.IfsOpicsCustService;
import com.singlee.slbpm.service.FlowLogService;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Liang
 * @date 2021/10/28 17:17
 * =======================
 */
public class CustDVPJobManger implements CronRunnable {

    private IPmtqServer pmtqService = SpringContextHolder.getBean("IPmtqServer");

    private IfsCustDVPService ifsCustDVPService = SpringContextHolder.getBean(IfsCustDVPService.class);

    private IfsOpicsCustService ifsOpicsCustService = SpringContextHolder.getBean(IfsOpicsCustService.class);

    private FlowLogService flowLogService = SpringContextHolder.getBean(FlowLogService.class);

    private TaUserMapper taUserMapper = SpringContextHolder.getBean(TaUserMapper.class);

    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");

    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {
        JY.info("[JOBS-START] ==> 开始执行【opics -->前置表】，DVP数据取回定时任务，开始······");
        List<IfsCustDVP> list1 = new ArrayList<IfsCustDVP>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
//        // 获取当前账务日期
        Date branchDate = baseServer.getOpicsSysDate("01");
        String cdate = new SimpleDateFormat("yyyy-MM-dd").format(branchDate);
        List<SlPmtqBean> pmtqBeans = pmtqService.selectBySetmeansAndProduct(cdate);
        pmtqBeans.forEach(e -> {
            IfsCustDVP ifsCustDVP = new IfsCustDVP();
            ifsCustDVP.setBr(StringUtils.trimToEmpty(e.getBr()));
            ifsCustDVP.setCno(StringUtils.trimToEmpty(e.getCno()));
            ifsCustDVP.setProduct(StringUtils.trimToEmpty(e.getProduct()));
            ifsCustDVP.setAmount(new BigDecimal(e.getAmount()));
            ifsCustDVP.setDealno(StringUtils.trimToEmpty(e.getDealno()));
            ifsCustDVP.setType(StringUtils.trimToEmpty(e.getType()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            ifsCustDVP.setVdate(sdf.format(e.getVdate()));
            ifsCustDVP.setSeq(StringUtils.trimToEmpty(e.getSeq()));
            ifsCustDVP.setPayrecind(StringUtils.trimToEmpty(e.getPayrecind()));

            //查询条件
            map1.put("type", ifsCustDVP.getType());
            map1.put("dealno", ifsCustDVP.getDealno());
            map1.put("seq", ifsCustDVP.getSeq());
            map1.put("product", ifsCustDVP.getProduct());
            map1.put("payrecind", ifsCustDVP.getPayrecind());
            //判断是否已存在相同数据
            List<IfsCustDVP> result = ifsCustDVPService.selectCustDVP(map1);
            if (result != null && result.size() > 0) {
                return;
            }

            map2.put("cno", StringUtils.trimToEmpty(e.getCno()));
            //根据客户号查询客户名称
            String cfn = ifsOpicsCustService.searchCnameByCno(map2);
            ifsCustDVP.setCname(cfn);

            //根据dealno查询fedealno
            List<String> list3 = pmtqService.queryInthBydealno(e.getDealno());
            if (list3.size() > 0) {
                String serial_no = list3.get(0);
                ifsCustDVP.setFedealno(serial_no);

                //根据fedealno查询审批经办人员
                List<String> list2 = flowLogService.selectBySerialNo(serial_no);
                if (list2 != null && list2.size() > 0) {
                    List<String> listUserName = taUserMapper.getAllUserByUserId(list2);
                    ifsCustDVP.setIoper(listUserName.size() > 0 ? listUserName.get(0) : "");
                    ifsCustDVP.setVoper(listUserName.size() > 1 ? listUserName.get(1) : "");
                    ifsCustDVP.setCcyauthoper(listUserName.size() > 2 ? listUserName.get(2) : "");
                    ifsCustDVP.setSecauthoper(listUserName.size() > 3 ? listUserName.get(3) : "");
                    ifsCustDVP.setAuthoriza(listUserName.size() > 4 ? listUserName.get(4) : "");
                }
            }
            list1.add(ifsCustDVP);
            return;
        });
        if (list1!=null&&list1.size()>0){
            ifsCustDVPService.addCustDVP(list1);
        }
        JY.info("[JOBS-END] ==> 开始执行【opics -->前置表】，DVP数据取回定时任务，结束······");
        return true;
    }

    @Override
    public void terminate() {

    }
}
