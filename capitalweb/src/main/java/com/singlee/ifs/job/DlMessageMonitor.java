package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.generated.model.field.Field20;
import com.singlee.generated.model.field.Field21;
import com.singlee.generated.model.field.Field79;
import com.singlee.generated.model.mt.mt2xx.MT299;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIboMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.ifs.model.IfsCfetsfxLend;
import com.singlee.ifs.model.IfsCfetsrmbIbo;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.swift.model.LogicalTerminalAddress;
import com.singlee.swift.model.SwiftBlock1;
import com.singlee.swift.model.SwiftBlock2.MessagePriority;
import com.singlee.swift.model.SwiftBlock2Input;
import com.singlee.swift.model.SwiftBlock4;
import com.singlee.swift.model.field.SwiftParseUtils;
import com.singlee.swift.model.mt.ServiceIdType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DlMessageMonitor implements CronRunnable {
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor= SpringContextHolder.getBean("IBaseServer");

	private IfsCfetsfxLendMapper cfetsfxLendMapper= SpringContextHolder.getBean(IfsCfetsfxLendMapper.class);

	private IfsCfetsrmbIboMapper cfetsrmbIboMapper= SpringContextHolder.getBean(IfsCfetsrmbIboMapper.class);

	private static Logger logManager = LoggerFactory.getLogger(DlMessageMonitor.class);
	private IfsSwiftMapper ifsSwiftMapper=SpringContextHolder.getBean(IfsSwiftMapper.class);
	private IfsOpicsCustMapper ifsOpicsCustMapper=SpringContextHolder.getBean(IfsOpicsCustMapper.class);
	private IBaseServer baseserver=SpringContextHolder.getBean(IBaseServer.class);
	

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		logManager.info("==================Start DlMessageMonitor execute==============================");
		
		/**
		 * 拆借模块分为信用拆借和外币拆借
		 */
		// 查询全部未复核
		List<IfsCfetsfxLend> listLend = cfetsfxLendMapper.getCfetsfxLendList();
		for (IfsCfetsfxLend ifsCfetsfxLend : listLend) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.DL.SERVER);
			inthBean.setTag(SlDealModule.DL.TAG);
			inthBean.setBr(ifsCfetsfxLend.getBr());
			inthBean.setFedealno(ifsCfetsfxLend.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				logManager.info("==========Start DlMessageMonitor ifsCfetsfxLend dealno dealno ="+slInthBean.getDealno()+"=============");
				cfetsfxLendMapper.updateFxLendByFedealno(map);
				IfsOpicsCust cust=ifsOpicsCustMapper.searchIfsOpicsCust(ifsCfetsfxLend.getCounterpartyInstId());
				if(StringUtils.isNotEmpty(ifsCfetsfxLend.getRefId())&"BCOH".equals(cust.getCname())&StringUtils.isNotEmpty(slInthBean.getDealno())){ //交通银行
					logManager.info("==========Create MT299 Message=============");
					MT299 mt299=new MT299();
					SwiftBlock1 block1 = new SwiftBlock1();
				    block1.setApplicationId(SwiftBlock1.APPLICATION_ID_FIN);
				    block1.setServiceId(ServiceIdType._01.number());
				    block1.setLogicalTerminal(new LogicalTerminalAddress("DLCBCNBD").getSenderLogicalTerminalAddress());
				    block1.setSessionNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSessionNum(), 4, "0"));
				    block1.setSequenceNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSequenceNum(),6, "0"));
					mt299.getSwiftMessage().setBlock1(block1);
				    SwiftBlock2Input block2Input =new SwiftBlock2Input();
				    block2Input.setMessageType(MT299.NAME);
				    block2Input.setReceiverAddress(new LogicalTerminalAddress(cust.getBic()).getReceiverLogicalTerminalAddress());
				    block2Input.setMessagePriority(MessagePriority.N.name());
					mt299.getSwiftMessage().setBlock2(block2Input);
					SimpleDateFormat sdf=new SimpleDateFormat("MMM.dd yyyy",Locale.ENGLISH);
					SimpleDateFormat sdOld=new SimpleDateFormat("yyyy-MM-dd");

					StringBuffer newStr = new StringBuffer();
					newStr.append(MessageFormat.format("ACCORDING TO THE(OFFSHORE INTERBANK FACILITY AGREEMENT) REFERENCE NUMBERE:{0}",ifsCfetsfxLend.getRefId())).append("\r\n");
					newStr.append(MessageFormat.format("SIGNED BETWEEN YOUR BANK COMMUNICATIONS CO.,LTD.AND OUR BANK OF DALIAN CO.," +
							"LTD,DATE:{0}",(sdf.format(sdOld.parse(ifsCfetsfxLend.getForDate())))).toUpperCase()).append("\r\n");
					newStr.append("WE,AS THE BORROWER WOULD LIKETO APPLY FOR A LOAN WITH DETAILS AS FOLLOWS:").append("\r\n");
					newStr.append(".").append("\r\n").append("\r\n");
					newStr.append(MessageFormat.format("VALUE DATE:{0}",ifsCfetsfxLend.getValueDate())).append("\r\n");
					newStr.append(MessageFormat.format("MARTURITY DATE:{0}",ifsCfetsfxLend.getMaturityDate())).append("\r\n");
					newStr.append(MessageFormat.format("DAYS:{0}",ifsCfetsfxLend.getOccupancyDays())).append("\r\n");
					newStr.append(MessageFormat.format("PRINCIPAL AMOUNT:{0}{1}",ifsCfetsfxLend.getCurrency(),ifsCfetsfxLend.getAmount())).append("\r\n");
					newStr.append(MessageFormat.format("INTEREST RATE:{0}",ifsCfetsfxLend.getRate())).append("\r\n");
					newStr.append(MessageFormat.format("INTEREST AMOUTN:{0}{1}",ifsCfetsfxLend.getCurrency(),ifsCfetsfxLend.getInterest())).append("\r\n");
					newStr.append(MessageFormat.format("HANDING CHARGE RATE:{0}",0)).append("\r\n");
					newStr.append(MessageFormat.format("HANDING CHARGE AMOUTN:{0}",0)).append("\r\n");
					newStr.append(".").append("\r\n").append("\r\n");
					newStr.append(MessageFormat.format("PLEASE REMIT THE PRINCIPLE AMOUNT TO OUR ACCOUNT WITH CITIBANK N.A," +
							"NEW YORK BRAMCH(SWIFT CODE:CITIUS33),ACCOUNT NO.{0}",ifsCfetsfxLend.getCounterpartyInstId())).append("\r\n");
					newStr.append("WE IRREVOCABLY AND UN CONDITIONALLY UNERTAKE TO REPAY TO YOU REFF AND CLEAR OF ANY " +
							"FEES AS ABOVE MENTIONED UPON MATURITY DATE TO YOUR ACCOUNT NUMBER SPECIFIED IN YOUR SWIFT CONFIRMATION").append("\r\n");
				    SwiftBlock4 block4=new SwiftBlock4();
					block4.append(new Field20(MessageFormat.format("DLCB{0}",slInthBean.getDealno())));					
					block4.append(new Field21(ifsCfetsfxLend.getRefId()));
					List<String> resultMsg = SwiftParseUtils.splitComponents(newStr.toString(),48);//按照48个字符分割
					Field79 field = new Field79();
					resultMsg = (resultMsg.size() > 34) ? resultMsg.subList(0, 34) : resultMsg;//只截取35行超出部分舍去.
					for (int i = 0; i < resultMsg.size(); i++) {
						field.setComponent(i+1,resultMsg.get(i).replace("\r",""));
					}
					block4.append(field);
					mt299.getSwiftMessage().setBlock4(block4);
					//System.out.println(mt299.message());
					SlSwiftBean swiftBean=new SlSwiftBean();
					String batchNo=DateUtil.format(baseserver.getOpicsSysDate(slInthBean.getBr()),"yyMMdd")+ifsCfetsfxLend.getTicketId().substring(ifsCfetsfxLend.getTicketId().length()-6);
					swiftBean.setBr(slInthBean.getBr());
					swiftBean.setBatchNo(batchNo);
					swiftBean.setDealNo(slInthBean.getDealno());
					swiftBean.setProdCode(ifsCfetsfxLend.getProduct());
					swiftBean.setMsgType(MT299.NAME);
					swiftBean.setMsg(mt299.message());
					swiftBean.setProdType(ifsCfetsfxLend.getProdType());
					Date dt = Calendar.getInstance().getTime();
					swiftBean.setSwftcDate(dt);
					swiftBean.setCdate(dt);
					ifsSwiftMapper.insertSwiftData(swiftBean);
					logManager.info("==========Create MT299 Message End =============");
				}
			
			}
		}
		//添加清算信息类
		CommonSettleManager settleManager=new CommonSettleManager();
		
		//信用拆借
		List<IfsCfetsrmbIbo> listIbo = cfetsrmbIboMapper.getCfetsIboList();
		for (IfsCfetsrmbIbo ifsCfetsrmbIbo : listIbo) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.DL.SERVER);
			inthBean.setTag(SlDealModule.DL.TAG);
			inthBean.setBr(ifsCfetsrmbIbo.getBr());
			inthBean.setFedealno(ifsCfetsrmbIbo.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				logManager.info("==========开始执行DlMessageMonitor ifsCfetsrmbIbo dealno dealno ="+slInthBean.getDealno()+"=============");
				cfetsrmbIboMapper.updateCfetsIboByFedealno(map);
				// 清算相关处理
				if(!"1".equals(ifsCfetsrmbIbo.getNettingStatus())){//不是净额清算的流向大额
					String br =StringUtils.trimToEmpty(slInthBean.getBr());
					String dealno =StringUtils.trimToEmpty(slInthBean.getDealno());
					String fedealno =StringUtils.trimToEmpty(slInthBean.getFedealno());
					String product =StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProduct());
					String prodtype =StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProdType());
					if (!"".equals(dealno)) {
						logManager.info("==========开始执行DlMessageMonitor ifsCfetsrmbIbo Settlement dealno ="+slInthBean.getDealno()+"=============");
						settleManager.addSettle(br, dealno,fedealno, product,prodtype);
						//logManager.info("==========开始执行DlMessageMonitor ifsCfetsrmbIbo swift dealno ="+slInthBean.getDealno()+"=============");
						//settleManager.addSwift(br, dealno, product,prodtype);
					}//清算end
				}
				
			}
		}
		//获取当天，净额清算的swift中dealno不存在的交易
		List<IfsCfetsrmbIbo> listIbo2 = cfetsrmbIboMapper.searchSwiftNotExist();
		for (IfsCfetsrmbIbo ifsCfetsrmbIbo : listIbo2) {
			String br = "01";
			String dealno = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getDealNo());
			String product = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProduct());
			String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProdType());
			logManager.info("==========开始执行 信用拆借swift dealno ="+dealno+"=============");
			settleManager.addSwift(br, dealno, product,prodtype);
		}
		
		logManager.info("==================End DlMessageMonitor execute==============================");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
