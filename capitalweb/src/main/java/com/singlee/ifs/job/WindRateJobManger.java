package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得产品利率数据
 *
 */
public class WindRateJobManger implements CronRunnable {

	private WindRateService windRateService = SpringContextHolder.getBean(WindRateService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindRateJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得产品利率数据==============================");
		Boolean run = windRateService.run();
		logManager.info("==================万得产品利率数据处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
