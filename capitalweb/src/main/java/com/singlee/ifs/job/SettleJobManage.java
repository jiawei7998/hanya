/**
 * Project Name:capitalweb
 * File Name:SettleJobManage.java
 * Package Name:com.singlee.ifs.job
 * Date:2018-9-15上午09:31:32
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;
import com.singlee.ifs.model.IfsCspiCsriHead;
import com.singlee.ifs.model.IfsSdvpHead;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName:SettleJobManage <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-9-15 上午09:31:32 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SettleJobManage implements CronRunnable{
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor= SpringContextHolder.getBean("IBaseServer");
	private IfsOpicsSettleManageMapper opicsSettleManageMapper = SpringContextHolder.getBean(IfsOpicsSettleManageMapper.class);
	
	private static Logger logManager = LoggerFactory.getLogger(SettleJobManage.class);
	

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		logManager.info("==================开始执行SettleJobManage==============================");
		// 查询全部已同步未确定是否成功的
 		List<IfsCspiCsriHead> listCspiCsri = opicsSettleManageMapper.getSyncCspiCsriList();
 		for (IfsCspiCsriHead ifsCspiCsriHead : listCspiCsri) {
 			SlInthBean inthBean = new SlInthBean();
 			if("P".equals(ifsCspiCsriHead.getPayrecind())){
 				inthBean.setServer(SlDealModule.SETTLE.SERVER_CSPI);
 				inthBean.setTag(SlDealModule.SETTLE.TAG_CSPI);
 			}else{
 				inthBean.setServer(SlDealModule.SETTLE.SERVER_CSRI);
 				inthBean.setTag(SlDealModule.SETTLE.TAG_CSRI);
 			}
 			inthBean.setBr(SlDealModule.BASE.BR);
			inthBean.setFedealno(ifsCspiCsriHead.getFedealno());
			// 只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				opicsSettleManageMapper.updateCspiCsriByFedealno(map);
			}
		}
 		// 查询全部已同步已确定是否成功的
 		List<IfsCspiCsriHead> listSCspiCsris = opicsSettleManageMapper.getSyncSCspiCsriList();
 		for (IfsCspiCsriHead ifsCspiCsriHead : listSCspiCsris) {
 			SlInthBean inthBean = new SlInthBean();
 			if("P".equals(ifsCspiCsriHead.getPayrecind())){
 				inthBean.setServer(SlDealModule.SETTLE.SERVER_CSPI);
 				inthBean.setTag(SlDealModule.SETTLE.TAG_CSPI);
 			}else{
 				inthBean.setServer(SlDealModule.SETTLE.SERVER_CSRI);
 				inthBean.setTag(SlDealModule.SETTLE.TAG_CSRI);
 			}
 			inthBean.setBr(SlDealModule.BASE.BR);
			inthBean.setFedealno(ifsCspiCsriHead.getFedealno());
			// 只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			if(slInthBean!=null){
				if("-4".equals(slInthBean.getStatcode().trim())){
					ifsCspiCsriHead.setStatus("2");
				}else{
					ifsCspiCsriHead.setStatus("3");
				}
				opicsSettleManageMapper.updateSyncStatus(ifsCspiCsriHead);
			}
		}
 		
 	// 查询全部已同步未确定是否成功的
 		List<IfsSdvpHead> listSdvp = opicsSettleManageMapper.getSyncSdvp();
 		for (IfsSdvpHead ifsSdvpHead : listSdvp) {
 			SlInthBean inthBean = new SlInthBean();
 			inthBean.setServer(SlDealModule.SETTLE.SERVER_SDVP);
 			inthBean.setTag(SlDealModule.SETTLE.TAG_SDVP);
 			inthBean.setBr(SlDealModule.BASE.BR);
			inthBean.setFedealno(ifsSdvpHead.getFedealno());
			// 只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				opicsSettleManageMapper.updateSdvpByFedealno(map);
			}
		}
 		// 查询全部已同步已确定是否成功的
 		List<IfsSdvpHead> listSSdvp = opicsSettleManageMapper.getSyncSSdvpList();
 		for (IfsSdvpHead ifsSdvpHead : listSSdvp) {
 			SlInthBean inthBean = new SlInthBean();
 			inthBean.setServer(SlDealModule.SETTLE.SERVER_SDVP);
 			inthBean.setTag(SlDealModule.SETTLE.TAG_SDVP);
 			inthBean.setBr(SlDealModule.BASE.BR);
			inthBean.setFedealno(ifsSdvpHead.getFedealno());
			// 只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			if(slInthBean!=null){
				if("-4".equals(slInthBean.getStatcode().trim())){
					ifsSdvpHead.setStatus("2");
				}else{
					ifsSdvpHead.setStatus("3");
				}
				opicsSettleManageMapper.updateSdvpSyncStatus(ifsSdvpHead);
			}
		}
 		logManager.info("==================执行完成SettleJobManage==============================");
		return false;
	}

	@Override
	public void terminate() {
		
		// TODO Auto-generated method stub
		
	}

}

