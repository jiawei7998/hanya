package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindSeclService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得债券收盘价数据
 *
 */
public class WindSeclJobManger implements CronRunnable {

	private WindSeclService windSeclService = SpringContextHolder.getBean(WindSeclService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindSeclJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得债券收盘价数据==============================");
		Boolean run = windSeclService.run();
		logManager.info("==================万得债券收盘价数据处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
