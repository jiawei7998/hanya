package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.GJ0011.*;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.marketdata.bean.FxRateTypes;
import com.singlee.financial.marketdata.bean.FxRevaluationRates;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.model.FileNameList;
import com.singlee.ifs.service.ListPriceJobService;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.*;

/**
 * 国结外汇即期(取中间价)
 */


public class middlePriceJob implements CronRunnable {

    private ListPriceJobService listPriceJobService = SpringContextHolder.getBean(ListPriceJobService.class);
    
    private MarketDataTransServer marketDataTransServer = SpringContextHolder.getBean(MarketDataTransServer.class);

	@Override
    public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，开始······");
		Date postdate = new Date();
		EsbPacket<SoapReqBody> requestPakcet = getgJ0011Req();
		HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
		httpHeadAttrConfig.setCharset("GB2312");
		httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//      httpHeadAttrConfig.setCharset("UTF-8");
		httpHeadAttrConfig.setConnTimeOut(3000);
		httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
		httpHeadAttrConfig.setContentLength(true);
		httpHeadAttrConfig.setContentMd5(true);
		String host = SystemProperties.esbIp;
		String port = SystemProperties.esbTransPort;
		String server = "S010003010GJ0011";
		String strURL = "http://" + host + ":" + port + "/" + server;
		httpHeadAttrConfig.setUrl(strURL);
		// 初始化输出对象
		EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class, "soapenv:Envelope");
		responsePakcet.setPackge_type("www.cqrcb.com.cn");
		responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);
		// 解析返回对象
		SoapResBody soapResBody = responsePakcet.getBody();
		// 获取牌价文件在ftp服务器上的绝对路径 File1Name
		String fileName = soapResBody.getGJ0011Res().getResponseBody().getFile1Name();
		if (fileName == null) {
			return false;
		}
		String path = SystemProperties.acupPath;
		String localFileName =  DateUtil.format(postdate, "yyyyMMdd")+fileName;
		File filedir = new File(path);
		if (!filedir.exists()) {
			filedir.mkdirs();
		}
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，文件路径绝对路径为"+SystemProperties.esbRoot+ fileName);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，esbIp为"+host+"端口为"+SystemProperties.esbFtpPort+"用户名为"+SystemProperties.esbUser);

		// 初始化FTP
		SingleeSFTPClient sftp = new SingleeSFTPClient(host, SystemProperties.esbFtpPort, SystemProperties.esbUser, SystemProperties.esbPasswd);
		// 开始连接
		sftp.connect();
		// 下载文件
		boolean flag = sftp.downloadFile(SystemProperties.esbRoot, fileName, SystemProperties.acupPath,localFileName);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，文件是否下载"+flag);

		if (flag) {
			// 文件解析 明细
			List<String> liseDeali = new ArrayList<String>();
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，开始解析文件");
			File file = new File(path, localFileName);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GB2312"));// 构造一个BufferedReader类来读取文件
			String s = null;
			while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
				liseDeali.add(s);
			}
			br.close();


			// 从第二行开始拿到所有的币种和消息
			for (int i = 1; i < liseDeali.size(); i++) {
				String line = liseDeali.get(i);
		    JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，文件第"+i+"行是："+line);
				List<String> fileNameLists = new ArrayList<String>();
				// 通过，分割拿到所有的价格
				String[] string = line.split("\\,");
				for (String str : string) {
					fileNameLists.add(str);
				}
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，解析完成 开始插入IFS_LIST_PRICE表");
				// 把解析的文件每一排都插入前置表
				insertPrice(liseDeali, fileNameLists);
			}
		
			
			
			Map<String, Object> rmap = new HashMap<>();
			Map<String, FxRevaluationRates> frRateMap = new HashMap<>();
			rmap.put("fileDate", DateUtil.format(postdate, "yyyyMMdd"));

			List<FileNameList> list = listPriceJobService.getlist(rmap);
			for (FileNameList price : list) {
			

					FxRevaluationRates base = new FxRevaluationRates();
					//即期实时汇率
					 BigDecimal spotRate=new BigDecimal(price.getCentralParity()).divide(new BigDecimal(100));
					// 机构
					base.setBr("01");
					// 币种
					base.setCcy(price.getCurrency());
					// 即期汇率
					base.setSpotRate(spotRate);
					//汇率
	                base.setRate(spotRate);
					// 价格日期
					base.setSpotDate(postdate);
					// 基准价
					frRateMap.put(price.getCurrency() + StringUtils.trim("SPOT"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("1D"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("1W"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("1M"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("3M"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("6M"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("9M"), base);
//					frRateMap.put(price.getCurrency() + StringUtils.trim("1Y"), base);
					
			}

					JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，开始插入调用saveFxRateData方法，把数据和插入irev和inth表");
					marketDataTransServer.saveFxRateData(frRateMap, FxRateTypes.Rate);

			
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->国结(取中间价)】，外汇代客即期任务，结束······");
		return true;
	}

	/**
	 * 组装GJ0011请求对象
	 * 
	 *
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapReqBody> getgJ0011Req() throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class, "soapenv:Envelope");
		request.setPackge_type("www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		// 申明报文对象
		GJ0011Req GJ0011ReqBody = new GJ0011Req();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		// 设置body中的请求头信息
		requestHeader.setBrchNo("00100");// 机构号
		// 设置报文中请求头对象
		GJ0011ReqBody.setRequestHeader(requestHeader);
		// body里面的内容
		requestBody.setMark("guojie");
		// 设置body
		GJ0011ReqBody.setRequestBody(requestBody);
		// 设置报文
		soapReqBody.setGJ0011Req(GJ0011ReqBody);
		return request;
	}

	/**
	 * 组装GJ0011返回对象
	 * 
	 *
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unused")
	private static EsbPacket<SoapResBody> getGJ0011Res() throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class, "soapenv:Envelope");
		response.setPackge_type("www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		GJ0011Res gJ0011ResBody = new GJ0011Res();
		ResponseBody responseBody = new ResponseBody();
		ResponseHeader responseHeader = new ResponseHeader();
		gJ0011ResBody.setResponseHeader(responseHeader);
		Fault fault = new Fault();
		fault.setFaultCode("002001000000");
		fault.setFaultString("成功");
		Detail detail = new Detail();
		detail.setTxnStat("SUCCESS");
		fault.setDetail(detail);
		gJ0011ResBody.setFault(fault);
		// 设置body
		gJ0011ResBody.setResponseBody(responseBody);
		// 设置报文
		soapResBody.setGJ0011Res(gJ0011ResBody);
		return response;
	}
	// 插入前置表
	public void insertPrice(List<String> liseDeali, List<String> fileNameLists) {

		Map<String, Object> remp = new HashMap<>();
		remp.put("fileTime", liseDeali.get(0));
		String fileTime = (String) remp.get("fileTime");
		String fileDate = fileTime.substring(0, 10);
		String fileDates=  fileDate.replace(".", "-");
	      
	    remp.put("fileDate",fileDates);
		remp.put("currency", fileNameLists.get(0));
		remp.put("base", fileNameLists.get(1));
		remp.put("foreignMarketPrice", fileNameLists.get(2));
		remp.put("foreignToUSDPrice", fileNameLists.get(3));
		remp.put("foreignToUSDType", fileNameLists.get(4));
		remp.put("purchasePrice", fileNameLists.get(5));
		remp.put("cashSellingRate", fileNameLists.get(6));
		remp.put("buyingRate", fileNameLists.get(7));
		remp.put("sellingRate", fileNameLists.get(8));
		remp.put("centralParity", fileNameLists.get(9));
		remp.put("systemFlatBuying", fileNameLists.get(10));
		remp.put("systemFlatSelling", fileNameLists.get(11));
		remp.put("finalPrice", fileNameLists.get(12));
		List<FileNameList> list = listPriceJobService.getlist(remp);
//      list为空才插入
		if (null==list|| 0==list.size()) {
			listPriceJobService.insert(remp);
		}

	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
