package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.MathUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.impl.DictionaryGetServiceImpl;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IfsForeignCcyMapper;
import com.singlee.ifs.model.IfsForeignccyDish;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 外汇代客发送opics定时任务
 */
public class FxAgentJobManager implements CronRunnable {

    private static Logger logManager = LoggerFactory.getLogger(FxAgentJobManager.class);

    private IFxdServer fxdServer = SpringContextHolder.getBean("IFxdServer");
    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
    private IfsForeignCcyMapper ccyMapper = SpringContextHolder.getBean(IfsForeignCcyMapper.class);
    private DictionaryGetServiceImpl dictionary = SpringContextHolder.getBean(DictionaryGetServiceImpl.class);

    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {
        logManager.info("********** Start FxAgentJobManager execute ***********");
        Map<String, Object> map = new HashMap<String, Object>();
        Date date = baseServer.getOpicsSysDate("01");
        // map.put("dealDate", DateUtil.format(date));
        map.put("dealDate", "2018-01-16");
        List<IfsForeignccyDish> list = ccyMapper.searchDishList(map);
        List<TaDictVo> dictList = dictionary.getTaDictByCode("AMTFLAG");
        if (dictList != null && dictList.size() > 0) {
            String amt = dictList.get(0).getDict_value();
            for (IfsForeignccyDish bean : list) {
                if (Double.valueOf(bean.getCcy2Amt()) >= Double.valueOf(amt)) {
                    SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean("01", SlDealModule.FXD.SERVER,
                        SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
                    // 交易日期、起息日
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    slFxSptFwdBean.setDealDate(sdf.parse(bean.getDealDate()));
                    slFxSptFwdBean.setValueDate(sdf.parse(bean.getvDate()));
                    // 交易对手
                    InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
                    contraPatryInstitutionInfo.setInstitutionId("100001");
                    slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
                    // 本方交易员
                    InstitutionBean institutionInfo = new InstitutionBean();
                    institutionInfo.setTradeId("SING");
                    slFxSptFwdBean.setInstitutionInfo(institutionInfo);
                    // 金额和币种
                    FxCurrencyBean currencyInfo = new FxCurrencyBean();
                    if ("1".equals(bean.getBusType())) {// 结售汇
                        if ("RMB".equals(bean.getCcy1())) {
                            currencyInfo.setCcy(bean.getCcy2()); // 主要币种
                            currencyInfo.setContraCcy("CNY"); // 次要币种
                            currencyInfo.setAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy2Amt()), 2))); // 主要币种金额
                            currencyInfo.setContraAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy1Amt()), 2))); // 次要币种金额
                        }
                        if ("RMB".equals(bean.getCcy2())) {
                            currencyInfo.setCcy(bean.getCcy1()); // 主要币种
                            currencyInfo.setContraCcy("CNY"); // 次要币种
                            currencyInfo.setAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy1Amt()), 2))); // 主要币种金额
                            currencyInfo.setContraAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy2Amt()), 2))); // 次要币种金额
                        }
                    } else {// 外汇买卖
                        if ("USD".equals(bean.getCcy1())) {// 美元是主要币种
                            currencyInfo.setCcy(bean.getCcy1()); // 主要币种
                            currencyInfo.setContraCcy(bean.getCcy2()); // 次要币种
                            currencyInfo.setAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy1Amt()), 2))); // 主要币种金额
                            currencyInfo.setContraAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy2Amt()), 2))); // 次要币种金额
                        }
                        if ("USD".equals(bean.getCcy2())) {
                            currencyInfo.setCcy(bean.getCcy2()); // 主要币种
                            currencyInfo.setContraCcy(bean.getCcy1()); // 次要币种
                            currencyInfo.setAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy2Amt()), 2))); // 主要币种金额
                            currencyInfo.setContraAmt(
                                BigDecimal.valueOf(MathUtil.roundWithNaN(Double.valueOf(bean.getCcy1Amt()), 2))); // 次要币种金额
                        }
                    }
                    // 点差
                    currencyInfo.setPoints(BigDecimal.valueOf(0));
                    slFxSptFwdBean.setCurrencyInfo(currencyInfo);

                    // 设置流水号
                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    String fedealno = ccyMapper.getFedealno(hashMap);
                    // SlExternalBean
                    SlExternalBean externalBean = new SlExternalBean();
                    if ("1".equals(bean.getBusType())) {
                        externalBean.setCost("1020113000");// 结售汇平盘
                    } else {
                        externalBean.setCost("1020213000");// 外汇平盘
                    }
                    externalBean.setProdcode("FXD");
                    externalBean.setProdtype("FX");
                    externalBean.setPort("FXDK");
                    externalBean.setBroker(SlDealModule.BROKER);
                    externalBean.setCustrefno(fedealno);
                    externalBean.setDealtext(fedealno);
                    externalBean.setAuthsi(SlDealModule.FXD.AUTHSI);
                    externalBean.setSiind(SlDealModule.FXD.SIIND);
                    externalBean.setSupconfind(SlDealModule.FXD.SUPCONFIND);
                    externalBean.setSuppayind(SlDealModule.FXD.SUPPAYIND);
                    externalBean.setSuprecind(SlDealModule.FXD.SUPRECIND);
                    externalBean.setVerind(SlDealModule.FXD.VERIND);
                    slFxSptFwdBean.setExternalBean(externalBean);
                    // SlInthBean
                    SlInthBean inthBean = slFxSptFwdBean.getInthBean();
                    inthBean.setFedealno(fedealno);
                    inthBean.setLstmntdate(date);
                    slFxSptFwdBean.setInthBean(inthBean);
                    // 买卖方向
                    String ps = bean.getDirection();
                    if (ps.endsWith("P")) {
                        slFxSptFwdBean.setPs(PsEnum.P);
                    }
                    if (ps.endsWith("S")) {
                        slFxSptFwdBean.setPs(PsEnum.S);
                    }
                    // 插入opics表
                    SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);// 用注入的方式调用opics相关方法

                    if (result.getRetStatus().equals(RetStatusEnum.S)) {
                        logManager.info("insert opics table success!......");
                        String id = bean.getForeignccyId();
                        String[] ids = id.split(",");
                        for (int i = 0; i < ids.length; i++) {
                            bean.setSyncStatus("1");
                            bean.setForeignccyId(ids[i]);
                            bean.setFedealno(fedealno);
                            ccyMapper.updateSyncStatus(bean);
                        }
                    } else {
                        logManager.info("insert opics table error！......");
                    }
                }
            }
        } else {
            logManager.info("*********字典项未设置金额参数**********");
        }

        logManager.info("*********update fxAgentSpot opics dealStatus **********");
        List<IfsForeignccyDish> synclist = ccyMapper.searchSyncDishList();
        for (IfsForeignccyDish ifsForeignccyDish : synclist) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FXD.SERVER);
            inthBean.setTag(SlDealModule.FXD.TAG);
            inthBean.setBr("01");
            inthBean.setFedealno(ifsForeignccyDish.getFedealno());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = baseServer.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
            if (slInthBean != null) {
                Map<String, Object> map2 = new HashMap<String, Object>();
                map2.put("fedealno", slInthBean.getFedealno());
                map2.put("statcode", slInthBean.getStatcode());
                map2.put("errorcode", slInthBean.getErrorcode());
                map2.put("dealno", slInthBean.getDealno());
                if ("-4".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                    map2.put("syncStatus", "2");
                } else if ("-6".equals(StringUtils.trimToEmpty(slInthBean.getStatcode()))) {
                    map2.put("syncStatus", "3");
                }
                ccyMapper.updateByFedealno(map2);
            }
        }

        logManager.info(" End FxAgentJobManager execute ");
        return true;
    }

    @Override
    public void terminate() {

    }
}