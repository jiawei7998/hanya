package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.financial.esb.hsbank.INupdServer;

import java.util.Map;

public class NupdJobManager implements CronRunnable{
	
	private INupdServer nupdServer = SpringContextHolder.getBean("INupdServer");
	
	
	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		nupdServer.nupdSend(null);
		return false;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
