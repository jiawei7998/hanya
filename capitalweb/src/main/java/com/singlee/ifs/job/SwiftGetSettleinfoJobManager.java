package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.ifs.service.IfsSwiftCallBackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

/**
 * 取响应报文Job
 *
 */
public class SwiftGetSettleinfoJobManager implements CronRunnable {
	private static Logger logManager = LoggerFactory.getLogger(SwiftJobManager.class);

	// SWIFT报文数据
	IfsSwiftMapper ifsSwiftMapper = SpringContextHolder.getBean(IfsSwiftMapper.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("=======start excute swiftGetJobManager==========");

		// 取回来报报文
		// 初始化FTP
		SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.swiftIp, SystemProperties.swiftPort,SystemProperties.swiftUserName, SystemProperties.swiftPassword);
		// 开始连接
		sftp.connect();
		List<String> fileList = sftp.batchDownLoadFile(SystemProperties.swiftCatalog + "out/",SystemProperties.swiftPath + "out/", null, ".txt", false);

		IfsSwiftCallBackService swiftExcute = SpringContextHolder.getBean("ifsSwiftAnalyzeServiceImpl");
		for (String localFile : fileList) {
			swiftExcute.swiftSave(new FileInputStream(localFile));
		}

		logManager.info("=======end excute swiftGetJobManager==========");
		return true;
	}

	@Override
	public void terminate() {
	}
}
