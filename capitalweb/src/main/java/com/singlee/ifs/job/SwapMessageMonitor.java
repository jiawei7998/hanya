package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsfxSwapMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbIrsMapper;
import com.singlee.ifs.model.IfsCfetsrmbIrs;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwapMessageMonitor implements CronRunnable {
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor= SpringContextHolder.getBean("IBaseServer");

	private IfsCfetsrmbIrsMapper cfetsrmbIrsMapper= SpringContextHolder.getBean(IfsCfetsrmbIrsMapper.class);

	private IfsCfetsfxSwapMapper cfetsfxSwapMapper= SpringContextHolder.getBean(IfsCfetsfxSwapMapper.class);

	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- OPICS】，互换模块交易状态同步任务处理，开始······");
		/**
		 * 互换模块分为利率互换和货币（互换）掉期
		 */
		//添加清算信息类
		CommonSettleManager settleManager=new CommonSettleManager();
		
		// 查询全部未复核
		List<IfsCfetsrmbIrs> listIrs = cfetsrmbIrsMapper.getCfetsrmbIrsList();
		for (IfsCfetsrmbIrs ifsCfetsrmbIrs : listIrs) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.SWAP.SERVER);
			inthBean.setTag(SlDealModule.SWAP.TAG);
			inthBean.setBr(ifsCfetsrmbIrs.getBr());
			inthBean.setFedealno(ifsCfetsrmbIrs.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				cfetsrmbIrsMapper.updateCfetsrmbIrsByFedealno(map);
				// 清算相关处理
				if(!"1".equals(ifsCfetsrmbIrs.getNettingStatus())){//不是净额清算的流向大额
					// 清算相关处理
					String br =StringUtils.trimToEmpty(slInthBean.getBr());
					String dealno =StringUtils.trimToEmpty(slInthBean.getDealno());
					String fedealno =StringUtils.trimToEmpty(slInthBean.getFedealno());
					String product =StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProduct());
					String prodtype =StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProdType());
					if (!"".equals(dealno)) {
						settleManager.addSettle(br, dealno,fedealno, product,prodtype);
						//settleManager.addSwift(br, dealno, product,prodtype);
					}//清算end
				}
				
			}
		}
		//获取当天，净额清算的swift中dealno不存在的交易
		List<IfsCfetsrmbIrs> listIrs2 = cfetsrmbIrsMapper.searchSwiftNotExist();
		for (IfsCfetsrmbIrs ifsCfetsrmbIrs : listIrs2) {
			String br = "01";
			String dealno = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getDealNo());
			String product = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProduct());
			String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProdType());
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- OPICS】，互换模块交易状态同步任务处理，开始执行 利率互换swift dealno ="+dealno);
			settleManager.addSwift(br, dealno, product,prodtype);
		}

		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- OPICS】，互换模块交易状态同步任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
