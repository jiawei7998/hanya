package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindBondAddService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得债券追加数据
 *
 */
public class WindBondAddJobManger implements CronRunnable {

	private WindBondAddService windBondAddService = SpringContextHolder.getBean(WindBondAddService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindBondAddJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得债券追加数据==============================");
		Boolean run = windBondAddService.run();
		logManager.info("==================万得债券追加数据处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
