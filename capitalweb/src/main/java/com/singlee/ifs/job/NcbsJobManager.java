package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.*;
import com.singlee.financial.esb.tlcb.INbhcxServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.ifs.mapper.IFSMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsNbhBean;
import com.singlee.ifs.model.IfsNbhGatherBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 核心内部户管理定时任务
 */
public class NcbsJobManager implements CronRunnable {
	
	private static Logger logManager = LoggerFactory.getLogger(NcbsJobManager.class);

	private INbhcxServer iNbhcxServer = SpringContextHolder.getBean("INbhcxServer");
	private IFxdServer fxdServer = SpringContextHolder.getBean("IFxdServer");
	private IBaseServer iBaseServer  = SpringContextHolder.getBean("IBaseServer");
	
	private IFSMapper ifsMapper = SpringContextHolder.getBean(IFSMapper.class);
	private IfsOpicsBondMapper ifsOpicsBondMapper = SpringContextHolder.getBean(IfsOpicsBondMapper.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("**********核心内部户查询跑批开始***********");
		
		String acctno = "";
		List<IfsNbhBean> list = ifsMapper.queryAcct(); // 查询六个配置账号
		if(list != null){
			for(int k=0;k<list.size();k++){
				acctno = list.get(k).getCcyacct();
        		Map<String, String> map = new HashMap<String, String>();
        		map.put("ACCT_NO", acctno);
        		map.put("userId", SlSessionHelper.getUserId()); // 传入用户名
        		SlNcbsBean resultMsg = iNbhcxServer.nbhcx(map);
        		List<GlInfoArray> list2 = resultMsg.getGL_INFO_ARRAY(); // 账务信息数组
        		for (int j = 0; j < list2.size(); j++) {
        			String tranDate = list2.get(j).getTRAN_DATE(); // 交易日期
        			String teller = list2.get(j).getTRNSCTN_USR(); // 交易柜员
        			String dcrFlag = list2.get(j).getDR_CR_FLAG(); // 借贷标志
        			BigDecimal amt = new BigDecimal(0);
        			BigDecimal drBal = new BigDecimal(0);
        			BigDecimal crBal = new BigDecimal(0);
        			if(list2.get(j).getACCT_AMT() != null){
        				list2.get(j).getACCT_AMT(); // 记账金额
        			}
        			if(list2.get(j).getDR_BAL() != null){
        				drBal = list2.get(j).getDR_BAL(); // 借方余额
        			}
        			if(list2.get(j).getCR_BAL() != null){
        				crBal = list2.get(j).getCR_BAL(); // 贷方余额
        			}
        			String hostDate = list2.get(j).getHOST_DATE(); // 主机日期
        			String tranTime = list2.get(j).getTX_TIME(); // 交易时间
        			String tranCode = list2.get(j).getTRAN_CODE(); // 交易码
        			String seqno = list2.get(j).getORI_SERV_SEQ_NO(); // 服务处理流水号
        			String ccy = list2.get(j).getCCY(); // 币种
        			String extSeqNo = list2.get(j).getEXT_SEQ_NO(); // 外部流水
        			String othrAcctNo = list2.get(j).getOTHR_ACCT_NO(); // 对方账号
        			String othrAcctNm = list2.get(j).getOTHR_ACCT_NM(); // 对方账户名称
        			String cashTranFg = list2.get(j).getCASH_TRAN_FLAG(); // 现转标志
        			String smmryCd = list2.get(j).getSMMRY_CD(); // 摘要代码
        			String smmryMsg = list2.get(j).getSMMRY_MSG(); // 摘要描述
        			String tranBranchId = list2.get(j).getTRAN_BRANCH_ID(); // 交易机构
        			String accountBranchId = list2.get(j).getACCOUNTING_BRANCH_ID(); // 核算机构
        			
        			GlInfoArray glInfoBean = new GlInfoArray();
        			glInfoBean.setTRAN_DATE(tranDate);
        			glInfoBean.setTRNSCTN_USR(teller);
        			glInfoBean.setDR_CR_FLAG(dcrFlag);
        			glInfoBean.setACCT_AMT(amt);
        			glInfoBean.setDR_BAL(drBal);
        			glInfoBean.setCR_BAL(crBal);
        			glInfoBean.setHOST_DATE(hostDate);
        			glInfoBean.setTX_TIME(tranTime);
        			glInfoBean.setTRAN_CODE(tranCode);
        			glInfoBean.setORI_SERV_SEQ_NO(seqno);
        			glInfoBean.setCCY(ccy);
        			glInfoBean.setEXT_SEQ_NO(extSeqNo);
        			glInfoBean.setOTHR_ACCT_NO(othrAcctNo);
        			glInfoBean.setOTHR_ACCT_NM(othrAcctNm);
        			glInfoBean.setCASH_TRAN_FLAG(cashTranFg);
        			glInfoBean.setSMMRY_CD(smmryCd);
        			glInfoBean.setSMMRY_MSG(smmryMsg);
        			glInfoBean.setTRAN_BRANCH_ID(tranBranchId);
        			glInfoBean.setACCOUNTING_BRANCH_ID(accountBranchId);
        			
        			Map<String,String> upmap = new HashMap<String, String>();
        			upmap.put("ccy",ccy);
        			upmap.put("seqno",seqno);
        			/** 查询内部户明细表 */
        			List<GlInfoArray> glList = ifsMapper.queryContent(upmap); // 币种+流水号是否能查询出数据
    				if(glList.size()==0){ // 若查询不到数据，则表明表中无此记录，则插入
        				ifsMapper.insertDetail(glInfoBean); // 插入内部户明细表
        			}
        		}
			}
			List<IfsNbhBean> ifsNbhBean = ifsMapper.queryData(); // 查询内部户处理后的数据
    		SlFxSptFwdBean slFxSptFwdBean = new SlFxSptFwdBean("01", SlDealModule.FXD.SERVER, SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
    		
    		for(int n=0;n<ifsNbhBean.size();n++){
    			String ccy_home = ifsNbhBean.get(n).getCcy_home();
    			String ccy_foreign = ifsNbhBean.get(n).getCcy_foreign();
    			BigDecimal jizhngje_home = ifsNbhBean.get(n).getJizhngje_home();
    			BigDecimal jizhngje_foreign = new BigDecimal(0);
    			if(ifsNbhBean.get(n).getJizhngje_foreign() != null){
    				jizhngje_foreign = ifsNbhBean.get(n).getJizhngje_foreign();
    			}
    			/** 设置金额和币种 */
    			FxCurrencyBean currencyInfo = new FxCurrencyBean();		
				currencyInfo.setAmt(jizhngje_foreign.abs()); // 外币金额
				if("156".equals(ccy_home)){
					ccy_home="CNY";
				}
				if("840".equals(ccy_foreign)){
					ccy_foreign="USD";
				}
				if("344".equals(ccy_foreign)){
					ccy_foreign="HKD";
				}
				if("392".equals(ccy_foreign)){
					ccy_foreign="JPY";
				}
				if("826".equals(ccy_foreign)){
					ccy_foreign="GBP";
				}
				if("978".equals(ccy_foreign)){
					ccy_foreign="EUR";
				}
				currencyInfo.setCcy(ccy_foreign); // 外币币种
				currencyInfo.setContraAmt(jizhngje_home.abs()); // 本币金额
				currencyInfo.setContraCcy(ccy_home); // 本币币种
				slFxSptFwdBean.setCurrencyInfo(currencyInfo);
				
				/* 取OPICS日期  */
				Date date = iBaseServer.getOpicsSysDate("01");
				
				InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
				InstitutionBean institutionInfo = new InstitutionBean();
				contraPatryInstitutionInfo.setInstitutionId("1000000002"); // 设置交易对手
				institutionInfo.setTradeId("NZY"); // 设置交易员
				slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
				slFxSptFwdBean.setInstitutionInfo(institutionInfo);
				
				SlExternalBean externalBean = new SlExternalBean();
				externalBean.setPort("FXPB"); // 设置投资组合
				externalBean.setCost("1020113000"); // 设置成本中心
				externalBean.setBroker(SlDealModule.BROKER); // 设置BROK
				externalBean.setDealtext("NCBS"); // 设置来源DEALTEXT
				externalBean.setSiind("Y"); // 设置SIIND
				externalBean.setVerind("Y"); // 设置VERIND
				externalBean.setSuppayind("Y"); // 设置SUPPAYIND
				externalBean.setSuprecind("Y"); // 设置SUPRECIND
				externalBean.setSupconfind("Y"); // 设置SUPCONFIND
				externalBean.setProdcode("FXD"); // 设置产品代码
				externalBean.setProdtype("FX"); // 设置产品类型
				externalBean.setAuthsi("Y"); // 设置AUTHSI
				slFxSptFwdBean.setExternalBean(externalBean);
				
				/** 生成FEDEALNO*/
				//设置流水号
				HashMap<String, Object> hashMap = new HashMap<String, Object>();
				String fedealno = ifsOpicsBondMapper.getTradeId(hashMap);
				
				/** 设置Fedealno和Inoutind */
				SlInthBean inthBean = slFxSptFwdBean.getInthBean();
				inthBean.setFedealno(fedealno);
				inthBean.setInoutind("I");
				slFxSptFwdBean.setInthBean(inthBean);
				
				slFxSptFwdBean.setValueDate(date); // 设置VDATE
				slFxSptFwdBean.setDealDate(date); // 设置DEALDATE
				
				IfsNbhGatherBean entity = new IfsNbhGatherBean();
				
				/*设置买卖方向(外币汇总金额:正数-买入-P；负数-卖出-S)*/
				if(jizhngje_foreign.signum() == 1){ // 正数
					slFxSptFwdBean.setPs(PsEnum.P);
					entity.setPs("P");
				} else if(jizhngje_foreign.signum() == -1){ // 负数
					slFxSptFwdBean.setPs(PsEnum.S);
					entity.setPs("S");
				}
				
				entity.setBr("01");
				entity.setServer(SlDealModule.FXD.SERVER);
				entity.setFedealno(fedealno);
				entity.setSeq(SlDealModule.FXD.SEQ);
				entity.setInoutind(SlDealModule.FXD.INOUTIND);
				entity.setTrad("NZY");
				entity.setVdate(date);
				entity.setCust("1000000002");
				entity.setBrok("D");
				entity.setPort("FXPB");
				entity.setCost("1020113000");
				entity.setDealdate(iBaseServer.getOpicsSysDate("01"));
				entity.setDealtext("NCBS");
				entity.setCcy(ccy_foreign);
				entity.setCcyamt(jizhngje_foreign.abs());
				entity.setCtrccy(ccy_home);
				entity.setCtramt(jizhngje_home.abs());
				entity.setSiind("Y");
				entity.setVerind("Y");
				entity.setSuppayind("Y");
				entity.setSuprecind("Y");
				entity.setSupconfind("Y");
				entity.setProduct("FXD");
				entity.setProdtype("FX");
				entity.setAuthsi("Y");
				entity.setLocalstatus("0"); // 状态默认为0-未同步
				
    			// 插入opics的IFXD表
    			SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);
    			if (!result.getRetStatus().equals(RetStatusEnum.S)) {
					JY.raise("插入opics表失败......");
				}
    			// OPICS处理成功后查询dealno
    			Map<String,Object> map = new HashMap<String, Object>();
    			map.put("fedealno", fedealno);
    			String dealno = fxdServer.queryDealno(map);
    			entity.setDealno(dealno); // 将查询到的dealno插入到界面上
    			ifsMapper.nbhGatherAdd(entity); // 插入内部户估值
    		}
		}
		logManager.info("**********核心内部户查询跑批结束***********");
		return true;
	}

	@Override
	public void terminate() {
		
	}
}