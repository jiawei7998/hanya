package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.financial.opics.IAcupServer;
import com.singlee.ifs.service.ISendCPISMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName SendCPISMessageJobManager.java
 * @Description 交易后确认自动发送任务
 * @createTime 2021年11月02日 16:58:00
 */
public class SendCPISMessageJobManager implements CronRunnable {
    private static Logger LOGGER = LoggerFactory.getLogger(SendCPISMessageJobManager.class);
    private IAcupServer acupServer = SpringContextHolder.getBean("IAcupServer");
    private ISendCPISMessageService sendCPISMessageService = SpringContextHolder
            .getBean(ISendCPISMessageService.class);

    @Override
    public boolean execute(Map<String, Object> parameters){
        LOGGER.info("==================开始执行SendCPISMessageJobManager==============================");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        String dateStr = sdf1.format(new Date());
        Map<String, Object> hldyParam = new HashMap<String, Object>();
        hldyParam.put("holidate", dateStr);
        hldyParam.put("calendarid", "CNY");
        int hldyState =  acupServer.checkHldy(hldyParam);
        if (hldyState != 0) {
            LOGGER.info("==================此任务SendCPISMessageJobManager不在工作日内====================");
            return true;
        }

        //发送反馈数据
        try {
            LOGGER.info("==================获取反馈信息==============================");
            sendCPISMessageService.retrieveCpisReturnBackMsg();
            LOGGER.info("==================完成反馈信息==============================");
        } catch (Exception e) {
            LOGGER.error("获取反馈信息处理异常: ", e);
        }

        //发送反馈数据
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String tradeDate=sdf.format(new Date());//默认当前日期
            Map<String, Object> dateMap = new HashMap<String, Object>();
            dateMap.put("cDate", tradeDate);//界面日期，没有默认当前日期
            dateMap.put("execID", "");//成交单号,默认所有

            //外币拆借
            LOGGER.info("==================开始发送外币拆借交易信息==============================");
            sendCPISMessageService.sendFxLend(dateMap);
            LOGGER.info("==================完成发送外币拆借交易信息==============================");

            LOGGER.info("==================开始发送即期交易信息==============================");
            sendCPISMessageService.sendFxSpt(dateMap);
//			List<SendForeignForwardSpotModel> forwardSpotModels= cpisMessageMapper.searchForeignSptMessage(dateMap);
//			sendSptFwd(forwardSpotModels,"12",tradeDate,typeMap);
            LOGGER.info("==================完成发送即期交易信息==============================");

            LOGGER.info("==================开始发送远期交易信息==============================");
            sendCPISMessageService.sendFxFwd(dateMap);
//						forwardSpotModels = cpisMessageMapper.searchForeignFwdMessage(dateMap);
//			sendSptFwd(forwardSpotModels,"14",tradeDate,typeMap);
            LOGGER.info("==================完成发送远期交易信息==============================");


            LOGGER.info("==================开始发送掉期交易信息==============================");
            sendCPISMessageService.sendFxSWAP(dateMap);
//						forwardSpotModels = cpisMessageMapper.searchForeignFwdMessage(dateMap);
//			sendSptFwd(forwardSpotModels,"14",tradeDate,typeMap);
            LOGGER.info("==================完成发送掉期交易信息==============================");

        } catch (Exception e) {
            LOGGER.error("处理异常: ", e);
        }

        LOGGER.info("==================执行完成SendCPISMessageJobManager==============================");
        return true;
    }

    @Override
    public void terminate() {

    }
}
