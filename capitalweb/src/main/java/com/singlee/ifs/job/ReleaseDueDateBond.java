package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.eu.model.EdInParams;
import com.singlee.capital.eu.service.EdCustManangeService;
import com.singlee.financial.bean.SlSecmRedemption;
import com.singlee.financial.opics.ISecurServer;
import com.singlee.ifs.mapper.BondLimitTemplateMapper;
import com.singlee.ifs.mapper.IfsCfetsfxLendMapper;
import com.singlee.ifs.mapper.IfsOpicsSettleManageMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReleaseDueDateBond implements CronRunnable{

	/**
	 * 对于到期还本的债券交易进行额度释放
	 */

	//查询opics到期还本
	private ISecurServer iSecurServer= SpringContextHolder.getBean("ISecurServer");
	
	//当前时间
	private IfsOpicsSettleManageMapper ifsOpicsSettleManageMapper = SpringContextHolder.getBean(IfsOpicsSettleManageMapper.class);
	
	private IfsCfetsfxLendMapper ifsCfetsfxLendMapper = SpringContextHolder.getBean(IfsCfetsfxLendMapper.class);
	
	private EdCustManangeService edCustManangeService = SpringContextHolder.getBean(EdCustManangeService.class);

	private BondLimitTemplateMapper bondLimitTemplateMapper = SpringContextHolder.getBean(BondLimitTemplateMapper.class);
	

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		String nowDate = ifsOpicsSettleManageMapper.getCurDate();
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，到期还本的债券交易进行额度释放任务处理，开始······");
		
		String dealNo = null;//流程中的交易流水号
		String prdNo = null;//交易编号
		String bondId = null;//已经到期的债券

		Map<String, Object> redemaptMap = new HashMap<String, Object>();
		redemaptMap.put("brprcindte", nowDate);
		List<SlSecmRedemption> redemptList = new ArrayList<SlSecmRedemption>();
		redemptList = iSecurServer.getRedemptdWithList(redemaptMap);
		for(int i=0;i<redemptList.size();i++){
			bondId = redemptList.get(i).getSecid().trim();
			
			Map<String, Object> messageMap = new HashMap<String, Object>();
			messageMap.put("nowDate", nowDate);
			messageMap.put("bondNo", bondId);//到期还本的债券
			
			//查询本地到期的债券交易
			List<EdInParams> creditListLimit = ifsCfetsfxLendMapper.limitBondExchangeInfo(messageMap);
			for(int j=0;j<creditListLimit.size();j++){
				prdNo = creditListLimit.get(j).getPrdNo();
				if("443".equals(prdNo)||"451".equals(prdNo)){
					//442买断式回购，443现劵买卖，451外币债
				
					dealNo = creditListLimit.get(j).getDealNo();
					//到期债券交易进行释放
					edCustManangeService.eduReleaseFlowService(dealNo);
					//到期释放债券
					bondLimitTemplateMapper.deleteOverTimeBond(dealNo);
					//到期的限额报表进行删除
					bondLimitTemplateMapper.deleteOverTimeBondLog(dealNo);
				}
			}
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->前置】，到期还本的债券交易进行额度释放任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
	
	
}
