package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.eu.mapper.TdEdCustBatchStatusMapper;
import com.singlee.capital.eu.model.TdEdCustBatchStatus;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.hrbextra.credit.service.HrbCreditLimitOccupyService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LimitReleaseJob implements CronRunnable {

	private TdEdCustBatchStatusMapper tdedCustBatchStatusMapper = SpringContextHolder.getBean(TdEdCustBatchStatusMapper.class);
	private HrbCreditLimitOccupyService hrbCreditLimitOccupyService = SpringContextHolder.getBean(HrbCreditLimitOccupyService.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 前置】，额度释放任务，开始······");
		// 通过log表查询今天额度到期的额度占用 com.singlee.ifs.job.LimitReleaseJob 10.190.11.40 */3 * *// * *
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> premp = new HashMap<>();
		premp.put("mDate", df.format(date));
		
		List<TdEdCustBatchStatus> list = tdedCustBatchStatusMapper.selectEdCustLogListAll(premp);
		for (int i = 0; i < list.size(); i++) {
			//现券买卖、MBS不自动释放
			if(!TradeConstants.ProductCode.MBS.equals(list.get(i).getPrdName())&&!TradeConstants.ProductCode.CBT.equals(list.get(i).getPrdName())){
				hrbCreditLimitOccupyService.LimitReleaseInsertByDealno(list.get(i).getDealNo());
			}

		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 前置】，额度释放任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}

}
