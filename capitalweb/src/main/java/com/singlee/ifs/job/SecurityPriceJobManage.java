package com.singlee.ifs.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.StringUtil;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.ifs.mapper.IfsIntfcSecurityPriceMapper;
import com.singlee.ifs.model.IfsIntfcSeclStatus;
import com.singlee.ifs.model.IfsIntfcSecurityPrice;

public class SecurityPriceJobManage implements CronRunnable{
	
	private IfsIntfcSecurityPriceMapper intfcOpicsMapper = SpringContextHolder.getBean(IfsIntfcSecurityPriceMapper.class);
	
//	private IfsIntfcSecurityPriceMapper intfcOpicsMapper;
	
	public Date date =new Date();
	public SimpleDateFormat sdfdate = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
	public String msgTypeCode=null;//标准报文数据种类代码
	public String  msgTypeCodeLen;//标准报文数据长度
	public int  msgTypeCodeLen1;//标准报文数据长度
	public String sysName;//系统姓名
	public String handleCode;//处理代码
	public String channelType;//渠道类型
	public String channelId;//渠道ID
	public String gainInst;//获取机构
	public String cdate;//日期
	public String ctime;//时间
	public String keyTrade;//KEY1, 交易
	public String keyTradeNo;//KEY2 , 参考号
	public String serveHandleCode;//服务处理代码
	public String FXFIG_CJUM;
	public String FXFIG_AJUM;
	public String FXFIG_SJUM;
	public String FXFIG_OPNO;
	public String FXFIG_OPGB;
	public String FXFIG_IBIL;
	public String FXFIG_AMOD;
	public String FXFIG_TELL;
	public String FXFIG_SSID;
	public String FXFIG_SSIL;
	public String FXFIG_TELLNM;
	public String FXFIG_THGB;
	public String FXFIG_TYPE;
	public String FXFIG_JUM_CD;
	public String FXFIG_CCY;
	
	public String FXFIG_SSCK;
	public String FXFIG_THID;
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	ChiosLogin chiosLogin = new ChiosLogin();
	StringUtil stringUtil = new StringUtil();
	
	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台债券收盘价数据文件，开始······");
		Map<String, Object> remap = new HashMap<>();
        remap.put("br", "01");
        String dealdate = intfcOpicsMapper.getOpicsSysDate(remap);// 获取当前系统日期
//        Date postdate = DateUtil.format(postdate, "yyyy-MM-dd");
		JY.info("[JOBS] ==> 获取当前系统日期:" + dealdate);
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("dealdate", dealdate);
		List<IfsIntfcSecurityPrice> securityPriceList = intfcOpicsMapper.queryAllOpicsSecurityPrice(instMap);
		IfsIntfcSecurityPrice securityPrice1 = null;
		for(IfsIntfcSecurityPrice securityPrice:securityPriceList)
		{
			securityPrice1 = new IfsIntfcSecurityPrice();
			securityPrice1.setFIFB13_CCY(securityPrice.getCCY().trim());
			securityPrice1.setFIFB13_CLOSING_PRICE(securityPrice.getCLSGPRICE_8());
			securityPrice1.setFIFB13_DEAL_IL(securityPrice.getLSTMNTDATE().trim());
			securityPrice1.setFIFB13_SECURITY_ID(securityPrice.getSECID().trim());
			securityPrice1.setFXFIG_GEOR("13");
			securityPrice1.setFXFIG_GWAM("FB");
			securityPrice1.setFXFIG_IBCD("FB13");
			securityPrice1.setFXFIG_OPNO("OPCS088");
			securityPrice1.setFXFIG_SVCN("FBO1300");
			try{
				JY.info("MSG:正在发送SECID["+securityPrice1.getFIFB13_SECURITY_ID()+"]债券收盘价...");
				int ret = executeHostVisitAction(securityPrice1);
				JY.info("ret:"+ret);
			}catch (Exception e) {
				JY.info("MSG:发送债券收盘价"+securityPrice.getFIFB13_SECURITY_ID().trim()+"数据异常:"+e.getMessage());
			}
		}
		System.out.println("[JOBS-END] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台债券收盘价数据文件，结束······");
		return true;
	}
	
	public int executeHostVisitAction(IfsIntfcSecurityPrice securityPrice) throws Exception{
		int retFlag = 0;
		try {
			IfsIntfcSeclStatus seclStatus = new IfsIntfcSeclStatus();
			seclStatus.setTableName("SECL");
			if(securityPrice != null) {
				seclStatus.setBr("01");
				seclStatus.setSecid(securityPrice.getFIFB13_SECURITY_ID());
				seclStatus.setClsPrice_8(securityPrice.getFIFB13_CLOSING_PRICE());
				seclStatus.setCcy(securityPrice.getFIFB13_CCY());
				seclStatus.setPostdate(securityPrice.getFIFB13_DEAL_IL());
				seclStatus.setUpCount(0);
				seclStatus.setUpOwn("TMAX");
				seclStatus.setUpTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				seclStatus.setSendTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			}
			
			long standardStampStart = System.currentTimeMillis();
			 //返回的柜员session
			String sess = chiosLogin.sendLogin(eaiDv, "OPS_CHS_DSS00001");
			
			StdTMsgGenerator generator = new StdTMsgGenerator();
			
			//报文头
			byte[] headerPacket=choisCommonMsg.makeHead();
			
			PropertiesConfiguration stdtmsgSysValue;
			
			stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
			String handleCode="FB1300";
			String serveHandleCode="OPCFB13";
			FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
			
			String coMsg=choisCommonMsg.makeMsg();//公共部分
			String coMsg2=choisCommonMsg.makeMsg2();//公共部分
			String coMsg3=choisCommonMsg.makeMsg3();//公共部分
			
			//交易数据
			String dealMsg=stringUtil.subStrToString(securityPrice.getFIFB13_SECURITY_ID(), 15)+securityPrice.getFIFB13_DEAL_IL()+securityPrice.getFIFB13_CCY()+stringUtil.subStrOrNumber(String.valueOf(securityPrice.getFIFB13_CLOSING_PRICE()), 16, 10);
			
			byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
			byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
			String serviceUrl = "OPS";
			byte[] msgData =totalPacket;
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			System.out.println("eaiDv:--"+eaiDv);
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
			System.out.println("FX返回信息："+new String(returnData, "UTF-8"));
			JY.info("SEND MSG:"+new String(msgData, "UTF-8"));
			//返回成功失败标识
			String sendResult=(new String(returnData, "UTF-8")).substring(290,292);
			//CHOIS参考号
			String choisRefNoString=(new String(returnData, "UTF-8")).substring((new String(returnData, "UTF-8")).length()-22,(new String(returnData, "UTF-8")).length()-7);
			String choisRefHisString=(new String(returnData, "UTF-8")).substring((new String(returnData, "UTF-8")).length()-7,(new String(returnData, "UTF-8")).length()-2);
			JY.info("RECV MSG:"+new String(returnData, "UTF-8"));
			
			try {
				if(sendResult.equals("10")) {
					seclStatus.setSendResult("SUCCESS");
				}else {
					seclStatus.setSendResult("FAIL");
					seclStatus.setErrMsg(new String(returnData, "UTF-8"));
				}
				seclStatus.setIsReSend("N");
				seclStatus.setIsSend("Y");
				seclStatus.setChois_His_No(choisRefHisString);
				seclStatus.setChois_Ref_No(choisRefNoString);
				seclStatus.setSendMsg(new String(msgData, "UTF-8"));
			} catch (Exception sv) {
				if(null != sv)
				{
					JY.info("WebtServiceException msg:"+sv.toString());
					seclStatus.setErrMsg(sv.toString());
				}
				
				seclStatus.setSendResult("FAIL");
				Map<String, Object> instMap =new HashMap<String, Object>();
				instMap.put("secid", seclStatus.getSecid());
				instMap.put("tableName", seclStatus.getTableName());
				instMap.put("postdate", seclStatus.getPostdate());
				IfsIntfcSeclStatus oldSeclStatus = intfcOpicsMapper.querySeclStatusByDealnoSeq(instMap);
				if(null != oldSeclStatus)
				{
					seclStatus.setUpCount(oldSeclStatus.getUpCount()+1);
					seclStatus.setUpOwn("TMAX");
					seclStatus.setIsReSend("Y");
					if(intfcOpicsMapper.updateSeclStatusByDealnoSeq(seclStatus)>0)
					{
						retFlag = 2;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]进行重发更新[SL_SECL_STATUS]成功.");
					}else
					{
						retFlag = 1;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]进行重发更新[SL_SECL_STATUS]失败.");
					}
				}else
				{
					if(intfcOpicsMapper.insertSeclStatusByNew(seclStatus)>0)
					{
						retFlag = 2;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]插入状态表[SL_SECL_STATUS]成功.");
					}else {
						retFlag = 1;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]插入状态表[SL_SECL_STATUS]失败.");
					}
				}
				chiosLogin.main();
				throw sv;
			}
			
			long standardStampEnd = System.currentTimeMillis();
			JY.info("提交至主机返回耗时 ："+ (standardStampEnd - standardStampStart) + "毫秒");
			if ((standardStampEnd - standardStampStart) > 60000) {
				chiosLogin.main();
				JY.info("[ROLLBACK]");
				return -1;
			} else {
				Map<String, Object> instMap =new HashMap<String, Object>();
				instMap.put("secid", seclStatus.getSecid());
				instMap.put("tableName", seclStatus.getTableName());
				instMap.put("postdate", seclStatus.getPostdate());
				IfsIntfcSeclStatus oldSeclStatus = intfcOpicsMapper.querySeclStatusByDealnoSeq(instMap);
				if(null != oldSeclStatus)
				{
					seclStatus.setUpCount(oldSeclStatus.getUpCount()+1);
					seclStatus.setUpOwn("TMAX");
					seclStatus.setIsReSend("Y");
					if(intfcOpicsMapper.updateSeclStatusByDealnoSeq(seclStatus)>0)
					{
						retFlag = 2;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]进行重发更新[SL_SECL_STATUS]成功.");
					}else
					{
						retFlag = 1;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]进行重发更新[SL_SECL_STATUS]失败.");
					}
				}else
				{
					if(intfcOpicsMapper.insertSeclStatusByNew(seclStatus)>0)
					{
						retFlag = 2;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]插入状态表[SL_SECL_STATUS]成功.");
					}else {
						retFlag = 1;
						JY.info("SECID["+seclStatus.getSecid()+"]TABLENAME["+seclStatus.getTableName()+"]插入状态表[SL_SECL_STATUS]失败.");
					}
				}
				JY.info("[COMMIT]");
			}
		} catch (Exception e) {
			chiosLogin.main();
			
			e.printStackTrace();
			JY.info("[ROLLBACK] EXCEPTION:"+e.getMessage());
			throw e;
		} finally {
			
			
		}
		
		return retFlag;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
