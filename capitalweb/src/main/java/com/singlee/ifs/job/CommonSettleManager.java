/**
 * Project Name:capitalweb
 * File Name:CommonSettleManager.java
 * Package Name:com.singlee.ifs.job
 * Date:2018-9-29下午03:07:11
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.ifs.job;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlPmtqBean;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.opics.IPmtqServer;
import com.singlee.financial.opics.ISwiftServer;
import com.singlee.ifs.mapper.IfsCustSettleMapper;
import com.singlee.ifs.mapper.IfsOpicsCustMapper;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.model.IfsCustSettle;
import com.singlee.ifs.model.IfsOpicsCust;
import com.singlee.ifs.model.IfsTrdSettle;
import org.apache.commons.lang.StringUtils;
import org.springframework.remoting.RemoteConnectFailureException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName:CommonSettleManager <br/>
 * Function: 添加清算路径的公共方法
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-9-29 下午03:07:11 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CommonSettleManager {
	// 大额信息
	private IPmtqServer pmtqServer = SpringContextHolder.getBean("IPmtqServer");
	// IFS_CUST_SETTLE_ACCT
	private IfsCustSettleMapper custSettleMapper = SpringContextHolder.getBean(IfsCustSettleMapper.class);
	//IFS_CUST_SETTLE
	private IfsTrdSettleMapper trdSettleMapper = SpringContextHolder.getBean(IfsTrdSettleMapper.class);
	//查询交易对手信息
	private IfsOpicsCustMapper opicsCust= SpringContextHolder.getBean(IfsOpicsCustMapper.class);
	//查询SWIFT
	private ISwiftServer iSwiftServer = SpringContextHolder.getBean("ISwiftServer");
	// SWIFT报文数据
	IfsSwiftMapper ifsSwiftMapper = SpringContextHolder.getBean(IfsSwiftMapper.class);
	
	public void addSettle(String br,String dealno,String fedealno,String product,String type) throws RemoteConnectFailureException, Exception{
		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("br", br);
		queryMap.put("dealno", dealno);
		queryMap.put("seq", "0");
		queryMap.put("product", product);
		queryMap.put("type", type);
		// 获取一笔交易的收付款信息
		List<SlPmtqBean> pmtqList = pmtqServer.getPmtq(queryMap);
		for (SlPmtqBean slPmtqBean : pmtqList) {
			if(slPmtqBean.getSetmeans()!=null&&("DVP".equals(StringUtils.trimToEmpty(slPmtqBean.getSetmeans()))|| "NOS".equals(StringUtils.trimToEmpty(slPmtqBean.getSetmeans())))){
				continue;
			}
			//查询交易对手配置的清算路径和账户
			Map<String, Object> acctMap = new HashMap<String, Object>();
			acctMap.put("cno", slPmtqBean.getCno().trim());
			acctMap.put("product", slPmtqBean.getProduct().trim());
			IfsCustSettle ifsCustSettle=custSettleMapper.selectByCno(acctMap);
			//查询本行配置的清算路径和账户
			/*PropertiesConfiguration properties = PropertiesUtil.parseFile("system.properties");
			String cname=properties.getString("my_bank_cname");*/
			String cname=SystemProperties.my_bank_cname;
			IfsOpicsCust cust=opicsCust.searchByCname(cname);
			Map<String, Object> myMap = new HashMap<String, Object>();
			myMap.put("cno", cust.getCno().trim());
			myMap.put("product", slPmtqBean.getProduct().trim());
			IfsCustSettle myCustSettle=custSettleMapper.selectByCno(myMap);
			
			IfsTrdSettle ifsTrdSettle=new IfsTrdSettle();
			ifsTrdSettle.setBr(slPmtqBean.getBr().trim());
			ifsTrdSettle.setProduct(slPmtqBean.getProduct().trim());
			ifsTrdSettle.setProdtype(slPmtqBean.getType().trim());
			ifsTrdSettle.setDealno(slPmtqBean.getDealno().trim());
			ifsTrdSettle.setSeq(slPmtqBean.getSeq().trim());
			ifsTrdSettle.setCno(slPmtqBean.getCno().trim());
			ifsTrdSettle.setVdate(DateUtil.format(slPmtqBean.getVdate(),"yyyy-MM-dd"));
			ifsTrdSettle.setPayrecind(slPmtqBean.getPayrecind().trim());
			ifsTrdSettle.setCcy(slPmtqBean.getCcy().trim());
			ifsTrdSettle.setAmount(BigDecimal.valueOf(Double.valueOf(slPmtqBean.getAmount())));
			ifsTrdSettle.setCdate(DateUtil.format(slPmtqBean.getCdate(),"yyyy-MM-dd"));
			ifsTrdSettle.setCtime(slPmtqBean.getCtime().trim());
			ifsTrdSettle.setSetmeans(slPmtqBean.getSetmeans().trim());
			ifsTrdSettle.setSetacct(slPmtqBean.getSetacct().trim());
			ifsTrdSettle.setStatus(slPmtqBean.getStatus().trim());
			/*本行收款*/
			if("R".equals(slPmtqBean.getPayrecind())){
				if(myCustSettle !=null){
					//收款行行号
					ifsTrdSettle.setRecBankid(myCustSettle.getRbnkno());
					//收款行行名
					ifsTrdSettle.setRecBankname(myCustSettle.getRbnknm());
					//收款账号
					ifsTrdSettle.setRecUserid(myCustSettle.getRacctno());
					//收款账户名称
					ifsTrdSettle.setRecUsername(myCustSettle.getRacctnm());
					
				}
				if(ifsCustSettle != null){
					//付款行行号
					ifsTrdSettle.setPayBankid(ifsCustSettle.getPbnkno());
					//付款行行名
					ifsTrdSettle.setPayBankname(ifsCustSettle.getPbnknm());
					//付款账号
					ifsTrdSettle.setPayUserid(ifsCustSettle.getPacctno());
					//付款账户名称
					ifsTrdSettle.setPayUsername(ifsCustSettle.getPacctnm());
				}
			}
			/*本行付款*/
			if("P".equals(slPmtqBean.getPayrecind())){
				if(ifsCustSettle != null){
					//收款行行号
					ifsTrdSettle.setRecBankid(ifsCustSettle.getRbnkno());
					//收款行行名
					ifsTrdSettle.setRecBankname(ifsCustSettle.getRbnknm());
					//收款账号
					ifsTrdSettle.setRecUserid(ifsCustSettle.getRacctno());
					//收款账户名称
					ifsTrdSettle.setRecUsername(ifsCustSettle.getRacctnm());
				}
				if(myCustSettle !=null){
					//付款行行号
					ifsTrdSettle.setPayBankid(myCustSettle.getPbnkno());
					//付款行行名
					ifsTrdSettle.setPayBankname(myCustSettle.getPbnknm());
					//付款账号
					ifsTrdSettle.setPayUserid(myCustSettle.getPacctno());
					//付款账户名称
					ifsTrdSettle.setPayUsername(myCustSettle.getPacctnm());
				}
			}
			ifsTrdSettle.setHurryLevel("1");
			ifsTrdSettle.setHvpType1("0");
			ifsTrdSettle.setRemarkYt("0");
			ifsTrdSettle.setVoidflag("0");
			ifsTrdSettle.setDealflag("0");
			ifsTrdSettle.setSn(trdSettleMapper.getSn());
			ifsTrdSettle.setFedealno(fedealno);
			ifsTrdSettle.setSettflag("1");
			trdSettleMapper.insertSettle(ifsTrdSettle);
		}
		Map<String, Object> vmap = new HashMap<String, Object>();
		vmap.put("dealno", dealno);
		vmap.put("fedealno", fedealno);
		String vdate=trdSettleMapper.getMaxVdate(vmap);
		vmap.put("vdate", vdate);
		vmap.put("payrecind", 'P');
		trdSettleMapper.updateVdate(vmap);
		
	}//清算end
	
	/**
	 * 查询swift报文
	 * @param br
	 * @param dealno
	 * @param product
	 * @param type
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	public void addSwift(String br,String dealno,String product,String type) throws RemoteConnectFailureException, Exception{
		//查询swift报文
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("br", br);
		map.put("dealno", dealno);
		map.put("product", product);
//		map.put("type", type);
		map.put("type", "");//因为202等其他报文这个值opics是空值
		List<SlSwiftBean> swiftBeans= iSwiftServer.searchPageSwift(map);
		for (SlSwiftBean slSwiftBean : swiftBeans) {
			ifsSwiftMapper.insertSwiftData(slSwiftBean);
		} 
	}
	
}

