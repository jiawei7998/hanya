package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsfxFwdMapper;
import com.singlee.ifs.mapper.IfsCfetsfxSptMapper;
import com.singlee.ifs.mapper.IfsCfetsfxSwapMapper;
import com.singlee.ifs.model.IfsCfetsfxFwd;
import com.singlee.ifs.model.IfsCfetsfxSpt;
import com.singlee.ifs.model.IfsCfetsfxSwap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FxdMessageMonitor implements CronRunnable {
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor = SpringContextHolder.getBean("IBaseServer");

	private IfsCfetsfxSptMapper cfetsfxSptMapper = SpringContextHolder.getBean(IfsCfetsfxSptMapper.class);

	private IfsCfetsfxFwdMapper cfetsfxFwdMapper = SpringContextHolder.getBean(IfsCfetsfxFwdMapper.class);

	private IfsCfetsfxSwapMapper cfetsfxSwapMapper = SpringContextHolder.getBean(IfsCfetsfxSwapMapper.class);
	
	//人民币期权
	//private IfsCfetsfxOptionMapper cfetsfxOptionMapper =SpringContextHolder.getBean(IfsCfetsfxOptionMapper.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇买卖交易状态同步任务，开始······");
		/**
		 * 外汇模块分为外汇即期，外汇远期，外汇掉期,人民币期权
		 */
		// 查询全部未复核
 		List<IfsCfetsfxSpt> listSpt = cfetsfxSptMapper.getCfetsfxSptList();
		for (IfsCfetsfxSpt ifsCfetsfxSpt : listSpt) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FXD.SERVER);
			inthBean.setTag(SlDealModule.FXD.TAG);
			inthBean.setBr(ifsCfetsfxSpt.getBr());
			inthBean.setFedealno(ifsCfetsfxSpt.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇即期买卖交易状态同步任务，业务编号dealno:"+slInthBean.getDealno());
				cfetsfxSptMapper.updateCfetsfxSptByFedealno(map);
			}
		}
		
		

		//外汇远期
		List<IfsCfetsfxFwd> listFwd = cfetsfxFwdMapper.getCfetsfxFwdList();
		for (IfsCfetsfxFwd ifsCfetsfxFwd : listFwd) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FXD.SERVER);
			inthBean.setTag(SlDealModule.FXD.TAG);
			inthBean.setBr(ifsCfetsfxFwd.getBr());
			inthBean.setFedealno(ifsCfetsfxFwd.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇远期买卖交易状态同步任务，业务编号dealno:"+slInthBean.getDealno());
				cfetsfxFwdMapper.updateCfetsfxFwdByFedealno(map);
				
			}
		}
		
		//外汇掉期
		List<IfsCfetsfxSwap> listSwap = cfetsfxSwapMapper.getCfetsfxSwapList();
		for (IfsCfetsfxSwap ifsCfetsfxSwap : listSwap) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FXD.SERVER);
			inthBean.setTag(SlDealModule.FXD.TAG);
			inthBean.setBr(ifsCfetsfxSwap.getBr());
			inthBean.setFedealno(ifsCfetsfxSwap.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				if(null!=slInthBean.getSwapdealno()){
					map.put("farDealNo", slInthBean.getSwapdealno());	
				}
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇掉期买卖交易状态同步任务，业务编号dealno:"+slInthBean.getDealno());
				cfetsfxSwapMapper.updateCfetsfxSwapByFedealno(map);
				
			}
		}
		
		
		//外汇期权
		/*List<IfsCfetsfxOption> listOption = cfetsfxOptionMapper.getCfetsfxOptionList();
		for (IfsCfetsfxOption ifsCfetsfxOption : listOption) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.OTC.SERVER);
			inthBean.setTag(SlDealModule.OTC.TAG);
			inthBean.setBr("01");
			inthBean.setFedealno(ifsCfetsfxOption.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				logManager.info("==========开始执行FxdMessageMonitor ifsCfetsfxOption dealno dealno ="+slInthBean.getDealno()+"=============");
				cfetsfxOptionMapper.updateCfetsfxOptionByFedealno(map);
				// 清算相关处理
				if(!ifsCfetsfxOption.getNettingStatus().equals("1")){//不是净额清算的流向大额
					// 清算相关处理
					String br =StringUtils.trimToEmpty(slInthBean.getBr());
					String dealno =StringUtils.trimToEmpty(slInthBean.getDealno());
					String fedealno =StringUtils.trimToEmpty(slInthBean.getFedealno());
					String product =StringUtils.trimToEmpty(ifsCfetsfxOption.getProduct());
					String prodtype =StringUtils.trimToEmpty(ifsCfetsfxOption.getProdType());
					if (!dealno.equals("")) {
						logManager.info("==========开始执行FxdMessageMonitor ifsCfetsfxOption Settlement dealno ="+slInthBean.getDealno()+"=============");
						settleManager.addSettle(br, dealno,fedealno, product,prodtype);
						//logManager.info("==========开始执行FxdMessageMonitor ifsCfetsfxOption swift dealno ="+slInthBean.getDealno()+"=============");
						//settleManager.addSwift(br, dealno, product,prodtype);
					}//清算end
				}
			}
		}*/
		//获取当天，净额清算的swift中dealno不存在的交易
		/*List<IfsCfetsfxOption> listOption2 = cfetsfxOptionMapper.searchSwiftNotExist();
		for (IfsCfetsfxOption ifsCfetsfxOption : listOption2) {
			String br = "01";
			String dealno = StringUtils.trimToEmpty(ifsCfetsfxOption.getDealNo());
			String product = StringUtils.trimToEmpty(ifsCfetsfxOption.getProduct());
			String prodtype = StringUtils.trimToEmpty(ifsCfetsfxOption.getProdType());
			logManager.info("==========开始执行 外汇期权swift dealno ="+dealno+"=============");
			settleManager.addSwift(br, dealno, product,prodtype);
		}*/
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇买卖交易状态同步任务，结束·······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
