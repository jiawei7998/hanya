package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.baseUtil.SingleeJschUtil;

import java.util.Map;


public class CfetsJobStartManager implements CronRunnable {
	String host = SystemProperties.cstpVmIp;
	String user = SystemProperties.cstpVmUsername;
	String psw = SystemProperties.cstpVmPassword;
	int port = SystemProperties.cstpVmPort;
	// 本币
	String cstpRmbHome = SystemProperties.cstpRmbHome;
	String cstpRmbAgentStart = SystemProperties.cstpRmbAgentStart;
	String cstpRmbClientStart = SystemProperties.cstpRmbClientStart;
	String cstpRmbStop = SystemProperties.cstpRmbStop;
	// 外币
	String cstpFxHome = SystemProperties.cstpFxHome;
	String cstpFxAgentStart = SystemProperties.cstpFxAgentStart;
	String cstpFxClientStart = SystemProperties.cstpFxClientStart;
	String cstpFxStop = SystemProperties.cstpFxStop;

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时启动，开始······");
		/** cfets开启前先关闭 **/
		String resultFx = SingleeJschUtil.exec(host, user, psw, port, cstpFxStop);
		String resultRmb = SingleeJschUtil.exec(host, user, psw, port, cstpRmbStop);
		JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时启动,先关闭，关闭状态CSTP-FX:[%s];CSTP-RMB:[%s]",resultFx,resultRmb));
		/** 启动cfets **/
		StringBuffer commSb = new StringBuffer();
		if (!"".equals(cstpFxAgentStart)) {
			commSb.append(cstpFxHome + ";" + cstpFxAgentStart + ";");// 启动外币AGENT
		}
		if (!"".equals(cstpFxClientStart)) {
			commSb.append(cstpFxHome + ";" + cstpFxClientStart + ";");// 启动外币客户端
		}
		String retFx = SingleeJschUtil.exec(host, user, psw, port, commSb.toString());
		JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时启动，CSTP-FX:[%s];RET:[%s]",commSb.toString(),retFx));
		commSb = new StringBuffer();
		if (!"".equals(cstpRmbAgentStart)) {
			commSb.append(cstpRmbHome + ";" + cstpRmbAgentStart + ";");// 启动本币AGENT
		}
		if (!"".equals(cstpRmbClientStart)) {
			commSb.append(cstpRmbHome + ";" + cstpRmbClientStart + ";");// 启动本币客户端
		}
		String retRmb = SingleeJschUtil.exec(host, user, psw, port, commSb.toString());
		JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时启动，CSTP-RMB:[%s];RET:[%s]",commSb.toString(),retRmb));
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时启动，结束······");
		return true;
	}

	@Override
	public void terminate() {

	}

}
