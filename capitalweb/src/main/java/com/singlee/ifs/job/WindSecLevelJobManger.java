package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindSecLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得债券评级数据
 *
 */
public class WindSecLevelJobManger implements CronRunnable {

	private WindSecLevelService windSecLevelService = SpringContextHolder.getBean(WindSecLevelService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindSecLevelJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得债券评级数据==============================");
		Boolean run = windSecLevelService.run();
		logManager.info("==================万得债券评级数据处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
