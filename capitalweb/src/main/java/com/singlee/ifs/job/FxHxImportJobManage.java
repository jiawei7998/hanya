package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.service.UserService;
import com.singlee.financial.bean.*;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IfsCorePositionMapper;
import com.singlee.ifs.mapper.IfsOpicsHldyMapper;
import com.singlee.ifs.model.IfsCorePositionBean;
import com.singlee.ifs.model.IfsOpicsHldy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 核心 分发平台数据文件
 * @author
 *
 */


public class FxHxImportJobManage  implements CronRunnable{
	
	private static Logger logManager = LoggerFactory.getLogger(FxHxImportJobManage.class);
	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private IfsCorePositionMapper corePositionMapper = SpringContextHolder.getBean(IfsCorePositionMapper.class);
	private IfsOpicsHldyMapper ifsOpicsHldyMapper = SpringContextHolder.getBean(IfsOpicsHldyMapper.class);
	private IFxdServer fxdServer = SpringContextHolder.getBean(IFxdServer.class);
	private UserService UserService = SpringContextHolder.getBean(UserService.class);

	

	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> parameters) throws Exception {
		Date spotDate = new Date();
		String postDate = new SimpleDateFormat("yyyyMMdd").format(spotDate);

		logManager.info("==================Start FxHxImportJobManage execute==============================");
		Date postdate = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		String date = DateUtil.format(postdate, "yyyyMMdd");

		   //文件名
		   String src_folder =SystemProperties.hxFxfilename;
           //文件储存地址
		   String filePath = SystemProperties.hxPath;

		logManager.info("==================文件获取路径为"+filePath+"==============================");

		logManager.info("==================日期为"+postDate+"==============================");

		String fileName = filePath +src_folder+postDate+".dat";
		logManager.info("==================获取文件的名称为"+fileName+"==============================");

		File file = new File(fileName);
		if(!file.exists()) {
		logManager.info("找不到" + src_folder + "文件");
			return true;
		}
		BufferedReader readfile = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GB2312"));// 构造一个BufferedReader类来读取文件
		String line = null;
		IfsCorePositionBean ifsCorePositionBean = null;
		logManager.info("==================文件行数为"+readfile+"==============================");

		while ((line = readfile.readLine()) != null) {
		logManager.info("==================正在解析文件==============================");

			ifsCorePositionBean = new IfsCorePositionBean();
			String[] field = line.split("/");
			logManager.info("==================文件行数为"+field+"==============================");
			if (field.length > 1) {
				for (int i = 0; i < field.length; i++) {
					GenerateCorePosition(i, field[i], ifsCorePositionBean);
				}
				IfsOpicsHldy entity = new IfsOpicsHldy();
					entity.setCalendarid("CNY");
					entity.setHolidate(new Date());
					IfsOpicsHldy searchEntity = ifsOpicsHldyMapper.searchEntity(entity);
					String isHd = searchEntity == null ? "yes" : "no";
					// 拿文件里边日期去判断是否为假期
					if ("no".equals(isHd)) {
						ifsCorePositionBean.setVdate(DateUtil.format(postdate));
						ifsCorePositionBean.setDealDate(ifsCorePositionBean.getVdate());
					} else {
						ifsCorePositionBean.setDealDate(ifsCorePositionBean.getVdate());
					}
				  
					
					
					//1、卖出   2、买入  入表反向
					if("2".equals(ifsCorePositionBean.getPsFlag())) {
					    ifsCorePositionBean.setPsFlag("S");  
					}else {
					    ifsCorePositionBean.setPsFlag("P");  
                    }
					//1、正常  2、冲正
					if("1".equals(ifsCorePositionBean.getRevFlag())) {
					    ifsCorePositionBean.setDealtype(TradeConstants.Dealtype.NORMAL);
					}else {
					    ifsCorePositionBean.setDealtype(TradeConstants.Dealtype.WRITE_OFF);
					}
					ifsCorePositionBean.setFlag("0");
				
					
					ifsCorePositionBean.setCust(SystemProperties.Positioncust);
					ifsCorePositionBean.setCost(SystemProperties.Positiongjjqcost);
					ifsCorePositionBean.setPort(SystemProperties.Positiongjjqport);
					ifsCorePositionBean.setTrad(SystemProperties.Positiontrad);
					ifsCorePositionBean.setSource("柜面");
					ifsCorePositionBean.setServer("POSITION");
					ifsCorePositionBean.setUpdateDate(new Timestamp(new Date().getTime()));
					ifsCorePositionBean.setStatus("SENDOK");
					
					IfsCorePositionBean icpb = new IfsCorePositionBean();
					icpb.setFedealNo(ifsCorePositionBean.getFedealNo());

						ifsCorePositionBean.setOpicsDate(postdate);
						logManager.info("先删除后插入");
		               
						IfsCorePositionBean   corePositionBeans=new IfsCorePositionBean();
						corePositionBeans.setFedealNo(ifsCorePositionBean.getFedealNo());
						 IfsCorePositionBean   corePositionBean=corePositionMapper.selectOne(corePositionBeans);
						if(corePositionBean==null) {
						    corePositionMapper.insertSelective(ifsCorePositionBean);
						    //插入INTH和IFXD表
	                        insertOpics(ifsCorePositionBean);
						}

						
					logManager.info("核心 分发平台数据【" + ifsCorePositionBean.getFedealNo() + "】保存到接口成功");
				


			}
		}


		readfile.close();
		
	logManager.info("==================End FxHxImportJobManage execute==============================");
		return true;
	}



	/**
	 * 解析文件 组装数据
	 * @param i
	 * @param str
	 * @param ifsCorePositionBean
	 */
	public void GenerateCorePosition(int i, String str, IfsCorePositionBean ifsCorePositionBean) {
		switch (i) {
		case 1:ifsCorePositionBean.setVdate(str.trim());				break;
		case 2:ifsCorePositionBean.setFedealNo(str.trim());				break;
		case 8:ifsCorePositionBean.setRevFlag(str.trim());               break;
		case 9:ifsCorePositionBean.setPsFlag(str.trim());             break;
		case 12:
			String ccy = StringUtil.isEmpty(str.trim()) ? "CNY" : str.trim();
			ifsCorePositionBean.setCcy(ccy);
			break;
		case 13:
			String ctrccy = StringUtil.isEmpty(str.trim()) ? "CNY" : str.trim();
			ifsCorePositionBean.setCtrCcy(ctrccy);
			break;
		case 26:
			BigDecimal b26 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b26.setScale(4, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCcyAmt(b26.toPlainString());
			break;
		case 27:
			BigDecimal b27 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b27.setScale(4, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCtrAmt(b27.toPlainString());
			break;
		case 22:
			BigDecimal b14 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b14.setScale(8, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCcyRate(b14.toPlainString());
			break;
//		case 5:ifsCorePositionBean.setTranbrief(str.trim());			break;
		case 39:ifsCorePositionBean.setRlTimeFromRate(str.trim());             break;
		case 40:ifsCorePositionBean.setRlTimeToRate(str.trim());             break;
		case 41:
			BigDecimal b41 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b41.setScale(2, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCust(b41.toPlainString());
			break;
		case 50:ifsCorePositionBean.setCost(str.trim());				break;
		default:break;
		}
	}



@Override
public void terminate() {
    // TODO Auto-generated method stub
    
}

@SuppressWarnings("null")
public void insertOpics(IfsCorePositionBean ifsCorePositionBean) throws RemoteConnectFailureException, Exception {
    Date fed=new Date();
    String feds=  DateUtil.format(fed,"yyyyMMdd");
    
    String server = null;
    if(ifsCorePositionBean.getDealtype().equals(TradeConstants.Dealtype.WRITE_OFF)) {   
        server=ifsCorePositionBean.getServer()+"_R";
    }else {
        server=ifsCorePositionBean.getServer();
    }
//获取机构号
    TaUser user= UserService.getUserById(SystemProperties.cfetsDefautUserId);
  
    String br="01";
    if(null==user){
    
        br=user.getOpicsBr();
   }
   
    
    SlFxSptFwdBean slFxSptFwdBean =new SlFxSptFwdBean(br, server, SlDealModule.FXD.TAG, SlDealModule.FXD.DETAIL);
    
    InstitutionBean institutionInfo = new InstitutionBean();
    InstitutionBean contraPatryInstitutionInfo = new InstitutionBean();
    FxCurrencyBean currencyInfo = new FxCurrencyBean();
    SlExternalBean externalBean = new SlExternalBean();

  
    SlInthBean inthBean = slFxSptFwdBean.getInthBean();
    slFxSptFwdBean.setDealDate(DateUtil.parse(ifsCorePositionBean.getDealDate()));
    slFxSptFwdBean.setValueDate(DateUtil.parse(ifsCorePositionBean.getDealDate()));
   
    //交易员id
    institutionInfo.setTradeId(SystemProperties.Positiontrad);
   contraPatryInstitutionInfo.setInstitutionId(SystemProperties.Positioncust);
//    
    slFxSptFwdBean.setInstitutionInfo(institutionInfo);
   slFxSptFwdBean.setContraPatryInstitutionInfo(contraPatryInstitutionInfo);
//    
    //货币币种、金额、汇率
    currencyInfo.setAmt(new BigDecimal(ifsCorePositionBean.getCcyAmt()));
    currencyInfo.setCcy(ifsCorePositionBean.getCcy());
    currencyInfo.setContraCcy(ifsCorePositionBean.getCtrCcy());
    currencyInfo.setContraAmt(new BigDecimal(ifsCorePositionBean.getCtrAmt()));
    //currencyInfo.setRate(new BigDecimal(ifsCorePositionBean.getCcyRate()));
   //成本中心、投资组合、fedealno
//    externalBean.setCost(SystemProperties.Positioncust);
    
    externalBean.setCost("102010");
    externalBean.setBroker("D");
    externalBean.setProdcode("FXD");
    externalBean.setProdtype("FX");
    externalBean.setPort(SystemProperties.Positiongjjqport);
    externalBean.setAuthsi("1");
    externalBean.setSiind("1");
    externalBean.setSupconfind("1");
    externalBean.setSuppayind("1");
    externalBean.setSuprecind("1");
    externalBean.setVerind("1");
    externalBean.setDealtext(ifsCorePositionBean.getFedealNo());
    slFxSptFwdBean.setExternalBean(externalBean);
    
    String fedss=corePositionMapper.getNm();
    inthBean.setFedealno(feds+fedss);
    inthBean.setLstmntdate(baseServer.getOpicsSysDate(br));
    slFxSptFwdBean.setInthBean(inthBean);
    slFxSptFwdBean.setDealDate(DateUtil.parse(ifsCorePositionBean.getDealDate()));

    if ("P".equals(ifsCorePositionBean.getPsFlag())) {
        slFxSptFwdBean.setPs(PsEnum.P);
    }
    if ("S".equals(ifsCorePositionBean.getPsFlag())) {
        slFxSptFwdBean.setPs(PsEnum.S);
    }
    slFxSptFwdBean.setExecId(ifsCorePositionBean.getFedealNo());
    slFxSptFwdBean.setCurrencyInfo(currencyInfo);
   

    SlOutBean result = fxdServer.fxSptFwd(slFxSptFwdBean);// 用注入的方式调用opics相关方法
    
    if (!result.getRetStatus().equals(RetStatusEnum.S)) {
        JY.raise("插入opics表失败......");
    }
    


}


}
