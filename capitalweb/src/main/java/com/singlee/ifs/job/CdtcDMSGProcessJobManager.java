package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.cdtc.CdtcDMSGProcessJobServer;
import com.singlee.financial.pojo.RetBean;

import java.util.Map;

/**
 * @apiNote 目前不执行
 * @author Administrator
 *
 */
public class CdtcDMSGProcessJobManager implements CronRunnable {

	private CdtcDMSGProcessJobServer cdtcDMSGProcessJobServer = SpringContextHolder.getBean("CdtcDMSGProcessJobServer");
//	@Autowired
//	private CdtcDMSGProcessJobServer cdtcDMSGProcessJobServer;
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 中债】，中债_解析报文，开始······");
		RetBean ret = cdtcDMSGProcessJobServer.readDMSGXmlFiles();
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 中债】，中债_解析报文，结束······");
		return true;
	}

	@Override
	public void terminate() {

	}

}
