package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.financial.bean.SlPmtqAcupBean;
import com.singlee.financial.esb.hbcb.bean.common.ExchangeHeader;
import com.singlee.financial.esb.hbcb.bean.common.MasterHeader;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.ReqDetail1;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.RequestBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.S84015Bean;
import com.singlee.financial.esb.hbcb.service.S100001001084015Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.financial.esb.hrbcb.ISlPmtqAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.model.IfsEsbMassage;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.text.DecimalFormat;
import java.util.*;

public class DaySettleJobManage implements CronRunnable {

	private ISlPmtqAcupServer iSlPmtqAcupServer = SpringContextHolder.getBean("ISlPmtqAcupServer");
	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private S100001001084015Service s100001001084015Service = SpringContextHolder.getBean(S100001001084015Service.class);
	private IfsEsbMassageMapper ifsEsbMassageMapper = SpringContextHolder.getBean(IfsEsbMassageMapper.class);
	private TaSysParamMapper taSysParamMapper = SpringContextHolder.getBean(TaSysParamMapper.class);

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->核心】，日间清算发送任务，开始······");
		Date date = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		//Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-12-17");
		SlPmtqAcupBean spab1 = new SlPmtqAcupBean();
		spab1.setPostdate(date);
		spab1.setIssuccess("N");// 未记账成功
		spab1.setIssend("N");// 未发送
		spab1.setRevflag("N");// 未冲销
		List<SlPmtqAcupBean> list = iSlPmtqAcupServer.getSlPmtqAcupList(spab1);

		if (null == list || list.size() == 0) {
			return true;
		}

		for (SlPmtqAcupBean spab2 : list) {
			// 开始组装数据
			S84015Bean s84015Bean = new S84015Bean();
			Map<String, Object> sysMap = new HashMap<String, Object>();
			sysMap.put("p_type", "ESB");
			List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
			// 请求头
			RequestHeader requestHeader = new RequestHeader();
			ExchangeHeader exchangeHeader = new ExchangeHeader();
			MasterHeader masterHeader = new MasterHeader();
			// 请求体
			RequestBody requestBody = new RequestBody();
			ReqDetail1 detail1 = new ReqDetail1();
			
			GenerateS84015Bean(requestBody, requestHeader, sysList);
			
			requestHeader.setBrchNo("00100");// 机构号
			requestHeader.setUUID(EsbSend.createUUID());
			requestHeader.setReqTm(String.valueOf(System.currentTimeMillis()));// 请求方交易时间戳
			requestHeader.setExchangeHeader(exchangeHeader);
			requestHeader.setMasterHeader(masterHeader);

			GenerateAcupDR(detail1, spab2.getDrno(), "0" + spab2.getBr() + "00");
			GenerateAcupCR(detail1, spab2.getCrno(), "0" + spab2.getBr() + "00");
			
			detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? "" : detail1.getPromptNum());// O 提示码   新科目号
			detail1.setAmt(new DecimalFormat("0.00").format(spab2.getAmount()));

			List<ReqDetail1> detailist = new ArrayList<>();
			detailist.add(detail1);
			s84015Bean.setReqDetail1(detailist);
			s84015Bean.setRequestBody(requestBody);
			s84015Bean.setRequestHeader(requestHeader);
			EsbOutBean outBean = s100001001084015Service.send(s84015Bean);
			if ("0020010000".equals(outBean.getRetCode())) {// 发送成功
				spab2.setIssuccess("Y");// 已处理
				spab2.setIssend("Y");// 已发送
				spab2.setFedealno(outBean.getClientNo());// 记账流水
			}
			spab2.setCoreDealno(outBean.getTellSeqNo());// 核心流水
			spab2.setErrmsg(outBean.getRetMsg());
			spab2.setSendmsg(outBean.getSendmsg());
			spab2.setResultmsg(outBean.getRecmsg());
			iSlPmtqAcupServer.updateSlPmtqAcup(spab2);// 更新处理状态为已成功发送
			// 保存报文
			IfsEsbMassage ifsEsbMassage = new IfsEsbMassage();
			ifsEsbMassage.setDealno(spab2.getDealno().trim());
			ifsEsbMassage.setCustno(spab2.getCno().trim());
			ifsEsbMassage.setProduct(spab2.getProduct().trim());
			ifsEsbMassage.setProducttype(spab2.getType().trim());
			List<IfsEsbMassage> select = ifsEsbMassageMapper.select(ifsEsbMassage);
			if (select.size() == 1) {
				ifsEsbMassage = select.get(0);
				ifsEsbMassage.setSenddate(new Date());
				ifsEsbMassage.setRecdate(new Date());
				ifsEsbMassage.setSendmsg(outBean.getSendmsg());
				ifsEsbMassage.setRecmsg(outBean.getRecmsg());
				ifsEsbMassage.setRescode(outBean.getRetCode());
				ifsEsbMassage.setResmsg(outBean.getRetMsg());
				Example example = new Example(IfsEsbMassage.class);
				example.createCriteria().andEqualTo("dealno", ifsEsbMassage.getDealno())
						.andEqualTo("custno", ifsEsbMassage.getCustno())
						.andEqualTo("product", ifsEsbMassage.getProduct())
						.andEqualTo("producttype", ifsEsbMassage.getProducttype())
						.andEqualTo("service",ifsEsbMassage.getService());
				ifsEsbMassageMapper.updateByExample(ifsEsbMassage, example);
			} else if (select.size() == 0) {
				ifsEsbMassage.setAmount(spab2.getAmount().toString());
				ifsEsbMassage.setCcy(spab2.getCcy());
				ifsEsbMassage.setSetmeans("NOS");
				ifsEsbMassage.setService("S100001001084015");
				ifsEsbMassage.setSenddate(new Date());
				ifsEsbMassage.setRecdate(new Date());
				ifsEsbMassage.setSendmsg(outBean.getSendmsg());
				ifsEsbMassage.setRecmsg(outBean.getRecmsg());
				ifsEsbMassage.setRescode(outBean.getRetCode());
				ifsEsbMassage.setResmsg(outBean.getRetMsg());
				ifsEsbMassageMapper.insert(ifsEsbMassage);
			}
			JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->核心】，日间清算发送任务，发送报文业务编号："+spab2.getDealno().trim());
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->核心】，日间清算发送任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
	
	/**
	 * 组装DR
	 * @param detail1
	 * @param intglno
	 * @param subbr
	 */
	public static void GenerateAcupDR(ReqDetail1 detail1, String intglno, String subbr) {
		int length = intglno.trim().length();
		if (length > 2) {// BGL
			detail1.setTfroutAcctNo(StringUtil.stringFormat(intglno, "0", 17, "L"));
			detail1.setTfroutAcctNoSys("BGL");
		} else if (length == 2) {// CGL
			detail1.setTfroutProCode(intglno);
			detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? intglno : "");// 两个账号都为CGL 该字段为空
			detail1.setTfroutBrch(subbr);
		}
	}
	
	/**
	 * 组装CR
	 * @param detail1
	 * @param intglno
	 * @param subbr
	 */
	public static void GenerateAcupCR(ReqDetail1 detail1, String intglno, String subbr) {
		int length = intglno.trim().length();
		if (length > 2) {// BGL
			detail1.setTfrinAcctNo(StringUtil.stringFormat(intglno, "0", 17, "L"));
			detail1.setTfrinAcctNoSys("BGL");
		} else if (length == 2) {
			detail1.setTfrInProCode(intglno);
			detail1.setPromptNum(StringUtil.isEmpty(detail1.getPromptNum()) ? intglno : "");// 两个账号都为CGL 该字段为空
			detail1.setTfrInBrch(subbr);
		}
	}

	/**
	 * 初始化请求报文
	 * @param requestBody
	 * @param requestHeader
	 * @param sysList
	 */
	public static void GenerateS84015Bean(RequestBody requestBody, RequestHeader requestHeader,List<TaSysParam> sysList) {
		for (TaSysParam taSysParam : sysList) {
			if ("VerNo".equals(taSysParam.getP_code())) {
				// 版本号 非必输
				requestHeader.setVerNo(taSysParam.getP_value().trim());
			} else if ("ReqSysCd".equals(taSysParam.getP_code())) {
				// 请求方系统代码 非必输
				requestHeader.setReqSysCd(taSysParam.getP_value().trim());
			} else if ("TxnTyp".equals(taSysParam.getP_code())) {
				// 交易类型 非必输
				requestHeader.setTxnTyp(taSysParam.getP_value().trim());
			} else if ("TxnMod".equals(taSysParam.getP_code())) {
				// 交易模式 非必输
				requestHeader.setTxnMod(taSysParam.getP_value().trim());
			} else if ("TerminalType".equals(taSysParam.getP_code())) {
				// 终端类型 输入有效值：0，1，2 0：柜员前端和ESB
				requestHeader.setTerminalType(taSysParam.getP_value().trim());
			} else if ("Flag1".equals(taSysParam.getP_code())) {
				// 标识1 0-正常交易 1-倒退日交易 2-正常授权交易 3-倒退日授权交易 4,5,6,7ATM/POS交易使用 倒退日交易见详细的各个模块交易说明
				requestHeader.setFlag1(taSysParam.getP_value().trim());
			} else if ("Flag4".equals(taSysParam.getP_code())) {
				// 标识4 渠道标识： 0-柜面 5-ESB（表示渠道交易）
				requestHeader.setFlag4(taSysParam.getP_value().trim());
			} else if ("ChnlNo".equals(taSysParam.getP_code())) {
				// 渠道号
				requestHeader.setChnlNo(taSysParam.getP_value().trim());
				// 渠道编号
				requestBody.setChannelNo(taSysParam.getP_value().trim());
			} else if ("TrmNo".equals(taSysParam.getP_code())) {
				// 终端号 柜员的终端号是通过银行内部分配的；渠道的终端号为“000”。
				requestHeader.setTrmNo(taSysParam.getP_value().trim());
			} else if ("TlrNo".equals(taSysParam.getP_code())) {
				// 柜员号
				requestHeader.setTlrNo(taSysParam.getP_value().trim());
			} else if ("BancsSeqNo".equals(taSysParam.getP_code())) {
				// 流水号 输入填0，输出为bancs的流水号. 多金融组合交易的流水号是一样的
				requestHeader.setBancsSeqNo(taSysParam.getP_value().trim());
			} else if ("SupervisorID".equals(taSysParam.getP_code())) {
				// 复核或授权时输入主管id号，复核或授权的时候才有效，其他建议填”0000000”。主管ID只提供给柜面前端使用，其他外围系统的授权处理在各自的业务系统
				requestHeader.setSupervisorID(taSysParam.getP_value().trim());
			}
		}
	}

}
