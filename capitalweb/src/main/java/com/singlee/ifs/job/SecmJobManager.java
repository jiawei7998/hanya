package com.singlee.ifs.job;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.capital.choistrd.service.SecmService;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;

public class SecmJobManager implements CronRunnable{
	
	private SecmService secmService = SpringContextHolder.getBean(SecmService.class);

	private static Logger logManager = LoggerFactory.getLogger(SecmJobManager.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================债券基础信息接口定时任务开始==============================");
		Boolean run = secmService.executeHostVisitAction();
		logManager.info("==================债券基础信息接口定时任务完成==============================");
		return run;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
