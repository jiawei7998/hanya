package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.opics.IFifoServer;
import com.singlee.ifs.service.IfsOpsCreateFifoService;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 *生成FIFO表数据
 */
public class OpcCreatFifoJobManage implements CronRunnable {

	private final IFifoServer fifoServer = SpringContextHolder.getBean("IFifoServer");
	private final IfsOpsCreateFifoService ifsOpsCreateFifoService = SpringContextHolder.getBean(IfsOpsCreateFifoService.class);


	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，增值税任务处理，开始······");
		String br = "01";
		Map<String,Object> map=new HashMap<>();
		map.put("br",br);
		ifsOpsCreateFifoService.generateData(map);
		/*// 根据机构删除数据
		fifoServer.deleteFifoByBr(br);

		List<SlSpshViewBean> svlist = fifoServer.selectSpshViewListGroup(br);// 查询总交易

		List<SlFifoBean> fbList = new ArrayList<SlFifoBean>();

		for (SlSpshViewBean sv : svlist) {

			int ind = 0;
			BigDecimal buyRemainAmount = new BigDecimal("0");
			BigDecimal sellRemainAmount = new BigDecimal("0");
			String currSellDealNo = "        ";
			Date currSellSettDate = null;

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("br", br);
			map.put("invtype", sv.getInvtype());
			map.put("secid", sv.getSecid());
			map.put("cost", sv.getCost());
			map.put("port", sv.getPort());

			List<SlSpshViewBean> svplist = fifoServer.selectSpshViewListP(map); // 查询买入交易

			for (SlSpshViewBean svp : svplist) {
				buyRemainAmount = svp.getFaceamt();

				do {
					if (sellRemainAmount.compareTo(new BigDecimal("0")) == 0) {
						map.put("dealno", currSellDealNo);
						SlSpshViewBean svs = fifoServer.selectSpshViewS(map); // 查询卖出
						if (svs == null) {
							ind = 0;

							SlFifoBean fifo = new SlFifoBean();
							fifo.setBr(svp.getBr());
							fifo.setPdealno(svp.getDealno());
							fifo.setSeq(String.valueOf(ind));
							fifo.setCost(svp.getCost());
							fifo.setCcy("CNY");
							fifo.setInvtype(svp.getInvtype());
							fifo.setPort(svp.getPort());
							fifo.setSecid(svp.getSecid());
							fifo.setPurchasedate(svp.getSettdate());
							fifo.setPamount(buyRemainAmount);

							fbList.add(fifo);
							break;
						} else {
							currSellDealNo = svs.getDealno();
							currSellSettDate = svs.getSettdate();
							sellRemainAmount = svs.getFaceamt();
							ind = 0;

						}
					}

					SlFifoBean fifo = new SlFifoBean();
					fifo.setBr(svp.getBr());
					fifo.setPdealno(svp.getDealno());
					fifo.setSeq(String.valueOf(ind));
					fifo.setCost(svp.getCost());
					fifo.setCcy("CNY");
					fifo.setInvtype(svp.getInvtype());
					fifo.setPort(svp.getPort());
					fifo.setSecid(svp.getSecid());
					fifo.setPurchasedate(svp.getSettdate());
					fifo.setSdealno(currSellDealNo);
					fifo.setSaledate(currSellSettDate);

					if (buyRemainAmount.compareTo(sellRemainAmount) > -1) {
						fifo.setPamount(sellRemainAmount);
						fifo.setSamount(sellRemainAmount);
						fbList.add(fifo);

						ind++;
						buyRemainAmount = buyRemainAmount.subtract(sellRemainAmount);
						sellRemainAmount = new BigDecimal("0");

						if (buyRemainAmount.compareTo(new BigDecimal("0")) == 0) {
							break;
						}
					}

					if (buyRemainAmount.compareTo(sellRemainAmount) == -1) {
						fifo.setPamount(buyRemainAmount);
						fifo.setSamount(buyRemainAmount);
						fbList.add(fifo);

						ind++;
						sellRemainAmount = sellRemainAmount.subtract(buyRemainAmount);
						buyRemainAmount = new BigDecimal("0");
						break;
					}

				} while (true);

			}
			if (fbList.size() > 200) {
				fifoServer.bathInsertFifo(fbList);
				fbList.clear();
			}
		}
		fifoServer.bathInsertFifo(fbList);*/
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->前置】，增值税任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}

}
