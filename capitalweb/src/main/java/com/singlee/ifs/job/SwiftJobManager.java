package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISwiftServer;
import com.singlee.generated.model.field.Field21;
import com.singlee.ifs.mapper.IfsSwiftMapper;
import com.singlee.swift.model.SwiftMessage;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报文取回任务
 *
 */
public class SwiftJobManager implements CronRunnable {

	private ISwiftServer swiftServer = SpringContextHolder.getBean("ISwiftServer");

	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");


	private DictionaryGetService dictionaryGetService = SpringContextHolder.getBean(DictionaryGetService.class);;
	// SWIFT报文数据
	IfsSwiftMapper ifsSwiftMapper = SpringContextHolder.getBean(IfsSwiftMapper.class);

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->SWIFT】，报文取回任务定时任务，开始······");
		// 查询swift报文
		Map<String, Object> map = new HashMap<String, Object>();
		Date cdate = baseServer.getOpicsSysDate("01");
		map.put("cdate", DateUtil.format(cdate));
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->SWIFT】，报文取回任务定时任务，查询当日SL_WMTI表里未发送的数据");
		List<SlSwiftBean> swiftBeans = swiftServer.searchSwiftBySendflag(map);
		for (SlSwiftBean slSwiftBean : swiftBeans) {
			// 屏蔽210类型报文
			if ("210".equals(slSwiftBean.getMsgType().trim())) {
				continue;
			}
			if ("SECUR".equals(slSwiftBean.getProdCode().trim())) {
				continue;
			}
			// 判断是否为 外汇 , 202类型报文
			slSwiftBean.setMsg(slSwiftBean.getMsg().replaceFirst("\r\n", ""));
			if ("FXD".equals(slSwiftBean.getProdCode().trim()) & "202".equals(slSwiftBean.getMsgType().trim())) {
				SwiftMessage swift = SwiftMessage.parse(slSwiftBean.getMsg());
				String field = dictionaryGetService.getTaDictByCodeAndKey("SwiftTag21", "202").getDict_value();
				swift.getBlock4().getTagByName(Field21.NAME).setValue(field);// 设置报文21域的值
				slSwiftBean.setMsg(swift.toMT().message());
			}
			ifsSwiftMapper.insertSwiftData(slSwiftBean);

			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("sendflag", "1");
			map2.put("br", StringUtils.trimToEmpty(slSwiftBean.getBr()));
			map2.put("dealno", StringUtils.trimToEmpty(slSwiftBean.getDealNo()));
			map2.put("batchno", StringUtils.trimToEmpty(slSwiftBean.getBatchNo()));
			// map2.put("seq", StringUtils.trimToEmpty(slSwiftBean.getseq()));
			map2.put("prodcode", StringUtils.trimToEmpty(slSwiftBean.getProdCode()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			map2.put("cdate", sdf.format(slSwiftBean.getCdate()));
			// map2.put("ctime", StringUtils.trimToEmpty(slSwiftBean.getCtime()));
			map2.put("msgtype", StringUtils.trimToEmpty(slSwiftBean.getMsgType()));
			// map2.put("msgseq", StringUtils.trimToEmpty(slSwiftBean.getM));
			// map2.put("msgstatus", StringUtils.trimToEmpty(slSwiftBean.getBr()));

			swiftServer.updateSendflag(map2);
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->SWIFT】，报文取回任务定时任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
	}

}
