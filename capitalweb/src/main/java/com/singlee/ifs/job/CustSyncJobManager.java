package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.ifs.service.IfsOpicsCustService;

import java.util.Map;

/**
 * @author Liang
 * @date 2021/11/3 15:30
 * =======================
 */
public class CustSyncJobManager implements CronRunnable {

    private IfsOpicsCustService ifsOpicsCustService= SpringContextHolder.getBean(IfsOpicsCustService.class);
    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {
        JY.info("[JOBS-START] ==> 开始执行【opics -->前置表】，CUST数据同步定时任务，开始······");
        ifsOpicsCustService.batchCalibration("2",null);
        JY.info("[JOBS-END] ==> 开始执行【opics -->前置表】，CUST数据同步定时任务，结束······");
        return true;
    }

    @Override
    public void terminate() {

    }
}
