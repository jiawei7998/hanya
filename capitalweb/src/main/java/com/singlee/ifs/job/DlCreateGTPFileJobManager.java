package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.esb.dlcb.ICreateGTPFileServer;
import com.singlee.financial.opics.IBaseServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class DlCreateGTPFileJobManager implements CronRunnable {
	/**
	 * TDH数据平台数据仓库供数
	 */

	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	
	private ICreateGTPFileServer createGTPFileServer = SpringContextHolder.getBean("ICreateGTPFileServer");
	
	private static Logger logManager = LoggerFactory.getLogger(DlCreateGTPFileJobManager.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始执行DlCreateGTPFileJobManager==============================");
		
		Map<String, Object> map=new HashMap<String, Object>();
		//根据OPICS系统日期查询
		/*Date sysDate=baseServer.getOpicsSysDate("01");
		String date=DateUtil.format(sysDate, "yyyyMMdd");
		map.put("queryDate",date);*/
		
		String curbranprcDate=DateUtil.getCurrentDateAsString("yyyyMMdd");
		map.put("queryDate",curbranprcDate);

		SlOutBean result =  createGTPFileServer.createGTPFile(map);
		String retCode = result.getRetCode();
		if(	null != retCode && "S".equals(retCode)){
			logManager.info("==================数据仓库文件生成成功==============================");
		}else {
			logManager.info("==================数据仓库文件生成失败==============================");
		}	
		logManager.info("==================DlCreateGTPFileJobManager任务结束==============================");
		return true;
	}

	
	
	
	
	
	@Override
	public void terminate() {
	}




}
