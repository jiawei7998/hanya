package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsrmbCbtMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDpMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDpOfferMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsCfetsrmbCbt;
import com.singlee.ifs.model.IfsCfetsrmbDp;
import com.singlee.ifs.model.IfsCfetsrmbDpOffer;
import com.singlee.ifs.model.IfsOpicsBond;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FiMessageMonitor implements CronRunnable {
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor = SpringContextHolder.getBean("IBaseServer");
	private IfsCfetsrmbCbtMapper cfetsrmbCbtMapper = SpringContextHolder.getBean(IfsCfetsrmbCbtMapper.class);
	private IfsCfetsrmbDpOfferMapper cfetsrmbDpOfferMapper = SpringContextHolder
			.getBean(IfsCfetsrmbDpOfferMapper.class);
	private IfsOpicsBondMapper ifsOpicsBondMapper = SpringContextHolder.getBean(IfsOpicsBondMapper.class);
	private IfsCfetsrmbDpMapper cfetsrmbDpMapper = SpringContextHolder
			.getBean(IfsCfetsrmbDpMapper.class);
	
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖交易状态同步任务，开始······");
		/**
		 * 现券买卖
		 */
		// 查询全部未复核
		List<IfsCfetsrmbCbt> listCbt = cfetsrmbCbtMapper.getCfetsrmbCbtList();
		for (IfsCfetsrmbCbt ifsCfetsrmbCbt : listCbt) {
			SlInthBean inthBean = new SlInthBean();
			IfsOpicsBond bond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());

			if (null != bond && "MBS".equals(StringUtils.trimToEmpty(bond.getIntcalcrule()))) {
				inthBean.setServer(SlDealModule.IMBD.SERVER);
				inthBean.setTag(SlDealModule.IMBD.TAG);
			} else {
				inthBean.setServer(SlDealModule.FI.SERVER);
				inthBean.setTag(SlDealModule.FI.TAG);
			}
			inthBean.setBr(ifsCfetsrmbCbt.getBr());
			inthBean.setFedealno(ifsCfetsrmbCbt.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				cfetsrmbCbtMapper.updateCfetsrmbCbtByFedealno(map);
			}
		}
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存单发行交易同步任务，开始······");
		/**
		 * 存单发行
		 */
		// 查询全部未复核
		List<IfsCfetsrmbDpOffer> listDpOffer = cfetsrmbDpOfferMapper.getCfetsrmbDpOfferList();
		for (IfsCfetsrmbDpOffer ifsCfetsrmbDpOffer : listDpOffer) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FI.SERVER);
			inthBean.setTag(SlDealModule.FI.TAG);
			inthBean.setBr(ifsCfetsrmbDpOffer.getBr());
			inthBean.setSeq(ifsCfetsrmbDpOffer.getSeq());
			inthBean.setFedealno(ifsCfetsrmbDpOffer.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				map.put("seq", slInthBean.getSeq());
				cfetsrmbDpOfferMapper.updateCfetsrmbDpOfferByFedealno(map);
				
				IfsCfetsrmbDp searchBondLon = cfetsrmbDpMapper.searchBondLon(slInthBean.getFedealno());
				if("-4".equals(slInthBean.getStatcode().trim()) ) {
					String dealno = StringUtils.isEmpty(searchBondLon.getDealNo()) ? "" : searchBondLon.getDealNo() + ",";
					searchBondLon.setDealNo(dealno + slInthBean.getDealno());
				}
				cfetsrmbDpMapper.updateByPrimaryKey(searchBondLon);
			}
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖、存单发行交易同步任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
