package com.singlee.ifs.job;

import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.dict.service.impl.DictionaryGetServiceImpl;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.mapper.IfsWdOpicsSecmapMapper;
import com.singlee.ifs.mapper.IfsWdSecMapper;
import com.singlee.ifs.model.IfsOpicsBond;
import com.singlee.ifs.model.IfsWdOpicsSecmap;
import com.singlee.ifs.model.IfsWdSec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WindSecJobManager implements CronRunnable{
	
	private IfsWdSecMapper wdSecMapper= SpringContextHolder.getBean(IfsWdSecMapper.class);
	/*private IfsWdSectypeMapper wdSectypeMapper= SpringContextHolder.getBean(IfsWdSectypeMapper.class);
	private IfsWdSecissuerMapper wdSecissuerMapper= SpringContextHolder.getBean(IfsWdSecissuerMapper.class);
	private IfsWdSeccustMapper WdSeccustMapper= SpringContextHolder.getBean(IfsWdSeccustMapper.class);*/
	//发行人映射
	private IfsWdOpicsSecmapMapper wdOpicsSecmapMapper = SpringContextHolder.getBean(IfsWdOpicsSecmapMapper.class);
	private IfsOpicsBondMapper opicsBondMapper = SpringContextHolder.getBean(IfsOpicsBondMapper.class);
	
	private DictionaryGetServiceImpl dictionary = SpringContextHolder.getBean(DictionaryGetServiceImpl.class);
	
	
	private static Logger logManager = LoggerFactory.getLogger(WindSecJobManager.class);

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		
		logManager.info("==================开始执行WindSecJobManager==============================");
		
		/*String curDate=DayendDateServiceProxy.getInstance().getSettlementDate();
		Date sysDate=DateUtil.parse(curDate);
		String vDate=DateUtil.format(sysDate, "yyyyMMdd");
		List<IfsWdSectype> sectypeList=wdSectypeMapper.queryByVDate(vDate);
		if(sectypeList!=null){
			wdSectypeMapper.delete();
			for (IfsWdSectype ifsWdSectype : sectypeList) {
				wdSectypeMapper.insert(ifsWdSectype);
			}
		}
		List<IfsWdSecissuer> secissuerList=wdSecissuerMapper.queryByVDate(vDate);
		if(secissuerList!=null){
			wdSecissuerMapper.delete();
			for (IfsWdSecissuer ifsWdSecissuer : secissuerList) {
				wdSecissuerMapper.insert(ifsWdSecissuer);
			}
		}
		
		List<IfsWdSec> secList=wdSecMapper.queryByVDate(vDate);
		if(secList!=null){
			wdSecMapper.delete();
			for (IfsWdSec ifsWdSec : secList) {
				wdSecMapper.insert(ifsWdSec);
			}
		}
		
		List<IfsWdSeccust> seccustList =WdSeccustMapper.query();
		if(seccustList!=null){
			WdSeccustMapper.delete();
			for (IfsWdSeccust ifsWdSeccust : seccustList) {
				WdSeccustMapper.insert(ifsWdSeccust);
			}
		}*/
		
		
		//已经映射的交易对手
		List<IfsWdOpicsSecmap> custmapList=wdOpicsSecmapMapper.queryCustList();
		for (IfsWdOpicsSecmap ifsWdOpicsSecmap : custmapList) {
			List<IfsWdSec> wdsecList=wdSecMapper.querySecidsByIssuer(ifsWdOpicsSecmap.getWdid());
			for (IfsWdSec ifsWdSec : wdsecList) {
				//判断债券是否已添加
				IfsOpicsBond bond=opicsBondMapper.searchById(ifsWdSec.getSecid());
				if(bond==null){
					IfsOpicsBond entity = new IfsOpicsBond();
					entity.setBndcd(ifsWdSec.getSecid());
					entity.setBndnm_en(ifsWdSec.getShortname());  //债券简称
					entity.setBndnm_cn(ifsWdSec.getLongname());   //债券全称
					entity.setCcy(ifsWdSec.getCcy());             //债券币种
					entity.setSettccy(ifsWdSec.getCcy());         //清算币种
					entity.setCouponrate(ifsWdSec.getRate());     //票面利率
					entity.setIssuer(ifsWdOpicsSecmap.getOpicsid()); //发行机构
					entity.setIssuername(ifsWdSec.getIssuer()); //机构名称
					entity.setIssprice(ifsWdSec.getIssueprice()==null?null:BigDecimal.valueOf(new Double(ifsWdSec.getIssueprice()))); //发行价
					entity.setIssuevol(ifsWdSec.getIssuesize()==null?null:BigDecimal.valueOf(new Double(ifsWdSec.getIssuesize())));  //实际发行量
					String pubdt=DateUtil.format(DateUtil.parse(ifsWdSec.getIssuedate(),"yyyyMMdd"));
					entity.setPubdt(pubdt);     //发行日期
					String issudt=DateUtil.format(DateUtil.parse(ifsWdSec.getEffdate(),"yyyyMMdd"));
					entity.setIssudt(issudt);      //起息日
					String matdt=DateUtil.format(DateUtil.parse(ifsWdSec.getMatdate(),"yyyyMMdd"));
					entity.setMatdt(matdt);       //到期日
					entity.setBondperiod(ifsWdSec.getBondmat());  //债券期限 
					entity.setBasicspread(ifsWdSec.getSpread());  //基本利差
					IfsWdOpicsSecmap freq=wdOpicsSecmapMapper.queryOpicsId(ifsWdSec.getFreq());
					if(freq!=null){
						entity.setIntpaycircle(freq.getOpicsid()); //付息周期
					}
					//债券性质
					IfsWdOpicsSecmap property=wdOpicsSecmapMapper.queryOpicsId(ifsWdSec.getBondproperty());
					if(property!=null){
						entity.setBondproperties(property.getOpicsid());
					}
					List<TaDictVo> cmpList=dictionary.getTaDictByCode("outCompany");
					for (TaDictVo taDictVo : cmpList) {
						if(StringUtil.isNotEmpty(ifsWdSec.getCreditratingagency1())){
							if(taDictVo.getDict_value().substring(0,4).equalsIgnoreCase(ifsWdSec.getCreditratingagency1().substring(0,4))){
								entity.setBondratingagency(taDictVo.getDict_key());         //评级机构
								if("2".equalsIgnoreCase(taDictVo.getDict_key())){
									List<TaDictVo> ratingList=dictionary.getTaDictByCode("RatingMd");
									for (TaDictVo taDictVo2 : ratingList) {
										if(StringUtil.isNotEmpty(ifsWdSec.getCreditrating1())){
											if(taDictVo2.getDict_value().equalsIgnoreCase(ifsWdSec.getCreditrating1())){
												entity.setBondrating(taDictVo2.getDict_key());
											}
										}
									}
								}else{
									List<TaDictVo> ratingList=dictionary.getTaDictByCode("Rating");
									for (TaDictVo taDictVo2 : ratingList) {
										if(StringUtil.isNotEmpty(ifsWdSec.getCreditrating1())){
											if(taDictVo2.getDict_value().equalsIgnoreCase(ifsWdSec.getCreditrating1())){
												entity.setBondrating(taDictVo2.getDict_key());
											}
										}
									}
								}
							}
						}
						
					}
					
					//entity.setBondrating(ifsWdSec.getCreditrating1());         //评级
	                //entity.setBondratingagency(ifsWdSec.getCreditratingagency1());   //评级机构
					entity.setBondratingclass("1");//评级类别
					// 设置 录入时间
					entity.setInputdate(DateUtil.getCurrentDateTimeAsString());
					//设置流水号
					HashMap<String, Object> hashMap = new HashMap<String, Object>();
					String dealno = opicsBondMapper.getTradeId(hashMap);
					entity.setDealno(dealno);
					entity.setStatus("A");
					entity.setBr("1");
					String marketDate=DateUtil.format(DateUtil.parse(ifsWdSec.getListingdate(),"yyyyMMdd"));
					entity.setMarketDate(marketDate); //上市日期
					
					opicsBondMapper.insert(entity);
				}
			}
		}
		logManager.info("==================执行完成WindSecJobManager==============================");
		
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
