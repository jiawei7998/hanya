package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsrmbCrMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailCrMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbOrMapper;
import com.singlee.ifs.model.IfsCfetsrmbCr;
import com.singlee.ifs.model.IfsCfetsrmbDetailCr;
import com.singlee.ifs.model.IfsCfetsrmbOr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepoMessageMonitor implements CronRunnable {
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor= SpringContextHolder.getBean("IBaseServer");

	private IfsCfetsrmbCrMapper cfetsrmbCrMapper= SpringContextHolder.getBean(IfsCfetsrmbCrMapper.class);

	private IfsCfetsrmbOrMapper cfetsrmbOrMapper= SpringContextHolder.getBean(IfsCfetsrmbOrMapper.class);
	
	private IfsCfetsrmbDetailCrMapper cfetsrmbDetailCrMapper= SpringContextHolder.getBean(IfsCfetsrmbDetailCrMapper.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- OPICS】，回购模块交易状态同步处理，开始······");
		/**
		 * 回购模块分为质押式回购和买断式回购
		 */
		// 查询全部未复核 回购本身
		List<IfsCfetsrmbCr> listCr = cfetsrmbCrMapper.getCfetsrmbCrList();
		for (IfsCfetsrmbCr ifsCfetsrmbCr : listCr) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.REPO.SERVER);
			inthBean.setTag(SlDealModule.REPO.TAG);
			inthBean.setBr(ifsCfetsrmbCr.getBr());
			inthBean.setSeq("0");
			inthBean.setFedealno(ifsCfetsrmbCr.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				cfetsrmbCrMapper.updateCfetsrmbCrByFedealno(map);
			}
		}
		//质押的券
		List<IfsCfetsrmbDetailCr> listDetailCr=cfetsrmbDetailCrMapper.getCfetsrmbDetailCrList();
		for (int i = 0; i < listDetailCr.size(); i++) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.REPO.SERVER);
			inthBean.setTag(SlDealModule.REPO.TAG);
			inthBean.setSeq(String.valueOf(i+1));
			inthBean.setFedealno(listDetailCr.get(i).getTicketId());
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				map.put("seq", String.valueOf(i+1));
				cfetsrmbDetailCrMapper.updateDetailCrByFedealno(map);
			}
		}
		
		
		//买断式回购本身
		List<IfsCfetsrmbOr> listOr = cfetsrmbOrMapper.getCfetsrmbOrList();
		for (IfsCfetsrmbOr ifsCfetsrmbOr : listOr) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.REPO.SERVER);
			inthBean.setTag(SlDealModule.REPO.TAG);
			inthBean.setBr(ifsCfetsrmbOr.getBr());
			inthBean.setSeq("0");
			inthBean.setFedealno(ifsCfetsrmbOr.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				cfetsrmbOrMapper.updateCfetsrmbOrByFedealno(map);
			}
		}
		//买断式回购 券
		List<IfsCfetsrmbOr> listOrBond = cfetsrmbOrMapper.getCfetsrmbOrBondList();
		for (IfsCfetsrmbOr ifsCfetsrmbOr : listOrBond) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.REPO.SERVER);
			inthBean.setTag(SlDealModule.REPO.TAG);
			inthBean.setSeq("1");
			inthBean.setFedealno(ifsCfetsrmbOr.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("bondStatcode", slInthBean.getStatcode());
				map.put("bondErrorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				cfetsrmbOrMapper.updateOrBondByFedealno(map);
			}
		}
		
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- OPICS】，回购模块交易状态同步处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
