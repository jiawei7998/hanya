package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCorePositionMapper;
import com.singlee.ifs.model.IfsCorePositionBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 国结  分发平台数据文件
 *
 */
public class FxGjImportJobManage implements CronRunnable {
	
	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private IfsCorePositionMapper corePositionMapper = SpringContextHolder.getBean(IfsCorePositionMapper.class);
	
	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 国结】，分发平台数据文件，开始······");
		Date postdate = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		String date = DateUtil.format(postdate, "yyyyMMdd");
		
		String src_folder = SystemProperties.PositionPath;
		String fileName = SystemProperties.PositiongjfileName + date + ".dat";// 文档名称
		
//		String archive_folder = SystemProperties.PositionArchive;
//		String indName = SystemProperties.PositiongjfileName + date + ".ind";// 归档文件名称
		
		File file = new File(src_folder,fileName);
		if(!file.exists()) {
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 国结】，分发平台数据文件，找不到" + fileName + "文件");
			return true;
		}
		
		BufferedReader readfile = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GB2312"));// 构造一个BufferedReader类来读取文件
		String line = null;
		IfsCorePositionBean ifsCorePositionBean = null;
		while ((line = readfile.readLine()) != null) {
			ifsCorePositionBean = new IfsCorePositionBean();
			String[] field = line.split("/");
			if (field.length > 1) {
				for (int i = 0; i < field.length; i++) {
					GenerateCorePosition(i, field[i], ifsCorePositionBean);
				}
				// 是否冲销的标识
				if (!StringUtil.isEmpty(ifsCorePositionBean.getRevFlag())
						&& "0".equals(ifsCorePositionBean.getRevFlag())) {
					ifsCorePositionBean.setRevFlag("FCX");// 未冲销
				} else {
					ifsCorePositionBean.setRevFlag("CX");// 冲销
				}
				// 国结即期远期标识
				if (!StringUtil.isEmpty(ifsCorePositionBean.getFlag()) && "0".equals(ifsCorePositionBean.getFlag())) {
					ifsCorePositionBean.setCost(SystemProperties.Positiongjjqcost);// 国结即期
					ifsCorePositionBean.setPort(SystemProperties.Positiongjjqport);
				} else {
					ifsCorePositionBean.setCost(SystemProperties.Positiongjyqcost);// 国结远期
					ifsCorePositionBean.setPort(SystemProperties.Positiongjyqport);
				}
				ifsCorePositionBean.setSource("国结");
				ifsCorePositionBean.setServer("POSITION");
				ifsCorePositionBean.setTrad(SystemProperties.Positiontrad);
				ifsCorePositionBean.setCust(SystemProperties.Positioncust);
				ifsCorePositionBean.setUpdateDate(new Timestamp(new Date().getTime()));
				ifsCorePositionBean.setStatus("SENDOK");
				
				IfsCorePositionBean icpb = new IfsCorePositionBean();
				icpb.setFedealNo(ifsCorePositionBean.getFedealNo());
				icpb.setRevFlag(ifsCorePositionBean.getRevFlag());
				List<IfsCorePositionBean> coreList = corePositionMapper.select(icpb);
				if (coreList != null && coreList.size() > 0) {
					JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 国结】，分发平台数据文件，交易【" + ifsCorePositionBean.getFedealNo() + "】重复 不再导入");
				} else {
					ifsCorePositionBean.setOpicsDate(postdate);
					corePositionMapper.insertSelective(ifsCorePositionBean);
					JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 国结】，分发平台数据文件，国结数据【" + ifsCorePositionBean.getFedealNo() + "】保存到接口成功");
				}
			}
		}
		
		readfile.close();
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 国结】，分发平台数据文件，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}

	/**
	 * 解析文件 组装数据
	 * 
	 * @param i
	 * @param str
	 * @param ifsCorePositionBean
	 */
	public void GenerateCorePosition(int i, String str, IfsCorePositionBean ifsCorePositionBean) {
		switch (i) {
		case 0:ifsCorePositionBean.setFedealNo(str.trim());				break;
		case 1:ifsCorePositionBean.setCcy(str.trim());					break;
		case 2:ifsCorePositionBean.setCtrCcy(str.trim());				break;
		case 3:ifsCorePositionBean.setPsFlag("P");						break;
		case 4:ifsCorePositionBean.setDealDate(str.trim());				break;
		case 5:ifsCorePositionBean.setVdate(str.trim());				break;
		case 6:
			BigDecimal b6 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b6.setScale(4, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCcyAmt(b6.toPlainString());
			break;
		case 7:
			BigDecimal b7 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b7.setScale(4, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCtrAmt(b7.toPlainString());
			break;
		case 8:
			BigDecimal b8 = new BigDecimal(StringUtil.isEmptyString(str) ? "" : str.trim());
			b8.setScale(8, BigDecimal.ROUND_HALF_UP);
			ifsCorePositionBean.setCcyRate(b8.toPlainString());
			break;
		case 9:ifsCorePositionBean.setFlag(str.trim());					break;
		case 10:ifsCorePositionBean.setRevFlag(str.trim());				break;
		case 11:ifsCorePositionBean.setDealtype(str.trim());			break;
		case 12:ifsCorePositionBean.setTranbrief(str.trim());			break;
		default:break;
		}
	}

}
