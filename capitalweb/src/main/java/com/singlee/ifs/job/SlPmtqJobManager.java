package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dict.mapper.TaDictVoMapper;
import com.singlee.capital.dict.model.TaDictVo;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.financial.bean.PcixBean;
import com.singlee.financial.bean.PsixBean;
import com.singlee.financial.bean.SlPmtqAccBean;
import com.singlee.financial.bean.SlPmtqBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IPmtqServer;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.model.IfsTrdSettle;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlPmtqJobManager implements CronRunnable {

    // opics支付信息
    private IPmtqServer pmtqServer = SpringContextHolder.getBean("IPmtqServer");
    // 前置往账经办
    private IfsTrdSettleMapper trdSettleMapper = SpringContextHolder.getBean(IfsTrdSettleMapper.class);
    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
    private TaSysParamMapper taSysParamMapper = SpringContextHolder.getBean(TaSysParamMapper.class);
    // 查询数据字典-本行清算客户对应opic部门 -opicsBankCname
    private TaDictVoMapper taDictVoMapper = SpringContextHolder.getBean(TaDictVoMapper.class);

    @Override
    @Transactional(value = "transactionManager", rollbackFor = Exception.class)
    public boolean execute(Map<String, Object> arg0) throws Exception {
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- OPICS】，大额清算信息获取任务处理，开始······");
        // 系统当前时间
        Date date = baseServer.getOpicsSysDate("01");
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- OPICS】，查询当日SL_PMTQ表里未发送的数据.");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("vdate", DateUtil.format(date));
        List<SlPmtqBean> cnapsList = pmtqServer.getPmtqByMap(map);

        for (SlPmtqBean slPmtqBean : cnapsList) {
            // 正常状态 插入
            if ("0".equals(slPmtqBean.getDealstatus())) {
                if(insert(slPmtqBean)){
                    continue;
                }
            } else {
                // 作废 撤销状态
                Map<String, Object> map2 = new HashMap<String, Object>();
                map2.put("br", StringUtils.trimToEmpty(slPmtqBean.getBr()));
                map2.put("dealno", StringUtils.trimToEmpty(slPmtqBean.getDealno()));
                map2.put("product", StringUtils.trimToEmpty(slPmtqBean.getProduct()));
                map2.put("prodType", StringUtils.trimToEmpty(slPmtqBean.getType()));
                map2.put("payrecind", StringUtils.trimToEmpty(slPmtqBean.getPayrecind()));
                map2.put("seq", StringUtils.trimToEmpty(slPmtqBean.getSeq()));
                map2.put("cdate", DateUtil.format(slPmtqBean.getCdate()));
                map2.put("ctime", StringUtils.trimToEmpty(slPmtqBean.getCtime()));
                map2.put("voidflag", StringUtils.trimToEmpty(slPmtqBean.getDealstatus()));
                IfsTrdSettle bean = trdSettleMapper.queryTrdSettle(map2);
                if (bean != null) {
                    trdSettleMapper.updateVoidflag(map2);
                } else {
                    if(insert(slPmtqBean)){
                        continue;
                    }
                }
            }
            // 同步到前置后修改SL_PMTQ状态 改为已发送
            Map<String, Object> map3 = new HashMap<String, Object>();
            map3.put("sendflag", "1");
            map3.put("br", StringUtils.trimToEmpty(slPmtqBean.getBr()));
            map3.put("dealno", StringUtils.trimToEmpty(slPmtqBean.getDealno()));
            map3.put("product", StringUtils.trimToEmpty(slPmtqBean.getProduct()));
            map3.put("prodType", StringUtils.trimToEmpty(slPmtqBean.getType()));
            map3.put("payrecind", StringUtils.trimToEmpty(slPmtqBean.getPayrecind()));
            map3.put("seq", StringUtils.trimToEmpty(slPmtqBean.getSeq()));
            map3.put("cdate", DateUtil.format(slPmtqBean.getCdate()));
            map3.put("ctime", StringUtils.trimToEmpty(slPmtqBean.getCtime()));
            pmtqServer.updateSendflag(map3);
        }
        JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- OPICS】，大额清算信息获取任务处理，结束······");
        return true;
    }

    public boolean insert(SlPmtqBean slPmtqBean) {
        /**
         * 获取机构映射
         */
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dict_id", "opicsBranchId");
        params.put("dict_key", StringUtils.trimToEmpty(slPmtqBean.getBr()));
        List<TaDictVo> opicsBranchIds = taDictVoMapper.getTaDictByDictId(params);
        String opicsBranchId = "";
        if (opicsBranchIds != null) {
            opicsBranchId = StringUtils.trimToEmpty(opicsBranchIds.get(0).getDict_value());
        }

        /**
         * 开始组装往账信息
         */
        IfsTrdSettle ifsTrdSettle = new IfsTrdSettle();
        ifsTrdSettle.setBr(StringUtils.trimToEmpty(slPmtqBean.getBr()));
        ifsTrdSettle.setProduct(StringUtils.trimToEmpty(slPmtqBean.getProduct()));
        ifsTrdSettle.setProdtype(StringUtils.trimToEmpty(slPmtqBean.getType()));
        ifsTrdSettle.setDealno(StringUtils.trimToEmpty(slPmtqBean.getDealno()));
        ifsTrdSettle.setSeq(StringUtils.trimToEmpty(slPmtqBean.getSeq()));
        ifsTrdSettle.setServer(StringUtils.trimToEmpty(opicsBranchId));
        ifsTrdSettle.setCno(StringUtils.trimToEmpty(slPmtqBean.getCno()));
        ifsTrdSettle.setVdate(DateUtil.format(slPmtqBean.getVdate(), "yyyy-MM-dd"));
        ifsTrdSettle.setPayrecind(StringUtils.trimToEmpty(slPmtqBean.getPayrecind()));
        ifsTrdSettle.setCcy(StringUtils.trimToEmpty(slPmtqBean.getCcy()));
        ifsTrdSettle.setCdate(DateUtil.format(slPmtqBean.getCdate(), "yyyy-MM-dd"));
        ifsTrdSettle.setCtime(StringUtils.trimToEmpty(slPmtqBean.getCtime()));
        ifsTrdSettle.setSetmeans(StringUtils.trimToEmpty(slPmtqBean.getSetmeans()));
        ifsTrdSettle.setSetacct(StringUtils.trimToEmpty(slPmtqBean.getSetacct()));
        ifsTrdSettle.setStatus(StringUtils.trimToEmpty(slPmtqBean.getStatus()));
        ifsTrdSettle.setVoidflag(StringUtils.trimToEmpty(slPmtqBean.getDealstatus())); // sl_pmtq的dealstatus状态
        ifsTrdSettle.setDealflag("0"); // 待经办
        ifsTrdSettle.setSettflag("1");// 待处理
        String sn = trdSettleMapper.getSn().trim();
        ifsTrdSettle.setSn(sn);
        ifsTrdSettle.setFedealno(StringUtils.trimToEmpty(slPmtqBean.getFedealno()));

        // 本行信息
        Map<String, Object> sysMap = new HashMap<String, Object>();
        if (isFM(slPmtqBean.getProduct().trim(), slPmtqBean.getType().trim())) {
            sysMap.put("p_type", "HRBCB");
        } else {
            sysMap.put("p_type", "HRBCBFX");
        }
        List<TaSysParam> sysList = taSysParamMapper.selectTaSysParam(sysMap);
        if ("P".equals(slPmtqBean.getPayrecind())) {
            for (TaSysParam taSysParam : sysList) {
                if ("Bankid".equals(taSysParam.getP_code())) {
                    ifsTrdSettle.setPayBankid(taSysParam.getP_value().trim());// 付款行行号
                } else if ("Userid".equals(taSysParam.getP_code())) {
                    ifsTrdSettle.setPayUserid(taSysParam.getP_value().trim());// 付款账号
                } else if ("Bankname".equals(taSysParam.getP_code())) {
                    ifsTrdSettle.setPayBankname(taSysParam.getP_value().trim());// 付款行行名
                } else if ("Username".equals(taSysParam.getP_code())) {
                    ifsTrdSettle.setPayUsername(taSysParam.getP_value().trim());// 付款账户名称
                }
            }
        }

        boolean flag = false;
        if ("REPO".equalsIgnoreCase(ifsTrdSettle.getProduct()) || "SECUR".equalsIgnoreCase(ifsTrdSettle.getProduct())) {
            if ("GD".equalsIgnoreCase(ifsTrdSettle.getProdtype()) || "ZD".equalsIgnoreCase(ifsTrdSettle.getProdtype()) ||
                    "ZQ".equalsIgnoreCase(ifsTrdSettle.getProdtype()) || "CB".equalsIgnoreCase(ifsTrdSettle.getProdtype())) {
                JY.info("开始处理[国库定期存款,线下押券,中期借贷便利,常备借贷便利]大额支付,交易号为:" + ifsTrdSettle.getDealno());

                flag = findRepoPackage(ifsTrdSettle, slPmtqBean);

                JY.info("结束处理[国库定期存款,线下押券,中期借贷便利,常备借贷便利]大额支付,交易号为:" + ifsTrdSettle.getDealno());
            } else {
                JY.info("开始处理[回购交易或债券交易]大额支付,交易号为" + ifsTrdSettle.getDealno());

                flag = findSecurPackage(ifsTrdSettle, slPmtqBean);

                JY.info("结束处理[回购交易或债券交易]大额支付,交易号为" + ifsTrdSettle.getDealno());
            }
        } else {
            JY.info("开始处理[外汇,拆借,SWAP,CALL交易]大额支付,交易号为" + ifsTrdSettle.getDealno());

            flag = firstFxdMMPackage(ifsTrdSettle, slPmtqBean);

            JY.info("结束处理[外汇,拆借,SWAP,CALL交易]大额支付,交易号为" + ifsTrdSettle.getDealno());
        }
        return flag;
    }

    /**
     * 是否为金融市场部
     *
     * @param prod
     * @param type
     * @return
     */
    public static boolean isFM(String prod, String type) {
        if ("FXD".equalsIgnoreCase(prod)) {
            return false;
        } else if ("MM".equalsIgnoreCase(prod)) {
            if ("BR".equalsIgnoreCase(type)) {
                return false;
            } else if ("LD".equalsIgnoreCase(type)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * 组装[外汇,拆借,SWAP,CALL交易]大额支付
     *
     * @param ifsTrdSettle
     * @param slPmtqBean
     * @return
     */
    public boolean firstFxdMMPackage(IfsTrdSettle ifsTrdSettle, SlPmtqBean slPmtqBean) {
        // 账号 行号信息
        PcixBean pcix = new PcixBean();
        pcix.setBr(slPmtqBean.getBr().trim());
        pcix.setProduct(slPmtqBean.getProduct().trim());
        pcix.setType(slPmtqBean.getType().trim());
        pcix.setDealno(slPmtqBean.getDealno().trim());
        pcix.setSeq(slPmtqBean.getSeq().trim());
        List<PcixBean> pcixList = pmtqServer.selectPcix(pcix);

        // 账户名 行名信息
        PsixBean psix = new PsixBean();
        psix.setBr(slPmtqBean.getBr().trim());
        psix.setProduct(slPmtqBean.getProduct().trim());
        psix.setType(slPmtqBean.getType().trim());
        psix.setDealno(slPmtqBean.getDealno().trim());
        psix.setSeq(slPmtqBean.getSeq().trim());
        List<PsixBean> psixList = pmtqServer.selectPsix(psix);

        //收款信息
        if (pcixList.size() > 0) {
            for (PcixBean p : pcixList) {
                if ("AW".equals(p.getPcc())) {
                    ifsTrdSettle.setRecBankid(p.getC1() == null ? null : p.getC1().trim());// 收款行行号
                } else if ("BE".equals(p.getPcc())) {
                    ifsTrdSettle.setRecUserid(p.getC1() == null ? null : p.getC1().trim());// 收款账号
                }
            }
        }
        if (psixList.size() > 0) {
            ifsTrdSettle.setRecBankname(psixList.get(0).getP1() == null ? null : psixList.get(0).getP1().trim());// 收款行行名
            ifsTrdSettle.setRecUsername(psixList.get(0).getP3() == null ? null : psixList.get(0).getP3().trim());// 收款账户名称
            ifsTrdSettle.setRemarkFy(psixList.get(0).getR1() == null ? null : psixList.get(0).getR1().trim());// 附言
        }

        ifsTrdSettle.setAmount(BigDecimal.valueOf(Double.valueOf(slPmtqBean.getAmount())));
        trdSettleMapper.insertSettle(ifsTrdSettle);
        return false;
    }

    /**
     * 组装[国库定期存款,线下押券,中期借贷便利,常备借贷便利]大额支付
     *
     * @param ifsTrdSettle
     * @param slPmtqBean
     * @return
     */
    public boolean findRepoPackage(IfsTrdSettle ifsTrdSettle, SlPmtqBean slPmtqBean) {
        Map<String, String> parm = new HashMap<String, String>();
        parm.put("dealno", ifsTrdSettle.getDealno());
        parm.put("br", ifsTrdSettle.getBr());
        SlPmtqAccBean slPmtqAccBean = pmtqServer.selectRepoAmt(parm);
        if(null == slPmtqAccBean){
            JY.info("清算信息未查到,交易号为" + ifsTrdSettle.getDealno());
            return true;
        }

        //本金
        String amount = slPmtqAccBean.getAmount();
        if (amount != null && amount.contains("-")) {
            amount = amount.substring(1);
        }
        ifsTrdSettle.setRecBankid(slPmtqAccBean.getBrecvbankno());
        ifsTrdSettle.setRecBankname(slPmtqAccBean.getBrecvbankname());
        ifsTrdSettle.setRecUserid(slPmtqAccBean.getBrecvaccno());
        ifsTrdSettle.setRecUsername(slPmtqAccBean.getBrecvaccname());
        ifsTrdSettle.setRemarkFy(slPmtqAccBean.getBmbfememo());
        ifsTrdSettle.setAmount(new BigDecimal(amount));
        trdSettleMapper.insertSettle(ifsTrdSettle);

        //利息
        String rate = slPmtqAccBean.getRate();
        if (rate != null && rate.contains("-")) {
            rate = rate.substring(1);
        }
        ifsTrdSettle.setRecBankid(slPmtqAccBean.getRrecvbankno());
        ifsTrdSettle.setRecBankname(slPmtqAccBean.getRrecvbankname());
        ifsTrdSettle.setRecUserid(slPmtqAccBean.getRrecvaccno());
        ifsTrdSettle.setRecUsername(slPmtqAccBean.getRrecvaccname());
        ifsTrdSettle.setRemarkFy(slPmtqAccBean.getRmbfememo());
        ifsTrdSettle.setAmount(new BigDecimal(rate));

        ifsTrdSettle.setDealno(ifsTrdSettle.getDealno() + "R");
        String sn = trdSettleMapper.getSn().trim();
        ifsTrdSettle.setSn(sn);
        trdSettleMapper.insertSettle(ifsTrdSettle);
        return false;
    }

    /**
     * 组装回购和债券除[国库定期存款,线下押券,中期借贷便利,常备借贷便利]交易的大额支付
     *
     * @param ifsTrdSettle
     * @param slPmtqBean
     * @return
     */
    public boolean findSecurPackage(IfsTrdSettle ifsTrdSettle, SlPmtqBean slPmtqBean) {
        Map<String, String> parm = new HashMap<String, String>();
        parm.put("dealno", ifsTrdSettle.getDealno());
        parm.put("br", ifsTrdSettle.getBr());
        SlPmtqAccBean slPmtqAccBean = pmtqServer.selectSecurAmt(parm);
        if(null == slPmtqAccBean){
            JY.info("清算信息未查到,交易号为" + ifsTrdSettle.getDealno());
            return true;
        }
        ifsTrdSettle.setRecBankid(slPmtqAccBean.getRecvbankno());
        ifsTrdSettle.setRecBankname(slPmtqAccBean.getRecvbankname());
        ifsTrdSettle.setRecUserid(slPmtqAccBean.getRecvaccno());
        ifsTrdSettle.setRecUsername(slPmtqAccBean.getRecvaccname());
        ifsTrdSettle.setRemarkFy(slPmtqAccBean.getMbfememo());
        ifsTrdSettle.setAmount(BigDecimal.valueOf(Double.valueOf(slPmtqBean.getAmount())));
        trdSettleMapper.insertSettle(ifsTrdSettle);
        return false;
    }

    @Override
    public void terminate() {
        // TODO Auto-generated method stub
    }

}
