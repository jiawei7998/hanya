package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlOpcKpsjBean;
import com.singlee.financial.common.util.SingleeSFTPClient;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IOpcKpsjServer;
import com.singlee.ifs.model.IfsOpsKpsjBean;
import com.singlee.ifs.service.IfsOpsKpsjService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 增值税送交易 开票数据接口，此报表为日报，每天晚上23点生成,涉及到的交易有拆借，回购，债券三类交易。
 * 
 *
 */
public class OpcKpsjJobManage implements CronRunnable {

	private final IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private final IOpcKpsjServer opcKpsjServer = SpringContextHolder.getBean("IOpcKpsjServer");
	private final IfsOpsKpsjService ifsOpsKpsjService = SpringContextHolder.getBean(IfsOpsKpsjService.class);


	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，开票数据任务处理，开始······");
		Date postdate = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		String datetime = DateUtil.format(postdate, "yyyy-MM-dd");

		String src_folder = SystemProperties.OpcPath;// 文件路径

		String fileName = SystemProperties.OpcKpsjfileName + datetime.replace("-", "") + ".dat";// 文档名称
		String archiveFileName = SystemProperties.OpcKpsjfileName + datetime.replace("-", "") + ".ind";// 备份文档名称

		// 接收结果
//		List<SlOpcKpsjBean> resultList = null;
		List<IfsOpsKpsjBean> resultList = null;
		// 统计结果
		List<List<String>> writeList = new ArrayList<List<String>>();

		Map<String,Object> map=new HashMap<>();
		map.put("datetime",datetime);
		// 拆借交易
//		resultList = opcKpsjServer.selectOpcKpsjDldt(datetime);
//		writeList.addAll(GenerateOpcKpsj(resultList, 1, datetime));
		map.put("type","1");
		resultList=ifsOpsKpsjService.getData(map);
		writeList.addAll(GenerateOpcKpsj(resultList, 1, datetime));

		// 回购交易
//		resultList = opcKpsjServer.selectOpcKpsjRepo(datetime);
//		writeList.addAll(GenerateOpcKpsj(resultList, 2, datetime));
		map.put("type","2");
		resultList=ifsOpsKpsjService.getData(map);
		writeList.addAll(GenerateOpcKpsj(resultList, 2, datetime));

		// 债券交易
//		resultList = opcKpsjServer.selectOpcKpsjSPsh(datetime);
//		writeList.addAll(GenerateOpcKpsj(resultList, 3, datetime));
		map.put("type","3");
		resultList=ifsOpsKpsjService.getData(map);
		writeList.addAll(GenerateOpcKpsj(resultList, 3, datetime));

		// 债券交易处置收益
//		resultList = opcKpsjServer.selectOpcKpsjSPshGain(datetime);
//		writeList.addAll(GenerateOpcKpsj(resultList, 4, datetime));
		map.put("type","4");
		resultList=ifsOpsKpsjService.getData(map);
		writeList.addAll(GenerateOpcKpsj(resultList, 4, datetime));

		// 生成文件
		new SingleeSFTPClient().GenerateFile(src_folder, fileName, archiveFileName, writeList);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->前置】，开票数据任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}

	/**
	 * 整理数据
	 * @param resultList
	 * @param type
	 * @param datetime
	 * @return
	 *//*
	public List<List<String>> GenerateOpcKpsj(List<SlOpcKpsjBean> resultList, int type, String datetime) {
		List<List<String>> writeList = new ArrayList<List<String>>();
		List<String> write = null;
		for (SlOpcKpsjBean slOpcKpsjBean : resultList) {
			write = new ArrayList<String>();
			write.add(slOpcKpsjBean.getCmne());// 1OPICS客户编号
			write.add(slOpcKpsjBean.getCmne());// 2OPICS客户编号
			write.add("");// 3
			write.add("");// 4
			write.add("00100");// 5交易机构
			write.add(slOpcKpsjBean.getDealno());// 6交易编号
			write.add(slOpcKpsjBean.getProduct() + slOpcKpsjBean.getType());// 7交易编号
			JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，开票数据任务处理，TYPE:"+type);
			if (type == 1 || type == 2 || type == 3) {
				write.add(slOpcKpsjBean.getDrcrind().substring(0, 1));// 8借贷标志
			} else if (type == 4) {
				write.add("D");// 8,借贷标志 (10.20需求变动 借贷标志是C/D)
			}
			if (type == 1 || type == 2 || type == 3) {
				if (!"".equals(slOpcKpsjBean.getAmount())) {// 9交易金额
					write.add(Math.abs(Double.parseDouble(slOpcKpsjBean.getAmount())) + "");
				} else {
					write.add("0.0");
				}
			} else if (type == 4) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("dealno", slOpcKpsjBean.getDealno());
				map.put("datetime", datetime);
				SlOpcKpsjBean okb = opcKpsjServer.selectOpcKpsjCountGain(map);
			if(null!=okb){
				if (!"".equals(okb.getCzsy())) {// 9交易金额
					write.add(Math.abs(Double.parseDouble(okb.getCzsy())) + "");
				} else {
					write.add("0.0");
				}
			}
			}
			write.add("CNY");// 10币种
			write.add("1");// 11交易顺序号
			write.add(""); // 12内部账号
			if (type == 1) {
				if ("HEBYHZL".equals(slOpcKpsjBean.getCmne())) {
					write.add("5020903");// 13核心会计科目号(与集团内往来利息收入)
				} else {
					write.add("50203");// 13核心会计科目号(拆放同业利息收入)
				}
			} else if (type == 2) {
				write.add("5020701");// 13核心会计科目号(买入返售金融资产利息收入)
			} else if (type == 3) {
				write.add(slOpcKpsjBean.getCoreglno());// 13核心会计科目号(买入返售金融资产利息收入)
			} else if (type == 4) {
				write.add(slOpcKpsjBean.getGlno());
			}
			if (type == 1 || type == 2 || type == 3) {
				write.add(slOpcKpsjBean.getPostdate());// 14
				write.add(slOpcKpsjBean.getCode());// 15
			} else if (type == 4) {
				write.add(datetime);// 14,
				write.add("");// 15
			}
			write.add("");// 16
			write.add("");// 17
			write.add("");// 18
			if (type == 1 || type == 2 || type == 4) {
				write.add("");// 19
				write.add("");// 20
				write.add("0");// 21
			} else if (type == 3) {
				String code = slOpcKpsjBean.getCode();
				if ("3000".equals(code) || "2970".equals(code) || "2980".equals(code)) {
					write.add("");// 19
					write.add("");// 20
					write.add("0");// 21
				} else {
					write.add(slOpcKpsjBean.getDealno());// 19,原交易编号
					List<SlOpcKpsjBean> revList = opcKpsjServer.selectOpcKpsjByDelno(slOpcKpsjBean.getDealno());
					if (revList != null && revList.size() > 0) {
						write.add(revList.get(0).getPostdate());
					} else {
						write.add("");// 20,原账务日期
					}
					write.add("1");// 21,冲正标识
				}
			}
			write.add("");// 22
			write.add("");// 23
			write.add("");// 24
			write.add("");// 25
			write.add("");// 26
			writeList.add(write);
		}
		return writeList;
	}*/

	/**
	 * 整理数据
	 * @param resultList
	 * @param type
	 * @param datetime
	 * @return
	 */
	public List<List<String>> GenerateOpcKpsj(List<IfsOpsKpsjBean> resultList, int type, String datetime) {
		List<List<String>> writeList = new ArrayList<List<String>>();
		List<String> write = null;
		for (IfsOpsKpsjBean slOpcKpsjBean : resultList) {
			write = new ArrayList<String>();
			write.add(slOpcKpsjBean.getCmne());// 1OPICS客户编号
			write.add(slOpcKpsjBean.getCmne());// 2OPICS客户编号
			write.add("");// 3
			write.add("");// 4
			write.add("00100");// 5交易机构
			write.add(slOpcKpsjBean.getDealno());// 6交易编号
			write.add(slOpcKpsjBean.getProduct() + slOpcKpsjBean.getType());// 7交易编号
			JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，开票数据任务处理，TYPE:"+type);
			if (type == 1 || type == 2 || type == 3) {
				write.add(slOpcKpsjBean.getDrcrind().substring(0, 1));// 8借贷标志
			} else if (type == 4) {
				write.add("D");// 8,借贷标志 (10.20需求变动 借贷标志是C/D)
			}
			if (type == 1 || type == 2 || type == 3) {
				if (!"".equals(slOpcKpsjBean.getAmount())) {// 9交易金额
					write.add(Math.abs(Double.parseDouble(slOpcKpsjBean.getAmount())) + "");
				} else {
					write.add("0.0");
				}
			} else if (type == 4) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("dealno", slOpcKpsjBean.getDealno());
				map.put("datetime", datetime);
				SlOpcKpsjBean okb = opcKpsjServer.selectOpcKpsjCountGain(map);
				if(null!=okb){
					if (!"".equals(okb.getCzsy())) {// 9交易金额
						write.add(Math.abs(Double.parseDouble(okb.getCzsy())) + "");
					} else {
						write.add("0.0");
					}
				}
			}
			write.add("CNY");// 10币种
			write.add("1");// 11交易顺序号
			write.add(""); // 12内部账号
			if (type == 1) {
				if ("HEBYHZL".equals(slOpcKpsjBean.getCmne())) {
					write.add("5020903");// 13核心会计科目号(与集团内往来利息收入)
				} else {
					write.add("50203");// 13核心会计科目号(拆放同业利息收入)
				}
			} else if (type == 2) {
				write.add("5020701");// 13核心会计科目号(买入返售金融资产利息收入)
			} else if (type == 3) {
				write.add(slOpcKpsjBean.getCoreglno());// 13核心会计科目号(买入返售金融资产利息收入)
			} else if (type == 4) {
				write.add(slOpcKpsjBean.getGlno());
			}
			if (type == 1 || type == 2 || type == 3) {
				write.add(slOpcKpsjBean.getPostdate());// 14
				write.add(slOpcKpsjBean.getCode());// 15
			} else if (type == 4) {
				write.add(datetime);// 14,
				write.add("");// 15
			}
			write.add("");// 16
			write.add("");// 17
			write.add("");// 18
			if (type == 1 || type == 2 || type == 4) {
				write.add("");// 19
				write.add("");// 20
				write.add("0");// 21
			} else if (type == 3) {
				String code = slOpcKpsjBean.getCode();
				if ("3000".equals(code) || "2970".equals(code) || "2980".equals(code)) {
					write.add("");// 19
					write.add("");// 20
					write.add("0");// 21
				} else {
					write.add(slOpcKpsjBean.getDealno());// 19,原交易编号
					List<SlOpcKpsjBean> revList = opcKpsjServer.selectOpcKpsjByDelno(slOpcKpsjBean.getDealno());
					if (revList != null && revList.size() > 0) {
						write.add(revList.get(0).getPostdate());
					} else {
						write.add("");// 20,原账务日期
					}
					write.add("1");// 21,冲正标识
				}
			}
			write.add("");// 22
			write.add("");// 23
			write.add("");// 24
			write.add("");// 25
			write.add("");// 26
			writeList.add(write);
		}
		return writeList;
	}
}
