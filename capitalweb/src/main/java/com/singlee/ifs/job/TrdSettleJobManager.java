package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSettleBean;
import com.singlee.financial.esb.tlcb.IJyztcxServer;
import com.singlee.ifs.mapper.IfsTrdSettleMapper;
import com.singlee.ifs.model.IfsTrdSettle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrdSettleJobManager implements CronRunnable{
	
	private IfsTrdSettleMapper ifsTrdSettleMapper= SpringContextHolder.getBean(IfsTrdSettleMapper.class);
	
	private IJyztcxServer jyztcxServer = SpringContextHolder.getBean("IJyztcxServer");
	
	private static Logger logManager = LoggerFactory.getLogger(TrdSettleJobManager.class);

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		
		logManager.info("==================开始执行TrdSettleJobManager==============================");
		List<IfsTrdSettle> list=ifsTrdSettleMapper.getIfsTrdSettleList();
		for (IfsTrdSettle ifsTrdSettle : list) {
			String fedealno  = ifsTrdSettle.getInterfaceNo(); // 流水号
			String date = ifsTrdSettle.getVdate(); // 日期
			String vdate = date.substring(0, 10).replaceAll("-", ""); // 去掉格式
			SlSettleBean settleBean = new SlSettleBean();
			settleBean.setBusDealNo(fedealno);
			settleBean.setVdate(vdate);
			SlOutBean outBean = jyztcxServer.cnaps02(settleBean);
			/**
			 * 交易成功后，更新清算状态
			 */
			if("000000".equals(outBean.getRetCode())){ 
				String dealStatus = outBean.getClientNo(); // 交易状态
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("fedealno", fedealno);
				map.put("dealStatus", dealStatus); // 将清算状态更新到IFS_CUST_SETTLE表中
				ifsTrdSettleMapper.updateDealStatus(map);
			}
		}
		
		logManager.info("==================执行完成TrdSettleJobManager==============================");
		
		return true;
	}

	@Override
	public void terminate() {
		
	}
}