package com.singlee.ifs.job;

import java.util.Map;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.ifs.controller.IfsIntfcIselController;
import com.singlee.ifs.controller.IfsIntfcIycrController;

public class SmclSendJobManage implements CronRunnable{
	private IfsIntfcIselController ifsIntfcIselController= SpringContextHolder.getBean(IfsIntfcIselController.class);
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// TODO Auto-generated method stub
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->债券收盘价】，批量获取债券收盘价定时任务，开始······");
		
		
		ifsIntfcIselController.ImportIselByExcelByZz();
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->债券收盘价】，批量获取债券收盘价定时任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
