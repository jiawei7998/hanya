package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s100001001700105.RequestBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.S700105Bean;
import com.singlee.financial.esb.hbcb.service.S100001001700105Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.financial.esb.hrbcb.ISlAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.model.IfsEsbMassage;
import tk.mybatis.mapper.entity.Example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.*;

/**
 * 取回日终响应文件
 *
 */
public class AutoRetrieveAcupJobManager implements CronRunnable {

	private TaSysParamMapper taSysParamMapper = SpringContextHolder.getBean(TaSysParamMapper.class);
	private ISlAcupServer iSlAcupServer = SpringContextHolder.getBean("ISlAcupServer");
	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private S100001001700105Service s100001001700105Service = SpringContextHolder.getBean(S100001001700105Service.class);
	private IfsEsbMassageMapper ifsEsbMassageMapper = SpringContextHolder.getBean(IfsEsbMassageMapper.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// 获取当前账务日期
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- 核心】，取回日终响应文件，开始······");
		Date postdates = baseServer.getOpicsSysDate("01");
		String strSendDate = ParameterUtil.getString(parameters, "coreDate", DateUtil.format(postdates));
		Date postdate = DateUtil.parse(strSendDate);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，取回日终响应文件，总账日期："+DateUtil.format(postdate, "yyyyMMdd")+"······");
		
		
		
		IfsEsbMassage ifsEsbMassage = new IfsEsbMassage();
		ifsEsbMassage.setSenddate(postdate);
		ifsEsbMassage.setRescode("0020010000");
		ifsEsbMassage.setService("S100001001700105");
		List<IfsEsbMassage> list = ifsEsbMassageMapper.select(ifsEsbMassage);
		ifsEsbMassage = CheckIfsEsbMassage(list);
		
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，检查是否已经取回文件");
		if(ifsEsbMassage == null || !StringUtil.isEmpty(ifsEsbMassage.getRefile())) {
			return true;
		}
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，检查核心是否处理完成  并将响应文件上传至服务器");
		Map<String, Object> sysMap = new HashMap<String, Object>();
		sysMap.put("p_type", "ESB");
		List<TaSysParam> EsbList = taSysParamMapper.selectTaSysParam(sysMap);
		sysMap.put("p_type", "ESBMS700105");
		List<TaSysParam> EsbMS700105List = taSysParamMapper.selectTaSysParam(sysMap);
		EsbList.addAll(EsbMS700105List);
		S700105Bean s700105Bean = new S700105Bean();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		// 初始化数据
		GenerateS700105Bean(requestBody, requestHeader, EsbList);
		requestHeader.setReqDt(DateUtil.format(postdate, "ddMMyyyy"));// 请求方交易日期
		requestHeader.setBrchNo("00100");// 机构号
		requestHeader.setUUID(EsbSend.createUUID());
		requestHeader.setSndFileNme(ifsEsbMassage.getSefile());
		requestBody.setTxCode("700106");//查询标志
		requestBody.setFileName(ifsEsbMassage.getSefile());// 需处理的文件全名
		requestBody.setBrchNum("00100");// 机构码
		requestBody.setSubmitDate(DateUtil.format(postdate, "ddMMyyyy"));// 提交日期
		requestBody.setBatSeq("000000" + DateUtil.getCurrentDateAsString("HHmmss"));// 顺序号 当日内不能重复。右对齐,左补0
		s700105Bean.setRequestBody(requestBody);
		s700105Bean.setRequestHeader(requestHeader);
		EsbOutBean outBean = s100001001700105Service.send(s700105Bean);
		
		if (!"0020010000".equals(outBean.getRetCode())||!"7".equals(outBean.getClientNo())) {
			return true;
		}
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，检查通过 。核心已上传响应文件");

		String fileName = ifsEsbMassage.getSefile().replace(".DAT", ".RET");
		String path = MessageFormat.format("{0}{1}{2}", SystemProperties.acupPath,DateUtil.format(postdate, "yyyyMMdd"), "/out/");
		File filedir = new File(path);
		if (!filedir.exists()) {
			filedir.mkdirs();
		}
		
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，开始取回文件:fileName="+fileName);
		// 初始化FTP
		SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.acupIp, SystemProperties.acupPort,SystemProperties.acupUserName, SystemProperties.acupPassword);
		// 开始连接
		sftp.connect();
		//下载文件
		boolean flag = sftp.downloadFile(SystemProperties.acupCatalog + "file_out/" + DateUtil.format(postdate, "yyyyMMdd") + "/", fileName,path , fileName);
		if(flag) {
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，取文件成功:fileName="+fileName);
			//文件解析 明细
			List<String> liseDeali = new ArrayList<String>();
			//转化为数据对象
			List<AcupSendBean> listBean = new ArrayList<AcupSendBean>();
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，开始解析文件");
			File file = new File(path, fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GB2312"));// 构造一个BufferedReader类来读取文件
			String s = null;
			while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
				liseDeali.add(s);
			}
			br.close();
			
			if(liseDeali.size() >= 1) {
				String strH = liseDeali.get(0);
				String succNum = strH.substring(138,150);//成功笔数
				String succAmt = strH.substring(150,168);//成功金额
				String failNum = strH.substring(168,180);//失败笔数
				String failAmt = strH.substring(180,198);//失败金额
				JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，其中成功笔数[%s],成功金额[%s];失败笔数[%s],失败金额[%s]", succNum,succAmt,failNum,failAmt));
			}
			
			AcupSendBean acupSendBean = null;
			for (int i = 1; i < liseDeali.size(); i++) {
				
				int lastIndexOf = liseDeali.get(i).lastIndexOf("#");
				String dealno = liseDeali.get(i).substring(lastIndexOf+1,lastIndexOf+8).trim();
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，开始解析：dealno="+dealno);
				acupSendBean = new  AcupSendBean();
				acupSendBean.setRetcode(liseDeali.get(i).substring(1782,1786).trim());//返回信息码
				acupSendBean.setRetmsg(liseDeali.get(i).substring(1786,1861).trim());//返回信息
				acupSendBean.setDealno(dealno);
				acupSendBean.setDealFlag("2");//处理成功
				listBean.add(acupSendBean);
			}
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，解析文件成功，开始操作数据库。");
			if (listBean.size() > 0) {
				iSlAcupServer.updateBatchSlAcupHendle(listBean);
			}
			
			ifsEsbMassage.setRefile(fileName);
			Example example = new Example(IfsEsbMassage.class);
			example.createCriteria().andEqualTo("dealno", ifsEsbMassage.getDealno())
									.andEqualTo("service",ifsEsbMassage.getService());
			ifsEsbMassageMapper.updateByExample(ifsEsbMassage, example);
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，操作数据库完成。");
			
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- 核心】，取回日终响应文件，结束······");
		return true;
	}

	@Override
	public void terminate() {

	}
	
	/**
	 * 初始化请求报文
	 * 
	 * @param requestBody
	 * @param requestHeader
	 * @param sysList
	 */
	public static void GenerateS700105Bean(RequestBody requestBody, RequestHeader requestHeader,
			List<TaSysParam> sysList) {
		for (TaSysParam taSysParam : sysList) {
			if ("VerNo".equals(taSysParam.getP_code())) {
				requestHeader.setVerNo(taSysParam.getP_value().trim());
			} else if ("ReqSysCd".equals(taSysParam.getP_code())) {
				requestHeader.setReqSysCd(taSysParam.getP_value().trim());
			} else if ("TxnTyp".equals(taSysParam.getP_code())) {
				requestHeader.setTxnTyp(taSysParam.getP_value().trim());
			} else if ("TxnMod".equals(taSysParam.getP_code())) {
				requestHeader.setTxnMod(taSysParam.getP_value().trim());
			} else if ("TerminalType".equals(taSysParam.getP_code())) {
				// 终端类型 输入有效值：0，1，2 0：柜员前端和ESB
				requestHeader.setTerminalType(taSysParam.getP_value().trim());
			} else if ("Flag1".equals(taSysParam.getP_code())) {
				// 标识1 0-正常交易 1-倒退日交易 2-正常授权交易 3-倒退日授权交易 4,5,6,7ATM/POS交易使用 倒退日交易见详细的各个模块交易说明
				requestHeader.setFlag1(taSysParam.getP_value().trim());
			} else if ("Flag4".equals(taSysParam.getP_code())) {
				// 标识4 渠道标识： 0-柜面 5-ESB（表示渠道交易）
				requestHeader.setFlag4(taSysParam.getP_value().trim());
			} else if ("ChnlNo".equals(taSysParam.getP_code())) {
				// 渠道号
				requestHeader.setChnlNo(taSysParam.getP_value().trim());
			} else if ("TrmNo".equals(taSysParam.getP_code())) {
				// 终端号
				requestHeader.setTrmNo(taSysParam.getP_value().trim());
			} else if ("TlrNo".equals(taSysParam.getP_code())) {
				// 柜员号
				requestHeader.setTlrNo(taSysParam.getP_value().trim());
			} else if ("BancsSeqNo".equals(taSysParam.getP_code())) {
				// 流水号 输入填0，输出为bancs的流水号. 多金融组合交易的流水号是一样的
				requestHeader.setBancsSeqNo(taSysParam.getP_value().trim());
			} else if ("SupervisorID".equals(taSysParam.getP_code())) {
				// 复核或授权时输入主管id号，复核或授权的时候才有效，其他建议填”0000000”。主管ID只提供给柜面前端使用，其他外围系统的授权处理在各自的业务系统
				requestHeader.setSupervisorID(taSysParam.getP_value().trim());
			} else if ("NotiType".equals(taSysParam.getP_code())) {
				// 通知处理类型
				requestBody.setNotiType(taSysParam.getP_value().trim());
			} else if ("RetuFlag".equals(taSysParam.getP_code())) {
				// 文件响应方式 I
				requestBody.setRetuFlag(taSysParam.getP_value().trim());
			} else if ("SysFlag".equals(taSysParam.getP_code())) {
				// 应用系统标志
				requestBody.setSysFlag(taSysParam.getP_value().trim());
			} else if ("Flag".equals(taSysParam.getP_code())) {
				// 柜面外围标志 0：柜面 1：外围系统
				requestBody.setFlag(taSysParam.getP_value().trim());
			}
		}
	}

	/**
	 * 取出最近一次传的文件
	 * @param list
	 * @return
	 */
	public static IfsEsbMassage CheckIfsEsbMassage(List<IfsEsbMassage> list) {
		if (list == null || list.size() == 0) {
			return null;
		} else if (list.size() == 0) {
			return list.get(0);
		} else {
			String dealno = list.get(0).getDealno().substring(0, 21);
			List<Integer> sort = new ArrayList<Integer>();
			for (IfsEsbMassage ifsEsbMassage : list) {
				sort.add(Integer.parseInt(ifsEsbMassage.getDealno().substring(21)));
			}
			Integer max = Collections.max(sort);
			dealno = dealno + StringUtil.stringFormat(String.valueOf(max), "0", 4, "L");
			for (IfsEsbMassage ifsEsbMassage : list) {
				if (dealno.equals(ifsEsbMassage.getDealno())) {
					return ifsEsbMassage;
				}
			}
			return null;
		}
	}
}
