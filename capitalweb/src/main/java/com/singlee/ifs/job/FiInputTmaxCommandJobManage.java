package com.singlee.ifs.job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.LoggerFactory;

import com.hanafn.eai.client.adpt.http.EAIHttpAdapter;
import com.hanafn.eai.client.adpt.util.AdapterConstants;
import com.hanafn.eai.client.stdtmsg.StdTMsgGenerator;
import com.singlee.capital.chois.util.ChiosLogin;
import com.singlee.capital.chois.util.ChoisCommonMsg;
import com.singlee.capital.chois.util.StringUtil;
import com.singlee.capital.chois.util.SubStrChinese;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;

import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.ifs.mapper.IfsIntfcFiInputMapper;
import com.singlee.ifs.model.IfsIntfcFi;
import com.singlee.ifs.model.IfsIntfcFiReverse;
import com.singlee.ifs.model.IfsIntfcFiStatus;

public class FiInputTmaxCommandJobManage implements CronRunnable{
	
	private IfsIntfcFiInputMapper intfcFiInputMapper = SpringContextHolder.getBean(IfsIntfcFiInputMapper.class);
	public static org.slf4j.Logger logger = LoggerFactory.getLogger("com");
	public Date date =new Date();
	public SimpleDateFormat sdfdate = new SimpleDateFormat("yyyyMMdd");
	public SimpleDateFormat sdftimes = new SimpleDateFormat("hhmmss");
	public String msgTypeCode=null;//标准报文数据种类代码
	public String  msgTypeCodeLen;//标准报文数据长度
	public int  msgTypeCodeLen1;//标准报文数据长度
	public String sysName;//系统姓名
	public String handleCode;//处理代码
	public String channelType;//渠道类型
	public String channelId;//渠道ID
	public String gainInst;//获取机构
	public String cdate;//日期
	public String ctime;//时间
	public String keyTrade;//KEY1, 交易
	public String keyTradeNo;//KEY2 , 参考号
	public String serveHandleCode;//服务处理代码
	public String FXFIG_CJUM;
	public String FXFIG_AJUM;
	public String FXFIG_SJUM;
	public String FXFIG_OPNO;
	public String FXFIG_OPGB;
	public String FXFIG_IBIL;
	public String FXFIG_AMOD;
	public String FXFIG_TELL;
	public String FXFIG_SSID;
	public String FXFIG_SSIL;
	public String FXFIG_TELLNM;
	public String FXFIG_THGB;
	public String FXFIG_TYPE;
	public String FXFIG_JUM_CD;
	public String FXFIG_CCY;
	
	public String FXFIG_SSCK;
	public String FXFIG_THID;
	String eaiDv = AdapterConstants.EAI_DOM_SVR;
	
	ChiosLogin chiosLogin = new ChiosLogin();
	StringUtil stringUtil = new StringUtil();
	SubStrChinese subStrChinese=new SubStrChinese();
	ChoisCommonMsg choisCommonMsg = new  ChoisCommonMsg();

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logger.info("[JOBS-START] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台债券买入数据文件，开始······");
		Map<String, Object> remap = new HashMap<>();
        remap.put("br", "01");
        //String dealdate = intfcFiInputMapper.getOpicsSysDate(remap);// 获取当前系统日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dealdate = simpleDateFormat.format(new Date());// 获取当前系统日期
        logger.info("[JOBS] ==> 获取当前系统日期:" + dealdate);
		try {
			List<IfsIntfcFi> intfcFiList = queryAllConditionFxListProc(dealdate,"FI","INITAIL");
			if(intfcFiList.size() >0) {
				for(IfsIntfcFi fi:intfcFiList) {
					executeHostVisitAction(fi);
				}
			}

		} catch (Exception e) {
			logger.info("MSG:executeHostVisitAction发送债券买入数据异常:"+e.getMessage());
		}
		logger.info("[JOBS-END] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台债券买入数据文件，结束······");
		
		logger.info("[JOBS-START] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台撤销债券数据文件，开始······");
		try {
			List<IfsIntfcFiReverse> intfcFiReverseList = queryAllConditionFiReverseListProc(dealdate,"FI","REVERSE");
			if(intfcFiReverseList.size() >0) {
				for(IfsIntfcFiReverse fiReverse:intfcFiReverseList) {
					executeHostVisitAction(fiReverse);
				}
			}

		} catch (Exception e) {
			logger.info("MSG:executeHostVisitAction发送债券买入数据异常:"+e.getMessage());
		}
		logger.info("[JOBS-END] ==> 开始执行交易状态同步【OPICS --> CHOIS】，分发平台撤销债券数据文件，结束······");
		
		return true;
	}
	
	public List<IfsIntfcFi> queryAllConditionFxListProc(String dealdate, String type,String condition){
		List<IfsIntfcFi> fiList = new ArrayList<IfsIntfcFi>();
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("dealdate", dealdate);
		instMap.put("type", type);
		instMap.put("condition", condition);
		
		List<IfsIntfcFi> intfcFiList = intfcFiInputMapper.queryAllConditionFiList(instMap);
		IfsIntfcFi intfcFi = null;
		for(IfsIntfcFi newfi :intfcFiList) {
			intfcFi = new IfsIntfcFi();
			intfcFi.setAccrualIntAmt(newfi.getAccrualIntAmt());
			intfcFi.setAccrualIntCcy(newfi.getAccrualIntCcy());
			intfcFi.setBuyAmt(newfi.getBuyAmt());
			intfcFi.setBuyCcy(newfi.getBuyCcy());
			intfcFi.setBuyPrice(newfi.getBuyPrice());
			intfcFi.setCno(newfi.getCno());
			intfcFi.setDealDate(newfi.getDealDate());
			intfcFi.setDealno(newfi.getDealno());
			intfcFi.setDiscountAmt(newfi.getDiscountAmt());
			intfcFi.setFullAmt(newfi.getFullAmt());
			intfcFi.setFullPrice(newfi.getFullPrice());
			intfcFi.setIndexRate(newfi.getIndexRate());
			intfcFi.setInputCode(newfi.getInputCode());
			intfcFi.setIntAmt(newfi.getIntAmt());
			intfcFi.setIntCcy(newfi.getIntCcy());
			intfcFi.setLstCouponDate(newfi.getLstCouponDate());
			intfcFi.setMatDate(newfi.getMatDate());
			intfcFi.setNextCouponDate(newfi.getNextCouponDate());
			intfcFi.setNominalAmt(newfi.getNominalAmt());
			intfcFi.setNominalCcy(newfi.getNominalCcy());
			intfcFi.setProdCode(newfi.getProdCode());
			intfcFi.setPurpose(newfi.getPurpose());
			intfcFi.setRemainMonth(newfi.getRemainMonth());
			intfcFi.setSecid(newfi.getSecid());
			intfcFi.setSeq(newfi.getSeq());
			intfcFi.setServer(newfi.getServer());
			intfcFi.setSettDate(newfi.getSettDate());
			intfcFi.setSettleMode(newfi.getSettleMode());
			intfcFi.setSpreadRate(newfi.getSpreadRate());
			intfcFi.setSubjectCode(newfi.getSubjectCode());
			intfcFi.setTotalRate(newfi.getTotalRate());
			intfcFi.setTradCode(newfi.getTradCode());
			intfcFi.setTraderCode(newfi.getTraderCode());
			intfcFi.setTranType(newfi.getTranType());
			intfcFi.setProdName(newfi.getProdName());
			intfcFi.setLongShortGb(newfi.getLongShortGb());
			intfcFi.setCancelEnableYn(newfi.getCancelEnableYn());
			intfcFi.setCollateRalinYn(newfi.getCollateRalinYn());
			intfcFi.setIntRcvIl(newfi.getIntRcvIl());
			intfcFi.setDealerId(newfi.getDealerId());
			intfcFi.setSetlCcy(newfi.getSetlCcy());
			intfcFi.setSetlAmt(newfi.getSetlAmt());
			intfcFi.setMarketGb(newfi.getMarketGb());
			intfcFi.setInoutGb(newfi.getInoutGb());
			intfcFi.setCounterPartyNm(newfi.getCounterPartyNm());
			intfcFi.setAcctIntCcy(newfi.getAcctIntCcy());
			/**
			 * 清算信息
			 */
			intfcFi.setCONFIRM_YN(newfi.getCONFIRM_YN());
			intfcFi.setCONFIRM_IL(newfi.getCONFIRM_IL());
			intfcFi.setPO_YN(newfi.getPO_YN());
			intfcFi.setPO_IL(newfi.getPO_IL());
			intfcFi.setOUR_RCV_DEPO_CD(newfi.getOUR_RCV_DEPO_CD());
			intfcFi.setOUR_RCV_DEPO_NM(newfi.getOUR_RCV_DEPO_NM());
			intfcFi.setOUR_PAY_DEPO_CD(newfi.getOUR_PAY_DEPO_CD());
			intfcFi.setOUR_PAY_DEPO_NM(newfi.getOUR_PAY_DEPO_NM());
			intfcFi.setTHR_RCV_DEPO_BIC(newfi.getTHR_RCV_DEPO_BIC());
			intfcFi.setTHR_RCV_DEPO_NM(newfi.getTHR_RCV_DEPO_NM());
			intfcFi.setCNAPS_YN(newfi.getCNAPS_YN());
			intfcFi.setCNAPS_RCV_ACCT_NO(newfi.getCNAPS_RCV_ACCT_NO());
			intfcFi.setCNAPS_CD(newfi.getCNAPS_CD());
			intfcFi.setCNAPS_NM(newfi.getCNAPS_NM());
			intfcFi.setCNAPS_RCV_ACCT_NM(newfi.getCNAPS_RCV_ACCT_NM());
			
			fiList.add(intfcFi);
		}
		
		return fiList;
	}
	public int executeHostVisitAction(IfsIntfcFi fi) throws Exception{
		int retFlag = 0;
		try {
			IfsIntfcFiStatus fiStatus = new IfsIntfcFiStatus();
			fiStatus.setTableName("SPSH");
			fiStatus.setBr("01");
			fiStatus.setReversal("N");
			fiStatus.setDealno(fi.getDealno());
			fiStatus.setSeq(fi.getSeq());
			fiStatus.setUpCount(0);
			fiStatus.setUpOwn("TMAX");
			fiStatus.setUpTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			fiStatus.setSendTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			
			long standardStampStart = System.currentTimeMillis();
			 //返回的柜员session
			String sess = chiosLogin.sendLogin(eaiDv, "OPS_CHS_DSS00001");
			
			StdTMsgGenerator generator = new StdTMsgGenerator();
			
			//报文头
			byte[] headerPacket=choisCommonMsg.makeHead();
			
			PropertiesConfiguration stdtmsgSysValue;
			
			stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
			String handleCode="FB1100";
			String serveHandleCode="OPCFB11";
			FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
			
			String coMsg=choisCommonMsg.makeMsg();//公共部分
			String coMsg2=choisCommonMsg.makeMsg2();//公共部分
			String coMsg3=choisCommonMsg.makeMsg3();//公共部分
			
			//交易数据
			System.out.println("intamt---"+String.valueOf(fi.getIntAmt()));
			String dealMsg=stringUtil.subStrToString(fi.getSecid(),15)
					+stringUtil.subStrToString(fi.getProdCode(),10)
					+stringUtil.subStrToString(fi.getProdName(),45)
					+String.format("%1$-6s",fi.getCno())
					+stringUtil.subStrToString(fi.getCounterPartyNm(),45)
					+fi.getDealDate()
					+fi.getSettDate()
					+fi.getMatDate()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getRemainMonth()), 3, 0)
					+fi.getTranType()
					+fi.getLongShortGb()
					+fi.getCancelEnableYn()
					+fi.getCollateRalinYn()
					+fi.getDealerId()
					+stringUtil.subStrToString(fi.getDealno(),8)
					+fi.getPurpose()
					+fi.getSettleMode()
					+fi.getNominalCcy()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getNominalAmt()), 16, 2)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getBuyPrice()), 16, 10)
					+fi.getBuyCcy()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getBuyAmt()),16,2)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getFullPrice()),16,10)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getFullAmt()),16,2)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getDiscountAmt()),16,2)
					+fi.getLstCouponDate()
					+fi.getNextCouponDate()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getIndexRate()),16,10)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getSpreadRate()),16,10)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getTotalRate()),16,10)
					+stringUtil.subStrOrNumber(String.valueOf(fi.getIntAmt()),16,2)
					+fi.getIntRcvIl()
					+fi.getIntCcy()
					+fi.getAccrualIntCcy()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getAccrualIntAmt()),16,2)
					+fi.getSetlCcy()
					+stringUtil.subStrOrNumber(String.valueOf(fi.getSetlAmt()),16,2)
					+fi.getMarketGb()+fi.getInoutGb()
					+fi.getCONFIRM_YN()
					+stringUtil.subStrToString(fi.getCONFIRM_IL(), 10)
					+fi.getPO_YN()+stringUtil.subStrToString(fi.getPO_IL(),10)
					+stringUtil.subStrToString(fi.getOUR_RCV_DEPO_CD(),4)
					+stringUtil.subStrToString(fi.getOUR_RCV_DEPO_NM(),54)
					+stringUtil.subStrToString(fi.getOUR_PAY_DEPO_CD(),4)
					+stringUtil.subStrToString(fi.getOUR_PAY_DEPO_NM(),54)
					+stringUtil.subStrToString(fi.getTHR_RCV_DEPO_BIC(),11)
					+subStrChinese.strAppendStr(fi.getTHR_RCV_DEPO_NM()==null?" ":fi.getTHR_RCV_DEPO_NM(),53," ")
					+fi.getCNAPS_YN()
					+stringUtil.subStrToString(fi.getCNAPS_RCV_ACCT_NO(),32)
					+subStrChinese.strAppendStr(fi.getCNAPS_RCV_ACCT_NM()==null?" ":fi.getCNAPS_RCV_ACCT_NM(),89," ")
					+stringUtil.subStrToString(fi.getCNAPS_CD(),12)
					+subStrChinese.strAppendStr(fi.getCNAPS_NM()==null?" ":fi.getCNAPS_NM(),89," ");
			
			byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
			byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
			String serviceUrl = "OPS";
			byte[] msgData =totalPacket;
			EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
			System.out.println("eaiDv:--"+eaiDv);
			byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
			System.out.println("债券返回信息："+new String(returnData, "UTF-8"));
			logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
			//返回成功失败标识
			String sendResult=(new String(returnData, "UTF-8")).substring(290,292);
			//CHOIS参考号
			String choisRefNoString=(new String(returnData, "UTF-8")).substring((new String(returnData, "UTF-8")).length()-22,(new String(returnData, "UTF-8")).length()-7);
			String choisRefHisString=(new String(returnData, "UTF-8")).substring((new String(returnData, "UTF-8")).length()-7,(new String(returnData, "UTF-8")).length()-2);
			logger.info("RECV MSG:"+new String(returnData, "UTF-8"));
			fiStatus.setErrMsg(new String(returnData, "UTF-8"));
			try {
				if(sendResult.equals("10")) {
					fiStatus.setSendResult("SUCCESS");
				}else {
					fiStatus.setSendResult("FAIL");
					
				}
				fiStatus.setIsReSend("N");
				fiStatus.setIsSend("Y");
				fiStatus.setChois_His_No(choisRefHisString);
				fiStatus.setChois_Ref_No(choisRefNoString);
				fiStatus.setSendMsg(new String(msgData, "UTF-8"));
			} catch (Exception sv) {
				if(null != sv)
				{
					logger.info("WebtServiceException msg:"+sv.toString());
					fiStatus.setErrMsg(sv.toString());
				}
				
				fiStatus.setSendResult("FAIL");
				Map<String, Object> instMap =new HashMap<String, Object>();
				instMap.put("dealno", fiStatus.getDealno());
				instMap.put("seq", fiStatus.getSeq());
				instMap.put("tableName", fiStatus.getTableName());
				IfsIntfcFiStatus oldFiStatus = intfcFiInputMapper.queryFiStatusByDealnoSeq(instMap);
				if(null != oldFiStatus)
				{
					fiStatus.setUpCount(oldFiStatus.getUpCount()+1);
					fiStatus.setUpOwn("TMAX");
					fiStatus.setIsReSend("Y");
					if(intfcFiInputMapper.updateFiStatusByDealnoSeq(fiStatus)>0)
					{
						retFlag = 2;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]成功.");
					}else
					{
						retFlag = 1;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]失败.");
					}
				}else
				{
					if(intfcFiInputMapper.insertFiStatusByNew(fiStatus)>0)
					{
						retFlag = 2;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]插入状态表[SL_FI_STATUS]成功.");
					}else {
						retFlag = 1;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]插入状态表[SL_FI_STATUS]失败.");
					}
				}
				chiosLogin.main();
				throw sv;
			}
			
			long standardStampEnd = System.currentTimeMillis();
			logger.info("提交至主机返回耗时 ："+ (standardStampEnd - standardStampStart) + "毫秒");
			if ((standardStampEnd - standardStampStart) > 60000) {
				chiosLogin.main();
				logger.info("[ROLLBACK]");
				return -1;
			} else {
				Map<String, Object> instMap =new HashMap<String, Object>();
				instMap.put("dealno", fiStatus.getDealno());
				instMap.put("seq", fiStatus.getSeq());
				instMap.put("tableName", fiStatus.getTableName());
				IfsIntfcFiStatus oldFiStatus = intfcFiInputMapper.queryFiStatusByDealnoSeq(instMap);
				if(null != oldFiStatus)
				{
					fiStatus.setUpCount(oldFiStatus.getUpCount()+1);
					fiStatus.setUpOwn("TMAX");
					fiStatus.setIsReSend("Y");
					if(intfcFiInputMapper.updateFiStatusByDealnoSeq(fiStatus)>0)
					{
						retFlag = 2;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]成功.");
					}else
					{
						retFlag = 1;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]失败.");
					}
				}else
				{
					if(intfcFiInputMapper.insertFiStatusByNew(fiStatus)>0)
					{
						retFlag = 2;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]插入状态表[SL_FI_STATUS]成功.");
					}else {
						retFlag = 1;
						logger.info("DEALNO["+fiStatus.getDealno()+"]TABLENAME["+fiStatus.getTableName()+"]插入状态表[SL_FI_STATUS]失败.");
					}
				}
				logger.info("[COMMIT]");
			}
			
		} catch (Exception e) {
			chiosLogin.main();
			
			e.printStackTrace();
			logger.info("[ROLLBACK] EXCEPTION:"+e.getMessage());
			throw e;
		}
		
		return retFlag;
	}
	
	
	public List<IfsIntfcFiReverse> queryAllConditionFiReverseListProc(String dealdate, String type,String condition){
		List<IfsIntfcFiReverse> fiReverseList = new ArrayList<IfsIntfcFiReverse>();
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("dealdate", dealdate);
		instMap.put("type", type);
		instMap.put("condition", condition);
		
		List<IfsIntfcFiReverse> intfcFiReverseList = intfcFiInputMapper.queryAllConditionFiReverseList(instMap);
		IfsIntfcFiReverse intfcFiReverse = null;
		for(IfsIntfcFiReverse newfiReverse :intfcFiReverseList) {
			intfcFiReverse = new IfsIntfcFiReverse();
			intfcFiReverse.setDealno(newfiReverse.getDealno());
			intfcFiReverse.setServer(newfiReverse.getServer());
			intfcFiReverse.setInputCode(newfiReverse.getInputCode());
			intfcFiReverse.setSubjectCode(newfiReverse.getSubjectCode());
			intfcFiReverse.setTradCode(newfiReverse.getTradCode());
			intfcFiReverse.setTraderCode(newfiReverse.getTraderCode());
			intfcFiReverse.setFXFIG_REF_NO(newfiReverse.getFXFIG_REF_NO());
			intfcFiReverse.setFXFIG_HIS_NO(newfiReverse.getFXFIG_HIS_NO());
			intfcFiReverse.setFIFB18_FRONT_SND_GB(newfiReverse.getFIFB18_FRONT_SND_GB());
			
			fiReverseList.add(intfcFiReverse);
		}
		
		return fiReverseList;
	}
	
	public int executeHostVisitAction(IfsIntfcFiReverse fiReverse) throws Exception{
		int retFlag = 0;
		IfsIntfcFiStatus fiStatus = new IfsIntfcFiStatus();
		fiStatus.setTableName("SPSH");	
		fiStatus.setChois_Ref_No(fiReverse.getFXFIG_REF_NO());
		fiStatus.setReversal("Y");
		fiStatus.setUpCount(0);
		fiStatus.setUpOwn("TMAX");
		fiStatus.setUpTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		fiStatus.setSendTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
		long standardStampStart = System.currentTimeMillis();

		//拼接报文
		
	     //返回的柜员session
		String sess =chiosLogin.sendLogin(eaiDv,"OPS_CHS_DSS00001");
		StdTMsgGenerator generator = new StdTMsgGenerator();
		//报文头
		byte[] headerPacket=choisCommonMsg.makeHead();
		
		//报文体
		 String handleCode="FB1800";
		 String serveHandleCode="OPCFB18";
		PropertiesConfiguration stdtmsgSysValue;
		
		stdtmsgSysValue = PropertiesUtil.parseFile("stdtmsgSysValue.properties");
		 
	
		FXFIG_SSCK=stdtmsgSysValue.getString("FXFIG_SSCK");
		
		String coMsg=choisCommonMsg.makeMsg();//公共部分
		String coMsg2=choisCommonMsg.makeMsg2();//公共部分
		String coMsg3=choisCommonMsg.makeMsg3();//公共部分
		//交易数据
		String dealMsg="FB11"+fiReverse.getFXFIG_REF_NO()+"00001"+fiReverse.getFIFB18_FRONT_SND_GB();
		byte[] dataPacket=(coMsg+handleCode+coMsg2+serveHandleCode+coMsg3+sess+FXFIG_SSCK+dealMsg).getBytes();
		
		byte[] totalPacket = generator.makeWholeMsg(new byte[][] { headerPacket, dataPacket });
		String serviceUrl = "OPS";
		byte[] msgData =totalPacket;
		EAIHttpAdapter httpCilent = EAIHttpAdapter.getInstance();
		System.out.println("eaiDv:--"+eaiDv);
		byte[] returnData = httpCilent.sendSychMsg(eaiDv, msgData, serviceUrl);
		System.out.println("债券交易撤销返回信息："+new String(returnData, "UTF-8"));
		logger.info("SEND MSG:"+new String(msgData, "UTF-8"));
		//返回成功失败标识
		String sendResult=(new String(returnData, "UTF-8")).substring(290,292);
		//CHOIS参考号
//		String choisRefNoString=(new String(returnData, "UTF-8")).substring(386,15);
		logger.info("RECV MSG:"+new String(returnData, "UTF-8"));
		fiStatus.setSendMsg(new String(msgData, "UTF-8"));
		fiStatus.setErrMsg(new String(returnData, "GBK"));
		try {
			if(sendResult.equals("10")) {
				fiStatus.setSendResult("SUCCESS");
			}else {
				fiStatus.setSendResult("FAIL");
				//fiStatus.setErrMsg(new String(returnData, "UTF-8"));
			}
			
			fiStatus.setIsReSend("N");
			fiStatus.setIsSend("Y");
			
		} catch (Exception sv) {
			
			
			logger.info("WebtServiceException msg:"+sv.toString());
			fiStatus.setErrMsg(sv.toString());
		}
		
		fiStatus.setUpOwn("TMAX");
		fiStatus.setIsReSend("Y");
		if(intfcFiInputMapper.updateFxStatusByChoisRefHisNo(fiStatus)>0)
		{
			retFlag = 2;
			logger.info("CHOIS_REF_NO["+fiStatus.getChois_Ref_No()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]成功.");
		}else {
			retFlag = 1;
			logger.info("CHOIS_REF_NO["+fiStatus.getChois_Ref_No()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]失败.");
		}
		
		
	
	
	
	long standardStampEnd = System.currentTimeMillis();
	logger.info("提交至主机返回耗时 ："+ (standardStampEnd - standardStampStart) + "毫秒");
	if ((standardStampEnd - standardStampStart) > 60000) {
		
		logger.info("提交主机超时:[ROLLBACK]");
		
	} else {
		if(intfcFiInputMapper.updateFxStatusByChoisRefHisNo(fiStatus)>0)
		{
			retFlag = 2;
			logger.info("CHOIS_REF_NO["+fiStatus.getChois_Ref_No()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]成功.");
		}else {
			retFlag = 1;
			logger.info("CHOIS_REF_NO["+fiStatus.getChois_Ref_No()+"]TABLENAME["+fiStatus.getTableName()+"]进行重发更新[SL_FI_STATUS]失败.");
		}
		logger.info("提交主机成功:[COMMIT]");
		
	}
		
		
		
		return retFlag;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
