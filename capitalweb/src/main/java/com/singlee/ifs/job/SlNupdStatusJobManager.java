package com.singlee.ifs.job;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.capital.choistrd.service.NupdService;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;

public class SlNupdStatusJobManager implements CronRunnable{
	
	private NupdService nupdService = SpringContextHolder.getBean(NupdService.class);

	private static Logger logManager = LoggerFactory.getLogger(SlNupdStatusJobManager.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================帐户行接口定时任务开始==============================");
		Boolean run = nupdService.executeHostVisitAction();
		logManager.info("==================帐户行接口定时任务完成==============================");
		return run;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
