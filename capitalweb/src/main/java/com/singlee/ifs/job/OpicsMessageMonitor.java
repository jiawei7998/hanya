package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.generated.model.field.Field20;
import com.singlee.generated.model.field.Field21;
import com.singlee.generated.model.field.Field79;
import com.singlee.generated.model.mt.mt2xx.MT299;
import com.singlee.ifs.mapper.*;
import com.singlee.ifs.model.*;
import com.singlee.swift.model.*;
import com.singlee.swift.model.field.SwiftParseUtils;
import com.singlee.swift.model.mt.ServiceIdType;
import org.apache.commons.lang.StringUtils;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 所有交易OPICS状态同步
 */
public class OpicsMessageMonitor implements CronRunnable {

    //Opics状态取回
	private IBaseServer monitor=SpringContextHolder.getBean(IBaseServer.class);
    //外汇即期
    private IfsCfetsfxSptMapper cfetsfxSptMapper = SpringContextHolder.getBean(IfsCfetsfxSptMapper.class);
    //外汇远期
    private IfsCfetsfxFwdMapper cfetsfxFwdMapper = SpringContextHolder.getBean(IfsCfetsfxFwdMapper.class);
    //外汇掉期
    private IfsCfetsfxSwapMapper cfetsfxSwapMapper = SpringContextHolder.getBean(IfsCfetsfxSwapMapper.class);
    //外币拆借
    private IfsCfetsfxLendMapper cfetsfxLendMapper = SpringContextHolder.getBean(IfsCfetsfxLendMapper.class);
    //信用拆借、同业借款
    private IfsCfetsrmbIboMapper cfetsrmbIboMapper = SpringContextHolder.getBean(IfsCfetsrmbIboMapper.class);
    //现券买卖、提前偿还（MBS）
    private IfsCfetsrmbCbtMapper cfetsrmbCbtMapper = SpringContextHolder.getBean(IfsCfetsrmbCbtMapper.class);
    //存单发行、债券发行
    private IfsCfetsrmbDpMapper cfetsrmbDpMapper = SpringContextHolder.getBean(IfsCfetsrmbDpMapper.class);
    //存单发行认购人明细、债券发行认购人明细
    private IfsCfetsrmbDpOfferMapper cfetsrmbDpOfferMapper = SpringContextHolder.getBean(IfsCfetsrmbDpOfferMapper.class);
    //质押式回购、交易所质押式回购、国库定期存款、常备借贷便利、中期借贷便利、线下押券
    private IfsCfetsrmbCrMapper cfetsrmbCrMapper = SpringContextHolder.getBean(IfsCfetsrmbCrMapper.class);
    //质押式回购质押券明细、交易所质押式回购质押券明细、国库定期存款质押券明细、常备借贷便利质押券明细、中期借贷便利质押券明细、线下押券质押券明细
    private IfsCfetsrmbDetailCrMapper cfetsrmbDetailCrMapper = SpringContextHolder.getBean(IfsCfetsrmbDetailCrMapper.class);
    //买断式回购、交易所买断式回购
    private IfsCfetsrmbOrMapper cfetsrmbOrMapper = SpringContextHolder.getBean(IfsCfetsrmbOrMapper.class);
    //利率互换
    private IfsCfetsrmbIrsMapper cfetsrmbIrsMapper = SpringContextHolder.getBean(IfsCfetsrmbIrsMapper.class);
    //债券远期
    private IfsCfetsrmbBondfwdMapper cfetsrmbBondfwdMapper = SpringContextHolder.getBean(IfsCfetsrmbBondfwdMapper.class);

    //SWIFT报文信息
    private IfsSwiftMapper ifsSwiftMapper = SpringContextHolder.getBean(IfsSwiftMapper.class);
    //交易对手信息
    private IfsOpicsCustMapper ifsOpicsCustMapper = SpringContextHolder.getBean(IfsOpicsCustMapper.class);
    //债券信息
    private IfsOpicsBondMapper ifsOpicsBondMapper = SpringContextHolder.getBean(IfsOpicsBondMapper.class);
    //存放同业(外币)
    private IfsFxDepositoutMapper ifsFxDepositoutMapper = SpringContextHolder.getBean(IfsFxDepositoutMapper.class);
    //存放同业
    private IfsRmbDepositoutMapper ifsRmbDepositoutMapper = SpringContextHolder.getBean(IfsRmbDepositoutMapper.class);


    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {

        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，OPICS交易状态同步任务，开始······");

        //添加清算信息类
        CommonSettleManager settleManager = new CommonSettleManager();

        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇即期交易状态同步任务，开始······");
        List<IfsCfetsfxSpt> listSpt = cfetsfxSptMapper.getCfetsfxSptList();
        for (IfsCfetsfxSpt ifsCfetsfxSpt : listSpt) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FXD.SERVER);
            inthBean.setTag(SlDealModule.FXD.TAG);
            inthBean.setBr(ifsCfetsfxSpt.getBr());
            inthBean.setFedealno(ifsCfetsfxSpt.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇即期交易状态同步任务，前置业务编号fedealno:" + ifsCfetsfxSpt.getTicketId() );
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇即期交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsfxSptMapper.updateCfetsfxSptByFedealno(map);
			}
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇即期交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇远期交易状态同步任务，开始······");
        List<IfsCfetsfxFwd> listFwd = cfetsfxFwdMapper.getCfetsfxFwdList();
        for (IfsCfetsfxFwd ifsCfetsfxFwd : listFwd) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FXD.SERVER);
            inthBean.setTag(SlDealModule.FXD.TAG);
            inthBean.setBr(ifsCfetsfxFwd.getBr());
            inthBean.setFedealno(ifsCfetsfxFwd.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇远期交易状态同步任务，前置业务编号fedealno:" + ifsCfetsfxFwd.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇远期交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsfxFwdMapper.updateCfetsfxFwdByFedealno(map);

            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇远期交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇掉期交易状态同步任务，开始······");
        List<IfsCfetsfxSwap> listSwap = cfetsfxSwapMapper.getCfetsfxSwapList();
        for (IfsCfetsfxSwap ifsCfetsfxSwap : listSwap) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FXD.SERVER);
            inthBean.setTag(SlDealModule.FXD.TAG);
            inthBean.setBr(ifsCfetsfxSwap.getBr());
            inthBean.setFedealno(ifsCfetsfxSwap.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇掉期交易状态同步任务，前置业务编号fedealno:" + ifsCfetsfxSwap.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                if (null != slInthBean.getSwapdealno()) {
                    map.put("farDealNo", slInthBean.getSwapdealno());
                }
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇掉期交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsfxSwapMapper.updateCfetsfxSwapByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外汇掉期交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，开始······");
        List<IfsCfetsfxLend> listLend = cfetsfxLendMapper.getCfetsfxLendList();
        for (IfsCfetsfxLend ifsCfetsfxLend : listLend) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.DL.SERVER);
            inthBean.setTag(SlDealModule.DL.TAG);
            inthBean.setBr(ifsCfetsfxLend.getBr());
            inthBean.setFedealno(ifsCfetsfxLend.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，前置业务编号fedealno:" + ifsCfetsfxLend.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsfxLendMapper.updateFxLendByFedealno(map);
                IfsOpicsCust cust = ifsOpicsCustMapper.searchIfsOpicsCust(ifsCfetsfxLend.getCounterpartyInstId());
                if (StringUtils.isNotEmpty(ifsCfetsfxLend.getRefId()) & "BCOH".equals(cust.getCname()) & StringUtils.isNotEmpty(slInthBean.getDealno())) { //交通银行
                    JY.info("[JOBS] ==> 开始创建MT299报文【前置】，外币拆借MT299报文创建任务，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
                    MT299 mt299 = new MT299();
                    SwiftBlock1 block1 = new SwiftBlock1();
                    block1.setApplicationId(SwiftBlock1.APPLICATION_ID_FIN);
                    block1.setServiceId(ServiceIdType._01.number());
                    block1.setLogicalTerminal(new LogicalTerminalAddress("DLCBCNBD").getSenderLogicalTerminalAddress());
                    block1.setSessionNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSessionNum(), 4, "0"));
                    block1.setSequenceNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSequenceNum(), 6, "0"));
                    mt299.getSwiftMessage().setBlock1(block1);
                    SwiftBlock2Input block2Input = new SwiftBlock2Input();
                    block2Input.setMessageType(MT299.NAME);
                    block2Input.setReceiverAddress(new LogicalTerminalAddress(cust.getBic()).getReceiverLogicalTerminalAddress());
                    block2Input.setMessagePriority(SwiftBlock2.MessagePriority.N.name());
                    mt299.getSwiftMessage().setBlock2(block2Input);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM.dd yyyy", Locale.ENGLISH);
                    SimpleDateFormat sdOld = new SimpleDateFormat("yyyy-MM-dd");
                    StringBuffer newStr = new StringBuffer();
                    newStr.append(MessageFormat.format("ACCORDING TO THE(OFFSHORE INTERBANK FACILITY AGREEMENT) REFERENCE NUMBERE:{0}", ifsCfetsfxLend.getRefId())).append("\r\n");
                    newStr.append(MessageFormat.format("SIGNED BETWEEN YOUR BANK COMMUNICATIONS CO.,LTD.AND OUR BANK OF DALIAN CO.,"
                            + "LTD,DATE:{0}", (sdf.format(sdOld.parse(ifsCfetsfxLend.getForDate())))).toUpperCase()).append("\r\n");
                    newStr.append("WE,AS THE BORROWER WOULD LIKETO APPLY FOR A LOAN WITH DETAILS AS FOLLOWS:").append("\r\n");
                    newStr.append(".").append("\r\n").append("\r\n");
                    newStr.append(MessageFormat.format("VALUE DATE:{0}", ifsCfetsfxLend.getValueDate())).append("\r\n");
                    newStr.append(MessageFormat.format("MARTURITY DATE:{0}", ifsCfetsfxLend.getMaturityDate())).append("\r\n");
                    newStr.append(MessageFormat.format("DAYS:{0}", ifsCfetsfxLend.getOccupancyDays())).append("\r\n");
                    newStr.append(MessageFormat.format("PRINCIPAL AMOUNT:{0}{1}", ifsCfetsfxLend.getCurrency(), ifsCfetsfxLend.getAmount())).append("\r\n");
                    newStr.append(MessageFormat.format("INTEREST RATE:{0}", ifsCfetsfxLend.getRate())).append("\r\n");
                    newStr.append(MessageFormat.format("INTEREST AMOUTN:{0}{1}", ifsCfetsfxLend.getCurrency(), ifsCfetsfxLend.getInterest())).append("\r\n");
                    newStr.append(MessageFormat.format("HANDING CHARGE RATE:{0}", 0)).append("\r\n");
                    newStr.append(MessageFormat.format("HANDING CHARGE AMOUTN:{0}", 0)).append("\r\n");
                    newStr.append(".").append("\r\n").append("\r\n");
                    newStr.append(MessageFormat.format("PLEASE REMIT THE PRINCIPLE AMOUNT TO OUR ACCOUNT WITH CITIBANK N.A,"
                            + "NEW YORK BRAMCH(SWIFT CODE:CITIUS33),ACCOUNT NO.{0}", ifsCfetsfxLend.getCounterpartyInstId())).append("\r\n");
                    newStr.append("WE IRREVOCABLY AND UN CONDITIONALLY UNERTAKE TO REPAY TO YOU REFF AND CLEAR OF ANY "
                            + "FEES AS ABOVE MENTIONED UPON MATURITY DATE TO YOUR ACCOUNT NUMBER SPECIFIED IN YOUR SWIFT CONFIRMATION").append("\r\n");
                    SwiftBlock4 block4 = new SwiftBlock4();
                    block4.append(new Field20(MessageFormat.format("DLCB{0}", slInthBean.getDealno())));
                    block4.append(new Field21(ifsCfetsfxLend.getRefId()));
                    List<String> resultMsg = SwiftParseUtils.splitComponents(newStr.toString(), 48);//按照48个字符分割
                    Field79 field = new Field79();
                    resultMsg = (resultMsg.size() > 34) ? resultMsg.subList(0, 34) : resultMsg;//只截取35行超出部分舍去.
                    for (int i = 0; i < resultMsg.size(); i++) {
                        field.setComponent(i + 1, resultMsg.get(i).replace("\r", ""));
                    }
                    block4.append(field);
                    mt299.getSwiftMessage().setBlock4(block4);
                    //System.out.println(mt299.message());
                    SlSwiftBean swiftBean = new SlSwiftBean();
                    String batchNo = DateUtil.format(monitor.getOpicsSysDate(slInthBean.getBr()), "yyMMdd") + ifsCfetsfxLend.getTicketId().substring(ifsCfetsfxLend.getTicketId().length() - 6);
                    swiftBean.setBr(slInthBean.getBr());
                    swiftBean.setBatchNo(batchNo);
                    swiftBean.setDealNo(slInthBean.getDealno());
                    swiftBean.setProdCode(ifsCfetsfxLend.getProduct());
                    swiftBean.setMsgType(MT299.NAME);
                    swiftBean.setMsg(mt299.message());
                    swiftBean.setProdType(ifsCfetsfxLend.getProdType());
                    Date dt = Calendar.getInstance().getTime();
                    swiftBean.setSwftcDate(dt);
                    swiftBean.setCdate(dt);
                    ifsSwiftMapper.insertSwiftData(swiftBean);
                    JY.info("[JOBS] ==> 创建结束MT299报文【前置】，外币拆借MT299报文创建任务，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
                }
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，信用拆借、同业借款交易状态同步任务，开始······");
        List<IfsCfetsrmbIbo> listIbo = cfetsrmbIboMapper.getCfetsIboList();
        for (IfsCfetsrmbIbo ifsCfetsrmbIbo : listIbo) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.DL.SERVER);
            inthBean.setTag(SlDealModule.DL.TAG);
            inthBean.setBr(ifsCfetsrmbIbo.getBr());
            inthBean.setFedealno(ifsCfetsrmbIbo.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，信用拆借、同业借款交易状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbIbo.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，信用拆借、同业借款交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsrmbIboMapper.updateCfetsIboByFedealno(map);
                // 清算相关处理
                if (!"1".equals(ifsCfetsrmbIbo.getNettingStatus())) {//不是净额清算的流向大额
                    String br = StringUtils.trimToEmpty(slInthBean.getBr());
                    String dealno = StringUtils.trimToEmpty(slInthBean.getDealno());
                    String fedealno = StringUtils.trimToEmpty(slInthBean.getFedealno());
                    String product = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProduct());
                    String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProdType());
                    if (!"".equals(dealno)) {
                        JY.info("[JOBS] ==> 开始执行创建清算信息【前置】，信用拆借、同业借款交易创建清算信息，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
                        settleManager.addSettle(br, dealno, fedealno, product, prodtype);
                    }
                }
            }
        }
        //获取当天，净额清算的swift中dealno不存在的交易
        List<IfsCfetsrmbIbo> listIbo2 = cfetsrmbIboMapper.searchSwiftNotExist();
        for (IfsCfetsrmbIbo ifsCfetsrmbIbo : listIbo2) {
            String br = "01";
            String dealno = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getDealNo());
            String product = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProduct());
            String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIbo.getProdType());
            JY.info("[JOBS] ==> 开始创建SWIFT报文【前置】，外币拆借、同业借款SWIFT报文创建任务，前置业务编号fedealno:" + ifsCfetsrmbIbo.getTicketId() + "业务编号dealno:" + dealno);
            settleManager.addSwift(br, dealno, product, prodtype);
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，信用拆借、同业借款交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖、提前偿还（MBS）交易状态同步任务，开始······");
        List<IfsCfetsrmbCbt> listCbt = cfetsrmbCbtMapper.getCfetsrmbCbtList();
        for (IfsCfetsrmbCbt ifsCfetsrmbCbt : listCbt) {
            SlInthBean inthBean = new SlInthBean();
            IfsOpicsBond bond = ifsOpicsBondMapper.searchById(ifsCfetsrmbCbt.getBondCode());
            if (null != bond && "MBS".equals(StringUtils.trimToEmpty(bond.getIntcalcrule()))) {
                inthBean.setServer(SlDealModule.IMBD.SERVER);
                inthBean.setTag(SlDealModule.IMBD.TAG);
            } else {
                inthBean.setServer(SlDealModule.FI.SERVER);
                inthBean.setTag(SlDealModule.FI.TAG);
            }
            inthBean.setBr(ifsCfetsrmbCbt.getBr());
            inthBean.setFedealno(ifsCfetsrmbCbt.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖、提前偿还（MBS）交易状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbCbt.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖、提前偿还（MBS）交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsrmbCbtMapper.updateCfetsrmbCbtByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，现券买卖、提前偿还（MBS）交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存单发行、债券发行交易状态同步任务，开始······");
        List<IfsCfetsrmbDpOffer> listDpOffer = cfetsrmbDpOfferMapper.getCfetsrmbDpOfferList();
        for (IfsCfetsrmbDpOffer ifsCfetsrmbDpOffer : listDpOffer) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FI.SERVER);
            inthBean.setTag(SlDealModule.FI.TAG);
            inthBean.setBr(ifsCfetsrmbDpOffer.getBr());
            inthBean.setSeq(ifsCfetsrmbDpOffer.getSeq());
            inthBean.setFedealno(ifsCfetsrmbDpOffer.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，存单发行、债券发行交易状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbDpOffer.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                map.put("seq", slInthBean.getSeq());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，存单发行、债券发行交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsrmbDpOfferMapper.updateCfetsrmbDpOfferByFedealno(map);
                IfsCfetsrmbDp searchBondLon = cfetsrmbDpMapper.searchBondLon(slInthBean.getFedealno());
                if ("-4".equals(slInthBean.getStatcode().trim())) {
                    String dealno = StringUtils.isEmpty(searchBondLon.getDealNo()) ? "" : searchBondLon.getDealNo() + ",";
                    searchBondLon.setDealNo(dealno + slInthBean.getDealno());
                }
                cfetsrmbDpMapper.updateByPrimaryKey(searchBondLon);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存单发行、债券发行交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购本身状态同步任务，开始······");
        List<IfsCfetsrmbCr> listCr = cfetsrmbCrMapper.getCfetsrmbCrList();
        for (IfsCfetsrmbCr ifsCfetsrmbCr : listCr) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.REPO.SERVER);
            inthBean.setTag(SlDealModule.REPO.TAG);
            inthBean.setBr(ifsCfetsrmbCr.getBr());
            inthBean.setSeq("0");
            inthBean.setFedealno(ifsCfetsrmbCr.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购本身状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbCr.getTicketId() );
			if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购本身状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsrmbCrMapper.updateCfetsrmbCrByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购本身状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购质押券明细状态同步任务，开始······");
        List<IfsCfetsrmbDetailCr> listDetailCr = cfetsrmbDetailCrMapper.getCfetsrmbDetailCrList();
        for (int i = 0; i < listDetailCr.size(); i++) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.REPO.SERVER);
            inthBean.setTag(SlDealModule.REPO.TAG);
            inthBean.setSeq(String.valueOf(listDetailCr.get(i).getSeq()));
            inthBean.setFedealno(listDetailCr.get(i).getTicketId());
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购质押券明细状态同步任务，前置业务编号fedealno:" + listDetailCr.get(i).getTicketId() );
			if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                map.put("seq", slInthBean.getSeq());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购质押券明细状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsrmbDetailCrMapper.updateDetailCrByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，质押式回购质押券明细状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购本身状态同步任务，开始······");
        List<IfsCfetsrmbOr> listOr = cfetsrmbOrMapper.getCfetsrmbOrList();
        for (IfsCfetsrmbOr ifsCfetsrmbOr : listOr) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.REPO.SERVER);
            inthBean.setTag(SlDealModule.REPO.TAG);
            inthBean.setBr(ifsCfetsrmbOr.getBr());
            inthBean.setSeq("0");
            inthBean.setFedealno(ifsCfetsrmbOr.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购本身状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbOr.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购本身状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsrmbOrMapper.updateCfetsrmbOrByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购回购本身状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购质押的券状态同步任务，开始······");
        List<IfsCfetsrmbOr> listOrBond = cfetsrmbOrMapper.getCfetsrmbOrBondList();
        for (IfsCfetsrmbOr ifsCfetsrmbOr : listOrBond) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.REPO.SERVER);
            inthBean.setTag(SlDealModule.REPO.TAG);
            inthBean.setSeq("1");
            inthBean.setFedealno(ifsCfetsrmbOr.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购质押的券状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbOr.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("bondStatcode", slInthBean.getStatcode());
                map.put("bondErrorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购质押的券状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsrmbOrMapper.updateOrBondByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，买断式回购质押的券状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，利率互换交易状态同步任务，开始······");
        List<IfsCfetsrmbIrs> listIrs = cfetsrmbIrsMapper.getCfetsrmbIrsList();
        for (IfsCfetsrmbIrs ifsCfetsrmbIrs : listIrs) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.SWAP.SERVER);
            inthBean.setTag(SlDealModule.SWAP.TAG);
            inthBean.setBr(ifsCfetsrmbIrs.getBr());
            inthBean.setFedealno(ifsCfetsrmbIrs.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
			JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，利率互换交易状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbIrs.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，利率互换交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
				cfetsrmbIrsMapper.updateCfetsrmbIrsByFedealno(map);
                // 清算相关处理
                if (!"1".equals(ifsCfetsrmbIrs.getNettingStatus())) {//不是净额清算的流向大额
                    // 清算相关处理
                    String br = StringUtils.trimToEmpty(slInthBean.getBr());
                    String dealno = StringUtils.trimToEmpty(slInthBean.getDealno());
                    String fedealno = StringUtils.trimToEmpty(slInthBean.getFedealno());
                    String product = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProduct());
                    String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProdType());
                    if (!"".equals(dealno)) {
                        JY.info("[JOBS] ==> 开始执行创建清算信息【前置】，利率互换交易创建清算信息，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
                        settleManager.addSettle(br, dealno, fedealno, product, prodtype);
                    }//清算end
                }
            }
        }
        //获取当天，净额清算的swift中dealno不存在的交易
        List<IfsCfetsrmbIrs> listIrs2 = cfetsrmbIrsMapper.searchSwiftNotExist();
        for (IfsCfetsrmbIrs ifsCfetsrmbIrs : listIrs2) {
            String br = "01";
            String dealno = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getDealNo());
            String product = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProduct());
            String prodtype = StringUtils.trimToEmpty(ifsCfetsrmbIrs.getProdType());
            JY.info("[JOBS] ==> 开始创建SWIFT报文【前置】，利率互换SWIFT报文创建任务，前置业务编号fedealno:" + ifsCfetsrmbIrs.getTicketId() + "业务编号dealno:" + dealno);
            settleManager.addSwift(br, dealno, product, prodtype);
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，利率互换交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，债券远期交易状态同步任务，开始······");
        List<IfsCfetsrmbBondfwd> listBondfwd = cfetsrmbBondfwdMapper.getCfetsrmbBondfwdList();
        for (IfsCfetsrmbBondfwd ifsCfetsrmbBondfwd : listBondfwd) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.FI.SERVER);inthBean.setTag(SlDealModule.FI.TAG);
            inthBean.setBr(ifsCfetsrmbBondfwd.getBr());
            inthBean.setFedealno(ifsCfetsrmbBondfwd.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，债券远期交易状态同步任务，前置业务编号fedealno:" + ifsCfetsrmbBondfwd.getTicketId() );
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，债券远期交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                cfetsrmbBondfwdMapper.updateCfetsrmbBondfwdByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，债券远期交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存放同业(外币)交易状态同步任务，开始······");
        List<IfsFxDepositout> listDeposits = ifsFxDepositoutMapper.searchDepositByTicketId();
        for (IfsFxDepositout ifsFxDepositout : listDeposits) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.DL.SERVER);
            inthBean.setTag(SlDealModule.DL.TAG);
            inthBean.setBr(ifsFxDepositout.getBr());
            inthBean.setFedealno(ifsFxDepositout.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，前置业务编号fedealno:" + ifsFxDepositout.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                ifsFxDepositoutMapper.updateDepositByFedealno(map);
//                IfsOpicsCust cust = ifsOpicsCustMapper.searchIfsOpicsCust(ifsCfetsfxLend.getCounterpartyInstId());
//                if (StringUtils.isNotEmpty(ifsCfetsfxLend.getRefId()) & "BCOH".equals(cust.getCname()) & StringUtils.isNotEmpty(slInthBean.getDealno())) { //交通银行
//                    JY.info("[JOBS] ==> 开始创建MT299报文【前置】，外币拆借MT299报文创建任务，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
//                    MT299 mt299 = new MT299();
//                    SwiftBlock1 block1 = new SwiftBlock1();
//                    block1.setApplicationId(SwiftBlock1.APPLICATION_ID_FIN);
//                    block1.setServiceId(ServiceIdType._01.number());
//                    block1.setLogicalTerminal(new LogicalTerminalAddress("DLCBCNBD").getSenderLogicalTerminalAddress());
//                    block1.setSessionNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSessionNum(), 4, "0"));
//                    block1.setSequenceNumber(StringUtil.leftFillStr(ifsSwiftMapper.getSequenceNum(), 6, "0"));
//                    mt299.getSwiftMessage().setBlock1(block1);
//                    SwiftBlock2Input block2Input = new SwiftBlock2Input();
//                    block2Input.setMessageType(MT299.NAME);
//                    block2Input.setReceiverAddress(new LogicalTerminalAddress(cust.getBic()).getReceiverLogicalTerminalAddress());
//                    block2Input.setMessagePriority(SwiftBlock2.MessagePriority.N.name());
//                    mt299.getSwiftMessage().setBlock2(block2Input);
//                    SimpleDateFormat sdf = new SimpleDateFormat("MMM.dd yyyy", Locale.ENGLISH);
//                    SimpleDateFormat sdOld = new SimpleDateFormat("yyyy-MM-dd");
//                    StringBuffer newStr = new StringBuffer();
//                    newStr.append(MessageFormat.format("ACCORDING TO THE(OFFSHORE INTERBANK FACILITY AGREEMENT) REFERENCE NUMBERE:{0}", ifsCfetsfxLend.getRefId())).append("\r\n");
//                    newStr.append(MessageFormat.format("SIGNED BETWEEN YOUR BANK COMMUNICATIONS CO.,LTD.AND OUR BANK OF DALIAN CO.,"
//                            + "LTD,DATE:{0}", (sdf.format(sdOld.parse(ifsCfetsfxLend.getForDate())))).toUpperCase()).append("\r\n");
//                    newStr.append("WE,AS THE BORROWER WOULD LIKETO APPLY FOR A LOAN WITH DETAILS AS FOLLOWS:").append("\r\n");
//                    newStr.append(".").append("\r\n").append("\r\n");
//                    newStr.append(MessageFormat.format("VALUE DATE:{0}", ifsCfetsfxLend.getValueDate())).append("\r\n");
//                    newStr.append(MessageFormat.format("MARTURITY DATE:{0}", ifsCfetsfxLend.getMaturityDate())).append("\r\n");
//                    newStr.append(MessageFormat.format("DAYS:{0}", ifsCfetsfxLend.getOccupancyDays())).append("\r\n");
//                    newStr.append(MessageFormat.format("PRINCIPAL AMOUNT:{0}{1}", ifsCfetsfxLend.getCurrency(), ifsCfetsfxLend.getAmount())).append("\r\n");
//                    newStr.append(MessageFormat.format("INTEREST RATE:{0}", ifsCfetsfxLend.getRate())).append("\r\n");
//                    newStr.append(MessageFormat.format("INTEREST AMOUTN:{0}{1}", ifsCfetsfxLend.getCurrency(), ifsCfetsfxLend.getInterest())).append("\r\n");
//                    newStr.append(MessageFormat.format("HANDING CHARGE RATE:{0}", 0)).append("\r\n");
//                    newStr.append(MessageFormat.format("HANDING CHARGE AMOUTN:{0}", 0)).append("\r\n");
//                    newStr.append(".").append("\r\n").append("\r\n");
//                    newStr.append(MessageFormat.format("PLEASE REMIT THE PRINCIPLE AMOUNT TO OUR ACCOUNT WITH CITIBANK N.A,"
//                            + "NEW YORK BRAMCH(SWIFT CODE:CITIUS33),ACCOUNT NO.{0}", ifsCfetsfxLend.getCounterpartyInstId())).append("\r\n");
//                    newStr.append("WE IRREVOCABLY AND UN CONDITIONALLY UNERTAKE TO REPAY TO YOU REFF AND CLEAR OF ANY "
//                            + "FEES AS ABOVE MENTIONED UPON MATURITY DATE TO YOUR ACCOUNT NUMBER SPECIFIED IN YOUR SWIFT CONFIRMATION").append("\r\n");
//                    SwiftBlock4 block4 = new SwiftBlock4();
//                    block4.append(new Field20(MessageFormat.format("DLCB{0}", slInthBean.getDealno())));
//                    block4.append(new Field21(ifsCfetsfxLend.getRefId()));
//                    List<String> resultMsg = SwiftParseUtils.splitComponents(newStr.toString(), 48);//按照48个字符分割
//                    Field79 field = new Field79();
//                    resultMsg = (resultMsg.size() > 34) ? resultMsg.subList(0, 34) : resultMsg;//只截取35行超出部分舍去.
//                    for (int i = 0; i < resultMsg.size(); i++) {
//                        field.setComponent(i + 1, resultMsg.get(i).replace("\r", ""));
//                    }
//                    block4.append(field);
//                    mt299.getSwiftMessage().setBlock4(block4);
//                    //System.out.println(mt299.message());
//                    SlSwiftBean swiftBean = new SlSwiftBean();
//                    String batchNo = DateUtil.format(monitor.getOpicsSysDate(slInthBean.getBr()), "yyMMdd") + ifsCfetsfxLend.getTicketId().substring(ifsCfetsfxLend.getTicketId().length() - 6);
//                    swiftBean.setBr(slInthBean.getBr());
//                    swiftBean.setBatchNo(batchNo);
//                    swiftBean.setDealNo(slInthBean.getDealno());
//                    swiftBean.setProdCode(ifsCfetsfxLend.getProduct());
//                    swiftBean.setMsgType(MT299.NAME);
//                    swiftBean.setMsg(mt299.message());
//                    swiftBean.setProdType(ifsCfetsfxLend.getProdType());
//                    Date dt = Calendar.getInstance().getTime();
//                    swiftBean.setSwftcDate(dt);
//                    swiftBean.setCdate(dt);
//                    ifsSwiftMapper.insertSwiftData(swiftBean);
//                    JY.info("[JOBS] ==> 创建结束MT299报文【前置】，外币拆借MT299报文创建任务，前置业务编号fedealno:" + slInthBean.getFedealno() + "业务编号dealno:" + slInthBean.getDealno());
//                }
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存放同业(外币)交易状态同步任务，结束······");


        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存放同业交易状态同步任务，开始······");
        List<IfsRmbDepositout> listRmbDeposits = ifsRmbDepositoutMapper.searchDepositByTicketId();
        for (IfsRmbDepositout ifsRmbDepositout : listRmbDeposits) {
            SlInthBean inthBean = new SlInthBean();
            inthBean.setServer(SlDealModule.DL.SERVER);
            inthBean.setTag(SlDealModule.DL.TAG);
            inthBean.setBr(ifsRmbDepositout.getBr());
            inthBean.setFedealno(ifsRmbDepositout.getTicketId());
            // 正常情况下单笔模块只会查询到一笔数据
            SlInthBean slInthBean = monitor.getInthStatus(inthBean);
            // 根据fedealno回查更新状态
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，前置业务编号fedealno:" + ifsRmbDepositout.getTicketId());
            if (slInthBean != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fedealno", slInthBean.getFedealno());
                map.put("statcode", slInthBean.getStatcode());
                map.put("errorcode", slInthBean.getErrorcode());
                map.put("dealno", slInthBean.getDealno());
                JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <--OPICS】，外币拆借交易状态同步任务，业务编号dealno:" + slInthBean.getDealno());
                ifsRmbDepositoutMapper.updateDepositByFedealno(map);
            }
        }
        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，存放同业交易状态同步任务，结束······");

        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <--OPICS】，OPICS交易状态同步任务，结束······");

        return true;
    }

    @Override
    public void terminate() {
        // TODO Auto-generated method stub
    }

}
