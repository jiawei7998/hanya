package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlCnapsBean;
import com.singlee.financial.esb.hsbank.ICnapsServer;
import com.singlee.ifs.model.IfsTrdSettle;
import com.singlee.ifs.service.IfsTrdSettleService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CnapsJobManager implements CronRunnable {

    private ICnapsServer cnapServer = SpringContextHolder.getBean("ICnapsServer");
    private IfsTrdSettleService ifsTrdSettleService = SpringContextHolder.getBean(IfsTrdSettleService.class);

    @Override
    @Transactional(value="transactionManager",rollbackFor=Exception.class)
    public boolean execute(Map<String, Object> arg0) throws Exception {
    	JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->大额】，大额定时发送任务，开始······");
        Map<String, Object> queryMap = new HashMap<String, Object>();
        List<String> settFlags = new ArrayList<String>();
        settFlags.add("1");
        queryMap.put("settFlags", settFlags);
        List<IfsTrdSettle> list = ifsTrdSettleService.getTrdSettleList(queryMap).getResult();

        if (null == list || list.size() == 0) {
            return false;
        }

        List<SlCnapsBean> cnapList = new ArrayList<SlCnapsBean>();
        for (int i = 0; i < list.size(); i++) {
            IfsTrdSettle its = list.get(i);
            SlCnapsBean cnaps = new SlCnapsBean();
            cnaps.setCertNo(its.getSn());
            cnaps.setBalanceDate(its.getVtime());
            cnaps.setAmount(its.getAmount());
            cnaps.setCertState(its.getStatus());
            cnaps.setRecNo(its.getRecUserid());
            cnaps.setRecName(its.getRecUsername());
            cnaps.setPayNo(its.getPayBankid());
            cnaps.setPayName(its.getPayBankname());
            cnaps.setRemark(its.getRemarkFy());
            cnapList.add(cnaps);

            // 更新表中settFlag为2
            Map<String, Object> updateMap = new HashMap<String, Object>();
            updateMap.put("dealflag", its.getDealflag());
            updateMap.put("settflag", "2");
            updateMap.put("dealno", its.getDealno());
            ifsTrdSettleService.updateTrdSettle(updateMap);
        }
        cnapServer.cnapsSend(cnapList);
        JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->大额】，大额定时发送任务，结束······");
        return false;
    }

    @Override
    public void terminate() {
        // TODO Auto-generated method stub

    }

}
