package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.cdtc.bean.RepoSecurity;
import com.singlee.financial.cdtc.bean.SlCnbondlinkSecurDeal;
import com.singlee.financial.cdtc.hrb.HrbCdtcForCheckClearJobServer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @apiNote 中债_待结算业务核对要素(7-19点，每2分钟一次)
 * @author Administrator
 *
 */
public class CdtcForCheckClearJobManager implements CronRunnable {

	private HrbCdtcForCheckClearJobServer cdtcForCheckClearJobServer = SpringContextHolder.getBean("HrbCdtcForCheckClearJobServer");

//	@Autowired
//	private HrbCdtcForCheckClearJobServer cdtcForCheckClearJobServer;

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 中债】，中债_待结算业务核对要素，开始······");
		String postDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		//TODO 判断是否工作日,查OPICS表
		Boolean isHoldDay=Boolean.TRUE;
		if (Boolean.TRUE) {
			isHoldDay=Boolean.TRUE;
		}
		//TODO 将债券信息插入SL_CDTC_CNBONDLINK_SECURDEAL表
		
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("postDate", postDate);
		params.put("isHoldDay", isHoldDay);
		Function<SlCnbondlinkSecurDeal, List<RepoSecurity>> getRepoList=(s)->{
			//根据SlCnbondlinkSecurDeal 查OPICS相关表 获取回购子券
			return null;
		};
		//核对交易要素
		cdtcForCheckClearJobServer.doQuartzJobCheck(params,getRepoList);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 中债】，中债_待结算业务核对要素，结束······");
		return true;
	}


	@Override
	public void terminate() {

	}

}
