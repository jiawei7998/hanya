package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindCmFincomeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得基金万份收益
 *
 */
public class WindCmFincomeJobManger implements CronRunnable {

	private WindCmFincomeService windCmFincomeService = SpringContextHolder.getBean(WindCmFincomeService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindCmFincomeJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得基金万份收益==============================");
		Boolean run = windCmFincomeService.run();
		logManager.info("==================万得基金万份收益处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
