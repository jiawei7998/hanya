package com.singlee.ifs.job;

import com.singlee.capital.chois.util.SwiftProcessor;
import com.singlee.capital.choistrd.controller.SendFxChoisController;
import com.singlee.capital.choistrd.controller.SendOtcController;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dict.service.DictionaryGetService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.ifs.baseUtil.SingleeSFTPClient;
import com.singlee.ifs.mapper.IfsSwiftMapper;

import java.io.File;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 期权批量发送报文定时任务
 *
 */
public class FxSendJobManage implements CronRunnable{
	
 	//IfsSwiftMapper ifsSwiftMapper = SpringContextHolder.getBean(IfsSwiftMapper.class);
 	
 	
 	private SendFxChoisController sendFxChoisController = SpringContextHolder.getBean(SendFxChoisController.class);;

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->FX】，批量发送报文定时任务，开始······");
		 SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd");
		 Date date =new Date();
		Map<String, Object> instMap =new HashMap<String, Object>();
		instMap.put("postdate", sdfdate.format(date));
		sendFxChoisController.sendFxChois(instMap);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->FX】，批量发送报文定时任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
	
}
