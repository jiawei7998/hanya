package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.CFtTpos;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基金持仓备份
 */
public class FundTposJobManager implements CronRunnable {

    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
    private CFtTposMapper cFtTposMapper = SpringContextHolder.getBean("CFtTposMapper");

    @Override
    @Transactional(value = "transactionManager", rollbackFor = Exception.class)
    public boolean execute(Map<String, Object> arg0) throws Exception {
        JY.info("[JOBS-START] ==> 开始执行基金持仓备份，开始······");
        Date date = baseServer.getOpicsSysDate("01");
        Date nextbranprcdate = baseServer.getOpicsNextbranprcdate("01");

        JY.info("[JOBS] ==> 账务日期:" + DateUtil.format(date) + "备份日期:" + DateUtil.format(nextbranprcdate));

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("postDate", DateUtil.format(date));

        List<CFtTpos> listCFtTpos = cFtTposMapper.getListCFtTpos(map);
        JY.info("[JOBS] ==> 账务日期 postDate:" + DateUtil.format(date) + "获取需要备份的数据" + listCFtTpos.size() + "条!");

        for (CFtTpos tpos : listCFtTpos) {
            JY.info("[JOBS] ==> 备份 基金代码 ： " + tpos.getFundCode() + "基金名称：" + tpos.getFundName());
            tpos.setPostdate(DateUtil.format(nextbranprcdate));

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("fundCode", tpos.getFundCode());
            params.put("postdate", tpos.getPostdate());
            params.put("sponinst", tpos.getSponInst());
            params.put("invtype", tpos.getInvtype());
            params.put("prdNo", tpos.getPrdNo());
            CFtTpos obj = cFtTposMapper.selectByMap(params);

            if(null == obj){
                cFtTposMapper.insertSelective(tpos);
                JY.info("[JOBS] ==> 备份 基金代码 ： " + tpos.getFundCode() + "基金名称：" + tpos.getFundName() + "成功！");
            }else{
                JY.info("[JOBS] ==> 备份 基金代码 ： " + tpos.getFundCode() + "基金名称：" + tpos.getFundName()+ "失败！已存在备份数据");
            }

        }

        JY.info("[JOBS-END] ==> 开始执行基金持仓备份，结束······");
        return true;
    }

    @Override
    public void terminate() {

    }


}
