package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsrmbSlMapper;
import com.singlee.ifs.model.IfsCfetsrmbSl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 债券借贷定时任务(无用)
 * 
 * @author shenzl
 *
 */
public class BondMessageMonitor implements CronRunnable {
	/**
	 * 债券借贷定时任务 查询opics的表
	 */
	private IBaseServer monitor = SpringContextHolder.getBean("IBaseServer");

	private IfsCfetsrmbSlMapper cfetsrmbSlMapper = SpringContextHolder.getBean(IfsCfetsrmbSlMapper.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券借贷定时任务，开始······");
		// 查询全部未复核
		List<IfsCfetsrmbSl> listSpt = cfetsrmbSlMapper.getCfetsrmbSlList();
		for (IfsCfetsrmbSl ifsCfetsrmbSl : listSpt) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.SECLEND.SERVER);
			inthBean.setTag(SlDealModule.SECLEND.TAG);
			inthBean.setBr(ifsCfetsrmbSl.getBr());
			inthBean.setSeq("0");
			inthBean.setFedealno(ifsCfetsrmbSl.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据dealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券借贷同步编号：fedealno:"+slInthBean.getFedealno()+"dealno:"+slInthBean.getDealno());
				cfetsrmbSlMapper.updateCfetsrmbSlByFedealno(map);
			}
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券借贷定时任务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
