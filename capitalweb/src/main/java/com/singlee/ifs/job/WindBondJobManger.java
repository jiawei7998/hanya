package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.hrbextra.wind.WindBondService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 万得债券信息数据
 *
 */
public class WindBondJobManger implements CronRunnable {

	private WindBondService windBondService = SpringContextHolder.getBean(WindBondService.class);

	private static Logger logManager = LoggerFactory.getLogger(WindBondJobManger.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================开始批量处理万得债券信息数据==============================");
		Boolean run = windBondService.run();
		logManager.info("==================万得债券信息数据处理完成==============================");
		return run;
	}

	@Override
	public void terminate() {
	}

}
