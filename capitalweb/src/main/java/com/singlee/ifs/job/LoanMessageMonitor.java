package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.credit.mapper.TcCustCreditDealMapper;
import com.singlee.capital.eu.mapper.TdEdCustMapper;
import com.singlee.financial.bean.LoanBean;
import com.singlee.financial.bean.TdedCust;
import com.singlee.financial.esb.tlcb.IFileServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 信贷额度定时任务
 */
public class LoanMessageMonitor implements CronRunnable {

	private static Logger logManager = LoggerFactory.getLogger(LoanMessageMonitor.class);
	
	private IFileServer iFileServer = SpringContextHolder.getBean("IFileServer");
	
	TcCustCreditDealMapper tcCustCreditDealMapper = SpringContextHolder.getBean(TcCustCreditDealMapper.class);
	TdEdCustMapper tdEdCustMapper = SpringContextHolder.getBean(TdEdCustMapper.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("**********信贷额度跑批开始***********");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String nowDate = sdf.format(new Date());
		/**
		 * 1、查询表中的数据
		 */
		List<LoanBean> detail = tcCustCreditDealMapper.getDetailEd(); // 明细
		List<TdedCust> gather = tdEdCustMapper.getGatherEd(); // 汇总

		/**
		 * 2、生成文件
		 */
		String detailLocalFile = "";
		String gatherLocalFile = "";
		// 生成明细文件
		if (detail.size() != 0) { // 保证表中有数据
			detailLocalFile = iFileServer.createLoanDetailFile(detail); // 本地文件路径
		}
		// 生成汇总文件
		if (gather.size() != 0) { // 保证表中有数据
			gatherLocalFile = iFileServer.createLoanGatherFile(gather); // 本地文件路径
		}

		/**
		 * 3、上传文件至ESB
		 */
		/*
		 * 传明细文件
		 */
		String[] datailSpl = detailLocalFile.split("/");
		String datailRemoteFile = "/OPICS/" + nowDate + "/" + datailSpl[datailSpl.length - 1];
		iFileServer.filePut("LOAN", detailLocalFile, datailRemoteFile, "8888");
		/*
		 * 传汇总文件
		 */
		String[] gatherSpl = gatherLocalFile.split("/");
		String gatherRemoteFile = "/OPICS/" + nowDate + "/" + gatherSpl[gatherSpl.length - 1];
		iFileServer.filePut("LOAN", gatherLocalFile, gatherRemoteFile, "8888");
		/*
		 * 传ok空文件（信贷要求标志文件）
		 */
		String blankLocalFile = iFileServer.createBlankFile();
		String[] spl = blankLocalFile.split("/");
		String blankRemoteFile = "/OPICS/" + nowDate + "/" + spl[spl.length - 1];
		iFileServer.filePut("LOAN", blankLocalFile, blankRemoteFile, "8888");

		logManager.info("**********信贷额度跑批结束***********");
		return true;
	}

	@Override
	public void terminate() {

	}
}