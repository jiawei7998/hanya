package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsmetalGoldMapper;
import com.singlee.ifs.model.IfsCfetsmetalGold;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class GoldJobManager implements CronRunnable {
	private IBaseServer monitor = SpringContextHolder.getBean("IBaseServer");
	private IfsCfetsmetalGoldMapper cfetsmetalGoldMapper = SpringContextHolder.getBean(IfsCfetsmetalGoldMapper.class);
	
	private static Logger logManager = LoggerFactory.getLogger(GoldJobManager.class);

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		logManager.info("==================Start GoldJobManager execute=====================");
		/**
		 * 黄金 即期，远期，掉期
		 */
		// 查询全部未复核
 		List<IfsCfetsmetalGold> listGold = cfetsmetalGoldMapper.getCfetsMetalGoldList();
 		for (IfsCfetsmetalGold ifsCfetsMetalGold : listGold) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.FXD.SERVER);
			inthBean.setTag(SlDealModule.FXD.TAG);
			inthBean.setBr(ifsCfetsMetalGold.getBr());
			inthBean.setFedealno(ifsCfetsMetalGold.getTicketId());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			
			// 根据fedealno回查更新状态
			if (slInthBean != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				if("-5".equals(slInthBean.getStatcode().replaceAll(" ", ""))){
					map.put("approveStatus", "8");
					}else{
					map.put("approveStatus", ifsCfetsMetalGold.getApproveStatus());
					}
				map.put("errorcode", slInthBean.getErrorcode());
				map.put("dealno", slInthBean.getDealno());
				if(null!=slInthBean.getSwapdealno()){
					map.put("swapDealno", slInthBean.getSwapdealno());	
				}
				cfetsmetalGoldMapper.updateCfetsMetalGoldByFedealno(map);
			}
 		}
 	     logManager.info("==================End GoldJobManager execute=====================");
		return true;
	}
	
	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

	

}
