package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsCfetsrmbDetailSlMapper;
import com.singlee.ifs.mapper.IfsCfetsrmbSlMapper;
import com.singlee.ifs.mapper.IfsOpicsBondMapper;
import com.singlee.ifs.model.IfsOpicsBond;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecurMessageMonitor implements CronRunnable{
	/**
	 * 查询opics的表
	 */
	private IBaseServer monitor= SpringContextHolder.getBean("IBaseServer");

	private IfsOpicsBondMapper opicsBondMapper= SpringContextHolder.getBean(IfsOpicsBondMapper.class);
	
	private IfsCfetsrmbSlMapper cfetsrmbSlMapper = SpringContextHolder.getBean(IfsCfetsrmbSlMapper.class);
	
	private IfsCfetsrmbDetailSlMapper cfetsrmbDetailSlMapper = SpringContextHolder.getBean(IfsCfetsrmbDetailSlMapper.class);

	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券模块交易状态同步处理，开始······");
		// 查询全部未复核
		List<IfsOpicsBond> listBond = opicsBondMapper.getOpicsBondList();
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券基本信息交易状态同步处理，开始······");
		for (IfsOpicsBond ifsOpicsBond : listBond) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.SECUR.SERVER);
			inthBean.setTag(SlDealModule.SECUR.TAG);
			inthBean.setBr(ifsOpicsBond.getBr());
			inthBean.setFedealno(ifsOpicsBond.getDealno());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			// 根据fedealno回查更新状态
			if(slInthBean!=null){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fedealno", slInthBean.getFedealno());
				map.put("statcode", slInthBean.getStatcode());
				map.put("errorcode", slInthBean.getErrorcode());
				opicsBondMapper.updateBondByFedealno(map);
			}
			
		}
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券模块交易状态同步处理，开始······");
		// 查询全部已复核
		List<IfsOpicsBond> listBond2 = opicsBondMapper.getSyncOpicsBondList();
		for (IfsOpicsBond ifsOpicsBond : listBond2) {
			SlInthBean inthBean = new SlInthBean();
			inthBean.setServer(SlDealModule.SECUR.SERVER);
			inthBean.setTag(SlDealModule.SECUR.TAG);
			inthBean.setBr(ifsOpicsBond.getBr());
			inthBean.setFedealno(ifsOpicsBond.getDealno());
			// 正常情况下单笔模块只会查询到一笔数据
			SlInthBean slInthBean = monitor.getInthStatus(inthBean);
			if(slInthBean!=null){
				Map<String, Object> sMap = new HashMap<String, Object>();
					if(slInthBean.getStatcode().trim().endsWith("-4")){
						sMap.put("status", "C");
						sMap.put("bndcd", ifsOpicsBond.getBndcd());
						opicsBondMapper.updateStatus(sMap);
					}else if(slInthBean.getStatcode().trim().endsWith("-6")){
						sMap.put("status", "D");
						sMap.put("bndcd", ifsOpicsBond.getBndcd());
						opicsBondMapper.updateStatus(sMap);
					}
			}
		}
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 <-- OPICS】，债券模块交易状态同步处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
