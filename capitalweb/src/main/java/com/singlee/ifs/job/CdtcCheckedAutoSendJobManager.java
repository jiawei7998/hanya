package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.cdtc.hrb.HrbCdtcCheckedAutoSendJobServer;
import com.singlee.financial.pojo.RetBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @apiNote 中债_发送核算完成指令确认(7-19点，每3分钟一次)
 * @author Administrator
 *
 */
public class CdtcCheckedAutoSendJobManager implements CronRunnable {

	private HrbCdtcCheckedAutoSendJobServer cdtcCheckedAutoSendJobServer = SpringContextHolder.getBean("HrbCdtcCheckedAutoSendJobServer");

//	@Autowired
//	private HrbCdtcCheckedAutoSendJobServer cdtcCheckedAutoSendJobServer;

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 中债】，中债_发送核算完成指令确认，开始······");
		String postDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		// TODO 判断是否工作日,查OPICS表
		Boolean isHoldDay = Boolean.TRUE;
		if (Boolean.TRUE) {
			isHoldDay = Boolean.TRUE;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("isHoldDay", isHoldDay);
		params.put("postDate", postDate);
		RetBean ret = cdtcCheckedAutoSendJobServer.autoSendCheckedXml(params);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 中债】，中债_发送核算完成指令确认，结束······");
		return true;
	}

	@Override
	public void terminate() {

	}

}
