package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.ifs.baseUtil.SingleeJschUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CfetsJobStopManager implements CronRunnable {
	private static Logger logger = LoggerFactory.getLogger(CfetsJobStopManager.class);
	String host = SystemProperties.cstpVmIp;
	String user = SystemProperties.cstpVmUsername;
	String psw = SystemProperties.cstpVmPassword;
	int port = SystemProperties.cstpVmPort;
	// 本币
	String cstpRmbStop = SystemProperties.cstpRmbStop;
	// 外币
	String cstpFxStop = SystemProperties.cstpFxStop;

	@Override
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时关闭，开始······");
		/** cfets开启前先关闭 **/
		logger.debug("CSTP FX IMIX STOP COMMAND ===>" + cstpFxStop);
		String resultFx = SingleeJschUtil.exec(host, user, psw, port, cstpFxStop);
		JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-FX[%s];RET:[%s]", cstpFxStop,resultFx));
		String resultRmb = SingleeJschUtil.exec(host, user, psw, port, cstpRmbStop);
		JY.info(String.format("[JOBS] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-FX[%s];RET:[%s]", cstpRmbStop,resultRmb));
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> CSTP-CLIENT】，CSTP-CLIENT定时关闭，结束······");
		return true;
	}

	@Override
	public void terminate() {

	}

}
