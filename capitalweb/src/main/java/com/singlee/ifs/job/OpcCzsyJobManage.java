package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.SlOpcCzsyBean;
import com.singlee.financial.common.util.SingleeSFTPClient;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IOpcCzsyServer;
import com.singlee.ifs.model.IfsOpsCzsyBean;
import com.singlee.ifs.service.IfsOpsCzsyService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 处置收益报表（买卖价差）,需要从"营业税"报表中取数。
 * 
 *
 */
public class OpcCzsyJobManage implements CronRunnable {

	private final IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private final IOpcCzsyServer opcCzsyServer = SpringContextHolder.getBean("IOpcCzsyServer");
	private final IfsOpsCzsyService ifsOpsCzsyService = SpringContextHolder.getBean(IfsOpsCzsyService.class);

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，收益报表任务处理，开始······");
		Date postdate = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		String datetime = DateUtil.format(postdate, "yyyy-MM-dd");
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，收益报表任务处理，当前系统时间为：" + datetime);
		String year = datetime.substring(0, 4);
		String month = datetime.substring(5, 7);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，收益报表任务处理，当前年份为：" + year + ";当前月份为：" + month);
		// 获取起始 结束日期
		String startDate = DateUtil.format(DateUtil.getStartDateTimeOfMonth(postdate));
		String endDate = DateUtil.format(DateUtil.getEndDateTimeOfMonth(postdate));
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，收益报表任务处理，当前时间取数起始日期为：" + startDate + ";当前时间取数结束日期为：" + endDate);
		String src_folder = SystemProperties.OpcPath;// 文件路径
		String fileName = SystemProperties.OpcCzsyfileName + datetime.replace("-", "") + ".dat";// 文档名称
		String archiveFileName = SystemProperties.OpcCzsyfileName + datetime.replace("-", "") + ".ind";// 备份文档名称

//		// 接收结果
//		List<SlOpcCzsyBean> resultList = null;
		List<IfsOpsCzsyBean> resultList = null;


		// 统计结果
		List<List<String>> writeList = new ArrayList<List<String>>();

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("datetime", datetime);
//		resultList = opcCzsyServer.selectOpcCzsy(map);
//		writeList = GenerateOpcCzsy(resultList, datetime);
		resultList = ifsOpsCzsyService.getData(map);
		writeList = GenerateOpcCzsy(resultList, datetime);


		// 生成文件
		new SingleeSFTPClient().GenerateFile(src_folder, fileName, archiveFileName, writeList);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->前置】，收益报表任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}

	/**
	 *  加工数据并生成文件
	 * 1	交易序号	报表中取	pdealno
	 * 2	账户类型	报表中取	iinvtype
	 * 3	债券类型	报表中取	acctdesc 去掉  "债券"
	 * 4	债券代码	报表中取	isecid
	 * 5	债券名称	报表中取	descr
	 * 6	机构编号	0100	
	 * 7	机构名称	空	
	 * 8	科目代码	债券本金的7位科目代码，如514042*	放置债券本金的科目代码
	 * 9	科目名称	空	放置债券本金的科目代码的名称
	 * 10	币种		CNY
	 * 11	处置收益	卖出净价-买入净价(U-P)
	 * 
	 * @param resultList
	 * @param datetime
	 * @return
	 */
//	public List<List<String>> GenerateOpcCzsy(List<SlOpcCzsyBean> resultList, String datetime) {
//		List<List<String>> writeList = new ArrayList<List<String>>();
//		List<String> write = null;
//		for (SlOpcCzsyBean slOpcCzsyBean : resultList) {
//			write = new ArrayList<String>();
//			write.add(slOpcCzsyBean.getPdealno());
//			write.add(slOpcCzsyBean.getIinvtype());
//			write.add(slOpcCzsyBean.getAcctdesc());
//			write.add(slOpcCzsyBean.getIsecid());
//			write.add(slOpcCzsyBean.getDescr());
//			write.add("00100");
//			write.add("哈尔滨银行总行结算中心");// 机构名称
//
//			// 取本金科目号
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("br", slOpcCzsyBean.getBr());
//			map.put("secid", slOpcCzsyBean.getSecid());
//			map.put("cost", slOpcCzsyBean.getCost());
//			map.put("product", slOpcCzsyBean.getProduct());
//			map.put("prodtype", slOpcCzsyBean.getProdtype());
//			map.put("seq", "T".equals(slOpcCzsyBean.getInvtype()) ? "11" : "33");
//
//			List<SlOpcCzsyBean> glList = opcCzsyServer.selectGlno(map);
//			if (glList != null && glList.size() > 0) {
//				write.add(glList.get(0).getGlno().substring(0, 7));
//			} else {
//				write.add("");
//			}
//
//			write.add("");// 科目名称
//			write.add("CNY");
//			write.add(slOpcCzsyBean.getDiff());
//			writeList.add(write);
//		}
//		return writeList;
//	}


	/**
	 *  加工数据并生成文件
	 * 1	交易序号	报表中取	pdealno
	 * 2	账户类型	报表中取	iinvtype
	 * 3	债券类型	报表中取	acctdesc 去掉  "债券"
	 * 4	债券代码	报表中取	isecid
	 * 5	债券名称	报表中取	descr
	 * 6	机构编号	0100
	 * 7	机构名称	空
	 * 8	科目代码	债券本金的7位科目代码，如514042*	放置债券本金的科目代码
	 * 9	科目名称	空	放置债券本金的科目代码的名称
	 * 10	币种		CNY
	 * 11	处置收益	卖出净价-买入净价(U-P)
	 *
	 * @param resultList
	 * @param datetime
	 * @return
	 */
	public List<List<String>> GenerateOpcCzsy(List<IfsOpsCzsyBean> resultList, String datetime) {
		List<List<String>> writeList = new ArrayList<List<String>>();
		List<String> write = null;
		for (IfsOpsCzsyBean slOpcCzsyBean : resultList) {
			write = new ArrayList<String>();
			write.add(slOpcCzsyBean.getPdealno());
			write.add(slOpcCzsyBean.getIinvtype());
			write.add(slOpcCzsyBean.getAcctdesc());
			write.add(slOpcCzsyBean.getIsecid());
			write.add(slOpcCzsyBean.getDescr());
			write.add("00100");
			write.add("哈尔滨银行总行结算中心");// 机构名称

			// 取本金科目号
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("br", slOpcCzsyBean.getBr());
			map.put("secid", slOpcCzsyBean.getSecid());
			map.put("cost", slOpcCzsyBean.getCost());
			map.put("product", slOpcCzsyBean.getProduct());
			map.put("prodtype", slOpcCzsyBean.getProdtype());
			map.put("seq", "T".equals(slOpcCzsyBean.getInvtype()) ? "11" : "33");

			List<SlOpcCzsyBean> glList = opcCzsyServer.selectGlno(map);
			if (glList != null && glList.size() > 0) {
				write.add(glList.get(0).getGlno().substring(0, 7));
			} else {
				write.add("");
			}

			write.add("");// 科目名称
			write.add("CNY");
			write.add(slOpcCzsyBean.getDiff());
			writeList.add(write);
		}
		return writeList;
	}
}
