package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.common.util.SingleeSFTPClient;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.IOpcNssbServer;
import com.singlee.ifs.model.IfsOpsNssbBean;
import com.singlee.ifs.service.IfsOpsNssbService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 增值税送交易  纳税申报申请取数
 * 
 *
 */
public class OpcNssbJobManage implements CronRunnable {

	private final IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private final IOpcNssbServer opcNssbServer = SpringContextHolder.getBean("IOpcNssbServer");
	private final IfsOpsNssbService ifsOpsNssbService = SpringContextHolder.getBean(IfsOpsNssbService.class);


	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public boolean execute(Map<String, Object> arg0) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 -->前置】，纳税申报申请取数任务处理，开始······");
		Date postdate = baseServer.getOpicsSysDate("01");// 获取当前系统日期
		String datetime = DateUtil.format(postdate, "yyyy-MM-dd");
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，纳税申报申请取数任务处理，当前系统时间为：" + datetime);
		String year = datetime.substring(0, 4);
		String month = datetime.substring(5, 7);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，纳税申报申请取数任务处理，当前年份为：" + year + ";当前月份为：" + month);
		// 获取起始 结束日期
		String startDate = DateUtil.format(DateUtil.getStartDateTimeOfMonth(postdate));
		String endDate = DateUtil.format(DateUtil.getEndDateTimeOfMonth(postdate));
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 -->前置】，纳税申报申请取数任务处理，当前时间取数起始日期为：" + startDate + ";当前时间取数结束日期为：" + endDate);
		String src_folder = SystemProperties.OpcPath;// 文件路径

		String fileName = SystemProperties.OpcNssbfileName + datetime.replace("-", "") + ".dat";// 文档名称
		String archiveFileName = SystemProperties.OpcNssbfileName + datetime.replace("-", "") + ".ind";// 备份文档名称

		// 接收结果
//		List<SlOpcNssbBean> resultList = null;
		List<IfsOpsNssbBean> resultList = null;
		// 统计结果
		List<List<String>> writeList = new ArrayList<List<String>>();

		/**
		 * Part1 1 OPICS系统中可供出售类当季国债利息收入（不取5140301数据） 对应5140301科目的1330401科目本季借方发生额 2
		 * OPICS系统中当期可供出售类地方政府债利息收入（不取5140307数据） 对应5140307科目的1330407科目本季借方发生额 3
		 * OPICS系统中可供出售央行票据、政策性银行债券、金融债、商业银行存单。
		 * 对应5140302科目的1330402科目本季借方发生额（不取5140302数据）+对应5140303科目的1330403科目本季借方发生额（不取5140303数据）+
		 * 对应5140304科目的1330402科目本季借方发生额（不取5140304数据）+对应5140305科目的1330405科目本季借方发生额（不取5140305数据）
		 * 
		 * Part1数据集结果为：第一列GLNO为科目号，第二列AMOUNT为汇总金额。
		 */
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startDate", startDate);
		map.put("endDate", endDate);
//		resultList = opcNssbServer.selectOpcNssbPayTaxes(map);
//		writeList.addAll(GenerateOpcKpsj(resultList, 1, datetime));
		map.put("type","1");
		resultList = ifsOpsNssbService.getData(map);
		writeList.addAll(GenerateOpcKpsj(resultList, 1, datetime));

		/**
		 * Part2 根据转让债权计算表的sql取得
		 * 
		 * Part2数据集结果为：第一列GLNO为科目号，第二列PS为买卖关系，第三列AMOUNT为汇总金额。
		 */
		map.put("type","2");
		map.put("datetime", datetime);
		map.put("glno", "144");
		map.put("ps", "S");
		resultList=ifsOpsNssbService.getData(map);
//		resultList = opcNssbServer.selectOpcNssbCreditorRight(map);

		map.put("ps", "P");
//		resultList.addAll(opcNssbServer.selectOpcNssbCreditorRight(map));
		resultList.addAll(ifsOpsNssbService.getData(map));

		map.put("glno", "148");
//		resultList.addAll(opcNssbServer.selectOpcNssbCreditorRight(map));
		resultList.addAll(ifsOpsNssbService.getData(map));

		map.put("ps", "S");
//		resultList.addAll(opcNssbServer.selectOpcNssbCreditorRight(map));
		resultList.addAll(ifsOpsNssbService.getData(map));

		writeList.addAll(GenerateOpcKpsj(resultList, 2, datetime));

		// 生成文件
		new SingleeSFTPClient().GenerateFile(src_folder, fileName, archiveFileName, writeList);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 -->前置】，纳税申报申请取数任务处理，结束······");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
	}


	/**
	 * listPart1 存储规则如下：				listPart2 存储规则如下：
	 * 									交易性卖出净价	交易性买入净价	可供出售卖出净价	可供出售买入净价
	 * 科目号		1330401					144			144			148			148
	 * 科目名称		空						空			空			空			空
	 * 机构号		0100					0100		0100		0100		0100
	 * 贷方发生额	空						汇总金额		空			汇总金额		空
	 * 借方发生额	100						空			汇总金额		空			汇总金额
	 * 贷方余额		空						空			空			空			空
	 * 借方余额		空						空			空			空			空
	 * 币种		CNY						CNY			CNY			CNY			CNY
	 * 交易日期		20160930				YYYYMMDD	YYYYMMDD	YYYYMMDD	YYYYMMDD
	 * 
	 * @param resultList
	 * @param type
	 * @param datetime
	 * @return
	 */
//	public List<List<String>> GenerateOpcKpsj(List<SlOpcNssbBean> resultList, int type, String datetime) {
//		List<List<String>> writeList = new ArrayList<List<String>>();
//		List<String> write = null;
//		for (SlOpcNssbBean slOpcNssbBean : resultList) {
//			write = new ArrayList<String>();
//			write.add(slOpcNssbBean.getGlno());
//			write.add("");// 科目名称 空
//			write.add("00100");// 机构号 0100
//
//			if (type == 1) {
//				write.add("");// 贷方发生额 空
//				write.add(slOpcNssbBean.getAmount());
//			} else if (type == 2) {
//				if ("P".equals(slOpcNssbBean.getPs())) {
//					write.add("");// 贷方发生额
//					write.add(slOpcNssbBean.getAmount());
//				} else {
//					write.add(slOpcNssbBean.getAmount());
//					write.add("");// 借方发生额
//				}
//			}
//
//			write.add("");// 贷方余额 空
//			write.add("");// 借方余额 空
//			write.add("CNY");// 币种 CNY
//			write.add(datetime);// 交易日期,月末
//
//			writeList.add(write);
//		}
//		return writeList;
//	}


	public List<List<String>> GenerateOpcKpsj(List<IfsOpsNssbBean> resultList, int type, String datetime) {
		List<List<String>> writeList = new ArrayList<List<String>>();
		List<String> write = null;
		for (IfsOpsNssbBean slOpcNssbBean : resultList) {
			write = new ArrayList<String>();
			write.add(slOpcNssbBean.getGlno());
			write.add("");// 科目名称 空
			write.add("00100");// 机构号 0100

			if (type == 1) {
				write.add("");// 贷方发生额 空
				write.add(slOpcNssbBean.getAmount());
			} else if (type == 2) {
				if ("P".equals(slOpcNssbBean.getPs())) {
					write.add("");// 贷方发生额
					write.add(slOpcNssbBean.getAmount());
				} else {
					write.add(slOpcNssbBean.getAmount());
					write.add("");// 借方发生额
				}
			}

			write.add("");// 贷方余额 空
			write.add("");// 借方余额 空
			write.add("CNY");// 币种 CNY
			write.add(datetime);// 交易日期,月末

			writeList.add(write);
		}
		return writeList;
	}

}
