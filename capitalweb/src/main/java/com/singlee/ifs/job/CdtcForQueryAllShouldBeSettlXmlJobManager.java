package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.financial.cdtc.hrb.HrbCdtcForQueryAllShouldBeSettlXmlJobServer;
import com.singlee.financial.pojo.RetBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @apiNote 中债_查询当日批量待结算业务 (7-19点，每三分钟一次)
 * @author Administrator
 *
 */
public class CdtcForQueryAllShouldBeSettlXmlJobManager implements CronRunnable{

	private HrbCdtcForQueryAllShouldBeSettlXmlJobServer cdtcForQueryAllShouldBeSettlXmlJobServer = SpringContextHolder.getBean(HrbCdtcForQueryAllShouldBeSettlXmlJobServer.class);
//	@Autowired
//	private HrbCdtcForQueryAllShouldBeSettlXmlJobServer cdtcForQueryAllShouldBeSettlXmlJobServer;
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 中债】，中债_查询当日批量待结算业务，开始······");
		String postDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		//TODO 判断是否工作日,查OPICS表
		Boolean isHoldDay=Boolean.TRUE;
		if (Boolean.TRUE) {
			isHoldDay=Boolean.TRUE;
		}
//
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("postDate", postDate);
		params.put("isHolyDay", false);
		RetBean ret = cdtcForQueryAllShouldBeSettlXmlJobServer.queryCdtcDetailToOracleDatabaseXmlTables(params);
		JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 中债】，中债_查询当日批量待结算业务，结束······");
		return true;
	}

	@Override
	public void terminate() {
		
	}

}
