package com.singlee.ifs.job;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.capital.choistrd.service.CustService;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;

public class CustJobManager implements CronRunnable{
	
	private CustService custService = SpringContextHolder.getBean(CustService.class);

	private static Logger logManager = LoggerFactory.getLogger(SecmJobManager.class);
	
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		logManager.info("==================客戶信息接口定时任务开始==============================");
		Boolean run = custService.executeHostVisitAction();
		logManager.info("==================客戶信息接口定时任务完成==============================");
		return run;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}

}
