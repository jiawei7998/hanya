package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.common.util.FileChannelUtil;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s100001001700105.RequestBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.S700105Bean;
import com.singlee.financial.esb.hbcb.service.S100001001700105Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.financial.esb.hrbcb.ISlAcupServer;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.ifs.mapper.IfsEsbMassageMapper;
import com.singlee.ifs.model.AcupFileBean;
import com.singlee.ifs.model.IfsEsbMassage;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * 日终自动送账
 */
public class AutoSendAcupJobManager implements CronRunnable {

    private TaSysParamMapper taSysParamMapper = SpringContextHolder.getBean(TaSysParamMapper.class);
    private ISlAcupServer iSlAcupServer = SpringContextHolder.getBean("ISlAcupServer");
    private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
    private S100001001700105Service s100001001700105Service = SpringContextHolder.getBean(S100001001700105Service.class);
    private IfsEsbMassageMapper ifsEsbMassageMapper = SpringContextHolder.getBean(IfsEsbMassageMapper.class);

    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {

        JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 --> 核心】，日终自动送账，开始······");

        Map<String, Object> map = new HashMap<String, Object>();
        // 获取当前账务日期
        Date postdates = baseServer.getOpicsSysDate("01");
        String strSendDate = ParameterUtil.getString(parameters, "coreDate", DateUtil.format(postdates));
        Date postdate = DateUtil.parse(strSendDate);
        map.put("postdate", strSendDate);
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，总账日期：" + DateUtil.format(postdate, "yyyyMMdd"));
        Map<String, Object> sysMap = new HashMap<String, Object>();
        sysMap.put("p_type", "ESB");
        List<TaSysParam> EsbList = taSysParamMapper.selectTaSysParam(sysMap);
        sysMap.put("p_type", "ESBMS700105");
        List<TaSysParam> EsbMS700105List = taSysParamMapper.selectTaSysParam(sysMap);
        EsbList.addAll(EsbMS700105List);
        S700105Bean s700105Bean = new S700105Bean();
        RequestBody requestBody = new RequestBody();
        RequestHeader requestHeader = new RequestHeader();
        // 初始化数据
        GenerateS700105Bean(requestBody, requestHeader, EsbList);
        // 本地文件目录
        String path = MessageFormat.format("{0}{1}{2}", SystemProperties.acupPath, DateUtil.format(postdate, "yyyyMMdd"), "/in/");
        File filedir = new File(path);
        if (!filedir.exists()) {
            filedir.mkdirs();
        }
        // 获取文件序号
        int num = getFileSeq(path);
        // 文件序号
        String fileSeq = "9" + StringUtil.stringFormat(String.valueOf(num), "0", 3, "L");
        String fileName = MessageFormat.format("{0}{1}{2}{3}{4}{5}", "I003", "00100", "SAFM", DateUtil.format(postdate, "yyyyMMdd"), fileSeq, ".DAT");
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，日终发送文件名" + fileName + "，并开始组装文件内容");
        // 获取当日需要送的账
        List<AcupSendBean> acupList = iSlAcupServer.getSlAcupList(map);
        if (acupList.size() < 1) {
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，当日无送账记录!!!");
            return true;
        }
        // 明细列表
        List<String> strlist = new ArrayList<String>();
        // 成功处理的交易
        List<AcupSendBean> listBean = new ArrayList<AcupSendBean>();
        AcupSendBean acupSendBean = null;
        // 按交易单号分组
        Map<String, List<AcupSendBean>> groupByDealnoMap = acupList.stream().collect(Collectors.groupingBy(AcupSendBean::getDealno));
        int seq = 0;// 明细序号
        Set<Entry<String, List<AcupSendBean>>> entrySetDealno = groupByDealnoMap.entrySet();
        for (Entry<String, List<AcupSendBean>> entryDealno : entrySetDealno) {
            seq++;// 自增
            String dealno = entryDealno.getKey(); // 交易单号
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，开始组装明细dealno" + dealno);
            // 该交易处理后对象列表
            List<AcupFileBean> afblist = new ArrayList<AcupFileBean>();
            // 交易下按套号分组
            Map<String, List<AcupSendBean>> groupBySetMap = entryDealno.getValue().stream().collect(Collectors.groupingBy(AcupSendBean::getSetno));
            Set<Entry<String, List<AcupSendBean>>> entrySetSet = groupBySetMap.entrySet();
            for (Entry<String, List<AcupSendBean>> entrySet : entrySetSet) {
                List<AcupSendBean> list = entrySet.getValue();// 套号下账
                if (list.size() == 2) {// 1:一借一贷 2：表外
                    if (list.get(0).getGlno().startsWith("8300") && list.get(1).getGlno().startsWith("8300")) {
                        afblist.add(creatBwBean(list.get(0)));// 表外
                        afblist.add(creatBwBean(list.get(1)));// 表外
                    } else {
                        afblist.add(creatJDBean(list));// 一借一贷
                    }
                } else if (list.size() == 3) {// 1:一借多贷 2:一贷多借
                    afblist.addAll(creatJDMBean(list));
                } else if (list.size() == 4) {// 1:多借多贷 2:一借多贷 3: 一贷多借 4:表外+ 一借一贷
                    List<AcupSendBean> acupBWList = new ArrayList<AcupSendBean>();
                    List<AcupSendBean> acupJDList = new ArrayList<AcupSendBean>();
                    for (AcupSendBean asb : list) {
                        if (asb.getGlno().startsWith("8300")) {
                            acupBWList.add(asb);
                        } else {
                            acupJDList.add(asb);
                        }
                    }
                    if (acupBWList.size() == 2 && acupJDList.size() == 2) {// 表外+ 一借一贷
                        afblist.add(creatBwBean(acupBWList.get(0)));// 表外
                        afblist.add(creatBwBean(acupBWList.get(1)));// 表外
                        afblist.add(creatJDBean(acupJDList));// 一借一贷
                    } else if (acupBWList.size() == 0 && acupJDList.size() == 4) {// 1:多借多贷 2:一借多贷 3: 一贷多借
                        Map<String, List<AcupSendBean>> collect = acupJDList.stream().collect(Collectors.groupingBy(AcupSendBean::getDrcrind));
                        if (collect.get("D").size() == collect.get("C").size()) {// 多借多贷
                            afblist.addAll(creatJMDMBean(acupJDList));// 多借多贷
                        } else {// 2:一借多贷 3: 一贷多借
                            afblist.addAll(creatJDMBean(acupJDList));
                        }
                    }
                } else if (list.size() == 6) {// 1:表外+ 2一借一贷
                    List<AcupSendBean> acupBWList = new ArrayList<AcupSendBean>();
                    List<AcupSendBean> acupJDList = new ArrayList<AcupSendBean>();
                    for (AcupSendBean asb : list) {
                        if (asb.getGlno().startsWith("8300")) {
                            acupBWList.add(asb);
                        } else {
                            acupJDList.add(asb);
                        }
                    }
                    if (acupBWList.size() == 2 && acupJDList.size() == 4) {
                        afblist.add(creatBwBean(acupBWList.get(0)));// 表外
                        afblist.add(creatBwBean(acupBWList.get(1)));// 表外
                        Map<String, List<AcupSendBean>> acupByAmountMap = acupJDList.stream().collect(Collectors.groupingBy(AcupSendBean::getAmount));// 用金额分组
                        Set<Entry<String, List<AcupSendBean>>> entrySetAmount = acupByAmountMap.entrySet();
                        for (Entry<String, List<AcupSendBean>> entryAmount : entrySetAmount) {
                            List<AcupSendBean> entList = entryAmount.getValue();
                            afblist.add(creatJDBean(entList));// 一借一贷
                        }
                    }
                }
            }
            if (afblist.size() > 0) {//成功处理
                strlist.add(GenerateD(afblist, seq, dealno, DateUtil.format(postdate, "yyyyMMdd")));
                acupSendBean = new AcupSendBean();
                acupSendBean.setDealFlag("1");
                acupSendBean.setDealno(dealno.trim());
                acupSendBean.setPostdate(DateUtil.format(postdate, "yyyyMMdd"));
                listBean.add(acupSendBean);
            } else {//未成功处理
                seq--;
            }
        }

        if (strlist.size() <= 0) {
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，当日无送账记录");
            return true;
        }

        // 创建文件
        File file = new File(path, fileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        String strh = GenerateH(requestHeader, seq, DateUtil.format(postdate, "ddMMyyyy"), file.getName());
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，文件首记录：" + strh);
        PrintWriter pw = new PrintWriter(file);
        pw.write(strh.toString() + "\r\n");
        for (String strd : strlist) {
            pw.write(strd.toString() + "\r\n");
        }
        pw.flush();
        pw.close();
        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，组装文件成功，开始上传文件。");
        // 初始化FTP
        //SingleeSFTPClient sftp = new SingleeSFTPClient(SystemProperties.acupIp, SystemProperties.acupPort,SystemProperties.acupUserName, SystemProperties.acupPassword);
        // 开始连接
        //sftp.connect();
        // 上传文件 SystemProperties.acupPath本地备份文件路径 SystemProperties.acupCatalog远程上传文件路径
        //boolean flag = sftp.uploadFile(SystemProperties.acupCatalog + "file_in/" + DateUtil.format(postdate, "yyyyMMdd") + "/", fileName, path, fileName);

        //创建日期文件夹
        String dts = SystemProperties.acupCatalog + "file_in/" + DateUtil.format(postdate, "yyyyMMdd") + "/";
        FileChannelUtil.createDirs(dts);
        //复制文件
        boolean flag = FileChannelUtil.fileCopy(file, new File(dts + fileName));

        if (flag) {
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，文件上传成功，开始发送通知报文。");
            // 文件上传成功 开始发送通知报文
            requestHeader.setReqDt(DateUtil.format(postdate, "ddMMyyyy"));// 请求方交易日期
            requestHeader.setBrchNo("00100");// 机构号
            requestHeader.setUUID(EsbSend.createUUID());
            requestHeader.setSndFileNme(fileName);

            requestBody.setFileName(fileName);// 需处理的文件全名
            requestBody.setBrchNum("00100");// 机构码
            requestBody.setSubmitDate(DateUtil.format(postdate, "ddMMyyyy"));// 提交日期
            requestBody.setBatSeq("000000" + DateUtil.getCurrentDateAsString("HHmmss"));// 顺序号 当日内不能重复。右对齐,左补0

            s700105Bean.setRequestBody(requestBody);
            s700105Bean.setRequestHeader(requestHeader);
            EsbOutBean outBean = s100001001700105Service.send(s700105Bean);
            JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，通知报文发送成功。");
            if ("0020010000".equals(outBean.getRetCode())) {
                iSlAcupServer.updateBatchSlAcupHendle(listBean);
            }

            // 保存报文
            IfsEsbMassage ifsEsbMassage = new IfsEsbMassage();
            ifsEsbMassage.setDealno(fileName.substring(0, 25).trim());
            List<IfsEsbMassage> select = ifsEsbMassageMapper.select(ifsEsbMassage);
            if (select.size() == 1) {
                ifsEsbMassage = select.get(0);
                ifsEsbMassage.setSenddate(postdate);
                ifsEsbMassage.setRecdate(postdate);
                ifsEsbMassage.setSendmsg(outBean.getSendmsg());
                ifsEsbMassage.setRecmsg(outBean.getRecmsg());
                ifsEsbMassage.setRescode(outBean.getRetCode());
                ifsEsbMassage.setResmsg(outBean.getRetMsg());
                Example example = new Example(IfsEsbMassage.class);
                example.createCriteria().andEqualTo("dealno", ifsEsbMassage.getDealno())
                        .andEqualTo("service", ifsEsbMassage.getService());
                ifsEsbMassageMapper.updateByExample(ifsEsbMassage, example);
            } else if (select.size() == 0) {
                ifsEsbMassage.setService("S100001001700105");
                ifsEsbMassage.setSefile(file.getName());
                ifsEsbMassage.setSenddate(postdate);
                ifsEsbMassage.setRecdate(postdate);
                ifsEsbMassage.setSendmsg(outBean.getSendmsg());
                ifsEsbMassage.setRecmsg(outBean.getRecmsg());
                ifsEsbMassage.setRescode(outBean.getRetCode());
                ifsEsbMassage.setResmsg(outBean.getRetMsg());
                ifsEsbMassageMapper.insert(ifsEsbMassage);
            }
        }

        JY.info("[JOBS-END] ==> 开始执行交易状态同步【前置 --> 核心】，日终自动送账，结束······");
        return true;
    }

    @Override
    public void terminate() {
    }

    /**
     * 初始化请求报文
     *
     * @param requestBody
     * @param requestHeader
     * @param sysList
     */
    public static void GenerateS700105Bean(RequestBody requestBody, RequestHeader requestHeader,
                                           List<TaSysParam> sysList) {
        for (TaSysParam taSysParam : sysList) {
            if ("VerNo".equals(taSysParam.getP_code())) {
                requestHeader.setVerNo(taSysParam.getP_value().trim());
            } else if ("ReqSysCd".equals(taSysParam.getP_code())) {
                requestHeader.setReqSysCd(taSysParam.getP_value().trim());
            } else if ("TxnTyp".equals(taSysParam.getP_code())) {
                requestHeader.setTxnTyp(taSysParam.getP_value().trim());
            } else if ("TxnMod".equals(taSysParam.getP_code())) {
                requestHeader.setTxnMod(taSysParam.getP_value().trim());
            } else if ("TerminalType".equals(taSysParam.getP_code())) {
                // 终端类型 输入有效值：0，1，2 0：柜员前端和ESB
                requestHeader.setTerminalType(taSysParam.getP_value().trim());
            } else if ("Flag1".equals(taSysParam.getP_code())) {
                // 标识1 0-正常交易 1-倒退日交易 2-正常授权交易 3-倒退日授权交易 4,5,6,7ATM/POS交易使用 倒退日交易见详细的各个模块交易说明
                requestHeader.setFlag1(taSysParam.getP_value().trim());
            } else if ("Flag4".equals(taSysParam.getP_code())) {
                // 标识4 渠道标识： 0-柜面 5-ESB（表示渠道交易）
                requestHeader.setFlag4(taSysParam.getP_value().trim());
            } else if ("ChnlNo".equals(taSysParam.getP_code())) {
                // 渠道号
                requestHeader.setChnlNo(taSysParam.getP_value().trim());
            } else if ("TrmNo".equals(taSysParam.getP_code())) {
                // 终端号
                requestHeader.setTrmNo(taSysParam.getP_value().trim());
            } else if ("TlrNo".equals(taSysParam.getP_code())) {
                // 柜员号
                requestHeader.setTlrNo(taSysParam.getP_value().trim());
            } else if ("BancsSeqNo".equals(taSysParam.getP_code())) {
                // 流水号 输入填0，输出为bancs的流水号. 多金融组合交易的流水号是一样的
                requestHeader.setBancsSeqNo(taSysParam.getP_value().trim());
            } else if ("SupervisorID".equals(taSysParam.getP_code())) {
                // 复核或授权时输入主管id号，复核或授权的时候才有效，其他建议填”0000000”。主管ID只提供给柜面前端使用，其他外围系统的授权处理在各自的业务系统
                requestHeader.setSupervisorID(taSysParam.getP_value().trim());
            } else if ("NotiType".equals(taSysParam.getP_code())) {
                // 通知处理类型
                requestBody.setNotiType(taSysParam.getP_value().trim());
            } else if ("RetuFlag".equals(taSysParam.getP_code())) {
                // 文件响应方式 I
                requestBody.setRetuFlag(taSysParam.getP_value().trim());
            } else if ("TxCode".equals(taSysParam.getP_code())) {
                // 交易码 通知：700105 查询：700106 撤销：700107
                requestBody.setTxCode(taSysParam.getP_value().trim());
            } else if ("SysFlag".equals(taSysParam.getP_code())) {
                // 应用系统标志
                requestBody.setSysFlag(taSysParam.getP_value().trim());
            } else if ("Flag".equals(taSysParam.getP_code())) {
                // 柜面外围标志 0：柜面 1：外围系统
                requestBody.setFlag(taSysParam.getP_value().trim());
            }
        }
    }

    /**
     * 获取文件序号
     *
     * @param path
     * @return
     */
    public static int getFileSeq(String path) {
        int j = 1;
        File[] files = new File(path).listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile() && files[i].getName().endsWith(".DAT")) {
                j++;
            }
        }
        return j;
    }

    /**
     * 格式化对象--表外
     *
     * @param asb
     */
    public static AcupFileBean creatBwBean(AcupSendBean acupSendBean) {
        AcupFileBean acupBw = new AcupFileBean();
        acupBw.setAmt(acupSendBean.getAmount());
        acupBw.setCcy(acupSendBean.getCcy());
        if (acupSendBean.getGlno().startsWith("83") && "D".equals(acupSendBean.getDrcrind())) {
            GenerateAcupDR(acupBw, acupSendBean.getIntglno(), acupSendBean.getSubbr());
            GenerateAcupCR(acupBw, "88", acupSendBean.getSubbr());
        } else if (acupSendBean.getGlno().startsWith("83") && "C".equals(acupSendBean.getDrcrind())) {
            GenerateAcupDR(acupBw, "88", acupSendBean.getSubbr());
            GenerateAcupCR(acupBw, acupSendBean.getIntglno(), acupSendBean.getSubbr());
        }
        return acupBw;
    }

    /**
     * 格式化对象--一借一贷
     *
     * @param list
     * @return
     */
    public static AcupFileBean creatJDBean(List<AcupSendBean> list) {
        AcupFileBean acupJD = new AcupFileBean();
        if (list.size() != 2) {
            System.out.println(list.get(0).getDealno() + "list中对象不等于2");
            return new AcupFileBean();// list中对象不等于2
        }
        if (!list.get(0).getCcy().equals(list.get(1).getCcy())
                || !list.get(0).getAmount().equals(list.get(1).getAmount())
                || list.get(0).getDrcrind().equals(list.get(1).getDrcrind())) {
            System.out.println(list.get(0).getDealno() + "list中对象 币种或金额不等 方向相等");
            return new AcupFileBean();//// list中对象 币种或金额不等 方向相等
        }
        for (AcupSendBean acupSendBean : list) {
            acupJD.setAmt(acupSendBean.getAmount());
            acupJD.setCcy(acupSendBean.getCcy());
            if ("D".equals(acupSendBean.getDrcrind())) {
                GenerateAcupDR(acupJD, acupSendBean.getIntglno(), acupSendBean.getSubbr());
            } else if ("C".equals(acupSendBean.getDrcrind())) {
                GenerateAcupCR(acupJD, acupSendBean.getIntglno(), acupSendBean.getSubbr());
            }
        }
        return acupJD;
    }

    /**
     * 格式化对象--一借多贷 一贷多借
     *
     * @param list
     * @return
     */
    public static List<AcupFileBean> creatJDMBean(List<AcupSendBean> list) {
        List<AcupFileBean> result = new ArrayList<AcupFileBean>();
        Map<String, List<AcupSendBean>> asbByDrcrindList = list.stream().collect(Collectors.groupingBy(AcupSendBean::getDrcrind));// 按方向分组
        BigDecimal amt = new BigDecimal("0");// 单方金额
        BigDecimal amtadd = new BigDecimal("0");// 多方金额和
        String drcrind = "";// 单方方向
        String intglno = "";// 单方科目
        String subbr = "";// 单方机构
        Set<Entry<String, List<AcupSendBean>>> entrySet = asbByDrcrindList.entrySet();
        for (Entry<String, List<AcupSendBean>> entry : entrySet) {
            List<AcupSendBean> asbList = entry.getValue();
            if (asbList.size() == 1) {
                amt = new BigDecimal(asbList.get(0).getAmount());
                intglno = asbList.get(0).getIntglno();
                subbr = asbList.get(0).getSubbr();
                drcrind = entry.getKey();
            } else if (asbList.size() > 1) {
                for (AcupSendBean asb : asbList) {
                    amtadd = amtadd.add(new BigDecimal(asb.getAmount()));
                }
            }
        }
        if (amt.compareTo(amtadd) != 0) {
            System.out.println(list.get(0).getDealno() + "借贷金额不等");
            return new ArrayList<AcupFileBean>();// 借贷金额不等
        }
        if ("".equals(drcrind) || "".equals(intglno) || "".equals(subbr)) {
            System.out.println(list.get(0).getDealno() + "非一借多贷 一代多借");
            return new ArrayList<AcupFileBean>();// 非一借多贷 一代多借
        }
        if ("D".equals(drcrind)) {// 单方为借DR 多方为贷CR
            List<AcupSendBean> asbList = asbByDrcrindList.get("C");
            for (AcupSendBean acupSendBean : asbList) {
                AcupFileBean acupJDM = new AcupFileBean();
                acupJDM.setCcy(acupSendBean.getCcy());
                acupJDM.setAmt(acupSendBean.getAmount());
                GenerateAcupCR(acupJDM, acupSendBean.getIntglno(), acupSendBean.getSubbr());
                GenerateAcupDR(acupJDM, intglno, subbr);
                result.add(acupJDM);
            }
        } else if ("C".equals(drcrind)) {// 单方为贷CR 多方为借DR
            List<AcupSendBean> asbList = asbByDrcrindList.get("D");
            for (AcupSendBean acupSendBean : asbList) {
                AcupFileBean acupJDM = new AcupFileBean();
                acupJDM.setCcy(acupSendBean.getCcy());
                acupJDM.setAmt(acupSendBean.getAmount());
                GenerateAcupDR(acupJDM, acupSendBean.getIntglno(), acupSendBean.getSubbr());
                GenerateAcupCR(acupJDM, intglno, subbr);
                result.add(acupJDM);
            }
        }
        return result;
    }

    /**
     * 格式化对象--多借多贷
     * 目前只支持俩借俩贷  且list账务金额符号相同
     *
     * @param list
     * @return
     */
    public static List<AcupFileBean> creatJMDMBean(List<AcupSendBean> list) {
        List<AcupFileBean> result = new ArrayList<AcupFileBean>();
        Map<String, List<AcupSendBean>> asbByDrcrindList = list.stream().collect(Collectors.groupingBy(AcupSendBean::getDrcrind));// 按方向分组
        List<AcupSendBean> absListDR = asbByDrcrindList.get("D");//借方账
        List<AcupSendBean> absListCR = asbByDrcrindList.get("C");//贷方账
        if (absListDR.size() != 2 || absListCR.size() != 2) {
            System.out.println(list.get(0).getDealno() + "非俩借俩贷");
            return new ArrayList<AcupFileBean>();// 非俩借俩贷
        }
        BigDecimal amtDR = new BigDecimal("0");//借方金额和
        BigDecimal amtCR = new BigDecimal("0");//贷方金额和
        SortBean(absListDR);//按金额降序排序
        SortBean(absListCR);//按金额降序排序
        for (AcupSendBean acupSendBean : absListDR) {
            amtDR = amtDR.add(new BigDecimal(acupSendBean.getAmount()));
        }
        for (AcupSendBean acupSendBean : absListCR) {
            amtCR = amtCR.add(new BigDecimal(acupSendBean.getAmount()));
        }
        if (amtCR.compareTo(amtDR) != 0) {
            System.out.println(list.get(0).getDealno() + "借贷金额不等");
            return new ArrayList<AcupFileBean>();// 借贷金额不等
        }

        AcupSendBean maxAsbDR = absListDR.get(0);
        AcupSendBean mixAsbDR = absListDR.get(1);
        AcupSendBean maxAsbCR = absListCR.get(0);
        AcupSendBean mixAsbCR = absListCR.get(1);

        BigDecimal maxDR = new BigDecimal(absListDR.get(0).getAmount()).abs();//借方最大值
        BigDecimal maxCR = new BigDecimal(absListCR.get(0).getAmount()).abs();//贷方最大值
        if (maxDR.compareTo(maxCR) > 0) { //maxDR 最大值大
            BigDecimal subNum = maxDR.subtract(maxCR);//相差值
            subNum = new BigDecimal(absListDR.get(0).getAmount()).compareTo(new BigDecimal("0")) > 0 ? subNum : subNum.negate(); //判断符号
            AcupFileBean acupFileBean = null;

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(maxAsbCR.getCcy());
            acupFileBean.setAmt(maxAsbCR.getAmount());
            GenerateAcupDR(acupFileBean, maxAsbDR.getIntglno(), maxAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, maxAsbCR.getIntglno(), maxAsbCR.getSubbr());
            result.add(acupFileBean);

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(mixAsbCR.getCcy());
            acupFileBean.setAmt(subNum.toString());
            GenerateAcupDR(acupFileBean, maxAsbDR.getIntglno(), maxAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, mixAsbCR.getIntglno(), mixAsbCR.getSubbr());
            result.add(acupFileBean);

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(mixAsbCR.getCcy());
            acupFileBean.setAmt(mixAsbDR.getAmount());
            GenerateAcupDR(acupFileBean, mixAsbDR.getIntglno(), mixAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, mixAsbCR.getIntglno(), mixAsbCR.getSubbr());
            result.add(acupFileBean);
        } else if (maxDR.compareTo(maxCR) < 0) { //maxCR 最大值大
            BigDecimal subNum = maxCR.subtract(maxDR);//相差值
            subNum = new BigDecimal(absListCR.get(0).getAmount()).compareTo(new BigDecimal("0")) > 0 ? subNum : subNum.negate(); //判断符号
            AcupFileBean acupFileBean = null;

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(maxAsbDR.getCcy());
            acupFileBean.setAmt(maxAsbDR.getAmount());
            GenerateAcupDR(acupFileBean, maxAsbDR.getIntglno(), maxAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, maxAsbCR.getIntglno(), maxAsbCR.getSubbr());
            result.add(acupFileBean);

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(maxAsbDR.getCcy());
            acupFileBean.setAmt(subNum.toString());
            GenerateAcupDR(acupFileBean, mixAsbDR.getIntglno(), mixAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, maxAsbCR.getIntglno(), maxAsbCR.getSubbr());
            result.add(acupFileBean);

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(mixAsbDR.getCcy());
            acupFileBean.setAmt(mixAsbCR.getAmount());
            GenerateAcupDR(acupFileBean, mixAsbDR.getIntglno(), mixAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, mixAsbCR.getIntglno(), mixAsbCR.getSubbr());
            result.add(acupFileBean);
        } else if (maxDR.compareTo(maxCR) == 0) {
            AcupFileBean acupFileBean = null;

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(maxAsbDR.getCcy());
            acupFileBean.setAmt(maxAsbDR.getAmount());
            GenerateAcupDR(acupFileBean, maxAsbDR.getIntglno(), maxAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, maxAsbCR.getIntglno(), maxAsbCR.getSubbr());
            result.add(acupFileBean);

            acupFileBean = new AcupFileBean();
            acupFileBean.setCcy(mixAsbDR.getCcy());
            acupFileBean.setAmt(mixAsbDR.getAmount());
            GenerateAcupDR(acupFileBean, mixAsbDR.getIntglno(), mixAsbDR.getSubbr());
            GenerateAcupCR(acupFileBean, mixAsbCR.getIntglno(), mixAsbCR.getSubbr());
            result.add(acupFileBean);
        }
        return result;
    }

    /**
     * 组装对象借方
     *
     * @param acupFileBean
     * @param intglno
     * @param subbr
     */
    public static void GenerateAcupDR(AcupFileBean acupFileBean, String intglno, String subbr) {
        int length = intglno.trim().length();
        if (length > 2) {// BGL
            acupFileBean.setOutOldAcount(intglno);
            acupFileBean.setOutType("BGL");
            acupFileBean.setOutGlno("");
            acupFileBean.setOutInstit("");
        } else if (length == 2) {// CGL
            acupFileBean.setOutOldAcount("");
            acupFileBean.setOutType("");
            acupFileBean.setDesc(StringUtil.isEmpty(acupFileBean.getDesc()) ? intglno : "");// 两个账号都为CGL 该字段为空
            acupFileBean.setOutGlno(intglno);
            acupFileBean.setOutInstit(subbr);
        }
    }

    /**
     * 组装对象贷方
     *
     * @param acupFileBean
     * @param intglno
     * @param subbr
     */
    public static void GenerateAcupCR(AcupFileBean acupFileBean, String intglno, String subbr) {
        int length = intglno.trim().length();
        if (length > 2) {// BGL
            acupFileBean.setInOldAcount(intglno);
            acupFileBean.setInType("BGL");
            acupFileBean.setInGlno("");
            acupFileBean.setInInstit("");
        } else if (length == 2) {
            acupFileBean.setInOldAcount("");
            acupFileBean.setInType("");
            acupFileBean.setDesc(StringUtil.isEmpty(acupFileBean.getDesc()) ? intglno : "");
            acupFileBean.setInGlno(intglno);
            acupFileBean.setInInstit(subbr);
        }
    }

    /**
     * 组装明细
     *
     * @param list     交易下账
     * @param seq      明细序号
     * @param seqno    交易号
     * @param postdate 交易时间
     * @return
     */
    public static String GenerateD(List<AcupFileBean> list, int seq, String dealno, String postdate) {
        StringBuffer strd = new StringBuffer();
        strd.append("D");// 明细记录标识 必须填D
        strd.append(StringUtil.stringFormat(String.valueOf(seq), "0", 12, "L"));// 顺序号 从1开始。
        strd.append("00100");// 机构号

        for (AcupFileBean acupFileBean : list) {
            strd.append(StringUtil.stringFormat("", "0", 17, "L"));// 转出新帐号
            strd.append(StringUtil.stringFormat(acupFileBean.getOutOldAcount().trim(), " ", 25, "R"));// 转出旧帐号
            strd.append(StringUtil.stringFormat("", " ", 19, "R"));// 转出卡号
            // 分为DEP BGL SET REM SRN CGL 对于冻结、解冻、销帐码还需要上送，其他转账操作不需要上送此信息 HOD: 圈存 FRE: 解圈
            strd.append(StringUtil.stringFormat(acupFileBean.getOutType().trim(), " ", 3, "R"));
            strd.append(StringUtil.stringFormat("", " ", 4, "R"));// 子账户类别号
            strd.append(StringUtil.stringFormat("", "0", 17, "L"));// 转入新帐号
            strd.append(StringUtil.stringFormat(acupFileBean.getInOldAcount().trim(), " ", 25, "R"));// 转入旧帐号
            strd.append(StringUtil.stringFormat("", " ", 19, "R"));// 转入卡号
            // 分为DEP BGL SET REM SRN CGL 对于冻结、解冻、销帐码还需要上送，其他转账操作不需要上送此信息 HOD: 圈存 FRE: 解圈
            strd.append(StringUtil.stringFormat(acupFileBean.getInType().trim(), " ", 3, "R"));
            strd.append(StringUtil.stringFormat("", " ", 4, "R"));// 子账户类别
            strd.append(StringUtil.stringFormat("", " ", 4, "R"));// 产品子类
            strd.append(StringUtil.stringFormat("", " ", 4, "R"));// 产品子类

            BigDecimal amt = new BigDecimal(acupFileBean.getAmt()).setScale(3);
            strd.append(StringUtil.stringFormat(amt.abs().toString().replace(".", ""), "0", 17, "L") + (amt.compareTo(new BigDecimal("0")) > 0 ? "+" : "-"));// 金额
            acupFileBean.setDesc(StringUtil.isEmpty(acupFileBean.getDesc()) ? "" : acupFileBean.getDesc());
            strd.append(StringUtil.stringFormat(acupFileBean.getDesc().trim(), " ", 2, "R"));// 提示码 冻结原因

            strd.append(StringUtil.stringFormat("", "0", 4, "L"));// 重空类型
            strd.append(StringUtil.stringFormat("", " ", 20, "R"));// 重空号码
            strd.append(StringUtil.stringFormat("", " ", 30, "R"));// 密码
            strd.append("0"); // 支取方式
            strd.append(StringUtil.stringFormat("", " ", 50, "R"));// 摘要
            strd.append(StringUtil.stringFormat(acupFileBean.getOutGlno().trim(), " ", 2, "R"));// 转出提示码
            strd.append(StringUtil.stringFormat(acupFileBean.getOutInstit().trim(), " ", 5, "R"));// 转出机构
            strd.append(StringUtil.stringFormat(acupFileBean.getInGlno().trim(), " ", 2, "R"));// 转入提示码
            strd.append(StringUtil.stringFormat(acupFileBean.getInInstit().trim(), " ", 5, "R"));// 转入机构
            strd.append(StringUtil.stringFormat(acupFileBean.getCcy().trim(), " ", 3, "R"));// 币种
            strd.append(" ");// 是否圈提标识 A:圈提（支持DEP-BGL的圈提）
        }
        strd.append(StringUtil.stringFormat("", " ", 287 * (6 - list.size()), "R"));// 6次循环剩余的

        strd.append(StringUtil.stringFormat("", "0", 9, "L"));// 流水号 解圈时输入核心流水号，表示圈存流水号
        strd.append(StringUtil.stringFormat("", "0", 8, "L"));// 原冻结日期/原圈存日期 当只有圈存时，作为“圈存到期日”使用；当“圈存+解圈”组合使用时，作为“原圈存日期”使用
        strd.append(" "); // 冲正标识 0 完全冲 1 忽略掉冻结（除去冻结，其他金额交易完全冲正）
        strd.append(" "); // 自动圈提标识 Y:是 N:否
        strd.append(StringUtil.stringFormat("", "0", 6, "L"));// 交易号
        strd.append(StringUtil.stringFormat("", "0", 9, "L"));// 流水号
        strd.append(postdate);// 交易日期
        strd.append(StringUtil.stringFormat("", " ", 4, "R"));// 返回信息码
        strd.append(StringUtil.stringFormat("", " ", 60, "R"));// 返回信息
        strd.append(StringUtil.stringFormat("", " ", 15, "R"));// FILLER
        strd.append(StringUtil.stringFormat(dealno.trim(), "#", 12, "L"));// 外围流水号 dealno
        strd.append(StringUtil.stringFormat("", " ", 20, "R"));// FILLER

        JY.info("[JOBS] ==> 开始执行交易状态同步【前置 --> 核心】，组装明细成功:dealno：" + dealno + "；strd：" + strd.toString());
        return strd.toString();
    }

    /**
     * 组装头
     *
     * @param requestHeader 报文请求头
     * @param acunt         明细笔数
     * @param postdate      账务时间
     * @param fileName      文件名
     * @return
     */
    public static String GenerateH(RequestHeader requestHeader, int acunt, String postdate, String fileName) {
        StringBuffer strh = new StringBuffer();
        strh.append("H");// 首记录标识
        strh.append("000000000000");// 顺序号 首记录的顺序号必须是000000000000
        strh.append("1");// 0: 使用系统默认的分行号/柜员号/终端号进行交易处理。 1: 使用明细记录中的机构号进行交易处理。
        strh.append(StringUtil.stringFormat(fileName.trim(), " ", 50, "R"));// 源文件名称 同上传的文件名称必须一致
        strh.append("00100");// 交易机构号
        strh.append("CNY");// 币种
        strh.append(StringUtil.stringFormat(String.valueOf(acunt), "0", 12, "L"));// 合计笔数 本文件的明细的汇总笔数
        strh.append(StringUtil.stringFormat("", "0", 17, "L") + "+");// 合计金额 本文件的明细的汇总金额
        strh.append(" ");// 渠道号
        strh.append(requestHeader.getTlrNo().trim());// 交易柜员号
        strh.append(requestHeader.getTrmNo().trim());// 交易终端号
        strh.append(StringUtil.stringFormat("", " ", 10, "R"));// 单位签约号 标识是哪个签约单位的数据
        strh.append("001");// 数据批次号 表示是处理的第几批数据，从001开始
        strh.append(postdate);// DDMMYYYY，预期的BANCS处理日期，如非本日记账日期，BANCS不予处理。
        strh.append("SAFM");// 文件处理类型
        strh.append(StringUtil.stringFormat("", "0", 12, "L"));// 成功笔数，主机返回填写，本文件的入帐成功明细的汇总笔数（返回时填）；上送时填零；BANCS处理后会把成功的笔数填在这里
        strh.append(StringUtil.stringFormat("", "0", 17, "L") + "+");// 成功金额，主机返回填写，本文件的入帐成功明细的汇总金额（返回时填）；上送时填零；BANCS处理后会把成功的金额填在这里
        strh.append(StringUtil.stringFormat("", "0", 12, "L"));// 失败笔数，主机返回填写，本文件的入帐失败明细的汇总笔数（返回时填）；上送时填零；BANCS处理后会把失败的笔数填在这里
        strh.append(StringUtil.stringFormat("", "0", 17, "L") + "+");// 失败金额，主机返回填写，本文件的入帐失败明细的汇总金额（返回时填）；上送时填零；BANCS处理后会失败的金额填在这里
        strh.append(StringUtil.stringFormat("", " ", 4, "L"));// 文件返回信息码，主机返回填写，上送时置空格；0－校验成功，其他－校验失败。如果文件校验失败，则不会进行任何帐务处理，而且合计笔数，合计金额，成功笔数，成功金额，失败笔数和失败金额都为0。（返回时填）；上送时填空格
        strh.append(StringUtil.stringFormat("", " ", 60, "R"));// 文件返回信息，主机返回填写，上送时置空格；成功处理/错误信息 （返回时填）；上送时填空格
        strh.append(requestHeader.getChnlNo());// 渠道号
        strh.append(StringUtil.stringFormat("", " ", 4, "R"));// 附言代码
        strh.append(StringUtil.stringFormat("", " ", 2, "R"));// 资金用途 01：现金业务 02：普通转账 03：消费和缴费支付 04：投资理财 05：贷款业务
        strh.append(StringUtil.stringFormat("", " ", 1617, "R"));
        strh.append(StringUtil.stringFormat("", " ", 5, "R"));
        return strh.toString();
    }

    /**
     * 对象list以金额排序 降序
     * 升序排的话就是第一个参数.compareTo(第二个参数);
     * 降序排的话就是第二个参数.compareTo(第一个参数);
     *
     * @param list
     */
    public static void SortBean(List<AcupSendBean> list) {
        Collections.sort(list, new Comparator<AcupSendBean>() {
            @Override
            public int compare(AcupSendBean o1, AcupSendBean o2) {
                return new BigDecimal(o2.getAmount()).abs().compareTo(new BigDecimal(o1.getAmount()).abs());
            }
        });
    }
}