package com.singlee.ifs.job;

import com.singlee.cap.accounting.model.InstructionPackage;
import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.duration.service.DurationConstants;
import com.singlee.hrbcap.service.Ifrs9BookkeepingEngineService;
import com.singlee.hrbcap.util.Ifrs9Constants;
import com.singlee.hrbcap.util.TradeConstants;
import com.singlee.ifs.mapper.IbFundPriceDayMapper;
import com.singlee.ifs.model.IbFundPriceDay;
import com.singlee.refund.mapper.CFtInfoMapper;
import com.singlee.refund.mapper.CFtTposMapper;
import com.singlee.refund.model.CFtInfo;
import com.singlee.refund.model.CFtTpos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基金日终估值
 */
public class FundDayEndManager implements CronRunnable {

    private static Logger accountLog = LoggerFactory.getLogger(FundDayEndManager.class);

    private CFtTposMapper ftTposMapper = SpringContextHolder.getBean(CFtTposMapper.class);
    private CFtInfoMapper ftInfoMapper = SpringContextHolder.getBean(CFtInfoMapper.class);

    private Ifrs9BookkeepingEngineService ifrs9BookkeepingEngineService = SpringContextHolder.getBean(Ifrs9BookkeepingEngineService.class);

    private IbFundPriceDayMapper ibFundPriceDayMapper = SpringContextHolder.getBean(IbFundPriceDayMapper.class);
    private DayendDateService dayendDateService = SpringContextHolder.getBean(DayendDateService.class);
    @Override
    public boolean execute(Map<String, Object> parameters) throws Exception {
//        parameters.put("postDate",dayendDateService.getDayendDate());
        parameters.put("postDate",ParameterUtil.getString(parameters, "postDate", ""));
        fundDailyValAccount(parameters);
        return true;
    }

    @Override
    public void terminate() {

    }

    /**
     * 基金跑批
     * @param params
     */
    public void fundDailyValAccount(Map<String, Object> params) {
        accountLog.info("******************开始进入基金每日估值账务************************");
        String postDate = ParameterUtil.getString(params, "postDate", "");//账务日期
        JY.require(StringUtil.isNotBlank(postDate), "请传账务日期postDate!");

        String vs_postdate = postDate;//当日
        String vs_last_postdate = DateUtil.getPreDateOfDays(vs_postdate,-1);//前一天

        accountLog.info("******************************"+"基金跑批开始，日期："+vs_postdate+"******************************");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("vs_postdate",vs_postdate);
        //1、查找[当日]基金持仓所持有的基金代码
        List<CFtTpos> holdFtTpos = ftTposMapper.getHandleFundCode(map);

        for(CFtTpos holdFtTpo:holdFtTpos){
            String fundCode = holdFtTpo.getFundCode();
            String sponInst = holdFtTpo.getSponInst();
            accountLog.info("******************************"+"基金"+fundCode+"，跑批日期："+vs_postdate+"******************************");
            //清理脏数据
            map.clear();
            map.put("vs_postdate",vs_postdate);

            map.put("sponinst",sponInst);
            map.put("last_postdate",vs_last_postdate);
            map.put("fundCode",fundCode);

            //2、查找单位净值历史表最大日期
            Map<String, Object> priceMap = new HashMap<String, Object>();
            priceMap.put("fundCode", fundCode);
            priceMap.put("versionId", Integer.valueOf(sponInst));
            String maxDate = ibFundPriceDayMapper.getMaxDateHis(priceMap);
            //历史表中不存在最大日期，代表新交易
            if(maxDate == null){
                map.put("postdate", vs_postdate);
                CFtTpos ftTpos = ftTposMapper.selectByMap(map);//查询今日持仓

                //首次申购日期
                String afpDate = ftTpos.getAfpDate();
                while (DateUtil.daysBetween(afpDate, vs_postdate)>=1){
                    map.put("postDate",afpDate);//T-1日
                    //查询首次申购日当日净值
                    IbFundPriceDay ibFundPriceDay = ibFundPriceDayMapper.getCurFtPrice(map);
                    if(ibFundPriceDay == null){
                        //该基金 date 净值为空 ，继续循环
                        afpDate = DateUtil.dateAdjust(afpDate,1);
                        continue;
                    }

                    map.put("price",ibFundPriceDay.getFundPrice());
                    map.put("KLFundType",ftTpos.getFundType());//出账时用到此维度：基金类型
                    calculateTpos(map);

                    //将净值表中 日期+基金代码 插入到历史表
                    map.put("versionId",Integer.valueOf(holdFtTpo.getSponInst()));
                    int version = ibFundPriceDayMapper.getFundPriceDayCount(map);

                    ibFundPriceDay.setVersionId(Integer.valueOf(holdFtTpo.getSponInst()));
                    accountLog.info("******************************基金，记录已计算净值信息入历史表"+"******************************");
                    if(version<=0){
                        ibFundPriceDayMapper.insertPriceHis(ibFundPriceDay);
                    }else{
                        ibFundPriceDayMapper.updatePriceHis(ibFundPriceDay);
                    }
                    afpDate = DateUtil.dateAdjust(afpDate,1);

                }


            }else {
                accountLog.info("******************************"+"基金"+fundCode+"，跑批计算日期："+maxDate+"--"+vs_postdate+"******************************");

                map.put("postdate", vs_postdate);//vs
                CFtTpos ftTpos = ftTposMapper.selectByMap(map);//查询今日持仓


                while(DateUtil.daysBetween(maxDate, vs_postdate)>=1){
                    //历史表中数据表示已计算，计算历史表---当前日期
                    String date = DateUtil.dateAdjust(maxDate,1);
                    map.put("postDate",date);//t-1
                    //单位净值表中日期+基金代码 获取价格
                    IbFundPriceDay ibFundPriceDay = ibFundPriceDayMapper.getCurFtPrice(map);
                    //单位净值表中日期+基金代码 获取基金持仓
                    if(ibFundPriceDay == null){
                        //该基金 date 净值为空 ，继续循环
                        maxDate = date;
                        continue;
                    }
                    map.put("price",ibFundPriceDay.getFundPrice());
                    map.put("KLFundType",ftTpos.getFundType());//出账时用到此维度：基金类型
                    calculateTpos(map);



                    //将净值表中 日期+基金代码 插入到历史表，并删除
                    map.put("versionId",Integer.valueOf(holdFtTpo.getSponInst()));
                    int count = ibFundPriceDayMapper.getFundPriceDayCount(map);
                    ibFundPriceDay.setVersionId(Integer.valueOf(holdFtTpo.getSponInst()));

                    if(count<=0){
                        ibFundPriceDayMapper.insertPriceHis(ibFundPriceDay);
                    }else{
                        ibFundPriceDayMapper.updateByPrimaryKeySelective(ibFundPriceDay);
                    }
                    maxDate = date;
                }

            }

        }

        accountLog.info("******************基金每日估值账务结束************************");

    }

    /**
     * 计算估值
     * @param param
     * param要素 ：      postdate:计算估值的日期 t
     *
     *                  price：postDate对应的单位净值/万份收益 t-1
     *
     *                  vs_postdate:当前账务日期 t
     *
     * 计算每日计提：
     * 普通型货币基金： 计提= 持仓总份额/10000 * 当前万分收益率
     * 净值型基金 ：        计提 = 持仓份额*当日净值-成本-累计计提
     */

    private void calculateTpos(Map<String,Object> param) {
        //计算估值的日期
        String priceDate = ParameterUtil.getString(param,"postDate",null);
        //计算估值日期对应的单位净值/万份收益
        BigDecimal fundPrice = (BigDecimal) param.get("price");
        //当前账务日期
        String vs_postdate =  ParameterUtil.getString(param,"vs_postdate",null);
        //基金代码
        String fundCode =  ParameterUtil.getString(param,"fundCode",null);
        //机构
        String sponinst =  ParameterUtil.getString(param,"sponinst",null);

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("fundCode",fundCode);
        map.put("sponinst",sponinst);
        map.put("postDate",priceDate);
        CFtTpos ftTpos = ftTposMapper.geCFtTposByContion(map);//T-1日
        accountLog.info("******************************获取"+priceDate+"，基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"持仓信息******************************"+ftTpos.toString());

        String newDate = DateUtil.dateAdjust(ftTpos.getPostdate(),1);
        map.put("postDate",newDate);
        CFtTpos newFtTpos = ftTposMapper.geCFtTposByContion(map);//T日
        accountLog.info("******************************获取"+newDate+"，基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"持仓信息******************************"+ftTpos.toString());
        //T日计算T-1日估值，故T日估值显示T-1日计提

        if(newFtTpos==null){
            accountLog.info("持仓不存在"+ftTpos.getFundCode()+","+ftTpos.getSponInst()+","+newDate);
            return;
        }

        //基金基本信息
        CFtInfo cFtInfo= ftInfoMapper.selectByPrimaryKey(fundCode);
        if(cFtInfo==null){
            accountLog.info("基金不存在"+cFtInfo.getFundCode());
            return;
        }

        //今日估值
        BigDecimal REVAL_AMT = BigDecimal.ZERO;

        //账务所需参数
        Map<String,Object> amtParam = new HashMap<String, Object>();

        if("803".equals(cFtInfo.getFundType())&&"0".equals(ftTpos.getInvtype())){
            /**
             * 专户基金 以摊余成本计量 不计 不做处理
             */
            accountLog.info("******************************"+priceDate+"，专户基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"以摊余成本计量 不做计提处理，******************************");
            return;
        }else{
            if("1".equals(cFtInfo.getNetWorthType())){
                /***
                 * 非净值型基金
                 * 万份收益 = 持仓总份额/10000 * 当前万份收益率
                 */
                REVAL_AMT = ftTpos.getUtQty().divide(new BigDecimal(10000),2, BigDecimal.ROUND_HALF_UP).multiply(fundPrice);

                amtParam.put(DurationConstants.AccType.LOAN_ASSMT,REVAL_AMT);//估值
                amtParam.put(DurationConstants.AccType.LOAN_ASSMT_LAST,ftTpos.getRevalAmt());//上一日估值

                //新累计总计提 = 累计总计提 + 当日计提
                BigDecimal tUnplAmt = ftTpos.gettUnplAmt()==null?BigDecimal.ZERO:ftTpos.gettUnplAmt();
                ftTpos.settUnplAmt(tUnplAmt.add(REVAL_AMT));
                //新未付收益 = 未付收益 + 当日计提
                BigDecimal yUnplAmt = ftTpos.getyUnplAmt() ==null?BigDecimal.ZERO:ftTpos.getyUnplAmt();
                ftTpos.setyUnplAmt(yUnplAmt.add(REVAL_AMT));
                accountLog.info("******************************"+priceDate+"，非净值型基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"当日估值,"+REVAL_AMT+"未付收益"+ftTpos.getyUnplAmt()+"，******************************");

            }else if("0".equals(cFtInfo.getNetWorthType())){
                /**
                 * 净值型基金
                 * 累计计提 = 持仓份额*当日净值-成本
                 * 当日计提 = 持仓份额*当日净值-成本-累计计提
                 */
                BigDecimal tUnplAmt = ftTpos.gettUnplAmt()==null?BigDecimal.ZERO:ftTpos.gettUnplAmt();
                //累计计提
                BigDecimal REVAL_AMT_HJ = ftTpos.getUtQty().multiply(fundPrice).setScale(2,BigDecimal.ROUND_HALF_UP).subtract(ftTpos.getAfpAmt());
                //当日计提
                REVAL_AMT = REVAL_AMT_HJ.subtract(tUnplAmt);

                amtParam.put(DurationConstants.AccType.LOAN_ASSMT,REVAL_AMT);//估值
                amtParam.put(DurationConstants.AccType.LOAN_ASSMT_LAST,ftTpos.getRevalAmt());//上一日估值

                //新累计总计提 = 当日计提
                ftTpos.settUnplAmt(REVAL_AMT_HJ);
                //新未付收益 = 未付收益 + 当日计提
                BigDecimal yUnplAmt = ftTpos.getyUnplAmt() ==null?BigDecimal.ZERO:ftTpos.getyUnplAmt();
                ftTpos.setyUnplAmt(yUnplAmt.add(REVAL_AMT));
                accountLog.info("******************************"+priceDate+"，净值型基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"当日估值,"+REVAL_AMT+"未付收益"+ftTpos.getyUnplAmt()+"，******************************");

            }else{

                accountLog.info("******************************"+priceDate+"，净值型基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"无法确定其计算方式，请检查持仓会计类型、债券净值类型，******************************");
                return;
            }
        }

        newFtTpos.setRevalAmt(REVAL_AMT);
        newFtTpos.setIsAssmt("1");//是否估值：1-已估值

        if(TradeConstants.ProductCode.CURRENCY_FUND.equals(ftTpos.getPrdNo())){//货币基金
            //净值类型
            param.put("priceType", cFtInfo.getNetWorthType());
        }

        param.put("tbAccountType",ftTpos.getInvtype());
        param.put("eventId", Ifrs9Constants.Ifrs9EventCode.Event_DINT);
        param.put("prdNo",ftTpos.getPrdNo());
        param.put("ccy",ftTpos.getCcy());
        generateAcup(param,amtParam);


        //更新T-1日（即估值日）信息：主要更新估值日未付收益、累计总计提
        ftTposMapper.updateByPrimaryKeySelective(ftTpos);
        //更新T日（即估值日+1）信息：主要更新估值日下一天的估值字段、是否已估值字段
        ftTposMapper.updateByPrimaryKeySelective(newFtTpos);

        Map<String,Object> tposParam = new HashMap<String, Object>();
        tposParam.put("prdNo",ftTpos.getPrdNo());
        tposParam.put("fundCode",ftTpos.getFundCode());
        tposParam.put("invtype",ftTpos.getInvtype());
        tposParam.put("sponinst",ftTpos.getSponInst());
        tposParam.put("postDate",ftTpos.getPostdate());

        /**
         * 计算红利再投，份额结转
         */
        if("1".equals(ftTpos.getBonusWay())){//红利再投

            param.put("fundCode",ftTpos.getFundCode());
            param.put("sponinst",ftTpos.getSponInst());

            if("3".equals(ftTpos.getCoverMode())){//每日
                //新累计已转投 = 累计已转投+累计未付收益
                ftTpos.setNcvAmt(ftTpos.getNcvAmt().add(ftTpos.getyUnplAmt()));
                //新成本 = 成本 + 累计未付收益
                ftTpos.setCost(ftTpos.getCost().add(ftTpos.getyUnplAmt()));
                BigDecimal utQty = BigDecimal.ZERO;
                if("1".equals(cFtInfo.getNetWorthType())){
                    //普通型货币基金 新持仓 = 持仓+累计未付收益/1
                    utQty = ftTpos.getyUnplAmt();
                    ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));

                }else  if("0".equals(cFtInfo.getNetWorthType())){
                    //净值型基金持仓 新持仓 = 持仓+累计未付收益/单位净值
                    utQty = ftTpos.getyUnplAmt().divide(fundPrice, 2, BigDecimal.ROUND_HALF_UP);
                    ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));
                }
                accountLog.info("******************************每日结转"+priceDate+"，普通型货币基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"，红利结转"+ftTpos.getyUnplAmt()+"，******************************");

                //红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓
                param.put("utQty",utQty);
                param.put("ncvAmt",ftTpos.getyUnplAmt());
                param.put("cost",ftTpos.getyUnplAmt());
                param.put("priceDate",ftTpos.getPostdate());
                param.put("T_UNPL_AMT",REVAL_AMT);
                param.put("YZ_UNPL_AMT",BigDecimal.ZERO);
                param.put("revalAmt",REVAL_AMT);
                accountLog.info("******************************红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓******************************");

                ftTposMapper.updateTopRdd(param);

                ftTposMapper.updateYUnplAmtZero(tposParam);

            }else if("4".equals(ftTpos.getCoverMode())){//  每月
                //2019-10-28
                String date = priceDate.substring(8,10);
                if(ftTpos.getCoverDay() == null || ftTpos.getCoverDay().length() < 10){
                    accountLog.info("******************************"+priceDate+"，净值型基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"转结日期格式不对，转结失败，******************************");
                    return;
                }
                String coverDay = ftTpos.getCoverDay().substring(8, 10);

                if(coverDay.equals(date)){
                    //新累计已转投 = 累计已转投+累计未付收益
                    ftTpos.setNcvAmt(ftTpos.getNcvAmt().add(ftTpos.getyUnplAmt()));
                    //新成本 = 成本 + 累计未付收益
                    ftTpos.setCost(ftTpos.getCost().add(ftTpos.getyUnplAmt()));

                    BigDecimal utQty = BigDecimal.ZERO;
                    if("1".equals(cFtInfo.getNetWorthType())){
                        //普通型货币基金 新持仓 = 持仓+累计未付收益/1
                        utQty = ftTpos.getyUnplAmt();
                        ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));
                    }else if("0".equals(cFtInfo.getNetWorthType())){
                        //净值型基金持仓 新持仓 = 持仓+累计未付收益/单位净值
                        utQty = ftTpos.getyUnplAmt().divide(fundPrice,2, BigDecimal.ROUND_HALF_UP);
                        ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));
                    }

                    //红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓
                    param.put("utQty",utQty);
                    param.put("ncvAmt",ftTpos.getyUnplAmt());
                    param.put("cost",ftTpos.getyUnplAmt());
                    param.put("priceDate",ftTpos.getPostdate());
                    param.put("revalAmt",REVAL_AMT);
                    param.put("T_UNPL_AMT",REVAL_AMT);
                    param.put("YZ_UNPL_AMT",BigDecimal.ZERO);
                    accountLog.info("******************************红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓******************************");
                    ftTposMapper.updateTopRdd(param);
                    accountLog.info("******************************每月结转"+priceDate+"，基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"，红利结转"+ftTpos.getyUnplAmt()+"，******************************");

                    ftTposMapper.updateYUnplAmtZero(tposParam);
                }else{
                    map.put("Y_UNPL_AMT",REVAL_AMT);
                    map.put("T_UNPL_AMT",REVAL_AMT);
                    map.put("priceDate",ftTpos.getPostdate());
                    map.put("vs_postdate", vs_postdate);
                    accountLog.info("******************************非结转日需要更新未付收益,累加"+REVAL_AMT+"******************************");
                    ftTposMapper.updateTopRdd(map);
                }

            }else if ("5".equals(ftTpos.getCoverMode())){// 每季
                String date = priceDate.substring(8,10);

                if(ftTpos.getCoverDay() == null || ftTpos.getCoverDay().length() < 10){
                    accountLog.info("******************************"+priceDate+"，净值型基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"转结日期格式不对，转结失败，******************************");
                    return;
                }
                String coverDay = ftTpos.getCoverDay().substring(8, 10);

                String[] array = {"03","06","09","12"};
                if(Arrays.binarySearch(array,"2")>0&&coverDay.equals(date)){
                    //新累计已转投 = 累计已转投+累计未付收益
                    ftTpos.setNcvAmt(ftTpos.getNcvAmt().add(ftTpos.getyUnplAmt()));
                    //新成本 = 成本 + 累计未付收益
                    ftTpos.setCost(ftTpos.getCost().add(ftTpos.getyUnplAmt()));

                    BigDecimal utQty = BigDecimal.ZERO;
                    if("1".equals(cFtInfo.getNetWorthType())){
                        //普通型货币基金 新持仓 = 持仓+累计未付收益/1
                        utQty = ftTpos.getyUnplAmt();
                        ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));
                    }else if("0".equals(cFtInfo.getNetWorthType())){
                        //净值型基金持仓 新持仓 = 持仓+累计未付收益/单位净值
                        utQty = ftTpos.getyUnplAmt().divide(fundPrice,2, BigDecimal.ROUND_HALF_UP);
                        ftTpos.setUtQty(ftTpos.getUtQty().add(utQty));
                    }
                    //红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓
                    param.put("utQty",utQty);
                    param.put("ncvAmt",ftTpos.getyUnplAmt());
                    param.put("cost",ftTpos.getyUnplAmt());
                    param.put("priceDate",ftTpos.getPostdate());
                    param.put("revalAmt",REVAL_AMT);
                    param.put("T_UNPL_AMT",REVAL_AMT);
                    param.put("YZ_UNPL_AMT",BigDecimal.ZERO);
                    accountLog.info("******************************红利转投完，除了更新当日，还需将转投更新到大于红利转投日，小于等于当日持仓******************************");
                    ftTposMapper.updateTopRdd(param);
                    accountLog.info("******************************每季结转"+priceDate+"，基金"+fundCode+"，所属机构"+ftTpos.getSponInst()+"，红利结转"+ftTpos.getyUnplAmt()+"，******************************");

                    ftTposMapper.updateYUnplAmtZero(tposParam);
                }
            }else{
                map.put("Y_UNPL_AMT",REVAL_AMT);
                map.put("T_UNPL_AMT",REVAL_AMT);
                map.put("priceDate",ftTpos.getPostdate());
                map.put("vs_postdate", vs_postdate);
                accountLog.info("******************************非结转日需要更新未付收益,累加"+REVAL_AMT+"******************************");

                ftTposMapper.updateTopRdd(map);
            }

        }else{
            /***
             * 累加未付收益、累计总计提
             * 累加日期范围：大于估值日期(T-1)，小于等于当前账务日期
             */
            map.put("Y_UNPL_AMT",REVAL_AMT);
            map.put("T_UNPL_AMT",REVAL_AMT);
            map.put("priceDate",ftTpos.getPostdate());
            map.put("vs_postdate", vs_postdate);
            accountLog.info("******************************非结转日需要更新未付收益,累加"+REVAL_AMT+"******************************");
            ftTposMapper.updateTopRdd(map);
        }


    }

    private void generateAcup(Map<String,Object> param,Map<String,Object> amtParam) {
        String postDate = ParameterUtil.getString(param,"postDate",null);
        JY.require(StringUtil.notNullOrEmpty(postDate), "缺少必要的参数postDate");
        postDate = DateUtil.dateAdjust(postDate,1);
        String fundCode = ParameterUtil.getString(param,"fundCode",null);
        //String eventId = ParameterUtil.getString(param, "eventId", "");
        accountLog.info("******************************"+fundCode+"基金开始出账，日期："+postDate+"******************************");
        param.put("postDate", postDate);
        param.put("dealNo", fundCode);
//        param.put("sponinst",ParameterUtil.getString(param,"sponinst",null));

        InstructionPackage instructionPackage = new InstructionPackage();
        instructionPackage.setInfoMap(param);
        instructionPackage.setAmtMap(amtParam);
        //edit by 2020-12-14  基金不出估值账务
        ifrs9BookkeepingEngineService.generateEntryIFRS9Entrance(instructionPackage, null);
        accountLog.info("******************************"+fundCode+"基金出账结束，日期："+postDate+"******************************");


    }
}
