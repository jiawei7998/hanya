package com.singlee.ifs.job;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.esb.hbcb.service.S002001010176013Service;
import com.singlee.financial.opics.IBaseServer;
import com.singlee.financial.opics.ISubjectEndOfDayCheckServer;
import com.singlee.ifs.model.IfsCoreaccount;
import com.singlee.ifs.model.IfsSubjectdetail;
import com.singlee.ifs.service.IfsCoreaccountService;
import com.singlee.ifs.service.IfsSubjectdetailService;

import java.math.BigDecimal;
import java.util.*;

/**
 * 更新科目余额、日终对账信息
 * @author copysun
 */
public class SubjectEndOfDayCheckJobManager implements CronRunnable {

	private TaSysParamMapper taSysParamMapper = SpringContextHolder.getBean(TaSysParamMapper.class);
	private ISubjectEndOfDayCheckServer iSubjectEndOfDayCheckServer = SpringContextHolder.getBean("ISubjectEndOfDayCheckServer");
	private IBaseServer baseServer = SpringContextHolder.getBean("IBaseServer");
	private IfsSubjectdetailService ifsSubjectdetailService = SpringContextHolder.getBean(IfsSubjectdetailService.class);
	private IfsCoreaccountService ifsCoreaccountService = SpringContextHolder.getBean(IfsCoreaccountService.class);
	private S002001010176013Service s002001010176013Service = SpringContextHolder.getBean(S002001010176013Service.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		// 获取当前账务日期
		JY.info("[JOBS-START] ==> 开始执行交易状态同步【前置 <-- 核心】，取回日终响应文件，开始······");
		Date postdates = baseServer.getOpicsSysDate("01");
		String strSendDate = ParameterUtil.getString(parameters, "coreDate", DateUtil.format(postdates));
		Date postdate = DateUtil.parse(strSendDate);
		JY.info("[JOBS] ==> 开始执行交易状态同步【前置 <-- 核心】，取回日终响应文件，总账日期：" + DateUtil.format(postdate, "yyyyMMdd") + "······");

		//取得opics acup科目发生额
		Map<String, Object> param1 = new HashMap<>(16);
		param1.put("postdate", postdate);
		List<SlAcupBean> opicsAcupList = iSubjectEndOfDayCheckServer.getList(param1);

		//从核心取回核心余额和核心发生额


		//从前置科目余额表取出对应科目余额
		Map<String, Object> param3 = new HashMap<>(16);
		param3.put("postdate", strSendDate);
		List<IfsSubjectdetail> ifsSubjectdetails = ifsSubjectdetailService.getListNoPage(param3);
		List<IfsSubjectdetail> updateIfsSubjectdetails = new ArrayList<>();
		//更新余额
		String finalStrSendDate = strSendDate;
		ifsSubjectdetails.forEach(e -> {
			e.setPostdate(finalStrSendDate);
			opicsAcupList.forEach(opicsAcup -> {
				if (e.getNewno().equals(opicsAcup.getIntglno())
						&& e.getCoreno().equals(opicsAcup.getSubject())
						&& e.getDrcr().equals(opicsAcup.getDrcrind())) {
					if (!e.getPostdate().equals(opicsAcup.getPostDate())) {
						e.setBalanceamt(e.getBalanceamt().add(new BigDecimal(opicsAcup.getAmount())));
						updateIfsSubjectdetails.add(e);
					}
				}
			});
		});

		//修改余额科目
		if (updateIfsSubjectdetails.size() > 0) {
			ifsSubjectdetailService.updateList(updateIfsSubjectdetails);
		}

		//将最新opics发送余额存到IFS_COREACCOUNT
		List<IfsCoreaccount> ifsCoreaccounts = new ArrayList<>();
		opicsAcupList.forEach(e -> {
			IfsCoreaccount ifsCoreaccount = new IfsCoreaccount();
			ifsCoreaccount.setNewno(e.getIntglno());
			ifsCoreaccount.setPostdate(e.getPostDate());
			ifsCoreaccount.setCoreno(e.getSubject());
			ifsCoreaccount.setDrcrind(e.getDrcrind());
			ifsCoreaccount.setBcoreamt(BigDecimal.ZERO);
			ifsCoreaccount.setBopicsamt(BigDecimal.ZERO);
			ifsCoreaccount.setBamt(BigDecimal.ZERO);
			ifsCoreaccount.setPcoreamt(BigDecimal.ZERO);
			ifsCoreaccount.setPopicsamt(new BigDecimal(e.getAmount()));
			ifsCoreaccount.setPamt(BigDecimal.ZERO);
			ifsCoreaccount.setDrcrname(e.getTdescr());
			//设置opics余额
			updateIfsSubjectdetails.forEach(ifsSubjectdetail -> {
				if (e.getIntglno().equals(ifsSubjectdetail.getNewno())
						&& e.getSubject().equals(ifsSubjectdetail.getCoreno())
						&& e.getDrcrind().equals(ifsSubjectdetail.getDrcr())) {
					ifsCoreaccount.setBopicsamt(ifsSubjectdetail.getBalanceamt());
				}
			});
			ifsCoreaccounts.add(ifsCoreaccount);

		});

		//先删除在添加
		ifsCoreaccountService.delByPostDate(strSendDate);
		if (ifsCoreaccounts.size() > 0) {
			ifsCoreaccountService.insertList(ifsCoreaccounts);
		}
		return true;
	}

	@Override
	public void terminate() {

	}

}
