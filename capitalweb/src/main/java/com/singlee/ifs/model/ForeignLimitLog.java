package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity	
@Table(name = "IFS_FOREIGN_LIMIT_LOG")
public class ForeignLimitLog implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * 外汇限额类型
	 */
	private String foreignLimitType;
	
	/**
	 * USD
	 */
	private String usd;
	
	/**
	 * EUR
	 */
	private String eur;
	
	/**
	 * JPY
	 */
	private String jpy;
	
	/**
	 * HKD
	 */
	private String hkd;
	
	/**
	 * GBP
	 */
	private String gbp;
	
	/**
	 * 总计
	 */
	private String total;
	
	/**
	 * 操作日期
	 */
	private String oprTime;
	
	/**
	 * 序列号
	 */
	private String limitId;
	
	/**
	 * 交易流水号
	 */
	private String ticketId;
	
	/**
	 * 备用1
	 */
	private String noUse1;
	
	/**
	 * 备用2
	 */
	private String noUse2;
	
	/**
	 * 备用3
	 */
	private String noUse3;

	public String getForeignLimitType() {
		return foreignLimitType;
	}

	public void setForeignLimitType(String foreignLimitType) {
		this.foreignLimitType = foreignLimitType;
	}

	public String getUsd() {
		return usd;
	}

	public void setUsd(String usd) {
		this.usd = usd;
	}

	public String getEur() {
		return eur;
	}

	public void setEur(String eur) {
		this.eur = eur;
	}

	public String getJpy() {
		return jpy;
	}

	public void setJpy(String jpy) {
		this.jpy = jpy;
	}

	public String getHkd() {
		return hkd;
	}

	public void setHkd(String hkd) {
		this.hkd = hkd;
	}

	public String getGbp() {
		return gbp;
	}

	public void setGbp(String gbp) {
		this.gbp = gbp;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getOprTime() {
		return oprTime;
	}

	public void setOprTime(String oprTime) {
		this.oprTime = oprTime;
	}

	public String getNoUse1() {
		return noUse1;
	}

	public void setNoUse1(String noUse1) {
		this.noUse1 = noUse1;
	}

	public String getNoUse2() {
		return noUse2;
	}

	public void setNoUse2(String noUse2) {
		this.noUse2 = noUse2;
	}

	public String getNoUse3() {
		return noUse3;
	}

	public void setNoUse3(String noUse3) {
		this.noUse3 = noUse3;
	}

	public String getLimitId() {
		return limitId;
	}

	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	
}