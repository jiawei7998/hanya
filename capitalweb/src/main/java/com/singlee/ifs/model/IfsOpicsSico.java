package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/***
 * opics 系统  SWIFT码
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_OPICS_SICO")
public class IfsOpicsSico implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**sic码*/
	private String sic;
	
	/**sic码英文详情*/
	private String sd;
	
	/**sic码中文详情*/
	private String sdcn;
	
	/**上次维护日期*/
	private Date lstmntdte;
	
	/**操作者*/
	private String operator;
	
	/**opics处理状态*/
	private String status;
	
	/**备注*/
	private String remark;

	public String getSic() {
		return sic;
	}

	public void setSic(String sic) {
		this.sic = sic;
	}

	public String getSd() {
		return sd;
	}

	public void setSd(String sd) {
		this.sd = sd;
	}

	public String getSdcn() {
		return sdcn;
	}

	public void setSdcn(String sdcn) {
		this.sdcn = sdcn;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
	

}
