/**
 * Project Name:capitalweb
 * File Name:IfsApproveBaseBean.java
 * Package Name:com.singlee.ifs.model
 * Date:2018年6月5日上午11:12:41
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/
/**
 * Project Name:capitalweb
 * File Name:IfsApproveBaseBean.java
 * Package Name:com.singlee.ifs.model
 * Date:2018年6月5日上午11:12:41
 * Copyright (c) 2018, singlee@singlee.com.cn All Rights Reserved.
 *
 */

package com.singlee.ifs.model;
/**
 * ClassName:IfsApproveBaseBean <br/>
 * Reason:	 事前交易基础类. <br/>
 * Date:     2018年6月5日 上午11:12:41 <br/>
 * @author   zhangcm
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class IfsApproveBaseBean   extends IfsBaseFlow{

			private static final long serialVersionUID = 1L;
			//交易单号(流水号系统自动生成)
			private String ticketId;
			//交易方向（本方）
			private String direction;
			//交易对手
			private String counterpartyInstId;
			//报送日期
			private String postDate;
			//交易状态
			private String dealTransType;
			
			public String getTicketId() {
				return ticketId;
			}
			public void setTicketId(String ticketId) {
				this.ticketId = ticketId;
			}
			public String getDirection() {
				return direction;
			}
			public void setDirection(String direction) {
				this.direction = direction;
			}
			public String getCounterpartyInstId() {
				return counterpartyInstId;
			}
			public void setCounterpartyInstId(String counterpartyInstId) {
				this.counterpartyInstId = counterpartyInstId;
			}
			public String getPostDate() {
				return postDate;
			}
			public void setPostDate(String postDate) {
				this.postDate = postDate;
			}
			public String getDealTransType() {
				return dealTransType;
			}
			public void setDealTransType(String dealTransType) {
				this.dealTransType = dealTransType;
			}
			
			
}

