package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangkai
 * @Description 
 * @Date 
 */
/**
    * 同业存放(外币)
    */
@Entity
@Table(name = "IFS_FX_DEPOSITIN")
public class IfsFxDepositIn extends FxCurrenyBasebean implements Serializable {

    private static final long serialVersionUID = -145126415816359573L;

    //基础信息
    /**
    * 交易状态
    */
    @Id
    private String dealTransType;

    /**
    * 交易单号
    */
    @Id
    private String ticketId;

    /**
    * 成交单号
    */
    private String contractId;

    /**
    * 审批状态
    */
    private String approveStatus;

    /**
    * 审批发起人
    */
    private String sponsor;

    /**
    * 审批发起机构
    */
    private String sponInst;

    /**
    * 审批开始时间
    */
    private String aDate;

    //交易对手信息
    /**
    * 客户号
    */
    private String custId;

    /**
     * 客户号
     */
    private String custName;

    /**
     * 客户号
     */
    private String ctype;

    /**
     * 客户号
     */
    private String cliType;

    //业务信息
    /**
    * 成交日期
    */
    private String forDate;

    /**
    * 协议开始日期
    */
    private String firstSettlementDate;

    /**
    * 协议到期日期
    */
    private String secondSettlementDate;

    /**
    * 币种
    */
    private String currency;

    /**
    * 本金(元)
    */
    private BigDecimal amt;

    /**
    * 利率类型
    */
    private String rateType;

    /**
    * 基准利率代码
    */
    private String basisRateCode;

    /**
    * 利率点差(千分之一)
    */
    private BigDecimal benchmarkSpread;

    /**
    * 利率(%)
    */
    private BigDecimal rate;

    /**
     * 利率(%)
     */
    private String basis;

    /**
    * 存期
    */
    private String tenor;

    /**
    * 提前支取利率(%)
    */
    private BigDecimal punishRate;

    /**
    * 付息频率
    */
    private String paymentFrequency;

    /**
    * 客户经理
    */
    private String trad;

    /**
    * 支取方式
    */
    private String payIncomWay;

    /**
    * 自动转存标志
    */
    private String autoTransInd;

    /**
    * 业务归属部门
    */
    private String bussInstId;

    /**
    * 备注
    */
    private String note;

    //业务清算信息
    /**
    * 付款账号
    */
    private String selfAcccode;

    /**
    * 付款账户名称
    */
    private String selfAccname;

    /**
    * 付款开户行大额行号
    */
    private String selfBankcode;

    /**
    * 付款开户行名称
    */
    private String selfBankname;

    /**
    * 收款账号
    */
    private String partyAcccode;

    /**
    * 收款账户名称
    */
    private String partyAccname;

    /**
    * 收款开户行大额行号
    */
    private String partyBankcode;

    /**
    * 收款开户行名称
    */
    private String partyBankname;

    /**
     * 五级分类
     */
    private String fiveLevelClass;

    /**
     * 归属部门
     */
    private String attributionDept;

    /**
     * 同业存款余额
     * @return
     */
    private BigDecimal depositBalance;

    /**
     * 偿还状态
     * @return
     */
    private String repayStatus;

    //前置产品编号
    private String fPrdCode;

    /**
     * 账户性质（1：投融资性，2:结算性）
     */
    private String accountNature;

    /**
     * 账户用途
     */
    private String accountUse;

    /**
     * 定价基准类型: 1-Shibor,2-Libor,3-Hibor,4-Euribor,5-LPR,
     * 6-CHN Treasury Rate,7-存款基准利率,8-贷款基准利率,9-再贷款利率,10-其他
     */
    private String pricingStandardType;

    /**
     * 利率浮动频率: 1-按日,2-按周,3-按月,4-按季,5-按半年，6-按年，7-其他
     */
    private String rateFloatFrequency;

    /**
     * 存款账号
     */
    private String depositAccount;

    /**
     * 付息类型
     */
    private String scheduleType;

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getDepositAccount() {
        return depositAccount;
    }

    public void setDepositAccount(String depositAccount) {
        this.depositAccount = depositAccount;
    }

    public String getPricingStandardType() {
        return pricingStandardType;
    }

    public void setPricingStandardType(String pricingStandardType) {
        this.pricingStandardType = pricingStandardType;
    }

    public String getRateFloatFrequency() {
        return rateFloatFrequency;
    }

    public void setRateFloatFrequency(String rateFloatFrequency) {
        this.rateFloatFrequency = rateFloatFrequency;
    }

    public String getAccountNature() {
        return accountNature;
    }

    public void setAccountNature(String accountNature) {
        this.accountNature = accountNature;
    }

    public String getAccountUse() {
        return accountUse;
    }

    public void setAccountUse(String accountUse) {
        this.accountUse = accountUse;
    }

    public String getfPrdCode() {
        return fPrdCode;
    }

    public void setfPrdCode(String fPrdCode) {
        this.fPrdCode = fPrdCode;
    }


    public String getFiveLevelClass() {
        return fiveLevelClass;
    }

    public void setFiveLevelClass(String fiveLevelClass) {
        this.fiveLevelClass = fiveLevelClass;
    }

    public String getAttributionDept() {
        return attributionDept;
    }

    public void setAttributionDept(String attributionDept) {
        this.attributionDept = attributionDept;
    }

    public String getDealTransType() {
        return dealTransType;
    }

    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getSponInst() {
        return sponInst;
    }

    public void setSponInst(String sponInst) {
        this.sponInst = sponInst;
    }

    public String getaDate() {
        return aDate;
    }

    public void setaDate(String aDate) {
        this.aDate = aDate;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getForDate() {
        return forDate;
    }

    public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    public String getFirstSettlementDate() {
        return firstSettlementDate;
    }

    public void setFirstSettlementDate(String firstSettlementDate) {
        this.firstSettlementDate = firstSettlementDate;
    }

    public String getSecondSettlementDate() {
        return secondSettlementDate;
    }

    public void setSecondSettlementDate(String secondSettlementDate) {
        this.secondSettlementDate = secondSettlementDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getBasisRateCode() {
        return basisRateCode;
    }

    public void setBasisRateCode(String basisRateCode) {
        this.basisRateCode = basisRateCode;
    }

    public BigDecimal getBenchmarkSpread() {
        return benchmarkSpread;
    }

    public void setBenchmarkSpread(BigDecimal benchmarkSpread) {
        this.benchmarkSpread = benchmarkSpread;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getPunishRate() {
        return punishRate;
    }

    public void setPunishRate(BigDecimal punishRate) {
        this.punishRate = punishRate;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public String getTrad() {
        return trad;
    }

    public void setTrad(String trad) {
        this.trad = trad;
    }

    public String getPayIncomWay() {
        return payIncomWay;
    }

    public void setPayIncomWay(String payIncomWay) {
        this.payIncomWay = payIncomWay;
    }

    public String getAutoTransInd() {
        return autoTransInd;
    }

    public void setAutoTransInd(String autoTransInd) {
        this.autoTransInd = autoTransInd;
    }

    public String getBussInstId() {
        return bussInstId;
    }

    public void setBussInstId(String bussInstId) {
        this.bussInstId = bussInstId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSelfAcccode() {
        return selfAcccode;
    }

    public void setSelfAcccode(String selfAcccode) {
        this.selfAcccode = selfAcccode;
    }

    public String getSelfAccname() {
        return selfAccname;
    }

    public void setSelfAccname(String selfAccname) {
        this.selfAccname = selfAccname;
    }

    public String getSelfBankcode() {
        return selfBankcode;
    }

    public void setSelfBankcode(String selfBankcode) {
        this.selfBankcode = selfBankcode;
    }

    public String getSelfBankname() {
        return selfBankname;
    }

    public void setSelfBankname(String selfBankname) {
        this.selfBankname = selfBankname;
    }

    public String getPartyAcccode() {
        return partyAcccode;
    }

    public void setPartyAcccode(String partyAcccode) {
        this.partyAcccode = partyAcccode;
    }

    public String getPartyAccname() {
        return partyAccname;
    }

    public void setPartyAccname(String partyAccname) {
        this.partyAccname = partyAccname;
    }

    public String getPartyBankcode() {
        return partyBankcode;
    }

    public void setPartyBankcode(String partyBankcode) {
        this.partyBankcode = partyBankcode;
    }

    public String getPartyBankname() {
        return partyBankname;
    }

    public void setPartyBankname(String partyBankname) {
        this.partyBankname = partyBankname;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getCliType() {
        return cliType;
    }

    public void setCliType(String cliType) {
        this.cliType = cliType;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
    }

    public String getRepayStatus() {
        return repayStatus;
    }

    public void setRepayStatus(String repayStatus) {
        this.repayStatus = repayStatus;
    }
}