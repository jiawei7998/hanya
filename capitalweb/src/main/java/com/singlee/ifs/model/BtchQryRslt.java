package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 批量结算合同返回的JAVA BEAN （这里主要需要存储并展示，所以需要虚拟成JAVA BEAN）
 * @author Administrator
 *
 */
public class BtchQryRslt implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String InstrId;//结算指令标识
	private String CtrctId;//结算合同标识
	private String GivAcctNm;//付券方账户名称
	private String GivAcctId;//付券方账户id
	private String TakAcctNm;//收券方账户名称
	private String TakAcctId;//收券方账户id
	private String TxId;//业务标识号
	private String BizTp;//业务类型
	private String SttlmTp1;//结算方式1
	private String SttlmTp2;//结算方式2
	private int BdCnt;//债券数目
	private BigDecimal AggtFaceAmt; //债券总额
	private BigDecimal Val1;//结算金额1
	private BigDecimal Val2;//结算金额2
	private String Dt1;//交割日1
	private String Dt2;//交割日2
	private String InstrSts;//指令处理状态
	private String CtrctSts;//合同处理状态
	private String CtrctBlckSts;//合同冻结状态
	private String LastUpdTm;//最新更新时间
	private String OrgtrCnfrmInd;//发令方确认标识
	private String CtrCnfrmInd;//对手方确认标识
	private String InstrOrgn;//指令来源
	
	
	/***************************************************/
	  private String Stdt;//清算时间
	  private String IOper;//经办人员
	  private Date ITime;//经办时间
	  private String VOper;//复核人员
	  private Date VTime;//复核时间
	  private String ADealWith;//报文发送处理标识
	  private Date ADTime;//处理时间
	  private String RIOper;//撤销经办人
	  private Date RITime;//撤销时间
	  private String RVOper;//撤销复核人员
	  private Date RVTime;//撤销复核时间
	  private String RDealWith;//撤销报文处理标识
	  private Date RDTime;//撤销处理时间
	  
	  private String CSBS001;
	  
	  private String CSBS003;
	/**
	 * @return the instrId
	 */
	public String getInstrId() {
		return InstrId;
	}
	/**
	 * @param instrId the instrId to set
	 */
	public void setInstrId(String instrId) {
		InstrId = instrId;
	}
	/**
	 * @return the ctrctId
	 */
	public String getCtrctId() {
		return CtrctId;
	}
	/**
	 * @param ctrctId the ctrctId to set
	 */
	public void setCtrctId(String ctrctId) {
		CtrctId = ctrctId;
	}
	/**
	 * @return the givAcctNm
	 */
	public String getGivAcctNm() {
		return GivAcctNm;
	}
	/**
	 * @param givAcctNm the givAcctNm to set
	 */
	public void setGivAcctNm(String givAcctNm) {
		GivAcctNm = givAcctNm;
	}
	/**
	 * @return the givAcctId
	 */
	public String getGivAcctId() {
		return GivAcctId;
	}
	/**
	 * @param givAcctId the givAcctId to set
	 */
	public void setGivAcctId(String givAcctId) {
		GivAcctId = givAcctId;
	}
	/**
	 * @return the takAcctNm
	 */
	public String getTakAcctNm() {
		return TakAcctNm;
	}
	/**
	 * @param takAcctNm the takAcctNm to set
	 */
	public void setTakAcctNm(String takAcctNm) {
		TakAcctNm = takAcctNm;
	}
	/**
	 * @return the takAcctId
	 */
	public String getTakAcctId() {
		return TakAcctId;
	}
	/**
	 * @param takAcctId the takAcctId to set
	 */
	public void setTakAcctId(String takAcctId) {
		TakAcctId = takAcctId;
	}
	/**
	 * @return the txId
	 */
	public String getTxId() {
		return TxId;
	}
	/**
	 * @param txId the txId to set
	 */
	public void setTxId(String txId) {
		TxId = txId;
	}
	/**
	 * @return the bizTp
	 */
	public String getBizTp() {
		return BizTp;
	}
	/**
	 * @param bizTp the bizTp to set
	 */
	public void setBizTp(String bizTp) {
		BizTp = bizTp;
	}
	/**
	 * @return the sttlmTp1
	 */
	public String getSttlmTp1() {
		return SttlmTp1;
	}
	/**
	 * @param sttlmTp1 the sttlmTp1 to set
	 */
	public void setSttlmTp1(String sttlmTp1) {
		SttlmTp1 = sttlmTp1;
	}
	/**
	 * @return the sttlmTp2
	 */
	public String getSttlmTp2() {
		return SttlmTp2;
	}
	/**
	 * @param sttlmTp2 the sttlmTp2 to set
	 */
	public void setSttlmTp2(String sttlmTp2) {
		SttlmTp2 = sttlmTp2;
	}
	/**
	 * @return the bdCnt
	 */
	public int getBdCnt() {
		return BdCnt;
	}
	/**
	 * @param bdCnt the bdCnt to set
	 */
	public void setBdCnt(int bdCnt) {
		BdCnt = bdCnt;
	}
	/**
	 * @return the aggtFaceAmt
	 */
	public BigDecimal getAggtFaceAmt() {
		return AggtFaceAmt;
	}
	/**
	 * @param aggtFaceAmt the aggtFaceAmt to set
	 */
	public void setAggtFaceAmt(BigDecimal aggtFaceAmt) {
		AggtFaceAmt = aggtFaceAmt;
	}
	/**
	 * @return the val1
	 */
	public BigDecimal getVal1() {
		return Val1;
	}
	/**
	 * @param val1 the val1 to set
	 */
	public void setVal1(BigDecimal val1) {
		Val1 = val1;
	}
	/**
	 * @return the val2
	 */
	public BigDecimal getVal2() {
		return Val2;
	}
	/**
	 * @param val2 the val2 to set
	 */
	public void setVal2(BigDecimal val2) {
		Val2 = val2;
	}
	/**
	 * @return the dt1
	 */
	public String getDt1() {
		return Dt1;
	}
	/**
	 * @param dt1 the dt1 to set
	 */
	public void setDt1(String dt1) {
		Dt1 = dt1;
	}
	/**
	 * @return the dt2
	 */
	public String getDt2() {
		return Dt2;
	}
	/**
	 * @param dt2 the dt2 to set
	 */
	public void setDt2(String dt2) {
		Dt2 = dt2;
	}
	/**
	 * @return the instrSts
	 */
	public String getInstrSts() {
		return InstrSts;
	}
	/**
	 * @param instrSts the instrSts to set
	 */
	public void setInstrSts(String instrSts) {
		InstrSts = instrSts;
	}
	/**
	 * @return the ctrctSts
	 */
	public String getCtrctSts() {
		return CtrctSts;
	}
	/**
	 * @param ctrctSts the ctrctSts to set
	 */
	public void setCtrctSts(String ctrctSts) {
		CtrctSts = ctrctSts;
	}
	/**
	 * @return the ctrctBlckSts
	 */
	public String getCtrctBlckSts() {
		return CtrctBlckSts;
	}
	/**
	 * @param ctrctBlckSts the ctrctBlckSts to set
	 */
	public void setCtrctBlckSts(String ctrctBlckSts) {
		CtrctBlckSts = ctrctBlckSts;
	}
	/**
	 * @return the lastUpdTm
	 */
	public String getLastUpdTm() {
		return LastUpdTm;
	}
	/**
	 * @param lastUpdTm the lastUpdTm to set
	 */
	public void setLastUpdTm(String lastUpdTm) {
		LastUpdTm = lastUpdTm;
	}
	/**
	 * @return the orgtrCnfrmInd
	 */
	public String getOrgtrCnfrmInd() {
		return OrgtrCnfrmInd;
	}
	/**
	 * @param orgtrCnfrmInd the orgtrCnfrmInd to set
	 */
	public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
		OrgtrCnfrmInd = orgtrCnfrmInd;
	}
	/**
	 * @return the ctrCnfrmInd
	 */
	public String getCtrCnfrmInd() {
		return CtrCnfrmInd;
	}
	/**
	 * @param ctrCnfrmInd the ctrCnfrmInd to set
	 */
	public void setCtrCnfrmInd(String ctrCnfrmInd) {
		CtrCnfrmInd = ctrCnfrmInd;
	}
	/**
	 * @return the instrOrgn
	 */
	public String getInstrOrgn() {
		return InstrOrgn;
	}
	/**
	 * @param instrOrgn the instrOrgn to set
	 */
	public void setInstrOrgn(String instrOrgn) {
		InstrOrgn = instrOrgn;
	}
	/**
	 * @return the stdt
	 */
	public String getStdt() {
		return Stdt;
	}
	/**
	 * @param stdt the stdt to set
	 */
	public void setStdt(String stdt) {
		Stdt = stdt;
	}
	/**
	 * @return the iOper
	 */
	public String getIOper() {
		return IOper;
	}
	/**
	 * @param oper the iOper to set
	 */
	public void setIOper(String oper) {
		IOper = oper;
	}
	/**
	 * @return the iTime
	 */
	public Date getITime() {
		return ITime;
	}
	/**
	 * @param time the iTime to set
	 */
	public void setITime(Date time) {
		ITime = time;
	}
	/**
	 * @return the vOper
	 */
	public String getVOper() {
		return VOper;
	}
	/**
	 * @param oper the vOper to set
	 */
	public void setVOper(String oper) {
		VOper = oper;
	}
	/**
	 * @return the vTime
	 */
	public Date getVTime() {
		return VTime;
	}
	/**
	 * @param time the vTime to set
	 */
	public void setVTime(Date time) {
		VTime = time;
	}
	/**
	 * @return the aDealWith
	 */
	public String getADealWith() {
		return ADealWith;
	}
	/**
	 * @param dealWith the aDealWith to set
	 */
	public void setADealWith(String dealWith) {
		ADealWith = dealWith;
	}
	/**
	 * @return the aDTime
	 */
	public Date getADTime() {
		return ADTime;
	}
	/**
	 * @param time the aDTime to set
	 */
	public void setADTime(Date time) {
		ADTime = time;
	}
	/**
	 * @return the rIOper
	 */
	public String getRIOper() {
		return RIOper;
	}
	/**
	 * @param oper the rIOper to set
	 */
	public void setRIOper(String oper) {
		RIOper = oper;
	}
	/**
	 * @return the rITime
	 */
	public Date getRITime() {
		return RITime;
	}
	/**
	 * @param time the rITime to set
	 */
	public void setRITime(Date time) {
		RITime = time;
	}
	/**
	 * @return the rVOper
	 */
	public String getRVOper() {
		return RVOper;
	}
	/**
	 * @param oper the rVOper to set
	 */
	public void setRVOper(String oper) {
		RVOper = oper;
	}
	/**
	 * @return the rVTime
	 */
	public Date getRVTime() {
		return RVTime;
	}
	/**
	 * @param time the rVTime to set
	 */
	public void setRVTime(Date time) {
		RVTime = time;
	}
	/**
	 * @return the rDealWith
	 */
	public String getRDealWith() {
		return RDealWith;
	}
	/**
	 * @param dealWith the rDealWith to set
	 */
	public void setRDealWith(String dealWith) {
		RDealWith = dealWith;
	}
	/**
	 * @return the rDTime
	 */
	public Date getRDTime() {
		return RDTime;
	}
	/**
	 * @param time the rDTime to set
	 */
	public void setRDTime(Date time) {
		RDTime = time;
	}
	/**
	 * @return the cSBS001
	 */
	public String getCSBS001() {
		return CSBS001;
	}
	/**
	 * @param csbs001 the cSBS001 to set
	 */
	public void setCSBS001(String csbs001) {
		CSBS001 = csbs001;
	}
	/**
	 * @return the cSBS003
	 */
	public String getCSBS003() {
		return CSBS003;
	}
	/**
	 * @param csbs003 the cSBS003 to set
	 */
	public void setCSBS003(String csbs003) {
		CSBS003 = csbs003;
	}

}
