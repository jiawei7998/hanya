package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "IFS_SDVP_HEAD")
public class IfsSdvpHead implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 42701870053101784L;
	private String br;
	private String server;
	private String fedealno;
	private String inoutind;
	private String seq;
	private String product;
	private String prodtype;
	private String cno;
	private String ccy;
	private String recordtype;
	private String delrecind;
	private String settmeans;
	private String settacct;
	private String usualid;
	private Date effdate;
	private String c1;
	private String c2;
	private String c3;
	private String c4;
	private String c5;
	private String c6;
	private String ac1;
	private String ac2;
	private String ac3;
	private String ac4;
	private String ac5;
	private String ac6;
	private String addupddel;
	private String supconfind;
	private String supsecind;
	private String supccyind;
	private String authind;
	private String swiftbothind;
	private String safekeepacct;
	private String slioper;
	private String slflag;
	private String slaction;
	private Date slidate;
	private String veroper;
	private Date verdate;
	//同步状态，0：未同步，1：已同步
	private String status;
	@Transient
	private List<IfsSdvpDetail> sdvpDetailList;
	//opics的statcode
	private String statcode;
	//opics错误码
	private String errorcode;
	//产品类型对应业务中文名称
	@Transient
	private String descrcn;
	//客户中文名称
	@Transient
	private String cliname;
	

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public String getC1() {
		return c1;
	}

	public void setC1(String c1) {
		this.c1 = c1;
	}

	public String getC2() {
		return c2;
	}

	public void setC2(String c2) {
		this.c2 = c2;
	}

	public String getC3() {
		return c3;
	}

	public void setC3(String c3) {
		this.c3 = c3;
	}

	public String getC4() {
		return c4;
	}

	public void setC4(String c4) {
		this.c4 = c4;
	}

	public String getC5() {
		return c5;
	}

	public void setC5(String c5) {
		this.c5 = c5;
	}

	public String getC6() {
		return c6;
	}

	public void setC6(String c6) {
		this.c6 = c6;
	}

	public String getAc1() {
		return ac1;
	}

	public void setAc1(String ac1) {
		this.ac1 = ac1;
	}

	public String getAc2() {
		return ac2;
	}

	public void setAc2(String ac2) {
		this.ac2 = ac2;
	}

	public String getAc3() {
		return ac3;
	}

	public void setAc3(String ac3) {
		this.ac3 = ac3;
	}

	public String getAc4() {
		return ac4;
	}

	public void setAc4(String ac4) {
		this.ac4 = ac4;
	}

	public String getAc5() {
		return ac5;
	}

	public void setAc5(String ac5) {
		this.ac5 = ac5;
	}

	public String getAc6() {
		return ac6;
	}

	public void setAc6(String ac6) {
		this.ac6 = ac6;
	}

	public String getAddupddel() {
		return addupddel;
	}

	public void setAddupddel(String addupddel) {
		this.addupddel = addupddel;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getSupsecind() {
		return supsecind;
	}

	public void setSupsecind(String supsecind) {
		this.supsecind = supsecind;
	}

	public String getSupccyind() {
		return supccyind;
	}

	public void setSupccyind(String supccyind) {
		this.supccyind = supccyind;
	}

	public String getAuthind() {
		return authind;
	}

	public void setAuthind(String authind) {
		this.authind = authind;
	}

	public String getSwiftbothind() {
		return swiftbothind;
	}

	public void setSwiftbothind(String swiftbothind) {
		this.swiftbothind = swiftbothind;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public String getSlioper() {
		return slioper;
	}

	public void setSlioper(String slioper) {
		this.slioper = slioper;
	}

	public String getSlflag() {
		return slflag;
	}

	public void setSlflag(String slflag) {
		this.slflag = slflag;
	}

	public String getSlaction() {
		return slaction;
	}

	public void setSlaction(String slaction) {
		this.slaction = slaction;
	}

	public Date getSlidate() {
		return slidate;
	}

	public void setSlidate(Date slidate) {
		this.slidate = slidate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}

	public String getVeroper() {
		return veroper;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setSdvpDetailList(List<IfsSdvpDetail> sdvpDetailList) {
		this.sdvpDetailList = sdvpDetailList;
	}

	public List<IfsSdvpDetail> getSdvpDetailList() {
		return sdvpDetailList;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setDescrcn(String descrcn) {
		this.descrcn = descrcn;
	}

	public String getDescrcn() {
		return descrcn;
	}

	public void setCliname(String cliname) {
		this.cliname = cliname;
	}

	public String getCliname() {
		return cliname;
	}

}
