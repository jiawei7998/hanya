package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/***
 * opics 清算账户表
 * @author lij
 *
 */
@Entity
@Table(name="IFS_OPICS_NOST")
public class IfsOpicsNost implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
     * 分支机构
     */
    private String br;

    /**
     * 清算账户
     */
    private String nos;
    /**
     * 币种
     */
    private String ccy;

    /**
     * 成本中心
     */
    private String cost;

    /**
     * 
     */
    private String genledgno;

    /**
     * 交易对手
     */
    private String cust;

    /**
     * 
     */
    private BigDecimal acctbal;

    /**
     * 
     */
    private BigDecimal amtbalprev;

    /**
     * 最后维护日期
     */
    private Date lstmntdte;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 同步状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;
    
    
    /**
     * 清算路径
     */
    @Transient
    private String smeans;
    
    /**
     * 交易对手简称
     */
    @Transient
    private String sn;
    
    
    
	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getNos() {
		return nos;
	}

	public void setNos(String nos) {
		this.nos = nos;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getGenledgno() {
		return genledgno;
	}

	public void setGenledgno(String genledgno) {
		this.genledgno = genledgno;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust;
	}

	public BigDecimal getAcctbal() {
		return acctbal;
	}

	public void setAcctbal(BigDecimal acctbal) {
		this.acctbal = acctbal;
	}

	public BigDecimal getAmtbalprev() {
		return amtbalprev;
	}

	public void setAmtbalprev(BigDecimal amtbalprev) {
		this.amtbalprev = amtbalprev;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}
    
    
    
    

}
