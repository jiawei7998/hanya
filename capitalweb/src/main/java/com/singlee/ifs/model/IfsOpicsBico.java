package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/***
 * opics 系统  工业标准码
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_OPICS_BICO")
public class IfsOpicsBico implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**bic码*/
	private String bic;
	
	/**bic码英文详情*/
	private String sn;
	
	/**bic码中文详情*/
	private String sncn;
	
	/***/
	private String ba1;
	
	/***/
	private String ba2;
	
	/***/
	private String ba3;
	
	/***/
	private String ba4;
	
	/***/
	private String ba5;
	
	/**上次维护日期*/
	private Date lstmntdte;
	
	/**操作者*/
	private String operator;
	
	/**opics处理状态*/
	private String status;
	
	/**备注*/
	private String remark;

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getSncn() {
		return sncn;
	}

	public void setSncn(String sncn) {
		this.sncn = sncn;
	}

	public String getBa1() {
		return ba1;
	}

	public void setBa1(String ba1) {
		this.ba1 = ba1;
	}

	public String getBa2() {
		return ba2;
	}

	public void setBa2(String ba2) {
		this.ba2 = ba2;
	}

	public String getBa3() {
		return ba3;
	}

	public void setBa3(String ba3) {
		this.ba3 = ba3;
	}

	public String getBa4() {
		return ba4;
	}

	public void setBa4(String ba4) {
		this.ba4 = ba4;
	}

	public String getBa5() {
		return ba5;
	}

	public void setBa5(String ba5) {
		this.ba5 = ba5;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
