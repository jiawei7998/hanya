package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_APPROVEFX_FWD")
public class IfsApprovefxFwd  extends IfsApproveBaseBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tradingModel;
	//交易模式
	private String tradingType;
	//交易方式
	private String currencyPair;
	//货币对
	private String price;
	//价格
	private String buyAmount;
	//买入方向金额
	private String sellAmount;
	//卖出方向金额
	
	private String valueDate;
	//起息日
	
	@Override
    public String getTradingModel() {
		return tradingModel;
	}

	@Override
    public void setTradingModel(String tradingModel) {
		this.tradingModel = tradingModel == null ? null : tradingModel.trim();
	}

	public String getTradingType() {
		return tradingType;
	}

	public void setTradingType(String tradingType) {
		this.tradingType = tradingType == null ? null : tradingType.trim();
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair == null ? null : currencyPair.trim();
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price == null ? null : price.trim();
	}

	public String getBuyAmount() {
		return buyAmount;
	}

	public void setBuyAmount(String buyAmount) {
		this.buyAmount = buyAmount == null ? null : buyAmount.trim();
	}

	public String getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(String sellAmount) {
		this.sellAmount = sellAmount == null ? null : sellAmount.trim();
	}


	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

}