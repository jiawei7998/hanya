package com.singlee.ifs.model;

import javax.persistence.Transient;

public class IfsLimitCondition {
   public String id;                   // ID
   public String product;              // 产品
   public String type;                 // 限额类型
   public String limitCondition;       // 限额条件
   public double quotaAmount;          // 限额金额
   public double availAmount;          // 可用金额
   public String inputTrader;          // 录入人员
   public String inputTime;            // 录入时间
   public String trader;               // 交易员
   @Transient
   private String userName;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getProduct() {
	return product;
}
public void setProduct(String product) {
	this.product = product;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getLimitCondition() {
	return limitCondition;
}
public void setLimitCondition(String limitCondition) {
	this.limitCondition = limitCondition;
}
public double getQuotaAmount() {
	return quotaAmount;
}
public void setQuotaAmount(double quotaAmount) {
	this.quotaAmount = quotaAmount;
}
public double getAvailAmount() {
	return availAmount;
}
public void setAvailAmount(double availAmount) {
	this.availAmount = availAmount;
}
public String getInputTrader() {
	return inputTrader;
}
public void setInputTrader(String inputTrader) {
	this.inputTrader = inputTrader;
}
public String getInputTime() {
	return inputTime;
}
public void setInputTime(String inputTime) {
	this.inputTime = inputTime;
}
public String getTrader() {
	return trader;
}
public void setTrader(String trader) {
	this.trader = trader;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}



   
}
