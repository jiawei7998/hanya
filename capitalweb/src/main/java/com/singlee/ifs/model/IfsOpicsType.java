package com.singlee.ifs.model;

import java.util.Date;

/***
 * opics 产品类型表
 *
 */

public class IfsOpicsType {
	/**
	 * 产品代码
	 */
	private String prodcode;
	/**
	 * 产品类型
	 */
	private String type;
	/**
	 * 描述
	 */
	private String descr;

	/**
	 * 产品中文描述
	 */
	private String descrcn;
	/**
	 * 维护时间
	 */
	private Date lstmntdte;
	/**
	 * 模块
	 */
	private String al;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 处理状态
	 */
	private String status;
	
	
	/**
	 * 备注
	 */
	private String remark;


	public String getProdcode() {
		return prodcode;
	}


	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}


	public String getDescrcn() {
		return descrcn;
	}


	public void setDescrcn(String descrcn) {
		this.descrcn = descrcn;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getAl() {
		return al;
	}


	public void setAl(String al) {
		this.al = al;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	


}
