package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Transient;

public class IfsCfetsrmbCbt extends IfsCfetsrmbCbtKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String forDate;
	private String execID;
	
	private String lastQty;
	
	private String orderID;
	
	private String price;
	
	private String securityID;
	
	private String side;
	
	private String symbol;
	
	private String text;
	
	private String settlDate;
	
	private String tradeDate;
	
	private String settlCurrAmt;
	
	private String execType;
	
	private String accruedInterestAmt;
	
	private int deliveryType;
	
	private String accruedInterestTotalAmt;
	
	private String marketIndicator;
	
	private String termToMaturity;
	
	private boolean remarkIndicator;
	
	private String tradeCashAmt;
	
	private String tradeTime;
	
	private int tradeType;
	
	private int dataCategoryIndicator;
	
	private String stipulationType;
	
	private String stipulationValue;
	
	private String takerPartyID;// 发起方成员唯一标识
	
	private String takername;// 发起方成员全称
	
	private String takerTradercode;// 发起方交易员代码
	
	private String takerTradername;// 发起方交易员名称
	
	private String takerSecuIns;// 发起方证券托管机构名称 111
	
	private String takerSecuAcct;// 发起方证券托管帐号 10
	
	private String takerSecuAcctNm;// 发起方证券托管账户户名 22
	
	private String takerSettleBank;// 发起方资金清算行联行号 112
	
	private String takerSettleBankNm;// 发起方资金清算行名称 110
	
	private String takerSettleAcctNm;// 发起方资金清算账户户名 23
	
	private String takerSettleAcct;// 发起方资金清算帐号 15
	
	private String makerPartyID;// 对手成员唯一标识
	
	private String makername;// 对手方成员全称 5
	
	private String makerTradername;// 对手方交易员名称 101
	
	private String makerSecuIns;// 对手方证券托管机构名称 111
	
	private String makerSecuAcct;// 对手方证券托管帐号 10
	
	private String makerSecuAcctNm;// 对手方证券托管账户户名 22
	
	private String makerSettleBank;// 对手方资金清算行联行号 112
	
	private String makerSettleBankNm;// 对手方资金清算行名称 110
	
	private String makerSettleAcctNm;// 对手方资金清算账户户名 23
	
	private String makerSettleAcct;// 对手方资金清算帐号 15
	
	private String retCode;
	
	private String retMsg;
	
	private String buyInst;

	private String buyTrader;

	private String buyTel;

	private String buyFax;

	private String buyCorp;

	private String buyAddr;

	private String sellInst;

	private String sellTrader;

	private String sellTel;

	private String sellFax;

	private String sellCorp;

	private String sellAddr;

	private String bondCode;

	private String bondName;

	private BigDecimal cleanPrice;

	private BigDecimal totalFaceValue;

	private BigDecimal yield;

	private BigDecimal tradeAmount;

	private BigDecimal accuredInterest;

	private BigDecimal totalAccuredInterest;

	private BigDecimal dirtyPrice;

	private BigDecimal settlementAmount;

	private String settlementMethod;

	private String settlementDate;

	private String buyAccname;

	private String buyOpbank;

	private String buyAccnum;

	private String buyPsnum;

	private String buyCaname;

	private String buyCustname;

	private String buyCanum;

	private String sellAccname;

	private String sellOpbank;

	private String sellAccnum;

	private String sellPsnum;

	private String sellCaname;

	private String sellCustname;

	private String sellCanum;
	
	private String invtype;
	
	//权重
	private String weight;

	//占用授信主体
	private String custNo;
	//占用授信主体类型
	private String custType;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	//价格偏离度
	private String priceDeviation;

	/**
	 * 额度占用类型（0：不占，1：发行人，2：交易对手）
	 */
	private String quotaOccupyType;
	
	@Transient
	private BigDecimal pricecheck;
	
	@Transient
	private BigDecimal amtcheck;
	
	@Transient
	private BigDecimal opicsprice;
	
	@Transient
	private BigDecimal opicssettleamt;
	
	@Transient
	private String  prdNo;
	
	private String  cfetsno;
	
	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;
	/**
	 * 是否异地业务
	 */
	private String  remoteService;

	//前置产品编号
	private String fPrdCode;

	/**
	 * 存款账号
	 */
	private String depositAccount;
	
	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getLastQty() {
		return lastQty;
	}

	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSecurityID() {
		return securityID;
	}

	public void setSecurityID(String securityID) {
		this.securityID = securityID;
	}
	
	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSettlDate() {
		return settlDate;
	}

	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getSettlCurrAmt() {
		return settlCurrAmt;
	}

	public void setSettlCurrAmt(String settlCurrAmt) {
		this.settlCurrAmt = settlCurrAmt;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getAccruedInterestAmt() {
		return accruedInterestAmt;
	}

	public void setAccruedInterestAmt(String accruedInterestAmt) {
		this.accruedInterestAmt = accruedInterestAmt;
	}

	public int getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(int deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getAccruedInterestTotalAmt() {
		return accruedInterestTotalAmt;
	}

	public void setAccruedInterestTotalAmt(String accruedInterestTotalAmt) {
		this.accruedInterestTotalAmt = accruedInterestTotalAmt;
	}


	public String getBuyInst() {
		return buyInst;
	}

	public void setBuyInst(String buyInst) {
		this.buyInst = buyInst;
	}

	public String getBuyTrader() {
		return buyTrader;
	}

	public void setBuyTrader(String buyTrader) {
		this.buyTrader = buyTrader;
	}

	public String getBuyTel() {
		return buyTel;
	}

	public void setBuyTel(String buyTel) {
		this.buyTel = buyTel;
	}

	public String getBuyFax() {
		return buyFax;
	}

	public void setBuyFax(String buyFax) {
		this.buyFax = buyFax;
	}

	public String getBuyCorp() {
		return buyCorp;
	}

	public void setBuyCorp(String buyCorp) {
		this.buyCorp = buyCorp;
	}

	public String getBuyAddr() {
		return buyAddr;
	}

	public void setBuyAddr(String buyAddr) {
		this.buyAddr = buyAddr;
	}

	public String getSellInst() {
		return sellInst;
	}

	public void setSellInst(String sellInst) {
		this.sellInst = sellInst;
	}

	public String getSellTrader() {
		return sellTrader;
	}

	public void setSellTrader(String sellTrader) {
		this.sellTrader = sellTrader;
	}

	public String getSellTel() {
		return sellTel;
	}

	public void setSellTel(String sellTel) {
		this.sellTel = sellTel;
	}

	public String getSellFax() {
		return sellFax;
	}

	public void setSellFax(String sellFax) {
		this.sellFax = sellFax;
	}

	public String getSellCorp() {
		return sellCorp;
	}

	public void setSellCorp(String sellCorp) {
		this.sellCorp = sellCorp;
	}

	public String getSellAddr() {
		return sellAddr;
	}

	public void setSellAddr(String sellAddr) {
		this.sellAddr = sellAddr;
	}

	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public String getBondName() {
		return bondName;
	}

	public void setBondName(String bondName) {
		this.bondName = bondName;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public BigDecimal getTotalFaceValue() {
		return totalFaceValue;
	}

	public void setTotalFaceValue(BigDecimal totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}

	public BigDecimal getYield() {
		return yield;
	}

	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getTotalAccuredInterest() {
		return totalAccuredInterest;
	}

	public void setTotalAccuredInterest(BigDecimal totalAccuredInterest) {
		this.totalAccuredInterest = totalAccuredInterest;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getBuyAccname() {
		return buyAccname;
	}

	public void setBuyAccname(String buyAccname) {
		this.buyAccname = buyAccname;
	}

	public String getBuyOpbank() {
		return buyOpbank;
	}

	public void setBuyOpbank(String buyOpbank) {
		this.buyOpbank = buyOpbank;
	}

	public String getBuyAccnum() {
		return buyAccnum;
	}

	public void setBuyAccnum(String buyAccnum) {
		this.buyAccnum = buyAccnum;
	}

	public String getBuyPsnum() {
		return buyPsnum;
	}

	public void setBuyPsnum(String buyPsnum) {
		this.buyPsnum = buyPsnum;
	}

	public String getBuyCaname() {
		return buyCaname;
	}

	public void setBuyCaname(String buyCaname) {
		this.buyCaname = buyCaname;
	}

	public String getBuyCustname() {
		return buyCustname;
	}

	public void setBuyCustname(String buyCustname) {
		this.buyCustname = buyCustname;
	}

	public String getBuyCanum() {
		return buyCanum;
	}

	public void setBuyCanum(String buyCanum) {
		this.buyCanum = buyCanum;
	}

	public String getSellAccname() {
		return sellAccname;
	}

	public void setSellAccname(String sellAccname) {
		this.sellAccname = sellAccname;
	}

	public String getSellOpbank() {
		return sellOpbank;
	}

	public void setSellOpbank(String sellOpbank) {
		this.sellOpbank = sellOpbank;
	}

	public String getSellAccnum() {
		return sellAccnum;
	}

	public void setSellAccnum(String sellAccnum) {
		this.sellAccnum = sellAccnum;
	}

	public String getSellPsnum() {
		return sellPsnum;
	}

	public void setSellPsnum(String sellPsnum) {
		this.sellPsnum = sellPsnum;
	}

	public String getSellCaname() {
		return sellCaname;
	}

	public void setSellCaname(String sellCaname) {
		this.sellCaname = sellCaname;
	}

	public String getSellCustname() {
		return sellCustname;
	}

	public void setSellCustname(String sellCustname) {
		this.sellCustname = sellCustname;
	}

	public String getSellCanum() {
		return sellCanum;
	}

	public void setSellCanum(String sellCanum) {
		this.sellCanum = sellCanum;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public String getPriceDeviation() {
		return priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation;
	}

	public String getQuotaOccupyType() {
		return quotaOccupyType;
	}

	public void setQuotaOccupyType(String quotaOccupyType) {
		this.quotaOccupyType = quotaOccupyType;
	}

	public BigDecimal getPricecheck() {
		return pricecheck;
	}

	public void setPricecheck(BigDecimal pricecheck) {
		this.pricecheck = pricecheck;
	}

	public BigDecimal getAmtcheck() {
		return amtcheck;
	}

	public void setAmtcheck(BigDecimal amtcheck) {
		this.amtcheck = amtcheck;
	}

	public BigDecimal getOpicsprice() {
		return opicsprice;
	}

	public void setOpicsprice(BigDecimal opicsprice) {
		this.opicsprice = opicsprice;
	}

	public BigDecimal getOpicssettleamt() {
		return opicssettleamt;
	}

	public void setOpicssettleamt(BigDecimal opicssettleamt) {
		this.opicssettleamt = opicssettleamt;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	public String getRemoteService() {
		return remoteService;
	}

	public void setRemoteService(String remoteService) {
		this.remoteService = remoteService;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getDepositAccount() {
		return depositAccount;
	}

	public void setDepositAccount(String depositAccount) {
		this.depositAccount = depositAccount;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getTermToMaturity() {
		return termToMaturity;
	}

	public void setTermToMaturity(String termToMaturity) {
		this.termToMaturity = termToMaturity;
	}

	public boolean isRemarkIndicator() {
		return remarkIndicator;
	}

	public void setRemarkIndicator(boolean remarkIndicator) {
		this.remarkIndicator = remarkIndicator;
	}

	public String getTradeCashAmt() {
		return tradeCashAmt;
	}

	public void setTradeCashAmt(String tradeCashAmt) {
		this.tradeCashAmt = tradeCashAmt;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public int getTradeType() {
		return tradeType;
	}

	public void setTradeType(int tradeType) {
		this.tradeType = tradeType;
	}

	public int getDataCategoryIndicator() {
		return dataCategoryIndicator;
	}

	public void setDataCategoryIndicator(int dataCategoryIndicator) {
		this.dataCategoryIndicator = dataCategoryIndicator;
	}

	public String getStipulationType() {
		return stipulationType;
	}

	public void setStipulationType(String stipulationType) {
		this.stipulationType = stipulationType;
	}

	public String getStipulationValue() {
		return stipulationValue;
	}

	public void setStipulationValue(String stipulationValue) {
		this.stipulationValue = stipulationValue;
	}

	public String getTakerPartyID() {
		return takerPartyID;
	}

	public void setTakerPartyID(String takerPartyID) {
		this.takerPartyID = takerPartyID;
	}

	public String getTakername() {
		return takername;
	}

	public void setTakername(String takername) {
		this.takername = takername;
	}

	public String getTakerTradercode() {
		return takerTradercode;
	}

	public void setTakerTradercode(String takerTradercode) {
		this.takerTradercode = takerTradercode;
	}

	public String getTakerTradername() {
		return takerTradername;
	}

	public void setTakerTradername(String takerTradername) {
		this.takerTradername = takerTradername;
	}

	public String getTakerSecuIns() {
		return takerSecuIns;
	}

	public void setTakerSecuIns(String takerSecuIns) {
		this.takerSecuIns = takerSecuIns;
	}

	public String getTakerSecuAcct() {
		return takerSecuAcct;
	}

	public void setTakerSecuAcct(String takerSecuAcct) {
		this.takerSecuAcct = takerSecuAcct;
	}

	public String getTakerSecuAcctNm() {
		return takerSecuAcctNm;
	}

	public void setTakerSecuAcctNm(String takerSecuAcctNm) {
		this.takerSecuAcctNm = takerSecuAcctNm;
	}

	public String getTakerSettleBank() {
		return takerSettleBank;
	}

	public void setTakerSettleBank(String takerSettleBank) {
		this.takerSettleBank = takerSettleBank;
	}

	public String getTakerSettleBankNm() {
		return takerSettleBankNm;
	}

	public void setTakerSettleBankNm(String takerSettleBankNm) {
		this.takerSettleBankNm = takerSettleBankNm;
	}

	public String getTakerSettleAcctNm() {
		return takerSettleAcctNm;
	}

	public void setTakerSettleAcctNm(String takerSettleAcctNm) {
		this.takerSettleAcctNm = takerSettleAcctNm;
	}

	public String getTakerSettleAcct() {
		return takerSettleAcct;
	}

	public void setTakerSettleAcct(String takerSettleAcct) {
		this.takerSettleAcct = takerSettleAcct;
	}

	public String getMakerPartyID() {
		return makerPartyID;
	}

	public void setMakerPartyID(String makerPartyID) {
		this.makerPartyID = makerPartyID;
	}

	public String getMakername() {
		return makername;
	}

	public void setMakername(String makername) {
		this.makername = makername;
	}

	public String getMakerTradername() {
		return makerTradername;
	}

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public void setMakerTradername(String makerTradername) {
		this.makerTradername = makerTradername;
	}

	public String getMakerSecuIns() {
		return makerSecuIns;
	}

	public void setMakerSecuIns(String makerSecuIns) {
		this.makerSecuIns = makerSecuIns;
	}

	public String getMakerSecuAcct() {
		return makerSecuAcct;
	}

	public void setMakerSecuAcct(String makerSecuAcct) {
		this.makerSecuAcct = makerSecuAcct;
	}

	public String getMakerSecuAcctNm() {
		return makerSecuAcctNm;
	}

	public void setMakerSecuAcctNm(String makerSecuAcctNm) {
		this.makerSecuAcctNm = makerSecuAcctNm;
	}

	public String getMakerSettleBank() {
		return makerSettleBank;
	}

	public void setMakerSettleBank(String makerSettleBank) {
		this.makerSettleBank = makerSettleBank;
	}

	public String getMakerSettleBankNm() {
		return makerSettleBankNm;
	}

	public void setMakerSettleBankNm(String makerSettleBankNm) {
		this.makerSettleBankNm = makerSettleBankNm;
	}

	public String getMakerSettleAcctNm() {
		return makerSettleAcctNm;
	}

	public void setMakerSettleAcctNm(String makerSettleAcctNm) {
		this.makerSettleAcctNm = makerSettleAcctNm;
	}

	public String getMakerSettleAcct() {
		return makerSettleAcct;
	}

	public void setMakerSettleAcct(String makerSettleAcct) {
		this.makerSettleAcct = makerSettleAcct;
	}
}