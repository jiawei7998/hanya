package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;

public class IfsCfetsBeforeDeal implements Serializable{

	private String ticketId;

	private String contractId;

	private String forDate;

	private String dealType;

	private String dealDealer;

	private String dealDiction;

	private String dealAmt;

	private String dealRate;

	private String status;
	
	private String dealAmt1;

	private String dealRate1;
	@Transient
	private String amt;
	@Transient
	private String rate;
	@Transient
	private String diction;
	private static final long serialVersionUID = 1L;
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getForDate() {
		return forDate;
	}
	public void setForDate(String forDate) {
		this.forDate = forDate;
	}
	public String getDealType() {
		return dealType;
	}
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	public String getDealDealer() {
		return dealDealer;
	}
	public void setDealDealer(String dealDealer) {
		this.dealDealer = dealDealer;
	}
	public String getDealDiction() {
		return dealDiction;
	}
	public void setDealDiction(String dealDiction) {
		this.dealDiction = dealDiction;
	}
	public String getDealAmt() {
		return dealAmt;
	}
	public void setDealAmt(String dealAmt) {
		this.dealAmt = dealAmt;
	}
	public String getDealRate() {
		return dealRate;
	}
	public void setDealRate(String dealRate) {
		this.dealRate = dealRate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDealAmt1() {
		return dealAmt1;
	}
	public void setDealAmt1(String dealAmt1) {
		this.dealAmt1 = dealAmt1;
	}
	public String getDealRate1() {
		return dealRate1;
	}
	public void setDealRate1(String dealRate1) {
		this.dealRate1 = dealRate1;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getDiction() {
		return diction;
	}
	public void setDiction(String diction) {
		this.diction = diction;
	}
	
	
}
