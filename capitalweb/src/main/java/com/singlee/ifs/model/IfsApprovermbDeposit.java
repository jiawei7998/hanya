package com.singlee.ifs.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

/**
    * 同业存款审批单
    */
@Table(name = "IFS_APPROVERMB_DEPOSIT")
public class IfsApprovermbDeposit {
    /**
     * 成交编号
     */
    @Id
    @Column(name = "TICKET_ID")
    private String ticketId;

    /**
     * 交易对手
     */
    @Column(name = "COUNTERPARTY_INST_ID")
    private String counterpartyInstId;

    /**
     * 报送日期
     */
    @Column(name = "POST_DATE")
    private String postDate;

    /**
     * 期限
     */
    @Column(name = "TENOR")
    private String tenor;

    /**
     * 利率
     */
    @Column(name = "RATE")
    private BigDecimal rate;

    /**
     * 金额
     */
    @Column(name = "AMT")
    private BigDecimal amt;

    /**
     * 应计利息
     */
    @Column(name = "ACCURED_INTEREST")
    private BigDecimal accuredInterest;

    /**
     * 到期还款金额
     */
    @Column(name = "SETTLEMENT_AMOUNT")
    private BigDecimal settlementAmount;

    /**
     * 首次结算日
     */
    @Column(name = "FIRST_SETTLEMENT_DATE")
    private String firstSettlementDate;

    /**
     * 到期结算日
     */
    @Column(name = "SECOND_SETTLEMENT_DATE")
    private String secondSettlementDate;

    /**
     * 实际占款天数
     */
    @Column(name = "OCCUPANCY_DAYS")
    private String occupancyDays;

    /**
     * 审批状态
     */
    @Column(name = "APPROVE_STATUS")
    private String approveStatus;

    /**
     * 审批发起人
     */
    @Column(name = "SPONSOR")
    private String sponsor;

    /**
     * 审批发起机构
     */
    @Column(name = "SPON_INST")
    private String sponInst;

    /**
     * 审批开始日期
     */
    @Column(name = "A_DATE")
    private String aDate;

    /**
     * 备注
     */
    @Column(name = "NOTE")
    private String note;

    /**
     * 录入时间
     */
    @Column(name = "INPUT_TIME")
    private String inputTime;

    /**
     * 最后修改时间
     */
    @Column(name = "LAST_TIME")
    private String lastTime;

    /**
     * 产品代码
     */
    @Column(name = "PRODUCT")
    private String product;

    /**
     * 产品类型
     */
    @Column(name = "PROD_TYPE")
    private String prodType;

    /**
     * 交易状态
     */
    @Column(name = "DEAL_TRANS_TYPE")
    private String dealTransType;
    /**
     * 计息基础
     */
    @Column(name = "basis")
    private String basis;
    /**
     * 币种
     */
    @Column(name = "currency")
    private String currency;

    /**
     * 付息频率
     */
    private String intFeq;

    /**
     * 收本频率
     */
    private String amtFeq;
    /**
     *本方清算
     */
    private String borrowBnknm;
    private String borrowBnkno;
    private String borrowAccnm;
    private String borrowAccno;
    /**
     * 对手方清算
     */
    private String removeBnknm;
    private String removeBnkno;
    private String removeAccnm;
    private String removeAccno;
    @Transient
    private String taskId;
    @Transient
    private String taskName;
    @Transient
    private String userName;
    @Transient
    private String instName;
    @Transient
    private String cname;

    @Column(name = "CORE_ACCT_NO")
    private String coreAcctNo;

    @Column(name = "CORE_ACCT_NAME")
    private String coreAcctName;

    @Column(name = "CUST_NO")
    private String custNo;
    @Column(name = "WEIGHT")
    private String weight;
    @Column(name = "CUST_TYPE")
    private String custType;

    /**
     * 五级分类
     */
    @Column(name = "five_level_class")
    private String fiveLevelClass;

    /**
     * 归属部门
     */
    @Column(name = "attribution_dept")
    private String attributionDept;

    public String getFiveLevelClass() {
        return fiveLevelClass;
    }

    public void setFiveLevelClass(String fiveLevelClass) {
        this.fiveLevelClass = fiveLevelClass;
    }

    public String getAttributionDept() {
        return attributionDept;
    }

    public void setAttributionDept(String attributionDept) {
        this.attributionDept = attributionDept;
    }
    /**
     * 获取成交编号
     *
     * @return TICKET_ID - 成交编号
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * 设置成交编号
     *
     * @param ticketId 成交编号
     */
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     * 获取交易对手
     *
     * @return COUNTERPARTY_INST_ID - 交易对手
     */
    public String getCounterpartyInstId() {
        return counterpartyInstId;
    }

    /**
     * 设置交易对手
     *
     * @param counterpartyInstId 交易对手
     */
    public void setCounterpartyInstId(String counterpartyInstId) {
        this.counterpartyInstId = counterpartyInstId;
    }

    /**
     * 获取报送日期
     *
     * @return POST_DATE - 报送日期
     */
    public String getPostDate() {
        return postDate;
    }

    /**
     * 设置报送日期
     *
     * @param postDate 报送日期
     */
    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    /**
     * 获取期限
     *
     * @return TENOR - 期限
     */
    public String getTenor() {
        return tenor;
    }

    /**
     * 设置期限
     *
     * @param tenor 期限
     */
    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    /**
     * 获取利率
     *
     * @return RATE - 利率
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 设置利率
     *
     * @param rate 利率
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * 获取金额
     *
     * @return AMT - 金额
     */
    public BigDecimal getAmt() {
        return amt;
    }

    /**
     * 设置金额
     *
     * @param amt 金额
     */
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    /**
     * 获取应计利息
     *
     * @return ACCURED_INTEREST - 应计利息
     */
    public BigDecimal getAccuredInterest() {
        return accuredInterest;
    }

    /**
     * 设置应计利息
     *
     * @param accuredInterest 应计利息
     */
    public void setAccuredInterest(BigDecimal accuredInterest) {
        this.accuredInterest = accuredInterest;
    }

    /**
     * 获取到期还款金额
     *
     * @return SETTLEMENT_AMOUNT - 到期还款金额
     */
    public BigDecimal getSettlementAmount() {
        return settlementAmount;
    }

    /**
     * 设置到期还款金额
     *
     * @param settlementAmount 到期还款金额
     */
    public void setSettlementAmount(BigDecimal settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    /**
     * 获取首次结算日
     *
     * @return FIRST_SETTLEMENT_DATE - 首次结算日
     */
    public String getFirstSettlementDate() {
        return firstSettlementDate;
    }

    /**
     * 设置首次结算日
     *
     * @param firstSettlementDate 首次结算日
     */
    public void setFirstSettlementDate(String firstSettlementDate) {
        this.firstSettlementDate = firstSettlementDate;
    }

    /**
     * 获取到期结算日
     *
     * @return SECOND_SETTLEMENT_DATE - 到期结算日
     */
    public String getSecondSettlementDate() {
        return secondSettlementDate;
    }

    /**
     * 设置到期结算日
     *
     * @param secondSettlementDate 到期结算日
     */
    public void setSecondSettlementDate(String secondSettlementDate) {
        this.secondSettlementDate = secondSettlementDate;
    }

    /**
     * 获取实际占款天数
     *
     * @return OCCUPANCY_DAYS - 实际占款天数
     */
    public String getOccupancyDays() {
        return occupancyDays;
    }

    /**
     * 设置实际占款天数
     *
     * @param occupancyDays 实际占款天数
     */
    public void setOccupancyDays(String occupancyDays) {
        this.occupancyDays = occupancyDays;
    }

    /**
     * 获取审批状态
     *
     * @return APPROVE_STATUS - 审批状态
     */
    public String getApproveStatus() {
        return approveStatus;
    }

    /**
     * 设置审批状态
     *
     * @param approveStatus 审批状态
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * 获取审批发起人
     *
     * @return SPONSOR - 审批发起人
     */
    public String getSponsor() {
        return sponsor;
    }

    /**
     * 设置审批发起人
     *
     * @param sponsor 审批发起人
     */
    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    /**
     * 获取审批发起机构
     *
     * @return SPON_INST - 审批发起机构
     */
    public String getSponInst() {
        return sponInst;
    }

    /**
     * 设置审批发起机构
     *
     * @param sponInst 审批发起机构
     */
    public void setSponInst(String sponInst) {
        this.sponInst = sponInst;
    }

    /**
     * 获取审批开始日期
     *
     * @return A_DATE - 审批开始日期
     */
    public String getaDate() {
        return aDate;
    }

    /**
     * 设置审批开始日期
     *
     * @param aDate 审批开始日期
     */
    public void setaDate(String aDate) {
        this.aDate = aDate;
    }

    /**
     * 获取备注
     *
     * @return NOTE - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * 获取录入时间
     *
     * @return INPUT_TIME - 录入时间
     */
    public String getInputTime() {
        return inputTime;
    }

    /**
     * 设置录入时间
     *
     * @param inputTime 录入时间
     */
    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    /**
     * 获取最后修改时间
     *
     * @return LAST_TIME - 最后修改时间
     */
    public String getLastTime() {
        return lastTime;
    }

    /**
     * 设置最后修改时间
     *
     * @param lastTime 最后修改时间
     */
    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    /**
     * 获取产品代码
     *
     * @return PRODUCT - 产品代码
     */
    public String getProduct() {
        return product;
    }

    /**
     * 设置产品代码
     *
     * @param product 产品代码
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * 获取产品类型
     *
     * @return PROD_TYPE - 产品类型
     */
    public String getProdType() {
        return prodType;
    }

    /**
     * 设置产品类型
     *
     * @param prodType 产品类型
     */
    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    /**
     * 获取交易状态
     *
     * @return DEAL_TRANS_TYPE - 交易状态
     */
    public String getDealTransType() {
        return dealTransType;
    }

    /**
     * 设置交易状态
     *
     * @param dealTransType 交易状态
     */
    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIntFeq() {
        return intFeq;
    }

    public void setIntFeq(String intFeq) {
        this.intFeq = intFeq;
    }

    public String getBorrowBnknm() {
        return borrowBnknm;
    }

    public void setBorrowBnknm(String borrowBnknm) {
        this.borrowBnknm = borrowBnknm;
    }

    public String getBorrowBnkno() {
        return borrowBnkno;
    }

    public void setBorrowBnkno(String borrowBnkno) {
        this.borrowBnkno = borrowBnkno;
    }

    public String getBorrowAccnm() {
        return borrowAccnm;
    }

    public void setBorrowAccnm(String borrowAccnm) {
        this.borrowAccnm = borrowAccnm;
    }

    public String getBorrowAccno() {
        return borrowAccno;
    }

    public void setBorrowAccno(String borrowAccno) {
        this.borrowAccno = borrowAccno;
    }

    public String getRemoveBnknm() {
        return removeBnknm;
    }

    public void setRemoveBnknm(String removeBnknm) {
        this.removeBnknm = removeBnknm;
    }

    public String getRemoveBnkno() {
        return removeBnkno;
    }

    public void setRemoveBnkno(String removeBnkno) {
        this.removeBnkno = removeBnkno;
    }

    public String getRemoveAccnm() {
        return removeAccnm;
    }

    public void setRemoveAccnm(String removeAccnm) {
        this.removeAccnm = removeAccnm;
    }

    public String getRemoveAccno() {
        return removeAccno;
    }

    public void setRemoveAccno(String removeAccno) {
        this.removeAccno = removeAccno;
    }

    public String getAmtFeq() {
        return amtFeq;
    }

    public void setAmtFeq(String amtFeq) {
        this.amtFeq = amtFeq;
    }

    public String getCoreAcctNo() {
        return coreAcctNo;
    }

    public void setCoreAcctNo(String coreAcctNo) {
        this.coreAcctNo = coreAcctNo;
    }

    public String getCoreAcctName() {
        return coreAcctName;
    }

    public void setCoreAcctName(String coreAcctName) {
        this.coreAcctName = coreAcctName;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }
}