package com.singlee.ifs.model;

import java.io.Serializable;


/***
 * opics 客户信息交易对手表
 * 
 * @author singlee4
 * 
 */
public class IfsOpicsCust implements Serializable{

	private static final long serialVersionUID = 8504388124583759564L;

	/* 交易对手编号 */
	private String cno;

	/* 交易对手代号 */
	private String cname;

	/* swift code */
	private String bic;

	/* 城市代码 */
	private String ccode;

	/* 行业代码 */
	private String sic;

	/* 证件类型 */
	private String gtype;

	/* 证件号码 */
	private String gid;

	/* 客户名称 */
	private String cliname;

	/* 客户号 */
	private String clino;

	/* 客户分类 */
	private String custclass;

	/* 境内外标志 */
	private String flag;

	/* 金融机构类型 */
	private String fintype;

	/* 客户金融许可证 */
	private String license;

	/* 注册地址 */
	private String regplace;

	/* 行业类型 */
	private String indtype;

	/* 评级类型 */
	private String ratype;

	/* 外部评级信息 */
	private String extrainfo;

	/* 外部评级结果 */
	private String ratresult1;

	/* 内部评级信息 */
	private String interinfo;

	/* 内部评级结果 */
	private String ratresult2;

	/* 交易对手简称 */
	private String sn;

	/* 交易对手全称 */
	private String cfn;

	/* 公司地址 */
	private String ca;

	/* 交易对手类型 */
	private String ctype;

	/* 邮编 */
	private String cpost;

	/* 维护更新日期 */
	private String lstmntdate;

	/* 国家代码 */
	private String uccode;

	/* 纳税人编号 */
	private String taxid;

	/* 会计类型 */
	private String acctngtype;

	/**
	 * 会计类型中文描述（IFS_OPICS_ACTY）
	 */
	private String acctngTypeDesc;
	/* 备注 */
	private String ratinfo;

	/* 备注1 */
	private String remark1;

	/* 备注2 */
	private String remark2;

	/* 所在城市 */
	private String location;

	/**
	 * 导入opics中的流水
	 */
	private String fedealno;

	/**
	 * opics处理状态
	 */
	private String statcode;

	/**
	 * opics错误码
	 */
	private String errorcode;

	/**
	 * 本地交易对手状态
	 */
	private String localstatus;

	/**
	 * 客户建档柜员号
	 */
	private String tellerno;

	/**
	 * 客户建档机构号
	 */
	private String orgno;

	/**
	 * CFETS机构编码
	 */
	private String cfetsno;

	/**
	 * CFETS机构简称
	 */
	private String cfetscn;

	/**
	 * 授信占用主体
	 */
	private String creditsub;

	/**
	 * 授信占用主体中文名称
	 */
	private String creditsubnm;
	
	/**
	 * 授信占用主体编号
	 */
	private String creditsubno;

	/**
	 * 客户类型
	 */
	private String clitype;
	
	/**
	 * 经营所在地地址
	 */
	private String address;
	
	
	/* 主体类型 */
	private String subjectType;
	/* 组织机构代码 */
	private String institucode;
	

	private String shtype;
	private String shcode;
	private String founddate;
	private String ecing;
	private String cre;
	private String comscale;
	private String holdtype;
	private String isacc;
	private String crelevel;
	private String ruciflag;
	private String ecotype;

	private String prdManager;
	private String trustee;
	private String transType;

	private String linkman;
	private String contact;

	//经办标识
	private String handle;
	//交易对手落地机构
	private String custorgan;
	//交易对手地域
	private String custarea;
	private String custtype;

	//经办人
	private String ioper;

	//复核人
	private String reviewer;
	/**
	 * 基本存款账号
	 */
	private String basicDepositAccount;
	/**
	 *基本账号开户行名称
	 */
	private String bankAccountName;

	public String getBasicDepositAccount() {
		return basicDepositAccount;
	}

	public void setBasicDepositAccount(String basicDepositAccount) {
		this.basicDepositAccount = basicDepositAccount;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getAcctngTypeDesc() {
		return acctngTypeDesc;
	}

	public void setAcctngTypeDesc(String acctngTypeDesc) {
		this.acctngTypeDesc = acctngTypeDesc;
	}

	public String getCusttype() {
		return custtype;
	}

	public void setCusttype(String custtype) {
		this.custtype = custtype;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getSic() {
		return sic;
	}

	public void setSic(String sic) {
		this.sic = sic;
	}

	public String getGtype() {
		return gtype;
	}

	public void setGtype(String gtype) {
		this.gtype = gtype;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getCliname() {
		return cliname;
	}

	public void setCliname(String cliname) {
		this.cliname = cliname;
	}

	public String getClino() {
		return clino;
	}

	public void setClino(String clino) {
		this.clino = clino;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getCfn() {
		return cfn;
	}

	public void setCfn(String cfn) {
		this.cfn = cfn;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getCpost() {
		return cpost;
	}

	public void setCpost(String cpost) {
		this.cpost = cpost;
	}

	public String getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getUccode() {
		return uccode;
	}

	public void setUccode(String uccode) {
		this.uccode = uccode;
	}

	public String getTaxid() {
		return taxid;
	}

	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getRatinfo() {
		return ratinfo;
	}

	public void setRatinfo(String ratinfo) {
		this.ratinfo = ratinfo;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getCustclass() {
		return custclass;
	}

	public void setCustclass(String custclass) {
		this.custclass = custclass;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFintype() {
		return fintype;
	}

	public void setFintype(String fintype) {
		this.fintype = fintype;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getRegplace() {
		return regplace;
	}

	public void setRegplace(String regplace) {
		this.regplace = regplace;
	}

	public String getIndtype() {
		return indtype;
	}

	public void setIndtype(String indtype) {
		this.indtype = indtype;
	}

	public String getRatype() {
		return ratype;
	}

	public void setRatype(String ratype) {
		this.ratype = ratype;
	}

	public String getExtrainfo() {
		return extrainfo;
	}

	public void setExtrainfo(String extrainfo) {
		this.extrainfo = extrainfo;
	}

	public String getRatresult1() {
		return ratresult1;
	}

	public void setRatresult1(String ratresult1) {
		this.ratresult1 = ratresult1;
	}

	public String getInterinfo() {
		return interinfo;
	}

	public void setInterinfo(String interinfo) {
		this.interinfo = interinfo;
	}

	public String getRatresult2() {
		return ratresult2;
	}

	public void setRatresult2(String ratresult2) {
		this.ratresult2 = ratresult2;
	}

	public String getLocalstatus() {
		return localstatus;
	}

	public void setLocalstatus(String localstatus) {
		this.localstatus = localstatus;
	}

	public String getTellerno() {
		return tellerno;
	}

	public void setTellerno(String tellerno) {
		this.tellerno = tellerno;
	}

	public String getOrgno() {
		return orgno;
	}

	public void setOrgno(String orgno) {
		this.orgno = orgno;
	}

	public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getCfetscn() {
		return cfetscn;
	}

	public void setCfetscn(String cfetscn) {
		this.cfetscn = cfetscn;
	}

	public String getCreditsub() {
		return creditsub;
	}

	public void setCreditsub(String creditsub) {
		this.creditsub = creditsub;
	}

	public String getCreditsubnm() {
		return creditsubnm;
	}

	public void setCreditsubnm(String creditsubnm) {
		this.creditsubnm = creditsubnm;
	}

	public String getClitype() {
		return clitype;
	}

	public void setClitype(String clitype) {
		this.clitype = clitype;
	}

	public void setCreditsubno(String creditsubno) {
		this.creditsubno = creditsubno;
	}

	public String getCreditsubno() {
		return creditsubno;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}

	public String getInstitucode() {
		return institucode;
	}

	public void setInstitucode(String institucode) {
		this.institucode = institucode;
	}

	public String getFounddate() {
		return founddate;
	}

	public void setFounddate(String founddate) {
		this.founddate = founddate;
	}

	public String getEcing() {
		return ecing;
	}

	public void setEcing(String ecing) {
		this.ecing = ecing;
	}

	public String getCre() {
		return cre;
	}

	public void setCre(String cre) {
		this.cre = cre;
	}

	public String getComscale() {
		return comscale;
	}

	public void setComscale(String comscale) {
		this.comscale = comscale;
	}

	public String getHoldtype() {
		return holdtype;
	}

	public void setHoldtype(String holdtype) {
		this.holdtype = holdtype;
	}

	public String getIsacc() {
		return isacc;
	}

	public void setIsacc(String isacc) {
		this.isacc = isacc;
	}

	public String getCrelevel() {
		return crelevel;
	}

	public void setCrelevel(String crelevel) {
		this.crelevel = crelevel;
	}

	public String getRuciflag() {
		return ruciflag;
	}

	public void setRuciflag(String ruciflag) {
		this.ruciflag = ruciflag;
	}

	public String getShtype() {
		return shtype;
	}

	public void setShtype(String shtype) {
		this.shtype = shtype;
	}

	public String getShcode() {
		return shcode;
	}

	public void setShcode(String shcode) {
		this.shcode = shcode;
	}

	public String getEcotype() {
		return ecotype;
	}

	public void setEcotype(String ecotype) {
		this.ecotype = ecotype;
	}

	public String getPrdManager() {
		return prdManager;
	}

	public void setPrdManager(String prdManager) {
		this.prdManager = prdManager;
	}

	public String getTrustee() {
		return trustee;
	}

	public void setTrustee(String trustee) {
		this.trustee = trustee;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getCustorgan() {
		return custorgan;
	}

	public void setCustorgan(String custorgan) {
		this.custorgan = custorgan;
	}

	public String getCustarea() {
		return custarea;
	}

	public void setCustarea(String custarea) {
		this.custarea = custarea;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
}