package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_APPROVEFX_DEBT")
public class IfsApprovefxDebt  extends IfsApproveBaseBean  implements Serializable{
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -97711378341082649L;
	private BigDecimal  jnumber;
	private String  discountRate;
	private BigDecimal  capital;
	private BigDecimal  price;
	private BigDecimal 	profit;
	private BigDecimal  interest;
	private String  countDate;
	private String  overweight;
	private BigDecimal  net;
	private String 	benchmarkPrice;
	private String  benchmarkProfit;
	private String  benchmarkDiscount;
	
	
	public BigDecimal getJnumber() {
		return jnumber;
	}
	public void setJnumber(BigDecimal jnumber) {
		this.jnumber = jnumber;
	}
	public String getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(String discountRate) {
		this.discountRate = discountRate;
	}
	public BigDecimal getCapital() {
		return capital;
	}
	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getProfit() {
		return profit;
	}
	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}
	public BigDecimal getInterest() {
		return interest;
	}
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	public String getCountDate() {
		return countDate;
	}
	public void setCountDate(String countDate) {
		this.countDate = countDate;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public BigDecimal getNet() {
		return net;
	}
	public void setNet(BigDecimal net) {
		this.net = net;
	}
	public String getBenchmarkPrice() {
		return benchmarkPrice;
	}
	public void setBenchmarkPrice(String benchmarkPrice) {
		this.benchmarkPrice = benchmarkPrice;
	}
	public String getBenchmarkProfit() {
		return benchmarkProfit;
	}
	public void setBenchmarkProfit(String benchmarkProfit) {
		this.benchmarkProfit = benchmarkProfit;
	}
	public String getBenchmarkDiscount() {
		return benchmarkDiscount;
	}
	public void setBenchmarkDiscount(String benchmarkDiscount) {
		this.benchmarkDiscount = benchmarkDiscount;
	}
	
}
