package com.singlee.ifs.model;

import java.io.Serializable;

public class SecmStatus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String secid;
	
	private String choisRefNo;
	
	private String choisHisNo;
	
	private String sendtime;
	
	private String issend;
	
	private String isresend;
	
	private String sendresult;
	
	private String sendmsg;
	
	private String tablename;
	
	private int upcount;
	
	private String upown;
	 
	private String uptime;
	
	private String inputtime;
	
	private String recvmsg;
	
	public String getRecvmsg() {
		return recvmsg;
	}

	public void setRecvmsg(String recvmsg) {
		this.recvmsg = recvmsg;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getChoisRefNo() {
		return choisRefNo;
	}

	public void setChoisRefNo(String choisRefNo) {
		this.choisRefNo = choisRefNo;
	}

	public String getChoisHisNo() {
		return choisHisNo;
	}

	public void setChoisHisNo(String choisHisNo) {
		this.choisHisNo = choisHisNo;
	}

	public String getSendtime() {
		return sendtime;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIsresend() {
		return isresend;
	}

	public void setIsresend(String isresend) {
		this.isresend = isresend;
	}

	public String getSendresult() {
		return sendresult;
	}

	public void setSendresult(String sendresult) {
		this.sendresult = sendresult;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public int getUpcount() {
		return upcount;
	}

	public void setUpcount(int upcount) {
		this.upcount = upcount;
	}

	public String getUpown() {
		return upown;
	}

	public void setUpown(String upown) {
		this.upown = upown;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

}
