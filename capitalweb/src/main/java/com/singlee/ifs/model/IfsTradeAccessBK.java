package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_TRADE_ACCESS_BK")//同业授信信息登记表
public class IfsTradeAccessBK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 核心客户号
	 */
	private String coreClntNo;
	/**
	 * 公司名称
	 */
	private String conm;
	/**
	 * 业务类型
	 */
	private String bussType;
	/**
	 * 业务类型名称
	 */
	private String bussTpnm;
	/**
	 * 机构码
	 */
	private String branchCode;
	/**
	 * 机构名称
	 */
	private String branchName;
	/**
	 * 包含下级标志
	 */
	private String inclSubFlg;
	/**
	 * 审批日期
	 */
	private String approvalDate;
	public String getCoreClntNo() {
		return coreClntNo;
	}
	public void setCoreClntNo(String coreClntNo) {
		this.coreClntNo = coreClntNo;
	}
	public String getConm() {
		return conm;
	}
	public void setConm(String conm) {
		this.conm = conm;
	}
	public String getBussType() {
		return bussType;
	}
	public void setBussType(String bussType) {
		this.bussType = bussType;
	}
	public String getBussTpnm() {
		return bussTpnm;
	}
	public void setBussTpnm(String bussTpnm) {
		this.bussTpnm = bussTpnm;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getInclSubFlg() {
		return inclSubFlg;
	}
	public void setInclSubFlg(String inclSubFlg) {
		this.inclSubFlg = inclSubFlg;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

}
