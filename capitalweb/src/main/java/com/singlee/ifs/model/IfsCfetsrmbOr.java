package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
@Entity
@Table(name = "IFS_CFETSRMB_OR")
public class IfsCfetsrmbOr extends IfsCfetsrmbOrKey implements Serializable {
    private String forDate;

    private String positiveInst;

    private String positiveTrader;

    private String positiveTel;

    private String positiveFax;

    private String positiveCorp;

    private String positiveAddr;

    private String reverseInst;

    private String reverseTrader;

    private String reverseTel;

    private String reverseFax;

    private String reverseCorp;

    private String reverseAddr;

    private String bondCode;

    private String bondName;

    private String tradingProduct;

    private String tenor;

    private BigDecimal repoRate;

    private BigDecimal totalFaceValue;

    private BigDecimal firstCleanPrice;

    private BigDecimal firstYield;

    private BigDecimal firstAccuredInterest;

    private BigDecimal secondCleanPrice;

    private BigDecimal secondYield;

    private BigDecimal secondAccuredInterest;

    private BigDecimal firstDirtyPrice;

    private BigDecimal firstSettlementAmount;

    private String firstSettlementMethod;

    private BigDecimal secondDirtyPrice;

    private BigDecimal secondSettlementAmount;

    private String secondSettlementMethod;

    private String firstSettlementDate;

    private String secondSettlementDate;

    private BigDecimal occupancyDays;

    private String positiveAccname;

    private String positiveOpenBank;

    private String positiveAccnum;

    private String positivePsnum;

    private String positiveCaname;

    private String positiveCustname;

    private String positiveCanum;

    private String reverseAccname;

    private String reverseOpenBank;

    private String reverseAccnum;

    private String reversePsnum;

    private String reverseCaname;

    private String reverseCustname;

    private String reverseCanum;
    
    private String invtype;
    
    private String bondStatcode;
    
    private String bondErrorcode;
    
    //权重
    private	String weight;

    //占用授信主体
  	private String custNo;
    //占用授信主体类型
    private String custType;
  	//审批单号
  	private String applyNo;
  	//产品名称
  	private String applyProd;
  	
  	private String bondCost;
  	
  	private String bondProduct;
  	
  	private String bondProdType;
  	
  	private String bondPort;
  	
  	private String basis;
  	
  //DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	//价格偏离度
	private String priceDeviation;
	
	@Transient
	private BigDecimal comcheck;
	
	@Transient
	private BigDecimal matcheck;
	
	@Transient
	private BigDecimal comamount;
	
	@Transient
	private BigDecimal matamount;
	
	@Transient
	private String  prdNo;
	
	private String  cfetsno;

	/**
	 * 额度占用类型（0:不占，1：发行人，2：交易对手）
	 */
	private String quotaOccupyType;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	/**
	 * 定价基准类型: 1-Shibor,2-Libor,3-Hibor,4-Euribor,5-LPR,
	 * 6-CHN Treasury Rate,7-存款基准利率,8-贷款基准利率,9-再贷款利率,10-其他
	 */
	private String pricingStandardType;

    //前置产品编号
    private String fPrdCode;

    public String getfPrdCode() {
        return fPrdCode;
    }

    public void setfPrdCode(String fPrdCode) {
        this.fPrdCode = fPrdCode;
    }

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	public String getQuotaOccupyType() {
		return quotaOccupyType;
	}

	public void setQuotaOccupyType(String quotaOccupyType) {
		this.quotaOccupyType = quotaOccupyType;
	}

	public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	private static final long serialVersionUID = 1L;


    public String getPositiveInst() {
        return positiveInst;
    }

    public void setPositiveInst(String positiveInst) {
        this.positiveInst = positiveInst == null ? null : positiveInst.trim();
    }

    public String getPositiveTrader() {
        return positiveTrader;
    }

    public void setPositiveTrader(String positiveTrader) {
        this.positiveTrader = positiveTrader == null ? null : positiveTrader.trim();
    }

    public String getPositiveTel() {
        return positiveTel;
    }

    public void setPositiveTel(String positiveTel) {
        this.positiveTel = positiveTel == null ? null : positiveTel.trim();
    }

    public String getPositiveFax() {
        return positiveFax;
    }

    public void setPositiveFax(String positiveFax) {
        this.positiveFax = positiveFax == null ? null : positiveFax.trim();
    }

    public String getPositiveCorp() {
        return positiveCorp;
    }

    public void setPositiveCorp(String positiveCorp) {
        this.positiveCorp = positiveCorp == null ? null : positiveCorp.trim();
    }

    public String getPositiveAddr() {
        return positiveAddr;
    }

    public void setPositiveAddr(String positiveAddr) {
        this.positiveAddr = positiveAddr == null ? null : positiveAddr.trim();
    }

    public String getReverseInst() {
        return reverseInst;
    }

    public void setReverseInst(String reverseInst) {
        this.reverseInst = reverseInst == null ? null : reverseInst.trim();
    }

    public String getReverseTrader() {
        return reverseTrader;
    }

    public void setReverseTrader(String reverseTrader) {
        this.reverseTrader = reverseTrader == null ? null : reverseTrader.trim();
    }

    public String getReverseTel() {
        return reverseTel;
    }

    public void setReverseTel(String reverseTel) {
        this.reverseTel = reverseTel == null ? null : reverseTel.trim();
    }

    public String getReverseFax() {
        return reverseFax;
    }

    public void setReverseFax(String reverseFax) {
        this.reverseFax = reverseFax == null ? null : reverseFax.trim();
    }

    public String getReverseCorp() {
        return reverseCorp;
    }

    public void setReverseCorp(String reverseCorp) {
        this.reverseCorp = reverseCorp == null ? null : reverseCorp.trim();
    }

    public String getReverseAddr() {
        return reverseAddr;
    }

    public void setReverseAddr(String reverseAddr) {
        this.reverseAddr = reverseAddr == null ? null : reverseAddr.trim();
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode == null ? null : bondCode.trim();
    }

    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName == null ? null : bondName.trim();
    }

    public String getTradingProduct() {
        return tradingProduct;
    }

    public void setTradingProduct(String tradingProduct) {
        this.tradingProduct = tradingProduct == null ? null : tradingProduct.trim();
    }

    public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getRepoRate() {
        return repoRate;
    }

    public void setRepoRate(BigDecimal repoRate) {
        this.repoRate = repoRate;
    }

    public BigDecimal getTotalFaceValue() {
        return totalFaceValue;
    }

    public void setTotalFaceValue(BigDecimal totalFaceValue) {
        this.totalFaceValue = totalFaceValue;
    }

    public BigDecimal getFirstCleanPrice() {
        return firstCleanPrice;
    }

    public void setFirstCleanPrice(BigDecimal firstCleanPrice) {
        this.firstCleanPrice = firstCleanPrice;
    }

    public BigDecimal getFirstYield() {
        return firstYield;
    }

    public void setFirstYield(BigDecimal firstYield) {
        this.firstYield = firstYield;
    }

    public BigDecimal getFirstAccuredInterest() {
        return firstAccuredInterest;
    }

    public void setFirstAccuredInterest(BigDecimal firstAccuredInterest) {
        this.firstAccuredInterest = firstAccuredInterest;
    }

    public BigDecimal getSecondCleanPrice() {
        return secondCleanPrice;
    }

    public void setSecondCleanPrice(BigDecimal secondCleanPrice) {
        this.secondCleanPrice = secondCleanPrice;
    }

    public BigDecimal getSecondYield() {
        return secondYield;
    }

    public void setSecondYield(BigDecimal secondYield) {
        this.secondYield = secondYield;
    }

    public BigDecimal getSecondAccuredInterest() {
        return secondAccuredInterest;
    }

    public void setSecondAccuredInterest(BigDecimal secondAccuredInterest) {
        this.secondAccuredInterest = secondAccuredInterest;
    }

    public BigDecimal getFirstDirtyPrice() {
        return firstDirtyPrice;
    }

    public void setFirstDirtyPrice(BigDecimal firstDirtyPrice) {
        this.firstDirtyPrice = firstDirtyPrice;
    }

    public BigDecimal getFirstSettlementAmount() {
        return firstSettlementAmount;
    }

    public void setFirstSettlementAmount(BigDecimal firstSettlementAmount) {
        this.firstSettlementAmount = firstSettlementAmount;
    }

    public String getFirstSettlementMethod() {
        return firstSettlementMethod;
    }

    public void setFirstSettlementMethod(String firstSettlementMethod) {
        this.firstSettlementMethod = firstSettlementMethod == null ? null : firstSettlementMethod.trim();
    }

    public BigDecimal getSecondDirtyPrice() {
        return secondDirtyPrice;
    }

    public void setSecondDirtyPrice(BigDecimal secondDirtyPrice) {
        this.secondDirtyPrice = secondDirtyPrice;
    }

    public BigDecimal getSecondSettlementAmount() {
        return secondSettlementAmount;
    }

    public void setSecondSettlementAmount(BigDecimal secondSettlementAmount) {
        this.secondSettlementAmount = secondSettlementAmount;
    }

    public String getSecondSettlementMethod() {
        return secondSettlementMethod;
    }

    public void setSecondSettlementMethod(String secondSettlementMethod) {
        this.secondSettlementMethod = secondSettlementMethod == null ? null : secondSettlementMethod.trim();
    }

    public String getFirstSettlementDate() {
        return firstSettlementDate;
    }

    public void setFirstSettlementDate(String firstSettlementDate) {
        this.firstSettlementDate = firstSettlementDate == null ? null : firstSettlementDate.trim();
    }

    public String getSecondSettlementDate() {
        return secondSettlementDate;
    }

    public void setSecondSettlementDate(String secondSettlementDate) {
        this.secondSettlementDate = secondSettlementDate == null ? null : secondSettlementDate.trim();
    }

    public BigDecimal getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(BigDecimal occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getPositiveAccname() {
        return positiveAccname;
    }

    public void setPositiveAccname(String positiveAccname) {
        this.positiveAccname = positiveAccname == null ? null : positiveAccname.trim();
    }

    public String getPositiveOpenBank() {
        return positiveOpenBank;
    }

    public void setPositiveOpenBank(String positiveOpenBank) {
        this.positiveOpenBank = positiveOpenBank == null ? null : positiveOpenBank.trim();
    }

    public String getPositiveAccnum() {
        return positiveAccnum;
    }

    public void setPositiveAccnum(String positiveAccnum) {
        this.positiveAccnum = positiveAccnum == null ? null : positiveAccnum.trim();
    }

    public String getPositivePsnum() {
        return positivePsnum;
    }

    public void setPositivePsnum(String positivePsnum) {
        this.positivePsnum = positivePsnum == null ? null : positivePsnum.trim();
    }

    public String getPositiveCaname() {
        return positiveCaname;
    }

    public void setPositiveCaname(String positiveCaname) {
        this.positiveCaname = positiveCaname == null ? null : positiveCaname.trim();
    }

    public String getPositiveCustname() {
        return positiveCustname;
    }

    public void setPositiveCustname(String positiveCustname) {
        this.positiveCustname = positiveCustname == null ? null : positiveCustname.trim();
    }

    public String getPositiveCanum() {
        return positiveCanum;
    }

    public void setPositiveCanum(String positiveCanum) {
        this.positiveCanum = positiveCanum == null ? null : positiveCanum.trim();
    }

    public String getReverseAccname() {
        return reverseAccname;
    }

    public void setReverseAccname(String reverseAccname) {
        this.reverseAccname = reverseAccname == null ? null : reverseAccname.trim();
    }

    public String getReverseOpenBank() {
        return reverseOpenBank;
    }

    public void setReverseOpenBank(String reverseOpenBank) {
        this.reverseOpenBank = reverseOpenBank == null ? null : reverseOpenBank.trim();
    }

    public String getReverseAccnum() {
        return reverseAccnum;
    }

    public void setReverseAccnum(String reverseAccnum) {
        this.reverseAccnum = reverseAccnum == null ? null : reverseAccnum.trim();
    }

    public String getReversePsnum() {
        return reversePsnum;
    }

    public void setReversePsnum(String reversePsnum) {
        this.reversePsnum = reversePsnum == null ? null : reversePsnum.trim();
    }

    public String getReverseCaname() {
        return reverseCaname;
    }

    public void setReverseCaname(String reverseCaname) {
        this.reverseCaname = reverseCaname == null ? null : reverseCaname.trim();
    }

    public String getReverseCustname() {
        return reverseCustname;
    }

    public void setReverseCustname(String reverseCustname) {
        this.reverseCustname = reverseCustname == null ? null : reverseCustname.trim();
    }

    public String getReverseCanum() {
        return reverseCanum;
    }

    public void setReverseCanum(String reverseCanum) {
        this.reverseCanum = reverseCanum == null ? null : reverseCanum.trim();
    }

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setBondStatcode(String bondStatcode) {
		this.bondStatcode = bondStatcode;
	}

	public String getBondStatcode() {
		return bondStatcode;
	}

	public void setBondErrorcode(String bondErrorcode) {
		this.bondErrorcode = bondErrorcode;
	}

	public String getBondErrorcode() {
		return bondErrorcode;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getBondCost() {
		return bondCost;
	}

	public void setBondCost(String bondCost) {
		this.bondCost = bondCost;
	}

	public String getBondProduct() {
		return bondProduct;
	}

	public void setBondProduct(String bondProduct) {
		this.bondProduct = bondProduct;
	}

	public String getBondProdType() {
		return bondProdType;
	}

	public void setBondProdType(String bondProdType) {
		this.bondProdType = bondProdType;
	}

	public String getBondPort() {
		return bondPort;
	}

	public void setBondPort(String bondPort) {
		this.bondPort = bondPort;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}
	
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	public String getPriceDeviation() {
		return priceDeviation==null?"":priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation==null?"":priceDeviation;
	}

	public BigDecimal getComcheck() {
		return comcheck;
	}

	public void setComcheck(BigDecimal comcheck) {
		this.comcheck = comcheck;
	}

	public BigDecimal getMatcheck() {
		return matcheck;
	}

	public void setMatcheck(BigDecimal matcheck) {
		this.matcheck = matcheck;
	}

	public BigDecimal getComamount() {
		return comamount;
	}

	public void setComamount(BigDecimal comamount) {
		this.comamount = comamount;
	}

	public BigDecimal getMatamount() {
		return matamount;
	}

	public void setMatamount(BigDecimal matamount) {
		this.matamount = matamount;
	}

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

	public String getPricingStandardType() {
		return pricingStandardType;
	}

	public void setPricingStandardType(String pricingStandardType) {
		this.pricingStandardType = pricingStandardType;
	}
}