package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSFX_FWD")
public class IfsCfetsfxFwd  extends FxCurrenyBasebean implements Serializable {
	private static final long serialVersionUID = 1L;
    //交易单号
    private String ticketId;
      // 交易状态
    private String dealTransType;
   // 本方机构
    private String instId;
    //对方机构
    private String counterpartyInstId;
   //本方交易员
    private String dealer;
    //对方交易员
    private String counterpartyDealer;
    
	private BigDecimal price;
	//价格
	private BigDecimal buyAmount;
	//买入方向金额
	private BigDecimal sellAmount;
	//卖出方向金额
	private String sellDirection;
	//卖出方向
	private String buyDirection;
	//买入方向
	private String tenorContract;
	//期限/合约名称
	private String valueDate;
	//起息日
	private String ccy;
	//交易币种
	private String opicsccy;
	//币种1(放重要币种)
	private String opicsctrccy;
	//币种2(放次要币种)
	//权重
	private String weight;
	//损益计算方法
	private String plmethod;
	//占用授信主体
	private String custNo;
	//占用授信主体类型
	private String custType;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//交易期限
	private String fwdPeriod;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	//升贴水点差
	private String spread;
	
	//净额清算路径
	private String nettingAddr;
	//成交汇率
	private String exchangeRate;
	
	//未交割头寸损益
	private String profitLoss;
	
	@Transient
	private String  prdNo;
	
	private String  cfetscn;
	
	//购售业务种类
	private String  transKind;
	//购售用途
	private String  purposeCode;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	//结算方式
	private String deliveryType;

	//交易后确认标识
	private String cfetsCnfmIndicator;

	//前置产品编号
	private String fPrdCode;
	/**
	 * CFETS期限代码
	 */
	private String tenorCode;

	public String getTenorCode() {
		return tenorCode;
	}

	public void setTenorCode(String tenorCode) {
		this.tenorCode = tenorCode;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getCfetsCnfmIndicator() {
		return cfetsCnfmIndicator;
	}

	public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
		this.cfetsCnfmIndicator = cfetsCnfmIndicator;
	}

	@Override
	public String getDeliveryType() {
		return deliveryType;
	}

	@Override
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	public String getCfetscn() {
		return cfetscn;
	}

	public void setCfetscn(String cfetscn) {
		this.cfetscn = cfetscn;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getPlmethod() {
		return plmethod;
	}

	public void setPlmethod(String plmethod) {
		this.plmethod = plmethod;
	}

	public String getSellDirection() {
		return sellDirection;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setSellDirection(String sellDirection) {
		this.sellDirection = sellDirection;
	}

	public String getBuyDirection() {
		return buyDirection;
	}

	public void setBuyDirection(String buyDirection) {
		this.buyDirection = buyDirection;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getBuyAmount() {
		return buyAmount;
	}

	public void setBuyAmount(BigDecimal buyAmount) {
		this.buyAmount = buyAmount;
	}

	public BigDecimal getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(BigDecimal sellAmount) {
		this.sellAmount = sellAmount;
	}

	public String getTenorContract() {
		return tenorContract;
	}

	public void setTenorContract(String tenorContract) {
		this.tenorContract = tenorContract == null ? null : tenorContract.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

	public String getOpicsccy() {
		return opicsccy;
	}

	public void setOpicsccy(String opicsccy) {
		this.opicsccy = opicsccy;
	}

	public String getOpicsctrccy() {
		return opicsctrccy;
	}

	public void setOpicsctrccy(String opicsctrccy) {
		this.opicsctrccy = opicsctrccy;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getFwdPeriod() {
		return fwdPeriod;
	}

	public void setFwdPeriod(String fwdPeriod) {
		this.fwdPeriod = fwdPeriod;
	}

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getSpread() {
		return spread;
	}

	public String getNettingAddr() {
		return nettingAddr;
	}

	public void setNettingAddr(String nettingAddr) {
		this.nettingAddr = nettingAddr;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public String getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(String profitLoss) {
		this.profitLoss = profitLoss;
	}

	public String getTransKind() {
		return transKind;
	}

	public void setTransKind(String transKind) {
		this.transKind = transKind;
	}

	public String getPurposeCode() {
		return purposeCode;
	}

	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}

    @Override
    public String getTicketId() {
        return ticketId;
    }

    @Override
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public String getDealTransType() {
        return dealTransType;
    }

    @Override
    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    @Override
    public String getInstId() {
        return instId;
    }

    @Override
    public void setInstId(String instId) {
        this.instId = instId;
    }

    @Override
    public String getCounterpartyInstId() {
        return counterpartyInstId;
    }

    @Override
    public void setCounterpartyInstId(String counterpartyInstId) {
        this.counterpartyInstId = counterpartyInstId;
    }

    @Override
    public String getDealer() {
        return dealer;
    }

    @Override
    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    @Override
    public String getCounterpartyDealer() {
        return counterpartyDealer;
    }

    @Override
    public void setCounterpartyDealer(String counterpartyDealer) {
        this.counterpartyDealer = counterpartyDealer;
    }

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}
}