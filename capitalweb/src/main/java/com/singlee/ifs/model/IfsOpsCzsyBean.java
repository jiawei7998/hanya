package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * @author copysun
 */
public class IfsOpsCzsyBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215431L;

	private String pdealno;
	private String iinvtype;
	private String acctdesc;
	private String isecid;
	private String descr;
	private String costamt1;
	private String costamt2;
	private String diff;
	private String settdate;
	private String br;
	private String secid;
	private String ccy;
	private String cost;
	private String product;
	private String prodtype;
	private String invtype;

	private String glno;


	public String getPdealno() {
		return pdealno;
	}

	public void setPdealno(String pdealno) {
		this.pdealno = pdealno;
	}

	public String getIinvtype() {
		return iinvtype;
	}

	public void setIinvtype(String iinvtype) {
		this.iinvtype = iinvtype;
	}

	public String getAcctdesc() {
		return acctdesc;
	}

	public void setAcctdesc(String acctdesc) {
		this.acctdesc = acctdesc;
	}

	public String getIsecid() {
		return isecid;
	}

	public void setIsecid(String isecid) {
		this.isecid = isecid;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getCostamt1() {
		return costamt1;
	}

	public void setCostamt1(String costamt1) {
		this.costamt1 = costamt1;
	}

	public String getCostamt2() {
		return costamt2;
	}

	public void setCostamt2(String costamt2) {
		this.costamt2 = costamt2;
	}

	public String getDiff() {
		return diff;
	}

	public void setDiff(String diff) {
		this.diff = diff;
	}

	public String getSettdate() {
		return settdate;
	}

	public void setSettdate(String settdate) {
		this.settdate = settdate;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}
}
