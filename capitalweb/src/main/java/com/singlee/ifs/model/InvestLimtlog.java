package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity	
@Table(name = "IFS_INVEST_LIMT_LOG")
public class InvestLimtlog implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * 账户类型
	 */
	private String invtype;
	
	/**
	 * 信用债投资
	 */
	private String creditInvestLimit;
	
	/**
	 * 投资限额
	 */
	private String investLimit;
	
	/**
	 * 信用债AA级及以下债券余额
	 */
	private String lowLevelLimit;
	
	
	/**
	 * 债券久期限额
	 */
	private String duration;
	
	/**
	 * DV01限额
	 */
	private String dv01;
	
	/**
	 * 年止损
	 */
	private String convexityYear;
	
	/**
	 * 月止损
	 */
	private String convexityMonth;
	
	/**
	 * 债券类型，1债券投资，2存单投资
	 */
	private String bondFlag;
	
	/**
	 * 操作时间
	 */
	private String oprTime;
	
	/**
	 * 序列号
	 */
	private String limitId;
	
	/**
	 * 交易流水号
	 */
	private String ticketId;
	
	/**
	 * 备用1
	 */
	private String noUse1;
	
	/**
	 * 备用2
	 */
	private String noUse2;
	
	/**
	 * 备用3
	 */
	private String noUse3;

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getInvestLimit() {
		return investLimit;
	}

	public void setInvestLimit(String investLimit) {
		this.investLimit = investLimit;
	}

	public String getLowLevelLimit() {
		return lowLevelLimit;
	}

	public void setLowLevelLimit(String lowLevelLimit) {
		this.lowLevelLimit = lowLevelLimit;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDv01() {
		return dv01;
	}

	public void setDv01(String dv01) {
		this.dv01 = dv01;
	}

	public String getConvexityYear() {
		return convexityYear;
	}

	public void setConvexityYear(String convexityYear) {
		this.convexityYear = convexityYear;
	}

	public String getConvexityMonth() {
		return convexityMonth;
	}

	public void setConvexityMonth(String convexityMonth) {
		this.convexityMonth = convexityMonth;
	}

	public String getBondFlag() {
		return bondFlag;
	}

	public void setBondFlag(String bondFlag) {
		this.bondFlag = bondFlag;
	}

	public String getOprTime() {
		return oprTime;
	}

	public void setOprTime(String oprTime) {
		this.oprTime = oprTime;
	}

	public String getNoUse1() {
		return noUse1;
	}

	public void setNoUse1(String noUse1) {
		this.noUse1 = noUse1;
	}

	public String getNoUse2() {
		return noUse2;
	}

	public void setNoUse2(String noUse2) {
		this.noUse2 = noUse2;
	}

	public String getNoUse3() {
		return noUse3;
	}

	public void setNoUse3(String noUse3) {
		this.noUse3 = noUse3;
	}

	public String getCreditInvestLimit() {
		return creditInvestLimit;
	}

	public void setCreditInvestLimit(String creditInvestLimit) {
		this.creditInvestLimit = creditInvestLimit;
	}

	public String getLimitId() {
		return limitId;
	}

	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	
	
	
}