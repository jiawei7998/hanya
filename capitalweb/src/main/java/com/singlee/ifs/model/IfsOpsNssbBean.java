package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * @author copysun
 */
public class IfsOpsNssbBean implements Serializable {

	private static final long serialVersionUID = 1901552160942215431L;

	private String glno;
	private String ps;
	private String amount;

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}
