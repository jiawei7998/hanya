package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IFS_WD_SECTYPE")
public class IfsWdSectype implements Serializable{
	private static final long serialVersionUID = -6961501957022205461L;
	private String  wdsecid;
	private String  type;
	private String  subtype;
	private Date  rpGenDatetime;
	@Transient
	private String vDate;
	
	public String getWdsecid() {
		return wdsecid;
	}
	public void setWdsecid(String wdsecid) {
		this.wdsecid = wdsecid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubtype() {
		return subtype;
	}
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
	public Date getRpGenDatetime() {
		return rpGenDatetime;
	}
	public void setRpGenDatetime(Date rpGenDatetime) {
		this.rpGenDatetime = rpGenDatetime;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getvDate() {
		return vDate;
	}

}
