package com.singlee.ifs.model;

import java.io.Serializable;


public class IfsApproveElement implements Serializable{
   public String id;                   // ID
   public String product;              // 产品
   public String type;                 // 操作类型
   public String productsComments;
   public String commentsGroup;
   public String tradeGroup;       // 审批组合
   public String inputTrader;          // 录入人员
   public String inputTime;            // 录入时间
   private static final long serialVersionUID = 1L;
   



public String getProductsComments() {
	return productsComments;
}
public void setProductsComments(String productsComments) {
	this.productsComments = productsComments;
}
public String getCommentsGroup() {
	return commentsGroup;
}
public void setCommentsGroup(String commentsGroup) {
	this.commentsGroup = commentsGroup;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getProduct() {
	return product;
}
public void setProduct(String product) {
	this.product = product;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}

public String getTradeGroup() {
	return tradeGroup;
}
public void setTradeGroup(String tradeGroup) {
	this.tradeGroup = tradeGroup;
}
public String getInputTrader() {
	return inputTrader;
}
public void setInputTrader(String inputTrader) {
	this.inputTrader = inputTrader;
}
public String getInputTime() {
	return inputTime;
}
public void setInputTime(String inputTime) {
	this.inputTime = inputTime;
}


}
