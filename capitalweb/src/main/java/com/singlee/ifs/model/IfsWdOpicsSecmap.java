package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IFS_WD_OPICS_SECMAP")
public class IfsWdOpicsSecmap implements Serializable{
	private static final long serialVersionUID = -354930641001419747L;
	//映射ID
	private String mapid;
	//映射名称
	private String mapname;
	 //万得字段ID
	private String wdid;
	//万得字段名称
	private String wdname;
	//opics字段ID
	private String opicsid;
	 //opics字段名称
	private String opicsname;
	 //备注1
	private String remark1;
	 //备注2
	private String remark2;
	public String getMapid() {
		return mapid;
	}
	public void setMapid(String mapid) {
		this.mapid = mapid;
	}
	public String getMapname() {
		return mapname;
	}
	public void setMapname(String mapname) {
		this.mapname = mapname;
	}
	public String getWdid() {
		return wdid;
	}
	public void setWdid(String wdid) {
		this.wdid = wdid;
	}
	public String getWdname() {
		return wdname;
	}
	public void setWdname(String wdname) {
		this.wdname = wdname;
	}
	public String getOpicsid() {
		return opicsid;
	}
	public void setOpicsid(String opicsid) {
		this.opicsid = opicsid;
	}
	public String getOpicsname() {
		return opicsname;
	}
	public void setOpicsname(String opicsname) {
		this.opicsname = opicsname;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

}
