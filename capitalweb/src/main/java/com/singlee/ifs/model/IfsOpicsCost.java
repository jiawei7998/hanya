package com.singlee.ifs.model;

import com.singlee.capital.common.util.TreeInterface;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/***
 * opics 系统成本中心表
 * @author singlee4
 *
 */

public class IfsOpicsCost implements Serializable, TreeInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 成本中心
	 */
	private String costcent;
	
	/**
	 * 成本中心
	 */
	private String costcentpid;
	
	/**
	 * 成本中心描述
	 */
	private String costdesc;
	/**
	 * 成本中心中文描述
	 */
	private String costdesccn;
	/**
	 * 最后维护时间
	 */
	private Date lstmntdte;
	/**
	 * 营业单位
	 */
	private String busunit;
	/**
	 * 组织单位
	 */
	private String orgunit;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 处理状态
	 */
	private String status;
	/**
	 * 备注
	 */
	private String remark;
	
	@Transient
	private List<IfsOpicsCost> children;
	
	public String getCostcent() {
		return costcent;
	}
	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}
	public String getCostcentpid() {
		return costcentpid;
	}
	public void setCostcentpid(String costcentpid) {
		this.costcentpid = costcentpid;
	}
	public String getCostdesc() {
		return costdesc;
	}
	public void setCostdesc(String costdesc) {
		this.costdesc = costdesc;
	}
	public String getCostdesccn() {
		return costdesccn;
	}
	public void setCostdesccn(String costdesccn) {
		this.costdesccn = costdesccn;
	}
	public Date getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public String getBusunit() {
		return busunit;
	}
	public void setBusunit(String busunit) {
		this.busunit = busunit;
	}
	public String getOrgunit() {
		return orgunit;
	}
	public void setOrgunit(String orgunit) {
		this.orgunit = orgunit;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<IfsOpicsCost> getChildren() {
		return children;
	}
	public void setChildren(List<IfsOpicsCost> children) {
		this.children = children;
	}
	@Override
	public String getId() {
		return getCostcent();
	}
	@Override
	public String getParentId() {
		return getCostcentpid();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<IfsOpicsCost>) list);
	}
	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}
	@Override
	public String getText() {
		return costcent;
	}
	@Override
	public String getValue() {
		return costcentpid;
	}
	@Override
	public int getSort() {
		return StringUtils.length(getCostcent());
	}
	@Override
	public String getIconCls() {
		return "";
	}


}
