package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 存单明细
 * 
 * @author xuqq
 * 
 * @date 2018-07-19
 */
@Entity
@Table(name = "IFS_CFETSRMB_DP_OFFER")
public class IfsCfetsrmbDpOffer implements Serializable {

	private String ticketId;
	private String seq;
	private String dealNo;
	private String lendAddr;
	private String lendCorp;
	private String lendFax;
	private String lendTel;
	private String lendTrader;
	private String lendInst;
	@Transient
	private String lendInstName;
	private String lendCanum;
	private String lendCustname;
	private String lendCaname;
	private String lendPsnum;
	private String lendAccnum;
	private String lendOpenBank;
	private String lendAccname;
	private String custNo;
	private String weight;
	private String applyProd;
	private String applyNo;
	private BigDecimal shouldPayMoney;
	private BigDecimal offerAmount;
	private String offerPhone;
	private String errorcode;
	private String statcode;
	private String dealSource;
	private String cost;
	private String prodType;
	private String product;
	private String port;
	private String ctrsacct;
	private String ctrsmeans;
	private String ccysacct;
	private String ccysmeans;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	private String br;

	//管理人类型
	private String manageType;
	//是否货币市场基金
	private String isCuryFund;
	
	private static final long serialVersionUID = 1L;
	
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId == null ? null : ticketId.trim();
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo == null ? null : dealNo.trim();
	}
	public String getLendAddr() {
		return lendAddr;
	}
	public void setLendAddr(String lendAddr) {
		this.lendAddr = lendAddr == null ? null : lendAddr.trim();
	}
	public String getLendCorp() {
		return lendCorp;
	}
	public void setLendCorp(String lendCorp) {
		this.lendCorp = lendCorp == null ? null : lendCorp.trim();
	}
	public String getLendFax() {
		return lendFax;
	}
	public void setLendFax(String lendFax) {
		this.lendFax = lendFax == null ? null : lendFax.trim();
	}
	public String getLendTel() {
		return lendTel;
	}
	public void setLendTel(String lendTel) {
		this.lendTel = lendTel == null ? null : lendTel.trim();
	}
	public String getLendTrader() {
		return lendTrader;
	}
	public void setLendTrader(String lendTrader) {
		this.lendTrader = lendTrader == null ? null : lendTrader.trim();
	}
	public String getLendInst() {
		return lendInst;
	}
	public void setLendInst(String lendInst) {
		this.lendInst = lendInst == null ? null : lendInst.trim();
	}
	public String getLendInstName() {
		return lendInstName;
	}
	public void setLendInstName(String lendInstName) {
		this.lendInstName = lendInstName;
	}
	public String getLendCanum() {
		return lendCanum;
	}
	public void setLendCanum(String lendCanum) {
		this.lendCanum = lendCanum == null ? null : lendCanum.trim();
	}
	public String getLendCustname() {
		return lendCustname;
	}
	public void setLendCustname(String lendCustname) {
		this.lendCustname = lendCustname == null ? null : lendCustname.trim();
	}
	public String getLendCaname() {
		return lendCaname;
	}
	public void setLendCaname(String lendCaname) {
		this.lendCaname = lendCaname == null ? null : lendCaname.trim();
	}
	public String getLendPsnum() {
		return lendPsnum;
	}
	public void setLendPsnum(String lendPsnum) {
		this.lendPsnum = lendPsnum == null ? null : lendPsnum.trim();
	}
	public String getLendAccnum() {
		return lendAccnum;
	}
	public void setLendAccnum(String lendAccnum) {
		this.lendAccnum = lendAccnum == null ? null : lendAccnum.trim();
	}
	public String getLendOpenBank() {
		return lendOpenBank;
	}
	public void setLendOpenBank(String lendOpenBank) {
		this.lendOpenBank = lendOpenBank == null ? null : lendOpenBank.trim();
	}
	public String getLendAccname() {
		return lendAccname;
	}
	public void setLendAccname(String lendAccname) {
		this.lendAccname = lendAccname == null ? null : lendAccname.trim();
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo == null ? null : custNo.trim();
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight == null ? null : weight.trim();
	}
	public String getApplyProd() {
		return applyProd;
	}
	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd == null ? null : applyProd.trim();
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo == null ? null : applyNo.trim();
	}
	public BigDecimal getShouldPayMoney() {
		return shouldPayMoney;
	}
	public void setShouldPayMoney(BigDecimal shouldPayMoney) {
		this.shouldPayMoney = shouldPayMoney;
	}
	public BigDecimal getOfferAmount() {
		return offerAmount;
	}
	public void setOfferAmount(BigDecimal offerAmount) {
		this.offerAmount = offerAmount;
	}
	public String getOfferPhone() {
		return offerPhone;
	}
	public void setOfferPhone(String offerPhone) {
		this.offerPhone = offerPhone == null ? null : offerPhone.trim();
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode== null ? null : errorcode.trim();
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode== null ? null : statcode.trim();
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost== null ? null : cost.trim();
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType== null ? null : prodType.trim();
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product== null ? null : product.trim();
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port== null ? null : port.trim();
	}
	public String getDealSource() {
		return dealSource;
	}
	public void setDealSource(String dealSource) {
		this.dealSource = dealSource== null ? null : dealSource.trim();
	}
	public String getCcysacct() {
		return ccysacct;
	}
	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct == null ? null : ccysacct.trim();
	}
	public String getCcysmeans() {
		return ccysmeans;
	}
	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans == null ? null : ccysmeans.trim();
	}
	public String getCtrsacct() {
		return ctrsacct;
	}
	public void setCtrsacct(String ctrsacct) {
		this.ctrsacct = ctrsacct== null ? null : ctrsacct.trim();
	}
	public String getCtrsmeans() {
		return ctrsmeans;
	}
	public void setCtrsmeans(String ctrsmeans) {
		this.ctrsmeans = ctrsmeans== null ? null : ctrsmeans.trim();
	}
	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public String getIsCuryFund() {
		return isCuryFund;
	}

	public void setIsCuryFund(String isCuryFund) {
		this.isCuryFund = isCuryFund;
	}
}
