package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(name="IFS_CUST_SETTLE_ACCT")
public class IfsCustSettle extends IfsBaseFlow  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cno;//交易对手号
	private String seq;//分序号
	private String cmneEn;// 客户英文简称 
	private String cmneCn;//客户中文名称 
	private String pbnkno;//付款行行号 
	private String pbnknm;//付款行行名 
	private String pacctno;//付款人账户号
	private String pacctnm;//付款人账户名
	private String pacctbankid;//cfets中付款行bankid
	private String rbnkno;// 收款行行号 
	private String rbnknm;//收款行行名
	private String racctno;//收款人账户号
	private String racctnm;//收款人账户名
	private String racctbankid;//cfets中收款行bankid
	private String cfetsCno;//cfets交易对手
	private String product;//
	private String isdefault;// 
	private String inbankFlag;//
	private String cityFlag;//
	private String dealFlag;//处理状态
	private Date inputdate;//
	private String note;//
	
	private String manager ;//经办用户
	private Date manageDate;//经办日期
	private String checker ;//复核用户
	private Date checkeDate;//复核日期
	
	
	public String getDealFlag() {
		return dealFlag;
	}
	public void setDealFlag(String dealFlag) {
		this.dealFlag = dealFlag;
	}
	@Override
    public String getCno() {
		return cno;
	}
	@Override
    public void setCno(String cno) {
		this.cno = cno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getCmneEn() {
		return cmneEn;
	}
	public void setCmneEn(String cmneEn) {
		this.cmneEn = cmneEn;
	}
	public String getCmneCn() {
		return cmneCn;
	}
	public void setCmneCn(String cmneCn) {
		this.cmneCn = cmneCn;
	}
	public String getPbnkno() {
		return pbnkno;
	}
	public void setPbnkno(String pbnkno) {
		this.pbnkno = pbnkno;
	}
	public String getPbnknm() {
		return pbnknm;
	}
	public void setPbnknm(String pbnknm) {
		this.pbnknm = pbnknm;
	}
	public String getPacctno() {
		return pacctno;
	}
	public void setPacctno(String pacctno) {
		this.pacctno = pacctno;
	}
	public String getPacctnm() {
		return pacctnm;
	}
	public void setPacctnm(String pacctnm) {
		this.pacctnm = pacctnm;
	}
	public String getPacctbankid() {
		return pacctbankid;
	}
	public void setPacctbankid(String pacctbankid) {
		this.pacctbankid = pacctbankid;
	}
	public String getRbnkno() {
		return rbnkno;
	}
	public void setRbnkno(String rbnkno) {
		this.rbnkno = rbnkno;
	}
	public String getRbnknm() {
		return rbnknm;
	}
	public void setRbnknm(String rbnknm) {
		this.rbnknm = rbnknm;
	}
	public String getRacctno() {
		return racctno;
	}
	public void setRacctno(String racctno) {
		this.racctno = racctno;
	}
	public String getRacctnm() {
		return racctnm;
	}
	public void setRacctnm(String racctnm) {
		this.racctnm = racctnm;
	}
	public String getRacctbankid() {
		return racctbankid;
	}
	public void setRacctbankid(String racctbankid) {
		this.racctbankid = racctbankid;
	}
	public String getCfetsCno() {
		return cfetsCno;
	}
	public void setCfetsCno(String cfetsCno) {
		this.cfetsCno = cfetsCno;
	}
	@Override
    public String getProduct() {
		return product;
	}
	@Override
    public void setProduct(String product) {
		this.product = product;
	}
	public String getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(String isdefault) {
		this.isdefault = isdefault;
	}
	public String getInbankFlag() {
		return inbankFlag;
	}
	public void setInbankFlag(String inbankFlag) {
		this.inbankFlag = inbankFlag;
	}
	public String getCityFlag() {
		return cityFlag;
	}
	public void setCityFlag(String cityFlag) {
		this.cityFlag = cityFlag;
	}
	public Date getInputdate() {
		return inputdate;
	}
	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}
	@Override
    public String getNote() {
		return note;
	}
	@Override
    public void setNote(String note) {
		this.note = note;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public Date getManageDate() {
		return manageDate;
	}
	public void setManageDate(Date manageDate) {
		this.manageDate = manageDate;
	}
	public String getChecker() {
		return checker;
	}
	public void setChecker(String checker) {
		this.checker = checker;
	}
	public Date getCheckeDate() {
		return checkeDate;
	}
	public void setCheckeDate(Date checkeDate) {
		this.checkeDate = checkeDate;
	}

}