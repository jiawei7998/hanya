package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.io.Serializable;

public class IfsCfetsfxSwap extends FxCurrenyBasebean implements Serializable {
    private static final long serialVersionUID = 1L;
     //交易单号
    private String ticketId;
      // 交易状态
    private String dealTransType;
   // 本方机构
    private String instId;
    //对方机构
    private String counterpartyInstId;
   //本方交易员
    private String dealer;
    //对方交易员
    private String counterpartyDealer;
    //日期
    private String forDate;
   // 时间
    private String forTime;
   // 交易模式
    private String tradingModel;
   // 交易方式
    private String tradingType;
   // 货币对
    private String currencyPair;
   // 交易币种
   private String ccy;
    //交易币种
    private String fwdRate;
    //远端汇率
    private String nearValuedate;
    //近端起息日
    private String direction;
    //交易方向
    private String fwdPrice;
    //远端交易金额
    private String nearRate;
    //近端汇率
    private String spread;
    //远端点差
    private String fwdValuedate;
    //远端起息日
    private String nearPrice;
    //近端交易金额
    private String sellCurreny;
    //清算货币
//  private String bankName1;
    //开户行名称1
    private String bankBic1;
    //开户行BIC CODE1
    private String dueBank1;
    //收款行名称1
    private String dueBankName1;
    //收款行名称BIC CODE1
    private String dueBankAccount1;
    //收款行账号1
    private String buyCurreny;
    //清算货币
    private String capitalBank1;
    //资金开户行1
    private String capitalBankName1;
    //资金账户户名1
    private String cnapsCode1;
    //支付系统行号1
    private String capitalAccount1;
    //资金账号1
    private String bankName2;
    //开户行名称2
    private String bankBic2;
    //开户行BIC CODE2
    private String dueBank2;
    //收款行名称2
    private String dueBankName2;
    //收款行名称BIC CODE2
    private String dueBankAccount2;
    //收款行账号
    private String capitalBank2;
    //资金开户行2
//  private String capitalBankName2;
    //资金账户户名2
//  private String cnapsCode2;
    //支付系统行号2
//  private String capitalAccount2;
    //资金账号2
    
    private String fwdReversePrice;
    //远端反向交易金额
    private String nearReversePrice;
    //近端反向交易金额
    private String opicsccy;
    //币种1(放重要币种)
    private String opicsctrccy;
    //币种2(放次要币种)
    //权重
    private String weight;
    //占用授信主体
    private String custNo;
    //占用授信主体类型
    private String custType;
    //审批单号
    private String applyNo;
    //产品名称
    private String applyProd;
    
    //DV01Risk
    private String DV01Risk;
    
    //止损
    private String lossLimit;
    
    //久期限
    private String longLimit;
    
    //风险程度
    private String riskDegree;
    
    //净额清算路径
    private String nettingAddr;
    
    //SPOT汇率
    private String price;
    //近端点差
    private String nearSpread;
    
    //掉期独有
    private String dueBank3;
    private String dueBankName3;
    private String dueBankAccount3;
    private String sellCurreny1;
    private String bankName4;
    private String bankBic4;
    private String dueBank4;
    private String dueBankName4;
    private String dueBankAccount4;
    private String intermediaryBankName3;
    private String intermediaryBankBicCode3;
    private String intermediaryBankName4;
    private String intermediaryBankBicCode4;
    private String buyCurreny1;
    private String bankName3;
    private String bankBic3;
    private String bankAccount3;
    //开户行行号3
    private String bankAccount4;
    //开户行行号4
    private String intermediaryBankAcctNo3;
    // 中间行行号3
    private String intermediaryBankAcctNo4;
    // 中间行行号4
    
    
    //期限
    private String tenor;
    
    //远期opics交易号
    private String farDealNo;
    
    //未交割头寸损益
    private String profitLoss;

    @Transient
    private String  prdNo;
    
	private String  cfetscn;
    
    //购售业务种类
    private String  transKind;
    //购售用途
    private String  purposeCode;

    /**
     * 五级分类
     */
    private String fiveLevelClass;

    /**
     * 归属部门
     */
    private String attributionDept;

    //结算方式
    private String deliveryType;

    //交易后确认标识
    private String cfetsCnfmIndicator;

    //前置产品编号
    private String fPrdCode;

    /**
     * CFETS近端期限代码
     */
    private String tenorCode;
    /**
     * CFETS远端期限代码
     */
    private String fwdTenorCode;

    public String getTenorCode() {
        return tenorCode;
    }

    public void setTenorCode(String tenorCode) {
        this.tenorCode = tenorCode;
    }

    public String getFwdTenorCode() {
        return fwdTenorCode;
    }

    public void setFwdTenorCode(String fwdTenorCode) {
        this.fwdTenorCode = fwdTenorCode;
    }

    public String getfPrdCode() {
        return fPrdCode;
    }

    public void setfPrdCode(String fPrdCode) {
        this.fPrdCode = fPrdCode;
    }

    public String getCfetsCnfmIndicator() {
        return cfetsCnfmIndicator;
    }

    public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
        this.cfetsCnfmIndicator = cfetsCnfmIndicator;
    }

    @Override
    public String getDeliveryType() {
        return deliveryType;
    }

    @Override
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getFiveLevelClass() {
        return fiveLevelClass;
    }

    public void setFiveLevelClass(String fiveLevelClass) {
        this.fiveLevelClass = fiveLevelClass;
    }

    public String getAttributionDept() {
        return attributionDept;
    }

    public void setAttributionDept(String attributionDept) {
        this.attributionDept = attributionDept;
    }

    public String getCfetscn() {
		return cfetscn;
	}

	public void setCfetscn(String cfetscn) {
		this.cfetscn = cfetscn;
	}

	public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getFwdRate() {
        return fwdRate;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public void setFwdRate(String fwdRate) {
        this.fwdRate = fwdRate;
    }

    public String getNearValuedate() {
        return nearValuedate;
    }

    public void setNearValuedate(String nearValuedate) {
        this.nearValuedate = nearValuedate;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getFwdPrice() {
        return fwdPrice;
    }

    public void setFwdPrice(String fwdPrice) {
        this.fwdPrice = fwdPrice;
    }

    public String getNearRate() {
        return nearRate;
    }

    public void setNearRate(String nearRate) {
        this.nearRate = nearRate;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }

    public String getFwdValuedate() {
        return fwdValuedate;
    }

    public void setFwdValuedate(String fwdValuedate) {
        this.fwdValuedate = fwdValuedate;
    }

    public String getNearPrice() {
        return nearPrice;
    }

    public void setNearPrice(String nearPrice) {
        this.nearPrice = nearPrice;
    }

    public String getFwdReversePrice() {
        return fwdReversePrice;
    }

    public void setFwdReversePrice(String fwdReversePrice) {
        this.fwdReversePrice = fwdReversePrice;
    }

    public String getNearReversePrice() {
        return nearReversePrice;
    }

    public void setNearReversePrice(String nearReversePrice) {
        this.nearReversePrice = nearReversePrice;
    }

    public String getOpicsccy() {
        return opicsccy;
    }

    public void setOpicsccy(String opicsccy) {
        this.opicsccy = opicsccy;
    }

    public String getOpicsctrccy() {
        return opicsctrccy;
    }

    public void setOpicsctrccy(String opicsctrccy) {
        this.opicsctrccy = opicsctrccy;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String getCustNo() {
        return custNo;
    }

    @Override
    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getApplyProd() {
        return applyProd;
    }

    public void setApplyProd(String applyProd) {
        this.applyProd = applyProd;
    }
    public String getDV01Risk() {
        return DV01Risk;
    }

    public void setDV01Risk(String dV01Risk) {
        this.DV01Risk = dV01Risk;
    }

    public String getLossLimit() {
        return lossLimit;
    }

    public void setLossLimit(String lossLimit) {
        this.lossLimit = lossLimit;
    }

    public String getLongLimit() {
        return longLimit;
    }

    public void setLongLimit(String longLimit) {
        this.longLimit = longLimit;
    }

    public String getRiskDegree() {
        return riskDegree;
    }

    public void setRiskDegree(String riskDegree) {
        this.riskDegree = riskDegree;
    }

    public String getNettingAddr() {
        return nettingAddr;
    }

    public void setNettingAddr(String nettingAddr) {
        this.nettingAddr = nettingAddr;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setNearSpread(String nearSpread) {
        this.nearSpread = nearSpread;
    }

    public String getNearSpread() {
        return nearSpread;
    }

    public String getDueBank3() {
        return dueBank3;
    }

    public void setDueBank3(String dueBank3) {
        this.dueBank3 = dueBank3;
    }

    public String getDueBankName3() {
        return dueBankName3;
    }

    public void setDueBankName3(String dueBankName3) {
        this.dueBankName3 = dueBankName3;
    }

    public String getDueBankAccount3() {
        return dueBankAccount3;
    }

    public void setDueBankAccount3(String dueBankAccount3) {
        this.dueBankAccount3 = dueBankAccount3;
    }

    public String getSellCurreny1() {
        return sellCurreny1;
    }

    public void setSellCurreny1(String sellCurreny1) {
        this.sellCurreny1 = sellCurreny1;
    }

    public String getBankName4() {
        return bankName4;
    }

    public void setBankName4(String bankName4) {
        this.bankName4 = bankName4;
    }

    public String getBankBic4() {
        return bankBic4;
    }

    public void setBankBic4(String bankBic4) {
        this.bankBic4 = bankBic4;
    }

    public String getDueBank4() {
        return dueBank4;
    }

    public void setDueBank4(String dueBank4) {
        this.dueBank4 = dueBank4;
    }

    public String getDueBankName4() {
        return dueBankName4;
    }

    public void setDueBankName4(String dueBankName4) {
        this.dueBankName4 = dueBankName4;
    }

    public String getDueBankAccount4() {
        return dueBankAccount4;
    }

    public void setDueBankAccount4(String dueBankAccount4) {
        this.dueBankAccount4 = dueBankAccount4;
    }

    public String getIntermediaryBankName3() {
        return intermediaryBankName3;
    }

    public void setIntermediaryBankName3(String intermediaryBankName3) {
        this.intermediaryBankName3 = intermediaryBankName3;
    }

    public String getIntermediaryBankBicCode3() {
        return intermediaryBankBicCode3;
    }

    public void setIntermediaryBankBicCode3(String intermediaryBankBicCode3) {
        this.intermediaryBankBicCode3 = intermediaryBankBicCode3;
    }

    public String getIntermediaryBankName4() {
        return intermediaryBankName4;
    }

    public void setIntermediaryBankName4(String intermediaryBankName4) {
        this.intermediaryBankName4 = intermediaryBankName4;
    }

    public String getIntermediaryBankBicCode4() {
        return intermediaryBankBicCode4;
    }

    public void setIntermediaryBankBicCode4(String intermediaryBankBicCode4) {
        this.intermediaryBankBicCode4 = intermediaryBankBicCode4;
    }

    public String getBuyCurreny1() {
        return buyCurreny1;
    }

    public void setBuyCurreny1(String buyCurreny1) {
        this.buyCurreny1 = buyCurreny1;
    }

    public String getBankName3() {
        return bankName3;
    }

    public void setBankName3(String bankName3) {
        this.bankName3 = bankName3;
    }

    public String getBankBic3() {
        return bankBic3;
    }

    public void setBankBic3(String bankBic3) {
        this.bankBic3 = bankBic3;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getTenor() {
        return tenor;
    }

    public String getFarDealNo() {
        return farDealNo;
    }

    public void setFarDealNo(String farDealNo) {
        this.farDealNo = farDealNo;
    }

    public String getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(String profitLoss) {
        this.profitLoss = profitLoss;
    }

    public String getBankAccount3() {
        return bankAccount3;
    }

    public void setBankAccount3(String bankAccount3) {
        this.bankAccount3 = bankAccount3;
    }

    public String getBankAccount4() {
        return bankAccount4;
    }

    public void setBankAccount4(String bankAccount4) {
        this.bankAccount4 = bankAccount4;
    }

    public String getIntermediaryBankAcctNo3() {
        return intermediaryBankAcctNo3;
    }

    public void setIntermediaryBankAcctNo3(String intermediaryBankAcctNo3) {
        this.intermediaryBankAcctNo3 = intermediaryBankAcctNo3;
    }

    public String getIntermediaryBankAcctNo4() {
        return intermediaryBankAcctNo4;
    }

    public void setIntermediaryBankAcctNo4(String intermediaryBankAcctNo4) {
        this.intermediaryBankAcctNo4 = intermediaryBankAcctNo4;
    }

    @Override
    public String getTicketId() {
        return ticketId;
    }

    @Override
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public String getDealTransType() {
        return dealTransType;
    }

    @Override
    public void setDealTransType(String dealTransType) {
        this.dealTransType = dealTransType;
    }

    @Override
    public String getInstId() {
        return instId;
    }

    @Override
    public void setInstId(String instId) {
        this.instId = instId;
    }

    @Override
    public String getCounterpartyInstId() {
        return counterpartyInstId;
    }

    @Override
    public void setCounterpartyInstId(String counterpartyInstId) {
        this.counterpartyInstId = counterpartyInstId;
    }

    @Override
    public String getDealer() {
        return dealer;
    }

    @Override
    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    @Override
    public String getCounterpartyDealer() {
        return counterpartyDealer;
    }

    @Override
    public void setCounterpartyDealer(String counterpartyDealer) {
        this.counterpartyDealer = counterpartyDealer;
    }

    @Override
    public String getForDate() {
        return forDate;
    }

    @Override
    public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    @Override
    public String getForTime() {
        return forTime;
    }

    @Override
    public void setForTime(String forTime) {
        this.forTime = forTime;
    }

    @Override
    public String getTradingModel() {
        return tradingModel;
    }

    @Override
    public void setTradingModel(String tradingModel) {
        this.tradingModel = tradingModel;
    }

    @Override
    public String getTradingType() {
        return tradingType;
    }

    @Override
    public void setTradingType(String tradingType) {
        this.tradingType = tradingType;
    }

    @Override
    public String getCurrencyPair() {
        return currencyPair;
    }

    @Override
    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    @Override
    public String getSellCurreny() {
        return sellCurreny;
    }

    @Override
    public void setSellCurreny(String sellCurreny) {
        this.sellCurreny = sellCurreny;
    }

    @Override
    public String getBankBic1() {
        return bankBic1;
    }

    @Override
    public void setBankBic1(String bankBic1) {
        this.bankBic1 = bankBic1;
    }

    @Override
    public String getDueBank1() {
        return dueBank1;
    }

    @Override
    public void setDueBank1(String dueBank1) {
        this.dueBank1 = dueBank1;
    }

    @Override
    public String getDueBankName1() {
        return dueBankName1;
    }

    @Override
    public void setDueBankName1(String dueBankName1) {
        this.dueBankName1 = dueBankName1;
    }

    @Override
    public String getDueBankAccount1() {
        return dueBankAccount1;
    }

    @Override
    public void setDueBankAccount1(String dueBankAccount1) {
        this.dueBankAccount1 = dueBankAccount1;
    }

    @Override
    public String getBuyCurreny() {
        return buyCurreny;
    }

    @Override
    public void setBuyCurreny(String buyCurreny) {
        this.buyCurreny = buyCurreny;
    }

    @Override
    public String getCapitalBank1() {
        return capitalBank1;
    }

    @Override
    public void setCapitalBank1(String capitalBank1) {
        this.capitalBank1 = capitalBank1;
    }

    @Override
    public String getCapitalBankName1() {
        return capitalBankName1;
    }

    @Override
    public void setCapitalBankName1(String capitalBankName1) {
        this.capitalBankName1 = capitalBankName1;
    }

    @Override
    public String getCnapsCode1() {
        return cnapsCode1;
    }

    @Override
    public void setCnapsCode1(String cnapsCode1) {
        this.cnapsCode1 = cnapsCode1;
    }

    @Override
    public String getCapitalAccount1() {
        return capitalAccount1;
    }

    @Override
    public void setCapitalAccount1(String capitalAccount1) {
        this.capitalAccount1 = capitalAccount1;
    }

    @Override
    public String getBankName2() {
        return bankName2;
    }

    @Override
    public void setBankName2(String bankName2) {
        this.bankName2 = bankName2;
    }

    @Override
    public String getBankBic2() {
        return bankBic2;
    }

    @Override
    public void setBankBic2(String bankBic2) {
        this.bankBic2 = bankBic2;
    }

    @Override
    public String getDueBank2() {
        return dueBank2;
    }

    @Override
    public void setDueBank2(String dueBank2) {
        this.dueBank2 = dueBank2;
    }

    @Override
    public String getDueBankName2() {
        return dueBankName2;
    }

    @Override
    public void setDueBankName2(String dueBankName2) {
        this.dueBankName2 = dueBankName2;
    }

    @Override
    public String getDueBankAccount2() {
        return dueBankAccount2;
    }

    @Override
    public void setDueBankAccount2(String dueBankAccount2) {
        this.dueBankAccount2 = dueBankAccount2;
    }

    @Override
    public String getCapitalBank2() {
        return capitalBank2;
    }

    @Override
    public void setCapitalBank2(String capitalBank2) {
        this.capitalBank2 = capitalBank2;
    }

    public String getTransKind() {
        return transKind;
    }

    public void setTransKind(String transKind) {
        this.transKind = transKind;
    }

    public String getPurposeCode() {
        return purposeCode;
    }

    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }
}