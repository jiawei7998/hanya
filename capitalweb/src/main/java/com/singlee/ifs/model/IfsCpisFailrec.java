package com.singlee.ifs.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author     ：meigy
 * @date       ：Created in 2021/11/12 18:40
 * @description：${description}
 * @modified By：
 * @version:     
 */
/**
    * 交易后-接收失败
    */
public class IfsCpisFailrec implements Serializable {
    /**
    * 0-数据匹配  其他-对应的问题编码
    */
    private String affirmStatus;

    /**
    * 查询匹配状态 1-数据不匹配
    */
    private String matchStatus;

    /**
    * 查询请求编号 
    */
    private String queryRequestId;

    /**
    * 未匹配成功字段
    */
    private String text;

    /**
    * 报文信息
    */
    private String message;

    /**
    * 更新时间
    */
    private Date updatetime;

    private static final long serialVersionUID = 1L;

    public String getAffirmStatus() {
        return affirmStatus;
    }

    public void setAffirmStatus(String affirmStatus) {
        this.affirmStatus = affirmStatus;
    }

    public String getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    public String getQueryRequestId() {
        return queryRequestId;
    }

    public void setQueryRequestId(String queryRequestId) {
        this.queryRequestId = queryRequestId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}