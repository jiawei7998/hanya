package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "IFS_CFETSRMB_IRS")
public class IfsCfetsrmbIrs extends IfsCfetsrmbIrsKey implements Serializable {
    private String forDate;

    private String fixedInst;

    private String fixedTrader;

    private String fixedTel;

    private String fixedFax;

    private String fixedCorp;

    private String fixedAddr;

    private String floatInst;

    private String floatTrader;

    private String floatTel;

    private String floatFax;

    private String floatCorp;

    private String floatAddr;

    private String productName;

    private String tenor;

    private BigDecimal lastQty;

    private String interestRateAdjustment;

    private String startDate;

    private String firstPeriodStartDate;

    private String endDate;

    private String couponPaymentDateReset;

    private String calculateAgency;

    private String tradeMethod;

    private BigDecimal legPrice;

    private String fixedPaymentFrequency;

    private String fixedPaymentDate;

    private String fixedLegDayCount;

    private String benchmarkCurveName;

    private BigDecimal benchmarkSpread;

    private String floatPaymentDate;

    private String floatPaymentFrequency;

    private String legInterestAccrualDate;

    private String interestResetFrequency;

    private String interestMethod;

    private String floatLegDayCount;

    private String fixedAccname;

    private String fixedOpenBank;

    private String fixedAccnum;

    private String fixedPsnum;

    private String floatAccname;

    private String floatOpenBank;

    private String floatAccnum;

    private String floatPsnum;
    
    //会计处理:trading/hedge(套期保值)
	private String accountingMethod;
	
	//本方利率曲线
	private String leftDiscountCurve;
	
	//对方利率曲线
	private String rightDiscountCurve;
	
	//本方利率代码
	private String leftRateCode;
	
	//对方利率代码
	private String rightRateCode;

	//浮动方利率代码(本方或对方)
	@Transient
	private String rateCode;
	/***
	 * P:'RECFLT/PAYFIX','PAYFIX/RECFLT'
	 * R:'RECFIX/PAYFLT','PAYFLT/RECFIX'
	 * X:'RECFIX/PAYFIX','PAYFIX/RECFIX'
	 * L:'RECFLT/PAYFLT','PAYFLT/RECFLT'
	 */
	//固定/浮动方向   值为P/R/X/L
	private String fixedFloatDir;
	
	//对方利率
	private BigDecimal rightRate; 
	
	//币种
	private String ccy;
	
	//左边固定/浮动     值为：Fixed、Float
	private String fixedFloatL;
	
	//右边固定/浮动     值为：Fixed、Float
	private String fixedFloatR;
	
	//左边  参考利率
	private String benchmarkCurveNameL;
	
	//左边  利差
	private BigDecimal benchmarkSpreadL;
	
	//左边  首次利率确定日
	private String legInterestAccrualDateL;
	
	//左边 重置频率
	private String interestResetFrequencyL;
	
	//左边 计息方式
	private String interestMethodL;
	
	//权重
	private String weight;
	//占用授信主体
	private String custNo;
    //占用授信主体类型
    private String custType;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	
	//DV01Risk
	private String DV01Risk;
	
	//止损
	private String lossLimit;
	
	//久期限
	private String longLimit;
	
	//风险程度
	private String riskDegree;
	
	private String fixedLinkTarget;//左边挂钩标的
	private String floatLinkTarget;//右端挂钩标的
	//利率互换与结构衍生区别
	private String prdFlag;
	private String feeFloat;//费用固定/浮动
	private String feePrice;//费用利率(%)
	private String feePaymentDate;//费用支付日
	private String feePaymentFrequency;//费用支付周期
	private String feeInterestAccrualDate;//费用首次利率确定日
	private String feeLegDayCount;//费用计息基准
	private String feeDiscountCurve;//费用利率曲线
	private String feeRateCode;//费用利率代码
	
	//未交割头寸损益
	private String profitLoss;
	@Transient
	private String  prdNo;
	

	private String  cfetsno;
	
    private static final long serialVersionUID = 1L;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

    //前置产品编号
    private String fPrdCode;

    public String getfPrdCode() {
        return fPrdCode;
    }

    public void setfPrdCode(String fPrdCode) {
        this.fPrdCode = fPrdCode;
    }

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

    public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getFixedInst() {
        return fixedInst;
    }

    public void setFixedInst(String fixedInst) {
        this.fixedInst = fixedInst == null ? null : fixedInst.trim();
    }

    public String getFixedTrader() {
        return fixedTrader;
    }

    public void setFixedTrader(String fixedTrader) {
        this.fixedTrader = fixedTrader == null ? null : fixedTrader.trim();
    }

    public String getFixedTel() {
        return fixedTel;
    }

    public void setFixedTel(String fixedTel) {
        this.fixedTel = fixedTel == null ? null : fixedTel.trim();
    }

    public String getFixedFax() {
        return fixedFax;
    }

    public void setFixedFax(String fixedFax) {
        this.fixedFax = fixedFax == null ? null : fixedFax.trim();
    }

    public String getFixedCorp() {
        return fixedCorp;
    }

    public void setFixedCorp(String fixedCorp) {
        this.fixedCorp = fixedCorp == null ? null : fixedCorp.trim();
    }

    public String getFixedAddr() {
        return fixedAddr;
    }

    public void setFixedAddr(String fixedAddr) {
        this.fixedAddr = fixedAddr == null ? null : fixedAddr.trim();
    }

    public String getFloatInst() {
        return floatInst;
    }

    public void setFloatInst(String floatInst) {
        this.floatInst = floatInst == null ? null : floatInst.trim();
    }

    public String getFloatTrader() {
        return floatTrader;
    }

    public void setFloatTrader(String floatTrader) {
        this.floatTrader = floatTrader == null ? null : floatTrader.trim();
    }

    public String getFloatTel() {
        return floatTel;
    }

    public void setFloatTel(String floatTel) {
        this.floatTel = floatTel == null ? null : floatTel.trim();
    }

    public String getFloatFax() {
        return floatFax;
    }

    public void setFloatFax(String floatFax) {
        this.floatFax = floatFax == null ? null : floatFax.trim();
    }

    public String getFloatCorp() {
        return floatCorp;
    }

    public void setFloatCorp(String floatCorp) {
        this.floatCorp = floatCorp == null ? null : floatCorp.trim();
    }

    public String getFloatAddr() {
        return floatAddr;
    }

    public void setFloatAddr(String floatAddr) {
        this.floatAddr = floatAddr == null ? null : floatAddr.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }


    public BigDecimal getLastQty() {
        return lastQty;
    }

    public void setLastQty(BigDecimal lastQty) {
        this.lastQty = lastQty;
    }

    public String getInterestRateAdjustment() {
        return interestRateAdjustment;
    }

    public void setInterestRateAdjustment(String interestRateAdjustment) {
        this.interestRateAdjustment = interestRateAdjustment == null ? null : interestRateAdjustment.trim();
    }

    @Override
    public String getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(String startDate) {
        this.startDate = startDate == null ? null : startDate.trim();
    }

    public String getFirstPeriodStartDate() {
        return firstPeriodStartDate;
    }

    public void setFirstPeriodStartDate(String firstPeriodStartDate) {
        this.firstPeriodStartDate = firstPeriodStartDate == null ? null : firstPeriodStartDate.trim();
    }

    @Override
    public String getEndDate() {
        return endDate;
    }

    @Override
    public void setEndDate(String endDate) {
        this.endDate = endDate == null ? null : endDate.trim();
    }

    public String getCouponPaymentDateReset() {
        return couponPaymentDateReset;
    }

    public void setCouponPaymentDateReset(String couponPaymentDateReset) {
        this.couponPaymentDateReset = couponPaymentDateReset == null ? null : couponPaymentDateReset.trim();
    }

    public String getCalculateAgency() {
        return calculateAgency;
    }

    public void setCalculateAgency(String calculateAgency) {
        this.calculateAgency = calculateAgency == null ? null : calculateAgency.trim();
    }

    public String getTradeMethod() {
        return tradeMethod;
    }

    public void setTradeMethod(String tradeMethod) {
        this.tradeMethod = tradeMethod == null ? null : tradeMethod.trim();
    }

    public BigDecimal getLegPrice() {
        return legPrice;
    }

    public void setLegPrice(BigDecimal legPrice) {
        this.legPrice = legPrice;
    }

    public String getFixedPaymentFrequency() {
        return fixedPaymentFrequency;
    }

    public void setFixedPaymentFrequency(String fixedPaymentFrequency) {
        this.fixedPaymentFrequency = fixedPaymentFrequency == null ? null : fixedPaymentFrequency.trim();
    }

    public String getFixedPaymentDate() {
        return fixedPaymentDate;
    }

    public void setFixedPaymentDate(String fixedPaymentDate) {
        this.fixedPaymentDate = fixedPaymentDate == null ? null : fixedPaymentDate.trim();
    }

    public String getFixedLegDayCount() {
        return fixedLegDayCount;
    }

    public void setFixedLegDayCount(String fixedLegDayCount) {
        this.fixedLegDayCount = fixedLegDayCount == null ? null : fixedLegDayCount.trim();
    }

    public String getBenchmarkCurveName() {
        return benchmarkCurveName;
    }

    public void setBenchmarkCurveName(String benchmarkCurveName) {
        this.benchmarkCurveName = benchmarkCurveName == null ? null : benchmarkCurveName.trim();
    }

    public BigDecimal getBenchmarkSpread() {
        return benchmarkSpread;
    }

    public void setBenchmarkSpread(BigDecimal benchmarkSpread) {
        this.benchmarkSpread = benchmarkSpread;
    }

    public String getFloatPaymentDate() {
        return floatPaymentDate;
    }

    public void setFloatPaymentDate(String floatPaymentDate) {
        this.floatPaymentDate = floatPaymentDate == null ? null : floatPaymentDate.trim();
    }

    public String getFloatPaymentFrequency() {
        return floatPaymentFrequency;
    }

    public void setFloatPaymentFrequency(String floatPaymentFrequency) {
        this.floatPaymentFrequency = floatPaymentFrequency == null ? null : floatPaymentFrequency.trim();
    }

    public String getLegInterestAccrualDate() {
        return legInterestAccrualDate;
    }

    public void setLegInterestAccrualDate(String legInterestAccrualDate) {
        this.legInterestAccrualDate = legInterestAccrualDate == null ? null : legInterestAccrualDate.trim();
    }

    public String getInterestResetFrequency() {
        return interestResetFrequency;
    }

    public void setInterestResetFrequency(String interestResetFrequency) {
        this.interestResetFrequency = interestResetFrequency == null ? null : interestResetFrequency.trim();
    }

    public String getInterestMethod() {
        return interestMethod;
    }

    public void setInterestMethod(String interestMethod) {
        this.interestMethod = interestMethod == null ? null : interestMethod.trim();
    }

    public String getFloatLegDayCount() {
        return floatLegDayCount;
    }

    public void setFloatLegDayCount(String floatLegDayCount) {
        this.floatLegDayCount = floatLegDayCount == null ? null : floatLegDayCount.trim();
    }

    public String getFixedAccname() {
        return fixedAccname;
    }

    public void setFixedAccname(String fixedAccname) {
        this.fixedAccname = fixedAccname == null ? null : fixedAccname.trim();
    }

    public String getFixedOpenBank() {
        return fixedOpenBank;
    }

    public void setFixedOpenBank(String fixedOpenBank) {
        this.fixedOpenBank = fixedOpenBank == null ? null : fixedOpenBank.trim();
    }

    public String getFixedAccnum() {
        return fixedAccnum;
    }

    public void setFixedAccnum(String fixedAccnum) {
        this.fixedAccnum = fixedAccnum == null ? null : fixedAccnum.trim();
    }

    public String getFixedPsnum() {
        return fixedPsnum;
    }

    public void setFixedPsnum(String fixedPsnum) {
        this.fixedPsnum = fixedPsnum == null ? null : fixedPsnum.trim();
    }

    public String getFloatAccname() {
        return floatAccname;
    }

    public void setFloatAccname(String floatAccname) {
        this.floatAccname = floatAccname == null ? null : floatAccname.trim();
    }

    public String getFloatOpenBank() {
        return floatOpenBank;
    }

    public void setFloatOpenBank(String floatOpenBank) {
        this.floatOpenBank = floatOpenBank == null ? null : floatOpenBank.trim();
    }

    public String getFloatAccnum() {
        return floatAccnum;
    }

    public void setFloatAccnum(String floatAccnum) {
        this.floatAccnum = floatAccnum == null ? null : floatAccnum.trim();
    }

    public String getFloatPsnum() {
        return floatPsnum;
    }

    public void setFloatPsnum(String floatPsnum) {
        this.floatPsnum = floatPsnum == null ? null : floatPsnum.trim();
    }

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getAccountingMethod() {
		return accountingMethod;
	}

	public void setAccountingMethod(String accountingMethod) {
		this.accountingMethod = accountingMethod;
	}

	public String getLeftDiscountCurve() {
		return leftDiscountCurve;
	}

	public void setLeftDiscountCurve(String leftDiscountCurve) {
		this.leftDiscountCurve = leftDiscountCurve;
	}

	public String getRightDiscountCurve() {
		return rightDiscountCurve;
	}

	public void setRightDiscountCurve(String rightDiscountCurve) {
		this.rightDiscountCurve = rightDiscountCurve;
	}

	public String getLeftRateCode() {
		return leftRateCode;
	}

	public void setLeftRateCode(String leftRateCode) {
		this.leftRateCode = leftRateCode;
	}

	public String getRightRateCode() {
		return rightRateCode;
	}

	public void setRightRateCode(String rightRateCode) {
		this.rightRateCode = rightRateCode;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getFixedFloatDir() {
		return fixedFloatDir;
	}

	public void setFixedFloatDir(String fixedFloatDir) {
		this.fixedFloatDir = fixedFloatDir;
	}

	public BigDecimal getRightRate() {
		return rightRate;
	}

	public void setRightRate(BigDecimal rightRate) {
		this.rightRate = rightRate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getFixedFloatL() {
		return fixedFloatL;
	}

	public void setFixedFloatL(String fixedFloatL) {
		this.fixedFloatL = fixedFloatL;
	}

	public String getFixedFloatR() {
		return fixedFloatR;
	}

	public void setFixedFloatR(String fixedFloatR) {
		this.fixedFloatR = fixedFloatR;
	}

	public String getBenchmarkCurveNameL() {
		return benchmarkCurveNameL;
	}

	public void setBenchmarkCurveNameL(String benchmarkCurveNameL) {
		this.benchmarkCurveNameL = benchmarkCurveNameL;
	}

	public BigDecimal getBenchmarkSpreadL() {
		return benchmarkSpreadL;
	}

	public void setBenchmarkSpreadL(BigDecimal benchmarkSpreadL) {
		this.benchmarkSpreadL = benchmarkSpreadL;
	}

	public String getLegInterestAccrualDateL() {
		return legInterestAccrualDateL;
	}

	public void setLegInterestAccrualDateL(String legInterestAccrualDateL) {
		this.legInterestAccrualDateL = legInterestAccrualDateL;
	}

	public String getInterestResetFrequencyL() {
		return interestResetFrequencyL;
	}

	public void setInterestResetFrequencyL(String interestResetFrequencyL) {
		this.interestResetFrequencyL = interestResetFrequencyL;
	}

	public String getInterestMethodL() {
		return interestMethodL;
	}

	public void setInterestMethodL(String interestMethodL) {
		this.interestMethodL = interestMethodL;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
    public String getCustNo() {
		return custNo;
	}

	@Override
    public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}
	

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	public String getFixedLinkTarget() {
		return fixedLinkTarget;
	}

	public void setFixedLinkTarget(String fixedLinkTarget) {
		this.fixedLinkTarget = fixedLinkTarget;
	}

	public String getFloatLinkTarget() {
		return floatLinkTarget;
	}

	public void setFloatLinkTarget(String floatLinkTarget) {
		this.floatLinkTarget = floatLinkTarget;
	}

	public String getPrdFlag() {
		return prdFlag==null?"":prdFlag;
	}

	public void setPrdFlag(String prdFlag) {
		this.prdFlag = prdFlag;
	}

	public String getFeeFloat() {
		return feeFloat;
	}

	public void setFeeFloat(String feeFloat) {
		this.feeFloat = feeFloat;
	}

	public String getFeePrice() {
		return feePrice;
	}

	public void setFeePrice(String feePrice) {
		this.feePrice = feePrice;
	}

	public String getFeePaymentDate() {
		return feePaymentDate;
	}

	public void setFeePaymentDate(String feePaymentDate) {
		this.feePaymentDate = feePaymentDate;
	}

	public String getFeePaymentFrequency() {
		return feePaymentFrequency;
	}

	public void setFeePaymentFrequency(String feePaymentFrequency) {
		this.feePaymentFrequency = feePaymentFrequency;
	}

	public String getFeeInterestAccrualDate() {
		return feeInterestAccrualDate;
	}

	public void setFeeInterestAccrualDate(String feeInterestAccrualDate) {
		this.feeInterestAccrualDate = feeInterestAccrualDate;
	}

	public String getFeeLegDayCount() {
		return feeLegDayCount;
	}

	public void setFeeLegDayCount(String feeLegDayCount) {
		this.feeLegDayCount = feeLegDayCount;
	}

	public String getFeeDiscountCurve() {
		return feeDiscountCurve;
	}

	public void setFeeDiscountCurve(String feeDiscountCurve) {
		this.feeDiscountCurve = feeDiscountCurve;
	}

	public String getFeeRateCode() {
		return feeRateCode;
	}

	public void setFeeRateCode(String feeRateCode) {
		this.feeRateCode = feeRateCode;
	}

	public String getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(String profitLoss) {
		this.profitLoss = profitLoss;
	}

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }
}