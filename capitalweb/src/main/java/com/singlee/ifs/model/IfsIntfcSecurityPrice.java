package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIntfcSecurityPrice implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String FXFIG_SVCN;
	private String FXFIG_IBCD;
	private String FXFIG_GWAM;
	private String FXFIG_GEOR;
	private String FXFIG_OPNO;
	private String FIFB13_SECURITY_ID;
	private String FIFB13_DEAL_IL;
	private String FIFB13_CCY;
	private BigDecimal FIFB13_CLOSING_PRICE;
	
	private String SECID;
	private String CCY;
	private String LSTMNTDATE;
	private BigDecimal CLSGPRICE_8;
	private String dealdate;
	
	
	public String getFXFIG_SVCN() {
		return FXFIG_SVCN;
	}
	public void setFXFIG_SVCN(String fXFIG_SVCN) {
		FXFIG_SVCN = fXFIG_SVCN;
	}
	public String getFXFIG_IBCD() {
		return FXFIG_IBCD;
	}
	public void setFXFIG_IBCD(String fXFIG_IBCD) {
		FXFIG_IBCD = fXFIG_IBCD;
	}
	public String getFXFIG_GWAM() {
		return FXFIG_GWAM;
	}
	public void setFXFIG_GWAM(String fXFIG_GWAM) {
		FXFIG_GWAM = fXFIG_GWAM;
	}
	public String getFXFIG_GEOR() {
		return FXFIG_GEOR;
	}
	public void setFXFIG_GEOR(String fXFIG_GEOR) {
		FXFIG_GEOR = fXFIG_GEOR;
	}
	public String getFXFIG_OPNO() {
		return FXFIG_OPNO;
	}
	public void setFXFIG_OPNO(String fXFIG_OPNO) {
		FXFIG_OPNO = fXFIG_OPNO;
	}
	public String getFIFB13_SECURITY_ID() {
		return FIFB13_SECURITY_ID;
	}
	public void setFIFB13_SECURITY_ID(String fIFB13_SECURITY_ID) {
		FIFB13_SECURITY_ID = fIFB13_SECURITY_ID;
	}
	public String getFIFB13_DEAL_IL() {
		return FIFB13_DEAL_IL;
	}
	public void setFIFB13_DEAL_IL(String fIFB13_DEAL_IL) {
		FIFB13_DEAL_IL = fIFB13_DEAL_IL;
	}
	public String getFIFB13_CCY() {
		return FIFB13_CCY;
	}
	public void setFIFB13_CCY(String fIFB13_CCY) {
		FIFB13_CCY = fIFB13_CCY;
	}
	public BigDecimal getFIFB13_CLOSING_PRICE() {
		return FIFB13_CLOSING_PRICE;
	}
	public void setFIFB13_CLOSING_PRICE(BigDecimal fIFB13_CLOSING_PRICE) {
		FIFB13_CLOSING_PRICE = fIFB13_CLOSING_PRICE;
	}
	public String getSECID() {
		return SECID;
	}
	public void setSECID(String sECID) {
		SECID = sECID;
	}
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public String getLSTMNTDATE() {
		return LSTMNTDATE;
	}
	public void setLSTMNTDATE(String lSTMNTDATE) {
		LSTMNTDATE = lSTMNTDATE;
	}
	public BigDecimal getCLSGPRICE_8() {
		return CLSGPRICE_8;
	}
	public void setCLSGPRICE_8(BigDecimal cLSGPRICE_8) {
		CLSGPRICE_8 = cLSGPRICE_8;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	
	
	

}
