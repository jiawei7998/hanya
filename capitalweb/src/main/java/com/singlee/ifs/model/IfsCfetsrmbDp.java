package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "IFS_CFETSRMB_DP")
public class IfsCfetsrmbDp extends IfsCfetsrmbDpKey implements Serializable{

	private String borrowCanum;
	private String borrowCustname;
	private String borrowCaname;
	private String borrowPsnum;
	private String borrowAccnum;
	private String borrowOpBank;
	private String borrowAccname;
	private String aDate;
	private String sponInst;
	private String sponsor;
	private String approveStatus;
	private String borrowTrader;
	private String borrowInst;
	private String note;
	private String grantSign;
	private String verifySign;
	private String settleAcct;
	private String settleMeans;
	private String lastTime;
	private String inputTime;
	private String dealNo;
	private String contractId;
	private String duedate;
	private BigDecimal benchmarkcurvename;
	private BigDecimal actualAmount;
	private String publishDate;
	private String termType;
	private String depositCode;
	private String currency;
	private String valueDate;
	private BigDecimal publishPrice;
	private BigDecimal planAmount;
	private String interestType;
	private BigDecimal depositTerm;
	private String depositName;
	private String depositAllName;
	private String forDate;
	private String ticketId;
	private String dealTransType;
	private String groupMod;
	private String groupName;
	private String groupId;
	private String borrowAddr;
	private String borrowCorp;
	private String borrowFax;
	private String borrowTel;
	private String priceDeviation;
	private String instrumentType;
	/**
	 * 存款账号
	 */
	private String depositAccount;
	@Transient
	private String basis;
	@Transient
	private String offerAmount;
	@Transient
	private String shouldPayMoney;
	
	@Transient
	private String  prdNo;


	
	private List<IfsCfetsrmbDpOffer> IfsCfetsrmbDpOffer;
	private static final long serialVersionUID = 1L;

	/**
	 * 五级分类
	 */
	private String fiveLevelClass;

	/**
	 * 归属部门
	 */
	private String attributionDept;

	/**
	 * 是否异地业务
	 */
	private String remoteService;

	//前置产品编号
	private String fPrdCode;

	/**
	 * 应收款项
	 */
	private BigDecimal receivable;

	public String getDepositAccount() {
		return depositAccount;
	}

	public void setDepositAccount(String depositAccount) {
		this.depositAccount = depositAccount;
	}

	public BigDecimal getReceivable() {
		return receivable;
	}

	public void setReceivable(BigDecimal receivable) {
		this.receivable = receivable;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getRemoteService() {
		return remoteService;
	}

	public void setRemoteService(String remoteService) {
		this.remoteService = remoteService;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}
	
	public String getPrdNo() {
		return prdNo;
	}
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	public String getBorrowAddr() {
		return borrowAddr;
	}
	public void setBorrowAddr(String borrowAddr) {
		this.borrowAddr = borrowAddr == null ? null : borrowAddr.trim();
	}
	public String getBorrowCorp() {
		return borrowCorp;
	}
	public void setBorrowCorp(String borrowCorp) {
		this.borrowCorp = borrowCorp == null ? null : borrowCorp.trim();
	}
	public String getBorrowFax() {
		return borrowFax;
	}
	public void setBorrowFax(String borrowFax) {
		this.borrowFax = borrowFax == null ? null : borrowFax.trim();
	}
	public String getBorrowTel() {
		return borrowTel;
	}
	public void setBorrowTel(String borrowTel) {
		this.borrowTel = borrowTel == null ? null : borrowTel.trim();
	}
	
	public List<IfsCfetsrmbDpOffer> getIfsCfetsrmbDpOffer() {
		return IfsCfetsrmbDpOffer;
	}
	public void setIfsCfetsrmbDpOffer(List<IfsCfetsrmbDpOffer> ifsCfetsrmbDpOffer) {
		IfsCfetsrmbDpOffer = ifsCfetsrmbDpOffer;
	}
	public String getBorrowCanum() {
		return borrowCanum;
	}
	public void setBorrowCanum(String borrowCanum) {
		this.borrowCanum = borrowCanum== null ? null : borrowCanum.trim();
	}
	public String getBorrowCustname() {
		return borrowCustname;
	}
	public void setBorrowCustname(String borrowCustname) {
		this.borrowCustname = borrowCustname== null ? null : borrowCustname.trim();
	}
	public String getBorrowCaname() {
		return borrowCaname;
	}
	public void setBorrowCaname(String borrowCaname) {
		this.borrowCaname = borrowCaname== null ? null : borrowCaname.trim();
	}
	public String getBorrowPsnum() {
		return borrowPsnum;
	}
	public void setBorrowPsnum(String borrowPsnum) {
		this.borrowPsnum = borrowPsnum== null ? null : borrowPsnum.trim();
	}
	public String getBorrowAccnum() {
		return borrowAccnum;
	}
	public void setBorrowAccnum(String borrowAccnum) {
		this.borrowAccnum = borrowAccnum== null ? null : borrowAccnum.trim();
	}
	public String getBorrowOpBank() {
		return borrowOpBank;
	}
	public void setBorrowOpBank(String borrowOpBank) {
		this.borrowOpBank = borrowOpBank== null ? null : borrowOpBank.trim();
	}
	public String getBorrowAccname() {
		return borrowAccname;
	}
	public void setBorrowAccname(String borrowAccname) {
		this.borrowAccname = borrowAccname== null ? null : borrowAccname.trim();
	}
	@Override
    public String getaDate() {
		return aDate;
	}
	@Override
    public void setaDate(String aDate) {
		this.aDate = aDate== null ? null : aDate.trim();
	}
	@Override
    public String getSponInst() {
		return sponInst;
	}
	@Override
    public void setSponInst(String sponInst) {
		this.sponInst = sponInst== null ? null : sponInst.trim();
	}
	@Override
    public String getSponsor() {
		return sponsor;
	}
	@Override
    public void setSponsor(String sponsor) {
		this.sponsor = sponsor== null ? null : sponsor.trim();
	}
	@Override
    public String getApproveStatus() {
		return approveStatus;
	}
	@Override
    public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus== null ? null : approveStatus.trim();
	}
	public String getBorrowTrader() {
		return borrowTrader;
	}
	public void setBorrowTrader(String borrowTrader) {
		this.borrowTrader = borrowTrader== null ? null : borrowTrader.trim();
	}
	public String getBorrowInst() {
		return borrowInst;
	}
	public void setBorrowInst(String borrowInst) {
		this.borrowInst = borrowInst== null ? null : borrowInst.trim();
	}
	@Override
    public String getNote() {
		return note;
	}
	@Override
    public void setNote(String note) {
		this.note = note== null ? null : note.trim();
	}
	@Override
    public String getGrantSign() {
		return grantSign;
	}
	@Override
    public void setGrantSign(String grantSign) {
		this.grantSign = grantSign== null ? null : grantSign.trim();
	}
	@Override
    public String getVerifySign() {
		return verifySign;
	}
	@Override
    public void setVerifySign(String verifySign) {
		this.verifySign = verifySign== null ? null : verifySign.trim();
	}
	@Override
    public String getSettleAcct() {
		return settleAcct;
	}
	@Override
    public void setSettleAcct(String settleAcct) {
		this.settleAcct = settleAcct== null ? null : settleAcct.trim();
	}
	@Override
    public String getSettleMeans() {
		return settleMeans;
	}
	@Override
    public void setSettleMeans(String settleMeans) {
		this.settleMeans = settleMeans== null ? null : settleMeans.trim();
	}
	@Override
    public String getLastTime() {
		return lastTime;
	}
	@Override
    public void setLastTime(String lastTime) {
		this.lastTime = lastTime== null ? null : lastTime.trim();
	}
	@Override
    public String getInputTime() {
		return inputTime;
	}
	@Override
    public void setInputTime(String inputTime) {
		this.inputTime = inputTime== null ? null : inputTime.trim();
	}
	@Override
    public String getDealNo() {
		return dealNo;
	}
	@Override
    public void setDealNo(String dealNo) {
		this.dealNo = dealNo== null ? null : dealNo.trim();
	}
	@Override
    public String getContractId() {
		return contractId;
	}
	@Override
    public void setContractId(String contractId) {
		this.contractId = contractId== null ? null : contractId.trim();
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate== null ? null : duedate.trim();
	}
	public BigDecimal getBenchmarkcurvename() {
		return benchmarkcurvename;
	}
	public void setBenchmarkcurvename(BigDecimal benchmarkcurvename) {
		this.benchmarkcurvename = benchmarkcurvename;
	}
	public BigDecimal getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate== null ? null : publishDate.trim();
	}
	public String getTermType() {
		return termType;
	}
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}
	public String getDepositCode() {
		return depositCode;
	}
	public void setDepositCode(String depositCode) {
		this.depositCode = depositCode == null ? null : depositCode.trim();
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}
	public BigDecimal getPublishPrice() {
		return publishPrice;
	}
	public void setPublishPrice(BigDecimal publishPrice) {
		this.publishPrice = publishPrice;
	}
	public BigDecimal getPlanAmount() {
		return planAmount;
	}
	public void setPlanAmount(BigDecimal planAmount) {
		this.planAmount = planAmount;
	}
	public String getInterestType() {
		return interestType;
	}
	public void setInterestType(String interestType) {
		this.interestType = interestType == null ? null : interestType.trim();
	}
	public BigDecimal getDepositTerm() {
		return depositTerm;
	}
	public void setDepositTerm(BigDecimal depositTerm) {
		this.depositTerm = depositTerm;
	}
	public String getDepositName() {
		return depositName;
	}
	public void setDepositName(String depositName) {
		this.depositName = depositName == null ? null : depositName.trim();
	}
	public String getDepositAllName() {
		return depositAllName;
	}
	public void setDepositAllName(String depositAllName) {
		this.depositAllName = depositAllName == null ? null : depositAllName.trim();
	}
	public String getForDate() {
		return forDate;
	}
	public void setForDate(String forDate) {
		this.forDate = forDate == null ? null : forDate.trim();
	}
	@Override
    public String getTicketId() {
		return ticketId;
	}
	@Override
    public void setTicketId(String ticketId) {
		this.ticketId = ticketId == null ? null : ticketId.trim();
	}
	@Override
    public String getDealTransType() {
		return dealTransType;
	}
	@Override
    public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType == null ? null : dealTransType.trim();
	}
	@Override
    public String getGroupMod() {
		return groupMod;
	}
	@Override
    public void setGroupMod(String groupMod) {
		this.groupMod = groupMod == null ? null : groupMod.trim();
	}
	@Override
    public String getGroupName() {
		return groupName;
	}
	@Override
    public void setGroupName(String groupName) {
		this.groupName = groupName == null ? null : groupName.trim();
	}
	@Override
    public String getGroupId() {
		return groupId;
	}
	@Override
    public void setGroupId(String groupId) {
		this.groupId = groupId == null ? null : groupId.trim();
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getOfferAmount() {
		return offerAmount;
	}
	public void setOfferAmount(String offerAmount) {
		this.offerAmount = offerAmount;
	}
	public String getShouldPayMoney() {
		return shouldPayMoney;
	}
	public void setShouldPayMoney(String shouldPayMoney) {
		this.shouldPayMoney = shouldPayMoney;
	}
	public String getPriceDeviation() {
		return priceDeviation;
	}
	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation;
	}
	public String getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	
}
