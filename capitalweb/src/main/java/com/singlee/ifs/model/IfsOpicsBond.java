package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 交易要素  实体对象
 * @author singlee4
 *
 */
@Entity
@Table(name="IFS_OPICS_BOND")
public class IfsOpicsBond implements Serializable{
	private static final long serialVersionUID = 1L;

	private String dealno;
	
	private String status;
	
	private String opicsdealno;
	
	private String br;
	
	private String ccy;
	
	private String secunit;
	
	private String denom;
	
	private String descr;
	
	private float spreadrate_8;
	
	private String intenddaterule;
	
	private String bndnm_cn;
	
	private String bndnm_en;
	
	private String bndcd;
	
	private String accountno;
	
	private String pubdt;
	
	private BigDecimal issuevol;
	
	private BigDecimal issprice;
	
	private String intpaymethod;
	
	private String intpaycircle;
	
	private String couponrate;
	
	private String issudt;
	
	private String matdt;
	
	private String bondperiod;
	
	private String basicspread;
	
	private String paragraphfirstplan;
	
	private String bondproperties;
	
	private String issuer;
	
	private String issuername;
	
	private String floatratemark;
	
	private String bondratingclass;
	
	private String bondratingagency;
	
	private String bondrating;
	
	private String product;

	private String prodtype;
	
	private String settccy;
	
	private String paramt;
	
	private String basis;
	
	private String intcalcrule;
	
	private String note;
	
	private String userid;
	
	private String checkUserId;
	
	private String element;
	
	private String inputdate;
	
	private String acctngtype;
	
	private String settdays;
	
	private String marketDate;
	
	private String rateType;
	
	private String isWeight;
	
	private String slPubMethod;

	private String paragraphlastplan;//最后息票日期

	private String intpaydatum;//付息基准

	private BigDecimal repayamt;//偿还金额

	private String instId;//所属机构编号

	private String instName;//交易所属机构名称

	/**
	 * 债权债务登记日
	 */
	private String bongRightDebtDate;

	/**
	 * 债券续发次数
	 */
	private Integer reissueFrequency;

	/**
	 * 最终投向类型
	 */
	private String finalInvestmentType;

	/**
	 * 最终投向行业
	 */
	private String finalInvestmentIndustry;

	/**
	 * 发行人企业规模
	 */
	private String issueEnterpriseScale;

	/**
	 * 投资金用途
	 */
	private String investmentFundsUse;

	/**
	 * 融资类型（1：地方政府承诺偿还、2：地方政府提供担保、3：地方政府长期支出责任）
	 */
	private String financingType;

	/**
	 * 债务人层级(1:省级、2：地市级、3：区县级)
	 */
	private String debtorLevel;


	/**
	 * 主体评级机构
	 */
	private String ztInst;

	/**
	 * 主体评级级别
	 */
	private String zxZtLevel;

	/**
	 * 主体评级时间
	 */
	private String fxZtDate;

	/**
	 * 债项评级机构
	 */
	private String zxInst;

	/**
	 * 债项评级级别
	 */
	private String zxZxLevel;

	/**
	 * 债项评级时间
	 */
	private String fxZxDate;


	public String getZtInst() {
		return ztInst;
	}

	public void setZtInst(String ztInst) {
		this.ztInst = ztInst;
	}

	public String getZxZtLevel() {
		return zxZtLevel;
	}

	public void setZxZtLevel(String zxZtLevel) {
		this.zxZtLevel = zxZtLevel;
	}

	public String getFxZtDate() {
		return fxZtDate;
	}

	public void setFxZtDate(String fxZtDate) {
		this.fxZtDate = fxZtDate;
	}

	public String getZxInst() {
		return zxInst;
	}

	public void setZxInst(String zxInst) {
		this.zxInst = zxInst;
	}

	public String getZxZxLevel() {
		return zxZxLevel;
	}

	public void setZxZxLevel(String zxZxLevel) {
		this.zxZxLevel = zxZxLevel;
	}

	public String getFxZxDate() {
		return fxZxDate;
	}

	public void setFxZxDate(String fxZxDate) {
		this.fxZxDate = fxZxDate;
	}

	public String getFinancingType() {
		return financingType;
	}

	public void setFinancingType(String financingType) {
		this.financingType = financingType;
	}

	public String getDebtorLevel() {
		return debtorLevel;
	}

	public void setDebtorLevel(String debtorLevel) {
		this.debtorLevel = debtorLevel;
	}

	public String getInstid() {
		return instId;
	}

	public void setInstid(String instid) {
		this.instId = instid;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOpicsdealno() {
		return opicsdealno;
	}

	public void setOpicsdealno(String opicsdealno) {
		this.opicsdealno = opicsdealno;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSecunit() {
		return secunit;
	}

	public void setSecunit(String secunit) {
		this.secunit = secunit;
	}

	public String getDenom() {
		return denom;
	}

	public void setDenom(String denom) {
		this.denom = denom;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public float getSpreadrate_8() {
		return spreadrate_8;
	}

	public void setSpreadrate_8(float spreadrate_8) {
		this.spreadrate_8 = spreadrate_8;
	}

	public String getIntenddaterule() {
		return intenddaterule;
	}

	public void setIntenddaterule(String intenddaterule) {
		this.intenddaterule = intenddaterule;
	}

	public String getBndnm_cn() {
		return bndnm_cn;
	}

	public void setBndnm_cn(String bndnm_cn) {
		this.bndnm_cn = bndnm_cn;
	}

	public String getBndnm_en() {
		return bndnm_en;
	}

	public void setBndnm_en(String bndnm_en) {
		this.bndnm_en = bndnm_en;
	}

	public String getBndcd() {
		return bndcd;
	}

	public void setBndcd(String bndcd) {
		this.bndcd = bndcd;
	}

	public String getAccountno() {
		return accountno;
	}

	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}

	public String getPubdt() {
		return pubdt;
	}

	public void setPubdt(String pubdt) {
		this.pubdt = pubdt;
	}
	
	public BigDecimal getIssuevol() {
		return issuevol;
	}

	public void setIssuevol(BigDecimal issuevol) {
		this.issuevol = issuevol;
	}

	public BigDecimal getIssprice() {
		return issprice;
	}

	public void setIssprice(BigDecimal issprice) {
		this.issprice = issprice;
	}

	public String getIntpaymethod() {
		return intpaymethod;
	}

	public void setIntpaymethod(String intpaymethod) {
		this.intpaymethod = intpaymethod;
	}

	public String getIntpaycircle() {
		return intpaycircle;
	}

	public void setIntpaycircle(String intpaycircle) {
		this.intpaycircle = intpaycircle;
	}

	public String getCouponrate() {
		return couponrate;
	}

	public void setCouponrate(String couponrate) {
		this.couponrate = couponrate;
	}

	public String getIssudt() {
		return issudt;
	}

	public void setIssudt(String issudt) {
		this.issudt = issudt;
	}

	public String getMatdt() {
		return matdt;
	}

	public void setMatdt(String matdt) {
		this.matdt = matdt;
	}

	public String getBondperiod() {
		return bondperiod;
	}

	public void setBondperiod(String bondperiod) {
		this.bondperiod = bondperiod;
	}

	public String getBasicspread() {
		return basicspread;
	}

	public void setBasicspread(String basicspread) {
		this.basicspread = basicspread;
	}

	public String getParagraphfirstplan() {
		return paragraphfirstplan;
	}

	public void setParagraphfirstplan(String paragraphfirstplan) {
		this.paragraphfirstplan = paragraphfirstplan;
	}

	public String getBondproperties() {
		return bondproperties;
	}

	public void setBondproperties(String bondproperties) {
		this.bondproperties = bondproperties;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuername() {
		return issuername;
	}

	public void setIssuername(String issuername) {
		this.issuername = issuername;
	}

	public String getFloatratemark() {
		return floatratemark;
	}

	public void setFloatratemark(String floatratemark) {
		this.floatratemark = floatratemark;
	}

	public String getBondratingclass() {
		return bondratingclass;
	}

	public void setBondratingclass(String bondratingclass) {
		this.bondratingclass = bondratingclass;
	}

	public String getBondratingagency() {
		return bondratingagency;
	}

	public void setBondratingagency(String bondratingagency) {
		this.bondratingagency = bondratingagency;
	}

	public String getBondrating() {
		return bondrating;
	}

	public void setBondrating(String bondrating) {
		this.bondrating = bondrating;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getSettccy() {
		return settccy;
	}

	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}

	public String getParamt() {
		return paramt;
	}

	public void setParamt(String paramt) {
		this.paramt = paramt;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCheckUserId() {
		return checkUserId;
	}

	public void setCheckUserId(String checkUserId) {
		this.checkUserId = checkUserId;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getSettdays() {
		return settdays;
	}

	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}

	public String getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(String marketDate) {
		this.marketDate = marketDate;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getIsWeight() {
		return isWeight;
	}

	public void setIsWeight(String isWeight) {
		this.isWeight = isWeight;
	}

	public String getSlPubMethod() {
		return slPubMethod;
	}

	public void setSlPubMethod(String slPubMethod) {
		this.slPubMethod = slPubMethod;
	}

	public String getParagraphlastplan() {
		return paragraphlastplan;
	}

	public void setParagraphlastplan(String paragraphlastplan) {
		this.paragraphlastplan = paragraphlastplan;
	}

	public String getIntpaydatum() {
		return intpaydatum;
	}

	public void setIntpaydatum(String intpaydatum) {
		this.intpaydatum = intpaydatum;
	}

	public BigDecimal getRepayamt() {
		return repayamt;
	}

	public void setRepayamt(BigDecimal repayamt) {
		this.repayamt = repayamt;
	}

	public String getBongRightDebtDate() {
		return bongRightDebtDate;
	}

	public void setBongRightDebtDate(String bongRightDebtDate) {
		this.bongRightDebtDate = bongRightDebtDate;
	}

	public Integer getReissueFrequency() {
		return reissueFrequency;
	}

	public void setReissueFrequency(Integer reissueFrequency) {
		this.reissueFrequency = reissueFrequency;
	}

	public String getFinalInvestmentType() {
		return finalInvestmentType;
	}

	public void setFinalInvestmentType(String finalInvestmentType) {
		this.finalInvestmentType = finalInvestmentType;
	}

	public String getFinalInvestmentIndustry() {
		return finalInvestmentIndustry;
	}

	public void setFinalInvestmentIndustry(String finalInvestmentIndustry) {
		this.finalInvestmentIndustry = finalInvestmentIndustry;
	}

	public String getIssueEnterpriseScale() {
		return issueEnterpriseScale;
	}

	public void setIssueEnterpriseScale(String issueEnterpriseScale) {
		this.issueEnterpriseScale = issueEnterpriseScale;
	}

	public String getInvestmentFundsUse() {
		return investmentFundsUse;
	}

	public void setInvestmentFundsUse(String investmentFundsUse) {
		this.investmentFundsUse = investmentFundsUse;
	}
}
