package com.singlee.ifs.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity	
@Table(name = "BOND_LIMIT_TEMPLATE")
public class BondTemplate implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id  
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_BOND_TEMPLATE.NEXTVAL FROM DUAL")
	private String bondId;
	private String trad;
	private String prd;
	private String prdCode;
	private	String prdType;
	private	String prdTypeCode;
	private String accType;
	private String dealSource;
	private String acctngtype;
	private String limitType;
	private String ccy;
	private String ccyCode;
	private String bondType;
	private String quotaAmount;
	private String availAmount;
	private String ctlType;
	private String operateUser;
	private String itime;
	private String direction;
	@Transient
	private String userName;
	
	//已经使用的额度，累计
	private String totalAmount;
	
	//交易员姓名
	private String tradName;
	
	
	public String getPrdCode() {
		return prdCode;
	}
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	public String getPrdTypeCode() {
		return prdTypeCode;
	}
	public void setPrdTypeCode(String prdTypeCode) {
		this.prdTypeCode = prdTypeCode;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getLimitType() {
		return limitType;
	}
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getPrd() {
		return prd;
	}
	public void setPrd(String prd) {
		this.prd = prd;
	}
	public String getPrdType() {
		return prdType;
	}
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	public String getDealSource() {
		return dealSource;
	}
	public void setDealSource(String dealSource) {
		this.dealSource = dealSource;
	}
	public String getBondId() {
		return bondId;
	}
	public void setBondId(String bondId) {
		this.bondId = bondId;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public String getAcctngtype() {
		return acctngtype;
	}
	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}
	public String getBondType() {
		return bondType;
	}
	public void setBondType(String bondType) {
		this.bondType = bondType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getQuotaAmount() {
		return quotaAmount;
	}
	public void setQuotaAmount(String quotaAmount) {
		this.quotaAmount = quotaAmount;
	}
	public String getAvailAmount() {
		return availAmount;
	}
	public void setAvailAmount(String availAmount) {
		this.availAmount = availAmount;
	}
	public String getCtlType() {
		return ctlType;
	}
	public void setCtlType(String ctlType) {
		this.ctlType = ctlType;
	}
	public String getOperateUser() {
		return operateUser;
	}
	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}
	public String getItime() {
		return itime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setItime(String itime) {
		this.itime = itime;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTradName() {
		return tradName;
	}
	public void setTradName(String tradName) {
		this.tradName = tradName;
	}
	
	
	
}