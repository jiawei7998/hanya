package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IFS_CFETSRMB_IBO")
public class IfsCfetsrmbIbo extends IfsCfetsrmbIboKey implements Serializable {
	private static final long serialVersionUID = 1L;
	//交易单号
	private String ticketId;
	// 交易状态
	private String dealTransType;
	// 本方机构
	private String instId;
	//对方机构
	private String counterpartyInstId;
	//本方交易员
	private String dealer;
	//对方交易员
	private String counterpartyDealer;
	private String forDate;
	private String borrowInst;
	private String borrowTrader;
	private String borrowTel;
	private String borrowFax;
	private String borrowCorp;
	private String borrowAddr;
	private String removeInst;
	private String removeTrader;
	private String removeTel;
	private String removeFax;
	private String removeCorp;
	private String removeAddr;
	private String tenor;
	private BigDecimal rate;
	private BigDecimal amt;
	private BigDecimal accuredInterest;
	private BigDecimal settlementAmount;
	private String firstSettlementDate;
	private String secondSettlementDate;
	private BigDecimal occupancyDays;
	private String borrowAccname;
	private String borrowOpenBank;
	private String borrowAccnum;
	private String borrowPsnum;
	private String removeAccname;
	private String removeOpenBank;
	private String removeAccnum;
	private String removePsnum;
	private String rateCode;
	//权重
	private String weight;
	//占用授信主体
	private String custNo;
	//占用授信主体类型
	private String custType;
	//审批单号
	private String applyNo;
	//产品名称
	private String applyProd;
	private String basis;
	//DV01Risk
	private String DV01Risk;
	//止损
	private String lossLimit;
	//久期限
	private String longLimit;
	//风险程度
	private String riskDegree;
	//交易品种
	private String tradingProduct;
	//价格偏离度
	private String priceDeviation;
	@Transient
	private String prdNo;
	private String cfetsno;
	//付息类型
	private String scheduleType;
	//付息周期
	private String intPayCycle;
	//利息付款日期
	private String intPayDay;
	//利息日期规则
	private String intDateRule;
	//利率差
	private BigDecimal spread8;
	/**
	 * 五级分类
	 */
	private String fiveLevelClass;
	/**
	 * 归属部门
	 */
	private String attributionDept;
	/**
	 * 账户性质（1:投融资性,2:结算性）
	 */
	private String accountNature;
	private String accountUse;
	//前置产品编号
	private String fPrdCode;
	/**
	 * 定价基准类型: 1-Shibor,2-Libor,3-Hibor,4-Euribor,5-LPR,
	 * 6-CHN Treasury Rate,7-存款基准利率,8-贷款基准利率,9-再贷款利率,10-其他
	 */
	private String pricingStandardType;
	/**
	 * 利率浮动频率: 1-按日,2-按周,3-按月,4-按季,5-按半年，6-按年，7-其他
	 */
	private String rateFloatFrequency;
	/**
	 * 币种
	 */
	private String ccy;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getPricingStandardType() {
		return pricingStandardType;
	}

	public void setPricingStandardType(String pricingStandardType) {
		this.pricingStandardType = pricingStandardType;
	}

	public String getRateFloatFrequency() {
		return rateFloatFrequency;
	}

	public void setRateFloatFrequency(String rateFloatFrequency) {
		this.rateFloatFrequency = rateFloatFrequency;
	}

	public String getfPrdCode() {
		return fPrdCode;
	}

	public void setfPrdCode(String fPrdCode) {
		this.fPrdCode = fPrdCode;
	}

	public String getFiveLevelClass() {
		return fiveLevelClass;
	}

	public void setFiveLevelClass(String fiveLevelClass) {
		this.fiveLevelClass = fiveLevelClass;
	}

	public String getAttributionDept() {
		return attributionDept;
	}

	public void setAttributionDept(String attributionDept) {
		this.attributionDept = attributionDept;
	}

	public String getIntPayDay() {
		return intPayDay;
	}

	public void setIntPayDay(String intPayDay) {
		this.intPayDay = intPayDay;
	}

	public String getIntDateRule() {
		return intDateRule;
	}

	public void setIntDateRule(String intDateRule) {
		this.intDateRule = intDateRule;
	}

	public BigDecimal getSpread8() {
		return spread8;
	}

	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getIntPayCycle() {
		return intPayCycle;
	}

	public void setIntPayCycle(String intPayCycle) {
		this.intPayCycle = intPayCycle;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getBorrowInst() {
		return borrowInst;
	}

	public void setBorrowInst(String borrowInst) {
		this.borrowInst = borrowInst == null ? null : borrowInst.trim();
	}

	public String getBorrowTrader() {
		return borrowTrader;
	}

	public void setBorrowTrader(String borrowTrader) {
		this.borrowTrader = borrowTrader == null ? null : borrowTrader.trim();
	}

	public String getBorrowTel() {
		return borrowTel;
	}

	public void setBorrowTel(String borrowTel) {
		this.borrowTel = borrowTel == null ? null : borrowTel.trim();
	}

	public String getBorrowFax() {
		return borrowFax;
	}

	public void setBorrowFax(String borrowFax) {
		this.borrowFax = borrowFax == null ? null : borrowFax.trim();
	}

	public String getBorrowCorp() {
		return borrowCorp;
	}

	public void setBorrowCorp(String borrowCorp) {
		this.borrowCorp = borrowCorp == null ? null : borrowCorp.trim();
	}

	public String getBorrowAddr() {
		return borrowAddr;
	}

	public void setBorrowAddr(String borrowAddr) {
		this.borrowAddr = borrowAddr == null ? null : borrowAddr.trim();
	}

	public String getRemoveInst() {
		return removeInst;
	}

	public void setRemoveInst(String removeInst) {
		this.removeInst = removeInst == null ? null : removeInst.trim();
	}

	public String getRemoveTrader() {
		return removeTrader;
	}

	public void setRemoveTrader(String removeTrader) {
		this.removeTrader = removeTrader == null ? null : removeTrader.trim();
	}

	public String getRemoveTel() {
		return removeTel;
	}

	public void setRemoveTel(String removeTel) {
		this.removeTel = removeTel == null ? null : removeTel.trim();
	}

	public String getRemoveFax() {
		return removeFax;
	}

	public void setRemoveFax(String removeFax) {
		this.removeFax = removeFax == null ? null : removeFax.trim();
	}

	public String getRemoveCorp() {
		return removeCorp;
	}

	public void setRemoveCorp(String removeCorp) {
		this.removeCorp = removeCorp == null ? null : removeCorp.trim();
	}

	public String getRemoveAddr() {
		return removeAddr;
	}

	public void setRemoveAddr(String removeAddr) {
		this.removeAddr = removeAddr == null ? null : removeAddr.trim();
	}


	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public BigDecimal getAccuredInterest() {
		return accuredInterest;
	}

	public void setAccuredInterest(BigDecimal accuredInterest) {
		this.accuredInterest = accuredInterest;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getFirstSettlementDate() {
		return firstSettlementDate;
	}

	public void setFirstSettlementDate(String firstSettlementDate) {
		this.firstSettlementDate = firstSettlementDate == null ? null : firstSettlementDate.trim();
	}

	public String getSecondSettlementDate() {
		return secondSettlementDate;
	}

	public void setSecondSettlementDate(String secondSettlementDate) {
		this.secondSettlementDate = secondSettlementDate == null ? null : secondSettlementDate.trim();
	}

	public BigDecimal getOccupancyDays() {
		return occupancyDays;
	}

	public void setOccupancyDays(BigDecimal occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getBorrowAccname() {
		return borrowAccname;
	}

	public void setBorrowAccname(String borrowAccname) {
		this.borrowAccname = borrowAccname == null ? null : borrowAccname.trim();
	}

	public String getBorrowOpenBank() {
		return borrowOpenBank;
	}

	public void setBorrowOpenBank(String borrowOpenBank) {
		this.borrowOpenBank = borrowOpenBank == null ? null : borrowOpenBank.trim();
	}

	public String getBorrowAccnum() {
		return borrowAccnum;
	}

	public void setBorrowAccnum(String borrowAccnum) {
		this.borrowAccnum = borrowAccnum;
	}

	public String getBorrowPsnum() {
		return borrowPsnum;
	}

	public void setBorrowPsnum(String borrowPsnum) {
		this.borrowPsnum = borrowPsnum;
	}

	public String getRemoveAccname() {
		return removeAccname;
	}

	public void setRemoveAccname(String removeAccname) {
		this.removeAccname = removeAccname == null ? null : removeAccname.trim();
	}

	public String getRemoveOpenBank() {
		return removeOpenBank;
	}

	public void setRemoveOpenBank(String removeOpenBank) {
		this.removeOpenBank = removeOpenBank == null ? null : removeOpenBank.trim();
	}

	public String getRemoveAccnum() {
		return removeAccnum;
	}

	public void setRemoveAccnum(String removeAccnum) {
		this.removeAccnum = removeAccnum;
	}

	public String getRemovePsnum() {
		return removePsnum;
	}

	public void setRemovePsnum(String removePsnum) {
		this.removePsnum = removePsnum;
	}

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Override
	public String getCustNo() {
		return custNo;
	}

	@Override
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getDV01Risk() {
		return DV01Risk;
	}

	public void setDV01Risk(String dV01Risk) {
		this.DV01Risk = dV01Risk;
	}

	public String getLossLimit() {
		return lossLimit;
	}

	public void setLossLimit(String lossLimit) {
		this.lossLimit = lossLimit;
	}

	public String getLongLimit() {
		return longLimit;
	}

	public void setLongLimit(String longLimit) {
		this.longLimit = longLimit;
	}

	public String getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}

	public String getTradingProduct() {
		return tradingProduct == null ? "" : tradingProduct;
	}

	public void setTradingProduct(String tradingProduct) {
		this.tradingProduct = tradingProduct == null ? "" : tradingProduct;
	}

	public String getPriceDeviation() {
		return priceDeviation == null ? "" : priceDeviation;
	}

	public void setPriceDeviation(String priceDeviation) {
		this.priceDeviation = priceDeviation == null ? "" : priceDeviation;
	}

	@Override
	public String getTicketId() {
		return ticketId;
	}

	@Override
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	@Override
	public String getDealTransType() {
		return dealTransType;
	}

	@Override
	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getCounterpartyInstId() {
		return counterpartyInstId;
	}

	public void setCounterpartyInstId(String counterpartyInstId) {
		this.counterpartyInstId = counterpartyInstId;
	}

	public String getDealer() {
		return dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	public String getCounterpartyDealer() {
		return counterpartyDealer;
	}

	public void setCounterpartyDealer(String counterpartyDealer) {
		this.counterpartyDealer = counterpartyDealer;
	}

	public String getCfetsno() {
		return cfetsno;
	}

	public void setCfetsno(String cfetsno) {
		this.cfetsno = cfetsno;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getAccountNature() {
		return accountNature;
	}

	public void setAccountNature(String accountNature) {
		this.accountNature = accountNature;
	}

	public String getAccountUse() {
		return accountUse;
	}

	public void setAccountUse(String accountUse) {
		this.accountUse = accountUse;
	}
}