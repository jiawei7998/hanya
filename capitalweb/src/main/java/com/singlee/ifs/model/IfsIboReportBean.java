package com.singlee.ifs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IfsIboReportBean implements Serializable{

	/**
	 * 统计拆借业务交易的报表字段
	 */
	private static final long serialVersionUID = -2827017115423221218L;
	
	//年
	private String year;
	
	//月
	private String month;
	
	private String ps;
	
	//数量
	private String pnum;
	
	//金额
	private BigDecimal pamt;
	
	//加权平均
	private BigDecimal pavg;
	
	private String snum;
	
	private BigDecimal samt;
	
	private BigDecimal savg;
	
	private String sdate;
	
	private String edate;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getPnum() {
		return pnum;
	}

	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public BigDecimal getPamt() {
		return pamt;
	}

	public void setPamt(BigDecimal pamt) {
		this.pamt = pamt;
	}

	public BigDecimal getPavg() {
		return pavg;
	}

	public void setPavg(BigDecimal pavg) {
		this.pavg = pavg;
	}

	public String getSnum() {
		return snum;
	}

	public void setSnum(String snum) {
		this.snum = snum;
	}

	public BigDecimal getSamt() {
		return samt;
	}

	public void setSamt(BigDecimal samt) {
		this.samt = samt;
	}

	public BigDecimal getSavg() {
		return savg;
	}

	public void setSavg(BigDecimal savg) {
		this.savg = savg;
	}

	public String getSdate() {
		return sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}
	
	
	

}
