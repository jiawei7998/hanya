package com.singlee.ifs.model;

import com.singlee.financial.bean.SlCspiCsriBean;
import com.singlee.financial.bean.SlCspiCsriDetailBean;
import com.singlee.financial.bean.SlSdvpBean;
import com.singlee.financial.bean.SlSdvpDetailBean;

import java.io.Serializable;
import java.util.List;

/**
 * 流程审批监控类
 */
public class IfsSearchSettleBean implements Serializable {
	
	private static final long serialVersionUID = 6658942447540415325L;

	/**
	 * 部门
	 */
	private String br;

	/**
	 * 成交单编号
	 */
	private String ticketid;

	/**
	 * 产品代码
	 */
	private String product;

	/**
	 * 产品类型
	 */
	private String prodtype;

	/**
	 * 付款币种
	 */
	private String pccy;

	/**
	 * 收款币种
	 */
	private String rccy;

	/**
	 * 交易对手编号
	 */
	private String cno;

	/**
	 * 交易日期
	 */
	private String fordate;

	/**
	 * ifs_cspicsri_head表
	 */
	private List<SlCspiCsriBean> list;

	/**
	 * 付款ifs_cspicsri_detail表
	 */
	private List<SlCspiCsriDetailBean> payDetailList;

	/**
	 * 收款ifs_cspicsri_detail表
	 */
	private List<SlCspiCsriDetailBean> recDetailList;

	/**
	 * 掉期远端付款明细
	 */
	private List<SlCspiCsriDetailBean> remotePayDetailList;

	/**
	 * 掉期远端收款明细
	 */
	private List<SlCspiCsriDetailBean> remoteRecDetailList;

	/**
	 * 托管账户
	 */
	private String safekeepacct;

	/**
	 * ifs_Sdvp_head表
	 */
	private List<SlSdvpBean> SlSdvpBean;

	/**
	 * 付款ifs_Sdvp_Detail表
	 */
	private List<SlSdvpDetailBean> payIfsSdvpDetail;

	/**
	 * 收款ifs_Sdvp_Detail表
	 */
	private List<SlSdvpDetailBean> recIfsSdvpDetail;

	/**
	 * 本方方向
	 */
	private String mydircn;

	/**
	 * 产品名称
	 */
	private String prdname;

	/**
	 * 净额清算状态
	 */
	private String nettingstatus;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getTicketid() {
		return ticketid;
	}

	public void setTicketid(String ticketid) {
		this.ticketid = ticketid;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getFordate() {
		return fordate;
	}

	public void setFordate(String fordate) {
		this.fordate = fordate;
	}

	public List<SlCspiCsriBean> getList() {
		return list;
	}

	public void setList(List<SlCspiCsriBean> list) {
		this.list = list;
	}

	public void setPccy(String pccy) {
		this.pccy = pccy;
	}

	public String getPccy() {
		return pccy;
	}

	public void setRccy(String rccy) {
		this.rccy = rccy;
	}

	public String getRccy() {
		return rccy;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public List<SlCspiCsriDetailBean> getPayDetailList() {
		return payDetailList;
	}

	public void setPayDetailList(List<SlCspiCsriDetailBean> payDetailList) {
		this.payDetailList = payDetailList;
	}

	public List<SlCspiCsriDetailBean> getRecDetailList() {
		return recDetailList;
	}

	public void setRecDetailList(List<SlCspiCsriDetailBean> recDetailList) {
		this.recDetailList = recDetailList;
	}

	public List<SlSdvpBean> getIfsSdvpHead() {
		return SlSdvpBean;
	}

	public void setIfsSdvpHead(List<SlSdvpBean> SlSdvpBean) {
		this.SlSdvpBean = SlSdvpBean;
	}

	public List<SlSdvpDetailBean> getPayIfsSdvpDetail() {
		return payIfsSdvpDetail;
	}

	public void setPayIfsSdvpDetail(List<SlSdvpDetailBean> payIfsSdvpDetail) {
		this.payIfsSdvpDetail = payIfsSdvpDetail;
	}

	public List<SlSdvpDetailBean> getRecIfsSdvpDetail() {
		return recIfsSdvpDetail;
	}

	public void setRecIfsSdvpDetail(List<SlSdvpDetailBean> recIfsSdvpDetail) {
		this.recIfsSdvpDetail = recIfsSdvpDetail;
	}

	public String getMydircn() {
		return mydircn;
	}

	public void setMydircn(String mydircn) {
		this.mydircn = mydircn;
	}

	public String getPrdname() {
		return prdname;
	}

	public void setPrdname(String prdname) {
		this.prdname = prdname;
	}

	public String getNettingstatus() {
		return nettingstatus;
	}

	public void setNettingstatus(String nettingstatus) {
		this.nettingstatus = nettingstatus;
	}

	public void setRemotePayDetailList(List<SlCspiCsriDetailBean> remotePayDetailList) {
		this.remotePayDetailList = remotePayDetailList;
	}

	public List<SlCspiCsriDetailBean> getRemotePayDetailList() {
		return remotePayDetailList;
	}

	public void setRemoteRecDetailList(List<SlCspiCsriDetailBean> remoteRecDetailList) {
		this.remoteRecDetailList = remoteRecDetailList;
	}

	public List<SlCspiCsriDetailBean> getRemoteRecDetailList() {
		return remoteRecDetailList;
	}
}