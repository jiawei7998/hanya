package com.singlee.ifs.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class IfsFlowApproveBean implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 3050430230831854783L;
	/**
	 * 业务编号
	 */
	private String serialNo;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 操作类型:1:同意继续 2-跳转
	 */
	private String opType;
	/**
	 * 审批备注
	 */
	private String msg;
	/**
	 * 特殊用户,用于邀请进入审批
	 */
	private String specificUser;
	/**
	 * 撤回
	 */
	private String backTo;
	/**
	 * 流程类型
	 */
	private String productType;
	/**
	 * 产品类型
	 */
	private String flowType;
	/**
	 * 流程定义ID
	 */
	private String flowId;
	/**
	 * 限额类型
	 */
	private String limitLevel;
	/**
	 * 审批流程状态变更监听service
	 */
	private String callBackService;
	/**
	 * 订单状态
	 */
	private String orderStatus;


	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getOpType() {
		return opType;
	}

	public void setOpType(String opType) {
		this.opType = opType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSpecificUser() {
		return specificUser;
	}

	public void setSpecificUser(String specificUser) {
		this.specificUser = specificUser;
	}

	public String getBackTo() {
		return backTo;
	}

	public void setBackTo(String backTo) {
		this.backTo = backTo;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getFlowType() {
		return flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getLimitLevel() {
		return limitLevel;
	}

	public void setLimitLevel(String limitLevel) {
		this.limitLevel = limitLevel;
	}

	public String getCallBackService() {
		return callBackService;
	}

	public void setCallBackService(String callBackService) {
		this.callBackService = callBackService;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Map<String, String> getTransMap() {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("serial_no", serialNo);
		paramMap.put("task_id", taskId);
		paramMap.put("op_type", opType);
		paramMap.put("msg", msg);
		paramMap.put("specific_user", specificUser);
		paramMap.put("back_to", backTo);
		paramMap.put("product_type", productType);
		paramMap.put("flow_type", flowType);
		paramMap.put("flow_td", flowId);
		paramMap.put("limit_level", limitLevel);
		paramMap.put("callBackService", callBackService);
		paramMap.put("order_status", orderStatus);
		return paramMap;
	}
}
