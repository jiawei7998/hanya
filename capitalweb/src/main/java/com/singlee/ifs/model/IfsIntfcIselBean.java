package com.singlee.ifs.model;

import java.io.Serializable;


public class IfsIntfcIselBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//分支机构
	private String br;
	//债券代码
	private String secid;
	//净价
	private String clsgprice_8;
	//买入价格
	private String askprice_8;
	//卖出价格
	private String bidprice_8;
	//债券名称
	private String descr;
	//备注
	private String remark;
	//日期
	private String lstmntdte;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getClsgprice_8() {
		return clsgprice_8;
	}
	public void setClsgprice_8(String clsgprice_8) {
		this.clsgprice_8 = clsgprice_8;
	}
	public String getAskprice_8() {
		return askprice_8;
	}
	public void setAskprice_8(String askprice_8) {
		this.askprice_8 = askprice_8;
	}
	public String getBidprice_8() {
		return bidprice_8;
	}
	public void setBidprice_8(String bidprice_8) {
		this.bidprice_8 = bidprice_8;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	
	

}
