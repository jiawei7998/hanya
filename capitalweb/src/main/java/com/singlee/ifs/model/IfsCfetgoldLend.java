package com.singlee.ifs.model;

import javax.persistence.Transient;
import java.math.BigDecimal;

public class IfsCfetgoldLend extends IfsBaseFlow {
	private static final long serialVersionUID = 1L;
	private String ticketId;
	//询价成交单编号
	private String dealTransType;
	//交易状态
	private String myMember;
	//本方会员
	private String myCustomer;
	//本方客户
	private String myBusinessPerson;
	//本方交易员
	private String myDirection;
	//本方买卖方向
	private String forDate;
	//成交日期
	private String marketType;
	//市场类型
	private String member;
	//对方会员
	private String customer;
	//对方客户
	private String businessPerson;
	//对方交易员
	private String direction;
	//对方买卖方向
	private String forTime;
	//成交时间
	private String liquidation;
	//清算方式
	private BigDecimal standardWeight;
	//拆借标重（千克）
	private BigDecimal annualRate;
	//拆借年利率（%）
	private String finalDate;
	//拆借到期日
	private String tradingVariety;
	//交易品种
	private String timeLimit;
	//拆借期限
	private String valueDate;
	//拆借起息日
	private String interestDay;
	//拆借付息日
	private String additionalTreaty;
	//附加条约
	private String nominalPrincipal;
	//名义本金
	private String rateBenchmarks;
	//拆借计息基准
	private String benchmarkPrice;
	//计息基准价（元/克）
	private String accrual;
	//利息
	private String accrualType;
	//过户类型
	private String borrowWarehouse;
	//借金仓库
	private String returnWarehouse;
	//还金仓库
	private String goldType;
	//还金交割品种
	private String borrowType;
	//拆借类型
	private String beforeId;
	//前接成交单编号
	
	
	@Transient
	private String  prdNo;
	
	
	
	public String getPrdNo() {
		return prdNo;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId == null ? null : ticketId.trim();
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType == null ? null : dealTransType.trim();
	}

	public String getMyMember() {
		return myMember;
	}

	public void setMyMember(String myMember) {
		this.myMember = myMember == null ? null : myMember.trim();
	}

	public String getMyCustomer() {
		return myCustomer;
	}

	public void setMyCustomer(String myCustomer) {
		this.myCustomer = myCustomer == null ? null : myCustomer.trim();
	}


	public String getMyDirection() {
		return myDirection;
	}

	public void setMyDirection(String myDirection) {
		this.myDirection = myDirection == null ? null : myDirection.trim();
	}

	public String getForDate() {
		return forDate;
	}

	public void setForDate(String forDate) {
		this.forDate = forDate == null ? null : forDate.trim();
	}

	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType == null ? null : marketType.trim();
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member == null ? null : member.trim();
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer == null ? null : customer.trim();
	}


	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction == null ? null : direction.trim();
	}

	public String getForTime() {
		return forTime;
	}

	public void setForTime(String forTime) {
		this.forTime = forTime == null ? null : forTime.trim();
	}

	public String getLiquidation() {
		return liquidation;
	}

	public void setLiquidation(String liquidation) {
		this.liquidation = liquidation == null ? null : liquidation.trim();
	}

	public BigDecimal getStandardWeight() {
		return standardWeight;
	}

	public void setStandardWeight(BigDecimal standardWeight) {
		this.standardWeight = standardWeight ;
	}

	public BigDecimal getAnnualRate() {
		return annualRate;
	}

	public void setAnnualRate(BigDecimal annualRate) {
		this.annualRate = annualRate ;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate == null ? null : finalDate.trim();
	}

	public String getTradingVariety() {
		return tradingVariety;
	}

	public void setTradingVariety(String tradingVariety) {
		this.tradingVariety = tradingVariety == null ? null : tradingVariety.trim();
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit == null ? null : timeLimit.trim();
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate == null ? null : valueDate.trim();
	}

	public String getInterestDay() {
		return interestDay;
	}

	public void setInterestDay(String interestDay) {
		this.interestDay = interestDay == null ? null : interestDay.trim();
	}

	public String getAdditionalTreaty() {
		return additionalTreaty;
	}

	public void setAdditionalTreaty(String additionalTreaty) {
		this.additionalTreaty = additionalTreaty == null ? null : additionalTreaty.trim();
	}

	public String getNominalPrincipal() {
		return nominalPrincipal;
	}

	public void setNominalPrincipal(String nominalPrincipal) {
		this.nominalPrincipal = nominalPrincipal == null ? null : nominalPrincipal.trim();
	}

	public String getRateBenchmarks() {
		return rateBenchmarks;
	}

	public void setRateBenchmarks(String rateBenchmarks) {
		this.rateBenchmarks = rateBenchmarks == null ? null : rateBenchmarks.trim();
	}

	public String getBenchmarkPrice() {
		return benchmarkPrice;
	}

	public void setBenchmarkPrice(String benchmarkPrice) {
		this.benchmarkPrice = benchmarkPrice == null ? null : benchmarkPrice.trim();
	}

	public String getAccrual() {
		return accrual;
	}

	public void setAccrual(String accrual) {
		this.accrual = accrual == null ? null : accrual.trim();
	}

	public String getAccrualType() {
		return accrualType;
	}

	public void setAccrualType(String accrualType) {
		this.accrualType = accrualType == null ? null : accrualType.trim();
	}

	public String getBorrowWarehouse() {
		return borrowWarehouse;
	}

	public void setBorrowWarehouse(String borrowWarehouse) {
		this.borrowWarehouse = borrowWarehouse == null ? null : borrowWarehouse.trim();
	}

	public String getReturnWarehouse() {
		return returnWarehouse;
	}

	public void setReturnWarehouse(String returnWarehouse) {
		this.returnWarehouse = returnWarehouse == null ? null : returnWarehouse.trim();
	}

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType == null ? null : goldType.trim();
	}

	public String getBorrowType() {
		return borrowType;
	}

	public void setBorrowType(String borrowType) {
		this.borrowType = borrowType == null ? null : borrowType.trim();
	}

	public String getBeforeId() {
		return beforeId;
	}

	public void setBeforeId(String beforeId) {
		this.beforeId = beforeId == null ? null : beforeId.trim();
	}

	public String getMyBusinessPerson() {
		return myBusinessPerson;
	}

	public void setMyBusinessPerson(String myBusinessPerson) {
		this.myBusinessPerson = myBusinessPerson == null ? null : myBusinessPerson.trim();
	}

	public String getBusinessPerson() {
		return businessPerson;
	}

	public void setBusinessPerson(String businessPerson) {
		this.businessPerson = businessPerson == null ? null : businessPerson.trim();
	}
}