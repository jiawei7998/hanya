package com.singlee.ifs.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity	
@Table(name = "TT_FXBASIC")
public class TtFxBasic implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * 币种
	 */
	private String currency;
	
	/**
	 * 开户银行名称
	 */
	private String opBank;
	
	/**
	 * 收款行名称
	 */
	private String reBank;
	
	/**
	 * 中间行名称
	 */
	private String interBank;
	
	/**
	 * 开户行账号
	 */
	private String opAccount;
	
	/**
	 * 收款行账号
	 */
	private String reAccount;
	
	/**
	 * 中间行账号
	 */
	private String interAccount;
	
	/**
	 * 开户行biccode
	 */
	private String opBicCode;
	
	/**
	 * 收款行biccode
	 */
	private String reBicCode;
	
	/**
	 * 中间行biccode
	 */
	private String interBicCode;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOpBank() {
		return opBank;
	}

	public void setOpBank(String opBank) {
		this.opBank = opBank;
	}

	public String getReBank() {
		return reBank;
	}

	public void setReBank(String reBank) {
		this.reBank = reBank;
	}

	public String getInterBank() {
		return interBank;
	}

	public void setInterBank(String interBank) {
		this.interBank = interBank;
	}

	public String getOpAccount() {
		return opAccount;
	}

	public void setOpAccount(String opAccount) {
		this.opAccount = opAccount;
	}

	public String getReAccount() {
		return reAccount;
	}

	public void setReAccount(String reAccount) {
		this.reAccount = reAccount;
	}

	public String getInterAccount() {
		return interAccount;
	}

	public void setInterAccount(String interAccount) {
		this.interAccount = interAccount;
	}

	public String getOpBicCode() {
		return opBicCode;
	}

	public void setOpBicCode(String opBicCode) {
		this.opBicCode = opBicCode;
	}

	public String getReBicCode() {
		return reBicCode;
	}

	public void setReBicCode(String reBicCode) {
		this.reBicCode = reBicCode;
	}

	public String getInterBicCode() {
		return interBicCode;
	}

	public void setInterBicCode(String interBicCode) {
		this.interBicCode = interBicCode;
	}
	
	
	
}