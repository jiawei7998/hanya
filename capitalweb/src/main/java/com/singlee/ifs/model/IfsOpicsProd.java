package com.singlee.ifs.model;

import com.singlee.capital.common.util.TreeInterface;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/***
 * opics 产品表
 *
 */

public class IfsOpicsProd implements Serializable, TreeInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 产品代码
	 */
	private String pcode;
	/**
	 * 产品描述
	 */
	private String pdesc;
	/**
	 * 所属模块
	 */
	private String sys;
	
	/**
	 * 最后维护时间
	 */
	private Date lstmntdte;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 处理状态
	 */
	private String status;
	
	/**
	 * 产品流水号
	 */
	private String psn;
	
	/**
	 * 产品父ID流水号
	 */
	private String ppid;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 产品中文描述
	 */
	private String pdesccn;
	
	
	@Transient
	private List<IfsOpicsProd> children;

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public String getSys() {
		return sys;
	}

	public void setSys(String sys) {
		this.sys = sys;
	}




	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPsn() {
		return psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	public String getPpid() {
		return ppid;
	}

	public void setPpid(String ppid) {
		this.ppid = ppid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPdesccn() {
		return pdesccn;
	}

	public void setPdesccn(String pdesccn) {
		this.pdesccn = pdesccn;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	@Override
	public String getId() {
		return getPsn();
	}

	@Override
	public String getParentId() {
		return getPpid();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<IfsOpicsProd>) list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}

	@Override
	public String getText() {
		return pcode;
	}

	@Override
	public String getValue() {
		return ppid;
	}

	@Override
	public int getSort() {
		return StringUtils.length(getPsn());
	}

	@Override
	public String getIconCls() {
		return "";
	}

	public List<IfsOpicsProd> getChildren() {
		return children;
	}

	public void setChildren(List<IfsOpicsProd> children) {
		this.children = children;
	}
	

}
