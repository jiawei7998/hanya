package com.singlee.ifs.model;

import java.io.Serializable;

/**
 * 总账进程记录
 * 
 * @author singlee
 * 
 * @date 2019-05-20
 */
public class IfsBatchStep implements Serializable {
	/**
	 * 主键
	 */
	private String id;

	/**
	 * 部门
	 */
	private String br;

	/**
	 * job名称
	 */
	private String jobname;

	/**
	 * job类型
	 */
	private String jobtype;

	/**
	 * 任务编号
	 */
	private String batId;

	/**
	 * 任务名称
	 */
	private String batName;

	/**
	 * 执行结果 已完成/已失败/等待中
	 */
	private String batStatus;

	/**
	 * 日期
	 */
	private String batDate;

	/**
	 * 任务返回码
	 */
	private String retCode;

	/**
	 * 任务返回信息
	 */
	private String retMsg;

	/**
	 * 执行结果日志描述
	 */
	private String batLog;

	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结束时间
	 */
	private String endTime;

	/**
	 * 运行时间
	 */
	private int execTime;

	/**
	 * 数据库时间
	 */
	private String dbTime;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br == null ? null : br.trim();
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname == null ? null : jobname.trim();
	}

	public String getJobtype() {
		return jobtype;
	}

	public void setJobtype(String jobtype) {
		this.jobtype = jobtype == null ? null : jobtype.trim();
	}

	public String getBatId() {
		return batId;
	}

	public void setBatId(String batId) {
		this.batId = batId == null ? null : batId.trim();
	}

	public String getBatName() {
		return batName;
	}

	public void setBatName(String batName) {
		this.batName = batName == null ? null : batName.trim();
	}

	public String getBatStatus() {
		return batStatus;
	}

	public void setBatStatus(String batStatus) {
		this.batStatus = batStatus == null ? null : batStatus.trim();
	}

	public String getBatDate() {
		return batDate;
	}

	public void setBatDate(String batDate) {
		this.batDate = batDate == null ? null : batDate.trim();
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode == null ? null : retCode.trim();
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg == null ? null : retMsg.trim();
	}

	public String getBatLog() {
		return batLog;
	}

	public void setBatLog(String batLog) {
		this.batLog = batLog == null ? null : batLog.trim();
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime == null ? null : startTime.trim();
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime == null ? null : endTime.trim();
	}

	public int getExecTime() {
		return execTime;
	}

	public void setExecTime(int execTime) {
		this.execTime = execTime;
	}

	public String getDbTime() {
		return dbTime;
	}

	public void setDbTime(String dbTime) {
		this.dbTime = dbTime == null ? null : dbTime.trim();
	}
}