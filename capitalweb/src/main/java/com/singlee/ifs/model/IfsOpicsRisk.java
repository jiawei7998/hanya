package com.singlee.ifs.model;

/***
 * 单笔风险审查表
 */
public class IfsOpicsRisk {

	/** 准入流水号 */
	private String dealNo;

	/** 审批单号 */
	private String applyNo;

	/** 审批类别 */
	private String applyType;

	/** 状态 */
	private String status;

	/** 提交用户 */
	private String applyOper;

	/** 产品名称 */
	private String applyProd;

	/** 收益率 */
	private String yield;

	/**  拟投资规模 */
	private String remakr;

	/** 有效到期日 */
	private String applyDate;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyType() {
		return applyType;
	}

	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApplyOper() {
		return applyOper;
	}

	public void setApplyOper(String applyOper) {
		this.applyOper = applyOper;
	}

	public String getApplyProd() {
		return applyProd;
	}

	public void setApplyProd(String applyProd) {
		this.applyProd = applyProd;
	}

	public String getYield() {
		return yield;
	}

	public void setYield(String yield) {
		this.yield = yield;
	}

	public String getRemakr() {
		return remakr;
	}

	public void setRemakr(String remakr) {
		this.remakr = remakr;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
}