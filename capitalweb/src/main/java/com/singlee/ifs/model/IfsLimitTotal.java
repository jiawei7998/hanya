package com.singlee.ifs.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 限额总表--哈尔滨
 */
@Table(name = "IFS_LIMIT_TOTAL")
public class IfsLimitTotal implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 限额流水号
     */
    @Column(name = "LIMIT_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_IFS_LIMIT_TOTAL.NEXTVAL FROM DUAL")
    private String limitId;

    /**
     * Opics债券类型
     */
    @Column(name = "ACCTNGTYPE")
    private String acctngtype;

    /**
     * Opics债券类型名称
     */
    @Column(name = "ACCTNGTYPE_NAME")
    private String acctngtypeName;
    /**
     * 账户类型
     */
    @Column(name = "ACCTYPE")
    private String accType;

    /**
     * 限额上线
     */
    @Column(name = "AVAIL_AMOUNT")
    private String availAmount;

    /**
     * 美元下线
     */
    @Column(name = "AVAIL_AMOUNT_U")
    private String availAmountU;

    /**
     * 债券类型
     */
    @Column(name = "BOND_TYPE")
    private String bondType;

    /**
     * 币种
     */
    @Column(name = "CCY")
    private String ccy;

    /**
     * 币种编码
     */
    @Column(name = "CCY_CODE")
    private String ccyCode;

    /**
     * 成本中心
     */
    @Column(name = "COST")
    private String cost;

    /**
     * 成本中心名字
     */
    @Column(name = "COST_NAME")
    private String costName;

    /**
     * 检测控制(0-监测 1-监控)
     */
    @Column(name = "CTL_TYPE")
    private String ctlType;

    /**
     * 交易来源
     */
    @Column(name = "DEAL_SOURCE")
    private String dealSource;

    /**
     * 交易方向
     */
    @Column(name = "DIRECTION")
    private String direction;

    /**
     * 经办时间
     */
    @Column(name = "ITIME")
    private String itime;

    /**
     * 累计笔数
     */
    @Column(name = "LIMIT_COUNT")
    private String limitCount;



    /**
     * 限额类型
     */
    @Column(name = "LIMIT_TYPE")
    private String limitType;

    /**
     * 经办人
     */
    @Column(name = "OPERATE_USER")
    private String operateUser;

    /**
     * 当日限额时间
     */
    @Column(name = "OPR_DAY_TIME")
    private String oprDayTime;

    /**
     * 产品
     */
    @Column(name = "PRD")
    private String prd;

    /**
     * 产品编码
     */
    @Column(name = "PRD_CODE")
    private String prdCode;

    /**
     * 产品类型
     */
    @Column(name = "PRD_TYPE")
    private String prdType;

    /**
     * 产品类型编码
     */
    @Column(name = "PRD_TYPE_CODE")
    private String prdTypeCode;

    /**
     * 限额下线
     */
    @Column(name = "QUOTA_AMOUNT")
    private String quotaAmount;

    /**
     * 美元上线
     */
    @Column(name = "QUOTA_AMOUNT_U")
    private String quotaAmountU;

    /**
     * 汇率
     */
    @Column(name = "RATE")
    private String rate;

    /**
     * 剩余期限
     */
    @Column(name = "TERM")
    private BigDecimal term;

    /**
     * 已经使用的额度，累计
     */
    @Column(name = "TOTAL_AMOUNT")
    private String totalAmount;

    /**
     * 交易员
     */
    @Column(name = "TRAD")
    private String trad;

    /**
     * 交易员姓名
     */
    @Column(name = "TRAD_NAME")
    private String tradName;

    /**
     * 限额币种
     */
    @Column(name = "CURRENCY")
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * 获取Opics债券类型
     *
     * @return ACCTNGTYPE - Opics债券类型
     */
    public String getAcctngtype() {
        return acctngtype;
    }

    /**
     * 设置Opics债券类型
     *
     * @param acctngtype Opics债券类型
     */
    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }

    /**
     * 获取限额上线
     *
     * @return AVAIL_AMOUNT - 限额上线
     */
    public String getAvailAmount() {
        return availAmount;
    }

    /**
     * 设置限额上线
     *
     * @param availAmount 限额上线
     */
    public void setAvailAmount(String availAmount) {
        this.availAmount = availAmount;
    }

    /**
     * 获取美元下线
     *
     * @return AVAIL_AMOUNT_U - 美元下线
     */
    public String getAvailAmountU() {
        return availAmountU;
    }

    /**
     * 设置美元下线
     *
     * @param availAmountU 美元下线
     */
    public void setAvailAmountU(String availAmountU) {
        this.availAmountU = availAmountU;
    }

    /**
     * 获取债券类型
     *
     * @return BOND_TYPE - 债券类型
     */
    public String getBondType() {
        return bondType;
    }

    /**
     * 设置债券类型
     *
     * @param bondType 债券类型
     */
    public void setBondType(String bondType) {
        this.bondType = bondType;
    }

    /**
     * 获取币种
     *
     * @return CCY - 币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 设置币种
     *
     * @param ccy 币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    /**
     * 获取币种编码
     *
     * @return CCY_CODE - 币种编码
     */
    public String getCcyCode() {
        return ccyCode;
    }

    /**
     * 设置币种编码
     *
     * @param ccyCode 币种编码
     */
    public void setCcyCode(String ccyCode) {
        this.ccyCode = ccyCode;
    }

    /**
     * 获取成本中心
     *
     * @return COST - 成本中心
     */
    public String getCost() {
        return cost;
    }

    /**
     * 设置成本中心
     *
     * @param cost 成本中心
     */
    public void setCost(String cost) {
        this.cost = cost;
    }

    /**
     * 获取成本中心名字
     *
     * @return COST_NAME - 成本中心名字
     */
    public String getCostName() {
        return costName;
    }

    /**
     * 设置成本中心名字
     *
     * @param costName 成本中心名字
     */
    public void setCostName(String costName) {
        this.costName = costName;
    }

    /**
     * 获取检测控制(0-监测 1-监控)
     *
     * @return CTL_TYPE - 检测控制(0-监测 1-监控)
     */
    public String getCtlType() {
        return ctlType;
    }

    /**
     * 设置检测控制(0-监测 1-监控)
     *
     * @param ctlType 检测控制(0-监测 1-监控)
     */
    public void setCtlType(String ctlType) {
        this.ctlType = ctlType;
    }

    /**
     * 获取交易来源
     *
     * @return DEAL_SOURCE - 交易来源
     */
    public String getDealSource() {
        return dealSource;
    }

    /**
     * 设置交易来源
     *
     * @param dealSource 交易来源
     */
    public void setDealSource(String dealSource) {
        this.dealSource = dealSource;
    }

    /**
     * 获取交易方向
     *
     * @return DIRECTION - 交易方向
     */
    public String getDirection() {
        return direction;
    }

    /**
     * 设置交易方向
     *
     * @param direction 交易方向
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * 获取经办时间
     *
     * @return ITIME - 经办时间
     */
    public String getItime() {
        return itime;
    }

    /**
     * 设置经办时间
     *
     * @param itime 经办时间
     */
    public void setItime(String itime) {
        this.itime = itime;
    }

    /**
     * 获取累计笔数
     *
     * @return LIMIT_COUNT - 累计笔数
     */
    public String getLimitCount() {
        return limitCount;
    }

    /**
     * 设置累计笔数
     *
     * @param limitCount 累计笔数
     */
    public void setLimitCount(String limitCount) {
        this.limitCount = limitCount;
    }

    /**
     * 获取限额流水号
     *
     * @return LIMIT_ID - 限额流水号
     */
    public String getLimitId() {
        return limitId;
    }

    /**
     * 设置限额流水号
     *
     * @param limitId 限额流水号
     */
    public void setLimitId(String limitId) {
        this.limitId = limitId;
    }

    /**
     * 获取限额类型
     *
     * @return LIMIT_TYPE - 限额类型
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * 设置限额类型
     *
     * @param limitType 限额类型
     */
    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    /**
     * 获取经办人
     *
     * @return OPERATE_USER - 经办人
     */
    public String getOperateUser() {
        return operateUser;
    }

    /**
     * 设置经办人
     *
     * @param operateUser 经办人
     */
    public void setOperateUser(String operateUser) {
        this.operateUser = operateUser;
    }

    /**
     * 获取当日限额时间
     *
     * @return OPR_DAY_TIME - 当日限额时间
     */
    public String getOprDayTime() {
        return oprDayTime;
    }

    /**
     * 设置当日限额时间
     *
     * @param oprDayTime 当日限额时间
     */
    public void setOprDayTime(String oprDayTime) {
        this.oprDayTime = oprDayTime;
    }

    /**
     * 获取产品
     *
     * @return PRD - 产品
     */
    public String getPrd() {
        return prd;
    }

    /**
     * 设置产品
     *
     * @param prd 产品
     */
    public void setPrd(String prd) {
        this.prd = prd;
    }

    /**
     * 获取产品编码
     *
     * @return PRD_CODE - 产品编码
     */
    public String getPrdCode() {
        return prdCode;
    }

    /**
     * 设置产品编码
     *
     * @param prdCode 产品编码
     */
    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    /**
     * 获取产品类型
     *
     * @return PRD_TYPE - 产品类型
     */
    public String getPrdType() {
        return prdType;
    }

    /**
     * 设置产品类型
     *
     * @param prdType 产品类型
     */
    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    /**
     * 获取产品类型编码
     *
     * @return PRD_TYPE_CODE - 产品类型编码
     */
    public String getPrdTypeCode() {
        return prdTypeCode;
    }

    /**
     * 设置产品类型编码
     *
     * @param prdTypeCode 产品类型编码
     */
    public void setPrdTypeCode(String prdTypeCode) {
        this.prdTypeCode = prdTypeCode;
    }

    /**
     * 获取限额下线
     *
     * @return QUOTA_AMOUNT - 限额下线
     */
    public String getQuotaAmount() {
        return quotaAmount;
    }

    /**
     * 设置限额下线
     *
     * @param quotaAmount 限额下线
     */
    public void setQuotaAmount(String quotaAmount) {
        this.quotaAmount = quotaAmount;
    }

    /**
     * 获取美元上线
     *
     * @return QUOTA_AMOUNT_U - 美元上线
     */
    public String getQuotaAmountU() {
        return quotaAmountU;
    }

    /**
     * 设置美元上线
     *
     * @param quotaAmountU 美元上线
     */
    public void setQuotaAmountU(String quotaAmountU) {
        this.quotaAmountU = quotaAmountU;
    }

    /**
     * 获取汇率
     *
     * @return RATE - 汇率
     */
    public String getRate() {
        return rate;
    }

    /**
     * 设置汇率
     *
     * @param rate 汇率
     */
    public void setRate(String rate) {
        this.rate = rate;
    }

    /**
     * 获取剩余期限
     *
     * @return TERM - 剩余期限
     */
    public BigDecimal getTerm() {
        return term;
    }

    /**
     * 设置剩余期限
     *
     * @param term 剩余期限
     */
    public void setTerm(BigDecimal term) {
        this.term = term;
    }

    /**
     * 获取已经使用的额度，累计
     *
     * @return TOTAL_AMOUNT - 已经使用的额度，累计
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * 设置已经使用的额度，累计
     *
     * @param totalAmount 已经使用的额度，累计
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * 获取交易员
     *
     * @return TRAD - 交易员
     */
    public String getTrad() {
        return trad;
    }

    /**
     * 设置交易员
     *
     * @param trad 交易员
     */
    public void setTrad(String trad) {
        this.trad = trad;
    }

    /**
     * 获取交易员姓名
     *
     * @return TRAD_NAME - 交易员姓名
     */
    public String getTradName() {
        return tradName;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    /**
     * 设置交易员姓名
     *
     * @param tradName 交易员姓名
     */
    public void setTradName(String tradName) {
        this.tradName = tradName;
    }

    public String getAcctngtypeName() {
        return acctngtypeName;
    }

    public void setAcctngtypeName(String acctngtypeName) {
        this.acctngtypeName = acctngtypeName;
    }
}
